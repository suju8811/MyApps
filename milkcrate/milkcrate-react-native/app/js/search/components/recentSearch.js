'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  ListView,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView,
  Alert,
  Platform,
  AsyncStorage
} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { bindActionCreators } from 'redux';
import * as searchActions from '../actions';
import * as commonActions from '../../common/actions';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import Cache from '../../components/Cache'
import UtilService from '../../components/util'
import bendService from '../../bend/bendService'

import BusinessesListCell from '../components/businessesListCell';
import EventsListCell from '../components/eventsListCell';
import RecentSearchCell from '../components/recentSearchCell';

import * as commonColors from '../../styles/commonColors';
import  * as commonStyles from '../../styles/commonStyles';

class RecentSearch extends Component {
  constructor(props) {
    super(props);

    this.dataSource = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      searchHistory:Cache.searchHistory
    };
    this.searchText = ""
  }

  componentDidMount() {
    this.hasMounted = true
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  renderRecentSearchRow(rowData, sectionID, rowID) {
    return (
      <RecentSearchCell
        title={ rowData.searchText }
        time={ rowData.time * 1000000 }
        onClick={ () => {
          this.props.commonActions.setSearchText(rowData.searchText);
          this.props.onSearch(rowData.searchText)

          setTimeout(()=>{
            this.props.commonActions.reset()
          }, 1000)
        } }
      />
    );
  }

  clearHistory() {
    Cache.searchHistory = []
    this.setState({
      searchHistory:Cache.searchHistory
    })
    AsyncStorage.setItem('@milkcrate:searchHistory', JSON.stringify(Cache.searchHistory));
  }

  get showRecentSearch() {
    let recentSearchList = this.state.searchHistory
    return (
      recentSearchList.length ?
        <View>
          <View style={ styles.sectionHeaderContainer }>
            <View style={{flex:1}}>
              <Text style={ styles.textSectionTitle }>{'Recent Searches'}</Text>
            </View>
            <TouchableOpacity style={ styles.buttonClear } onPress={ () => this.clearHistory() }>
              <Text style={ styles.textClear}>Clear Search History</Text>
            </TouchableOpacity>
          </View>
          <ListView
            enableEmptySections={ true }
            dataSource={ this.dataSource.cloneWithRows(recentSearchList) }
            renderRow={ this.renderRecentSearchRow.bind(this) }
            contentContainerStyle={ styles.listViewWrapper }
          />
        </View>
        :
        null
    );
  }

  render() {
    return (<KeyboardAwareScrollView>
      {this.showRecentSearch}
    </KeyboardAwareScrollView>)
  }
}

export default connect(state => ({
    status: state.search.status
  }),
  (dispatch) => ({
    actions: bindActionCreators(searchActions, dispatch),
    commonActions: bindActionCreators(commonActions, dispatch)
  })
)(RecentSearch);

const styles = StyleSheet.create({
  listViewWrapper: {
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
  },
  sectionHeaderContainer: {
    flexDirection: 'row',
    marginTop: 16,
    paddingHorizontal: 8,
    marginBottom: 8,
  },
  textSectionTitle: {
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
  },
  buttonClear:{
    flex:1
  },
  textClear:{
    color: commonColors.title,
    fontFamily: 'Open Sans',
    fontSize: 13,
    textAlign:'right'
  },
  emptyPage: {
    flex:1,
    alignItems: 'center',
  },
  noResultText: {
    fontSize: 16,
    fontFamily: 'OpenSans-Semibold',
    color: commonColors.grayMoreText,
    textAlign: 'center',
    lineHeight: 30,
    marginTop: 50,
  }
});
