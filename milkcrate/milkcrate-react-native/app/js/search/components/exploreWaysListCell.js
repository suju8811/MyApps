import React, { Component, PropTypes } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';

import * as commonStyles from '../../styles/commonStyles';
import * as commonColors from '../../styles/commonColors';

export default class ExploreWaysListCell extends Component {

  static propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    iconWidth: PropTypes.number,
    iconHeight: PropTypes.number,
    onClick: PropTypes.func,
  }

  static defaultProps = {
    onClick: () => {},
    iconWidth: 16,
    iconHeight: 16,    
  }

  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    if (this.props.onClick) {
      this.props.onClick();
    }
  }

  render() {
    const {
      title,
      description,
      icon,
      iconWidth,
      iconHeight,
      onClick,
    } = this.props;

    return (
      <TouchableOpacity onPress={ () => onClick() } style={{paddingHorizontal:8, paddingVertical:8}}>
        <View style={ styles.cellContainer }>
          <View style={ styles.iconContainer }>
            {icon && <Image style={ [{ width: iconWidth }, { height: iconHeight }, {resizeMode:'contain'}] } source={ icon }/>}
          </View>
          <View style={ [styles.contentContainer, title=='Recent'?{borderBottomWidth:0}:{}] }>
            <Text style={ styles.textTitle }>{ title }</Text>
            {/*<Text style={ styles.textDscription }>{ description } </Text>*/}
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  cellContainer: {
    flexDirection: 'row',
    paddingVertical: 8,
    paddingHorizontal:7,
    height:38,
  },
  iconContainer: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  contentContainer: {
    height:38,
    flex: 7,
    alignItems: 'flex-start',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: commonColors.line,
    borderStyle: 'solid',
  },
  textTitle: {
    flex: 1,
    color: commonColors.title,
    fontFamily: 'Open Sans',
    fontSize: 14,
  },
  textDscription: {
    flex: 1,
    color: commonColors.grayText,
    fontFamily: 'Open Sans',
    fontSize: 12,
  },
});
