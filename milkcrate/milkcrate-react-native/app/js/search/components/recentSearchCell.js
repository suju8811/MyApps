import React, { Component, PropTypes } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableHighlight,
} from 'react-native';

import * as commonStyles from '../../styles/commonStyles';
import * as commonColors from '../../styles/commonColors';
import Point from '../../components/Point';
import UtilService from '../../components/util';
import Icon from 'react-native-vector-icons/Ionicons';


export default class RecentSearchCell extends Component {

  static propTypes = {
    title: PropTypes.string.isRequired,
    onClick: PropTypes.func,
  }

  static defaultProps = {
    onClick: () => {}
  }

  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    if (this.props.onClick) {
      this.props.onClick();
    }
  }

  render() {
    const {
      title,
      time,
      onClick,
    } = this.props;

    return (
      <TouchableHighlight onPress={ () => onClick() }>
        <View style={ styles.cellContainer }>
          <View style={styles.icon}>
            <Icon
              name={ 'md-search' } size={ 20 }
              color={ '#82CCBE' }
            />
          </View>
          <Text style={ styles.title }>{ title } </Text>
          <Text style={ styles.time }>{ UtilService.getPastDateTime(time) } </Text>
        </View>
      </TouchableHighlight>
    );
  }
}
const styles = StyleSheet.create({
  cellContainer: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    paddingVertical: 8,
    paddingHorizontal: 8,
    borderBottomWidth: 1,
    borderBottomColor: commonColors.line,
    borderStyle: 'solid',
    alignItems: 'center',
  },
  icon: {
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  },
  title: {
    flex: 5,
    color: commonColors.title,
    fontFamily: 'Open Sans',
    fontSize: 14,
  },
  time: {
    flex: 3,
    color: commonColors.grayMoreText,
    fontFamily: 'Open Sans',
    fontSize: 12,
    textAlign:'right'
  },
});
