'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ScrollView,
  ListView,
  TouchableOpacity,
  NetInfo,
  Alert,
} from 'react-native';

import { bindActionCreators } from 'redux';
import * as searchActions from '../actions';
import { connect } from 'react-redux';

import { Actions } from 'react-native-router-flux';
import ImageButton from '../components/imageButton';
import { screenWidth, activityCellSize, categoryCellSize } from '../../styles/commonStyles';
import * as commonColors from '../../styles/commonColors';
import Cache from '../../components/Cache'
import UtilService from '../../components/util'
import bendService from '../../bend/bendService'
import * as _ from 'underscore'
import NavTitleBar from '../../components/navTitleBar';
import  * as commonStyles from '../../styles/commonStyles';

const categoryTitles = [
  'Animals',
  'Arts',
  'Baby',
  'Beauty',
  'Bicycles',
  'Civic',
  'Coffee',
  'Community',
  'Construction',
  'Dining',
  'Drinks',
  'Education',
  'Energy',
  'Fashion',
  'Finance',
  'Food',
  'Garden',
  'Green Space',
  'Health & Wellness',
  'Home & Office',
  'Media',
  'Products',
  'Services',
  'Special Events',
  'Tourism & Hospitality',
  'Transit',
  'Waste'
];
class BrowseCategory extends Component {
  constructor(props) {
    super(props);
    this.dataSourceCategories = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      loading:true,
      community:{},
      categories:[],
    };

    this.categoryCellMargin = 0;
  }

  componentDidMount() {
    this.hasMounted = true

    if (this.props.subOne != null) {

      let subOne = this.props.subOne;

      for (let i = 0 ; i < categoryTitles.length ; i++) {
        if (categoryTitles[i].toLowerCase() == subOne.toLocaleString()) {
          this.onSelectCategory ( i );
          return;
        }
      }
    }

    bendService.getCommunity((err, ret)=>{
      if(err) {
        console.log(err);
        this.hasMounted && this.setState({
          loading:false
        })
        return
      }

      this.hasMounted && this.setState({
        loading:false,
        community:ret
      })
    })

    bendService.getCategories((err, rets)=>{
      if(err) {
        console.log(err);
        return;
      }
      this.hasMounted && this.setState({
        categories:_.sortBy(rets, (o)=>{
          return o.name
        })
      })
    })
  }
  componentWillUnmount() {
    this.hasMounted = false
  }

  onSelectCategory (cat) {

    Actions.CategoryView({ title: cat.name, category: cat });
  }
  onBack() {
    Actions.pop()
  }

  renderCategoriesRow(rowData, sectionID, rowID) {
    if(!rowData) return null;

    if(!this.props.countByCategory[rowData._id])
        return null;
    let imgUrl = UtilService.getCategoryRemoteButton(rowData)
    let imgSelectedUrl = UtilService.getCategoryRemoteActiveButton(rowData)||imgUrl

    return (
      <View style={ [styles.categoryCellWrap, { marginHorizontal: this.categoryCellMargin },] }>
        <View style={ styles.categoryCellButtonWrapper }>
          <ImageButton
            style={ styles.categoryCellImage }
            appearance={{
              normal: imgUrl,
              highlight: imgSelectedUrl
            }}
            onPress={ () => this.onSelectCategory(rowData) }
          />
          <Text style={ styles.cagegoryCellText }>
            { rowData.name }
          </Text>
        </View>
      </View>
    );
  }

  caculateCategoryCellMargin() {
    const cellNumber = Math.round(screenWidth / categoryCellSize);
    this.categoryCellMargin = ( screenWidth - categoryCellSize * cellNumber) / cellNumber / 2;
  }

  render() {
    this.caculateCategoryCellMargin();

    if(this.state.loading)
        return null;

    return (
      <View style={ styles.container }>
        <NavTitleBar
          buttons={ commonStyles.NavBackButton }
          onBack={ this.onBack }
          title ='Browse By Category'
        />
        <ScrollView style={{paddingTop:10}}>
          {this.state.community.showCategoriesInSearch !== false && <ListView
            pageSize = { this.state.categories.length }
            enableEmptySections={ true }
            dataSource={ this.dataSourceCategories.cloneWithRows(this.state.categories) }
            renderRow={ this.renderCategoriesRow.bind(this) }
            contentContainerStyle={ styles.listViewCategories }
          />}
        </ScrollView>
      </View>
    );
  }
}

export default connect(state => ({
  status: state.search.status
  }),
  (dispatch) => ({
    actions: bindActionCreators(searchActions, dispatch)
  })
)(BrowseCategory);

const styles = StyleSheet.create({
  container: {
    flex : 1,
  },
  textTitle: {
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    color: commonColors.grayMoreText,
    paddingTop: 24,
    paddingBottom: 8,
    paddingLeft: screenWidth * 0.05,
  },
  listViewCategories: {
    flexDirection:'row',
    flexWrap: 'wrap',
  },
  listViewExploreWays: {
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
  },
  categoryCellWrap: {
    padding: 10,
    width: categoryCellSize,
    height: categoryCellSize * 1.2,
  },
  categoryCellButtonWrapper: {
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  categoryCellImage: {
    // width: categoryCellSize - 40,
    // height : categoryCellSize - 40,
    width: 60,
    height: 60,
  },
  cagegoryCellText: {
    width: categoryCellSize,
    height : categoryCellSize - 40,
    textAlign: 'center',
    color: commonColors.grayMoreText,
    fontFamily: 'Open Sans',
    fontWeight: 'bold',
    fontSize: 12,
    paddingTop: 8,
  },
});
