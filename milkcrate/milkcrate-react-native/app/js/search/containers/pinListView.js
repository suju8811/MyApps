import React, { Component } from 'react';
import {
  StyleSheet,
  ListView,
  Text,
  View,
  Image,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
  Alert,
  Platform
} from 'react-native';

import { bindActionCreators } from 'redux';
import * as searchActions from '../actions';
import { connect } from 'react-redux';
import * as commonActions from '../../common/actions';
import * as commonActionTypes from '../../common/actionTypes';

import { Actions } from 'react-native-router-flux';

import NavTitleBar from '../../components/navTitleBar';
import BusinessesListCell from '../components/businessesListCell';
import EventsListCell from '../components/eventsListCell';
import * as commonColors from '../../styles/commonColors';
import  * as commonStyles from '../../styles/commonStyles';

import UtilService from '../../components/util'
import Cache from '../../components/Cache'
import bendService from '../../bend/bendService'

import Swipeout from 'react-native-swipeout';
import * as _ from 'underscore'

class PinListView extends Component {
  constructor(props) {
    super(props);

    this.dataSource = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      isRefreshing: true,
      currentLocation: null,
      activities: {
        event: [],
        service: [],
        action: [],
        volunteer_opportunity: [],
        business: [],
      },
      initialized:false,
    };
  }

  componentDidMount() {
    this.hasMounted = true
    this.loadAllData();
    UtilService.mixpanelEvent("Viewed Pinned Activities")
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  componentWillReceiveProps(newProps) {
    
    if (newProps.commonStatus === commonActionTypes.ALL_PINNED_ACTIVITIES) {
      this.hasMounted && this.setState({
        activities: newProps.allPinnedActivities,
        isRefreshing: false,
        initialized:true
      });
    }    
  }


  loadAllData() {
    this.hasMounted && this.setState({
      isRefreshing: true,
      activities: {
        event: [],
        service: [],
        action: [],
        volunteer_opportunity: [],
        business: [],
      },
    });

    Cache.locationEnabled && navigator.geolocation.getCurrentPosition(
      (position) => {
        if (position) {
          this.hasMounted && this.setState({ currentLocation: position })
        }
      },
      (error) => {
        console.log(JSON.stringify(error));
        // alert(JSON.stringify(error));
      },
      { enableHighAccuracy: commonStyles.geoLocation.enableHighAccuracy, timeout: commonStyles.geoLocation.timeout, maximumAge: commonStyles.geoLocation.maximumAge }
    );

    this.props.commonActions.updateAllPinnedActivities();
    
  }

  onBack() {
    Actions.pop()
  }

  onRecentActivityCellPressed (activity) {
    if (activity.type == 'business') {
      Actions.BusinessesDetail({ business: activity.activity });
    } else if(activity.type == 'action') {
      Actions.ActionDetail({ action: activity.activity });
    } else if(activity.type == 'event') {
      Actions.EventDetail({ event: activity.activity });
    } else if(activity.type == 'volunteer_opportunity') {
      Actions.VolunteerDetail({ volunteer: activity.activity });
    }
  }

  unpinActivity(type, activity) {
    bendService.unpinActivity({
      type: type,
      id: activity.activity._id,
      name: activity.activity.name,
    }, (err, ret)=>{
      if(err) {
        console.log(err);
        return;
      }
      this.setState((prev)=>{
        var exist = _.find(prev.activities[type], (o)=>{
          return o._id == activity._id
        })
        if(exist) {
          prev.activities[type].splice(prev.activities[type].indexOf(exist), 1)

          this.props.commonActions.updateRecentPinnedActivities();
          this.props.commonActions.updateAllPinnedActivities();
        }

        return prev
      })
    })
  }


  renderActionsListRow(rowData, sectionID, rowID) {
    var swipeoutBtns = [
      {
        text: 'Delete',
        backgroundColor:'red',
        onPress:()=>{
          this.unpinActivity('action', rowData)
        }
      }
    ]

    return (
      <Swipeout key={rowData._id} right={swipeoutBtns}>
        <EventsListCell
          title={ rowData.activity.name }
          icon={ UtilService.getCategoryIconFromSlug(rowData.activity) }
          points={ UtilService.getPoints(rowData.activity)}
          onClick={ () => this.onRecentActivityCellPressed(rowData) }
        />
      </Swipeout>
    );
  }

  renderBusinessesListRow(rowData, sectionID, rowID) {
    var swipeoutBtns = [
      {
        text: 'Delete',
        backgroundColor:'red',
        onPress:()=>{
          this.unpinActivity('business', rowData)
        }
      }
    ]
    return (
      <Swipeout key={rowData._id} right={swipeoutBtns}>
      <BusinessesListCell
        title={ rowData.activity.name }
        icon={ UtilService.getCategoryIconFromSlug(rowData.activity)}
        description={ rowData.activity.description }
        distance={ rowData.activity._geoloc&&this.state.currentLocation ? UtilService.getDistanceFromLatLonInMile(rowData.activity._geoloc[1], rowData.activity._geoloc[0],
        this.state.currentLocation.coords.latitude, this.state.currentLocation.coords.longitude) : null }
        price={ Number(rowData.activity.priceTier) }
        rating={ Number(rowData.activity.rating || 0) }
        onClick={ () => this.onRecentActivityCellPressed(rowData) }
      />
      </Swipeout>
    );
  }

  renderEventsListRow(rowData, sectionID, rowID) {
    var swipeoutBtns = [
      {
        text: 'Delete',
        backgroundColor:'red',
        onPress:()=>{
          this.unpinActivity('event', rowData)
        }
      }
    ]
    return (
      <Swipeout key={rowData._id} right={swipeoutBtns}>
      <EventsListCell
        title={ rowData.activity.name }
        icon={ UtilService.getCategoryIconFromSlug(rowData.activity) }
        points={ UtilService.getPoints(rowData.activity) }
        onClick={ () => this.onRecentActivityCellPressed(rowData) }
      />
      </Swipeout>
    );
  }

  renderVolunteerListRow(rowData, sectionID, rowID) {
    var swipeoutBtns = [
      {
        text: 'Delete',
        backgroundColor:'red',
        onPress:()=>{
          this.unpinActivity('volunteer_opportunity', rowData)
        }
      }
    ]
    return (
      <Swipeout key={rowData._id} right={swipeoutBtns}>
      <EventsListCell
        title={ rowData.activity.name }
        icon={ UtilService.getCategoryIconFromSlug(rowData.activity) }
        points={ UtilService.getPoints(rowData) }
        onClick={ () => this.onRecentActivityCellPressed(rowData) }
      />
      </Swipeout>
    );
  }

  get showActions() {
    if(Cache.community.actionsEnabled === false) return null;

    return(
      this.state.activities.action.length ?
        <View>
          <View style={ styles.sectionHeaderContainer }>
            <Text style={ styles.textSectionTitle }>Actions</Text>
          </View>
          <ListView
            enableEmptySections={ true }
            dataSource={ this.dataSource.cloneWithRows(this.state.activities.action) }
            renderRow={ this.renderActionsListRow.bind(this) }
            contentContainerStyle={ styles.listViewWrapper }
          />
        </View>
      :
        null
    );
  }

  get showBusinesses() {
    if(Cache.community.placesEnabled === false) return null;

    return (
      this.state.activities.business.length ?
        <View>
          <View style={ styles.sectionHeaderContainer }>
            <Text style={ styles.textSectionTitle }>Places</Text>
          </View>
          <ListView
            enableEmptySections={ true }
            dataSource={ this.dataSource.cloneWithRows(this.state.activities.business) }
            renderRow={ this.renderBusinessesListRow.bind(this) }
            contentContainerStyle={ styles.listViewWrapper }
          />
        </View>
      :
        null
    );
  }

  get showEvents() {
    if(Cache.community.eventsEnabled === false) return null;

    return (
      this.state.activities.event.length ?
        <View>
          <View style={ styles.sectionHeaderContainer }>
            <Text style={ styles.textSectionTitle }>Events</Text>
          </View>
          <ListView
            enableEmptySections={ true }
            dataSource={ this.dataSource.cloneWithRows(this.state.activities.event) }
            renderRow={ this.renderEventsListRow.bind(this) }
            contentContainerStyle={ styles.listViewWrapper }
          />
        </View>
      :
        null
    );
  }

  get showVolunteer() {
    if(Cache.community.volunteerOpportunitiesEnabled === false) return null;

    return (
      this.state.activities.volunteer_opportunity.length ?
        <View>
          <View style={ styles.sectionHeaderContainer }>
            <Text style={ styles.textSectionTitle }>Volunteer Opportunities</Text>
          </View>
          <ListView
            enableEmptySections={ true }
            dataSource={ this.dataSource.cloneWithRows(this.state.activities.volunteer_opportunity) }
            renderRow={ this.renderVolunteerListRow.bind(this) }
            contentContainerStyle={ styles.listViewWrapper }
          />
        </View>
      :
        null
    );
  }

  /*get showServices() {
    return (
      this.state.activities.service.length ?
        <View>
          <View style={ styles.sectionHeaderContainer }>
            <Text style={ styles.textSectionTitle }>Services</Text>
          </View>
          <ListView
            enableEmptySections={ true }
            dataSource={ this.dataSource.cloneWithRows(this.state.activities.service) }
            renderRow={ this.renderServicesListRow.bind(this) }
            contentContainerStyle={ styles.listViewWrapper }
          />
        </View>
      :
        null
    );
  }*/

  onRefresh() {
    this.hasMounted && this.setState({ isRefreshing: true });
    this.loadAllData();    
  }

  render() {
    let {activities, initialized} = this.state
    var activityCount = 0
    if(activities.action && Cache.community.actionsEnabled)
      activityCount += activities.action.length;
    if(activities.business && Cache.community.placesEnabled)
      activityCount += activities.business.length;
    if(activities.event && Cache.community.eventsEnabled)
      activityCount += activities.event.length;
    if(activities.volunteer_opportunity && Cache.community.volunteerOpportunitiesEnabled)
      activityCount += activities.volunteer_opportunity.length;

    return (
      <View style={ styles.container }>
        <NavTitleBar
/*
          buttons={ commonStyles.NavBackButton }
          onBack={ this.onBack }
*/
          title={ 'MY PINNED ACTIVITIES' }
        />
        {initialized && activityCount > 0 && <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={ this.state.isRefreshing }
              onRefresh={ () => this.onRefresh() }
              tintColor={ commonColors.theme }
            />
          }
        >
          { this.showActions }
          { this.showBusinesses }
          { this.showEvents }
          { this.showVolunteer }
          {/*{ this.showServices }*/}
        </ScrollView>}
        {initialized && activityCount == 0 &&<View style={styles.emptyPage}>
          <Image source={require('../../../assets/imgs/empty-pin.png')} style={styles.emptyPin}></Image>
          <Text style={styles.bigText}>Pin your favorite activities</Text>
          <Text style={styles.smallText}>On your mark, get set… pin! Tap the pin icon on any activity page to save it here.</Text>
        </View>}
      </View>
    );
  }
}

export default connect(props => ({
    status: props.search.status,
    commonStatus: props.common.status,
    allPinnedActivities: props.common.allPinnedActivities,
  }),
  (dispatch) => ({
    actions: bindActionCreators(searchActions, dispatch),
    commonActions: bindActionCreators(commonActions, dispatch),
  })
)(PinListView);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#FAFAFA'
  },
  listViewWrapper: {
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
  },
  sectionHeaderContainer: {
    flexDirection: 'row',
  },
  textSectionTitle: {
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    marginTop: 16,
    marginLeft: 8,
    marginBottom: 8,
  },
  activityIndicator: {
    marginTop: 10,
    flex: 1,
  },
  emptyPage: {
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  },
  emptyPin: {
    width:124,
    height:124
  },
  bigText:{
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 20,
    marginTop:20,
    textAlign:'center',
  },
  smallText:{
    marginTop:15,
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    textAlign:'center',
    width:276
  }
});
