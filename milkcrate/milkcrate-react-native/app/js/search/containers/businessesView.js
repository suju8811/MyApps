'use strict';

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    ScrollView,
    TouchableOpacity,
    Keyboard,
    Alert,
    Platform
} from 'react-native';

import { bindActionCreators } from 'redux';
import * as searchActions from '../actions';
import * as commonActions from '../../common/actions';
import * as commonActionTypes from '../../common/actionTypes';

import { connect } from 'react-redux';

import { Actions } from 'react-native-router-flux';
import { SegmentedControls } from 'react-native-radio-buttons';

import NavSearchBar from '../../components/navSearchBar';
import BusinessesListView from './businessesListView';
import BusinessesMapView from './businessesMapView';

import  * as commonStyles from '../../styles/commonStyles';
import * as commonColors from '../../styles/commonColors';
import { LocalStorage } from '../../styles/localStorage';

const currentLocation = require('../../../assets/imgs/current_location_button.png');

import bendService from '../../bend/bendService'
import * as _ from 'underscore'
import UtilService from '../../components/util'
import Cache from '../../components/Cache'
import RecentSearch from '../components/recentSearch';

class BusinessesView extends Component {
  constructor(props) {
    super(props);

    this.businesses = [];
    this.offset = 0;
    this.limit = 20;
    this.searchText = '';
    this.more = true;

    this.state = {
      isRefreshing: false,

      selectedIndex: 'List',
      currentLocation: null,
      businesses: [],
      categoryIcons: [],

      isListMode: true,
      searchAutoFocus: false,

      businessesQuery:{
        more: true,
        loading: false,
      },
      specialSearch: false,
      isSearchingMode: false,
    };
  }

  componentDidMount() {
    this.hasMounted = true
    this.loadAllData();
    UtilService.mixpanelEvent("Browsed Places")
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  componentWillReceiveProps(newProps) {
    if (newProps.commonStatus === commonActionTypes.UPDATE_USER_ACTIVITY) {
      var param = newProps.param;
      if(param.activityType == 'business') {
        var businesses = this.state.businesses;
        var exist = _.find(businesses, (o)=>{
          return o._id == param.activityId;
        })

        if(exist) {
          exist.userActivity = param.userActivity

          this.setState(businesses)
        }
      }
    }
  }
  loadAllData() {
    this.businesses = [];
    this.offset = 0;
    this.limit = 20;
    this.searchText = '';
    this.more = true;

    this.hasMounted && this.setState({
      selectedIndex: 'List',
      currentLocation: null,
      businesses: [],
      categoryIcons: [],

      businessesQuery:{
        more: true,
        loading: false,
      },

      isSearchingMode: false,
      specialSearch: false,
    });

    this.loadBusinesses();
  }

  onBack() {
    Actions.pop()
  }

  onFilter() {
    alert("Tapped filter button!");
  }

  onCurrentLocation() {
    Cache.locationEnabled && navigator.geolocation.getCurrentPosition(
      (position) => {
        LocalStorage.save({
          key: commonStyles.geoLocation.currentLocation,
          rawData: position,
        });
        this.hasMounted && this.setState({ currentLocation: position });
      },
      (error) => {
        console.log(JSON.stringify(error));
      },
      { enableHighAccuracy: commonStyles.geoLocation.enableHighAccuracy, timeout: commonStyles.geoLocation.timeout, maximumAge: commonStyles.geoLocation.maximumAge }
    );
  }

  loadBusinesses() {
    //if (this.more === false)
    //  return;

    if(this.isLoadBusiness) return;

    this.isLoadBusiness = true

    console.log('loadBusinesses')

    this.hasMounted && this.setState( (state) => {
      state.businessesQuery.loading = true;
      return state;
    });

    // navigator.geolocation.getCurrentPosition(
    //   (position) => {
    //     console.log("current location business view", position)
    //     this.search(position);
    //   },
    //   (error) => {
    //     console.log("current location business view error", JSON.stringify(error));
    //     this.search(null);
    //   }
    //     ,
    //     Platform.OS === 'ios'?{ enableHighAccuracy: commonStyles.geoLocation.enableHighAccuracy, timeout: commonStyles.geoLocation.timeout, maximumAge: commonStyles.geoLocation.maximumAge }:null
    // );
    LocalStorage.load({
      key: commonStyles.geoLocation.currentLocation,
    }).then( (position) => {
      this.search(position);
    })
    .catch( (error) => {
      this.search(null);
    });

  }

  search(position) {
    if (position) {
      this.hasMounted && this.setState({ currentLocation: position })
    }

    var param = {
      type: 'business',
      offset: this.offset,
      limit: this.limit,
      query: this.searchText,
    }

    if(_.isEqual(param, this.oldParam)) {
      this.isLoadBusiness = false;
      return;
    }

    this.oldParam = param

    if(position) {
      param.lat = position.coords.latitude;
      param.long = position.coords.longitude;
    } else {
      if(Cache.community && Cache.community._geoloc) {
        param.lat = Cache.community._geoloc[1];
        param.long = Cache.community._geoloc[0];
      }
    }

    bendService.searchActivity(param, (error, result) => {
      this.isLoadBusiness = false
      this.hasMounted && this.setState({
        showRecentSearch:false
      })

      if (param.query != this.searchText)
        return;

      this.hasMounted && this.setState( (state) => {
        state.businessesQuery.loading = false;
        return state;
      });

      this.hasMounted && this.setState({ isRefreshing: false });

      if (error) {
        console.log("search failed", error)
        return
      }

      if (result.data.business.length < this.limit) {
        this.more = false;
        this.hasMounted && this.setState( (state) => {
          state.businessesQuery.more = false;
          return state;
        });
      }

      if(this.offset == 0)
        this.businesses = result.data.business;
      else {
        this.businesses = this.businesses.concat(result.data.business);
      }

      if ((this.offset == 0) && (this.businesses.length > 0) && (this.state.isSearchingMode)) {
        let distance = 0;
        if (this.businesses[0]._geoloc && this.state.currentLocation) {
          distance = UtilService.getDistanceFromLatLonInMile(this.businesses[0]._geoloc[1], this.businesses[0]._geoloc[0],
            this.state.currentLocation.coords.latitude, this.state.currentLocation.coords.longitude);
        }

        if (distance > 50) {
          this.hasMounted && this.setState({ specialSearch: true });
        }
      }

      this.hasMounted && this.setState({
        businesses: this.businesses,
      });

      const imageOffset = this.offset;
      this.offset += this.limit;

      result.data.business.map( (business, index) => {
        this.hasMounted && this.setState( (state) => {
          state.categoryIcons[imageOffset + index] = UtilService.getCategoryIconFromSlug(business);
          return state;
        })
      });
    })
  }

  onSearchChange(text) {
    if(text == '') {
      this.hasMounted && this.setState({
        showRecentSearch:true
      })

      return
    }
    this.hasMounted && this.setState({
      showRecentSearch:false
    })

    this.searchText = text
    this.businesses = [];
    this.state.businesses = [];
    this.offset = 0;
    this.limit = 20;
    this.more = true;

    this.hasMounted && this.setState({
      currentLocation: null,
      businesses: this.state.businesses,
      categoryIcons: [],

      businessesQuery:{
        more: true,
        loading: false,
      },
    });

    console.log('onSearchChange')
    this.loadBusinesses();
  }

  onSearchCancel() {
    this.hasMounted && this.setState({
      showRecentSearch:false
    })

    if (this.state.selectedIndex ===  'Map') {
      this.hasMounted && this.setState({ selectedIndex: 'List' });
    }

    this.offset = 0;
    this.searchText = '';
    this.more = true;
    this.businesses = [];
    this.state.businesses = [];
    this.oldParam = {}

    this.hasMounted && this.setState( (state) => {
      state.businessesQuery.more = true;
      state.businesses = [];
      state.categoryIcons = [];
      state.isSearchingMode = false;
      state.specialSearch = false;
      return state;
    })
    console.log('onSearchCancel')

    this.loadBusinesses();
  }

  onRefresh() {
    this.hasMounted && this.setState({ isRefreshing: true });
    this.loadAllData();
  }

  onSelectSegment(option) {

    Keyboard.dismiss();

    if (this.businesses.length === 0) {
      this.hasMounted && this.setState({ selectedIndex: 'List' });
      return;
    }

    this.hasMounted && this.setState({
      selectedIndex: option,
    });

    let isListMode = false;

    if (option === 'List') {
      isListMode = true;
    }

    this.hasMounted && this.setState({
      isListMode: isListMode,
      searchAutoFocus: false,
    });
  }

  onGoSearchScreen() {

    this.onSelectSegment('List');

    this.hasMounted && this.setState({
      searchAutoFocus: true,
    });
  }

  onSearchFocus() {
    this.hasMounted && this.setState({
      searchAutoFocus: false,
      showRecentSearch: this.searchText=='',
      isSearchingMode: true,
    });
  }

  get renderSelectedContent() {
    if (this.state.selectedIndex == 'List') {
      return (
        <BusinessesListView
          businesses={ this.state.businesses }
          currentLocation={ this.state.currentLocation }
          moreBusinesses={ this.state.businessesQuery.more }
          loading={ this.state.businessesQuery.loading }
          onLoadBusinesses={ () => this.loadBusinesses() }
          isRefreshing={ this.state.isRefreshing }
          onRefresh={ () => this.onRefresh() }
          onPressedCell={ (rowData) => {
            Actions.BusinessesDetail({ business: rowData });
            this.props.commonActions.saveSearchText(this.searchText);
          } }
          specialSearch={ this.state.specialSearch && this.state.isSearchingMode}
        />
      );
    }

    return (
      <BusinessesMapView
        onPressCell={ (rowData) => {
          Actions.BusinessesDetail({ business: rowData });
          this.props.commonActions.saveSearchText(this.searchText);
        } }
        businesses={ this.state.businesses }
        currentLocation={ this.state.currentLocation }
      />
    );
  }

  renderMainContent() {
    const {showRecentSearch} = this.state

    if(showRecentSearch) {
      return (<RecentSearch onSearch={this.onSearchChange.bind(this)}/>)
    }

    return (
      <View style={{flex:1}}>
        <View style={ styles.segmentedWrap }>
          <View style={ styles.segmentedLeft }/>
          <View style={ styles.segmented }>
            <SegmentedControls
              tint={ '#fff' }
              selectedTint= { commonColors.theme }
              backTint= { commonColors.theme }
              options={ ['List', 'Map'] }
              allowFontScaling={ false } // default: true
              onSelection={ (option) => this.onSelectSegment(option) }
              selectedOption={ this.state.selectedIndex }
            />
          </View>
          <View style={ styles.segmentedRight }>
            {
              this.state.selectedIndex == 'Map' ?
                <TouchableOpacity activeOpacity={ .5 } onPress={ () => this.onCurrentLocation() }>
                  <Image style={ styles.imageCurrentLocation } source={ currentLocation }/>
                </TouchableOpacity>
                :
                null
            }
          </View>
        </View>
        {this.renderSelectedContent}
      </View>
    )
  }

  render() {
    return (
      <View style={ styles.container }>
        <NavSearchBar
          buttons={ commonStyles.NavBackButton }
          onBack={ this.onBack }
          placeholder={ 'Search for places' }
          onSearchChange={ (text) => this.onSearchChange(text) }
          onCancel={ () => this.onSearchCancel() }
          onFocus={ () => this.onSearchFocus() }
          searchMode={ this.state.isListMode }
          onGoSearchScreen={ () => this.onGoSearchScreen() }
          searchAutoFocus={ this.state.searchAutoFocus }
        />
        {this.renderMainContent()}
      </View>
    );
  }
}

export default connect(state => ({
    status: state.search.status,
    commonStatus: state.common.status,
    param: state.common.param,
  }),
  (dispatch) => ({
    actions: bindActionCreators(searchActions, dispatch),
    commonActions: bindActionCreators(commonActions, dispatch),
  })
)(BusinessesView);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    width: commonStyles.screenWidth,
    height: commonStyles.screenHeight,
  },
  segmentedWrap: {
    flexDirection: 'row',
    backgroundColor: commonColors.theme,
    height: 44,
  },
  segmented: {
    flex: 6,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  segmentedLeft: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  segmentedRight: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 5,
  },
  imageCurrentLocation: {
    width: 20,
    height: 22,
  },
  tabStyle: {
    borderWidth: 1,
    borderColor: '#fff',
    borderStyle: 'solid',
    backgroundColor: commonColors.theme,
  },
  tabTextStyle: {
    color: '#fff',
  },
  activeTabStyle: {
    borderWidth: 1,
    borderColor: '#fff',
    borderStyle: 'solid',
    backgroundColor: '#fff',
  },
  activeTabTextStyle: {
    color: commonColors.theme,
  },
});
