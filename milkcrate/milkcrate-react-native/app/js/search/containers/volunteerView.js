'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  ListView,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView,
  ActivityIndicator,
  RefreshControl,
  Alert,
  Platform
} from 'react-native';

import { bindActionCreators } from 'redux';
import * as searchActions from '../actions';
import * as commonActions from '../../common/actions';
import * as commonActionTypes from '../../common/actionTypes';

import { connect } from 'react-redux';

import { Actions } from 'react-native-router-flux';
import NavSearchBar from '../../components/navSearchBar';
import EventsListCell from '../components/eventsListCell';
import * as commonColors from '../../styles/commonColors';
import  * as commonStyles from '../../styles/commonStyles';
import InfiniteScrollView from 'react-native-infinite-scroll';

import Cache from '../../components/Cache'
import UtilService from '../../components/util'
import bendService from '../../bend/bendService'
import * as _ from 'underscore'
import RecentSearch from '../components/recentSearch';
import { LocalStorage } from '../../styles/localStorage';

class VolunteerView extends Component {
  constructor(props) {
    super(props);

    this.dataSource = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });

    this.offset = 0;
    this.limit = 20;
    this.searchText = '';
    this.more = true;

    this.state = {
      isRefreshing: false,

      // currentLocation: null,
      volunteeres: [],
      categoryIcons: [],
      volunteeresQuery: {
        more: true,
        loading: false,
      },
    };
  }

  componentDidMount() {
    this.hasMounted = true
    this.loadAllData();
    UtilService.mixpanelEvent("Browsed Volunteer Opportunities")
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  componentWillReceiveProps(newProps) {
    if (newProps.commonStatus === commonActionTypes.UPDATE_USER_ACTIVITY) {
      var param = newProps.param;
      if(param.activityType == 'volunteer_opportunity') {
        var volunteeres = this.state.volunteeres;
        var exist = _.find(volunteeres, (o)=>{
          return o._id == param.activityId;
        })

        if(exist) {
          exist.userActivity = param.userActivity

          this.setState(volunteeres)
        }
      }
    }
  }

  loadAllData() {
    this.offset = 0;
    this.limit = 20;
    this.searchText = '';
    this.more = true;

    this.hasMounted && this.setState({
      // currentLocation: null,
      volunteeres: [],
      categoryIcons: [],
      volunteeresQuery: {
        more: true,
        loading: false,
      },
    });

    this.loadVolunteer();
  }

  onBack() {
    Actions.pop()
  }

  onPressedCell (volunteer) {
    Actions.VolunteerDetail({
      volunteer: volunteer
    })

    this.props.commonActions.saveSearchText(this.searchText);
  }

  loadVolunteer () {
    //if (this.more == false)
    //  return;

    this.hasMounted && this.setState( (state) => {
      state.volunteeresQuery.loading = true;
      return state;
    });

    // navigator.geolocation.getCurrentPosition( 
    //   (position) => {
    //     this.search(position)
    //   },
    //   (error) => {
    //     console.log(JSON.stringify(error));
    //     this.search(null)
    //   },
    //     Platform.OS === 'ios'?{ enableHighAccuracy: commonStyles.geoLocation.enableHighAccuracy, timeout: commonStyles.geoLocation.timeout, maximumAge: commonStyles.geoLocation.maximumAge }:null
    // );

    LocalStorage.load({
      key: commonStyles.geoLocation.currentLocation,
    }).then( (position) => {
      this.search(position);
    })
    .catch( (error) => {
      this.search(null);
    });
  }

  renderListRow(rowData, sectionID, rowID) {
    return (
      <EventsListCell
        title={ rowData.name }
        icon={ this.state.categoryIcons[rowID] }
        points={ UtilService.getPoints(rowData) }
        onClick={ () => this.onPressedCell(rowData) }
        userActivity={rowData.userActivity}
      />
    );
  }

  search(position) {
    this.setState({
      showRecentSearch:false
    })

    // if(position)
    //   this.hasMounted && this.setState({ currentLocation: position })

    var param = {
      type: 'volunteer_opportunity',
      offset: this.offset,
      limit: this.limit,
      query: this.searchText
    }

    if(_.isEqual(param, this.oldParam)) {
      return;
    }

    this.oldParam = param

    if(position) {
      param.lat = position.coords.latitude;
      param.long = position.coords.longitude;
    } else {
      if(Cache.community && Cache.community._geoloc) {
        param.lat = Cache.community._geoloc[1];
        param.long = Cache.community._geoloc[0];
      }
    }

    bendService.searchActivity(param, (error, result) => {
      if(param.query != this.searchText) return;

      this.hasMounted && this.setState( (state) => {
        state.volunteeresQuery.loading = false;
        return state;
      });

      this.hasMounted && this.setState({ isRefreshing: false });

      if (error) {
        console.log("search failed", error)
        return
      }

      if (result.data.volunteer_opportunity.length < this.limit) {
        this.more = false;
        this.hasMounted && this.setState( (state) => {
          state.volunteeresQuery.more = false;
          return state;
        });
      }

      this.state.volunteeres = this.state.volunteeres.concat(result.data.volunteer_opportunity);
      this.hasMounted && this.setState({ volunteeres: this.state.volunteeres });

      const imageOffset = this.offset;
      this.offset += this.limit;

      result.data.volunteer_opportunity.map((item, index) => {
        this.hasMounted && this.setState( (state) => {
          state.categoryIcons[imageOffset + index] = UtilService.getCategoryIconFromSlug(item);
          return state;
        });
      });
    })
  }

  onSearchChange(text) {
    if(text == '') {
      this.setState({
        showRecentSearch:true
      })

      return
    } else {
      this.setState({
        showRecentSearch:false
      })
    }

    this.searchText = text
    this.state.volunteeres = [];
    this.offset = 0;
    this.limit = 20;
    this.more = true;

    this.hasMounted && this.setState({
      // currentLocation: null,
      volunteeres: this.state.volunteeres,
      categoryIcons: [],

      volunteeresQuery:{
        more: true,
        loading: false,
      },
    });

    this.loadVolunteer()
  }

  onSearchCancel() {
    this.setState({
      showRecentSearch:false
    })

    this.offset = 0;
    this.searchText = '';
    this.more = true;
    this.oldParam = {}

    this.hasMounted && this.setState( (state) => {
      state.volunteeresQuery.more = true;
      state.volunteeres = [];
      state.categoryIcons = [];
      return state;
    })

    this.loadVolunteer();
  }

  renderFooter() {
    if ((!this.state.isRefreshing) && (this.state.volunteeresQuery.loading)) {
      return (
        <ActivityIndicator
          hidesWhenStopped={ true }
          style={{ marginVertical: 15 }}
        />
      );
    } 

    return null;
  }

  onRefresh() {
    this.hasMounted && this.setState({ isRefreshing: true });
    this.loadAllData();    
  }

  renderContent() {
    const {showRecentSearch} = this.state

    if(showRecentSearch) {
      return (<RecentSearch onSearch={this.onSearchChange.bind(this)}/>)
    }

    return (<ListView
      renderScrollComponent={ props => <InfiniteScrollView distanceFromEnd={10} {...props}/> }
      enableEmptySections={ true }
      dataSource={ this.dataSource.cloneWithRows(this.state.volunteeres) }
      renderRow={ this.renderListRow.bind(this) }
      contentContainerStyle={ styles.listViewWrapper }
      renderFooter={ this.renderFooter.bind(this) }

      refreshControl={
        <RefreshControl
          refreshing={ this.state.isRefreshing }
          onRefresh={ () => this.onRefresh() }
          tintColor={ commonColors.theme }
        />
      }
      canLoadMore={ this.state.volunteeresQuery.more }
      onLoadMoreAsync={ ()=> this.loadVolunteer() }
    />      )
  }
  render() {
    return (
      <View style={ styles.container }>
        <NavSearchBar
          buttons={ commonStyles.NavBackButton }
          onBack={ this.onBack }
          onFocus={ () => {
            this.setState({
              showRecentSearch:this.searchText==''
            })
          } }
          placeholder={ 'Search for volunteer opportunities' }
          onSearchChange={ (text) => this.onSearchChange(text) }
          onCancel={ () => this.onSearchCancel() }
        />
        {this.renderContent()}
      </View>
    );
  }
}

export default connect(state => ({
    status: state.search.status,
    commonStatus: state.common.status,
    param: state.common.param,
  }),
  (dispatch) => ({
    actions: bindActionCreators(searchActions, dispatch),
    commonActions: bindActionCreators(commonActions, dispatch),
  })
)(VolunteerView);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listViewWrapper: {
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
  },
});
