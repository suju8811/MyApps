'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  ListView,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  ActivityIndicator,
  RefreshControl,
  Alert,
} from 'react-native';

import { bindActionCreators } from 'redux';
import * as searchActions from '../actions';
import * as commonActions from '../../common/actions';
import * as commonActionTypes from '../../common/actionTypes';
import { connect } from 'react-redux';

import { Actions } from 'react-native-router-flux';
import InfiniteScrollView from 'react-native-infinite-scroll';

import BusinessesListCell from '../components/businessesListCell';
import * as commonColors from '../../styles/commonColors';
import * as commonStyles from '../../styles/commonStyles';

import UtilService from '../../components/util'
const specialSearchImage = require('../../../assets/imgs/searching-image.png');

class BusinessesListView extends Component {
  constructor(props) {
    super(props);

    this.dataSource = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });
  }

  onPressedCell (rowData) {
    Actions.BusinessesDetail({ business: rowData });
  }

  renderRow(rowData, sectionID, rowID) {
    const caetgoriIcon = UtilService.getCategoryIconFromSlug(rowData);

    return (
      <BusinessesListCell
        title={ rowData.name }
        icon={ caetgoriIcon }
        description={ rowData.description }
        distance={ rowData._geoloc && this.props.currentLocation ? UtilService.getDistanceFromLatLonInMile(rowData._geoloc[1], rowData._geoloc[0],
        this.props.currentLocation.coords.latitude, this.props.currentLocation.coords.longitude) : null }
        price={ Number(rowData.price) }
        rating={ Number(rowData.rating || 0) }
        onClick={ () => this.props.onPressedCell(rowData) }
        userActivity={rowData.userActivity}
      />
    );
  }
  
  get renderSpecialHeader() {
    if (this.props.specialSearch) {
      return (
        <View style={ styles.specialHeader }>
          <View style={ styles.specialTopContentContainer }>
            <Image style={ styles.imageSpecialSearch } source={ specialSearchImage } />
            <Text style={ styles.textRat }>Rats! We couldn't find any matches nearby.</Text>
          </View>
          <Text style={ styles.textDescription }>Below are some matches in our wider network.</Text>
        </View>              
      );
    }

    return null;
  }

  renderFooter() {
    if ((!this.props.isRefreshing) && (this.props.loading)) {
      return (
        <ActivityIndicator
          hidesWhenStopped={ true }
          style={{ marginVertical: 15 }}
        />
      );
    } 

    return null;
  }

  render() {
    const { 
      businesses,
      onLoadBusinesses,
      moreBusinesses,
      isRefreshing,
      loading,
      onRefresh,
    } = this.props;

    return (
      <InfiniteScrollView
        refreshControl={
          <RefreshControl
            refreshing={ isRefreshing }
            onRefresh={ () => onRefresh() }
            tintColor={ commonColors.theme }
          />
        }
        distanceFromEnd={10}
        canLoadMore={ moreBusinesses }
        onLoadMoreAsync={ ()=> onLoadBusinesses() }
      >
        {this.renderSpecialHeader}
        <ListView
          enableEmptySections={ true }
          dataSource={ this.dataSource.cloneWithRows(businesses) }
          renderRow={ this.renderRow.bind(this) }
          contentContainerStyle={ styles.categoryDetailListView }
          renderFooter={ this.renderFooter.bind(this) }
        />
      </InfiniteScrollView>
    );
  }
}

export default connect(state => ({
  status: state.search.status,
    commonStatus: state.common.status,
    param: state.common.param,
  }),
  (dispatch) => ({
    actions: bindActionCreators(searchActions, dispatch)
  })
)(BusinessesListView);

const styles = StyleSheet.create({
  categoryDetailListView: {
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
  },
  specialHeader: {
    width: commonStyles.screenWidth,
    // height: 152,
    justifyContent: 'center',
    alignItems: 'center',
  },
  specialTopContentContainer: {
    marginHorizontal: 34,
    marginTop: 14,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageSpecialSearch: {
    width: 107,
    height: 99,
  },
  textRat: {
    flex: 1,
    marginLeft: 20,
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
  },
  textDescription: {
    marginTop: 7,
    marginBottom: 10,
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans',
    fontSize: 12,
  },
});