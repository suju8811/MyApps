'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  ListView,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  RefreshControl,
  ActivityIndicator,
  Alert,
} from 'react-native';

import { bindActionCreators } from 'redux';
import * as searchActions from '../actions';
import * as commonActions from '../../common/actions';
import * as commonActionTypes from '../../common/actionTypes';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'
import InfiniteScrollView from 'react-native-infinite-scroll';

import NavSearchBar from '../../components/navSearchBar';
import EventsListCell from '../components/eventsListCell';
import * as commonColors from '../../styles/commonColors';
import  * as commonStyles from '../../styles/commonStyles';

import UtilService from '../../components/util'
import bendService from '../../bend/bendService'
import * as _ from 'underscore'
import RecentSearch from '../components/recentSearch';

class ActionView extends Component {
  constructor(props) {
    super(props);

    this.dataSource = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });

    this.offset = 0;
    this.limit = 20;
    this.searchText = '';
    this.more = true;

    this.state = {
      isRefreshing: false,

      // currentLocation: null,
      actions: [],
      categoryIcons: [],

      actionsQuery: {
        more: true,
        loading: false,
      },
    };
  }

  componentDidMount() {
    this.hasMounted = true
    this.loadAllData();
    UtilService.mixpanelEvent("Browsed Actions")
  }

  componentWillReceiveProps(newProps) {
    if (newProps.commonStatus === commonActionTypes.UPDATE_USER_ACTIVITY) {
      var param = newProps.param;
      if(param.activityType == 'action') {
        var actions = this.state.actions;
        var exist = _.find(actions, (o)=>{
          return o._id == param.activityId;
        })

        if(exist) {
          exist.userActivity = param.userActivity

          this.setState(actions)
        }
      }
    }
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  onBack() {
    Actions.pop()
  }

  onPressedActionsCell (action) {
    Actions.ActionDetail({
      action: action
    })
    this.props.commonActions.saveSearchText(this.searchText);
  }

  loadAllData() {
    this.offset = 0;
    this.limit = 20;
    this.searchText = '';
    this.more = true;

    this.hasMounted && this.setState({
      // currentLocation: null,
      actions: [],
      categoryIcons: [],
      actionsQuery: {
        more: true,
        loading: false,
      },
    });

    this.loadActions();
  }

  loadActions () {
    console.log("loadActions")
    this.setState({
      showRecentSearch:false
    })

    //if (this.more == false)
    //  return;

    this.hasMounted && this.setState( (state) => {
      state.actionsQuery.loading = true;
      return state;
    });

    /*navigator.geolocation.getCurrentPosition( 
      (position) => {
        this.setState({ currentLocation: position })
      },
      (error) => {
        console.log(JSON.stringify(error));
      },
      { enableHighAccuracy: commonStyles.geoLocation.enableHighAccuracy, timeout: commonStyles.geoLocation.timeout, maximumAge: commonStyles.geoLocation.maximumAge }
    );*/

    var searchText = this.searchText
    var param = {
      type: 'action',
      offset: this.offset,
      limit: this.limit,
      query: this.searchText
    }

    console.log('loadActions', param)

    if(_.isEqual(param, this.oldParam)) {
      return;
    }

    this.oldParam = param

    bendService.searchActivity(param, (error, result) => {

      if(searchText != this.searchText) {
        return;
      }

      this.hasMounted && this.setState( (state) => {
        state.actionsQuery.loading = false;
        return state;
      });

      this.hasMounted && this.setState({ isRefreshing: false });

      if (error) {
        console.log("search failed", error)
        return
      }

      if (result.data.action.length < this.limit) {
        this.more = false;
        this.hasMounted && this.setState( (state) => {
          state.actionsQuery.more = false;
          return state;
        });
      }

      this.state.actions = this.state.actions.concat(result.data.action);
      this.hasMounted && this.setState({ actions: this.state.actions });

      const imageOffset = this.offset;
      this.offset += this.limit;

      result.data.action.map( (action, index) => {

        this.hasMounted && this.setState( (state) => {
          state.categoryIcons[imageOffset + index] = UtilService.getCategoryIconFromSlug(action);
          return state;
        });

      });
    })
  }

  renderActionsListRow(rowData, sectionID, rowID) {

    return (
      <EventsListCell
        title={ rowData.name }
        icon={ this.state.categoryIcons[rowID] }
        points={ UtilService.getPoints(rowData) }
        onClick={ () => this.onPressedActionsCell(rowData) }
        userActivity={rowData.userActivity}
      />
    );
  }

  onSearchChange(text) {
    if(text == '') {
      this.setState({
        showRecentSearch:true
      })

      return
    } else {
      this.setState({
        showRecentSearch:false
      })
    }
    this.searchText = text
    this.state.actions = [];
    this.offset = 0;
    this.limit = 20;
    this.more = true;

    this.hasMounted && this.setState({
      // currentLocation: null,
      actions: this.state.actions,
      categoryIcons: [],

      actionsQuery:{
        more: true,
        loading: false,
      },
    });

    this.loadActions();
  }

  onSearchCancel() {
    this.setState({
      showRecentSearch:false
    })

    this.offset = 0;
    this.searchText = '';
    this.more = true;
    this.oldParam = {}

    this.hasMounted && this.setState( (state) => {
      state.actionsQuery.more = true;
      state.actions = [];
      state.categoryIcons = [];
      return state;
    })

    this.loadActions();
  }

  renderFooter() {
    if ((!this.state.isRefreshing) && (this.state.actionsQuery.loading)) {
      return (
        <ActivityIndicator
          hidesWhenStopped={ true }
          style={{ marginVertical: 15 }}
        />
      );
    } 

    return null;
  }

  onRefresh() {
    this.hasMounted && this.setState({ isRefreshing: true });
    this.loadAllData();    
  }

  renderContent() {
    const {showRecentSearch} = this.state

    if(showRecentSearch) {
      return (<RecentSearch onSearch={this.onSearchChange.bind(this)}/>)
    }

    return (
      <KeyboardAwareView>
        <ListView
          renderScrollComponent={ props => <InfiniteScrollView distanceFromEnd={10} {...props}/> }
          enableEmptySections={ true }
          dataSource={ this.dataSource.cloneWithRows(this.state.actions) }
          renderRow={ this.renderActionsListRow.bind(this) }
          contentContainerStyle={ styles.listViewWrapper }
          renderFooter={ this.renderFooter.bind(this) }

          refreshControl={
            <RefreshControl
              refreshing={ this.state.isRefreshing }
              onRefresh={ () => this.onRefresh() }
              tintColor={ commonColors.theme }
            />
          }
          canLoadMore={ this.state.actionsQuery.more }
          onLoadMoreAsync={ ()=> this.loadActions() }
        />
      </KeyboardAwareView>
    )
  }

  render() {
    return (
      <View style={ styles.container }>
        <NavSearchBar
          buttons={ commonStyles.NavBackButton }
          onBack={ this.onBack }
          onFocus={ () => {
            this.setState({
              showRecentSearch:this.searchText==''
            })
          } }
          placeholder={ 'Search for actions' }
          onSearchChange={ (text) => this.onSearchChange(text) }
          onCancel={ () => this.onSearchCancel() }
          query={ this.searchText }
        />
        {this.renderContent()}
      </View>
    );
  }
}

export default connect(state => ({
  status: state.search.status,
  commonStatus: state.common.status,
  param: state.common.param,
  }),
  (dispatch) => ({
    actions: bindActionCreators(searchActions, dispatch),
    commonActions: bindActionCreators(commonActions, dispatch)
  })
)(ActionView);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listViewWrapper: {
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
  },
});
