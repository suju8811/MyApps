import {
  AsyncStorage
} from 'react-native';

import * as types from './actionTypes';
import bendService from '../bend/bendService'
import Cache from '../components/Cache'
import * as _ from 'underscore'

export function likeRecentActivity(activity, like) {
  return dispatch => {
    bendService.likeActivity(activity, like, (error, result) => {
      if (error) {
        console.log(error);
        dispatch({ type: types.RECENT_ACTIVITY_LIKE_ERROR });
      } 
      else {
        dispatch({ type: types.RECENT_ACTIVITY_LIKE_SUCCESS, result: result, recentActivityId: activity._id, recentActivityLike: like });
      }
    })
  }  
}

export function captureActivity(activityId) {
  //console.log("captureActivity", activityId, flag)
  return dispatch => {
    dispatch({ type: types.ACTIVITY_CAPTURE_SUCCESS, activityId: activityId});
  }
}

export function removeActivity(activityId) {
  //console.log("captureActivity", activityId, flag)
  return dispatch => {
    dispatch({ type: types.ACTIVITY_REMOVE_SUCCESS, activityId: activityId});
  }
}

export function updateRecentPinnedActivities() {
  return dispatch => {
    bendService.getRecentPinnedActivities( (error, result) => {
      if (!error) {
        if(result && result.length > 0) {
          Cache.gotPinSuggestion = 'Yes'
        } else {
          //got pin suggestion
          AsyncStorage.getItem('@milkcrate:gotPinSuggestion@' + bendService.getActiveUser()._id).then((ret)=>{
            if(ret != null) {
              Cache.gotPinSuggestion = ret
            } else {
              Cache.gotPinSuggestion = null
            }
          })
        }
        dispatch({ type: types.RECENT_PINNED_ACTIVITIES, recentPinnedActivities: result });
      }
    });
  }
}


export function updateAllPinnedActivities() {
  return dispatch => {
    bendService.getAllPinnedActivities((error, result) => {
      if (!error) {
        dispatch({ type: types.ALL_PINNED_ACTIVITIES, allPinnedActivities: result });
      }
    });
  }
}


export function getCurrentUserProfile() {

  return dispatch => {
    bendService.getUser( (error, result) => {
      if (!error) {
        dispatch({ type: types.CURRENT_USER_PROFILE, currentUser: result });
      }
    });
  }
}

export function setSearchText(text) {

  return dispatch => {
    dispatch({ type: types.SET_SEARCH_TEXT, searchText: text });
  }
}

export function saveSearchText(text) {
  return dispatch => {
    if ((text === '') || (text === undefined))  {
      return;
    };

    var old = _.find(Cache.searchHistory, (o)=>{
      return o.searchText == text
    })
    if(old) {
      Cache.searchHistory.splice(Cache.searchHistory.indexOf(old), 1)
    }
    Cache.searchHistory.unshift({
      searchText:text,
      time:Date.now()
    })

    /*if(Cache.searchHistory.length > 15) {
      Cache.searchHistory.length = 15 //keep 10 in maximum
    }*/

    AsyncStorage.setItem('@milkcrate:searchHistory', JSON.stringify(Cache.searchHistory));

    dispatch({ type: types.SAVE_SEARCH_TEXT, searchText: text });
  }
}

export function updateUserAddress() {
  return dispatch => {
    dispatch({ type: types.UPDATE_USER_ADDRESS});
  }
}

export function reset() {

  return dispatch => {
    dispatch({ type: types.RESET });
  }
}

export function appActivated() {
  return dispatch => {
    dispatch({ type: types.APP_ACTIVATED});
  }
}

export function appInActivated() {
  return dispatch => {
    dispatch({ type: types.APP_INACTIVATED});
  }
}

export function updateUserActivity(activityId, activityType, userActivityData) {
  console.log('updateUserActivity', activityId, activityType, userActivityData)
  return dispatch => {
    dispatch({ type: types.UPDATE_USER_ACTIVITY, param:{
      activityId: activityId,
      activityType: activityType,
      userActivity: userActivityData
    }});
  }
}
