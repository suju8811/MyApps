'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ScrollView,
  ListView,
  Linking,
  TouchableOpacity,
  TouchableHighlight,
  RefreshControl,
  Alert,
  WebView,
} from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';

import { bindActionCreators } from 'redux';
import * as homeActions from '../actions';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux'
import * as commonActions from '../../common/actions';
import * as commonActionTypes from '../../common/actionTypes';

import Modal from 'react-native-modalbox';
import { default as NewModal } from 'react-native-modal';

import Carousel from 'react-native-snap-carousel';
import timer from 'react-native-timer';

import NavSearchBar from '../../components/navSearchBar';

import ChallengeCarousel from '../components/challengeCarousel';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import DailyPollStateCell from '../components/dailyPollStateCell';
import Point from '../../components/Point';
import FadeInView from '../components/fadeInView';
import FadeOutView from '../components/fadeOutView';
import EventsListCell from '../../search/components/eventsListCell';

import Swipeout from 'react-native-swipeout';

import bendService from '../../bend/bendService'
import * as _ from 'underscore'
import UtilService from '../../components/util'
import Cache from '../../components/Cache'
import AwesomeModal from '../../components/AwesomeModal';
import CarbonTracking from './carbonTracking';
import EarnedPoint from '../../components/earnedPoint';
import ExploreWaysListCell from '../../search/components/exploreWaysListCell';

import * as commonStyles from '../../styles/commonStyles';
import * as commonColors from '../../styles/commonColors';
import NavTitleBar from '../../components/navTitleBar';
import EntypoIcon from 'react-native-vector-icons/Entypo';

import { screenWidth, screenHeight } from '../../styles/commonStyles';
import FilterSearch from '../../search/containers/filterSearch';
import SimpleProfile from "../components/SimpleProfile";

const carouselLeftMargin = (commonStyles.carouselerWidth - commonStyles.carouselItemWidth) / 2 - commonStyles.carouselItemHorizontalPadding;

const pinIcon = require('../../../assets/imgs/pin-home.png');
const carbonIcon = require('../../../assets/imgs/carbon.png');

const trainIcon = require('../../../assets/imgs/carbon-icons/train.png');
const trainSelectIcon = require('../../../assets/imgs/carbon-icons/train-select.png');
const carIcon = require('../../../assets/imgs/carbon-icons/car.png');
const carSelectIcon = require('../../../assets/imgs/carbon-icons/car-select.png');
const bicycleIcon = require('../../../assets/imgs/carbon-icons/bicycle.png');
const bicycleSelectIcon = require('../../../assets/imgs/carbon-icons/bicycle-select.png');
const walkIcon = require('../../../assets/imgs/carbon-icons/walk.png');
const walkSelectIcon = require('../../../assets/imgs/carbon-icons/walk-select.png');
const busIcon = require('../../../assets/imgs/carbon-icons/bus.png');
const busSelectIcon = require('../../../assets/imgs/carbon-icons/bus-select.png');

const exploreWays = [
  {
    key: 'businesses',
    title: 'Places',
    description: 'Check in to local, sustainable businesses nearby',
    icon: require('../../../assets/imgs/businesses.png'),
    iconWidth: 26,
    iconHeight: 21,
    enabled:true,
  },
  {
    key: 'events',
    title: 'Events',
    description: 'Register for green events and add to your calendar',
    icon: require('../../../assets/imgs/events.png'),
    iconWidth: 26,
    iconHeight: 25,
    enabled:true,
  },
  {
    key: 'volunteer_opportunities',
    title: 'Volunteer Opportunities',
    description: 'Find one that’s right for you',
    icon: require('../../../assets/imgs/volunteer.png'),
    iconWidth: 26,
    iconHeight: 25,
    enabled:true,
  },
  {
    key: 'actions',
    title: 'Take Action',
    description: 'Explore easy, self-reported lifestyle behaviors',
    icon: require('../../../assets/imgs/actions.png'),
    iconWidth: 26,
    iconHeight: 22,
    enabled:true,
  },
  {
    key: 'browse',
    title: 'Browse By Category',
    description: 'See your most recent activities',
    icon: require('../../../assets/imgs/browse.png'),
    iconWidth: 26,
    iconHeight: 21,
    enabled:true,
  },
  {
    key: 'recent',
    title: 'Recent',
    description: 'See your most recent activities',
    icon: require('../../../assets/imgs/recent.png'),
    iconWidth: 26,
    iconHeight: 21,
    enabled:true,
  },
];

const carbonIcons = [
  {
    icon:trainIcon,
    selectedIcon:trainSelectIcon,
    name:'Train',
    value:13,
  },
  {
    icon:carIcon,
    selectedIcon:carSelectIcon,
    name:'Car',
    value:10,
  },
  {
    icon:bicycleIcon,
    selectedIcon:bicycleSelectIcon,
    name:'Bicycle',
    value:8,
  },
  {
    icon:walkIcon,
    selectedIcon:walkSelectIcon,
    name:'Walk',
    value:24,
  },
  {
    icon:busIcon,
    selectedIcon:busSelectIcon,
    name:'Bus',
    value:12,
  },
]

class Feed extends Component {
  constructor(props) {
    super(props);

    this.dataSource = new ListView.DataSource(
        { rowHasChanged: (r1, r2) => r1 !== r2 });

    this.dataSourceExploreWays = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });

    this.dataSourceCategories = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });

    this.dataSourceRecentActivity = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      tiles:Cache.getScreen('Home').tiles || [],
      exploreWays: _.clone(exploreWays),

      isRefreshing: true,

      selectedDailyPollValue: '',
      selectedDailyPollIndex: -1,
      selectedDailyPollStateMode: false,

      challenges: [],
      community: {},
      categories: [],
      pollQuestion:{
        question: {},
        answers: [],
        myAnswer: null,
      },

      hasNewAlert: false,
      alerts: [],
      lastAlertTime: 0,
      //recentPines:[],
      //hasMorePins:false,
      showAwesomeModal:false,
      user:{},
      showCarbonModal:false,
      userAddress:{
        homeAddress:null,
        workAddress:null,
      },
      filterMode:false,
      searchText: '',
      searchAutoFocus: false,

      animationPoints: 0,
      showAnimiation: false,
    };

    this.websiteChallengePoints = 0;
    this.renderTile.bind(this)
  }

  componentDidMount() {
    this.hasMounted = true
    this.loadAllData();
    this.loadAlerts();
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  componentWillReceiveProps(newProps) {

    if (newProps.commonStatus === commonActionTypes.APP_ACTIVATED) {
      if (this.websiteChallengePoints > 0) {
        this.hasMounted && this.setState({
          showAnimiation: true,
          animationPoints: this.websiteChallengePoints,
        });
        this.websiteChallengePoints = 0;
      }
    } else if (newProps.commonStatus === commonActionTypes.ACTIVITY_CAPTURE_SUCCESS) {
      this.loadChallenges()
    } else if (newProps.commonStatus === commonActionTypes.ACTIVITY_REMOVE_SUCCESS) {
      this.loadChallenges()
    } else if(newProps.commonStatus == commonActionTypes.UPDATE_USER_ADDRESS) {
      console.log('UPDATE_USER_ADDRESS')
      this.setState({
        userAddress:_.clone(Cache.userAddress)
      })
    }

    if (!this.state.pollQuestion.question._id || this.state.pollQuestion.myAnswer) {
      this.loadPollQuestion()
    }
  }

  loadAllData() {
    //console.log("home - loadAllData")
    this.hasMounted&&this.setState({
      selectedDailyPollValue: '',
      selectedDailyPollIndex: -1,
      selectedDailyPollStateMode: false,

      challenges: [],
      community: {},
      categories: [],
      //recentPines:[],
      //hasMorePins:false,
      pollQuestion:{
        question: {},
        answers: [],
        myAnswer: null,
      },
      user:{}
    });

    this.props.commonActions.updateRecentPinnedActivities();
    bendService.getUser((err, ret)=>{
      this.setState({
        currentUser:ret
      })
    })

    bendService.getCategories( (error, result) => {
      if (!error) {
        this.hasMounted&&this.setState({
          categories: result
        })
      }
    })

    bendService.getCommunity( (error, ret) => {
      if (error) {
        console.log(error);
        return;
      }

      let exploreWays = this.state.exploreWays;
      exploreWays[0].title = ret.placesTitle || exploreWays[0].title;
      exploreWays[0].description = ret.placesDescription || exploreWays[0].description;
      exploreWays[0].enabled = ret.placesEnabled;
      exploreWays[1].title = ret.eventsTitle || exploreWays[1].title;
      exploreWays[1].description = ret.eventsDescription || exploreWays[1].description;
      exploreWays[1].enabled = ret.eventsEnabled;
      exploreWays[2].title = ret.volunteerOpportunitiesTitle || exploreWays[2].title;
      exploreWays[2].description = ret.volunteerOpportunitiesDescription || exploreWays[2].description;
      exploreWays[2].enabled = ret.volunteerOpportunitiesEnabled;
      exploreWays[3].title = ret.actionsTitle || exploreWays[3].title;
      exploreWays[3].description = ret.actionsDescription || exploreWays[3].description;
      exploreWays[3].enabled = ret.actionsEnabled;
      
      this.hasMounted && this.setState({
        community: ret,
        exploreWays: exploreWays,
      })
    })

    this.loadPollQuestion()
    this.loadChallenges()
    this.loadUserAddresses()
  }

  loadUserAddresses() {
    bendService.getUserAddresses(bendService.getActiveUser()._id, (err, rets)=>{
      if(!err) {
        rets.map((address)=>{
          if(address.type == 'home') {
            Cache.userAddress.homeAddress = address
            this.state.userAddress.homeAddress = address
          } else {
            Cache.userAddress.workAddress = address
            this.state.userAddress.workAddress = address
          }
        })

        this.setState({
          userAddress:this.state.userAddress
        })
      }
    })
  }

  onSelectExploreWays (index, query) {
    switch (Number(index)) {
      case 0://Businesses
        Actions.BusinessesView({ query: query });
        break;
      case 1://Events
        Actions.EventsView({ query: query });
        break;
      case 2://Volunteer
        Actions.VolunteerView({ query : query });
        break;
      case 3://Take Action
        Actions.ActionView({ query: query });
        break;
      case 4://Volunteer
        Actions.BrowseCategory({ countByCategory : this.props.countByCategory });
        break;
      case 5://Recent
        Actions.RecentView();
        break;

      default:
    }
  }

  renderExploreWaysRow(rowData, sectionID, rowID) {
    if(rowData.enabled === false)
      return null;

    if(rowData.title == 'Recent' && this.state.recentCount == 0)  return null;

    return (
      <ExploreWaysListCell
        key={ rowID }
        title={ rowData.title }
        description={ rowData.description }
        icon={ rowData.icon }
        iconWidth={ rowData.iconWidth }
        iconHeight={ rowData.iconHeight }
        onClick={ () => this.onSelectExploreWays(rowID, null) }
      />
    );
  }

  loadChallenges() {
    bendService.getWeeklyChallenges( "", (error, result) => {
      if (error) {
        console.log(error);
        return;
      }

      console.log("challenges", result)
      this.hasMounted&&this.setState({ challenges: result });
      Cache.setMapData('challenges', result)
    })
  }

  loadAlerts() {
    //first load alerts
    bendService.getUserAlerts( (error, result)=>{
      if (error) {
        console.log(error);
        return;
      }

      if (result.length > 0) {
        this.hasMounted && this.setState({
          alerts: result,
          lastAlertTime:result[0]._bmd.createdAt
        })
      }
    })

    var intervalHandler = setInterval(()=>{
      if(!this.hasMounted) {
        clearInterval(intervalHandler);
        return;
      }
      if (bendService.getActiveUser()) {
        bendService.getLastAlerts(this.state.lastAlertTime, (error, result) => {
          if (error) {
            console.log(error);
            return;
          }

          if (result.length > 0) {
            this.state.alerts = result.concat(this.state.alerts)
            this.hasMounted && this.setState({
              alerts: this.state.alerts,
              lastAlertTime: result[0]._bmd.createdAt,
              hasNewAlert:true
            })
          }
        })
      }
    }, 3000)
  }


  loadPollQuestion() {

    bendService.getPollQuestion( (error, question, answers, myAnswer) => {

      if (this.hasMounted) {
        this.setState({ isRefreshing: false });
      }

      if (error) {
        console.log('poll questions erroror', error);
          this.setState({
              selectedDailyPollValue: '',
              selectedDailyPollIndex: -1,
              selectedDailyPollStateMode: false,
              pollQuestion:{
                  question: {},
                  answers: [],
                  myAnswer: null,
              },
          });
        return;
      }

      if (this.hasMounted) {
        this.setState({
          pollQuestion: {
            question: question,
            answers: answers,
            myAnswer: myAnswer
          }
        });
      }
    });
  }


  onLearnMore() {
    Actions.LearnMoreModal({question:this.state.pollQuestion.question});
  }

  
  onOpenWebsite(activity) {
    this.websiteChallengePoints = activity.points;
    this.setState({
      challenges: [],
    },() => {
      this.props.commonActions.captureActivity(activity._id);
    });
  }


  getChallengeCarousel (entries) {
    return entries.map( (entry, index) => {
      let image, subtitle, icon, points, url;
      let funcOpenWebsite = null;

      if (entry.type != 'website') {
        const category = bendService.getActivityCategory(this.state.categories, entry.activity)
        if (category == null) {
          return null;
        }
        image = UtilService.getChallengeRemoteImage(category, entry.imageNum||1)
        subtitle = entry.activity.name
        icon = UtilService.getCategoryRemoteIcon(category)
        points = Number(entry.points)||(entry.activity.points ? Math.max(Number(entry.activity.points), 1) : 1)
        url = entry.activity.url
      } else {
        image = {uri:entry.coverImage._downloadURL};
        subtitle = entry.description;
        icon = null;
        points = Number(entry.points) || 1;
        url = entry.url;
        funcOpenWebsite = this.onOpenWebsite.bind(this);
      }

      return (
        <ChallengeCarousel
          key={index}
          title={entry.title}
          subtitle={subtitle}
          image={image}
          icon={icon}
          points={points}
          link={url}
          rawData={entry}
          onOpenWebsite={funcOpenWebsite}
        />
      );
    });
  }


  get showCarbon() {
    let {community, userAddress, selectedCarbon} = this.state
    if(!community.carbonTrackerEnabled) return null;
    var isValidAddress = false
    if(userAddress.homeAddress || userAddress.workAddress) {
      isValidAddress = true
    }

    return (
      <View style={{paddingHorizontal:8, paddingVertical:8}}>
        <View style={{backgroundColor:'white', borderRadius:4,borderWidth: 1, borderColor: commonColors.line, }}>
          <View style={{flexDirection:'row', borderBottomWidth:1, borderBottomColor:'#5EF2D7'}}>
            <View style={{flex:1, flexDirection:'row', padding:8}}>
              <Text style={{fontSize:14, fontFamily:'OpenSans-Semibold', color:'#A4A4A3'}}>
                Carbon Tracking
              </Text>
              <Image source={carbonIcon} style={{width:17,height:14,marginTop:3, marginLeft:5}}/>
            </View>
            {!isValidAddress && <TouchableOpacity style={{flex:1, justifyContent:'center', paddingRight:10}} onPress={()=>{
              this.setState({showCarbonModal:true})
            }}>
              <Text style={{color:'#5A90AA', fontSize:14, fontFamily:'Open Sans', textAlign:'right'}}>Get Started &gt;</Text>
            </TouchableOpacity>}
          </View>
          <View style={{paddingHorizontal:24, paddingVertical:15}}>
            {!isValidAddress && <Text style={{flexWrap:'wrap', color:'#808080', fontSize:14}}>Track your carbon emissions with our new carbon tracker and see your daily impact.</Text>}
            {isValidAddress &&
            <View style={{flex:1}}>
              <Text style={{fontSize:12, color:'#808080', fontFamily:'Open Sans'}}>
                {!selectedCarbon?'How did you get to work today?':'Thanks for letting us know!'}
              </Text>
              <View style={{flexDirection:'row', justifyContent:'space-between', marginHorizontal:-20}}>
                {
                  carbonIcons.map((carbonIcon, idx)=>{
                    var selected = (selectedCarbon==carbonIcon.name)
                    return (
                      <View key={idx} style={{flex:1, alignItems:'center', justifyContent:'flex-start', paddingTop:10}}>
                        <TouchableOpacity onPress={()=>{
                          this.setState({
                            selectedCarbon:carbonIcon.name
                          })
                        }}>
                          <Image source={selected?carbonIcon.selectedIcon:carbonIcon.icon} style={{width:32,height:32}}/>
                        </TouchableOpacity>
                        <Text style={{fontSize:10, fontFamily:"Open Sans",color:"#808080", paddingTop:3}}>{carbonIcon.name}</Text>
                        {selectedCarbon && <View style={{marginTop:10, paddingVertical:5, width:(commonStyles.screenWidth/5 - 20),
                          height:(50 + Number(carbonIcon.value) * 3), backgroundColor:(selected?'#82CCBE40':'#D4EBF640'),
                          alignItems:'center', justifyContent:'flex-start', borderWidth:1, borderColor:'#D4EBF6'}}>
                          <Text style={{color:'#5E8AA3', fontSize:14, fontFamily:'OpenSans-Semibold'}}>{carbonIcon.value} kg</Text>
                          <Text style={{color:'#5E8AA3', fontSize:14, fontFamily:'OpenSans-Semibold'}}>CO2</Text>
                        </View>}
                      </View>
                    )
                  })
                }
              </View>
            </View>}
          </View>
        </View>
      </View>
    )
  }

  showChallenges () {
    if (this.state.challenges.length == 0)
      return null;

    return (
      <View style={{height:commonStyles.carouselHeight + 16}}>
        <Carousel
          sliderWidth={ commonStyles.carouselerWidth }
          itemWidth={ commonStyles.carouselItemWidth }
          itemHeight={ commonStyles.carouselHeight }
          sliderHeight={ commonStyles.carouselHeight }
          inactiveSlideScale={ 1 }
          inactiveSlideOpacity={ 1 }
          enableMomentum={ false }
          containerCustomStyle={ styles.slider }
          contentContainerCustomStyle={ styles.sliderContainer }
          showsHorizontalScrollIndicator={ false }
          snapOnAndroid={ true }
          removeClippedSubviews={ false }
          onSnapToItem={(idx)=>{
            UtilService.mixpanelEvent("Swiped Challenges")
          }}
        >
          { this.getChallengeCarousel( this.state.challenges ) }
        </Carousel>
      </View>
    );
  }

  onPlayIntroVideo() {
    UtilService.mixpanelEvent("Played Intro Video")
    Actions.VideoPlayModal();
  }

  onRecentActivityCellPressed (activity) {
    if (activity.type == 'business') {
      Actions.BusinessesDetail({ business: activity.activity });
    } else if(activity.type == 'action') {
      Actions.ActionDetail({ action: activity.activity });
    } else if(activity.type == 'event') {
      Actions.EventDetail({ event: activity.activity });
    } else if(activity.type == 'volunteer_opportunity') {
      Actions.VolunteerDetail({ volunteer: activity.activity });
    }
  }

  showProfile() {
    let {currentUser, community} = this.state

    if(!currentUser) return null
    var avatar = currentUser.avatar?{uri:UtilService.getSmallImage(currentUser.avatar)}:UtilService.getDefaultAvatar(currentUser.defaultAvatar)
    return (
      <SimpleProfile currentUser={currentUser} avatar={avatar} community={community}/>
    )
  }

  showVideo() {
    var delta = (Date.now() - (bendService.getActiveUser()._bmd.createdAt/1000000))/1000;

    if (delta > 5 * 3600 * 24) {
      return null;
    }

    return (
      <View style={ styles.videoContainer }>
        <View style={ styles.videoTitleContainer }>
          <Text style={ styles.textTitle }>Intro Video</Text>
        </View>
        <TouchableOpacity activeOpacity={ .5 } onPress={ () => this.onPlayIntroVideo() }>
          <View style={ styles.imageVideoView }>
            <Image style={ styles.imageVideo } resizeMode='contain' source={require('../../../assets/imgs/vid.png')} />
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  get showMainDailyPollSelectMode() {
    return (
      this.state.selectedDailyPollIndex == -1 ?
        this.showDailyPollSelectMode
        :
        <FadeOutView>
          { this.showDailyPollSelectMode }
        </FadeOutView>
    );
  }

  get showDailyPollSelectMode() {
    return (
      <View style={ styles.dailyPollSelectContentContainer }>
        <RadioForm
          formHorizontal={ false }
          animation={ true }
          style={ styles.radioFormWrapper }
        >
          {
            this.state.pollQuestion.answers.map( (obj, index) => {
              var onPressRadioButton = (value, index) => {
                //console.log(value, index)
                this.hasMounted&&this.setState({
                  selectedDailyPollValue: value,
                  selectedDailyPollIndex: index
                });

                //do polling
                bendService.pollResponse(this.state.pollQuestion.question, this.state.pollQuestion.answers[index], (error, result)=>{
                  if (error) {
                    console.log(error);
                    return
                  }

                  this.hasMounted && this.setState({
                    showAnimiation: true,
                    animationPoints: UtilService.getPoints(result.activity)
                  });

                  UtilService.mixpanelEvent("Answered Daily Poll Question")
                  this.setState({
                    challenges: [],
                  },() => {
                    this.props.commonActions.captureActivity(result.activity._id);
                  });

                  //update values locally
                  var communityId = bendService.getActiveUser().community._id
                  this.state.pollQuestion.question.responseCounts = this.state.pollQuestion.question.responseCounts||{}
                  this.state.pollQuestion.question.responseCounts[communityId] = this.state.pollQuestion.question.responseCounts[communityId]||0
                  this.state.pollQuestion.question.responseCounts[communityId]++;
                  this.state.pollQuestion.answers[index].counts = this.state.pollQuestion.answers[index].counts||{}
                  this.state.pollQuestion.answers[index].counts[communityId] = this.state.pollQuestion.answers[index].counts[communityId]||0
                  this.state.pollQuestion.answers[index].counts[communityId] ++

                  this.state.pollQuestion.answers[index].percentages = this.state.pollQuestion.answers[index].percentages||{}
                  this.state.pollQuestion.answers[index].percentages[communityId] = Math.round(this.state.pollQuestion.answers[index].counts[communityId] * 100 / this.state.pollQuestion.question.responseCounts[communityId]);

                  _.map(this.state.pollQuestion.answers, (obj)=>{
                    obj.percentages = obj.percentages||{}
                    obj.counts = obj.counts||{}
                    obj.percentages[communityId] = Math.round((obj.counts[communityId]||0) * 100 / this.state.pollQuestion.question.responseCounts[communityId]);
                  })

                  this.hasMounted&&this.setState({
                    pullQuestion:this.state.pollQuestion
                  })
                })

                timer.setTimeout( this, 'DailyPollTimer', () => {
                  timer.clearTimeout(this, 'DailyPollTimer');
                  this.state.pollQuestion.myAnswer = this.state.pollQuestion.answers[index]
                  this.hasMounted&&this.setState({ selectedDailyPollStateMode: true, myAnswer: this.state.pollQuestion.myAnswer});
                }, 500);
              }

              return (
                <RadioButton
                  labelHorizontal={ true }
                  key={ index }
                  style={ (this.state.pollQuestion.answers.length - 1) == index ? styles.radioButtonWrapper : [styles.radioButtonWrapper, styles.radioButtonBorder] }
                >
                  <RadioButtonInput
                    obj={{
                      label:obj.title,
                      value:obj.position
                    }}
                    index={ index }
                    isSelected={ this.state.selectedDailyPollIndex === index }
                    onPress={ onPressRadioButton }
                    borderWidth={ 1 }
                    buttonInnerColor={ commonColors.theme }
                    buttonOuterColor={ this.state.selectedDailyPollIndex === index ? commonColors.theme : commonColors.grayMoreText }
                    buttonSize={ 16 }
                    buttonOuterSize={ 16 }
                    buttonStyle={{ }}
                    buttonWrapStyle={ styles.radioButtonInputWrapper }
                  />
                  <RadioButtonLabel
                    obj={{
                      label:obj.title,
                      value:obj.position
                    }}
                    index={ index }
                    labelHorizontal={ true }
                    onPress={ onPressRadioButton }
                    labelStyle={ styles.textRadioButtonLabel }
                    labelWrapStyle={ styles.radioButtonLabelWrapper }
                  />
                </RadioButton>
              )
            }
          )}
        </RadioForm>
      </View>
    );
  }

  showDailyPollStateMode() {
    return (
      <FadeInView>
        <View style={ styles.dailyPollSelectContentContainer }>
          {
            this.state.pollQuestion.answers.map( (obj, index) => {
              var communityId = bendService.getActiveUser().community._id;
              var percent = obj.percentages?Number(obj.percentages[communityId]||0):0
              return (
                <DailyPollStateCell
                  key={ index }
                  percent={ percent }
                  description={ obj.title }
                  selected={ this.state.selectedDailyPollIndex === index ? true : false }
                  bottomLine={ (this.state.pollQuestion.answers.length - 1) === index ? false : true }
                />
              );
            })
          }
        </View>
      </FadeInView>
    );
  }

  showDailyPoll() {
    if ((this.state.pollQuestion.question.length === 0 ) || (this.state.pollQuestion.answers.length === 0 )) {
      return null;
    }

    return (
      <View style={ styles.dailyPollContainer }>
        <Text style={ styles.textTitle }>Poll</Text>
        <View style={ styles.dailyPollMainContentContainer }>
          <View style={ styles.dailyPollTopContentContainer }>
            <View style={ styles.dailyPollTopPointContainer }>
              <View style={ styles.dailyPollLearnMoreContainer }>
                <Text style={ styles.textQuestion }>{this.state.pollQuestion.question.question||this.state.pollQuestion.question.topic}</Text>
                <View style={ styles.buttonWrapper }>
                  <TouchableOpacity activeOpacity={ .5 } onPress={ () => this.onLearnMore() }>
                    <Text style={ styles.textReadMoreButton }>Learn More</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <Point point={ 10 }/>
            </View>
          </View>

          {
            !this.state.pollQuestion.myAnswer ? this.showMainDailyPollSelectMode : this.showDailyPollStateMode
          }

        </View>
      </View>
    );
  }


  onNotifications() {
    if (this.hasMounted) {
      this.setState({ hasNewAlert: false });
    }

    Actions.Notifications({ alerts: this.state.alerts });
  }

  onGoSearchScreen() {
    this.props.onSearch();
  }

  onRefresh() {
    if (this.hasMounted) {
      this.setState({ isRefreshing: true });
    }

    this.loadAllData();
  }

  postPointAnimation() {
    this.setState({
      showAwesomeModal: Cache.isFirstPoint ? true : false,
      showAnimiation: false,
    })
  }

  onFilter() {
    this.setState({
      filterMode:true,
      searchAutoFocus: true,
    })
  }

  onSearchChange(text) {
    this.hasMounted && this.setState({ searchText: text });
  }

  onSearchFocus() {
    this.hasMounted && this.setState({
      filterMode: true,
      searchAutoFocus: false
    });
  }

  onSearchCancel() {
    this.hasMounted && this.setState({
      searchAutoFocus: false,
      filterMode: false
    });
  }

  renderActivityMenu() {
    return (
      <View style={{backgroundColor:'white', flex:1, paddingTop:30}}>
        <View style={{flexDirection:'row'}}>
          <Text style={ [styles.textTitle, {flex:1}] }>Earn Points</Text>
          <View style={{height:2,backgroundColor:'#E595D5', flex:3, marginTop:20}}></View>
        </View>
        <ListView
          dataSource={ this.dataSourceExploreWays.cloneWithRows(this.state.exploreWays) }
          renderRow={ this.renderExploreWaysRow.bind(this) }
          contentContainerStyle={ styles.listViewExploreWays }
        />
      </View>
    )
  }

  renderImageTile(tile) {
    return (
      <Image source={{uri:tile.source}} style={{width:screenWidth-16, borderRadius: 5, height:tile.maxHeight, resizeMode:'cover', margin:8, marginTop: 0}}></Image>
    )
  }

  renderButtonTile(tile) {
    /*
    "items": [
                {
                    "backgroundColor": "#336699",
                    "icon": "fa-bomb",
                    "textColor": "#ffffff",
                    "title": "Bomb"
                },
                {
                    "backgroundColor": "#003366",
                    "icon": "fa-anchor",
                    "textColor": "#ffffff",
                    "title": "Anchor"
                },
                {
                    "backgroundColor": "#6699CC",
                    "icon": "fa-puzzle-piece",
                    "textColor": "#ffffff",
                    "title": "Puzzles"
                }
            ],
     */
    let gap = 8
    let itemCount = tile.items.length
    let width = (screenWidth - gap) / itemCount - gap
    return (
      <View style={{flexDirection:'row', justifyContent:'space-between', paddingHorizontal:gap, paddingVertical:gap}}>
        {
          tile.items.map((item, index)=>{
            let iconName = item.icon.substr(3).replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); });

            return (
              <TouchableOpacity
                key={index}
                style={{backgroundColor:item.backgroundColor, borderRadius:4, width:width, height:width,
                  justifyContent:'center', alignItems:'center'}}
                onPress={()=>{}}>
                <Text style={{fontSize: 24, textAlign: 'center', color:item.textColor}}>
                  <FontAwesome>{Icons[iconName]}</FontAwesome>
                </Text>
                <Text style={{fontSize: 16, textAlign: 'center', color:item.textColor, marginTop:10}}>
                  {item.title}
                </Text>
              </TouchableOpacity>
            )
          })
        }
      </View>
    )
  }

  renderTile(tile, key) {
    let renderTile = null
    switch(tile.type) {
      case 'user-summary':
        renderTile = this.showProfile()
        break;
      case 'challenge-carousel':
        renderTile= this.showChallenges()
        break;
      case 'poll-question':
        renderTile = this.showDailyPoll()
        break;
      case 'activity-menu':
        renderTile = this.renderActivityMenu();
        break;
      case 'buttons-1x3':
        renderTile =  this.renderButtonTile(tile);
        break;
      case 'image':
        renderTile = this.renderImageTile(tile);
        break;
    }

    return (
      <View key={key}>
        {
          renderTile
        }
      </View>
    )
  }

  render() {
    let {community, filterMode, tiles} = this.state
    return (
      <View style={ styles.container }>
        <NavSearchBar
          buttons={ !filterMode?commonStyles.NavNotificationButton|commonStyles.NavFilterButton:commonStyles.NavNotificationButton }
          isNewNotification={ this.state.hasNewAlert }
          onNotifications={ () => this.onNotifications() }
          onFilter={ () => this.onFilter() }
          onSearchChange={ (text) => this.onSearchChange(text) }
          onCancel={ () => this.onSearchCancel() }
          onFocus={ () => this.onSearchFocus() }
          searchAutoFocus={ this.state.searchAutoFocus }
          title={!filterMode?(community.name||'Title'):null}
        />
        {!filterMode?<ScrollView
          style={ styles.scrollView }
          showsVerticalScrollIndicator={ false }
          refreshControl={
            <RefreshControl
              refreshing={ this.state.isRefreshing }
              onRefresh={ () => this.onRefresh() }
              tintColor={ commonColors.theme }
            />
          }
        >

          {
            tiles.map((tile, index)=>{
              return this.renderTile(tile, index)
            })
          }
        </ScrollView>:
          <FilterSearch
            searchText={ this.state.searchText }
          />
        }
        <Modal
          isOpen={this.state.showAwesomeModal}
          style={styles.modalBoxStyle}
          ref={"modal"}
          swipeToClose={false}
          backdropPressToClose={false}
        >
          <AwesomeModal
            points={Cache.cacheMap["user"]?Cache.cacheMap["user"].points:0}
            onPressOK={() => {
              this.setState({showAwesomeModal:false})
            }}
          />
        </Modal>
        <NewModal
          style={styles.modalStyle}
          backdropColor='transparent'
          isVisible={this.state.showCarbonModal}
        >
          <CarbonTracking
            onClose={()=>{
              this.setState({
                showCarbonModal:false
              })
            }}
          />
        </NewModal>

        <EarnedPoint
          show={this.state.showAnimiation}
          point={this.state.animationPoints}
          onFinish={this.postPointAnimation.bind(this)}
        />
      </View>
    );
  }
}

export default connect(props => ({
    status: props.home.status,
    commonStatus: props.common.status,
    //recentPines: props.common.recentPinnedActivities,
  }),
  (dispatch) => ({
    actions: bindActionCreators(homeActions, dispatch),
    commonActions: bindActionCreators(commonActions, dispatch),
  })
) (Feed);

const pageMargin=24;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
    backgroundColor: '#f4fafd',
  },
  slider: {
  },
  sliderContainer: {
    marginLeft: -carouselLeftMargin,
  },
  textTitle: {
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    padding: 10,
  },
  videoContainer: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 10,
  },
  videoTitleContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  dailyPollContainer: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 10,
  },
  dailyPollMainContentContainer: {
    flex: 1,
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
    borderBottomWidth: 1,
    borderBottomColor: commonColors.line,
  },
  dailyPollTopContentContainer: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    borderStyle: 'solid',
    borderBottomWidth: 1,
    borderBottomColor: commonColors.line,
    paddingVertical: 10,
    paddingLeft: 25,
    paddingRight: 10,
  },
  dailyPollTopPointContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  dailyPollLearnMoreContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  textQuestion: {
    color: commonColors.question,
    fontFamily: 'Open Sans',
    fontSize: 14,
  },
  buttonWrapper: {
    flex: 1,
    paddingTop: 8,
  },
  textReadMoreButton: {
    color: commonColors.title,
    fontFamily: 'Open Sans',
    fontSize: 14,
    backgroundColor: 'transparent',
  },
  textRadioButtonLabel: {
    color: commonColors.title,
    fontFamily: 'Open Sans',
    fontSize: 14,
    textAlign: 'left',
    backgroundColor: 'transparent',
  },
  radioButtonLabelWrapper: {
    flex: 1,
    marginLeft: 5,
    marginRight: 10,
    paddingVertical: 10,
  },
  radioButtonInputWrapper: {
    paddingVertical: 10,
    marginLeft: 20,
  },
  radioButtonWrapper: {
    flex: 1,
  },
  radioButtonBorder: {
    borderBottomWidth: 1,
    borderBottomColor: commonColors.line,
  },
  radioFormWrapper: {
    flex: 1,
  },
  dailyPollSelectContentContainer: {

  },
  imageVideo: {
    flexShrink: 1,
    height: 200,
    backgroundColor:'black',
  },
  imageVideoView: {
    flexDirection:'row',
    alignItems:'center',
  },
  sectionHeaderContainer: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: commonColors.line,
    paddingTop:10
  },
  textSectionTitle: {
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    marginLeft: 10,
    marginBottom: 8,
  },
  pinCellContainer: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    paddingVertical: 15,
    paddingHorizontal: 22,
    borderBottomWidth: 1,
    borderBottomColor: commonColors.line,
    borderStyle: 'solid',
    alignItems: 'center',
  },
  pinTextTitle: {
    flex: 1,
    color: commonColors.title,
    fontFamily: 'Open Sans',
    fontSize: 14,
  },
  imagePin: {
    width: 9,
    height: 13,
    marginLeft: 5,
    marginTop: 5
  },
  modalBoxStyle: {
    backgroundColor:'transparent',
  },
  modalStyle: {
    margin: 0,
  },
  rightIcon: {
    paddingTop: 2.5,
    alignSelf: 'flex-end',
  },
});
