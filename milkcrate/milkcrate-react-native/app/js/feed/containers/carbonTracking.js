'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  Alert,
} from 'react-native';

import { bindActionCreators } from 'redux';
import * as commonActions from '../../common/actions';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import Spinner from 'react-native-loading-spinner-overlay';
import timer from 'react-native-timer';
import TextField from 'react-native-md-textinput';
import ModalDropdown from 'react-native-modal-dropdown';
import moment from 'moment';

import NavTitleBar from '../../components/navTitleBar';
import * as commonColors from '../../styles/commonColors';
import * as commonStyles from '../../styles/commonStyles';

import bendService from '../../bend/bendService'
import Cache from '../../components/Cache'
import * as _ from 'underscore'
import * as async from 'async'
import UtilService from '../../components/util'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Geocoder from 'react-native-geocoder';

const triangle_down = require('../../../assets/imgs/triangle_down.png');
const camera = require('../../../assets/imgs/camera_full.png');

const arrayGender = ['Male', 'Female', 'Other', 'Prefer not to say'];


class CarbonTracking extends Component {

  constructor(props) {
    super(props);

    this.user = bendService.getActiveUser();
    this.state = {
      user: this.user,
      homeAddress: {
        user:bendService.makeBendRef('userAddress', this.user._id),
        type:'home',
      },
      workAddress: {
        user:bendService.makeBendRef('userAddress', this.user._id),
        type:'work'
      },
    };
  }

  componentDidMount() {
    this.hasMounted = true
    console.log("Cache.userAddress", Cache.userAddress)
    _.extend(this.state.homeAddress, Cache.userAddress.homeAddress)
    _.extend(this.state.workAddress, Cache.userAddress.workAddress)
    this.setState({
      homeAddress:this.state.homeAddress,
      workAddress:this.state.workAddress,
    })
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  getAddress(address) {
    if(!address) return null;

    if(address.address1 || address.city || address.state) {
      var ret = []
      if(address.address1) {
        ret.push(address.address1)
      }
      if(address.city) {
        ret.push(address.city)
      }
      if(address.state) {
        ret.push(address.state)
      }
      return ret.join(',')
    } else
      return null
  }

  async onSave() {

    this.hasMounted && this.setState({ activityStatus: true });

    var homeAddress = this.getAddress(this.state.homeAddress)
    var workAddress = this.getAddress(this.state.workAddress)

    if(homeAddress == workAddress) {
      Alert.alert('Wrong address', 'A home address must be different with a work address.');
      return;
    }

    var oldHomeAddress = this.getAddress(Cache.userAddress.homeAddress)
    if(homeAddress && homeAddress != oldHomeAddress) {
      try {
        const res = await Geocoder.geocodeAddress(homeAddress);
        console.log('homeAddress geocoder', res)
        if(res && res[0].position) {
          this.state.homeAddress._geoloc = [res[0].position.lng, res[0].position.lat]
        }
      } catch(e) {
        console.log('home address geo coder error', e)
      }
    }

    var oldWorkAddress = this.getAddress(Cache.userAddress.workAddress)
    if(workAddress && workAddress != oldWorkAddress) {
      try {
        const res = await Geocoder.geocodeAddress(workAddress);
        if(res && res[0].position) {
          console.log('work address geocoder', res)
          this.state.workAddress._geoloc = [res[0].position.lng, res[0].position.lat]
        }
      } catch(e) {
        console.log('work address geo coder error', e)
      }
    }

    if(_.isEqual(this.state.homeAddress._geoloc, this.state.workAddress._geoloc)) {
      Alert.alert('Wrong address', 'A home address must be different with a work address.');
      return;
    }

    async.parallel([
      (cb)=>{
        bendService.saveUserAddress(this.state.homeAddress, (error, ret)=>{
          if(!error) {
            Cache.userAddress.homeAddress = ret
          }
          cb(error, ret)
        })
      },
      (cb)=>{
        bendService.saveUserAddress(this.state.workAddress, (error, ret)=>{
          if(!error) {
            Cache.userAddress.workAddress = ret
          }
          cb(error, ret)
        })
      }
    ], (err, ret)=>{
      console.log('save user address', err, ret)
      this.setState({ activityStatus: false })
      if(!err) {
        this.props.actions.updateUserAddress();
      }
      this.props.onClose()
    })
  }

  onSelectHomeState(data) {
    this.state.homeAddress.state = data;
    this.hasMounted && this.setState({ homeAddress: this.state.homeAddress });
  }

  onSelectWorkState(data) {
    this.state.workAddress.state = data;
    this.hasMounted && this.setState({ workAddress: this.state.workAddress });
  }

  render() {
    return (
      <View style={ styles.container }>
        <Spinner visible={ this.state.activityStatus }/>

        <NavTitleBar
          onCarbonClose={ this.props.onClose }
          onSave={ this.onSave.bind(this) }
          title ='Carbon Tracking'
        />
        <KeyboardAwareScrollView>
          <View style={{flex:1}}>
            <Image source={require('../../../assets/imgs/carbon-cover.png')} style={{width:commonStyles.screenWidth, height:200, resizeMode:'cover'}}></Image>
          </View>
          <Text style={{fontSize:16, color:'#A4A4A3', padding:10, textAlign:'center'}}>Enter your home and work addresses below.</Text>
          <View style={ styles.titleContainer }>
            <Text style={ styles.textTitle }>Home</Text>
          </View>

          {/*<Text style={ styles.textSettingsSection }>User Profile</Text>*/}
          <TextField
            label='Home Street Address'
            autoCorrect={ false }
            inputStyle={ inputStyle }
            labelStyle={ labelStyle }
            wrapperStyle={ wrapperStyle }
            highlightColor='#fff'
            borderColor='#fff'
            onChangeText={ (text) => { this.state.homeAddress.address1 = text }}
            value={ this.state.homeAddress.address1 }
          />
          <View style={{flexDirection:'row'}}>
            <View style={{flex:2}}>
              <TextField
                label='Home City'
                autoCorrect={ false }
                inputStyle={ inputStyle }
                labelStyle={ labelStyle }
                wrapperStyle={ wrapperStyle }
                highlightColor='#fff'
                borderColor='transparent'
                value={this.state.homeAddress.city}
                onChangeText={ (text) => { this.state.homeAddress.city = text }}
              />
            </View>
            <View style={[{flex:1}, styles.cellContainer]}>
              <Text style={ styles.textCellTitle }>Home State</Text>
              <View style={ styles.dropDownWrapper }>
                <ModalDropdown
                  options={ UtilService.allStates }
                  defaultValue={ this.state.homeAddress.state }
                  style={ styles.dropdown }
                  textStyle ={ styles.dropDownText }
                  dropdownStyle={ styles.dropdownStyle }
                  dropdownTextStyle={ styles.dropdownTextStyle }
                  onSelect={ (rowId, rowData) => this.onSelectHomeState(rowData) }
                />
                <Image source={ triangle_down } style={ styles.imageTriangleDown }/>
              </View>
            </View>
          </View>

          <View style={ [styles.titleContainer, {borderTopWidth:1,borderTopColor:commonColors.line}] }>
            <Text style={ styles.textTitle }>Work</Text>
          </View>

          {/*<Text style={ styles.textSettingsSection }>User Profile</Text>*/}
          <TextField
            label='Work Street Address'
            autoCorrect={ false }
            inputStyle={ inputStyle }
            labelStyle={ labelStyle }
            wrapperStyle={ wrapperStyle }
            highlightColor='#fff'
            borderColor='#fff'
            onChangeText={ (text) => { this.state.workAddress.address1 = text }}
            value={ this.state.workAddress.address1 }
          />
          <View style={{flexDirection:'row', marginBottom:50}}>
            <View style={{flex:2}}>
              <TextField
                label='Work City'
                autoCorrect={ false }
                inputStyle={ inputStyle }
                labelStyle={ labelStyle }
                wrapperStyle={ wrapperStyle }
                highlightColor='#fff'
                borderColor='transparent'
                value={this.state.workAddress.city}
                onChangeText={ (text) => { this.state.workAddress.city = text }}
              />
            </View>
            <View style={[{flex:1}, styles.cellContainer]}>
              <Text style={ styles.textCellTitle }>Work State</Text>
              <View style={ styles.dropDownWrapper }>
                <ModalDropdown
                  options={ UtilService.allStates }
                  defaultValue={ this.state.workAddress.state }
                  style={ styles.dropdown }
                  textStyle ={ styles.dropDownText }
                  dropdownStyle={ styles.dropdownStyle }
                  dropdownTextStyle={ styles.dropdownTextStyle }
                  onSelect={ (rowId, rowData) => this.onSelectWorkState(rowData) }
                />
                <Image source={ triangle_down } style={ styles.imageTriangleDown }/>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

export default connect(state => ({
  }),
  (dispatch) => ({
    actions: bindActionCreators(commonActions, dispatch)
  })
)(CarbonTracking);

const inputStyle = {
  color: commonColors.grayText,
  fontFamily: 'OpenSans-Semibold',
  fontSize: 14,
  paddingHorizontal: 16,
  marginTop:-5
};
const labelStyle={
  color: commonColors.grayMoreText,
  fontFamily: 'Open Sans',
  fontSize: 12,
  paddingHorizontal: 16,
  marginTop:-7
};

const wrapperStyle={
  height: 60,
  backgroundColor: '#fff',
  borderTopWidth: 1,
  borderTopColor: commonColors.line,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
  },
  scrollView: {
    backgroundColor: 'transparent',
  },
  photoWrapper: {
    width: commonStyles.screenWidth * 0.22,
    height:  commonStyles.screenWidth * 0.22,
    borderRadius: 5,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imagePhoto: {
    width: commonStyles.screenWidth * 0.22,
    height:  commonStyles.screenWidth * 0.22,
    borderRadius: 5,
  },
  textSettingsSection: {
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    marginTop: 30,
    marginLeft: 8,
    marginBottom: 8,
  },
  cellContainer: {
    height: 60,
    backgroundColor: '#fff',
    // alignItems: 'flex-start',
    // justifyContent: 'center',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
    paddingHorizontal: 16,
  },
  textCellTitle: {
    color: commonColors.grayMoreText,
    fontFamily: 'Open Sans',
    height: 18,
    fontSize: 12,
    marginTop: 8,
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  textTitle: {
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    padding: 10,
    paddingBottom:5
  },

  dropDownWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  dropdown: {
    width: commonStyles.screenWidth/3,
    height: 33,
    borderColor: '#fff',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderRadius: 4,
    marginLeft: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropDownText: {
    color: commonColors.grayText,
    width: commonStyles.screenWidth/3,
    fontFamily: 'OpenSans-Semibold',
    height: 22,
    fontSize: 14,
  },
  dropdownStyle: {
    height: 140,
    width: commonStyles.screenWidth/3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdownTextStyle: {
    width: commonStyles.screenWidth/3 - 2,
    textAlign: 'center',
  },
  imageTriangleDown: {
    width: 8,
    height: 6,
    position: 'absolute',
    right: 10,
  },
  line: {
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
  },
  saveProfileButtonWrapper: {
    height: 56,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
    borderBottomWidth: 1,
    borderBottomColor: commonColors.line,
    marginTop: 24,
  },
  textSaveProfile: {
    color: '#82ccbe',
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
  },
});
