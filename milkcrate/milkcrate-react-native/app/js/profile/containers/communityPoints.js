'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  ListView,
  RefreshControl,
  Alert,
  Image,
  ActivityIndicator,
} from 'react-native';

import { bindActionCreators } from 'redux';
import * as communityPointsActions from '../actions';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as commonActions from '../../common/actions';
import * as commonActionTypes from '../../common/actionTypes';

import NavTitleBar from '../../components/navTitleBar';
import * as commonColors from '../../styles/commonColors';
import * as commonStyles from '../../styles/commonStyles';
import LeaderboardListCell from '../components/leaderboardListCell';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import InfiniteScrollView from 'react-native-infinite-scroll';

import bendService from '../../bend/bendService'
import * as _ from 'underscore'
import UtilService from '../../components/util'
import Cache from '../../components/Cache'

class CommunityPoints extends Component {
  constructor(props) {
    super(props);

    this.dataSourceLeaderboard = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });

    this.more = true;
    this.offset = 0;
    this.loading = false;
    this.limit = 25;

    this.state = {
      userList: [],
      query:{
        more: true,
        isActivityIndicator: false,
      },
      community: {},
      isRefreshing: false,
    };
  }

  
  componentDidMount() {
    this.hasMounted = true
    this.loadAllData();
    UtilService.mixpanelEvent("Viewed Community Points")
  }


  componentWillUnmount() {
    this.hasMounted = false
  }


  componentWillReceiveProps(newProps) {    
  }


  loadAllData() {

    this.more = true;
    this.offset = 0;
    this.loading = false;
    this.limit = 25;

    this.hasMounted && this.setState({
      userList: [],
      query:{
        more: true,
        isActivityIndicator: false,
      },
      community: {},
    });

    bendService.getCommunity( (error, result) => {

      console.log("getLeaderBoardSimpleList : ", result)

      if (!error) {
        this.hasMounted && this.setState({
          community: result,
        })
      }
    });

    this.loadUserPage();    
  }


  loadUserPage() {
    if ((this.more === false) || (this.loading === true)){
      return;
    }

    this.loading = true;

    this.hasMounted && this.setState( (state) => {
        state.query.isActivityIndicator = true;
      return state;
    });

    bendService.getLeaderBoardPage(this.offset, this.limit + 1, (error, result) => {
      //console.log("getLeaderBoardPage", error, result)
      this.hasMounted && this.setState( (state) => {
        state.query.isActivityIndicator = false;
        return state;
      });

      this.setState({ isRefreshing: false });
      
      if (error) {
        console.log(error);
        return;
      }

      if (result.length < this.limit + 1) {
        this.more = false;
        this.hasMounted && this.setState( (state) => {
          state.query.more = false;
          return state;
        });
      } else {
        result.pop()
      }

      if (result.length > 0) {
        this.state.userList = this.state.userList.concat(result)
        this.offset += result.length
        this.hasMounted && this.setState({
          userList: this.state.userList
        })
      }

      this.loading = false; 
    })
  }

  renderLeaderboardRow(rowData, sectionID, rowID) {
    var previousRank = rowData.previousSprintRank, currentRank = rowData.sprintRank

    return (
      <LeaderboardListCell
        status={ previousRank == -1?0 : (previousRank < currentRank ? 2 : (previousRank > currentRank ? 1 : 0)) }
        index={ rowData.sprintRank }
        name={ rowData.name||rowData.username }
        points={ rowData.sprintPoints }
        avatar={ rowData.avatar ? UtilService.getSmallImage(rowData.avatar) : '' }
        avatarBackColor={ UtilService.getBackColor(rowData.avatar) }
        defaultAvatar={ UtilService.getDefaultAvatar(rowData.defaultAvatar) }
        currentUser={ bendService.getActiveUser().sprintRank==rowData.sprintRank }
      />
    );
  }

  onBack() {
    Actions.pop();
  }

  renderFooter() {
    if ((!this.state.isRefreshing) && (this.state.query.isActivityIndicator)) {
      return (
        <ActivityIndicator
          hidesWhenStopped={ true }
          style={{ marginVertical: 15 }}
        />
      );
    } 

    return null;
  }


  onRefresh() {
    this.hasMounted && this.setState({ isRefreshing: true });
    this.loadAllData();    
  }


  render() {
    const currentUser = bendService.getActiveUser();
    const community = this.state.community;

    return (
      <View style={ styles.container }>
        <NavTitleBar
          buttons={ commonStyles.NavBackButton }
          onBack={ this.onBack }
          title ='Your Community'
        />
        <InfiniteScrollView
          distanceFromEnd={10}
          refreshControl={
            <RefreshControl
              refreshing={ this.state.isRefreshing }
              onRefresh={ () => this.onRefresh() }
              tintColor={ commonColors.theme }
            />
          }

          canLoadMore={ this.state.query.more }
          onLoadMoreAsync={ ()=> this.loadUserPage() }
        >
          <View style={ styles.topContainer }>
            <View style={ styles.logoContainer }>
              { community.logo && <Image source={{ uri: community.logo._downloadURL }} style={ styles.imageComcast } resizeMode="contain"/> }
              { !community.logo && <Text style={ styles.textName }>{ community.name }</Text> }
            </View>
            <View style={ styles.pointContainer }>
              <View style={ styles.pointSubContainer }>
                <Text style={ styles.textValue }>{ community.points || 0 }</Text>
                <Text style={ styles.textSmall }>Total Points</Text>
              </View>
              <View style={ styles.pointSubContainer }>
                <Text style={ styles.textValue }>{ community.hours || 0 }</Text>
                <Text style={ styles.textSmall }>Hours Volunteered</Text>
              </View>
            </View>
          </View>

          <Text style={ styles.textLeaderboard }>LEADERBOARD</Text>
          <View style={ styles.orderContainer }>
            <Text style={ styles.textOrder }>{ UtilService.getPositionString(currentUser.sprintRank) } out of { community.userCount } people</Text>
            <Text style={ styles.textUpdate }>Updated every 15 mins</Text>
          </View>

          <ListView
            enableEmptySections={ true }
            dataSource={ this.dataSourceLeaderboard.cloneWithRows(this.state.userList) }
            renderRow={ this.renderLeaderboardRow.bind(this) }
            contentContainerStyle={ styles.leaderboardListView }
            renderFooter={ this.renderFooter.bind(this) }
          />

        </InfiniteScrollView>
      </View>
    );
  }
}

export default connect(state => ({
    status: state.profile.status,
    commonStatus: state.common.status,
  }),
  (dispatch) => ({
    actions: bindActionCreators(communityPointsActions, dispatch),
  })
) (CommunityPoints);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  topContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  textName: {
    color: commonColors.title,
    fontFamily: 'Open Sans',
    fontSize: 24,
    backgroundColor: 'transparent',
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  imageComcast: {
    width: 300,
    height: 100,
    marginBottom: 8
  },
  pointContainer: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  pointSubContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  textValue: {
    color: commonColors.bottomButton,
    fontFamily: 'Open Sans',
    fontSize: 16,
    fontWeight: 'bold',
  },
  textSmall: {
    color: commonColors.grayMoreText,
    fontFamily: 'Open Sans',
    fontSize: 12,
    backgroundColor: 'transparent',
  },
  orderContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  textOrder: {
    color: commonColors.grayText,
    fontFamily: 'Open Sans',
    fontSize: 12,
  },
  textUpdate: {
    marginTop: 10,
    color: commonColors.grayText,
    fontFamily: 'Open Sans',
    // fontWeight: 'bold',
    fontSize: 10,
  },
  leaderboardListView: {
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
  },
  textLeaderboard: {
    color: commonColors.grayText,
    fontFamily: 'Open Sans',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign:  'center',
  },  
});
