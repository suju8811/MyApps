'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TextInput,
  Platform,
  TouchableOpacity,
  TouchableHighlight,
  Alert,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Picker,
  Keyboard,
} from 'react-native';
import Modal from 'react-native-modalbox';
import { bindActionCreators } from 'redux';
import * as profileActions from '../actions';
import * as profileActionTypes from '../actionTypes';
import { connect } from 'react-redux';

import { Actions } from 'react-native-router-flux';
import ResponsiveImage from 'react-native-responsive-image';
import ImagePicker from 'react-native-image-picker';
import ModalDropdown from 'react-native-modal-dropdown';
import DatePicker from 'react-native-datepicker'
import timer from 'react-native-timer';
import Permissions from 'react-native-permissions';
import Orientation from 'react-native-orientation';
import ModalSelector from 'react-native-modal-selector'

import * as commonColors from '../../styles/commonColors';
import { screenWidth, screenHeight } from '../../styles/commonStyles';

import moment from 'moment';
import bendService from '../../bend/bendService'
import UtilService from '../../components/util'
import Cache from '../../components/Cache'

const background = require('../../../assets/imgs/background_profile.png');
const camera = require('../../../assets/imgs/camera_full.png');
const triangle_down = require('../../../assets/imgs/triangle_down.png');
const arrayGender = ['Male', 'Female', 'Other', 'Prefer not to say'];

class SetupProfile extends Component {
  constructor(props) {
    super(props);

    var user = bendService.getActiveUser();

    this.state = {
      profilePhoto: camera,
      profilePhotoFile: null,
      name: user.name ? user.name : '',
      birthday: user.birthdate ? moment(user.birthdate, 'YYYY-MM-DD').format('MMM DD, YYYY') : '',
      gender: user.gender ? user.gender : 'Gender',
      isUploadingFile: false,
      community:{},
      zipCode:'',
      race:'',
      modal:false,
      tempGender:null
    };
  }

  componentDidMount() {
    Orientation.unlockAllOrientations();

    if (Platform.OS === 'ios') {
      setTimeout(()=>{
        Permissions.requestPermission('notification')
          .then(response => {
            console.log(response)
          });
      }, 1000)
    }
    this.hasMounted = true
    bendService.getCommunity((err, ret)=>{
      if(!err) {
        this.hasMounted && this.setState({
          community:ret
        })
      }
    })
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  componentWillReceiveProps(newProps) {
    console.log('SetupProfile - componentWillReceiveProps')
    if (newProps.status === profileActionTypes.UPDATE_RACE) {
      console.log('UPDATE_RACE', newProps.param)
      this.setState({
        race:newProps.param.race.join(',')
      })
    }
  }

  onSelectGender(gender) {
    this.hasMounted && this.setState({
      gender: gender,
    });
  }

  onChangeBirthday(date) {
    let birthday = moment(date, 'MMM DD, YYYY');
    let today = moment();

    const age = today.diff(birthday, 'years');
    if (Number(age) < 13) {
      timer.setTimeout( this, 'AgeRequirementTimer', () => {
        timer.clearTimeout(this, 'AgeRequirementTimer');
        Alert.alert('Age Requirement Not Met', 'You must be at least 13 years of age to use this app.');
      }, 500);
      return;
    }
    this.hasMounted && this.setState({ birthday: date });
  }

  onCompleteProfile() {
    //check name
    if (this.state.name == '') {
      alert("Please input your first and last name");
      return;
    }

    this.hasMounted && this.setState({
      isUploadingFile:true
    })
    if (this.state.profilePhotoFile) {
      //upload image first
      bendService.uploadFile(this.state.profilePhotoFile, (error, file)=>{
          this.hasMounted && this.setState({
            isUploadingFile:false
          })
          if (error) {
            Alert.alert("Failed to upload file", "Please try again later");
            return;
          }

          this.updateUserInfo(file);
        },
        {
          _workflow: 'avatar'
        });
    } else {
      this.updateUserInfo();
    }
  }

  updateUserInfo(f) {
    var userData = bendService.getActiveUser();

    if (f) {
      userData.avatar = bendService.makeBendFile(f._id)
    }

    userData.name = this.state.name;

    if (this.state.birthday) {
      userData.birthdate = moment(new Date(this.state.birthday)).format('YYYY-MM-DD');
    }

    if (this.state.gender) {
      userData.gender = this.state.gender;
    }

    if(userData.email == '') {
      Alert.alert('E-mail Required', 'Please enter your email address.');
      return;
    }

    userData.email = userData.email.toLowerCase()

    if(this.state.zipCode && this.state.zipCode != '') {
      userData.zipCode = this.state.zipCode
    }
    //console.log(userData);

    bendService.updateUser(userData, (error, result)=>{
      console.log("updateUser", error, result)
      this.hasMounted && this.setState({
        isUploadingFile:false
      })
      if (error) {
        if(error.name) {
          setTimeout(()=>{
            Alert.alert("Failed to update", error.name)
          }, 200)
        }
        return;
      }

      Actions.Main();
    })
  }

  onPickProfilePhoto() {

    let options;

    if (this.state.profilePhotoFile == null) {
      options = {
        quality: 1.0,
        storageOptions: {
          skipBackup: true,
        }
      };
    } else {
      options = {
        quality: 1.0,
        storageOptions: {
          skipBackup: true,
        },
        customButtons:[{
          name:"remove",
          title:"Remove Photo"
        }]
      };
    }

    ImagePicker.showImagePicker(options, (response) => {

      if (response.customButton == 'remove') {
        this.hasMounted && this.setState({
          profilePhoto: camera,
          profilePhotoFile: null,
        });
        return;
      }
      //console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else {
        let source = { uri: response.uri };

        this.hasMounted && this.setState({
          profilePhoto: source,
          profilePhotoFile: response,
        });
      }
    });
  }

  openRaceView() {
    Cache.tempProfile = bendService.getActiveUser()
    Actions.RaceEthnicityView()
  }

  render() {
    let showRace = this.state.community.promptForRace
    let showZip = this.state.community.promptForZipCode
    let genders = ['Male', 'Female', 'Other', 'Prefer not to say']
    return (
      <View style={ styles.container }>
        <Image source={ background } style={ styles.background } resizeMode="cover">
          <View style={ styles.descriptionContainer }>
            <Text style={ styles.textTitle }>Set up Your Profile!</Text>
          </View>
          <View style={ styles.photoContainer }>
            <TouchableOpacity activeOpacity={ .5 } onPress={ () => this.onPickProfilePhoto() }>
              <View style={ styles.photoWrapper }>
                <ResponsiveImage source={ this.state.profilePhoto } style={ styles.imagePhoto } />
              </View>
            </TouchableOpacity>
            <Text style={ styles.textDescription }>Snap or upload profile photo</Text>
          </View>
          <View style={ styles.inputContainer }>
            <TextInput
              autoCapitalize="none"
              autoCorrect={ false }
              placeholder="First & Last Name"
              placeholderTextColor={ commonColors.placeholderText }
              textAlign="center"
              style={ styles.input }
              underlineColorAndroid="transparent"
              returnKeyType={ 'next' }
              onChangeText={ (text) => this.setState({ name: text }) }
            />
            <View style={ styles.inputRowContainer }>
              <DatePicker
                style={ styles.birthdayWrapper }
                date={ this.state.birthday }
                mode="date"
                androidMode="spinner"
                placeholder="Birthday"
                format="MMM DD, YYYY"
                minDate="Jan 01, 1900"
                maxDate="Dec 31, 2200"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                showIcon={ false }
                customStyles={{
                  dateInput: {
                    borderColor: '#fff',
                  },
                  placeholderText: {
                    color: commonColors.placeholderText,
                  },
                  dateText: {
                    color: '#000',
                  },
                }}
                onOpenModal={() => {Keyboard.dismiss()}}
                onDateChange={ (date) => this.onChangeBirthday(date) }
              />
              <TouchableOpacity 
                style={ styles.genderDropDownWrapper }
                activeOpacity={0.5}
                onPress={() => {
                  this.setState({
                    modal: true,
                    tempGender:this.state.gender
                  });
                  Keyboard.dismiss();
                }}
              >
                <Text
                  ref="genderText"
                  style={[styles.textGenderDropdown, {color: this.state.gender === 'Gender' ? commonColors.placeholderText : '#000'}]}
                >
                  {this.state.gender}
                </Text>
              </TouchableOpacity>
            </View>
            {showRace && <TouchableOpacity style={ styles.dropDownStyle } onPress={ this.openRaceView.bind(this) }>
              {this.state.race == '' && <Text style={ styles.dropDownTextStyle }>Race/Ethnicity</Text>}
              {this.state.race != '' && <Text numberOfLines={1} style={ styles.dropDownTextSelectedStyle }>{this.state.race}</Text>}
            </TouchableOpacity>}
            {showZip && <TextInput
              autoCapitalize="none"
              autoCorrect={ false }
              placeholder="ZIP Code"
              placeholderTextColor={ commonColors.placeholderText }
              textAlign="center"
              style={ styles.input }
              underlineColorAndroid="transparent"
              keyboardType="numeric"
              returnKeyType={ 'next' }
              onChangeText={ (text) => this.setState({ zipCode: text }) }
            />}
            <View style={ styles.buttonCompleteProfileWrapper }>
              {!this.state.isUploadingFile&&<TouchableOpacity activeOpacity={ .5 } onPress={ () => this.onCompleteProfile() }>
                <View style={ styles.buttonCompleteProfile }>
                  <Text style={ styles.textButton }>Complete Profile</Text>
                </View>
              </TouchableOpacity>}
              {this.state.isUploadingFile&&
              <View style={ styles.buttonCompleteProfile }>
                <ActivityIndicator
                  hidesWhenStopped={ true }
                  animating={ true }
                />
              </View>
              }
            </View>
          </View>
          <View style={ styles.bottomContainer }>
          </View>
        </Image>
        {this.state.modal && <Modal
          isOpen={this.state.modal}
          style={{ width: screenWidth, height: 250, position: 'absolute'}}
          position={'bottom'}
          swipeToClose={false}
          onClosed={() => {this.setState({ modal: false });this.refs.genderText.blur();}}
          backdropOpacity={0.5}
        >
          <View style={{flex:1}}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={ ()=>{
                this.setState({modal:false});
                this.refs.genderText.blur();
              }
              } style={{position:'absolute',left:20,top:10}}>
                <Text style={styles.closeButtonText}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={ ()=>{
                this.refs.genderText.blur();
                this.setState({
                  modal:false,
                  gender:(this.state.tempGender=='Gender'?'Male':this.state.tempGender)
                })}
              } style={{position:'absolute',right:20,top:10}}>
                <Text style={styles.confirmButtonText}>Confirm</Text>
              </TouchableOpacity>
            </View>
            <Picker
              itemStyle={{ marginLeft: 0, paddingLeft: 15 }}
              selectedValue={this.state.tempGender}
              onValueChange={(data) => this.setState({tempGender:data})}>
              {
                genders.map((gender, index) => {
                  return (<Picker.Item key={index} label={gender} value={gender} />)
                })
              }
            </Picker>
          </View>
        </Modal>}
      </View>
    );
  }
}

export default connect(state => ({
    status: state.profile.status,
    param: state.profile.param
  }),
  (dispatch) => ({
    actions: bindActionCreators(profileActions, dispatch)
  })
)(SetupProfile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    width: screenWidth,
    height: screenHeight,
  },
  descriptionContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 20,
  },
  photoContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  photoWrapper: {
    width: screenWidth * 0.22,
    height:  screenWidth * 0.22,
    borderRadius: 5,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageCamera: {
    width: 22,
    height: 20,
  },
  imagePhoto: {
    width: screenWidth * 0.22,
    height:  screenWidth * 0.22,
    borderRadius: 5,
    resizeMode:'cover'
  },
  inputContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 40,
  },
  inputRowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 5,
    alignSelf: 'stretch',
  },
  bottomContainer: {
    flex: 1,
  },
  textTitle: {
    color: commonColors.title,
    fontFamily: 'Blanch',
    fontSize: 48,
    backgroundColor: 'transparent',
  },
  textDescription: {
    color: commonColors.title,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 12,
    paddingVertical: 10,
    backgroundColor: 'transparent',
  },
  buttonCompleteProfileWrapper: {
    alignSelf: 'stretch',
  },
  buttonCompleteProfile: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: commonColors.theme,
    borderRadius: 4,
    borderWidth: 4,
    borderColor: commonColors.theme,
    borderStyle: 'solid',
    marginTop: 10,
    height: 40,
  },
  textButton: {
    color: '#fff',
    fontFamily: 'Open Sans',
    fontWeight: 'bold',
    fontSize: 14,
    backgroundColor: 'transparent',
  },
  input: {
    alignSelf: 'stretch',
    fontSize: 14,
    // color: commonColors.title,
    height: 45,
    borderColor: '#fff',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderRadius: 4,
  },
  birthdayWrapper: {
    height: 45,
    width: (screenWidth - 80) / 2 - 3,
    borderColor: '#fff',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderRadius: 4,
  },
  dropdown: {
    width: (screenWidth - 80) / 2 - 3,
    height: 45,
    borderColor: '#fff',
    backgroundColor: 'red',
    borderWidth: 1,
    borderRadius: 4,
    marginLeft: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  genderDropDownWrapper: {
    height: 45,
    width: (screenWidth - 80) / 2 - 3,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: '#fff',
  },
  textGenderDropdown: {
    color: "#000",
    fontFamily: 'Open Sans',
    fontSize: 14,
    textAlign: 'center',
    backgroundColor:'transparent',
  },
  dropDownStyle:{
    width:(screenWidth - 80),
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'white',
    marginBottom:5
  },
  dropDownSelectStyle:{
    width:(screenWidth - 80),
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'white'
  },
  dropDownTextStyle:{
    color: commonColors.placeholderText,
    fontFamily: 'OpenSans',
    fontSize: 14,
  },
  dropDownTextSelectedStyle:{
    color: '#000',
    fontFamily: 'OpenSans',
    fontSize: 14,
    flex:1,
    paddingHorizontal:10,
    marginTop:12
  },
  imageTriangleDown: {
    width: 8,
    height: 6,
    position: 'absolute',
    right: 10,
  },
  buttonContainer: {
    borderTopColor: '#e2e2e2',
    borderTopWidth: 1,
    borderBottomColor: '#e2e2e2',
    borderBottomWidth:1,
    height:40
  },
  closeButton: {
    paddingRight:10,
    paddingTop:10,
    paddingBottom:10
  },
  buttonText: {
    textAlign: 'center'
  },
  closeButtonText: {
    fontSize:16,
    color: '#666'
  },
  confirmButtonText: {
    fontSize:16,
    color: '#46cf98'
  },
  modalContainer: {
    flex:1
  },
  cellContainer2: {
    flexDirection: 'row',
    height: 56,
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
  },
  textCellTitle2: {
    color: commonColors.title,
    fontFamily: 'Open Sans',
    fontSize: 14,
  },
});