'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  ListView,
  RefreshControl,
  Alert,
  Image,
  ActivityIndicator,
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import NavTitleBar from '../../components/navTitleBar';
import * as commonColors from '../../styles/commonColors';
import * as commonStyles from '../../styles/commonStyles';
import InfiniteScrollView from 'react-native-infinite-scroll';

import bendService from '../../bend/bendService'
import * as _ from 'underscore'
import UtilService from '../../components/util'
import Cache from '../../components/Cache'
import { VictoryBar, VictoryChart, VictoryTheme, VictoryAxis } from "victory-native";

var groups = ['Diet', 'Transit', 'Shopping', 'Waste', 'Home', 'Community']

var group_icons= [
  {
    icon:require('../../../assets/imgs/chart/icons/diet-white.png'),
    name:'Diet',
  },
  {
    icon:require('../../../assets/imgs/chart/icons/transit-white.png'),
    name:'Transit',
  },
  {
    icon:require('../../../assets/imgs/chart/icons/shopping-white.png'),
    name:'Shopping',
  },
  {
    icon:require('../../../assets/imgs/chart/icons/waste-white.png'),
    name:'Waste',
  },
  {
    icon:require('../../../assets/imgs/chart/icons/home-white.png'),
    name:'Home',
  },
  {
    icon:require('../../../assets/imgs/chart/icons/community-white.png'),
    name:'Community',
  },
];

class YourPoints extends Component {
  constructor(props) {
    super(props);

    this.dataSourceLeaderboard = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });

    this.more = true;
    this.offset = 0;
    this.loading = false;
    this.limit = 25;

    let dataByGroup = {}
    this.param = {
      from : (Date.now() - 8 * 7 * 24 * 3600 * 1000) * 1000000, //from 8 weeks ago
      to:Date.now()* 1000000
    }
    this.dates = UtilService.getChartWeeks(this.param.from, this.param.to);
    this.groups = this.props.groups
    console.log('this.groups', this.groups)
    if(this.groups) {
      var groupIds = []
      _.map(this.groups, (o)=>{
        groupIds.push(o._id)
      })
      this.param.groupIds = groupIds
      _.map(this.groups, (group)=>{
        dataByGroup[group._id] = []
        _.each(this.dates, (date)=>{
          let value = {
            x:UtilService.getFirstDayOfWeek(date),
            y:0,
          }
          dataByGroup[group._id].push(value)
        })
      })
    } else {
      _.map(groups, (group)=>{
        dataByGroup[group] = []
        _.each(this.dates, (date)=>{
          let value = {
            x:UtilService.getFirstDayOfWeek(date),
            y:0,
          }
          dataByGroup[group].push(value)
        })
      })
    }

    this.state = {
      userList: [],
      query:{
        more: true,
        isActivityIndicator: false,
      },
      selectedGroup:this.props.group,
      isRefreshing: false,
      chartGroupData:dataByGroup,
      maxValue:1
    };
  }

  
  componentDidMount() {
    this.hasMounted = true
    this.loadAllData();
  }


  componentWillUnmount() {
    this.hasMounted = false
  }


  componentWillReceiveProps(newProps) {    
  }


  loadAllData() {
    bendService.getActivitiesByCategoryGroup(this.param, (err, ret)=>{
      if(err) {
        console.log(err);return
      }

      //console.log('dates', dates, ret)
      var dataByGroup = {}
      var maxValue = 0

      _.map(Object.keys(ret), (group)=>{
        var groupValue = ret[group]
        dataByGroup[group] = []
        _.each(this.dates, (date)=>{
          let value = {
            x:UtilService.getFirstDayOfWeek(date),
            y:groupValue[date]||0
          }

          maxValue = Math.max(maxValue, (groupValue[date]||0) + 1)
          dataByGroup[group].push(value)
        })
      })

      this.hasMounted && this.setState({
        chartGroupData:dataByGroup,
        maxValue:maxValue
      })
    })
  }


  loadUserPage() {

  }

  onBack() {
    Actions.pop();
  }

  renderFooter() {
    if ((!this.state.isRefreshing) && (this.state.query.isActivityIndicator)) {
      return (
        <ActivityIndicator
          hidesWhenStopped={ true }
          style={{ marginVertical: 15 }}
        />
      );
    } 

    return null;
  }


  onRefresh() {
    this.hasMounted && this.setState({ isRefreshing: true });
    this.loadAllData();    
  }


  render() {
    let {chartGroupData, selectedGroup, maxValue} = this.state
    return (
      <View style={ styles.container }>
        <NavTitleBar
          buttons={ commonStyles.NavBackButton }
          onBack={ this.onBack }
          title ='Your Points'
        />
        <InfiniteScrollView
          distanceFromEnd={10}
          refreshControl={
            <RefreshControl
              refreshing={ this.state.isRefreshing }
              onRefresh={ () => this.onRefresh() }
              tintColor={ commonColors.theme }
            />
          }

          canLoadMore={ this.state.query.more }
          onLoadMoreAsync={ ()=> this.loadUserPage() }
        >
          <View style={ styles.topContainer }>
            {
              !this.groups && group_icons.map((item, index)=>{
                let selected = (item.name == selectedGroup)
                return (
                  <TouchableOpacity style={{width:60,alignItems:'center'}} key={index} onPress={()=>{
                    this.setState({selectedGroup:item.name})
                  }}>
                    <View style={{width:32,height:32, borderRadius:16, backgroundColor:(selected?'#F59174':'#A4A4A3'), alignItems:'center', justifyContent:'center'}}>
                      <Image source={item.icon} style={{height:16, width:16, resizeMode:'contain'}}/>
                    </View>
                    <Text numberOfLines={1} style={{fontSize:10, paddingTop:3, fontWeight:(selected?'bold':'normal')}}>{item.name}</Text>
                  </TouchableOpacity>
                )
              })
            }
            {
              this.groups && this.groups.map((item, index)=>{
                let selected = (item._id == selectedGroup)
                return (
                  <TouchableOpacity style={{width:60,alignItems:'center'}} key={index} onPress={()=>{
                    this.setState({selectedGroup:item._id})
                  }}>
                    <View style={{width:32,height:32, borderRadius:16, backgroundColor:(selected?'#F59174':'#A4A4A3'), alignItems:'center', justifyContent:'center'}}>
                      <Image source={item.inIcon} style={{height:16, width:16, resizeMode:'contain'}}/>
                    </View>
                    <Text numberOfLines={1} style={{fontSize:10, paddingTop:3, fontWeight:(selected?'bold':'normal')}}>{item.title}</Text>
                  </TouchableOpacity>
                )
              })
            }
          </View>

          <View style={ styles.chartContainer }>
            <VictoryChart
              animate={{
                duration: 100,
                onLoad: { duration: 50 }
              }}
              domainPadding={{ x: 15 }}
              domain={{y: [0, maxValue]}}
              theme={VictoryTheme.material} padding={{top:20,left:40,bottom:50,right:20}} height={200}>
              {/*<VictoryAxis
                tickValues={this.dates}
                tickFormat={(val, index)=>{
                  if(index % 2 == 0) return val;

                  return ''
                }}
              />*/}
              <VictoryBar
                size={5}
                alignment="start"
                style={{ data: { fill: "tomato", opacity: 0.5 } }}
                data={chartGroupData[selectedGroup]}
              />
            </VictoryChart>
          </View>
          <View style={ styles.activityContainer }>
            <Text style={ styles.textSectionTitle }>Recommended Activities</Text>
            <View style={ styles.recentActivityListViewWrapper }>

            </View>
          </View>
        </InfiniteScrollView>
      </View>
    );
  }
}

export default YourPoints;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  topContainer: {
    paddingTop: 24,
    paddingBottom: 16,
    paddingHorizontal: 8,
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  chartContainer:{
    flex:1
  },
  activityContainer:{
    flex:1,
  },
  chart:{
    height:180,
    width:200
  },
  textName: {
    color: commonColors.title,
    fontFamily: 'Open Sans',
    fontSize: 24,
    backgroundColor: 'transparent',
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  imageComcast: {
    width: 300,
    height: 100,
    marginBottom: 8
  },
  pointContainer: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  pointSubContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  textValue: {
    color: commonColors.bottomButton,
    fontFamily: 'Open Sans',
    fontSize: 16,
    fontWeight: 'bold',
  },
  textSmall: {
    color: commonColors.grayMoreText,
    fontFamily: 'Open Sans',
    fontSize: 12,
    backgroundColor: 'transparent',
  },
  textSectionTitle: {
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    padding: 10,
  },
});
