'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ListView,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,  
  Alert,
} from 'react-native';

import { bindActionCreators } from 'redux';
import * as leaderboardActions from '../actions';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import NavTitleBar from '../../components/navTitleBar';
import * as commonColors from '../../styles/commonColors';
import * as commonStyles from '../../styles/commonStyles';

import LeaderboardListCell from '../components/leaderboardListCell';
import { LeaderboardEntries } from '../../components/dummyEntries';
import InfiniteScrollView from 'react-native-infinite-scroll';

import bendService from '../../bend/bendService'
import * as _ from 'underscore'
import UtilService from '../../components/util'
import Cache from '../../components/Cache'

class Leaderboard extends Component {

  constructor(props) {
    super(props);

    this.dataSourceLeaderboard = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });

    this.more = true;
    this.offset = 0;
    this.loading = false;
    this.limit = 25;

    this.state = {
      currentUserIndex: 3,
      userList: [],
      query:{
        more: true,
        isActivityIndicator: false,
      },
      community: {}
    };

    this.loadUserPage.bind(this);
  }

  componentDidMount() {
    this.hasMounted = true
    bendService.getCommunity((error, result)=>{
      if (!error)
        this.hasMounted && this.setState({
          community: result,
        })
    })

    this.loadUserPage();
    UtilService.mixpanelEvent("Viewed Leaderboard")
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  loadUserPage() {

    if ((this.more === false) || (this.loading === true)){
      return;
    }

    this.loading = true;

    this.hasMounted && this.setState( (state) => {
        state.query.isActivityIndicator = true;
      return state;
    });

    bendService.getLeaderBoardPage(this.offset, this.limit + 1, (error, result) => {
      //console.log("getRecentActivities", error, result)
      this.hasMounted && this.setState( (state) => {
        state.query.isActivityIndicator = false;
        return state;
      });

      if (error) {
        console.log(error);
        return;
      }

      if (result.length < this.limit + 1) {
        this.more = false;
        this.hasMounted && this.setState( (state) => {
          state.query.more = false;
          return state;
        });
      } else {
        result.pop()
      }

      if (result.length > 0) {
        this.state.userList = this.state.userList.concat(result)
        this.offset += result.length
        this.hasMounted && this.setState({
          userList: this.state.userList
        })
      }

      this.loading = false; 
    })
  }

  renderLeaderboardRow(rowData, sectionID, rowID) {
    var previousRank = rowData.previousSprintRank, currentRank = rowData.sprintRank

    return (
      <LeaderboardListCell
        status={ previousRank == -1?0 : (previousRank < currentRank ? 2 : (previousRank > currentRank ? 1 : 0)) }
        index={ rowData.sprintRank }
        name={ rowData.name||rowData.username }
        points={ rowData.sprintPoints }
        avatar={ rowData.avatar ? UtilService.getSmallImage(rowData.avatar) : '' }
        avatarBackColor={ UtilService.getBackColor(rowData.avatar) }
        defaultAvatar={ UtilService.getDefaultAvatar(rowData.defaultAvatar) }
        currentUser={ bendService.getActiveUser().sprintRank==rowData.sprintRank }
      />
    );
  }

  onBack () {
    Actions.pop()
  }

  onCheckin() {
    alert("Tapped 'I Did This Today' button!");
  }

  renderFooter() {
    if (this.state.query.isActivityIndicator) {
      return (
        <ActivityIndicator
          hidesWhenStopped={ true }
          style={{ marginVertical: 15 }}
        />
      );
    } 

    return null;
  }

  render() {
    const {
      total,
    } = this.props;

    const currentUser = bendService.getActiveUser();
    const community = this.state.community;
    return (
      <View style={ styles.container }>
        <NavTitleBar
          buttons={ commonStyles.NavBackButton }
          onBack={ this.onBack }
          title ='LEADERBOARD'
        />
        <View style={ styles.orderContainer }>
          { community.logo && <Image source={{ uri : community.logo._downloadURL }} style={ styles.imageComcast } resizeMode="contain"/> }
          <Text style={ styles.textOrder }>{ UtilService.getPositionString(currentUser.sprintRank) } out of { total } people</Text>
          <Text style={ styles.textUpdate }>Updated every 15 mins</Text>
        </View>

        <ListView
          renderScrollComponent={ props => <InfiniteScrollView distanceFromEnd={10} {...props}/> }
          enableEmptySections={ true }
          dataSource={ this.dataSourceLeaderboard.cloneWithRows(this.state.userList) }
          renderRow={ this.renderLeaderboardRow.bind(this) }
          contentContainerStyle={ styles.leaderboardListView }
          renderFooter={ this.renderFooter.bind(this) }

          canLoadMore={ this.state.query.more }
          onLoadMoreAsync={ ()=> this.loadUserPage() }
        />
      </View>
    );
  }
}

export default connect(state => ({
  status: state.search.status
  }),
  (dispatch) => ({
    actions: bindActionCreators(leaderboardActions, dispatch)
  })
)(Leaderboard);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  orderContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 24,
    paddingBottom: 35,
  },
  imageComcast: {
    width: 300,
    height: 100,
    marginBottom: 8
  },
  textOrder: {
    color: commonColors.grayText,
    fontFamily: 'Open Sans',
    fontSize: 12,
  },
  textUpdate: {
    marginTop: 10,
    color: commonColors.grayText,
    fontFamily: 'Open Sans',
    // fontWeight: 'bold',
    fontSize: 10,
  },
  leaderboardListViewWrapper: {
    flex: 1,
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
    borderBottomWidth: 1,
    borderBottomColor: commonColors.line,
  },
  leaderboardListView: {
    // flex: 1,
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
  },
});
