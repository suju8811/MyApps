'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TextInput,
  Platform,
  TouchableOpacity,
  TouchableHighlight,
  Alert,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Picker,
  Keyboard,
  ListView,
  ScrollView
} from 'react-native';
import Modal from 'react-native-modalbox';
import { bindActionCreators } from 'redux';
import * as profileActions from '../actions';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import NavTitleBar from '../../components/navTitleBar';
import * as commonColors from '../../styles/commonColors';
import * as commonStyles from '../../styles/commonStyles';

import bendService from '../../bend/bendService'
import * as _ from 'underscore'
import UtilService from '../../components/util'
import Cache from '../../components/Cache'

const checkmark = require('../../../assets/imgs/icon-checkmark.png');

class MyTeams extends Component {
  constructor(props) {
    super(props);

    var user = bendService.getActiveUser();
    this.dataSource = new ListView.DataSource(
      { rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      teams:[],
      myTeams:user.teams||[],
      mode:!user.name?'afterLogin':'setting',
    };

    //console.log('team-mode', !user.name?'afterLogin':'setting')

    bendService.getCommunityTeams(user.community._id, (err, ret)=>{
      if(!err) {
        this.setState({
          teams:ret
        })
      }
    })
  }

  componentDidMount() {
    this.hasMounted = true
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  toggleSelectTeam(id) {
    let {myTeams} = this.state
    if(Cache.community && Cache.community.enableMultipleTeams) {
      var index = myTeams.indexOf(id)
      if(index == -1) {
        myTeams.push(id);
      } else {
        myTeams.splice(index, 1)
      }
    } else {
      myTeams = [id]
    }

    this.setState({
      myTeams:myTeams
    })

    bendService.saveUserTeam(myTeams, (err, ret)=>{
      if(!err) {
        bendService.getUserWithoutCache(()=>{
          this.props.actions.updateTeams();
        });
      }
    })
  }

  renderTeamRow(rowData, sectionID, rowID) {
    let {myTeams} = this.state
    let isSelected = myTeams.indexOf(rowData._id)==-1?false:true
    return (
      <TouchableWithoutFeedback onPress={ this.toggleSelectTeam.bind(this, rowData._id) }>
        <View style={{paddingHorizontal:8, paddingVertical:8}}>
          <View style={ styles.cellContainer }>
            <View style={ styles.iconContainer }>
              <View style={{ width: 30, height:30, backgroundColor:rowData.color, borderRadius:6, justifyContent:'center', alignItems:'center', marginTop:-5}}>
                <Text style={{textAlign:'center'}}>{ rowData.initials }</Text>
              </View>
            </View>
            <View style={ styles.contentContainer }>
              <Text style={ styles.textSubTitle }>{ rowData.name }</Text>
              {isSelected && <Image source={checkmark} style={styles.checkmark}/>}
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  onNext() {
    let {teams, mode} = this.state
    if(mode=='afterLogin') {
      Actions.SetupProfile()
    } else {
      Actions.pop();
    }
  }

  onBack() {
    Actions.pop();
  }

  render() {
    let {teams, mode} = this.state
    let multipleMode = (Cache.community && Cache.community.enableMultipleTeams)?true:false
    return (
      <View style={ styles.container }>
        <NavTitleBar
          buttons={ mode=='afterLogin'?commonStyles.NavNextButton:commonStyles.NavBackButton }
          onNext={ this.onNext.bind(this) }
          onBack={ this.onBack.bind(this) }
          nextButtonText = {mode=='afterLogin'?'Next':'Done'}
          title ={multipleMode?'MY TEAMS':'My TEAM'}
        />
        <ScrollView style={ styles.scrollView }>
          <View style={{backgroundColor:'white', flex:1, paddingTop:20}}>
            <View style={{flexDirection:'row'}}>
              <Text style={ [styles.textTitle, {width:160}] }>{multipleMode?'Choose your teams':'Choose your team'}</Text>
              <View style={{height:2,backgroundColor:'#E595D5', flex:2, marginTop:20, width:commonStyles.screenWidth-160}}></View>
            </View>
            <ListView
              enableEmptySections={ true }
              dataSource={ this.dataSource.cloneWithRows(teams) }
              renderRow={ this.renderTeamRow.bind(this) }
              contentContainerStyle={ styles.listTeamRow }
            />
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default connect(state => ({
    status: state.profile.status
  }),
  (dispatch) => ({
    actions: bindActionCreators(profileActions, dispatch)
  })
)(MyTeams);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
    backgroundColor: '#f4fafd',
  },
  textTitle: {
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    padding: 10,
  },
  listTeamRow: {
    borderTopWidth: 1,
    borderTopColor: commonColors.line
  },
  cellContainer: {
    flexDirection: 'row',
    paddingVertical: 8,
    paddingHorizontal:7,
    height:38,
  },
  iconContainer: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  contentContainer: {
    height:38,
    flex: 7,
    alignItems: 'flex-start',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: commonColors.line,
    borderStyle: 'solid',
  },
  textSubTitle: {
    flex: 1,
    color: commonColors.title,
    fontFamily: 'Open Sans',
    fontSize: 14,
  },
  checkmark: {
    position:'absolute',
    right:20,
    top:0,
    width:24,
    height:24
  },
});