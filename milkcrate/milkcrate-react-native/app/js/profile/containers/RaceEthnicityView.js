'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  Alert,
} from 'react-native';

import { bindActionCreators } from 'redux';
import * as profileActions from '../actions';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import TextField from 'react-native-md-textinput';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import NavTitleBar from '../../components/navTitleBar';
import * as commonColors from '../../styles/commonColors';
import * as commonStyles from '../../styles/commonStyles';

import bendService from '../../bend/bendService'
import * as _ from 'underscore'
import UtilService from '../../components/util'
import Cache from '../../components/Cache'

const races = ['American Indian or Alaska Native', 'Asian', 'Black or African American', 'Native Hawaiian or Other Pacific Islander', 'White']
const ethnicities = ['Hispanic or Latino', 'Not Hispanic or Latino']
const checkmark = require('../../../assets/imgs/icon-checkmark.png');
class RaceEthnicityView extends Component {

  constructor(props) {
    super(props);
    var user = Cache.tempProfile
    this.state = ({
      race:user.race?user.race.split(','):[],
      ethnicity:user.ethnicity||'Not Hispanic or Latino'
    })
  }

  onBack() {
    Actions.pop();
  }

  toggleRaceItem(item) {
    let {race, ethnicity} = this.state
    var idx = race.indexOf(item)
    if(idx == -1){
      race.push(item)
    } else {
      race.splice(idx, 1)
    }

    Cache.tempProfile.race = race.join(',')

    this.setState(race)
    this.props.actions.updateRace({
      race:race,
      ethnicity:ethnicity
    })
  }

  toggleEthnicityItem(item) {
    let {race, ethnicity} = this.state
    this.setState({
      ethnicity:item
    })
    Cache.tempProfile.ethnicity = item
    this.props.actions.updateRace({
      race:race,
      ethnicity:item
    })
  }

  render() {
    let {race, ethnicity} = this.state
    return (
      <View style={ styles.container }>
        <NavTitleBar
          buttons={ commonStyles.NavBackButton }
          onBack={ this.onBack }
          title ='Race/Ethnicity'
        />
        <Text style={ styles.textSettingsSection }>RACE</Text>
        {
          races.map((item, index)=>{
            return (<TouchableHighlight key={index} onPress={ this.toggleRaceItem.bind(this, item) }>
              <View style={ styles.cellContainer }>
                <Text style={ styles.textCellTitle }>{item}</Text>
                {race.indexOf(item) != -1 && <Image source={checkmark} style={styles.checkmark}/>}
              </View>
            </TouchableHighlight>)
          })
        }
        <View style={ styles.blueLine }></View>
        <Text style={ styles.textSettingsSection }>ETHNICITY</Text>
        {
          ethnicities.map((item, index)=>{
            return (<TouchableHighlight key={index} onPress={ this.toggleEthnicityItem.bind(this, item) }>
              <View style={ styles.cellContainer }>
                <Text style={ styles.textCellTitle }>{item}</Text>
                {ethnicity == item && <Image source={checkmark} style={styles.checkmark}/>}
              </View>
            </TouchableHighlight>)
          })
        }
        <View style={ styles.blueLine }></View>
      </View>
    );
  }
}

export default connect(state => ({
    status: state.profile.status
  }),
  (dispatch) => ({
    actions: bindActionCreators(profileActions, dispatch)
  })
)(RaceEthnicityView);

const inputStyle = {
  color: commonColors.grayText,
  fontFamily: 'OpenSans-Semibold',
  fontSize: 14,
  paddingHorizontal: 16,
};

const labelStyle={
  color: commonColors.grayMoreText,
  fontFamily: 'Open Sans',
  fontSize: 12,
  paddingHorizontal: 16,
};

const wrapperStyle={
  height: 72,
  backgroundColor: '#fff',
  borderTopWidth: 1,
  borderTopColor: commonColors.line,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
  },
  scrollView: {
    backgroundColor: 'transparent',
  },
  textSettingsSection: {
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    marginTop: 30,
    marginLeft: 16,
    marginBottom: 8,
  },
  textOtherSection: {
    color: commonColors.grayMoreText,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    marginTop: 16,
    marginLeft: 8,
    marginBottom: 8,
  },
  line: {
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
  },
  checkmark: {
    position:'absolute',
    right:20,
    top:15,
    width:24,
    height:24
  },
  cellContainer: {
    flexDirection: 'row',
    height: 56,
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
  },
  blueLine: {
    width:'100%',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
  },
  textCellTitle: {
    color: commonColors.title,
    fontFamily: 'Open Sans',
    fontSize: 14,
  },
  updatePasswordButtonWrapper: {
    height: 56,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopWidth: 1,
    borderTopColor: commonColors.line,
    borderBottomWidth: 1,
    borderBottomColor: commonColors.line,
    marginTop: 24,
  },
  textUpdatePassword: {
    color: '#82ccbe',
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
  },
});
