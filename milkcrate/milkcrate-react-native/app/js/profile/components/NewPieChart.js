import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Surface,
  Group,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Platform,
  Button,
  Animated,
  Easing,
  Dimensions
} from 'react-native';
import * as _ from 'underscore'

var profile = require('../../../assets/imgs/chart/profile.png');
var line = require('../../../assets/imgs/chart/line.png');

import Svg,{
  Circle,
  Ellipse,
  G,
  LinearGradient,
  RadialGradient,
  Line,
  Path,
  Polygon,
  Polyline,
  Rect,
  Symbol,
  Use,
  Defs,
  Stop
} from 'react-native-svg';
const { width, height } = Dimensions.get('window');
var chartRadius = 150
var chartInnerRadius = 35
var rate = 0.50;

var colors = ["#F69274", "#F0B074", "#EAD575", "#B7D099", "#82CCBE"];
var colors2 = ["#5E8AA3", "#B7D099", "#EAD575", "#82CCBE", "#F69274", "#95B5D8"];

export default class NewPieChart extends Component {

  constructor(props) {
    super(props);

    this.state={
      viewMode:0,
      animIndex:[],
      labelOpacity:[new Animated.Value(0),new Animated.Value(0),new Animated.Value(0),new Animated.Value(0),new Animated.Value(0),new Animated.Value(0)],
      currentWeek:'week1',
      chartValue:[0,0,0,0,0,0],
      showChartValue:[false,false,false,false,false,false],
      chartMode:'Mode1',
      logoMode:'avatar',
      newChartData: props.chartData,
    }
    this.oldScaleData = [0,0,0,0,0,0]
    this.newScaleData = [0,0,0,0,0,0]

    this.groups = ['Home', 'Community', "Diet", "Transit", "Shopping", "Waste"]
    props.chartData.map((item)=>this.state.animIndex.push(0))
  }

  componentDidMount(){
    this.hasMounted = true
    this.updateChartData(this.props.chartData)
  }

  componentWillReceiveProps(newProps) {

    var chartData = newProps.chartData;
    this.setState({newChartData:newProps.chartData})
    this.updateChartData(this.props.chartData)
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  updateChartData=(data)=>{
    this.newUpdateChart(data)
  }

  updateChartMode=(mode)=>{
    this.hasMounted && this.setState({
      chartMode:mode
    })
  }

  updateLogoMode=(mode)=>{
    this.hasMounted && this.setState({
      logoMode:mode
    })
  }

  newUpdateChart=(newData)=>{
    //get new Scale Data
    var total = 0;
    var maxValue = 0;
    newData.map((item, index)=>{
      total = total + item.value
      if ( maxValue < item.value ) maxValue = item.value
    })

    if(maxValue == 0) return;
    var unitScale = maxValue/5;//total < 20 ? 1 : total / 20;
    var newScaleData = []
    newData.map((item, index)=>{
      newScaleData.push(this.getScaleValue(item.value/unitScale))
    })
    this.newScaleData = newScaleData

    newScaleData.map((newData, idx)=>{
      this.updateChartSlice(this.oldScaleData[idx], newScaleData[idx], idx)
    })
  }

  updateChartSliceValue=(idx)=>{
    var itemValue = this.props.chartData[idx].value
    this.state.chartValue[idx] = itemValue
    this.hasMounted && this.setState({
      chartValue:this.state.chartValue
    })
  }

  updateChartSlice=(oldScale, newScale, idx) => {
    //console.log('updateChartSlice', oldScale, newScale, idx)
    if(oldScale == newScale) {
      //only replace label Text
      this.updateChartSliceValue(idx);
    } else {
      var chartValue = this.state.showChartValue
      chartValue[idx] = true;
      this.state.animIndex[idx] = newScale
      //console.log("showChartValue", this.state.showChartValue)
      this.hasMounted && this.setState({
        showChartValue:chartValue,
        animIndex:this.state.animIndex
      })
      this.updateChartSliceValue(idx);
      this.oldScaleData[idx] = newScale;
    }
  }

  getScaleValue(val) {
    if(val < 1) return 1;
    if(val > 5) return 5;

    return Math.round(val);
  }

  showLabelAnimation() {

    Animated.timing(this.state.labelOpacity, {
      duration: 1000,
      toValue: 1,
      easing: Easing.out(Easing.cubic),
    }).start(()=>{

    })
  }

  getColors(index) {
    if (this.state.logoMode == 'avatar'){
      return colors2[index];
    }
    if(this.oldScaleData[index] == this.newScaleData[index]) {
      var currentScale = this.state.animIndex[index];
      return colors[currentScale-1]
    } else {
      var currentScale = this.state.animIndex[index];
      var color1 = this.hexToRgb(colors[Math.max(this.oldScaleData[index]-1, 0)]);
      var color2 = this.hexToRgb(colors[Math.max(this.newScaleData[index]-1, 0)]);
      var weight = (currentScale - this.oldScaleData[index]) / (this.newScaleData[index] - this.oldScaleData[index])
      var rgbColor = this.pickHex(color1, color2, 1-weight)
      return this.rgbToHex(rgbColor[0], rgbColor[1], rgbColor[2])
    }
  }

  getNewColors(index) {
    if (this.state.logoMode == 'avatar'){
      return this.state.newChartData[index].backgroundColor;
    }
    if(this.oldScaleData[index] == this.newScaleData[index]) {
      var currentScale = this.state.animIndex[index];
      return colors[currentScale-1]
    } else {
      var currentScale = this.state.animIndex[index];
      var color1 = this.hexToRgb(colors[Math.max(this.oldScaleData[index]-1, 0)]);
      var color2 = this.hexToRgb(colors[Math.max(this.newScaleData[index]-1, 0)]);
      var weight = (currentScale - this.oldScaleData[index]) / (this.newScaleData[index] - this.oldScaleData[index])
      var rgbColor = this.pickHex(color1, color2, 1-weight)
      return this.rgbToHex(rgbColor[0], rgbColor[1], rgbColor[2])
    }
  }

  pickHex(color1, color2, weight) {
    var p = weight;
    var w = p * 2 - 1;
    var w1 = (w/1+1) / 2;
    var w2 = 1 - w1;
    var rgb = [Math.round(color1.r * w1 + color2.r * w2),
      Math.round(color1.g * w1 + color2.g * w2),
      Math.round(color1.b * w1 + color2.b * w2)];
    return rgb;
  }

  hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }

  rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
  }

  changeViewMode(mode) {
    this.hasMounted && this.setState({
      viewMode:mode
    })
  }

  renderPieChart() {
    if(this.state.viewMode != 0) return null;

    const {
      avatar,
      defaultAvatar,
      avatarBackColor,
    } = this.props;

    return (
      <View style={styles.chartContainer}>
        <View style={{width:chartRadius * 2,height:chartRadius * 2, justifyContent: 'center',alignItems: 'center'}}>
          <Svg height={chartRadius * 2} width={chartRadius * 2}>
            <G>
              {
                this.state.chartMode == 'Mode1'&&
                this.state.animIndex.map( (item, index) => (
                    <Path key={index}
                          d={this._drawPieBack(item, index)}
                          fill={this.getNewColors(index)}
                          opacity="0.2"
                    />
                  )
                )
              }

              {
                this.state.animIndex.map( (item, index) => (
                    <Path key={index}
                          d={this._createPieChart(item, index)}
                          fill={this.getNewColors(index)} />
                  )
                )
              }
            </G>
          </Svg>
          <View style = {styles.percentView}>
            { (avatar != '') && <Image style={styles.avartar} source={{ uri:avatar }}/> }
            { (avatar == '') && <Image style={ styles.avartar } source={ defaultAvatar }/> }
          </View>
          {
            this.state.animIndex.map(
              (val, index) => this.renderNewChartValue(val, index)
            )
          }
          {
            this.state.chartMode == 'Mode2'&&
            this.state.animIndex.map(
              (val, index) => this.renderChartPercent(val, index)
            )
          }
          <Svg height={chartRadius * 2} width={chartRadius * 2} style={{position:'absolute'}}>
            <G>
              {
                this.state.chartMode == 'Mode1'&&
                this.state.animIndex.map( (item, index) => (
                    <TouchableWithoutFeedback key={index} onPress={()=>this.props.onClick(index)}>
                      <Path key={index}
                            d={this._drawPieBack(item, index)}
                            fill={this.getNewColors(index)}
                            opacity={Platform.OS=='ios'?0:0.1}
                      />
                    </TouchableWithoutFeedback>
                  )
                )
              }
            </G>
          </Svg>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style = {styles.container}>
        {
          this.renderPieChart()
        }
      </View>
    );
  }


  renderNewChartValue=(val, index) =>{
    //console.log("renderChartValue", val, index)
    let icon  = this._newCreatePieImage(val, index)
    return (
      <View key={index + 'view'} style = {[this._createPieDetailViewCSS(val, index)]}>
        {
          icon&&<Image source = {this._newCreatePieImage(val, index)} style = {{marginRight: 5, marginLeft: 5, width: 20, height: 20, resizeMode: 'contain'}}/>
        }
        {
          this.state.newChartData[index].title!=''&&<Text style = {this._createPieLabelCSS(val, index)}>{this.state.newChartData[index].title}</Text>
        }
        {
          this.state.showChartValue[index]&&this.state.chartMode != 'Mode2'&&
          <Text style = {this._createPiePercentCSS(val, index)}>{this.state.newChartData[index].value + ' pts'}</Text>
        }
      </View>
    )
  }

  renderChartPercent=(val, index) =>{
    return (
      <View key={index + 'percent'} style = {[this._createPieDetailPercentCSS(val, index)]}>
        {<Text style = {this._createPiePercentCSS(val, index)}>{this.state.chartValue[index]}</Text>}
      </View>
    )
  }

  _drawPieBack(data, index) {
    let itemCount = this.state.newChartData.length
    var radius = 110;
    var padding = 3;
    var dx = padding * Math.cos(Math.PI / itemCount + Math.PI / itemCount * (2*index+1));
    var dy = padding * Math.sin(Math.PI / itemCount + Math.PI / itemCount * (2*index+1));
    var innerRadius = chartInnerRadius - padding;

    var mx1 = chartRadius + (innerRadius) * Math.cos(Math.PI / itemCount + 2*Math.PI / itemCount * index)+dx;
    var my1 = chartRadius + (innerRadius) * Math.sin(Math.PI / itemCount + 2*Math.PI / itemCount * index)+dy;
    var l1 = chartRadius + (innerRadius + radius) * Math.cos(Math.PI / itemCount + 2*Math.PI / itemCount * index)+dx;
    var l2 = chartRadius + (innerRadius + radius) * Math.sin(Math.PI / itemCount + 2*Math.PI / itemCount * index)+dy;
    var mx11 = chartRadius + (innerRadius) * Math.cos(Math.PI / itemCount + 2*Math.PI / itemCount * (index + 1))+dx;
    var my12 = chartRadius + (innerRadius) * Math.sin(Math.PI / itemCount + 2*Math.PI / itemCount * (index + 1))+dy;
    var l11 = chartRadius + (innerRadius + radius) * Math.cos(Math.PI / itemCount + 2*Math.PI / itemCount * (index + 1))+dx;
    var l12 = chartRadius + (innerRadius + radius) * Math.sin(Math.PI / itemCount + 2*Math.PI / itemCount * (index + 1))+dy;

    var path1 = "M" + l1 + " " + l2 + " " + "L" + mx1 + " " + my1;
    var path2 = "A" + chartInnerRadius + " " + chartInnerRadius + " 0 0 1 " + mx11 + " " + my12;
    var path3 = "L" + l11 + " " + l12;
    var path4 = "A" + (chartInnerRadius + radius) + " " + (chartInnerRadius + radius) + " 0 0 0 " + l1 + " " + l2;
    return path1 + " " + path2 + " " + path3 + " " + path4;
  }

  _createPieChart(data, index) {
    let itemCount = this.state.newChartData.length
    var padding = 3;
    var radius = 10 + data * 20;
    var innerRadius = chartInnerRadius;
    if(this.state.chartMode == 'Mode2'){
      radius = 13 + data * 11;
      innerRadius = 25;
    }
    innerRadius -= padding;
    var dx = padding * Math.cos(Math.PI / itemCount + Math.PI / itemCount * (2*index+1));
    var dy = padding * Math.sin(Math.PI / itemCount + Math.PI / itemCount * (2*index+1));

    var mx1 = chartRadius + (innerRadius) * Math.cos(Math.PI / itemCount + 2 * Math.PI / itemCount * index)+dx;
    var my1 = chartRadius + (innerRadius) * Math.sin(Math.PI / itemCount + 2 * Math.PI / itemCount * index)+dy;
    var l1 = chartRadius + (innerRadius + radius) * Math.cos(Math.PI / itemCount + 2 * Math.PI / itemCount * index)+dx;
    var l2 = chartRadius + (innerRadius + radius) * Math.sin(Math.PI / itemCount + 2 * Math.PI / itemCount * index)+dy;
    var mx11 = chartRadius + (innerRadius) * Math.cos(Math.PI / itemCount + 2 * Math.PI / itemCount * (index + 1))+dx;
    var my12 = chartRadius + (innerRadius) * Math.sin(Math.PI / itemCount + 2 * Math.PI / itemCount * (index + 1))+dy;
    var l11 = chartRadius + (innerRadius + radius) * Math.cos(Math.PI / itemCount + 2 * Math.PI / itemCount * (index + 1))+dx;
    var l12 = chartRadius + (innerRadius + radius) * Math.sin(Math.PI / itemCount + 2 * Math.PI / itemCount * (index + 1))+dy;



    var path1 = "M" + l1 + " " + l2 + " " + "L" + mx1 + " " + my1;
    var path2 = "A" + (innerRadius+padding) + " " + (innerRadius+padding) + " 0 0 1 " + mx11 + " " + my12;
    var path3 = "L" + l11 + " " + l12;
    var path4 = "A" + (innerRadius + radius+padding) + " " + (innerRadius + radius+padding) + " 0 0 0 " + l1 + " " + l2;
    return path1 + " " + path2 + " " + path3 + " " + path4;
  }

  _createPieDetailViewCSS(scale, index) {
    let itemCount = this.state.newChartData.length
    let extraRadius = itemCount > 5? (itemCount - 5)*2: 0
    var radius = 10 + scale * 20 + extraRadius
    var css = {};
    var xRadius = chartRadius-40;
    var yRadius = chartRadius-40;
    if (this.state.chartMode == 'Mode2'){
      radius = 0;
      xRadius = chartRadius - 25
    }
    css['width'] = 80;
    css['height'] = 80;
    css['justifyContent'] = 'center';
    css['backgroundColor'] = 'transparent';
    css['alignItems'] = 'center';
    css['position'] = 'absolute';
    if(radius > (chartRadius - chartInnerRadius)/2 + extraRadius){
      css['left'] = chartRadius + (radius - 5) * Math.cos(2*Math.PI / itemCount + 2*Math.PI / itemCount * index) - 40;
      css['top'] = chartRadius + (radius - 5) * Math.sin(2*Math.PI / itemCount + 2*Math.PI / itemCount * index) - 40;
    } else {
      css['left'] = chartRadius + xRadius * Math.cos(2*Math.PI / itemCount + 2*Math.PI / itemCount * index) - 40;
      css['top'] = chartRadius + yRadius * Math.sin(2*Math.PI / itemCount + 2*Math.PI / itemCount * index) - 40;
    }

    return css;
  }

  _createPieDetailPercentCSS(scale, index) {
    let itemCount = this.state.newChartData
    var radius = 15 + scale * 11
    var css = {};
    css['width'] = 80;
    css['height'] = 80;
    css['justifyContent'] = 'center';
    css['backgroundColor'] = 'transparent';
    css['alignItems'] = 'center';
    css['position'] = 'absolute';
    css['left'] = chartRadius + (30 + scale * 8) * Math.cos(2*Math.PI / itemCount + 2*Math.PI / itemCount * index) - 40;
    css['top'] = chartRadius + (30 + scale * 8) * Math.sin(2*Math.PI / itemCount + 2*Math.PI / itemCount * index) - 40;

    return css;
  }

  _newCreatePieImage(scale, index) {
    var radius = 10 + scale * 20;
    if(this.state.chartMode == 'Mode2'){
      radius = 0;
    }
    if(radius > (chartRadius - chartInnerRadius)/2) return this.state.newChartData[index].inIcon;
    else return this.state.newChartData[index].outIcon;
  }

  _createPiePercentCSS(scale, index) {
    var radius = 10 + scale * 20;
    if ( this.state.chartMode == 'Mode2'){
      radius = 110;
    }
    var css = {};
    css['textAlign'] = 'center';
    css['fontSize'] = 9;
    if ( this.state.logoMode == 'avatar'){
      if ( this.state.newChartData.length > 6 ) css['fontSize'] = 22 - this.state.newChartData.length;
      else css['fontSize'] = 16
    }
    if(radius > (chartRadius - chartInnerRadius)/2) css['color'] = 'white';
    else css['color'] = '#5e8aa3';
    css['fontWeight'] = 'bold';
    return css;
  }

  _createPieLabelCSS(scale, index) {
    var radius = 10 + scale * 20
    if ( this.state.chartMode == 'Mode2'){
      radius = 0;
    }
    var css = {};
    css['textAlign'] = 'center';
    css['fontSize'] = 8;
    if(radius > (chartRadius - chartInnerRadius)/2) css['color'] = 'white';
    else css['color'] = '#5e8aa3';
    css['fontWeight'] = 'bold';
    return css;
  }

  _createPieCenter(){
    var css = {};
    css['width'] = 64;
    css['height'] = 64;
    css['resizeMode'] = 'contain';
    if ( this.state.chartMode == 'Mode2'){
      css['width'] = 44;
      css['height'] = 44;
    }
    if ( this.state.logoMode == 'logo'){
      css['width'] -= 4;
      css['height'] -= 4;
    }
    return css;
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1, alignItems: 'center', justifyContent: 'center', position: 'relative',width: null,
  },
  chartContainer:{
    flex:1,justifyContent: 'center',alignItems: 'center', width:width,marginTop:20,
  },
  avartar:{
    width:64,
    height:64,
    //resizeMode:'contain',
    borderRadius: 32
  },
  percentView: {
    position: 'absolute',
    left: 118,
    top: 118,
    width: 64,
    height: 64,
    justifyContent: 'center',
    alignItems: 'center',
  },
  weekLabel:{
    marginLeft:10,
    marginRight:10
  }
})