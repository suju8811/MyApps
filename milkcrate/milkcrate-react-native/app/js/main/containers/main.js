'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  StatusBar,
  Platform,
  AsyncStorage,
  Linking,
  Alert,
  NativeAppEventEmitter,
  AppState
} from 'react-native';

import TabNavigator from 'react-native-tab-navigator';
import Permissions from 'react-native-permissions';
import Home from '../../home/containers/home';
import Feed from '../../feed/containers/feed';
// import Search from '../../search/containers/search';
import PinListView from '../../search/containers/pinListView'
import Notifications from '../../alert/containers/notifications';
import Activity from '../../activity/containers/activity';
import Profile from '../../profile/containers/profile';
// import BackgroundGeolocation from 'react-native-background-geolocation';
import DeviceInfo from 'react-native-device-info';
import Orientation from 'react-native-orientation';
import Spinner from 'react-native-loading-spinner-overlay';
import Modal from 'react-native-modalbox';
import { Actions } from 'react-native-router-flux';
import NotifRequestModal from '../../components/NotifRequestModal';
import AllowLocationModal from '../../components/AllowLocationModal';
import MissingNotifyModal from '../../components/MissingNotifyModal';
import AccessLocationModal from '../../components/AccessLocationModal';
import { LocalStorage } from '../../styles/localStorage';
import Geocoder from 'react-native-geocoder';
import * as StoreReview from 'react-native-store-review';

import * as commonColors from '../../styles/commonColors';
import * as commonStyles from '../../styles/commonStyles';

import {runningMode} from '../../../config'

const homeIcon = require('../../../assets/imgs/tabbar-icons/home.png');
const homeSelectedIcon = require('../../../assets/imgs/tabbar-icons/home_selected.png');
const searchIcon = require('../../../assets/imgs/tabbar-icons/search.png');
const searchSelectedIcon = require('../../../assets/imgs/tabbar-icons/search_selected.png');
const pinIcon = require('../../../assets/imgs/tabbar-icons/pin.png');
const pinSelectedIcon = require('../../../assets/imgs/tabbar-icons/pin_selected.png');
const activityIcon = require('../../../assets/imgs/tabbar-icons/activity.png');
const activitySelectedIcon = require('../../../assets/imgs/tabbar-icons/activity_selected.png');
const youIcon = require('../../../assets/imgs/tabbar-icons/you.png');
const youSelectedIcon = require('../../../assets/imgs/tabbar-icons/you_selected.png');

import bendService from '../../bend/bendService'
import * as _ from 'underscore'
import * as async from 'async'
import UtilService from '../../components/util'
import Cache from '../../components/Cache'
import PushNotification from 'react-native-push-notification';
import DeepLinking from 'react-native-deep-linking';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as commonActions from '../../common/actions';
import * as commonActionTypes from '../../common/actionTypes';

console.log('commonColors', commonColors)

Text.defaultProps.allowFontScaling=false

let ONE_WEEK_MILISEC = 7 * 24 * 3600 * 1000

class Main extends Component {
  constructor(props) {
    super(props);
    this.sessionCount = {
      lastTime:0,
      count:0,
      version:0
    };

    let tab = this.props.tab;

    if ((tab == null) || (tab == 'modal')) {
      tab = 'home';
    }

    this.state = {
      selectedTab: tab,
      badge: 0,
      searchAutoFocus: false,
      showingModal: false,
      countByCategory:{},
      showEnableNotifModal: false,
      showEnableLocationModal: false,
      showDisabledNotifModal: false,
      showDisabledLocationModal: false,
      appState: AppState.currentState,
      initialize:false
    };

    this.last = null;

    StatusBar.setHidden(false);

    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('light-content', false);
    } else if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor(commonColors.theme, false);
    }

    this.geofenceEventHandler = null
  }

  componentDidMount() {
    const _this = this
    const { isRequestingNotif, isRequestingLocation } = this.state;
    if (this.props.tab === 'modal') {
      this.showActivityDetailModal();
    }

    console.log('Cache.deviceModel', DeviceInfo.getSystemName(), DeviceInfo.getSystemVersion(), DeviceInfo.getDeviceId())

    this.hasMounted = true

    Orientation.unlockAllOrientations();

    let activeUser = bendService.getActiveUser();
    if(!activeUser.name) {
      bendService.getCommunity((err, ret)=>{
        if(!err) {
          if(ret.teamsPromptEnabled) {
            Actions.MyTeams();
          } else {
            Actions.SetupProfile();
          }
        }
      })
      return;
    }
    this.setState({
      initialize:true
    })

    AppState.addEventListener('change', this._handleAppStateChange);

    AsyncStorage.getItem('@milkcrate:sessionCount').then((ret)=>{
      if(ret) {
        this.sessionCount = JSON.parse(ret)
      }

      if(DeviceInfo.getVersion() != this.sessionCount.version) {
        //reset session count
        this.sessionCount.count = 0;
        this.sessionCount.version = DeviceInfo.getVersion()
        this.sessionCount.lastTime = 0
      }

      this.increaseSessionCount();
    })

      /*
      if (Platform.OS === 'ios') {

        PushNotificationIOS.addEventListener('register', (token)=>{
            this.saveInstallationInfo(activeUser, token)

        });
        PushNotificationIOS.addEventListener('registrationError', ()=>{
            this.saveInstallationInfo(activeUser, null)
        });
        PushNotificationIOS.addEventListener('notification', this._onRemoteNotification);
        PushNotificationIOS.addEventListener('localNotification', this._onLocalNotification);

        PushNotificationIOS.requestPermissions();
      } else if (Platform.OS === 'android') {

      }
      */

    this._checkPushNotification();
    // PushNotification.checkPermissions((ret)=>{
    //   console.log('checkPermissions : ', ret)
    //   if(!ret.alert) {
    //     this._checkPushNotification();
    //   } else {
    //     this._checkLocation()
    //   }
    // })

    bendService.getCommunityActivityCountByCategory((err, ret)=>{
      if(err){
        console.log(err);
        return;
      }

      this.setState({
        countByCategory:ret
      })
    })
  }

  componentWillUnmount() {
    this.hasMounted = false;
    navigator.geolocation.clearWatch(this.watchID);
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      this.increaseSessionCount();
      this.props.commonActions.appActivated();
    } else {
      // this.props.commonActions.appInActivated();
    }
    this.setState({appState: nextAppState});
  }

  increaseSessionCount() {
    this.sessionCount.count++;
    bendService.getUser(()=>{
      console.log('this.sessionCount', this.sessionCount, Cache.cacheMap["user"].points)

      if (this.sessionCount.count >= 5 && Cache.cacheMap["user"].points >= 10) {
        if(this.sessionCount.lastTime == 0 || Date.now()/1000 - this.sessionCount.lastTime > 30 * 24 * 3600) {
          this.sessionCount.lastTime = Date.now()/1000

          if (StoreReview.isAvailable) {
            StoreReview.requestReview();
          }
        }
      }

      AsyncStorage.setItem('@milkcrate:sessionCount', JSON.stringify(this.sessionCount));
    })
  }

  componentWillReceiveProps(nextProps){

    // console.log( 'main - componentWillReceiveProps');

    // if (nextProps.tab != this.state.selectedTab) {
    //   this.setState({ selectedTab: tab });
    // }
  }

  _checkPushNotification() {
    //check enableNotifyAnswer
    AsyncStorage.getItem('enablePushNotifyAnswer', (err, ret)=>{
      console.log('enablePushNotifyAnswer', err, ret)
      if(err) {
        this._checkLocation();
        return;
      }
      if(!ret) {
        this._openEnableNotify()
        return;
      }

      this.onEnableNotify()
      if(ret == 'yes') {
        //check push notification
        AsyncStorage.getItem('nativePushNotifyPrompt', (err, ret)=>{
          console.log('nativePushNotifyPrompt', err, ret)
          if(err) {
            this._checkLocation();
            return;
          }

          if(ret == 'yes') {
            //nothing to do
            this._checkLocation();
          } else {
            //get native prompt time
            AsyncStorage.getItem('disabledPushNotifyTime', (err, ret)=>{
              if(err) {
                this._checkLocation();
                return;
              }

              var pastTime = Date.now() - Number(ret);
              if(pastTime > ONE_WEEK_MILISEC) {
                this._openDisablePushNotifyModal()
                return;
              } else {
                this._checkLocation();
              }
            })
          }
        })
      } else {
        this._openEnableNotify()
        /*//get last time
        AsyncStorage.getItem('enablePushNotifyTime', (err, ret)=>{
          if(err) {
            return;
          }

          var pastTime = Date.now() - Number(ret);
          if(pastTime > ONE_WEEK_MILISEC) {
            this._openEnableNotify()
            return;
          }
        })*/
      }
    })
  }

  _checkLocation() {
    console.log('_checkLocation')
    Permissions.checkMultiplePermissions(['location'])
      .then(response => {

        console.log('location permission', response['location'])
        if(response['location'] == 'authorized') {

          this.runLocationService();
        } else {
          //check enableLocationAccessAnswer
          AsyncStorage.getItem('enableLocationAccessAnswer', (err, ret)=>{
            if(err) {
              return;
            }
            if(!ret) {
              this._openEnableLocation()
              return;
            }
            if(ret == 'yes') {
              //check push notification
              AsyncStorage.getItem('nativeLocationAccessPrompt', (err, ret)=>{
                if(err) {
                  return;
                }

                if(ret == 'yes') {
                  //nothing to do
                } else {
                  //get native prompt time
                  AsyncStorage.getItem('disabledLocationAccessTime', (err, ret)=>{
                    if(err) {
                      return;
                    }

                    var pastTime = Date.now() - Number(ret);
                    if(pastTime > ONE_WEEK_MILISEC) {
                      this._openDisableLocationAccessModal()
                      return;
                    }
                  })
                }
              })
            } else {
              this._openEnableLocation()
              /*//get last time
              AsyncStorage.getItem('enablePushNotifyTime', (err, ret)=>{
                if(err) {
                  return;
                }

                var pastTime = Date.now() - Number(ret);
                if(pastTime > ONE_WEEK_MILISEC) {
                  this._openEnableNotify()
                  return;
                }
              })*/
            }
          })
        }
      });
  }

  _openEnableNotify() {
    this.setState({
      showEnableNotifModal:true
    })
    AsyncStorage.setItem('enablePushNotifyTime', String(Date.now()))
  }

  _openDisablePushNotifyModal() {
    this.setState({
      showDisabledNotifModal:true
    })
    AsyncStorage.setItem('disabledPushNotifyTime', String(Date.now()))
  }

  _openEnableLocation() {
    this.setState({
      showEnableLocationModal:true
    })
    AsyncStorage.setItem('enableLocationAccessTime', String(Date.now()))
  }

  _openDisableLocationAccessModal() {
    this.setState({
      showDisabledLocationModal:true
    })
    AsyncStorage.setItem('disabledLocationAccessTime', String(Date.now()))
  }

  _onRemoteNotification(notification) {
    //console.log("notification : ", notification)
    let url;

    if (Platform.OS == 'ios') {
      url = notification.data.deeplink;
    } else {
      url = notification.deeplink;
    }

    if (notification.foreground) {
      //in-use
      if (url) {
        Alert.alert(
          notification.title,
          notification.message,
          [
            { text: notification.cancelButton || 'Dismiss', onPress: () => {
              console.log("Canceled")
            }},
            { text: notification.actionButton || 'View Now', onPress: () => {
              Linking.openURL(url)
            }},
          ]
        );
      } else {
        Alert.alert(
          notification.title,
          notification.message
        )
      }
    } else {
      //background notification
      UtilService.mixpanelEvent('App Launched from Push', {
        notification:notification
      })
      if (url)
        Linking.openURL(url)
    }
  }

  _onLocalNotification(notification){

  }

  saveInstallationInfo(activeUser, token) {
    /*
      - appVersion
      - bundleId (this is the package name for  android)
      - user – if a user is logged in
      - deviceName  – "Kostas' iPhone"
      - deviceModel – "iPhone"
      - deviceVersion – "iPhone7,2"
      - systemName – the OS ("iOS")
      - systemVersion – the OS version
      - deviceId – the vendorID on iOS, unique device id on android
      - buildType – "release" or "development" or "staging"
      - apnsToken – iOS only, the push token
      - gcmRegistrationId – Android only, the android push token
    */

    AsyncStorage.getItem('milkcrate-installation-info', (err, ret)=>{
      var installInfo = ret
      //console.log("installationInfo", installInfo)
      if (installInfo) {
        installInfo = JSON.parse(installInfo)
      } else {
        installInfo = {}
      }
      _.extend(installInfo, {
        appVersion: DeviceInfo.getVersion(),
        bundleId: DeviceInfo.getBundleId(),
        user: activeUser ? {
          "_type": "BendRef",
          "_id": activeUser._id,
          "_collection": "user"
        } : null,
        deviceName: DeviceInfo.getDeviceName(),
        deviceModel: DeviceInfo.getModel(),
        deviceVersion: DeviceInfo.getDeviceId(),
        systemName: DeviceInfo.getSystemName(),
        systemVersion: DeviceInfo.getSystemVersion(),
        deviceId: DeviceInfo.getUniqueID(),
        buildType: (__DEV__ ? "development" : "production"),
      })

      if (token) {
        if (Platform.OS == 'ios') {
          installInfo.apnsToken = token
        } else {
          installInfo.gcmRegistrationId = token
        }
      }

      console.log('installInfo', installInfo)

      bendService.saveInstallInformation(installInfo, (error, ret)=>{
        if (!error) {
          AsyncStorage.setItem('milkcrate-installation-info', JSON.stringify(ret.result));
        }
      })
    });
  }

  showActivityDetailModal() {
    const {
      subOne,
      id,
    } = this.props;

    this.hasMounted && this.setState({ showingModal: true });

    bendService.getActivityWithId(subOne, id, (error, result) => {

      this.hasMounted && this.setState({ showingModal: false });

      if (error) {
        console.log(error);
        return;
      }

      switch( subOne ){
        case 'action':
          Actions.ActionDetailModal({ action: result, modal: true });
          break;

        case 'business':
          Actions.BusinessesDetailModal({ business: result, modal: true });
          break;

        case 'event':
          Actions.EventDetailModal({ event: result, modal: true});
          break;

        case 'volunteer_opportunity':
          Actions.VolunteerDetailModal({ volunteer: result, modal: true });
          break;

        default:
          break;
      }


    });
  }

  onSelectSearch() {
    this.hasMounted && this.setState({
      selectedTab: 'search',
      searchAutoFocus: true,
    });
  }

  onSelectTab( tab ) {
    this.hasMounted && this.setState({
      selectedTab: tab,
    });
  }

  onDeniedNotification() {

  }

  onEnableNotify() {
    const _this = this
    this.setState({showEnableNotifModal: false})
    console.log('Enabled Notification Permission!')

    AsyncStorage.setItem('enablePushNotifyAnswer', 'yes')
    let activeUser = bendService.getActiveUser();

    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: (token)=> {
        console.log('PushNotification onRegister', token)
        this.saveInstallationInfo(activeUser, (token ? token.token : null))
        if(Platform.OS != 'ios') {
          AsyncStorage.setItem('nativePushNotifyPrompt', 'yes')
          this._checkLocation();
        }
      },
      onError:()=>{
        console.log('PushNotification onError')
        if(Platform.OS != 'ios') {
          AsyncStorage.setItem('nativePushNotifyPrompt', 'no')
          this._openDisablePushNotifyModal()
        }
      },

      // (required) Called when a remote or local notification is opened or received
      onNotification: (notification)=> {
        this._onRemoteNotification(notification)
      },

      // ANDROID ONLY: GCM Sender ID (optional - not required for local notifications, but is need to receive remote push notifications)
      senderID: "887728351696", //"1491633624875",

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       */
      requestPermissions: Platform.OS == 'ios'?false:true,
    });

    if(Platform.OS == 'ios') {
      PushNotification.requestPermissions().then(response => {
        console.log(' PushNotification.requestPermissions response', response)
        if(response.alert == 1) {
          AsyncStorage.setItem('nativePushNotifyPrompt', 'yes')
          this._checkLocation();
        } else {
          AsyncStorage.setItem('nativePushNotifyPrompt', 'no')

          this._openDisablePushNotifyModal()
        }
      });
    }

  }

  onDeniedNotify() {
    AsyncStorage.setItem('enablePushNotifyAnswer', 'no')
    this.setState({showEnableNotifModal: false})
    console.log('Denied Notification Permission!')
    this._checkLocation();
  }

  onEnableLocation() {
    this.setState({showEnableLocationModal: false})
    console.log('Enabled Location Permission!')
    AsyncStorage.setItem('enableLocationAccessAnswer', 'yes')

    Permissions.requestPermission('location', 'always')
      .then(response => {
        console.log('request location permission', response)
        if(response == 'authorized') {
          this.runLocationService()
        } else {
          AsyncStorage.setItem('nativeLocationAccessPrompt', 'no')
          this._openDisableLocationAccessModal()
        }
      });
  }

  runLocationService() {
    Cache.locationEnabled = true;
    navigator.geolocation.getCurrentPosition(
      (position) => {
        LocalStorage.save({
          key: commonStyles.geoLocation.currentLocation,
          rawData: position,
        });
      },
      (error) => {
        console.log(JSON.stringify(error));
      },
      {enableHighAccuracy: commonStyles.geoLocation.enableHighAccuracy, timeout: commonStyles.geoLocation.timeout, maximumAge: commonStyles.geoLocation.maximumAge}
    );

    this.watchID = navigator.geolocation.watchPosition((position) => {
      LocalStorage.save({
        key: commonStyles.geoLocation.currentLocation,
        rawData: position,
      });
    });

    AsyncStorage.setItem('nativeLocationAccessPrompt', 'yes')

    /*BackgroundGeolocation.configure({
      desiredAccuracy: 10,
      stationaryRadius: 50,
      distanceFilter: 50,
      debug: false,
      stopOnTerminate: false,
      startForeground: false,
      interval: 10000
    }, function () {});

    BackgroundGeolocation.on('location', (location) => {
      if (this.last) {
        if (this.last.latitude == location.latitude && this.last.longitude == location.longitude) {
          return;
        }
      }

      this.last = location;
      //console.log('[DEBUG] BackgroundGeolocation location', location);

      Geocoder.geocodePosition({
        lat: location.latitude,
        lng: location.longitude
      }).then(res => {
        //console.log('geocodePosition', res)
        //save to bend
        bendService.saveGeoLocation({
          latitude: location.latitude,
          longitude: location.longitude,
          speed: location.speed,
          altitude: location.altitude,
          accuracy: location.accuracy,
          address: res[0]?res[0].formattedAddress:null
        }, (error, result)=>{
          //console.log(error, result);
        })
      })
    });

    BackgroundGeolocation.start(() => {
      console.log('[DEBUG] BackgroundGeolocation started successfully');
    });*/

    // BackgroundGeolocation.configure({
    //   // Geolocation Config
    //   desiredAccuracy: 10,
    //   distanceFilter: 50,
    //   // Activity Recognition
    //   stopTimeout: 1,
    //   // Application config
    //   debug: false, // <-- enable this hear sounds for background-geolocation life-cycle.
    //   logLevel: BackgroundGeolocation.LOG_LEVEL_VERBOSE,
    //   stopOnTerminate: false,   // <-- Allow the background-service to continue tracking when user closes the app.
    //   startOnBoot: true,        // <-- Auto start tracking when device is powered-up.
    // }, (state) => {
    //   console.log("- BackgroundGeolocation is configured and ready: ", state.enabled);

    //   if (!state.enabled) {
    //     // 3. Start tracking!
    //     BackgroundGeolocation.start(function() {
    //       console.log("- Start success");
    //     });
    //   }
    // });

    // BackgroundGeolocation.on('geofence', (params) => {
    //   console.log('geofence event', params, params.action, params.identifier)
    //   bendService.saveGeofenceTrigger({
    //     trigger:this.getTrigerName(params.action, params.identifier),
    //     lat:params.location.coords.latitude,
    //     long:params.location.coords.longitude,
    //     userAddress:params.identifier=='home'?Cache.userAddress.homeAddress._id:Cache.userAddress.workAddress._id
    //   }, (err, ret)=>{
    //     console.log(err, ret)
    //   })
    // });

    // BackgroundGeolocation.on('location', (location) => {
    //   console.log('BackgroundGeolocation location', location)
    //   if (this.last) {
    //     if (this.last.latitude == location.coords.latitude && this.last.longitude == location.coords.longitude) {
    //       return;
    //     }
    //   }

    //   this.last = location.coords;
    //   //console.log('[DEBUG] BackgroundGeolocation location', location);

    //   Geocoder.geocodePosition({
    //     lat: location.coords.latitude,
    //     lng: location.coords.longitude
    //   }).then(res => {
    //     //console.log('geocodePosition', res)
    //     //save to bend
    //     bendService.saveGeoLocation({
    //       latitude: location.coords.latitude,
    //       longitude: location.coords.longitude,
    //       speed: location.coords.speed,
    //       altitude: location.coords.altitude,
    //       accuracy: location.coords.accuracy,
    //       address: res[0]?res[0].formattedAddress:null
    //     }, (error, result)=>{
    //       //console.log(error, result);
    //     })
    //   })
    // });
  }

  getTrigerName(action, identifier) {
    var prefix = action=='ENTER'?'arrived':'left';
    return prefix + '-' + identifier
  }

  onDeniedLocation() {
    AsyncStorage.setItem('enableLocationAccessAnswer', 'no')
    this.setState({showEnableLocationModal: false})
    console.log('Denied Location Permission!')
  }

  render() {
    let {initialize} = this.state
    const {
      subOne,
        subTwo
    } = this.props;
    const { modalStyle, modalStyleSmall } = styles;

    if(!initialize) return null

    let DynamicHome = runningMode=='Normal'?Home:Feed
    return (
      <View style={ styles.container }>
        <Spinner visible={ this.state.showingModal }/>
        <TabNavigator
          tabBarStyle = { styles.tab }
          sceneStyle={ styles.scene }
        >
          {/* Home */}
          <TabNavigator.Item
            selected={ this.state.selectedTab === 'home' }
            title="Home"
            selectedTitleStyle={ [styles.selectedText, {color:commonColors.tabIconActiveColor}] }
            titleStyle={ styles.text }
            renderIcon={ () => <Image source={ homeIcon } style={ [styles.iconTabbar1, {tintColor:commonColors.tabIconInactiveColor}] }/> }
            renderSelectedIcon={ () => <Image source={ homeSelectedIcon } style={ [styles.iconTabbar1, {tintColor:commonColors.tabIconActiveColor}] }/> }
            onPress={ () => this.onSelectTab('home') }>
            {<DynamicHome
              selectedTab={ this.state.selectedTab }
              countByCategory={this.state.countByCategory}
              subOne={ subOne }
              onSearch={ () => this.onSelectSearch() }
            />}
          </TabNavigator.Item>

          {/* pin */}
          <TabNavigator.Item
            selected={ this.state.selectedTab === 'pin' }
            title="Pinned"
            selectedTitleStyle={ [styles.selectedText, {color:commonColors.tabIconActiveColor}] }
            titleStyle={ styles.text }
            renderIcon={ () => <Image source={ pinIcon } style={ [styles.iconTabbar2, {tintColor:commonColors.tabIconInactiveColor}] }/> }
            renderSelectedIcon={ () => <Image source={ pinSelectedIcon } style={ [styles.iconTabbar2, {tintColor:commonColors.tabIconActiveColor}] }/> }
            onPress={ () => this.onSelectTab('pin') }>
            <PinListView/>
          </TabNavigator.Item>

          {/* Activity */}
          <TabNavigator.Item
            selected={ this.state.selectedTab === 'activity' }
            title="Activity"
            selectedTitleStyle={ [styles.selectedText, {color:commonColors.tabIconActiveColor}] }
            titleStyle={ styles.text }
            renderIcon={ () => <Image source={ activityIcon } style={ [styles.iconTabbar3, {tintColor:commonColors.tabIconInactiveColor}] } resizeMode="contain"/> }
            renderSelectedIcon={ () => <Image source={ activitySelectedIcon } style={ [styles.iconTabbar3, {tintColor:commonColors.tabIconActiveColor}] }/> }
            badgeText={ this.state.badge }
            onPress={ () => this.onSelectTab('activity') }>
            {<Activity
              subOne={ subOne }
              onSearch={ () => this.onSelectSearch() }
            />}
          </TabNavigator.Item>

          {/* Profile */}
          <TabNavigator.Item
            selected={ this.state.selectedTab === 'profile' }
            title="Profile"
            selectedTitleStyle={ [styles.selectedText, {color:commonColors.tabIconActiveColor}] }
            titleStyle={ styles.text }
            renderIcon={ () => <Image source={ youIcon } style={ [styles.iconTabbar4, {tintColor:commonColors.tabIconInactiveColor}] }/> }
            renderSelectedIcon={ () => <Image source={ youSelectedIcon } style={ [styles.iconTabbar4, {tintColor:commonColors.tabIconActiveColor}] }/> }
            onPress={ () => this.onSelectTab('profile') }>
            <Profile
              subOne={ subOne }
              subTwo={ subTwo }
              selectedTab={ this.state.selectedTab }
              onSearch={ () => this.onSelectSearch() }
            />
          </TabNavigator.Item>
        </TabNavigator>
        <Modal
          isOpen={this.state.showEnableNotifModal}
          style={modalStyle}
          ref={"modal"}
          swipeToClose={false}
        >
          <NotifRequestModal
            onPressYes={() => this.onEnableNotify()}
            onPressNo={() => this.onDeniedNotify()}
          />
        </Modal>
        <Modal
          isOpen={this.state.showEnableLocationModal}
          style={modalStyle}
          ref={"modal"}
          swipeToClose={false}
        >
          <AllowLocationModal
            onPressYes={() => this.onEnableLocation()}
            onPressNo={() => this.onDeniedLocation()}
          />
        </Modal>
        <Modal
          isOpen={this.state.showDisabledNotifModal}
          style={modalStyle}
          ref={"modal"}
          swipeToClose={false}
        >
          <MissingNotifyModal
            onPressNo={() => {
              this.setState({showDisabledNotifModal: false})
              this._checkLocation()
            }}
          />
        </Modal>
        <Modal
          isOpen={this.state.showDisabledLocationModal}
          style={modalStyle}
          ref={"modal"}
          swipeToClose={false}
        >
          <AccessLocationModal
            onPressNo={() => this.setState({showDisabledLocationModal: false})}
          />
        </Modal>
      </View>
    );
  }
}

export default connect(props => ({
    commonStatus: props.common.status,
  }),
  (dispatch) => ({
    commonActions: bindActionCreators(commonActions, dispatch),
  })
) (Main);


const pageMargin = 8;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    width: commonStyles.screenWidth,
    height: commonStyles.screenHeight,
  },
  iconTabbar1: {
    width: 20,
    height: 20,
    resizeMode:'contain'
  },
  iconTabbar2: {
    width: 20,
    height: 20,
    resizeMode:'contain'
  },
  iconTabbar3: {
    width: 20,
    height: 20,
    resizeMode:'contain'
  },
  iconTabbar4: {
    width: 20,
    height: 20,
    resizeMode:'contain'
  },
  text: {
    fontFamily: 'Open Sans',
    fontSize: 10,
  },
  selectedText: {
    color: commonColors.theme,
    fontFamily: 'Open Sans',
    fontSize: 10,
  },
  tab: {
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: commonColors.theme,
    paddingBottom:commonStyles.isIphoneX()?20:0,
    height:commonStyles.isIphoneX()?70:50
  },
  scene: {
    paddingBottom:commonStyles.isIphoneX()?70:50
  },
  modalStyle: {
    backgroundColor:'transparent',
  },
});
