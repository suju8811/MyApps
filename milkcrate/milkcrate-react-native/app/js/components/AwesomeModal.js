import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  Image,
  Text,
  Platform,
  Dimensions,
  TouchableWithoutFeedback,
  Linking
} from 'react-native';

import UtilService from '../components/util'

import { Actions } from 'react-native-router-flux';
import * as commonColors from '../styles/commonColors';
import * as commonStyles from '../styles/commonStyles';
import { screenWidth, screenHeight } from '../styles/commonStyles';
const close_button = require('../../assets/imgs/close_button.png');
const pageMargin = 24;
const awesome = require('../../assets/imgs/modal-imgs/awesome.png');

export default class AwesomeModal extends Component {

  constructor(props) {
    super(props);
  }

  onClose() {
    Actions.pop();
  }

  render() {
    const { containerStyle,
            containerStyleSmall,
            imageStyle,
            imageStyleSmall,
            headerStyle,
            bodyStyle,
            buttonContainerStyle,
            buttonStyle,
      buttonFullStyle,
            buttonTextStyle,
            closeStyle,
            actionButton
          } = styles;
    const { points, onPressOK } = this.props;
    return (
      <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
        <View style={screenWidth > 320? containerStyle : containerStyleSmall}>
          <Text style={headerStyle}>
            AWESOME!
          </Text>

          <Text style={bodyStyle}>
            You just earned {points} points!
          </Text>
          <Text style={[bodyStyle, {color:'#A4A4A3'}]}>
            Activities are assigned point values based on their level of impact. You will see each opportunity's point value next to its title!
          </Text>

          <View style={buttonContainerStyle}>
            <TouchableWithoutFeedback onPress={onPressOK}>
              <View style={[buttonStyle,{backgroundColor: '#82CCBE'}]}>
                <Text style={[buttonTextStyle,{color: 'white'}]}>OK</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
        <Image source={awesome} style={imageStyle}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    width: screenWidth - pageMargin * 2,
    height: 240,
    padding: 26,
    paddingTop: 30,
    backgroundColor:'white',
    borderRadius:4
  },

  containerStyleSmall: {
    width: screenWidth - pageMargin * 2,
    padding: 26,
    paddingTop: 30,
    backgroundColor:'white',
    borderRadius:4
  },

  imageStyle: {
    width: 211,
    height: 135,
    position: 'absolute',
    right:pageMargin,
    top: screenHeight/2 -235
  },

  headerStyle: {
    fontFamily: 'Blanch',
    fontSize: 48,
    color: '#82CCBE',
    textAlign: 'center',
    paddingBottom:5
  },

  bodyStyle: {
    fontFamily: 'Open Sans',
    fontSize: 14,
    color: '#82CCBE',
    textAlign: 'center',
  },

  buttonTextStyle: {
    fontFamily: 'Open Sans',
    fontWeight: '900',
    fontSize: 14,
    color: '#A4A4A3',
  },

  buttonContainerStyle: {
    flex: 1,
    height: 64,
    width: screenWidth - pageMargin * 2,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0
  },

  buttonStyle: {
    flex: 1,
    backgroundColor: '#F3F3F3',
    justifyContent: 'center',
    alignItems: 'center',
  },

  actionButton: {
    height: 64,
    width: screenWidth - pageMargin * 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#82CCBE',
    position: 'absolute',
    bottom: 0
  },

  closeStyle: {
    width: 48,
    height: 48,
    position: 'absolute',
    bottom: -108,
    alignSelf: 'center'
  }
});
