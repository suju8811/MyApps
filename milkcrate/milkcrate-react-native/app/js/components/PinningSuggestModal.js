import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  Image,
  Text,
  Platform,
  Dimensions,
  TouchableWithoutFeedback,
  AsyncStorage
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import * as commonColors from '../styles/commonColors';
import * as commonStyles from '../styles/commonStyles';
import { screenWidth, screenHeight } from '../styles/commonStyles';
import Cache from '../components/Cache'
import bendService from '../bend/bendService'

const close_button = require('../../assets/imgs/close_button.png');
const pageMargin = 24;
const pinLocator = require('../../assets/imgs/modal-imgs/pin_locator.png');
const imageMask = require('../../assets/imgs/modal-imgs/image-mask.png');

export default class PinningSuggestModal extends Component {

  constructor(props) {
    super(props);
  }

  onGotIt() {
    const { onPressGotit } = this.props;
    AsyncStorage.setItem('@milkcrate:gotPinSuggestion@' + bendService.getActiveUser()._id, 'Yes')
    Cache.gotPinSuggestion = 'Yes'
    onPressGotit();
  }

  render() {
    const { containerStyle,
            containerStyleSmall,
            imageStyle,
            imageStyleSmall,
            headerStyle,
            bodyStyle,
            buttonContainerStyle,
            buttonStyle,
            buttonTextStyle,
            actionButton,
      imageMaskStyle
          } = styles;
    const { showMarker } = this.props;

    return (
      <View style={{flex:1, alignItems:'center'}}>
        <View style={screenWidth > 320? containerStyle : containerStyleSmall}>
          <Text style={headerStyle}>
            GREAT!
          </Text>

          <Text style={bodyStyle}>
            Tap on the pin icon to pin this action to your home screen for easy access next time
          </Text>

          <TouchableWithoutFeedback onPress={this.onGotIt.bind(this)}>
              <View style={styles.actionFullButton}>
                  <Text style={buttonTextStyle}>Got it</Text>
              </View>
          </TouchableWithoutFeedback>
        </View>
        <Image source={pinLocator} style={imageStyle}/>
        {showMarker&&<Image source={imageMask} style={imageMaskStyle}/>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    width: screenWidth - pageMargin * 2,
    height: 240,
    padding: 26,
    paddingTop: 60,
    marginTop:155,
    backgroundColor:'white',
    borderRadius:4
  },

  containerStyleSmall: {
    width: screenWidth - pageMargin * 2,
    height: 240,
    padding: 26,
    paddingTop: 60,
    marginTop:155,
    backgroundColor:'white',
    borderRadius:4
  },

  imageStyle: {
    width: 92,
    height: 130,
    position: 'absolute',
    right: 20,
    top: 90
  },

  imageMaskStyle: {
    width: 34,
    height: 34,
    position: 'absolute',
    right: 13,
    top: (Platform.OS === 'ios'?23:3)
  },

  headerStyle: {
    fontFamily: 'Blanch',
    fontSize: 48,
    color: '#82CCBE',
    textAlign: 'center',
    lineHeight:40,
    letterSpacing:0.53,
    marginBottom:5
  },

  bodyStyle: {
    fontFamily: 'Open Sans',
    fontSize: 14,
    color: '#82CCBE',
    textAlign: 'center',
    marginBottom: 20
  },

  buttonTextStyle: {
    fontFamily: 'Open Sans',
    fontWeight: '900',
    fontSize: 14,
    color: 'white',
  },

  buttonContainerStyle: {
    flex: 1,
    height: 64,
    width: screenWidth - pageMargin * 2,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    left:0
  },

  buttonStyle: {
    flex: 1,
    backgroundColor: '#F3F3F3',
    justifyContent: 'center',
    alignItems: 'center',
  },

  actionFullButton: {
    height: 64,
    width: screenWidth - pageMargin * 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#82CCBE',
    position: 'absolute',
    bottom: 0,
    left:0,
    borderBottomRightRadius:4,
    borderBottomLeftRadius:4,
  }
});
