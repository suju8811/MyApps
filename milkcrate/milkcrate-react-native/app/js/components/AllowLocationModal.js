import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  Image,
  Text,
  Platform,
  Dimensions,
  TouchableWithoutFeedback
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import * as commonColors from '../styles/commonColors';
import * as commonStyles from '../styles/commonStyles';
import { screenWidth, screenHeight } from '../styles/commonStyles';
const close_button = require('../../assets/imgs/close_button.png');
const pageMargin = 8;
const image = require('../../assets/imgs/modal-imgs/locationRequest.png');

export default class AllowLocationModal extends Component {

  constructor(props) {
    super(props);
  }

  onClose() {
    Actions.pop();
  }

  render() {
    const { containerStyle,
            containerStyleSmall,
            imageStyle,
            imageStyleSmall,
            headerStyle,
            bodyStyle,
            buttonContainerStyle,
            buttonStyle,
            buttonTextStyle,
            closeStyle,
          } = styles;
    const { onPressYes, onPressNo } = this.props;

    return (
      <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
        <View style={screenWidth > 320? containerStyle : containerStyleSmall}>

          <Text style={headerStyle}>
            Please Allow Us to Access Your Location
          </Text>

          <Text style={bodyStyle}>
            Enabling location access will allow you to discover and check into places nearby where you might be able to earn points.
          </Text>

          <View style={buttonContainerStyle}>
            <TouchableWithoutFeedback onPress={onPressNo}>
              <View style={buttonStyle}>
                <Text style={buttonTextStyle}>Not Now</Text>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback onPress={onPressYes}>
              <View style={[buttonStyle,{backgroundColor: '#82CCBE'}]}>
                <Text style={[buttonTextStyle,{color: 'white'}]}>Allow</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <TouchableWithoutFeedback style={closeStyle} onPress={onPressNo}>
            <Image source={close_button} style={closeStyle}/>
          </TouchableWithoutFeedback>
        </View>
        <Image source={image} style={screenWidth > 320? imageStyle : imageStyleSmall}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    width: 359,
    height: 355,
    padding: 26,
    paddingTop: 150,
    alignItems: 'center',
    borderRadius: 4,
    backgroundColor:'white'
  },

  containerStyleSmall: {
    width: screenWidth - pageMargin * 2,
    padding: 26,
    paddingTop: 150,
    alignItems: 'center',
    borderRadius: 4,
    backgroundColor:'white'
  },

  imageStyle: {
    width: 300,
    height: 221,
    position: 'absolute',
    alignSelf: 'center',
    top: screenHeight/2 - 268
  },

  imageStyleSmall: {
    width: screenWidth * 0.8,
    height: screenWidth * 0.8 * 221 / 300,
    position: 'absolute',
    alignSelf: 'center',
    top: screenHeight/2 - 268
  },

  headerStyle: {
    fontFamily: 'Open Sans',
    fontWeight: '900',
    fontSize: 20,
    color: '#5E8AA3',
    textAlign: 'center',
    marginBottom: 12,
    width: 250
  },

  bodyStyle: {
    fontFamily: 'Open Sans',
    fontSize: 14,
    color: '#696969',
    textAlign: 'center',
    marginBottom: 20
  },

  buttonTextStyle: {
    fontFamily: 'Open Sans',
    fontWeight: '900',
    fontSize: 14,
    color: '#A4A4A3',
  },

  buttonContainerStyle: {
    flex: 1,
    height: 64,
    width: 359,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0
  },

  buttonStyle: {
    flex: 1,
    backgroundColor: '#F3F3F3',
    justifyContent: 'center',
    alignItems: 'center',
  },

  closeStyle: {
    width: 48,
    height: 48,
    position: 'absolute',
    bottom: -108,
    alignSelf: 'center'
  }
});
