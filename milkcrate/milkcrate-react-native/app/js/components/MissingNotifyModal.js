import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  Image,
  Text,
  Platform,
  Dimensions,
  TouchableWithoutFeedback
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import * as commonColors from '../styles/commonColors';
import * as commonStyles from '../styles/commonStyles';
import { screenWidth, screenHeight } from '../styles/commonStyles';
const close_button = require('../../assets/imgs/close_button.png');
const pageMargin = 8;
const notifDisabled = require('../../assets/imgs/modal-imgs/notifDisabled.png');

export default class MissingNotifyModal extends Component {

  constructor(props) {
    super(props);
  }

  onClose() {
    Actions.pop();
  }

  render() {
    const { containerStyle,
            containerStyleSmall,
            imageStyle,
            imageStyleSmall,
            headerStyle,
            bodyStyle,
            buttonContainerStyle,
            buttonStyle,
            buttonTextStyle,
            closeStyle,
            actionButton
          } = styles;
    const { onPressYes, onPressNo } = this.props;

    return (
      <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
        <View style={screenWidth > 320? containerStyle : containerStyleSmall}>

          <Text style={headerStyle}>
            You’re Missing Out!
          </Text>

          <Text style={bodyStyle}>
            Enabling Push Notification will allow us to remind you of upcoming challenges, events and keep you up to date.
          </Text>

          <TouchableWithoutFeedback onPress={onPressYes}>
              <View style={styles.actionButton}>
                  <Text style={[buttonTextStyle, {fontWeight: '200'}]}>To turn on notifications:</Text>
                  <Text style={buttonTextStyle}>Settings > Notifications</Text>
              </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback style={closeStyle} onPress={onPressNo}>
            <Image source={close_button} style={closeStyle}/>
          </TouchableWithoutFeedback>
        </View>
        <Image source={notifDisabled} style={screenWidth > 320? imageStyle : imageStyleSmall}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    width: 359,
    height: 355,
    padding: 26,
    paddingTop: 168,
    borderRadius: 4,
    backgroundColor:'white'
  },

  containerStyleSmall: {
    width: screenWidth - pageMargin * 2,
    padding: 26,
    paddingTop: 168,
    borderRadius: 4,
    backgroundColor:'white'
  },

  imageStyle: {
    width: 300,
    height: 233,
    position: 'absolute',
    alignSelf: 'center',
    top: screenHeight/2 - 268
  },

  imageStyleSmall: {
    width: screenWidth * 0.8,
    height: screenWidth * 0.8 * 233 / 300,
    position: 'absolute',
    alignSelf: 'center',
    top: screenHeight/2 - 268
  },

  headerStyle: {
    fontFamily: 'Open Sans',
    fontWeight: '900',
    fontSize: 20,
    color: '#5E8AA3',
    textAlign: 'center',
    marginBottom: 12
  },

  bodyStyle: {
    fontFamily: 'Open Sans',
    fontSize: 14,
    color: '#696969',
    textAlign: 'center',
    marginBottom: 20
  },

  buttonTextStyle: {
    fontFamily: 'Open Sans',
    fontWeight: '900',
    fontSize: 14,
    color: 'white',
  },

  buttonContainerStyle: {
    flex: 1,
    height: 64,
    width: 359,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0
  },

  buttonStyle: {
    flex: 1,
    backgroundColor: '#F3F3F3',
    justifyContent: 'center',
    alignItems: 'center',
  },

  actionButton: {
    height: 64,
    width: 359,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#82CCBE',
    position: 'absolute',
    bottom: 0
  },

  closeStyle: {
    width: 48,
    height: 48,
    position: 'absolute',
    bottom: -108,
    alignSelf: 'center'
  }
});
