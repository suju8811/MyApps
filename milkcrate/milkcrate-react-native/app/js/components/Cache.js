import * as _ from 'underscore'
class Cache {
  constructor() {
    this.categories = null
    this.screens = []
    this.community = null
    this.cacheMap = {}
    this.searchHistory = []
    Cache.gotPinSuggestion = null
    this.startedGeoFencing = {
      home:false,
      work:false
    }

    this.userAddress = {
      homeAddress:null,
      workAddress:null,
    }
  }

  setCategories(categories) {
    this.categories = categories
  }
  
  setCommunity(community) {
    this.community = community
  }

  setMapData(key, val) {
    this.cacheMap[key] = val
  }

  getMapData(key) {
    return this.cacheMap[key]
  }

  removeMapData(key) {
    if(this.cacheMap[key])
      delete this.cacheMap[key]
  }

  resetMap() {
    this.cacheMap = {}
  }

  getScreen(screenName) {
    return _.find(this.screens, (o)=>{
      return o.name == screenName
    })
  }
}

export default (new Cache())
