import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  Image,
  Text,
  Platform,
  Dimensions,
  TouchableWithoutFeedback,
  Linking
} from 'react-native';

import UtilService from '../components/util'

import { Actions } from 'react-native-router-flux';
import * as commonColors from '../styles/commonColors';
import * as commonStyles from '../styles/commonStyles';
import { screenWidth, screenHeight } from '../styles/commonStyles';
const close_button = require('../../assets/imgs/close_button.png');
const pageMargin = 24;
const certificated = require('../../assets/imgs/modal-imgs/certificated.png');

export default class CertifiedModal extends Component {

  constructor(props) {
    super(props);
  }

  onClose() {
    Actions.pop();
  }

  onGoLink() {
    const { certification, onPressOK } = this.props;
    onPressOK();

    var url = UtilService.fixUrl(certification.url);
    if(url) {
      Linking.canOpenURL(url).then(supported => {
        if (supported) {
          Linking.openURL(url);
        }
      }).catch((error)=>{
        //console.log("URL open error");
      });
    }
  }

  render() {
    const { containerStyle,
            containerStyleSmall,
            imageStyle,
            imageStyleSmall,
            headerStyle,
            bodyStyle,
            buttonContainerStyle,
            buttonStyle,
      buttonFullStyle,
            buttonTextStyle,
            closeStyle,
            actionButton
          } = styles;
    const { certification, onPressOK } = this.props;
    let url = certification.url
    return (
      <View style={screenWidth > 320? containerStyle : containerStyleSmall}>
        <Image source={certificated} style={imageStyle}/>

        <Text style={headerStyle}>
          {certification.name}
        </Text>

        <Text style={bodyStyle}>
          {certification.description}
        </Text>

        <View style={buttonContainerStyle}>
          {url&&<TouchableWithoutFeedback onPress={this.onGoLink.bind(this)}>
            <View style={buttonStyle}>
              <Text style={buttonTextStyle}>Visit Website</Text>
            </View>
          </TouchableWithoutFeedback>}

          <TouchableWithoutFeedback onPress={onPressOK}>
            <View style={[buttonStyle,{backgroundColor: '#82CCBE'}]}>
              <Text style={[buttonTextStyle,{color: 'white'}]}>OK</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    width: screenWidth - pageMargin * 2,
    height: 240,
    padding: 26,
    paddingTop: 40,
  },

  containerStyleSmall: {
    width: screenWidth - pageMargin * 2,
    padding: 26,
    paddingTop: 40,
  },

  imageStyle: {
    width: 127,
    height: 127,
    position: 'absolute',
    left:0,
    top: -88
  },

  headerStyle: {
    fontFamily: 'Blanch',
    fontWeight: '500',
    fontSize: 48,
    color: '#82CCBE',
    textAlign: 'center',
  },

  bodyStyle: {
    fontFamily: 'Open Sans',
    fontSize: 14,
    color: '#82CCBE',
    textAlign: 'center',
    marginBottom: 20
  },

  buttonTextStyle: {
    fontFamily: 'Open Sans',
    fontWeight: '900',
    fontSize: 14,
    color: '#A4A4A3',
  },

  buttonContainerStyle: {
    flex: 1,
    height: 64,
    width: screenWidth - pageMargin * 2,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0
  },

  buttonStyle: {
    flex: 1,
    backgroundColor: '#F3F3F3',
    justifyContent: 'center',
    alignItems: 'center',
  },

  actionButton: {
    height: 64,
    width: screenWidth - pageMargin * 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#82CCBE',
    position: 'absolute',
    bottom: 0
  },

  closeStyle: {
    width: 48,
    height: 48,
    position: 'absolute',
    bottom: -108,
    alignSelf: 'center'
  }
});
