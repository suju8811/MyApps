import {app} from '../../config'
console.log('app-module', app)
let appModule = app||'milkcrate'

switch(appModule) {
  case 'milkcrate' :
    module.exports = require('./apps/milkcrate-commonColors');break;
  case 'stamp' :
    module.exports = require('./apps/stamp-commonColors');break;
  case 'maxercise' :
    module.exports = require('./apps/maxercise-commonColors');break;
  case 'marlborough' :
    module.exports = require('./apps/marlborough-commonColors');break;
  case 'wharton' :
    module.exports = require('./apps/wharton-commonColors');break;
  case 'sch' :
    module.exports = require('./apps/sch-commonColors');break;
  case 'bodhi' :
    module.exports = require('./apps/bodhi-commonColors');break;
  default :
    module.exports = require('./apps/milkcrate-commonColors');
}