import React, { Component, PropTypes } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
  TouchableHighlight,
  Linking
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import Point from '../../components/Point';
import * as commonStyles from '../../styles/commonStyles';
import * as commonColors from '../../styles/commonColors';
import bendService from '../../bend/bendService'

const entryBorderRadius = 5;

const animal1 = require('../../../assets/imgs/challenges/animals1.png');

Text.defaultProps.allowFontScaling=false


export default class ChallengeCarousel extends Component {

  static propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string,
    points: PropTypes.number,
    onOpenWebsite: PropTypes.func,
  };

  static defaultProps = {
    onOpenWebsite: null,
  }


  goChallengeActivityDetail() {
    var challenge = this.props.rawData;
    if (challenge.type == 'business') {
      Actions.BusinessesDetail({ business: challenge.activity });
    } else if(challenge.type == 'action') {
      Actions.ActionDetail({ action: challenge.activity });
    } else if(challenge.type == 'event') {
      Actions.EventDetail({ event: challenge.activity });
    } else if(challenge.type == 'volunteer_opportunity') {
      Actions.VolunteerDetail({ volunteer: challenge.activity });
    } else if(challenge.type == 'website') {
      var url = challenge.url
      Linking.canOpenURL(url).then((supported) => {
        if (supported) {
          Linking.openURL(url);
          bendService.captureActivityForWebsite(challenge._id, challenge.points, (error, result) => {
            if (this.props.onOpenWebsite !== null) {
              this.props.onOpenWebsite(result.activity);
            }
          });
        } else {
          console.log("captureActivityForWebsite Error : " + url);
        }
      });
    }
  }

  render () {
    const {
      title,
      subtitle,
      icon,
      points,
      image,
      rawData
    } = this.props;
    var textLines = 1
    if(rawData.type == 'website' && rawData.headline) {
      //expect one letter width is about 8
      var onelineSize = (commonStyles.carouselItemWidth - 60) / 9;
      textLines = Math.ceil(rawData.headline.length / onelineSize)
    }
    return (
      <TouchableHighlight
        activeOpacity={ 0.5 }
        underlayColor={ '#fff' }
        style={ styles.slideInnerContainer }
        onPress={ () => { this.goChallengeActivityDetail() }}
      >
        <View style={ rawData.type=='website'?styles.contentContainerWithoutBorder:styles.contentContainer }>
          {/*<View style={styles.bannerBorderStyle}/>*/}
          {image && <Image source={image} style={rawData.type=='website'?styles.fullImageStyle:styles.fullImageStyle}/>}
          {/*{icon&&<Image style={ styles.icon } source={ icon } />}*/}

          {rawData.type!='website'&&<View style={ styles.topContainer }>
            <Text numberOfLines={2} style={ styles.textTitle }>{ title }</Text>
          </View>}
          {rawData.type!='website'&&<View style={{paddingHorizontal:10}}>
            <View style={styles.centerContainer}>
              <Text numberOfLines={2} style={ styles.description }>{ subtitle } </Text>
            </View>
          </View>}
          {rawData.type=='website'&&rawData.headline&&<View style={styles.centerContainerForWeb}>
            <Text style={[styles.textHeadline, {color:(rawData.headlineColor||'#ffffff')}]}
            >
              { rawData.headline }
            </Text>
          </View>}
          {rawData.type!='website'&&<View style={ styles.bottomContainer }>
            { points > 0 && <Point point={ points }/> }
          </View>}
        </View>
      </TouchableHighlight>
    );
  }
}


const styles = StyleSheet.create({
  container: {

  },
  slideInnerContainer: {
    width: commonStyles.carouselItemWidth,
    height: commonStyles.carouselHeight,
    paddingHorizontal: commonStyles.carouselItemHorizontalPadding,
    marginVertical: 10,
  },
  contentContainer: {
    flex: 1,
    borderWidth: 1,
    borderColor: commonColors.line,
    borderStyle: 'solid',
    borderRadius: entryBorderRadius,
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  contentContainerWithoutBorder: {
    flex: 1,
    borderRadius: entryBorderRadius,
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  topContainer: {
    borderTopLeftRadius: entryBorderRadius,
    borderTopRightRadius: entryBorderRadius,
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingTop:5,
    height:30
  },
  centerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  centerContainerForWeb: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
    paddingVertical: 30,
    flex:1,
  },
  bottomContainer: {
    borderBottomLeftRadius: entryBorderRadius,
    borderBottomRightRadius: entryBorderRadius,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingHorizontal: 15,
    height:40,
    paddingBottom: 5,
  },
  textTitle: {
    color: commonColors.title,
    fontFamily: 'Blanch',
    fontSize: 28,
    textAlign: 'center',
    backgroundColor:'transparent'
  },
  textHeadline: {
    fontFamily: 'Blanch',
    fontSize: 36,
    textAlign: 'center',
    flexWrap: 'wrap',
    backgroundColor:'transparent',
    lineHeight:39,
    textShadowColor: 'rgba(0, 0, 0, 1)',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 5
  },
  icon: {
    width: 44,
    height: 44,
    top: 0,
    right: 0,
    position: 'absolute',
  },
  description: {
    color: commonColors.title,
    fontFamily: 'Open Sans',
    fontSize: 14,
    paddingHorizontal: 45,
    textAlign:'center',
    backgroundColor:'transparent',
  },
  bannerStyle: {
    width: 200,
    height: 155,
    left: 0,
    bottom: 0,
    position: 'absolute',
    zIndex: -1,
  },
  fullImageStyle:{
    width: '100%',
    height: '100%',
    left: 0,
    bottom: 0,
    position: 'absolute',
    resizeMode:'stretch',
    zIndex: -1,
    borderRadius:entryBorderRadius
  },
  bannerBorderStyle: {
    width: commonStyles.carouselItemWidth-10,
    height: 14,
    backgroundColor: '#8DD1C5',
    bottom: 0,
    position: 'absolute',
    borderBottomLeftRadius: entryBorderRadius,
    borderBottomRightRadius: entryBorderRadius,
  }
});
