import React, { Component, PropTypes } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';

import { screenWidth } from '../../styles/commonStyles';
import * as commonColors from '../../styles/commonColors';
import { Actions } from 'react-native-router-flux'
import EntypoIcon from 'react-native-vector-icons/Entypo';
import UtilService from '../../components/util'

export default class SimpleProfile extends Component {

  static propTypes = {
  }

  static defaultProps = {
  }

  constructor(props) {
    super(props);
  }

  render() {
    let {currentUser, avatar, community} = this.props
    return (
      <View style={styles.container}>
        <View style={styles.avatar}>
          {avatar&&<Image source={avatar} style={styles.avatarImage}></Image>}
        </View>
        <View style={styles.username}>
          <Text style={styles.usernameText}>
            {currentUser.name||currentUser.username}
          </Text>
        </View>
        <View style={styles.rank}>
          {currentUser.sprintRank&&<Text style={styles.sprintRank}>
            {currentUser.sprintRank}
          </Text>}
          {currentUser.sprintRank&&<Text style={styles.sprintRankPos}>
            {UtilService.getPositionStringPostfix(currentUser.sprintRank)}
          </Text>}
          {currentUser.sprintRank&&<Text style={styles.text1}>
            out of
          </Text>}
          {currentUser.sprintRank&&<Text style={styles.userCount}>
            {community.userCount}
          </Text>}
        </View>
        <TouchableOpacity style={{flex:1}} onPress={()=>{Actions.CommunityPoints();}}>
          <EntypoIcon style={ styles.rightIcon } name="chevron-thin-right" size={ 16 } color={ commonColors.grayMoreText }/>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection:'row', paddingHorizontal:14, paddingVertical:10, backgroundColor:'white', alignItems:'center'
  },
  avatar: {
    flex:2, alignItems:'center', justifyContent:'center'
  },
  avatarImage: {
    width:52,height:52,resizeMode:'contain', borderRadius:26
  },
  username:{
    flex:4, height:52, alignItems:'flex-start', justifyContent:'center', paddingHorizontal:7
  },
  usernameText:{
    fontSize:20,fontFamily:'OpenSans-Semibold',color:'#5E8AA3', flexWrap:'wrap', textAlign:'left', paddingLeft:10
  },
  rank:{
    flex:5, flexDirection:'row', height:42, justifyContent:'flex-end', paddingTop:5, paddingRight:10
  },
  sprintRank:{
    fontSize:36, lineHeight:40, fontFamily:'Open Sans',color:'#82CCBE',fontWeight:'bold'
  },
  sprintRankPos:{
    fontSize:20, lineHeight:40, fontFamily:'Open Sans',color:'#82CCBE',fontWeight:'bold'
  },
  text1: {
    fontSize:14, lineHeight:38, fontFamily:'Open Sans',color:'#9B9B9B', paddingHorizontal:5
  },
  userCount: {
    fontSize:14, lineHeight:38, fontFamily:'Open Sans',color:'#9B9B9B',fontWeight:'bold'
  },

  rightIcon: {
    paddingTop: 2.5,
    alignSelf: 'flex-end',
  },
});
