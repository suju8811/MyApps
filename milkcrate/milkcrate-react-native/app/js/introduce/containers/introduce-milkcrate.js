'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  StatusBar,
  Alert,
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import AppIntro from '../components/AppIntro';
import Signup from '../../auth/containers/signup';
import Orientation from 'react-native-orientation';

import * as commonStyles from '../../styles/commonStyles';
import * as commonColors from '../../styles/commonColors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    width: commonStyles.screenWidth,
    height: commonStyles.screenHeight,
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  paddingTop: {
    flex: 5,
  },
  paddingBottom: {
    flex: 1,
  },
  contentWrap: {
    flex : 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: commonColors.title,
    fontFamily: 'Blanch',
    fontSize: 48 * commonStyles.scaleScreen(),
  },
  description: {
    color: commonColors.title,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 13 * commonStyles.scaleScreen(),
    paddingVertical: 2,
    textAlign:'center',
    lineHeight:24,
    marginLeft: 10,
    marginRight: 10
  },
  description_white: {
    color: '#ffffff',
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14 * commonStyles.scaleScreen(),
    paddingVertical: 2,
    textAlign:'center',
    lineHeight:24
  },
});

const customStyles = {
  dotStyle: {
    backgroundColor: 'rgba(255,255,255,.3)',
    width: 5,
    height: 5,
    borderRadius: 7,
    marginLeft: 7,
    marginRight: 7,
    marginTop: 7,
    marginBottom: 7,
  },
  nextButtonText: {
    fontSize: 14,
    fontWeight: 'normal',
    fontFamily: 'Open Sans',
  },
  controllText: {
    color: '#fff',
    fontSize: 14,
    fontWeight: 'normal',
    fontFamily: 'Open Sans',
  },
};

const customStyles2 = {
  rightTextColor: '#000',
  leftTextColor: '#000',
  dotStyle: {
    backgroundColor: 'rgba(255,255,255,.3)',
    width: 5,
    height: 5,
    borderRadius: 7,
    marginLeft: 7,
    marginRight: 7,
    marginTop: 7,
    marginBottom: 7,
  },
  nextButtonText: {
    fontSize: 14,
    fontWeight: 'normal',
    fontFamily: 'Open Sans',
  },
  controllText: {
    color: '#fff',
    fontSize: 14,
    fontWeight: 'normal',
    fontFamily: 'Open Sans',
  },
};

const milkcrate_screens = [
  {
    background:require('../../../assets/imgs/introduce/background_introduce1.png'),
    subtitle:'Let your sustainable journey begin –\nand get ready to earn rewards\nfor making an impact!',
    subtitleStyle:styles.description
  },
  {
    background:require('../../../assets/imgs/introduce/background_introduce2.png'),
    title:"DISCOVER BUSINESSES",
    subtitle:"Earn points for checking in to \nhundreds of sustainable businesses\nin your neighborhood.",
    subtitleStyle:styles.description
  },
  {
    background:require('../../../assets/imgs/introduce/background_introduce3.png'),
    title:"TAKE ACTION",
    subtitle:"See your points increase as you take part\nin weekly challenges and for completing\nactivities throughout the week.",
    subtitleStyle:styles.description
  },
  {
    background:require('../../../assets/imgs/introduce/background_introduce4.png'),
    title:"GET INVOLVED",
    subtitle:"Explore amazing local volunteer\n opportunities and green-themed events.",
    subtitleStyle:styles.description
  },
  {
    background:require('../../../assets/imgs/introduce/background_introduce5.png'),
    title:"CELEBRATE WITH REWARDS",
    subtitle:"Use your earned points from all of\n your activities and challenges on\n great local rewards.",
    subtitleStyle:styles.description
  },
  {
    background:require('../../../assets/imgs/introduce/background_introduce6.png'),
    title:"VIEW YOUR PROGRESS",
    subtitle:"See your individual impact and personal\nachievements as well as your own community’s\noverall activity for comparison and competition.",
    subtitleStyle:styles.description
  },
]

export default class Introduce extends Component {
  constructor(props) {
    super(props);
    this.state= {
      slideIndex:0
    }
    StatusBar.setHidden(true);
  }

  componentDidMount() {
    this.hasMounted = true
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  onGoLogin() {
    Actions.Login();
  }

  render() {
    let {slideIndex} = this.state

    var renderScreens = []
    var screens = milkcrate_screens;


    screens.map((screen, index)=>{
      renderScreens.push(
        <View style={ styles.slide } key={'screen-' + index}>
          <Image source={ screen.background } style={ styles.background } resizeMode="cover">
            <View style={ styles.paddingTop }></View>
            <View style={ styles.contentWrap }>
              {screen.title&&<Text style={ styles.title }>{screen.title}</Text>}
              <Text style={ screen.subtitleStyle }>
                {screen.subtitle}
              </Text>
            </View>
            <View style={ styles.paddingBottom }></View>
          </Image>
        </View>
      )
    })

    return (
      <View style={ styles.container }>
        <AppIntro
          skipBtnLabel="Log In"
          nextBtnLabel="Next"
          doneBtnLabel=""
          rightTextColor={ commonColors.title }
          rightTextColor2={ commonColors.title }
          leftTextColor={ commonColors.title }
          leftTextColor2={ commonColors.title }
          dotColor= { commonColors.line }
          activeDotColor={ commonColors.title }
          customStyles={ customStyles }
          onSkipBtnClick={ (position) => this.onGoLogin() }
          onDoneBtnClick={ () => this.onGoLogin() }
        >
          {renderScreens[0]}
          {renderScreens[1]}
          {renderScreens[2]}
          {renderScreens[3]}
          {renderScreens[4]}
          {renderScreens[5]}
          <Signup/>
        </AppIntro>
      </View>
    );
  }
}
