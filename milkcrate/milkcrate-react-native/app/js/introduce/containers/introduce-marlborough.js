'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  StatusBar,
  Alert,
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import AppIntro from '../components/AppIntro';
import Signup from '../../auth/containers/signup';
import Orientation from 'react-native-orientation';

import * as commonStyles from '../../styles/commonStyles';
import * as commonColors from '../../styles/commonColors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    width: commonStyles.screenWidth,
    height: commonStyles.screenHeight,
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  paddingTop: {
    flex: 5,
  },
  paddingBottom: {
    flex: 1,
  },
  contentWrap: {
    flex : 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: commonColors.title,
    fontFamily: 'Blanch',
    fontSize: 48 * commonStyles.scaleScreen(),
  },
  description: {
    color: commonColors.title,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 13 * commonStyles.scaleScreen(),
    paddingVertical: 2,
    textAlign:'center',
    lineHeight:24,
    marginLeft: 10,
    marginRight: 10
  },
  description_white: {
    color: '#ffffff',
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14 * commonStyles.scaleScreen(),
    paddingVertical: 2,
    textAlign:'center',
    lineHeight:24
  },
});

const customStyles = {
  dotStyle: {
    backgroundColor: 'rgba(255,255,255,.3)',
    width: 5,
    height: 5,
    borderRadius: 7,
    marginLeft: 7,
    marginRight: 7,
    marginTop: 7,
    marginBottom: 7,
  },
  nextButtonText: {
    fontSize: 14,
    fontWeight: 'normal',
    fontFamily: 'Open Sans',
  },
  controllText: {
    color: '#fff',
    fontSize: 14,
    fontWeight: 'normal',
    fontFamily: 'Open Sans',
  },
};

const customStyles2 = {
  rightTextColor: '#000',
  leftTextColor: '#000',
  dotStyle: {
    backgroundColor: 'rgba(255,255,255,.3)',
    width: 5,
    height: 5,
    borderRadius: 7,
    marginLeft: 7,
    marginRight: 7,
    marginTop: 7,
    marginBottom: 7,
  },
  nextButtonText: {
    fontSize: 14,
    fontWeight: 'normal',
    fontFamily: 'Open Sans',
  },
  controllText: {
    color: '#fff',
    fontSize: 14,
    fontWeight: 'normal',
    fontFamily: 'Open Sans',
  },
};

const marlborough_screens = [
  {
    background:require('../../../assets/imgs/introduce_marlborough/01-compassion.png'),
    title: "COMPASSION",
    subtitle:"Give Back",
    subtitleStyle:styles.description
  },
  {
    background:require('../../../assets/imgs/introduce_marlborough/02-fitness.png'),
    title:"FITNESS",
    subtitle:"Rise + Rally",
    subtitleStyle:styles.description
  },
  {
    background:require('../../../assets/imgs/introduce_marlborough/03-meditation.png'),
    title:"MEDITATION",
    subtitle:"Feed Your Soul",
    subtitleStyle:styles.description
  },
  {
    background:require('../../../assets/imgs/introduce_marlborough/04-rest-recovery.png'),
    title:"REST AND RECOVERY",
    subtitle:"Refresh",
    subtitleStyle:styles.description
  },
  {
    background:require('../../../assets/imgs/introduce_marlborough/05-nutrition.png'),
    title:"NUTRITION",
    subtitle:"Nourish Your Body",
    subtitleStyle:styles.description
  },
]

export default class Introduce extends Component {
  constructor(props) {
    super(props);
    this.state= {
      slideIndex:0
    }
    StatusBar.setHidden(true);
  }

  componentDidMount() {
    this.hasMounted = true
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  onGoLogin() {
    Actions.Login();
  }

  render() {
    let {slideIndex} = this.state

    var renderScreens = []
    var screens = marlborough_screens

    screens.map((screen, index)=>{
      renderScreens.push(
        <View style={ styles.slide } key={'screen-' + index}>
          <Image source={ screen.background } style={ styles.background } resizeMode="cover">
            <View style={ styles.paddingTop }></View>
            <View style={ styles.contentWrap }>
              {screen.title&&<Text style={ styles.title }>{screen.title}</Text>}
              <Text style={ screen.subtitleStyle }>
                {screen.subtitle}
              </Text>
            </View>
            <View style={ styles.paddingBottom }></View>
          </Image>
        </View>
      )
    })

    return (
      <View style={ styles.container }>
        <AppIntro
          skipBtnLabel="Log In"
          nextBtnLabel="Next"
          doneBtnLabel=""
          rightTextColor={ commonColors.title }
          rightTextColor2={ commonColors.title }
          leftTextColor={ commonColors.title }
          leftTextColor2={ commonColors.title }
          dotColor= { commonColors.line }
          activeDotColor={ commonColors.title }
          customStyles={ customStyles }
          onSkipBtnClick={ (position) => this.onGoLogin() }
          onDoneBtnClick={ () => this.onGoLogin() }
        >
          {renderScreens[0]}
          {renderScreens[1]}
          {renderScreens[2]}
          {renderScreens[3]}
          {renderScreens[4]}
          <Signup/>
        </AppIntro>
      </View>
    );
  }
}
