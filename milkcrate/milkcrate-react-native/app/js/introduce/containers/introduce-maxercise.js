'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  StatusBar,
  Alert,
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import AppIntro from '../components/AppIntro';
import Signup from '../../auth/containers/signup';
import Orientation from 'react-native-orientation';

import * as commonStyles from '../../styles/commonStyles';
import * as commonColors from '../../styles/commonColors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    width: commonStyles.screenWidth,
    height: commonStyles.screenHeight,
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  paddingTop: {
    flex: 5,
  },
  paddingBottom: {
    flex: 1,
  },
  contentWrap: {
    flex : 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: commonColors.title,
    fontFamily: 'Blanch',
    fontSize: 48 * commonStyles.scaleScreen(),
  },
  description: {
    color: commonColors.title,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 13 * commonStyles.scaleScreen(),
    paddingVertical: 2,
    textAlign:'center',
    lineHeight:24,
    marginLeft: 10,
    marginRight: 10
  },
  description_white: {
    color: '#ffffff',
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14 * commonStyles.scaleScreen(),
    paddingVertical: 2,
    textAlign:'center',
    lineHeight:24
  },
});

const customStyles = {
  dotStyle: {
    backgroundColor: 'rgba(255,255,255,.3)',
    width: 5,
    height: 5,
    borderRadius: 7,
    marginLeft: 7,
    marginRight: 7,
    marginTop: 7,
    marginBottom: 7,
  },
  nextButtonText: {
    fontSize: 14,
    fontWeight: 'normal',
    fontFamily: 'Open Sans',
  },
  controllText: {
    color: '#fff',
    fontSize: 14,
    fontWeight: 'normal',
    fontFamily: 'Open Sans',
  },
};

const customStyles2 = {
  rightTextColor: '#000',
  leftTextColor: '#000',
  dotStyle: {
    backgroundColor: 'rgba(255,255,255,.3)',
    width: 5,
    height: 5,
    borderRadius: 7,
    marginLeft: 7,
    marginRight: 7,
    marginTop: 7,
    marginBottom: 7,
  },
  nextButtonText: {
    fontSize: 14,
    fontWeight: 'normal',
    fontFamily: 'Open Sans',
  },
  controllText: {
    color: '#fff',
    fontSize: 14,
    fontWeight: 'normal',
    fontFamily: 'Open Sans',
  },
};

const maxercise_screens = [
  {
    background:require('../../../assets/imgs/introduce_maxercise/background_introduce1.png'),
    title: "WELCOME TO MAXERCISE",
    subtitle:"Shake hands, begin when you're ready...",
    subtitleStyle:styles.description
  },
  {
    background:require('../../../assets/imgs/introduce_maxercise/background_introduce2.png'),
    title:"GROW YOUR GAME",
    subtitle:"Review the new moves of the week to improve your technique while you’re off the mat.",
    subtitleStyle:styles.description
  },
  {
    background:require('../../../assets/imgs/introduce_maxercise/background_introduce3.png'),
    title:"TRACK PROGRESS",
    subtitle:"Earn points for checking into the gym, see how often you are coming over time and hit milestones.",
    subtitleStyle:styles.description
  },
  {
    background:require('../../../assets/imgs/introduce_maxercise/background_introduce4.png'),
    title:"REGISTER FOR COMPETITIONS",
    subtitle:"Get pumped for NAGA and other gatherings by seeing who else is going - and earn extra points!",
    subtitleStyle:styles.description
  },
  {
    background:require('../../../assets/imgs/introduce_maxercise/background_introduce5.png'),
    title:"CELEBRATE WITH THE TEAM",
    subtitle:"Don’t miss out on fun social gatherings - and cheer each other on at promotion time!",
    subtitleStyle:styles.description
  },
]

export default class Introduce extends Component {
  constructor(props) {
    super(props);
    this.state= {
      slideIndex:0
    }
    StatusBar.setHidden(true);
  }

  componentDidMount() {
    this.hasMounted = true
    Orientation.lockToPortrait();
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  onGoLogin() {
    Actions.Login();
  }

  render() {
    let {slideIndex} = this.state

    var renderScreens = []
    var screens = maxercise_screens;

    screens.map((screen, index)=>{
      renderScreens.push(
        <View style={ styles.slide } key={'screen-' + index}>
          <Image source={ screen.background } style={ styles.background } resizeMode="cover">
            <View style={ styles.paddingTop }></View>
            <View style={ styles.contentWrap }>
              {screen.title&&<Text style={ styles.title }>{screen.title}</Text>}
              <Text style={ screen.subtitleStyle }>
                {screen.subtitle}
              </Text>
            </View>
            <View style={ styles.paddingBottom }></View>
          </Image>
        </View>
      )
    })

    return (
      <View style={ styles.container }>
        <AppIntro
          skipBtnLabel="Log In"
          nextBtnLabel="Next"
          doneBtnLabel=""
          rightTextColor={ commonColors.title }
          rightTextColor2={ commonColors.title }
          leftTextColor={ commonColors.title }
          leftTextColor2={ commonColors.title }
          dotColor= { commonColors.line }
          activeDotColor={ commonColors.title }
          customStyles={ customStyles }
          onSkipBtnClick={ (position) => this.onGoLogin() }
          onDoneBtnClick={ () => this.onGoLogin() }
        >
          {renderScreens[0]}
          {renderScreens[1]}
          {renderScreens[2]}
          {renderScreens[3]}
          {renderScreens[4]}
          <Signup/>
        </AppIntro>
      </View>
    );
  }
}
