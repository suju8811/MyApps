import {app} from '../../../config'
let appModule = app||'milkcrate'

switch(appModule) {
  case 'milkcrate' :
    module.exports = require('./introduce-milkcrate');break;
  case 'stamp' :
    module.exports = require('./introduce-stamp');break;
  case 'maxercise' :
    module.exports = require('./introduce-maxercise');break;
  case 'marlborough' :
    module.exports = require('./introduce-marlborough');break;
  case 'wharton' :
    module.exports = require('./introduce-wharton');break;
  case 'sch' :
    module.exports = require('./introduce-sch');break;
  case 'bodhi' :
    module.exports = require('./introduce-bodhi');break;
  default :
    module.exports = require('./introduce-milkcrate');
}