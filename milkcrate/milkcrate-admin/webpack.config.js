const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const publicPath = path.resolve(__dirname, 'public')

const isProduction = process.env.NODE_ENV === 'production'

const extractLess = new ExtractTextPlugin({
  filename: '[name].[contenthash].css',
  disable: !isProduction
})

module.exports = {
  entry: {
    main: './app/scripts/app.js'
  },
  output: {
    path: publicPath,
    publicPath: '',
    filename: 'app.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.less$/,
        use: ['css-hot-loader'].concat(extractLess.extract({
          use: [{
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          }, {
            loader: 'less-loader',
            options: {
              sourceMap: true
            }
          }],
          fallback: 'style-loader'
        }))
      },
      {
        test: /\.(png|jpg|gif|woff|woff2|eot|ttf|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192
            }
          }
          // limit => file.size =< 8192 bytes ? DataURI : File
        ]
      }
    ]
  },
  devtool: isProduction ? '' : 'source-map',
  plugins: [
    new HtmlWebpackPlugin({
      template: 'app/index.html'
    }),
    new CopyWebpackPlugin([
      {
        from: 'app/scripts',
        to: 'scripts'
      },
      {
        from: 'app/views',
        to: 'views'
      },
      {
        from: 'app/assets/img',
        to: 'assets/img/'
      },
      {
        from: 'app/assets/plugins',
        to: 'assets/plugins/'
      },
      {
        from: 'bower_components',
        to: 'bower_components'
      }
    ]),
    extractLess
  ],
  devServer: {
    hot: true,
    inline: true
  }
}
