# MilkCrate Admin

Install dependencies

```bash
npm i
```

Start development

```bash
npm run dev
```

Lint source code

```bash
npm run lint
```