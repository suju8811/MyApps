module.exports = {
    'extends': 'standard',
    'globals': {
      'angular': true,
      '$': true,
      '_': true,
      'BEND_DEBUG': true,
      'Bend': true,
      'initInjector': true,
      'async': true,
      'applyChangesOnScope': true,
      'moment': true,
      'google': true,
      'PNotify': true,
      'DocumentTouch': true,
    },
    rules: {
      'eqeqeq': 0,
      'handle-callback-err': 0,
      'no-extend-native': 0
    },
    'env': {
      'browser': true,
      'node': true
    }
}
