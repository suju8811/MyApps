/* globals _: true, async: true, applyChangesOnScope: true */
angular.module('app.controllers')
  .controller('TasksController', ['$scope', '$bend', 'BendAuth', 'CommonUtil',
    function ($scope, $bend, BendAuth, CommonUtil) {
      'use strict'

      $scope.newTaskTitle = ''
      $scope.newTaskLabelText = ''
      $scope.showTasksTab = true
      $scope.showCompletedTab = false
      $scope.showForm = false
      var user = $bend.getActiveUser()
      var communityId = user.communityAdmin ? user.community._id : $scope.communityId

      $scope.tasks = []

      $scope.tasksComplete = []

      $scope.selectedItem = {}

      $scope.options = {}

      $scope.remove = function (scope, item) {
        $bend.DataStore.get('task', item._id).then(function (ret) {
          ret.deleted = true
          $bend.DataStore.update('task', ret).then(function () {
            $scope.tasks.splice(scope.index(), 1)
          }, function (err) {
            console.log(err)
          })
        }, function (err) {
          console.log(err)
        })
      }

      $scope.complete = function (scope, item) {
        var cloneItem = _.clone(item)
        cloneItem.completed = true
        delete cloneItem.$$hashKey
        delete cloneItem.editing
        $bend.DataStore.update('task', cloneItem).then(function (ret) {
          $scope.tasksComplete.push(ret)
          $scope.tasks.splice(scope.index(), 1)
        }, function (err) {
          console.log(err)
        })
      }

      $scope.incomplete = function (item, index) {
        var cloneItem = _.clone(item)
        cloneItem.completed = false
        delete cloneItem.$$hashKey
        delete cloneItem.editing
        $bend.DataStore.update('task', cloneItem).then(function (ret) {
          $scope.tasks.push(ret)
          $scope.tasksComplete.splice(index, 1)
        }, function (err) {
          console.log(err)
        })
      }

      $scope.newItem = function (title, label, color) {
        if (title === '') {
          return
        }

        var currentInstance = this

        var newTask = {
          title: title,
          label: label,
          color: color,
          completed: false,
          community: CommonUtil.makeBendRef(communityId, 'community')
        }
        $bend.DataStore.save('task', newTask).then(function (ret) {
          applyChangesOnScope($scope, function () {
            $scope.tasks.push(ret)
            currentInstance.newTaskTitle = ''
            currentInstance.newTaskLabelText = ''
            currentInstance.showForm = false
          })
        }, function (err) {
          console.log(err)
        })
      }

      $scope.edit = function (item) {
        item.editing = true
        item.titlePrev = item.title
        item.labelPrev = item.label
        item.colorPrev = item.color
      }

      $scope.cancelEdit = function ($event, item) {
        if ($event.keyCode !== 27) {
          return
        }
        item.title = item.titlePrev
        item.label = item.labelPrev
        item.color = item.colorPrev
        item.editing = false
      }

      $scope.cancelAdd = function ($event) {
        if ($event.keyCode !== 27) {
          return
        }
        this.newTaskTitle = ''
        this.newTaskLabelText = ''
        this.showForm = false
      }

      $scope.doneEditing = function (item) {
        var cloneItem = _.clone(item)
        delete cloneItem.$$hashKey
        delete cloneItem.editing

        $bend.DataStore.update('task', cloneItem).then(function () {
          item.editing = false
        }, function (err) {
          console.log(err)
        })
      }

      var q = new $bend.Query()
      q.notEqualTo('deleted', true)
      q.equalTo('community._id', communityId)
      $bend.DataStore.find('task', q).then(function (rets) {
        $scope.tasks = _.filter(rets, function (o) {
          return o.completed !== true
        })
        $scope.tasksComplete = _.filter(rets, function (o) {
          return o.completed === true
        })
      }, function (err) {

      })
    }])
