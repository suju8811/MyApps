angular.module('app.controllers')
  .controller('DashboardController', ['$scope', '$timeout', '$window', '$bend', 'BendAuth', 'BendService', 'CommonUtil', '$rootScope', '$location', 'pinesNotifications', '$modal',
    function ($scope, $timeout, $window, $bend, BendAuth, BendService, CommonUtil, $rootScope, $location, pinesNotifications, $modal) {
      'use strict'
      // var moment = $window.moment
      $scope.data = {
        totalUsers: 0,
        totalActivities: 0,
        totalPoints: 0,
        thisWeekPoints: 0
      }

      $scope.user = BendAuth.getActiveUser()
      $scope.communityId = $scope.user.community._id
      $scope.leaderBoard = []
      $scope.communities = []
      $scope.newUsers = []

      $scope.leaderboardPage = {
        itemsPerPage: 10,
        totalItems: 0,
        currentPage: 1,
        numPages: 0
      }

      $scope.newUsersPage = {
        itemsPerPage: 10,
        totalItems: 0,
        currentPage: 1,
        numPages: 0
      }

      async.parallel([
        function (_c) {
          BendService.getStatisticsData(function (err, ret) {
            if (err) {
              console.log(err)
              _c(null, null)
              return
            }

            console.log('statistics data', ret)
            $scope.data = ret

            _c(null, null)
          })
        }
      ], function (err, rets) {
        $scope.isLoading = false
      })

      $scope.loadLeaderboardPage = function () {
        // if($scope.leaderboardPage.currentPage > $scope.leaderboardPage.numPages) return;

        var q = new $bend.Query()
        q.notEqualTo('deleted', true)
        q.equalTo('enabled', true)
        if ($scope.user.communityAdmin) { q.equalTo('community._id', $scope.communityId) }
        q.ascending('rank')
        q.greaterThan('rank', 0)
        q.limit($scope.leaderboardPage.itemsPerPage)
        q.skip(($scope.leaderboardPage.currentPage - 1) * $scope.leaderboardPage.itemsPerPage)

        $bend.User.find(q, {
          relations: {
            avatar: 'BendFile'
          }
        }).then(function (rets) {
          console.log(rets)
          $scope.leaderBoard = rets
        }, function (err) {
          console.log(err)
        })

        q = new $bend.Query()
        q.notEqualTo('deleted', true)
        q.equalTo('enabled', true)
        q.equalTo('community._id', $scope.communityId)
        q.greaterThan('rank', 0)
        $bend.User.count(q).then(function (count) {
          applyChangesOnScope($scope, function () {
            $scope.leaderboardPage.totalItems = count
            $scope.leaderboardPage.numPages = $scope.leaderboardPage.totalItems / $scope.leaderboardPage.itemsPerPage + 1
          })
        }, function (err) {
          console.log(err)
        })
      }

      $scope.loadCommunities = function () {
        // if($scope.leaderboardPage.currentPage > $scope.leaderboardPage.numPages) return;

        BendService.getCommunityList(function (rets) {
          $scope.communities = rets
        })
      }

      $scope.loadCommunities()

      $scope.loadNewUsersPage = function () {
        // if($scope.leaderboardPage.currentPage > $scope.leaderboardPage.numPages) return;

        var q = new $bend.Query()
        q.notEqualTo('deleted', true)
        q.equalTo('enabled', true)
        if ($scope.user.communityAdmin) { q.equalTo('community._id', $scope.communityId) }
        q.descending('_bmd.createdAt')
        q.limit($scope.newUsersPage.itemsPerPage)
        q.skip(($scope.newUsersPage.currentPage - 1) * $scope.newUsersPage.itemsPerPage)

        $bend.User.find(q, {
          relations: {
            avatar: 'BendFile',
            community: 'community'
          }
        }).then(function (rets) {
          console.log(rets)
          $scope.newUsers = rets
        }, function (err) {
          console.log(err)
        })

        q = new $bend.Query()
        q.notEqualTo('deleted', true)
        q.equalTo('enabled', true)
        q.equalTo('community._id', $scope.communityId)
        $bend.User.count(q).then(function (count) {
          applyChangesOnScope($scope, function () {
            $scope.newUsersPage.totalItems = count
            $scope.newUsersPage.numPages = $scope.newUsersPage.totalItems / $scope.newUsersPage.itemsPerPage + 1
          })
        }, function (err) {
          console.log(err)
        })
      }

      if ($scope.user.communityAdmin) { $scope.loadLeaderboardPage() }

      $scope.loadNewUsersPage()

      $scope.onLeaderBoardPageChange = function () {
        $scope.loadLeaderboardPage()
      }

      $scope.onNewUsersPageChange = function () {
        $scope.loadNewUsersPage()
      }
    }])
