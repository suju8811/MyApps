'use strict'

angular.module('app.controllers')
  .controller('ActivityTabController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout) {
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.activityList = []
      $scope.imageMap = {}
      $scope.collectionView = {
        searchTerm: '',
        itemsPerPage: 20,
        isLoading: true,
        totalItems: 0,
        currentPage: 1,
        numPages: 0
      }

      $scope.user = $bend.getActiveUser()

      $scope.communityList = [{
        name: 'All client',
        _id: null
      }]
      $scope.filter = {
        community: ''
      }

      var type = ''
      var activityId = ''
      if ($location.path().indexOf('businesses') != -1) {
        type = 'business'
      } else if ($location.path().indexOf('actions') != -1) {
        type = 'action'
      } else if ($location.path().indexOf('events') != -1) {
        type = 'event'
      } else if ($location.path().indexOf('volunteerings') != -1) {
        type = 'volunteer_opportunity'
      }

      activityId = $location.path().substr($location.path().lastIndexOf('/') + 1)

      console.log(type, activityId)

      BendService.getCommunityList(function (rets) {
        $scope.communityList = $scope.communityList.concat(rets)
      })

      $scope.loadList = function () {
        var searchTerm = $scope.collectionView.searchTerm
        $scope.isLoading = true

        var q = new $bend.Query()
        if (searchTerm != '') {
          q.matches('summary', searchTerm, {
            ignoreCase: true
          })
        }
        q.notEqualTo('deleted', true)
        q.descending('_bmd.createdAt')
        q.equalTo('type', type)
        q.equalTo('activity._id', activityId)
        q.limit($scope.collectionView.itemsPerPage)
        q.skip(($scope.collectionView.currentPage - 1) * $scope.collectionView.itemsPerPage)
        if ($scope.filter.community instanceof Object && $scope.filter.community._id) {
          q.equalTo('community._id', $scope.filter.community._id)
        }
        $bend.DataStore.find('activity', q, {
          relations: {
            user: 'user',
            community: 'community'
          }
        }).then(function (rets) {
          $scope.activityList = rets
          $scope.isLoading = false

          $scope.imageMap = {}
          var imageIds = []
          _.each(rets, function (o) {
            if (o.user.avatar) {
              if (imageIds.indexOf(o.user.avatar._id) == -1) { imageIds.push(o.user.avatar._id) }
            }
          })

          var query = new $bend.Query()
          query.contains('_id', imageIds)
          $bend.File.find(query).then(function (rets) {
            _.each(rets, function (o) {
              $scope.imageMap[o._id] = o
            })
            _.map($scope.activityList, function (o) {
              if (o.user.avatar) {
                o.user.avatar = $scope.imageMap[o.user.avatar._id]
              }
            })
          })
        }, function (err) {
          console.log(err)
        })

        $bend.DataStore.count('activity', q).then(function (count) {
          applyChangesOnScope($scope, function () {
            $scope.collectionView.totalItems = count
            $scope.collectionView.numPages = $scope.collectionView.totalItems / $scope.collectionView.itemsPerPage + 1
          })
        }, function (err) {
          console.log(err)
        })
      }

      $scope.loadList()

      $scope.onSearch = function () {
        $scope.loadList()
      }

      $scope.onPageChange = function () {
        $scope.loadList()
      }

      $scope.deleteActivity = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Deleting the activity is permanent and can not be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deleteActivity(id, function (ret) {
              if (ret) {
                for (var i = 0; i < $scope.activityList.length; i++) {
                  if ($scope.activityList[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.activityList.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $scope.editActivity = function (activity, $event) {
        $modal.open({
          templateUrl: 'activityDetail.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance, activity) {
            $scope.activity = _.clone(activity)
            /* if(activity.user._id) {
                        BendService.getUser(activity.user._id, function(user){
                            $scope.activity.user = user
                        })
                    } */
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }
          },
          resolve: {
            activity: function () {
              return activity
            }
          }
        })
      }
    }])
