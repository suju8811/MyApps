'use strict'

angular.module('app.controllers')
  .controller('CommunityUserActivityEditController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$http', 'pinesNotifications',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $http, pinesNotifications) {
      // Init.
      $scope.CommonUtil = CommonUtil
      $scope.activityId = null
      $scope.userId = $routeParams.userId
      if ($routeParams.id != 'new') { $scope.activityId = $routeParams.id }
      $scope.isLoading = true
      $scope.communityList = []
      $scope.businessList = []
      $scope.actionList = []
      $scope.eventList = []
      $scope.volunteeringList = []
      $scope.serviceList = []
      $scope.activityList = []
      $scope.TypeList = ['action', 'business', 'event', 'volunteer_opportunity']
      $scope.user = {}
      $scope.formData = {
        communities: []
      }
      $scope.filter = {
        user: '',
        activity: ''
      }
      $scope.username = ''
      $scope.activity = {
        community: ''
      }
      $scope.community = ''
      async.parallel([
        function (callback) {
          BendService.getUser($scope.userId, function (ret) {
            $scope.user = ret
            callback(null, null)
          })
        }
      ], function (err, rets) {
        if (err) {
          console.log(err)
          return
        }
        if ($scope.activityId) {
          BendService.getActivity($scope.activityId, function (ret) {
            $scope.activity = ret

            if ($scope.activity.activity) {
              $scope.activityList = []
              $scope.activityList.push($scope.activity.activity)
              $scope.filter.activity = $scope.activity.activity
            }

            $scope.isLoading = false
          })
        }
      })

      $scope.refreshActivity = function (keyword) {
        if (!$scope.activity.type) return
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        if (keyword != '') { query.matches('name', new RegExp(keyword + '+', 'gi')) }
        query.ascending('name')
        query.limit(30)

        if (BendAuth.getActiveUser().communityAdmin) {
          var q = new Bend.Query()
          q.equalTo('community._id', BendAuth.getActiveUser().community._id)// .or().exists("community", false)
          query.and(q)
        }

        $bend.DataStore.find($scope.activity.type, query).then(function (rets) {
          $scope.activityList = rets
        }, function (err) {
          console.log(err)
        })
      }

      $scope.onChangeActivity = function () {
        if ($scope.filter.activity) {
          $scope.activity.summary = $scope.filter.activity.name
          $scope.activity.points = $scope.filter.activity.points || 0
          $scope.activity.link = $scope.filter.activity.url
        } else {
          $scope.activity.summary = ''
          $scope.activity.points = 0
        }
      }

      $scope.onChangeType = function () {
        $scope.activityList = []
        $scope.filter.activity = ''
      }
      $scope.saveActivityDo = function () {
        if ($scope.filter.activity != '') {
          $scope.activity.activity = CommonUtil.makeBendRef($scope.filter.activity._id, $scope.activity.type)
        } else {
          delete $scope.activity.activity
        }

        $scope.activity.user = CommonUtil.makeBendRef($scope.user._id, 'user')

        $scope.activity.community = CommonUtil.makeBendRef($scope.user.community._id, 'community')

        var activityData = _.clone($scope.activity)

        if ($scope.activityId) {
          delete activityData.$$hashKey
          BendService.updateActivity(activityData, function (ret) {
            $location.path('/communityUsers/' + $scope.userId)
          })
        } else {
          BendService.createActivity(activityData, function (ret) {
            $location.path('/communityUsers/' + $scope.userId)
          })
        }
      }
    }])
