'use strict'
angular.module('app.controllers')
  .controller('CollectionController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal) {
      // Init.
      $scope.collectionList = []
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.communityList = []
      $scope.user = $bend.getActiveUser()

      BendService.getCommunityEnabledList(function (rets) {
        $scope.communityList = rets

        BendService.getCollectionList(function (rets) {
          $scope.collectionList = rets
          $scope.isLoading = false
        })
      })

      $scope.deleteCollection = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Deleting the collection is permanent and can not be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deleteCollection(id, function (ret) {
              if (ret) {
                for (var i = 0; i < $scope.collectionList.length; i++) {
                  if ($scope.collectionList[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.collectionList.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $scope.editCollection = function (collection) {
        $scope.openEditCollection(collection)
      }
      $scope.createCollection = function () {
        var collectionObj = {
          enabled: true
        }
        $scope.openEditCollection(collectionObj)
      }

      $scope.openEditCollection = function (collectionObj) {
        $modal.open({
          templateUrl: 'editCollection.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance, collectionObj, collectionList, communityList, user) {
            $scope.communityList = communityList
            $scope.user = user
            $scope.collection = _.clone(collectionObj)

            setTimeout(function () {
              if ($scope.collection.community) {
                $scope.collection.community = $scope.collection.community._id
              }
            }, 10)

            $scope.CommonUtil = CommonUtil
            $scope.fileProgress = []
            $scope.isUploading = []
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }

            $scope.saveCollectionDo = function () {
              var data = _.clone($scope.collection)
              if (data.coverImage) {
                data.coverImage = CommonUtil.makeBendFile(data.coverImage._id)
              }

              if (data.community && data.community != '') {
                data.community = CommonUtil.makeBendRef(data.community, 'community')
              } else {
                delete data.community
              }

              if (data._id) {
                delete data.$$hashKey
                BendService.updateCollection(data, function (ret) {
                  console.log('updateCollection', ret)
                  var ref = _.find(collectionList, function (o) {
                    return o._id == ret._id
                  })

                  _.extend(ref, $scope.collection)
                  if (ref.community) {
                    ref.community = _.find($scope.communityList, function (o) {
                      return o._id == ref.community
                    })
                  }
                  $modalInstance.dismiss('cancel')
                })
              } else {
                if ($scope.user.communityAdmin) {
                  data.community = CommonUtil.makeBendRef($scope.user.community._id, 'community')
                }
                BendService.createCollection(data, function (ret) {
                  console.log('createCollection', ret)
                  $scope.collection._id = ret._id
                  if ($scope.collection.community) {
                    $scope.collection.community = _.find($scope.communityList, function (o) {
                      return o._id == $scope.collection.community
                    })
                  }
                  collectionList.push($scope.collection)
                  $modalInstance.dismiss('cancel')
                })
              }
            }
            $scope.deleteFile = function (tag, $ev) {
              $ev.stopPropagation()
              $ev.preventDefault()
              applyChangesOnScope($scope, function () {
                delete $scope.collection.coverImage
              })
            }

            $scope.showFileLoading = function (tag, bShow) {
              $scope.isUploading[tag] = bShow
            }

            $scope.selectFileOpen = function (fileId, $ev) {
              if ($ev.target.tagName == 'DIV') {
                setTimeout(function () {
                  $('#' + fileId).click()
                }, 0)
              }
            }

            $scope.onFileUpload = function (files, tag) {
              var file = files[0]
              $scope.fileProgress[tag] = 0
              $scope.showFileLoading(tag, true)

              BendService.upload(file, function (error, uploadedFile) {
                // console.log("uploadedFile", uploadedFile);
                if (error) {
                  $scope.showFileLoading(tag, false)
                  return
                }
                // console.log(uploadedFile);
                BendService.getFile(uploadedFile, function (o) {
                  $scope.collection.coverImage = o
                  $scope.showFileLoading(tag, false)
                  $scope.form.validateForm.$dirty = true
                })
              }, {
                _workflow: 'coverPhoto'
              }, function (total, prog) {
                applyChangesOnScope($scope, function () {
                  $scope.fileProgress[tag] = prog * 100 / total
                })
              })
            }
            $scope.canSubmitValidationForm = function () {
              return $scope.form.validateForm.$valid
            }
          },
          resolve: {
            collectionObj: function () {
              return collectionObj
            },
            collectionList: function () {
              return $scope.collectionList
            },
            communityList: function () {
              return $scope.communityList
            },
            user: function () {
              return $scope.user
            }
          }
        })
      }
    }])
