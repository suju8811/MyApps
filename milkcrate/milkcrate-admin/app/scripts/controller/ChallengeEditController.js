'use strict'

angular.module('app.controllers')
  .controller('ChallengeEditController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$http', 'pinesNotifications',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $http, pinesNotifications) {
      // Init.
      $scope.CommonUtil = CommonUtil
      $scope.challengeId = null
      $scope.editMode = false
      if ($routeParams.id != 'new') {
        $scope.challengeId = $routeParams.id
        $scope.editMode = true
      }
      $scope.isLoading = true
      $scope.user = BendAuth.getActiveUser()

      $scope.communityList = []
      $scope.businessList = []
      $scope.actionList = []
      $scope.eventList = []
      $scope.volunteeringList = []
      $scope.serviceList = []
      $scope.activityList = []
      $scope.categories = []
      $scope.selectedCategory = {}

      $scope.fileProgress = []
      $scope.isUploading = []

      $scope.filter = {
        activity: ''
      }
      $scope.TypeList = ['action', 'business', 'event', 'volunteer_opportunity', 'website']

      $scope.challenge = {
        type: ''
      }

      $scope.formData = {
        communities: [],
        teams: []
      }
      $scope.teams = []
      $scope.timeS = new Date().valueOf()
      async.parallel([
        function (callback) {
          BendService.getCommunityList(function (rets) {
            $scope.communityList = rets
            callback(null, null)
          })
        },
        function (callback) {
          BendService.getCategoryListWithImages(function (rets) {
            $scope.categories = rets
            callback(null, null)
          })
        }
      ], function (err, retsError) {
        if (err) {
          console.log(err)
          return
        }
        if ($scope.challengeId) {
          BendService.getChallenge($scope.challengeId, function (ret) {
            // console.log("challenge", ret)
            $scope.challenge = ret
            $scope.isLoading = false

            // get team list
            BendService.getCommunityTeamList($scope.challenge.community._id, function (err, rets) {
              if (err) { console.log(err); return }
              $scope.teams = rets

              if ($scope.challenge.teams) {
                $scope.formData.teams = _.filter($scope.teams, function (o) {
                  return $scope.challenge.teams.indexOf(o._id) != -1
                })
              }
            })

            if ($scope.challenge.communities && $scope.challenge.communities.length > 0) {
              // console.log($scope.communityList);
              $scope.formData.communities = _.filter($scope.communityList, function (o) {
                return $scope.challenge.communities.indexOf(o._id) != -1
              })
              // console.log($scope.formData.categories);
            }

            if ($scope.challenge.activity) {
              $scope.activityList = []
              $scope.activityList.push($scope.challenge.activity)
              $scope.filter.activity = $scope.challenge.activity

              $scope.selectedCategory = $scope.getCategory($scope.challenge.activity)
            }

            if ($scope.challenge.startsAt) {
              $scope.StartsAt = CommonUtil.formatDateWithFormat(ret.startsAt, 'YYYY-MM-DD LT')// CommonUtil.formatDate(ret.startsAt);//CommonUtil.formatDateWithFormat(ret.startsAt,"YYYY-MM-DD");
            }
            if ($scope.challenge.endsAt) {
              $scope.EndsAt = CommonUtil.formatDateWithFormat(ret.endsAt, 'YYYY-MM-DD LT')// CommonUtil.formatDate(ret.endsAt);//CommonUtil.formatDateWithFormat(ret.endsAt,"YYYY-MM-DD");
            }
          })
        }
      })

      $scope.getCategory = function (activity) {
        if (!activity.categories || activity.categories.length == 0) { return {} }

        var category = _.find($scope.categories, function (o) {
          return o._id == activity.categories[0]
        })

        if (category) {
          return category
        }

        return {}
      }

      $scope.refreshActivity = function (keyword) {
        if ($scope.challenge.type == '') return

        var query = new $bend.Query()
        query.notEqualTo('deleted', true)

        if (keyword) {
          query.matches('name', new RegExp('' + keyword + '+', 'gi'))
        }
        query.ascending('name')
        query.limit(30)

        if (BendAuth.getActiveUser().communityAdmin) {
          var q = new Bend.Query()
          q.equalTo('community._id', BendAuth.getActiveUser().community._id)// .or().exists("community", false)
          query.and(q)
        }

        $bend.DataStore.find($scope.challenge.type, query).then(function (rets) {
          $scope.activityList = rets
          // console.log($scope.activityList);
        }, function (err) {
          console.log(err)
        })
      }

      $scope.isValidForm = function () {
        var isValid = $scope.form.validateForm.$dirty && $scope.form.validateForm.$valid
        if ($scope.isSaving) {
          isValid = false
        }

        return isValid
      }

      $scope.onChangeType = function () {
        $scope.activityList = []
        $scope.filter.activity = ''
        if ($scope.challenge.type == 'website') {
          $scope.challenge.headlineColor = '#ffffff'
        }
      }
      $scope.onChangeActivity = function () {
        $scope.challenge.points = $scope.filter.activity.points
        $scope.selectedCategory = $scope.getCategory($scope.filter.activity)
        $scope.challenge.imageNum = 1
      }
      $scope.saveChallengeDo = function () {
        if ($scope.StartsAt) {
          $scope.challenge.startsAt = CommonUtil.convertStringToDate($scope.StartsAt, 'YYYY-MM-DD LT').getTime() * 1000000
        } else {
          delete $scope.challenge.startsAt
        }
        if ($scope.EndsAt) {
          $scope.challenge.endsAt = CommonUtil.convertStringToDate($scope.EndsAt, 'YYYY-MM-DD LT').getTime() * 1000000
        } else {
          delete $scope.challenge.endsAt
        }

        if ($scope.challenge.type) {
          if ($scope.filter.activity != '') {
            $scope.challenge.activity = CommonUtil.makeBendRef($scope.filter.activity._id, $scope.challenge.type)
          } else {
            delete $scope.challenge.activity
          }
        }

        if ($scope.challenge.points && $scope.challenge.points != '') {
          $scope.challenge.points = Number($scope.challenge.points)
        } else {
          delete $scope.challenge.points
        }

        var challengeData = _.clone($scope.challenge)
        if (challengeData.coverImage) {
          challengeData.coverImage = CommonUtil.makeBendFile(challengeData.coverImage._id)
        }

        if ($scope.challengeId) {
          $scope.isSaving = true
          delete challengeData.$$hashKey

          if ($scope.formData.teams && $scope.formData.teams.length > 0) {
            challengeData.teams = CommonUtil.getIdList($scope.formData.teams)
          } else {
            delete challengeData.teams
          }

          BendService.updateChallenge(challengeData, function (ret) {
            $scope.isSaving = false
            $location.path('/challenges')
          })
        } else {
          $scope.isSaving = true
          if (!$scope.user.communityAdmin) {
            BendService.createChallenge(challengeData, $scope.formData.communities, function (err, ret) {
              $scope.isSaving = false
              if (err) {
                console.log(err); return
              }

              $location.path('/challenges')
            })
          } else {
            BendService.createChallenge(challengeData, [$scope.user.community], function (err, ret) {
              $scope.isSaving = false
              if (err) {
                console.log(err); return
              }

              $location.path('/challenges')
            })
          }
        }
      }

      $scope.deleteFile = function (tag, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        applyChangesOnScope($scope, function () {
          delete $scope.challenge[tag]
        })
      }

      $scope.showFileLoading = function (tag, bShow) {
        $scope.isUploading[tag] = bShow
      }

      $scope.selectFileOpen = function (fileId, $ev) {
        if ($ev.target.tagName == 'DIV') {
          setTimeout(function () {
            $('#' + fileId).click()
          }, 0)
        }
      }

      $scope.onFileUpload = function (files, tag) {
        var file = files[0]
        $scope.fileProgress[tag] = 0
        $scope.showFileLoading(tag, true)
        var reader = new FileReader()
        reader.onload = function (e) {
          $scope.uploadingFileUrl = e.target.result
        }
        reader.readAsDataURL(file)

        BendService.upload(file, function (error, uploadedFile) {
          if (error) {
            $scope.showFileLoading(tag, false)
            $scope.uploadingFileUrl = null
            return
          }
          BendService.getFile(uploadedFile, function (o) {
            $scope.challenge[tag] = o
            $scope.showFileLoading(tag, false)
            $scope.form.validateForm.$dirty = true
            $scope.uploadingFileUrl = null
          })
        }, {
          _workflow: 'coverPhoto'
        }, function (total, prog) {
          applyChangesOnScope($scope, function () {
            $scope.fileProgress[tag] = prog * 100 / total
          })
        })
      }

      $rootScope.fileUploadFromSearch2 = function (file, tag) {
        file._filename = Date.now() + ''
        var files = []
        files.push(file)
        $scope.onFileUpload(files, tag)
      }

      $scope.searchImages2 = function ($ev, tag) {
        $ev.stopPropagation()
        $ev.preventDefault()
        $modal.open({
          templateUrl: 'views/templates/searchImages.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance) {
            $scope.CommonUtil = CommonUtil
            $scope.searchResults = 0
            $scope.pages = 1
            $scope.showLoadMore = false
            // var cacheSearchKey = ''

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }

            $scope.searchImages = function (searchVal) {
              console.log('searchVal', searchVal)
              // cacheSearchKey = searchVal

              $scope.pages = 1
              if (searchVal) {
                var URL = 'https://pixabay.com/api/?key=2706353-ee3016f8af51ca406a8c8a3db&q=' + encodeURIComponent(searchVal) + '&image_type=photo&page=' + $scope.pages
                $.getJSON(URL, function (data) {
                  console.log(data)
                  if (parseInt(data.totalHits) > 0) {
                    applyChangesOnScope($scope, function () {
                      if (data.totalHits > $scope.pages * 20) { $scope.showLoadMore = true } else { $scope.showLoadMore = false }

                      $scope.searchResults = data.hits
                    })
                  } else { console.log('No hits') }
                })
              }
            }

            $scope.searchImagesMore = function (tabIdx, searchVal) {
              $scope.pages++
              if (searchVal) {
                var URL = 'https://pixabay.com/api/?key=2706353-ee3016f8af51ca406a8c8a3db&q=' + encodeURIComponent(searchVal) + '&image_type=photo&page=' + $scope.pages
                $.getJSON(URL, function (data) {
                  console.log(data)
                  if (parseInt(data.totalHits) > 0) {
                    applyChangesOnScope($scope, function () {
                      if (data.totalHits > $scope.pages * 20) { $scope.showLoadMore = true } else { $scope.showLoadMore = false }

                      $scope.searchResults = $scope.searchResults.concat(data.hits)
                    })
                  } else { console.log('No hits') }
                })
              }
            }

            $scope.selectImage = function (searchItem) {
              $scope.isDownloading = true
              console.log(searchItem)
              var imageUrl = ''
              imageUrl = searchItem.webformatURL

              var xhr = new XMLHttpRequest()
              xhr.open('GET', imageUrl, true)
              xhr.responseType = 'blob'
              xhr.onload = function (e) {
                if (this.status == 200) {
                  var myBlob = this.response
                  console.log('blob', myBlob)
                  $rootScope.fileUploadFromSearch2(myBlob, tag)
                  // myBlob is now the blob that the object URL pointed to.
                  $scope.cancel()
                }
              }
              xhr.send()
            }
          }
        })
      }
    }])
