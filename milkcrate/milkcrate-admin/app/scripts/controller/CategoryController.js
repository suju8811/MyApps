'use strict'
angular.module('app.controllers')
  .controller('CategoryController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal) {
      // Init.
      $scope.catList = []
      $scope.communityList = []
      $scope.allCommunityList = [{
        name: 'All client',
        _id: null
      }]
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.filter = {
        community: (CommonUtil.getStorage('category-filter') ? CommonUtil.getStorage('category-filter').community : '')
      }

      $scope.onSearch = function () {
        $scope.isLoading = true
        CommonUtil.setStorage('category-filter', {
          community: $scope.filter.community
        })

        var q = new $bend.Query()
        q.notEqualTo('deleted', true)
        q.ascending('name')

        if ($scope.filter.community instanceof Object && $scope.filter.community._id) {
          q.equalTo('community._id', $scope.filter.community._id)
        }

        $bend.DataStore.find('category', q, {
          relations: {
            community: 'community'
          }
        }).then(function (rets) {
          $scope.catList = rets
          $scope.isLoading = false
        }, function (err) {
          console.log(err)
          $scope.isLoading = false
        })
      }

      $scope.onSearch()
      BendService.getCommunityEnabledList(function (rets) {
        $scope.communityList = rets
        $scope.allCommunityList = $scope.allCommunityList.concat(rets)
      })

      $scope.deleteCategory = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Deleting the category is permanent and can not be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deleteCategory(id, function (ret) {
              if (ret) {
                for (var i = 0; i < $scope.catList.length; i++) {
                  if ($scope.catList[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.catList.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $scope.editCategory = function (cat) {
        $scope.openEditCategory(cat)
      }
      $scope.createCategory = function () {
        var categoryObj = {
          enabled: true
        }
        $scope.openEditCategory(categoryObj)
      }

      $scope.openEditCategory = function (categoryObj) {
        $modal.open({
          templateUrl: 'editCategory.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance, categoryObj, catList, communityList) {
            // $scope.group = $scope.groupList[3];
            // console.log($scope.group.value);    */
            $scope.groupList = CommonUtil.AllCategoryGroups
            $scope.CommonUtil = CommonUtil
            $scope.communityList = communityList
            $scope.fileProgress = []
            $scope.isUploading = []

            BendService.getCategory(categoryObj._id, function (ret) {
              $scope.category = ret
              if ($scope.category.community && typeof $scope.category.community === 'object') {
                $scope.category.community = $scope.category.community._id
              }
            })

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }

            $scope.saveCategoryDo = function () {
              var category = _.clone($scope.category)
              if (category.community) {
                category.community = CommonUtil.makeBendRef(category.community, 'community')
              } else {
                delete category.community
              }

              delete category.community_id
              if (category.coverImage) {
                category.coverImage = CommonUtil.makeBendFile(category.coverImage._id)
              }
              if (category.image1) {
                category.image1 = CommonUtil.makeBendFile(category.image1._id)
              }
              if (category.image2) {
                category.image2 = CommonUtil.makeBendFile(category.image2._id)
              }
              if (category.image3) {
                category.image3 = CommonUtil.makeBendFile(category.image3._id)
              }
              if (category.image4) {
                category.image4 = CommonUtil.makeBendFile(category.image4._id)
              }
              if (category.buttonImage) {
                category.buttonImage = CommonUtil.makeBendFile(category.buttonImage._id)
              }
              if (category.buttonImageSelected) {
                category.buttonImageSelected = CommonUtil.makeBendFile(category.buttonImageSelected._id)
              }
              if (category.icon) {
                category.icon = CommonUtil.makeBendFile(category.icon._id)
              }
              if (category.iconSticker) {
                category.iconSticker = CommonUtil.makeBendFile(category.iconSticker._id)
              }
              if (category._id) {
                delete category.$$hashKey

                BendService.updateCategory(category, function (ret) {
                  var ref = _.find(catList, function (o) {
                    return o._id == ret._id
                  })

                  _.extend(ref, $scope.category)

                  $modalInstance.dismiss('cancel')
                })
              } else {
                delete category.$$hashKey
                BendService.createCategory(category, function (ret) {
                  $scope.category._id = ret._id
                  catList.push($scope.category)
                  $modalInstance.dismiss('cancel')
                })
              }
            }
            $scope.deleteFile = function (tag, $ev) {
              $ev.stopPropagation()
              $ev.preventDefault()
              applyChangesOnScope($scope, function () {
                delete $scope.category[tag]
              })
            }

            $scope.showFileLoading = function (tag, bShow) {
              $scope.isUploading[tag] = bShow
            }

            $scope.selectFileOpen = function (fileId, $ev) {
              if ($ev.target.tagName == 'DIV') {
                setTimeout(function () {
                  $('#' + fileId).click()
                }, 0)
              }
            }

            $scope.onFileUpload = function (files, tag, workflow) {
              var file = files[0]
              $scope.fileProgress[tag] = 0
              $scope.showFileLoading(tag, true)

              BendService.upload(file, function (error, uploadedFile) {
                if (error) {
                  $scope.showFileLoading(tag, false)
                  return
                }
                BendService.getFile(uploadedFile, function (o) {
                  $scope.category[tag] = o
                  $scope.showFileLoading(tag, false)
                  $scope.form.validateForm.$dirty = true
                })
              }, {
                _workflow: workflow
              }, function (total, prog) {
                applyChangesOnScope($scope, function () {
                  $scope.fileProgress[tag] = prog * 100 / total
                })
              })
            }
            $scope.canSubmitValidationForm = function () {
              var isValid = $scope.form.validateForm.$valid
              if (!$scope.category || !$scope.category.community) { return isValid }

              isValid = isValid && ($scope.category.iconSticker || $scope.category.icon || ($scope.category.buttonImage && $scope.category.buttonImageSelected))
              return isValid
            }
          },
          resolve: {
            categoryObj: function () {
              return categoryObj
            },
            catList: function () {
              return $scope.catList
            },
            communityList: function () {
              return $scope.communityList
            }
          }
        })
      }
    }])
