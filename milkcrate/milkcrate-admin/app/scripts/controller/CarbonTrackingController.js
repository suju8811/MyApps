'use strict'

angular.module('app.controllers')
  .controller('CarbonTrackingController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$http', 'pinesNotifications', 'BendPusher',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $http, pinesNotifications, BendPusher) {
      // Init.
      $scope.CommonUtil = CommonUtil
      $scope.userId = $routeParams.id
      $scope.form = {}
      $scope.userAddressHome = {
        type: 'home',
        geofenceRadius: 500
      }
      $scope.userAddressWork = {
        type: 'work',
        geofenceRadius: 500
      }
      $scope.stateList = CommonUtil.AllStates

      $rootScope.setLocation = function (userAddress, lat, lng) {
        userAddress._geoloc = [lng, lat]

        $scope.form.validateForm.$setDirty()
      }

      BendService.getUserAddresses($scope.userId, function (err, rets) {
        if (err) {
          console.log(err); return
        }
        var userAddresses = []
        _.map(rets, function (ret) {
          userAddresses[ret.type] = ret
        })

        if (userAddresses['home']) {
          $scope.userAddressHome = userAddresses['home']
        }
        if (userAddresses['work']) {
          $scope.userAddressWork = userAddresses['work']
        }
      })

      $scope.saveDo = function () {
        if ($scope.userAddressHome._geoloc && $scope.userAddressHome._geoloc[0] && $scope.userAddressHome._geoloc[1] && $scope.userAddressHome._geoloc[0] != '' && $scope.userAddressHome._geoloc[1] != '') { $scope.userAddressHome._geoloc = [parseFloat($scope.userAddressHome._geoloc[0]), parseFloat($scope.userAddressHome._geoloc[1])] } else { delete $scope.userAddressHome._geoloc }
        $scope.userAddressHome.geofenceRadius = Number($scope.userAddressHome.geofenceRadius) || 0

        if ($scope.userAddressWork._geoloc && $scope.userAddressWork._geoloc[0] && $scope.userAddressWork._geoloc[1] && $scope.userAddressWork._geoloc[0] != '' && $scope.userAddressWork._geoloc[1] != '') { $scope.userAddressWork._geoloc = [parseFloat($scope.userAddressWork._geoloc[0]), parseFloat($scope.userAddressWork._geoloc[1])] } else { delete $scope.userAddressWork._geoloc }
        $scope.userAddressHome.geofenceRadius = Number($scope.userAddressWork.geofenceRadius) || 0

        $scope.userAddressHome.user = CommonUtil.makeBendRef($scope.userId, 'user')
        $scope.userAddressWork.user = CommonUtil.makeBendRef($scope.userId, 'user')

        async.parallel([
          function (cb) {
            BendService.saveUserAddress($scope.userAddressHome, function (err, ret) {
              cb(err, ret)
            })
          },
          function (cb) {
            BendService.saveUserAddress($scope.userAddressWork, function (err, ret) {
              cb(err, ret)
            })
          }
        ], function (err, ret) {
          if (err) {
            console.log(err); return
          }

          var notify = pinesNotifications.notify({
            text: '<p>Saved successfully.</p>',
            type: 'info',
            width: '200px'
          })
          setTimeout(function () {
            notify.remove()
          }, 2000)
          $scope.form.validateForm.$setPristine()
        })
      }

      $scope.geocodeAddress = function (userAddress) {
        var addressList = []
        if (userAddress.address1 && userAddress.address1 != '') { addressList.push(userAddress.address1) }
        if (userAddress.city && userAddress.city != '') { addressList.push(userAddress.city) }
        if (userAddress.state && userAddress.state != '') { addressList.push(userAddress.state) }
        if (userAddress.postalCode && userAddress.postalCode != '') { addressList.push(userAddress.postalCode) }

        $modal.open({
          templateUrl: 'views/templates/geocodeAddress.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance) {
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.confirmAddress = function () {
              var lat = $('#geoLat').val()
              var lng = $('#geoLng').val()
              if (lat == '' || lng == '') return

              $rootScope.setLocation(userAddress, lat, lng)
              $modalInstance.dismiss('cancel')
            }

            var marker = null
            $scope.initMap = function () {
              // console.log(document.getElementById('geo_map'))
              var map = new google.maps.Map(document.getElementById('geo_map'), {
                zoom: 12,
                center: {lat: 42.3005383, lng: -71.0654838}
              })

              var geocoder = new google.maps.Geocoder()

              if (addressList.length > 0) {
                var address = addressList.join(', ')
                document.getElementById('address').value = address
                $scope.geocodeAddress(geocoder, map)
              } else {
                if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                      lat: position.coords.latitude,
                      lng: position.coords.longitude
                    }

                    map.setCenter(pos)
                  })
                }
              }

              document.getElementById('submit').addEventListener('click', function () {
                $scope.geocodeAddress(geocoder, map)
              })

              map.addListener('click', function (e) {
                $scope.placeMarkerAndPanTo(e.latLng, map)
              })
            }

            setTimeout(function () {
              $scope.initMap()
            }, 100)

            $scope.placeMarkerAndPanTo = function (latLng, map) {
              if (marker) {
                marker.setPosition(latLng)
              } else {
                marker = new google.maps.Marker({
                  map: map,
                  position: latLng
                })
              }

              $('#geoLat').val(latLng.lat())
              $('#geoLng').val(latLng.lng())
            }

            $scope.geocodeAddress = function (geocoder, resultsMap) {
              var address = document.getElementById('address').value
              geocoder.geocode({'address': address}, function (results, status) {
                if (status === 'OK') {
                  resultsMap.setCenter(results[0].geometry.location)
                  $scope.placeMarkerAndPanTo(results[0].geometry.location, resultsMap)
                } else {
                  alert('Geocode was not successful for the following reason: ' + status)
                }
              })
            }
          }
        })
      }
    }])
