'use strict'

angular.module('app.controllers')
  .controller('PushTemplatesController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', 'BendUtils',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, BendUtils) {
      // Init.
      $scope.templates = []
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil

      BendService.getPushTemplateList(function (rets) {
        $scope.templates = rets
        $scope.isLoading = false
      })

      $scope.deleteTemplate = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Are you sure you want to delete? This cannot be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deletePushTemplate(id, function (ret) {
              if (ret) {
                for (var i = 0; i < $scope.templates.length; i++) {
                  if ($scope.templates[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.templates.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $rootScope.editTemplate = function (template) {
        $scope.openTemplateModal(template).then(function (push) {
          if (push.timevalue && push.timevalue instanceof Date) {
            push.time = push.timevalue.getHours() + ':' + push.timevalue.getMinutes() + ' AM'
          }
          BendService.updatePushTemplate(push, function (result) {
            // console.log(result);
          })
        })
      }

      $scope.createTemplate = function () {
        $scope.openTemplateModal().then(function (model) {
          if (model.obj.timevalue && model.obj.timevalue instanceof Date) {
            model.obj.time = model.obj.timevalue.getHours() + ':' + model.obj.timevalue.getMinutes() + ' AM'
          }
          BendService.createPushTemplate(model, function (result) {
            $scope.templates.push(result)
          })
        })
      }

      $scope.openTemplateModal = function (nt) {
        var modalInstance = $modal.open({
          templateUrl: 'views/push/push-template-edit-modal.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance) {
            $scope.audience = {
              audiences: [],
              defaultQuery: {}
            }
            $scope.deeplinks = []
            $scope.formData = {
              audiences: [],
              deeplink: null
            }
            $scope.textLimit = 140
            // var timevalue = new Date()
            if (nt) {
              $scope.model = nt
            } else {
              $scope.model = {
                name: '',
                obj: {
                  params: [],
                  text: '',
                  userQuery: '{}'
                }
              }
            }

            $scope.removeParam = function (param) {
              $scope.model.obj.params = _.without($scope.model.obj.params, param)
            }

            $scope.addParam = function () {
              // var count = $scope.model.obj.params.length + 1
              $scope.model.obj.params.push({
                key: '',
                value: ''
              })
            }

            $bend.execute('get-push-audiences').then(function (ret) {
              $scope.audience = ret
              if (ret.audiences) {
                if ($scope.model.obj.audience) {
                  var audiences = []
                  _.map($scope.model.obj.audience.audiences, function (o) {
                    var exist = _.find(ret.audiences, function (_o) {
                      return o == _o.query['community._id']
                    })
                    if (exist) {
                      audiences.push(exist)
                    }
                  })
                  $scope.formData.audiences = audiences
                }
              }
              $scope.runQuery()
            })

            $scope.searchDeepLinks = function (query) {
              $bend.execute('get-push-deeplinks', {query: query}).then(function (ret) {
                $scope.deeplinks = ret.result
              })
            }

            $scope.checkDeepLink = function (params) {
              var exist = _.find(params, function (o) {
                return o.key == 'deeplink'
              })

              if (exist) {
                return exist.value
              } else { return null }
            }

            $scope.updateParam = function () {
              console.log($scope.formData.deeplink)
              if ($scope.formData.deeplink) {
                var exist = _.find($scope.model.obj.params, function (o) {
                  return o.key == 'deeplink'
                })

                if (exist) {
                  exist.value = $scope.formData.deeplink.deeplink
                } else {
                  $scope.model.obj.params.push({
                    key: 'deeplink',
                    value: $scope.formData.deeplink.deeplink
                  })
                }
              } else {
                exist = _.find($scope.model.obj.params, function (o) {
                  return o.key == 'deeplink'
                })

                if (exist) {
                  $scope.removeParam(exist)
                }
              }
            }

            $scope.searchDeepLinks('')

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }

            $scope.proceed = function () {
              $scope.model.obj.audience = {
                defaultQuery: $scope.audience.defaultQuery,
                audiences: []
              }
              _.map($scope.formData.audiences, function (o) {
                $scope.model.obj.audience.audiences.push(o.query['community._id'])
              })

              $modalInstance.close($scope.model)
            }

            $scope.validateTextLength = function () {
              return $scope.textLimit - $scope.model.obj.text.length > 0
            }

            $scope.validateUserQuery = function () {
              $scope.userQueryError = CommonUtil.jsonValidate($scope.model.obj.userQuery)
              return !$scope.userQueryError
            }
            $scope.usersCount = null
            $scope.runQuery = function () {
              if ($scope.validateUserQuery() && $scope.model.obj && $scope.model.obj.userQuery) {
                var query = new $bend.Query({filter: JSON.parse($scope.model.obj.userQuery)})
                query.notEqualTo('deleted', true).equalTo('enabled', true)

                var qq
                if ($scope.formData.audiences.length > 0) {
                  var filter = _.clone($scope.formData.audiences[0].query)
                  qq = new $bend.Query({filter: filter})
                  _.map($scope.formData.audiences, function (o, idx) {
                    if (idx > 0) { qq.or({filter: _.clone(o.query)}) }
                  })
                } else {
                  qq = new $bend.Query({filter: _.clone($scope.audience.defaultQuery)})
                }

                query.and(qq)

                BendUtils.count('user', query, function (err, res) {
                  $scope.usersCount = res
                })
              } else {
                $scope.usersCount = null
              }
            }
            $scope.runQuery()

            $scope.validate = function () {
              return $scope.model.name && $scope.model.obj.text && $scope.validateTextLength() && $scope.validateUserQuery()
            }
          }
        })

        return modalInstance.result
      }
    }])
