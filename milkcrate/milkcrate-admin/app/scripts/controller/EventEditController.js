'use strict'

angular.module('app.controllers')
  .controller('EventEditController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$http', 'pinesNotifications',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $http, pinesNotifications) {
      // Init.
      $scope.CommonUtil = CommonUtil
      $scope.eventId = null
      if ($routeParams.id != 'new') { $scope.eventId = $routeParams.id }
      $scope.isLoading = true
      $scope.isUploading = []

      $scope.event = {
        repeatable: true
      }

      $scope.tagList = []
      $scope.categoryList = []
      $scope.collectionList = []
      $scope.communityList = []

      $scope.stateList = CommonUtil.AllStates
      $scope.formData = {
        categories: [],
        collections: [],
        intervalminute: 0,
        intervalhour: 0,
        intervalday: 0
      }
      $scope.certificationList = []
      $scope.state = $scope.stateList[0]
      $scope.fileProgress = []
      $scope.openedDateSelector = [false, false]
      $scope.openedTimeDateSelector = []
      $scope.event = {
        times: []
      }
      $scope.times = []

      $scope.user = BendAuth.getActiveUser()
      $scope.form = {}

      async.parallel([
        function (callback) {
          BendService.getCommunityList(function (rets) {
            $scope.communityList = rets
            callback(null, null)
          })
        }, function (callback) {
          BendService.getCategoryList(function (rets) {
            $scope.categoryList = rets
            callback(null, null)
          })
        }, function (callback) {
          BendService.getCollectionEnabledList(function (rets) {
            $scope.collectionList = rets
            callback(null, null)
          })
        }, function (callback) {
          BendService.getCertificationList(function (rets) {
            $scope.certificationList = rets
            callback(null, null)
          })
        }], function (err, rets) {
        if (err) {
          console.log(err)
          return
        }
        if ($scope.eventId) {
          BendService.getEvent($scope.eventId, function (ret) {
            ret.enabled = (ret.enabled !== false)
            $scope.event = ret
            $scope.isLoading = false

            if ($scope.event.repeatInterval) {
              $scope.formData.intervalday = parseInt($scope.event.repeatInterval / (24 * 3600))
              $scope.formData.intervalhour = parseInt(($scope.event.repeatInterval - $scope.formData.intervalday * (24 * 3600)) / (3600))
              $scope.formData.intervalminute = parseInt(($scope.event.repeatInterval - (($scope.formData.intervalday * (24 * 3600)) + $scope.formData.intervalhour * 3600)) / 60)
            }

            if ($scope.event.state) {
              $scope.state = $scope.event.state
            }
            if ($scope.event.categories && $scope.event.categories.length > 0) {
              $scope.formData.categories = _.filter($scope.categoryList, function (o) {
                return $scope.event.categories.indexOf(o._id) != -1
              })
            }

            if ($scope.event.certification) {
              $scope.event.certification = $scope.event.certification._id
            } else {
              $scope.event.certification = ''
            }

            if ($scope.event.community) {
              $scope.event.community = $scope.event.community._id
            } else {
              $scope.event.community = ''
            }

            if ($scope.event.collections && $scope.event.collections.length > 0) {
              // console.log($scope.collectionList);
              $scope.formData.collections = _.filter($scope.collectionList, function (o) {
                return $scope.event.collections.indexOf(o._id) != -1
              })
              // console.log($scope.formData.categories);
            }

            /* if ($scope.event.startsAt)
                    {
                        $scope.StartsAt = CommonUtil.formatDateWithFormat(ret.startsAt,"YYYY-MM-DD");//CommonUtil.formatDate(ret.startsAt);//CommonUtil.formatDateWithFormat(ret.startsAt,"YYYY-MM-DD");
                    }

                    if ($scope.event.endsAt)
                    {
                        $scope.EndsAt = CommonUtil.formatDateWithFormat(ret.endsAt,"YYYY-MM-DD");//CommonUtil.formatDate(ret.endsAt);//CommonUtil.formatDateWithFormat(ret.endsAt,"YYYY-MM-DD");

                    } */

            if ($scope.event.times) {
              $scope.add_count = $scope.event.times.length
              for (var i = 0; i < $scope.event.times.length; i++) {
                var time = $scope.event.times[i]
                time['index'] = i
                $scope.times.push(time)
                $scope.openedTimeDateSelector.push(false)
              }
            }
          })
        }
      })

      $scope.AddTimes = function () {
        if (!$scope.times) {
          $scope.times = []
        }
        $scope.times.push({
          'index': $scope.add_count,
          'days': '',
          'from': '',
          'until': ''
        })

        $scope.form.validateForm.$setDirty()

        $scope.add_count++
        $scope.IsEmptyTimes = ($scope.times.length > 0)
        if ($scope.openedTimeDateSelector.length < $scope.add_count) {
          $scope.openedTimeDateSelector.push(false)
        } else { $scope.openedTimeDateSelector[$scope.add_count - 1] = false }
      }

      $scope.RemoveTime = function (obj, e) {
        if (e) {
          e.preventDefault()
          e.stopPropagation()
        }

        var idx = $scope.times.indexOf(obj)
        $scope.times.splice(idx, 1)
        $scope.add_count--
        $scope.form.validateForm.$setDirty()
      }

      $scope.getDays = function (etime) {
        // console.log('date',$scope.Date[etime]);
        return etime['date']
      }
      $scope.getFrom = function (etime) {
        return etime['from']
      }
      $scope.getUntil = function (etime) {
        return etime['until']
      }

      $scope.saveEventDo = function () {
        var repeatInterval = 0
        if ($scope.formData.intervalday != 0) {
          repeatInterval = $scope.formData.intervalday * (24 * 3600)
        }
        if ($scope.formData.intervalhour != 0) {
          repeatInterval = repeatInterval + $scope.formData.intervalhour * (3600)
        }
        if ($scope.formData.intervalminute != 0) {
          repeatInterval = repeatInterval + $scope.formData.intervalminute * (60)
        }

        if (repeatInterval > 0) {
          $scope.event.repeatInterval = repeatInterval
        }

        /* if(typeof $scope.StartsAt == "object") {
                $scope.event.startsAt = $scope.StartsAt.getTime() * 1000000;//, "YYYY-MM-DD")
            }

            if(typeof $scope.EndsAt == "object") {
                $scope.event.endsAt= $scope.EndsAt.getTime() * 1000000;// * 1000000, "YYYY-MM-DD")
            } */

        if ($scope.event.certification != '') {
          $scope.event.certification = CommonUtil.makeBendRef($scope.event.certification, 'certification')
        }

        if ($scope.event.community && $scope.event.community != '') {
          $scope.event.community = CommonUtil.makeBendRef($scope.event.community, 'community')
        } else {
          delete $scope.event.community
        }

        if ($scope.event._geoloc && $scope.event._geoloc[0] && $scope.event._geoloc[1] && $scope.event._geoloc[0] != '' && $scope.event._geoloc[1] != '') { $scope.event._geoloc = [parseFloat($scope.event._geoloc[0]), parseFloat($scope.event._geoloc[1])] } else { delete $scope.event._geoloc }

        if ($scope.formData.collections.length > 0) {
          $scope.event.collections = CommonUtil.getIdList($scope.formData.collections)
        } else {
          delete $scope.event.collections
        }

        // if ($scope.business.hours)

        var days = document.getElementsByName('day-input')
        var froms = document.getElementsByName('from-input')
        var untils = document.getElementsByName('until-input')

        if ($scope.formData.categories.length > 0) {
          $scope.event.categories = CommonUtil.getIdList($scope.formData.categories)
          _.map($scope.formData.categories, function (o) {
            // searchTextList.push(o.name);
          })
        } else {
          delete $scope.event.categories
        }

        var eventData = _.clone($scope.event)
        if (eventData.coverImage) {
          eventData.coverImage = CommonUtil.makeBendFile(eventData.coverImage._id)
        }

        if ($scope.eventId) {
          var times = []
          var startsAt = null
          var endsAt = null
          for (var i = 0; i < days.length; i++) {
            var time = {'date': days[i].value, 'from': froms[i].value, 'until': untils[i].value}
            // console.log("hour", hour);
            times.push(time)

            if (!startsAt) {
              startsAt = time.date + ' ' + time.from
            } else {
              if (startsAt > time.date + ' ' + time.from) { startsAt = time.date + ' ' + time.from }
            }

            if (!endsAt) {
              endsAt = time.date + ' ' + time.until
            } else {
              if (endsAt < time.date + ' ' + time.until) { endsAt = time.date + ' ' + time.until }
            }
          }

          if (startsAt) {
            eventData.startsAt = new Date(startsAt).getTime() * 1000000
          }
          if (endsAt) {
            eventData.endsAt = new Date(endsAt).getTime() * 1000000
          }

          eventData.times = times

          delete eventData.$$hashKey
          BendService.updateEvent(eventData, function (ret) {
            $location.path('/events')
          })
        } else {
          delete eventData.$$hashKey
          times = []
          if (days.length > 0) {
            for (i = 0; i < days.length; i++) {
              if (days[i].value != '' && froms[i].value != '' && untils[i].value != '') {
                time = {'date': days[i].value, 'from': froms[i].value, 'until': untils[i].value}

                times.push(time)
              }
            }
          }

          if (times.length > 0) {
            async.map(times, function (time, callback) {
              var eData = $.extend(true, {}, eventData)

              var startsAt = time.date + ' ' + time.from
              var endsAt = time.date + ' ' + time.until

              if (startsAt) {
                eData.startsAt = new Date(startsAt).getTime() * 1000000
              }
              if (endsAt) {
                eData.endsAt = new Date(endsAt).getTime() * 1000000
              }
              eData.times = [time]
              BendService.createEvent(eData, function (ret) {
                callback(null, ret)
              })
            }, function (err, rets) {
              if (err) {
                console.log(err)
                return
              }

              $location.path('/events')
            })
          } else {
            BendService.createEvent(eventData, function (ret) {
              $location.path('/events')
            })
          }
        }
      }

      $scope.openDateWindow = function ($event, idx) {
        $event.preventDefault()
        $event.stopPropagation()

        $scope.openedDateSelector[idx] = true
      }
      $scope.openTimeDateWindow = function ($event, idx) {
        $event.preventDefault()
        $event.stopPropagation()

        $scope.openedTimeDateSelector[idx] = true
      }

      $rootScope.syncPickItems = function (pickedItems, type) {
        applyChangesOnScope($scope, function () {
          if (type == 'category') {
            $scope.formData.categories = pickedItems
          } else if (type == 'giftType') {
            $scope.formData.giftTypes = pickedItems
          } else if (type == 'interest') {
            $scope.formData.interests = pickedItems
          }
        })
      }

      $scope.deleteFile = function (tag, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        applyChangesOnScope($scope, function () {
          delete $scope.event.coverImage
        })
      }

      $scope.showFileLoading = function (tag, bShow) {
        $scope.isUploading[tag] = bShow
      }

      $scope.selectFileOpen = function (fileId, $ev) {
        if ($ev.target.tagName == 'DIV') {
          setTimeout(function () {
            $('#' + fileId).click()
          }, 0)
        }
      }

      $scope.onFileUpload = function (files, tag) {
        var file = files[0]
        $scope.fileProgress[tag] = 0
        $scope.showFileLoading(tag, true)
        var reader = new FileReader()
        reader.onload = function (e) {
          $scope.uploadingFileUrl = e.target.result
        }
        reader.readAsDataURL(file)

        BendService.upload(file, function (error, uploadedFile) {
          if (error) {
            $scope.uploadingFileUrl = null
            $scope.showFileLoading(tag, false)
            return
          }
          BendService.getFile(uploadedFile, function (o) {
            $scope.uploadingFileUrl = null
            $scope.event.coverImage = o
            $scope.showFileLoading(tag, false)
            $scope.form.validateForm.$dirty = true
          })
        }, {
          _workflow: 'coverPhoto'
        }, function (total, prog) {
          applyChangesOnScope($scope, function () {
            $scope.fileProgress[tag] = prog * 100 / total
          })
        })
      }

      $rootScope.setLocation = function (lat, lng) {
        $scope.event._geoloc = [lng, lat]
        $scope.form.validateForm.$setDirty()
      }

      $scope.geocodeAddress = function () {
        var addressList = []
        if ($scope.event.address1 && $scope.event.address1 != '') { addressList.push($scope.event.address1) }
        if ($scope.event.city && $scope.event.city != '') { addressList.push($scope.event.city) }
        if ($scope.event.state && $scope.event.state != '') { addressList.push($scope.event.state) }
        if ($scope.event.postalCode && $scope.event.postalCode != '') { addressList.push($scope.event.postalCode) }

        $modal.open({
          templateUrl: 'views/templates/geocodeAddress.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance) {
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.confirmAddress = function () {
              var lat = $('#geoLat').val()
              var lng = $('#geoLng').val()
              if (lat == '' || lng == '') return

              $rootScope.setLocation(lat, lng)
              $modalInstance.dismiss('cancel')
            }

            var marker = null
            $scope.initMap = function () {
              // console.log(document.getElementById('geo_map'))
              var map = new google.maps.Map(document.getElementById('geo_map'), {
                zoom: 12,
                center: {lat: 42.3005383, lng: -71.0654838}
              })

              var geocoder = new google.maps.Geocoder()

              if (addressList.length > 0) {
                var address = addressList.join(', ')
                document.getElementById('address').value = address
                $scope.geocodeAddress(geocoder, map)
              } else {
                if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                      lat: position.coords.latitude,
                      lng: position.coords.longitude
                    }

                    map.setCenter(pos)
                  })
                }
              }

              document.getElementById('submit').addEventListener('click', function () {
                $scope.geocodeAddress(geocoder, map)
              })

              map.addListener('click', function (e) {
                $scope.placeMarkerAndPanTo(e.latLng, map)
              })
            }

            setTimeout(function () {
              $scope.initMap()
            }, 100)

            $scope.placeMarkerAndPanTo = function (latLng, map) {
              if (marker) {
                marker.setPosition(latLng)
              } else {
                marker = new google.maps.Marker({
                  map: map,
                  position: latLng
                })
              }

              $('#geoLat').val(latLng.lat())
              $('#geoLng').val(latLng.lng())
            }

            $scope.geocodeAddress = function (geocoder, resultsMap) {
              var address = document.getElementById('address').value
              geocoder.geocode({'address': address}, function (results, status) {
                if (status === 'OK') {
                  resultsMap.setCenter(results[0].geometry.location)
                  $scope.placeMarkerAndPanTo(results[0].geometry.location, resultsMap)
                } else {
                  alert('Geocode was not successful for the following reason: ' + status)
                }
              })
            }
          }
        })
      }
      $rootScope.fileUploadFromSearch2 = function (file, tag) {
        file._filename = Date.now() + ''
        var files = []
        files.push(file)
        $scope.onFileUpload(files, tag)
      }

      $scope.searchImages2 = function ($ev, tag) {
        $ev.stopPropagation()
        $ev.preventDefault()
        $modal.open({
          templateUrl: 'views/templates/searchImages.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance) {
            $scope.CommonUtil = CommonUtil
            $scope.searchResults = 0
            $scope.pages = 1
            $scope.showLoadMore = false
            // var cacheSearchKey = ''

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }

            $scope.searchImages = function (searchVal) {
              console.log('searchVal', searchVal)
              // cacheSearchKey = searchVal

              $scope.pages = 1
              if (searchVal) {
                var URL = 'https://pixabay.com/api/?key=2706353-ee3016f8af51ca406a8c8a3db&q=' + encodeURIComponent(searchVal) + '&image_type=photo&page=' + $scope.pages
                $.getJSON(URL, function (data) {
                  console.log(data)
                  if (parseInt(data.totalHits) > 0) {
                    applyChangesOnScope($scope, function () {
                      if (data.totalHits > $scope.pages * 20) { $scope.showLoadMore = true } else { $scope.showLoadMore = false }

                      $scope.searchResults = data.hits
                    })
                  } else { console.log('No hits') }
                })
              }
            }

            $scope.searchImagesMore = function (tabIdx, searchVal) {
              $scope.pages++
              if (searchVal) {
                var URL = 'https://pixabay.com/api/?key=2706353-ee3016f8af51ca406a8c8a3db&q=' + encodeURIComponent(searchVal) + '&image_type=photo&page=' + $scope.pages
                $.getJSON(URL, function (data) {
                  console.log(data)
                  if (parseInt(data.totalHits) > 0) {
                    applyChangesOnScope($scope, function () {
                      if (data.totalHits > $scope.pages * 20) { $scope.showLoadMore = true } else { $scope.showLoadMore = false }

                      $scope.searchResults = $scope.searchResults.concat(data.hits)
                    })
                  } else { console.log('No hits') }
                })
              }
            }

            $scope.selectImage = function (searchItem) {
              $scope.isDownloading = true
              console.log(searchItem)
              var imageUrl = ''
              imageUrl = searchItem.webformatURL

              var xhr = new XMLHttpRequest()
              xhr.open('GET', imageUrl, true)
              xhr.responseType = 'blob'
              xhr.onload = function (e) {
                if (this.status == 200) {
                  var myBlob = this.response
                  console.log('blob', myBlob)
                  $rootScope.fileUploadFromSearch2(myBlob, tag)
                  // myBlob is now the blob that the object URL pointed to.
                  $scope.cancel()
                }
              }
              xhr.send()
            }
          }
        })
      }
    }])
