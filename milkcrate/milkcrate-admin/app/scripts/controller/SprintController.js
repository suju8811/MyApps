'use strict'

angular.module('app.controllers')
  .controller('SprintController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout) {
      $scope.sprintList = []
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.communityList = []

      $scope.collectionView = {
        searchTerm: '',
        itemsPerPage: 20,
        isLoading: true,
        totalItems: 0,
        currentPage: 1,
        numPages: 0
      }

      BendService.getCommunityEnabledList(function (rets) {
        $scope.communityList = rets
      })

      $scope.loadList = function () {
        $scope.isLoading = true

        var q = new $bend.Query()
        q.notEqualTo('deleted', true)
        // q.descending("_bmd.createdAt");
        q.limit($scope.collectionView.itemsPerPage)
        q.skip(($scope.collectionView.currentPage - 1) * $scope.collectionView.itemsPerPage)

        q.ascending('name')

        $bend.DataStore.find('sprint', q, {
          relations: {
            community: 'community'
          }
        }).then(function (rets) {
          $scope.sprintList = rets
          $scope.isLoading = false
        }, function (err) {
          console.log(err)
        })

        $bend.DataStore.count('sprint', q).then(function (count) {
          applyChangesOnScope($scope, function () {
            $scope.collectionView.totalItems = count
            $scope.collectionView.numPages = $scope.collectionView.totalItems / $scope.collectionView.itemsPerPage + 1
          })
        }, function (err) {
          console.log(err)
        })
      }

      $scope.loadList('')

      $scope.onSearch = function () {
        $scope.loadList($scope.collectionView.searchTerm)
      }

      $scope.onPageChange = function () {
        $scope.loadList($scope.collectionView.searchTerm)
      }

      $scope.deleteSprint = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Deleting the sprint is permanent and can not be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deleteSprint(id, function (ret) {
              if (ret) {
                for (var i = 0; i < $scope.sprintList.length; i++) {
                  if ($scope.sprintList[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.sprintList.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $scope.editSprint = function (act) {
        $scope.openEditSprint(act)
      }
      $scope.createSprint = function () {
        var actObj = {
          repeatable: true
        }
        $scope.openEditSprint(actObj)
      }

      $scope.openEditSprint = function (sprintObj) {
        $modal.open({
          templateUrl: 'editSprint.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance, sprintObj, sprintList, communityList) {
            $scope.sprint = _.clone(sprintObj)
            $scope.CommonUtil = CommonUtil
            $scope.communityList = communityList
            $scope.openedDateSelector = [false, false]
            if ($scope.sprint.startDate) {
              $scope.startDate = $scope.sprint.startDate
            }
            if ($scope.sprint.endDate) {
              $scope.endDate = $scope.sprint.endDate
            }

            if ($scope.sprint.community) {
              $scope.sprint.community = _.find(communityList, function (o) {
                return o._id == $scope.sprint.community._id
              })
            }

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }

            $scope.saveSprintDo = function () {
              var sprint = _.clone($scope.sprint)
              if (sprint.community) {
                sprint.community = CommonUtil.makeBendRef(sprint.community._id, 'community')
              }
              if (typeof $scope.startDate === 'object') {
                sprint.startDate = CommonUtil.formatDateWithFormat($scope.startDate.getTime() * 1000000, 'YYYY-MM-DD')
              }
              if (typeof $scope.endDate === 'object') {
                sprint.endDate = CommonUtil.formatDateWithFormat($scope.endDate.getTime() * 1000000, 'YYYY-MM-DD')
              }
              if (sprint._id) {
                delete sprint.$$hashKey
                BendService.updateSprint(sprint, function (ret) {
                  var ref = _.find(sprintList, function (o) {
                    return o._id == ret._id
                  })

                  _.extend(ref, ret)
                  if (ref.community) {
                    ref.community = _.find(communityList, function (o) {
                      return o._id == ref.community._id
                    })
                  }
                  $modalInstance.dismiss('cancel')
                })
              } else {
                BendService.createSprint(sprint, function (ret) {
                  if (ret.community) {
                    ret.community = _.find(communityList, function (o) {
                      return o._id == ret.community._id
                    })
                  }
                  sprintList.push(ret)
                  $modalInstance.dismiss('cancel')
                })
              }
            }
            $scope.openDateWindow = function ($event, idx) {
              $event.preventDefault()
              $event.stopPropagation()

              $scope.openedDateSelector[idx] = true
            }
            $scope.canSubmitValidationForm = function () {
              return $scope.form.sprintValidateForm.$valid
            }
          },
          resolve: {
            sprintObj: function () {
              return sprintObj
            },
            sprintList: function () {
              return $scope.sprintList
            },
            communityList: function () {
              return $scope.communityList
            }
          }
        })
      }
    }])
