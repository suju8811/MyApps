'use strict'

angular.module('app.controllers')
  .controller('ContentController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout', 'pinesNotifications',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout, pinesNotifications) {
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.community = {
        enabled: true,
        points: 0,
        welcomeEmailEnabled: false,
        actionsEnabled: true,
        placesEnabled: true,
        servicesEnabled: true,
        eventsEnabled: true,
        volunteerOpportunitiesEnabled: true,
        actionsTitle: 'Actions',
        placesTitle: 'Places',
        servicesTitle: 'Services',
        eventsTitle: 'Events',
        volunteerOpportunitiesTitle: 'Volunteer Opportunities',
        actionsDescription: 'Explore easy, self-reported lifestyle behaviors',
        placesDescription: 'Check in to local, sustainable businesses nearby',
        eventsDescription: 'Register for green events and add to your calendar',
        servicesDescription: 'Sign up for eco-friendly lifestyle services',
        volunteerOpportunitiesDescription: "Find one that's right for you",
        showCategoriesInSearch: true
      }

      $scope.fileProgress = []
      $scope.isUploading = []
      $scope.formData = {
        collections: [],
        sprints: []
      }
      $scope.collectionList = []
      $scope.sprintList = []

      $scope.stateList = CommonUtil.AllStates

      $scope.communityId = null

      $scope.communityId = BendAuth.getActiveUser().community._id

      $scope.form = {}

      // console.log("$scope.communityId", $scope.communityId)

      if ($scope.communityId) {
        BendService.getCollectionEnabledList(function (rets) {
          $scope.collectionList = rets
          BendService.getCommunity2($scope.communityId, function (err, ret) {
            $scope.community = Object.assign($scope.community, ret)

            $scope.isLoading = false
            if (ret.collections && ret.collections.length > 0) {
              $scope.formData.collections = _.filter($scope.collectionList, function (o) {
                return ret.collections.indexOf(o._id) != -1
              })
            }
          })
        })
      }

      $scope.saveCommunityDo = function () {
        var data = _.clone($scope.community)
        if ($scope.formData.collections.length > 0) {
          data.collections = CommonUtil.getIdList($scope.formData.collections)
        } else { delete $scope.community.collections }

        BendService.updateCommunity(data, function (err, ret) {
          if (err) {
            console.log(err)
            return
          }

          var notify = pinesNotifications.notify({
            text: '<p>Saved successfully.</p>',
            type: 'info',
            width: '200px'
          })
          setTimeout(function () {
            notify.remove()
          }, 2000)
        })
      }
    }])
