'use strict'

angular.module('app.controllers')
  .controller('PollQuestionController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout) {
      $scope.isLoading = true
      $scope.user = BendAuth.getActiveUser()
      $scope.CommonUtil = CommonUtil
      $scope.pollquestionList = []
      $scope.communityList = [{
        name: 'All Client',
        _id: null
      }]

      $scope.teams = []
      $scope.communityTeams = []

      $scope.filter = {
        community: (CommonUtil.getStorage('poll-filter') ? CommonUtil.getStorage('poll-filter').community : ''),
        team: ''
      }

      async.parallel([
        function (cb) {
          BendService.getAllTeamList(function (err, rets) {
            $scope.teams = rets
            cb(null)
          })
        },
        function (cb) {
          BendService.getCommunityList(function (rets) {
            $scope.communityList = $scope.communityList.concat(rets)
            cb(null)
          })
        }
      ], function (err, rets) {
        $scope.getPollQuestions()
      })

      $scope.getTeamNames = function (teamIds) {
        if (!teamIds || teamIds.length == 0) return ''

        var names = []
        _.map($scope.teams, function (o) {
          if (teamIds.indexOf(o._id) != -1) { names.push(o.name) }
        })

        return names.join(',')
      }

      $scope.getPollQuestions = function () {
        CommonUtil.setStorage('poll-filter', $scope.filter)

        if ($scope.filter.community && $scope.filter.community._id) {
          var teams = _.filter($scope.teams, function (o) {
            return o.community._id == $scope.filter.community._id
          })
          if (teams.length > 0) {
            $scope.communityTeams = [{
              name: 'All Teams',
              _id: null
            }].concat(teams)
          } else {
            $scope.communityTeams = []
          }
        } else {
          $scope.communityTeams = []
        }

        $scope.isLoading = true
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.descending('_bmd.createdAt')

        if ($scope.filter.community instanceof Object && $scope.filter.community._id) {
          query.containsAll('communities', [$scope.filter.community._id])
        }

        if ($scope.user.communityAdmin) {
          query.containsAll('communities', [$scope.user.community._id])
        }

        if ($scope.communityTeams.length > 0 && $scope.filter.team != '' && $scope.filter.team._id != null) {
          query.contains('teams', [$scope.filter.team._id])
        }

        $bend.DataStore.find('pollQuestion', query, {
          relations: {
            community: 'community'
          }
        }).then(function (rets) {
          $scope.pollquestionList = rets
          $scope.isLoading = false
        }, function (err) {
          console.log(err)
          $scope.isLoading = false
        })
      }

      $scope.getCommunityNames = function (list) {
        var ret = []
        _.map(list, function (o) {
          var client = _.find($scope.communityList, function (_o) {
            return _o._id == o
          })
          if (client) { ret.push(client.name) }
        })

        return ret.join(', ')
      }

      $scope.deletePollQuestion = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Deleting the pollquestion is permanent and can not be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deletePollQuestion(id, function (ret) {
              if (ret) {
                for (var i = 0; i < $scope.pollquestionList.length; i++) {
                  if ($scope.pollquestionList[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.pollquestionList.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $scope.editPollQuestion = function (pollquestion, $event) {
        if ($event.metaKey || $event.ctrlKey) {
          window.open('#/pollQuestiones/' + pollquestion._id, '_blank')
        } else {
          return $location.path('/pollQuestions/' + pollquestion._id)
        }
      }

      $scope.createPollQuestion = function ($event) {
        if ($event.metaKey || $event.ctrlKey) {
          window.open('#/pollQuestions/new', '_blank')
        } else { return $location.path('/pollQuestions/new') }
      }

      $scope.getResponseCount = function (list) {
        if (!list || list.length == 0) return 0

        if ($scope.user.communityAdmin) {
          return list[$scope.user.community._id] || 0
        }

        var count = 0
        _.map(list, function (o) {
          count += o
        })

        return count
      }
    }])
