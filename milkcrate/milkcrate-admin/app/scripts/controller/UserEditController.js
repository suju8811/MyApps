'use strict'

angular.module('app.controllers')
  .controller('UserEditController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$http', 'pinesNotifications', 'BendPusher',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $http, pinesNotifications, BendPusher) {
      // Init.
      $scope.user = {
        gender: ''
      }
      $scope.CommonUtil = CommonUtil
      $scope.communityList = []
      $scope.communityList2 = []
      $scope.userId = $routeParams.id
      $scope.openedDateSelector = [false]
      $scope.isLoading = true
      $scope.fileProgress = []
      $scope.isUploading = []
      $scope.tagList = []

      $scope.collectionView = {
        searchTerm: '',
        itemsPerPage: 20,
        isLoading: true,
        totalItems: 0,
        currentPage: 1,
        numPages: 0
      }
      $scope.form = {}
      $scope.teamList = []
      $scope.formData = {
        teams: []
      }

      async.parallel([
        function (cb) {
          BendService.getCommunityEnabledList(function (rets) {
            $scope.communityList = rets
            cb(null, null)
          })
        }
      ], function (err, ret) {
        if ($scope.userId) {
          BendService.getUser($scope.userId, function (ret) {
            $scope.user = ret
            $scope.isLoading = false

            if ($scope.user.community) {
              $scope.user.community = $scope.user.community._id
              BendService.getCommunityTeamList($scope.user.community, function (err, rets) {
                if (err) {
                  // cb(err)
                  return
                }
                $scope.teamList = rets

                $scope.user.teams = $scope.user.teams || []
                $scope.formData.teams = _.filter($scope.teamList, function (o) {
                  return $scope.user.teams.indexOf(o._id) != -1
                })
              })
            } else {
              $scope.user.community = ''
            }

            $scope.user.gender = $scope.user.gender || ''

            if ($scope.user.birthdate) {
              $scope.BirthDate = $scope.user.birthdate// CommonUtil.formatDateWithFormat(ret.birthdate,"YYYY-MM-DD");
            }

            $scope.user.email = $scope.user.email || $scope.user.username

            BendService.getAdminList(function (err, adminList) {
              var exist = _.find(adminList, function (o) {
                return o._id == $scope.userId
              })

              if (exist) { $scope.user.isAdmin = true } else { $scope.user.isAdmin = false }
            })
          })
        }
      })

      $scope.saveUserDo = function () {
        var userData = _.clone($scope.user)
        if (userData.community != '') {
          var community = _.find($scope.communityList, function (o) {
            return o._id == userData.community
          })
          userData.code = community.code
          userData.community = CommonUtil.makeBendRef(userData.community, 'community')
        } else {
          delete userData.community
        }

        if (typeof $scope.BirthDate === 'object') {
          userData.birthdate = CommonUtil.formatDateWithFormat($scope.BirthDate.getTime() * 1000000, 'YYYY-MM-DD')
        }

        if (userData.gender == '') {
          delete userData.gender
        } else {
          userData.gender = userData.gender.toLowerCase()
        }

        if (userData.avatar) { userData.avatar = CommonUtil.makeBendFile(userData.avatar._id) }
        if (userData.coverImage) { userData.coverImage = CommonUtil.makeBendFile(userData.coverImage._id) }

        if (userData.email != userData.username) {
          userData.username = userData.email
        }

        // teams
        var teams = []
        _.map($scope.formData.teams, function (o) {
          teams.push(o._id)
        })

        userData.teams = teams
        BendService.updateUser(userData, function (ret) {
          $location.path('/users')
          BendService.setUserAsAdmin(userData._id, $scope.user.isAdmin, function (err) {
            console.log('setUserAsAdmin', err)
          })
        })
      }
      $scope.openDateWindow = function ($event, idx) {
        $event.preventDefault()
        $event.stopPropagation()

        $scope.openedDateSelector[idx] = true
      }

      $scope.deleteUser = function () {
        var msg = 'Deleting the user is permanent and can not be undone'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deleteUser($scope.userId, function (ret) {
              applyChangesOnScope($scope, function () {
                $location.url('/users')
              })
            })
          }
        })
      }

      $scope.deleteFile = function (tag, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        if (tag == 'userAvatar') {
          applyChangesOnScope($scope, function () {
            delete $scope.user.avatar
          })
        }
        if (tag == 'coverImage') {
          applyChangesOnScope($scope, function () {
            delete $scope.user.coverImage
          })
        }

        $scope.form.validateForm.$setDirty()
      }

      $scope.showFileLoading = function (tag, bShow) {
        $scope.isUploading[tag] = bShow
      }

      $scope.selectFileOpen = function (fileId, $ev) {
        if ($ev.target.tagName == 'DIV') {
          setTimeout(function () {
            $('#' + fileId).click()
          }, 0)
        }
      }

      $scope.onFileUpload = function (files, tag) {
        var file = files[0]
        $scope.fileProgress[tag] = 0
        $scope.showFileLoading(tag, true)
        if (tag == 'userAvatar') {
          BendService.upload(file, function (error, uploadedFile) {
            if (error) {
              $scope.showFileLoading(tag, false)
              return
            }

            BendService.getFile(uploadedFile, function (o) {
              $scope.user.avatar = o
              $scope.showFileLoading(tag, false)
              $scope.form.validateForm.$dirty = true
            })
          }, {
            _workflow: 'avatar'
          }, function (total, prog) {
            applyChangesOnScope($scope, function () {
              $scope.fileProgress[tag] = prog * 100 / total
            })
          })
        } else if (tag == 'coverImage') {
          BendService.upload(file, function (error, uploadedFile) {
            if (error) {
              $scope.showFileLoading(tag, false)
              return
            }
            BendService.getFile(uploadedFile, function (o) {
              $scope.user.coverImage = o
              $scope.showFileLoading(tag, false)
              $scope.form.validateForm.$dirty = true
            })
          }, {
            _workflow: 'coverPhoto'
          }, function (total, prog) {
            applyChangesOnScope($scope, function () {
              $scope.fileProgress[tag] = prog * 100 / total
            })
          })
        }
      }

      $scope.changePassword = function () {
        $modal.open({
          templateUrl: 'changePassword.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance, user) {
            $scope.user = {}

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }

            user.community = CommonUtil.makeBendRef(user.community, 'community')
            $scope.changePasswordDo = function () {
              user.password = $scope.user.password
              if (user.community) {
                delete user.community.$$hashKey
              }
              BendService.updateUser(user, function (ret) {
                console.log(ret)
                user.community = user.community._id
                $modalInstance.dismiss('cancel')
              })
            }

            $scope.canSubmitValidationForm = function () {
              return $scope.form.validateForm.$valid
            }
          },
          resolve: {
            user: function () {
              return $scope.user
            }
          }
        })
      }

      $scope.loadActivities = function () {
        $scope.isLoading = true

        var q = new $bend.Query()
        q.notEqualTo('deleted', true)
        q.descending('_bmd.createdAt')
        q.equalTo('user._id', $scope.userId)
        q.limit($scope.collectionView.itemsPerPage)
        q.skip(($scope.collectionView.currentPage - 1) * $scope.collectionView.itemsPerPage)

        $bend.DataStore.find('activity', q, {
          relations: {
            user: 'user',
            community: 'community'
          }
        }).then(function (rets) {
          $scope.activityList = rets
          $scope.isLoading = false

          $scope.imageMap = {}
          var imageIds = []
          _.each(rets, function (o) {
            if (o.user.avatar) {
              if (imageIds.indexOf(o.user.avatar._id) == -1) { imageIds.push(o.user.avatar._id) }
            }
          })

          var query = new $bend.Query()
          query.contains('_id', imageIds)
          $bend.File.find(query).then(function (rets) {
            _.each(rets, function (o) {
              $scope.imageMap[o._id] = o
            })
            _.map($scope.activityList, function (o) {
              if (o.user.avatar) {
                o.user.avatar = $scope.imageMap[o.user.avatar._id]
              }
            })
          })
        }, function (err) {
          console.log(err)
        })

        $bend.DataStore.count('activity', q).then(function (count) {
          applyChangesOnScope($scope, function () {
            $scope.collectionView.totalItems = count
            $scope.collectionView.numPages = $scope.collectionView.totalItems / $scope.collectionView.itemsPerPage + 1
          })
        }, function (err) {
          console.log(err)
        })
      }

      $scope.loadActivities()

      $scope.onPageChange = function () {
        $scope.loadActivities()
      }

      $scope.deleteActivity = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Deleting the activity is permanent and can not be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deleteActivity(id, function (ret) {
              if (ret) {
                for (var i = 0; i < $scope.activityList.length; i++) {
                  if ($scope.activityList[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.activityList.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $scope.editActivity = function (activity, $event) {
        if ($event.metaKey || $event.ctrlKey) {
          window.open('#/users/' + $scope.userId + '/activities/' + activity._id, '_blank')
        } else { return $location.path('/users/' + $scope.userId + '/activities/' + activity._id) }
      }

      $scope.createActivity = function ($event) {
        if ($event.metaKey || $event.ctrlKey) {
          window.open('#/users/' + $scope.userId + '/activities/new', '_blank')
        } else { return $location.path('/users/' + $scope.userId + '/activities/new') }
      }
    }])
