'use strict'

angular.module('app.controllers')
  .controller('TeamEditController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$http', 'pinesNotifications',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $http, pinesNotifications) {
      // Init.
      $scope.CommonUtil = CommonUtil
      $scope.teamId = null
      $scope.editMode = false
      if ($routeParams.id != 'new') {
        $scope.teamId = $routeParams.id
        $scope.editMode = true
      }
      $scope.isLoading = true
      $scope.user = BendAuth.getActiveUser()

      $scope.communityList = []

      async.parallel([
        function (callback) {
          BendService.getCommunityList(function (rets) {
            $scope.communityList = rets
            callback(null, null)
          })
        }
      ], function (err, retsError) {
        if (err) {
          console.log(err)
          return
        }
        if ($scope.teamId) {
          BendService.getTeam($scope.teamId, function (err, ret) {
            if (err) {
              console.log(err); return
            }
            // console.log("team", ret)
            $scope.team = ret
            if ($scope.team.community) {
              $scope.team.community = _.find($scope.communityList, function (o) {
                return o._id == $scope.team.community._id
              })
            }

            $scope.isLoading = false
          })
        }
      })

      $scope.isValidForm = function () {
        var isValid = $scope.form.validateForm.$dirty && $scope.form.validateForm.$valid
        if ($scope.isSaving) {
          isValid = false
        }

        return isValid
      }

      $scope.saveTeamDo = function () {
        var teamData = _.clone($scope.team)

        if ($scope.teamId) {
          $scope.isSaving = true
          delete teamData.$$hashKey
          BendService.updateTeam(teamData, function (ret) {
            $scope.isSaving = false
            $location.path('/teams')
          })
        } else {
          $scope.isSaving = true
          if ($scope.user.communityAdmin) {
            teamData.community = $scope.user.community
          }
          BendService.createTeam(teamData, function (err, ret) {
            $scope.isSaving = false
            if (err) {
              console.log(err); return
            }

            $location.path('/teams')
          })
        }
      }
    }])
