/* eslint no-useless-escape: 0 */

'use strict'

angular.module('app.controllers')
  .controller('CommunityEditController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout) {
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.community = {
        enabled: true,
        points: 0,
        welcomeEmailEnabled: false,
        actionsEnabled: true,
        placesEnabled: true,
        servicesEnabled: true,
        eventsEnabled: true,
        volunteerOpportunitiesEnabled: true,
        actionsTitle: 'Actions',
        placesTitle: 'Places',
        servicesTitle: 'Services',
        eventsTitle: 'Events',
        volunteerOpportunitiesTitle: 'Volunteer Opportunities',
        actionsDescription: 'Explore easy, self-reported lifestyle behaviors',
        placesDescription: 'Check in to local, sustainable businesses nearby',
        eventsDescription: 'Register for green events and add to your calendar',
        servicesDescription: 'Sign up for eco-friendly lifestyle services',
        volunteerOpportunitiesDescription: "Find one that's right for you",
        showCategoriesInSearch: true,
        includeCommonActions: false,
        includeCommonPlaces: false,
        includeCommonServices: false,
        includeCommonEvents: false,
        includeCommonVolunteerOpportunities: false,
        profileChartsEnabled: true,
        whitelistEnabled: true
      }
      $scope.whitelistedEmailsList = []
      $scope.whitelistedDomains = []
      $scope.fileProgress = []
      $scope.isUploading = []
      $scope.formData = {
        collections: [],
        categories: [],
        sprints: []
      }
      $scope.collectionList = []
      $scope.sprintList = []
      $scope.tasks = []
      $scope.categories = []
      $scope.defaultCategories = ['Animals', 'Arts', 'Baby', 'Beauty', 'Bicycles', 'Civic', 'Coffee', 'Community', 'Construction', 'Dining', 'Drinks', 'Education', 'Energy', 'Fashion', 'Finance', 'Food', 'Garden', 'Green Space', 'Health & Wellness', 'Home & Office', 'Media', 'Products', 'Services', 'Special Events', 'Tourism & Hospitality', 'Transit', 'Waste']

      $scope.stateList = CommonUtil.AllStates

      $scope.communityId = null

      if ($routeParams.id != 'new') { $scope.communityId = $routeParams.id }

      $scope.form = {}

      console.log('$scope.communityId', $scope.communityId)

      async.parallel([
        function (callback) {
          BendService.getCollectionEnabledList(function (rets) {
            $scope.collectionList = rets
            callback(null, null)
          })
        },
        function (callback) {
          if ($scope.communityId) {
            BendService.getCategoryEnabledList(function (rets) {
              $scope.categories = rets
              callback(null, null)
            })
          } else {
            BendService.getGlobalCategoryEnabledList(function (rets) {
              $scope.categories = rets
              callback(null, null)
            })
          }
        },
        function (callback) {
          BendService.getTaskList($scope.communityId, function (err, rets) {
            console.log('get tasks', err, rets)
            if (!err) { $scope.tasks = rets }
            callback(null, null)
          })
        }
      ], function (err, rets) {
        if ($scope.communityId) {
          BendService.getCommunity2($scope.communityId, function (err, ret) {
            $scope.community = Object.assign($scope.community, ret)
            if (ret.collections && ret.collections.length > 0) {
              $scope.formData.collections = _.filter($scope.collectionList, function (o) {
                return ret.collections.indexOf(o._id) != -1
              })
            }
            if (ret.categories && ret.categories.length > 0) {
              $scope.formData.categories = _.filter($scope.categories, function (o) {
                return ret.categories.indexOf(o._id) != -1
              })
            }
            /* if(ret.sprints && ret.sprints.length > 0 ) {
                ret.sprints.map(function(o){
                    var exist = _.find($scope.sprintList, function(_o){
                        return _o._id == o
                    })
                    $scope.formData.sprints.push(exist)
                })
            } */
            $scope.isLoading = false
          })
        } else {
          $scope.formData.categories = _.filter($scope.categories, function (o) {
            return $scope.defaultCategories.indexOf(o.name) != -1
          })
        }
      })

      $scope.formatDate = function (time) {
        return moment(time / 1000000).format('MMMM Do YYYY, HH:mm:ss')
      }

      $scope.fetchWhiteListedEmails = function (callback) {
        var query = new $bend.Query()
        query.equalTo('community', $scope.communityId)
        query.notEqualTo('deleted', true)
        query.ascending('email')
        $bend.DataStore.find('whitelisted', query).then(function (data) {
          $scope.whitelistedEmailsList = data
          if (callback instanceof Function) {
            callback(data)
          }
        })
      }

      $scope.handleWhiteListedEmailsDeleteClick = function (item) {
        $scope.isLoading = true
        var dataToUpdate = {
          _id: item._id,
          community: item.community,
          email: item.email,
          deleted: true
        }
        $bend.DataStore.update('whitelisted', dataToUpdate).then(function (data) {
          $scope.fetchWhiteListedEmails(function () {
            $scope.isLoading = false
          })
        })
      }

      $scope.addWhiteListEmails = function () {
        var $rootScope = $scope
        var modalInstance = $modal.open({
          templateUrl: 'views/communities/whitelist.modal.html',
          controller: function ($scope, $modalInstance) {
            $scope.whitelistEmails = '' // ng-model
            var validateEmail = function (email) {
              var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
              return re.test(email)
            }
            $scope.addToWhitelist = function () {
              var invalidEmails = $scope.whitelistEmails
                .split('\n')
                .filter(function (email) {
                  return !validateEmail(email)
                })
              var errorMessage = invalidEmails
                .map(function (email) {
                  return 'invalid: ' + email
                })
                .join('\n')
              if (invalidEmails.length) {
                alert(errorMessage)
                return false
              }
              var dataToSave = {
                emails: $scope.whitelistEmails.split('\n'),
                communityId: $rootScope.communityId
              }
              $rootScope.isLoading = true
              $bend.execute('addToWhitelist', dataToSave).then(function (data) {
                $scope.cancel()
                $rootScope.fetchWhiteListedEmails(function () {
                  $rootScope.isLoading = false
                })
              }).catch(function (err) {
                $rootScope.isLoading = false
                alert(err.name)
              })
            }
            $scope.cancel = function () {
              modalInstance.dismiss('cancel')
            }
          }
        })
      }

      $scope.saveCommunityDo = function () {
        if ($scope.formData.collections.length > 0) {
          $scope.community.collections = CommonUtil.getIdList($scope.formData.collections)
        } else { delete $scope.community.collections }

        if ($scope.formData.categories.length > 0) {
          $scope.community.categories = CommonUtil.getIdList($scope.formData.categories)
        } else { delete $scope.community.categories }
        /* if($scope.formData.sprints.length > 0) {
            $scope.community.sprints = CommonUtil.getIdList($scope.formData.sprints);
        }
        else {
            delete $scope.community.sprints;
        } */

        if ($scope.community._geoloc && $scope.community._geoloc[0] && $scope.community._geoloc[1] && $scope.community._geoloc[0] != '' && $scope.community._geoloc[1] != '') { $scope.community._geoloc = [parseFloat($scope.community._geoloc[0]), parseFloat($scope.community._geoloc[1])] } else { delete $scope.community._geoloc }

        var data = _.clone($scope.community)
        data.code = data.code.toLowerCase()
        if (data.logo) {
          data.logo = CommonUtil.makeBendFile(data.logo._id)
        }

        if (data._id) {
          delete data.$$hashKey
          BendService.updateCommunity(data, function (ret) {
            $location.path('/communities')
          })
        } else {
          BendService.createCommunity(data, function (ret) {
            // console.log(ret);
            $scope.community._id = ret._id
            $location.path('/communities')
          })
        }
      }

      $scope.canSubmitValidationForm = function () {
        return $scope.form.validateForm.$valid
      }

      $scope.openFile = function (fileName) {
        $("input[name='" + fileName + "']").click()
      }

      $scope.deleteFile = function (tag, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        applyChangesOnScope($scope, function () {
          delete $scope.community.logo
        })

        $scope.form.validateForm.$setDirty()
      }

      $scope.showFileLoading = function (tag, bShow) {
        $scope.isUploading[tag] = bShow
      }

      $scope.selectFileOpen = function (fileId, $ev) {
        if ($ev.target.tagName == 'DIV') {
          setTimeout(function () {
            $('#' + fileId).click()
          }, 0)
        }
      }

      $scope.onFileUpload = function (files, tag) {
        var file = files[0]

        $scope.fileProgress[tag] = 0
        $scope.showFileLoading(tag, true)
        BendService.upload(file, function (error, uploadedFile) {
          if (error) {
            $scope.showFileLoading(tag, false)
            return
          }

          BendService.getFile(uploadedFile, function (o) {
            $scope.community.logo = o
            $scope.showFileLoading(tag, false)
          })
          $scope.form.validateForm.$setDirty()
        }, {
          _workflow: 'logo'
        }, function (total, prog) {
          applyChangesOnScope($scope, function () {
            $scope.fileProgress[tag] = prog * 100 / total
          })
        })
      }

      $rootScope.setLocation = function (lat, lng) {
        $scope.community._geoloc = [lng, lat]
        $scope.form.validateForm.$setDirty()
      }

      $scope.geocodeAddress = function () {
        var addressList = []
        if ($scope.community.address1 && $scope.community.address1 != '') { addressList.push($scope.community.address1) }
        if ($scope.community.city && $scope.community.city != '') { addressList.push($scope.community.city) }
        if ($scope.community.state && $scope.community.state != '') { addressList.push($scope.community.state) }
        if ($scope.community.postalCode && $scope.community.postalCode != '') { addressList.push($scope.community.postalCode) }

        $modal.open({
          templateUrl: 'views/templates/geocodeAddress.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance) {
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.confirmAddress = function () {
              var lat = $('#geoLat').val()
              var lng = $('#geoLng').val()
              if (lat == '' || lng == '') return

              $rootScope.setLocation(lat, lng)
              $modalInstance.dismiss('cancel')
            }

            var marker = null
            $scope.initMap = function () {
              // console.log(document.getElementById('geo_map'))
              var map = new google.maps.Map(document.getElementById('geo_map'), {
                zoom: 12,
                center: {lat: 42.3005383, lng: -71.0654838}
              })

              var geocoder = new google.maps.Geocoder()

              if (addressList.length > 0) {
                var address = addressList.join(', ')
                document.getElementById('address').value = address
                $scope.geocodeAddress(geocoder, map)
              } else {
                if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                      lat: position.coords.latitude,
                      lng: position.coords.longitude
                    }

                    map.setCenter(pos)
                  })
                }
              }

              document.getElementById('submit').addEventListener('click', function () {
                $scope.geocodeAddress(geocoder, map)
              })

              map.addListener('click', function (e) {
                $scope.placeMarkerAndPanTo(e.latLng, map)
              })
            }

            setTimeout(function () {
              $scope.initMap()
            }, 100)

            $scope.placeMarkerAndPanTo = function (latLng, map) {
              if (marker) {
                marker.setPosition(latLng)
              } else {
                marker = new google.maps.Marker({
                  map: map,
                  position: latLng
                })
              }

              $('#geoLat').val(latLng.lat())
              $('#geoLng').val(latLng.lng())
            }

            $scope.geocodeAddress = function (geocoder, resultsMap) {
              var address = document.getElementById('address').value
              geocoder.geocode({'address': address}, function (results, status) {
                if (status === 'OK') {
                  resultsMap.setCenter(results[0].geometry.location)
                  $scope.placeMarkerAndPanTo(results[0].geometry.location, resultsMap)
                } else {
                  alert('Geocode was not successful for the following reason: ' + status)
                }
              })
            }
          }
        })
      }
    }])
