'use strict'

angular.module('app.controllers')
  .controller('CommunityPushController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', 'BendUtils',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, BendUtils) {
      // Init.
      $scope.pushes = []
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil

      BendService.getCommunityPushList(function (rets) {
        $scope.pushes = rets
        $scope.isLoading = false
      })

      $scope.deletePush = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Are you sure you want to delete? This cannot be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deletePush(id, function (ret) {
              if (ret) {
                for (var i = 0; i < $scope.pushes.length; i++) {
                  if ($scope.pushes[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.pushes.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $rootScope.editPush = function (push) {
        $scope.openPushModal(push).then(function (ret) {
          if (typeof ret.datetime === 'object') {
            // console.log(ret.datetime)
            var dateStr = moment(ret.datetime).format('YYYY-MM-DD HH:mm:ss')
            // console.log(dateStr)
            var dateStrWithTimeZone = moment.tz(dateStr, push.timezone).format()
            // console.log(dateStrWithTimeZone)
            ret.datetime = new Date(dateStrWithTimeZone).getTime() * 1000000
            // console.log(ret.datetime)
          }
          _.extend(push, ret)
          /* BendService.updatePush(ret, function(result){
            _.extend(push, result);
          }); */
        })
      }

      $scope.createPush = function () {
        $scope.openPushModal().then(function (ret) {
          console.log('createPush', ret)
          $scope.pushes.unshift(ret)
          /* BendService.createPush(ret, function(result){
            $scope.pushes.unshift(result);
          }); */
        })
      }

      $scope.existPending = function () {
        var exist = _.filter($scope.pushes, function (o) {
          return o.status == 'pending'
        })

        if (exist) { return false }

        return true
      }

      $scope.executeSchedule = function () {
        $scope.isLoading = true
        BendUtils.wrapInCallback($bend.execute('send-scheduled-notifications'), function (error, result) {
          console.log(error, result)
          BendService.getCommunityPushList(function (rets) {
            $scope.pushes = rets
            $scope.isLoading = false
          })
        })
      }
      $scope.openPushModal = function (nt) {
        var modalInstance = $modal.open({
          templateUrl: 'views/push/push-edit-modal.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance) {
            $scope.textLimit = 140
            $scope.timezones = moment.tz.names()
            $scope.audience = {
              audiences: [],
              defaultQuery: {}
            }
            $scope.push = {}
            $scope.deeplinks = []

            $scope.formData = {
              audiences: [],
              deeplink: null
            }

            if (nt) {
              $scope.push = _.clone(nt)
              $scope.push.timezone = $scope.push.timezone || 'US/Eastern'
              $scope.push.datetime = new Date(nt.datetime / 1000000)
            } else {
              $scope.push = {
                params: [],
                title: '',
                text: '',
                scheduled: false,
                userQuery: '{}',
                status: 'pending',
                datetime: new Date(), // CommonUtil.formatDateWithFormat(new Date() * 1000000, 'YYYY-MM-DD')
                timezone: 'US/Eastern'
              }
            }

            // console.log($scope.push.datetime)

            $scope.dateTimeValue = CommonUtil.formatDateWithFormatWithTimezone($scope.push.datetime.getTime() * 1000000, 'YYYY-MM-DD HH:mm', $scope.push.timezone)

            $bend.execute('get-push-audiences').then(function (ret) {
              $scope.audience = ret
              if (ret.audiences) {
                if ($scope.push.audience) {
                  var audiences = []
                  _.map($scope.push.audience.audiences, function (o) {
                    var exist = _.find(ret.audiences, function (_o) {
                      return o == _o.query['community._id']
                    })
                    if (exist) {
                      audiences.push(exist)
                    }
                  })
                  $scope.formData.audiences = audiences
                }
              }
              $scope.runQuery()
            })

            $scope.searchDeepLinks = function (query) {
              $bend.execute('get-push-deeplinks', {query: query}).then(function (ret) {
                $scope.deeplinks = ret.result
              })
            }

            $scope.searchDeepLinks('')

            $scope.checkDeepLink = function (params) {
              var exist = _.find(params, function (o) {
                return o.key == 'deeplink'
              })

              if (exist) {
                return exist.value
              } else { return null }
            }

            $scope.setTemplate = function (template) {
              if (template.obj) {
                _.extend($scope.push, template.obj)
                $scope.push.datetime = new Date()
                if ($scope.runQuery) {
                  $scope.runQuery()
                }
              }
            }

            $scope.removeParam = function (param) {
              $scope.push.params = _.without($scope.push.params, param)
            }

            $scope.addParam = function () {
              // var count = $scope.push.params.length + 1
              $scope.push.params.push({
                key: '',
                value: ''
              })
            }

            $scope.updateParam = function () {
              console.log($scope.formData.deeplink)
              if ($scope.formData.deeplink) {
                var exist = _.find($scope.push.params, function (o) {
                  return o.key == 'deeplink'
                })

                if (exist) {
                  exist.value = $scope.formData.deeplink.deeplink
                } else {
                  $scope.push.params.push({
                    key: 'deeplink',
                    value: $scope.formData.deeplink.deeplink
                  })
                }
              } else {
                exist = _.find($scope.push.params, function (o) {
                  return o.key == 'deeplink'
                })

                if (exist) {
                  $scope.removeParam(exist)
                }
              }
            }

            $scope.dateOptions = {
              // formatYear: 'yy',
              showWeeks: false,
              format: 'yyyy-MM-dd'
            }

            $scope.onDateTimeSet = function (newDate, oldDate) {
              $scope.dateTimeValue = CommonUtil.formatDateTime2(newDate)
              $scope.push.datetime = newDate
            }

            $scope.scheduledDate = {
              opened: false,
              open: function ($event) {
                $event.preventDefault()
                $event.stopPropagation()

                this.opened = true
              }
            }

            $scope.isInitializing = true

            $scope.fetch = function (callback) {
              async.parallel({
                templates: function (callback) {
                  var query = new $bend.Query()
                  BendUtils.fetch('pushTemplate', query, {}, callback)
                }
              }, callback)
            }

            $scope.initialize = function () {
              $scope.fetch(function (err, data) {
                if (err) {
                  $scope.isInitializing = false
                  console.log('Error while fetch package data!')
                  console.log(err)
                  return
                }
                $scope.templates = data.templates
                console.log(data)

                $scope.isInitializing = false
              })
            }

            $scope.initialize()

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }

            $scope.proceed = function () {
              var ret = _.clone($scope.push)
              delete ret.$$hashKey
              if (ret.params) {
                ret.params.map(function (o) {
                  delete o.$$hashKey
                })
              }
              if (ret.datetime) {
                var dateStr = moment(ret.datetime).format('YYYY-MM-DD HH:mm:ss')
                var dateStrWithTimeZone = moment.tz(dateStr, ret.timezone).format()
                ret.datetime = new Date(dateStrWithTimeZone).getTime() * 1000000
              }
              ret.audience = {
                defaultQuery: $scope.audience.defaultQuery,
                audiences: []
              }
              _.map($scope.formData.audiences, function (o) {
                ret.audience.audiences.push(o.query['community._id'])
              })

              ret.group = BendAuth.getActiveUser().community._id

              BendUtils.wrapInCallback($bend.execute('save-and-send-notification', {
                obj: ret
              }), function (error, result) {
                if (error) {
                  console.log(error)
                  return
                }

                $modalInstance.close(result)
              })
            }

            $scope.sendToUser = function (username) {
              $scope.isUserSending = true
              var obj = angular.copy($scope.push)
              // var param = {
              //   obj: obj,
              //   username: username
              // }
              // console.log(param);
              BendUtils.wrapInCallback($bend.execute('save-and-send-notification', {
                obj: obj,
                username: username
              }), function (error, result) {
                $scope.isUserSending = false
                if (error) {
                  console.log(result)
                }
              })
            }

            $scope.validateTextLength = function () {
              return $scope.textLimit - $scope.push.text.length > 0
            }

            $scope.validateUserQuery = function () {
              $scope.userQueryError = CommonUtil.jsonValidate($scope.push.userQuery)
              return !$scope.userQueryError
            }

            $scope.usersCount = null
            $scope.runQuery = function () {
              if ($scope.validateUserQuery() && $scope.push && $scope.push.userQuery) {
                var query = new $bend.Query({filter: JSON.parse($scope.push.userQuery)})
                query.notEqualTo('deleted', true).equalTo('enabled', true)

                var qq
                if ($scope.formData.audiences.length > 0) {
                  var filter = _.clone($scope.formData.audiences[0].query)
                  qq = new $bend.Query({filter: filter})
                  _.map($scope.formData.audiences, function (o, idx) {
                    if (idx > 0) { qq.or({filter: _.clone(o.query)}) }
                  })
                } else {
                  qq = new $bend.Query({filter: _.clone($scope.audience.defaultQuery)})
                }

                query.and(qq)
                BendUtils.count('user', query, function (err, res) {
                  console.log('count', res, $scope.formData.audiences)
                  $scope.usersCount = res
                })
              } else {
                $scope.usersCount = null
              }
            }

            $scope.$watch('formData.audiences.length', function () {
              console.log('here')
              $scope.runQuery()
            }, true)

            $scope.validate = function () {
              return $scope.push.text && $scope.validateTextLength() && $scope.validateUserQuery()
            }
          }
        })

        return modalInstance.result
      }
    }])
