'use strict'

angular.module('app.controllers')
  .controller('ActivityEditController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$http', 'pinesNotifications',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $http, pinesNotifications) {
      // Init.
      $scope.CommonUtil = CommonUtil
      $scope.activityId = null
      if ($routeParams.id != 'new') { $scope.activityId = $routeParams.id }
      $scope.isLoading = true
      $scope.communityList = []
      $scope.businessList = []
      $scope.actionList = []
      $scope.eventList = []
      $scope.volunteeringList = []
      $scope.serviceList = []
      $scope.activityList = []
      $scope.TypeList = ['action', 'business', 'event', 'volunteer_opportunity']
      $scope.userList = []
      $scope.formData = {
        communities: []
      }
      $scope.filter = {
        user: '',
        activity: ''
      }
      $scope.username = ''
      $scope.activity = {
        community: ''
      }
      $scope.community = ''
      async.parallel([
        function (callback) {
          BendService.getUserList(function (rets) {
            $scope.userList = rets
            callback(null, null)
          })
        }, function (callback) {
          BendService.getCommunityList(function (rets) {
            $scope.communityList = rets
            callback(null, null)
          })
        }
      ], function (err, rets) {
        if (err) {
          console.log(err)
          return
        }
        if ($scope.activityId) {
          BendService.getActivity($scope.activityId, function (ret) {
            $scope.activity = ret

            if ($scope.activity.activity) {
              $scope.activityList = []
              $scope.activityList.push($scope.activity.activity)
              $scope.filter.activity = $scope.activity.activity
            }

            if ($scope.activity.user) {
              _.find($scope.userList, function (u) {
                if (u._id == $scope.activity.user._id) {
                  $scope.filter.user = u
                }
              })
            } else {
              $scope.activity.user = ''
            }

            if ($scope.activity.community) {
              _.find($scope.communityList, function (com) {
                if (com._id == $scope.activity.community._id) {
                  $scope.activity.community = com._id
                  $scope.community = com.name
                }
              })
            } else {
              $scope.activity.community = ''
            }
            $scope.isLoading = false
          })
        }
      })

      $scope.refreshActivity = function (keyword) {
        if (!$scope.activity.type) return
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        if (keyword != '') { query.matches('name', new RegExp(keyword + '+', 'gi')) }
        query.ascending('name')
        query.limit(30)

        $bend.DataStore.find($scope.activity.type, query).then(function (rets) {
          $scope.activityList = rets
        }, function (err) {
          console.log(err)
        })
      }

      $scope.onChangeUser = function () {
        if ($scope.filter.user) {
          $scope.activity.community = $scope.filter.user.community._id
          if ($scope.activity.community) {
            _.find($scope.communityList, function (com) {
              if (com._id == $scope.activity.community) {
                $scope.community = com.name
              }
            })
          } else {
            $scope.activity.community = ''
          }
        } else {
          $scope.activity.community = ''
        }
      }

      $scope.onChangeActivity = function () {
        if ($scope.filter.activity) {
          $scope.activity.summary = $scope.filter.activity.name
          $scope.activity.points = $scope.filter.activity.points || 0
          $scope.activity.link = $scope.filter.activity.url
        } else {
          $scope.activity.summary = ''
          $scope.activity.points = 0
        }
      }

      $scope.onChangeType = function () {
        $scope.activityList = []
        $scope.filter.activity = ''
      }
      $scope.saveActivityDo = function () {
        if ($scope.activity.type) {
          if ($scope.filter.activity != '') {
            $scope.activity.activity = CommonUtil.makeBendRef($scope.filter.activity._id, $scope.activity.type)
          } else {
            delete $scope.activity.activity
          }
        }

        if ($scope.filter.user) {
          $scope.activity.user = CommonUtil.makeBendRef($scope.filter.user._id, 'user')
        } else {
          delete $scope.activity.user
        }

        if ($scope.activity.community != '') {
          $scope.activity.community = CommonUtil.makeBendRef($scope.activity.community, 'community')
        } else {
          delete $scope.activity.community
        }

        var activityData = _.clone($scope.activity)

        if ($scope.activityId) {
          delete activityData.$$hashKey
          BendService.updateActivity(activityData, function (ret) {
            $location.path('/activities')
          })
        } else {
          BendService.createActivity(activityData, function (ret) {
            $location.path('/activities')
          })
        }
      }
    }])
