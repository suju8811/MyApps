'use strict'
angular.module('app.controllers')
  .controller('CertificationController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal) {
      // Init.
      $scope.certificationList = []
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil

      BendService.getCertificationList(function (rets) {
        $scope.certificationList = rets
        $scope.isLoading = false
      })

      $scope.deleteCertification = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Deleting the certification is permanent and can not be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deleteCertification(id, function (ret) {
              if (ret) {
                for (var i = 0; i < $scope.certificationList.length; i++) {
                  if ($scope.certificationList[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.certificationList.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $scope.editCertification = function (certification) {
        $scope.openEditCertification(certification)
      }
      $scope.createCertification = function () {
        var certificationObj = {
          enabled: true
        }
        $scope.openEditCertification(certificationObj)
      }

      $scope.openEditCertification = function (certificationObj) {
        $modal.open({
          templateUrl: 'editCertification.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance, certificationObj, certificationList) {
            $scope.certification = _.clone(certificationObj)
            $scope.CommonUtil = CommonUtil
            $scope.fileProgress = []
            $scope.isUploading = []
            $scope.typeList = ['Business', 'Event']
            // $scope.typeValue = {"Business":1, "Event":2};
            // document.getElementById('typeSelect').value = $scope.source.type;

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.deleteFile = function (attr, $ev) {
              if ($ev) {
                $ev.stopPropagation()
                $ev.preventDefault()
              }
              delete $scope.certification[attr]
            }

            $scope.selectFileOpen = function ($event) {
              if ($event.target.tagName == 'INPUT') { return }

              var obj = $event.target
              setTimeout(function () {
                $(obj).parents('.upload-file-container').find("input[type='file']").eq(0).click()
              }, 0)
            }

            $scope.showFileLoading = function (tag, bShow) {
              $scope.isUploading[tag] = bShow
            }

            $scope.onFileUpload = function (files, attr) {
              var file = files[0]
              if (!CommonUtil.checkFileType(file.type, 'image')) { return }

              $scope.form.validateForm.$setDirty()

              $scope.fileProgress[attr] = 0
              $scope.showFileLoading(attr, true)
              BendService.upload(file, function (error, uploadedFile) {
                if (error) {
                  $scope.showFileLoading(attr, false)
                  return
                }

                BendService.getFile(uploadedFile, function (o) {
                  applyChangesOnScope($scope, function () {
                    $scope.certification[attr] = o
                    $scope.showFileLoading(attr, false)
                  })
                })
              }, {
                _workflow: 'avatar'
              }, function (total, prog) {
                applyChangesOnScope($scope, function () {
                  $scope.fileProgress[attr] = prog * 100 / total
                })
              })
            }
            $scope.saveCertificationDo = function () {
              var certificationData = _.clone($scope.certification)
              if (certificationData.badge) {
                certificationData.badge = CommonUtil.makeBendFile(certificationData.badge._id)
              }
              if (certificationData._id) {
                delete certificationData.$$hashKey
                BendService.updateCertification(certificationData, function (ret) {
                  var ref = _.find(certificationList, function (o) {
                    return o._id == ret._id
                  })

                  _.extend(ref, $scope.certification)
                  $modalInstance.dismiss('cancel')
                })
              } else {
                BendService.createCertification(certificationData, function (ret) {
                  $scope.certification._id = ret._id
                  certificationList.push($scope.certification)
                  $modalInstance.dismiss('cancel')
                })
              }
            }

            $scope.canSubmitValidationForm = function () {
              return $scope.form.validateForm.$valid
            }
          },
          resolve: {
            certificationObj: function () {
              return certificationObj
            },
            certificationList: function () {
              return $scope.certificationList
            }
          }
        })
      }
    }])
