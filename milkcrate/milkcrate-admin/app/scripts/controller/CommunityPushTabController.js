'use strict'

angular.module('app.controllers')
  .controller('CommunityPushTabController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout', 'pinesNotifications',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout, pinesNotifications) {
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.pushConfig = {}
      $scope.fileProgress = []
      $scope.isUploading = []
      $scope.communityId = $routeParams.id

      $scope.form = {}
      console.log('$scope.communityId', $scope.communityId)

      BendService.getCommunityPushConfig($scope.communityId, function (err, ret) {
        if (!err) { $scope.pushConfig = ret || {} }
        console.log('pushConfig', $scope.pushConfig)
      })

      $scope.saveDo = function () {
        var data = _.clone($scope.pushConfig)
        if (data.appleCertificate) {
          data.appleCertificate = CommonUtil.makeBendFile(data.appleCertificate._id)
        }
        data.community = CommonUtil.makeBendRef($scope.communityId, 'community')
        BendService.saveCommunityPushConfig(data, function (err, ret) {
          if (!err) {
            var notify = pinesNotifications.notify({
              text: '<p>Saved successfully.</p>',
              type: 'info',
              width: '200px'
            })
            setTimeout(function () {
              notify.remove()
            }, 2000)
          }
        })
      }

      $scope.canSubmitValidationForm = function () {
        return $scope.form.validateForm.$valid
      }

      $scope.openFile = function (fileName) {
        $("input[name='" + fileName + "']").click()
      }

      $scope.deleteFile = function (tag, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        applyChangesOnScope($scope, function () {
          delete $scope.pushConfig[tag]
        })

        $scope.form.validateForm.$setDirty()
      }

      $scope.showFileLoading = function (tag, bShow) {
        $scope.isUploading[tag] = bShow
      }

      $scope.selectFileOpen = function (fileId, $ev) {
        if ($ev.target.tagName == 'DIV') {
          setTimeout(function () {
            $('#' + fileId).click()
          }, 0)
        }
      }

      $scope.onFileUpload = function (files, tag) {
        var file = files[0]

        $scope.fileProgress[tag] = 0
        $scope.showFileLoading(tag, true)
        BendService.upload(file, function (error, uploadedFile) {
          if (error) {
            $scope.showFileLoading(tag, false)
            return
          }

          BendService.getFile(uploadedFile, function (o) {
            $scope.pushConfig[tag] = o
            $scope.showFileLoading(tag, false)
          })
          $scope.form.validateForm.$setDirty()
        }, null, function (total, prog) {
          applyChangesOnScope($scope, function () {
            $scope.fileProgress[tag] = prog * 100 / total
          })
        })
      }
    }])
