'use strict'

angular.module('app.controllers')
  .controller('TeamController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout) {
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.teamList = []

      $scope.communityList = []

      $scope.filter = {
        community: (CommonUtil.getStorage('team-filter') ? CommonUtil.getStorage('team-filter').community : '')
      }

      $scope.user = BendAuth.getActiveUser()

      $scope.getTeamList = function () {
        CommonUtil.setStorage('team-filter', $scope.filter)
        $scope.isLoading = true
        var query = new $bend.Query()

        query.notEqualTo('deleted', true)

        if ($scope.filter.community && $scope.filter.community._id != null) {
          query.equalTo('community._id', $scope.filter.community._id)
        }

        if ($scope.user.communityAdmin) {
          query.equalTo('community._id', $scope.user.community._id)
        }
        query.ascending('position')

        console.log('$scope.user.community._id', $scope.user.community._id)

        $bend.DataStore.find('team', query, {
          relations: {
            community: 'community'
          }
        }).then(function (rets) {
          $scope.teamList = rets
          $scope.isLoading = false

          rets.map(function (o) {
            BendService.getTeamUserCount(o._id, function (err, count) {
              if (!err) o.userCount = count
            })
          })

          setTimeout(function () {
            $scope.enableSortable()
          }, 100)
        }, function (err) {
          console.log(err)
        })
      }

      if ($scope.user.communityAdmin) { $scope.getTeamList() } else {
        BendService.getCommunityList(function (rets) {
          $scope.communityList = $scope.communityList.concat(rets)
          $scope.filter.community = $scope.communityList[0]

          $scope.getTeamList()
        })
      }

      $scope.deleteTeam = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Deleting the team is permanent and can not be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deleteTeam(id, function (err, ret) {
              if (err) {
                console.log(err); return
              }
              if (ret) {
                for (var i = 0; i < $scope.teamList.length; i++) {
                  if ($scope.teamList[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.teamList.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $scope.editTeam = function (team, $event) {
        if ($event.metaKey || $event.ctrlKey) {
          window.open('#/teams/' + team._id, '_blank')
        } else { return $location.path('/teams/' + team._id) }
      }

      $scope.createTeam = function ($event) {
        if ($event.metaKey || $event.ctrlKey) {
          window.open('#/teams/new', '_blank')
        } else { return $location.path('/teams/new') }
      }

      // var oldPosList = []
      var oldTeamPosList = []
      $scope.enableSortable = function () {
        $('#teams-container').sortable({
          opacity: 0.5,
          dropOnEmpty: true,
          start: function (event, ui) {
            ui.item[0].style.backgroundColor = '#eee'
            oldTeamPosList = []
            $scope.teamList.map(function (o) {
              oldTeamPosList[o._id] = o.position
            })
          },
          update: function (event, ui) {
            // console.log('content', $(".team-content"));
            $('.team-content').each(function (idx) {
              var teamKey = $(this).find('#teamId-input')[0].value
              var team = _.find($scope.teamList, function (_o) {
                return _o._id == teamKey
              })
              if (team.position != idx + 1) {
                team.position = idx + 1
                // update team
                BendService.updateTeam(team, function (err, ret) {
                  // console.log("team update", err, ret)
                })
              }
            })

            applyChangesOnScope($scope, function () {
              $scope.teamList = _.sortBy($scope.teamList, function (o) {
                return o.position
              })
            })
            ui.item[0].style.backgroundColor = ''
          }
        })
        $('#teams-container').sortable('option', 'disabled', false)
      }
    }])
