'use strict'

angular.module('app.controllers')
  .controller('ActionEditController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$http', 'pinesNotifications',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $http, pinesNotifications) {
      // Init.
      if ($routeParams.id != 'new') { $scope.actionId = $routeParams.id }
      $scope.action = {
        repeatable: true
      }
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.tagList = []
      $scope.categoryList = []
      $scope.collectionList = []
      $scope.communityList = []
      $scope.isUploading = []
      $scope.fileProgress = []
      $scope.formData = {
        categories: [],
        collections: [],
        intervalminute: 0,
        intervalhour: 0,
        intervalday: 0
      }
      $scope.form = {}
      $scope.user = BendAuth.getActiveUser()

      async.parallel([
        function (callback) {
          BendService.getCommunityList(function (rets) {
            $scope.communityList = rets
            callback(null, null)
          })
        }, function (callback) {
          BendService.getCategoryList(function (rets) {
            $scope.categoryList = rets
            callback(null, null)
          })
        }, function (callback) {
          BendService.getCollectionEnabledList(function (rets) {
            $scope.collectionList = rets
            callback(null, null)
          })
        }, function (callback) {
          BendService.getCertificationList(function (rets) {
            $scope.certificationList = rets
            callback(null, null)
          })
        }], function (err, rets) {
        if (err) {
          console.log(err)
          return
        }
        if ($scope.actionId) {
          BendService.getAction($scope.actionId, function (ret) {
            ret.enabled = (ret.enabled !== false)
            $scope.action = ret
            if ($scope.action.repeatInterval) {
              $scope.formData.intervalday = parseInt($scope.action.repeatInterval / (24 * 3600))
              $scope.formData.intervalhour = parseInt(($scope.action.repeatInterval - $scope.formData.intervalday * (24 * 3600)) / (3600))
              $scope.formData.intervalminute = parseInt(($scope.action.repeatInterval - (($scope.formData.intervalday * (24 * 3600)) + $scope.formData.intervalhour * 3600)) / 60)
            }
            if ($scope.action.community) {
              $scope.action.community = $scope.action.community._id
            } else {
              delete $scope.action.community
            }

            if ($scope.action.categories && $scope.action.categories.length > 0) {
              // console.log($scope.categoryList);
              $scope.formData.categories = _.filter($scope.categoryList, function (o) {
                return $scope.action.categories.indexOf(o._id) != -1
              })
            }

            if ($scope.action.collections && $scope.action.collections.length > 0) {
              // console.log($scope.collectionList);
              $scope.formData.collections = _.filter($scope.collectionList, function (o) {
                return $scope.action.collections.indexOf(o._id) != -1
              })
              // console.log($scope.formData.categories);
            }

            $scope.isLoading = false
          })
        }
      })

      $scope.cancel = function () {
        // $modalInstance.dismiss('cancel')
      }
      $scope.close = function () {
        // $modalInstance.dismiss('cancel')
      }

      $scope.saveActionDo = function () {
        var repeatInterval = 0
        // console.log($scope.formData.intervalminute)
        if ($scope.formData.intervalday != 0) {
          repeatInterval = $scope.formData.intervalday * (24 * 3600)
        }
        if ($scope.formData.intervalhour != 0) {
          repeatInterval = repeatInterval + $scope.formData.intervalhour * (3600)
        }
        if ($scope.formData.intervalminute != 0) {
          repeatInterval = repeatInterval + $scope.formData.intervalminute * (60)
        }
        // console.log('repeatInterval',repeatInterval);

        if (repeatInterval > 0) {
          $scope.action.repeatInterval = repeatInterval
        }

        if ($scope.action.community && $scope.action.community != '') {
          $scope.action.community = CommonUtil.makeBendRef($scope.action.community, 'community')
        } else {
          delete $scope.action.community
        }

        if ($scope.formData.categories.length > 0) {
          $scope.action.categories = CommonUtil.getIdList($scope.formData.categories)
        } else {
          delete $scope.action.categories
        }

        if ($scope.formData.collections.length > 0) {
          $scope.action.collections = CommonUtil.getIdList($scope.formData.collections)
        } else {
          delete $scope.action.collections
        }

        if ($scope.action.startMonth && $scope.action.startMonth != '') {
          $scope.action.startMonth = Number($scope.action.startMonth)
        } else {
          delete $scope.action.startMonth
        }
        if ($scope.action.startDay && $scope.action.startDay != '') {
          $scope.action.startDay = Number($scope.action.startDay)
        } else {
          delete $scope.action.startDay
        }

        if ($scope.action.endMonth && $scope.action.endMonth != '') {
          $scope.action.endMonth = Number($scope.action.endMonth)
        } else {
          delete $scope.action.endMonth
        }
        if ($scope.action.endDay && $scope.action.endDay != '') {
          $scope.action.endDay = Number($scope.action.endDay)
        } else {
          delete $scope.action.endDay
        }

        if ($scope.action._id) {
          delete $scope.action.$$hashKey
          BendService.updateAction($scope.action, function (ret) {
            $location.path('/actions')
          })
        } else {
          BendService.createAction($scope.action, function (ret) {
            $location.path('/actions')
          })
        }
      }
      $scope.deleteFile = function (tag, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        applyChangesOnScope($scope, function () {
          $scope.action.coverImage = null
          delete $scope.action.coverImage
        })
      }

      $scope.showFileLoading = function (tag, bShow) {
        $scope.isUploading[tag] = bShow
      }

      $scope.selectFileOpen = function (fileId, $ev) {
        if ($ev.target.tagName == 'DIV') {
          setTimeout(function () {
            $('#' + fileId).click()
          }, 0)
        }
      }

      $scope.onFileUpload = function (files, tag) {
        var file = files[0]
        $scope.fileProgress[tag] = 0
        $scope.showFileLoading(tag, true)
        var reader = new FileReader()
        reader.onload = function (e) {
          $scope.uploadingFileUrl = e.target.result
        }
        reader.readAsDataURL(file)

        BendService.upload(file, function (error, uploadedFile) {
          if (error) {
            $scope.uploadingFileUrl = null
            $scope.showFileLoading(tag, false)
            return
          }
          BendService.getFile(uploadedFile, function (o) {
            $scope.action.coverImage = o
            $scope.showFileLoading(tag, false)
            $scope.form.validateForm.$dirty = true
            $scope.uploadingFileUrl = null
          })
        }, {
          _workflow: 'coverPhoto'
        }, function (total, prog) {
          applyChangesOnScope($scope, function () {
            $scope.fileProgress[tag] = prog * 100 / total
          })
        })
      }
      $rootScope.fileUploadFromSearch2 = function (file, tag) {
        file._filename = Date.now() + ''
        var files = []
        files.push(file)
        $scope.onFileUpload(files, tag)
      }

      $scope.searchImages2 = function ($ev, tag) {
        $ev.stopPropagation()
        $ev.preventDefault()
        console.log(22)
        $modal.open({
          templateUrl: 'views/templates/searchImages.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance) {
            $scope.CommonUtil = CommonUtil
            $scope.searchResults = 0
            $scope.pages = 1
            $scope.showLoadMore = false
            // var cacheSearchKey = ''

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }

            $scope.searchImages = function (searchVal) {
              // console.log('searchVal', searchVal)
              // cacheSearchKey = searchVal

              $scope.pages = 1
              if (searchVal) {
                var URL = 'https://pixabay.com/api/?key=2706353-ee3016f8af51ca406a8c8a3db&q=' + encodeURIComponent(searchVal) + '&image_type=photo&page=' + $scope.pages
                $.getJSON(URL, function (data) {
                  console.log(data)
                  if (parseInt(data.totalHits) > 0) {
                    applyChangesOnScope($scope, function () {
                      if (data.totalHits > $scope.pages * 20) { $scope.showLoadMore = true } else { $scope.showLoadMore = false }

                      $scope.searchResults = data.hits
                    })
                  } else { console.log('No hits') }
                })
              }
            }

            $scope.searchImagesMore = function (tabIdx, searchVal) {
              $scope.pages++
              if (searchVal) {
                var URL = 'https://pixabay.com/api/?key=2706353-ee3016f8af51ca406a8c8a3db&q=' + encodeURIComponent(searchVal) + '&image_type=photo&page=' + $scope.pages
                $.getJSON(URL, function (data) {
                  console.log(data)
                  if (parseInt(data.totalHits) > 0) {
                    applyChangesOnScope($scope, function () {
                      if (data.totalHits > $scope.pages * 20) { $scope.showLoadMore = true } else { $scope.showLoadMore = false }

                      $scope.searchResults = $scope.searchResults.concat(data.hits)
                    })
                  } else { console.log('No hits') }
                })
              }
            }

            $scope.selectImage = function (searchItem) {
              $scope.isDownloading = true
              console.log(searchItem)
              var imageUrl = ''
              imageUrl = searchItem.webformatURL

              var xhr = new XMLHttpRequest()
              xhr.open('GET', imageUrl, true)
              xhr.responseType = 'blob'
              xhr.onload = function (e) {
                if (this.status == 200) {
                  var myBlob = this.response
                  console.log('blob', myBlob)
                  $rootScope.fileUploadFromSearch2(myBlob, tag)
                  // myBlob is now the blob that the object URL pointed to.
                  $scope.cancel()
                }
              }
              xhr.send()
            }
          }
        })
      }
    }])
