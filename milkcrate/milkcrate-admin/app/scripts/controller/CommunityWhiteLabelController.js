'use strict'

angular.module('app.controllers')
  .controller('CommunityWhiteLabelController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout', 'pinesNotifications',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout, pinesNotifications) {
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.clientAppConfig = {}
      $scope.fileProgress = []
      $scope.isUploading = []
      $scope.communityId = $routeParams.id

      $scope.form = {}

      console.log('$scope.communityId', $scope.communityId)

      BendService.getCommunityAppConfig($scope.communityId, function (err, ret) {
        if (!err) { $scope.clientAppConfig = ret || {} }
        console.log('clientAppConfig', $scope.clientAppConfig)
      })

      $scope.saveDo = function () {
        var data = _.clone($scope.clientAppConfig)
        if (data.welcomeBackground1) {
          data.welcomeBackground1 = CommonUtil.makeBendFile(data.welcomeBackground1._id)
        }
        if (data.welcomeBackground2) {
          data.welcomeBackground2 = CommonUtil.makeBendFile(data.welcomeBackground2._id)
        }
        if (data.welcomeBackground3) {
          data.welcomeBackground3 = CommonUtil.makeBendFile(data.welcomeBackground3._id)
        }
        if (data.welcomeBackground4) {
          data.welcomeBackground4 = CommonUtil.makeBendFile(data.welcomeBackground4._id)
        }
        if (data.welcomeBackground5) {
          data.welcomeBackground5 = CommonUtil.makeBendFile(data.welcomeBackground5._id)
        }
        if (data.welcomeBackground6) {
          data.welcomeBackground6 = CommonUtil.makeBendFile(data.welcomeBackground6._id)
        }
        if (data.iosAppIcon) {
          data.iosAppIcon = CommonUtil.makeBendFile(data.iosAppIcon._id)
        }
        if (data.iosScreenshot1) {
          data.iosScreenshot1 = CommonUtil.makeBendFile(data.iosScreenshot1._id)
        }
        if (data.iosScreenshot2) {
          data.iosScreenshot2 = CommonUtil.makeBendFile(data.iosScreenshot2._id)
        }
        if (data.iosScreenshot3) {
          data.iosScreenshot3 = CommonUtil.makeBendFile(data.iosScreenshot3._id)
        }
        if (data.iosScreenshot4) {
          data.iosScreenshot4 = CommonUtil.makeBendFile(data.iosScreenshot4._id)
        }
        if (data.iosScreenshot5) {
          data.iosScreenshot5 = CommonUtil.makeBendFile(data.iosScreenshot5._id)
        }
        if (data.androidAppIcon) {
          data.androidAppIcon = CommonUtil.makeBendFile(data.androidAppIcon._id)
        }
        if (data.androidScreenshot1) {
          data.androidScreenshot1 = CommonUtil.makeBendFile(data.androidScreenshot1._id)
        }
        if (data.androidScreenshot2) {
          data.androidScreenshot2 = CommonUtil.makeBendFile(data.androidScreenshot2._id)
        }
        if (data.androidScreenshot3) {
          data.androidScreenshot3 = CommonUtil.makeBendFile(data.androidScreenshot3._id)
        }
        if (data.androidScreenshot4) {
          data.androidScreenshot4 = CommonUtil.makeBendFile(data.androidScreenshot4._id)
        }
        if (data.androidScreenshot5) {
          data.androidScreenshot5 = CommonUtil.makeBendFile(data.androidScreenshot5._id)
        }
        if (data.androidCoverImage) {
          data.androidCoverImage = CommonUtil.makeBendFile(data.androidCoverImage._id)
        }
        data.community = CommonUtil.makeBendRef($scope.communityId, 'community')
        BendService.saveCommunityAppConfig(data, function (err, ret) {
          if (!err) {
            var notify = pinesNotifications.notify({
              text: '<p>Saved successfully.</p>',
              type: 'info',
              width: '200px'
            })
            setTimeout(function () {
              notify.remove()
            }, 2000)
          }
        })
      }

      $scope.canSubmitValidationForm = function () {
        return $scope.form.validateForm.$valid
      }

      $scope.openFile = function (fileName) {
        $("input[name='" + fileName + "']").click()
      }

      $scope.deleteFile = function (tag, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        applyChangesOnScope($scope, function () {
          delete $scope.clientAppConfig[tag]
        })

        $scope.form.validateForm.$setDirty()
      }

      $scope.showFileLoading = function (tag, bShow) {
        $scope.isUploading[tag] = bShow
      }

      $scope.selectFileOpen = function (fileId, $ev) {
        if ($ev.target.tagName == 'DIV') {
          setTimeout(function () {
            $('#' + fileId).click()
          }, 0)
        }
      }

      $scope.onFileUpload = function (files, tag) {
        var file = files[0]

        $scope.fileProgress[tag] = 0
        $scope.showFileLoading(tag, true)
        BendService.upload(file, function (error, uploadedFile) {
          if (error) {
            $scope.showFileLoading(tag, false)
            return
          }

          BendService.getFile(uploadedFile, function (o) {
            $scope.clientAppConfig[tag] = o
            $scope.showFileLoading(tag, false)
          })
          $scope.form.validateForm.$setDirty()
        }, null, function (total, prog) {
          applyChangesOnScope($scope, function () {
            $scope.fileProgress[tag] = prog * 100 / total
          })
        })
      }
    }])
