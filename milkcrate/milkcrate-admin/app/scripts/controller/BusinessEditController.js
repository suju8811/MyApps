'use strict'

angular.module('app.controllers')
  .controller('BusinessEditController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$http', 'pinesNotifications',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $http, pinesNotifications) {
      // Init.
      $scope.CommonUtil = CommonUtil
      if ($routeParams.id != 'new') { $scope.bussinessId = $routeParams.id }
      $scope.business = {
        repeatable: true,
        points: 5,
        hours: []
      }

      $scope.isLoading = true
      $scope.isUploading = []
      $scope.fileProgress = []
      $scope.tagList = []
      $scope.categoryList = []
      $scope.collectionList = []
      $scope.communityList = []
      $scope.foursquareTagsList = []
      $scope.stateList = CommonUtil.AllStates
      $scope.Tags = []
      $scope.FoursquareTags = []
      $scope.formData = {
        categories: [],
        collections: [],
        certifications: [],
        intervalminute: 0,
        intervalhour: 0,
        intervalday: 0
      }
      $scope.certificationList = []
      $scope.state = $scope.stateList[0]
      $scope.add_count = 0

      $scope.IsEmptyTimes = true
      $scope.user = BendAuth.getActiveUser()
      $scope.form = {}

      async.parallel([
        function (callback) {
          BendService.getCommunityList(function (rets) {
            $scope.communityList = rets
            callback(null, null)
          })
        }, function (callback) {
          BendService.getCategoryList(function (rets) {
            $scope.categoryList = rets
            // console.log($scope.categoryList);
            callback(null, null)
          })
        }, function (callback) {
          BendService.getCollectionEnabledList(function (rets) {
            $scope.collectionList = rets
            // console.log($scope.collectionList);
            callback(null, null)
          })
        }, function (callback) {
          BendService.getCertificationList(function (rets) {
            $scope.certificationList = rets
            callback(null, null)
          })
        }], function (err, rets) {
        if (err) {
          console.log(err)
          return
        }
        if ($scope.bussinessId) {
          BendService.getBusiness($scope.bussinessId, function (ret) {
            ret.enabled = (ret.enabled !== false)
            $scope.business = ret
            $scope.business.points = $scope.business.points || 5
            // console.log("business", $scope.business)
            $scope.isLoading = false
            if ($scope.business.repeatInterval) {
              $scope.formData.intervalday = parseInt($scope.business.repeatInterval / (24 * 3600))
              $scope.formData.intervalhour = parseInt(($scope.business.repeatInterval - $scope.formData.intervalday * (24 * 3600)) / (3600))
              $scope.formData.intervalminute = parseInt(($scope.business.repeatInterval - (($scope.formData.intervalday * (24 * 3600)) + $scope.formData.intervalhour * 3600)) / 60)
            }
            if ($scope.business.hours) { $scope.IsEmptyTimes = ($scope.business.hours.length > 0) }
            if ($scope.business.state) {
              $scope.state = $scope.business.state
            }
            if ($scope.business.categories && $scope.business.categories.length > 0) {
              // console.log($scope.categoryList);
              $scope.formData.categories = _.filter($scope.categoryList, function (o) {
                return $scope.business.categories.indexOf(o._id) != -1
              })
            }

            if ($scope.business.collections && $scope.business.collections.length > 0) {
              $scope.formData.collections = _.filter($scope.collectionList, function (o) {
                return $scope.business.collections.indexOf(o._id) != -1
              })
            }

            if ($scope.business.certifications && $scope.business.certifications.length > 0) {
              $scope.formData.certifications = _.filter($scope.certificationList, function (o) {
                return $scope.business.certifications.indexOf(o._id) != -1
              })
            }

            if ($scope.business.community) {
              $scope.business.community = $scope.business.community._id
            } else {
              $scope.business.community = ''
            }
            $scope.isLoading = false
          })
        }
      })

      $scope.RemoveHour = function (obj, e) {
        if (e) {
          e.preventDefault()
          e.stopPropagation()
        }

        var idx = $scope.business.hours.indexOf(obj)
        $scope.business.hours.splice(idx, 1)
        $scope.IsEmptyTimes = ($scope.business.hours.length > 0)
        $scope.form.validateForm.$setDirty()
      }

      $scope.getDays = function (hours) {
        return hours['days']
      }

      $scope.getOpenTimes = function (hours) {
        var time = []
        for (var j = 0; j < hours['open'].length; j++) {
          var fromuntil = CommonUtil.convertWithAMPM(hours['open'][j]['from']) + '-' + CommonUtil.convertWithAMPM(hours['open'][j]['until'])
          time.push(fromuntil)
        }

        return time.join(', ')
      }

      $scope.AddHours = function () {
        if (!$scope.business.hours) {
          $scope.business.hours = []
        }

        $scope.business.hours.push({
          'days': '',
          'open': []
        })
        $scope.IsEmptyTimes = ($scope.business.hours.length > 0)
        $scope.form.validateForm.$setDirty()
      }

      $scope.onChange = function () {
        $scope.form.validateForm.$setDirty()
      }

      $scope.deleteFile = function (tag, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        applyChangesOnScope($scope, function () {
          delete $scope.business.coverImage
        })
      }

      $scope.showFileLoading = function (tag, bShow) {
        $scope.isUploading[tag] = bShow
      }

      $scope.selectFileOpen = function (fileId, $ev) {
        if ($ev.target.tagName == 'DIV') {
          setTimeout(function () {
            $('#' + fileId).click()
          }, 0)
        }
      }

      $scope.onFileUpload = function (files, tag) {
        var file = files[0]
        $scope.fileProgress[tag] = 0
        $scope.showFileLoading(tag, true)
        var reader = new FileReader()
        reader.onload = function (e) {
          $scope.uploadingFileUrl = e.target.result
        }
        reader.readAsDataURL(file)

        BendService.upload(file, function (error, uploadedFile) {
          // console.log("uploadedFile", uploadedFile);
          if (error) {
            $scope.uploadingFileUrl = null

            $scope.showFileLoading(tag, false)
            return
          }
          // console.log(uploadedFile);
          BendService.getFile(uploadedFile, function (o) {
            $scope.uploadingFileUrl = null

            $scope.business.coverImage = o
            $scope.showFileLoading(tag, false)
            $scope.form.validateForm.$dirty = true
          })
        }, {
          _workflow: 'coverPhoto'
        }, function (total, prog) {
          applyChangesOnScope($scope, function () {
            $scope.fileProgress[tag] = prog * 100 / total
          })
        })
      }

      $scope.saveBusinessDo = function () {
        // set points as number
        $scope.business.points = Number($scope.business.points)
        var repeatInterval = 0
        if ($scope.formData.intervalday != 0) {
          repeatInterval = $scope.formData.intervalday * (24 * 3600)
        }
        if ($scope.formData.intervalhour != 0) {
          repeatInterval = repeatInterval + $scope.formData.intervalhour * (3600)
        }
        if ($scope.formData.intervalminute != 0) {
          repeatInterval = repeatInterval + $scope.formData.intervalminute * (60)
        }

        if (repeatInterval > 0) {
          $scope.business.repeatInterval = repeatInterval
        }

        if ($scope.business.community && $scope.business.community != '') {
          $scope.business.community = CommonUtil.makeBendRef($scope.business.community, 'community')
        } else {
          delete $scope.business.community
        }

        if ($scope.business._geoloc && $scope.business._geoloc[0] && $scope.business._geoloc[1] && $scope.business._geoloc[0] != '' && $scope.business._geoloc[1] != '') { $scope.business._geoloc = [parseFloat($scope.business._geoloc[0]), parseFloat($scope.business._geoloc[1])] } else { delete $scope.business._geoloc }

        // if ($scope.business.hours)
        var days = document.getElementsByName('days-input')
        var times = document.getElementsByName('times-input')
        var hours = []
        for (var i = 0; i < days.length; i++) {
          var open = []
          var time = times[i].value
          var timeArray = time.split(',')
          for (var j = 0; j < timeArray.length; j++) {
            var oneOpen = timeArray[j]
            oneOpen = oneOpen.toUpperCase()
            var from = oneOpen.split('-')[0]
            var until = oneOpen.split('-')[1]

            if (!from || !until) continue

            from = from.replace(/\s/g, '')
            until = until.replace(/\s/g, '')

            var fromTime = parseInt(from.split(':')[0])
            var untilTime = parseInt(until.split(':')[0])

            // console.log(until, untilTime)

            if (from.indexOf('AM') != -1) {
              from = from.split('AM')[0]
              if (fromTime >= 12) {
                fromTime = fromTime - 12
                from = fromTime + ':' + (from.split(':')[1] ? from.split(':')[1] : '00')
              }
            } else if (from.indexOf('PM') != -1) {
              from = from.split('PM')[0]
              if (fromTime < 12) {
                fromTime = fromTime + 12
                from = fromTime + ':' + (from.split(':')[1] ? from.split(':')[1] : '00')
              }
            }

            if (until.indexOf('AM') != -1) {
              until = until.split('AM')[0]
              if (untilTime >= 12) {
                untilTime = untilTime - 12
                until = untilTime + ':' + (until.split(':')[1] ? until.split(':')[1] : '00')
              }
            } else if (until.indexOf('PM') != -1) {
              until = until.split('PM')[0]
              if (untilTime < 12) {
                untilTime = untilTime + 12
                until = untilTime + ':' + (until.split(':')[1] ? until.split(':')[1] : '00')
              }
            }

            fromTime = parseInt(from.split(':')[0])
            untilTime = parseInt(until.split(':')[0])

            // console.log(until, untilTime)

            if (fromTime > untilTime) {
              if (fromTime < 12) {
                untilTime = untilTime + 12
                until = untilTime + ':' + (until.split(':')[1] ? until.split(':')[1] : '00')
              }
            }

            var fromuntil = {'from': from, 'until': until}
            open.push(fromuntil)
          }
          // console.log("open", open);
          var hour = {'days': days[i].value, 'open': open}
          // console.log("hour", hour);
          hours.push(hour)
        }

        console.log('hours', hours)

        $scope.business.hours = hours

        if ($scope.formData.categories.length > 0) {
          $scope.business.categories = CommonUtil.getIdList($scope.formData.categories)
          _.map($scope.formData.categories, function (o) {
            // searchTextList.push(o.name);
          })
        } else {
          delete $scope.business.categories
        }

        if ($scope.formData.collections.length > 0) {
          $scope.business.collections = CommonUtil.getIdList($scope.formData.collections)
        } else {
          delete $scope.business.collections
        }
        if ($scope.formData.certifications.length > 0) {
          $scope.business.certifications = CommonUtil.getIdList($scope.formData.certifications)
          $scope.business.certification = CommonUtil.makeBendRef($scope.formData.certifications[0]._id, 'certification')
        } else {
          delete $scope.business.certifications
          delete $scope.business.certification
        }
        var businessData = _.clone($scope.business)
        if (businessData.coverImage) {
          businessData.coverImage = CommonUtil.makeBendFile(businessData.coverImage._id)
        }
        if ($scope.bussinessId) {
          delete businessData.$$hashKey
          console.log('update', businessData)
          BendService.updateBusiness(businessData, function (ret) {
            $location.path('/businesses')
          })
        } else {
          BendService.createBusiness(businessData, function (ret) {
            $location.path('/businesses')
          })
        }
      }

      $rootScope.setLocation = function (lat, lng) {
        $scope.business._geoloc = [lng, lat]

        $scope.form.validateForm.$setDirty()
      }

      $scope.geocodeAddress = function () {
        var addressList = []
        if ($scope.business.address1 && $scope.business.address1 != '') { addressList.push($scope.business.address1) }
        if ($scope.business.city && $scope.business.city != '') { addressList.push($scope.business.city) }
        if ($scope.business.state && $scope.business.state != '') { addressList.push($scope.business.state) }
        if ($scope.business.postalCode && $scope.business.postalCode != '') { addressList.push($scope.business.postalCode) }

        $modal.open({
          templateUrl: 'views/templates/geocodeAddress.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance) {
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.confirmAddress = function () {
              var lat = $('#geoLat').val()
              var lng = $('#geoLng').val()
              if (lat == '' || lng == '') return

              $rootScope.setLocation(lat, lng)
              $modalInstance.dismiss('cancel')
            }

            var marker = null
            $scope.initMap = function () {
              // console.log(document.getElementById('geo_map'))
              var map = new google.maps.Map(document.getElementById('geo_map'), {
                zoom: 12,
                center: {lat: 42.3005383, lng: -71.0654838}
              })

              var geocoder = new google.maps.Geocoder()

              if (addressList.length > 0) {
                var address = addressList.join(', ')
                document.getElementById('address').value = address
                $scope.geocodeAddress(geocoder, map)
              } else {
                if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                      lat: position.coords.latitude,
                      lng: position.coords.longitude
                    }

                    map.setCenter(pos)
                  })
                }
              }

              document.getElementById('submit').addEventListener('click', function () {
                $scope.geocodeAddress(geocoder, map)
              })

              map.addListener('click', function (e) {
                $scope.placeMarkerAndPanTo(e.latLng, map)
              })
            }

            setTimeout(function () {
              $scope.initMap()
            }, 100)

            $scope.placeMarkerAndPanTo = function (latLng, map) {
              if (marker) {
                marker.setPosition(latLng)
              } else {
                marker = new google.maps.Marker({
                  map: map,
                  position: latLng
                })
              }

              $('#geoLat').val(latLng.lat())
              $('#geoLng').val(latLng.lng())
            }

            $scope.geocodeAddress = function (geocoder, resultsMap) {
              var address = document.getElementById('address').value
              geocoder.geocode({'address': address}, function (results, status) {
                if (status === 'OK') {
                  resultsMap.setCenter(results[0].geometry.location)
                  $scope.placeMarkerAndPanTo(results[0].geometry.location, resultsMap)
                } else {
                  alert('Geocode was not successful for the following reason: ' + status)
                }
              })
            }
          }
        })
      }
      $rootScope.fileUploadFromSearch2 = function (file, tag) {
        file._filename = Date.now() + ''
        var files = []
        files.push(file)
        $scope.onFileUpload(files, tag)
      }

      $scope.searchImages2 = function ($ev, tag) {
        $ev.stopPropagation()
        $ev.preventDefault()
        $modal.open({
          templateUrl: 'views/templates/searchImages.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance) {
            $scope.CommonUtil = CommonUtil
            $scope.searchResults = 0
            $scope.pages = 1
            $scope.showLoadMore = false
            // var cacheSearchKey = ''

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }

            $scope.searchImages = function (searchVal) {
              console.log('searchVal', searchVal)
              // cacheSearchKey = searchVal

              $scope.pages = 1
              if (searchVal) {
                var URL = 'https://pixabay.com/api/?key=2706353-ee3016f8af51ca406a8c8a3db&q=' + encodeURIComponent(searchVal) + '&image_type=photo&page=' + $scope.pages
                $.getJSON(URL, function (data) {
                  console.log(data)
                  if (parseInt(data.totalHits) > 0) {
                    applyChangesOnScope($scope, function () {
                      if (data.totalHits > $scope.pages * 20) { $scope.showLoadMore = true } else { $scope.showLoadMore = false }

                      $scope.searchResults = data.hits
                    })
                  } else { console.log('No hits') }
                })
              }
            }

            $scope.searchImagesMore = function (tabIdx, searchVal) {
              $scope.pages++
              if (searchVal) {
                var URL = 'https://pixabay.com/api/?key=2706353-ee3016f8af51ca406a8c8a3db&q=' + encodeURIComponent(searchVal) + '&image_type=photo&page=' + $scope.pages
                $.getJSON(URL, function (data) {
                  console.log(data)
                  if (parseInt(data.totalHits) > 0) {
                    applyChangesOnScope($scope, function () {
                      if (data.totalHits > $scope.pages * 20) { $scope.showLoadMore = true } else { $scope.showLoadMore = false }

                      $scope.searchResults = $scope.searchResults.concat(data.hits)
                    })
                  } else { console.log('No hits') }
                })
              }
            }

            $scope.selectImage = function (searchItem) {
              $scope.isDownloading = true
              console.log(searchItem)
              var imageUrl = ''
              imageUrl = searchItem.webformatURL

              var xhr = new XMLHttpRequest()
              xhr.open('GET', imageUrl, true)
              xhr.responseType = 'blob'
              xhr.onload = function (e) {
                if (this.status == 200) {
                  var myBlob = this.response
                  console.log('blob', myBlob)
                  $rootScope.fileUploadFromSearch2(myBlob, tag)
                  // myBlob is now the blob that the object URL pointed to.
                  $scope.cancel()
                }
              }
              xhr.send()
            }
          }
        })
      }
    }])
