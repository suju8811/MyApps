'use strict'

angular.module('app.controllers')
  .controller('EventController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout) {
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.eventList = []
      $scope.user = BendAuth.getActiveUser()
      $scope.collectionView = {
        searchTerm: (CommonUtil.getStorage('event-filter') ? CommonUtil.getStorage('event-filter').searchTerm : ''),
        itemsPerPage: 20,
        isLoading: true,
        totalItems: 0,
        currentPage: 1,
        numPages: 0
      }

      $scope.sortDirection = {
        'name': 1,
        'startsAt': 1,
        'endsAt': 1,
        '_bmd.updatedAt': 1
      }
      $scope.sortKey = 'name'

      $scope.filter = {
        category: (CommonUtil.getStorage('event-filter') ? CommonUtil.getStorage('event-filter').category : ''),
        collection: (CommonUtil.getStorage('event-filter') ? CommonUtil.getStorage('event-filter').collection : ''),
        community: (CommonUtil.getStorage('event-filter') ? CommonUtil.getStorage('event-filter').community : '')
      }

      $scope.categories = [{
        name: 'All categories',
        _id: null
      }]
      $scope.categoryGroup = {}
      $scope.communityList = [{
        name: 'All client',
        _id: null
      }]

      $scope.reset = function () {
        $scope.filter = {
          category: '',
          collection: '',
          community: ''
        }
        $scope.collectionView.searchTerm = ''

        $scope.loadEvents()
      }

      $scope.collectionList = []
      async.parallel([
        function (cb) {
          BendService.getCollectionEnabledList(function (rets) {
            $scope.allcollections = {
              name: 'All Collections',
              _id: null
            }
            $scope.collectionList = [$scope.allcollections].concat(rets)
            cb(null)
          })
        },
        function (cb) {
          BendService.getCommunityList(function (rets) {
            $scope.communityList = $scope.communityList.concat(rets)
            cb(null)
          })
        },
        function (cb) {
          BendService.getCategoryList(function (rets) {
            rets = _.sortBy(rets, function (o) {
              return o.group
            })

            var group = ''
            var cats = []
            _.forEach(rets, function (o, idx) {
              if (o.group != group) {
                group = o.group
                $scope.categoryGroup[group] = []

                cats.push({
                  name: group,
                  type: 'group'
                })
              }

              cats.push(o)
              $scope.categoryGroup[group].push(o._id)
            })

            // console.log("categories", cats, $scope.categoryGroup)

            $scope.categories = $scope.categories.concat(cats)
            cb(null, null)
          })
        }
      ], function (err, ret) {
        $scope.loadEvents()
      })

      $scope.loadEvents = function () {
        $scope.isLoading = true
        CommonUtil.setStorage('event-filter', Object.assign({}, $scope.filter, {
          searchTerm: $scope.collectionView.searchTerm
        }))

        var searchTerm = $scope.collectionView.searchTerm

        var q = new $bend.Query()
        if (searchTerm != '') {
          q.matches('name', searchTerm, {
            ignoreCase: true
          })
        }

        if ($scope.filter.category && $scope.filter.category != '') {
          if (!$scope.filter.category._id) {
            if ($scope.filter.category.type == 'group') { q.contains('categories', $scope.categoryGroup[$scope.filter.category.name]) }
          } else {
            var id = $scope.filter.category._id
            q.contains('categories', [id])
          }
        }

        if ($scope.filter.collection != '' && $scope.filter.collection._id != null) {
          q.containsAll('collections', [$scope.filter.collection._id])
        }

        if ($scope.filter.community && $scope.filter.community != '' && $scope.filter.community._id != null) {
          q.equalTo('community._id', $scope.filter.community._id)
        }

        q.notEqualTo('deleted', true)
        // q.descending("_bmd.createdAt");
        q.limit($scope.collectionView.itemsPerPage)
        q.skip(($scope.collectionView.currentPage - 1) * $scope.collectionView.itemsPerPage)

        if ($scope.sortDirection[$scope.sortKey] == 1) { q.ascending($scope.sortKey) }
        if ($scope.sortDirection[$scope.sortKey] == -1) { q.descending($scope.sortKey) }

        if (BendAuth.getActiveUser().communityAdmin) {
          q.equalTo('community._id', BendAuth.getActiveUser().community._id)
        }
        $bend.DataStore.find('event', q).then(function (rets) {
          var userIds = []
          _.map(rets, function (o) {
            if (o._acl && o._acl.creator) {
              userIds.push(o._acl.creator)
            }
          })

          userIds = _.uniq(userIds)
          var q = new $bend.Query()
          q.contains('_id', userIds)
          $bend.User.find(q, {
            relations: {
              avatar: 'BendFile'
            }
          }).then(function (users) {
            _.map(rets, function (o) {
              if (o._acl && o._acl.creator) {
                var u = _.find(users, function (_o) {
                  return _o._id == o._acl.creator
                })

                if (u) {
                  o._acl.creator = u
                }
              }
            })
            $scope.eventList = rets
            $scope.isLoading = false
          }, function (err) {
            console.log(err)
          })
        }, function (err) {
          console.log(err)
        })

        $bend.DataStore.count('event', q).then(function (count) {
          applyChangesOnScope($scope, function () {
            $scope.collectionView.totalItems = count
            $scope.collectionView.numPages = $scope.collectionView.totalItems / $scope.collectionView.itemsPerPage + 1
          })
        }, function (err) {
          console.log(err)
        })
      }

      $scope.sortBy = function (key) {
        $scope.sortKey = key
        $scope.sortDirection[key] = (-1) * $scope.sortDirection[key]
        $scope.loadEvents()
      }

      $scope.onSearch = function () {
        $scope.loadEvents()
      }

      $scope.onPageChange = function () {
        $scope.loadEvents()
      }

      $scope.deleteEvent = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Deleting the event is permanent and can not be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deleteEvent(id, function (ret) {
              if (ret) {
                for (var i = 0; i < $scope.eventList.length; i++) {
                  if ($scope.eventList[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.eventList.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $scope.editEvent = function (event, $e) {
        if ($e.metaKey || $e.ctrlKey) {
          window.open('#/events/' + event._id, '_blank')
        } else { return $location.path('/events/' + event._id) }
      }

      $scope.createEvent = function ($event) {
        if ($event.metaKey || $event.ctrlKey) {
          window.open('#/events/new', '_blank')
        } else { return $location.path('/events/new') }
      }
    }])
