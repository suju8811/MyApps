'use strict'

angular.module('app.controllers')
  .controller('VolunteeringEditController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$http', 'pinesNotifications',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $http, pinesNotifications) {
      // Init.
      $scope.CommonUtil = CommonUtil
      $scope.volunteeringId = null
      if ($routeParams.id != 'new') { $scope.volunteeringId = $routeParams.id }
      $scope.isLoading = true
      $scope.isUploading = []
      $scope.volunteering = {
        repeatable: true
      }

      $scope.tagList = []
      $scope.categoryList = []
      $scope.collectionList = []
      $scope.communityList = []
      $scope.stateList = CommonUtil.AllStates
      $scope.formData = {
        categories: [],
        collections: [],
        intervalminute: 0,
        intervalhour: 0,
        intervalday: 0,
        StartsAt: '',
        EndsAt: ''
      }
      $scope.state = $scope.stateList[0]
      $scope.fileProgress = []
      $scope.openedDateSelector = [false, false]
      $scope.openedTimeDateSelector = []
      $scope.volunteering = {
        _geoloc: [0, 0]
      }
      $scope.user = BendAuth.getActiveUser()
      $scope.form = {}

      async.parallel([
        function (callback) {
          BendService.getCommunityList(function (rets) {
            $scope.communityList = rets
            callback(null, null)
          })
        },
        function (callback) {
          BendService.getCategoryList(function (rets) {
            $scope.categoryList = rets
            // console.log($scope.categoryList);
            callback(null, null)
          })
        }, function (callback) {
          BendService.getCollectionEnabledList(function (rets) {
            $scope.collectionList = rets
            // console.log($scope.collectionList);
            callback(null, null)
          })
        }], function (err, rets) {
        if (err) {
          console.log(err)
          return
        }
        if ($scope.volunteeringId) {
          BendService.getVolunteering($scope.volunteeringId, function (ret) {
            ret.enabled = (ret.enabled !== false)
            $scope.volunteering = ret
            $scope.isLoading = false
            if ($scope.volunteering.repeatInterval) {
              $scope.formData.intervalday = parseInt($scope.volunteering.repeatInterval / (24 * 3600))
              $scope.formData.intervalhour = parseInt(($scope.volunteering.repeatInterval - $scope.formData.intervalday * (24 * 3600)) / (3600))
              $scope.formData.intervalminute = parseInt(($scope.volunteering.repeatInterval - (($scope.formData.intervalday * (24 * 3600)) + $scope.formData.intervalhour * 3600)) / 60)
            }

            if ($scope.volunteering.state) {
              $scope.state = $scope.volunteering.state
            }
            if ($scope.volunteering.categories && $scope.volunteering.categories.length > 0) {
              // console.log($scope.categoryList);
              $scope.formData.categories = _.filter($scope.categoryList, function (o) {
                return $scope.volunteering.categories.indexOf(o._id) != -1
              })
            }

            if ($scope.volunteering.collections && $scope.volunteering.collections.length > 0) {
              // console.log($scope.collectionList);
              $scope.formData.collections = _.filter($scope.collectionList, function (o) {
                return $scope.volunteering.collections.indexOf(o._id) != -1
              })
              // console.log($scope.formData.categories);
            }

            if ($scope.volunteering.community) {
              $scope.volunteering.community = $scope.volunteering.community._id
            } else {
              $scope.volunteering.community = ''
            }

            if ($scope.volunteering.startsAt) {
              $scope.formData.StartsAt = CommonUtil.formatDateWithFormat(ret.startsAt, 'YYYY-MM-DD LT')// CommonUtil.formatDate(ret.startsAt);//CommonUtil.formatDateWithFormat(ret.startsAt,"YYYY-MM-DD");
            }
            if ($scope.volunteering.endsAt) {
              $scope.formData.EndsAt = CommonUtil.formatDateWithFormat(ret.endsAt, 'YYYY-MM-DD LT')// CommonUtil.formatDate(ret.endsAt);//CommonUtil.formatDateWithFormat(ret.endsAt,"YYYY-MM-DD");
            }
          })
        }
      })

      $scope.saveVolunteeringDo = function () {
        var repeatInterval = 0
        if ($scope.formData.intervalday != 0) {
          repeatInterval = $scope.formData.intervalday * (24 * 3600)
        }
        if ($scope.formData.intervalhour != 0) {
          repeatInterval = repeatInterval + $scope.formData.intervalhour * (3600)
        }
        if ($scope.formData.intervalminute != 0) {
          repeatInterval = repeatInterval + $scope.formData.intervalminute * (60)
        }

        if (repeatInterval > 0) {
          $scope.volunteering.repeatInterval = repeatInterval
        }

        // console.log("start at, end at", $scope.StartsAt, $scope.EndsAt)
        if ($scope.formData.StartsAt && $scope.formData.StartsAt != '') {
          $scope.volunteering.startsAt = CommonUtil.convertStringToDate($scope.formData.StartsAt, 'YYYY-MM-DD LT').getTime() * 1000000
        } else { delete $scope.volunteering.startsAt }
        if ($scope.formData.EndsAt && $scope.formData.EndsAt != '') {
          $scope.volunteering.endsAt = CommonUtil.convertStringToDate($scope.formData.EndsAt, 'YYYY-MM-DD LT').getTime() * 1000000
        } else {
          delete $scope.volunteering.endsAt
        }

        if ($scope.volunteering._geoloc && $scope.volunteering._geoloc[0] && $scope.volunteering._geoloc[1] && $scope.volunteering._geoloc[0] != '' && $scope.volunteering._geoloc[1] != '') { $scope.volunteering._geoloc = [parseFloat($scope.volunteering._geoloc[0]), parseFloat($scope.volunteering._geoloc[1])] } else { delete $scope.volunteering._geoloc }

        if ($scope.formData.collections.length > 0) {
          $scope.volunteering.collections = CommonUtil.getIdList($scope.formData.collections)
        } else {
          delete $scope.volunteering.collections
        }

        if ($scope.formData.categories.length > 0) {
          $scope.volunteering.categories = CommonUtil.getIdList($scope.formData.categories)
        } else {
          delete $scope.volunteering.categories
        }

        var volunteeringData = _.clone($scope.volunteering)
        if (volunteeringData.coverImage) {
          volunteeringData.coverImage = CommonUtil.makeBendFile(volunteeringData.coverImage._id)
        }

        if (volunteeringData.community != '') {
          volunteeringData.community = CommonUtil.makeBendRef(volunteeringData.community, 'community')
        } else {
          delete volunteeringData.community
        }

        if ($scope.volunteeringId) {
          delete volunteeringData.$$hashKey
          BendService.updateVolunteering(volunteeringData, function (ret) {
            $location.path('/volunteerings')
          })
        } else {
          BendService.createVolunteering(volunteeringData, function (ret) {
            $location.path('/volunteerings')
          })
        }
      }

      $scope.openDateWindow = function ($event, idx) {
        $event.preventDefault()
        $event.stopPropagation()

        $scope.openedDateSelector[idx] = true
      }
      $scope.openTimeDateWindow = function ($event, idx) {
        $event.preventDefault()
        $event.stopPropagation()

        $scope.openedTimeDateSelector[idx] = true
      }

      $rootScope.syncPickItems = function (pickedItems, type) {
        applyChangesOnScope($scope, function () {
          if (type == 'category') {
            $scope.formData.categories = pickedItems
          } else if (type == 'giftType') {
            $scope.formData.giftTypes = pickedItems
          } else if (type == 'interest') {
            $scope.formData.interests = pickedItems
          }
        })
      }

      $scope.pickCategory = function (e) {
        e.stopPropagation()
        e.preventDefault()

        $scope.openPickModal($scope.categoryList, $scope.formData.categories, 'category')
      }
      $scope.openPickModal = function (selectedItems, pickedItems, type) {
        $modal.open({
          style: {display: 'flex'},
          templateUrl: 'pickItems.html',
          controller: function ($scope, $modalInstance, selectedItems, pickedItems, type) {
            $scope.selectedItems = selectedItems
            $scope.pickedItems = pickedItems
            $scope.title = 'Select Categories'

            if (type == 'giftType') { $scope.title = 'Select Gift Types' } else if (type == 'interest') { $scope.title = 'Select Interests' }

            $scope.isPickedItem = function (item) {
              return $scope.pickedItems.indexOf(item) != -1
            }

            $scope.togglePickItem = function (item, e) {
              var idx = $scope.pickedItems.indexOf(item)
              if (idx == -1) {
                $scope.pickedItems.push(item)
              } else {
                $scope.pickedItems.splice(idx, 1)
              }

              $rootScope.syncPickItems($scope.pickedItems, type)
            }

            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }
          },
          resolve: {
            selectedItems: function () {
              return selectedItems
            },
            pickedItems: function () {
              return pickedItems
            },
            type: function () {
              return type
            }
          }
        })
      }

      $scope.deleteFile = function (tag, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        applyChangesOnScope($scope, function () {
          delete $scope.volunteering.coverImage
        })
      }

      $scope.showFileLoading = function (tag, bShow) {
        $scope.isUploading[tag] = bShow
      }

      $scope.selectFileOpen = function (fileId, $ev) {
        if ($ev.target.tagName == 'DIV') {
          setTimeout(function () {
            $('#' + fileId).click()
          }, 0)
        }
      }

      $scope.onFileUpload = function (files, tag) {
        var file = files[0]
        $scope.fileProgress[tag] = 0
        $scope.showFileLoading(tag, true)
        var reader = new FileReader()
        reader.onload = function (e) {
          $scope.uploadingFileUrl = e.target.result
        }
        reader.readAsDataURL(file)

        BendService.upload(file, function (error, uploadedFile) {
          // console.log("uploadedFile", uploadedFile);
          if (error) {
            $scope.showFileLoading(tag, false)
            $scope.uploadingFileUrl = null

            return
          }
          // console.log(uploadedFile);
          BendService.getFile(uploadedFile, function (o) {
            $scope.volunteering.coverImage = o
            $scope.uploadingFileUrl = null

            $scope.showFileLoading(tag, false)
            $scope.form.validateForm.$dirty = true
          })
        }, {
          _workflow: 'coverPhoto'
        }, function (total, prog) {
          applyChangesOnScope($scope, function () {
            $scope.fileProgress[tag] = prog * 100 / total
          })
        })
      }

      $rootScope.setLocation = function (lat, lng) {
        $scope.volunteering._geoloc = [lng, lat]
        $scope.form.validateForm.$setDirty()
      }

      $scope.geocodeAddress = function () {
        var addressList = []
        if ($scope.volunteering.address1 && $scope.volunteering.address1 != '') { addressList.push($scope.volunteering.address1) }
        if ($scope.volunteering.city && $scope.volunteering.city != '') { addressList.push($scope.volunteering.city) }
        if ($scope.volunteering.state && $scope.volunteering.state != '') { addressList.push($scope.volunteering.state) }
        if ($scope.volunteering.postalCode && $scope.volunteering.postalCode != '') { addressList.push($scope.volunteering.postalCode) }

        $modal.open({
          templateUrl: 'views/templates/geocodeAddress.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance) {
            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.confirmAddress = function () {
              var lat = $('#geoLat').val()
              var lng = $('#geoLng').val()
              if (lat == '' || lng == '') return

              $rootScope.setLocation(lat, lng)
              $modalInstance.dismiss('cancel')
            }

            var marker = null
            $scope.initMap = function () {
              // console.log(document.getElementById('geo_map'))
              var map = new google.maps.Map(document.getElementById('geo_map'), {
                zoom: 12,
                center: {lat: 42.3005383, lng: -71.0654838}
              })

              var geocoder = new google.maps.Geocoder()

              if (addressList.length > 0) {
                var address = addressList.join(', ')
                document.getElementById('address').value = address
                $scope.geocodeAddress(geocoder, map)
              } else {
                if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                      lat: position.coords.latitude,
                      lng: position.coords.longitude
                    }

                    map.setCenter(pos)
                  })
                }
              }

              document.getElementById('submit').addEventListener('click', function () {
                $scope.geocodeAddress(geocoder, map)
              })

              map.addListener('click', function (e) {
                $scope.placeMarkerAndPanTo(e.latLng, map)
              })
            }

            setTimeout(function () {
              $scope.initMap()
            }, 100)

            $scope.placeMarkerAndPanTo = function (latLng, map) {
              if (marker) {
                marker.setPosition(latLng)
              } else {
                marker = new google.maps.Marker({
                  map: map,
                  position: latLng
                })
              }

              $('#geoLat').val(latLng.lat())
              $('#geoLng').val(latLng.lng())
            }

            $scope.geocodeAddress = function (geocoder, resultsMap) {
              var address = document.getElementById('address').value
              geocoder.geocode({'address': address}, function (results, status) {
                if (status === 'OK') {
                  resultsMap.setCenter(results[0].geometry.location)
                  $scope.placeMarkerAndPanTo(results[0].geometry.location, resultsMap)
                } else {
                  alert('Geocode was not successful for the following reason: ' + status)
                }
              })
            }
          }
        })
      }
      $rootScope.fileUploadFromSearch2 = function (file, tag) {
        file._filename = Date.now() + ''
        var files = []
        files.push(file)
        $scope.onFileUpload(files, tag)
      }

      $scope.searchImages2 = function ($ev, tag) {
        $ev.stopPropagation()
        $ev.preventDefault()
        $modal.open({
          templateUrl: 'views/templates/searchImages.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance) {
            $scope.CommonUtil = CommonUtil
            $scope.searchResults = 0
            $scope.pages = 1
            $scope.showLoadMore = false
            // var cacheSearchKey = ''

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }

            $scope.searchImages = function (searchVal) {
              console.log('searchVal', searchVal)
              // cacheSearchKey = searchVal

              $scope.pages = 1
              if (searchVal) {
                var URL = 'https://pixabay.com/api/?key=2706353-ee3016f8af51ca406a8c8a3db&q=' + encodeURIComponent(searchVal) + '&image_type=photo&page=' + $scope.pages
                $.getJSON(URL, function (data) {
                  console.log(data)
                  if (parseInt(data.totalHits) > 0) {
                    applyChangesOnScope($scope, function () {
                      if (data.totalHits > $scope.pages * 20) { $scope.showLoadMore = true } else { $scope.showLoadMore = false }

                      $scope.searchResults = data.hits
                    })
                  } else { console.log('No hits') }
                })
              }
            }

            $scope.searchImagesMore = function (tabIdx, searchVal) {
              $scope.pages++
              if (searchVal) {
                var URL = 'https://pixabay.com/api/?key=2706353-ee3016f8af51ca406a8c8a3db&q=' + encodeURIComponent(searchVal) + '&image_type=photo&page=' + $scope.pages
                $.getJSON(URL, function (data) {
                  console.log(data)
                  if (parseInt(data.totalHits) > 0) {
                    applyChangesOnScope($scope, function () {
                      if (data.totalHits > $scope.pages * 20) { $scope.showLoadMore = true } else { $scope.showLoadMore = false }

                      $scope.searchResults = $scope.searchResults.concat(data.hits)
                    })
                  } else { console.log('No hits') }
                })
              }
            }

            $scope.selectImage = function (searchItem) {
              $scope.isDownloading = true
              console.log(searchItem)
              var imageUrl = ''
              imageUrl = searchItem.webformatURL

              var xhr = new XMLHttpRequest()
              xhr.open('GET', imageUrl, true)
              xhr.responseType = 'blob'
              xhr.onload = function (e) {
                if (this.status == 200) {
                  var myBlob = this.response
                  console.log('blob', myBlob)
                  $rootScope.fileUploadFromSearch2(myBlob, tag)
                  // myBlob is now the blob that the object URL pointed to.
                  $scope.cancel()
                }
              }
              xhr.send()
            }
          }
        })
      }
    }])
