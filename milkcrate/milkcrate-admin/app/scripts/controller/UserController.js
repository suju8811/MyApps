'use strict'

angular.module('app.controllers')
  .controller('UserController', ['$scope','$bend','$location','$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox','$rootScope','$modal','$timeout',
    function ($scope,$bend,$location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal,  $timeout) {

      // Init.
      $scope.userList = [];
      $scope.isLoading=true;
      $scope.CommonUtil = CommonUtil;
      $scope.communityList = [{
        name:"All client",
        _id:null
      }]
      $scope.filter = {
        community:(CommonUtil.getStorage("user-filter")?CommonUtil.getStorage("user-filter").community:""),
      }
      $scope.collectionView ={
        searchTerm: (CommonUtil.getStorage("user-filter")?CommonUtil.getStorage("user-filter").searchTerm:""),
        itemsPerPage: 20,
        isLoading: true,
        totalItems: 0,
        currentPage: 1,
        numPages: 0
      };

      $scope.exportData = []

      BendService.getCommunityList(function(rets){
        $scope.communityList = $scope.communityList.concat(rets);
      })
      $scope.reset = function() {
        $scope.filter = {
          community:"",
        }
        $scope.collectionView ={
          searchTerm:"",
          itemsPerPage: 20,
          isLoading: true,
          totalItems: 0,
          currentPage: 1,
          numPages: 0
        };

        $scope.loadUsers()
      }
      $scope.loadUsers = function() {
        //console.log('load users', $scope.collectionView)
        CommonUtil.setStorage("user-filter", {
          community:$scope.filter.community,
          searchTerm:$scope.collectionView.searchTerm
        })
        $scope.isLoading=true;

        function query(str) {
          var query=new $bend.Query();
          if(str==="") return query;
          var q2 = new $bend.Query().matches("name",str,{
            ignoreCase: true
          })
            .or().matches("name",str,{
              ignoreCase: true
            })
            .or().matches("email",str,{
              ignoreCase: true
            });
          return query.and(q2);

          return query;
        }

        var q = query($scope.collectionView.searchTerm);
        q.notEqualTo("deleted", true);
        q.descending("_bmd.createdAt");
        q.limit($scope.collectionView.itemsPerPage);
        q.skip(($scope.collectionView.currentPage-1)*$scope.collectionView.itemsPerPage);

        if($scope.filter.community != "" && $scope.filter.community._id != null) {
          q.equalTo("community._id", $scope.filter.community._id);
        }

        $bend.User.find(q, {
          relations:{
            avatar:"BendFile",
            community: "community"
          }
        }).then(function(users) {
          $scope.userList = users;
          $scope.isLoading = false;

          var userIds = CommonUtil.getIdList(users)

          //get engagements
          var q = new $bend.Query()
          q.contains('user._id', userIds)
          var aggregation = $bend.Group.count();
          aggregation.by('user._id')
          aggregation.query(q);
          //get all leaderboard first
          $bend.DataStore.group("activity", aggregation).then(function(rets){
            //console.log('aggregation', rets)
            _.map($scope.userList, function(user){
              var exist = _.find(rets, function(o){
                return o['user._id'] == user._id
              })
              if(exist) {
                user.engagements = exist.result
              } else {
                user.engagements = 0
              }
            })
          });
        }, function(error) {
          console.log(error);
        });

        $bend.User.count(q).then(function(count){
          applyChangesOnScope($scope, function(){
            $scope.collectionView.totalItems=count;
            $scope.collectionView.numPages=$scope.collectionView.totalItems/$scope.collectionView.itemsPerPage+1;
          });
        },function(err){
          console.log(err);
        });

        //for excel export
        var qq = query($scope.collectionView.searchTerm);
        qq.notEqualTo("deleted", true);
        qq.descending("_bmd.createdAt");

        if($scope.filter.community != "" && $scope.filter.community._id != null) {
          qq.equalTo("community._id", $scope.filter.community._id);
        }
        qq.fields(['name', 'email'])

        $bend.User.find(qq).then(function(users) {
          //console.log('users', users)
          $scope.exportData = users
        }, function(error) {
          console.log(error);
        });
      };

      $scope.loadUsers();
      $scope.onSearch = function() {
        $scope.loadUsers();
      };

      $scope.onPageChange = function() {
        $scope.loadUsers();
      };

      $scope.deleteUser = function(id, $ev) {
        $ev.stopPropagation();
        $ev.preventDefault();
        var msg = "Deleting the user is permanent and can not be undone";
        $bootbox.confirm(msg, function(result) {
          if(result) {
            BendService.deleteUser(id, function(ret){
              if(ret){
                for(var i = 0 ; i < $scope.userList.length ; i++) {
                  if($scope.userList[i]._id == id) {
                    applyChangesOnScope($scope, function(){
                      $scope.userList.splice(i, 1);
                      return;
                    })
                  }
                }
              }
            })
          }
        });
      }

      $rootScope.editUser = function(user, $event) {
        if($event.metaKey || $event.ctrlKey) {
          window.open("#/users/" + user._id + "?t=" + Date.now(), '_blank');
        } else
          return $location.url("/users/" + user._id + "?t=" + Date.now());
      }

      $rootScope.addUser = function(user) {
        $scope.userList.push(user);
      }

      $scope.createUser = function() {
        var modalInstance = $modal.open({
          templateUrl: 'createUser.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance, userList) {
            $scope.user = {
              enabled:true,
              defaultAvatar:CommonUtil.animals[0],
              gender:"male"
            };
            $scope.CommonUtil = CommonUtil;
            $scope.fileProgress = [];
            $scope.isUploading = [];
            $scope.CommunityList = [];
            $scope.openedDateSelector = [false];
            $scope.filter = {}

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };
            $scope.close = function () {
              $modalInstance.dismiss('cancel');
            };
            $scope.openDateWindow = function($event, idx) {
              $event.preventDefault();
              $event.stopPropagation();

              $scope.openedDateSelector[idx] = true;
            };
            BendService.getCommunityList(function(rets){
              $scope.CommunityList = rets;
            })
            $scope.createUserDo = function () {
              $scope.isSaving = true;
              if($scope.user.community) {
                $scope.user.community = CommonUtil.makeBendRef($scope.user.community._id, "community");
              } else {
                delete $scope.user.community;
              }
              if(typeof $scope.BirthDate == "object") {
                $scope.user.birthdate = CommonUtil.formatDateWithFormat($scope.BirthDate.getTime() * 1000000, "YYYY-MM-DD")
              }

              $scope.user.username = $scope.user.email

              if($scope.user.gender == "") {
                delete $scope.user.gender;
              } else {
                $scope.user.gender = $scope.user.gender.toLowerCase()
              }

              $scope.user.allowNotifications = true

              if($scope.user.avatar)
                $scope.user.avatar = CommonUtil.makeBendFile($scope.user.avatar._id)
              BendService.createUser($scope.user, function(ret){
                console.log(ret);
                $modalInstance.dismiss('cancel');
                BendService.getUser(ret._id, function(ret){
                  $rootScope.addUser(ret);
                })
              })
            };

            $scope.openFile = function(fileName) {
              $("input[name='" + fileName + "']").click();
            }

            $scope.deleteFile = function(tag, $ev){
              $ev.stopPropagation();
              $ev.preventDefault();
              applyChangesOnScope($scope, function(){
                delete $scope.user.avatar;
              })
            }

            $scope.onChangeDefaultAvatar = function () {
              $scope.user.defaultAvatar = $scope.filter.defaultAvatar;
            }

            $scope.showFileLoading = function(tag, bShow) {
              $scope.isUploading[tag] = bShow;
            }

            $scope.canSubmitValidationForm = function() {
              return $scope.form.validateForm.$valid;
            };

            $scope.selectFileOpen = function(fileId, $ev) {
              if($ev.target.tagName == 'DIV') {
                setTimeout(function(){
                  $('#' + fileId).click();
                }, 0);
              }
            }

            $scope.onFileUpload = function (files, tag) {
              var file = files[0];

              $scope.fileProgress[tag] = 0;
              $scope.showFileLoading(tag, true);
              BendService.upload(file,function(error,uploadedFile){
                if(error) {
                  $scope.showFileLoading(tag, false);
                  return;
                }

                BendService.getFile(uploadedFile, function(o){
                  $scope.user.avatar = o;
                  $scope.showFileLoading(tag, false);
                })
              },{
                _workflow: 'avatar'
              }, function (total, prog){
                applyChangesOnScope($scope, function(){
                  $scope.fileProgress[tag] = prog * 100 / total;
                })
              });
            };
          },
          resolve: {
            userList: function () {
              return $scope.userList;
            },
          }
        });
      };
    }]);