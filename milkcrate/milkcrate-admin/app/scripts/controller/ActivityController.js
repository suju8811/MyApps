'use strict'

angular.module('app.controllers')
  .controller('ActivityController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout) {
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.activityList = []
      $scope.imageMap = {}
      $scope.collectionView = {
        searchTerm: (CommonUtil.getStorage('activity-filter') ? CommonUtil.getStorage('activity-filter').searchTerm : ''),
        itemsPerPage: 20,
        isLoading: true,
        totalItems: 0,
        currentPage: 1,
        numPages: 0
      }

      $scope.activityTypes = [
        {value: 'action', name: 'Actions'},
        {value: 'volunteer_oppotunity', name: 'Volunteer Opportunities'},
        {value: 'business', name: 'Places'},
        {value: 'event', name: 'Events'},
        {value: 'poll', name: 'Poll Questions'},
        {value: 'comment', name: 'Comments'}
      ]

      $scope.communityList = [{
        name: 'All Client',
        _id: null
      }]
      $scope.allTeams = []
      $scope.teams = []

      $scope.filter = {
        type: (CommonUtil.getStorage('activity-filter') ? CommonUtil.getStorage('activity-filter').type : ''),
        community: (CommonUtil.getStorage('activity-filter') ? CommonUtil.getStorage('activity-filter').community : ''),
        team: (CommonUtil.getStorage('activity-filter') ? CommonUtil.getStorage('activity-filter').team : '')
      }

      BendService.getAllTeamList(function (err, rets) {
        if (err) {
          console.log(err); return
        }

        $scope.allTeams = rets
        // console.log('$scope.allTeams', $scope.allTeams)

        $scope.loadList()
      })

      $scope.reset = function () {
        $scope.filter = {
          type: '',
          community: '',
          team: ''
        }
        $scope.collectionView.searchTerm = ''

        $scope.loadList()
      }

      BendService.getCommunityList(function (rets) {
        $scope.communityList = $scope.communityList.concat(rets)
      })

      $scope.loadList = function () {
        CommonUtil.setStorage('activity-filter', Object.assign({}, $scope.filter, {
          searchTerm: $scope.collectionView.searchTerm
        }))

        if ($scope.filter.community && $scope.filter.community._id) {
          var teams = _.filter($scope.allTeams, function (o) {
            return o.community._id == $scope.filter.community._id
          })
          if (teams.length > 0) {
            $scope.teams = [{
              name: 'All Teams',
              _id: null
            }].concat(teams)
          } else {
            $scope.teams = []
          }
        } else {
          $scope.teams = []
        }

        var searchTerm = $scope.collectionView.searchTerm
        $scope.isLoading = true

        var q = new $bend.Query()
        if (searchTerm != '') {
          q.matches('summary', searchTerm, {
            ignoreCase: true
          })
        }
        q.notEqualTo('deleted', true)
        q.descending('_bmd.createdAt')
        q.limit($scope.collectionView.itemsPerPage)
        q.skip(($scope.collectionView.currentPage - 1) * $scope.collectionView.itemsPerPage)
        if ($scope.filter.community instanceof Object && $scope.filter.community._id) {
          q.equalTo('community._id', $scope.filter.community._id)
        }
        // console.log('$scope.filter.team', $scope.filter.team)
        if ($scope.teams.length > 0 && $scope.filter.team != '' && $scope.filter.team._id != null) {
          q.contains('teams', [$scope.filter.team._id])
        }

        if ($scope.filter.type != '') {
          q.equalTo('type', $scope.filter.type)
        }
        $bend.DataStore.find('activity', q, {
          relations: {
            user: 'user',
            community: 'community'
          }
        }).then(function (rets) {
          $scope.activityList = rets
          $scope.isLoading = false

          $scope.imageMap = {}
          var imageIds = []
          _.each(rets, function (o) {
            if (o.user.avatar) {
              if (imageIds.indexOf(o.user.avatar._id) == -1) { imageIds.push(o.user.avatar._id) }
            }
          })

          var query = new $bend.Query()
          query.contains('_id', imageIds)
          $bend.File.find(query).then(function (rets) {
            _.each(rets, function (o) {
              $scope.imageMap[o._id] = o
            })
            _.map($scope.activityList, function (o) {
              if (o.user.avatar) {
                o.user.avatar = $scope.imageMap[o.user.avatar._id]
              }
            })
          })
        }, function (err) {
          console.log(err)
        })

        $bend.DataStore.count('activity', q).then(function (count) {
          applyChangesOnScope($scope, function () {
            $scope.collectionView.totalItems = count
            $scope.collectionView.numPages = $scope.collectionView.totalItems / $scope.collectionView.itemsPerPage + 1
          })
        }, function (err) {
          console.log(err)
        })
      }

      $scope.onSearch = function () {
        $scope.loadList()
      }

      $scope.onPageChange = function () {
        $scope.loadList()
      }

      $scope.deleteActivity = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Deleting the activity is permanent and can not be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deleteActivity(id, function (ret) {
              if (ret) {
                for (var i = 0; i < $scope.activityList.length; i++) {
                  if ($scope.activityList[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.activityList.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $scope.editActivity = function (activity, $event) {
        if ($event.metaKey || $event.ctrlKey) {
          window.open('#/activities/' + activity._id, '_blank')
        } else { return $location.path('/activities/' + activity._id) }
      }

      $scope.createActivity = function ($event) {
        if ($event.metaKey || $event.ctrlKey) {
          window.open('#/activities/new', '_blank')
        } else { return $location.path('/activities/new') }
      }
    }])
