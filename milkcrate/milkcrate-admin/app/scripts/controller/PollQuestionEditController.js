'use strict'

angular.module('app.controllers')
  .controller('PollQuestionEditController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$http', 'pinesNotifications',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $http, pinesNotifications) {
      // Init.
      $scope.CommonUtil = CommonUtil
      $scope.user = BendAuth.getActiveUser()
      $scope.isEditMode = false
      if ($routeParams.id != 'new') {
        $scope.pollQuestionId = $routeParams.id
        $scope.isEditMode = true
      }

      $scope.communityList = []
      $scope.answerList = []
      $scope.responseList = []
      $scope.userList = []
      $scope.isAnswer = true
      $scope.pollQuestion = {
        answerCount: 0,
        responseCount: 0,
        points: 10
      }
      $scope.questionList = []
      $scope.isShowType = 0

      $scope.formData = {
        communities: [],
        openedDateSelector: false,
        teams: []
      }

      $scope.form = {}

      $scope.teams = []

      // var oldPosList = []
      var oldAnswersPosList = []
      // var sourceAnswerId

      BendService.getCommunityList(function (rets) {
        $scope.communityList = rets
        if ($scope.isEditMode) {
          async.parallel([
            function (cb) {
              BendService.getPollQuestion($scope.pollQuestionId, function (ret) {
                $scope.pollQuestion = ret

                if ($scope.pollQuestion.communities && $scope.pollQuestion.communities.length > 0) {
                  $scope.formData.communities = _.filter($scope.communityList, function (o) {
                    return $scope.pollQuestion.communities.indexOf(o._id) != -1
                  })
                }
                if ($scope.pollQuestion.pollDate) {
                  $scope.formData.pollDate = $scope.pollQuestion.pollDate
                }
                cb(null, null)
              })
            },
            function (cb) {
              BendService.getAnswerList($scope.pollQuestionId, function (rets) {
                $scope.answerList = rets
                cb(null, null)
              })
            },
            function (cb) {
              BendService.getResponseList($scope.pollQuestionId, function (rets) {
                if ($scope.pollQuestionId) {
                  $scope.responseList = rets
                }
                cb(null, null)
              })
            }
          ], function (err, ret) {
            $scope.enableSortableAnswers()

            // get team list
            BendService.getCommunityTeamList($scope.pollQuestion.community._id, function (err, rets) {
              if (err) { console.log(err); return }
              $scope.teams = rets

              if ($scope.pollQuestion.teams) {
                $scope.formData.teams = _.filter($scope.teams, function (o) {
                  return $scope.pollQuestion.teams.indexOf(o._id) != -1
                })
              }
            })

            var chartData = []
            _.map($scope.answerList, function (o) {
              chartData.push({
                label: o.title,
                data: $scope.getCount(o)
              })
            })

            // console.log('chartData', chartData)
            $.plot('#response-pie', chartData, {
              series: {
                pie: {
                  show: true
                }
              },
              legend: {
                show: false
              }
            })
          })
        }
      })

      $scope.enableSortableAnswers = function () {
        $('#answers-container').sortable({
          opacity: 0.5,
          dropOnEmpty: true,
          start: function (event, ui) {
            ui.item[0].style.backgroundColor = '#eee'
            oldAnswersPosList = []
            $scope.answerList.map(function (o) {
              oldAnswersPosList[o._id] = o.position
            })
          },
          update: function (event, ui) {
            // console.log('content', $(".answer-content"));
            $('.answer-content').each(function (idx) {
              var answerKey = $(this).find('#answerId-input')[0].value
              var answer = _.find($scope.answerList, function (_o) {
                return _o.$$hashKey == answerKey
              })
              answer.position = idx + 1
            })

            applyChangesOnScope($scope, function () {
              $scope.answerList = _.sortBy($scope.answerList, function (o) {
                return o.position
              })
            })
            ui.item[0].style.backgroundColor = ''

            // update bend database
            // console.log("updated answerList", $scope.answerList);
            // console.log('oldAnswersPosList',oldAnswersPosList);

            /* async.map($scope.answerList, function(o, callback){
                if(o.position == oldAnswersPosList[o._id]) {
                    //console.log("pass", o._id);
                    callback(null, null);
                } else {
                    //console.log("update answer", o)
                    BendService.updatePollQuestionAnswer(o, function(ret){
                        callback(null, ret);
                    })
                }

            }, function(err, rets){
                if(err) {
                    console.log(err);
                    return;
                }

                //console.log("updated answers", rets);
            }) */
          }
        })
        $('#answers-container').sortable('option', 'disabled', false)
      }
      $scope.disableSortableAnswers = function () {
        $('#answers-container').sortable('disable')
      }

      $scope.editPollQuestionAnswer = function (answer) {
        $scope.openPollQuestionAnswer(answer)
      }

      $scope.createPollQuestionAnswer = function () {
        var answerObj = {
          count: 0,
          percentage: 0,
          position: $scope.answerList.length + 1,
          question: $scope.pollQuestion
        }
        $scope.openPollQuestionAnswer(answerObj)
      }

      $scope.getCount = function (item) {
        var count = 0

        if ($scope.user.communityAdmin) {
          return item.counts[$scope.user.community._id] || 0
        }
        _.map(item.counts || [], function (o) {
          count += o
        })

        return count
      }

      $scope.isValidForm = function () {
        var isValid = $scope.form.validateForm.$valid
        if ($scope.isSaving) {
          isValid = false
        }

        return isValid
      }
      $scope.deletePollQuestionAnswer = function (item, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        // var msg = "Deleting the Poll Question Answer is permanent and can not be undone.";
        $scope.answerList = _.difference($scope.answerList, item)
        $scope.answerList.map(function (o, idx) {
          o.position = idx + 1
        })
        /* for(var i = 0 ; i < $scope.answerList.length ; i++) {
            if($scope.answerList[i]._id == id) {
                applyChangesOnScope($scope, function(){
                    $scope.answerList.splice(i, 1);
                    return;
                })
            }
        } */
        /* $bootbox.confirm(msg, function(result) {
            if(result) {
                BendService.deletePollQuestionAnswer(id, function(ret){
                    if(ret){
                        BendService.pollQuestionAnswerCountUpdate($scope.pollQuestion._id, function(){
                            for(var i = 0 ; i < $scope.answerList.length ; i++) {
                                if($scope.answerList[i]._id == id) {
                                    applyChangesOnScope($scope, function(){
                                        $scope.answerList.splice(i, 1);
                                        return;
                                    })
                                }
                            }
                        });
                    }
                })
            }
        }); */
      }

      $rootScope.enableSort = function () {
        $scope.enableSortableAnswers()
      }

      $scope.openDateWindow = function ($event) {
        $event.preventDefault()
        $event.stopPropagation()

        $scope.formData.openedDateSelector = true
      }

      $scope.openPollQuestionAnswer = function (answerObj) {
        $modal.open({
          templateUrl: 'editAnswer.html',
          backdrop: 'static',
          controller: function ($scope, $modalInstance, answerObj, answerList) {
            $scope.CommonUtil = CommonUtil
            $scope.pollAnswer = _.clone(answerObj)

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }

            $scope.saveAnswerDo = function () {
              if ($scope.pollAnswer.$$hashKey) {
                // delete $scope.pollAnswer.$$hashKey;
                var ref = _.find(answerList, function (o) {
                  return o.$$hashKey == $scope.pollAnswer.$$hashKey
                })

                _.extend(ref, $scope.pollAnswer)
                $modalInstance.dismiss('cancel')
                /* if($scope.pollAnswer.question != "") {
                    $scope.pollAnswer.question = CommonUtil.makeBendRef($scope.pollAnswer.question._id, "pollQuestion")
                }

                BendService.updatePollQuestionAnswer($scope.pollAnswer, function(ret){
                    var ref = _.find(answerList, function(o){
                        return o._id == ret._id;
                    })

                    _.extend(ref, ret);

                    //update answer count of question
                    BendService.pollQuestionAnswerCountUpdate($scope.pollAnswer.question._id, function(){
                        $modalInstance.dismiss('cancel');
                    });
                }) */
              } else {
                // $scope.pollAnswer.question = CommonUtil.makeBendRef($scope.pollAnswer.question._id, "pollQuestion")
                answerList.push($scope.pollAnswer)
                $modalInstance.dismiss('cancel')
                /* BendService.createPollQuestionAnswer($scope.pollAnswer, function(ret){
                    BendService.pollQuestionAnswerCountUpdate($scope.pollAnswer.question._id, function(){
                        $modalInstance.dismiss('cancel');
                    });
                }) */

                $rootScope.enableSort()
              }
            }

            $scope.canSubmitValidationForm = function () {
              return $scope.form.validateForm2.$valid
            }
          },
          resolve: {
            answerObj: function () {
              return answerObj
            },
            answerList: function () {
              return $scope.answerList
            }
          }
        })
      }

      $scope.savePollQuestionDo = function () {
        // console.log('pollquestion',$scope.pollQuestion);
        var pollQData = _.clone($scope.pollQuestion)
        if (pollQData.community && pollQData.community._id != '') {
          pollQData.community = CommonUtil.makeBendRef(pollQData.community._id, 'community')
        } else {
          delete pollQData.community
        }

        if (typeof $scope.formData.pollDate === 'object') {
          if ($scope.formData.pollDate) { pollQData.pollDate = CommonUtil.formatDateWithFormat($scope.formData.pollDate.getTime() * 1000000, 'YYYY-MM-DD') } else { delete pollQData.pollDate }
        }

        /* if($scope.formData.communities.length > 0) {
            pollQData.communities = CommonUtil.getIdList($scope.formData.communities);
        }
        else {
            delete pollQData.communities;
        } */

        if ($scope.pollQuestionId) {
          $scope.isSaving = true
          if ($scope.formData.teams && $scope.formData.teams.length > 0) {
            pollQData.teams = CommonUtil.getIdList($scope.formData.teams)
          } else {
            delete pollQData.teams
          }
          BendService.updatePollQuestion(pollQData, $scope.answerList, function (err, ret) {
            $scope.isSaving = false
            if (err) {
              console.log(err); return
            }
            $location.path('/pollQuestions')
          })
        } else {
          // create question
          $scope.isSaving = true
          if (!$scope.user.communityAdmin) {
            BendService.createPollQuestion(pollQData, $scope.formData.communities, $scope.answerList, function (err, ret) {
              $scope.isSaving = false
              if (err) {
                console.log(err); return
              }

              $location.path('/pollQuestions')
            })
          } else {
            BendService.createPollQuestion(pollQData, [$scope.user.community], $scope.answerList, function (err, ret) {
              if (err) {
                $scope.isSaving = false
                console.log(err); return
              }

              $location.path('/pollQuestions')
            })
          }
        }
      }
    }])
