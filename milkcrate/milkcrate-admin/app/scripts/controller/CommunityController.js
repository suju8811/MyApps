'use strict'

angular.module('app.controllers')
  .controller('CommunityController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout) {
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.communityList = []
      $scope.statusList = ['Live', 'Onboarding', 'Demo']

      $scope.filter = {
        status: (CommonUtil.getStorage('community-filter') ? CommonUtil.getStorage('community-filter').status : '')
      }

      $scope.onSearch = function () {
        console.log('onSearch')
        CommonUtil.setStorage('community-filter', {
          status: $scope.filter.status
        })
        $scope.isLoading = true

        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('name')

        if ($scope.filter.status != '') {
          query.equalTo('status', $scope.filter.status)
        }

        $bend.DataStore.find('community', query, {
          relations: {
            logo: 'BendFile'
          }
        }).then(function (rets) {
          $scope.communityList = rets
          $scope.isLoading = false
        }, function (err) {
          console.log(err)
        })
      }

      $scope.onSearch()

      $scope.deleteCommunity = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Deleting the client is permanent and can not be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deleteCommunity(id, function (ret) {
              if (ret) {
                for (var i = 0; i < $scope.communityList.length; i++) {
                  if ($scope.communityList[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.communityList.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $scope.createCommunity = function () {
        return $location.path('/communities/new')
      }

      $scope.editCommunity = function (communityObj) {
        return $location.path('/communities/' + communityObj._id)
      }
    }])
