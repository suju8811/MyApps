angular.module('app.controllers')
  .controller('ChallengeCalendarController', ['$scope', '$theme', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout',
    function ($scope, $theme, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout) {
      'use strict'

      $scope.filter = {
        community: null
      }
      $scope.events = []
      $scope.eventSources = [$scope.events]
      $scope.communityList = []
      $scope.CommonUtil = CommonUtil
      $scope.user = BendAuth.getActiveUser()

      BendService.getCommunityList(function (rets) {
        $scope.communityList = rets
        $scope.filter.community = $scope.communityList[0]

        $scope.getChallengeList()
      })

      $scope.uiConfig = {
        calendar: {
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
          selectable: true,
          buttonText: {
            prev: '<i class="fa fa-angle-left"></i>',
            next: '<i class="fa fa-angle-right"></i>',
            prevYear: '<i class="fa fa-angle-double-left"></i>', // <<
            nextYear: '<i class="fa fa-angle-double-right"></i>', // >>
            today: 'Today',
            month: 'Month',
            week: 'Week',
            day: 'Day'
          }
        }
      }

      $scope.getChallengeList = function () {
        $scope.isLoading = true
        var query = new $bend.Query()

        query.notEqualTo('deleted', true)
        query.exists('startsAt', true)
        query.exists('endsAt', true)

        if ($scope.filter.community instanceof Object && $scope.filter.community._id) {
          query.containsAll('communities', [$scope.filter.community._id])
        }

        if ($scope.user.communityAdmin) {
          query.equalTo('community._id', $scope.user.community._id)
        }
        query.descending('startsAt')

        $bend.DataStore.find('challenge', query).then(function (rets) {
          $scope.challengeList = rets
          $scope.isLoading = false

          var current = Date.now()
          $scope.events.length = 0
          _.map($scope.challengeList, function (o) {
            if (o.startsAt && o.endsAt) {
              $scope.events.push({
                title: o.title,
                start: new Date(o.startsAt / 1000000),
                end: new Date(o.endsAt / 1000000),
                url: '#/challenges/' + o._id,
                allDay: false,
                backgroundColor: (
                  o.startsAt / 1000000 > current ? $theme.getBrandColor('primary') : (
                    o.endsAt / 1000000 < current ? $theme.getBrandColor('midnightblue') : $theme.getBrandColor('warning')
                  )
                )
              })
            }
          })
        }, function (err) {
          console.log(err)
        })
      }

      // var date = new Date()
      // var d = date.getDate()
      // var m = date.getMonth()
      // var y = date.getFullYear()
    }])
