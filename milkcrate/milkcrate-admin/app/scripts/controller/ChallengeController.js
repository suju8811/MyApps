'use strict'

angular.module('app.controllers')
  .controller('ChallengeController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout) {
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.challengeList = []
      $scope.activityList = []
      $scope.actionList = []
      $scope.collectionView = {
        searchTerm: '',
        itemsPerPage: 20,
        isLoading: true,
        totalItems: 0,
        currentPage: 1,
        numPages: 0
      }

      $scope.communityList = [{
        name: 'All Client',
        _id: null
      }]

      $scope.filterTypeList = [{
        name: 'All Challenges',
        value: ''
      }, {
        name: 'Expired Challenges',
        value: '1'
      }, {
        name: 'Unexpired Challenges',
        value: '2'
      }]

      $scope.filter = {
        community: (CommonUtil.getStorage('challenge-filter') ? CommonUtil.getStorage('challenge-filter').community : ''),
        type: (CommonUtil.getStorage('challenge-filter') ? CommonUtil.getStorage('challenge-filter').type : $scope.filterTypeList[0]),
        team: ''
      }

      $scope.teams = []
      $scope.communityTeams = []

      $scope.user = BendAuth.getActiveUser()

      $scope.reset = function () {
        $scope.filter = {
          community: '',
          type: $scope.filterTypeList[0]
        }

        $scope.getChallengeList()
      }

      async.parallel([
        function (cb) {
          BendService.getAllTeamList(function (err, rets) {
            $scope.teams = rets
            cb(null)
          })
        },
        function (cb) {
          BendService.getCommunityList(function (rets) {
            $scope.communityList = $scope.communityList.concat(rets)
            cb(null)
          })
        }
      ], function (err, rets) {
        $scope.getChallengeList()
      })

      $scope.getTeamNames = function (teamIds) {
        if (!teamIds || teamIds.length == 0) return ''

        var names = []
        _.map($scope.teams, function (o) {
          if (teamIds.indexOf(o._id) != -1) { names.push(o.name) }
        })

        return names.join(',')
      }

      $scope.getChallengeList = function () {
        CommonUtil.setStorage('challenge-filter', $scope.filter)

        if ($scope.filter.community && $scope.filter.community._id) {
          var teams = _.filter($scope.teams, function (o) {
            return o.community._id == $scope.filter.community._id
          })
          if (teams.length > 0) {
            $scope.communityTeams = [{
              name: 'All Teams',
              _id: null
            }].concat(teams)
          } else {
            $scope.communityTeams = []
          }
        } else {
          $scope.communityTeams = []
        }

        $scope.isLoading = true
        var query = new $bend.Query()

        query.limit($scope.collectionView.itemsPerPage)
        query.skip(($scope.collectionView.currentPage - 1) * $scope.collectionView.itemsPerPage)

        query.notEqualTo('deleted', true)
        var type = $scope.filter.type
        if (type && type.value == '1') {
          query.lessThan('endsAt', CommonUtil.getToday() * 1000000)
        } else if (type && type.value == '2') {
          query.greaterThanOrEqualTo('endsAt', CommonUtil.getToday() * 1000000)
        }

        if ($scope.filter.community instanceof Object && $scope.filter.community._id) {
          query.containsAll('communities', [$scope.filter.community._id])
        }

        if ($scope.user.communityAdmin) {
          query.equalTo('community._id', $scope.user.community._id)
        }

        if ($scope.communityTeams.length > 0 && $scope.filter.team != '' && $scope.filter.team._id != null) {
          query.contains('teams', [$scope.filter.team._id])
        }

        query.descending('startsAt')

        $bend.DataStore.find('challenge', query, {
          relations: {
            activity: ['action', 'business', 'event', 'volunteer_opportunity']
          }
        }).then(function (rets) {
          $scope.challengeList = rets
          $scope.isLoading = false

          _.map($scope.challengeList, function (o) {
            if (o.startsAt) {
              o.startsAt = CommonUtil.formatDateWithFormat(o.startsAt, 'YYYY-MM-DD LT')// CommonUtil.formatDate(ret.startsAt);//CommonUtil.formatDateWithFormat(ret.startsAt,"YYYY-MM-DD");
              // console.log(o.startsAt);
            }
            if (o.endsAt) {
              o.endsAt = CommonUtil.formatDateWithFormat(o.endsAt, 'YYYY-MM-DD LT')// CommonUtil.formatDate(ret.endsAt);//CommonUtil.formatDateWithFormat(ret.endsAt,"YYYY-MM-DD");
            }
          })
        }, function (err) {
          console.log(err)
        })

        $bend.DataStore.count('challenge', query).then(function (count) {
          applyChangesOnScope($scope, function () {
            $scope.collectionView.totalItems = count
            $scope.collectionView.numPages = $scope.collectionView.totalItems / $scope.collectionView.itemsPerPage + 1
          })
        }, function (err) {
          console.log(err)
        })
      }

      $scope.onPageChange = function () {
        $scope.getChallengeList()
      }

      $scope.deleteChallenge = function (id, $ev) {
        $ev.stopPropagation()
        $ev.preventDefault()
        var msg = 'Deleting the places is permanent and can not be undone.'
        $bootbox.confirm(msg, function (result) {
          if (result) {
            BendService.deleteChallenge(id, function (ret) {
              if (ret) {
                for (var i = 0; i < $scope.challengeList.length; i++) {
                  if ($scope.challengeList[i]._id == id) {
                    applyChangesOnScope($scope, function () {
                      $scope.challengeList.splice(i, 1)
                    })
                  }
                }
              }
            })
          }
        })
      }

      $scope.editChallenge = function (challenge, $event) {
        if ($event.metaKey || $event.ctrlKey) {
          window.open('#/challenges/' + challenge._id, '_blank')
        } else { return $location.path('/challenges/' + challenge._id) }
      }

      $scope.createChallenge = function ($event) {
        if ($event.metaKey || $event.ctrlKey) {
          window.open('#/challenges/new', '_blank')
        } else { return $location.path('/challenges/new') }
      }

      $scope.getCommunityNames = function (list) {
        var ret = []
        _.map(list, function (o) {
          var client = _.find($scope.communityList, function (_o) {
            return _o._id == o
          })
          if (client) { ret.push(client.name) }
        })

        return ret.join(', ')
      }
    }])
