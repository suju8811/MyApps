'use strict'

angular.module('app.controllers')
  .controller('LeaderboardController', ['$scope', '$bend', '$location', '$routeParams', 'BendAuth', 'BendService', 'CommonUtil', '$bootbox', '$rootScope', '$modal', '$timeout',
    function ($scope, $bend, $location, $routeParams, BendAuth, BendService, CommonUtil, $bootbox, $rootScope, $modal, $timeout) {
      $scope.isLoading = true
      $scope.CommonUtil = CommonUtil
      $scope.userList = []
      $scope.collectionView = {
        searchTerm: (CommonUtil.getStorage('leaderboard-filter') ? CommonUtil.getStorage('leaderboard-filter').searchTerm : ''),
        itemsPerPage: 20,
        isLoading: true,
        totalItems: 0,
        currentPage: 1,
        numPages: 0
      }

      $scope.user = $bend.getActiveUser()

      $scope.offset = 0
      $scope.isMore = false

      $scope.totalPoints = 0
      $scope.userPoints = {}

      $scope.communityList = [{
        name: 'All Clients',
        _id: null
      }]

      $scope.sprintList = [{
        name: 'All Time',
        _id: null
      }]

      $scope.categories = [{
        name: 'All Activities',
        _id: null
      }]

      var sprintList = []

      $scope.filter = {
        community: (CommonUtil.getStorage('leaderboard-filter') ? CommonUtil.getStorage('leaderboard-filter').community : null),
        category: (CommonUtil.getStorage('leaderboard-filter') ? CommonUtil.getStorage('leaderboard-filter').category : null),
        sprint: (CommonUtil.getStorage('leaderboard-filter') ? CommonUtil.getStorage('leaderboard-filter').sprint : null)
      }

      if ($scope.user.communityAdmin) {
        $scope.filter.community = {
          name: '',
          _id: $scope.user.community._id
        }
      }

      $scope.user = BendAuth.getActiveUser()

      $scope.reset = function () {
        $scope.filter = {
          community: '',
          category: null,
          sprint: null
        }
        $scope.collectionView.searchTerm = ''

        $scope.getList()
      }

      async.parallel([
        function (cb) {
          if ($scope.user.communityAdmin) {
            cb(null, null)
          } else {
            BendService.getCommunityList(function (rets) {
              $scope.communityList = $scope.communityList.concat(rets)
              cb(null, null)
            })
          }
        },
        function (cb) {
          BendService.getCategoryEnabledList(function (rets) {
            $scope.categories = $scope.categories.concat(rets)
            cb(null, null)
          })
        },
        function (cb) {
          BendService.getSprintList(function (rets) {
            _.map(rets, function (o) {
              o.name = o.startDate + ' ~ ' + o.endDate
            })
            sprintList = rets
            cb(null, null)
          })
        }
      ], function (err, ret) {
        $scope.selectClient()
      })

      $scope.getList = function () {
        CommonUtil.setStorage('leaderboard-filter', Object.assign({}, $scope.filter, {
          searchTerm: $scope.collectionView.searchTerm
        }))
        $scope.isLoading = true
        $scope.offset = 0
        $scope.isMore = false
        var q = new $bend.Query()
        if ($scope.filter.community && $scope.filter.community._id) { q.equalTo('community._id', $scope.filter.community._id) }
        q.notEqualTo('deleted', true)
        q.exists('points', true)

        if ($scope.filter.category) {
          q.containsAll('categories', [$scope.filter.category._id])
        }

        if ($scope.filter.sprint && $scope.filter.sprint._id) {
          var startTime = new Date($scope.filter.sprint.startDate.replace(/-/g, '/')).getTime() * 1000000
          var endTime = new Date($scope.filter.sprint.endDate.replace(/-/g, '/') + ' 23:59:59').getTime() * 1000000
          console.log('start, end', $scope.filter.sprint, startTime, endTime)
          q.greaterThanOrEqualTo('_bmd.createdAt', startTime)
          q.lessThanOrEqualTo('_bmd.createdAt', endTime)
        }
        var aggregation = $bend.Group.sum('points')
        aggregation.by('user._id')
        aggregation.query(q)
        $bend.DataStore.group('activity', aggregation).then(function (sumVal) {
          sumVal = _.sortBy(sumVal, function (o) {
            return o.result * (-1)
          })
          console.log('getLeaderboard', sumVal)
          $scope.userIdList = []
          $scope.userPoints = {}
          // var startIdx = 0
          // var endIdx = $scope.collectionView.itemsPerPage
          var totalPoints = 0
          _.map(sumVal, function (o, idx) {
            $scope.userIdList.push(o['user._id'])
            $scope.userPoints[o['user._id']] = o.result
            totalPoints += o.result
          })

          $scope.totalPoints = totalPoints

          var query = new $bend.Query()
          query.notEqualTo('deleted', true)

          $scope.offset = $scope.collectionView.itemsPerPage
          query.contains('_id', $scope.userIdList.slice(0, $scope.offset))
          query.descending('_bmd.startedAt')
          // query.limit($scope.collectionView.itemsPerPage);
          // query.skip(($scope.collectionView.currentPage-1)*$scope.collectionView.itemsPerPage);

          $bend.User.find(query, {
            relations: {
              avatar: 'BendFile'
            }
          }).then(function (rets) {
            $scope.userList = _.sortBy(rets, function (o) {
              return $scope.userPoints[o._id] * (-1)
            })

            $scope.isMore = $scope.offset < $scope.userIdList.length
            $scope.isLoading = false
          })
        }, function (err) {
          $scope.isLoading = false
          console.log(err)
        })
      }

      $scope.selectClient = function () {
        if ($scope.filter.community) {
          // reset sprint filter
          var filteredSprintList = _.filter(sprintList, function (o) {
            return o.community._id == $scope.filter.community._id
          })

          console.log(filteredSprintList)
          $scope.sprintList = [{
            name: 'All Time',
            _id: null
          }]

          $scope.sprintList = $scope.sprintList.concat(filteredSprintList)
        } else {
          $scope.sprintList = [{
            name: 'All Time',
            _id: null
          }]
        }
        $scope.filter.sprint = null

        console.log($scope.filter.community, $scope.filter.sprint)

        $scope.getList()
      }

      $scope.loadMore = function () {
        $scope.isMore = false
        $scope.isLoading = true
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)

        query.contains('_id', $scope.userIdList.slice($scope.offset, $scope.offset + $scope.collectionView.itemsPerPage))
        $scope.offset += $scope.collectionView.itemsPerPage
        query.descending('_bmd.startedAt')
        // query.limit($scope.collectionView.itemsPerPage);
        // query.skip(($scope.collectionView.currentPage-1)*$scope.collectionView.itemsPerPage);

        $bend.User.find(query, {
          relations: {
            avatar: 'BendFile'
          }
        }).then(function (rets) {
          $scope.userList = $scope.userList.concat(_.sortBy(rets, function (o) {
            return $scope.userPoints[o._id] * (-1)
          }))

          $scope.isMore = $scope.offset < $scope.userIdList.length
          $scope.isLoading = false
        })
      }

      $scope.onPageChange = function () {
        $scope.getList()
      }
    }])
