import '../assets/less/styles.less';
angular
  .module('themesApp', [
    'theme',
    'theme.core.services',
    'theme.chart.flot',
    'oc.lazyLoad',
    /* 'theme.demos', */
    'LocalStorageModule',
    'theme.core.directives',
    'bend',
    'app.common',
    'app.auth',
    'app.pusher',
    'app.directives',
    'app.model',
    'app.service',
    'app.services',
    'ui.bootstrap.datetimepicker',
    'base64',
    'ngCookies',
    'ui.select',
    'ngSanitize',
    'ngCsv',
    'ui.ace',
    'app.controllers',
    'colorpicker.module',
    'theme.calendar',
    'ui.calendar',
    'ckeditor'
  ])
  .config(['$provide', '$routeProvider', 'localStorageServiceProvider', '$sceDelegateProvider', function ($provide, $routeProvider, localStorageServiceProvider, $sceDelegateProvider) {
    'use strict'
    localStorageServiceProvider.setPrefix('Milkcrate')
    $sceDelegateProvider.resourceUrlWhitelist(['**'])
    $routeProvider
      .when('/', {
        templateUrl: 'views/index.html',
        resolve: {
        }
      })
      .when('/users', {
        templateUrl: function (param) {
          return 'views/users/users.html'
        }
      })
      .when('/users/create', {
        templateUrl: function (param) {
          return 'views/users/userEdit.html'
        }
      })
      .when('/users/:id', {
        templateUrl: function (param) {
          return 'views/users/userEdit.html'
        }
      })
      .when('/signin', {
        templateUrl: function (param) {
          return 'views/signin.html'
        }
      })
      .when('/actions', {
        templateUrl: function (param) {
          return 'views/actions/actions.html'
        }
      })
      .when('/actions/:id', {
        templateUrl: function (param) {
          return 'views/actions/actionEdit.html'
        }
      })
      .when('/actions/new', {
        templateUrl: function (param) {
          return 'views/actions/actionEdit.html'
        }
      })
      .when('/businesses', {
        templateUrl: function (param) {
          return 'views/businesses/businesses.html'
        }
      })
      .when('/businesses/:id', {
        templateUrl: function (param) {
          return 'views/businesses/businessEdit.html'
        }
      })
      .when('/businesses/new', {
        templateUrl: function (param) {
          return 'views/businesses/businessEdit.html'
        }
      })
      .when('/challenges', {
        templateUrl: function (param) {
          return 'views/challenges/challenges.html'
        }
      })
      .when('/challenges/:id', {
        templateUrl: function (param) {
          return 'views/challenges/challengeEdit.html'
        }
      })
      .when('/challenges/new', {
        templateUrl: function (param) {
          return 'views/challenges/challengeEdit.html'
        }
      })
      .when('/pollQuestions', {
        templateUrl: function (param) {
          return 'views/polls/pollQuestions.html'
        }
      })
      .when('/pollQuestions/:id', {
        templateUrl: function (param) {
          return 'views/polls/pollQuestionEdit.html'
        }
      })
      .when('/pollQuestions/new', {
        templateUrl: function (param) {
          return 'views/polls/pollQuestionEdit.html'
        }
      })
      .when('/categories', {
        templateUrl: function (param) {
          return 'views/categories/categories.html'
        }
      })
      .when('/collections', {
        templateUrl: function (param) {
          return 'views/collections/collections.html'
        }
      })
      .when('/communities', {
        templateUrl: function (param) {
          return 'views/communities/communities.html'
        }
      })
      .when('/communities/:id', {
        templateUrl: function (param) {
          return 'views/communities/communityEdit.html'
        }
      })
      .when('/communities/new', {
        templateUrl: function (param) {
          return 'views/communities/communityEdit.html'
        }
      })
      .when('/events', {
        templateUrl: function (param) {
          return 'views/events/events.html'
        }
      })
      .when('/events/:id', {
        templateUrl: function (param) {
          return 'views/events/eventEdit.html'
        }
      })
      .when('/events/new', {
        templateUrl: function (param) {
          return 'views/events/eventEdit.html'
        }
      })
      .when('/volunteerings', {
        templateUrl: function (param) {
          return 'views/volunteering/volunteering.html'
        }
      })
      .when('/volunteerings/:id', {
        templateUrl: function (param) {
          return 'views/volunteering/volunteeringEdit.html'
        }
      })
      .when('/volunteerings/new', {
        templateUrl: function (param) {
          return 'views/volunteering/volunteeringEdit.html'
        }
      })
      .when('/services', {
        templateUrl: function (param) {
          return 'views/services/services.html'
        }
      })
      .when('/services/:id', {
        templateUrl: function (param) {
          return 'views/services/serviceEdit.html'
        }
      })
      .when('/services/new', {
        templateUrl: function (param) {
          return 'views/services/serviceEdit.html'
        }
      })
      .when('/activities', {
        templateUrl: function (param) {
          return 'views/activities/activities.html'
        }
      })
      .when('/activities/:id', {
        templateUrl: function (param) {
          return 'views/activities/activityEdit.html'
        }
      })
      .when('/users/:userId/activities/:id', {
        templateUrl: function (param) {
          return 'views/activities/userActivityEdit.html'
        }
      })
      .when('/activities/new', {
        templateUrl: function (param) {
          return 'views/activities/activityEdit.html'
        }
      })
      .when('/users/:userId/activities/new', {
        templateUrl: function (param) {
          return 'views/activities/userActivityEdit.html'
        }
      })
      .when('/certifications', {
        templateUrl: function (param) {
          return 'views/certifications/certifications.html'
        }
      })
      .when('/pushes', {
        templateUrl: function (param) {
          return 'views/push/pushes.html'
        }
      })
      .when('/templates', {
        templateUrl: function (param) {
          return 'views/push/templates.html'
        }
      })
      .when('/notifications', {
        templateUrl: function (param) {
          return 'views/notifications/notifications.html'
        }
      })
      .when('/communityActivities', {
        templateUrl: function (param) {
          return 'views/communityActivities/activities.html'
        }
      })
      .when('/communityActivities/:id', {
        templateUrl: function (param) {
          return 'views/communityActivities/activityEdit.html'
        }
      })
      .when('/communityActivities/new', {
        templateUrl: function (param) {
          return 'views/communityActivities/activityEdit.html'
        }
      })
      .when('/sprints', {
        templateUrl: function (param) {
          return 'views/sprints/sprints.html'
        }
      })
      .when('/communityUsers', {
        templateUrl: function (param) {
          return 'views/users/community/users.html'
        }
      })
      .when('/communityUsers/create', {
        templateUrl: function (param) {
          return 'views/users/community/userEdit.html'
        }
      })
      .when('/communityUsers/:id', {
        templateUrl: function (param) {
          return 'views/users/community/userEdit.html'
        }
      })
      .when('/communityUsers/:userId/activities/:id', {
        templateUrl: function (param) {
          return 'views/users/community/communityUserActivityEdit.html'
        }
      })
      .when('/communityUsers/:userId/activities/new', {
        templateUrl: function (param) {
          return 'views/users/community/communityUserActivityEdit.html'
        }
      })
      .when('/uncodedBusinesses', {
        templateUrl: function (param) {
          return 'views/businesses/uncodedBusinesses.html'
        }
      })
      .when('/teams', {
        templateUrl: function (param) {
          return 'views/teams/teams.html'
        }
      })
      .when('/teams/:id', {
        templateUrl: function (param) {
          return 'views/teams/teamEdit.html'
        }
      })
      .when('/teams/new', {
        templateUrl: function (param) {
          return 'views/teams/teamEdit.html'
        }
      })
      .when('/challengeCalendar', {
        templateUrl: function (param) {
          return 'views/calendars/challengeCalendar.html'
        }
      })
      .when('/content', {
        templateUrl: function (param) {
          return 'views/communities/content.html'
        }
      })
      .when('/leaderboards', {
        templateUrl: function (param) {
          return 'views/leaderboards/leaderboards.html'
        }
      })
      .when('/utilities', {
        templateUrl: function (param) {
          return 'views/utilities/index.html'
        }
      })
      .when('#', {
        templateUrl: 'views/index.html'
      })
      .otherwise({
        redirectTo: '/'
      })
  }])
  .run(['$rootScope', 'BendAuth', 'BendPusher', 'pinesNotifications', '$bend', '$cookieStore', '$cookies', function ($rootScope, BendAuth, BendPusher, pinesNotifications, $bend, $cookieStore, $cookies) {
    // Init globals.
    $rootScope.globals = {
      userSearchFilter: {
        searchTerm: '',
        searchRole: '',
        searchPlan: '',
        customQuery: '',
        sortFlag: [false, false, false, false, false, false, false]
      },
      isAdmin: false
    }
    $rootScope.logoutNotify = null

    if (!$rootScope.itemsPerPage) {
      var saved = parseInt($cookies.itemsPerPage)
      $rootScope.itemsPerPage = saved || 20
    }
    $rootScope.setItemsPerPage = function (val) {
      if (val) {
        val = parseInt(val)
        $cookies.itemsPerPage = val
        $rootScope.itemsPerPage = val
      }
    }

    // Start auth check workflow.
    BendAuth.checkAuth()
    if (BendAuth.isLoggedIn()) {
      // BendPusher.init();
      var stateObj = $cookieStore.get(BendAuth.getActiveUser()._id + '_state')
      // console.log("cookie data", BendAuth.getActiveUser()._id, stateObj);
      if (stateObj) { $rootScope.globals.state = stateObj }
    }

    setInterval(function () {
      var now = Date.now()

      if (BendAuth.isLoggedIn()) { $bend.LocalStorage.save('tokenLastUsedTime', now.valueOf()) }
    }, 1000 * 60)
  }]);

(function () {
  var initInjector = angular.injector(['ng', 'bend', 'app.auth', 'app.service'])
  var BendAuthBootstrap = initInjector.get('BendAuthBootstrap')
  BendAuthBootstrap.bootstrapService()
})()
