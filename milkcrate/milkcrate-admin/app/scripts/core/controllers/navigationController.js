angular
  .module('theme.core.navigation_controller', ['theme.core.services'])
  .controller('NavigationController', ['$scope', '$location', '$timeout', 'BendService', 'BendAuth', '$bend', '$rootScope', function ($scope, $location, $timeout, BendService, BendAuth, $bend, $rootScope) {
    'use strict'

    console.log('NavigationController')
    $scope.menu = [{
      label: '',
      iconClasses: '',
      separator: true,
      type: 'both'
    }, {
      label: 'Dashboard',
      iconClasses: 'glyphicon glyphicon-home',
      url: '#/',
      type: 'both'
    }]

    $rootScope.initMenu = function () {
      if (BendAuth.getActiveUser() == null) return
      if (!BendAuth.getActiveUser().communityAdmin) {
        $scope.isAdmin = true
        $scope.menu = [{
          label: 'User Management',
          iconClasses: '',
          separator: true,
          type: 'both'
        }, {
          label: 'Dashboard',
          iconClasses: 'glyphicon glyphicon-home',
          url: '#/',
          type: 'both'
        }, {
          label: 'Users',
          iconClasses: 'glyphicon glyphicon-user',
          url: '#/users',
          type: 'both'
        }, {
          label: 'User Activity',
          iconClasses: 'fa fa-eye',
          url: '#/activities',
          type: 'both'
        }, {
          label: 'Sprints',
          iconClasses: 'fa fa-calendar',
          url: '#/sprints',
          type: 'both'
        }, {
          label: 'Teams',
          iconClasses: 'fa fa-users',
          url: '#/teams',
          type: 'both'
        }, {
          label: 'Leaderboards',
          iconClasses: 'fa fa-table',
          url: '#/leaderboards',
          type: 'both'
        }, {
          label: 'Communication Tools',
          iconClasses: '',
          separator: true
        }, {
          label: 'Push Notifications',
          iconClasses: 'glyphicon glyphicon-bell',
          type: 'both',
          children: [{
            label: 'Push Notifications',
            url: '#/pushes'
          }, {
            label: 'Templates',
            url: '#/templates'
          }]
        }, {
          label: 'Poll Questions',
          iconClasses: 'fa fa-question-circle-o',
          url: '#/pollQuestions',
          type: 'both'
        }, {
          label: 'Challenges',
          iconClasses: 'fa fa-cog',
          url: '#/challenges',
          type: 'both'
        }, {
          label: 'Create Activities',
          iconClasses: '',
          separator: true
        }, {
          label: 'Places',
          iconClasses: 'glyphicon glyphicon-th-large',
          url: '#/businesses',
          type: 'both'
        },
          /* {
                  label: 'Services',
                  iconClasses: 'glyphicon glyphicon-briefcase',
                  url: '#/services',
                  type:'both'
                }, */
        {
          label: 'Events',
          iconClasses: 'glyphicon glyphicon-calendar',
          url: '#/events',
          type: 'both'
        }, {
          label: 'Volunteering',
          iconClasses: 'glyphicon glyphicon-heart',
          url: '#/volunteerings',
          type: 'both'
        }, {
          label: 'Actions',
          iconClasses: 'glyphicon glyphicon-tasks',
          url: '#/actions',
          type: 'both'
        }, {
          label: 'MilkCrate Admin',
          iconClasses: '',
          separator: true
        }, {
          label: 'Clients',
          iconClasses: 'glyphicon glyphicon-globe',
          url: '#/communities',
          type: 'both'
        }, {
          label: 'Categories',
          iconClasses: 'glyphicon glyphicon-tags',
          url: '#/categories',
          type: 'both'
        }, {
          label: 'Collections',
          iconClasses: 'glyphicon glyphicon-link',
          url: '#/collections',
          type: 'both'
        }, {
          label: 'Certifications',
          iconClasses: 'glyphicon glyphicon-bookmark',
          url: '#/certifications',
          type: 'both'
        }, {
          label: 'Calendars',
          iconClasses: 'fa fa-calendar',
          type: 'both',
          children: [{
            label: 'Challenges',
            url: '#/challengeCalendar'
          }]
        }, {
          label: 'Utilities',
          iconClasses: 'fa fa-wrench',
          url: '#/utilities',
          type: 'both'
        }
        ]
      } else {
        $scope.menu = [{
          label: 'User Management',
          iconClasses: '',
          separator: true,
          type: 'both'
        }, {
          label: 'Dashboard',
          iconClasses: 'glyphicon glyphicon-home',
          url: '#/',
          type: 'both'
        }, {
          label: 'Users',
          iconClasses: 'glyphicon glyphicon-user',
          url: '#/communityUsers',
          type: 'both'
        }, {
          label: 'User Activity',
          iconClasses: 'fa fa-eye',
          url: '#/communityActivities',
          type: 'both'
        }, {
          label: 'Teams',
          iconClasses: 'fa fa-users',
          url: '#/teams',
          type: 'both'
        }, {
          label: 'Leaderboards',
          iconClasses: 'fa fa-table',
          url: '#/leaderboards',
          type: 'both'
        }, {
          label: 'Create Activities',
          iconClasses: '',
          separator: true,
          type: 'both'
        }, {
          label: 'Places',
          iconClasses: 'glyphicon glyphicon-th-large',
          url: '#/businesses',
          type: 'both'
        }, /* {
                  label: 'Services',
                  iconClasses: 'glyphicon glyphicon-briefcase',
                  url: '#/services',
                  type:'both'
              }, */
        {
          label: 'Events',
          iconClasses: 'glyphicon glyphicon-calendar',
          url: '#/events',
          type: 'both'
        }, {
          label: 'Volunteering',
          iconClasses: 'glyphicon glyphicon-heart',
          url: '#/volunteerings',
          type: 'both'
        }, {
          label: 'Actions',
          iconClasses: 'glyphicon glyphicon-tasks',
          url: '#/actions',
          type: 'both'
        }, {
          label: 'Communication Tools',
          iconClasses: '',
          separator: true,
          type: 'both'
        }, {
          label: 'Push Notifications',
          iconClasses: 'glyphicon glyphicon-bell',
          type: 'both',
          children: [{
            label: 'Push Notifications',
            url: '#/notifications'
          }, {
            label: 'Templates',
            url: '#/templates'
          }]
        }, {
          label: 'Poll Questions',
          iconClasses: 'fa fa-question-circle-o',
          url: '#/pollQuestions',
          type: 'both'
        }, {
          label: 'Challenges',
          iconClasses: 'fa fa-cog',
          url: '#/challenges',
          type: 'both'
        }, {
          label: 'Configuration',
          iconClasses: '',
          separator: true,
          type: 'both'
        }, {
          label: 'Collections',
          iconClasses: 'glyphicon glyphicon-link',
          url: '#/collections',
          type: 'both'
        }, {
          label: 'Content',
          iconClasses: 'fa fa-filter',
          url: '#/content',
          type: 'both'
        }
        ]
      }
    }

    $rootScope.initMenu()

    var setParent = function (children, parent) {
      angular.forEach(children, function (child) {
        child.parent = parent
        if (child.children !== undefined) {
          setParent(child.children, child)
        }
      })
    }

    $scope.findItemByUrl = function (children, url) {
      for (var i = 0, length = children.length; i < length; i++) {
        if (children[i].url && children[i].url.replace('#', '') === url) {
          return children[i]
        }
        if (children[i].children !== undefined) {
          var item = $scope.findItemByUrl(children[i].children, url)
          if (item) {
            return item
          }
        }
      }
    }

    setParent($scope.menu, null)

    $scope.openItems = []; $scope.selectedItems = []; $scope.selectedFromNavMenu = false

    $scope.select = function (item) {
      // close open nodes
      if (item.open) {
        item.open = false
        return
      }

      for (var i = $scope.openItems.length - 1; i >= 0; i--) {
        $scope.openItems[i].open = false
      }
      $scope.openItems = []
      var parentRef = item
      while (parentRef) {
        parentRef.open = true
        $scope.openItems.push(parentRef)
        parentRef = parentRef.parent
      }

      // handle leaf nodes
      if (!item.children || (item.children && item.children.length < 1)) {
        $scope.selectedFromNavMenu = true
        for (var j = $scope.selectedItems.length - 1; j >= 0; j--) {
          $scope.selectedItems[j].selected = false
        }
        $scope.selectedItems = []
        parentRef = item
        while (parentRef) {
          parentRef.selected = true
          $scope.selectedItems.push(parentRef)
          parentRef = parentRef.parent
        }
      }
    }

    $scope.highlightedItems = []
    var highlight = function (item) {
      var parentRef = item
      while (parentRef) {
        if (parentRef.selected) {
          parentRef = null
          continue
        }
        parentRef.selected = true
        $scope.highlightedItems.push(parentRef)
        parentRef = parentRef.parent
      }
    }

    var highlightItems = function (children, query) {
      angular.forEach(children, function (child) {
        if (child.label.toLowerCase().indexOf(query) > -1) {
          highlight(child)
        }
        if (child.children !== undefined) {
          highlightItems(child.children, query)
        }
      })
    }

    // $scope.searchQuery = '';
    $scope.$watch('searchQuery', function (newVal, oldVal) {
      var currentPath = '#' + $location.path()
      if (newVal === '') {
        for (var i = $scope.highlightedItems.length - 1; i >= 0; i--) {
          if ($scope.selectedItems.indexOf($scope.highlightedItems[i]) < 0) {
            if ($scope.highlightedItems[i] && $scope.highlightedItems[i] !== currentPath) {
              $scope.highlightedItems[i].selected = false
            }
          }
        }
        $scope.highlightedItems = []
      } else
      if (newVal !== oldVal) {
        for (var j = $scope.highlightedItems.length - 1; j >= 0; j--) {
          if ($scope.selectedItems.indexOf($scope.highlightedItems[j]) < 0) {
            $scope.highlightedItems[j].selected = false
          }
        }
        $scope.highlightedItems = []
        highlightItems($scope.menu, newVal.toLowerCase())
      }
    })

    $scope.$on('$routeChangeSuccess', function () {
      if ($scope.selectedFromNavMenu === false) {
        var item = $scope.findItemByUrl($scope.menu, $location.path())
        if (item) {
          $timeout(function () {
            applyChangesOnScope($scope, function () {
              $scope.select(item)
            })
          })
        }
      }
      $scope.selectedFromNavMenu = false
      $scope.searchQuery = ''
    })
  }])
