angular.module('theme.core.main_controller',
  [
    'theme.core.services',
    'bend',
    'app.auth'
  ])
  .controller('MainController', ['$scope', '$theme', '$timeout', 'progressLoader', '$location', 'BendAuth', 'BendService', '$rootScope', '$modal', '$bend', 'pinesNotifications', 'CommonUtil',
    function ($scope, $theme, $timeout, progressLoader, $location, BendAuth, BendService, $rootScope, $modal, $bend, pinesNotifications, CommonUtil) {
      'use strict'
      // $scope.layoutIsSmallScreen = false;
      $scope.layoutFixedHeader = $theme.get('fixedHeader')
      $scope.layoutPageTransitionStyle = $theme.get('pageTransitionStyle')
      $scope.layoutDropdownTransitionStyle = $theme.get('dropdownTransitionStyle')
      $scope.layoutPageTransitionStyleList = ['bounce',
        'flash',
        'pulse',
        'bounceIn',
        'bounceInDown',
        'bounceInLeft',
        'bounceInRight',
        'bounceInUp',
        'fadeIn',
        'fadeInDown',
        'fadeInDownBig',
        'fadeInLeft',
        'fadeInLeftBig',
        'fadeInRight',
        'fadeInRightBig',
        'fadeInUp',
        'fadeInUpBig',
        'flipInX',
        'flipInY',
        'lightSpeedIn',
        'rotateIn',
        'rotateInDownLeft',
        'rotateInDownRight',
        'rotateInUpLeft',
        'rotateInUpRight',
        'rollIn',
        'zoomIn',
        'zoomInDown',
        'zoomInLeft',
        'zoomInRight',
        'zoomInUp'
      ]

      $scope.layoutLoading = true
      $scope.userInfo = null
      $scope.CommonUtil = CommonUtil
      $scope.org = {}
      $scope.community = {}

      $scope.initUser = function () {
        $scope.userInfo = BendAuth.getActiveUser()

        if ($scope.userInfo.communityAdmin) {
          BendService.getCommunity2($scope.userInfo.community._id, function (err, ret) {
            if (err) {
              console.log(err); return
            }

            $scope.community = ret
          })
        }
      }

      if (BendAuth.isLoggedIn()) {
        $scope.initUser()
      }

      $scope.$on('login_initialize', function () {
        $scope.initUser()
      })

      $scope.getLayoutOption = function (key) {
        return $theme.get(key)
      }

      $scope.setNavbarClass = function (classname, $event) {
        $event.preventDefault()
        $event.stopPropagation()
        $theme.set('topNavThemeClass', classname)
      }

      $scope.setSidebarClass = function (classname, $event) {
        $event.preventDefault()
        $event.stopPropagation()
        $theme.set('sidebarThemeClass', classname)
      }

      $scope.$watch('layoutFixedHeader', function (newVal, oldval) {
        if (newVal === undefined || newVal === oldval) {
          return
        }
        $theme.set('fixedHeader', newVal)
      })
      $scope.$watch('layoutLayoutBoxed', function (newVal, oldval) {
        if (newVal === undefined || newVal === oldval) {
          return
        }
        $theme.set('layoutBoxed', newVal)
      })
      $scope.$watch('layoutLayoutHorizontal', function (newVal, oldval) {
        if (newVal === undefined || newVal === oldval) {
          return
        }
        $theme.set('layoutHorizontal', newVal)
      })
      $scope.$watch('layoutPageTransitionStyle', function (newVal) {
        $theme.set('pageTransitionStyle', newVal)
      })
      $scope.$watch('layoutDropdownTransitionStyle', function (newVal) {
        $theme.set('dropdownTransitionStyle', newVal)
      })

      $scope.hideHeaderBar = function () {
        $theme.set('headerBarHidden', true)
      }

      $scope.showHeaderBar = function ($event) {
        $event.stopPropagation()
        $theme.set('headerBarHidden', false)
      }

      $scope.toggleLeftBar = function () {
        if ($scope.layoutIsSmallScreen) {
          return $theme.set('leftbarShown', !$theme.get('leftbarShown'))
        }
        $theme.set('leftbarCollapsed', !$theme.get('leftbarCollapsed'))
      }

      $scope.toggleRightBar = function () {
        $theme.set('rightbarCollapsed', !$theme.get('rightbarCollapsed'))
      }

      $scope.toggleSearchBar = function ($event) {
        $event.stopPropagation()
        $event.preventDefault()
        $theme.set('showSmallSearchBar', !$theme.get('showSmallSearchBar'))
      }

      $scope.$on('themeEvent:maxWidth767', function (event, newVal) {
        $timeout(function () {
          $scope.layoutIsSmallScreen = newVal
          if (!newVal) {
            $theme.set('leftbarShown', false)
          } else {
            $theme.set('leftbarCollapsed', false)
          }
        })
      })
      $scope.$on('themeEvent:changed:fixedHeader', function (event, newVal) {
        $scope.layoutFixedHeader = newVal
      })
      $scope.$on('themeEvent:changed:layoutHorizontal', function (event, newVal) {
        $scope.layoutLayoutHorizontal = newVal
      })
      $scope.$on('themeEvent:changed:layoutBoxed', function (event, newVal) {
        $scope.layoutLayoutBoxed = newVal
      })

      // there are better ways to do this, e.g. using a dedicated service
      // but for the purposes of this demo this will do :P
      $scope.isLoggedIn = true
      $scope.logOut = function () {
        BendAuth.logOut()
      }
      $scope.logIn = function () {
        $scope.isLoggedIn = true
      }

      $scope.getUserName = function () {
        var user = BendAuth.getActiveUser()
        if (user) { return user.username } else { return '' }
      }

      $scope.rightbarAccordionsShowOne = false
      $scope.rightbarAccordions = [{
        open: true
      }, {
        open: true
      }, {
        open: true
      }, {
        open: true
      }, {
        open: true
      }, {
        open: true
      }, {
        open: true
      }]

      $scope.$on('$routeChangeStart', function () {
        BendAuth.checkAuth()

        progressLoader.start()
        progressLoader.set(50)
      })
      $scope.$on('$routeChangeSuccess', function () {
        progressLoader.end()
        if ($scope.layoutLoading) {
          $scope.layoutLoading = false
        }
      })

      $scope.$on('update-organization', function (evt, org) {
        console.log(org)
        $scope.org.name = org.name
      })

      $scope.openProfile = function () {
        $modal.open({
          templateUrl: 'userProfile.html',
          controller: function ($scope, $modalInstance) {
            $scope.isLoadingSaveProfile = false
            $scope.isLoadingSaveAccout = false
            var activeUser = BendAuth.getActiveUser()

            $scope.user = activeUser
            $scope.form = {
              name: $scope.user.name,
              email: $scope.user.email
            }
            $scope.form.isValidPassword = false

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.close = function () {
              $modalInstance.dismiss('cancel')
            }
            $scope.saveProfile = function () {
              $scope.isLoadingSaveProfile = true
              activeUser.name = $scope.form.name
              $bend.User.update(activeUser).then(function (user) {
                $scope.isLoadingSaveProfile = false
              }, function (err) {
                console.log(err)
                $scope.isLoadingSaveProfile = false
              })
            }

            $scope.saveAccount = function () {
              var activeUserClone = _.clone(activeUser)
              $bend.setActiveUser(null)
              var credentials = {username: activeUser.username, password: $scope.form.oldPassword}
              $bend.User.login(credentials).then(
                function (res) {
                  $scope.isLoadingSaveAccout = true
                  activeUser = BendAuth.getActiveUser()
                  activeUser.password = $scope.form.newPassword
                  $bend.User.update(activeUser).then(function (user) {
                    var creditial = {username: activeUser.username, password: $scope.form.newPassword}
                    $bend.Sync.destruct()
                    $bend.setActiveUser(null)
                    $scope.isLoadingSaveAccout = false
                    BendAuth.logIn(creditial)

                    var notify = pinesNotifications.notify({
                      text: '<p>Password changed</p>',
                      type: 'info',
                      width: '200px'
                    })
                    setTimeout(function () {
                      notify.remove()
                    }, 2000)
                    // return $location.path('/signin');
                  }, function (err) {
                    console.log(err)
                    $scope.isLoadingSaveAccout = false
                  })
                },
                function (error) {
                  console.log(error)
                  $scope.form.isValidPassword = true
                  $bend.setActiveUser(activeUserClone)
                  activeUser = BendAuth.getActiveUser()
                }
              )
            }

            $scope.canSubmitValidationForm = function () {
              return $scope.form.validateForm.$valid
            }
            $scope.canSubmitAccountForm = function () {
              return $scope.form.accountForm.$valid
            }
          },
          size: 'lg'
        })
      }
    }])
