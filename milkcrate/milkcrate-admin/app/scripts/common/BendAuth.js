'use strict'
angular
  .module('app.auth', [])
  .factory('BendAuthBootstrap', ['$bend', function ($bend) {
    return {
      APP_KEY: '589d36e94bad3014f50128ce',
      APP_SECRET: 'deduKe8DAuA1ry2cYYQXSQEFHgZy9qTvrL0D2lsc',
      APP_ADMIN_GROUP: 'admin',
      APP_URL: 'https://api.bend.io/group/',

      checkAuthToken: function (callback) {
        callback = callback || function () {}

        var expiresIn = 1 * 60 * 60 * 1000 // ~1 hours.
        Bend.appKey = this.APP_KEY
        $bend.LocalStorage.get('tokenLastUsedTime').then(function (timestamp) {
          if (timestamp) {
            var diff = Date.now() - parseInt(timestamp)
            if (diff > expiresIn) {
              console.log('Token has been expired!')
              $bend.LocalStorage.destroy('activeUser')
            }

            callback(null)
            return
          }

          callback(null)
        }, function (error) { callback(error) })
      },
      bootstrapService: function () {
        var that = this
        this.checkAuthToken(function (error) {
          if (error) {
            console.log(error)
          }

          $bend.init({
            appKey: that.APP_KEY,
            appSecret: that.APP_SECRET
          }).then(
            function (activeUser) {
              console.log('init bend')

              angular.bootstrap(document.getElementById('app'), ['themesApp'])
              if (activeUser) {

              }
            },
            function (error) {
              console.log(error)
              angular.bootstrap(document.getElementById('app'), ['themesApp'])
            }
          )
        })
      }
    }
  }])
  .factory('rememberMeService', function () {
    function fetchValue (name) {
      var gCookieVal = document.cookie.split('; ')
      for (var i = 0; i < gCookieVal.length; i++) {
        // a name/value pair (a crumb) is separated by an equal sign
        var gCrumb = gCookieVal[i].split('=')
        if (name === gCrumb[0]) {
          var value = ''
          try {
            value = angular.fromJson(gCrumb[1])
          } catch (e) {
            value = unescape(gCrumb[1])
          }
          return value
        }
      }
      // a cookie with the requested name does not exist
      return null
    }
    return function (name, values) {
      if (arguments.length === 1) return fetchValue(name)
      var cookie = name + '='
      if (typeof values === 'object') {
        var expires = ''
        cookie += (typeof values.value === 'object') ? angular.toJson(values.value) + ';' : values.value + ';'
        if (values.expires) {
          var date = new Date()
          date.setTime(date.getTime() + (values.expires * 24 * 60 * 60 * 1000))
          expires = date.toGMTString()
        }
        cookie += (!values.session) ? 'expires=' + expires + ';' : ''
        cookie += (values.path) ? 'path=' + values.path + ';' : ''
        cookie += (values.secure) ? 'secure;' : ''
      } else {
        cookie += values + ';'
      }
      document.cookie = cookie
    }
  })
  .factory('BendAuth', [
    '$location', '$http', '$bend', 'BendAuthBootstrap', 'BendPusher', '$rootScope', 'rememberMeService', function ($location, $http, $bend, BendAuthBootstrap, BendPusher, $rootScope, rememberMeService) {
      var BendAuth = {}

      // Testing
      BendAuth.APP_KEY = BendAuthBootstrap.APP_KEY
      BendAuth.APP_SECRET = BendAuthBootstrap.APP_SECRET
      BendAuth.GROUP_ID = BendAuthBootstrap.APP_ADMIN_GROUP
      BendAuth.URL = BendAuthBootstrap.APP_URL

      // Production
      // BendAuth.APP_KEY = '?';
      // BendAuth.APP_SECRET = '?';
      // BendAuth.GROUP_ID = "577625462bfca8009b00343c";

      BendAuth.bootstrapService = function () {
        console.log('BendAuth.bootstrapService')
        var $bend = initInjector.get('$bend')
        $bend.init({
          appKey: this.APP_KEY,
          appSecret: this.APP_SECRET
        }).then(function () {
          angular.bootstrap(document.getElementById('app'), ['themesApp'])
        })
      }

      BendAuth.checkAuth = function () {
        if (!this.isLoggedIn()) {
          return this.redirectToLogin()
        } else {
          var user = this.getActiveUser()

          $rootScope.globals.admin = {
            name: (_.isUndefined(user.fullName)) ? user.username : user.fullName,
            id: user._id,
            avatarUrl: '/assets/img/avatarPlaceholder.png'
          }

          if (user.avatar instanceof Object) {
            $bend.File.find(new $bend.Query().equalTo('_id', user.avatar._id)).then(function (res) {
              if (res.length > 0) {
                $rootScope.globals.admin.avatarUrl = _.first(res)._downloadURL
              }
            })
          }
        }
      }

      BendAuth.getActiveUser = function () {
        return $bend.getActiveUser()
      }

      BendAuth.init = function () {
        return $bend.init({
          appKey: BendAuth.APP_KEY,
          appSecret: BendAuth.APP_SECRET
        })
      }

      BendAuth.isLoggedIn = function () {
        return $bend.getActiveUser() != null
      }

      BendAuth.checkEmail = function (username, callback) {
        $bend.User.exists(username).then(function (ret) {
          callback(Boolean(ret))
        }, function () {

        })
      }

      BendAuth.isExpired = function () {
        var activeUser = BendAuth.getActiveUser()
        var now = new Date().getTime() * 1000000
        if (activeUser.subscriptionType == 0 || activeUser.subscriptionType == 1 || activeUser.subscriptionType == 6) {
          if (activeUser.expirationDate) {
            if (activeUser.expirationDate < now) {
              return true
            }
          } else {
            return true
          }
        } else if (activeUser.subscriptionType == 2 || activeUser.subscriptionType == 3 || activeUser.subscriptionType == 4) {
          if (activeUser.expirationDate) {
            if (activeUser.expirationDate < now) {
              return true
            }
          } else {
            return true
          }
        }

        return false
      }

      BendAuth.logIn = function (loginData, callback) {
        callback = callback || function () {}
        var credentials = {
          username: loginData.username,
          password: loginData.password
        }
        $bend.User.login(credentials).then(
          function (res) {
            console.log(res)
            if (res.communityAdmin) {
              callback(null)
            } else {
              BendAuth.isAdmin(res, function (isAdmin) {
                var data = isAdmin ? null : {
                  name: 'Privilege is not sufficient.'
                }
                callback(data)
                if (!isAdmin) {
                  BendAuth.logOut()
                }
              })
            }
          },
          function () {
            var data = {
              name: 'The username or password is incorrect.'
            }
            callback(data)
          }
        )
      }

      BendAuth.logOut = function () {
        if (this.isLoggedIn()) {
          return $bend.User.logout({
            success: function () {
              localStorage.clear()
              BendAuth.redirectToLogin()
              return $bend.Sync.destruct()
            },
            failure: function () {
              return $location.path('/')
            }
          })
        }
      }

      BendAuth.redirectToDashboard = function () {
        return $location.path('/')
      }

      BendAuth.redirectToLogin = function () {
        if ($location.$$url == '/signup') {
          return $location.path('/signup')
        } else if ($location.$$url == '/agreement') {
          return $location.path('/agreement')
        } else if ($location.$$url !== '/signin') {
          return $location.path('/signin')
        }
      }

      BendAuth.isAdmin = function (user, callback) {
        var authString = 'Bend ' + user._bmd.authtoken// res._bmd.authtoken;;
        $.ajax({
          url: BendAuth.URL + BendAuth.APP_KEY + '/' + BendAuth.GROUP_ID,
          headers: {'Authorization': authString}
        }).then(function (ret) {
          console.log('user group', ret)
          var adminList = ret.users.list
          var adminUser = _.find(adminList, function (o) {
            return o._id == user._id
          })
          callback(Boolean(adminUser))
        }, function () {
          var data = false
          callback(data)
        })
      }

      BendAuth.setAdmin = function (user, isAdmin, callback) {
        callback = callback || function () {}
      }
      return BendAuth
    }
  ])
