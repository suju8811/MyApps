'use strict'
/* eslint no-undef: 0 */

angular
  .module('app.service', [])
  .factory('BendService', [
    '$location', '$bend', 'BendAuth', 'BendPusher', '$rootScope', 'CommonUtil', '$cookieStore', function ($location, $bend, BendAuth, BendPusher, $rootScope, CommonUtil, $cookieStore) {
      var BendService = {}

      BendService.init = function () {
        $rootScope.globals = {
          userSearchFilter: {
            searchTerm: '',
            searchRole: '',
            searchPlan: '',
            customQuery: '',
            sortFlag: [false, false, false, false, false, false, false]
          },
          isAdmin: false
        }
        var stateObj = $cookieStore.get(BendAuth.getActiveUser()._id + '_state')
        console.log('cookie data', BendAuth.getActiveUser()._id, stateObj)
        if (stateObj) { $rootScope.globals.state = stateObj }
      }

      BendService.getFile = function (refObj, callback) {
        if (refObj) {
          var query = new $bend.Query()
          query.equalTo('_id', refObj._id)
          $bend.File.find(query).then(function (rets) {
            callback(rets[0])
          }, function (err) {
            console.log(err)
          })
        } else {
          callback(refObj)
        }
      }

      BendService.upload = function (file, callback, ext, progressCallback) {
        var obj = {
          _filename: file.name,
          size: file.size,
          mimeType: file.type
        }

        if (ext) {
          _.extend(obj, ext)
        }

        console.log(obj)

        $bend.File.upload(file, obj, {'public': true}, function (res) {
          console.log(res)
          callback(null, res)
        }, function (total, prog) {
          if (progressCallback) {
            progressCallback(total, prog)
          }
        }).then(function (res) {
          // callback(null, res);
        }, function (error) {
          callback(error)
        })
      }

      // user
      BendService.getUserList = function (callback) {
        var query = new $bend.Query()
        /* query.notEqualTo("deleted", true); */
        query.descending('_bmd.createdAt')
        $bend.User.find(query).then(function (users) {
          callback(users)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.checkUserName = function (username, callback) {
        var query = new $bend.Query()
        query.matches('username', new RegExp('^' + username + '$', 'gi'))
        $bend.User.find(query).then(function (ret) {
          var data = ret && ret.length > 0
          callback(data)
        }, function (err) {
          callback(err)
        })
      }

      BendService.getValidUserList = function (callback) {
        var query = new $bend.Query()
        query.equalTo('enabled', true)
        /* query.notEqualTo("deleted", true); */
        query.ascending('firstName').ascending('lastName')
        $bend.User.find(query).then(function (users) {
          console.log(users)
          callback(users)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.searchUserList = function (searchWords, callback) {
        var query = new $bend.Query()
        query.equalTo('enabled', true)
        /* query.notEqualTo("deleted", true); */
        query.ascending('username')

        query.matches('username', new RegExp('' + searchWords + '+', 'gi'))
        $bend.User.find(query).then(function (users) {
          users.map(function (o) {
            o.fullName = o.firstName + ' ' + o.lastName
          })
          callback(users)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.searchUserPage = function (param) {
        var query = new $bend.Query()
        /* query.notEqualTo("deleted", true); */
        query.ascending('firstName').ascending('lastName')
        if (param.selectedUserType == 1) { query.notEqualTo('provider', true) } else if (param.selectedUserType == 2) {
          query.equalTo('provider', true)
        }
        if (param.searchUserText != '') {
          query.and(new $bend.Query().matches('firstName', new RegExp('' + param.searchUserText + '+', 'gi'))
            .or(new $bend.Query().matches('lastName', new RegExp('' + param.searchUserText + '+', 'gi')))
            .or(new $bend.Query().matches('email', new RegExp('' + param.searchUserText + '+', 'gi')))
            .or(new $bend.Query().matches('username', new RegExp('' + param.searchUserText + '+', 'gi'))))
        }
        query.limit(param.limit).skip(param.limit * (param.page - 1))

        return $bend.User.find(query, {
          relations: {
            avatar: 'BendFile'
          }
        })
      }

      BendService.getUserPageTotalCount = function (param, callback) {
        var query = new $bend.Query()
        /* query.notEqualTo("deleted", true); */
        query.ascending('firstName').ascending('lastName')
        if (param.searchUserText && param.searchUserText != '') {
          query.and(new $bend.Query().matches('firstName', new RegExp('' + param.searchUserText + '+', 'gi'))
            .or(new $bend.Query().matches('lastName', new RegExp('' + param.searchUserText + '+', 'gi')))
            .or(new $bend.Query().matches('email', new RegExp('' + param.searchUserText + '+', 'gi')))
            .or(new $bend.Query().matches('username', new RegExp('' + param.searchUserText + '+', 'gi'))))
        }
        if (param.selectedUserType == 1) { query.notEqualTo('provider', true) } else if (param.selectedUserType == 2) {
          query.equalTo('provider', true)
        }

        $bend.User.count(query).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getAllUserCount = function (param, callback) {
        var query = new $bend.Query()
        /* query.notEqualTo("deleted", true); */
        query.ascending('firstName').ascending('lastName')
        if (param.searchUserText && param.searchUserText != '') {
          query.and(new $bend.Query().matches('firstName', new RegExp('' + param.searchUserText + '+', 'gi'))
            .or(new $bend.Query().matches('lastName', new RegExp('' + param.searchUserText + '+', 'gi')))
            .or(new $bend.Query().matches('email', new RegExp('' + param.searchUserText + '+', 'gi')))
            .or(new $bend.Query().matches('username', new RegExp('' + param.searchUserText + '+', 'gi'))))
        }

        $bend.User.count(query).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getNormalUserCount = function (param, callback) {
        var query = new $bend.Query()
        /* query.notEqualTo("deleted", true); */
        query.ascending('firstName').ascending('lastName')
        if (param.searchUserText && param.searchUserText != '') {
          query.and(new $bend.Query().matches('firstName', new RegExp('' + param.searchUserText + '+', 'gi'))
            .or(new $bend.Query().matches('lastName', new RegExp('' + param.searchUserText + '+', 'gi')))
            .or(new $bend.Query().matches('email', new RegExp('' + param.searchUserText + '+', 'gi')))
            .or(new $bend.Query().matches('username', new RegExp('' + param.searchUserText + '+', 'gi'))))
        }
        query.notEqualTo('provider', true)

        $bend.User.count(query).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getUser = function (userId, callback) {
        $bend.User.get(userId, {
          relations: {
            avatar: 'BendFile',
            coverImage: 'BendFile',
            community: 'community'
          }
        }).then(function (user) {
          callback(user)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.createUser = function (Data, callback) {
        var userData = _.clone(Data)

        delete userData.passwordConfirm

        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('createUser', userData).then(function (ret) {
            callback(ret.user)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.User.create(userData, {
            state: false
          }).then(function (ret) {
            console.log(ret)
            callback(ret)

            // create "createuser" event
            $bend.DataStore.save('event', {
              type: 'createuser',
              user: ret._id,
              date: CommonUtil.getToday()
            }).then(function (ret) {
              console.log(ret)
            }, function (err) {
              console.log(err)
            })
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.updateUser = function (user, callback) {
        var userData = _.clone(user)
        delete userData.$$hashKey

        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('updateUser', userData).then(function (ret) {
            callback(ret.user)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.User.update(userData).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.deleteUser = function (id, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('deleteUser', {userId: id}).then(function (ret) {
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.User.destroy(id).then(function (ret) {
            if (callback) { callback(ret) }
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.setUserAsAdmin = function (userId, isAdmin, callback) {
        var authString = 'Bend ' + BendAuth.getActiveUser()._bmd.authtoken
        $.ajax({
          url: BendAuth.URL + BendAuth.APP_KEY + '/' + BendAuth.GROUP_ID,
          headers: {'Authorization': authString}
        }).then(function (ret) {
          var adminList = ret.users.list

          var adminUser = _.find(adminList, function (o) {
            return o._id == userId
          })

          if (isAdmin) { // add
            if (adminUser) {
              // nothing do
              callback(null)
            } else {
              adminList.push(CommonUtil.makeBendRef(userId, 'user'))
            }
          } else { // remove
            if (adminUser) {
              var idx = adminList.indexOf(adminUser)
              adminList.splice(idx, 1)
            } else {
              // nothing do
              callback(null)
            }
          }

          $.ajax({
            url: BendAuth.URL + BendAuth.APP_KEY + '/' + BendAuth.GROUP_ID,
            headers: {'Authorization': authString},
            type: 'PUT',
            data: JSON.stringify(ret)
          }).then(function (ret) {
            console.log('update group', isAdmin, adminList, ret)
            callback(null)
          }, function (err) {
            console.log(err)
            callback(err)
          })
        }, function (err) {
          callback(err)
          console.log(err)
        })
      }

      BendService.getAdminList = function (callback) {
        var authString = 'Bend ' + BendAuth.getActiveUser()._bmd.authtoken
        $.ajax({
          url: BendAuth.URL + BendAuth.APP_KEY + '/' + BendAuth.GROUP_ID,
          headers: {'Authorization': authString}
        }).then(function (ret) {
          var adminList = ret.users.list
          callback(null, adminList)
        }, function (err) {
          callback(err)
          console.log(err)
        })
      }

      // category
      BendService.getCategoryEnabledList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.equalTo('enabled', true)
        query.ascending('name')

        $bend.DataStore.find('category', query).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getGlobalCategoryEnabledList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.equalTo('enabled', true)
        query.exists('community', false)
        query.ascending('name')

        $bend.DataStore.find('category', query).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }
      BendService.getCollectionEnabledList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.equalTo('enabled', true)
        query.ascending('name')

        if ($bend.getActiveUser().communityAdmin) {
          // query.and(new $bend.Query().equalTo('community._id', Bend.getActiveUser().community._id).or().exists('community', false))
          query.and(new $bend.Query().equalTo('community._id', Bend.getActiveUser().community._id))
        }

        $bend.DataStore.find('collection', query).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getCategoryList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('name')

        if ($bend.getActiveUser().communityAdmin) {
          // query.and(new $bend.Query().equalTo('community._id', Bend.getActiveUser().community._id).or().exists('community', false))
          query.and(new $bend.Query().equalTo('community._id', Bend.getActiveUser().community._id).or().exists('community', false))
        }

        $bend.DataStore.find('category', query, {
          relations: {
            coverImage: 'BendFile',
            image1: 'BendFile',
            image2: 'BendFile',
            image3: 'BendFile',
            image4: 'BendFile'
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getCategoryListWithImages = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.equalTo('enabled', true)
        query.ascending('name')

        $bend.DataStore.find('category', query, {
          relations: {
            image1: 'BendFile',
            image2: 'BendFile',
            image3: 'BendFile',
            image4: 'BendFile'
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getCollectionList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('name')

        if ($bend.getActiveUser().communityAdmin) {
          query.equalTo('community._id', Bend.getActiveUser().community._id)
        }

        $bend.DataStore.find('collection', query, {
          relations: {
            coverImage: 'BendFile',
            community: 'community'
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }
      BendService.getBusinessList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('name')

        $bend.DataStore.find('business', query).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getChallengeList = function (type, callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('name')
        if (type && type.value == '1') {
          query.lessThan('endsAt', CommonUtil.getToday() * 1000000)
        } else if (type && type.value == '2') {
          query.greaterThanOrEqualTo('endsAt', CommonUtil.getToday() * 1000000)
        }

        $bend.DataStore.find('challenge', query, {
          relations: {
            activity: ['action', 'business', 'event', 'volunteer_opportunity']
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getEventList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('name')

        $bend.DataStore.find('event', query).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getVolunteeringList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('name')

        $bend.DataStore.find('volunteer_opportunity', query).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getActionList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('name')

        $bend.DataStore.find('action', query).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getActivityList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('name')

        $bend.DataStore.find('activity', query, {
          relations: {
            community: 'community',
            user: 'user',
            'user.avatar': 'BendFile'
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getAnswerList = function (questionId, callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.equalTo('question._id', questionId)
        query.ascending('position')

        $bend.DataStore.find('pollQuestionAnswer', query, {
          relations: {
            question: 'pollQuestion'
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }
      BendService.getResponseList = function (questionId, callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.descending('_bmd.createdAt')
        query.equalTo('question._id', questionId)
        query.exists('community', true)

        if ($bend.getActiveUser().communityAdmin) {
          query.equalTo('community._id', $bend.getActiveUser().community._id)
        }

        $bend.DataStore.find('pollQuestionResponse', query, {
          relations: {
            user: 'user',
            answer: 'pollQuestionAnswer',
            question: 'pollQuestion'
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getPollQuestionList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.descending('_bmd.createdAt')

        $bend.DataStore.find('pollQuestion', query, {
          relations: {
            community: 'community'
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }
      BendService.getCommunityList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('name')

        $bend.DataStore.find('community', query, {
          relations: {
            logo: 'BendFile'
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getCommunity2 = function (id, callback) {
        $bend.DataStore.get('community', id, {
          relations: {
            logo: 'BendFile'
          }
        }).then(function (ret) {
          callback(null, ret)
        }, function (err) {
          callback(err, null)
        })
      }

      BendService.getCommunityEnabledList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.equalTo('enabled', true)
        query.ascending('name')

        $bend.DataStore.find('community', query).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getCertificationList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('name')

        $bend.DataStore.find('certification', query, {
          relations: {
            badge: 'BendFile'
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }
      BendService.getCategory = function (id, callback) {
        $bend.DataStore.get('category', id, {
          relations: {
            coverImage: 'BendFile',
            image1: 'BendFile',
            image2: 'BendFile',
            image3: 'BendFile',
            image4: 'BendFile',
            buttonImage: 'BendFile',
            buttonImageSelected: 'BendFile',
            icon: 'BendFile',
            iconSticker: 'BendFile',
            community: 'community'
          }
        }).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }
      BendService.getCollection = function (id, callback) {
        $bend.DataStore.get('collection', id).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getChallenge = function (id, callback) {
        $bend.DataStore.get('challenge', id, {
          relations: {
            activity: ['action', 'business', 'event', 'volunteer_opportunity'],
            coverImage: 'BendFile'
          }
        }).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getEvent = function (id, callback) {
        $bend.DataStore.get('event', id, {
          relations: {
            coverImage: 'BendFile'
          }
        }).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getBusiness = function (id, callback) {
        $bend.DataStore.get('business', id, {
          relations: {
            coverImage: 'BendFile'
          }
        }).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getVolunteering = function (id, callback) {
        $bend.DataStore.get('volunteer_opportunity', id, {
          relations: {
            coverImage: 'BendFile'
          }
        }).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getActivity = function (id, callback) {
        $bend.DataStore.get('activity', id, {
          relations: {
            activity: ['action', 'business', 'event', 'volunteer_opportunity'],
            community: 'community',
            user: 'user'
          }
        }).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getPollQuestionAnswer = function (id, callback) {
        $bend.DataStore.get('pollQuestionAnswer', id).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.pollQuestionAnswerCountUpdate = function (id, callback) {
        $bend.DataStore.get('pollQuestion', id).then(function (ret) {
          var query = new $bend.Query()
          query.equalTo('question._id', id)
          query.notEqualTo('deleted', true)
          $bend.DataStore.count('pollQuestionAnswer', query).then(function (cnt) {
            ret.answerCount = cnt
            $bend.DataStore.update('pollQuestion', ret).then(function (r) {
              if (callback) { callback(null, r) }
            }, function (e) {
              if (callback) { callback(e) }
            })
          })
          if (callback) {
            callback(ret)
          }
        }, function (err, ret) {
          if (callback) {
            callback(ret)
          }
        })
      }

      BendService.getPollQuestion = function (id, callback) {
        $bend.DataStore.get('pollQuestion', id, {
          relations: {
            community: 'community'
          }
        }).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getCommunity = function (id, callback) {
        $bend.DataStore.get('community', id).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getCertification = function (id, callback) {
        $bend.DataStore.get('certification', id).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }
      BendService.createCategory = function (data, callback) {
        $bend.DataStore.save('category', data).then(function (ret) {
          console.log(ret)
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }
      BendService.createCollection = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveDataForCommunityAdmin', {
            type: 'collection',
            data: data
          }).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.save('collection', data).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.createActivity = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveActivityForCommunityAdmin', data).then(function (ret) {
            callback(ret.result)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.save('activity', data).then(function (ret) {
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.updateActivity = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveActivityForCommunityAdmin', data).then(function (ret) {
            callback(ret.result)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.update('activity', data).then(function (ret) {
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.deleteActivity = function (id, callback) {
        $bend.execute('removeActivityByAdmin', {
          id: id
        }).then(function (ret) {
          var data = true
          callback(data)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.createPollQuestion = function (data, communityList, answerList, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          var newData = _.clone(data)
          newData.community = CommonUtil.makeBendRef($bend.getActiveUser().community._id, 'community')
          newData.communities = [$bend.getActiveUser().community._id] // for backward capability
          newData.answerCount = answerList.length

          $bend.execute('saveDataForCommunityAdmin', {
            type: 'pollQuestion',
            data: newData
          }).then(function (ret) {
            // save answers
            var answers = $.extend(true, {}, answerList)
            _.map(answers, function (o) {
              delete o.$$hashKey
              o.question = CommonUtil.makeBendRef(ret._id, 'pollQuestion')
            })

            async.map(answers, function (answer, _cb) {
              $bend.execute('saveDataOnlyForCommunityAdmin', {
                type: 'pollQuestionAnswer',
                data: answer
              }).then(function (ret) {
                _cb(null, ret)
              }, function (err) {
                _cb(err)
              })
            }, function (err, ret) {
              callback(err, ret)
            })
          }, function (err) {
            callback(err)
          })
        } else {
          async.map(communityList, function (community, cb) {
            var newData = _.clone(data)
            newData.community = CommonUtil.makeBendRef(community._id, 'community')
            newData.communities = [community._id] // for backward capability
            newData.answerCount = answerList.length

            $bend.DataStore.save('pollQuestion', newData).then(function (ret) {
              // save answers
              var answers = $.extend(true, {}, answerList)
              _.map(answers, function (o) {
                delete o.$$hashKey
                o.question = CommonUtil.makeBendRef(ret._id, 'pollQuestion')
              })

              async.map(answers, function (answer, _cb) {
                $bend.DataStore.save('pollQuestionAnswer', answer).then(function (ret) {
                  _cb(null, ret)
                }, function (err) {
                  _cb(err)
                })
              }, function (err, ret) {
                cb(err, ret)
              })
            }, function (err) {
              cb(err)
            })
          }, function (err, ret) {
            callback(err, ret)
          })
        }
      }

      BendService.createPollQuestionAnswer = function (data, callback) {
        $bend.DataStore.save('pollQuestionAnswer', data).then(function (ret) {
          console.log(ret)
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.createCommunity = function (data, callback) {
        $bend.DataStore.save('community', data).then(function (ret) {
          console.log(ret)
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.createCertification = function (data, callback) {
        $bend.DataStore.save('certification', data).then(function (ret) {
          console.log(ret)
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }
      BendService.createChallenge = function (data, communityList, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          var newData = _.clone(data)
          newData.communities = [$bend.getActiveUser().community._id] // for backward capability
          $bend.execute('saveDataForCommunityAdmin', {
            type: 'challenge',
            data: newData
          }).then(function (ret) {
            callback(null, ret)
          }, function (err) {
            callback(err)
          })
        } else {
          async.map(communityList, function (community, cb) {
            var newData = _.clone(data)
            newData.community = CommonUtil.makeBendRef(community._id, 'community')
            newData.communities = [community._id] // for backward capability
            $bend.DataStore.save('challenge', newData).then(function (ret) {
              cb(null, ret)
            }, function (err) {
              cb(err)
            })
          }, function (err, ret) {
            callback(err, ret)
          })
        }
      }

      BendService.createEvent = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveDataForCommunityAdmin', {
            type: 'event',
            data: data
          }).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.save('event', data).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.createVolunteering = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveDataForCommunityAdmin', {
            type: 'volunteer_opportunity',
            data: data
          }).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.save('volunteer_opportunity', data).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.updateCategory = function (data, callback) {
        $bend.DataStore.update('category', data).then(function (ret) {
          console.log(ret)
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }
      BendService.updateCollection = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveDataForCommunityAdmin', {
            type: 'collection',
            data: data
          }).then(function (ret) {
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.update('collection', data).then(function (ret) {
            // console.log(ret);
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }
      BendService.updateChallenge = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveDataForCommunityAdmin', {
            type: 'challenge',
            data: data
          }).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.update('challenge', data).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.updateEvent = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveDataForCommunityAdmin', {
            type: 'event',
            data: data
          }).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.update('event', data).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.updateVolunteering = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveDataForCommunityAdmin', {
            type: 'volunteer_opportunity',
            data: data
          }).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.update('volunteer_opportunity', data).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.updatePollQuestionAnswer = function (data, callback) {
        var pollData = _.clone(data)
        delete pollData.$$hashKey
        $bend.DataStore.update('pollQuestionAnswer', pollData).then(function (ret) {
          console.log(ret)
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.updatePollQuestion = function (data, answerList, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          var question = _.clone(data)
          question.answerCount = answerList.length
          var responseCount = 0
          _.map(answerList, function (o) {
            responseCount += (o.count || 0)
          })
          question.responseCount = responseCount

          $bend.execute('saveDataForCommunityAdmin', {
            type: 'pollQuestion',
            data: question
          }).then(function (ret) {
            // save answers
            var answers = $.extend(true, {}, answerList)
            _.map(answers, function (o) {
              delete o.$$hashKey
              if (responseCount > 0) { o.percentage = o.count * 100 / responseCount }
            })

            // get all original answers
            var query = new $bend.Query()
            query.equalTo('question._id', question._id)
            query.notEqualTo('deleted', true)
            $bend.DataStore.find('pollQuestionAnswer', query).then(function (oldAnswers) {
              var deletedAnswers = []
              _.map(oldAnswers, function (o) {
                var exist = _.find(answers, function (_o) {
                  return _o._id == o._id
                })

                if (!exist) {
                  // delete
                  o.deleted = true
                  deletedAnswers.push(o)
                }
              })

              async.parallel([
                function (cb) {
                  // save answer
                  async.map(answers, function (answer, _cb) {
                    $bend.execute('saveDataOnlyForCommunityAdmin', {
                      type: 'pollQuestionAnswer',
                      data: answer
                    }).then(function (ret) {
                      _cb(null, ret)
                    }, function (err) {
                      _cb(err)
                    })
                  }, function (err, ret) {
                    cb(err, ret)
                  })
                },
                function (cb) {
                  // delete answer
                  async.map(deletedAnswers, function (answer, _cb) {
                    $bend.execute('saveDataOnlyForCommunityAdmin', {
                      type: 'pollQuestionAnswer',
                      data: answer
                    }).then(function (ret) {
                      _cb(null, ret)
                    }, function (err) {
                      _cb(err)
                    })
                  }, function (err, ret) {
                    cb(err, ret)
                  })
                }
              ], function (err, ret) {
                callback(err, ret)
              })
            }, function (err) {
              callback(err)
            })
          }, function (err) {
            cb(err)
          })
        } else {
          question = _.clone(data)
          question.answerCount = answerList.length

          responseCount = 0
          _.map(answerList, function (o) {
            responseCount += (o.count || 0)
          })

          question.responseCount = responseCount
          $bend.DataStore.update('pollQuestion', question).then(function (ret) {
            var answers = $.extend(true, {}, answerList)
            _.map(answers, function (o) {
              delete o.$$hashKey
              if (responseCount > 0) { o.percentage = o.count * 100 / responseCount }
            })

            // get all original answers
            var query = new $bend.Query()
            query.equalTo('question._id', question._id)
            query.notEqualTo('deleted', true)
            $bend.DataStore.find('pollQuestionAnswer', query).then(function (oldAnswers) {
              var deletedAnswers = []
              _.map(oldAnswers, function (o) {
                var exist = _.find(answers, function (_o) {
                  return _o._id == o._id
                })

                if (!exist) {
                  // delete
                  o.deleted = true
                  deletedAnswers.push(o)
                }
              })

              async.parallel([
                function (cb) {
                  // save answer
                  async.map(answers, function (answer, _cb) {
                    $bend.DataStore.save('pollQuestionAnswer', answer).then(function (ret) {
                      _cb(null, ret)
                    }, function (err) {
                      _cb(err)
                    })
                  }, function (err, ret) {
                    cb(err, ret)
                  })
                },
                function (cb) {
                  // delete answer
                  async.map(deletedAnswers, function (answer, _cb) {
                    $bend.DataStore.save('pollQuestionAnswer', answer).then(function (ret) {
                      _cb(null, ret)
                    }, function (err) {
                      _cb(err)
                    })
                  }, function (err, ret) {
                    cb(err, ret)
                  })
                }
              ], function (err, ret) {
                callback(err, ret)
              })
            }, function (err) {
              callback(err)
            })
          }, function (err) {
            console.log(err)
          })
        }
      }
      BendService.updateCommunity = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveDataOnlyForCommunityAdmin', {
            type: 'community',
            data: data
          }).then(function (ret) {
            callback(null, ret)
          }, function (err) {
            callback(err)
          })
        } else {
          $bend.DataStore.update('community', data).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }
      BendService.updateCertification = function (data, callback) {
        $bend.DataStore.update('certification', data).then(function (ret) {
          console.log(ret)
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.deleteCategory = function (id, callback) {
        BendService.getCategory(id, function (ret) {
          ret.deleted = true
          BendService.updateCategory(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }
      BendService.deleteCollection = function (id, callback) {
        BendService.getCollection(id, function (ret) {
          ret.deleted = true
          BendService.updateCollection(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }
      BendService.deleteCommunity = function (id, callback) {
        BendService.getCommunity(id, function (ret) {
          ret.deleted = true
          BendService.updateCommunity(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }

      BendService.deletePollQuestionAnswer = function (id, callback) {
        BendService.getPollQuestionAnswer(id, function (ret) {
          ret.deleted = true
          BendService.updatePollQuestionAnswer(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }
      BendService.deletePollQuestion = function (id, callback) {
        BendService.getPollQuestion(id, function (ret) {
          ret.deleted = true
          BendService.updatePollQuestion(ret, [], function (ret) {
            var data = true
            callback(data)
          })
        })
      }

      BendService.deleteChallenge = function (id, callback) {
        BendService.getChallenge(id, function (ret) {
          ret.deleted = true
          BendService.updateChallenge(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }

      BendService.deleteEvent = function (id, callback) {
        BendService.getEvent(id, function (ret) {
          ret.deleted = true
          BendService.updateEvent(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }

      BendService.deleteVolunteering = function (id, callback) {
        BendService.getVolunteering(id, function (ret) {
          ret.deleted = true
          BendService.updateVolunteering(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }

      BendService.deleteService = function (id, callback) {
        BendService.getService(id, function (ret) {
          ret.deleted = true
          BendService.updateService(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }
      BendService.deleteCertification = function (id, callback) {
        BendService.getCertification(id, function (ret) {
          ret.deleted = true
          BendService.updateCertification(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }

      // push
      BendService.getPushList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('_bmd.createdAt')

        $bend.DataStore.find('push', query, {
          relations: {
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getCommunityPushList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.equalTo('group', $bend.getActiveUser().community._id)
        query.ascending('_bmd.createdAt')

        $bend.DataStore.find('push', query, {
          relations: {
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getPush = function (id, callback) {
        $bend.DataStore.get('push', id, {
          relations: {
          }
        }).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.createPush = function (data, callback) {
        var newData = _.clone(data)
        if (newData.params) {
          newData.params.map(function (o) {
            delete o.$$hashKey
          })
        }

        $bend.DataStore.save('push', newData).then(function (ret) {
          console.log(ret)
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.updatePush = function (notification, callback) {
        var newData = _.clone(notification)
        delete newData.$$hashKey
        if (newData.params) {
          newData.params.map(function (o) {
            delete o.$$hashKey
          })
        }

        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('savePushForCommunityAdmin', newData).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.update('push', newData).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.deletePush = function (id, callback) {
        BendService.getPush(id, function (ret) {
          ret.deleted = true
          BendService.updatePush(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }

      // pushTemplate
      BendService.getPushTemplateList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('name')

        $bend.DataStore.find('pushTemplate', query, {
          relations: {
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getPushTemplate = function (id, callback) {
        $bend.DataStore.get('pushTemplate', id, {
          relations: {
          }
        }).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.createPushTemplate = function (data, callback) {
        var newData = _.clone(data)
        if (newData.obj.params) {
          newData.obj.params.map(function (o) {
            delete o.$$hashKey
          })
        }

        $bend.DataStore.save('pushTemplate', newData).then(function (ret) {
          console.log(ret)
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.updatePushTemplate = function (notification, callback) {
        var newData = _.clone(notification)
        delete newData.$$hashKey
        if (newData.obj.params) {
          newData.obj.params.map(function (o) {
            delete o.$$hashKey
          })
        }

        $bend.DataStore.update('pushTemplate', newData).then(function (ret) {
          console.log(ret)
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.deletePushTemplate = function (id, callback) {
        BendService.getPushTemplate(id, function (ret) {
          ret.deleted = true
          BendService.updatePushTemplate(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }

      BendService.deleteBusiness = function (id, callback) {
        BendService.getBusiness(id, function (ret) {
          ret.deleted = true
          delete ret.certification
          BendService.updateBusiness(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }

      BendService.deleteAction = function (id, callback) {
        BendService.getAction(id, function (ret) {
          ret.deleted = true
          BendService.updateAction(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }

      BendService.updateAction = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveDataForCommunityAdmin', {
            type: 'action',
            data: data
          }).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.update('action', data).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.updateBusiness = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveDataForCommunityAdmin', {
            type: 'business',
            data: data
          }).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.update('business', data).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.createBusiness = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveDataForCommunityAdmin', {
            type: 'business',
            data: data
          }).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.save('business', data).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.createAction = function (data, callback) {
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveDataForCommunityAdmin', {
            type: 'action',
            data: data
          }).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        } else {
          $bend.DataStore.save('action', data).then(function (ret) {
            console.log(ret)
            callback(ret)
          }, function (err) {
            console.log(err)
          })
        }
      }

      BendService.getAction = function (id, callback) {
        $bend.DataStore.get('action', id).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      // sprint
      BendService.createSprint = function (data, callback) {
        $bend.DataStore.save('sprint', data).then(function (ret) {
          console.log(ret)
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getSprint = function (id, callback) {
        $bend.DataStore.get('sprint', id).then(function (ret) {
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.deleteSprint = function (id, callback) {
        BendService.getSprint(id, function (ret) {
          ret.deleted = true
          BendService.updateSprint(ret, function (ret) {
            var data = true
            callback(data)
          })
        })
      }

      BendService.updateSprint = function (data, callback) {
        $bend.DataStore.update('sprint', data).then(function (ret) {
          console.log(ret)
          callback(ret)
        }, function (err) {
          console.log(err)
        })
      }

      BendService.getSprintList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('_bmd.createdAt')

        $bend.DataStore.find('sprint', query, {
          relations: {
            'collection': 'collection'
          }
        }).then(function (rets) {
          callback(rets)
        }, function (err) {
          console.log(err)
        })
      }

      // dashboard
      BendService.getStatisticsData = function (callback) {
        // var currentDate = new Date()
        // var today = new Date(new Date().setHours(0, 0, 0, 0)).getTime() * 1000000
        var startDateOfWeek = CommonUtil.getDateOfWeek()[0].getTime() * 1000000
        // var startDateOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1).getTime() * 1000000
        var user = BendAuth.getActiveUser()
        var isCommunityAdmin = user.communityAdmin
        var communityId = user.community._id
        var data = {
          totalUsers: 0,
          totalActivities: 0,
          totalPoints: 0,
          thisWeekPoints: 0
        }
        async.parallel([
          function (_callback) {
            var query = new $bend.Query()
            query.notEqualTo('deleted', true)

            if (isCommunityAdmin) {
              query.equalTo('community._id', communityId)
            }

            $bend.User.count(query).then(function (count) {
              data.totalUsers = count
              _callback(null, null)
            }, function (err) {
              _callback(err, null)
            })
          },
          function (_callback) {
            var query = new $bend.Query()
            query.notEqualTo('deleted', true)
            if (isCommunityAdmin) {
              query.equalTo('community._id', communityId)
            }
            $bend.DataStore.count('activity', query).then(function (count) {
              data.totalActivities = count
              _callback(null, null)
            }, function (err) {
              _callback(err, null)
            })
          },
          function (_callback) {
            if (isCommunityAdmin) {
              $bend.DataStore.get('community', communityId).then(function (community) {
                data.totalPoints = community.points
                _callback(null, null)
              }, function (err) {
                _callback(err, null)
              })
            } else {
              var q = new $bend.Query()
              q.notEqualTo('deleted', true)
              q.exists('points', true)
              var aggregation = $bend.Group.sum('points')
              aggregation.query(q)
              // get all leaderboard first
              $bend.DataStore.group('community', aggregation).then(function (ret) {
                data.totalPoints = ret.length > 0 ? ret[0].result : 0
                _callback(null, null)
              }, function (err) {
                _callback(err, null)
              })
            }
          },
          function (_callback) {
            var q = new $bend.Query()
            q.notEqualTo('deleted', true)
            q.exists('points', true)
            q.greaterThan('_bmd.createdAt', startDateOfWeek)
            if (isCommunityAdmin) {
              q.equalTo('community._id', communityId)
            }
            var aggregation = $bend.Group.sum('points')
            aggregation.query(q)
            // get all leaderboard first
            $bend.DataStore.group('activity', aggregation).then(function (ret) {
              console.log('activity count', ret)
              data.thisWeekPoints = ret.length > 0 ? ret[0].result : 0
              _callback(null, null)
            }, function (err) {
              _callback(err, null)
            })
          }
        ], function (err, rets) {
          callback(err, data)
        })
      }

      // teams
      BendService.getTeam = function (id, callback) {
        $bend.DataStore.get('team', id, {
          relations: {
            community: 'community'
          }
        }).then(function (ret) {
          callback(null, ret)
        }, function (err) {
          callback(err)
        })
      }

      BendService.getCommunityTeamList = function (communityId, callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.equalTo('community._id', communityId)
        query.ascending('position')
        $bend.DataStore.find('team', query).then(function (rets) {
          callback(null, rets)
        }, function (err) {
          callback(err)
        })
      }

      BendService.getAllTeamList = function (callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.ascending('position')
        $bend.DataStore.find('team', query).then(function (rets) {
          callback(null, rets)
        }, function (err) {
          callback(err)
        })
      }

      BendService.getTeamUserCount = function (teamId, callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.contains('teams', [teamId])
        $bend.User.count(query).then(function (c) {
          callback(null, c)
        }, function (err) {
          callback(err)
        })
      }

      BendService.createTeam = function (data, callback) {
        data = _.clone(data)
        if (data.community) {
          data.community = CommonUtil.makeBendRef(data.community._id, 'community')
        }
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.equalTo('community._id', data.community._id)
        $bend.DataStore.count('team', query).then(function (count) {
          data.position = count + 1
          if ($bend.getActiveUser().communityAdmin) {
            $bend.execute('saveDataForCommunityAdmin', {
              type: 'team',
              data: data
            }).then(function (ret) {
              callback(null, ret)
            }, function (err) {
              callback(err)
            })
          } else {
            $bend.DataStore.save('team', data).then(function (ret) {
              callback(null, ret)
            }, function (err) {
              callback(err)
            })
          }
        })
      }

      BendService.updateTeam = function (data, callback) {
        data = _.clone(data)
        delete data.$$hashKey
        delete data.userCount
        if ($bend.getActiveUser().communityAdmin) {
          $bend.execute('saveDataForCommunityAdmin', {
            type: 'team',
            data: data
          }).then(function (ret) {
            callback(null, ret)
          }, function (err) {
            callback(err)
          })
        } else {
          if (data.community) {
            data.community = CommonUtil.makeBendRef(data.community._id, 'community')
          }
          $bend.DataStore.update('team', data).then(function (ret) {
            callback(null, ret)
          }, function (err) {
            callback(err)
          })
        }
      }

      BendService.deleteTeam = function (id, callback) {
        BendService.getTeam(id, function (err, ret) {
          if (err) {
            callback(err)
            return
          }
          ret.deleted = true
          BendService.updateTeam(ret, function (err, ret) {
            callback(err, ret)
          })
        })
      }

      BendService.getCommunityAppConfig = function (communityId, callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.equalTo('community._id', communityId)
        $bend.DataStore.find('clientAppConfig', query, {
          relations: {
            welcomeBackground1: 'BendFile',
            welcomeBackground2: 'BendFile',
            welcomeBackground3: 'BendFile',
            welcomeBackground4: 'BendFile',
            welcomeBackground5: 'BendFile',
            welcomeBackground6: 'BendFile',
            iosAppIcon: 'BendFile',
            iosScreenshot1: 'BendFile',
            iosScreenshot2: 'BendFile',
            iosScreenshot3: 'BendFile',
            iosScreenshot4: 'BendFile',
            iosScreenshot5: 'BendFile',
            androidAppIcon: 'BendFile',
            androidScreenshot1: 'BendFile',
            androidScreenshot2: 'BendFile',
            androidScreenshot3: 'BendFile',
            androidScreenshot4: 'BendFile',
            androidScreenshot5: 'BendFile',
            androidCoverImage: 'BendFile'
          }
        }).then(function (rets) {
          if (rets.length > 0) {
            callback(null, rets[0])
          } else { callback(null, null) }
        }, function (err) {
          callback(err)
        })
      }

      BendService.saveCommunityAppConfig = function (data, callback) {
        $bend.DataStore.save('clientAppConfig', data).then(function (ret) {
          callback(null, ret)
        }, function (err) {
          callback(err)
        })
      }

      BendService.getLeaderboard = function (communityId, sprint, callback) {
        var q = new $bend.Query()
        if (communityId) { q.equalTo('community._id', communityId) }
        q.equalTo('enabled', true)
        q.notEqualTo('deleted', true)
        if (sprint) {
          var startTime = new Date(sprint.startDate).getTime() * 1000000
          var endTime = new Date(sprint.endDate + ' 23:59:59').getTime() * 1000000
          q.greaterThanOrEqualTo('_bmd.createdAt', startTime)
          q.lessThanOrEqualTo('_bmd.createdAt', endTime)
        }
        var aggregation = $bend.Group.sum('points')
        aggregation.by('user._id')
        aggregation.query(q)
        $bend.DataStore.group('activity', aggregation).then(function (sumVal) {
          console.log('getLeaderboard', sumVal)
          callback(null, [])
        }, function (err) {
          callback(err)
        })
      }
      BendService.getTaskList = function (communityId, callback) {
        var query = new $bend.Query()
        query.equalTo('community._id', communityId)
        query.notEqualTo('deleted', true)
        $bend.DataStore.find('task', query).then(function (rets) {
          callback(null, rets)
        }, function (err) {
          callback(err)
        })
      }

      BendService.getCommunityPushConfig = function (communityId, callback) {
        var query = new $bend.Query()
        query.notEqualTo('deleted', true)
        query.equalTo('community._id', communityId)
        $bend.DataStore.find('pushConfig', query, {
          relations: {
            appleCertificate: 'BendFile',
            community: 'community'
          }
        }).then(function (rets) {
          if (rets.length > 0) {
            callback(null, rets[0])
          } else { callback(null, null) }
        }, function (err) {
          callback(err)
        })
      }

      BendService.saveCommunityPushConfig = function (data, callback) {
        $bend.DataStore.save('pushConfig', data).then(function (ret) {
          callback(null, ret)
        }, function (err) {
          callback(err)
        })
      }

      BendService.getUserAddresses = function (userId, callback) {
        var query = new $bend.Query()
        query.equalTo('user._id', userId)

        $bend.DataStore.find('userAddress', query).then(function (rets) {
          callback(null, rets)
        }, function (err) {
          callback(err)
        })
      }

      BendService.saveUserAddress = function (data, callback) {
        $bend.DataStore.save('userAddress', data).then(function (ret) {
          callback(null, ret)
        }, function (err) {
          callback(err)
        })
      }

      return BendService
    }
  ])
