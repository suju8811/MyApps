//
//  HomeViewController.h
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/1/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>


@interface HomeViewController : UIViewController
{
    IBOutlet UIView *centerView;
    
    IBOutlet UIView *circleView;
    
    IBOutlet UILabel *lblPlanName;
    IBOutlet UILabel *lblTimeLimitStart;
    IBOutlet UILabel *lblTimeLimitEnd;
}
@end
