//
//  AppDelegate.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 3/31/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "MMDrawerController.h"
#import "DrawerViewController.h"
#import "MMDrawerVisualState.h"
#import "Preference.h"
#import "Constants.h"
#import "Color.h"
#import "PayPalMobile.h"
#import "Branch.h"

@interface AppDelegate ()
@property (nonatomic,strong) MMDrawerController * drawerController;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

#warning "Enter your credentials"
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"AXMA07peHxmh630VIqAZpsQRW0yyRPw_V3hmbela0e1pqID_kem7kIl8SO5Wj5qO6RCgiF1UX-5-jOys",
                                                           PayPalEnvironmentSandbox : @"YOUR_CLIENT_ID_FOR_SANDBOX"}];
    
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        if (!error && params) {
            // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
            // params will be empty if no data found
            // ... insert custom logic here ...
            NSLog(@"params: %@", params.description);
            _refer_info = params;
        }
    }];
    
    [Color initConstants];
    BOOL isLogined = [[Preference getInstance] getSharedPreference:nil :IS_LOGINED WithBOOL:false];
    //isLogined = true;
    if(isLogined) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;
    
        // 1. Initialize window
        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        // 2. Get storyboard
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        // 3. Create vc
    
        UINavigationController *navViewController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    
        //MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([MainViewController class])];
        DrawerViewController *drawerViewController = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([DrawerViewController class])];
    
        [drawerViewController.view setFrame:CGRectMake(0, 0, 240, screenHeight)];
    
        self.drawerController = [[MMDrawerController alloc]
                             initWithCenterViewController:navViewController
                             leftDrawerViewController:drawerViewController];
    
        [self.drawerController setShowsShadow:NO];
        [self.drawerController setRestorationIdentifier:@"MMDrawer"];
        [self.drawerController setMaximumLeftDrawerWidth:240];
        [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
        [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
        //[self.drawerController setDrawerVisualStateBlock:[MMDrawerVisualState slideAndScaleVisualStateBlock]];
        // 4. Set as root
        self.window.rootViewController = self.drawerController;
        // 5. Call to show views
        [self.window makeKeyAndVisible];
    }
    return YES;
}

// Respond to URI scheme links
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    // pass the url to the handle deep link call
    [[Branch getInstance] handleDeepLink:url];
    
    // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
    return YES;
}

// Respond to Universal Links
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler {
    BOOL handledByBranch = [[Branch getInstance] continueUserActivity:userActivity];
    
    return handledByBranch;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
