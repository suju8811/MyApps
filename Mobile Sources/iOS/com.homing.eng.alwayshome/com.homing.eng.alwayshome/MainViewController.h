//
//  ViewController.h
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 3/31/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "PlanListViewController.h"
#import "ShareViewController.h"

@interface MainViewController : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate>
{
    HomeViewController *homeVC;
    PlanListViewController *planListVC;
    ShareViewController *shareVC;
    
    
    IBOutlet UIImageView *ivMenu;
    IBOutlet UILabel *lblTitle;
    
    IBOutlet UIImageView *ivHome;
    IBOutlet UIImageView *ivPlan;
    IBOutlet UIImageView *ivShare;
    
    
    IBOutlet UIButton *btnHome;
    IBOutlet UIButton *btnPlan;
    IBOutlet UIButton *btnShare;
    
}

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end

