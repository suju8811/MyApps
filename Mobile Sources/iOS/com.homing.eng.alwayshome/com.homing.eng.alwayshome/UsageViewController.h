//
//  UsageViewController.h
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/2/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsageViewController : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate>
{
    
    IBOutlet UIButton *btnDaily;
    IBOutlet UIButton *btnWeekly;
    IBOutlet UIButton *btnMonthly;
    
    IBOutlet UILabel *lblDaily;
    IBOutlet UILabel *lblWeekly;
    IBOutlet UILabel *lblMonthly;    
    IBOutlet UIView *indicator;
    IBOutlet UIView *vSwipeView;
}

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end
