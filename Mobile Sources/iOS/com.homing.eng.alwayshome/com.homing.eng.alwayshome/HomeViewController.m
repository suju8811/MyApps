//
//  HomeViewController.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/1/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "HomeViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Preference.h"
#import "Constants.h"
#import "Utility.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   /* NSLog(@"%f %f",circleView.frame.size.width, circleView.frame.size.height);
    circleView.bounds = CGRectMake(([[UIScreen mainScreen] bounds].size.width-centerView.bounds.size.height)/2, 0, centerView.bounds.size.height, centerView.bounds.size.height);
    circleView.layer.cornerRadius = circleView.frame.size.width/2;
    circleView.clipsToBounds = true;
    */

}

-(void)viewDidAppear:(BOOL)animated
{
    NSString *plan_name = [[Preference getInstance] getSharedPreference:nil :PREF_USERDETAIL_PLAN_NAME :@""];
    
    if(plan_name.length == 0)
    {
        lblPlanName.text = @"You Haven't get a Plan yet.";
        lblTimeLimitStart.text = @"";
        lblTimeLimitEnd.text = @"";
    } else {
        lblPlanName.text = [NSString stringWithFormat:@"Current Plan:%@", plan_name];
        
        lblTimeLimitStart.text = [NSString stringWithFormat:@"Start: %@", [[Utility getInstance] getDateTime:[[Preference getInstance] getSharedPreference:nil :PREF_USERDETAIL_START_TIME :@""]]];
        lblTimeLimitEnd.text = [NSString stringWithFormat:@"End: %@", [[Utility getInstance] getDateTime:[[Preference getInstance] getSharedPreference:nil :PREF_USERDETAIL_EXPIRE_TIME :@""]]];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
