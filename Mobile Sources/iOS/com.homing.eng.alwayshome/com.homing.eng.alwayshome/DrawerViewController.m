//
//  DrawerViewController.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/1/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "DrawerViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "Utility.h"

@interface DrawerViewController ()

@end

@implementation DrawerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickLogout:(id)sender {
    [[Utility getInstance] LogOut:self];
}

- (IBAction)onClickUsage:(id)sender {
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
}

- (IBAction)onClickReferrals:(id)sender {
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
}

- (IBAction)onClickSetting:(id)sender {
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
}

- (IBAction)onClickHelp:(id)sender {
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
