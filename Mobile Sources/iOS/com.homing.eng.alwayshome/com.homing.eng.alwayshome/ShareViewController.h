//
//  ShareViewController.h
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/1/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface ShareViewController : UIViewController<SKProductsRequestDelegate, SKPaymentTransactionObserver>

@end
