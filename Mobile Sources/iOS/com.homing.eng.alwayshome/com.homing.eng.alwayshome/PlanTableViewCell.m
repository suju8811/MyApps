//
//  PlanTableViewCell.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/3/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "PlanTableViewCell.h"
#import "AppDelegate.h"
#import "PaySelectionViewController.h"
#import "PaymentConfirmViewController.h"
#import "Utility.h"
#import "Preference.h"
#import "Constants.h"
#import "Branch.h"
#import "AppDelegate.h"

#import "ServerAPIPath.h"
#import "ServerAPICall.h"

@implementation PlanTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)bookPlan
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:_detail.strId forKey:@"plan"];
    [params setObject:@"0" forKey:@"pid"];
    
    [ServerAPICall callPOST:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_POST_BOOK_PLAN]
                    success: ^(ServerResultRef s) {
                        @try {
                            Branch *branch = [Branch getInstance];
                            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
                            NSMutableDictionary *refer_info = appDelegate.refer_info;
                            [refer_info setObject:[[Preference getInstance] getSharedPreference:nil :PREF_USERDETAIL_USER_NAME :@""] forKey:@"user_email"];
                            [branch userCompletedAction:@"refer_purchase" withState:refer_info];
                        } @catch (NSException *exception) {
                            
                        }
                        
                        [[Utility getInstance] showToastMessage:@"Purchase success."];
                        [[Preference getInstance] putSharedPreference:nil :PREF_HAS_TRY_FREE_PLAN WithBOOL:true];
                    }failure:^(ServerErrorResult *err) {
                        [[Utility getInstance] showToastMessage:err];
                    }
                     params:params];
}

-(void)showPaymentConfirmDialog
{
    PaymentConfirmViewController *vc = [[PaymentConfirmViewController alloc] initWithNibName:@"PaymentConfirmViewController" bundle:nil];
    if ([UIDevice currentDevice].systemVersion.integerValue > 7)
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    else
        [_parentVC setModalPresentationStyle:UIModalPresentationCurrentContext];
    [_parentVC presentViewController:vc animated:NO completion:nil];
}

- (IBAction)onClickBuy:(id)sender {
    if([_detail.price isEqualToString:@"0"]) {
        if(![[Preference getInstance] getSharedPreference:nil :PREF_HAS_TRY_FREE_PLAN WithBOOL:false])
        {
            [self bookPlan];
        } else
           [[Utility getInstance] showToastMessage:@"you have already tried this free plan."];
    } else {
        if(![[Preference getInstance] getSharedPreference:nil :PREF_LAST_PAYMENT WithBOOL:false])
        {
            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PaySelectionViewController *destViewController = [storyboard instantiateViewControllerWithIdentifier:@"PaySelectionViewController"];
            destViewController.detail = self.detail;
            [[appDelegate.window rootViewController] presentViewController:destViewController animated:YES completion:nil];
        } else {
            [self showPaymentConfirmDialog];
        }
    }
}


@end
