//
//  PaymentConfirmViewController.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/7/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "PaymentConfirmViewController.h"
#import "Preference.h"
#import "Constants.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "Branch.h"

#import "ServerAPIPath.h"
#import "ServerAPICall.h"

@interface PaymentConfirmViewController ()

@end

@implementation PaymentConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _lblPayment.text = [NSString stringWithFormat:@"name:%@     price:%@",[[Preference getInstance] getSharedPreference:nil :PREF_LAST_PAYMENT_NAME :@""],[[Preference getInstance] getSharedPreference:nil :PREF_LAST_PAYMENT_PRICE :@""]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickConfirm:(id)sender {
    //[[Utility getInstance] getUserInfo];
    //[self dismissViewControllerAnimated:NO completion:nil];
    
    [Utility showProgressDialog:self];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[[Preference getInstance] getSharedPreference:nil :PREF_LAST_PAYMENT_ID :@""] forKey:@"plan"];
    [params setObject:[[Preference getInstance] getSharedPreference:nil :PREF_LAST_PAYMENT_PID :@""] forKey:@"pid"];
    NSLog(@"plan:%@ pid:%@",[[Preference getInstance] getSharedPreference:nil :PREF_LAST_PAYMENT_ID :@""], [[Preference getInstance] getSharedPreference:nil :PREF_LAST_PAYMENT_PID :@""]);
    
    [ServerAPICall callPOST:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_POST_BOOK_PLAN]
                    success: ^(ServerResultRef s) {
                        @try {
                            Branch *branch = [Branch getInstance];
                            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
                            NSMutableDictionary *refer_info = appDelegate.refer_info;
                            [refer_info setObject:[[Preference getInstance] getSharedPreference:nil :PREF_USERDETAIL_USER_NAME :@""] forKey:@"user_email"];
                            [branch userCompletedAction:@"refer_purchase" withState:refer_info];
                        } @catch (NSException *exception) {
                            
                        }
                        
                        [[Utility getInstance] showToastMessage:@"Purchase success."];
                        [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT WithBOOL:false];
                        [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT_ID :@""];
                        [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT_PID :@""];
                        [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT_NAME :@""];
                        [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT_PRICE :@""];
                        
                        [[Utility getInstance] getUserInfo];
                        [Utility hideProgressDialog];
                        [self dismissViewControllerAnimated:NO completion:nil];
                        
                    }failure:^(ServerErrorResult *err) {
                        [Utility hideProgressDialog];
                        [[Utility getInstance] showToastMessage:err];
                        if([[NSString stringWithFormat:@"%@",err] isEqualToString:@"invalid payment"]) {
                            [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT WithBOOL:false];
                            [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT_ID :@""];
                            [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT_PID :@""];
                            [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT_NAME :@""];
                            [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT_PRICE :@""];
                            [self dismissViewControllerAnimated:NO completion:nil];
                        }
                    }
                     params:params];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
