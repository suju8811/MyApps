//
//  PaySelectionViewController.h
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/2/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlanDetail.h"
#import "PayPalMobile.h"

@interface PaySelectionViewController : UIViewController<PayPalPaymentDelegate>
{
    IBOutlet UILabel *lblPlanType;
    IBOutlet UILabel *lblMonthFlow;
    IBOutlet UILabel *lblDeviceNum;
    
    IBOutlet UILabel *lblPrice;
    
    
}
@property PlanDetail *detail;
@end
