//
//  ChartViewController.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/2/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "ChartViewController.h"
#import "Utility.h"
#import "Preference.h"
#import "Constants.h"

#import "ServerAPIPath.h"
#import "ServerAPICall.h"

@interface ChartViewController ()
{
    NSString *resolution;
    NSString *start,*end;
}
@end

@implementation ChartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //ivChart.hidden = YES;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    if(self.type == 0) {
        [ivChart setImage:[UIImage imageNamed:@"daily.png"]];
        start = [NSString stringWithFormat:@"%@ 00:00:00",[dateFormatter stringFromDate:[NSDate date]]];
        end = [NSString stringWithFormat:@"%@ 23:59:59",[dateFormatter stringFromDate:[NSDate date]]];
        resolution = @"2";
    } else if(self.type == 1) {
        [ivChart setImage:[UIImage imageNamed:@"weekly.png"]];
        NSCalendar *cal = [NSCalendar currentCalendar];
        [cal setFirstWeekday:1];    //2 is monday. 1:Sunday .. 7:Saturday don't set it, if user's locale should determine the start of a week
        NSDate *now = [NSDate date];
        NSDate *sunday;
        [cal rangeOfUnit:NSWeekCalendarUnit  // we want to have the start of the week
               startDate:&sunday             // we will write the date object to monday
                interval:NULL                // we don't care for the seconds a week has
                 forDate:now];               // we want the monday of today's week
        start = [NSString stringWithFormat:@"%@ 00:00:00",[dateFormatter stringFromDate:sunday]];
        
        NSDate *saturday;
        [cal setFirstWeekday:7];
        [cal rangeOfUnit:NSWeekCalendarUnit  // we want to have the start of the week
               startDate:&saturday             // we will write the date object to monday
                interval:NULL                // we don't care for the seconds a week has
                 forDate:now];               // we want the monday of today's week
        
        end = [NSString stringWithFormat:@"%@ 23:59:59",[dateFormatter stringFromDate:saturday]];
        resolution = @"3";
    } else if(self.type == 2) {
        [ivChart setImage:[UIImage imageNamed:@"monthly.png"]];
        NSDate *curDate = [NSDate date];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* comps = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:curDate]; // Get necessary date components
        
        [comps setDay:1];
        NSDate *firstDateMonth = [calendar dateFromComponents:comps];
        start = [NSString stringWithFormat:@"%@ 00:00:00",[dateFormatter stringFromDate:firstDateMonth]];
        // set last of month
        [comps setMonth:[comps month]+1];
        [comps setDay:0];
        NSDate *tDateMonth = [calendar dateFromComponents:comps];
        end = [NSString stringWithFormat:@"%@ 23:59:59",[dateFormatter stringFromDate:tDateMonth]];
        
        resolution = @"3";
    }
    
    
    NSLog(@"start %@ end %@",start,end);
    [self getUsageData];
}

-(void)getUsageData
{
    
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:@"aa" forKey:@"device_name"];
    [params setObject:start forKey:@"start"];
    [params setObject:end forKey:@"end"];
    [params setObject:resolution forKey:@"resolution"];
    
    [ServerAPICall callPOST:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_POST_GET_USAGE_DATA]
                    success: ^(ServerResultRef s) {
                        if(s == nil) {
                            return;
                        }
                        
                    }failure:^(ServerErrorResult *err) {
                        [[Utility getInstance] showToastMessage:err];
                        [Utility hideProgressDialog];
                    }
                     params:params];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
