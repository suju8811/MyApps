//
//  LoginViewController.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/1/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "LoginViewController.h"
#import "Utility.h"
#import "Preference.h"
#import "Constants.h"

#import "ServerAPIPath.h"
#import "ServerAPICall.h"


@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)memberLogin
{
    [Utility showProgressDialog:self];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:tfEmail.text forKey:@"username"];
    [params setObject:tfPassword.text forKey:@"password"];
    
    [ServerAPICall callPOST:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_POST_LOGIN]
                    success: ^(ServerResultRef s) {
                        if(s == nil) {
                            [Utility hideProgressDialog];
                            return;
                        }
                        NSDictionary* jsonObject = (NSDictionary*)s;
                        Preference* pref = [Preference getInstance];
                        [pref putSharedPreference:nil :IS_LOGINED WithBOOL:true];
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_COOKIE_NAME :[jsonObject objectForKey:@"cookie_name"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_DISPLAY_NAME :[jsonObject objectForKey:@"name"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_REMARK :[jsonObject objectForKey:@"remark"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_MONTH_FLOW_BALANCE :[jsonObject objectForKey:@"month_flow_balance"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_EXTRA_FLOW_BALANCE :[jsonObject objectForKey:@"extra_flow_balance"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_START_TIME :[jsonObject objectForKey:@"start_time"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_EXPIRE_TIME :[jsonObject objectForKey:@"expire_time"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_DEVICE_NUM :[jsonObject objectForKey:@"device_num"]];} @catch (NSException *exception) {}
                        
                        NSDictionary* userObject = [jsonObject objectForKey:@"user"];
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_USER_ID :[userObject objectForKey:@"id"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_USER_NAME :[userObject objectForKey:@"username"]];} @catch (NSException *exception) {}
                        
                        NSDictionary* planObject = [jsonObject objectForKey:@"plan"];
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_ID :[planObject objectForKey:@"id"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_NAME :[planObject objectForKey:@"name"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_REMARK :[planObject objectForKey:@"remark"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_MONTH_FLOW :[planObject objectForKey:@"month_flow"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_EXTRA_FLOW :[planObject objectForKey:@"extra_flow"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_SPEED_LIMIT :[planObject objectForKey:@"speed_limit"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_DEVICE_NUMBER_LIMIT :[planObject objectForKey:@"device_number_limit"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_DURATION :[planObject objectForKey:@"duration"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_PRICE :[planObject objectForKey:@"price"]];} @catch (NSException *exception) {}
                        @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_PID :[planObject objectForKey:@"pid"]];} @catch (NSException *exception) {}
                        
                        [pref putSharedPreference:nil :PREF_HAS_TRY_FREE_PLAN WithBOOL:false];
                        [pref putSharedPreference:nil :PREF_LAST_PAYMENT WithBOOL:false];
                        [pref putSharedPreference:nil :PREF_LAST_PAYMENT_ID :@""];
                        [pref putSharedPreference:nil :PREF_LAST_PAYMENT_NAME :@""];
                        [pref putSharedPreference:nil :PREF_LAST_PAYMENT_PRICE :@""];
                        [pref putSharedPreference:nil :PREF_LAST_PAYMENT_PID :@""];
                        
                        [self getShareUrl];
                    }
                    failure:^(ServerErrorResult *err) {
                        [[Utility getInstance] showToastMessage:err];
                        [Utility hideProgressDialog];
                    }
                     params:params];
}

-(void)getShareUrl
{
    [ServerAPICall callGET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_GET_SHARE_URL]
                   success: ^(ServerResultRef s) {
                       [Utility hideProgressDialog];
                       @try {
                           NSDictionary* jsonObject = (NSDictionary*)s;
                           @try {
                               [[Preference getInstance] putSharedPreference:nil :PREF_SHARE_URL :[jsonObject objectForKey:@"data"]];
                           } @catch (NSException *exception) {
                           }
                       } @catch (NSException *exception) {
                           NSLog(@"exception:%@",exception.reason);
                       }
                       [[Utility getInstance] gotoScreen:self :@"MainViewController"];
                   }
                   failure:^(ServerErrorResult *err) {
                       //[[Utility getInstance] showToastMessage:err];
                       [Utility hideProgressDialog];
                       [[Utility getInstance] gotoScreen:self :@"MainViewController"];
                   }];
}

- (IBAction)onClickLogin:(id)sender {
    if(![Utility IsValidEmail:tfEmail.text] ) {
        [[Utility getInstance] showToastMessage:@"Please input correct email."];
        return;
    }
    if([tfPassword.text isEqualToString:@""]) {
        [[Utility getInstance] showToastMessage:@"Please input your password."];
        return;
    }
    [self memberLogin];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark UITouch Methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [[Utility getInstance] cmdHideKeyboard:txtfControl];
    
}

#pragma mark -
#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    txtfControl = textField;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [[Utility getInstance] cmdHideKeyboard:textField];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *str = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    
    
    return true;
}

@end
