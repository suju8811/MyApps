//
//  PaymentConfirmViewController.h
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/7/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentConfirmViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblPayment;

@end
