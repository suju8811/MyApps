//
//  ViewController.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 3/31/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "MainViewController.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerVisualState.h"
#import "MMDrawerController.h"


@interface MainViewController ()
{

}
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    planListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlanListViewController"];
    shareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareViewController"];
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    //self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    UIViewController *homeViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[homeViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    self.pageViewController.view.frame = CGRectMake(5, 70, self.view.frame.size.width-5, self.view.frame.size.height-120);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    

    
    [self setBottomTabBar:0];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSInteger index = 0;
    
    if(viewController == homeVC)
    {
        index = 0;
    }
    if(viewController == planListVC)
    {
        index = 1;
    }
    if(viewController == shareVC)
    {
        index = 2;
    }
    
    if (index == 0) {
        
        return nil;
    }
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSInteger index = 0;
    if(viewController == homeVC)
    {
        index = 0;
    }
    if(viewController == planListVC)
    {
        index = 1;
    }
    if(viewController == shareVC)
    {
        index = 2;
    }
    
    index++;
    
    if (index == 3) {
        return nil;
    }
    

    return [self viewControllerAtIndex:index];
}

-(UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    UIViewController *temp;
    
    switch (index) {
        case 0:
            temp = homeVC;
            break;
        case 1:
            temp = planListVC;
            break;
        case 2:
            temp = shareVC;
            break;
    }
    
    return temp;
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return 3;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return -1;
}

- (void)pageViewController:(UIPageViewController *)viewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (!completed){return;}
    
    // Find index of current page
    UIViewController *currentViewController = [self.pageViewController.viewControllers lastObject];
    
    int current_index = 0;
    if(currentViewController == homeVC)
    {
        current_index = 0;
    }
    if(currentViewController == planListVC)
    {
        current_index = 1;
    }
    if(currentViewController == shareVC)
    {
        current_index = 2;
    }
    [self setBottomTabBar:current_index];
    
}

-(void)setBottomTabBar:(int)index{
    
    [ivHome setImage:[UIImage imageNamed:@"bottom_home_white.png"]];
    [ivPlan setImage:[UIImage imageNamed:@"bottom_plan_white.png"]];
    [ivShare setImage:[UIImage imageNamed:@"bottom_share_white.png"]];
    
    switch (index) {
        case 0:
            [ivHome setImage:[UIImage imageNamed:@"bottom_home_blue.png"]];
            [lblTitle setText:@"AlwaysHome mobile"];
            break;
        case 1:
            [ivPlan setImage:[UIImage imageNamed:@"bottom_plan_blue.png"]];
            [lblTitle setText:@"Pay Service"];
            break;
        case 2:
            [ivShare setImage:[UIImage imageNamed:@"bottom_share_blue.png"]];
            [lblTitle setText:@"Refer one Get two"];
            break;
        default:
            break;
    }
    
}

- (IBAction)bottomTabClick:(id)sender {
    int index = 0;
    if(sender == btnHome){
        index = 0;
    }else if(sender == btnPlan){
        index = 1;
    }else if(sender == btnShare){
        index = 2;
    }
    
    [self setBottomTabBar:index];
    
    
    UIViewController *viewController = [self viewControllerAtIndex:index];
    NSArray *viewControllers = @[viewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}


- (IBAction)onClickMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}


@end
