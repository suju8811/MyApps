//
//  PaySelectionViewController.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/2/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "PaySelectionViewController.h"
#import "PaymentConfirmViewController.h"
#import "Preference.h"
#import "Constants.h"
#import "Utility.h"

#define kPayPalEnvironment PayPalEnvironmentProduction

@interface PaySelectionViewController ()
@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;
@end

@implementation PaySelectionViewController

- (BOOL)acceptCreditCards {
    return self.payPalConfig.acceptCreditCards;
}

- (void)setAcceptCreditCards:(BOOL)acceptCreditCards {
    self.payPalConfig.acceptCreditCards = acceptCreditCards;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    lblPlanType.text = self.detail.name;
    lblDeviceNum.text = self.detail.device_number_limit;
    if([self.detail.month_flow intValue] == 0) {
        lblMonthFlow.text = @"Unlimited";
    } else {
        lblMonthFlow.text = self.detail.month_flow;
    }
    lblPrice.text = [NSString stringWithFormat:@"price: %@$",self.detail.price];
    
    [self setPayPalConfig];
}

-(void)setPayPalConfig{
    _payPalConfig.acceptCreditCards = YES;
    _payPalConfig.merchantName = @"Alwayshome";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    // Preconnect to PayPal early
    [self setPayPalEnvironment:kPayPalEnvironment];
}

- (void)setPayPalEnvironment:(NSString *)environment {
    [PayPalMobile preconnectWithEnvironment:environment];
}

- (void)pay {
    // Note: For purposes of illustration, this example shows a payment that includes
    //       both payment details (subtotal, shipping, tax) and multiple items.
    //       You would only specify these if appropriate to your situation.
    //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
    //       and simply set payment.amount to your total charge.
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = [[NSDecimalNumber alloc] initWithString:@"0.01"];
    payment.currencyCode = @"USD";
    payment.shortDescription = self.detail.name;
    payment.items = nil;  // if not including multiple items, then leave payment.items as nil
    payment.paymentDetails = nil; // if not including payment details, then leave payment.paymentDetails as nil
    
    if (!payment.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    // Update payPalConfig re accepting credit cards.
    self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    if(completedPayment.confirmation != nil) {
        @try {
            NSLog(@"PayPal Payment Success!");
            NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
            self.detail.pid = [[completedPayment.confirmation objectForKey:@"response"] objectForKey:@"id"];
            [self showPaymentConfirmDialog];
        } @catch (NSException *exception) {
            [[Utility getInstance] showToastMessage:[NSString stringWithFormat:@"an extermely unlikely failure occurred: %@",exception.reason]];
        }
    } else {
    }
    [paymentViewController dismissViewControllerAnimated:NO completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    [[Utility getInstance] showToastMessage:@"The user canceled"];
    [paymentViewController dismissViewControllerAnimated:NO completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)showPaymentConfirmDialog
{
    [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT WithBOOL:true];
    [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT_ID :self.detail.strId];
    [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT_PID :self.detail.pid];
    [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT_NAME :self.detail.name];
    [[Preference getInstance] putSharedPreference:nil :PREF_LAST_PAYMENT_PRICE :self.detail.price];
    
    PaymentConfirmViewController *vc = [[PaymentConfirmViewController alloc] initWithNibName:@"PaymentConfirmViewController" bundle:nil];
    if ([UIDevice currentDevice].systemVersion.integerValue > 7)
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    else
        [self setModalPresentationStyle:UIModalPresentationCurrentContext];
    [self presentViewController:vc animated:NO completion:nil];
}

- (IBAction)onClickPaypalPay:(id)sender {
    [self pay];
}


- (IBAction)onClickClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
