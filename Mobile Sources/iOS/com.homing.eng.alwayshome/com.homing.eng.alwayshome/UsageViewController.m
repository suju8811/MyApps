//
//  UsageViewController.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/2/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "UsageViewController.h"
#import "ChartViewController.h"
#import "Color.h"

@interface UsageViewController ()
{
    ChartViewController *dailyVC,*weeklyVC,*monthlyVC;
    
}
@end

@implementation UsageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dailyVC= [self.storyboard instantiateViewControllerWithIdentifier:@"ChartViewController"];
    dailyVC.type = 0;
    weeklyVC= [self.storyboard instantiateViewControllerWithIdentifier:@"ChartViewController"];
    weeklyVC.type = 1;
    monthlyVC= [self.storyboard instantiateViewControllerWithIdentifier:@"ChartViewController"];
    monthlyVC.type = 2;
    
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    UIViewController *dailyViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[dailyViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    
    self.pageViewController.view.frame = CGRectMake(0, 0, vSwipeView.frame.size.width, vSwipeView.frame.size.height);
    
    [self addChildViewController:_pageViewController];
    [vSwipeView addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    [self setTopTabBar:0];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSInteger index = ((ChartViewController*)viewController).type;
    
    if (index == 0) {
        
        return nil;
    }
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSInteger index = ((ChartViewController*)viewController).type;
    index++;
    
    if (index == 3) {
        return nil;
    }
    
    
    return [self viewControllerAtIndex:index];
}

-(UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
//    ChartViewController *temp = [self.storyboard instantiateViewControllerWithIdentifier:@"ChartViewController"];
//    temp.type = index;
    
    UIViewController *temp;
    
    switch (index) {
        case 0:
            temp = dailyVC;
            break;
        case 1:
            temp = weeklyVC;
            break;
        case 2:
            temp = monthlyVC;
            break;
    }
    
    return temp;
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return 3;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return -1;
}

- (void)pageViewController:(UIPageViewController *)viewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (!completed){return;}
    
    // Find index of current page
    UIViewController *currentViewController = [self.pageViewController.viewControllers lastObject];
    
    NSInteger current_index = ((ChartViewController*)currentViewController).type;
    
    [self setTopTabBar:current_index];
    
}

-(void)setTopTabBar:(int)index{
    
    [lblDaily setTextColor:dark_white];
    [lblWeekly setTextColor:dark_white];
    [lblMonthly setTextColor:dark_white];

    switch (index) {
        case 0:
            [lblDaily setTextColor:[UIColor whiteColor]];
            break;
        case 1:
            [lblWeekly setTextColor:[UIColor whiteColor]];
            break;
        case 2:
            [lblMonthly setTextColor:[UIColor whiteColor]];
            break;
        default:
            break;
    }
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    indicator.frame = CGRectMake(screenWidth/3*index, 37,screenWidth/3,3);
    
}

- (IBAction)topTabClick:(id)sender {
    int index = 0;
    if(sender == btnDaily){
        index = 0;
    }else if(sender == btnWeekly){
        index = 1;
    }else if(sender == btnMonthly){
        index = 2;
    }
    
    [self setTopTabBar:index];
    
    
    UIViewController *viewController = [self viewControllerAtIndex:index];
    NSArray *viewControllers = @[viewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}


- (IBAction)onClickClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
