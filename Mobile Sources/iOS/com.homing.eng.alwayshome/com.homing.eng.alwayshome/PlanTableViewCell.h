//
//  PlanTableViewCell.h
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/3/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlanDetail.h"

@interface PlanTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblRemark;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;

@property PlanDetail *detail;
@property UIViewController *parentVC;
@end
