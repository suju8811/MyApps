//
//  GoodListViewController.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/1/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "PlanListViewController.h"
#import "PlanTableViewCell.h"
#import "PlanDetail.h"
#import "Utility.h"
#import "Preference.h"
#import "Constants.h"

#import "ServerAPIPath.h"
#import "ServerAPICall.h"

@interface PlanListViewController ()
{
    NSMutableArray *arrPlan;
}
@end

@implementation PlanListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrPlan = [[NSMutableArray alloc] initWithCapacity:0];
    [self getPlanList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) responseList:(NSDictionary*) json {
    [arrPlan removeAllObjects];
    if([json isKindOfClass:[NSNull class]] == true || ([json isKindOfClass:[NSString class]] == true && ([(NSString*)json isEqualToString:@"<null>"] == true || [(NSString*)json isEqualToString:@"null"] == true)))
    {
        return;
    }
    
    NSArray *dataArray = (NSArray*)json;
    for(int i = 0; i < dataArray.count; i++) {
        NSDictionary *jsonObject = [dataArray objectAtIndex:(i)];
        PlanDetail *model = [[PlanDetail alloc] init:jsonObject];
        [arrPlan addObject:model];
    }
    
    [tvPlan reloadData];
}

-(void)getPlanList
{
    [Utility showProgressDialog:self];
    [ServerAPICall callGET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_GET_PLAN_LIST]
                   success: ^(ServerResultRef s) {
                       [Utility hideProgressDialog];
                       if(s == nil) {
                           return;
                       }
                       @try {
                           NSDictionary* jsonObject = (NSDictionary*)s;
                           @try {
                               [self responseList:[jsonObject objectForKey:@"data"]];
                           } @catch (NSException *exception) {
                           }
                       } @catch (NSException *exception) {
                           NSLog(@"exception:%@",exception.reason);
                       }
                   }
                   failure:^(ServerErrorResult *err) {
                       [[Utility getInstance] showToastMessage:err];
                       [Utility hideProgressDialog];
                   }];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlanTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"PlanTableViewCell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[PlanTableViewCell alloc] init];
    }
    
    PlanDetail *detail = [arrPlan objectAtIndex:indexPath.row];
    cell.detail = detail;
    cell.lblName.text = detail.name;
    cell.lblRemark.text = detail.remark;
    if([detail.price isEqualToString:@"0"])
        cell.lblPrice.text = @"0.0$";
    else {
        cell.lblPrice.text = [NSString stringWithFormat:@"%@$",detail.price];
    }
    cell.parentVC = self;
    return cell;
}

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section
{
    return arrPlan.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

@end
