//
//  PlanDetail.h
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/7/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlanDetail : NSObject
@property (nonatomic, retain) NSString *strId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *remark;
@property (nonatomic, retain) NSString *month_flow;
@property (nonatomic, retain) NSString *extra_flow;
@property (nonatomic, retain) NSString *speed_limit;
@property (nonatomic, retain) NSString *device_number_limit;
@property (nonatomic, retain) NSString *duration;
@property (nonatomic, retain) NSString *price;
@property (nonatomic, retain) NSString *pid;

-(id)init:(NSDictionary*)jsonObject;
-(void) initWithJsonObject:(NSDictionary*)jsonObject;
@end
