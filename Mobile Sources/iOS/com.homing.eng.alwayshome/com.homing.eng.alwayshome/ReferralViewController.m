//
//  ReferralViewController.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/2/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "ReferralViewController.h"
#import "Utility.h"
#import "Preference.h"
#import "Constants.h"

#import "ServerAPIPath.h"
#import "ServerAPICall.h"

@interface ReferralViewController ()

@end

@implementation ReferralViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getReferral];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getReferral
{
    [Utility showProgressDialog:self];
    [ServerAPICall callGET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_GET_REFERRALS]
                    success: ^(ServerResultRef s) {
                        [Utility hideProgressDialog];
                        if(s == nil) {
                            return;
                        }
                        @try {
                            NSDictionary* jsonObject = (NSDictionary*)s;
                            jsonObject = [jsonObject objectForKey:@"data"];
                            @try {
                                lblInstalls.text = [NSString stringWithFormat:@"%@", [jsonObject objectForKey:@"install"]];
                            } @catch (NSException *exception) {
                            }
                            @try {
                                lblRegistrations.text = [NSString stringWithFormat:@"%@", [jsonObject objectForKey:@"register"]];
                            } @catch (NSException *exception) {}
                            @try {
                                lblPurchases.text = [NSString stringWithFormat:@"%@", [jsonObject objectForKey:@"purchase"]];
                            } @catch (NSException *exception) {}

                        } @catch (NSException *exception) {
                            NSLog(@"exception:%@",exception.reason);
                        }
                        
                    }
                    failure:^(ServerErrorResult *err) {
                        [[Utility getInstance] showToastMessage:err];
                        [Utility hideProgressDialog];
                        //[[Utility getInstance] gotoScreen:self :@"MainViewController"];
                    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClickClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
