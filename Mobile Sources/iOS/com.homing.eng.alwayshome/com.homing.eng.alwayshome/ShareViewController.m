//
//  ShareViewController.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/1/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "ShareViewController.h"
#import "Utility.h"
#import "Preference.h"
#import "Constants.h"

#import "ServerAPIPath.h"
#import "ServerAPICall.h"

@interface ShareViewController ()
{
    NSString *product_identifier;
}
@end

@implementation ShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    product_identifier = @"product_identifier";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





-(void)startShare
{

    NSString *texttoshare=[NSString stringWithFormat:@"Alwayshoming Mobile is a reliable and fast space-shifting solution.\nI used it and it works perfcetly.\nFollow this link to sign up and enjoy an extra Free month service when you buy! \nthis link:%@",[[Preference getInstance] getSharedPreference:nil :PREF_SHARE_URL :@""]]; //this is your text string to share
    
    //    NSString *texttoshare = [NSString stringWithFormat:@"%@\n%@",MSG_SHARE, APP_PAGE_URL];
    NSURL *appLink = [NSURL URLWithString:[[Preference getInstance] getSharedPreference:nil :PREF_SHARE_URL :@""]];
    NSArray *activityItems = @[texttoshare,appLink];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityVC animated:TRUE completion:nil];
}

- (IBAction)onClickShare:(id)sender {
    if([[Preference getInstance] getSharedPreference:nil :PREF_SHARE_URL :@""].length == 0) {
        [self getShareUrl];
    } else {
        [self startShare];
    }
}

-(void)getShareUrl
{
    [Utility showProgressDialog:self];
    [ServerAPICall callGET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_GET_SHARE_URL]
                   success: ^(ServerResultRef s) {
                       [Utility hideProgressDialog];
                       if(s == nil) {
                           return;
                       }
                       @try {
                           NSDictionary* jsonObject = (NSDictionary*)s;
                           @try {
                               [[Preference getInstance] putSharedPreference:nil :PREF_SHARE_URL :[jsonObject objectForKey:@"data"]];
                               [self startShare];
                           } @catch (NSException *exception) {
                           }
                       } @catch (NSException *exception) {
                           NSLog(@"exception:%@",exception.reason);
                       }
                   }
                   failure:^(ServerErrorResult *err) {
                       [[Utility getInstance] showToastMessage:err];
                       [Utility hideProgressDialog];
                   }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)restoreCompletedTransactions
{
    [Utility  showProgressDialog:self];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    [Utility hideProgressDialog];
    /*NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    BOOL bIsReleasePack = [preferences boolForKey:@"mega_effect_pack"];
    if(bIsReleasePack) {
        _m_btnMetaPack.hidden = YES;
    }
    [preferences setBool:YES forKey:@"purchase_restored"];
    _m_btnRestore.enabled = NO;
     */
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                      message:@"Restore successful!"
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    [Utility hideProgressDialog];
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                      message:@"Restore Error!"
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
    
}




-(void)purchase
{
    if ([SKPaymentQueue canMakePayments]) {
        NSSet *productIdentifiers = [NSSet setWithObjects:product_identifier, nil];
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
        productsRequest.delegate = self;
        [productsRequest start];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Prohibited"
                                  message:@"Parental Control is enabled, cannot make a purchase!"
                                  delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
    }
    
}

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    
    // remove wait view here
    SKProduct *validProduct = nil;
    int count = (int)[response.products count];
    
    if (count>0) {
        validProduct = [response.products objectAtIndex:0];
        
        if ([validProduct.productIdentifier isEqualToString:product_identifier]) {
            NSLog(@"Product Title: %@",validProduct.localizedTitle);
            NSLog(@"Product Desc: %@",validProduct.localizedDescription);
            NSLog(@"Product Price: %@",validProduct.price);
        }
        
        SKPayment *payment = [SKMutablePayment paymentWithProduct:validProduct];
        //SKPayment *payment = [SKPayment paymentWithProductIdentifier:MegaPackProductID];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        
        
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Not Available"
                                  message:@"No products to purchase"
                                  delegate:self
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier isEqualToString:product_identifier]) {
                    NSLog(@"Purchased ");
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Purchase is completed succesfully" message:nil delegate:
                                              self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [alertView show];
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [self finishPayment];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored ");
                if(transaction.originalTransaction.payment.productIdentifier)
                {
                    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
                    [preferences setBool:YES forKey:transaction.originalTransaction.payment.productIdentifier];
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [self finishPayment];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed:  %@", transaction.error);
                break;
            default:
                break;
        }
    }
}

-(void)finishPayment {
}
@end
