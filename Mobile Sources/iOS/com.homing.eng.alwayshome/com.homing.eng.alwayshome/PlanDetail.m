//
//  PlanDetail.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/7/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "PlanDetail.h"

@implementation PlanDetail

-(id)init:(NSDictionary*)jsonObject {
    if((self = [super init]) != nil ) {
        [self initWithJsonObject:jsonObject];
    }
    
    return self;
}

-(void) initWithJsonObject:(NSDictionary*)jsonObject {
    @try { self.strId = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"id"]];} @catch (NSException *exception) {}
    @try { self.name = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"name"]];} @catch (NSException *exception) {}
    @try { self.remark = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"remark"]];} @catch (NSException *exception) {}
    @try { self.month_flow = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"month_flow"]];} @catch (NSException *exception) {}
    @try { self.extra_flow = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"extra_flow"]];} @catch (NSException *exception) {}
    @try { self.speed_limit = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"speed_limit"]];} @catch (NSException *exception) {}
    @try { self.device_number_limit = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"device_number_limit"]];} @catch (NSException *exception) {}
    @try { self.duration = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"duration"]];} @catch (NSException *exception) {}
    @try { self.price = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"price"]];} @catch (NSException *exception) {}
    @try { self.pid = [NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"pid"]];} @catch (NSException *exception) {}
}
@end
