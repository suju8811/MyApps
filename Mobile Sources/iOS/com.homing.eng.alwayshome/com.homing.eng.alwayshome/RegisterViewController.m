//
//  RegisterViewController.m
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/1/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import "RegisterViewController.h"
#import "Utility.h"

#import "Preference.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "Branch.h"

#import "ServerAPIPath.h"
#import "ServerAPICall.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    confirmDialog.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)memberRegist
{
    [Utility showProgressDialog:self];
    
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *user = [[NSMutableDictionary alloc] init];
    [param setObject:@"n/a" forKey:@"remark"];
    [param setObject:tfDisplayname.text forKey:@"name"];
    [user setObject:tfEmail.text forKey:@"username"];
    [user setObject:tfPassword.text forKey:@"password"];
    [param setObject:user forKey:@"user"];

    
    [ServerAPICall callPOST:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_POST_REGIST]
                    success: ^(ServerResultRef s) {
                        [Utility hideProgressDialog];
                        if(s == nil) {
                            return;
                        }
                        [[Utility getInstance] showToastMessage:@"Register succeed."];
                        NSDictionary* jsonObject = (NSDictionary*)s;
                        Preference* pref = [Preference getInstance];
                        confirmDialog.hidden = NO;
                        
                        @try {
                            Branch *branch = [Branch getInstance];
                            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
                            NSMutableDictionary *refer_info = appDelegate.refer_info;
                            [refer_info setObject:tfEmail.text forKey:@"new_user_email"];
                            [branch userCompletedAction:@"refer_regist" withState:refer_info];
                        } @catch (NSException *exception) {
                            
                        }
                        
                    }
                    failure:^(ServerErrorResult *err) {
                        [[Utility getInstance] showToastMessage:err];
                        [Utility hideProgressDialog];
                        //[[Utility getInstance] gotoScreen:self :@"MainViewController"];
                    }
     
                     params:param];
}

- (IBAction)onClickSignup:(id)sender {
    if(![Utility IsValidEmail:tfEmail.text] ) {
        [[Utility getInstance] showToastMessage:@"Please input correct email."];
        return;
    }
    if([tfPassword.text isEqualToString:@""]) {
        [[Utility getInstance] showToastMessage:@"Please input your password."];
        return;
    }
    
    if(![tfPasswordagain.text isEqualToString:tfPassword.text]) {
        [[Utility getInstance] showToastMessage:@"Please confirm your password."];
        return;
    }
    
    if([tfDisplayname.text isEqualToString:@""]) {
        [[Utility getInstance] showToastMessage:@"Please input your displayname."];
        return;
    }
    
    [self memberRegist];
    
}

- (IBAction)onClickConfirmLogin:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onClickResend:(id)sender {
    [self onClickSignup:nil];
}


- (IBAction)onClickToLogin:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark -
#pragma mark UITouch Methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [[Utility getInstance] cmdHideKeyboard:txtfControl];
    
}

#pragma mark -
#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    txtfControl = textField;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [[Utility getInstance] cmdHideKeyboard:textField];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *str = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    
    
    return true;
}


@end
