//
//  AppDelegate.h
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 3/31/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property NSMutableDictionary *refer_info;
@end

