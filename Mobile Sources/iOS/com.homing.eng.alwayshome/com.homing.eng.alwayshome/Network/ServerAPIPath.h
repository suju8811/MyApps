//
//  ServerAPIPath.h
//  etip
//


#import <Foundation/Foundation.h>

#define API_BASE_URL @"http://120.76.177.162:8000/"

//device
extern NSString *const API_POST_ONLINE;
extern NSString *const API_POST_OFFLINE;
extern NSString *const API_POST_USAGE;

//main
extern NSString *const API_GET_PLAN_LIST;  //get
extern NSString *const API_GET_PLAN_INFO; //{plan-id}/   get
extern NSString *const API_GET_PACKAGE_LIST;  //get
extern NSString *const API_GET_PACKAGE_INFO; //{package-id}/   get

extern NSString *const API_POST_BOOK_PLAN;
extern NSString *const API_POST_RECHARGE;

extern NSString *const API_GET_BOOK_ORDER_LIST;
extern NSString *const API_GET_RECHARGE_ORDER_LIST;


//user
extern NSString *const API_POST_LOGIN;
extern NSString *const API_POST_LOGOUT;

extern NSString *const API_POST_REGIST;
extern NSString *const API_POST_CHANGE_PASSWORD;
extern NSString *const API_GET_USER_INFO;
extern NSString *const API_POST_UPDATE_INFO;

extern NSString *const API_GET_SHARE_URL;  //get
extern NSString *const API_GET_REFERRALS; //get

extern NSString *const API_POST_GET_USAGE_DATA;

@interface ServerAPIPath : NSObject

@end
