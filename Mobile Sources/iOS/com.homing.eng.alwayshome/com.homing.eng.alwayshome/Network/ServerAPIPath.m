//
//  ServerAPIPath.m
//  etip
//


#import "ServerAPIPath.h"

//device
NSString *const API_POST_ONLINE =  @"online/";
NSString *const API_POST_OFFLINE =  @"offline/";
NSString *const API_POST_USAGE =  @"usage/";

//main
NSString *const API_GET_PLAN_LIST =  @"plans/";  //get
NSString *const API_GET_PLAN_INFO =  @"plan-detail/"; //{plan-id}/   get
NSString *const API_GET_PACKAGE_LIST =  @"packages/";  //get
NSString *const API_GET_PACKAGE_INFO =  @"package-detail/"; //{package-id}/   get

NSString *const API_POST_BOOK_PLAN =  @"book/";
NSString *const API_POST_RECHARGE =  @"recharge/";

NSString *const API_GET_BOOK_ORDER_LIST =  @"book-order-list/";
NSString *const API_GET_RECHARGE_ORDER_LIST =  @"recharge-order-list/";


//user
NSString *const API_POST_LOGIN =  @"login/";
NSString *const API_POST_LOGOUT = @"logout/";

NSString *const API_POST_REGIST = @"register/";
NSString *const API_POST_CHANGE_PASSWORD = @"change-password/";
NSString *const API_GET_USER_INFO = @"customer-detail/";
NSString *const API_POST_UPDATE_INFO = @"customer-update/";

NSString *const API_GET_SHARE_URL = @"share-url";  //get
NSString *const API_GET_REFERRALS = @"referring/"; //get

NSString *const API_POST_GET_USAGE_DATA = @"usage/";

@implementation ServerAPIPath

@end
