//
//  ServerAPICall.m
//  etip
//


#import "ServerAPICall.h"
#import "ServerAPIPath.h"
#import "AFNetworking.h"
#import "Preference.h"
#import "Utility.h"
#import "Constants.h"
#import <Foundation/Foundation.h>

typedef enum _METHOD {
    GET,
    POST,
    MULTIPOST,
    MULTIPOST_ARRAY,
    PUT,
    DELETE
}METHOD;

NSString *const  ERR_INVALID_FORMAT_DATA = @"Server error.Try again later.";
NSString *const  ERR_NO_NETWORK = @"No network,please check";

#define  TIME_LIMIT (180)

#define  UPLOAD_MAX_WIDTH (1020)

@interface ServerAPICall ()
{
@private
    onResponse      _successBlock;
    onErrorResponse _errorBlock;
    int             _method;
    NSString       *_url;
    NSDictionary   *_params;
    NSMutableArray *_arrImage;
    NSMutableArray *_arrImageParam;
    UIImage        *_image;
    NSString       *_imageParam;
}

@end

@implementation ServerAPICall

+(void) callGET:(NSString*)url success:(onResponse)success failure:(onErrorResponse)failure {

    ServerAPICall *call = [[ServerAPICall alloc] init];
    
    [call callGET:url success:success failure:failure];
}

+(void) callPOST:(NSString*)url success:(onResponse)success failure:(onErrorResponse)failure params:(NSDictionary*)params {
    
    ServerAPICall *call = [[ServerAPICall alloc] init];
    
    [call callPOST:url success:success failure:failure params:params];
}


+(void) callPOSTMultipart:(NSString*)url image:(UIImage*)image :(NSString *)imageParam success:(onResponse)success failure:(onErrorResponse)failure params:(NSDictionary*)params {
    
    ServerAPICall *call = [[ServerAPICall alloc] init];
    if(image == nil)
        [call callPOST:url success:success failure:failure params:params];
    else
        [call callPOSTMultipart :url image:image :imageParam success:success failure:failure params:params];
}

+(void) callPOSTMultipart:(NSString*)url imageArray:(NSMutableArray*)arrImage :(NSMutableArray*)arrImageParam success:(onResponse)success failure:(onErrorResponse)failure params:(NSDictionary*)params {
    
    ServerAPICall *call = [[ServerAPICall alloc] init];
    if(arrImage == nil)
        [call callPOST:url success:success failure:failure params:params];
    else
        [call callPOSTMultipart :url imageArray:arrImage :arrImageParam success:success failure:failure params:params];
}


+(void) uploadPhoto:(NSString*)url image:(UIImage*)image success:(onResponse)success failure:(onErrorResponse)failure {
   
    ServerAPICall *call = [[ServerAPICall alloc] init];

    [call setCallBack:success failure:failure];
    [call uploadPhoto:url image:image];
}

-(void) setCallBack:(onResponse)sucess failure:(onErrorResponse)failure {
    _successBlock = sucess;
    _errorBlock = failure;
}

-(void) callGET:(NSString*)url success:(onResponse)success failure:(onErrorResponse)failure {
    _successBlock = success;
    _errorBlock = failure;
    _method = GET;
    _url = url;
    
    [self callServerAPI];
}


-(void) callPOST:(NSString*)url success:(onResponse)success failure:(onErrorResponse)failure params:(NSDictionary*)params {
    _successBlock = success;
    _errorBlock = failure;
    _method = POST;
    _url = url;
    _params = params;
    
    [self callServerAPI];
}

-(void) callPOSTMultipart:(NSString*)url  image:(UIImage*)image :(NSString *)imageParam success:(onResponse)success failure:(onErrorResponse)failure params:(NSDictionary*)params {
    _successBlock = success;
    _errorBlock = failure;
    _method = MULTIPOST;
    _url = url;
    _params = params;
    _image = image;
    _imageParam = imageParam;
    [self callServerAPI];
}

-(void) callPOSTMultipart:(NSString*)url  imageArray:(NSMutableArray*)arrImage :(NSMutableArray*)arrImageParam success:(onResponse)success failure:(onErrorResponse)failure params:(NSDictionary*)params {
    _successBlock = success;
    _errorBlock = failure;
    _method = MULTIPOST_ARRAY;
    _url = url;
    _params = params;
    _arrImage = arrImage;
    _arrImageParam = arrImageParam;
    [self callServerAPI];
}


-(void) callServerAPI {
    // 1
    NSURL *baseURL = [NSURL URLWithString:_url];
    
    // 2
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:TIME_LIMIT];  //Time out after TIME_LIMIT seconds
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    [self initHttpHeader:manager.requestSerializer];
    
    if(_method == GET) {
        [manager GET:_url parameters:nil
             success:^(NSURLSessionDataTask *task, id responseObject) {
                 NSHTTPURLResponse *response = ((NSHTTPURLResponse *)[task response]);
                 
                 NSDictionary *headers = [response allHeaderFields];
                 
                 NSString *set_cookie = [headers valueForKey:@"Set-Cookie"];
                 if(set_cookie != nil && set_cookie.length>0)
                 {
                     NSLog(@"%@", set_cookie);
                     set_cookie = [set_cookie stringByReplacingOccurrencesOfString:@"Path=/," withString:@""];
                     set_cookie = [set_cookie stringByReplacingOccurrencesOfString:@"Path=/" withString:@""];
                     NSArray *split = [set_cookie componentsSeparatedByString:@";"];
                     NSString *cookie = @"";
                     
                     for(int i=0;i<split.count;i++)
                     {
                         NSString *str = [split[i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                         if([str hasPrefix:@"sessionid"]) {
                             cookie = [NSString stringWithFormat:@"%@; %@",str,cookie];
                         } else if([str hasPrefix:@"csrftoken"]) {
                             cookie = [NSString stringWithFormat:@"%@%@",str,cookie];
                             NSString *csrftoken = [str stringByReplacingOccurrencesOfString:@"csrftoken=" withString:@""];
                             [[Preference getInstance] putSharedPreference:nil :PREF_CSRFTOKEN :csrftoken];
                         }
                     }
                     [[Preference getInstance] putSharedPreference:nil :PREF_COOKIE :cookie];
                     
                 }
                 
                 [self preprocessResult:responseObject:task];
             } failure:^(NSURLSessionDataTask *task, NSError *error) {
                 NSHTTPURLResponse *response = ((NSHTTPURLResponse *)[task response]);
                 if(response.statusCode > 299) {
                     if(response.statusCode == 403 && [[Preference getInstance] getSharedPreference:nil :IS_LOGINED WithBOOL:false]) {
                         [[Utility getInstance] gotoScreen:nil :@"LoginViewController"];
                         if([[Preference getInstance] getSharedPreference:nil :IS_LOGINED WithBOOL:false]) {
                             _errorBlock(@"Session timeout or You may login in another device.");
                         } else {
                             _errorBlock(@"Login please.");
                         }
                     } else {
                         if(error.localizedDescription != nil)
                             _errorBlock(error.localizedDescription);
                         else
                             _errorBlock(@"Server error.Try again later.");
                     }
                 } else
                     _errorBlock(ERR_NO_NETWORK);
             }];
    }
    else if(_method == POST){
        
        /*NSError *err;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:_params options:0 error:&err];
        NSString *strParam = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"params:%@",strParam);
         */
       /* NSError *err;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:_params options:0 error:&err];
        NSString *strParam = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"params:%@",strParam);*/
        
        
        [manager POST:_url parameters:_params
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  NSHTTPURLResponse *response = ((NSHTTPURLResponse *)[task response]);
                  
                  NSDictionary *headers = [response allHeaderFields];
                  
                  NSString *set_cookie = [headers valueForKey:@"Set-Cookie"];
                  if(set_cookie != nil && set_cookie.length>0)
                  {
                      NSLog(@"%@", set_cookie);
                      set_cookie = [set_cookie stringByReplacingOccurrencesOfString:@"Path=/," withString:@""];
                      set_cookie = [set_cookie stringByReplacingOccurrencesOfString:@"Path=/" withString:@""];
                      NSArray *split = [set_cookie componentsSeparatedByString:@";"];
                      NSString *cookie = @"";
                      
                      for(int i=0;i<split.count;i++)
                      {
                          NSString *str = [split[i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                          if([str hasPrefix:@"sessionid"]) {
                              cookie = [NSString stringWithFormat:@"%@; %@",str,cookie];
                          } else if([str hasPrefix:@"csrftoken"]) {
                              cookie = [NSString stringWithFormat:@"%@%@",str,cookie];
                              NSString *csrftoken = [str stringByReplacingOccurrencesOfString:@"csrftoken=" withString:@""];
                              [[Preference getInstance] putSharedPreference:nil :PREF_CSRFTOKEN :csrftoken];
                          }
                      }
                      [[Preference getInstance] putSharedPreference:nil :PREF_COOKIE :cookie];
                      
                  }
                  
                  [self preprocessResult:responseObject:task];
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  NSHTTPURLResponse *response = ((NSHTTPURLResponse *)[task response]);
                  if(response.statusCode > 299) {
                      if(response.statusCode == 403 && [[Preference getInstance] getSharedPreference:nil :IS_LOGINED WithBOOL:false]) {
                          [[Utility getInstance] gotoScreen:nil :@"LoginViewController"];
                          if([[Preference getInstance] getSharedPreference:nil :IS_LOGINED WithBOOL:false]) {
                              _errorBlock(@"Session timeout or You may login in another device.");
                          } else {
                              _errorBlock(@"Login please.");
                          }
                      } else {
                          if(error.localizedDescription != nil)
                              _errorBlock(error.localizedDescription);
                          else
                              _errorBlock(@"Server error.Try again later.");
                      }
                  } else
                      _errorBlock(ERR_NO_NETWORK);
              }];
    }  
    
}

-(void) initHttpHeader:(AFHTTPRequestSerializer*)request {
    
    [request setValue:[[Preference getInstance] getSharedPreference:nil :PREF_COOKIE :@""] forHTTPHeaderField:@"Cookie"];
    [request setValue:[[Preference getInstance] getSharedPreference:nil :PREF_CSRFTOKEN :@""] forHTTPHeaderField:@"X-CSRFTOKEN"];
    //[request setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
}


-(void) preprocessResult:(id) JSON :(NSURLSessionDataTask *)task {
   
    @try {
        NSError  *error = nil;
        NSDictionary *response = (NSDictionary*)JSON;
        
        if(error != nil) {
            _errorBlock(ERR_INVALID_FORMAT_DATA);
            return;
        }
        
        NSLog(@"post %@",response);
        @try {
            int       code = [[response objectForKey:@"code"] intValue];
            NSString *err  = [NSString stringWithFormat:@"%@",[response objectForKey:@"error"]];
            
            NSLog(@"%@ - code : %d" , _url, code);
            NSLog(@"%@ - error : %@" , _url, err);
            
            if (code != 0) {
                _errorBlock(err);
                return;
            }
        }
        @catch (NSException *exception) {
            
        }
        
        NSObject *result = response;
        if(result == nil) {
            _successBlock(nil);
        }
        else {
            if([result isKindOfClass:[NSNull class]] == true || ([result isKindOfClass:[NSString class]] == true && ([(NSString*)result isEqualToString:@"<null>"] == true || [(NSString*)result isEqualToString:@"null"] == true))) {
                _successBlock(nil);
            }
            else {
                _successBlock(result);
            }
        }
    }
    @catch (NSException *e) {
        NSLog(@"Post - %@",e.reason);
        _errorBlock(ERR_INVALID_FORMAT_DATA);
    }
}

@end
