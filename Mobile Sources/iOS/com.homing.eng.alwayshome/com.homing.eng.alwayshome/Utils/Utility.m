//
//  Utility.m
//  e_tip
//

#import "MainViewController.h"
#import "DrawerViewController.h"
#import "MMDrawerController.h"
#import "MMDrawerVisualState.h"
#import "Utility.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "Preference.h"
#import "Constants.h"

#import "ServerAPIPath.h"
#import "ServerAPICall.h"


MBProgressHUD  *gHUD = nil;
BOOL gIsShowHUD = NO;
int  gHUDRequesCount = 0;

@implementation Utility
static Utility *singletone = nil;

+ (Utility *)getInstance
{
    if (singletone == nil)
        singletone = [[Utility alloc] init];
    
    return singletone;
}

- (void)cmdHideKeyboard:(UITextField*)_txtField {
    
    if (_txtField != nil && [_txtField isFirstResponder]) {
        [_txtField resignFirstResponder];
    }
    
}

-(NSString *)getDateTime:(NSString *)str
{
    str = [str stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    str = [str stringByReplacingOccurrencesOfString:@"Z" withString:@" "];
    return str;
}

+(BOOL) IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)showToastMessage:(NSString *)message
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:appDelegate.window
                                              animated:YES];
    if (!isiPhone) {
        hud.detailsLabelFont=[UIFont systemFontOfSize:25.0];
    }
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabelText = message;
    hud.margin = 10.f;
    hud.yOffset = 150.f;
    
    
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:3.0];
}

+ (void) showProgressDialog:(UIViewController*)controller {
    if(YES == gIsShowHUD) {
        gHUDRequesCount++;
        return;
    }
    
    if(true) {
        UIWindow* w_pMainWindow = [[UIApplication sharedApplication].windows objectAtIndex:0];
        
        //gHUD = [MBProgressHUD showHUDAddedTo:w_pMainWindow animated:YES];
        gHUD = [MBProgressHUD showHUDAddedTo:controller.view animated:YES];
        gHUD.mode = MBProgressHUDModeIndeterminate;
        gHUD.labelText = @"Loading......";
        //        gHUD.labelText = @"";
        [gHUD show:NO];
    }
    
    gIsShowHUD = YES;
}

+ (void) hideProgressDialog {
    
    if(gHUDRequesCount != 0) {
        gHUDRequesCount--;
    }
    else if(gIsShowHUD == YES) {
        
        if(true) {
            [gHUD hide:NO];
        }
        
        gIsShowHUD = NO;
        gHUDRequesCount = 0;
    }
}

- (UIImage*)circularScaleNCrop:(UIImage*)images  with :(CGRect) rect
{
    //Create the bitmap graphics context
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(rect.size.width, rect.size.height), NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //Get the width and heights
    CGFloat imageWidth = images.size.width;
    CGFloat imageHeight = images.size.height;
    CGFloat rectWidth = rect.size.width;
    CGFloat rectHeight = rect.size.height;
    
    //Calculate the scale factor
    CGFloat scaleFactorX = rectWidth/imageWidth;
    CGFloat scaleFactorY = rectHeight/imageHeight;
    
    //Calculate the centre of the circle
    CGFloat imageCentreX = rectWidth/2;
    CGFloat imageCentreY = rectHeight/2;
    
    // Create and CLIP to a CIRCULAR Path
    // (This could be replaced with any closed path if you want a different shaped clip)
    CGFloat radius = rectWidth/2;
    CGContextBeginPath (context);
    CGContextAddArc (context, imageCentreX, imageCentreY, radius, 0, 2*M_PI, 0);
    CGContextClosePath (context);
    CGContextClip (context);
    
    CGContextScaleCTM (context, scaleFactorX, scaleFactorY);
    
    // Draw the IMAGE
    CGRect myRect = CGRectMake(0, 0, imageWidth, imageHeight);
    [images drawInRect:myRect];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 320; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            
            // landscape right
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            
            // landscape left
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            
            // Portrait Mode
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

-(void)gotoScreen:(UIViewController *)curController :(NSString*)storyboardID

{
    CATransition* transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if(curController == nil) curController = [appDelegate.window rootViewController];
    if([storyboardID isEqualToString:@"MainViewController"])
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;
        
        // 3. Create vc
        
        UINavigationController *navViewController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        
        //MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([MainViewController class])];
        DrawerViewController *drawerViewController = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([DrawerViewController class])];
        
        [drawerViewController.view setFrame:CGRectMake(0, 0, 240, screenHeight)];
        
        MMDrawerController * drawerController = [[MMDrawerController alloc]
                                 initWithCenterViewController:navViewController
                                 leftDrawerViewController:drawerViewController];
        
        [drawerController setShowsShadow:NO];
        [drawerController setRestorationIdentifier:@"MMDrawer"];
        [drawerController setMaximumLeftDrawerWidth:240];
        [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
        [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];

        [curController.view.window.layer addAnimation:transition forKey:nil];
        [appDelegate.window setRootViewController:drawerController];
        [curController dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    UIViewController *destViewController = [storyboard instantiateViewControllerWithIdentifier:storyboardID];
    [curController.view.window.layer addAnimation:transition forKey:nil];
    //[destViewController.view.layer addAnimation:transition forKey:kCATransition];    
    //[curController presentViewController:destViewController animated:YES completion:nil];
    [appDelegate.window setRootViewController:destViewController];
    [curController dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)LogOut:(UIViewController *)curController
{
    [Utility showProgressDialog:curController];
    
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [ServerAPICall callPOST:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_POST_LOGOUT]
                    success: ^(ServerResultRef s) {
                        [Utility hideProgressDialog];
                        if(s == nil) {
                            return;
                        }
                        //NSDictionary* jsonObject = (NSDictionary*)s;
                        Preference* pref = [Preference getInstance];
                        [pref putSharedPreference:nil :IS_LOGINED WithBOOL:false];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_COOKIE_NAME :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_DISPLAY_NAME :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_REMARK :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_MONTH_FLOW_BALANCE :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_EXTRA_FLOW_BALANCE :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_START_TIME :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_EXPIRE_TIME :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_DEVICE_NUM :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_USER_ID :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_USER_NAME :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_ID :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_NAME :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_REMARK :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_MONTH_FLOW :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_EXTRA_FLOW :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_SPEED_LIMIT :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_DEVICE_NUMBER_LIMIT :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_DURATION :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_PRICE :@""];
                        [pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_PID :@""];
                        
                        [pref putSharedPreference:nil :PREF_HAS_TRY_FREE_PLAN WithBOOL:false];
                        [pref putSharedPreference:nil :PREF_LAST_PAYMENT WithBOOL:false];
                        [pref putSharedPreference:nil :PREF_LAST_PAYMENT_ID :@""];
                        [pref putSharedPreference:nil :PREF_LAST_PAYMENT_NAME :@""];
                        [pref putSharedPreference:nil :PREF_LAST_PAYMENT_PRICE :@""];
                        [pref putSharedPreference:nil :PREF_LAST_PAYMENT_PID :@""];
                        
                        CATransition* transition = [CATransition animation];
                        transition.duration = .25;
                        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
                        transition.type = kCATransitionPush;
                        transition.subtype = kCATransitionFromRight;

                        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UIViewController *destViewController = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                        [curController.view.window.layer addAnimation:transition forKey:nil];
                        //[destViewController.view.layer addAnimation:transition forKey:kCATransition];
                        //[curController presentViewController:destViewController animated:YES completion:nil];
                        [curController dismissViewControllerAnimated:YES completion:nil];
                        [appDelegate.window setRootViewController:destViewController];
                    }
                    failure:^(ServerErrorResult *err) {
                        [[Utility getInstance] showToastMessage:err];
                        [Utility hideProgressDialog];
                    }
                     params:nil];
   

}

-(void)getUserInfo
{
    [ServerAPICall callGET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_GET_USER_INFO]
                   success: ^(ServerResultRef s) {
                       [Utility hideProgressDialog];
                       if(s == nil) {
                           return;
                       }
                       @try {
                           NSDictionary* jsonObject = (NSDictionary*)s;
                           @try {
                               jsonObject = [jsonObject objectForKey:@"data"];
                               Preference* pref = [Preference getInstance];
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_DISPLAY_NAME :[jsonObject objectForKey:@"name"]];} @catch (NSException *exception) {}
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_REMARK :[jsonObject objectForKey:@"remark"]];} @catch (NSException *exception) {}
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_MONTH_FLOW_BALANCE :[jsonObject objectForKey:@"month_flow_balance"]];} @catch (NSException *exception) {}
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_EXTRA_FLOW_BALANCE :[jsonObject objectForKey:@"extra_flow_balance"]];} @catch (NSException *exception) {}
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_START_TIME :[jsonObject objectForKey:@"start_time"]];} @catch (NSException *exception) {}
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_EXPIRE_TIME :[jsonObject objectForKey:@"expire_time"]];} @catch (NSException *exception) {}
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_DEVICE_NUM :[jsonObject objectForKey:@"device_num"]];} @catch (NSException *exception) {}
                               
                               NSDictionary* userObject = [jsonObject objectForKey:@"user"];
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_USER_NAME :[userObject objectForKey:@"username"]];} @catch (NSException *exception) {}
                               
                               NSDictionary* planObject = [jsonObject objectForKey:@"plan"];
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_ID :[planObject objectForKey:@"id"]];} @catch (NSException *exception) {}
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_NAME :[planObject objectForKey:@"name"]];} @catch (NSException *exception) {}                               
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_REMARK :[planObject objectForKey:@"remark"]];} @catch (NSException *exception) {}
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_MONTH_FLOW :[planObject objectForKey:@"month_flow"]];} @catch (NSException *exception) {}
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_EXTRA_FLOW :[planObject objectForKey:@"extra_flow"]];} @catch (NSException *exception) {}
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_SPEED_LIMIT :[planObject objectForKey:@"speed_limit"]];} @catch (NSException *exception) {}
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_DEVICE_NUMBER_LIMIT :[planObject objectForKey:@"device_number_limit"]];} @catch (NSException *exception) {}
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_DURATION :[planObject objectForKey:@"duration"]];} @catch (NSException *exception) {}
                               @try {[pref putSharedPreference:nil :PREF_USERDETAIL_PLAN_PRICE :[planObject objectForKey:@"price"]];} @catch (NSException *exception) {}
                           } @catch (NSException *exception) {
                           }
                       } @catch (NSException *exception) {
                           NSLog(@"exception:%@",exception.reason);
                       }
                   }
                   failure:^(ServerErrorResult *err) {
                       [[Utility getInstance] showToastMessage:err];
                   }];
}

@end
