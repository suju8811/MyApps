//
//  Utility.h
//  e_tip
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// Set the environment:
// - For live charges, use PayPalEnvironmentProduction (default).
// - To use the PayPal sandbox, use PayPalEnvironmentSandbox.
// - For testing, use PayPalEnvironmentNoNetwork.
#define kPayPalEnvironment PayPalEnvironmentNoNetwork

//iPhone Or iPad
#define isiPhone ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)

//iOS7 Or less
#define ISIOS7 (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)

@interface Utility : NSObject

+ (Utility *)getInstance;
- (void)cmdHideKeyboard:(UITextField*)_txtField;
+(BOOL) IsValidEmail:(NSString *)checkString;
-(void)showToastMessage:(NSString *)message;

+ (void) showProgressDialog:(UIViewController*)controller;
+ (void) hideProgressDialog;

- (UIImage*)circularScaleNCrop:(UIImage*)images  with :(CGRect) rect;
- (UIImage *)scaleAndRotateImage:(UIImage *)image;


-(void)gotoScreen:(UIViewController *)curController :(NSString*)storyboardID;
-(void)LogOut:(UIViewController *)curController;
-(void)getUserInfo;

-(NSString *)getDateTime:(NSString *)str;

@end
