//
//  Color.h
//  mimishop
//
//  Created by KCJ on 6/12/15.
//  Copyright (c) 2015 wuguang. All rights reserved.
//

#import <UIKit/UIKit.h>


extern UIColor *dark_white;
extern UIColor *common_blue;

extern UIColor *grey;
extern UIColor *dark_grey;
extern UIColor *light_grey;
extern UIColor *green;
extern UIColor *dark_green;
extern UIColor *light_green;
extern UIColor *navy;
extern UIColor *dark_navy;
extern UIColor *light_navy;
extern UIColor *blue;
extern UIColor *dark_blue;
extern UIColor *light_blue;
extern UIColor *red;
extern UIColor *dark_red;
extern UIColor *light_red;
extern UIColor *purple;
extern UIColor *dark_purple;
extern UIColor *light_purple;
extern UIColor *bluesky;
extern UIColor *dark_bluesky;
extern UIColor *light_bluesky;
extern UIColor *white;
extern UIColor *light_white;
extern UIColor *main_bottom_bar_background;
extern UIColor *border_gray;
extern UIColor *dark_border_gray;
extern UIColor *event_border_gray;
extern UIColor *button_gray;
extern UIColor *layout_gray;
extern UIColor *layout_dark_gray;
extern UIColor *more_dark_gray;

extern UIColor *text_gray;
extern UIColor *light_text_gray;
extern UIColor *button_star_bg;
extern UIColor *cast_bg_gray;
extern UIColor *shop_img_bg;
extern UIColor *my_jim_list_bg;
extern UIColor *envelop_select_bg;
extern UIColor *tube_bg1;
extern UIColor *tube_bg2;
extern UIColor *tube_bg3;
extern UIColor *tube_bg4;
extern UIColor *tube_bg5;
extern UIColor *tube_bg6;
extern UIColor *tube_bg7;
extern UIColor *tube_bg8;
extern UIColor *tube_bg9;
extern UIColor *tube_bg10;
extern UIColor *tube_bg11;
extern UIColor *best_cast_text_bg;
extern UIColor *cast_comment_popup_bg;
extern UIColor *cast_comment_bg;
extern UIColor *cast_comment_btn_bg;
extern UIColor *shop_horizental_bg;
extern UIColor *orange;
extern UIColor *list_item_title;
extern UIColor *list_background;
extern UIColor *list_background_pressed;
extern UIColor *list_divider;
extern UIColor *dialog_category_list_divider;
extern UIColor *list_background_selected_color;

extern UIColor *font_orange;
extern UIColor *transparent;
extern UIColor *light_translucent;
extern UIColor *translucent;
extern UIColor *black;
extern UIColor *black_overlay;
extern UIColor *event_font_color_title;
extern UIColor *font_light_gray;
extern UIColor *font_color_dark_gray;
extern UIColor *font_color_more_dark_gray;
extern UIColor *font_color_gray;
extern UIColor *font_color_red;
extern UIColor *location_list_bg;
extern UIColor *location_list_text_color;
extern UIColor *location_divider_color;
extern UIColor *exit_dlg_btn_bg_selected;

extern UIColor *home_bg_shop;
extern UIColor *home_bg_cast;
extern UIColor *home_bg_life;
extern UIColor *home_bg_talk;
extern UIColor *intro_info;


@interface ColorSelector : NSObject

@property (nonatomic, strong) UIColor *normal;
@property (nonatomic, strong) UIColor *selected;

- (id) initWithColors:(UIColor*)normal selected:(UIColor*)selected;

@end

extern ColorSelector *text_cast_comment_search_selector;
extern ColorSelector *text_set_current_location_selector;
@interface Color : NSObject

+(void)initConstants;
+(UIColor*)colorWithHexString:(NSString*)hex;

@end



