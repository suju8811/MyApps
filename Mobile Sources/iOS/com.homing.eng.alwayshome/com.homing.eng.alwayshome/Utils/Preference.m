//
//  Perference.m
//  freecket
//


#import "Preference.h"

static Preference *mInstance;

@implementation Preference

+(Preference*) getInstance {
    if (nil == mInstance) {
        mInstance = [[Preference alloc] init];
    }
    return mInstance;
}

-(void) putSharedPreference:(id) context :(NSString*) key  :(NSString*) value {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:key];
}

-(void) putSharedPreference:(id) context :(NSString*) key  WithBOOL:(BOOL) value {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if(YES == value) {
        [defaults setObject:@"YES" forKey:key];
    }
    else {
        [defaults setObject:@"NO" forKey:key];
    }
    
}

-(void) putSharedPreference:(id) context :(NSString*) key  WithINT:(int) value {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:[NSString stringWithFormat:@"%d", value] forKey:key];
}

-(NSString*) getSharedPreference:(id) context  :(NSString*) key  :(NSString*) defaultValue {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *str = [defaults objectForKey:key];
    
    if(nil  == str) {
        return defaultValue;
    }
    
    return str;
}

-(int) getSharedPreference:(id) context  :(NSString*) key  WithINT:(int) defaultValue  {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *str = [defaults objectForKey:key];
    
    if(nil  == str) {
        return defaultValue;
    }
    
    return [str intValue];
}

-(BOOL) getSharedPreference:(id) context  :(NSString*) key  WithBOOL:(BOOL) defaultValue  {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *str = [defaults objectForKey:key];
    
    if(nil  == str) {
        return defaultValue;
    }
    
    if([str isEqualToString:@"YES"] == YES) {
        return YES;
    }
    
    return NO;
}

@end
