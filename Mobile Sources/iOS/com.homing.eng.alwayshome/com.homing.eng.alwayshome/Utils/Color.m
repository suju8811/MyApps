//
//  Color.m
//  mimishop
//
//  Created by KCJ on 6/12/15.
//  Copyright (c) 2015 wuguang. All rights reserved.
//

#import "Color.h"

UIColor *dark_white;
UIColor *common_blue;

UIColor *grey;
UIColor *dark_grey;
UIColor *light_grey;
UIColor *green;
UIColor *dark_green;
UIColor *light_green;
UIColor *navy;
UIColor *dark_navy;
UIColor *light_navy;
UIColor *blue;
UIColor *dark_blue;
UIColor *light_blue;
UIColor *red;
UIColor *dark_red;
UIColor *light_red;
UIColor *purple;
UIColor *dark_purple;
UIColor *light_purple;
UIColor *bluesky;
UIColor *dark_bluesky;
UIColor *light_bluesky;
UIColor *white;
UIColor *light_white;
UIColor *main_bottom_bar_background;
UIColor *border_gray;
UIColor *dark_border_gray;
UIColor *event_border_gray;
UIColor *button_gray;
UIColor *layout_gray;
UIColor *layout_dark_gray;
UIColor *more_dark_gray;

UIColor *text_gray;
UIColor *light_text_gray;
UIColor *button_star_bg;
UIColor *cast_bg_gray;
UIColor *shop_img_bg;
UIColor *my_jim_list_bg;
UIColor *envelop_select_bg;
UIColor *tube_bg1;
UIColor *tube_bg2;
UIColor *tube_bg3;
UIColor *tube_bg4;
UIColor *tube_bg5;
UIColor *tube_bg6;
UIColor *tube_bg7;
UIColor *tube_bg8;
UIColor *tube_bg9;
UIColor *tube_bg10;
UIColor *tube_bg11;
UIColor *best_cast_text_bg;
UIColor *cast_comment_popup_bg;
UIColor *cast_comment_bg;
UIColor *cast_comment_btn_bg;
UIColor *shop_horizental_bg;
UIColor *orange;
UIColor *list_item_title;
UIColor *list_background;
UIColor *list_background_pressed;
UIColor *list_divider;
UIColor *dialog_category_list_divider;
UIColor *list_background_selected_color;

UIColor *font_orange;
UIColor *transparent;
UIColor *light_translucent;
UIColor *translucent;
UIColor *black;
UIColor *black_overlay;
UIColor *event_font_color_title;
UIColor *font_light_gray;
UIColor *font_color_dark_gray;
UIColor *font_color_more_dark_gray;
UIColor *font_color_gray;
UIColor *font_color_red;
UIColor *location_list_bg;
UIColor *location_list_text_color;
UIColor *location_divider_color;
UIColor *exit_dlg_btn_bg_selected;

UIColor *home_bg_shop;
UIColor *home_bg_cast;
UIColor *home_bg_life;
UIColor *home_bg_talk;
UIColor *intro_info;

ColorSelector *text_cast_comment_search_selector;
ColorSelector *text_set_current_location_selector;

@implementation Color

+(void)initConstants {
    dark_white  = [Color colorWithHexString:@"#d8d8d8"];
    common_blue  = [Color colorWithHexString:@"#149dd8"];
    
    grey  = [Color colorWithHexString:@"#f7f7f7"];
    dark_grey  = [Color colorWithHexString:@"#3c3c3c"];
    light_grey  = [Color colorWithHexString:@"#c3c3c3"];
    more_dark_gray = [Color colorWithHexString:@"#9c9ca1"];
    
    green  = [Color colorWithHexString:@"#3fd399"];
    dark_green  = [Color colorWithHexString:@"#25b46b"];
    light_green  = [Color colorWithHexString:@"#82f1b9"];
    
    navy  = [Color colorWithHexString:@"#f2916e"];
    dark_navy  = [Color colorWithHexString:@"#ff7b23"];
    light_navy  = [Color colorWithHexString:@"#f7b081"];
    
    blue  = [Color colorWithHexString:@"#ff4973"];
    dark_blue  = [Color colorWithHexString:@"#11627a"];
    light_blue  = [Color colorWithHexString:@"#25acd3"];
    
    red  = [Color colorWithHexString:@"#ff1b4c"];
    dark_red  = [Color colorWithHexString:@"#c1042c"];
    light_red  = [Color colorWithHexString:@"#ff7b23"];
    
    purple  = [Color colorWithHexString:@"#a990b7"];
    dark_purple  = [Color colorWithHexString:@"#ff7d0f8a"];
    light_purple  = [Color colorWithHexString:@"#a749db"];
    
    bluesky  = [Color colorWithHexString:@"#509DFF"];
    dark_bluesky  = [Color colorWithHexString:@"#2C5FC7"];
    light_bluesky  = [Color colorWithHexString:@"#BACBD9"];
    
    white  = [Color colorWithHexString:@"#ffffffff"];
    light_white  = [Color colorWithHexString:@"#afffffff"];
    
    main_bottom_bar_background  = [Color colorWithHexString:@"#f9f7f8"];
    border_gray  = [Color colorWithHexString:@"#e1dfe1"];
    dark_border_gray  = [Color colorWithHexString:@"#888888"];
    event_border_gray  = [Color colorWithHexString:@"#dddddd"];
    button_gray  = [Color colorWithHexString:@"#b0b0b0"];
    layout_gray  = [Color colorWithHexString:@"#cccccc"];
    layout_dark_gray  = [Color colorWithHexString:@"#464646"];
    text_gray  = [Color colorWithHexString:@"#444444"];
    light_text_gray  = [Color colorWithHexString:@"#999999"];
    button_star_bg  = [Color colorWithHexString:@"#7ad4e1"];
    cast_bg_gray  = [Color colorWithHexString:@"#f2f2f2"];
    shop_img_bg  = [Color colorWithHexString:@"#ffe9ee"];
    my_jim_list_bg  = [Color colorWithHexString:@"#f1f1f1"];
    envelop_select_bg  = [Color colorWithHexString:@"#fff6f8"];
    tube_bg1  = [Color colorWithHexString:@"#011b6f"];
    tube_bg2  = [Color colorWithHexString:@"#26a500"];
    tube_bg3  = [Color colorWithHexString:@"#ff7d01"];
    tube_bg4  = [Color colorWithHexString:@"#0184fa"];
    tube_bg5  = [Color colorWithHexString:@"#8207b7"];
    tube_bg6  = [Color colorWithHexString:@"#682c2b"];
    tube_bg7  = [Color colorWithHexString:@"#49682c"];
    tube_bg8  = [Color colorWithHexString:@"#fd017c"];
    tube_bg9  = [Color colorWithHexString:@"#a89536"];
    tube_bg10  = [Color colorWithHexString:@"#ae0606"];
    tube_bg11  = [Color colorWithHexString:@"#f9c003"];
    best_cast_text_bg  = [Color colorWithHexString:@"#484848"];
    cast_comment_popup_bg  = [Color colorWithHexString:@"#ffffff"];
    cast_comment_bg  = [Color colorWithHexString:@"#cecece"];
    cast_comment_btn_bg  = [Color colorWithHexString:@"#969699"];
    shop_horizental_bg  = [Color colorWithHexString:@"#e2e2e2"];
    
    orange  = [Color colorWithHexString:@"#fbc038"];
    list_item_title  = [Color colorWithHexString:@"#000fff"];
    list_background  = [Color colorWithHexString:@"#303030"];
    list_background_pressed  = [Color colorWithHexString:@"#992416"];
    list_divider  = [Color colorWithHexString:@"#272727"];
    dialog_category_list_divider  = [Color colorWithHexString:@"#fff0f0f0"];
    list_background_selected_color  = [Color colorWithHexString:@"#40039fd0"];
    font_orange  = [Color colorWithHexString:@"#ffff9600"];
    
    transparent  = [Color colorWithHexString:@"#00000000"];
    light_translucent  = [Color colorWithHexString:@"#40000000"];
    translucent  = [Color colorWithHexString:@"#80000000"];
    black  = [Color colorWithHexString:@"#ff000000"];
    black_overlay  = [Color colorWithHexString:@"#66000000"];
   
    event_font_color_title  = [Color colorWithHexString:@"#ff8f8f8f"];
    
    font_light_gray  = [Color colorWithHexString:@"#ffb1b1b1"];
    font_color_dark_gray  = [Color colorWithHexString:@"#ff666666"];
    font_color_more_dark_gray  = [Color colorWithHexString:@"#ff616161"];
    font_color_gray  = [Color colorWithHexString:@"#ff909090"];
    font_color_red  = [Color colorWithHexString:@"#ffe6301f"];
  
    location_list_bg  = [Color colorWithHexString:@"#333333"];
    location_list_text_color  = [Color colorWithHexString:@"#f2f2f2"];
    location_divider_color  = [Color colorWithHexString:@"#444444"];
    
    exit_dlg_btn_bg_selected  = [Color colorWithHexString:@"#ffedf1"];
    
    home_bg_shop  = [Color colorWithHexString:@"#5b5b5b"];
    home_bg_cast  = [Color colorWithHexString:@"#54bee9"];
    home_bg_life  = [Color colorWithHexString:@"#80cf61"];
    home_bg_talk  = [Color colorWithHexString:@"#ff4973"];
    intro_info  = [Color colorWithHexString:@"#efefef"];
    
}

+(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X, # if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];
    
    if ([cString length] != 6 && [cString length] != 8) return  [UIColor grayColor];
    
    int rIdx = 0;
    NSString *alpha = nil;
    NSRange range;
    
    if([cString length] == 8) {
        range.location = 0;
        range.length = 2;
        alpha = [cString substringWithRange:range];
        rIdx = 2;
    }
    
    // Separate into r, g, b substrings
    range.location = rIdx;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location += 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location += 2;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b, a = 255;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
   
    if(alpha != nil) {
        [[NSScanner scannerWithString:alpha] scanHexInt:&a];
    }
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:((float) a / 255.0f)];
}


@end

@implementation ColorSelector

- (id) initWithColors:(UIColor*)normal selected:(UIColor*)selected {
    if((self = [super init]) != nil) {
        self.selected = selected;
        self.normal = normal;
    }
    
    return self;
}

@end
