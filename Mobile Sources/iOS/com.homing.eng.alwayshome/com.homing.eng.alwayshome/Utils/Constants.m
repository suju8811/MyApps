//
//  Constants.m
//  StarRentCar
//


#import "Constants.h"


NSString *const IS_LOGINED = @"is_logined";
NSString *const PREF_COOKIE = @"cookie";
NSString *const PREF_CSRFTOKEN = @"csrftoken";

NSString *const PREF_USERDETAIL_COOKIE_NAME = @"cookie_name";
NSString *const PREF_USERDETAIL_DISPLAY_NAME = @"display_name";
NSString *const PREF_USERDETAIL_REMARK = @"remark";
NSString *const PREF_USERDETAIL_MONTH_FLOW_BALANCE = @"month_flow_balance";
NSString *const PREF_USERDETAIL_EXTRA_FLOW_BALANCE = @"extra_flow_balance";
NSString *const PREF_USERDETAIL_START_TIME = @"start_time";
NSString *const PREF_USERDETAIL_EXPIRE_TIME = @"expire_time";
NSString *const PREF_USERDETAIL_DEVICE_NUM = @"device_num";

NSString *const PREF_USERDETAIL_USER_ID = @"user_id";
NSString *const PREF_USERDETAIL_USER_NAME = @"user_name";


NSString *const PREF_USERDETAIL_PLAN_ID = @"plan_id";
NSString *const PREF_USERDETAIL_PLAN_NAME = @"plan_name";
NSString *const PREF_USERDETAIL_PLAN_REMARK = @"plan_remark";
NSString *const PREF_USERDETAIL_PLAN_MONTH_FLOW = @"plan_month_flow";
NSString *const PREF_USERDETAIL_PLAN_EXTRA_FLOW = @"plan_extra_flow";
NSString *const PREF_USERDETAIL_PLAN_SPEED_LIMIT = @"plan_speed_limit";
NSString *const PREF_USERDETAIL_PLAN_DEVICE_NUMBER_LIMIT = @"plan_device_number_limit";
NSString *const PREF_USERDETAIL_PLAN_DURATION = @"plan_duration";
NSString *const PREF_USERDETAIL_PLAN_PRICE = @"plan_price";
NSString *const PREF_USERDETAIL_PLAN_PID = @"plan_pid";

NSString *const PREF_SHARE_URL = @"share_url";

NSString *const PREF_REFER_INFO = @"refer_info";
NSString *const PREF_HAS_TRY_FREE_PLAN = @"has_try_free_plan";

NSString *const PREF_LAST_PAYMENT = @"last_payment";

NSString *const PREF_LAST_PAYMENT_ID = @"last_payment_id";
NSString *const PREF_LAST_PAYMENT_NAME = @"last_payment_name";
NSString *const PREF_LAST_PAYMENT_PRICE = @"last_payment_price";
NSString *const PREF_LAST_PAYMENT_PID = @"last_payment_pid";

@implementation Constants

@end
