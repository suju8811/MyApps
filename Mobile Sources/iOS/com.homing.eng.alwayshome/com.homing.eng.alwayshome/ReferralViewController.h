//
//  ReferralViewController.h
//  com.homing.eng.alwayshome
//
//  Created by zhaochenghe on 4/2/17.
//  Copyright © 2017 jn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReferralViewController : UIViewController
{
    
    IBOutlet UILabel *lblInstalls;
    IBOutlet UILabel *lblRegistrations;
    IBOutlet UILabel *lblPurchases;
    
}
@end
