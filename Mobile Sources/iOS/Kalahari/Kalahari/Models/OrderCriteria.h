//
//  OrderCriteria.h
//  Kalahari
//
//  Created by LMAN on 4/14/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Products.h"

typedef NS_ENUM(NSInteger, OrderKind) {
    
    ORDER_KIND_NONE                 = 0,
    ORDER_KIND_NEW                  = 1,
    ORDER_KIND_CURRENT              = 2,
    ORDER_KIND_PENDING              = 3,
    ORDER_KIND_TRACK                = 4,
    ORDER_KIND_FAVORITE             = 5,
};

@interface OrderCriteria : NSObject

@property (nonatomic, assign) OrderKind kind;
@property (nonatomic, strong) NSString * searchStr;
@property (nonatomic, strong) NSString * orderNo;
@property (nonatomic, strong) Products * product;
@property (nonatomic, assign) NSInteger count;

@end
