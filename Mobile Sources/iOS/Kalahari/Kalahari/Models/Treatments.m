//
//  Treatments.m
//  Kalahari
//
//  Created by Michael Lee on 11/19/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "Treatments.h"

@implementation Treatments

- (id)init {
    
    self = [super init];
    
    [self initTreatment];
    
    return self;
}

- (void)initTreatment {
    
    self.objectId = @"";
    self.title = @"";
    self.show = NO;
    self.image = @"";
    self.shortDescription = @"";
    self.longDescription = @"";
    self.created = nil;
    self.updated = nil;
}

@end
