//
//  Category.h
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Categorys : NSObject

@property (nonatomic, strong)   NSString * objectId;
@property (nonatomic, assign)   NSInteger categoryId;
@property (nonatomic, assign)   NSInteger parentId;
@property (nonatomic, strong)   NSString * image;
@property (nonatomic, strong)   NSString * name;
@property (nonatomic, assign)   BOOL hasChild;

- (void)initCategory;

@end
