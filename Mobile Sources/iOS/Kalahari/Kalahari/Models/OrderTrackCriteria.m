//
//  OrderTrackCriteria.m
//  Kalahari
//
//  Created by LMAN on 4/15/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "OrderTrackCriteria.h"

@implementation OrderTrackCriteria

- (id)init {
    
    self = [super init];
    
    self.orderNo = @"";
    self.trackingNumber = @"";
    
    return self;
}
@end
