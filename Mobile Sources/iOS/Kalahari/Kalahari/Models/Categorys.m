//
//  Category.m
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "Categorys.h"

@implementation Categorys

- (id)init {
    
    self = [super init];
    
    [self initCategory];
    
    return self;
}

- (void)initCategory {
    
    self.objectId = @"";
    self.categoryId = 0;
    self.parentId = 0;
    self.image = @"";
    self.name = @"";
    self.hasChild = NO;
}

@end
