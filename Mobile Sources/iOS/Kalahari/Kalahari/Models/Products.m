//
//  Product.m
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "Products.h"

@implementation Products

- (id)init {
    
    self = [super init];
    
    [self initProduct];
    
    return self;
}

- (void)initProduct {
    
    self.objectId = nil;
    self.categoryId = 0;
    self.code = @"";
    self.name = @"";
    self.image = @"";
    self.specialImage = @"";
    self.size = @"";
    self.price = 0.0f;
    self.retailPrice = 0.0f;
    self.shortDescription = @"";
    self.longDescription = @"";
    self.special = NO;
    self.pod = 0;
    self.swopDescription = @"";
    self.tag1 = @"";
    self.tag2 = @"";
    self.tag3 = @"";
    self.tag4 = @"";
    self.tag5 = @"";
}

- (NSString*)displayedTag {

    NSMutableArray * arrTag = [[NSMutableArray alloc] init];
    
    if (![_tag1 isEqualToString:@""]) {
        [arrTag addObject:_tag1];
    }
    
    if (![_tag2 isEqualToString:@""]) {
        [arrTag addObject:_tag2];
    }
    
    if (![_tag3 isEqualToString:@""]) {
        [arrTag addObject:_tag3];
    }
    
    if (![_tag4 isEqualToString:@""]) {
        [arrTag addObject:_tag4];
    }
    
    if (![_tag5 isEqualToString:@""]) {
        [arrTag addObject:_tag5];
    }
    
    return [arrTag componentsJoinedByString:@","];
}

- (NSString*)displayedPrice {
    return [NSString stringWithFormat:@"R %.2f", _price];
}

- (NSString*)displayedPod {
    return [NSString stringWithFormat:@"POD %d", (int)_pod];
}

- (CGFloat)calculateGP {
    
    CGFloat gp  = _retailPrice - _price;
    if (gp < 0)
        gp = 0;
    return gp;
}

- (NSString*)displayedGP {

    return [NSString stringWithFormat:@"%@ %.2f %@", @"R", [self calculateGP], @"GP"];
}

- (NSString*)displayedAll {
    return [NSString stringWithFormat:@"%@, %@, %@", [self displayedPrice], [self displayedPod], [self displayedGP]];
}

- (void)copyValues:(Products*)obj {
    
    if (obj == nil) {
        return;
    }
    
    _objectId = obj.objectId;
    _categoryId = obj.categoryId;
    _code = obj.code;
    _name = obj.name;
    _specialImage = obj.specialImage;
    _image = obj.image;
    _size = obj.size;
    _price = obj.price;
    _shortDescription = obj.shortDescription;
    _longDescription = obj.longDescription;
    _special = obj.special;
    
    _retailPrice = obj.retailPrice;
    _pod = obj.pod;
    _swopDescription = obj.swopDescription;
    
    _tag1 = obj.tag1;
    _tag2 = obj.tag2;
    _tag3 = obj.tag3;
    _tag4 = obj.tag4;
    _tag5 = obj.tag5;
}

@end
