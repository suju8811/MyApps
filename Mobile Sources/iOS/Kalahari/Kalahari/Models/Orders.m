//
//  Order.m
//  Kalahari
//
//  Created by LMAN on 3/30/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "Orders.h"
#import "OrderManager.h"

@implementation Orders

- (id)init {
    
    self = [super init];
    
    [self initOrder];
    
    return self;
}

- (void)initOrder {
    
    self.objectId = nil;
    self.userId = @"";;
    self.orderNo = @"";
    self.orderName = @"";
    self.orderDate = [NSDate date];
    self.totalPrice = 0.0f;
    self.paid = NO;
    self.favourite = NO;
    self.totalGP = 0.0f;;
    self.totalPOD = 0;
    self.transactionId = @"";
    self.transactionDate = [NSDate date];
    self.transactionDetail = @"";
    self.trackingNumber = @"";
    self.details = nil;
    self.details = [[NSMutableArray alloc] init];
}

- (void)copyValue:(Orders*)obj {
    
    if (obj == nil) {
        return;
    }
    
    _objectId = obj.objectId;
    _userId = obj.userId;
    _orderNo = obj.orderNo;
    _orderName = obj.orderName;
    _orderDate = obj.orderDate;
    _totalPrice = obj.totalPrice;
    _paid = obj.paid;
    
    _favourite = obj.favourite;
    
    _transactionId = obj.transactionId;
    _transactionDate = obj.transactionDate;
    _transactionDetail = obj.transactionDetail;
    _trackingNumber = obj.trackingNumber;
    
    _details = obj.details;
}

- (NSString*)displayedOrderName {
    return _orderName;
}

- (NSString*)displayedOrderNo {
    return [NSString stringWithFormat:@"Order: %@", _orderNo];
}

- (NSString*)displayedPrice {
    return [OrderManager getDisplayedPrice:_totalPrice];
}

- (NSString*)displayedTotalWithOutPOD {
    return [NSString stringWithFormat:@"%@, %@", [OrderManager getDisplayedPrice:_totalPrice], [OrderManager getDisplayedGP:_totalGP]];
}

- (NSString*)displayedTotal {
    return [NSString stringWithFormat:@"%@, %@, %@", [OrderManager getDisplayedPrice:_totalPrice], [OrderManager getDisplayedPOD:_totalPOD], [OrderManager getDisplayedGP:_totalGP]];
}

- (NSString*)displayedOrderDate {
    
    NSString * date = @"";
    
    NSDateFormatter* formatter = nil;
    if (formatter == nil)
    {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
    }
    
    date = [formatter stringFromDate:_orderDate];
    return [NSString stringWithFormat:@"(%@)", date];
}

@end
