//
//  OrderDetail.h
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Products.h"

@interface OrderDetails : NSObject

@property (nonatomic, strong)   NSString * objectId;
@property (nonatomic, assign)   NSInteger count;
@property (nonatomic, strong)   Products * product;

- (void)initOrderDetail;
- (void)initDetailValue;
- (void)copyValue:(OrderDetails*)obj;

@end
