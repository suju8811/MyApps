//
//  ProductCriteria.m
//  Kalahari
//
//  Created by LMAN on 4/11/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "ProductCriteria.h"

@implementation ProductCriteria

- (id)init {
    
    self = [super init];
    _kind = PRODUCT_KIND_NORM;
    _categoryID = -1;
    _searchStr = @"";
    return self;
}

@end
