//
//  NewsLetter.h
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Newsletters : NSObject

@property (nonatomic, strong)   NSString * objectId;
@property (nonatomic, strong)   NSString * title;
@property (nonatomic, assign)   BOOL show;
@property (nonatomic, strong)   NSString * image;
@property (nonatomic, strong)   NSString * shortDescription;
@property (nonatomic, strong)   NSString * longDescription;
@property (nonatomic, strong)   NSDate * created;
@property (nonatomic, strong)   NSDate * updated;

- (void)initNewsLetter;

@end
