//
//  OrderManager.m
//  Kalahari
//
//  Created by LMAN on 4/14/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "OrderManager.h"

@implementation OrderManager

+ (NSString*)generateOrderNo {
    
    NSMutableString * data = [NSMutableString stringWithString:@""];
    NSArray * arrCharset = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"0"];
    
    for (int i = 0; i < 6; i++) {
        
        [data appendString:arrCharset[arc4random_uniform((u_int32_t)arrCharset.count)]];
    }
    
    return data;
}

+ (NSString*)getCurrentOrderDate {
    
    NSDateFormatter* formatter = nil;
    NSDate * curDate = [NSDate date];
    if (formatter == nil) {
        
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    }
    
    NSString * dateStr = [formatter stringFromDate:curDate];
    return dateStr;
}

+ (NSDate*)getCurrentOrderDate:(NSString*)value {
    
    if (value.length == 0) {
        return nil;
    }
    
    NSDateFormatter* formatter = nil;
    if (formatter == nil) {
        
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    }
    
    NSDate * currentOrderDate = [formatter dateFromString:value];
    return currentOrderDate;
}

+ (NSString*)getDisplayedPrice:(CGFloat)price {
    return [NSString stringWithFormat:@"%@ %.2f", @"R", price];
}

+ (NSString*)getDisplayedGP:(CGFloat)gp {
    return [NSString stringWithFormat:@"%@ %.2f %@", @"R", gp, @"GP"];
}

+ (NSString*)getDisplayedPOD:(NSInteger)pod {
    return [NSString stringWithFormat:@"%d %@", (int)pod, @"POD"];
}

+ (NSString*)getDisplayedTag1:(NSString*)tag1 tag2:(NSString*)tag2 tag3:(NSString*)tag3 tag4:(NSString*)tag4 tag5:(NSString*)tag5 {
    
    if (tag1.length == 0 && tag2.length == 0 && tag3.length == 0 && tag4.length == 0 && tag5.length == 0) {
        return @"";
    }
    
    NSString * result = tag1;
    result = [self addTag:result tag:tag2];
    result = [self addTag:result tag:tag3];
    result = [self addTag:result tag:tag4];
    result = [self addTag:result tag:tag5];
    
    return result;
}

+ (NSString*)addTag:(NSString*)result tag:(NSString*)tag {
    
    if (tag.length == 0) {
        return result;
    }
    
    NSMutableString * ret = [NSMutableString stringWithString:result];
    
    if (ret.length > 0) {
        [ret appendString:@", "];
    }
    
    [ret appendString:tag];
    
    return ret;
}

@end
