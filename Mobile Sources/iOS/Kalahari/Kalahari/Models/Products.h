//
//  Product.h
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Products : NSObject

@property (nonatomic, strong)   NSString * objectId;
@property (nonatomic, assign)   NSInteger categoryId;
@property (nonatomic, strong)   NSString * code;
@property (nonatomic, strong)   NSString * name;
@property (nonatomic, strong)   NSString * image;
@property (nonatomic, strong)   NSString * specialImage;
@property (nonatomic, strong)   NSString * size;
@property (nonatomic, assign)   CGFloat price;
@property (nonatomic, assign)   CGFloat retailPrice;
@property (nonatomic, strong)   NSString * shortDescription;
@property (nonatomic, strong)   NSString * longDescription;
@property (nonatomic, assign)   BOOL special;
@property (nonatomic, assign)   NSInteger pod;
@property (nonatomic, strong)   NSString * swopDescription;
@property (nonatomic, strong)   NSString * tag1;
@property (nonatomic, strong)   NSString * tag2;
@property (nonatomic, strong)   NSString * tag3;
@property (nonatomic, strong)   NSString * tag4;
@property (nonatomic, strong)   NSString * tag5;

- (void)initProduct;
- (NSString*)displayedTag;
- (NSString*)displayedAll;
- (void)copyValues:(Products*)obj;
- (CGFloat)calculateGP;

@end
