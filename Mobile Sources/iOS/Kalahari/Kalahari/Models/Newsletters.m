//
//  NewsLetter.m
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "Newsletters.h"

@implementation Newsletters

- (id)init {
    
    self = [super init];
    
    [self initNewsLetter];
    
    return self;
}

- (void)initNewsLetter {
    
    self.objectId = @"";
    self.title = @"";
    self.show = NO;
    self.image = @"";
    self.shortDescription = @"";
    self.longDescription = @"";
    self.created = nil;
    self.updated = nil;
}

@end
