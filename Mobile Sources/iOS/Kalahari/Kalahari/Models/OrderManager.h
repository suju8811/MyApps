//
//  OrderManager.h
//  Kalahari
//
//  Created by LMAN on 4/14/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Global.h"

@interface OrderManager : NSObject

+ (NSString*)generateOrderNo;
+ (NSString*)getCurrentOrderDate;
+ (NSDate*)getCurrentOrderDate:(NSString*)value;
+ (NSString*)getDisplayedPrice:(CGFloat)price;
+ (NSString*)getDisplayedGP:(CGFloat)gp;
+ (NSString*)getDisplayedPOD:(NSInteger)pod;
+ (NSString*)getDisplayedTag1:(NSString*)tag1 tag2:(NSString*)tag2 tag3:(NSString*)tag3 tag4:(NSString*)tag4 tag5:(NSString*)tag5;
+ (NSString*)addTag:(NSString*)result tag:(NSString*)tag;

@end
