//
//  Menu.h
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Menu : NSObject

@property (nonatomic, assign)   NSInteger action;
@property (nonatomic, assign)   NSInteger icon;
@property (nonatomic, strong)   NSString * name;
@property (nonatomic, assign)   BOOL isEnd;

- (void)initMenu;

@end
