//
//  OrderCriteria.m
//  Kalahari
//
//  Created by LMAN on 4/14/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "OrderCriteria.h"

@implementation OrderCriteria

- (id)init {
    
    self = [super init];
    
    _kind = ORDER_KIND_NONE;
    _searchStr = @"";
    _orderNo = @"";
    _product = nil;
    _count = 0;
    
    return self;
}

@end
