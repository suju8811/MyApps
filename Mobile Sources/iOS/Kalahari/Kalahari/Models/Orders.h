//
//  Order.h
//  Kalahari
//
//  Created by LMAN on 3/30/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DELIVERY_FEE_LIMIT      4000.0f
#define DELIVERY_FEE            100.0f

@interface Orders : NSObject

@property (nonatomic, strong) NSString * objectId;
@property (nonatomic, strong) NSString * userId;
@property (nonatomic, strong) NSString * salonName;
@property (nonatomic, strong) NSString * orderNo;
@property (nonatomic, strong) NSString * orderName;
@property (nonatomic, strong) NSDate * orderDate;
@property (nonatomic, assign) CGFloat totalPrice;
@property (nonatomic, assign) BOOL paid;
@property (nonatomic, assign) BOOL favourite;
@property (nonatomic, assign) CGFloat totalGP;
@property (nonatomic, assign) NSInteger totalPOD;
@property (nonatomic, strong) NSString * transactionId;
@property (nonatomic, strong) NSDate * transactionDate;
@property (nonatomic, strong) NSString * transactionDetail;
@property (nonatomic, strong) NSString * trackingNumber;
@property (nonatomic, strong) NSMutableArray * details; // Array of order details

- (void)initOrder;

- (NSString*)displayedOrderName;
- (NSString*)displayedOrderNo;
- (NSString*)displayedPrice;
- (NSString*)displayedTotal;
- (NSString*)displayedTotalWithOutPOD;
- (NSString*)displayedOrderDate;

- (void)copyValue:(Orders*)obj;

@end
