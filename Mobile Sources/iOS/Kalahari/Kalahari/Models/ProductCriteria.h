//
//  ProductCriteria.h
//  Kalahari
//
//  Created by LMAN on 4/11/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ProductKind) {
    
    PRODUCT_KIND_NORM               = -1,
    PRODUCT_KIND_BY_CATEGORY        = 1,
    PRODUCT_KIND_BY_SPECIAL         = 2,
    PRODUCT_KIND_BY_SEARCH          = 3,
};

@interface ProductCriteria : NSObject

@property (nonatomic, assign) ProductKind kind;
@property (nonatomic, assign) NSInteger categoryID;
@property (nonatomic, strong) NSString * searchStr;

@end
