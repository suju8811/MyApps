//
//  OrderTrackCriteria.h
//  Kalahari
//
//  Created by LMAN on 4/15/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderTrackCriteria : NSObject

@property (nonatomic, strong) NSString * orderNo;
@property (nonatomic, strong) NSString * trackingNumber;

@end
