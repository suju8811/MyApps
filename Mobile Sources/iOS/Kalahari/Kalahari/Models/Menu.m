//
//  Menu.m
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "Menu.h"

@implementation Menu

- (id)init {
    
    self = [super init];
    
    [self initMenu];
    
    return self;
}

- (void)initMenu {
    
    self.action = 0;
    self.icon = 0;
    self.name = @"";
    self.isEnd = NO;
}

@end
