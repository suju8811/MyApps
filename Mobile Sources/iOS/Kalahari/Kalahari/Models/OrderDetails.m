//
//  OrderDetail.m
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "OrderDetails.h"

@implementation OrderDetails

- (id)init {
    
    self = [super init];
    
    [self initOrderDetail];
    
    return self;
}

- (void)initOrderDetail {
    
    self.objectId = nil;
    self.count = 0;
    self.product = nil;
    self.product = [[Products alloc] init];
}

- (void)initDetailValue {
    
    self.count = 0;
    self.product = nil;
}

- (void)copyValue:(OrderDetails*)obj {
    
    if (obj == nil) {
        return;
    }
    
    _objectId = obj.objectId;
    _count = obj.count;
    _product = obj.product;
}

@end
