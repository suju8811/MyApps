//
//  HomeVC.m
//  Kalahari
//
//  Created by LMAN on 3/24/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "HomeVC.h"
#import "KAlert.h"
#import "Products.h"
#import "Categorys.h"
#import "Orders.h"
#import "UserManager.h"
#import "ProductCriteria.h"
#import "OrderCriteria.h"

#import "SpecialProductVC.h"
#import "CategoryVC.h"
#import "FavoriteOrderVC.h"
#import "ProductVC.h"
#import "MenuVC.h"
#import "OrderVC.h"
#import "OrderInfoVC.h"
#import "ProductDetailVC.h"
#import "BackendlessCachePolicy.h"

@interface HomeVC () <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

/**
 *  Tap gesture recognizer for Keyboard
 */

@property (nonatomic, strong) UITapGestureRecognizer * tap;

/**
 *  Arrays for special products, categories and favorites
 */

@property (nonatomic, strong) NSMutableArray * arrSpecials;
@property (nonatomic, strong) NSMutableArray * arrCategorys;
@property (nonatomic, strong) NSMutableArray * arrFavorites;

/**
 *  Subviews for special products
 */

@property (nonatomic, strong) UIPageViewController * pageViewController;
@property (nonatomic, assign) NSInteger pageIndex;

/**
 *  Subviews for categories
 */

@property (nonatomic, strong) CategoryVC * categoryVC;

/**
 *  Subviews for favorite orders
 */

@property (nonatomic, strong) FavoriteOrderVC * favoriteOrderVC;

@end

@implementation HomeVC

#pragma mark - 
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];

    KLLog(@"View Did Load");
    KLLog(@"View Did Load:%d times", 4);
    
    NSLog(@"User Properties : %@", [backendless.userService.currentUser getProperty:@"deliveryFee"]);
    
    /**
     *  Initialize Page View Controller for special products
     */
    
    _pageIndex = 0;
    _pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    _pageViewController.dataSource = self;
    _pageViewController.delegate = self;
    
    [self addChildViewController:_pageViewController];
    [_productPageView addSubview:_pageViewController.view];
    [_pageViewController didMoveToParentViewController:self];
    
    /**
     *  Initialize subviews for categories
     */
    
    _categoryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SID_CATEGORY"];
    [self addChildViewController:_categoryVC];
    [_viewCategory addSubview:_categoryVC.view];
    
    /**
     *  Initialize subviews for favorite order
     */
    
    _favoriteOrderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SID_FAVORITE_ORDER"];
    [self addChildViewController:_favoriteOrderVC];
    [_viewFavoriteOrder addSubview:_favoriteOrderVC.view];
    
    /**
     *  Initialize tap gesture for keyboard
     */
    
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    _tap.enabled = NO;
    
    [self.view addGestureRecognizer:_tap];
    
    /**
     *  Initialize data array
     */
    
    _arrSpecials = [[NSMutableArray alloc] init];
    _arrCategorys = [[NSMutableArray alloc] init];
    _arrFavorites = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];

    /**
     *  Reload dashboard data
     */
    
    if (!gKAppSetting.bDashboardLoaded) {
        
        /*
         *  Hide scroll view
         */
        _scrollView.hidden = YES;
        
        /*
         *  Initialize data array
         */
        _arrSpecials = [[NSMutableArray alloc] init];
        _arrCategorys = [[NSMutableArray alloc] init];
        _arrFavorites = [[NSMutableArray alloc] init];
        
        /*
         *  Load data from backendless
         */
        [self performSelector:@selector(loadSpecials) withObject:nil afterDelay:.2f];
    }
    
    /**
     * Hide tab bar
     */
    
    if (![UserManager isOwner]) {
        self.tabBarController.tabBar.hidden = YES;
    }
    
    /*
     *  Add observer for keyboard notification
     */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    /*
     *  Remove observer for keyboard notification
     */
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    /*
     *  Hide keyboard
     */
    [_txtSearch resignFirstResponder];
}

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    /*
     *  Set sub view appearance
     */
    _btnAddToCart.layer.cornerRadius = CGRectGetHeight(_btnAddToCart.frame) / 2;
    _btnAddToCart.layer.borderColor = [COLOR_THEME CGColor];
    _btnAddToCart.layer.borderWidth = 1.f;
    
    /*
     *  Reset special products page view frame
     */
    _pageViewController.view.frame = CGRectMake(0, 0, CGRectGetWidth(_productPageView.frame), CGRectGetHeight(_productPageView.frame));
    [_pageViewController.view layoutIfNeeded];
    _productPageControl.currentPage = 0;
    
    /*
     *  Reset categories subview frame
     */
    _categoryVC.view.frame = CGRectMake(0, 0, CGRectGetWidth(_viewCategory.frame), CGRectGetHeight(_viewCategory.frame));
    
    /*
     *  Reset favorite orders subview frame
     */
    _favoriteOrderVC.view.frame = CGRectMake(0, 0, CGRectGetWidth(_viewFavoriteOrder.frame), CGRectGetHeight(_viewFavoriteOrder.frame));
}

#pragma mark -
#pragma mark - Search String Validation

- (BOOL)validateSearchString {
    
    if ([_txtSearch.text isEqualToString:@""]) {
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Please input search criteria" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

#pragma mark -
#pragma mark - Storyboard prepare segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"SG_PRODUCT"]) {
        
        ProductVC *vc = [segue destinationViewController];
        vc.criteria = (ProductCriteria*)sender;
    } else if ([[segue identifier] isEqualToString:@"SG_MENU"]) {
        
        MenuVC * vc = [segue destinationViewController];
        vc.categoryId = [(NSNumber*)sender integerValue];
    } else if ([[segue identifier] isEqualToString:@"SG_ORDER"]) {
        
        OrderVC * vc = [segue destinationViewController];
        vc.criteria = (OrderCriteria*)sender;
    } else if ([[segue identifier] isEqualToString:@"SG_ORDER_INFO_FROM_HOME"]) {
        
        OrderInfoVC * orderInfoVC = [segue destinationViewController];
        orderInfoVC.criteria = (OrderCriteria*)sender;
    } else if ([[segue identifier] isEqualToString:@"SG_PRODUCT_DETAIL"]) {
            
        ProductDetailVC *vc = [segue destinationViewController];
        NSIndexPath * indexPath = (NSIndexPath*)sender;
        vc.product = [_arrSpecials objectAtIndex:indexPath.row];
        vc.kind = PRODUCT_DETAIL_KIND_ADD;
    }
}

#pragma mark - 
#pragma mark - Actions

- (IBAction)actionSearch:(id)sender {
    
    if (![self validateSearchString]) {
        return;
    }

    ProductCriteria * criteria = [[ProductCriteria alloc] init];
    criteria.kind = PRODUCT_KIND_BY_SEARCH;
    criteria.searchStr = _txtSearch.text;
    
    [self performSegueWithIdentifier:@"SG_PRODUCT" sender:criteria];
}

- (IBAction)actionSeeAllProducts:(id)sender {
    
    ProductCriteria * criteria = [[ProductCriteria alloc] init];
    criteria.kind = PRODUCT_KIND_BY_SPECIAL;
    
    [self performSegueWithIdentifier:@"SG_PRODUCT" sender:criteria];
}

- (IBAction)actionAddToCart:(id)sender {
    
    OrderCriteria * orderCriteria = [[OrderCriteria alloc] init];
    orderCriteria.kind = ORDER_KIND_CURRENT;
    orderCriteria.orderNo = gKAppSetting.currentOrderNo;
    orderCriteria.product = [_arrSpecials objectAtIndex:_pageIndex];
    [self performSegueWithIdentifier:@"SG_ORDER_INFO_FROM_HOME" sender:orderCriteria];
}

- (IBAction)actionSeeAllMenu:(id)sender {
    
    NSNumber * categoryID = [NSNumber numberWithInteger:-1];
    [self performSegueWithIdentifier:@"SG_MENU" sender:categoryID];
}

- (IBAction)actionSeeAllFavoriteOrder:(id)sender {
    
    OrderCriteria * criteria = [[OrderCriteria alloc] init];
    criteria.kind = ORDER_KIND_FAVORITE;;
    [self performSegueWithIdentifier:@"SG_ORDER" sender:criteria];
}

#pragma mark -
#pragma mark - Backendless Request

- (void)loadSpecials {
    
    if (![KCommon checkBLLogin:self]) {
        return;
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.queryOptions.offset = [NSNumber numberWithInteger:0];
    query.queryOptions.pageSize = [NSNumber numberWithInteger:5];
    query.whereClause = @"special=1 and specialImage<>''";
    query.queryOptions.sortBy = @[@"created DESC"];
    
    [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    
    BackendlessCachePolicy * cachePolicy = [[BackendlessCachePolicy alloc] init];
    [cachePolicy cachePolicy:BackendlessCachePolicyIgnoreCache];
    [backendless setCachePolicy:cachePolicy];
    
    [[backendless.persistenceService of:[Products class]] find:query response:^(BackendlessCollection * collection) {
        
        KLLog(@"Load Special Success");
        KLLog(@"Collection data count:%d", (int)collection.data.count);
        KLLog(@"Collection data Total Count:%d", (int)[collection.totalObjects integerValue]);
        
        if (collection != nil) {
            
            for (NSInteger i = 0; i < collection.data.count; i++)  {
                
                [_arrSpecials addObject:[collection.data objectAtIndex:i]];
                if (_arrSpecials.count >= 5) {
                    break;
                }
            }
        }
        
        [self performSelector:@selector(loadCategories) withObject:nil afterDelay:0.1];
        
    } error:^(Fault * fault) {
    
        [KAlert hideLoading:self.tabBarController.view];
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to load specials" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"Failed to load special : %@", fault.description);
    }];
}

- (void)loadCategories {
    
    if (![KCommon checkBLLogin:self]) {
        return;
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = @"parentId=0";
    query.queryOptions.sortBy = @[@"categoryId ASC"];
    
    [backendless clearAllCache];
    [[backendless.persistenceService of:[Categorys class]] find:query response:^(BackendlessCollection * collection) {
        
        KLLog(@"Load Categories Success");
        KLLog(@"Collection data count:%d", (int)collection.data.count);
        KLLog(@"Collection data Total Count:%d", (int)[collection.totalObjects integerValue]);
        
        if (collection != nil) {
            
            [_arrCategorys addObjectsFromArray:collection.data];
        }
        
        [self performSelector:@selector(loadFavorites) withObject:nil afterDelay:0.1];
        
    } error:^(Fault * fault) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to load categories" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"Failed to load categories : %@", fault.description);
    }];
}

- (void)loadFavorites {
    
    if (![KCommon checkBLLogin:self]) {
        return;
    }
    
    NSString * userID = [UserManager getUserID];
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.queryOptions.offset = [NSNumber numberWithInteger:0];
    query.queryOptions.pageSize = [NSNumber numberWithInteger:5];
    query.whereClause = [NSString stringWithFormat:@"userId='%@' and favourite=1", userID];
    query.queryOptions.sortBy = @[@"orderDate DESC"];
    
    [backendless clearAllCache];
    [[backendless.persistenceService of:[Orders class]] find:query response:^(BackendlessCollection * collection) {

        KLLog(@"Load Favorites Success");
        KLLog(@"Collection data count:%d", (int)collection.data.count);
        KLLog(@"Collection data Total Count:%d", (int)[collection.totalObjects integerValue]);
        
        if (collection != nil) {
            [_arrFavorites addObjectsFromArray:collection.data];
        }
        
        [KAlert hideLoading:self.tabBarController.view];
        [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.1];
        
    } error:^(Fault * fault) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to load special" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"Failed to load special : %@", fault.description);
    }];
    
}

#pragma mark - 
#pragma mark - Refresh Subviews

- (void)refreshView {
    
    /*
     *  Set dashboard loaded variable
     */
    gKAppSetting.bDashboardLoaded = YES;
    
    /*
     *  Set special products pager
     */
    if (_arrSpecials.count > 0 && [UserManager isOwner]) {
        
        SpecialProductVC *initialViewController = [self viewControllerAtIndex:0];
        NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
        [_pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        _consTopSpecial.constant = 0;
        _productView.hidden = NO;
        
        KLLog(@"Top Constant : %f", _consTopSpecial.constant);
    } else {
        _consTopSpecial.constant = - CGRectGetHeight(_productView.frame);
        _productView.hidden = YES;
        
        KLLog(@"Top Constant : %f", _consTopSpecial.constant);
    }
    
    /*
     *  Set category view data
     */
    _categoryVC.arrCategorys = _arrCategorys;
    [_categoryVC.clvCategory reloadData];
    
    /*
     *  Set favorite order view data
     */
    _favoriteOrderVC.arrFavorites = _arrFavorites;
    [_favoriteOrderVC.clvOrder reloadData];
    
    /*
     *  Show view after load data
     */
    _scrollView.hidden = NO;
    
    if (![UserManager isOwner]) {
        _favoriteView.hidden = YES;
    }
}

#pragma mark -
#pragma mark - UIPageViewController Datasource and Delegate

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(SpecialProductVC *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    index--;
    
    SpecialProductVC * retVC = [self viewControllerAtIndex:index];
    [pageViewController addChildViewController:retVC];
    return retVC;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(SpecialProductVC *)viewController index];
    
    index++;
    
    if (index == _arrSpecials.count) {
        return nil;
    }
    
    SpecialProductVC * retVC = [self viewControllerAtIndex:index];
    [pageViewController addChildViewController:retVC];
    return retVC;
}

- (SpecialProductVC *)viewControllerAtIndex:(NSUInteger)index {
    
    SpecialProductVC * productVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SID_SPECIAL_PRODUCT"];
    
    productVC.index = index;
    
    if (_arrSpecials.count == 0) {
        return productVC;
    }
    
    Products * product = [_arrSpecials objectAtIndex:index];
    productVC.product = product;
    
    [productVC.view layoutIfNeeded];
    
    return productVC;
}

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers NS_AVAILABLE_IOS(6_0) {
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
    
    SpecialProductVC *currentPageVC = (SpecialProductVC*)[pageViewController.viewControllers objectAtIndex:0];
    
    _pageIndex = currentPageVC.index;
    _productPageControl.currentPage = _pageIndex;
}

#pragma mark - 
#pragma mark - UIKeyboard Notficiation

- (void)hideKeyboard {
 
    [_txtSearch resignFirstResponder];
}

- (void) resizeForKeyboard:(NSNotification*)aNotification {
    
    BOOL up = aNotification.name == UIKeyboardWillShowNotification;
    
    NSDictionary * userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationOptions animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    if (up) {
        
        
        [UIView animateWithDuration:animationDuration animations:^{
        } completion:^(BOOL finished) {
            _tap.enabled = YES;
        }];
    }
    else {
        _tap.enabled = NO;
        [UIView animateWithDuration:animationDuration animations:^{
        } completion:^(BOOL finished) {
        }];
    }
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
