//
//  CategoryVC.m
//  Kalahari
//
//  Created by LMAN on 4/8/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "CategoryVC.h"
#import "CategoryCell.h"
#import "Categorys.h"
#import "UIImageView+AFNetworking.h"
#import "ProductCriteria.h"

#define cellRatio               (170.f / 176.f)

@interface CategoryVC ()

@end

@implementation CategoryVC

#pragma mark -
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    _clvCategory.backgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 
#pragma mark - Actions

- (void)tapCategory:(UIGestureRecognizer*)recognizer {
    
    /**
     *  Get index path of tapped category
     */
    
    CGPoint buttonPosition = [recognizer locationInView:_clvCategory];
    NSIndexPath *indexPath = [_clvCategory indexPathForItemAtPoint:buttonPosition];
    
    /**
     *  Go to news letter
     */
    
    if (indexPath.row == _arrCategorys
        .count) {
        
        NSNumber * categoryID = [NSNumber numberWithInteger:90];
        [self.parentViewController performSegueWithIdentifier:@"SG_MENU" sender:categoryID];
        return;
    }
    
    /**
     *  Go to contact us
     */
    
    if (indexPath.row == _arrCategorys.count + 1) {
        
        [self.parentViewController performSegueWithIdentifier:@"SG_NEWS_LETTER" sender:nil];
        return;
    }
    
    if (indexPath.row == _arrCategorys.count + 2) {
        
        [self.parentViewController performSegueWithIdentifier:@"SG_CONTACT_US" sender:nil];
        return;
    }
    
    /**
     *  Go to category list view
     */
    
    Categorys * category = [_arrCategorys objectAtIndex:indexPath.row];
    
    if (category.hasChild) {
        
        NSNumber * categoryId = [NSNumber numberWithInteger:category.categoryId];
        [self.parentViewController performSegueWithIdentifier:@"SG_MENU" sender:categoryId];
    } else {
        
        ProductCriteria * criteria = [[ProductCriteria alloc] init];
        criteria.kind = PRODUCT_KIND_BY_CATEGORY;
        criteria.categoryID = category.categoryId;
        
        [self.parentViewController performSegueWithIdentifier:@"SG_PRODUCT" sender:criteria];
    }
}

#pragma mark -
#pragma mark - UICollectionView Delegate and Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _arrCategorys.count + 3;
}

- (CategoryCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CategoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"category_cell" forIndexPath:indexPath];
    
    UITapGestureRecognizer * tapCategory = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCategory:)];
    cell.imgCategory.userInteractionEnabled = YES;
    [cell.imgCategory addGestureRecognizer:tapCategory];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat cellHeight = CGRectGetHeight(_clvCategory.frame);
    CGFloat cellWidth = cellHeight * cellRatio;
    return CGSizeMake(cellWidth, cellHeight);
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(CategoryCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < _arrCategorys.count) {
        
        Categorys * category = [_arrCategorys objectAtIndex:indexPath.row];
        cell.imgCategory.image = nil;
        [cell.imgCategory setImageWithURL:[NSURL URLWithString:category.image] placeholderImage:nil];
        cell.lblTitle.text = category.name;
    } else {
        if (indexPath.row == _arrCategorys.count) {
            
            cell.imgCategory.image = [UIImage imageNamed:@"menu_treatment"];
            cell.lblTitle.text = @"Treatments";
        } else if (indexPath.row == _arrCategorys.count + 1) {
            
            cell.imgCategory.image = [UIImage imageNamed:@"menu_newsletter"];
            cell.lblTitle.text = @"Carina's advice";
        } else if (indexPath.row == _arrCategorys.count + 2) {
            
            cell.imgCategory.image = [UIImage imageNamed:@"menu_contactus"];
            cell.lblTitle.text = @"Contact us";
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *) collectionView layout:(UICollectionViewLayout *) collectionViewLayout insetForSectionAtIndex:(NSInteger) section {
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *) collectionView layout:(UICollectionViewLayout *) collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
    
    return 0.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0f;
}

@end
