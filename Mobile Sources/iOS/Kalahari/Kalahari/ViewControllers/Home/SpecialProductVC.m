//
//  SpecialProductVC.m
//  Kalahari
//
//  Created by LMAN on 3/30/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "SpecialProductVC.h"
#import "UIImageView+AFNetworking.h"

@implementation SpecialProductVC

#pragma mark - 
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {

    [super viewDidLoad];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSpecialProduct:)];
    _imgProduct.userInteractionEnabled = YES;
    [_imgProduct addGestureRecognizer:tap];
}

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    NSURL * imageURL = [NSURL URLWithString:_product.specialImage];
    [_imgProduct setImageWithURL:imageURL];
}

#pragma mark - 
#pragma mark - Actions

- (void)tapSpecialProduct:(UIGestureRecognizer*)recognizer {
    
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:_index inSection:0];
    [self.parentViewController.parentViewController performSegueWithIdentifier:@"SG_PRODUCT_DETAIL" sender:indexPath];
}

@end
