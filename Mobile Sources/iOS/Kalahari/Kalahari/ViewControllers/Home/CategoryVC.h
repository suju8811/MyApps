//
//  CategoryVC.h
//  Kalahari
//
//  Created by LMAN on 4/8/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryVC : UIViewController

/*
 *  Array for categories
 */
@property (nonatomic, strong) NSMutableArray * arrCategorys;

/*
 *  Collection view to show categories
 */
@property (weak, nonatomic) IBOutlet UICollectionView *clvCategory;

@end
