//
//  SpecialProductVC.h
//  Kalahari
//
//  Created by LMAN on 3/30/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Products.h"

@interface SpecialProductVC : UIViewController

@property (assign, nonatomic) NSInteger index;
@property (weak, nonatomic) IBOutlet UIImageView *imgProduct;
@property (nonatomic, strong) Products * product;

@end
