//
//  FavoriteOrderVC.m
//  Kalahari
//
//  Created by LMAN on 4/8/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "FavoriteOrderVC.h"
#import "FavoriteOrderCell.h"
#import "Orders.h"
#import "OrderInfoVC.h"

@interface FavoriteOrderVC ()

@end

@implementation FavoriteOrderVC

#pragma mark - 
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    _clvOrder.backgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - UICollectionView Delegate and Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _arrFavorites.count;
}

- (FavoriteOrderCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FavoriteOrderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"favorite_order_cell" forIndexPath:indexPath];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat cellHeight = CGRectGetHeight(_clvOrder.frame);
    CGFloat cellWidth = cellHeight;
    return CGSizeMake(cellWidth, cellHeight);
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(FavoriteOrderCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Orders * order = [_arrFavorites objectAtIndex:indexPath.row];
    cell.imgOrder.image = [UIImage imageNamed:@"order"];
    
    NSString * orderName = order.orderName;
    if (orderName == nil || orderName.length == 0) {
        orderName = @"New Order";
    }
    
    cell.lblTitle.text = orderName;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Orders * order = [_arrFavorites objectAtIndex:indexPath.row];
    
    OrderCriteria * criteriaInfo = [[OrderCriteria alloc] init];
    
    if (order.paid) {
        
        if (order.trackingNumber == nil || order.trackingNumber.length == 0) {
            criteriaInfo.kind = ORDER_KIND_PENDING;
        }
        else {
            criteriaInfo.kind = ORDER_KIND_TRACK;
        }
    } else {
        criteriaInfo.kind = ORDER_KIND_CURRENT;
    }
    
    criteriaInfo.orderNo = order.orderNo;
    [self.parentViewController performSegueWithIdentifier:@"SG_ORDER_INFO_FROM_HOME" sender:criteriaInfo];
}

- (UIEdgeInsets)collectionView:(UICollectionView *) collectionView layout:(UICollectionViewLayout *) collectionViewLayout insetForSectionAtIndex:(NSInteger) section {
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *) collectionView layout:(UICollectionViewLayout *) collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
    
    return 0.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0f;
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
