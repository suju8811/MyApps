//
//  FavoriteOrderVC.h
//  Kalahari
//
//  Created by LMAN on 4/8/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteOrderVC : UIViewController

/*
 *  Array collection for favorite orders
 */
@property (nonatomic, strong) NSMutableArray * arrFavorites;

/*
 *  Collection view to show favorite orders
 */
@property (weak, nonatomic) IBOutlet UICollectionView *clvOrder;

@end
