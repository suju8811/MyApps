//
//  HomeVC.h
//  Kalahari
//
//  Created by LMAN on 3/24/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeVC : UIViewController

/*
 *  Top bar
 */
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;

/*
 *  Whole scroll view
 */
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

/*
 *  Views for special product
 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTopSpecial;
@property (weak, nonatomic) IBOutlet UIView *productView;
@property (weak, nonatomic) IBOutlet UIView *productPageView;
@property (weak, nonatomic) IBOutlet UIPageControl *productPageControl;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToCart;

/*
 *  View for category
 */
@property (weak, nonatomic) IBOutlet UIView *viewCategory;

/*
 *  View for favorite order
 */
@property (weak, nonatomic) IBOutlet UIView *favoriteView;
@property (weak, nonatomic) IBOutlet UIView *viewFavoriteOrder;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consFavoriteBottom;

@end
