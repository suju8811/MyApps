//
//  ProductPayCell.h
//  Kalahari
//
//  Created by LMAN on 4/14/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductPayCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *txtAmount;
@property (weak, nonatomic) IBOutlet UIButton *btnLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnRight;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

@end
