//
//  ProductPaidCell.h
//  Kalahari
//
//  Created by LMAN on 4/14/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductPaidCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblBill;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

@end
