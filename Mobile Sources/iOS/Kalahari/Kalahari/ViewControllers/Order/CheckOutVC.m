//
//  CheckOutVC.m
//  Kalahari
//
//  Created by LMAN on 4/15/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "CheckOutVC.h"
#import "OrderDetails.h"
#import "OrderManager.h"
#import "ProductPaidCell.h"
#import "ProductDetailVC.h"
#import "PaymentVC.h"
#import "UserManager.h"
#import "OrderVC.h"

@interface CheckOutVC ()

@property (nonatomic, strong) NSMutableArray * orderProducts;

@end

@implementation CheckOutVC

#pragma mark - 
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];

    _orderProducts = [[NSMutableArray alloc] init];
    
    _order = [[Orders alloc] init];
    _order.orderNo = _orderNo;
    
    [self getOrderNo:_order.orderNo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    /*
     *  Compose layout
     */
    
    _viewImgOrder.layer.borderColor = [COLOR_THEME CGColor];
    _viewImgOrder.layer.borderWidth = 1.f;
    _viewImgOrder.layer.cornerRadius = CGRectGetHeight(_viewImgOrder.frame) / 2;
    
    _btnCheckOut.layer.borderColor = [COLOR_THEME CGColor];
    _btnCheckOut.layer.borderWidth = 1.f;
    _btnCheckOut.layer.cornerRadius = CGRectGetHeight(_btnCheckOut.frame) / 2;
    
    _btnCancel.layer.borderColor = [COLOR_THEME CGColor];
    _btnCancel.layer.borderWidth = 1.f;
    _btnCancel.layer.cornerRadius = CGRectGetHeight(_btnCancel.frame) / 2;
}

#pragma mark -
#pragma mark - Storyboard prepare segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"SG_PRODUCT_DETAIL_FROM_CHECK_OUT"]) {
        
        NSIndexPath * indexPath = (NSIndexPath*)sender;
        
        OrderDetails * detail = [_orderProducts objectAtIndex:indexPath.row];
        
        ProductDetailVC *vc = [segue destinationViewController];
        vc.product = detail.product;
        vc.kind = PRODUCT_DETAIL_KIND_VIEW;
    }
    
    if ([[segue identifier] isEqualToString:@"SG_PAYMENT"]) {
        
        PaymentVC * vc = [segue destinationViewController];
        vc.order = sender;
    }
}

#pragma mark - 
#pragma mark - Backendless request

- (void)onComplete {
    
    
    _lblOrderName.text = [_order displayedOrderName];
    _lblOrderNo.text = [_order displayedOrderNo];
    _lblOrderDate.text = [_order displayedOrderDate];
    _lblPrice.text = [_order displayedTotalWithOutPOD];
    
    [self initProductList];
}

- (void)initProductList {
    
    NSMutableArray * list = _order.details;
    if (list == nil) {
        list = [[NSMutableArray alloc] init];
    }
    _order.details = list;
    
    CGFloat total = 0;
    NSInteger count = 0;
    CGFloat gp = 0;
    NSInteger pod = 0;
    CGFloat delivery_fee = 0;
    
    for (NSInteger i = 0; i < list.count; i++) {
        
        OrderDetails * item = [list objectAtIndex:i];
        if (item.product == nil) {
            continue;
        }
        
        if (item.count == 0) {
            continue;
        }
        
        [_orderProducts addObject:item];
        count ++;
        
        total += item.product.price * item.count;
        gp +=   item.product.price * item.count;
        pod += item.product.pod * item.count;
    }
    
    delivery_fee = [[[backendless.userService login:gKAppSetting.userEmail password:gKAppSetting.userPwd] getProperty:@"deliveryFee"] integerValue];
    
    if (total > DELIVERY_FEE_LIMIT) {
        delivery_fee = 0;
    }
    
    _order.totalPrice = total + delivery_fee;
    _order.totalGP = gp;
    _order.totalPOD = pod;
    
    _lblPrice.text = [_order displayedTotalWithOutPOD];
    
    _consHeightTableView.constant = 44 * count;
    if (count >= 1) {
    
        _lblDeliveryFee.text = [OrderManager getDisplayedPrice:delivery_fee];
        _lblTotalPrice.text = [_order displayedPrice];
    } else {
        
        _consTopTableView.constant = 0;
        _viewTotal.hidden = YES;
        _consHeightTotalView.constant = 0;
    }
    
    [_tblProducts reloadData];
    
    [KAlert hideLoading:self.tabBarController.view];
}

- (void)getOrderNo:(NSString*)orderNo {
    
    [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    
    NSString * userID = [UserManager getUserID];
    
    BackendlessDataQuery * query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"userId='%@' and orderNo='%@' ", userID, orderNo];
    
    [[backendless.persistenceService of:[Orders class]] find:query response:^(BackendlessCollection * collection) {
        
        if (collection != nil) {
            if ([collection.totalObjects integerValue] != 0) {
                
                Orders * order = [collection.data objectAtIndex:0];
                _order = order;
                [self performSelector:@selector(onComplete) withObject:nil afterDelay:.1f];
            } else {
                [KAlert hideLoading:self.tabBarController.view];
            }
        } else {
            [KAlert hideLoading:self.tabBarController.view];
        }
    } error:^(Fault * fault) {
        [KAlert hideLoading:self.tabBarController.view];
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to load data" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"Failed to load data : %@", fault.description);
    }];
}

#pragma mark -
#pragma mark - UITableView Datasource and Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_orderProducts == nil) {
        return 0;
    }
    
    return _orderProducts.count;
}

- (ProductPaidCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductPaidCell * cell = (ProductPaidCell*)[tableView dequeueReusableCellWithIdentifier:@"product_paid_cell"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(ProductPaidCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderDetails * detail = [_orderProducts objectAtIndex:indexPath.row];
    
    cell.lblBill.text = [NSString stringWithFormat:@"%d x %@", (int)detail.count, detail.product.name];
    cell.lblPrice.text = [OrderManager getDisplayedPrice:detail.product.price * detail.count];
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderDetails * detail = [_orderProducts objectAtIndex:indexPath.row];
    Products * product = detail.product;
    
    if (product != nil) {
        
        [self performSegueWithIdentifier:@"SG_PRODUCT_DETAIL_FROM_CHECK_OUT" sender:indexPath];
    }
    
    return indexPath;
}

#pragma mark -
#pragma mark - Actions

- (IBAction)actionBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionCancel:(id)sender {
    
    if (!_order.paid) {
        
        UIAlertController * alert = [KAlert alertWithTitle:kAppName Message:@"Are you sure to delete this order?" LeftButton:@"YES" RightButton:@"NO" leftBlock:^(UIAlertAction *action) {
            
            [self deleteOrder:_order];
        } RightBlock:^(UIAlertAction *action) {
            
        }];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        [self presentViewController:[KAlert notifyAlertWithTitle:kAppName Message:@"Order can not be deleted as it is already paid" ButtonTitle:@"OK"] animated:YES completion:nil];
    }
}

- (void)deleteOrder:(Orders*)order {
    
    [KAlert showLoading:@"Deleting" parent:self.tabBarController.view];
    
    [[backendless.persistenceService of:[Orders class]] remove:order response:^(NSNumber * aLong) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        if (order.favourite) {
            gKAppSetting.bDashboardLoaded = NO;
        }
        
        [self performSelector:@selector(gotoOrderList) withObject:nil afterDelay:.1f];
        
    } error:^(Fault * fault) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to delete order" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"Failed to delete order : %@", fault.description);
    }];
}

- (void)gotoOrderList {

    for (UIViewController * viewController in self.navigationController.viewControllers) {
        
        if ([viewController isKindOfClass:[OrderVC class]]) {
            
            gKAppSetting.bTapChanged = YES;
            [self.navigationController popToViewController:viewController animated:YES];
            return;
        }
    }
}

- (IBAction)actionCheckOut:(id)sender {
    
    [self performSegueWithIdentifier:@"SG_PAYMENT" sender:_order];
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
