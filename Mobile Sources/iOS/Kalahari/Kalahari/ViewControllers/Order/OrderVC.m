//
//  OrderVC.m
//  Kalahari
//
//  Created by LMAN on 3/24/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "OrderVC.h"
#import "OrderCell.h"
#import "UserManager.h"
#import "Orders.h"
#import "OrderInfoVC.h"
#import "SVPullToRefresh.h"

@interface OrderVC ()

@property (nonatomic, strong) UITapGestureRecognizer * tap;

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger total;

@property (nonatomic, strong) NSMutableArray * arrOrders;

@end

@implementation OrderVC

#pragma mark -
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    /**
     *  Set back button appearance
     */
    
    _btnBack.hidden = gKAppSetting.bTapChanged;
    
    /**
     *  Remove footer view
     */
    
    _tblOrder.tableFooterView = [[UIView alloc] init];
    
    /**
     *  Set tap gesture recognizer for keyboard
     */
    
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    _tap.enabled = NO;
    [self.view addGestureRecognizer:_tap];
    
    /**
     *  Set initial page
     */
    
    _page = 0;
    _total = 0;
    
    [_tblOrder addInfiniteScrollingWithActionHandler:^{
        [self refreshTableView];
    }];
}

- (void)refreshTableView {
    
    NSInteger page_size = _total / BL_PAGE_SIZE;
    if (_total % BL_PAGE_SIZE != 0)
        page_size++;
    
    if (_page < page_size - 1) {
        
        _page++;
        [self loadOrders];
    } else {
        _tblOrder.showsInfiniteScrolling = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    /*
     *  Add observer for keyboard notification
     */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    
    /*
     *  Load Data
     */
    if (gKAppSetting.bTapChanged) {
        
        /*
         *  Initialize data
         */
        _arrOrders = [[NSMutableArray alloc] init];
        [_tblOrder reloadData];
        
        /*
         *  Load orders
         */
        [self performSelector:@selector(loadOrders) withObject:nil afterDelay:.2f];
        
        /*
         *  Set data loaded
         */
        gKAppSetting.bTapChanged = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    /*
     *  Remove observer for keyboard notification
     */
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark -
#pragma mark - Backendless Request

- (void)loadOrders {
    
    if (![KCommon checkBLLogin:self]) {
        return;
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.queryOptions.offset = [NSNumber numberWithInteger:BL_PAGE_SIZE*_page];
    query.queryOptions.pageSize = [NSNumber numberWithInteger:BL_PAGE_SIZE];
    
    NSMutableString * where = [NSMutableString stringWithFormat:@"userId='%@'", [UserManager getUserID]];
    
    if (_criteria.kind == ORDER_KIND_FAVORITE) {
        [where appendString:@" and favourite=1"];
    } else {
        
        BOOL isPaid = _criteria.kind == ORDER_KIND_PENDING || _criteria.kind == ORDER_KIND_TRACK ? YES : NO;
        [where appendString:[NSString stringWithFormat:@" and paid=%d", isPaid ? 1 : 0]];
        
        if (_criteria.kind == ORDER_KIND_PENDING) {
            [where appendString:@" and trackingNumber=''"];
        }
        if (_criteria.kind == ORDER_KIND_TRACK) {
            [where appendString:@" and trackingNumber<>''"];
        }
    }
    
    if (_txtSearch.text.length > 0) {
        [where appendString:[NSString stringWithFormat:@" and orderName like '%%%@%%' ", _txtSearch.text]];
    }
    
    query.whereClause = where;
    query.queryOptions.sortBy = @[@"orderDate DESC"];

    if (_page == 0) {
        [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    }
    
    [backendless clearAllCache];
    
    [[backendless.persistenceService of:[Orders class]] find:query response:^(BackendlessCollection * collection) {

        if (_page == 0) {
            [KAlert hideLoading:self.tabBarController.view];
        }
        
        if (collection == nil) {
            return;
        }
        
        KLLog(@"Load Orders Success");
        KLLog(@"Collection data count:%d", (int)collection.data.count);
        KLLog(@"Collection data Total Count:%d", (int)[collection.totalObjects integerValue]);
        
        if (_total == 0) {
            _total = [collection.totalObjects integerValue];
        }
        
        [_arrOrders addObjectsFromArray:collection.data];
        
        [_tblOrder.infiniteScrollingView stopAnimating];
        
        [_tblOrder reloadData];
        
    } error:^(Fault * fault) {
        
        if (_page == 0) {
            [KAlert hideLoading:self.tabBarController.view];
        } else {
            [_tblOrder.infiniteScrollingView stopAnimating];
        }
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to load orders" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"Failed to load order : %@", fault.description);
    }];
}

- (void)deleteOrder:(Orders*)order {
    
    [KAlert showLoading:@"Deleting" parent:self.tabBarController.view];
    
    [[backendless.persistenceService of:[Orders class]] remove:order response:^(NSNumber * aLong) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        if (order.favourite) {
            gKAppSetting.bDashboardLoaded = NO;
        }
        
        _arrOrders = [[NSMutableArray alloc] init];
        _page = 0;
        _total = 0;
        
        [self performSelector:@selector(loadOrders) withObject:nil afterDelay:.1f];
        
    } error:^(Fault * fault) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to delete order" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"Failed to delete order : %@", fault.description);
    }];
}

- (void)setFavoriteToOrder:(Orders*)order {
    
    [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    
    order.favourite = !order.favourite;
    
    [[backendless.persistenceService of:[Orders class]] save:order response:^(Orders * order) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        gKAppSetting.bDashboardLoaded = NO;
        
        _arrOrders = [[NSMutableArray alloc] init];
        _page = 0;
        _total = 0;
        
        [self performSelector:@selector(loadOrders) withObject:nil afterDelay:.1f];
        
    } error:^(Fault * fault) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to set favorite" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"Failed to set favorite : %@", fault.description);
    }];
}

#pragma mark -
#pragma mark - Storyboard prepare segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"SG_ORDER_INFO"]) {
        
        OrderInfoVC * orderInfoVC = [segue destinationViewController];
        orderInfoVC.criteria = (OrderCriteria*)sender;
    }
}

#pragma mark - Actions
#pragma mark - IBActions

- (IBAction)actionBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionSearch:(id)sender {
    
    [self loadOrders];
}

#pragma mark - On Long Item Click
- (IBAction)actionLongPress:(UILongPressGestureRecognizer*)sender {
    
    if (sender.state == UIGestureRecognizerStateBegan) {
        
        CGPoint p = [sender locationInView:_tblOrder];
        
        NSIndexPath *indexPath = [_tblOrder indexPathForRowAtPoint:p];
        if (indexPath == nil) {
            KLLog(@"long press on table view but not on a row");
        } else {
            
            UITableViewCell *cell = [_tblOrder cellForRowAtIndexPath:indexPath];
            if (cell.isHighlighted) {
                
                KLLog(@"long press on table view at section %d row %d", (int)indexPath.section, (int)indexPath.row);
                Orders * order = [_arrOrders objectAtIndex:indexPath.row];
                
                if (!order.paid) {
                    
                    UIAlertController * alert = [KAlert alertWithTitle:kAppName Message:@"Are you sure to delete this order?" LeftButton:@"YES" RightButton:@"NO" leftBlock:^(UIAlertAction *action) {
                    
                        [self deleteOrder:order];
                    } RightBlock:^(UIAlertAction *action) {
                        
                    }];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
        }
    }
}

#pragma mark - Table Cell Actions

- (void)actionFavorite:(UIGestureRecognizer*)recognizer {
    
    /*
     *  Get index path of tapped category
     */
    CGPoint buttonPosition = [recognizer locationInView:_tblOrder];
    NSIndexPath *indexPath = [_tblOrder indexPathForRowAtPoint:buttonPosition];
    
    /*
     *  Set favorite to order
     */
    Orders * order = [_arrOrders objectAtIndex:indexPath.row];
    [self setFavoriteToOrder:order];
}

#pragma mark -
#pragma mark - UITableView Datasource and Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 72.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_arrOrders == nil) {
        return 0;
    }
    
    return _arrOrders.count;
}

- (OrderCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderCell * cell = [tableView dequeueReusableCellWithIdentifier:@"order_cell"];
    
    UILongPressGestureRecognizer * longTapCell = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(actionLongPress:)];
    [cell addGestureRecognizer:longTapCell];
    UITapGestureRecognizer * tapFavorite = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionFavorite:)];
    cell.imgFavorite.userInteractionEnabled = YES;
    [cell.imgFavorite addGestureRecognizer:tapFavorite];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(OrderCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
     *  Set newsletter title
     */
    Orders * order = [_arrOrders objectAtIndex:indexPath.row];
    
    NSString * orderName = order.orderName;
    
    if (orderName == nil || orderName.length == 0) {
        orderName = @"New Order";
    }
    
    NSString * title = [NSString stringWithFormat:@"%@ %@ ", orderName, [order displayedOrderDate]];
    
    cell.lblTitle.text = title;
    
    if (order.favourite) {
        cell.imgFavorite.image = [UIImage imageNamed:@"like"];
    } else {
        cell.imgFavorite.image = [UIImage imageNamed:@"dislike"];
    }
    
    /*
     *  Set table cell separator inset
     */
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Orders * order = [_arrOrders objectAtIndex:indexPath.row];
    
    OrderCriteria * criteriaInfo = [[OrderCriteria alloc] init];
    
    if (_criteria.kind == ORDER_KIND_FAVORITE) {
        
        if (order.paid) {
            if (order.trackingNumber == nil || order.trackingNumber.length == 0) {
                
                criteriaInfo.kind = ORDER_KIND_PENDING;
                criteriaInfo.orderNo = order.orderNo;
            }
            else {
                
                criteriaInfo.kind = ORDER_KIND_TRACK;
                criteriaInfo.orderNo = order.orderNo;
            }
        }
        else {
            
            criteriaInfo.kind = ORDER_KIND_CURRENT;
            criteriaInfo.orderNo = order.orderNo;
        }
    } else {
        
        criteriaInfo.kind = _criteria.kind;
        criteriaInfo.orderNo = order.orderNo;
    }
    
    [self performSegueWithIdentifier:@"SG_ORDER_INFO" sender:criteriaInfo];
    
    return indexPath;
}

#pragma mark -
#pragma mark - UIKeyboard Notficiation

- (void)hideKeyboard {
    
    [_txtSearch resignFirstResponder];
}

- (void) resizeForKeyboard:(NSNotification*)aNotification {
    
    BOOL up = aNotification.name == UIKeyboardWillShowNotification;
    
    NSDictionary * userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationOptions animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    if (up) {
        
        
        [UIView animateWithDuration:animationDuration animations:^{
        } completion:^(BOOL finished) {
            _tap.enabled = YES;
        }];
    }
    else {        
        _tap.enabled = NO;
        [UIView animateWithDuration:animationDuration animations:^{
        } completion:^(BOOL finished) {
        }];
    }
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
