//
//  CheckOutVC.h
//  Kalahari
//
//  Created by LMAN on 4/15/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Orders.h"

@interface CheckOutVC : UIViewController

@property (nonatomic, strong) Orders * order;
@property (nonatomic, strong) NSString * orderNo;

/**
 *  Outlets title
 */

@property (weak, nonatomic) IBOutlet UILabel *lblOrderName;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderNo;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderDate;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

/**
 *  Outlets product
 */

@property (weak, nonatomic) IBOutlet UITableView *tblProducts;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTopTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consHeightTableView;
@property (weak, nonatomic) IBOutlet UIImageView *viewImgOrder;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryFee;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPrice;
@property (weak, nonatomic) IBOutlet UIView *viewTotal;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consHeightTotalView;

/**
 *  Outlets button
 */

@property (weak, nonatomic) IBOutlet UIButton *btnCheckOut;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@end
