//
//  OrderInfoVC.m
//  Kalahari
//
//  Created by LMAN on 4/13/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "OrderInfoVC.h"
#import "Orders.h"
#import "Products.h"
#import "OrderDetails.h"
#import "OrderManager.h"
#import "Global.h"
#import "ProductPayCell.h"
#import "ProductPaidCell.h"
#import "ProductDetailVC.h"
#import "UserManager.h"
#import "MenuVC.h"
#import "OrderTrackCriteria.h"
#import "OrderTrackVC.h"
#import "CheckOutVC.h"

@interface OrderInfoVC ()

@property (nonatomic, strong) UITapGestureRecognizer * tap;
@property (nonatomic, strong) Orders * order;
@property (nonatomic, strong) NSMutableArray * orderProducts;
@property (nonatomic, assign) BOOL isDuplicate;
@property (nonatomic, assign) BOOL isSync;
@property (nonatomic, assign) NSInteger retry;
@property (nonatomic, strong) Orders * tmpOrder;
@property (nonatomic, strong) OrderDetails * tmpOrderDetail;
@property (nonatomic, strong) UITextField * activeTextField;

@end

@implementation OrderInfoVC

#pragma mark -
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    /**
     *  Set back button visible
     */
    
    _btnBack.hidden = NO;
    
    /**
     *  Set table view appearance
     */
    
    _tblProducts.tableFooterView = [[UIView alloc] init];
    _tblProducts.backgroundColor = [UIColor clearColor];
    
    /**
     *  Remove unnecessary footer of table view
     */
    
    _tblProducts.tableFooterView = [[UIView alloc] init];
}

- (void)initVariables {
    
    _order = [[Orders alloc] init];
    
    _isSync = NO;
    _isDuplicate = NO;
    
    _tmpOrderDetail = [[OrderDetails alloc] init];
    _tmpOrder = [[Orders alloc] init];
    
    _order.orderNo = _criteria.orderNo;
    _orderProducts = [[NSMutableArray alloc] init];
    
    _retry = 5;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    /**
     *  Init variables
     */
    
    [self initVariables];
    
    /**
     *  Set tap gesture recognizer for keyboard
     */
    
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    _tap.enabled = NO;
    [self.view addGestureRecognizer:_tap];
    
    /**
     *  Set keyboard notification observer
     */
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    
    /**
     *  Show values
     */
    
    [self showValues];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillLayoutSubviews {

    [super viewWillLayoutSubviews];
    
    /*
     *  Compose layout
     */
    
    _viewImgOrder.layer.borderColor = [COLOR_THEME CGColor];
    _viewImgOrder.layer.borderWidth = 1.f;
    _viewImgOrder.layer.cornerRadius = CGRectGetHeight(_viewImgOrder.frame) / 2;
    
    _btnAddProduct.layer.borderColor = [COLOR_THEME CGColor];
    _btnAddProduct.layer.borderWidth = 1.f;
    _btnAddProduct.layer.cornerRadius = CGRectGetHeight(_btnAddProduct.frame) / 2;
    
    _btnCheckOut.layer.borderColor = [COLOR_THEME CGColor];
    _btnCheckOut.layer.borderWidth = 1.f;
    _btnCheckOut.layer.cornerRadius = CGRectGetHeight(_btnCheckOut.frame) / 2;
    
    _btnDuplicateOrder.layer.borderColor = [COLOR_THEME CGColor];
    _btnDuplicateOrder.layer.borderWidth = 1.f;
    _btnDuplicateOrder.layer.cornerRadius = CGRectGetHeight(_btnDuplicateOrder.frame) / 2;
    
    _btnDuplicateOrder2.layer.borderColor = [COLOR_THEME CGColor];
    _btnDuplicateOrder2.layer.borderWidth = 1.f;
    _btnDuplicateOrder2.layer.cornerRadius = CGRectGetHeight(_btnDuplicateOrder2.frame) / 2;
    
    _btnTrackOrder.layer.borderColor = [COLOR_THEME CGColor];
    _btnTrackOrder.layer.borderWidth = 1.f;
    _btnTrackOrder.layer.cornerRadius = CGRectGetHeight(_btnTrackOrder.frame) / 2;
    
    _btnAddProduct2.layer.borderColor = [COLOR_THEME CGColor];
    _btnAddProduct2.layer.borderWidth = 1.f;
    _btnAddProduct2.layer.cornerRadius = CGRectGetHeight(_btnAddProduct2.frame) / 2;
}

#pragma mark - 
#pragma mark - Show values

- (void)showValues {
    
    KLLog(@"showValues called!");
    
    /**
     *  Set view hidden
     */
    
    _scrollView.hidden = YES;
    _orderView.hidden = YES;
    
    /**
     *  Show values accroding to the order kind
     */
    
    if (_criteria.kind == ORDER_KIND_NEW) {
        [self generateOrderNo];
    } else if (_criteria.kind == ORDER_KIND_CURRENT) {
        
        if (_order.orderNo == nil || [_order.orderNo isEqualToString:@""]) {
            
            Orders * newOrder = [gKAppSetting getNewOrder];
            if (newOrder == nil) {
                [self generateOrderNo];
            } else {
                [self checkOrderNo:_order.orderNo];
            }
        } else {
            [self checkOrderNo:_order.orderNo];
        }
    }
    
    if (_criteria.kind == ORDER_KIND_TRACK || _criteria.kind == ORDER_KIND_PENDING) {
        
        if (_order.orderNo.length == 0) {
            UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to load data" ButtonTitle:@"OK"];
            [self presentViewController:notifyAlert animated:YES completion:nil];
        } else {
            [self getOrderNo:_order.orderNo];
        }
    }
}

#pragma mark -
#pragma mark - Actions

- (IBAction)actionBack:(id)sender {
    
    if (_criteria.kind == ORDER_KIND_NEW) {
        self.tabBarController.selectedIndex = 0;
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)actionAddProduct:(id)sender {
    [self startSync:[NSNumber numberWithInteger:ORDER_BUTTON_ADD_PRODUCT] action:[NSNumber numberWithInteger:0]];
}

- (IBAction)actionCheckOut:(id)sender {
    [self startSync:[NSNumber numberWithInteger:ORDER_BUTTON_CHECK_OUT] action:[NSNumber numberWithInteger:0]];
}

- (IBAction)actionDuplicateOrder:(id)sender {
    [self startSync:[NSNumber numberWithInteger:ORDER_BUTTON_DUPLICATE_ORDER] action:[NSNumber numberWithInteger:0]];
}

- (IBAction)actionTrackOrder:(id)sender {
    [self startSync:[NSNumber numberWithInteger:ORDER_BUTTON_TRACK_ORDER] action:[NSNumber numberWithInteger:0]];
}

#pragma mark - 
#pragma mark - Refresh values

- (void)onGenerateNewOrderSuccess {
    
    KLLog(@"onGenerateNewOrderSuccess called!");
    Orders * newOrder = [gKAppSetting getNewOrder];
    
    [KAlert hideLoading:self.tabBarController.view];
    
    if (newOrder != nil) {
        
        if (newOrder.orderName == nil || newOrder.orderName.length == 0) {
            newOrder.orderName = @"";
        }
     
        [self saveOrders:newOrder];
    } else {
    }
}

#pragma mark - 
#pragma mark - Backendless request

- (void)onComplete {
    
    KLLog(@"onComplete Called!");
    
    _txtOrderName.text = [_order displayedOrderName];
    _lblOrderNo.text = [_order displayedOrderNo];
    _lblOrderDate.text = [_order displayedOrderDate];
    _lblOrderPrice.text = [_order displayedTotalWithOutPOD];
    
    /**
     *  Set title focusable
     */
    
    NSLog(@"Orer Name : %@", _txtOrderName.text);
    if (_criteria.kind == ORDER_KIND_NEW || _txtOrderName.text.length == 0) {
        _txtOrderName.userInteractionEnabled = YES;
    }
    else {
        _txtOrderName.userInteractionEnabled = NO;
    }
    
    BOOL isPaid = _criteria.kind == ORDER_KIND_TRACK || _criteria.kind == ORDER_KIND_PENDING;
    _isSync = NO;
    
    if (_criteria.product == nil) {
        [self initProductList];
    } else {
        
        if (!isPaid) {
            
            OrderDetails * detail = [[OrderDetails alloc] init];
            Products * product = [[Products alloc] init];
            [product copyValues:_criteria.product];
            
            detail.product = product;
            detail.count = _criteria.count;
            
            _criteria.count = 0;
            [_tmpOrderDetail initDetailValue];
            
            KLLog(@"Saving Detail");
            [[backendless.persistenceService of:[Products class]] save:detail response:^(OrderDetails * orderDetail) {
                
                if (orderDetail == nil) {
                    [self performSelector:@selector(failedToAddProduct) withObject:nil afterDelay:.1f];
                } else {
                    
                    _isSync = YES;
                    [_tmpOrderDetail copyValue:orderDetail];
                    
                    [self performSelector:@selector(initProductList) withObject:nil afterDelay:.1f];
                }
            } error:^(Fault * fault) {
                [self performSelector:@selector(failedToAddProduct) withObject:nil afterDelay:.1f];
            }];
        } else {
            [self initProductList];
        }
    }
}

- (void)failedToAddProduct {
    
    UIAlertController * failAlert = [KAlert alertWithTitle:kAppName Message:@"Failed to add product" LeftButton:@"OK" RightButton:nil leftBlock:^(UIAlertAction *action) {
        [self initProductList];
    } RightBlock:nil];
    [self presentViewController:failAlert animated:YES completion:nil];
}

- (void)initProductList {
    
    KLLog(@"Init Product List Called!");
    
    NSMutableArray * list = _order.details;
    if (list == nil) {
        list = [[NSMutableArray alloc] init];
    }
    
    if (_isSync) {
        
        OrderDetails * detail = [[OrderDetails alloc] init];
        [detail copyValue:_tmpOrderDetail];
        [list addObject:detail];
        _order.details = list;
    }
    
    _orderProducts = [[NSMutableArray alloc] init];
    
    CGFloat total = 0;
    CGFloat gp = 0;
    NSInteger pod = 0;
    CGFloat delivery_fee = 0;
    
    for (NSInteger i = 0; i < list.count; i++ ) {
        
        OrderDetails * detail = [list objectAtIndex:i];
        if (detail.product == nil) {
            continue;
        }
        
        [_orderProducts addObject:detail];
        
        total += detail.product.price * detail.count;
        gp += [detail.product calculateGP] * detail.count;
        pod += detail.product.pod * detail.count;
    }
    
    delivery_fee = [[[backendless.userService login:gKAppSetting.userEmail password:gKAppSetting.userPwd] getProperty:@"deliveryFee"] integerValue];
    if (total > DELIVERY_FEE_LIMIT) {
        delivery_fee = 0;
    }
    
    _order.totalPrice = total + delivery_fee;
    _order.totalGP = gp;
    _order.totalPOD = pod;
    
    _lblOrderPrice.text = _order.displayedTotalWithOutPOD;
    
    _consTblHeight.constant = 44 * _orderProducts.count;
    _consTblBottom.constant = (_orderProducts.count == 0) ? 0 : 20;
    [_tblProducts reloadData];
    
    [KAlert hideLoading:self.tabBarController.view];
    if (_isSync) {
        
        _isSync = NO;
        [self saveOrders];
    } else {
        
        [self onDone];
    }
}

- (void)onDone {
    
    /*
     * Show hidden view
     */
    
    _scrollView.hidden = NO;
    _orderView.hidden = NO;
    _viewButtonFirst.hidden = NO;
    _viewButtonSecond.hidden = NO;
    _btnAddProduct.hidden = NO;
    _btnDuplicateOrder.hidden = NO;
    _btnCheckOut.hidden = NO;
    _btnDuplicateOrder2.hidden = NO;
    _btnAddProduct2.hidden = NO;
    _btnTrackOrder.hidden = NO;
    
    /*
     *  Adjust layout for different condition
     */
    
    if (_criteria.kind == ORDER_KIND_TRACK || _criteria.kind == ORDER_KIND_PENDING) {
        
        NSString * trackNumber = _order.trackingNumber;
        if (trackNumber != nil && trackNumber.length > 0) {
            
            _consButtonView2Top.constant = - CGRectGetHeight(_viewButtonFirst.frame);
            _btnAddProduct2.hidden = YES;
            _viewButtonFirst.hidden = YES;
        } else {
            
            _btnAddProduct.hidden = YES;
            _btnCheckOut.hidden = YES;
            _consScrollContentBottom.constant = - CGRectGetHeight(_viewButtonSecond.frame);
            _viewButtonSecond.hidden = YES;
        }
    }
    
    if (_criteria.kind == ORDER_KIND_NEW || _criteria.kind == ORDER_KIND_CURRENT) {
        
        if (_order.details != nil && _order.details.count > 0) {
            
            _btnDuplicateOrder.hidden = YES;
            _consScrollContentBottom.constant = -CGRectGetHeight(_viewButtonSecond.frame);
            _viewButtonSecond.hidden = YES;
        } else {
            
            _consButtonView2Top.constant = - CGRectGetHeight(_viewButtonFirst.frame);
            _btnDuplicateOrder2.hidden = YES;
            _btnTrackOrder.hidden = YES;
            _viewButtonFirst.hidden = YES;
        }
    }
    
    /*
     *  Calculate and refresh total money
     */
    [self refreshTotalMoney];
}

- (void)generateOrderNo {
    
    KLLog(@"generateOrderNo called!");
    if (![KCommon checkBLLogin:self]) {
        return;
    }
    
    NSString * orderNo = [OrderManager generateOrderNo];
    
    if (_isDuplicate) {
        [KAlert showLoading:@"Duplicating" parent:self.tabBarController.view];
    } else {
        [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.queryOptions.pageSize = [NSNumber numberWithInteger:1];
    query.whereClause = [NSString stringWithFormat:@"orderNo='%@' ", orderNo];

    [[backendless.persistenceService of:[Orders class]] find:query response:^(BackendlessCollection * collection) {
        
        KLLog(@"Generating success!");
        if (collection.data != nil && collection.data.count > 0) {
            
            [KAlert hideLoading:self.tabBarController.view];
            
            [self performSelector:@selector(generateOrderNo) withObject:nil afterDelay:0.1];
        
        } else {
            if (_isDuplicate) {
                
                _tmpOrder.orderNo = orderNo;
                [self performSelector:@selector(finishDuplicate) withObject:nil afterDelay:.1f];
            } else {
                
                KLLog(@"OrderNo to be saved:%@", orderNo);
                [gKAppSetting saveNewOrder:orderNo];
                [self performSelector:@selector(onGenerateNewOrderSuccess) withObject:nil afterDelay:.1f];
            }
        }
    } error:^(Fault * fault) {
        
        KLLog(@"Generating no failed!");
        [KAlert hideLoading:self.tabBarController.view];
        
        NSString * errMsg = _isDuplicate ? @"Failed to duplicate order" : @"Failed to load order";
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:errMsg ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"%@ : %@", errMsg, fault.description);
    }];
}

- (void)checkOrderNo:(NSString*)orderNo {
    
    KLLog(@"checkOrderNo called!");
    
    [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    NSString * userId = [UserManager getUserID];
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    
    query.whereClause = [NSString stringWithFormat:@"userId='%@' and orderNo='%@' ", userId, orderNo];
    
    [[backendless.persistenceService of:[Orders class]] find:query response:^(BackendlessCollection * collection) {

        if ([collection.totalObjects integerValue] > 0) {
            
            Orders * order = [collection.data objectAtIndex:0];
            
            if (order.paid) {
                
                [KAlert hideLoading:self.tabBarController.view];
                [self performSelector:@selector(generateOrderNo) withObject:nil afterDelay:.1f];
            } else {
                
                _order = order;
                [self performSelector:@selector(onComplete) withObject:nil afterDelay:.1f];
            }
        } else {
            
            [KAlert hideLoading:self.tabBarController.view];
            [self performSelector:@selector(generateOrderNo) withObject:nil afterDelay:.1f];
        }
    } error:^(Fault * fault) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        NSString * errMsg = @"Failed to load order";
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:errMsg ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"%@ : %@", errMsg, fault.description);
    }];
}

- (void)getOrderNo:(NSString*)orderNo {
    
    KLLog(@"getOrderNo called!");
    [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    NSString * userId = [UserManager getUserID];
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    
    query.whereClause = [NSString stringWithFormat:@"userId='%@' and orderNo='%@' ", userId, orderNo];
    
    [[backendless.persistenceService of:[Orders class]] find:query response:^(BackendlessCollection * collection) {
        
        if (collection.data != nil && collection.data.count > 0) {
            
            Orders * order = [collection.data objectAtIndex:0];
            _order = order;
            [self performSelector:@selector(onComplete) withObject:nil afterDelay:.1f];
        }
        
    } error:^(Fault * fault) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        NSString * errMsg = @"Failed to load order";
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:errMsg ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"%@ : %@", errMsg, fault.description);
    }];
}

- (void)saveOrders:(Orders*)order {
    
    KLLog(@"saveOrders: called!");
    [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    
    NSString * userId = [UserManager getUserID];
    order.userId = userId;
    order.salonName = [UserManager getUserName];
    
    [[backendless.persistenceService of:[Orders class]] save:order response:^(Orders * retOrder) {
    
        KLLog(@"Response:%@", retOrder);
        
        [_order copyValue:retOrder];
        [self performSelector:@selector(onComplete) withObject:nil afterDelay:.1f];
        
    } error:^(Fault * fault) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        NSString * errMsg = @"Failed to load save";
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:errMsg ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"%@ : %@", errMsg, fault.description);
        
    }];
}

- (void)saveOrders {
    
    KLLog(@"saveOrders called!");
    
    [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    
    [[backendless.persistenceService of:[Orders class]] save:_order response:^(Orders * order) {
        
        _order = order;
        
        OrderCriteria * criteria = [[OrderCriteria alloc] init];
        criteria.kind = _criteria.kind;
        criteria.orderNo = _order.orderNo;
        
        self.criteria = criteria;
    
        [KAlert hideLoading:self.tabBarController.view];
        
        [self performSelector:@selector(showValues) withObject:nil afterDelay:.1f];
        
    } error:^(Fault * fault) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        NSString * errMsg = @"Failed to load save";
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:errMsg ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"%@ : %@", errMsg, fault.description);
        
    }];
}

- (void)syncOrders:(NSInteger)index count:(NSInteger)count {
    
    NSMutableArray * list = [_order.details mutableCopy];
    if (list != nil) {
        
        if (list.count > index) {
            
            OrderDetails * detail = [list objectAtIndex:index];
            detail.count = count;
            [list replaceObjectAtIndex:index withObject:detail];
            _order.details = list;
            _isSync = true;
            [self refreshTotalMoney];
        }
    }
}

- (void)refreshTotalMoney {
    
    NSMutableArray * list = [_order.details mutableCopy];
    
    if (list != nil) {
        
        CGFloat total = 0;
        NSInteger count = 0;
        CGFloat gp = 0;
        NSInteger pod = 0;
        CGFloat delivery_fee = 0;
        
        for (OrderDetails * detail in list) {
            
            total += detail.product.price * detail.count;
            gp += [detail.product calculateGP] * detail.count;
            pod += detail.product.pod * detail.count;
            count++;
        }
        
        delivery_fee = [[[backendless.userService login:gKAppSetting.userEmail password:gKAppSetting.userPwd] getProperty:@"deliveryFee"] integerValue];
        
        if (total > DELIVERY_FEE_LIMIT) {
            delivery_fee = 0;
        }
        
        if (total == 0) {
            delivery_fee = 0;
        }
        
        _order.totalPrice = total + delivery_fee;
        _order.totalGP = gp;
        _order.totalPOD = pod;
        
        _lblOrderPrice.text = [_order displayedTotalWithOutPOD];
        
        if (count >= 1) {
            
            _lblDeliveryFee.text = [OrderManager getDisplayedPrice:delivery_fee];
            _lblTotalPrice.text = [_order displayedPrice];
            
        } else {
            
            _viewTotal.hidden = YES;
            _consViewTotalHeight.constant = 0;
        }
    }
}

- (BOOL)startSync:(NSNumber*)buttonId action:(NSNumber*)action {
    
    NSString * orderName = _txtOrderName.text;
    if (orderName.length == 0 || (_criteria.kind == ORDER_KIND_CURRENT && [orderName isEqualToString:@""])) {
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Please input order name" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        return NO;
    }
    
    if (![_order.orderName isEqualToString:orderName]) {
        
        [_order setOrderName:orderName];
        _isSync = YES;
    }
    
    _retry = 5;
    
    [self prepareAction:buttonId action:action];
    
    return true;
}

- (void)prepareAction:(NSNumber*)buttonID action:(NSNumber*)action {
    
    if (_isSync) {
        
        _retry --;
        
        if (_retry < 0) {
            
            [self onSyncFailed:buttonID action:action];
            return;
        }
        
        [KAlert showLoading:@"Synchronizing" parent:self.tabBarController.view];
        
        [[backendless.persistenceService of:[Orders class]] save:_order response:^(Orders * order) {
            
            if (order != nil) {
                
                [KAlert hideLoading:self.tabBarController.view];
                
                _order = order;
                [self doAction:buttonID action:action];
            } else {
                [self prepareAction:buttonID action:action];
            }
        } error:^(Fault * fault) {
            [self prepareAction:buttonID action:action];
        }];
    } else {
        [self doAction:buttonID action:action];
    }
}

- (void)doAction:(NSNumber*)buttonID action:(NSNumber*)action {
    
    BOOL bSync = _isSync;
    _isSync = NO;
    
    switch ([buttonID integerValue]) {
        case ORDER_BUTTON_ADD_PRODUCT: {
            [gKAppSetting setCurrentOrderNo:_order.orderNo];
            NSNumber * categoryID = [NSNumber numberWithInteger:0];
            [self performSegueWithIdentifier:@"SG_MENU_FROM_ORDER_INFO" sender:categoryID];
        }
            break;
        
        case ORDER_BUTTON_DUPLICATE_ORDER:
            [self duplicateOrders];
            break;
            
        case ORDER_BUTTON_TRACK_ORDER:
            [self trackingOrders];
            break;
            
        case ORDER_BUTTON_CHECK_OUT:
            [self checkOut:bSync];
            break;
    }
    
    if (buttonID == 0) {
        [self doFooterAction:action];
    }
}

- (void)onSyncFailed:(NSNumber*)buttonID action:(NSNumber*)action {
    
    [KAlert hideLoading:self.tabBarController.view];
    
    UIAlertController * alert = [KAlert alertWithTitle:kAppName Message:@"Failed to synchronize" LeftButton:@"OK" RightButton:nil leftBlock:^(UIAlertAction *alertAction) {
        if (buttonID == 0) {
            [self doFooterAction:action];
        }
    } RightBlock:nil];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)doFooterAction:(NSNumber*)action {
//    switch (action) {
//        case R.id.menu_home:
//            mBridge.switchTo(DashboardFragment.newInstance(), true);
//            break;
//            
//        case R.id.menu_new_orders:
//            mBridge.switchTo(OrderInfoFragment.newInstance(Constants.ORDER_NEW), false);
//            break;
//            
//        case R.id.menu_current_orders:
//            mBridge.switchTo(OrderListFragment.newInstance(Constants.ORDER_CURRENT), false);
//            break;
//            
//        case R.id.menu_pending_orders:
//            mBridge.switchTo(OrderListFragment.newInstance(Constants.ORDER_PENDING), false);
//            break;
//            
//        case R.id.menu_track_orders:
//            mBridge.switchTo(OrderListFragment.newInstance(Constants.ORDER_TRACK), false);
//            break;
//            
//        default:
//            mBridge.onBack();
//    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)deletProduct:(NSInteger)index {
    
    if (_order.details != nil) {
        
        if (_order.details.count > index) {
            
            Orders * order = [[Orders alloc] init];
            [order copyValue:_order];
            
            OrderDetails * detail = [[OrderDetails alloc] init];
            [detail copyValue:[_order.details objectAtIndex:index]];
            [_order.details removeObjectAtIndex:index];
            
            [self deleteOrderDetails:(Orders*)order detail:(OrderDetails*)detail];
        }
    }
}

- (void)deleteProduct:(NSInteger)index {

    if (_order.details != nil) {
        
        if (_order.details.count > index) {
            
            Orders * order = [[Orders alloc] init];
            [order copyValue:_order];

            OrderDetails * detail = [[OrderDetails alloc] init];
            [detail copyValue:[order.details objectAtIndex:index]];
            [order.details removeObjectAtIndex:index];
            
            [self deleteOrderDetails:order detail:detail];
        }
    }
}

- (void)deleteOrderDetails:(Orders*)order detail:(OrderDetails*)detail {
    
    [KAlert showLoading:@"Deleting" parent:self.tabBarController.view];
    
    [[backendless.persistenceService of:[OrderDetails class]] remove:detail response:^(NSNumber * aLong) {
        [self deleteOrders:order];
    } error:^(Fault * fault) {
        
        [KAlert hideLoading:self.tabBarController.view];
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to delete order" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
    }];
}

- (void)deleteOrders:(Orders*) order {
    
    [[backendless.persistenceService of:[Orders class]] save:order response:^(Orders * order) {
        
        [KAlert hideLoading:self.tabBarController.view];
        _order = order;
        
        OrderCriteria * criteria = [[OrderCriteria alloc] init];
        criteria.kind = _criteria.kind;
        criteria.orderNo = _order.orderNo;
        
        _criteria = criteria;
        
        [self initVariables];
        [self showValues];
        
    } error:^(Fault * fault) {
        
        [KAlert hideLoading:self.tabBarController.view];
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to delete order" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        
    }];
}

- (void)duplicateOrders {
    
    _isDuplicate = true;
    [_tmpOrder copyValue:_order];
    _tmpOrder.objectId = nil;
    _tmpOrder.paid = NO;
    
    [self generateOrderNo];
}

- (void)finishDuplicate {
    
    [[backendless.persistenceService of:[Orders class]] save:_tmpOrder response:^(Orders * order) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        [gKAppSetting saveNewOrder:_tmpOrder.orderNo];
        
        OrderCriteria * criteria = [[OrderCriteria alloc] init];
        criteria.kind = ORDER_KIND_CURRENT;
        criteria.orderNo = _tmpOrder.orderNo;
        self.criteria = criteria;
        
        [self initVariables];
        [self showValues];
        
    } error:^(Fault * fault) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to duplicate order" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        
    }];
}

- (void)trackingOrders {
    
    OrderTrackCriteria * trackCriteria = [[OrderTrackCriteria alloc] init];
    trackCriteria.orderNo = _order.orderNo;
    trackCriteria.trackingNumber = _order.trackingNumber;
    [self performSegueWithIdentifier:@"SG_ORDER_TRACK" sender:trackCriteria];
}

- (void)checkOut:(BOOL)bSync {
    
    if (_order.totalPrice == 0) {
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Please add product!" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
    } else {
        
        [self performSegueWithIdentifier:@"SG_CHECK_OUT" sender:_order.orderNo];
    }
}

#pragma mark -
#pragma mark - Table Cell Action

- (void)actionDecreaseAmount:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tblProducts];
    NSIndexPath *indexPath = [_tblProducts indexPathForRowAtPoint:buttonPosition];
    ProductPayCell * cell = [_tblProducts cellForRowAtIndexPath:indexPath];
    OrderDetails * detail = [_orderProducts objectAtIndex:indexPath.row];
    
    NSString * amount = cell.txtAmount.text;
    if (amount.length == 0) {
        
        cell.txtAmount.text = @"0";
        cell.lblPrice.text = [OrderManager getDisplayedPrice:detail.product.price * 0];
        [self syncOrders:indexPath.row count:0];
        return;
    }
    
    NSInteger iAmount = amount.integerValue;
    iAmount--;
    
    if (iAmount < 0) {
        iAmount = 0;
    }
    
    cell.txtAmount.text = [NSString stringWithFormat:@"%d", (int)iAmount];
    cell.lblPrice.text = [OrderManager getDisplayedPrice:detail.product.price * iAmount];
    [self syncOrders:indexPath.row count:iAmount];
}

- (void)actionIncreaseAmount:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tblProducts];
    NSIndexPath *indexPath = [_tblProducts indexPathForRowAtPoint:buttonPosition];
    ProductPayCell * cell = [_tblProducts cellForRowAtIndexPath:indexPath];
    OrderDetails * detail = [_orderProducts objectAtIndex:indexPath.row];
    
    NSString * amount = ((ProductPayCell*)cell).txtAmount.text;
    if (amount.length == 0) {
        
        ((ProductPayCell*)cell).txtAmount.text = @"0";
        ((ProductPayCell*)cell).lblPrice.text = [OrderManager getDisplayedPrice:detail.product.price*0];
        [self syncOrders:indexPath.row count:0];
        return;
    }
    
    NSInteger iAmount = amount.integerValue;
    iAmount++;
    if (iAmount < 0) {
        iAmount = 0;
    }
    
    ((ProductPayCell*)cell).txtAmount.text = [NSString stringWithFormat:@"%d", (int)iAmount];
    ((ProductPayCell*)cell).lblPrice.text = [OrderManager getDisplayedPrice:detail.product.price * iAmount];
    [self syncOrders:indexPath.row count:iAmount];
}

- (void)actionBeginEditAmount:(id)sender {

    _activeTextField = sender;
}

- (void)actionEditAmount:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tblProducts];
    NSIndexPath *indexPath = [_tblProducts indexPathForRowAtPoint:buttonPosition];
    ProductPayCell * cell = [_tblProducts cellForRowAtIndexPath:indexPath];
    
    NSString * amount = ((ProductPayCell*)cell).txtAmount.text;
    NSInteger iAmount = 0;
    
    if (amount.length > 0) {
        iAmount = amount.integerValue;
    }
    
    [self syncOrders:indexPath.row count:iAmount];
}

- (IBAction)actionLongPress:(UILongPressGestureRecognizer*)sender {
    
    if (sender.state == UIGestureRecognizerStateBegan) {
        
        CGPoint p = [sender locationInView:_tblProducts];
        
        NSIndexPath *indexPath = [_tblProducts indexPathForRowAtPoint:p];
        if (indexPath == nil) {
            KLLog(@"long press on table view but not on a row");
        } else {
            
            UITableViewCell *cell = [_tblProducts cellForRowAtIndexPath:indexPath];
            if (cell.isHighlighted) {
                
                KLLog(@"long press on table view at section %d row %d", (int)indexPath.section, (int)indexPath.row);
                OrderDetails * detail = [_orderProducts objectAtIndex:indexPath.row];
                
                Products * product = detail.product;
                
                if (product == nil) {
                    return;
                }
                
                UIAlertController * alertDelete = [KAlert alertWithTitle:kAppName Message:@"Are you sure to delete this product?" LeftButton:@"Yes" RightButton:@"No" leftBlock:^(UIAlertAction *action) {
                    [self deleteProduct:indexPath.row];
                } RightBlock:^(UIAlertAction *action) {
                }];
                [self presentViewController:alertDelete animated:YES completion:nil];
            }
        }
    }
}

#pragma mark -
#pragma mark - UITableView Datasource and Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_orderProducts == nil) {
        return 0;
    }
    
    return _orderProducts.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell * cell;
    
    BOOL isPaid = _criteria.kind == ORDER_KIND_TRACK || _criteria.kind == ORDER_KIND_PENDING;
    
    if (!isPaid) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"product_pay_cell"];
        
        [((ProductPayCell*)cell).btnLeft addTarget:self action:@selector(actionDecreaseAmount:) forControlEvents:UIControlEventTouchUpInside];
        [((ProductPayCell*)cell).btnRight addTarget:self action:@selector(actionIncreaseAmount:) forControlEvents:UIControlEventTouchUpInside];
        
        [((ProductPayCell*)cell).txtAmount addTarget:self action:@selector(actionEditAmount:) forControlEvents:UIControlEventEditingChanged];
        
        [((ProductPayCell*)cell).txtAmount addTarget:self action:@selector(actionBeginEditAmount:) forControlEvents:UIControlEventEditingDidBegin];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"product_paid_cell"];
    }
    
    UILongPressGestureRecognizer * longTapCell = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(actionLongPress:)];
    [cell addGestureRecognizer:longTapCell];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderDetails * detail = [_orderProducts objectAtIndex:indexPath.row];
    
    BOOL isPaid = _criteria.kind == ORDER_KIND_TRACK || _criteria.kind == ORDER_KIND_PENDING;
    
    if (isPaid) {
        
        ((ProductPaidCell*)cell).lblBill.text = [NSString stringWithFormat:@"%d x %@", (int)detail.count, detail.product.name];
        ((ProductPaidCell*)cell).lblPrice.text = [OrderManager getDisplayedPrice:detail.product.price * detail.count];
        
    } else {
        
        ((ProductPayCell*)cell).lblTitle.text = detail.product.name;
        ((ProductPayCell*)cell).txtAmount.text = [NSString stringWithFormat:@"%d", (int)detail.count];
        ((ProductPayCell*)cell).lblPrice.text = [OrderManager getDisplayedPrice:detail.product.price * detail.count];
    }
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OrderDetails * detail = [_orderProducts objectAtIndex:indexPath.row];
    Products * product = detail.product;
    
    if (product != nil) {
        
        [self performSegueWithIdentifier:@"SG_PRODUCT_DETAIL_FROM_ORDER_INFO" sender:indexPath];
    }
    
    return indexPath;
}

#pragma mark -
#pragma mark - Storyboard prepare segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"SG_PRODUCT_DETAIL_FROM_ORDER_INFO"]) {
        
        NSIndexPath * indexPath = (NSIndexPath*)sender;
        
        OrderDetails * detail = [_orderProducts objectAtIndex:indexPath.row];
        
        ProductDetailVC *vc = [segue destinationViewController];
        vc.product = detail.product;
        vc.kind = PRODUCT_DETAIL_KIND_VIEW;
    }
    
    if ([[segue identifier] isEqualToString:@"SG_MENU_FROM_ORDER_INFO"]) {
        
        MenuVC * menuVC = [segue destinationViewController];
        menuVC.categoryId = [((NSNumber*)sender)integerValue];
    }
    
    if ([[segue identifier] isEqualToString:@"SG_ORDER_TRACK"]) {
        
        OrderTrackVC * vc = [segue destinationViewController];
        vc.orderNo = ((OrderTrackCriteria*)sender).orderNo;
        vc.trackingNumber = ((OrderTrackCriteria*)sender).trackingNumber;
    }
    
    if ([[segue identifier] isEqualToString:@"SG_CHECK_OUT"]) {
        
        CheckOutVC * vc = [segue destinationViewController];
        vc.orderNo = sender;
    }
}

#pragma mark -
#pragma mark - UIKeyboard Notification

- (void) hideKeyboard {
    
    [_txtOrderName resignFirstResponder];
    [_activeTextField resignFirstResponder];
}

- (void) resizeForKeyboard:(NSNotification*)aNotification {
    
    BOOL up = aNotification.name == UIKeyboardWillShowNotification;
    
    NSDictionary * userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationOptions animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    if (up) {
        
        [UIView animateWithDuration:animationDuration animations:^{
            
        } completion:^(BOOL finished) {
            
            _tap.enabled = YES;
        }];
    }
    else {
        
        KLLog(@"Keyboard Down");
        
        [UIView animateWithDuration:animationDuration animations:^{
            
        } completion:^(BOOL finished) {
            
            _tap.enabled = NO;
        }];
    }
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
