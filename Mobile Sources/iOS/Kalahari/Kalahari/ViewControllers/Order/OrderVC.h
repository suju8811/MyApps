//
//  OrderVC.h
//  Kalahari
//
//  Created by LMAN on 3/24/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Orders.h"
#import "OrderCriteria.h"

@interface OrderVC : UIViewController

/*
 *  Outlets
 */
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (nonatomic, strong) OrderCriteria * criteria;
@property (weak, nonatomic) IBOutlet UITableView *tblOrder;

@end
