//
//  OrderInfoVC.h
//  Kalahari
//
//  Created by LMAN on 4/13/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderCriteria.h"

typedef NS_ENUM(NSInteger, OrderButton) {
    
    ORDER_BUTTON_ADD_PRODUCT        = 1,
    ORDER_BUTTON_DUPLICATE_ORDER    = 2,
    ORDER_BUTTON_CHECK_OUT          = 3,
    ORDER_BUTTON_TRACK_ORDER        = 4,
};

@interface OrderInfoVC : UIViewController

@property (nonatomic, strong) OrderCriteria * criteria;

/*
 *  Outlets back button
 */
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

/*
 *  Outlets title
 */
@property (weak, nonatomic) IBOutlet UITextField *txtOrderName;

/*
 *  Outlets main scroll view
 */
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

/*
 *  Outlets order
 */
@property (weak, nonatomic) IBOutlet UIView *orderView;
@property (weak, nonatomic) IBOutlet UIView *viewImgOrder;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderNo;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderDate;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderPrice;

/*
 *  Outlets product
 */
@property (weak, nonatomic) IBOutlet UITableView *tblProducts;
@property (weak, nonatomic) IBOutlet UIView *viewTotal;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consViewTotalHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTblBottom;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryFee;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPrice;

/*
 *  Outlets button
 */
@property (weak, nonatomic) IBOutlet UIView *viewButtonFirst;
@property (weak, nonatomic) IBOutlet UIView *viewButtonSecond;
@property (weak, nonatomic) IBOutlet UIButton *btnAddProduct;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckOut;
@property (weak, nonatomic) IBOutlet UIButton *btnDuplicateOrder;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consScrollContentBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consButtonView2Top;
@property (weak, nonatomic) IBOutlet UIButton *btnDuplicateOrder2;
@property (weak, nonatomic) IBOutlet UIButton *btnTrackOrder;
@property (weak, nonatomic) IBOutlet UIButton *btnAddProduct2;

@end
