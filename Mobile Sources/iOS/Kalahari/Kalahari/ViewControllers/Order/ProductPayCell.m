//
//  ProductPayCell.m
//  Kalahari
//
//  Created by LMAN on 4/14/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "ProductPayCell.h"

@implementation ProductPayCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    _txtAmount.layer.borderColor = [COLOR_THEME CGColor];
    _txtAmount.layer.borderWidth = 1.f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
