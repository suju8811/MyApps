//
//  ForgotVC.h
//  Kalahari
//
//  Created by LMAN on 3/23/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *viewLoginFrame;

@property (weak, nonatomic) IBOutlet UIView *viewEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnResetPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBottom;

@end
