//
//  LoginVC.m
//  Kalahari
//
//  Created by LMAN on 3/23/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "LoginVC.h"

@interface LoginVC ()

@property (nonatomic, strong) UITapGestureRecognizer * tap;

@property (nonatomic, assign) CGRect rectEmail;
@property (nonatomic, assign) CGRect rectPassword;

@end

@implementation LoginVC

#pragma mark -
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];
    
    _rectEmail = [_viewLoginFrame convertRect:_viewEmail.frame toView:self.view];
    _rectPassword = [_viewLoginFrame convertRect:_viewPassword.frame toView:self.view];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    _tap.enabled = NO;
    [self.view addGestureRecognizer:_tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillLayoutSubviews {

    [super viewWillLayoutSubviews];
    
    _viewEmail.layer.cornerRadius = CGRectGetHeight(_viewEmail.frame) / 2;
    _viewEmail.layer.borderColor = [COLOR_THEME CGColor];
    _viewEmail.layer.borderWidth = 1.f;
    
    _viewPassword.layer.cornerRadius = CGRectGetHeight(_viewPassword.frame) / 2;
    _viewPassword.layer.borderColor = [COLOR_THEME CGColor];
    _viewPassword.layer.borderWidth = 1.f;
    
    _btnLogin.layer.cornerRadius = CGRectGetHeight(_btnLogin.frame) / 2;
}

#pragma mark -
#pragma mark - Actions

- (IBAction)actionLogin:(id)sender {

    NSString * notifyStr;
    if (![self validateInputValues:&notifyStr]) {
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:notifyStr ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        return;
    }
    
    [self login];
}

- (IBAction)actionForgotPassword:(id)sender {
    
    [self performSegueWithIdentifier:@"SG_FORGOT_PASSWORD" sender:nil];
}

#pragma mark - 
#pragma mark - Backendless Request

- (BOOL)validateInputValues:(NSString**)notifyStr {
    
    if ([_txtEmail.text isEqualToString:@""]) {
        
        *notifyStr = @"Please input email address.";
        return NO;
    }
    
    if (![KCommon validateEmail:_txtEmail.text]) {
        
        *notifyStr = @"Please input valid email address.";
        return NO;
    }
    
    if ([_txtPassword.text isEqualToString:@""]) {
        *notifyStr = @"Please input password.";
        return NO;
    }
    
    return YES;
}

- (void)login {
    
    [KAlert showLoading:@"Loading" parent:self.view];
    
    [backendless.userService login:_txtEmail.text password:_txtPassword.text response:^(BackendlessUser * user) {
        
        KLLog(@"Login Success!");
        [KAlert hideLoading:self.view];
    
        gKAppSetting.userEmail = _txtEmail.text;
        gKAppSetting.userPwd = _txtPassword.text;
        
        [self.navigationController popToRootViewControllerAnimated:NO];
    } error:^(Fault * fault) {
        
        KLLog(@"Login Fail! %@", fault != nil ? fault.description : @"Login Fail!");
        [KAlert hideLoading:self.view];
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:@"Login Fail" Message:@"User name or password is invalid." ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
    }];
}

#pragma mark - 
#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == _txtEmail) {
        
        [_txtPassword becomeFirstResponder];
    } else {
        
        [_txtEmail resignFirstResponder];
        [_txtPassword resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - 
#pragma mark - UIKeyboard Notification

- (void) hideKeyboard {
    
    [_txtEmail resignFirstResponder];
    [_txtPassword resignFirstResponder];
}

- (void) resizeForKeyboard:(NSNotification*)aNotification {
    
    BOOL up = aNotification.name == UIKeyboardWillShowNotification;
    
    NSDictionary * userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationOptions animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    if (up) {
    
        KLLog(@"Keyboard Up");
        CGRect rectEdit;
        rectEdit = [_txtEmail isFirstResponder] ? _rectEmail : _rectPassword;
        
        _consTop.constant = CGRectGetMinY(keyboardEndFrame) - CGRectGetMaxY(rectEdit) - 20;
        _consBottom.constant =  - CGRectGetMinY(keyboardEndFrame) + CGRectGetMaxY(rectEdit);
        
        [UIView animateWithDuration:animationDuration animations:^{
            
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            _tap.enabled = YES;
        }];
    }
    else {
        
        KLLog(@"Keyboard Down");
        _consTop.constant = -20;
        _consBottom.constant = 0;
        [UIView animateWithDuration:animationDuration animations:^{
            
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
           
            _tap.enabled = NO;
        }];
    }
}

@end
