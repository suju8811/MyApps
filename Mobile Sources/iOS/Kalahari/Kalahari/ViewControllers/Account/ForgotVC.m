//
//  ForgotVC.m
//  Kalahari
//
//  Created by LMAN on 3/23/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "ForgotVC.h"

@interface ForgotVC ()

@property (nonatomic, strong) UITapGestureRecognizer * tap;

@property (nonatomic, assign) CGRect rectEmail;

@end

@implementation ForgotVC

#pragma mark - 
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    _rectEmail = [_viewLoginFrame convertRect:_viewEmail.frame toView:self.view];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    _tap.enabled = NO;
    [self.view addGestureRecognizer:_tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillLayoutSubviews {

    [super viewWillLayoutSubviews];
    
    _viewEmail.layer.cornerRadius = CGRectGetHeight(_viewEmail.frame) / 2;
    _viewEmail.layer.borderColor = [COLOR_THEME CGColor];
    _viewEmail.layer.borderWidth = 1.f;
    
    _btnResetPassword.layer.cornerRadius = CGRectGetHeight(_btnResetPassword.frame) / 2;
    _btnLogin.layer.cornerRadius = CGRectGetHeight(_btnLogin.frame)/2;
}

#pragma mark - 
#pragma mark - Actions

- (IBAction)actionResetPassword:(id)sender {
}

- (IBAction)actionLogin:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - Backendless Request

- (BOOL)validateInputValues:(NSString**)notifyStr {
    
    if ([_txtEmail.text isEqualToString:@""]) {
        
        *notifyStr = @"Please input email address.";
        return NO;
    }
    
    if (![KCommon validateEmail:_txtEmail.text]) {
        
        *notifyStr = @"Please input valid email address.";
        return NO;
    }
    
    return YES;
}

- (void)resetPassword {
    
    [KAlert showLoading:@"Resetting Password" parent:self.view];
    
    [backendless.userService restorePassword:_txtEmail.text response:^(id response) {
        
        KLLog(@"Reset Password Success!");
        [KAlert hideLoading:self.view];
    } error:^(Fault * fault) {
        
        KLLog(@"Reset Password Fail! %@", fault != nil ? fault.description : @"Reset Password Fail!");
        [KAlert hideLoading:self.view];
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:@"Reset Password Fail" Message:@"Please input correct email address." ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
    }];
}

#pragma mark -
#pragma mark - UIKeyboard Notification

- (void) hideKeyboard {
    [_txtEmail resignFirstResponder];
}

- (void) resizeForKeyboard:(NSNotification*)aNotification {
    
    BOOL up = aNotification.name == UIKeyboardWillShowNotification;
    
    NSDictionary * userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationOptions animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    if (up) {
        
        KLLog(@"Keyboard Up");

        _consTop.constant = CGRectGetMinY(keyboardEndFrame) - CGRectGetMaxY(_rectEmail) - 20;
        _consBottom.constant =  - CGRectGetMinY(keyboardEndFrame) + CGRectGetMaxY(_rectEmail);
        
        [UIView animateWithDuration:animationDuration animations:^{
            
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            _tap.enabled = YES;
        }];
    }
    else {
        
        KLLog(@"Keyboard Down");
        _consTop.constant = -20;
        _consBottom.constant = 0;
        [UIView animateWithDuration:animationDuration animations:^{
            
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            _tap.enabled = NO;
        }];
    }
}

@end
