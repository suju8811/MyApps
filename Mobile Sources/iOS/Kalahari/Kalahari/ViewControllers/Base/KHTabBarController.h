//
//  KHTabBarController.h
//  Kalahari
//
//  Created by LMAN on 3/24/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KHTabBarController : UITabBarController

@property (nonatomic, assign) BOOL enabled;

@end
