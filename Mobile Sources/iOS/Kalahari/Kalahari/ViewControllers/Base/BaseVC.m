//
//  BaseViewController.m
//  Mandarin
//
//  Created by Michale on 1/21/15.
//  Copyright (c) 2015 Dragon. All rights reserved.
//

#import "BaseVC.h"

@interface BaseVC ()

@end

@implementation BaseVC

#pragma mark - 
#pragma mark - UIView Overrides

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIImage * imgAdd = [UIImage imageNamed:@"btn_nav_add"];
    UIImage * imgHelp = [UIImage imageNamed:@"btn_nav_help"];
    
    _rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:imgHelp landscapeImagePhone:imgHelp style:UIBarButtonItemStylePlain target:self action:@selector(handleHelpMenu)];
    _addItem = [[UIBarButtonItem alloc] initWithImage:imgAdd landscapeImagePhone:imgAdd style:UIBarButtonItemStylePlain target:self action:@selector(handleAddMenu)];
    _helpItem = [[UIBarButtonItem alloc] initWithImage:imgHelp landscapeImagePhone:imgAdd style:UIBarButtonItemStylePlain target:self action:@selector(handleHelpMenu)];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self setupUI];
}

#pragma mark -
#pragma mark - Navigation Helper Methods

- (void)handleBackMenu {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)handleAddMenu {
}

- (void)handleHelpMenu {
}

-(void)backBtnPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setRightBarItemStyle:(MSBarButtonItemStyle)rightStyle {

    _rightStyle = rightStyle;
    
    _rightBarButtonItem = [self getBarButtonItemByStyle:rightStyle];
    
    [self setRightNavigationBarButtonItem];
}

- (void)setBarButtonItemStylesLeft:(MSBarButtonItemStyle) leftStyle Right:(MSBarButtonItemStyle) rightStyle {
    
    _leftStyle = leftStyle;
    _rightStyle = rightStyle;
    
    _leftBarButtonItem = [self getBarButtonItemByStyle:leftStyle];
    _rightBarButtonItem = [self getBarButtonItemByStyle:rightStyle];
    
    [self setNavigationBarButtonItems];
}

- (UIBarButtonItem*)getBarButtonItemByStyle:(MSBarButtonItemStyle)itemStyle {
    
    switch (itemStyle) {
        case MSBarButtonItemStyleAdd:
            return _addItem;
        case MSBarButtonItemStyleHelp:
            return _helpItem;
        default:
            return nil;
    }
}

- (void)setRightNavigationBarButtonItem {
    
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = _rightBarButtonItem;
}

- (void)setNavigationBarButtonItems {
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.leftBarButtonItem = _leftBarButtonItem;
    self.navigationItem.rightBarButtonItem = _rightBarButtonItem;
}

#pragma mark -
#pragma mark - Setup UI

- (void) setupUI {
    
    _topViewSpacing = 10;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:18.0f]};
    
    if (self.navigationController)
        self.navigationController.navigationBarHidden = NO;
    
    self.sidePanGesture = NO;
    
    [self.navigationController setToolbarHidden:YES animated:NO];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav"] forBarMetrics:UIBarMetricsDefault];
    
    [self setRightNavigationBarButtonItem];
}

@end
