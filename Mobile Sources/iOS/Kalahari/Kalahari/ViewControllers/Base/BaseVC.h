//
//  BaseViewController.h
//  Mandarin
//
//  Created by Michale on 1/21/15.
//  Copyright (c) 2015 Dragon. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GADBannerView;

typedef enum {
    MSBarButtonItemStyleNone,
    MSBarButtonItemStyleBack,
    MSBarButtonItemStyleAdd,
    MSBarButtonItemStyleHelp,
} MSBarButtonItemStyle;

@interface BaseVC : UIViewController {
    
    UIBarButtonItem * _leftBarButtonItem;
    UIBarButtonItem * _rightBarButtonItem;
    UIBarButtonItem * _addItem;
    UIBarButtonItem * _helpItem;
}

@property(nonatomic)                BOOL bannerIsVisible;
@property(nonatomic, strong)        GADBannerView *bannerView;
@property(nonatomic, strong)        UIViewController *contr;

@property (nonatomic)               int topViewSpacing;
@property (weak, nonatomic)         IBOutlet UIView *topView;
@property (nonatomic, assign)       BOOL sidePanGesture;
@property (nonatomic)               MSBarButtonItemStyle leftStyle;
@property (nonatomic)               MSBarButtonItemStyle rightStyle;

- (void)setRightBarItemStyle:(MSBarButtonItemStyle)leftStyle;
- (void)setBarButtonItemStylesLeft:(MSBarButtonItemStyle) leftStyle Right:(MSBarButtonItemStyle) rightStyle;
- (void)setupUI;

- (void)handleAddMenu;
- (void)handleHelpMenu;

@end
