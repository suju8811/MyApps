//
//  KHTabBarController.m
//  Kalahari
//
//  Created by LMAN on 3/24/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "KHTabBarController.h"
#import "OrderCriteria.h"
#import "OrderVC.h"
#import "OrderInfoVC.h"
#import "HomeVC.h"

@interface KHTabBarController () <UITabBarControllerDelegate>
@property (nonatomic, assign) NSInteger currentTab;

@end

@implementation KHTabBarController

#pragma mark - 
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];

    KLLog(@"Tabbarcontroller Loaded!");
    
    self.currentTab = 0;
    
    self.enabled = YES;
    
    self.delegate = self;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        
        for (UITabBarItem *tbi in self.tabBar.items) {
            
            tbi.image = [[self image:tbi.selectedImage withColor:[UIColor whiteColor]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
    }
}

- (UIImage *)image:(UIImage*)orgImage withColor:(UIColor *)color {
    
    UIGraphicsBeginImageContextWithOptions(orgImage.size, NO, orgImage.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, orgImage.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, orgImage.size.width, orgImage.size.height);
    CGContextClipToMask(context, rect, orgImage.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    
    _currentTab = tabBarController.selectedIndex;
    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {

    if (_currentTab == tabBarController.selectedIndex) {
        return;
    }
    
    UINavigationController * navVC = (UINavigationController*)viewController;
    [navVC popToRootViewControllerAnimated:NO];
    
    gKAppSetting.bTapChanged = YES;
    
    OrderCriteria * criteria = [[OrderCriteria alloc] init];
    
    switch (tabBarController.selectedIndex) {
            
        case 0:
            break;
        case 1:
            criteria.kind = ORDER_KIND_NEW;
            ((OrderInfoVC*)navVC.topViewController).criteria = criteria;
            break;
        case 2:
            criteria.kind = ORDER_KIND_CURRENT;
            ((OrderVC*)navVC.topViewController).criteria = criteria;
            break;
        case 3:
            criteria.kind = ORDER_KIND_PENDING;
            ((OrderVC*)navVC.topViewController).criteria = criteria;
            break;
        case 4:
            criteria.kind = ORDER_KIND_TRACK;
            ((OrderVC*)navVC.topViewController).criteria = criteria;
            break;
    }
}

@end
