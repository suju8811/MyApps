//
//  NewsLetterDetailVC.h
//  Kalahari
//
//  Created by LMAN on 4/13/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Newsletters.h"

@interface NewsLetterDetailVC : UIViewController

/*
 *  Newsletter to show
 */
@property (nonatomic, strong) Newsletters * newsLetter;

/*
 *  Outlets
 */
@property (weak, nonatomic) IBOutlet UIImageView *imgNewsLetter;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;

@end
