//
//  NewsLetterVC.h
//  Kalahari
//
//  Created by LMAN on 4/13/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsLetterVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblNewsLetter;

@end
