//
//  NewsLetterDetailVC.m
//  Kalahari
//
//  Created by LMAN on 4/13/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "NewsLetterDetailVC.h"
#import "UIImageView+AFNetworking.h"

@interface NewsLetterDetailVC ()

@property (nonatomic, strong) UIImageView * viewBigImage;
@property (nonatomic, strong) UIView * shadowView;

@end

@implementation NewsLetterDetailVC

#pragma mark - 
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillLayoutSubviews {

    [super viewWillLayoutSubviews];
    
    /*
     *  Compose Layout
     */
    _imgNewsLetter.layer.borderColor = [COLOR_THEME CGColor];
    _imgNewsLetter.layer.borderWidth = 1.f;
    _imgNewsLetter.layer.cornerRadius = CGRectGetHeight(_imgNewsLetter.frame) / 2;
    
    /*
     *  Big image showing tap
     */
    _shadowView = [[UIView alloc] init];
    _shadowView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3f];
    _viewBigImage = [[UIImageView alloc] init];
    _viewBigImage.backgroundColor = [UIColor whiteColor];
    _viewBigImage.userInteractionEnabled = YES;
    _viewBigImage.contentMode = UIViewContentModeScaleAspectFit;
    UITapGestureRecognizer * tapImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImage:)];
    _imgNewsLetter.userInteractionEnabled = YES;
    [_imgNewsLetter addGestureRecognizer:tapImage];
    UITapGestureRecognizer * tapBigImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBigImage:)];
    [_viewBigImage addGestureRecognizer:tapBigImageView];
    
    /*
     *  Show values
     */
    [self showValues];
}

#pragma mark - 
#pragma mark - Show Values

- (void)showValues {
    
    if (_newsLetter == nil) {
        return;
    }
    
    [_imgNewsLetter setImageWithURL:[NSURL URLWithString:_newsLetter.image]];
    _lblName.text = _newsLetter.title;
    _lblDetail.text = _newsLetter.shortDescription;
    _lblDesc.text = _newsLetter.longDescription;
}

#pragma mark -
#pragma mark - Actions

- (IBAction)actionBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tapImage:(UITapGestureRecognizer*)recognizer {
    
    [self.tabBarController.view addSubview:_shadowView];
    [self.tabBarController.view addSubview:_viewBigImage];
    [_viewBigImage setImageWithURL:[NSURL URLWithString:_newsLetter.image]];
}

- (void)tapBigImage:(UITapGestureRecognizer*)recognizer {
    
    [_shadowView removeFromSuperview];
    [_viewBigImage removeFromSuperview];
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
