//
//  NewsLetterVC.m
//  Kalahari
//
//  Created by LMAN on 4/13/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "NewsLetterVC.h"
#import "Newsletters.h"
#import "NewsLetterCell.h"
#import "NewsLetterDetailVC.h"
#import "SVPullToRefresh.h"

@interface NewsLetterVC ()

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger total;
@property (nonatomic, strong) NSMutableArray * arrNewsLetter;

@end

@implementation NewsLetterVC

#pragma mark - 
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    /*
     *  Remove blank table view footer
     */
    _tblNewsLetter.tableFooterView = [[UIView alloc] init];
    
    /*
     *  Initialize pagination variable
     */
    _page = 0;
    _total = 0;
    
    [_tblNewsLetter addInfiniteScrollingWithActionHandler:^{
        [self refreshTableView];
    }];

    _arrNewsLetter = [[NSMutableArray alloc] init];
    [self loadNewsLetters];
}

- (void)refreshTableView {
    
    NSInteger page_size = _total / BL_PAGE_SIZE;
    if (_total % BL_PAGE_SIZE != 0)
        page_size++;
    
    if (_page < page_size - 1) {
        
        _page++;
        [self loadNewsLetters];
    } else {
        _tblNewsLetter.showsInfiniteScrolling = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
}

#pragma mark -
#pragma mark - Storyboard prepare segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"SG_NEWS_LETTER_DETAIL"]) {
        
        NewsLetterDetailVC *vc = [segue destinationViewController];
        vc.newsLetter = (Newsletters*)sender;
    }
}

#pragma mark -
#pragma mark - Actions

- (IBAction)actionBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - Backendless Request

- (void)loadNewsLetters {
    
    if (![KCommon checkBLLogin:self]) {
        return;
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.queryOptions.offset = [NSNumber numberWithInteger:BL_PAGE_SIZE*_page];
    query.queryOptions.pageSize = [NSNumber numberWithInteger:BL_PAGE_SIZE];
    query.whereClause = @"show=1";
    query.queryOptions.sortBy = @[@"created DESC"];

    if (_page == 0) {
        [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    }
    [[backendless.persistenceService of:[Newsletters class]] find:query response:^(BackendlessCollection * collection) {
        
        if (_page == 0) {
            [KAlert hideLoading:self.tabBarController.view];
        }
        
        if (collection == nil) {
            return;
        }
        
        if (_total == 0) {
            _total = [collection.totalObjects integerValue];
        }
        
        [_arrNewsLetter addObjectsFromArray:collection.data];
        
        [_tblNewsLetter.infiniteScrollingView stopAnimating];
        
        [_tblNewsLetter performSelector:@selector(reloadData) withObject:nil afterDelay:0.1];
        
    } error:^(Fault * fault) {
        
        if (_page == 0) {
            [KAlert hideLoading:self.view];
        } else {
            [_tblNewsLetter.infiniteScrollingView stopAnimating];
        }
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to load newsletters" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"Failed to load newsletters : %@", fault.description);
    }];
}

#pragma mark -
#pragma mark - UITableView Datasource and Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 72.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_arrNewsLetter == nil) {
        return 0;
    }
    
    return _arrNewsLetter.count;
}

- (NewsLetterCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NewsLetterCell * cell = [tableView dequeueReusableCellWithIdentifier:@"newsletter_cell"];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(NewsLetterCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
     *  Set newsletter title
     */
    Newsletters * newsLetter = [_arrNewsLetter objectAtIndex:indexPath.row];
    cell.lblTitle.text = newsLetter.title;
    KLLog(@"News Letter Title:%@", newsLetter.title);
    
    /*
     *  Set table cell separator inset
     */
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Newsletters * newsLetter = [_arrNewsLetter objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"SG_NEWS_LETTER_DETAIL" sender:newsLetter];
    return indexPath;
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
