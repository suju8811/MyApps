//
//  MenuVC.h
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuVC : UIViewController

/*
 *  Category ID to be shown
 */
@property (nonatomic, assign) NSInteger categoryId;

/*
 *  Outlets
 */
@property (weak, nonatomic) IBOutlet UITableView *tblMenu;

@end
