//
//  MenuCell.h
//  Kalahari
//
//  Created by LMAN on 4/13/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
