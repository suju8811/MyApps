//
//  MenuVC.m
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "MenuVC.h"
#import "Categorys.h"
#import "MenuCell.h"
#import "ProductCriteria.h"
#import "ProductVC.h"
#import "TreatmentsViewController.h"
#import "NewsLetterVC.h"
#import "ContactUsVC.h"

@interface MenuVC ()

/*
 *  Backendless collection for categories
 */
@property (nonatomic, strong) BackendlessCollection * collectionCategorys;

@end

@implementation MenuVC

#pragma mark -
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {

    [super viewDidLoad];
    
    /*
     *  Remove blank table view footer
     */
    _tblMenu.tableFooterView = [[UIView alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    /*
     *  Load categories
     */
    [self loadCategories];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
}

#pragma mark -
#pragma mark - Backendless Request

- (void)loadCategories {
    
    if (![KCommon checkBLLogin:self]) {
        return;
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"parentId=%d", (int)((int)_categoryId == -1 ? 0:_categoryId)];
    query.queryOptions.sortBy = @[@"categoryId ASC"];
    
    [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    [[backendless.persistenceService of:[Categorys class]] find:query response:^(BackendlessCollection * collection) {
        
        [KAlert hideLoading:self.tabBarController.view];
        _collectionCategorys = collection;
        
        [_tblMenu reloadData];
        
    } error:^(Fault * fault) {
        
        [KAlert hideLoading:self.tabBarController.view];
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to load categories" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"Failed to load categories : %@", fault.description);
    }];
}

#pragma mark -
#pragma mark - UITableView Datasource and Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 72.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = 0;
    if (_collectionCategorys != nil) {
        count = _collectionCategorys.data.count;
    }
    
    if (_categoryId == -1) {
        count = count + 3;
    }
    
    return count;
}

- (MenuCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuCell * cell = [tableView dequeueReusableCellWithIdentifier:@"menu_cell"];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(MenuCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
     *  Set category title
     */
    
    if (indexPath.row == _collectionCategorys.data.count) {
        cell.lblTitle.text = @"Treatments";
    }
    else if (indexPath.row == _collectionCategorys.data.count + 1) {
        cell.lblTitle.text = @"Carina's advice";
    }
    else if (indexPath.row == _collectionCategorys.data.count + 2) {
        cell.lblTitle.text = @"Contact Us";
    }
    else {
        Categorys * category = [_collectionCategorys.data objectAtIndex:indexPath.row];
        cell.lblTitle.text = category.name;
    }
    
    /*
     *  Set table cell separator inset
     */
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row >= _collectionCategorys.data.count) {
        
        if (indexPath.row == _collectionCategorys.data.count) {
            
            MenuVC * newMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SID_MENU"];
            newMenuVC.categoryId = 90;
            
            [self.navigationController pushViewController:newMenuVC animated:YES];
        }
        else if (indexPath.row == _collectionCategorys.data.count + 1) {
            
            NewsLetterVC * newsLetterVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SB_NEWS_LETTER"];
            [self.navigationController pushViewController:newsLetterVC animated:YES];
        }
        else if (indexPath.row == _collectionCategorys.data.count + 2) {
            
            ContactUsVC * contactUsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SB_CONTACT_US"];
            [self.navigationController pushViewController:contactUsVC animated:YES];
        }
        
        return indexPath;
    }
    
    Categorys * category = [_collectionCategorys.data
                            objectAtIndex:indexPath.row];
    
    if (category.hasChild) {
        
        MenuVC * newMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SID_MENU"];
        newMenuVC.categoryId = category.categoryId;
        
        [self.navigationController pushViewController:newMenuVC animated:YES];
    }
    else if (category.parentId == 90) {
        
        [self performSegueWithIdentifier:@"SG_TREATMENTS" sender:[NSNumber numberWithInteger:category.categoryId]];
    }
    else {
    
        ProductCriteria * criteria = [[ProductCriteria alloc] init];
        criteria.kind = PRODUCT_KIND_BY_CATEGORY;
        criteria.categoryID = category.categoryId;
        [self performSegueWithIdentifier:@"SG_PRODUCT_FROM_MENU" sender:criteria];
    }
    
    return indexPath;
}


#pragma mark -
#pragma mark - Storyboard prepare segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"SG_PRODUCT_FROM_MENU"]) {
        
        ProductVC *vc = [segue destinationViewController];
        vc.criteria = (ProductCriteria*)sender;
    }
    else if ([[segue identifier] isEqualToString:@"SG_TREATMENTS"]) {
        
        TreatmentsViewController *vc = [segue destinationViewController];
        vc.parentId = ((NSNumber*) sender).integerValue;
    }
}

#pragma mark - 
#pragma mark - Actions

- (IBAction)actionBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
