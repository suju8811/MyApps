//
//  OrderTrackVC.m
//  Kalahari
//
//  Created by LMAN on 4/15/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "OrderTrackVC.h"

#define TRACKING_URL @"http://myportal.bex.co.za/portals/bex/WebQuickTrackDetailViewer.aspx?w="

@interface OrderTrackVC ()

@end

@implementation OrderTrackVC

#pragma mark -
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", TRACKING_URL, _trackingNumber]]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 
#pragma mark - Action

- (IBAction)actionBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
