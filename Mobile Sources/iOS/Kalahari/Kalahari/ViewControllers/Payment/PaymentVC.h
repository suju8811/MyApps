//
//  PaymentVC.h
//  Kalahari
//
//  Created by LMAN on 4/15/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Orders.h"

@interface PaymentVC : UIViewController

@property (nonatomic, strong) Orders * order;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
