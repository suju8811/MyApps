//
//  OrderTrackVC.h
//  Kalahari
//
//  Created by LMAN on 4/15/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderTrackVC : UIViewController

@property (nonatomic, strong) NSString * orderNo;
@property (nonatomic, strong) NSString * trackingNumber;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UILabel *lblEmpty;

@end
