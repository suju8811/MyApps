//
//  PaymentVC.m
//  Kalahari
//
//  Created by LMAN on 4/15/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "PaymentVC.h"
#import "UserManager.h"
#import "HomeVC.h"
#import "OrderDetails.h"

//#define PAYMENT_URL     @"https://sandbox.payfast.co.za/eng/process/?merchant_id=10002751&merchant_key=z5ur80ozsdcuv"
#define PAYMENT_URL     @"https://payfast.co.za/eng/process/?merchant_id=10268056&merchant_key=8l5xkghftotab"

@interface PaymentVC ()

@property (nonatomic, assign) NSInteger retry;
@property (nonatomic, assign) BOOL loadFinished;

@end

@implementation PaymentVC

#pragma mark -
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];

    _loadFinished = NO;
    _retry = 5;
    
    [self performPayment];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 
#pragma mark - Perform Payment

- (void)performPayment {
    
    [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    
    KLLog(@"Perform Payment");
    NSString * payment = [[NSString stringWithFormat:@"%@ - %@ %@", [_order displayedOrderNo], _order.orderName, [_order displayedOrderDate]] stringByRemovingPercentEncoding];
    
    NSString * paymentURLStr = [NSString stringWithFormat:@"%@&email=%@&amount=%2.f&item_name=%@", PAYMENT_URL, [UserManager getUserEmail], _order.totalPrice, payment];

//    NSURL * paymentURL = [NSURL URLWithString:paymentURLStr];
    NSURL * paymentURL = [NSURL URLWithString:[paymentURLStr  stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
    NSURLRequest * paymentURLRequest = [NSURLRequest requestWithURL:paymentURL];
    [_webView loadRequest:paymentURLRequest];
}

#pragma mark - 
#pragma mark - Actions

- (IBAction)actionBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - UIWebView Delegate

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    
    KLLog(@"Payment URL:%@", request.URL);
    KLLog(@"Payment URL String:%@", request.URL.absoluteString);
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [KAlert hideLoading:self.tabBarController.view];

    KLLog(@"Payment URL End:%@", webView.request.URL);
    KLLog(@"Payment URL String End:%@", webView.request.URL.absoluteString);
    
    if (!_loadFinished) {
        
        [self checkWebPageUrl:webView.request.URL.absoluteString];
    }
    
    [webView stringByEvaluatingJavaScriptFromString:@"for (i = 0; i < document.getElementsByClassName(\"wordbreak\").length; i++) {var value = document.getElementsByClassName(\"wordbreak\")[i].innerHTML; document.getElementsByClassName(\"wordbreak\")[i].innerHTML = decodeURI(value);}"];
}

#pragma mark -
#pragma mark - Check URL

- (void)checkWebPageUrl:(NSString*)url {
    
    if (url != nil && [url rangeOfString:@"payfast.co"].location !=NSNotFound && [url rangeOfString:@"process/finish"].location != NSNotFound) {
        
        _loadFinished = YES;
        UIAlertController * notifyAlert = [KAlert alertWithTitle:kAppName Message:@"Payment Sucess!" LeftButton:@"OK" RightButton:nil leftBlock:^(UIAlertAction *action) {
            
            _order.paid = YES;
            [self saveOrders];
        } RightBlock:nil];
        [self presentViewController:notifyAlert animated:YES completion:nil];
    }
}

- (void)saveOrders {
    
    _retry --;
    
    if (_retry < 0) {
        [self onFailed];
    } else {
        [self requestSaveOrders];
        
    }
}

- (void)requestSaveOrders {
    
    [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    
    [[backendless.persistenceService of:[Orders class]] save:_order response:^(Orders * order) {
        if (order != nil) {
            [self performSelector:@selector(onSuccess) withObject:nil afterDelay:0.1];
        } else {
            [self performSelector:@selector(saveOrders) withObject:nil afterDelay:0.1];
        }
    } error:^(Fault * fault) {
        [self performSelector:@selector(saveOrders) withObject:nil afterDelay:0.1];
    }];
}

- (void)onSuccess {
    
    [KAlert hideLoading:self.tabBarController.view];
    
    NSString *subject = @"New salon app order";
    NSString *body = [NSString stringWithFormat:@"Order ID: %@, Amount:%.2f <br/>", _order.displayedOrderNo, _order.totalPrice];
    NSString *recipient = @"orders@kalaharistyle.com";
    
    NSMutableString * bodyMutable = [NSMutableString stringWithString:body];
    
    [bodyMutable appendFormat:@"Salon Name: %@<br/>", [UserManager getUserName]];
    [bodyMutable appendFormat:@"Client Name: %@<br/>", [UserManager getClientName]];
    [bodyMutable appendFormat:@"Delivery Address: %@<br/>", [UserManager getDeliveryAddress]];
    [bodyMutable appendFormat:@"Cell Number: %@<br/>", [UserManager getCellNumber]];
    
    [bodyMutable appendString:@"<br/>Products <br/>"];
    
    int i = 1;
    for (OrderDetails * orderDetail in _order.details) {
        
        [bodyMutable appendFormat:@"%d   %@      R%.2f x %d<br/>", i, orderDetail.product.name, orderDetail.product.price, (int)orderDetail.count];
        i++;
    }
    
    CGFloat delivery_fee = [[[backendless.userService login:gKAppSetting.userEmail password:gKAppSetting.userPwd] getProperty:@"deliveryFee"] integerValue];
    
    [bodyMutable appendFormat:@"Delivery Fee    R%.2f<br/>", delivery_fee];
    [bodyMutable appendFormat:@"Total           R%.2f", _order.totalPrice];
    
    [backendless.messagingService sendHTMLEmail:subject body:bodyMutable to:@[recipient]];
    
    OrderCriteria * criteria = [[OrderCriteria alloc] init];
    criteria.kind = ORDER_KIND_PENDING;
    [KCommon gotoOrderFrom:self criteria:criteria];
}

- (void)onFailed {
    
    [KAlert hideLoading:self.tabBarController.view];
    
    UIAlertController * alert = [KAlert alertWithTitle:kAppName Message:@"Payment Success, but failed to save data!\nPlease contact us your details!" LeftButton:@"OK" RightButton:nil leftBlock:^(UIAlertAction *action) {
        [self.tabBarController setSelectedIndex:0];
    } RightBlock:nil];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
