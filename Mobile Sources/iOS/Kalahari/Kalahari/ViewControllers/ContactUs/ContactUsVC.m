//
//  ContactUsVC.m
//  Kalahari
//
//  Created by LMAN on 4/13/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "ContactUsVC.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface ContactUsVC () <MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) UITapGestureRecognizer * tap;

@end

@implementation ContactUsVC

#pragma mark -
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    /**
     *  Set tap gesture recognizer for keyboard
     */
    
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    _tap.enabled = NO;
    [self.view addGestureRecognizer:_tap];
    
    /**
     *  Set keyboard notification observer
     */
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillLayoutSubviews {

    [super viewWillLayoutSubviews];
    
    /*
     *  Compose Layout
     */
    _btnSubmit.layer.borderColor = [COLOR_THEME CGColor];
    _btnSubmit.layer.borderWidth = 1.f;
    _btnSubmit.layer.cornerRadius = CGRectGetHeight(_btnSubmit.frame) / 2;
    
    _editMsg.layer.borderColor = [COLOR_MSG_BORDER CGColor];
    _editMsg.layer.borderWidth = 1.4;
    _editMsg.layer.cornerRadius = 5.f;
}

#pragma mark -
#pragma mark - Actions

- (IBAction)actionBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionSubmit:(id)sender {
    
    NSString * message = _editMsg.text;
    
    if (message.length == 0) {
    
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Please input message" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        return;
    }
    
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    [controller setToRecipients:[NSArray arrayWithObjects:@"info@kalaharistyle.com", nil]];
    [controller setSubject:@"Contact Us"];
    [controller setMessageBody:message isHTML:NO];
    controller.mailComposeDelegate = self;
    
    if (controller) {
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark -
#pragma mark - MFMailComposeViewControllerDelegate

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
        {
            break;
        }
        case MFMailComposeResultFailed:
        {
            break;
        }
        case MFMailComposeResultSent:
        {
        }
        default:
            break;
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - UIKeyboard Notification

- (void) hideKeyboard {
    
    [_editMsg resignFirstResponder];
}

- (void) resizeForKeyboard:(NSNotification*)aNotification {
    
    BOOL up = aNotification.name == UIKeyboardWillShowNotification;
    
    NSDictionary * userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationOptions animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    if (up) {
        
        KLLog(@"Keyboard Up");
        
        _consBottomScrollView.constant = CGRectGetHeight(keyboardEndFrame) - TAB_BAR_HEIGHT;
        
        [UIView animateWithDuration:animationDuration animations:^{
            
            [self.view layoutIfNeeded];
            
            CGPoint bottomOffset = CGPointMake(0, _scrollView.contentSize.height - _scrollView.bounds.size.height);
            
            if (bottomOffset.y > 0) {
                [self.scrollView setContentOffset:bottomOffset animated:NO];
            }
        } completion:^(BOOL finished) {
            
            _tap.enabled = YES;
        }];
    }
    else {
        
        KLLog(@"Keyboard Down");
        
        _consBottomScrollView.constant = 0;
        [UIView animateWithDuration:animationDuration animations:^{
            
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            _tap.enabled = NO;
        }];
    }
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
