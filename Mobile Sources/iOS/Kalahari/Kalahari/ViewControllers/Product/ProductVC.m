//
//  ProductVC.m
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "ProductVC.h"
#import "Products.h"
#import "UserManager.h"
#import "ProductCell.h"
#import "UserManager.h"
#import "UIImageView+AFNetworking.h"
#import "ProductDetailVC.h"
#import "SVPullToRefresh.h"

@interface ProductVC () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger total;
@property (nonatomic, assign) NSInteger preLast;
@property (nonatomic, strong) NSMutableArray * arrProducts;

@end

@implementation ProductVC

#pragma mark - 
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    /*
     *  Remove table view footer
     */
    _tblProducts.tableFooterView = [[UIView alloc] init];
    
    /*
     *  Initialize criteria
     */
    if (_criteria == nil) {
        _criteria = [[ProductCriteria alloc] init];
    }
    
    /*
     *  Init products array
     */
    _page = 0;
    _total = 0;
    _arrProducts = [[NSMutableArray alloc] init];
    
    [_tblProducts addInfiniteScrollingWithActionHandler:^{
        [self refreshTableView];
    }];
    
    [self loadProducts];
}

- (void)refreshTableView {
    
    NSInteger page_size = _total / BL_PAGE_SIZE;
    if (_total % BL_PAGE_SIZE != 0)
        page_size++;
    
    if (_page < page_size - 1) {
        
        _page++;
        [self loadProducts];
    } else {
        _tblProducts.showsInfiniteScrolling = NO;
    }
}

- (void)insertRowsAtBottom:(NSMutableArray*)arrInsert {
    
    __weak ProductVC *weakSelf = self;
    
    int64_t delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        NSUInteger rowCount = [weakSelf.tblProducts numberOfRowsInSection:0];
        NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < arrInsert.count; i++) {
            
            [weakSelf.arrProducts addObject:[arrInsert objectAtIndex:i]];
            
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i+rowCount - 1 inSection:0];
            [indexPaths addObject:indexpath];
        }
        
        [weakSelf.tblProducts beginUpdates];
        
        [weakSelf.tblProducts insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        [weakSelf.tblProducts endUpdates];
        
        [weakSelf.tblProducts.infiniteScrollingView stopAnimating];
    });
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

#pragma mark - 
#pragma mark - Backendless request

- (void)loadProducts {
    
    if (![KCommon checkBLLogin:self]) {
        return;
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.queryOptions.offset = [NSNumber numberWithInteger:BL_PAGE_SIZE*_page];
    query.queryOptions.pageSize = [NSNumber numberWithInteger:BL_PAGE_SIZE];
    
    NSString * where  = @"";
    if (_criteria.kind == PRODUCT_KIND_BY_CATEGORY) {
        where = [NSString stringWithFormat:@"categoryId=%d", (int)_criteria.categoryID];
    }
    
    if (_criteria.kind == PRODUCT_KIND_BY_SPECIAL) {
        where = @"special=1";
    }
    
    if (_criteria.kind == PRODUCT_KIND_BY_SEARCH) {
        where = [NSString stringWithFormat:@"name like '%%%@%%' or tag1='%@' or tag2='%@' or tag3='%@' or tag4='%@' or tag5='%@' or longDescription like '%%%@%%' or shortDescription like '%%%@%%' or swopDescription like '%%%@%%' ", _criteria.searchStr, _criteria.searchStr, _criteria.searchStr, _criteria.searchStr, _criteria.searchStr, _criteria.searchStr, _criteria.searchStr, _criteria.searchStr, _criteria.searchStr];
    }
    
    BOOL isOwner = [UserManager isOwner];
    if (!isOwner) {
        where = [NSString stringWithFormat:@"( %@ )  and special=0 ", where];
    }
    
    query.whereClause = where;
    query.queryOptions.sortBy = @[@"ProductOrder ASC"];
    
    if (_page == 0) {
        [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    }
    
    [[backendless.persistenceService of:[Products class]] find:query response:^(BackendlessCollection * collection) {
        
        if (_page == 0) {
            [KAlert hideLoading:self.tabBarController.view];
        }
        
        if (collection == nil) {
            return;
        }
        
        if (_total == 0) {
            _total = [collection.totalObjects integerValue];
        }
        
        for (Products * product in collection.data) {
            
            if (!isOwner && product.special) {
                continue;
            }
            
            [_arrProducts addObject:product];
        }
        
        [_tblProducts.infiniteScrollingView stopAnimating];
        
        [_tblProducts reloadData];
        
    } error:^(Fault * fault) {
        
        if (_page == 0) {
            [KAlert hideLoading:self.tabBarController.view];
        } else {
            [_tblProducts.infiniteScrollingView stopAnimating];
        }
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to load products" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"Failed to load special : %@", fault.description);
    }];
}

#pragma mark -
#pragma mark - UITableView Datasource and Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_arrProducts == nil) {
        return 0;
    }
    
    return _arrProducts.count;
}

- (ProductCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductCell * cell = [tableView dequeueReusableCellWithIdentifier:@"product_cell"];
    [cell.btnAddCart addTarget:self action:@selector(actionAddCart:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDetail addTarget:self action:@selector(actionDetail:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDetail1 addTarget:self action:@selector(actionDetail:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(ProductCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Products * product = [_arrProducts objectAtIndex:indexPath.row];
    
    if (product.special) {
        
        [cell.btnDetail setTitle:@"VIEW SPECIAL" forState:UIControlStateNormal];
        [cell.btnDetail1 setTitle:@"VIEW SPECIAL" forState:UIControlStateNormal];
    } else {
        [cell.btnDetail setTitle:@"VIEW PRODUCT" forState:UIControlStateNormal];
        [cell.btnDetail1 setTitle:@"VIEW PRODUCT" forState:UIControlStateNormal];
    }
    
    cell.lblName.text = product.name;
    cell.lblDetail.text = product.shortDescription;
    cell.lblPrice.text = [NSString stringWithFormat:@"R %.2f", product.price];
    cell.lblTag.text = [product displayedTag];
    
    if ([UserManager isOwner]) {
        
        cell.btnAddCart.hidden = NO;
        cell.btnDetail.hidden = NO;
        cell.btnDetail1.hidden = YES;
        cell.buttonSeparator.hidden = NO;
        
    } else {
        
        cell.btnAddCart.hidden = YES;
        cell.btnDetail.hidden = YES;
        cell.btnDetail1.hidden = NO;
        cell.buttonSeparator.hidden = YES;
    }

    [cell.imgProduct setImageWithURL:[NSURL URLWithString:product.image]];
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    KLLog(@"Table Cell Selected");
    [self performSegueWithIdentifier:@"SG_PRODUCT_DETAIL" sender:indexPath];
    return indexPath;
}

#pragma mark -
#pragma mark - Storyboard prepare segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"SG_PRODUCT_DETAIL"]) {
        
        ProductDetailVC *vc = [segue destinationViewController];
        NSIndexPath * indexPath = (NSIndexPath*)sender;
        vc.product = [_arrProducts objectAtIndex:indexPath.row];
        vc.kind = PRODUCT_DETAIL_KIND_ADD;
    }
}

#pragma mark - 
#pragma mark - TableView Cell Action

- (IBAction)actionBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)actionAddCart:(id)sender {
    
    /**
     *  Get product at index
     */
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tblProducts];
    NSIndexPath *indexPath = [_tblProducts indexPathForRowAtPoint:buttonPosition];
    Products * product = [_arrProducts objectAtIndex:indexPath.row];
    
    /**
     *  Should go to order info fragment
     */
    
    OrderCriteria * criteria = [[OrderCriteria alloc] init];
    criteria.kind = ORDER_KIND_CURRENT;
    criteria.orderNo = gKAppSetting.currentOrderNo;
    criteria.product = product;
    criteria.count = 1;
    [KCommon gotoOrderInfoFrom:self criteria:criteria];
}

- (void)actionDetail:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tblProducts];
    NSIndexPath *indexPath = [_tblProducts indexPathForRowAtPoint:buttonPosition];
    
    KLLog(@"Detail Button Selected At:%d", (int)indexPath.row);
    [self performSegueWithIdentifier:@"SG_PRODUCT_DETAIL" sender:indexPath];
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
