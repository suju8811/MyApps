//
//  ProductDetailVC.h
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Products.h"

@interface ProductDetailVC : UIViewController

/*
 *  Product variables
 */
@property (nonatomic, strong) Products * product;
@property (nonatomic, assign) ProductDetailKind kind;

/*
 *  Outlets
 */
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBottomScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imgProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblTag;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UIView *viewAmount;
@property (weak, nonatomic) IBOutlet UITextField *txtAmount;

@property (weak, nonatomic) IBOutlet UIView *viewCart;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToCart;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToCart2;
@property (weak, nonatomic) IBOutlet UIView *viewShow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consBottomShowView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consTopShowView;
@property (weak, nonatomic) IBOutlet UIButton *btnLearnMore;
@property (weak, nonatomic) IBOutlet UIButton *btnLearnMore2;

@end
