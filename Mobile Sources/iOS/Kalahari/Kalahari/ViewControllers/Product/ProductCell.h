//
//  ProductCell.h
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UITableViewCell

/**
 *  Image view
 */

@property (weak, nonatomic) IBOutlet UIImageView *imgProduct;

/**
 *  Content labels
 */

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblTag;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

/**
 *  Separator
 */

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consSeparator;
@property (weak, nonatomic) IBOutlet UIView *buttonSeparator;


/**
 *  Buttons
 */

@property (weak, nonatomic) IBOutlet UIView *viewButtons;
@property (weak, nonatomic) IBOutlet UIButton *btnAddCart;
@property (weak, nonatomic) IBOutlet UIButton *btnDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnDetail1;

@end
