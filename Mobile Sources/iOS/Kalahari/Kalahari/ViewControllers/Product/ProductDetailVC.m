//
//  ProductDetailVC.m
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "ProductDetailVC.h"
#import "UIImageView+AFNetworking.h"
#import "UserManager.h"
#import "KAlert.h"
#import "ProductSwopVC.h"
#import "OrderInfoVC.h"
#import "OrderManager.h"

@interface ProductDetailVC ()

@property (nonatomic, strong) UITapGestureRecognizer * tap;
@property (nonatomic, strong) UIImageView * viewBigImage;
@property (nonatomic, strong) UIView * shadowView;

@end

@implementation ProductDetailVC

#pragma mark -
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    /*
     *  Set tap gesture recognizer for keyboard
     */
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    _tap.enabled = NO;
    [self.view addGestureRecognizer:_tap];
    
    /*
     *  Set keyboard notification observer
     */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resizeForKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    
    /*
     *  Big image showing tap
     */
    _shadowView = [[UIView alloc] init];
    _shadowView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3f];
    _viewBigImage = [[UIImageView alloc] init];
    _viewBigImage.backgroundColor = [UIColor whiteColor];
    _viewBigImage.userInteractionEnabled = YES;
    _viewBigImage.contentMode = UIViewContentModeScaleAspectFit;
    UITapGestureRecognizer * tapImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImage:)];
    _imgProduct.userInteractionEnabled = YES;
    [_imgProduct addGestureRecognizer:tapImage];
    UITapGestureRecognizer * tapBigImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBigImage:)];
    [_viewBigImage addGestureRecognizer:tapBigImageView];
    
    /*
     *  Show values
     */
    [self showValues];
}

- (void)showValues {
    
    [_imgProduct setImageWithURL:[NSURL URLWithString:_product.image]];
    
    _lblName.text = _product.name;
    _lblDetail.text = _product.shortDescription;
    _lblPrice.text = [_product displayedAll];
    _lblDesc.text = _product.longDescription;
    _lblTag.text = [_product displayedTag];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    /*
     *  Intialize Big Image View
     */
    CGRect screenFrame = self.tabBarController.view.frame;
    _shadowView.frame = screenFrame;
    _viewBigImage.frame = CGRectInset(screenFrame, 20, 20);
    
    /*
     *  Compose Layout
     */
    _imgProduct.layer.borderColor = [COLOR_THEME CGColor];
    _imgProduct.layer.borderWidth = 1.f;
    _imgProduct.layer.cornerRadius = CGRectGetHeight(_imgProduct.frame) / 2;
    
    _txtAmount.layer.borderColor = [COLOR_THEME CGColor];
    _txtAmount.layer.borderWidth = 1.f;
    
    _btnAddToCart.layer.cornerRadius = CGRectGetHeight(_btnAddToCart.frame) / 2;
    _btnAddToCart.layer.borderColor = [COLOR_THEME CGColor];
    _btnAddToCart.layer.borderWidth = 1.f;
    
    _btnAddToCart2.layer.cornerRadius = CGRectGetHeight(_btnAddToCart2.frame) / 2;
    _btnAddToCart2.layer.borderColor = [COLOR_THEME CGColor];
    _btnAddToCart2.layer.borderWidth = 1.f;
    
    _btnLearnMore.layer.cornerRadius = CGRectGetHeight(_btnLearnMore.frame) / 2;
    _btnLearnMore.layer.borderColor = [COLOR_THEME CGColor];
    _btnLearnMore.layer.borderWidth = 1.f;
    
    _btnLearnMore2.layer.cornerRadius = CGRectGetHeight(_btnLearnMore2.frame) / 2;
    _btnLearnMore2.layer.borderColor = [COLOR_THEME CGColor];
    _btnLearnMore2.layer.borderWidth = 1.f;
    
    if (![UserManager isOwner]) {
        _kind = PRODUCT_DETAIL_KIND_VIEW;
    }
    
    if (_kind == PRODUCT_DETAIL_KIND_VIEW) {
        
        _viewCart.hidden = YES;
        if (_product.swopDescription.length == 0) {
            _consBottomShowView.constant =  - CGRectGetHeight(_viewShow.frame) - CGRectGetHeight(_viewCart.frame);
            _viewShow.hidden = YES;
        } else {
            _consTopShowView.constant = - CGRectGetHeight(_viewCart.frame);
        }
    } else {
        _consBottomShowView.constant = - CGRectGetHeight(_viewShow.frame);
        
        _viewShow.hidden = YES;
        if (_product.swopDescription.length == 0) {
            
            _btnAddToCart2.hidden = YES;
            _btnLearnMore2.hidden = YES;
        } else {
            
            _btnAddToCart.hidden = YES;
        }
    }
}

#pragma mark -
#pragma mark - Storyboard prepare segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"SG_PRODUCT_SWOP"]) {
        
        ProductSwopVC *vc = [segue destinationViewController];
        vc.products = sender;
    }
}

#pragma mark -
#pragma mark - UIKeyboard Notification

- (void) hideKeyboard {
    
    [_txtAmount resignFirstResponder];
}

- (void) resizeForKeyboard:(NSNotification*)aNotification {
    
    BOOL up = aNotification.name == UIKeyboardWillShowNotification;
    
    NSDictionary * userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationOptions animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    if (up) {
        
        KLLog(@"Keyboard Up");
        
        _consBottomScrollView.constant = CGRectGetHeight(keyboardEndFrame) - 49;
        
        [UIView animateWithDuration:animationDuration animations:^{
            
            [self.view layoutIfNeeded];
            
            CGPoint bottomOffset = CGPointMake(0, _scrollView.contentSize.height - _scrollView.bounds.size.height);
            
            if (bottomOffset.y > 0) {
                [self.scrollView setContentOffset:bottomOffset animated:NO];
            }
        } completion:^(BOOL finished) {
            
            _tap.enabled = YES;
        }];
    }
    else {
        
        KLLog(@"Keyboard Down");
        
        _consBottomScrollView.constant = 0;
        [UIView animateWithDuration:animationDuration animations:^{
            
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            _tap.enabled = NO;
        }];
    }
}

#pragma mark - 
#pragma mark - Actions

- (void)tapImage:(UITapGestureRecognizer*)recognizer {

    [self.tabBarController.view addSubview:_shadowView];
    [self.tabBarController.view addSubview:_viewBigImage];
    [_viewBigImage setImageWithURL:[NSURL URLWithString:_product.image]];
}

- (void)tapBigImage:(UITapGestureRecognizer*)recognizer {
    
    [_shadowView removeFromSuperview];
    [_viewBigImage removeFromSuperview];
}

- (IBAction)actionBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionAmountDec:(id)sender {
    
    NSInteger amount = [_txtAmount.text integerValue];
    amount --;
    
    if (amount < 0) {amount = 0;}
    
    _txtAmount.text = [NSString stringWithFormat:@"%d", (int)amount];
}

- (IBAction)actionAmountInc:(id)sender {
    
    NSInteger amount = [_txtAmount.text integerValue];
    amount ++;
    _txtAmount.text = [NSString stringWithFormat:@"%d", (int)amount];
}

- (IBAction)actionAddToCart:(id)sender {
    
    /*
     *  Check amount
     */
    NSInteger amount = [_txtAmount.text integerValue];
    if (amount == 0) {
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Please input amount." ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        return;
    }
    
    /*
     *  Should go to order info fragment
     */
    OrderCriteria * criteria = [[OrderCriteria alloc] init];
    criteria.kind = ORDER_KIND_CURRENT;
    criteria.orderNo = gKAppSetting.currentOrderNo;
    criteria.product = _product;
    criteria.count = [_txtAmount.text integerValue];
    [KCommon gotoOrderInfoFrom:self criteria:criteria];
}

- (IBAction)actionLearnMore:(id)sender {
    
    [self performSegueWithIdentifier:@"SG_PRODUCT_SWOP" sender:_product];
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
