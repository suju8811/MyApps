//
//  ProductCell.m
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "ProductCell.h"

@implementation ProductCell

- (void)awakeFromNib {
    
    [super awakeFromNib];

    _btnAddCart.layer.cornerRadius = CGRectGetHeight(_btnAddCart.frame) / 2;
    _btnAddCart.layer.borderColor = [COLOR_THEME CGColor];
    _btnAddCart.layer.borderWidth = 1.f;
    
    _btnDetail.layer.cornerRadius = CGRectGetHeight(_btnDetail.frame) / 2;
    _btnDetail.layer.borderColor = [COLOR_THEME CGColor];
    _btnDetail.layer.borderWidth = 1.f;
}

@end
