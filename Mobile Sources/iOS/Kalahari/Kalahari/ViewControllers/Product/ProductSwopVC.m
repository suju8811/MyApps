//
//  ProductSwopVC.m
//  Kalahari
//
//  Created by LMAN on 4/16/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "ProductSwopVC.h"
#import "UIImageView+AFNetworking.h"

@interface ProductSwopVC ()

@property (nonatomic, strong) UIImageView * viewBigImage;
@property (nonatomic, strong) UIView * shadowView;

@end

@implementation ProductSwopVC

#pragma mark - 
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    /*
     *  Compose Layout
     */
    _imgProduct.layer.borderColor = [COLOR_THEME CGColor];
    _imgProduct.layer.borderWidth = 1.f;
    _imgProduct.layer.cornerRadius = CGRectGetHeight(_imgProduct.frame) / 2;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    /*
     *  Big image showing tap
     */
    _shadowView = [[UIView alloc] init];
    _shadowView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3f];
    _viewBigImage = [[UIImageView alloc] init];
    _viewBigImage.backgroundColor = [UIColor whiteColor];
    _viewBigImage.userInteractionEnabled = YES;
    _viewBigImage.contentMode = UIViewContentModeScaleAspectFit;
    UITapGestureRecognizer * tapImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImage:)];
    _imgProduct.userInteractionEnabled = YES;
    [_imgProduct addGestureRecognizer:tapImage];
    UITapGestureRecognizer * tapBigImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBigImage:)];
    [_viewBigImage addGestureRecognizer:tapBigImageView];
    
    [self showValues];
}

#pragma mark - 
#pragma mark - Show values

- (void)showValues {

    [_imgProduct setImageWithURL:[NSURL URLWithString:_products.image]];
    _lblName.text = _products.name;
    _lblDetail.text = _products.shortDescription;
    _lblTag.text = [_products displayedTag];
    _lblPrice.text = [_products displayedAll];
    _lblDesc.text = _products.swopDescription;
}

#pragma mark - 
#pragma mark - Actions

- (IBAction)actionBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tapImage:(UITapGestureRecognizer*)recognizer {
    
    [self.tabBarController.view addSubview:_shadowView];
    [self.tabBarController.view addSubview:_viewBigImage];
    [_viewBigImage setImageWithURL:[NSURL URLWithString:_products.image]];
}

- (void)tapBigImage:(UITapGestureRecognizer*)recognizer {
    
    [_shadowView removeFromSuperview];
    [_viewBigImage removeFromSuperview];
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


@end
