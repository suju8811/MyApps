//
//  ProductSwopVC.h
//  Kalahari
//
//  Created by LMAN on 4/16/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Products.h"

@interface ProductSwopVC : UIViewController

@property (nonatomic, strong) Products * products;

/*
 *  Outlets
 */
@property (weak, nonatomic) IBOutlet UIImageView *imgProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblTag;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;

@end
