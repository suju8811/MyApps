//
//  ProductVC.h
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductCriteria.h"

@interface ProductVC : UIViewController

/*
 *  Product filtering criteria
 */
@property (nonatomic, strong) ProductCriteria * criteria;

/*
 *  Outlet for table view
 */
@property (weak, nonatomic) IBOutlet UITableView *tblProducts;

@end
