//
//  TreatmentsViewController.h
//  Kalahari
//
//  Created by Michael Lee on 11/19/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TreatmentsViewController : UIViewController

@property (nonatomic, assign)   NSInteger parentId;
@property (weak, nonatomic) IBOutlet UITableView *tblTreatment;

@end
