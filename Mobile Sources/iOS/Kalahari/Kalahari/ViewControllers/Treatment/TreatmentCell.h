//
//  TreatmentCell.h
//  Kalahari
//
//  Created by Michael Lee on 11/19/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TreatmentCell : UITableViewCell

/**
 *  Outlet
 */

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
