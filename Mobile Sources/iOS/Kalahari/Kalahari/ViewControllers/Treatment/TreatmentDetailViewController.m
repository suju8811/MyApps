//
//  TreatmentDetailViewController.m
//  Kalahari
//
//  Created by Michael Lee on 11/19/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "TreatmentDetailViewController.h"
#import "UIImageView+AFNetworking.h"

@interface TreatmentDetailViewController ()

@property (nonatomic, strong) UIImageView * viewBigImage;
@property (nonatomic, strong) UIView * shadowView;

@end

@implementation TreatmentDetailViewController

#pragma mark -
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.view layoutIfNeeded];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    /*
     *  Compose Layout
     */
    _imgTreatment.layer.borderColor = [COLOR_THEME CGColor];
    _imgTreatment.layer.borderWidth = 1.f;
    _imgTreatment.layer.cornerRadius = CGRectGetHeight(_imgTreatment.frame) / 2;
    
    /*
     *  Big image showing tap
     */
    _shadowView = [[UIView alloc] init];
    _shadowView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3f];
    _viewBigImage = [[UIImageView alloc] init];
    _viewBigImage.backgroundColor = [UIColor whiteColor];
    _viewBigImage.userInteractionEnabled = YES;
    _viewBigImage.contentMode = UIViewContentModeScaleAspectFit;
    UITapGestureRecognizer * tapImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImage:)];
    _imgTreatment.userInteractionEnabled = YES;
    [_imgTreatment addGestureRecognizer:tapImage];
    UITapGestureRecognizer * tapBigImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBigImage:)];
    [_viewBigImage addGestureRecognizer:tapBigImageView];
    
    /**
     *  Show values
     */
    [self showValues];
}

#pragma mark -
#pragma mark - Show Values

- (void)showValues {
    
    if (_treatment == nil) {
        return;
    }
    
    [_imgTreatment setImageWithURL:[NSURL URLWithString:_treatment.image]];
    _lblName.text = _treatment.title;
    _lblDetail.text = _treatment.shortDescription;
    _lblDesc.text = _treatment.longDescription;
}

#pragma mark -
#pragma mark - Actions

- (IBAction)actionBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tapImage:(UITapGestureRecognizer*)recognizer {
    
    [self.tabBarController.view addSubview:_shadowView];
    [self.tabBarController.view addSubview:_viewBigImage];
    [_viewBigImage setImageWithURL:[NSURL URLWithString:_treatment.image]];
}

- (void)tapBigImage:(UITapGestureRecognizer*)recognizer {
    
    [_shadowView removeFromSuperview];
    [_viewBigImage removeFromSuperview];
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
