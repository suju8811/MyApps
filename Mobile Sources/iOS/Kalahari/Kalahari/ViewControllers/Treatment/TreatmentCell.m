//
//  TreatmentCell.m
//  Kalahari
//
//  Created by Michael Lee on 11/19/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "TreatmentCell.h"

@implementation TreatmentCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
