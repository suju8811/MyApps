//
//  TreatmentsViewController.m
//  Kalahari
//
//  Created by Michael Lee on 11/19/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "TreatmentsViewController.h"
#import "SVPullToRefresh.h"
#import "Treatments.h"
#import "TreatmentCell.h"
#import "TreatmentDetailViewController.h"

@interface TreatmentsViewController ()

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger total;
@property (nonatomic, strong) NSMutableArray * arrTreatment;

@end

@implementation TreatmentsViewController

#pragma mark - UIView Lifecycle and Init
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self initVariables];
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    [self initControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Init Variables

- (void)initVariables {
    
    /**
     *  Initialize pagination variable
     */
    
    _page = 0;
    _total = 0;
}

#pragma mark - Init Controls

- (void)initControls {
    
    /**
     *  Remove blank table view footer
     */
    
    _tblTreatment.tableFooterView = [[UIView alloc] init];
    
    [_tblTreatment addInfiniteScrollingWithActionHandler:^{
        [self refreshTableView];
    }];
    
    _arrTreatment = [[NSMutableArray alloc] init];
    [self loadTreatments];
}

- (void)refreshTableView {
    
    NSInteger page_size = _total / BL_PAGE_SIZE;
    if (_total % BL_PAGE_SIZE != 0)
        page_size++;
    
    if (_page < page_size - 1) {
        
        _page++;
        [self loadTreatments];
    } else {
        _tblTreatment.showsInfiniteScrolling = NO;
    }
}

#pragma mark -
#pragma mark - Storyboard prepare segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"SG_TREATMENT_DETAIL"]) {
        
        TreatmentDetailViewController *vc = [segue destinationViewController];
        vc.treatment = (Treatments*)sender;
    }
}

#pragma mark -
#pragma mark - Backendless Request

- (void)loadTreatments {
    
    if (![KCommon checkBLLogin:self]) {
        return;
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.queryOptions.offset = [NSNumber numberWithInteger:BL_PAGE_SIZE*_page];
    query.queryOptions.pageSize = [NSNumber numberWithInteger:BL_PAGE_SIZE];
    query.whereClause = [NSString stringWithFormat:@"show=1 and categoryId=%d", (int)_parentId];
    query.queryOptions.sortBy = @[@"created DESC"];
    
    if (_page == 0) {
        [KAlert showLoading:@"Loading" parent:self.tabBarController.view];
    }
    [[backendless.persistenceService of:[Treatments class]] find:query response:^(BackendlessCollection * collection) {
        
        if (_page == 0) {
            [KAlert hideLoading:self.tabBarController.view];
        }
        
        if (collection == nil) {
            return;
        }
        
        if (_total == 0) {
            _total = [collection.totalObjects integerValue];
        }
        
        [_arrTreatment addObjectsFromArray:collection.data];
        
        [_tblTreatment.infiniteScrollingView stopAnimating];
        
        [_tblTreatment performSelector:@selector(reloadData) withObject:nil afterDelay:0.1];
        
    } error:^(Fault * fault) {
        
        if (_page == 0) {
            [KAlert hideLoading:self.view];
        } else {
            [_tblTreatment.infiniteScrollingView stopAnimating];
        }
        
        UIAlertController * notifyAlert = [KAlert notifyAlertWithTitle:kAppName Message:@"Failed to load newsletters" ButtonTitle:@"OK"];
        [self presentViewController:notifyAlert animated:YES completion:nil];
        KLLog(@"Failed to load newsletters : %@", fault.description);
    }];
}

#pragma mark -
#pragma mark - UITableView Datasource and Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 72.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_arrTreatment == nil) {
        return 0;
    }
    
    return _arrTreatment.count;
}

- (TreatmentCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TreatmentCell * cell = [tableView dequeueReusableCellWithIdentifier:@"treatment_cell"];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(TreatmentCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /**
     *  Set newsletter title
     */
    
    Treatments * treatment = [_arrTreatment objectAtIndex:indexPath.row];
    cell.lblTitle.text = treatment.title;
    KLLog(@"Treatment Title:%@", treatment.title);
    
    /**
     *  Set table cell separator inset
     */
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Treatments * treatment = [_arrTreatment objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"SG_TREATMENT_DETAIL" sender:treatment];
    return indexPath;
}

#pragma mark -
#pragma mark - Preferred Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Actions and Its Processes
#pragma mark - Actions

- (IBAction)actionBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
