//
//  TreatmentDetailViewController.h
//  Kalahari
//
//  Created by Michael Lee on 11/19/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Treatments.h"

@interface TreatmentDetailViewController : UIViewController

@property (nonatomic, strong)   Treatments * treatment;

/**
 *  Outlets
 */

@property (weak, nonatomic) IBOutlet UIImageView *imgTreatment;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;

@end
