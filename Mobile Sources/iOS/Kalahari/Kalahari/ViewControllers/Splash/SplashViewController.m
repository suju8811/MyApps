//
//  SplashVC.m
//  Kalahari
//
//  Created by LMAN on 3/23/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "SplashViewController.h"
#import "UserManager.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

#pragma mark - 
#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    NSString * userID = [UserManager getUserID];
    if (userID.length == 0) {
        [self performSelector:@selector(gotoLogin) withObject:self afterDelay:2.0f];
    } else {
        [self performSelector:@selector(login) withObject:self afterDelay:2.0f];
    }
}

- (void)gotoLogin {
    
    [self performSegueWithIdentifier:@"SG_LOGIN" sender:nil];
}

- (void)gotoHome {
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)login {
    
    if (gKAppSetting.userEmail == nil || gKAppSetting.userPwd == nil) {
        
        [self gotoLogin];
        return;
    }
    
    [backendless.userService login:gKAppSetting.userEmail password:gKAppSetting.userPwd response:^(BackendlessUser * user) {
        [self gotoHome];
    } error:^(Fault * fault) {
        
        KLLog(@"Login Fail! %@", fault != nil ? fault.description : @"Login Fail!");
        [self gotoLogin];
    }];
}

@end
