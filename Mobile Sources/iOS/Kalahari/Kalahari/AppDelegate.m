//
//  AppDelegate.m
//  Kalahari
//
//  Created by LMAN on 3/23/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [backendless initApp:BL_APP_ID secret:BL_SEC_KEY version:BL_VERSION_NUM];
    [backendless.userService setStayLoggedIn:YES];
    
    //selected tint color
//    [[UITabBar appearance] setTintColor:COLOR_TAB_BAR_ITEM_SEL];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    
    //text tint color
//    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:COLOR_TAB_BAR_ITEM_NORM, NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    //    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:COLOR_TAB_BAR_ITEM_SEL, NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
    // Show Splash View Controller
    UIStoryboard * storyBoard = self.window.rootViewController.storyboard;
    UIViewController * splashVC = [storyBoard instantiateViewControllerWithIdentifier:@"SplashViewController"];
    UINavigationController * navVC = (UINavigationController *) self.window.rootViewController;
    
    [navVC pushViewController:splashVC animated:NO];
    
    [self.window makeKeyWindow];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

@end
