//
//  Global.h
//  Kalahari
//
//  Created by LMAN on 3/23/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#ifndef Global_h
#define Global_h

#import "KAppSetting.h"

typedef NS_ENUM(NSInteger, ProductDetailKind) {
    
    PRODUCT_DETAIL_KIND_NONE                = 0,
    PRODUCT_DETAIL_KIND_ADD                 = 1,
    PRODUCT_DETAIL_KIND_VIEW                = 2,
};

/*
 * Log define
 */
#define KL_DEBUG

#ifdef KL_DEBUG
#define KLLog( s, ... ) NSLog( @"<%@:%d> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__,  [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define KLLog( s, ... )
#endif

/*
 *  App Setting
 */
#define kAppName                    @"Kalahari"
#define gKAppSetting                appSetting()

/*
 * Appearances
 */
#define TAB_BAR_HEIGHT                      59
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#define COLOR_THEME                 UIColorFromRGB(0x653600)
#define COLOR_TAB_BAR_ITEM_NORM     UIColorFromRGB(0xe45f25)
#define COLOR_TAB_BAR_ITEM_SEL      UIColorFromRGB(0xed8c62)
#define COLOR_MSG_BORDER      UIColorFromRGB(0x9f988f)

/*
 * Backend less
 */
#define BL_APP_ID                   @"5173F609-6FFF-6B88-FF07-81CF0E02FD00"
#define BL_SEC_KEY                  @"FD56CBBB-4E05-BA7D-FF97-E55118714400"
#define BL_VERSION_NUM              @"v1"
#define BL_PAGE_SIZE                10

#endif /* Global_h */
