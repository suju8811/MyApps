//
//  KCommon.h
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderCriteria.h"

@interface KCommon : NSObject

/*
 *  Validate Email Address Format
 */
+ (BOOL)validateEmail:(NSString*)email;

/*
 *  Check backendless login
 */
+ (BOOL)checkBLLogin:(UIViewController*)parentViewController;

/*
 *  Navigate view controller
 */

+ (void)gotoOrderFrom:(UIViewController*)fromVC criteria:(OrderCriteria*)criteria;
+ (void)gotoOrderInfoFrom:(UIViewController*)fromVC criteria:(OrderCriteria*)criteria;

@end
