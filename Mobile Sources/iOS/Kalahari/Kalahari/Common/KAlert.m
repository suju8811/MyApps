//
//  KAlert.m
//  Kalahari
//
//  Created by Marcus Lee on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "KAlert.h"

@implementation KAlert

+ (UIAlertController*)alertWithTitle:(NSString*)title
                                 Message:(NSString*)message
                              LeftButton:(NSString*)leftButton
                             RightButton:(NSString*)rightButton
                               leftBlock:(void (^)(UIAlertAction *action))actionLeft
                              RightBlock:(void (^)(UIAlertAction *action))actionRight {
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    if (leftButton != nil) {
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(leftButton, @"Cancel action")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           if (actionLeft != nil)
                                               actionLeft(action);
                                       }];
        
        [alertController addAction:cancelAction];
    }
    
    if (rightButton != nil) {
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(rightButton, @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       if (actionRight != nil)
                                           actionRight(action);
                                   }];
        [alertController addAction:okAction];
    }
    
    return alertController;
}

+ (UIAlertController*)actionSheetWithTitle:(NSString*)title
                                       message:(NSString*)message
                             cancelButtonTitle:(NSString*)cancel
                                        action:(void (^)(UIAlertAction *action))actionBlock
                                        button:(UIView*)fromView
                             otherButtonTitles:(NSString*)otherButtonTitles, ... {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    va_list args;
    va_start(args, otherButtonTitles);
    for (NSString *arg = otherButtonTitles; arg != nil; arg = va_arg(args, NSString*)) {
        
        UIAlertAction *action = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(arg, @"OK action")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action)
                                 {
                                     actionBlock(action);
                                 }];
        [alertController addAction:action];
    }
    va_end(args);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        alertController.popoverPresentationController.sourceView = fromView;
    
    return alertController;
}

+ (UIAlertController*)notifyAlertWithTitle:(NSString*)title
                                   Message:(NSString*)message
                                ButtonTitle:(NSString*)buttonTitle {
    
    return [KAlert alertWithTitle:title Message:message LeftButton:buttonTitle RightButton:nil leftBlock:nil RightBlock:nil];
}

+ (void)showLoading:(NSString*)labelText parent:(UIView*)view {
    
    NSLog(@"Show Loading");
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.label.text = labelText;
}

+ (void)hideLoading:(UIView*)parentView {
    
    NSLog(@"Hide Loading");
//    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:parentView animated:YES];
//    });
}

@end
