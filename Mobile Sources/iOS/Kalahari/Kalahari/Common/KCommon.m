//
//  KCommon.m
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "UserManager.h"
#import "OrderInfoVC.h"
#import "OrderVC.h"

@implementation KCommon

+ (BOOL)validateEmail:(NSString*)email {
    
    NSString *regex1 = @"\\A[A-Za-z0-9-]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,4}\\z";
    NSString *regex2 = @"^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*";
    NSPredicate *test1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex1];
    NSPredicate *test2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex2];
    
    return [test1 evaluateWithObject:email] && [test2 evaluateWithObject:email];
}

+ (BOOL)checkBLLogin:(UIViewController*)parentViewController {
    
//    if (![UserManager isOwner]) {
//        
//        UIAlertController * alert = [KAlert notifyAlertWithTitle:@"Login Again" Message:@"Login session expired." ButtonTitle:@"OK"];
//        [parentViewController presentViewController:alert animated:YES completion:nil];
//        
//        UIViewController * splashVC = [parentViewController.storyboard instantiateViewControllerWithIdentifier:@"SplashViewController"];
//        [parentViewController.navigationController pushViewController:splashVC animated:NO];
//        return NO;
//    }
    
    return YES;
}

+ (void)gotoOrderInfoFrom:(UIViewController*)fromVC criteria:(OrderCriteria*)criteria {
    
    OrderInfoVC * orderInfoVC = nil;
    for (UIViewController * viewController in fromVC.navigationController.viewControllers) {
        
        if ([viewController isKindOfClass:[OrderInfoVC class]]) {
            
            orderInfoVC = (OrderInfoVC*)viewController;
            orderInfoVC.criteria = criteria;
            [fromVC.navigationController popToViewController:orderInfoVC animated:YES];
            return;
        }
    }
        
    orderInfoVC = [fromVC.storyboard instantiateViewControllerWithIdentifier:@"SID_ORDER_INFO"];
    orderInfoVC.criteria = criteria;
    [fromVC.navigationController pushViewController:orderInfoVC animated:YES];
}

+ (void)gotoOrderFrom:(UIViewController*)fromVC criteria:(OrderCriteria*)criteria {
    
    OrderVC * orderVC = nil;
    for (UIViewController * viewController in fromVC.navigationController.viewControllers) {
        
        if ([viewController isKindOfClass:[OrderVC class]]) {
            
            orderVC = (OrderVC*)viewController;
            orderVC.criteria = criteria;
            [fromVC.navigationController popToViewController:orderVC animated:YES];
            return;
        }
    }
    
    orderVC = [fromVC.storyboard instantiateViewControllerWithIdentifier:@"SID_ORDER"];
    orderVC.criteria = criteria;
    [fromVC.navigationController pushViewController:orderVC animated:YES];
}
@end
