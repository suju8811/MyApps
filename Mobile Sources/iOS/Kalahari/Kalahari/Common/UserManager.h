//
//  UserManager.h
//  Kalahari
//
//  Created by LMAN on 3/30/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserManager : NSObject

+ (NSString*)getUserID;
+ (NSString*)getUserEmail;
+ (NSString*) getUserName;
+ (NSString*) getClientName;
+ (NSString*) getDeliveryAddress;
+ (NSString*) getCellNumber;
+ (BOOL)isOwner;

@end
