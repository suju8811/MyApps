//
//  KAppSetting.m
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "KAppSetting.h"
#import "OrderManager.h"

#define PREFERENCE_NEW_ORDER_NO     @"Kalahari.OrderManager_no"
#define PREFERENCE_NEW_ORDER_DATE   @"Kalahari.OrderManager_date"
#define PREFERENCE_USER_LOGIN       @"Kalahari.User_Login"
#define PREFERENCE_USER_PWD         @"Kalahari.User_Pwd"

@implementation KAppSetting

+ (KAppSetting *) sharedInstance {
    
    static KAppSetting * instance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        instance = [[KAppSetting alloc] init];
    });
    
    return instance;
}

- (void)loadSetting {
    
    _currentOrderNo = @"";
    _bDashboardLoaded = NO;
    _bTapChanged = NO;
    _deliveryFee = 100;
    _userEmail = [self stringForKey:PREFERENCE_USER_LOGIN defVal:nil];
    _userPwd = [self stringForKey:PREFERENCE_USER_PWD defVal:nil];
}

- (void)saveNewOrder:(NSString*)order_no {
    
    [self setString:order_no forKey:PREFERENCE_NEW_ORDER_NO];
    [self setString:[OrderManager getCurrentOrderDate] forKey:PREFERENCE_NEW_ORDER_DATE];
}

- (Orders*)getNewOrder {

    Orders * result = [[Orders alloc] init];
    result.orderNo = [self stringForKey:PREFERENCE_NEW_ORDER_NO defVal:@""];
    NSString * dateStr = [self stringForKey:PREFERENCE_NEW_ORDER_DATE defVal:@""];
    result.orderDate = [OrderManager getCurrentOrderDate:dateStr];
    
    if (result.orderNo.length > 0) {
        return result;
    }
    
    return nil;
}

- (void)setUserEmail:(NSString *)userEmail {

    _userEmail = userEmail;
    [self setString:_userEmail forKey:PREFERENCE_USER_LOGIN];
}

- (void)setUserPwd:(NSString *)userPwd {
    
    _userPwd = userPwd;
    [self setString:_userPwd forKey:PREFERENCE_USER_PWD];
}

@end
