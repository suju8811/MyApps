//
//  UserManager.m
//  Kalahari
//
//  Created by LMAN on 3/30/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "UserManager.h"

@implementation UserManager

+ (NSString*)getUserID {
    
    BackendlessUser * user = backendless.userService.currentUser;
    if (user == nil)
        return @"";
    
    return user.objectId;
}

+ (NSString*)getUserEmail {
    
    BackendlessUser * user = backendless.userService.currentUser;
    if (user == nil)
        return @"";
    
    return user.email;
}

+ (NSString*) getUserName {
    
    BackendlessUser * user = backendless.userService.currentUser;
    if (user == nil)
        return @"";
    
    return [user getProperty:@"salonName"];
}

+ (NSString*) getClientName {
    
    BackendlessUser * user = backendless.userService.currentUser;
    if (user == nil)
        return @"";
    
    return [user getProperty:@"ClientName"];
}

+ (NSString*) getDeliveryAddress {
    
    BackendlessUser * user = backendless.userService.currentUser;
    if (user == nil)
        return @"";
    
    return [user getProperty:@"deliveryAddress"];
}

+ (NSString*) getCellNumber {
    
    BackendlessUser * user = backendless.userService.currentUser;
    if (user == nil)
        return @"";
    
    return [user getProperty:@"cellNumber"];
}

+ (BOOL)isOwner {
    
    BackendlessUser * user = backendless.userService.currentUser;
    if (user == nil)
        return NO;
    
    id owner = [user getProperty:@"owner"];
    if (owner == nil)
        return NO;
    
    return [owner boolValue];
}

@end
