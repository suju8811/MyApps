//
//  KAlert.h
//  Kalahari
//
//  Created by Marcus Lee on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KAlert : NSObject

/*
 *  Create Common Alert Controller
 */
+ (UIAlertController*)alertWithTitle:(NSString*)title
                             Message:(NSString*)message
                          LeftButton:(NSString*)leftButton
                         RightButton:(NSString*)rightButton
                           leftBlock:(void (^)(UIAlertAction *action))actionLeft
                          RightBlock:(void (^)(UIAlertAction *action))actionRight;

/*
 *  Create Common Action Sheet
 */
+ (UIAlertController*)actionSheetWithTitle:(NSString*)title
                                   message:(NSString*)message
                         cancelButtonTitle:(NSString*)cancel
                                    action:(void (^)(UIAlertAction *action))actionBlock
                                    button:(UIView*)fromView
                         otherButtonTitles:(NSString*)otherButtonTitles, ...;

/*
 *  Create Simple Notification Alert Controller
 */
+ (UIAlertController*)notifyAlertWithTitle:(NSString*)title
                                   Message:(NSString*)message
                               ButtonTitle:(NSString*)buttonTitle;

/*
 *  Show waitting dialog
 */
+ (void)showLoading:(NSString*)labelText parent:(UIView*)view;

/*
 *  Hide waitting dialog
 */
+ (void)hideLoading:(UIView*)parentView;

@end
