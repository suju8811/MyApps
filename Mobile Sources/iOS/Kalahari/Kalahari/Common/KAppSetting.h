//
//  KAppSetting.h
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"
#import "AppSettingBase.h"
#import "Orders.h"

@interface KAppSetting : AppSettingBase

@property (nonatomic, strong) NSString * currentOrderNo;

/**
 *  Is data loaded
 */

@property (nonatomic, assign) BOOL bDashboardLoaded;
@property (nonatomic, assign) BOOL bTapChanged;
@property (nonatomic, strong) NSString * userEmail;
@property (nonatomic, strong) NSString * userPwd;

/**
 *  Delivery Fee
 */

@property (nonatomic, assign) BOOL deliveryFee;

+ (KAppSetting *) sharedInstance;
- (void)saveNewOrder:(NSString*)order_no;
- (Orders*)getNewOrder;

@end

static inline KAppSetting * appSetting() {
    
    return [KAppSetting sharedInstance];
}
