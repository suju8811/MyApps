package com.youlocalcall.quickpay.view.billing;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.google.gson.Gson;
import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.executive.billing.BillingDisplayContext;
import com.youlocalcall.quickpay.executive.billing.BillingExecutive;
import com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingActor;
import com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingDisplay;
import com.youlocalcall.quickpay.model.billing.BillingFilterRequest;
import com.youlocalcall.quickpay.model.billing.BillingRequest;
import com.youlocalcall.quickpay.model.billing.BillingResponseArray;
import com.youlocalcall.quickpay.model.payment.refund.RefundRequest;
import com.youlocalcall.quickpay.style.ButtonTabBilling;
import com.youlocalcall.quickpay.style.TextViewRegular;
import com.youlocalcall.quickpay.view.BaseFragment;

import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingActor.Customer;
import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingActor.Inmate;

public class BillingFragment extends BaseFragment implements View.OnClickListener,
        BillingDisplay,
        DatePickerDialog.OnDateSetListener {

    private static final String ARG_DISPLAY_CONTEXT = "displayContext";

    private BillingDisplayContext customerDisplayContext;
    private BillingDisplayContext inmateDisplayContext;
    private BillingContentFragment customerFragment;
    private BillingContentFragment inmateFragment;
    private BillingActor selectedActor = Customer;
    private BillingExecutive executive;

    //
    // Controls
    //

    private TextViewRegular titleTextView;
    private TextViewRegular subTitleTextView;
    private ButtonTabBilling customerTab;
    private ButtonTabBilling inmateTab;

    public BillingFragment() {
    }

    public static BillingFragment newInstance(BillingDisplayContext displayContext) {
        BillingFragment fragment = new BillingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DISPLAY_CONTEXT, new Gson().toJson(displayContext));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String contextJSON = getArguments().getString(ARG_DISPLAY_CONTEXT);
            customerDisplayContext = new Gson().fromJson(contextJSON, BillingDisplayContext.class);
            customerDisplayContext.setActor(Customer);
            inmateDisplayContext = new Gson().fromJson(contextJSON, BillingDisplayContext.class);
            inmateDisplayContext.setActor(Inmate);
        }

        executive = new BillingExecutive(this, customerDisplayContext, inmateDisplayContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_billing, container, false);

        //
        // Create child fragments
        //

        customerFragment = BillingContentFragment.newInstance(customerDisplayContext);
        customerFragment.setBillingExecutive(executive);
        inmateFragment = BillingContentFragment.newInstance(inmateDisplayContext);
        inmateFragment.setBillingExecutive(executive);

        //
        // Set Content
        //

        titleTextView = (TextViewRegular) view.findViewById(R.id.titleTextView);
        subTitleTextView = (TextViewRegular) view.findViewById(R.id.subTitleTextView);
        customerTab = (ButtonTabBilling) view.findViewById(R.id.buttonTabCustomer);
        inmateTab = (ButtonTabBilling) view.findViewById(R.id.buttonTabInmate);

        //
        // Set Event Handler
        //

        view.findViewById(R.id.imageViewSideMenu).setOnClickListener(this);
        customerTab.setOnClickListener(this);
        inmateTab.setOnClickListener(this);

        //
        // Display Did Load
        //

        executive.displayDidLoad();

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewSideMenu:
                containerExecutive.didPressSideMenuButton();
                break;
            case R.id.buttonTabCustomer:
                executive.didPressCustomerButton();
                break;
            case R.id.buttonTabInmate:
                executive.didPressInmateButton();
                break;
        }
    }

    @Override
    public void refreshDisplay(BillingDisplayContext customerContext, BillingDisplayContext inmateContext, BillingExecutive.BillingActor selectedActor) {
        customerDisplayContext = customerContext;
        inmateDisplayContext = inmateContext;
        this.selectedActor = selectedActor;

        //
        // Set title and sub title
        //

        titleTextView.setText(customerDisplayContext.getTitle());
        subTitleTextView.setText(customerDisplayContext.getSubtitle());

        //
        // Set tab
        //

        customerTab.setSelected(selectedActor == Customer);
        inmateTab.setSelected(selectedActor == Inmate);

        //
        // Switch View
        //

        if (selectedActor == Customer) {
            placeContent(R.id.billing_content_container, customerFragment);
        } else {
            placeContent(R.id.billing_content_container, inmateFragment);
        }
    }

    @Override
    public void showCalendar(int year, int month, int day, BillingExecutive.DateType dateType) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DatePickerDialog, this, year, month, day);
        datePickerDialog.getDatePicker().setTag(dateType);
        datePickerDialog.show();
    }

    @Override
    public BillingRequest getBillingRequest(BillingExecutive.BillingActor actor) {
        BillingExecutive.BillingActor currentActor;
        if (actor != null) {
            currentActor = actor;
        } else {
            currentActor = selectedActor;
        }
        switch (currentActor) {
            case Customer:
                return customerFragment.getBillingRequest();
            case Inmate:
                return inmateFragment.getBillingRequest();
        }
        return new BillingRequest();
    }

    @Override
    public BillingFilterRequest getCustomerFilterRequest() {
        return customerFragment.getFilterFragment().getFilterRequest();
    }

    @Override
    public BillingFilterRequest getInmateFilterRequest() {
        return inmateFragment.getFilterFragment().getFilterRequest();
    }

    @Override
    public void reloadBillingTable(BillingExecutive.BillingActor actor, BillingResponseArray billingResponseArray) {
        BillingExecutive.BillingActor currentActor;
        if (actor != null) {
            currentActor = actor;
        } else {
            currentActor = selectedActor;
        }
        switch (currentActor) {
            case Customer:
                customerFragment.reloadBillingList(billingResponseArray);
                break;
            case Inmate:
                inmateFragment.reloadBillingList(billingResponseArray);
                break;
        }
    }

    @Override
    public void gotoRefund(RefundRequest getRefundListRequest) {
        containerExecutive.didPressRefundButton(getRefundListRequest);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        BillingExecutive.DateType dateType = (BillingExecutive.DateType) datePicker.getTag();
        executive.didDateChanged(year, month, day, dateType);
    }
}
