package com.youlocalcall.quickpay.view.quickpay;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.executive.payment.PaymentHomeExecutive;
import com.youlocalcall.quickpay.executive.payment.PaymentHomeExecutive.PaymentHomeDisplay;
import com.youlocalcall.quickpay.model.account.Inmate.Inmate;
import com.youlocalcall.quickpay.model.account.Inmate.InmateRequest;
import com.youlocalcall.quickpay.model.payment.PaymentRequest;
import com.youlocalcall.quickpay.model.payment.confirm.PayPalConfirmRequest;
import com.youlocalcall.quickpay.style.EditTextFieldCommon;
import com.youlocalcall.quickpay.view.BaseFragment;
import com.youlocalcall.quickpay.view.billing.BillingDetailFragment;
import com.youlocalcall.quickpay.view.container.PayPalPaymentInterface;

import java.util.ArrayList;

public class QuickPayFragment extends BaseFragment implements View.OnClickListener,
        PaymentHomeDisplay {

    //
    // Delegate variables
    //

    private BillingDetailFragment billingDetailFragment;
    private PaymentHomeExecutive executive;
    private PayPalPaymentInterface payPalPaymentInterface;

    //
    // Binded Controls
    //

    private EditTextFieldCommon firstNameEditText;
    private EditTextFieldCommon lastNameEditText;
    private EditTextFieldCommon bopNumberEditText;
    private View searchButtonsView;
    private Button searchButton;
    private Button addInmateButton;
    private RecyclerView inmatesRecycler;
    private EditTextFieldCommon amountEditText;
    private ArrayList<Inmate> inmates = new ArrayList<>();
    private InmatesRecyclerViewAdapter adapter = new InmatesRecyclerViewAdapter();

    public QuickPayFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        billingDetailFragment = new BillingDetailFragment();
        placeContent(R.id.billing_detail_container, billingDetailFragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quick_pay, container, false);

        //
        // Set Content
        //

        firstNameEditText = (EditTextFieldCommon) view.findViewById(R.id.firstNameEditText);
        lastNameEditText = (EditTextFieldCommon) view.findViewById(R.id.lastNameEditText);
        bopNumberEditText = (EditTextFieldCommon) view.findViewById(R.id.bopNumberEditText);
        searchButtonsView = view.findViewById(R.id.searchButtonsView);
        searchButton = (Button) view.findViewById(R.id.searchButton);
        addInmateButton = (Button) view.findViewById(R.id.addInmateButton);
        amountEditText = (EditTextFieldCommon) view.findViewById(R.id.amountEditText);
        inmatesRecycler = (RecyclerView) view.findViewById(R.id.inmatesRecycler);
        inmatesRecycler.setNestedScrollingEnabled(false);
        inmatesRecycler.setAdapter(adapter);
        amountEditText = (EditTextFieldCommon) view.findViewById(R.id.amountEditText);

        //
        // Set Event Handler
        //

        view.findViewById(R.id.imageViewSideMenu).setOnClickListener(this);
        searchButton.setOnClickListener(this);
        addInmateButton.setOnClickListener(this);
        view.findViewById(R.id.submitButton).setOnClickListener(this);

        //
        // Display did load
        //

        executive = new PaymentHomeExecutive(this, payPalPaymentInterface);
        billingDetailFragment.setParentExecutive(executive);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewSideMenu:
                containerExecutive.didPressSideMenuButton();
                break;
            case R.id.searchButton:
                executive.didPressSearchButton();
                break;
            case R.id.addInmateButton:
                executive.didPressAddInmateButton();
                break;
            case R.id.submitButton:
                executive.didPressSubmitButton();
                break;
        }
    }

    @Override
    public void loadDisplay() {
    }

    @Override
    public void refreshSearchResult(ArrayList<Inmate> inmates) {
        this.inmates = inmates;
        if (!this.inmates.isEmpty()) {
            this.inmates.get(0).setSelected(true);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public InmateRequest getInmateRequest() {
        return new InmateRequest(firstNameEditText.getText().toString(),
                lastNameEditText.getText().toString(),
                bopNumberEditText.getText().toString());
    }

    @Override
    public PaymentRequest getPaymentRequest() {
        PaymentRequest paymentRequest = billingDetailFragment.getPaymentRequest();
        if (AppData.user.getInmateId() != null) {
            paymentRequest.setInmateId(AppData.user.getInmateId());
        } else {
            for (Inmate inmate : inmates) {
                if (inmate.isSelected()) {
                    paymentRequest.setInmateId(inmate.getId());
                    break;
                }
            }
        }
        paymentRequest.setTotalAmount(amountEditText.getText().toString());
        return paymentRequest;
    }

    @Override
    public void showAddButton(boolean isShow) {
        addInmateButton.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void enableSearchView(boolean isEnable, Inmate inmate) {
        firstNameEditText.setEnabled(isEnable);
        lastNameEditText.setEnabled(isEnable);
        bopNumberEditText.setEnabled(isEnable);
        searchButtonsView.setVisibility(View.GONE);
        if (!isEnable) {
            firstNameEditText.setText(inmate.getFirstName());
            lastNameEditText.setText(inmate.getLastName());
            bopNumberEditText.setText(inmate.getBOPNumber());
        }
    }

    @Override
    public PayPalConfirmRequest getPayPalConfirmRequest(String paymentId) {
        String inmateId = "";
        if (AppData.user.getInmateId() != null) {
            inmateId = String.valueOf(AppData.user.getInmateId());
        } else {
            for (Inmate inmate : inmates) {
                if (inmate.isSelected()) {
                    inmateId = String.valueOf(inmate.getId());
                    break;
                }
            }
        }
        return new PayPalConfirmRequest(inmateId,
                paymentId,
                String.valueOf(AppData.user.getId()),
                amountEditText.getText().toString());
    }

    public void setPayPalPaymentInterface(PayPalPaymentInterface payPalPaymentInterface) {
        this.payPalPaymentInterface = payPalPaymentInterface;
    }

    private class InmatesRecyclerViewAdapter extends RecyclerView.Adapter<InmatesRecyclerViewAdapter.ViewHolder> {

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return 0;
            } else {
                return 1;
            }
        }

        @Override
        public InmatesRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;
            if (viewType == 0) {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_inmate_header, parent, false);
            } else {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_inmate_content, parent, false);
            }
            return new InmatesRecyclerViewAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(InmatesRecyclerViewAdapter.ViewHolder holder, int position) {
            if (position == 0) {
                return;
            }
            final Inmate inmate = inmates.get(position - 1);
            if (inmate.isSelected()) {
                holder.firstNameTextView.setBackgroundResource(R.color.colorPrimary);
                holder.lastNameTextView.setBackgroundResource(R.color.colorPrimary);
                holder.bopNumberTextView.setBackgroundResource(R.color.colorPrimary);
            } else {
                holder.firstNameTextView.setBackgroundResource(android.R.color.white);
                holder.lastNameTextView.setBackgroundResource(android.R.color.white);
                holder.bopNumberTextView.setBackgroundResource(android.R.color.white);
            }
            holder.firstNameTextView.setText(inmate.getFirstName());
            holder.lastNameTextView.setText(inmate.getLastName());
            holder.bopNumberTextView.setText(inmate.getBOPNumber());
            holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (Inmate iterator : inmates) {
                        iterator.setSelected(false);
                    }
                    inmate.setSelected(true);
                    adapter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return inmates.isEmpty() ? 0 : inmates.size() + 1;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final View container;
            final TextView firstNameTextView;
            final TextView lastNameTextView;
            final TextView bopNumberTextView;

            ViewHolder(View view) {
                super(view);

                container = view.findViewById(R.id.container);
                firstNameTextView = (TextView) view.findViewById(R.id.firstNameTextView);
                lastNameTextView = (TextView) view.findViewById(R.id.lastNameTextView);
                bopNumberTextView = (TextView) view.findViewById(R.id.bopNumberTextView);
            }
        }
    }
}
