package com.youlocalcall.quickpay.model.account.Inmate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.youlocalcall.quickpay.model.CommonResponse;

/**
 * Created by Michael on 6/27/17.
 */

public class InmateByIdResponse extends CommonResponse {

    @SerializedName("result")
    @Expose
    private InmateByIdResponseResult result;

    public InmateByIdResponseResult getResult() {
        return result;
    }
}
