package com.youlocalcall.quickpay.model.account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Michael on 6/20/17.
 */

public class User extends CommonUserInfo {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("inmateID")
    @Expose
    private Integer inmateId;

    @SerializedName("allowCustomer")
    @Expose
    private Integer allowCustomer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInmateId() {
        return inmateId;
    }

    public void setInmateId(Integer inmateId) {
        this.inmateId = inmateId;
    }

    public Integer getAllowCustomer() {
        return allowCustomer;
    }

    public void setAllowCustomer(Integer allowCustomer) {
        this.allowCustomer = allowCustomer;
    }
}
