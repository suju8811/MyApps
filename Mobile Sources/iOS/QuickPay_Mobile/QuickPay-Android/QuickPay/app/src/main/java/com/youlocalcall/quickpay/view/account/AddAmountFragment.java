package com.youlocalcall.quickpay.view.account;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.executive.payment.AddPrepayAmountExecutive;
import com.youlocalcall.quickpay.executive.payment.AddPrepayAmountExecutive.AddPrepayAmountDisplay;
import com.youlocalcall.quickpay.model.payment.PaymentAmountRequest;
import com.youlocalcall.quickpay.model.payment.confirm.PayPalConfirmRequest;
import com.youlocalcall.quickpay.style.EditTextFieldCommon;
import com.youlocalcall.quickpay.view.BaseFragment;
import com.youlocalcall.quickpay.view.billing.BillingDetailFragment;
import com.youlocalcall.quickpay.view.container.PayPalPaymentInterface;

public class AddAmountFragment extends BaseFragment implements View.OnClickListener, AddPrepayAmountDisplay {

    EditTextFieldCommon totalAmountEditText;
    private BillingDetailFragment billingDetailFragment;
    private PayPalPaymentInterface payPalPaymentInterface;

    //
    // Bind controls
    //
    private AddPrepayAmountExecutive executive;

    public AddAmountFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        billingDetailFragment = new BillingDetailFragment();
        placeContent(R.id.billing_detail_container, billingDetailFragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_amount, container, false);

        //
        // Set Content
        //

        totalAmountEditText = (EditTextFieldCommon) view.findViewById(R.id.totalAmountEditText);

        //
        // Set Event Handler
        //

        view.findViewById(R.id.imageViewBack).setOnClickListener(this);
        view.findViewById(R.id.submitButton).setOnClickListener(this);

        //
        // Set Executive
        //

        executive = new AddPrepayAmountExecutive(this, payPalPaymentInterface);
        billingDetailFragment.setParentExecutive(executive);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack:
                containerExecutive.didPressChildBackButton();
                break;
            case R.id.submitButton:
                executive.didPressSubmitButton();
                break;
        }
    }

    @Override
    public void showWarningMessage() {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.app_name)
                .setMessage(R.string.quick_pay_recommend)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        executive.proceedAddAmount();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();
    }

    @Override
    public PaymentAmountRequest getPaymentAmountRequest() {
        PaymentAmountRequest request = billingDetailFragment.getPaymentAmountRequest();
        request.setTotalPrice(totalAmountEditText.getText().toString());
        return request;
    }

    @Override
    public PayPalConfirmRequest getPayPalConfirmRequest(String paymentId) {
        return new PayPalConfirmRequest(paymentId,
                String.valueOf(AppData.user.getId()),
                totalAmountEditText.getText().toString());
    }

    public void setPayPalPaymentInterface(PayPalPaymentInterface payPalPaymentInterface) {
        this.payPalPaymentInterface = payPalPaymentInterface;
    }
}
