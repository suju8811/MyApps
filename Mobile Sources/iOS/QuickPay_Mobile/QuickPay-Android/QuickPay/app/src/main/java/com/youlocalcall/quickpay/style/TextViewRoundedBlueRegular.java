package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.AttributeSet;

/**
 * Created by Michael on 6/20/17.
 */

public class TextViewRoundedBlueRegular extends android.support.v7.widget.AppCompatTextView {
    public TextViewRoundedBlueRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        BaseFontStyles.applyArialBoldFont(this);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(3.0f);
        gradientDrawable.setColor(BaseColors.YLC_BLUE2);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setBackground(gradientDrawable);
        } else {
            setBackgroundDrawable(gradientDrawable);
        }
    }
}
