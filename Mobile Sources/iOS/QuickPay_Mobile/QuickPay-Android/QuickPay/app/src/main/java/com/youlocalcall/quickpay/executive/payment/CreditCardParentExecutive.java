package com.youlocalcall.quickpay.executive.payment;

import com.youlocalcall.quickpay.executive.CommonExecutive;

/**
 * Created by Michael on 6/28/17.
 */

public class CreditCardParentExecutive extends CommonExecutive {

    protected CreditCardInfoExecutive childExecutive;
    public CreditCardParentExecutive(CommonDisplay display) {
        super(display);
    }

    public void displayDidLoad() {
    }

    public interface CreditCardCommonDisplay extends CommonExecutive.CommonDisplay {
    }
}
