package com.youlocalcall.quickpay.view.billing;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.executive.payment.BillingDetailExecutive;
import com.youlocalcall.quickpay.executive.payment.BillingDetailExecutive.BillingDetailDisplay;
import com.youlocalcall.quickpay.executive.payment.BillingDetailParentExecutive;
import com.youlocalcall.quickpay.model.payment.PaymentAmountRequest;
import com.youlocalcall.quickpay.model.payment.PaymentMethod;
import com.youlocalcall.quickpay.model.payment.PaymentRequest;
import com.youlocalcall.quickpay.model.payment.creditcard.CreditCardPaymentRequest;
import com.youlocalcall.quickpay.model.payment.profile.PaymentProfile;
import com.youlocalcall.quickpay.style.RadioButtonRegular;
import com.youlocalcall.quickpay.view.BaseFragment;
import com.youlocalcall.quickpay.view.creditcard.CreditCardInfoFragment;

public class BillingDetailFragment extends BaseFragment implements BillingDetailDisplay {

    //
    // Delegate variables
    //

    RadioButtonRegular payViaCreditCardBeforeButton;
    RadioButtonRegular payViaCreditCardButton;
    RadioButtonRegular payViaPayPalButton;
    View creditCardContainer;

    //
    // Bind controls
    //
    private CreditCardInfoFragment creditCardInfoFragment;
    private BillingDetailExecutive executive;
    private BillingDetailParentExecutive parentExecutive;
    private PaymentMethod paymentMethod;

    public BillingDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        creditCardInfoFragment = new CreditCardInfoFragment();
        placeContent(R.id.credit_card_info_container, creditCardInfoFragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_billing_detail, container, false);

        //
        // Set Content
        //

        payViaCreditCardBeforeButton = (RadioButtonRegular) view.findViewById(R.id.payViaCreditCardBefore);
        payViaCreditCardButton = (RadioButtonRegular) view.findViewById(R.id.payViaCreditCard);
        payViaPayPalButton = (RadioButtonRegular) view.findViewById(R.id.payViaPayPal);
        creditCardContainer = view.findViewById(R.id.credit_card_info_container);

        //
        // Set Event Handler
        //

        payViaCreditCardBeforeButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    executive.didSelectPaymentMethod(PaymentMethod.CreditCardBefore);
                }
            }
        });
        payViaCreditCardButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    executive.didSelectPaymentMethod(PaymentMethod.CreditCard);
                }
            }
        });
        payViaPayPalButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    executive.didSelectPaymentMethod(PaymentMethod.PayPal);
                }
            }
        });

        //
        // Set executive
        //

        executive = new BillingDetailExecutive(this, parentExecutive);
        creditCardInfoFragment.setParentExecutive(executive);

        return view;
    }

    @Override
    public void loadLayout(PaymentProfile context) {
        String paymentProfileID = context.getPaymentProfileID();
        if (paymentProfileID != null && !paymentProfileID.isEmpty()) {
            payViaCreditCardBeforeButton.setVisibility(View.VISIBLE);
            String cardNumber = context.getCardNumber();
            if (cardNumber != null) {
                payViaCreditCardBeforeButton.setText("Pay via Credit Card " + cardNumber);
            }
            payViaCreditCardBeforeButton.setChecked(true);
        } else {
            payViaCreditCardBeforeButton.setVisibility(View.GONE);
            payViaCreditCardButton.setChecked(true);
        }
    }

    @Override
    public void selectPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
        if (paymentMethod == PaymentMethod.CreditCard) {
            creditCardContainer.setVisibility(View.VISIBLE);
        } else {
            creditCardContainer.setVisibility(View.GONE);
        }
    }

    public void setParentExecutive(BillingDetailParentExecutive parentExecutive) {
        this.parentExecutive = parentExecutive;
    }

    public PaymentAmountRequest getPaymentAmountRequest() {
        PaymentProfile paymentProfile = creditCardInfoFragment.getCreditCardUpdateRequest();
        return new PaymentAmountRequest(paymentProfile,
                paymentMethod == PaymentMethod.PayPal,
                paymentMethod == PaymentMethod.CreditCard,
                "");
    }

    public PaymentRequest getPaymentRequest() {
        PaymentRequest paymentRequest;
        if (paymentMethod == PaymentMethod.PayPal) {
            paymentRequest = new PaymentRequest();
        } else {
            paymentRequest = new CreditCardPaymentRequest();
        }
        PaymentProfile paymentProfile = creditCardInfoFragment.getCreditCardUpdateRequest();
        paymentRequest.setCardHolder(paymentProfile.getCardHolderName());
        paymentRequest.setCardNumber(paymentProfile.getCardNumber());
        paymentRequest.setCvc(paymentProfile.getCvc());
        paymentRequest.setExpiration(paymentProfile.getExpiration());
        if (paymentMethod != PaymentMethod.PayPal) {
            ((CreditCardPaymentRequest) paymentRequest).setEmail(paymentProfile.getEmail());
            ((CreditCardPaymentRequest) paymentRequest).setPhoneNumber(paymentProfile.getPhoneNumber());
            ((CreditCardPaymentRequest) paymentRequest).setUsePersonEmail(paymentProfile.getUsePersonEmail() != null && paymentProfile.getUsePersonEmail() == 1);
            ((CreditCardPaymentRequest) paymentRequest).setCountry(paymentProfile.getPersonAddress().getCountry().getName());
            ((CreditCardPaymentRequest) paymentRequest).setBillingAdress(paymentProfile.getPersonAddress().getBillingAddress());
            ((CreditCardPaymentRequest) paymentRequest).setUsePersonAddress(paymentProfile.getUsePersonAddress() != null && paymentProfile.getUsePersonAddress() == 1);
            ((CreditCardPaymentRequest) paymentRequest).setCity(paymentProfile.getPersonAddress().getCity());
            ((CreditCardPaymentRequest) paymentRequest).setState(paymentProfile.getPersonAddress().getState().getStateName());
            ((CreditCardPaymentRequest) paymentRequest).setZipCode(paymentProfile.getPersonAddress().getZipCode());
            if (paymentMethod == PaymentMethod.CreditCard) {
                ((CreditCardPaymentRequest) paymentRequest).setUpdate(true);
            }
        }
        return paymentRequest;
    }
}
