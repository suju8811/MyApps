package com.youlocalcall.quickpay.model.payment.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.youlocalcall.quickpay.model.account.User;

/**
 * Created by Michael on 6/27/17.
 */

public class PaymentProfileResponseResult {

    @SerializedName("payment")
    @Expose

    private PaymentProfile paymentProfile;

    @SerializedName("user")
    @Expose

    private User userProfile;

    public PaymentProfile getPaymentProfile() {
        return paymentProfile;
    }

    public void setPaymentProfile(PaymentProfile paymentProfile) {
        this.paymentProfile = paymentProfile;
    }

    public User getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(User userProfile) {
        this.userProfile = userProfile;
    }
}
