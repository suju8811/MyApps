package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Michael on 6/20/17.
 */

public class ButtonRegularRoundedDarkGrayGray extends ButtonRegularRounded {
    public ButtonRegularRoundedDarkGrayGray(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        setRoundedBackground(BaseColors.YLC_GRAY6, -1, 0);
        setTextColor(BaseColors.YLC_GRAY5);
    }
}
