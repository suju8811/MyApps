package com.youlocalcall.quickpay.executive.account;

import com.youlocalcall.quickpay.model.account.address.State;

import java.util.ArrayList;

/**
 * Created by Michael on 6/28/17.
 */

public class CountryStateDisplayContext {
    private Integer selectedCountry;
    private Integer selectedState;
    private ArrayList<State> states = new ArrayList<>();

    /**
     * Initialize values
     */

    public void initialize() {
        selectedState = null;
        selectedCountry = null;
        states.clear();
    }

    public Integer getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(Integer selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public Integer getSelectedState() {
        return selectedState;
    }

    public void setSelectedState(Integer selectedState) {
        this.selectedState = selectedState;
    }

    public ArrayList<State> getStates() {
        return states;
    }

    public void setStates(ArrayList<State> states) {
        this.states = states;
    }
}