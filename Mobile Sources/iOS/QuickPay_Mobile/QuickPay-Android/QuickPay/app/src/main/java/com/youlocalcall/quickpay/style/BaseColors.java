package com.youlocalcall.quickpay.style;

import android.graphics.Color;

/**
 * Created by scottgregg on 27/02/2017.
 */

public class BaseColors {
    public static final int YLC_WHITE = Color.parseColor("#FFFFFF");
    public static final int YLC_BLACK = Color.BLACK;
    public static final int YLC_CLEAR = Color.TRANSPARENT;
    public static final int YLC_PINK = Color.parseColor("#0B0080");
    public static final int YLC_BLUE1 = Color.parseColor("#0645AD");
    public static final int YLC_BLUE2 = Color.parseColor("#0089FF");
    public static final int YLC_BLUE3 = Color.parseColor("#0089FF");
    public static final int YLC_GRAY1 = Color.parseColor("#FAFAFA");
    public static final int YLC_GRAY2 = Color.parseColor("#EEEEEE");
    public static final int YLC_GRAY3 = Color.parseColor("#C8C8C8");
    public static final int YLC_GRAY4 = Color.parseColor("#646C77");
    public static final int YLC_GRAY5 = Color.parseColor("#666666");
    public static final int YLC_GRAY6 = Color.parseColor("#E2E2E2");
    public static final int YLC_GREEN = Color.parseColor("#70A01C");
}
