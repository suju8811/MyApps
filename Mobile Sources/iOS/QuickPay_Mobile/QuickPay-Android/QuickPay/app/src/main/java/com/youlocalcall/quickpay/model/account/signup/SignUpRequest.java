package com.youlocalcall.quickpay.model.account.signup;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.account.BaseRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Michael on 6/20/17.
 */

public class SignUpRequest extends BaseRequest {

    private String userName;
    private String password;
    private String confirmPassword;
    private String email;
    private String phoneNumber;
    private boolean isAcceptTerms;
    private Integer isPhone = 1;

    public SignUpRequest(String userName,
                         String password,
                         String confirmPassword,
                         String email,
                         String phoneNumber,
                         boolean isAcceptTerms) {
        this.userName = userName;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.isAcceptTerms = isAcceptTerms;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public boolean isAcceptTerms() {
        return isAcceptTerms;
    }

    public void setIsPhone(int isPhone) {
        this.isPhone = isPhone;
    }

    public void post(final APIUtility.APIResponse<CommonResponse> apiResponse, final CommonExecutive executive) {
        accountService.signUp(userName,
                password,
                email,
                phoneNumber,
                isMobile,
                isPhone).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }
}
