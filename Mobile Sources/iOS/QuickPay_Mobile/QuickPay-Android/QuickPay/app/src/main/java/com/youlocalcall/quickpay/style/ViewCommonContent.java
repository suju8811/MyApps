package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Michael on 6/20/17.
 */

public class ViewCommonContent extends View {
    public ViewCommonContent(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(6.0f);
        gradientDrawable.setColor(BaseColors.YLC_GRAY1);
        gradientDrawable.setStroke(3, BaseColors.YLC_GRAY2);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setBackground(gradientDrawable);
        } else {
            setBackgroundDrawable(gradientDrawable);
        }
    }
}
