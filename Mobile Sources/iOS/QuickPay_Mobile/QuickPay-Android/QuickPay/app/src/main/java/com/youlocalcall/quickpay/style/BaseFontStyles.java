package com.youlocalcall.quickpay.style;

import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by scottgregg on 16/11/2016.
 */

class BaseFontStyles {
    private static final String REGULAR_FONT_ASSET_NAME = "arial.ttf";
    private static final String BOLD_FONT_ASSET_NAME = "arial-bold.ttf";
    private static final String ITALIC_FONT_ASSET_NAME = "arial-italic.ttf";

    /**
     * Configures a text view to use the regular font.
     *
     * @param textView The text view to configure.
     * @return The text view that was configured.
     */
    public static TextView applyArialRegularFont(TextView textView) {
        textView.setTypeface(
                Typeface.createFromAsset(textView.getResources().getAssets(), REGULAR_FONT_ASSET_NAME));

        return textView;
    }

    public static TextView applyArialBoldFont(TextView textView) {
        textView.setTypeface(
                Typeface.createFromAsset(textView.getResources().getAssets(), BOLD_FONT_ASSET_NAME));

        return textView;
    }

    public static TextView applyArialItalicFont(TextView textView) {
        textView.setTypeface(
                Typeface.createFromAsset(textView.getResources().getAssets(), ITALIC_FONT_ASSET_NAME));
        return textView;
    }
}
