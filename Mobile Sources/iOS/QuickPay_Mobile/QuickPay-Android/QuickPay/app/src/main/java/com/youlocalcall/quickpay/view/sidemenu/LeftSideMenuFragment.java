package com.youlocalcall.quickpay.view.sidemenu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.executive.container.ContainerExecutive;
import com.youlocalcall.quickpay.style.BaseColors;
import com.youlocalcall.quickpay.style.TextViewRegular;
import com.youlocalcall.quickpay.view.BaseFragment;

import java.util.ArrayList;
import java.util.Arrays;

public class LeftSideMenuFragment extends BaseFragment implements View.OnClickListener {

    private TextViewRegular userNameTextView;
    private TextViewRegular emailTextView;
    private ImageView quickPayImageView;
    private ImageView accountImageView;
    private ImageView creditCardImageView;
    private ImageView billingImageView;
    private ImageView logoutImageView;
    private TextViewRegular quickPayTextView;
    private TextViewRegular accountTextView;
    private TextViewRegular creditCardTextView;
    private TextViewRegular billingTextView;
    private TextViewRegular invoiceTextView;
    private TextViewRegular paymentTextView;
    private TextViewRegular logoutTextView;

    public LeftSideMenuFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_left_side_menu, container, false);

        //
        // Set Content
        //

        userNameTextView = (TextViewRegular) view.findViewById(R.id.userNameTextView);
        emailTextView = (TextViewRegular) view.findViewById(R.id.emailTextView);
        quickPayImageView = (ImageView) view.findViewById(R.id.imageViewQuickPay);
        accountImageView = (ImageView) view.findViewById(R.id.imageViewAccount);
        creditCardImageView = (ImageView) view.findViewById(R.id.imageViewCreditCard);
        billingImageView = (ImageView) view.findViewById(R.id.imageViewBilling);
        logoutImageView = (ImageView) view.findViewById(R.id.imageViewLogout);
        quickPayTextView = (TextViewRegular) view.findViewById(R.id.quickPayTextView);
        accountTextView = (TextViewRegular) view.findViewById(R.id.accountTextView);
        creditCardTextView = (TextViewRegular) view.findViewById(R.id.creditCardTextView);
        billingTextView = (TextViewRegular) view.findViewById(R.id.billingTextView);
        invoiceTextView = (TextViewRegular) view.findViewById(R.id.invoiceTextView);
        paymentTextView = (TextViewRegular) view.findViewById(R.id.paymentTextView);
        logoutTextView = (TextViewRegular) view.findViewById(R.id.logoutTextView);

        //
        // Set Event Handler
        //

        view.findViewById(R.id.quickPayButton).setOnClickListener(this);
        view.findViewById(R.id.accountButton).setOnClickListener(this);
        view.findViewById(R.id.creditCardButton).setOnClickListener(this);
        view.findViewById(R.id.billingInvoiceButton).setOnClickListener(this);
        view.findViewById(R.id.billingPaymentButton).setOnClickListener(this);
        view.findViewById(R.id.logoutButton).setOnClickListener(this);

        //
        // Set default select menu
        //

        setHighLightItem(ContainerExecutive.SideMenuItem.QuickPay);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.quickPayButton:
                containerExecutive.didPressSideMenuItem(ContainerExecutive.SideMenuItem.QuickPay);
                break;
            case R.id.accountButton:
                containerExecutive.didPressSideMenuItem(ContainerExecutive.SideMenuItem.Account);
                break;
            case R.id.creditCardButton:
                containerExecutive.didPressSideMenuItem(ContainerExecutive.SideMenuItem.CreditCard);
                break;
            case R.id.billingInvoiceButton:
                containerExecutive.didPressSideMenuItem(ContainerExecutive.SideMenuItem.BillingInvoice);
                break;
            case R.id.billingPaymentButton:
                containerExecutive.didPressSideMenuItem(ContainerExecutive.SideMenuItem.BillingPayments);
                break;
            case R.id.logoutButton:
                containerExecutive.didPressSideMenuItem(ContainerExecutive.SideMenuItem.LogOut);
                break;
        }
    }

    public void setHighLightItem(ContainerExecutive.SideMenuItem item) {
        ArrayList<ImageView> itemImageViews = new ArrayList<>(Arrays.asList(quickPayImageView, accountImageView, creditCardImageView, billingImageView, logoutImageView));
        ArrayList<TextViewRegular> itemTextViews = new ArrayList<>(Arrays.asList(quickPayTextView, accountTextView, creditCardTextView, billingTextView, invoiceTextView, paymentTextView, logoutTextView));
        ArrayList<Integer> imageItems = new ArrayList<>(Arrays.asList(R.drawable.ic_quickpay, R.drawable.ic_account, R.drawable.ic_creditcard, R.drawable.ic_billing, R.drawable.ic_sign_out));

        //
        // Deselect all items
        //

        for (int index = 0; index < itemImageViews.size(); index++) {
            itemImageViews.get(index).setImageResource(imageItems.get(index));
        }
        for (TextViewRegular itemTextView : itemTextViews) {
            itemTextView.setTextColor(BaseColors.YLC_GRAY4);
        }

        //
        // Select item
        //

        switch (item) {
            case QuickPay:
                quickPayImageView.setImageResource(R.drawable.ic_quickpay_sel);
                quickPayTextView.setTextColor(BaseColors.YLC_BLUE1);
                break;
            case Account:
                accountImageView.setImageResource(R.drawable.ic_accout_sel);
                accountTextView.setTextColor(BaseColors.YLC_BLUE1);
                break;
            case CreditCard:
                creditCardImageView.setImageResource(R.drawable.ic_creditcard_sel);
                creditCardTextView.setTextColor(BaseColors.YLC_BLUE1);
                break;
            case BillingInvoice:
                invoiceTextView.setTextColor(BaseColors.YLC_BLUE1);
                break;
            case BillingPayments:
                paymentTextView.setTextColor(BaseColors.YLC_BLUE1);
                break;
        }
    }
}
