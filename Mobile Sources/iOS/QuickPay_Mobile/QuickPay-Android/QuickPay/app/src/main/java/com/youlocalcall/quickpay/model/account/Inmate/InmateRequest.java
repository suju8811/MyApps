package com.youlocalcall.quickpay.model.account.Inmate;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.account.BaseRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Michael on 6/29/17.
 */

public class InmateRequest extends BaseRequest {
    private String firstName;
    private String lastName;
    private String bopNumber;

    public InmateRequest(String firstName,
                         String lastName,
                         String bopNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.bopNumber = bopNumber;
    }

    public void get(final APIUtility.APIResponse<InmateResponse> apiResponse, final CommonExecutive executive) {
        accountService.searchInmates(firstName,
                lastName,
                bopNumber).enqueue(new Callback<InmateResponse>() {
            @Override
            public void onResponse(Call<InmateResponse> call, Response<InmateResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<InmateResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }

    public void add(final APIUtility.APIResponse<InmateResponse> apiResponse, final CommonExecutive executive) {
        accountService.addInmate(firstName,
                lastName,
                bopNumber).enqueue(new Callback<InmateResponse>() {
            @Override
            public void onResponse(Call<InmateResponse> call, Response<InmateResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<InmateResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBopNumber() {
        return bopNumber;
    }
}
