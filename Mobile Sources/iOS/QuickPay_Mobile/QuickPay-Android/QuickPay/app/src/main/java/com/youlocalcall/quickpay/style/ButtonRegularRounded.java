package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

/**
 * Created by Michael on 6/20/17.
 */

public class ButtonRegularRounded extends ButtonRounded {
    public ButtonRegularRounded(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTransformationMethod(null);
        BaseFontStyles.applyArialRegularFont(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setStateListAnimator(null);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }
}
