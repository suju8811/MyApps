package com.youlocalcall.quickpay.model.payment;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.account.BaseRequest;
import com.youlocalcall.quickpay.model.payment.profile.PaymentProfile;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Michael on 6/20/17.
 */

public class PaymentAmountRequest extends BaseRequest {

    PaymentProfile paymentProfile = new PaymentProfile();
    boolean isPayPal = false;
    boolean isUpdate = false;
    String totalPrice = "";

    public PaymentAmountRequest(PaymentProfile paymentProfile,
                                boolean isPayPal,
                                boolean isUpdate,
                                String totalPrice) {
        this.paymentProfile = paymentProfile;
        this.isPayPal = isPayPal;
        this.isUpdate = isUpdate;
        this.totalPrice = totalPrice;
    }

    public void post(final APIUtility.APIResponse<CommonResponse> apiResponse, final CommonExecutive executive) {
        paymentService.addAmountByCreditCard(paymentProfile.getPaymentProfileID(),
                paymentProfile.getCardHolderName(),
                paymentProfile.getCvc(),
                paymentProfile.getCardNumber(),
                paymentProfile.getExpiration(),
                "",
                totalPrice,
                paymentProfile.getPhoneNumber(),
                paymentProfile.getEmail(),
                paymentProfile.getUsePersonEmail(),
                paymentProfile.getPersonAddress().getCountry().getName(),
                paymentProfile.getPersonAddress().getBillingAddress(),
                paymentProfile.getUsePersonAddress(),
                paymentProfile.getPersonAddress().getCity(),
                paymentProfile.getPersonAddress().getState().getStateName(),
                paymentProfile.getPersonAddress().getZipCode(),
                isMobile).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }

    public PaymentProfile getPaymentProfile() {
        return paymentProfile;
    }

    public boolean isPayPal() {
        return isPayPal;
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }
}
