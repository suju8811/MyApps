package com.youlocalcall.quickpay.model.payment.refund;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Michael on 6/20/17.
 */

public class GetRefundResponse {

    @SerializedName("customerPaymentID")
    @Expose

    Integer customerPaymentID;

    @SerializedName("customerID")
    @Expose

    Integer customerID;

    @SerializedName("customerNumberID")
    @Expose

    Integer customerNumberID;

    @SerializedName("inmatePaymentID")
    @Expose

    Integer inmatePaymentID;

    @SerializedName("inmateID")
    @Expose

    Integer inmateID;

    @SerializedName("inmateNumberID")
    @Expose

    Integer inmateNumberID;

    @SerializedName("paymentType")
    @Expose

    Integer paymentType;

    @SerializedName("paymentAmount")
    @Expose

    Integer paymentAmount;

    @SerializedName("transactionID")
    @Expose

    String transactionID = "";

    @SerializedName("nameOnCard")
    @Expose

    String nameOnCard;

    @SerializedName("last4DigitsOfCard")
    @Expose

    String last4DigitsOfCard;

    @SerializedName("paymentDate")
    @Expose

    String paymentDate;

    @SerializedName("isPaid")
    @Expose

    Integer isPaid;

    @SerializedName("isRemit")
    @Expose

    Integer isRemit;

    @SerializedName("isInvoice")
    @Expose

    Integer isInvoice;

    @SerializedName("isRefunded")
    @Expose

    Integer isRefunded;

    @SerializedName("description")
    @Expose

    String description;

    @SerializedName("callID")
    @Expose

    Integer callID;

    @SerializedName("duration")
    @Expose

    Integer duration;

    @SerializedName("paymentProfileID")
    @Expose

    String paymentProfileID;

    @SerializedName("paymentOption")
    @Expose

    Integer paymentOption;

    @SerializedName("referralID")
    @Expose

    Integer referralID;

    @SerializedName("createdBy")
    @Expose

    Integer createdBy;

    @SerializedName("createdAt")
    @Expose

    String createdAt;

    public Integer getCustomerPaymentID() {
        return customerPaymentID;
    }

    public void setCustomerPaymentID(Integer customerPaymentID) {
        this.customerPaymentID = customerPaymentID;
    }

    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }

    public Integer getCustomerNumberID() {
        return customerNumberID;
    }

    public void setCustomerNumberID(Integer customerNumberID) {
        this.customerNumberID = customerNumberID;
    }

    public Integer getInmatePaymentID() {
        return inmatePaymentID;
    }

    public void setInmatePaymentID(Integer inmatePaymentID) {
        this.inmatePaymentID = inmatePaymentID;
    }

    public Integer getInmateID() {
        return inmateID;
    }

    public void setInmateID(Integer inmateID) {
        this.inmateID = inmateID;
    }

    public Integer getInmateNumberID() {
        return inmateNumberID;
    }

    public void setInmateNumberID(Integer inmateNumberID) {
        this.inmateNumberID = inmateNumberID;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public Integer getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Integer paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public String getLast4DigitsOfCard() {
        return last4DigitsOfCard;
    }

    public void setLast4DigitsOfCard(String last4DigitsOfCard) {
        this.last4DigitsOfCard = last4DigitsOfCard;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Integer getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Integer isPaid) {
        this.isPaid = isPaid;
    }

    public Integer getIsRemit() {
        return isRemit;
    }

    public void setIsRemit(Integer isRemit) {
        this.isRemit = isRemit;
    }

    public Integer getIsInvoice() {
        return isInvoice;
    }

    public void setIsInvoice(Integer isInvoice) {
        this.isInvoice = isInvoice;
    }

    public Integer getIsRefunded() {
        return isRefunded;
    }

    public void setIsRefunded(Integer isRefunded) {
        this.isRefunded = isRefunded;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCallID() {
        return callID;
    }

    public void setCallID(Integer callID) {
        this.callID = callID;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getPaymentProfileID() {
        return paymentProfileID;
    }

    public void setPaymentProfileID(String paymentProfileID) {
        this.paymentProfileID = paymentProfileID;
    }

    public Integer getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(Integer paymentOption) {
        this.paymentOption = paymentOption;
    }

    public Integer getReferralID() {
        return referralID;
    }

    public void setReferralID(Integer referralID) {
        this.referralID = referralID;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
