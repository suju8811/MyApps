package com.youlocalcall.quickpay.model.payment.refund;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Michael on 6/20/17.
 */

public class GetRefundListResponse {

    @SerializedName("price")
    @Expose

    String price;

    @SerializedName("payment")
    @Expose

    ArrayList<GetRefundResponse> payments = new ArrayList<>();

    @SerializedName("is_settle")
    @Expose

    Integer isSettle;

    @SerializedName("transaction")
    @Expose

    String transaction;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public ArrayList<GetRefundResponse> getPayments() {
        return payments;
    }

    public void setPayments(ArrayList<GetRefundResponse> payments) {
        this.payments = payments;
    }

    public Integer getIsSettle() {
        return isSettle;
    }

    public void setIsSettle(Integer isSettle) {
        this.isSettle = isSettle;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }
}
