package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Michael on 6/20/17.
 */

public class TextViewBold extends android.support.v7.widget.AppCompatTextView {
    public TextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        BaseFontStyles.applyArialBoldFont(this);
    }
}
