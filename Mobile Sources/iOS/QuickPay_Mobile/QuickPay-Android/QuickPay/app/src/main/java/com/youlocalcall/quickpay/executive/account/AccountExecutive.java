package com.youlocalcall.quickpay.executive.account;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.Utility.StringUtility;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.account.ChangePasswordRequest;
import com.youlocalcall.quickpay.model.account.CommonUserInfo;
import com.youlocalcall.quickpay.model.account.EditProfileRequest;
import com.youlocalcall.quickpay.model.account.address.PersonAddress;
import com.youlocalcall.quickpay.model.payment.profile.PaymentPersonAddress;

/**
 * Created by Michael on 6/21/17.
 */

public class AccountExecutive extends PersonAddressParentExecutive {
    private AccountDisplay display;
    private AccountDisplayContext accountDisplayContext = new AccountDisplayContext();

    public AccountExecutive(AccountDisplay display) {
        super(display);
        this.display = display;
    }

    public void displayDidLoad() {
        accountDisplayContext.personAddressDisplayContext = new PersonAddressDisplayContext();
        PersonAddress userPersonAddress = AppData.user.copy();
        userPersonAddress.setAddressType(PaymentPersonAddress.AddressType.TwoAddress);
        accountDisplayContext.getPersonAddressDisplayContext().setPersonAddress(userPersonAddress);
        CommonUserInfo userCommonInfo = AppData.user.copyCommonUserInfo();
        accountDisplayContext.setCommonUserInfo(userCommonInfo);
        display.loadDisplay(accountDisplayContext);
        childExecutive.loadDisplay(accountDisplayContext.personAddressDisplayContext);
    }

    public void didPressAddPrepayAmountButton() {
        display.displayQuickPayRecommendAlert();
    }

    public void didPressResetButton() {
        accountDisplayContext.initialize();
        display.loadDisplay(accountDisplayContext);
        childExecutive.loadDisplay(accountDisplayContext.personAddressDisplayContext);
    }

    public void didPressUpdateProfileButton() {
        final EditProfileRequest request = display.getEditProfileRequest();
        if (request == null) {
            return;
        }
        int validateString = validateEditProfile(request);
        if (validateString != R.string.input_validate) {
            display.showError(validateString);
            return;
        }
        display.showLoading(true);
        request.post(new APIUtility.APIResponse<CommonResponse>() {
            @Override
            public void onResponse(CommonResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.showError(response.getMessage());
            }
        }, this);
    }

    public void didPressResetPasswordButton() {
        display.resetPassword();
    }

    public void didPressUpdatePasswordButton() {
        ChangePasswordRequest request = display.getChangePasswordRequest();
        if (request == null) {
            return;
        }
        int validateString = validateChangePassword(request);
        if (validateString != R.string.input_validate) {
            display.showError(validateString);
            return;
        }
        display.showLoading(true);
        request.post(new APIUtility.APIResponse<CommonResponse>() {
            @Override
            public void onResponse(CommonResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.showError(response.getMessage());
            }
        }, this);
    }

    int validateEditProfile(EditProfileRequest request) {
        if (request.getCommonInfo().getFirstName() == null ||
                request.getCommonInfo().getFirstName().isEmpty()) {
            return R.string.empty_first_name;
        }
        if (request.getCommonInfo().getLastName() == null ||
                request.getCommonInfo().getLastName().isEmpty()) {
            return R.string.empty_last_name;
        }
        if (request.getCommonInfo().getEmail() != null &&
                !request.getCommonInfo().getEmail().isEmpty() &&
                !StringUtility.isValidEmail(request.getCommonInfo().getEmail())) {
            return R.string.invalid_email;
        }
        if (request.getCommonInfo().getMobileNumber() != null &&
                !request.getCommonInfo().getMobileNumber().isEmpty() &&
                !StringUtility.isValidPhone(request.getCommonInfo().getMobileNumber())) {
            return R.string.invalid_phone;
        }
        if (request.getAddressInfo().getCountry().getId() == null) {
            return R.string.empty_country;
        }
        return R.string.input_validate;
    }

    int validateChangePassword(ChangePasswordRequest request) {
        if (request.getOldPassword().isEmpty()) {
            return R.string.empty_old_password;
        }
        if (request.getNewPassword().isEmpty()) {
            return R.string.empty_new_password;
        }
        if (!request.getNewPassword().equals(request.getConfirmPassword())) {
            return R.string.password_mismatch;
        }
        return R.string.input_validate;
    }

    public interface AccountDisplay extends CommonDisplay {
        void loadDisplay(AccountDisplayContext acountDisplayContext);

        void resetPassword();

        EditProfileRequest getEditProfileRequest();

        ChangePasswordRequest getChangePasswordRequest();

        void displayQuickPayRecommendAlert();
    }
}
