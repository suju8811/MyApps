package com.youlocalcall.quickpay.view.account;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.executive.account.PersonAddressDisplayContext;
import com.youlocalcall.quickpay.executive.account.PersonAddressExecutive;
import com.youlocalcall.quickpay.executive.account.PersonAddressExecutive.PersonAddressDisplay;
import com.youlocalcall.quickpay.executive.account.PersonAddressParentExecutive;
import com.youlocalcall.quickpay.model.account.address.Country;
import com.youlocalcall.quickpay.model.account.address.PersonAddress;
import com.youlocalcall.quickpay.model.account.address.State;
import com.youlocalcall.quickpay.model.payment.profile.PaymentPersonAddress;
import com.youlocalcall.quickpay.style.EditTextFieldCommon;
import com.youlocalcall.quickpay.style.SpinnerRoundedBorderRegular;
import com.youlocalcall.quickpay.view.BaseFragment;

import java.util.ArrayList;

public class PersonAddressFragment extends BaseFragment implements PersonAddressDisplay {

    //
    // Delegate data
    //

    private PersonAddressParentExecutive parentExecutive;
    private PersonAddressExecutive executive;
    private ArrayList<State> states = new ArrayList<>();

    //
    // Controls
    //

    private SpinnerRoundedBorderRegular countrySpinner;
    private View address1View;
    private EditTextFieldCommon address1EditText;
    private View address2View;
    private EditTextFieldCommon address2EditText;
    private View billingAddressView;
    private EditTextFieldCommon billingAddressEditText;
    private EditTextFieldCommon cityEditText;
    private SpinnerRoundedBorderRegular stateSpinner;
    private EditTextFieldCommon zipCodeEditText;

    //
    // Helper
    //

    private boolean countryAdapted = false;

    public PersonAddressFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_person_address, container, false);

        //
        // Set Content
        //

        countrySpinner = (SpinnerRoundedBorderRegular) view.findViewById(R.id.countrySpinner);
        address1View = view.findViewById(R.id.address1View);
        address1EditText = (EditTextFieldCommon) view.findViewById(R.id.address1EditText);
        address2View = view.findViewById(R.id.address2View);
        address2EditText = (EditTextFieldCommon) view.findViewById(R.id.address2EditText);
        billingAddressView = view.findViewById(R.id.billingAddressView);
        billingAddressEditText = (EditTextFieldCommon) view.findViewById(R.id.billingAddressEditText);
        cityEditText = (EditTextFieldCommon) view.findViewById(R.id.cityEditText);
        stateSpinner = (SpinnerRoundedBorderRegular) view.findViewById(R.id.stateSpinner);
        zipCodeEditText = (EditTextFieldCommon) view.findViewById(R.id.zipCodeEditText);

        //
        // Create Executive
        //

        executive = new PersonAddressExecutive(this, parentExecutive);
        executive.displayDidLoad();

        return view;
    }

    @Override
    public void refreshLayout(PersonAddressDisplayContext context) {
        if (context.getPersonAddress().getAddressType() == PaymentPersonAddress.AddressType.OneAddress) {
            address1View.setVisibility(View.GONE);
            address2View.setVisibility(View.GONE);
            billingAddressView.setVisibility(View.VISIBLE);
        } else {
            address1View.setVisibility(View.VISIBLE);
            address2View.setVisibility(View.VISIBLE);
            billingAddressView.setVisibility(View.GONE);
        }
    }

    @Override
    public void loadDisplay(PersonAddressDisplayContext context) {

        //
        // Configure Drop Down
        //

        ArrayList<String> countryNames = new ArrayList<>();
        for (Country country : AppData.countries) {
            if (country.getName() != null) {
                countryNames.add(country.getName());
            }
        }
        final ArrayAdapter<String> countryAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, countryNames);
        countrySpinner.setAdapter(countryAdapter);
        Integer selectedCountry = context.getCountryState().getSelectedCountry();
        if (selectedCountry != null) {
            countrySpinner.setSelection(selectedCountry);
        }
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int index, long l) {
                if (!countryAdapted) {
                    countryAdapted = true;
                    return;
                }
                executive.didSelectCountry(index);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        refreshDisplay(context);
    }

    private void refreshDisplay(PersonAddressDisplayContext context) {

        //
        // Refresh Drop Down
        //

        reloadStateDropDown(context);

        //
        // Refresh Values
        //

        address1EditText.setText(context.getPersonAddress().getAddress1());
        address2EditText.setText(context.getPersonAddress().getAddress2());
        billingAddressEditText.setText(context.getPersonAddress().getBillingAddress());
        cityEditText.setText(context.getPersonAddress().getCity());
        zipCodeEditText.setText(context.getPersonAddress().getZipCode());
    }

    @Override
    public void usePersonAddress(boolean isUse) {

        countrySpinner.setEnabled(!isUse);
        if (isUse) {
            String userCountryName = AppData.user.getCountry().getName();
            for (Integer index = 0; index < AppData.countries.size(); index++) {
                if (AppData.countries.get(index).getName() == userCountryName) {
                    countrySpinner.setSelection(index);
                    break;
                }
            }
        }
        billingAddressEditText.setEnabled(!isUse);
        if (isUse) {
            billingAddressEditText.setText(AppData.user.getAddress1());
        }
        cityEditText.setEnabled(!isUse);
        if (isUse) {
            cityEditText.setText(AppData.user.getCity());
        }
        stateSpinner.setEnabled(!isUse);
        if (isUse) {
            String userStateName = AppData.user.getState().getName();
            for (Integer index = 0; index < states.size(); index++) {
                if (states.get(index).getName() == userStateName) {
                    stateSpinner.setSelection(index);
                    break;
                }
            }
        }
        zipCodeEditText.setEnabled(!isUse);
        if (isUse) {
            zipCodeEditText.setText(AppData.user.getZipCode());
        }
    }

    @Override
    public PersonAddress getPersonAddressInfo() {
        PersonAddress personAddress = new PersonAddress();
        Integer selectedCountryIndex = countrySpinner.getSelectedItemPosition();
        if (selectedCountryIndex != null) {
            personAddress.setCountry(AppData.countries.get(selectedCountryIndex));
        }
        personAddress.setAddress1(address1EditText.getText().toString());
        personAddress.setAddress2(address2EditText.getText().toString());
        personAddress.setBillingAddress(billingAddressEditText.getText().toString());
        personAddress.setCity(cityEditText.getText().toString());
        Integer selectedStateIndex = stateSpinner.getSelectedItemPosition();
        if (selectedStateIndex != null) {
            personAddress.setState(states.get(selectedStateIndex));
        }
        personAddress.setZipCode(zipCodeEditText.getText().toString());
        return personAddress;
    }

    @Override
    public void reloadStateDropDown(PersonAddressDisplayContext context) {
        states = context.getCountryState().getStates();
        String statesStrings[] = new String[states.size()];
        for (int index = 0; index < states.size(); index++) {
            statesStrings[index] = states.get(index).getStateName();
        }
        ArrayAdapter<String> stateSpinnerAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, statesStrings);
        stateSpinner.setAdapter(stateSpinnerAdapter);
        if (context.getCountryState().getSelectedState() != null) {
            stateSpinner.setSelection(context.getCountryState().getSelectedState());
        }
    }

    public void setParentExecutive(PersonAddressParentExecutive parentExecutive) {
        this.parentExecutive = parentExecutive;
    }
}
