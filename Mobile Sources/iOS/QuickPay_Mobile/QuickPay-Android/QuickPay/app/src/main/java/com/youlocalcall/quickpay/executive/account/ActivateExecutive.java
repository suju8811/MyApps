package com.youlocalcall.quickpay.executive.account;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.account.activate.ActivateRequest;
import com.youlocalcall.quickpay.model.account.activate.ResendActivationRequest;

/**
 * Created by Michael on 6/21/17.
 */

public class ActivateExecutive extends CommonExecutive {
    ActivateDisplay display;
    String sessionUserName;

    public ActivateExecutive(ActivateDisplay display) {
        super(display);
        this.display = display;
    }

    public void activate() {
        ActivateRequest request = display.getActivateRequest();
        if (request == null) {
            return;
        }
        int validateString = validateRequest(request);
        if (validateString != R.string.input_validate) {
            display.showError(validateString);
            return;
        }
        display.showLoading(true);
        request.post(new APIUtility.APIResponse<CommonResponse>() {
            @Override
            public void onResponse(CommonResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.activated();
            }
        }, this);
    }

    public void resendActivation() {
        if (sessionUserName == null
                || sessionUserName.isEmpty()) {
            sessionUserName = display.getActivateRequest().getUserName();
        }

        int validateString = validateSessionUserName();
        if (validateString != R.string.input_validate) {
            display.showError(validateString);
            return;
        }
        display.displayVerificationMethodSelectMessage(new ResendActivationRequest(sessionUserName));
    }

    public void sendResendActivationRequest(ResendActivationRequest request, boolean isPhone) {
        request.setIsPhone(isPhone ? 1 : 0);
        display.showLoading(true);
        request.post(new APIUtility.APIResponse<CommonResponse>() {
            @Override
            public void onResponse(CommonResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.showError(response.getMessage());
            }
        }, this);
    }

    int validateSessionUserName() {
        if (sessionUserName == null ||
                sessionUserName.isEmpty()) {
            return R.string.empty_user_name;
        }
        return R.string.input_validate;
    }

    int validateRequest(ActivateRequest request) {
        if (request.getUserName().isEmpty()) {
            return R.string.empty_user_name;
        }
        if (request.getPassword().isEmpty()) {
            return R.string.empty_password;
        }
        if (request.getActivationCode().isEmpty()) {
            return R.string.empty_activation_code;
        }
        return R.string.input_validate;
    }

    public interface ActivateDisplay extends CommonExecutive.CommonDisplay {
        void activated();

        ActivateRequest getActivateRequest();

        void displayVerificationMethodSelectMessage(ResendActivationRequest request);
    }
}
