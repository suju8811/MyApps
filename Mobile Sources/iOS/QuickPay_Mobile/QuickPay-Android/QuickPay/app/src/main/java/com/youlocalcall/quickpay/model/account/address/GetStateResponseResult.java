package com.youlocalcall.quickpay.model.account.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.youlocalcall.quickpay.model.CommonResponse;

import java.util.ArrayList;

/**
 * Created by Michael on 6/27/17.
 */

public class GetStateResponseResult extends CommonResponse {

    @SerializedName("state")
    @Expose

    private ArrayList<State> states = new ArrayList<>();

    public ArrayList<State> getStates() {
        return states;
    }

    public void setStates(ArrayList<State> states) {
        this.states = states;
    }
}
