package com.youlocalcall.quickpay.model.payment.refund;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.executive.billing.BillingExecutive;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.account.BaseRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingActor.Customer;

/**
 * Created by Michael on 6/20/17.
 */

public class RefundRequest extends BaseRequest {
    private String from;
    private String kind;
    private Integer paymentId;
    private BillingExecutive.BillingActor actor;

    public RefundRequest() {
        from = "frontend";
        kind = "get";
        paymentId = 0;
        actor = Customer;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public BillingExecutive.BillingActor getActor() {
        return actor;
    }

    public void setActor(BillingExecutive.BillingActor actor) {
        this.actor = actor;
    }

    public void get(final APIUtility.APIResponse<GetRefundRawResponse> apiResponse, final CommonExecutive executive) {
        billingService.getRefund(actor.toString().toLowerCase(),
                from,
                kind,
                paymentId).enqueue(new Callback<GetRefundRawResponse>() {
            @Override
            public void onResponse(Call<GetRefundRawResponse> call, Response<GetRefundRawResponse> response) {
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<GetRefundRawResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }

    public void post(final APIUtility.APIResponse<CommonResponse> apiResponse, final CommonExecutive executive) {
        billingService.postRefund(actor.toString().toLowerCase(),
                from,
                kind,
                paymentId).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }
}
