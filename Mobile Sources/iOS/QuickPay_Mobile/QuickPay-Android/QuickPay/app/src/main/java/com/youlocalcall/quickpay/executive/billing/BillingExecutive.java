package com.youlocalcall.quickpay.executive.billing;

import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.billing.BillingFilterRequest;
import com.youlocalcall.quickpay.model.billing.BillingRequest;
import com.youlocalcall.quickpay.model.billing.BillingResponseArray;
import com.youlocalcall.quickpay.model.payment.refund.RefundRequest;

import java.util.Calendar;

import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingActor.Customer;
import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingActor.Inmate;
import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingType.Invoice;
import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.DateType.From;
import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.DateType.To;

/**
 * Created by Michael on 6/21/17.
 */

public class BillingExecutive extends CommonExecutive {

    private final String InvoiceTitle = "View Invoices";
    private final String InvoiceSubTitle = "Browse All Invoices";
    private final String PaymentTitle = "View Payments";
    private final String PaymentSubTitle = "Browse All Payment Details";
    BillingDisplay display;
    BillingFilterExecutive filterExecutive;
    private BillingDisplayContext customerDisplayContext = new BillingDisplayContext();
    private BillingDisplayContext inmateDisplayContext = new BillingDisplayContext();
    private BillingActor selectedActor = Customer;

    public BillingExecutive(BillingDisplay display, BillingDisplayContext customerContext, BillingDisplayContext inmateContext) {
        super(display);
        this.display = display;
        customerDisplayContext = customerContext;
        inmateDisplayContext = inmateContext;
    }

    //
    // Event Handler
    //

    public void displayDidLoad() {
        customerDisplayContext.setTitle(getDisplayTitle());
        customerDisplayContext.setSubtitle(getDisplaySubTitle());
        refreshDisplay();
    }

    public void reloadFilter(BillingFilterExecutive filterExecutive, BillingActor actor) {
        BillingFilterRequest beforeRequest;
        if (actor == Customer) {
            beforeRequest = customerDisplayContext.getFilterRequest();
        } else {
            beforeRequest = inmateDisplayContext.getFilterRequest();
        }

        filterExecutive.displayDidLoad(beforeRequest);
        filterExecutive.refreshLayout(beforeRequest);
    }

    public void didPressCustomerButton() {
        if (selectedActor == Customer) {
            return;
        }
        selectedActor = Customer;
        inmateDisplayContext.setFilterRequest(display.getInmateFilterRequest());
        refreshDisplay();
    }

    public void didPressInmateButton() {
        if (selectedActor == Inmate) {
            return;
        }
        selectedActor = Inmate;
        customerDisplayContext.setFilterRequest(display.getCustomerFilterRequest());
        refreshDisplay();
    }

    public void didPressDateButton(BillingFilterExecutive filterExecutive, DateType dateType) {
        this.filterExecutive = filterExecutive;
        BillingDisplayContext displayContext = customerDisplayContext;
        if (selectedActor == Inmate) {
            displayContext = inmateDisplayContext;
        }
        String date = displayContext.getFilterRequest().getDate();
        if (dateType == From) {
            date = displayContext.getFilterRequest().getDateStart();
        } else if (dateType == To) {
            date = displayContext.getFilterRequest().getDateEnd();
        }
        String components[] = date.split("/");
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DATE);
        if (components.length == 3) {
            month = Integer.valueOf(components[0]);
            day = Integer.valueOf(components[1]);
            year = Integer.valueOf(components[2]);
        }
        display.showCalendar(year, month, day, dateType);
    }

    public void didDateChanged(int year, int month, int day, DateType type) {
        String date = String.format("%02d/%02d/%4d", month + 1, day, year);
        if (selectedActor == Customer) {
            customerDisplayContext.setFilterRequest(display.getCustomerFilterRequest());
            switch (type) {
                case Current:
                    customerDisplayContext.getFilterRequest().setDate(date);
                    break;
                case From:
                    customerDisplayContext.getFilterRequest().setDateStart(date);
                    break;
                case To:
                    customerDisplayContext.getFilterRequest().setDateEnd(date);
                    break;
            }
            filterExecutive.displayDidLoad(customerDisplayContext.getFilterRequest());
        } else {
            inmateDisplayContext.setFilterRequest(display.getInmateFilterRequest());
            switch (type) {
                case Current:
                    inmateDisplayContext.getFilterRequest().setDate(date);
                    break;
                case From:
                    inmateDisplayContext.getFilterRequest().setDateStart(date);
                    break;
                case To:
                    inmateDisplayContext.getFilterRequest().setDateEnd(date);
                    break;
            }
            filterExecutive.displayDidLoad(inmateDisplayContext.getFilterRequest());
        }
    }

    //
    // Helper
    //

    String getDisplayTitle() {
        String title = PaymentTitle;
        if (customerDisplayContext.getFilterRequest().getBillingType() == Invoice) {
            title = InvoiceTitle;
        }
        return title;
    }

    String getDisplaySubTitle() {
        String subTitle = PaymentSubTitle;
        if (customerDisplayContext.getFilterRequest().getBillingType() == Invoice) {
            subTitle = InvoiceSubTitle;
        }
        return subTitle;
    }

    void refreshDisplay() {
        display.refreshDisplay(customerDisplayContext, inmateDisplayContext, selectedActor);
    }

    public void loadBilling() {
        BillingRequest request = display.getBillingRequest(selectedActor);
        if (request == null) {
            return;
        }
        display.showLoading(true);
        request.post(new APIUtility.APIResponse<BillingResponseArray>() {
            @Override
            public void onResponse(BillingResponseArray billings) {
                display.showLoading(false);
                if (billings != null) {
                    display.reloadBillingTable(selectedActor, billings);
                }
            }
        }, this);
    }

    public void gotoRefund(Integer paymentId) {
        RefundRequest getRefundListRequest = new RefundRequest();
        getRefundListRequest.setPaymentId(paymentId);
        getRefundListRequest.setActor(selectedActor);
        display.gotoRefund(getRefundListRequest);
    }

    public enum BillingType {
        Invoice("Invoice"),
        Payment("Payment");

        private final String rawValue;

        private BillingType(String rawValue) {
            this.rawValue = rawValue;
        }

        @Override
        public String toString() {
            return rawValue;
        }
    }

    public enum DateType {
        Current,
        From,
        To
    }

    public enum BillingActor {
        Customer("Customer"),
        Inmate("Inmate");

        private final String rawValue;

        private BillingActor(String rawValue) {
            this.rawValue = rawValue;
        }

        @Override
        public String toString() {
            return rawValue;
        }
    }

    public interface BillingDisplay extends CommonExecutive.CommonDisplay {
        void refreshDisplay(BillingDisplayContext customerContext, BillingDisplayContext inmateContext, BillingActor selectedActor);
        void showCalendar(int year, int month, int day, DateType dateType);
        BillingRequest getBillingRequest(BillingActor actor);
        BillingFilterRequest getCustomerFilterRequest();
        BillingFilterRequest getInmateFilterRequest();
        void reloadBillingTable(BillingActor actor, BillingResponseArray billingResponseArray);
        void gotoRefund(RefundRequest getRefundListRequest);
    }
}
