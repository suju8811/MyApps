package com.youlocalcall.quickpay.model.billing;

import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.payment.refund.GetRefundRawResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface BillingService {

    @FormUrlEncoded
    @POST("billing/{billingType}/load_{billingActor}.do")
    Call<BillingResponseArray> loadBillings(@Field("draw") Integer draw,
                                            @Field("order") ArrayList<BillingOrderRequest> orderRequests,
                                            @Field("start") Integer start,
                                            @Field("length") Integer length,
                                            @Field("search[value]") String searchValue,
                                            @Field("search[regex]") Boolean searchRegex,
                                            @Field("payment_date") String paymentDate,
                                            @Field("payment_date_start") String paymentDateStart,
                                            @Field("payment_date_end") String paymentDateEnd,
                                            @Field("payment_date_option") Integer paymentDateOption,
                                            @Field("payment_type") String paymentType,
                                            @Field("payment_amount") String paymentAmount,
                                            @Field("payment_amount_start") String paymentAmountStart,
                                            @Field("payment_amount_end") String paymentAmountEnd,
                                            @Field("payment_amount_option") Integer paymentAmountOption,
                                            @Field("inmate_balance") String inmateBalance,
                                            @Field("inmate_balance_start") String inmateBalanceStart,
                                            @Field("inmate_balance_end") String inmateBalanceEnd,
                                            @Field("inmate_balance_option") Integer inmateBalanceOption,
                                            @Path("billingType") String billingType,
                                            @Path("billingActor") String billingActor);

    @FormUrlEncoded
    @POST("billing/refund/{billingActor}.do")
    Call<GetRefundRawResponse> getRefund(@Path("billingActor") String billingActor,
                                         @Field("from") String from,
                                         @Field("kind") String kind,
                                         @Field("payment_id") Integer paymentId);

    @FormUrlEncoded
    @POST("billing/refund/{billingActor}.do")
    Call<CommonResponse> postRefund(@Path("billingActor") String billingActor,
                                    @Field("from") String from,
                                    @Field("kind") String kind,
                                    @Field("payment_id") Integer paymentId);

}
