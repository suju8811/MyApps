package com.youlocalcall.quickpay.model.payment;

import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.payment.profile.PaymentProfileResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PaymentService {

    @POST("payment/get_profile.do")
    Call<PaymentProfileResponse> loadPaymentProfile();

    @FormUrlEncoded
    @POST("customer/update_creditcard.do")
    Call<CommonResponse> updatePaymentProfile(@Field("email_address") String email,
                                              @Field("phone_number") String phone,
                                              @Field("use_person_email") Integer usePersonEmail,
                                              @Field("cardholder") String cardHolder,
                                              @Field("cvc") String cvc,
                                              @Field("card_number") String cardNumber,
                                              @Field("expiration") String expiration,
                                              @Field("use_person_address") Integer usePersonAddress,
                                              @Field("country") String countryName,
                                              @Field("address") String address,
                                              @Field("city") String city,
                                              @Field("state") String stateName,
                                              @Field("zip_code") String zipCode,
                                              @Field("auto_payment") String autoPayment,
                                              @Field("is_mobile") Integer isMobile);

    @FormUrlEncoded
    @POST("payment/quick/add.do")
    Call<CommonResponse> paymentByCreditCard(@Field("inmate_id") Integer inmateId,
                                             @Field("payment_profile_id") String paymentProfileID,
                                             @Field("cardholder") String cardHolder,
                                             @Field("cvc") String cvc,
                                             @Field("card_number") String cardNumber,
                                             @Field("expiration") String expiration,
                                             @Field("is_paypal") String isPayPal,
                                             @Field("total_price") String totalPrice,
                                             @Field("phone_number") String phone,
                                             @Field("email_address") String email,
                                             @Field("use_person_email") Integer usePersonEmail,
                                             @Field("country") String country,
                                             @Field("address") String address,
                                             @Field("use_person_address") Integer usePersonAddress,
                                             @Field("city") String city,
                                             @Field("state") String state,
                                             @Field("zip_code") String zipCode);

    @FormUrlEncoded
    @POST("payment/amount/add.do")
    Call<CommonResponse> addAmountByCreditCard(@Field("payment_profile_id") String paymentProfileID,
                                               @Field("cardholder") String cardHolder,
                                               @Field("cvc") String cvc,
                                               @Field("card_number") String cardNumber,
                                               @Field("expiration") String expiration,
                                               @Field("is_paypal") String isPayPal,
                                               @Field("total_price") String totalPrice,
                                               @Field("phone_number") String phone,
                                               @Field("email_address") String email,
                                               @Field("use_person_email") Integer usePersonEmail,
                                               @Field("country") String country,
                                               @Field("address") String address,
                                               @Field("use_person_address") Integer usePersonAddress,
                                               @Field("city") String city,
                                               @Field("state") String state,
                                               @Field("zip_code") String zipCode,
                                               @Field("is_mobile") Integer isMobile);

    @FormUrlEncoded
    @POST("paypal/{userID}/{inmateId}/{totalAmount}/inmate.do")
    Call<CommonResponse> proceedQuickPayConfirm(@Path("userID") String userId,
                                                @Path("inmateId") String inmateId,
                                                @Path("totalAmount") String totalAmount,
                                                @Field("payment_id") String paymentId);

    @FormUrlEncoded
    @POST("paypal/{userID}/{totalAmount}/customer.do")
    Call<CommonResponse> proceedPrepayPayConfirm(@Path("userID") String userId,
                                                 @Path("totalAmount") String totalAmount,
                                                 @Field("payment_id") String paymentId);
}
