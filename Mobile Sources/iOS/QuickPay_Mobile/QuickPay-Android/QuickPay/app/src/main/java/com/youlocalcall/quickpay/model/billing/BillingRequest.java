package com.youlocalcall.quickpay.model.billing;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.executive.billing.BillingExecutive;
import com.youlocalcall.quickpay.model.account.BaseRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingActor.Customer;
import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingType.Payment;

/**
 * Created by Michael on 6/20/17.
 */

public class BillingRequest extends BaseRequest {
    private Integer draw = 0;
    private ArrayList<BillingOrderRequest> order = new ArrayList<>();
    private Integer start = 0;
    private Integer length = 10;
    private BillingSearchRequest search = new BillingSearchRequest();
    private BillingFilterRequest filterRequest = new BillingFilterRequest();
    private BillingExecutive.BillingType billingType = Payment;
    private BillingExecutive.BillingActor billingActor = Customer;

    public Integer getDraw() {
        return draw;
    }

    public void setDraw(Integer draw) {
        this.draw = draw;
    }

    public ArrayList<BillingOrderRequest> getOrder() {
        return order;
    }

    public void setOrder(ArrayList<BillingOrderRequest> order) {
        this.order = order;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public BillingSearchRequest getSearch() {
        return search;
    }

    public void setSearch(BillingSearchRequest search) {
        this.search = search;
    }

    public BillingFilterRequest getFilterRequest() {
        return filterRequest;
    }

    public void setFilterRequest(BillingFilterRequest filterRequest) {
        this.filterRequest = filterRequest;
    }

    public BillingExecutive.BillingType getBillingType() {
        return billingType;
    }

    public void setBillingType(BillingExecutive.BillingType billingType) {
        this.billingType = billingType;
    }

    public BillingExecutive.BillingActor getBillingActor() {
        return billingActor;
    }

    public void setBillingActor(BillingExecutive.BillingActor billingActor) {
        this.billingActor = billingActor;
    }

    public void post(final APIUtility.APIResponse<BillingResponseArray> apiResponse, final CommonExecutive executive) {
        billingService.loadBillings(draw,
                order,
                start,
                length,
                search.getValue(),
                search.getRegex(),
                filterRequest.getDate(),
                filterRequest.getDateStart(),
                filterRequest.getDateEnd(),
                filterRequest.getDateOption(),
                filterRequest.getType(),
                filterRequest.getAmount(),
                filterRequest.getAmountStart(),
                filterRequest.getAmountEnd(),
                filterRequest.getAmountOption(),
                filterRequest.getInmateBalance(),
                filterRequest.getInmateBalanceStart(),
                filterRequest.getInmateBalanceEnd(),
                filterRequest.getInmateBalanceOption(),
                billingType.toString().toLowerCase(),
                billingActor.toString().toLowerCase()).enqueue(new Callback<BillingResponseArray>() {
            @Override
            public void onResponse(Call<BillingResponseArray> call, Response<BillingResponseArray> response) {
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<BillingResponseArray> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }
}
