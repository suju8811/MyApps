package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Michael on 6/22/17.
 */

public class CheckBoxRegular extends android.support.v7.widget.AppCompatCheckBox {
    public CheckBoxRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        BaseFontStyles.applyArialRegularFont(this);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        setTextColor(BaseColors.YLC_BLACK);
    }
}
