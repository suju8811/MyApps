package com.youlocalcall.quickpay.model.account.address;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.account.BaseRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetStateRequest extends BaseRequest {
    private Integer country;

    public GetStateRequest(Integer country) {
        this.country = country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public void post(final APIUtility.APIResponse<GetStateResponse> apiResponse, final CommonExecutive executive) {
        accountService.getState(country).enqueue(new Callback<GetStateResponse>() {
            @Override
            public void onResponse(Call<GetStateResponse> call, Response<GetStateResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<GetStateResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }
}
