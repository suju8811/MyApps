package com.youlocalcall.quickpay.executive.billing;

import com.youlocalcall.quickpay.model.billing.BillingFilterRequest;

public class BillingDisplayContext {
    private BillingExecutive.BillingActor actor = BillingExecutive.BillingActor.Customer;
    private String title = "";
    private String subtitle = "";
    private BillingFilterRequest filterRequest = new BillingFilterRequest();

    public BillingExecutive.BillingActor getActor() {
        return actor;
    }

    public void setActor(BillingExecutive.BillingActor actor) {
        this.actor = actor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public BillingFilterRequest getFilterRequest() {
        return filterRequest;
    }

    public void setFilterRequest(BillingFilterRequest filterRequest) {
        this.filterRequest = filterRequest;
    }
}
