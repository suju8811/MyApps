package com.youlocalcall.quickpay.model.payment.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.account.address.PersonAddress;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Michael on 6/20/17.
 */

public class PaymentProfile extends PaymentPersonAddress {

    @SerializedName("paymentProfileID")
    @Expose
    String paymentProfileID;

    @SerializedName("firstName")
    @Expose
    String firstName;

    @SerializedName("lastName")
    @Expose
    String lastName;

    @SerializedName("cardHolderName")
    @Expose
    String cardHolderName;

    @SerializedName("cvc")
    @Expose
    String cvc;

    @SerializedName("usePersonAddress")
    @Expose
    Integer usePersonAddress;

    @SerializedName("cardNumber")
    @Expose
    String cardNumber;

    @SerializedName("expiration")
    @Expose
    String expiration;

    @SerializedName("email")
    @Expose
    String email;

    @SerializedName("usePersonEmail")
    @Expose
    Integer usePersonEmail;

    @SerializedName("phoneNumber")
    @Expose
    String phoneNumber;

    @SerializedName("autoPayment")
    @Expose
    String autoPayment;

    @SerializedName("message")
    @Expose
    String message;

    @SerializedName("newCVC")
    @Expose
    Boolean newCVC;

    private PersonAddress personAddress = new PersonAddress();

    public String getPaymentProfileID() {
        return paymentProfileID;
    }

    public void setPaymentProfileID(String paymentProfileID) {
        this.paymentProfileID = paymentProfileID;
    }

    public PaymentProfile copy() {
        PaymentProfile paymentProfile = new PaymentProfile();
        paymentProfile.paymentProfileID = paymentProfileID;
        paymentProfile.firstName = firstName;
        paymentProfile.lastName = lastName;
        paymentProfile.cardHolderName = cardHolderName;
        paymentProfile.cvc = cvc;
        paymentProfile.usePersonAddress = usePersonAddress;
        paymentProfile.personAddress = personAddress.copy();
        paymentProfile.cardNumber = cardNumber;
        paymentProfile.expiration = expiration;
        paymentProfile.email = email;
        paymentProfile.usePersonEmail = usePersonEmail;
        paymentProfile.phoneNumber = phoneNumber;
        paymentProfile.autoPayment = autoPayment;
        paymentProfile.message = message;
        paymentProfile.newCVC = newCVC;
        return paymentProfile;
    }

    public void post(final APIUtility.APIResponse<CommonResponse> apiResponse, final CommonExecutive executive) {
        paymentService.updatePaymentProfile(email,
                phoneNumber,
                usePersonEmail,
                cardHolderName,
                cvc,
                cardNumber,
                expiration,
                usePersonAddress,
                personAddress.getCountry().getName(),
                personAddress.getBillingAddress(),
                personAddress.getCity(),
                personAddress.getState().getStateName(),
                personAddress.getZipCode(),
                autoPayment,
                isMobile).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public Integer getUsePersonAddress() {
        return usePersonAddress;
    }

    public void setUsePersonAddress(Integer usePersonAddress) {
        this.usePersonAddress = usePersonAddress;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getUsePersonEmail() {
        return usePersonEmail;
    }

    public void setUsePersonEmail(Integer usePersonEmail) {
        this.usePersonEmail = usePersonEmail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAutoPayment() {
        return autoPayment;
    }

    public void setAutoPayment(String autoPayment) {
        this.autoPayment = autoPayment;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getNewCVC() {
        return newCVC;
    }

    public void setNewCVC(Boolean newCVC) {
        this.newCVC = newCVC;
    }

    public PersonAddress getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(PersonAddress personAddress) {
        this.personAddress = personAddress;
    }
}
