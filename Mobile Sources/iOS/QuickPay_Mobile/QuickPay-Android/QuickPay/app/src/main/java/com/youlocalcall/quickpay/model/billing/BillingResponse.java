package com.youlocalcall.quickpay.model.billing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillingResponse {

    @SerializedName("transaction_id")
    @Expose

    private String transactionID;

    @SerializedName("prepaid_amount")
    @Expose

    private Double prepaidAmount;

    @SerializedName("additional")
    @Expose

    private String additional;

    @SerializedName("inmate_balance")
    @Expose

    private String inmateBalance;

    @SerializedName("payment_amount")
    @Expose

    private String paymentAmount;

    @SerializedName("description")
    @Expose

    private String description;

    @SerializedName("is_remit")
    @Expose

    private Integer isRemit;

    @SerializedName("inmate_name")
    @Expose

    private String inmateName;

    @SerializedName("payment_type")
    @Expose

    private String paymentType;

    @SerializedName("last_4_digits")
    @Expose

    private String last4Digits;

    @SerializedName("is_refunded")
    @Expose

    private Integer isRefunded;

    @SerializedName("payment_profile_id")
    @Expose

    private String paymentProfileID;

    @SerializedName("notification_amount")
    @Expose

    private Integer notificationAmount;

    @SerializedName("id")
    @Expose

    private Integer id;

    @SerializedName("is_paid")
    @Expose

    private Integer isPaid;

    @SerializedName("payment_date")
    @Expose

    private String paymentDate;

    @SerializedName("name_on_card")
    @Expose

    private String nameOnCard;

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public Double getPrepaidAmount() {
        return prepaidAmount;
    }

    public void setPrepaidAmount(Double prepaidAmount) {
        this.prepaidAmount = prepaidAmount;
    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    public String getInmateBalance() {
        return inmateBalance;
    }

    public void setInmateBalance(String inmateBalance) {
        this.inmateBalance = inmateBalance;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIsRemit() {
        return isRemit;
    }

    public void setIsRemit(Integer isRemit) {
        this.isRemit = isRemit;
    }

    public String getInmateName() {
        return inmateName;
    }

    public void setInmateName(String inmateName) {
        this.inmateName = inmateName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public void setLast4Digits(String last4Digits) {
        this.last4Digits = last4Digits;
    }

    public Integer getIsRefunded() {
        return isRefunded;
    }

    public void setIsRefunded(Integer isRefunded) {
        this.isRefunded = isRefunded;
    }

    public String getPaymentProfileID() {
        return paymentProfileID;
    }

    public void setPaymentProfileID(String paymentProfileID) {
        this.paymentProfileID = paymentProfileID;
    }

    public Integer getNotificationAmount() {
        return notificationAmount;
    }

    public void setNotificationAmount(Integer notificationAmount) {
        this.notificationAmount = notificationAmount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Integer isPaid) {
        this.isPaid = isPaid;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }
}
