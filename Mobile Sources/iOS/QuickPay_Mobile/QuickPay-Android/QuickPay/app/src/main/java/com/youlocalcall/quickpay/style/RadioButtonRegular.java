package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

/**
 * Created by scottgregg on 28/02/2017.
 */

public class RadioButtonRegular extends android.support.v7.widget.AppCompatRadioButton {
    public RadioButtonRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTransformationMethod(null);
        BaseFontStyles.applyArialRegularFont(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setStateListAnimator(null);
        }
    }
}
