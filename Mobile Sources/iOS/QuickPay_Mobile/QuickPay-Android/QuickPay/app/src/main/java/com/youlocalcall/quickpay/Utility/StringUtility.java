package com.youlocalcall.quickpay.Utility;

import android.os.Build;
import android.text.Html;

/**
 * Created by Michael on 6/23/17.
 */

public class StringUtility {
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public final static boolean isValidPhone(CharSequence target) {
        if (target == null) {
            return false;
        }
        return target.length() == 10;
    }

    public final static String removeNonNumber(String original) {
        return original.replaceAll("[^0-9]", "");
    }

    public final static String html2String(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(html).toString();
        }
    }
}
