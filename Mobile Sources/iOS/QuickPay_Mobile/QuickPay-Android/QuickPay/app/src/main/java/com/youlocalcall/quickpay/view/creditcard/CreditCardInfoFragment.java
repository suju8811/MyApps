package com.youlocalcall.quickpay.view.creditcard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.StringUtility;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.executive.payment.CreditCardInfoExecutive;
import com.youlocalcall.quickpay.executive.payment.CreditCardInfoExecutive.CreditCardInfoDisplay;
import com.youlocalcall.quickpay.executive.payment.CreditCardParentExecutive;
import com.youlocalcall.quickpay.model.payment.profile.PaymentProfile;
import com.youlocalcall.quickpay.style.CheckBoxRegular;
import com.youlocalcall.quickpay.style.EditTextFieldCommon;
import com.youlocalcall.quickpay.view.BaseFragment;
import com.youlocalcall.quickpay.view.account.PersonAddressFragment;

import br.com.sapereaude.maskedEditText.MaskedEditText;

public class CreditCardInfoFragment extends BaseFragment implements View.OnClickListener, CreditCardInfoDisplay {

    //
    // Delegated variable
    //

    private PersonAddressFragment personAddressFragment;
    private CreditCardInfoExecutive executive;
    private CreditCardParentExecutive parentExecutive;

    //
    // Bind controls
    //

    private EditTextFieldCommon emailEditText;
    private CheckBoxRegular usePersonEmailCheckBox;
    private MaskedEditText phoneNumberEditText;
    private EditTextFieldCommon cardHolderEditText;
    private EditTextFieldCommon cardNumberEditText;
    private EditTextFieldCommon cvcEditText;
    private EditTextFieldCommon expirationEditText;
    private CheckBoxRegular usePersonAddressCheckBox;

    public CreditCardInfoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        personAddressFragment = new PersonAddressFragment();
        placeContent(R.id.person_address_container, personAddressFragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_credit_card_info, container, false);

        //
        // Set Content
        //

        emailEditText = (EditTextFieldCommon) view.findViewById(R.id.emailEditText);
        usePersonEmailCheckBox = (CheckBoxRegular) view.findViewById(R.id.usePersonEmailCheckBox);
        phoneNumberEditText = (MaskedEditText) view.findViewById(R.id.phoneNumberEditText);
        cardHolderEditText = (EditTextFieldCommon) view.findViewById(R.id.cardHolderEditText);
        cardNumberEditText = (EditTextFieldCommon) view.findViewById(R.id.cardNumberEditText);
        cvcEditText = (EditTextFieldCommon) view.findViewById(R.id.cvcEditText);
        expirationEditText = (EditTextFieldCommon) view.findViewById(R.id.expirationEditText);
        usePersonAddressCheckBox = (CheckBoxRegular) view.findViewById(R.id.usePersonAddressCheckBox);

        //
        // Set Event Handler
        //
        usePersonEmailCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                executive.didSelectPersonEmail(!usePersonEmailCheckBox.isSelected());
            }
        });
        usePersonAddressCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                executive.didSelectPersonAddress(!usePersonAddressCheckBox.isSelected());
            }
        });

        //
        // Set executive
        //

        executive = new CreditCardInfoExecutive(this, parentExecutive);
        personAddressFragment.setParentExecutive(executive);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
        }
    }

    @Override
    public void loadDisplay(PaymentProfile context) {
        emailEditText.setText(context.getEmail());
        usePersonEmailCheckBox.setSelected(context.getUsePersonEmail() != null &&
                context.getUsePersonEmail() == 1);
        phoneNumberEditText.setText(context.getPhoneNumber());
        cardHolderEditText.setText(context.getCardHolderName());
        cardNumberEditText.setText(context.getCardNumber());
        cvcEditText.setText(context.getCvc());
        expirationEditText.setText(context.getExpiration());
    }

    @Override
    public void usePersonEmail(boolean isUse) {
        usePersonEmailCheckBox.setSelected(isUse);
        emailEditText.setEnabled(!isUse);
        if (isUse) {
            emailEditText.setText(AppData.user.getEmail());
        }
    }

    @Override
    public void usePersonAddress(boolean isUse) {
        usePersonAddressCheckBox.setSelected(isUse);
    }

    @Override
    public PaymentProfile getCreditCardUpdateRequest() {
        PaymentProfile request = new PaymentProfile();
        request.setUsePersonEmail(usePersonEmailCheckBox.isSelected() ? 1 : 0);
        request.setEmail(emailEditText.getText().toString());
        request.setPhoneNumber(StringUtility.removeNonNumber(phoneNumberEditText.getText().toString()));
        request.setCardHolderName(cardHolderEditText.getText().toString());
        request.setCardNumber(cardNumberEditText.getText().toString());
        request.setCvc(cvcEditText.getText().toString());
        request.setExpiration(expirationEditText.getText().toString());
        request.setUsePersonAddress(usePersonAddressCheckBox.isSelected() ? 1 : 0);
        request.setPersonAddress(personAddressFragment.getPersonAddressInfo());
        return request;
    }

    public void setParentExecutive(CreditCardParentExecutive parentExecutive) {
        this.parentExecutive = parentExecutive;
    }
}
