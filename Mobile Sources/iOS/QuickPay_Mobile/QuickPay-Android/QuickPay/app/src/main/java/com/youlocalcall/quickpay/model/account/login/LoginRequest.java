package com.youlocalcall.quickpay.model.account.login;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.account.BaseRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRequest extends BaseRequest {

    private String userName;
    private String password;

    public LoginRequest(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public void post(final APIUtility.APIResponse<LoginResponse> apiResponse,
                     final CommonExecutive executive) {

        accountService.login(userName,
                password,
                isMobile).enqueue(new Callback<LoginResponse>() {

            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                AppData.user = response.body().getResult().getUser();
                if (AppData.user.getInmateId() == 0) {
                    AppData.user.setInmateId(null);
                }
                if (AppData.user.getAllowCustomer() == 1) {
                    AppData.user.setInmateId(null);
                }
                AppData.payPalInfo = response.body().getResult().getPayPalInfo();
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }
}
