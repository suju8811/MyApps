package com.youlocalcall.quickpay.model.billing;

import com.youlocalcall.quickpay.executive.billing.BillingExecutive;

import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingType.Invoice;

/**
 * Created by Michael on 6/20/17.
 */

public class BillingFilterRequest {
    private String date = "";
    private String dateStart = "";
    private String dateEnd = "";
    private Integer dateOption = 0;
    private String type = "";
    private BillingExecutive.BillingType billingType = Invoice;
    private String amount = "";
    private String amountStart = "";
    private String amountEnd = "";
    private Integer amountOption = 1;
    private String inmateBalance = "";
    private String inmateBalanceStart = "";
    private String inmateBalanceEnd = "";
    private Integer inmateBalanceOption = 1;
    private boolean expanded = false;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Integer getDateOption() {
        return dateOption;
    }

    public void setDateOption(Integer dateOption) {
        this.dateOption = dateOption;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BillingExecutive.BillingType getBillingType() {
        return billingType;
    }

    public void setBillingType(BillingExecutive.BillingType billingType) {
        this.billingType = billingType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmountStart() {
        return amountStart;
    }

    public void setAmountStart(String amountStart) {
        this.amountStart = amountStart;
    }

    public String getAmountEnd() {
        return amountEnd;
    }

    public void setAmountEnd(String amountEnd) {
        this.amountEnd = amountEnd;
    }

    public Integer getAmountOption() {
        return amountOption;
    }

    public void setAmountOption(Integer amountOption) {
        this.amountOption = amountOption;
    }

    public String getInmateBalance() {
        return inmateBalance;
    }

    public void setInmateBalance(String inmateBalance) {
        this.inmateBalance = inmateBalance;
    }

    public String getInmateBalanceStart() {
        return inmateBalanceStart;
    }

    public void setInmateBalanceStart(String inmateBalanceStart) {
        this.inmateBalanceStart = inmateBalanceStart;
    }

    public String getInmateBalanceEnd() {
        return inmateBalanceEnd;
    }

    public void setInmateBalanceEnd(String inmateBalanceEnd) {
        this.inmateBalanceEnd = inmateBalanceEnd;
    }

    public Integer getInmateBalanceOption() {
        return inmateBalanceOption;
    }

    public void setInmateBalanceOption(Integer inmateBalanceOption) {
        this.inmateBalanceOption = inmateBalanceOption;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }
}
