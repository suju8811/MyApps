package com.youlocalcall.quickpay.model.billing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Michael on 6/20/17.
 */

public class BillingSearchRequest {

    @SerializedName("regex")
    @Expose

    Boolean regex = false;
    @SerializedName("value")
    @Expose

    private String value = "";

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getRegex() {
        return regex;
    }

    public void setRegex(Boolean regex) {
        this.regex = regex;
    }
}
