package com.youlocalcall.quickpay.model.account.Inmate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Michael on 6/27/17.
 */

public class InmateResponseResult {

    @SerializedName("inmate")
    @Expose

    private ArrayList<Inmate> inmates = new ArrayList<>();

    public ArrayList<Inmate> getInmates() {
        return inmates;
    }
}
