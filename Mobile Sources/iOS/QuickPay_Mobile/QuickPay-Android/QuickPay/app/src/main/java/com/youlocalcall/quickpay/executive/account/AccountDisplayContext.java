package com.youlocalcall.quickpay.executive.account;

import com.youlocalcall.quickpay.model.account.CommonUserInfo;

/**
 * Created by Michael on 6/28/17.
 */

public class AccountDisplayContext {

    PersonAddressDisplayContext personAddressDisplayContext = new PersonAddressDisplayContext();
    CommonUserInfo commonUserInfo = new CommonUserInfo();

    /**
     * Initialize values
     */

    void initialize() {
        personAddressDisplayContext.initialize();
        commonUserInfo.initializeCommonUserInfo();
    }

    public PersonAddressDisplayContext getPersonAddressDisplayContext() {
        return personAddressDisplayContext;
    }

    public void setPersonAddressDisplayContext(PersonAddressDisplayContext personAddressDisplayContext) {
        this.personAddressDisplayContext = personAddressDisplayContext;
    }

    public CommonUserInfo getCommonUserInfo() {
        return commonUserInfo;
    }

    public void setCommonUserInfo(CommonUserInfo commonUserInfo) {
        this.commonUserInfo = commonUserInfo;
    }
}
