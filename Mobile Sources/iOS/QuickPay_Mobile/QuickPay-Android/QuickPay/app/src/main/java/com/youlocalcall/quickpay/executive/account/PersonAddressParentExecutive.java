package com.youlocalcall.quickpay.executive.account;

import com.youlocalcall.quickpay.executive.CommonExecutive;

/**
 * Created by Michael on 6/28/17.
 */

public class PersonAddressParentExecutive extends CommonExecutive {

    protected PersonAddressExecutive childExecutive;

    public PersonAddressParentExecutive(CommonDisplay display) {
        super(display);
    }

    public void displayDidLoad() {
    }
}
