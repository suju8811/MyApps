package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.util.AttributeSet;

public class ButtonRegularRoundedWhiteGray extends ButtonRegularRounded {
    public ButtonRegularRoundedWhiteGray(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        setRoundedBackground(BaseColors.YLC_WHITE, BaseColors.YLC_GRAY3, 1);
        setTextColor(BaseColors.YLC_GRAY5);
    }
}
