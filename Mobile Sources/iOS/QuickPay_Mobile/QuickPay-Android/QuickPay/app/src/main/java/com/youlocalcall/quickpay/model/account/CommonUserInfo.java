package com.youlocalcall.quickpay.model.account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.youlocalcall.quickpay.model.account.address.PersonAddress;

/**
 * Created by Michael on 6/20/17.
 */

public class CommonUserInfo extends PersonAddress {

    @SerializedName("userName")
    @Expose

    private String name;

    @SerializedName("firstName")
    @Expose

    private String firstName;

    @SerializedName("middleName")
    @Expose

    private String middleName;

    @SerializedName("lastName")
    @Expose

    private String lastName;

    @SerializedName("email")
    @Expose

    private String email;

    @SerializedName("mobileNumber")
    @Expose

    private String mobileNumber;

    public CommonUserInfo copyCommonUserInfo() {
        CommonUserInfo commonUserInfo = new CommonUserInfo();
        commonUserInfo.name = name;
        commonUserInfo.firstName = firstName;
        commonUserInfo.middleName = middleName;
        commonUserInfo.lastName = lastName;
        commonUserInfo.email = email;
        commonUserInfo.mobileNumber = mobileNumber;
        return commonUserInfo;
    }

    public void initializeCommonUserInfo() {
        firstName = null;
        middleName = null;
        lastName = null;
        email = null;
        mobileNumber = null;
        initialize();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
