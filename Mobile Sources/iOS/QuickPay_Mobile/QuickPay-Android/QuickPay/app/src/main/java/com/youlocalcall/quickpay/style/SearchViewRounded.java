package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.AttributeSet;

public class SearchViewRounded extends android.support.v7.widget.SearchView {

    private GradientDrawable gradientDrawable = new GradientDrawable();

    public SearchViewRounded(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        gradientDrawable.setCornerRadius(3.0f);
        gradientDrawable.setColor(BaseColors.YLC_WHITE);
        gradientDrawable.setStroke(3, BaseColors.YLC_GRAY3);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setBackground(gradientDrawable);
        } else {
            setBackgroundDrawable(gradientDrawable);
        }
    }
}
