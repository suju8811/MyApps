package com.youlocalcall.quickpay.model.account.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Michael on 6/27/17.
 */

public class CountryResponseResult {

    @SerializedName("country")
    @Expose

    private ArrayList<Country> countries = new ArrayList<>();

    public ArrayList<Country> getCountries() {
        return countries;
    }
}
