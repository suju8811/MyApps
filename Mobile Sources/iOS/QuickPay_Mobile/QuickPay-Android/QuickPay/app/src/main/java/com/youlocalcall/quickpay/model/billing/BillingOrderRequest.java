package com.youlocalcall.quickpay.model.billing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Michael on 6/20/17.
 */

public class BillingOrderRequest {

    @SerializedName("column")
    @Expose

    private Integer column = 0;

    @SerializedName("dir")
    @Expose

    private String dir = "desc";

    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }
}
