package com.youlocalcall.quickpay.executive.payment;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.Utility.StringUtility;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.account.Inmate.Inmate;
import com.youlocalcall.quickpay.model.account.Inmate.InmateByIdRequest;
import com.youlocalcall.quickpay.model.account.Inmate.InmateByIdResponse;
import com.youlocalcall.quickpay.model.account.Inmate.InmateRequest;
import com.youlocalcall.quickpay.model.account.Inmate.InmateResponse;
import com.youlocalcall.quickpay.model.account.address.CountryRequest;
import com.youlocalcall.quickpay.model.account.address.CountryResponse;
import com.youlocalcall.quickpay.model.payment.PaymentRequest;
import com.youlocalcall.quickpay.model.payment.confirm.PayPalConfirmRequest;
import com.youlocalcall.quickpay.model.payment.creditcard.CreditCardPaymentRequest;
import com.youlocalcall.quickpay.model.payment.profile.PaymentProfileRequest;
import com.youlocalcall.quickpay.model.payment.profile.PaymentProfileResponse;
import com.youlocalcall.quickpay.view.container.PayPalConfirmInterface;
import com.youlocalcall.quickpay.view.container.PayPalPaymentInterface;
import com.youlocalcall.quickpay.view.container.PaymentType;

import java.util.ArrayList;

public class PaymentHomeExecutive extends BillingDetailParentExecutive implements PayPalConfirmInterface {
    private PaymentHomeDisplay display;
    private PayPalPaymentInterface payPalPaymentInterface;

    public PaymentHomeExecutive(PaymentHomeDisplay display,
                                PayPalPaymentInterface payPalConfirmInterface) {
        super(display);
        this.display = display;
        this.payPalPaymentInterface = payPalConfirmInterface;
    }

    public void displayDidLoad() {
        display.showAddButton(false);
        display.showLoading(true);
        CountryRequest request = new CountryRequest();
        request.post(new APIUtility.APIResponse<CountryResponse>() {
            @Override
            public void onResponse(CountryResponse response) {
                if (response == null) {
                    display.showLoading(false);
                    return;
                }
                PaymentProfileRequest paymentProfileRequest = new PaymentProfileRequest();
                paymentProfileRequest.get(new APIUtility.APIResponse<PaymentProfileResponse>() {
                    @Override
                    public void onResponse(PaymentProfileResponse response) {
                        display.showLoading(false);
                        if (response == null) {
                            return;
                        }
                        display.loadDisplay();
                        childExecutive.loadDisplay();
                        if (AppData.user.getInmateId() != null) {
                            display.showLoading(true);
                            InmateByIdRequest inmateByIdRequest = new InmateByIdRequest(AppData.user.getInmateId());
                            inmateByIdRequest.post(new APIUtility.APIResponse<InmateByIdResponse>() {
                                @Override
                                public void onResponse(InmateByIdResponse response) {
                                    display.showLoading(false);
                                    if (response != null) {
                                        Inmate inmate = response.getResult().getInmate();
                                        display.enableSearchView(false, inmate);
                                    }
                                }
                            }, PaymentHomeExecutive.this);
                        }
                    }
                }, PaymentHomeExecutive.this);
            }
        }, this);
    }

    public void didPressSearchButton() {
        final InmateRequest request = display.getInmateRequest();
        int validateString = validateSearch(request);
        if (validateString != R.string.input_validate) {
            display.showError(validateString);
            return;
        }
        display.showLoading(true);
        request.get(new APIUtility.APIResponse<InmateResponse>() {
            @Override
            public void onResponse(InmateResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                if (response.getResult().getInmates().isEmpty()) {
                    display.showError(R.string.inmate_not_found);
                    display.showAddButton(true);
                    return;
                }
                display.showAddButton(false);
                display.refreshSearchResult(response.getResult().getInmates());
            }
        }, this);
    }

    public void didPressAddInmateButton() {
        final InmateRequest request = display.getInmateRequest();
        int validateString = validateAdd(request);
        if (validateString != R.string.input_validate) {
            display.showError(validateString);
            return;
        }
        display.showLoading(true);
        request.add(new APIUtility.APIResponse<InmateResponse>() {
            @Override
            public void onResponse(InmateResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.showAddButton(false);
                display.refreshSearchResult(response.getResult().getInmates());
            }
        }, this);
    }

    public void didPressSubmitButton() {
        PaymentRequest request = display.getPaymentRequest();
        if (request == null) {
            return;
        }
        if (request instanceof CreditCardPaymentRequest) {
            proceedPaymentByCreditCard((CreditCardPaymentRequest) request);
        } else {
            proceedPaymentByPayPal(request);
        }
    }

    private void proceedPaymentByCreditCard(CreditCardPaymentRequest request) {
        int validateString = validateCreditCardPayment(request);
        if (validateString != R.string.input_validate) {
            display.showError(validateString);
            return;
        }
        if (!request.isUpdate()) {
            request.setPaymentProfileID(AppData.paymentProfile.getPaymentProfileID());
        }
        display.showLoading(true);
        request.post(new APIUtility.APIResponse<CommonResponse>() {
            @Override
            public void onResponse(CommonResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.showError(StringUtility.html2String(response.getMessage()));
            }
        }, this);
    }

    private void proceedPaymentByPayPal(PaymentRequest request) {

        int validateString = validatePayPalPayment(request);
        if (validateString != R.string.input_validate) {
            display.showError(validateString);
            return;
        }
        payPalPaymentInterface.proceedPayPalPayment(PaymentType.QuickPay, request.getTotalAmount(), this);
    }

    private int validateSearch(InmateRequest request) {
        if (request.getFirstName().isEmpty() && request.getLastName().isEmpty() && request.getBopNumber().isEmpty()) {
            return R.string.invalid_search_criteria;
        }
        return R.string.input_validate;
    }

    private int validateAdd(InmateRequest request) {
        if (request.getFirstName().isEmpty()) {
            return R.string.empty_first_name;
        }
        if (request.getLastName().isEmpty()) {
            return R.string.empty_last_name;
        }
        if (request.getBopNumber().isEmpty()) {
            return R.string.empty_bop_number;
        }
        if (request.getBopNumber().length() != 8) {
            return R.string.invalid_bop_number;
        }
        return R.string.input_validate;
    }

    private int validatePayPalPayment(PaymentRequest request) {
        if (request.getInmateId() == null) {
            return R.string.no_selected_inmate;
        }
        if (request.getTotalAmount().isEmpty()) {
            return R.string.empty_total_amount;
        }
        return R.string.input_validate;
    }

    private int validateCreditCardPayment(CreditCardPaymentRequest request) {
        if (request.getInmateId() == null) {
            return R.string.no_selected_inmate;
        }
        if (request.getTotalAmount().isEmpty()) {
            return R.string.empty_total_amount;
        }
        if (!request.isUpdate()) {
            return R.string.input_validate;
        }
        String email = request.getEmail();
        if (email.isEmpty()) {
            return R.string.empty_email;
        }
        if (!StringUtility.isValidEmail(email)) {
            return R.string.invalid_email;
        }
        String phone = request.getPhoneNumber();
        if (phone.isEmpty()) {
            return R.string.empty_phone_number;
        }
        if (!StringUtility.isValidPhone(phone)) {
            return R.string.invalid_phone;
        }
        if (request.getCardHolder().isEmpty()) {
            return R.string.empty_card_holder_name;
        }
        if (request.getCardNumber().isEmpty()) {
            return R.string.empty_card_number;
        }
        if (request.getExpiration().isEmpty()) {
            return R.string.empty_expiration;
        }
        if (request.getCvc().isEmpty()) {
            return R.string.empty_cvc;
        }
        if (request.getCountry().isEmpty()) {
            return R.string.empty_country;
        }
        if (request.getBillingAdress().isEmpty()) {
            return R.string.empty_billing_address;
        }
        if (request.getCity().isEmpty()) {
            return R.string.empty_city;
        }
        if (request.getState().isEmpty()) {
            return R.string.empty_state;
        }
        if (request.getZipCode().isEmpty()) {
            return R.string.empty_zip_code;
        }
        return R.string.input_validate;
    }

    @Override
    public void proceedPayPalConfirm(String paymentId) {
        PayPalConfirmRequest request = display.getPayPalConfirmRequest(paymentId);
        display.showLoading(true);
        request.quickPayConfirm(new APIUtility.APIResponse<CommonResponse>() {
            @Override
            public void onResponse(CommonResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.showError(StringUtility.html2String(response.getMessage()));
            }
        }, this);
    }

    public interface PaymentHomeDisplay extends CommonExecutive.CommonDisplay {
        void loadDisplay();
        void refreshSearchResult(ArrayList<Inmate> inmates);
        InmateRequest getInmateRequest();
        PaymentRequest getPaymentRequest();

        void showAddButton(boolean isShow);
        void enableSearchView(boolean isEnable, Inmate inmate);

        PayPalConfirmRequest getPayPalConfirmRequest(String paymentId);
    }
}
