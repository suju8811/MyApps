package com.youlocalcall.quickpay.model.account.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.youlocalcall.quickpay.model.account.PayPalInfo;
import com.youlocalcall.quickpay.model.account.User;

/**
 * Created by Michael on 6/23/17.
 */

public class LoginResponseResult {

    @SerializedName("paypal")
    @Expose
    public PayPalInfo payPalInfo;
    @SerializedName("user")
    @Expose
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public PayPalInfo getPayPalInfo() {
        return payPalInfo;
    }

    public void setPayPalInfo(PayPalInfo payPalInfo) {
        this.payPalInfo = payPalInfo;
    }
}
