package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.util.AttributeSet;

public class TextViewRegular extends android.support.v7.widget.AppCompatTextView {
    public TextViewRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        BaseFontStyles.applyArialRegularFont(this);
    }
}
