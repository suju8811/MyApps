package com.youlocalcall.quickpay.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.common.Constants;
import com.youlocalcall.quickpay.common.customview.LoadingDialog;
import com.youlocalcall.quickpay.executive.CommonExecutive.CommonDisplay;

/**
 * Created by Michael on 6/21/17.
 */

public class BaseActivity extends AppCompatActivity implements Constants, CommonDisplay {

    LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showActivity(Class activity) {
        try {
            Intent intent = new Intent(this, activity);
            startActivity(intent);
            finish();
        } catch (Exception e) {
        }
    }

    public void showActivity(Class activity, int animation, boolean finish) {
        try {
            Intent intent = new Intent(this, activity);
            startActivity(intent);

            switch (animation) {
                case Constants.ANIMATION_RIGHT_TO_LEFT:
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    break;

                case Constants.ANIMATION_LEFT_TO_RIGHT:
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    break;

                case Constants.ANIMATION_BOTTOM_TO_UP:
                    overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                    break;

                case Constants.ANIMATION_UP_TO_BOTTOM:
                    overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                    break;
            }

            if (finish) {
                finish();
            }
        } catch (Exception e) {
        }
    }

    public void showActivityAndClearStack(Class activity, int animation) {
        Intent newIntent = new Intent(this, activity);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        switch (animation) {
            case Constants.ANIMATION_RIGHT_TO_LEFT:
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;

            case Constants.ANIMATION_LEFT_TO_RIGHT:
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;

            case Constants.ANIMATION_BOTTOM_TO_UP:
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                break;

            case Constants.ANIMATION_UP_TO_BOTTOM:
                overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                break;
        }

        startActivity(newIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (loadingDialog != null) {
            loadingDialog.show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (loadingDialog != null) {
            loadingDialog.dismiss();
        }
    }

    @Override
    public void showLoading(boolean isShow) {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
        if (isShow) {

            loadingDialog = new LoadingDialog(this, getResources().getString(R.string.loading));
            loadingDialog.show();
        }
    }

    @Override
    public void showError(String message) {
        try {
            Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    @Override
    public void showError(int message) {
        try {
            Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }
}
