package com.youlocalcall.quickpay.executive.payment;

import com.youlocalcall.quickpay.executive.account.PersonAddressDisplayContext;
import com.youlocalcall.quickpay.executive.account.PersonAddressParentExecutive;
import com.youlocalcall.quickpay.model.payment.profile.PaymentProfile;

/**
 * Created by Michael on 6/21/17.
 */

public class CreditCardInfoExecutive extends PersonAddressParentExecutive {

    private CreditCardInfoDisplay display;
    private CreditCardParentExecutive parentExecutive;
    private PaymentProfile context;
    private PersonAddressDisplayContext personAddressDisplayContext;

    public CreditCardInfoExecutive(CreditCardInfoDisplay display, CreditCardParentExecutive parentExecutive) {
        super(display);
        this.display = display;
        this.parentExecutive = parentExecutive;
        this.parentExecutive.childExecutive = this;
    }

    public void displayDidLoad() {
        parentExecutive.displayDidLoad();
    }

    public void loadDisplay(PaymentProfile paymentContext, final PersonAddressDisplayContext addressContext) {
        this.context = paymentContext;
        this.personAddressDisplayContext = addressContext;
        refreshDisplay(context, personAddressDisplayContext);
    }

    public void refreshDisplay(PaymentProfile context, PersonAddressDisplayContext personAddressDisplayContext) {
        display.loadDisplay(context);
        childExecutive.loadDisplay(personAddressDisplayContext);
    }

    public void didSelectPersonEmail(boolean isSelected) {
        display.usePersonEmail(isSelected);
    }

    public void didSelectPersonAddress(boolean isSelected) {
        display.usePersonAddress(isSelected);
        childExecutive.didSelectUsePersonAddress(isSelected);
    }

    public interface CreditCardInfoDisplay extends CommonDisplay {
        void loadDisplay(PaymentProfile context);

        void usePersonEmail(boolean isUse);

        void usePersonAddress(boolean isUse);

        PaymentProfile getCreditCardUpdateRequest();
    }
}
