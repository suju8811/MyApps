package com.youlocalcall.quickpay.model.account.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.model.payment.profile.PaymentPersonAddress;

import static com.youlocalcall.quickpay.model.payment.profile.PaymentPersonAddress.AddressType.OneAddress;

public class PersonAddress {

    private PaymentPersonAddress.AddressType addressType = OneAddress;
    @SerializedName("billingAddress")
    @Expose
    private String billingAddress;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("country")
    @Expose
    private Country country = new Country();
    @SerializedName("state")
    @Expose
    private State state = new State();
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("zipCode")
    @Expose
    private String zipCode;

    public PersonAddress() {
        initialize();
    }

    public PersonAddress(PaymentPersonAddress paymentPersonAddress) {
        billingAddress = paymentPersonAddress.getBillingAddress();
        address1 = paymentPersonAddress.getAddress1();
        address2 = paymentPersonAddress.getAddress2();
        country = new Country();
        country.setName(paymentPersonAddress.getCountry());
        for (Country iterator : AppData.countries) {
            if (iterator.getName().equalsIgnoreCase(country.getName())) {
                country.setId(iterator.getId());
                break;
            }
        }
        state = new State();
        state.setStateName(paymentPersonAddress.getState());
        city = paymentPersonAddress.getCity();
        zipCode = paymentPersonAddress.getZipCode();
    }

    public void initialize() {
        billingAddress = null;
        address1 = null;
        address2 = null;
        country = new Country();
        state = new State();
        city = null;
        zipCode = null;
    }

    public PersonAddress copy() {
        PersonAddress personAddress = new PersonAddress();
        personAddress.setAddressType(addressType);
        personAddress.setBillingAddress(billingAddress);
        personAddress.setAddress1(address1);
        personAddress.setAddress2(address2);
        if (country != null) {
            personAddress.setCountry(country.copy());
        } else {
            personAddress.setCountry(new Country());
        }
        if (state != null) {
            personAddress.setState(state.copyState());
        } else {
            personAddress.setState(new State());
        }
        personAddress.setCity(city);
        personAddress.setZipCode(zipCode);
        return personAddress;
    }

    public PaymentPersonAddress.AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(PaymentPersonAddress.AddressType addressType) {
        this.addressType = addressType;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    enum AddressType {
        OneAddress,
        TwoAddress
    }
}
