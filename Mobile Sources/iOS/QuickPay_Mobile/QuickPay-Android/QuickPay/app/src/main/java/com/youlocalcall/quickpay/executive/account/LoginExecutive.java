package com.youlocalcall.quickpay.executive.account;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.account.forgot.ForgotRequest;
import com.youlocalcall.quickpay.model.account.login.LoginRequest;
import com.youlocalcall.quickpay.model.account.login.LoginResponse;

/**
 * Created by Michael on 6/21/17.
 */

public class LoginExecutive extends CommonExecutive {
    LoginDisplay display;

    public LoginExecutive(LoginDisplay display) {
        super(display);
        this.display = display;
    }

    public void login() {
        LoginRequest request = display.getLoginRequest();
        if (request == null) {
            return;
        }
        int validateString = validateLoginRequest(request);
        if (validateString != R.string.input_validate) {
            display.showError(validateString);
            return;
        }

        display.showLoading(true);
        request.post(new APIUtility.APIResponse<LoginResponse>() {
            @Override
            public void onResponse(LoginResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.loggedIn();
            }
        }, this);
    }

    public void forgotPassword(String username) {
        ForgotRequest request = new ForgotRequest(username);
        display.showLoading(true);
        request.post(new APIUtility.APIResponse<CommonResponse>() {
            @Override
            public void onResponse(CommonResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.showError(response.getMessage());
            }
        }, this);
    }

    public int validateLoginRequest(LoginRequest request) {
        if (request.getUserName().isEmpty()) {
            return R.string.empty_user_name;
        }
        if (request.getPassword().isEmpty()) {
            return R.string.empty_password;
        }
        return R.string.input_validate;
    }

    public int validaeForgotPasswordRequest(String username) {
        return R.string.input_validate;
    }

    public interface LoginDisplay extends CommonExecutive.CommonDisplay {
        public void loggedIn();
        public LoginRequest getLoginRequest();
    }
}
