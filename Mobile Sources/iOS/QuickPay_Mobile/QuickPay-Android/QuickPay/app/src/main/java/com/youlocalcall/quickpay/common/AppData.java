package com.youlocalcall.quickpay.common;

import com.youlocalcall.quickpay.model.account.PayPalInfo;
import com.youlocalcall.quickpay.model.account.User;
import com.youlocalcall.quickpay.model.account.address.Country;
import com.youlocalcall.quickpay.model.payment.profile.PaymentProfile;

import java.util.ArrayList;

/**
 * Created by Michael on 6/23/17.
 */

public class AppData {
    public static User user = null;
    public static PayPalInfo payPalInfo = null;
    public static PaymentProfile paymentProfile = new PaymentProfile();
    public static ArrayList<Country> countries = new ArrayList<>();
//    public static PaypalConfiguration payPalConfiguration = PayPalConfiguration();

    public static void init() {
        user = null;
        payPalInfo = null;
        paymentProfile = new PaymentProfile();
        countries = new ArrayList<>();
//        payPalConfiguration = PayPalConfiguration();
    }
}
