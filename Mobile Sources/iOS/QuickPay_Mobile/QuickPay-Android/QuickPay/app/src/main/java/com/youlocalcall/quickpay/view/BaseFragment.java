package com.youlocalcall.quickpay.view;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.common.customview.LoadingDialog;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.executive.container.ContainerExecutive;

public abstract class BaseFragment extends Fragment implements CommonExecutive.CommonDisplay {
    public ContainerExecutive containerExecutive;
    LoadingDialog loadingDialog;

    public void placeContent(int containerId, BaseFragment fragment) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerId, fragment)
                .commit();
    }

    @Override
    public void showLoading(boolean isShow) {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
        if (isShow) {

            loadingDialog = new LoadingDialog(getActivity(), getActivity().getResources().getString(R.string.loading));
            loadingDialog.show();
        }
    }

    @Override
    public void showError(String message) {
        try {
            Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    @Override
    public void showError(int message) {
        try {
            Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }
}
