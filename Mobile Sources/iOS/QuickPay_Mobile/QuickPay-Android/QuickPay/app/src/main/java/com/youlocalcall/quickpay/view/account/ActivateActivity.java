package com.youlocalcall.quickpay.view.account;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.executive.account.ActivateExecutive;
import com.youlocalcall.quickpay.model.account.activate.ActivateRequest;
import com.youlocalcall.quickpay.model.account.activate.ResendActivationRequest;
import com.youlocalcall.quickpay.style.EditTextFieldCommon;
import com.youlocalcall.quickpay.view.BaseActivity;

public class ActivateActivity extends BaseActivity implements View.OnClickListener, ActivateExecutive.ActivateDisplay {

    EditTextFieldCommon userNameEditText;
    EditTextFieldCommon passwordEditText;
    EditTextFieldCommon activationCodeEditText;
    private ActivateExecutive executive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activate);
        linkControls();
        executive = new ActivateExecutive(this);
    }

    void linkControls() {
        findViewById(R.id.backButton).setOnClickListener(this);
        findViewById(R.id.activateAccountButton).setOnClickListener(this);
        findViewById(R.id.resendCodeButton).setOnClickListener(this);
        userNameEditText = (EditTextFieldCommon) findViewById(R.id.userNameEditText);
        passwordEditText = (EditTextFieldCommon) findViewById(R.id.passwordEditText);
        activationCodeEditText = (EditTextFieldCommon) findViewById(R.id.activationCodeEditText);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                finish();
                break;
            case R.id.activateAccountButton:
                executive.activate();
                break;
            case R.id.resendCodeButton:
                executive.resendActivation();
                break;
        }
    }

    @Override
    public void activated() {
        showActivityAndClearStack(LoginActivity.class, ANIMATION_LEFT_TO_RIGHT);
    }

    @Override
    public ActivateRequest getActivateRequest() {
        return new ActivateRequest(userNameEditText.getText().toString(),
                passwordEditText.getText().toString(),
                activationCodeEditText.getText().toString());
    }

    @Override
    public void displayVerificationMethodSelectMessage(final ResendActivationRequest request) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setMessage(R.string.which_account_verification_method)
                .setPositiveButton(R.string.verification_phone, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        executive.sendResendActivationRequest(request, true);
                    }
                })
                .setNegativeButton(R.string.verification_email, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        executive.sendResendActivationRequest(request, false);
                    }
                })
                .show();
    }
}
