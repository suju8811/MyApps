package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by scottgregg on 28/02/2017.
 */

public class TextViewItalicPink extends TextViewItalic {
    public TextViewItalicPink(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTextColor(BaseColors.YLC_PINK);
    }
}
