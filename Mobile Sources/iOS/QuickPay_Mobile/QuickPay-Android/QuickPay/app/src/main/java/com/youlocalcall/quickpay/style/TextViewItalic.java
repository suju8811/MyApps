package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by scottgregg on 28/02/2017.
 */

public class TextViewItalic extends android.support.v7.widget.AppCompatTextView {
    public TextViewItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        BaseFontStyles.applyArialItalicFont(this);
    }
}
