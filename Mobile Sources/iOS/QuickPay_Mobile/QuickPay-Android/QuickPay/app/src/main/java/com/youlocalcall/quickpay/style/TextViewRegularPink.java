package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by scottgregg on 28/02/2017.
 */

public class TextViewRegularPink extends TextViewRegular {
    public TextViewRegularPink(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        setTextColor(BaseColors.YLC_PINK);
    }
}
