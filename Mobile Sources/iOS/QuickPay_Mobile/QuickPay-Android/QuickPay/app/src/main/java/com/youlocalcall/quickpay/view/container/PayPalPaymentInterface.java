package com.youlocalcall.quickpay.view.container;

public interface PayPalPaymentInterface {
    void proceedPayPalPayment(PaymentType paymentType, String amount, PayPalConfirmInterface confirmInterface);
}
