package com.youlocalcall.quickpay.view.account;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.StringUtility;
import com.youlocalcall.quickpay.executive.account.AccountDisplayContext;
import com.youlocalcall.quickpay.executive.account.AccountExecutive;
import com.youlocalcall.quickpay.executive.account.AccountExecutive.AccountDisplay;
import com.youlocalcall.quickpay.model.account.ChangePasswordRequest;
import com.youlocalcall.quickpay.model.account.CommonUserInfo;
import com.youlocalcall.quickpay.model.account.EditProfileRequest;
import com.youlocalcall.quickpay.model.account.address.PersonAddress;
import com.youlocalcall.quickpay.style.EditTextFieldCommon;
import com.youlocalcall.quickpay.view.BaseFragment;

import br.com.sapereaude.maskedEditText.MaskedEditText;

public class AccountFragment extends BaseFragment implements View.OnClickListener, AccountDisplay {

    //
    // Delegate variables
    //

    private PersonAddressFragment personAddressFragment;
    private AccountExecutive executive;

    //
    // Bind controls
    //

    private EditTextFieldCommon userNameEditText;
    private EditTextFieldCommon firstNameEditText;
    private EditTextFieldCommon middleNameEditText;
    private EditTextFieldCommon lastNameEditText;
    private EditTextFieldCommon emailEditText;
    private MaskedEditText phoneNumberEditText;
    private EditTextFieldCommon oldPasswordEditText;
    private EditTextFieldCommon newPasswordEditText;
    private EditTextFieldCommon confirmPasswordEditText;

    public AccountFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        personAddressFragment = new PersonAddressFragment();
        placeContent(R.id.person_address_container, personAddressFragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        //
        // Set Content
        //

        userNameEditText = (EditTextFieldCommon) view.findViewById(R.id.userNameEditText);
        firstNameEditText = (EditTextFieldCommon) view.findViewById(R.id.firstNameEditText);
        middleNameEditText = (EditTextFieldCommon) view.findViewById(R.id.middleNameEditText);
        lastNameEditText = (EditTextFieldCommon) view.findViewById(R.id.lastNameEditText);
        emailEditText = (EditTextFieldCommon) view.findViewById(R.id.emailEditText);
        phoneNumberEditText = (MaskedEditText) view.findViewById(R.id.phoneNumberEditText);
        oldPasswordEditText = (EditTextFieldCommon) view.findViewById(R.id.oldPasswordEditText);
        newPasswordEditText = (EditTextFieldCommon) view.findViewById(R.id.newPasswordEditText);
        confirmPasswordEditText = (EditTextFieldCommon) view.findViewById(R.id.confirmPasswordEditText);

        //
        // Set Event Handler
        //

        view.findViewById(R.id.imageViewSideMenu).setOnClickListener(this);
        view.findViewById(R.id.addAmountButton).setOnClickListener(this);
        view.findViewById(R.id.accountResetButton).setOnClickListener(this);
        view.findViewById(R.id.accountUpdateButton).setOnClickListener(this);
        view.findViewById(R.id.passwordResetButton).setOnClickListener(this);
        view.findViewById(R.id.passwordUpdateButton).setOnClickListener(this);

        //
        // Set Executive
        //

        executive = new AccountExecutive(this);
        personAddressFragment.setParentExecutive(executive);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewSideMenu:
                containerExecutive.didPressSideMenuButton();
                break;
            case R.id.addAmountButton:
                executive.didPressAddPrepayAmountButton();
                break;
            case R.id.accountResetButton:
                executive.didPressResetButton();
                break;
            case R.id.accountUpdateButton:
                executive.didPressUpdateProfileButton();
                break;
            case R.id.passwordResetButton:
                executive.didPressResetPasswordButton();
                break;
            case R.id.passwordUpdateButton:
                executive.didPressUpdatePasswordButton();
                break;
        }
    }

    @Override
    public void loadDisplay(AccountDisplayContext accountDisplayContext) {
        userNameEditText.setText(accountDisplayContext.getCommonUserInfo().getName());
        firstNameEditText.setText(accountDisplayContext.getCommonUserInfo().getFirstName());
        middleNameEditText.setText(accountDisplayContext.getCommonUserInfo().getMiddleName());
        lastNameEditText.setText(accountDisplayContext.getCommonUserInfo().getLastName());
        emailEditText.setText(accountDisplayContext.getCommonUserInfo().getEmail());
        phoneNumberEditText.setText(accountDisplayContext.getCommonUserInfo().getMobileNumber());
    }

    @Override
    public void resetPassword() {
        oldPasswordEditText.setText("");
        newPasswordEditText.setText("");
        confirmPasswordEditText.setText("");
    }

    @Override
    public EditProfileRequest getEditProfileRequest() {
        CommonUserInfo commonUserInfo = new CommonUserInfo();
        commonUserInfo.setName(userNameEditText.getText().toString());
        commonUserInfo.setFirstName(firstNameEditText.getText().toString());
        commonUserInfo.setMiddleName(middleNameEditText.getText().toString());
        commonUserInfo.setLastName(lastNameEditText.getText().toString());
        commonUserInfo.setEmail(emailEditText.getText().toString());
        commonUserInfo.setMobileNumber(StringUtility.removeNonNumber(phoneNumberEditText.getText().toString()));
        PersonAddress personAddressInfo = personAddressFragment.getPersonAddressInfo();
        return new EditProfileRequest(commonUserInfo,
                personAddressInfo);
    }

    @Override
    public ChangePasswordRequest getChangePasswordRequest() {
        return new ChangePasswordRequest(oldPasswordEditText.getText().toString(),
                newPasswordEditText.getText().toString(),
                confirmPasswordEditText.getText().toString());
    }

    @Override
    public void displayQuickPayRecommendAlert() {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.app_name)
                .setMessage(R.string.quick_pay_recommend)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        containerExecutive.didPressAddAmountButton();
                    }
                })
                .show();
    }
}
