package com.youlocalcall.quickpay.model.payment;

/**
 * Created by Michael on 6/29/17.
 */

public enum PaymentMethod {
    CreditCardBefore,
    CreditCard,
    PayPal
}
