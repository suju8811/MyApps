package com.youlocalcall.quickpay.view.container;

public enum PaymentType {
    QuickPay,
    PrePay,
}
