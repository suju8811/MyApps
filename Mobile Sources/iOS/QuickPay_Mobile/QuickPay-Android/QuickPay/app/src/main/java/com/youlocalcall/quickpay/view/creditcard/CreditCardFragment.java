package com.youlocalcall.quickpay.view.creditcard;

import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.executive.account.PersonAddressDisplayContext;
import com.youlocalcall.quickpay.executive.payment.CreditCardExecutive;
import com.youlocalcall.quickpay.executive.payment.CreditCardExecutive.CreditCardDisplay;
import com.youlocalcall.quickpay.model.payment.profile.PaymentProfile;
import com.youlocalcall.quickpay.view.BaseFragment;

public class CreditCardFragment extends BaseFragment implements View.OnClickListener, CreditCardDisplay {

    //
    // Delegate variables
    //

    CreditCardExecutive executive;
    private CreditCardInfoFragment creditCardInfoFragment;

    //
    // Controls
    //

    private SwitchCompat autoPaymentSwitch;

    public CreditCardFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        creditCardInfoFragment = new CreditCardInfoFragment();
        placeContent(R.id.credit_card_info_container, creditCardInfoFragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_credit_card, container, false);

        //
        // Set Content
        //

        autoPaymentSwitch = (SwitchCompat) view.findViewById(R.id.autoPaymentSwitch);

        //
        // Set Event Handler
        //

        view.findViewById(R.id.resetButton).setOnClickListener(this);
        view.findViewById(R.id.updateButton).setOnClickListener(this);
        view.findViewById(R.id.imageViewSideMenu).setOnClickListener(this);

        //
        // Set executive
        //

        executive = new CreditCardExecutive(this);
        creditCardInfoFragment.setParentExecutive(executive);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewSideMenu:
                containerExecutive.didPressSideMenuButton();
                break;
            case R.id.resetButton:
                executive.didPressResetButton();
                break;
            case R.id.updateButton:
                executive.didPressUpdateButton();
                break;
        }
    }

    @Override
    public void loadDisplay(PaymentProfile context, PersonAddressDisplayContext personAddressDisplayContext) {
        autoPaymentSwitch.setSelected(context.getAutoPayment() == "1");
    }

    @Override
    public PaymentProfile getCreditCardUpdateRequest() {
        PaymentProfile request = creditCardInfoFragment.getCreditCardUpdateRequest();
        if (autoPaymentSwitch.isSelected()) {
            request.setAutoPayment("0");
        } else {
            request.setAutoPayment("1");
        }
        return request;
    }
}
