package com.youlocalcall.quickpay.model.payment.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.youlocalcall.quickpay.model.account.BaseRequest;

import static com.youlocalcall.quickpay.model.payment.profile.PaymentPersonAddress.AddressType.OneAddress;

/**
 * Created by Michael on 6/27/17.
 */

public class PaymentPersonAddress extends BaseRequest {

    AddressType addressType = OneAddress;
    @SerializedName("billingAddress")
    @Expose

    String billingAddress;
    @SerializedName("address1")
    @Expose

    String address1;
    @SerializedName("address2")
    @Expose

    String address2;
    @SerializedName("country")
    @Expose

    String country;
    @SerializedName("state")
    @Expose

    String state;
    @SerializedName("city")
    @Expose

    String city;
    @SerializedName("zipCode")
    @Expose

    String zipCode;

    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public enum AddressType {
        OneAddress,
        TwoAddress
    }
}
