package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.util.AttributeSet;

import com.youlocalcall.quickpay.R;

/**
 * Created by Michael on 6/20/17.
 */

public class ButtonTabBilling extends ButtonRegular {
    public ButtonTabBilling(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        int drawable = isSelected() ? R.drawable.billing_tab_selected : R.drawable.billing_tab_normal;
        int textColor = isSelected() ? BaseColors.YLC_WHITE : BaseColors.YLC_BLACK;
        setBackgroundResource(drawable);
        setTextColor(textColor);
    }
}
