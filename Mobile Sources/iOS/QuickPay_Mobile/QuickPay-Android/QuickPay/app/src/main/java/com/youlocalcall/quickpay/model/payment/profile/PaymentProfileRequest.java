package com.youlocalcall.quickpay.model.payment.profile;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.account.BaseRequest;
import com.youlocalcall.quickpay.model.account.address.PersonAddress;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Michael on 6/27/17.
 */

public class PaymentProfileRequest extends BaseRequest {

    public void get(final APIUtility.APIResponse<PaymentProfileResponse> apiResponse, final CommonExecutive executive) {
        paymentService.loadPaymentProfile().enqueue(new Callback<PaymentProfileResponse>() {
            @Override
            public void onResponse(Call<PaymentProfileResponse> call, Response<PaymentProfileResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                Integer userId = AppData.user.getId();
                Integer inmateId = AppData.user.getInmateId();
                AppData.user = response.body().getResult().getUserProfile();
                AppData.user.setId(userId);
                AppData.user.setInmateId(inmateId);
                if (AppData.user.getAllowCustomer() == 1) {
                    AppData.user.setInmateId(null);
                }
                AppData.paymentProfile = response.body().getResult().getPaymentProfile();
                AppData.paymentProfile.setPersonAddress(new PersonAddress(AppData.paymentProfile));
                if (AppData.paymentProfile.usePersonAddress != 0) {
                    AppData.paymentProfile.setPersonAddress(AppData.user);
                }
                if (AppData.paymentProfile.usePersonEmail != 0) {
                    AppData.paymentProfile.setEmail(AppData.user.getEmail());
                }
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<PaymentProfileResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }

//    public void update(final APIUtility.APIResponse<CommonResponse> apiResponse, final CommonExecutive executive) {
//        paymentService.updatePaymentProfile().enqueue(new Callback<PaymentProfileResponse>() {
//            @Override
//            public void onResponse(Call<PaymentProfileResponse> call, Response<PaymentProfileResponse> response) {
//                if (!executive.validateResponse(response)) {
//                    apiResponse.onResponse(null);
//                    return;
//                }
//                AppData.user = response.body().getResult().getUserProfile();
//                AppData.paymentProfile = response.body().getResult().getPaymentProfile();
//                AppData.paymentProfile.setPersonAddress(new PersonAddress(AppData.paymentProfile));
//                if (AppData.paymentProfile.usePersonAddress != 0) {
//                    AppData.paymentProfile.setPersonAddress(AppData.user);
//                }
//                if (AppData.paymentProfile.usePersonEmail != 0) {
//                    AppData.paymentProfile.setEmail(AppData.user.getEmail());
//                }
//                apiResponse.onResponse(response.body());
//            }
//
//            @Override
//            public void onFailure(Call<PaymentProfileResponse> call, Throwable t) {
//                executive.display.showError(R.string.request_failed);
//                apiResponse.onResponse(null);
//            }
//        });
//    }
}
