package com.youlocalcall.quickpay.model.account.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.youlocalcall.quickpay.model.CommonResponse;

/**
 * Created by Michael on 6/27/17.
 */

public class CountryResponse extends CommonResponse {

    @SerializedName("result")
    @Expose

    private CountryResponseResult result = new CountryResponseResult();

    public CountryResponseResult getResult() {
        return result;
    }
}
