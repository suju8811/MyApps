package com.youlocalcall.quickpay.model.account.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Michael on 6/20/17.
 */

public class Country {

    @SerializedName("countryID")
    @Expose

    private Integer id;

    @SerializedName("countryName")
    @Expose

    private String name;

    @SerializedName("countryAbbr")
    @Expose

    private String abbreviation;

    @SerializedName("countryCode")
    @Expose

    private Integer code;

    public Country copy() {
        Country country = new Country();
        country.setId(id);
        country.setName(name);
        country.setAbbreviation(abbreviation);
        country.setCode(code);
        return country;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
