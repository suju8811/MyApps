package com.youlocalcall.quickpay.view.account;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.common.Constants;
import com.youlocalcall.quickpay.executive.account.LoginExecutive;
import com.youlocalcall.quickpay.model.account.login.LoginRequest;
import com.youlocalcall.quickpay.style.EditTextFieldCommon;
import com.youlocalcall.quickpay.view.BaseActivity;
import com.youlocalcall.quickpay.view.container.ContainerActivity;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseActivity implements Constants, View.OnClickListener, LoginExecutive.LoginDisplay {

    private EditTextFieldCommon userNameEditText;
    private EditTextFieldCommon passwordEditText;
    private LoginExecutive executive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        linkControls();
        executive = new LoginExecutive(this);
    }

    void linkControls() {
        findViewById(R.id.forgotButton).setOnClickListener(this);
        findViewById(R.id.signInButton).setOnClickListener(this);
        findViewById(R.id.signUpButton).setOnClickListener(this);
        findViewById(R.id.activateButton).setOnClickListener(this);
        userNameEditText = (EditTextFieldCommon) findViewById(R.id.userNameEditText);
        passwordEditText = (EditTextFieldCommon) findViewById(R.id.passwordEditText);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.forgotButton:
                final EditTextFieldCommon textFieldCommon = new EditTextFieldCommon(this, null);
                FrameLayout container = new FrameLayout(this);
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.leftMargin = getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin_small);
                params.rightMargin = getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin_small);
                textFieldCommon.setLayoutParams(params);
                container.addView(textFieldCommon);
                new AlertDialog.Builder(this)
                        .setTitle(R.string.app_name)
                        .setMessage(R.string.input_user_name)
                        .setView(container)
                        .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        })
                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                executive.forgotPassword(textFieldCommon.getText().toString());
                            }
                        })
                        .show();
                break;
            case R.id.signInButton:
                executive.login();
                break;
            case R.id.signUpButton:
                showActivity(SignUpActivity.class, Constants.ANIMATION_RIGHT_TO_LEFT, false);
                break;
            case R.id.activateButton:
                showActivity(ActivateActivity.class, Constants.ANIMATION_RIGHT_TO_LEFT, false);
                break;
            default:
                break;
        }
    }

    @Override
    public void loggedIn() {
        showActivity(ContainerActivity.class, Constants.ANIMATION_RIGHT_TO_LEFT, true);
    }

    @Override
    public LoginRequest getLoginRequest() {
        LoginRequest loginRequest = new LoginRequest(
                userNameEditText.getText().toString(),
                passwordEditText.getText().toString());
        return loginRequest;
    }
}

