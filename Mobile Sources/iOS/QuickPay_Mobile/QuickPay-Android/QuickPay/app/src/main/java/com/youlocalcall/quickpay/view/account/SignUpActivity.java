package com.youlocalcall.quickpay.view.account;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.common.Constants;
import com.youlocalcall.quickpay.executive.account.SignUpExecutive;
import com.youlocalcall.quickpay.model.account.signup.SignUpRequest;
import com.youlocalcall.quickpay.style.CheckBoxRegularPink;
import com.youlocalcall.quickpay.style.EditTextFieldCommon;
import com.youlocalcall.quickpay.view.BaseActivity;

import br.com.sapereaude.maskedEditText.MaskedEditText;

public class SignUpActivity extends BaseActivity implements View.OnClickListener, SignUpExecutive.SignUpDisplay {

    EditTextFieldCommon userNameEditText;
    EditTextFieldCommon passwordEditText;
    EditTextFieldCommon confirmPasswordEditText;
    EditTextFieldCommon emailEditText;
    MaskedEditText phoneNumberTextField;
    CheckBoxRegularPink acceptTermsCheckBox;
    private SignUpExecutive executive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        linkControls();
        executive = new SignUpExecutive(this);
    }

    void linkControls() {
        findViewById(R.id.backButton).setOnClickListener(this);
        findViewById(R.id.createAccountButton).setOnClickListener(this);
        userNameEditText = (EditTextFieldCommon) findViewById(R.id.userNameEditText);
        passwordEditText = (EditTextFieldCommon) findViewById(R.id.passwordEditText);
        confirmPasswordEditText = (EditTextFieldCommon) findViewById(R.id.passwordConfirmEditText);
        emailEditText = (EditTextFieldCommon) findViewById(R.id.emailEditText);
        phoneNumberTextField = (MaskedEditText) findViewById(R.id.phoneNumberEditText);
        acceptTermsCheckBox = (CheckBoxRegularPink) findViewById(R.id.acceptTermsCheckBox);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                finish();
                break;
            case R.id.createAccountButton:
                executive.signUp();
                break;
            default:
                break;
        }
    }

    @Override
    public void signedUp(String message) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        showActivity(ActivateActivity.class, Constants.ANIMATION_RIGHT_TO_LEFT, false);
                    }
                })
                .show();
    }

    @Override
    public SignUpRequest getSignUpRequest() {
        return new SignUpRequest(userNameEditText.getText().toString(),
                passwordEditText.getText().toString(),
                confirmPasswordEditText.getText().toString(),
                emailEditText.getText().toString(),
                phoneNumberTextField.getRawText().toString(),
                acceptTermsCheckBox.isChecked());
    }

    @Override
    public void displayVerificationMethodSelectMessage(final SignUpRequest request) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setMessage(R.string.which_account_verification_method)
                .setPositiveButton(R.string.verification_phone, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        executive.sendSignUpRequest(request, true);
                    }
                })
                .setNegativeButton(R.string.verification_email, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        executive.sendSignUpRequest(request, false);
                    }
                })
                .show();
    }
}
