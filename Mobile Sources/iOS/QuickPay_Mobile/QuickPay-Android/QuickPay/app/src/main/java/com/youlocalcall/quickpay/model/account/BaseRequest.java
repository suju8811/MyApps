package com.youlocalcall.quickpay.model.account;

import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.model.billing.BillingService;
import com.youlocalcall.quickpay.model.payment.PaymentService;

/**
 * Created by Michael on 6/23/17.
 */

public class BaseRequest {

    protected AccountService accountService = APIUtility.getAccountService();
    protected BillingService billingService = APIUtility.getBillingService();
    protected PaymentService paymentService = APIUtility.getPaymentService();
    protected Integer isMobile = 1;
}
