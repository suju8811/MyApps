package com.youlocalcall.quickpay.model.account;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.account.address.PersonAddress;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Michael on 6/20/17.
 */

public class EditProfileRequest extends BaseRequest {

    private CommonUserInfo commonInfo;
    private PersonAddress addressInfo;

    public EditProfileRequest(CommonUserInfo commonInfo,
                              PersonAddress addressInfo) {
        this.commonInfo = commonInfo;
        this.addressInfo = addressInfo;
    }

    public CommonUserInfo getCommonInfo() {
        return commonInfo;
    }

    public PersonAddress getAddressInfo() {
        return addressInfo;
    }

    public void post(final APIUtility.APIResponse<CommonResponse> apiResponse, final CommonExecutive executive) {
        Integer stateId = addressInfo.getState().getStateId();
        accountService.updateProfile(commonInfo.getFirstName(),
                commonInfo.getMiddleName(),
                commonInfo.getLastName(),
                commonInfo.getEmail(),
                commonInfo.getMobileNumber(),
                String.valueOf(addressInfo.getCountry().getId()),
                addressInfo.getAddress1(),
                addressInfo.getAddress2(),
                addressInfo.getCity(),
                stateId == null ? "" : String.valueOf(stateId),
                addressInfo.getZipCode(),
                isMobile).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }
}
