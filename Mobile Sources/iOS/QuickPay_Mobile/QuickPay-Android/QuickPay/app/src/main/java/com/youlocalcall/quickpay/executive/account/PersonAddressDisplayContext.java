package com.youlocalcall.quickpay.executive.account;

import com.youlocalcall.quickpay.model.account.address.PersonAddress;

/**
 * Created by Michael on 6/28/17.
 */

public class PersonAddressDisplayContext {

    CountryStateDisplayContext countryState = new CountryStateDisplayContext();
    PersonAddress personAddress = new PersonAddress();

    /**
     * Initialize values
     */

    void initialize() {
        countryState.initialize();
        personAddress.initialize();
    }

    public CountryStateDisplayContext getCountryState() {
        return countryState;
    }

    public void setCountryState(CountryStateDisplayContext countryState) {
        this.countryState = countryState;
    }

    public PersonAddress getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(PersonAddress personAddress) {
        this.personAddress = personAddress;
    }
}
