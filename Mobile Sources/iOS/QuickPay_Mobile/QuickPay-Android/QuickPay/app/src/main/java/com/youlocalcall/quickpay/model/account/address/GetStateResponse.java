package com.youlocalcall.quickpay.model.account.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.youlocalcall.quickpay.model.CommonResponse;

/**
 * Created by Michael on 6/27/17.
 */

public class GetStateResponse extends CommonResponse {

    @SerializedName("result")
    @Expose

    private GetStateResponseResult result = new GetStateResponseResult();

    public GetStateResponseResult getResult() {
        return result;
    }

    public void setResult(GetStateResponseResult result) {
        this.result = result;
    }
}
