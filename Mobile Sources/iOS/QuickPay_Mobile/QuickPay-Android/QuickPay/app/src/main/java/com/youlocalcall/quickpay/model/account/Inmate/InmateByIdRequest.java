package com.youlocalcall.quickpay.model.account.Inmate;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.account.BaseRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Michael on 6/27/17.
 */

public class InmateByIdRequest extends BaseRequest {
    private Integer id;
    private String from;

    public InmateByIdRequest() {
    }

    public InmateByIdRequest(Integer id) {
        this.id = id;
        this.from = "frontend";
    }

    public void post(final APIUtility.APIResponse<InmateByIdResponse> apiResponse, final CommonExecutive executive) {
        accountService.getInmateById(id, from).enqueue(new Callback<InmateByIdResponse>() {
            @Override
            public void onResponse(Call<InmateByIdResponse> call, Response<InmateByIdResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<InmateByIdResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }
}
