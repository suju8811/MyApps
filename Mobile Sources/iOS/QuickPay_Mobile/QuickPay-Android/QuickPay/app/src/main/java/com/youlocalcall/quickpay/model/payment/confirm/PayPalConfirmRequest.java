package com.youlocalcall.quickpay.model.payment.confirm;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.account.BaseRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayPalConfirmRequest extends BaseRequest {
    private String inmateId = "";
    private String paymentId = "";
    private String userId = "";
    private String totalPrice = "";

    public PayPalConfirmRequest(String paymentId,
                                String userId,
                                String totalPrice) {
        this.paymentId = paymentId;
        this.userId = userId;
        this.totalPrice = totalPrice;
    }

    public PayPalConfirmRequest(String inmateId,
                                String paymentId,
                                String userId,
                                String totalPrice) {
        this.inmateId = inmateId;
        this.paymentId = paymentId;
        this.userId = userId;
        this.totalPrice = totalPrice;
    }

    public void quickPayConfirm(final APIUtility.APIResponse<CommonResponse> apiResponse, final CommonExecutive executive) {
        paymentService.proceedQuickPayConfirm(userId,
                inmateId,
                totalPrice,
                paymentId).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }

    public void prePayConfirm(final APIUtility.APIResponse<CommonResponse> apiResponse, final CommonExecutive executive) {
        paymentService.proceedPrepayPayConfirm(userId,
                totalPrice,
                paymentId).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }
}
