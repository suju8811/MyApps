package com.youlocalcall.quickpay.executive.payment;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.Utility.StringUtility;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.executive.account.PersonAddressDisplayContext;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.payment.profile.PaymentPersonAddress.AddressType;
import com.youlocalcall.quickpay.model.payment.profile.PaymentProfile;

/**
 * Created by Michael on 6/21/17.
 */

public class CreditCardExecutive extends CreditCardParentExecutive {
    private CreditCardDisplay display;

    public CreditCardExecutive(CreditCardDisplay display) {
        super(display);
        this.display = display;
    }

    public void displayDidLoad() {

        //
        // Load main information
        //

        AppData.paymentProfile.getPersonAddress().setAddressType(AddressType.OneAddress);
        final PaymentProfile context = AppData.paymentProfile;

        //
        // Load person address context
        //

        PersonAddressDisplayContext personAddressDisplayContext = new PersonAddressDisplayContext();
        personAddressDisplayContext.setPersonAddress(context.getPersonAddress().copy());
        display.loadDisplay(context, personAddressDisplayContext);
        childExecutive.loadDisplay(context, personAddressDisplayContext);
    }

    public void didPressResetButton() {
        PaymentProfile newProfile = new PaymentProfile();
        PersonAddressDisplayContext newAddressContext = new PersonAddressDisplayContext();
        display.loadDisplay(newProfile, newAddressContext);
        childExecutive.refreshDisplay(newProfile, newAddressContext);
    }

    public void didPressUpdateButton() {
        final PaymentProfile request = display.getCreditCardUpdateRequest();
        if (request == null) {
            return;
        }

        int validateString = validate(request);
        if (validateString != R.string.input_validate) {
            display.showError(validateString);
            return;
        }
        display.showLoading(true);
        request.post(new APIUtility.APIResponse<CommonResponse>() {
            @Override
            public void onResponse(CommonResponse response) {
                display.showLoading(false);
                String paymentProfileID = AppData.paymentProfile.getPaymentProfileID();
                AppData.paymentProfile = request;
                AppData.paymentProfile.setPaymentProfileID(paymentProfileID);
                if (response.getMessage() != null) {
                    display.showError(response.getMessage());
                }
            }
        }, this);
    }

    public int validate(PaymentProfile request) {
        if (request.getCardHolderName() == null ||
                request.getCardHolderName().isEmpty()) {
            return R.string.empty_card_holder_name;
        }
        if (request.getCardNumber() == null ||
                request.getCardNumber().isEmpty()) {
            return R.string.empty_card_number;
        }
        if (request.getExpiration() == null ||
                request.getExpiration().isEmpty()) {
            return R.string.empty_expiration;
        }
        if (request.getCvc() == null ||
                request.getCvc().isEmpty()) {
            return R.string.empty_cvc;
        }
        if (request.getPhoneNumber() == null ||
                request.getPhoneNumber().isEmpty()) {
            return R.string.empty_phone_number;
        }
        if (!StringUtility.isValidPhone(request.getPhoneNumber())) {
            return R.string.invalid_phone;
        }
        if (request.getEmail() == null ||
                request.getEmail().isEmpty()) {
            return R.string.empty_email;
        }
        if (!StringUtility.isValidEmail(request.getEmail())) {
            return R.string.invalid_email;
        }
        if (request.getPersonAddress().getCountry().getName() == null ||
                request.getPersonAddress().getCountry().getName().isEmpty()) {
            return R.string.empty_country;
        }
        if (request.getPersonAddress().getBillingAddress() == null ||
                request.getPersonAddress().getBillingAddress().isEmpty()) {
            return R.string.empty_billing_address;
        }
        if (request.getPersonAddress().getCity() == null ||
                request.getPersonAddress().getCity().isEmpty()) {
            return R.string.empty_city;
        }
        if (request.getPersonAddress().getZipCode() == null ||
                request.getPersonAddress().getZipCode().isEmpty()) {
            return R.string.empty_zip_code;
        }
        return R.string.input_validate;
    }

    public interface CreditCardDisplay extends CommonDisplay {
        void loadDisplay(PaymentProfile context, PersonAddressDisplayContext personAddressDisplayContext);

        PaymentProfile getCreditCardUpdateRequest();
    }
}
