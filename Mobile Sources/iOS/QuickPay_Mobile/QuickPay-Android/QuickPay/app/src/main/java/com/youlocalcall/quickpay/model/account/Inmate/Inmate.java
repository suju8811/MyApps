package com.youlocalcall.quickpay.model.account.Inmate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Michael on 6/20/17.
 */

public class Inmate {

    @SerializedName("inmateID")
    @Expose
    Integer id;

    @SerializedName("inmateCode")
    @Expose
    String code;

    @SerializedName("locationID")
    @Expose
    Integer locationID;

    @SerializedName("bopNumber")
    @Expose
    String BOPNumber;

    @SerializedName("firstName")
    @Expose
    String firstName;

    @SerializedName("lastName")
    @Expose
    String lastName;

    boolean selected;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getLocationID() {
        return locationID;
    }

    public void setLocationID(Integer locationID) {
        this.locationID = locationID;
    }

    public String getBOPNumber() {
        return BOPNumber;
    }

    public void setBOPNumber(String BOPNumber) {
        this.BOPNumber = BOPNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
