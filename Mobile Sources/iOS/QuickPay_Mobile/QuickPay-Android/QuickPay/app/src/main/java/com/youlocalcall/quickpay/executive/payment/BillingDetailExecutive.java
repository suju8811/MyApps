package com.youlocalcall.quickpay.executive.payment;

import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.executive.account.PersonAddressDisplayContext;
import com.youlocalcall.quickpay.model.payment.PaymentMethod;
import com.youlocalcall.quickpay.model.payment.profile.PaymentPersonAddress;
import com.youlocalcall.quickpay.model.payment.profile.PaymentProfile;

public class BillingDetailExecutive extends CreditCardParentExecutive {
    private BillingDetailDisplay display;
    private BillingDetailParentExecutive parentExecutive;

    public BillingDetailExecutive(BillingDetailDisplay display, BillingDetailParentExecutive parentExecutive) {
        super(display);
        this.display = display;
        this.parentExecutive = parentExecutive;
        this.parentExecutive.childExecutive = this;
    }

    public void displayDidLoad() {
        parentExecutive.displayDidLoad();
    }

    public void loadDisplay() {

        //
        // Load main information
        //

        AppData.paymentProfile.getPersonAddress().setAddressType(PaymentPersonAddress.AddressType.OneAddress);
        PaymentProfile context = AppData.paymentProfile;

        //
        // Load person address context
        //

        PersonAddressDisplayContext personAddressDisplayContext = new PersonAddressDisplayContext();
        personAddressDisplayContext.setPersonAddress(context.getPersonAddress().copy());
        display.loadLayout(context);
        childExecutive.loadDisplay(context, personAddressDisplayContext);
    }

    public void didSelectPaymentMethod(PaymentMethod paymentMethod) {
        display.selectPaymentMethod(paymentMethod);
    }

    public interface BillingDetailDisplay extends CommonDisplay {
        void loadLayout(PaymentProfile context);
        void selectPaymentMethod(PaymentMethod paymentMethod);
    }
}
