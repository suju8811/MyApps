package com.youlocalcall.quickpay.model.payment.creditcard;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.payment.PaymentRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Michael on 6/20/17.
 */

public class CreditCardPaymentRequest extends PaymentRequest {
    private String phoneNumber = "";
    private String email = "";
    private boolean usePersonEmail = false;
    private String country = "";
    private String billingAdress = "";
    private boolean usePersonAddress = false;
    private String city = "";
    private String state = "";
    private String zipCode = "";
    private boolean isUpdate = false;

    public void post(final APIUtility.APIResponse<CommonResponse> apiResponse, final CommonExecutive executive) {
        paymentService.paymentByCreditCard(getInmateId(),
                getPaymentProfileID(),
                getCardHolder(),
                getCvc(),
                getCardNumber(),
                getExpiration(),
                "",
                getTotalAmount(),
                phoneNumber,
                email,
                usePersonEmail ? 1 : 0,
                country,
                billingAdress,
                usePersonAddress ? 1 : 0,
                city,
                state,
                zipCode).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isUsePersonEmail() {
        return usePersonEmail;
    }

    public void setUsePersonEmail(boolean usePersonEmail) {
        this.usePersonEmail = usePersonEmail;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBillingAdress() {
        return billingAdress;
    }

    public void setBillingAdress(String billingAdress) {
        this.billingAdress = billingAdress;
    }

    public boolean isUsePersonAddress() {
        return usePersonAddress;
    }

    public void setUsePersonAddress(boolean usePersonAddress) {
        this.usePersonAddress = usePersonAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    public void setUpdate(boolean update) {
        isUpdate = update;
    }
}
