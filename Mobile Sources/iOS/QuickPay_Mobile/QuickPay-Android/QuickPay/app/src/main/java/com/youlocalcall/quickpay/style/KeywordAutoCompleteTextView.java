package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;

public class KeywordAutoCompleteTextView extends AppCompatAutoCompleteTextView {
    public KeywordAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTransformationMethod(null);
        BaseFontStyles.applyArialRegularFont(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setStateListAnimator(null);
        }
    }
}
