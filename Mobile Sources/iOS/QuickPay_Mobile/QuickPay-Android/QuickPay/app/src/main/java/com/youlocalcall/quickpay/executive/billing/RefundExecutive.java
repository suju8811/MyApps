package com.youlocalcall.quickpay.executive.billing;

import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.payment.refund.GetRefundListResponse;
import com.youlocalcall.quickpay.model.payment.refund.GetRefundRawResponse;
import com.youlocalcall.quickpay.model.payment.refund.RefundRequest;

import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingActor.Customer;

/**
 * Created by Michael on 6/27/17.
 */

public class RefundExecutive extends CommonExecutive {

    static final String UnavailableRefund = "\nThis payment is still unsettlement. After 24 hours passed, You can refund it.\n";
    RefundDisplay display;
    private BillingExecutive.BillingActor selectedActor = Customer;

    public RefundExecutive(RefundDisplay display) {
        super(display);
        this.display = display;
    }

    public void displayDidLoad(RefundRequest getRefundListRequest) {
        display.showLoading(true);
        getRefundListRequest.get(new APIUtility.APIResponse<GetRefundRawResponse>() {
            @Override
            public void onResponse(GetRefundRawResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.loadRefundList(response.getResult());
            }
        }, this);
    }

    public void didPressRefundButton(RefundRequest getRefundListRequest) {
        RefundRequest refundRequest = new RefundRequest();
        refundRequest.setPaymentId(getRefundListRequest.getPaymentId());
        refundRequest.setKind("process");
        display.showLoading(true);
        refundRequest.post(new APIUtility.APIResponse<CommonResponse>() {
            @Override
            public void onResponse(CommonResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.showResultMessage(response.getMessage());
            }
        }, this);
    }

    public interface RefundDisplay extends CommonExecutive.CommonDisplay {
        void loadRefundList(GetRefundListResponse refundListResponse);

        void showResultMessage(String message);
    }
}
