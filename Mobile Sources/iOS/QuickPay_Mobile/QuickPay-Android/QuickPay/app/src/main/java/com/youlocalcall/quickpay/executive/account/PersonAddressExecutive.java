package com.youlocalcall.quickpay.executive.account;

import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.account.address.Country;
import com.youlocalcall.quickpay.model.account.address.GetStateRequest;
import com.youlocalcall.quickpay.model.account.address.GetStateResponse;
import com.youlocalcall.quickpay.model.account.address.PersonAddress;
import com.youlocalcall.quickpay.model.account.address.State;

import java.util.ArrayList;

/**
 * Created by Michael on 6/21/17.
 */

public class PersonAddressExecutive extends CommonExecutive {
    PersonAddressDisplay display;
    PersonAddressDisplayContext context = new PersonAddressDisplayContext();
    PersonAddressParentExecutive parentExecutive;

    public PersonAddressExecutive(PersonAddressDisplay display, PersonAddressParentExecutive parentExecutive) {
        super(display);
        this.display = display;
        this.parentExecutive = parentExecutive;
        this.parentExecutive.childExecutive = this;
    }

    public void displayDidLoad() {
        parentExecutive.displayDidLoad();
    }

    public void loadDisplay(PersonAddressDisplayContext displayContext) {
        this.context = displayContext;
        refreshLayout(context);
        Country country = context.getPersonAddress().getCountry();
        if (country.getId() != null) {
            for (int index = 0; index < AppData.countries.size(); index++) {
                if (country.getName().equalsIgnoreCase(AppData.countries.get(index).getName())) {
                    context.getCountryState().setSelectedCountry(index);
                    break;
                }
            }
            new GetStateRequest(country.getId()).post(new APIUtility.APIResponse<GetStateResponse>() {
                @Override
                public void onResponse(GetStateResponse response) {
                    ArrayList<State> states = response.getResult().getStates();
                    context.getCountryState().setStates(states);
                    String stateName = AppData.paymentProfile.getPersonAddress().getState().getStateName();
                    for (int index = 0; index < states.size(); index++) {
                        String compareStateName = states.get(index).getStateName();
                        if (compareStateName.trim().equalsIgnoreCase(stateName.trim())) {
                            AppData.paymentProfile.getPersonAddress().setState(states.get(index));
                            context.getCountryState().setSelectedState(index);
                            break;
                        }
                    }
                    refreshDisplay(context);
                }
            }, this);
            return;
        }
        refreshDisplay(context);
    }

    public void refreshDisplay(PersonAddressDisplayContext displayContext) {
        context = displayContext;
        display.loadDisplay(context);
    }

    public void refreshLayout(PersonAddressDisplayContext displayContext) {
        context = displayContext;
        display.refreshLayout(context);
    }

    public void didSelectCountry(Integer index) {
        Integer countryId = AppData.countries.get(index).getId();
        display.showLoading(true);
        new GetStateRequest(countryId).post(new APIUtility.APIResponse<GetStateResponse>() {
            @Override
            public void onResponse(GetStateResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                context.getCountryState().setStates(response.getResult().getStates());
                context.countryState.setSelectedState(null);
                display.reloadStateDropDown(context);
            }
        }, this);
    }

    public void didSelectUsePersonAddress(boolean isSelected) {
        display.usePersonAddress(isSelected);
    }

    public interface PersonAddressDisplay extends CommonExecutive.CommonDisplay {
        void refreshLayout(PersonAddressDisplayContext context);

        void loadDisplay(PersonAddressDisplayContext context);

        void usePersonAddress(boolean isUse);

        PersonAddress getPersonAddressInfo();

        void reloadStateDropDown(PersonAddressDisplayContext context);
    }
}
