package com.youlocalcall.quickpay.common;

import android.app.Activity;
import android.graphics.Point;
import android.view.Display;

/**
 * Created by Michael on 6/22/17.
 */

public class Common {

    public static Point screenSize(Activity context) {
        Point size = new Point();
        Display display = context.getWindowManager().getDefaultDisplay();
        display.getSize(size);
        return size;
    }
}
