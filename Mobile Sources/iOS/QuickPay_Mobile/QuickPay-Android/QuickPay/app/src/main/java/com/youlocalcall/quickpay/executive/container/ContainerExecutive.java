package com.youlocalcall.quickpay.executive.container;

import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.payment.refund.RefundRequest;

/**
 * Created by Michael on 6/21/17.
 */

public class ContainerExecutive extends CommonExecutive {
    ContainerDisplay display;

    private SideMenuItem selectedItem = SideMenuItem.QuickPay;

    public ContainerExecutive(ContainerDisplay display) {
        super(display);
        this.display = display;
    }

    public void showLoading(boolean isShow) {
        display.showLoading(isShow);
    }

    public void didPressSideMenuItem(SideMenuItem item) {
        if (selectedItem == item) {
            return;
        }
        selectedItem = item;
        display.selectSideMenuItem(item);
    }

    public void didPressAddAmountButton() {
        display.replaceAddAmount();
    }

    public void didPressRefundButton(RefundRequest request) {
        display.replaceRefund(request);
    }

    public void didPressChildBackButton() {
        display.removeChildFragment();
    }

    public void didPressSideMenuButton() {
        display.showSideMenu(selectedItem);
    }

    public enum SideMenuItem {
        QuickPay,
        Account,
        CreditCard,
        BillingInvoice,
        BillingPayments,
        LogOut
    }

    public interface ContainerDisplay extends CommonDisplay {
        void selectSideMenuItem(SideMenuItem item);
        void showSideMenu(SideMenuItem item);
        void replaceAddAmount();

        void replaceRefund(RefundRequest request);

        void removeChildFragment();
    }
}
