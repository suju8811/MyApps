package com.youlocalcall.quickpay.model.billing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Michael on 6/20/17.
 */

public class BillingResponseArray {

    @SerializedName("data")
    @Expose

    ArrayList<BillingResponse> data = new ArrayList<>();

    @SerializedName("recordsTotal")
    @Expose

    private Integer recordsTotal;

    @SerializedName("recordsFiltered")
    @Expose

    private Integer recordsFiltered;

    public Integer getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(Integer recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public Integer getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(Integer recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public ArrayList<BillingResponse> getData() {
        return data;
    }

    public void setData(ArrayList<BillingResponse> data) {
        this.data = data;
    }
}
