package com.youlocalcall.quickpay.executive;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.model.CommonResponse;

import retrofit2.Response;

/**
 * Created by Michael on 6/21/17.
 */

public class CommonExecutive {
    public CommonDisplay display;

    public CommonExecutive(CommonDisplay display) {
        this.display = display;
    }

    public boolean validateResponse(Response response) {
        if (!response.isSuccessful()) {
            display.showError(R.string.request_failed);
            return false;
        }
        CommonResponse responseBody = ((Response<CommonResponse>) response).body();
        if (responseBody == null ||
                responseBody.getCode() != 0) {
            display.showError(responseBody.getMessage());
            return false;
        }
        return true;
    }

    public interface CommonDisplay {
        public void showLoading(boolean isShow);
        public void showError(String message);
        public void showError(int message);
    }
}
