package com.youlocalcall.quickpay.view.billing;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.executive.billing.RefundExecutive;
import com.youlocalcall.quickpay.model.payment.refund.GetRefundListResponse;
import com.youlocalcall.quickpay.model.payment.refund.GetRefundResponse;
import com.youlocalcall.quickpay.model.payment.refund.RefundRequest;
import com.youlocalcall.quickpay.style.TextViewRegular;
import com.youlocalcall.quickpay.view.BaseFragment;

import static com.youlocalcall.quickpay.view.billing.BillingContentFragment.BillingItemType.BillingContent;
import static com.youlocalcall.quickpay.view.billing.BillingContentFragment.BillingItemType.BillingHeader;

public class RefundFragment extends BaseFragment implements View.OnClickListener,
        RefundExecutive.RefundDisplay {

    private RefundRequest getRefundListRequest;
    private RefundsRecyclerViewAdapter recyclerViewAdapter = new RefundsRecyclerViewAdapter();
    private GetRefundListResponse refundListData = new GetRefundListResponse();
    private RefundExecutive executive;

    //
    // Controls
    //

    private RecyclerView refundList;
    private TextViewRegular unavailableTextView;
    private TextViewRegular transactionTextView;
    private View buttonsView;


    public RefundFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_refund, container, false);

        //
        // Set Content
        //

        refundList = (RecyclerView) view.findViewById(R.id.refundListView);
        refundList.setAdapter(recyclerViewAdapter);
        unavailableTextView = (TextViewRegular) view.findViewById(R.id.unavailableTextView);
        transactionTextView = (TextViewRegular) view.findViewById(R.id.transactionTextView);
        buttonsView = view.findViewById(R.id.buttonView);

        //
        // Set Event Handler
        //

        view.findViewById(R.id.imageViewBack).setOnClickListener(this);
        view.findViewById(R.id.cancelButton).setOnClickListener(this);
        view.findViewById(R.id.refundButton).setOnClickListener(this);

        //
        // Display did load
        //

        executive = new RefundExecutive(this);
        executive.displayDidLoad(getRefundListRequest);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack:
            case R.id.cancelButton:
                containerExecutive.didPressChildBackButton();
                break;
            case R.id.refundButton:
                executive.didPressRefundButton(getRefundListRequest);
                break;
            default:
                break;
        }
    }

    public void setGetRefundListRequest(RefundRequest getRefundListRequest) {
        this.getRefundListRequest = getRefundListRequest;
    }

    @Override
    public void loadRefundList(GetRefundListResponse refundListResponse) {
        this.refundListData = refundListResponse;
        transactionTextView.setText(refundListResponse.getTransaction());
        if (refundListResponse.getIsSettle() == 0) {
            buttonsView.setVisibility(View.GONE);
            unavailableTextView.setVisibility(View.VISIBLE);
            unavailableTextView.setText(R.string.refund_unsettle);
        } else {
            buttonsView.setVisibility(View.VISIBLE);
            unavailableTextView.setVisibility(View.GONE);
        }
        recyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void showResultMessage(String message) {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        containerExecutive.didPressChildBackButton();
                    }
                })
                .show();
    }

    public class RefundsRecyclerViewAdapter extends RecyclerView.Adapter<RefundFragment.RefundsRecyclerViewAdapter.ViewHolder> {

        @Override
        public int getItemViewType(int position) {
            if (position == refundListData.getPayments().size()) {
                return BillingHeader.getValue();
            } else {
                return BillingContent.getValue();
            }
        }

        @Override
        public RefundFragment.RefundsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;
            if (viewType == BillingHeader.getValue()) {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_refund_total, parent, false);
                view.setTag(BillingHeader);
            } else {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_refund_item, parent, false);
                view.setTag(BillingContent);
            }
            return new RefundFragment.RefundsRecyclerViewAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (position == refundListData.getPayments().size()) {
                holder.valueTextView.setText(refundListData.getPrice());
                return;
            }
            final GetRefundResponse response = refundListData.getPayments().get(position);
            holder.titleTextView.setText(response.getDescription());
            holder.valueTextView.setText(String.valueOf(response.getPaymentAmount()));
        }

        @Override
        public int getItemCount() {
            return refundListData.getPayments().size() + 1;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final TextViewRegular titleTextView;
            final TextViewRegular valueTextView;

            ViewHolder(View view) {
                super(view);

                titleTextView = (TextViewRegular) view.findViewById(R.id.titleTextView);
                valueTextView = (TextViewRegular) view.findViewById(R.id.valueTextView);
            }
        }
    }
}
