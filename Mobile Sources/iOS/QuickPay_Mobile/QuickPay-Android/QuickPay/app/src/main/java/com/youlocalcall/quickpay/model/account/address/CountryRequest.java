package com.youlocalcall.quickpay.model.account.address;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.account.BaseRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountryRequest extends BaseRequest {

    public void post(final APIUtility.APIResponse<CountryResponse> apiResponse, final CommonExecutive executive) {
        accountService.getCountries().enqueue(new Callback<CountryResponse>() {
            @Override
            public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                AppData.countries = response.body().getResult().getCountries();
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<CountryResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }
}
