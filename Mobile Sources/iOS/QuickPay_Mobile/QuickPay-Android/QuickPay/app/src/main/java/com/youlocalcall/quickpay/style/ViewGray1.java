package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Michael on 6/20/17.
 */

public class ViewGray1 extends View {
    public ViewGray1(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackgroundColor(BaseColors.YLC_GRAY1);
    }
}
