package com.youlocalcall.quickpay.view;

/**
 * Created by CJH on 2016.01.21.
 */
public interface Bridge {

    void onBack();

    void cleanBackStack();

    void switchTo(BaseFragment fragment, boolean cleanBackStack);

    void switchTo(BaseFragment fragment, boolean cleanBackStack, boolean popFist);

    void switchTo(BaseFragment fragment, int pop);

    void switchTo(BaseFragment fragment, String match);

    void replaceTo(BaseFragment fragment, boolean cleanBackStack);
}
