package com.youlocalcall.quickpay.common;

/**
 * Created by Michael on 6/21/17.
 */

public interface Constants {

    //
    // View transition animation
    //

    public final static int ANIMATION_RIGHT_TO_LEFT = 1;
    public final static int ANIMATION_LEFT_TO_RIGHT = 2;
    public final static int ANIMATION_BOTTOM_TO_UP = 3;
    public final static int ANIMATION_UP_TO_BOTTOM = 4;
}
