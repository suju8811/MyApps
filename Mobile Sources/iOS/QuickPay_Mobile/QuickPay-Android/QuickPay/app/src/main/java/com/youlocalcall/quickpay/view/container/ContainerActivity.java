package com.youlocalcall.quickpay.view.container;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.common.Common;
import com.youlocalcall.quickpay.common.Constants;
import com.youlocalcall.quickpay.executive.billing.BillingDisplayContext;
import com.youlocalcall.quickpay.executive.container.ContainerExecutive;
import com.youlocalcall.quickpay.model.payment.refund.RefundRequest;
import com.youlocalcall.quickpay.view.BaseFragment;
import com.youlocalcall.quickpay.view.account.AccountFragment;
import com.youlocalcall.quickpay.view.account.AddAmountFragment;
import com.youlocalcall.quickpay.view.account.LoginActivity;
import com.youlocalcall.quickpay.view.billing.BillingFragment;
import com.youlocalcall.quickpay.view.billing.RefundFragment;
import com.youlocalcall.quickpay.view.creditcard.CreditCardFragment;
import com.youlocalcall.quickpay.view.quickpay.QuickPayFragment;
import com.youlocalcall.quickpay.view.sidemenu.LeftSideMenuFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingType.Invoice;
import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingType.Payment;

public class ContainerActivity extends BaseSlidingActivity implements ContainerExecutive.ContainerDisplay, PayPalPaymentInterface {

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private BaseFragment contentFragment;

    //
    // PayPal
    //
    private LeftSideMenuFragment menuFragment;
    private ContainerExecutive executive;
    private String CONFIG_ENVIRONMENT;
    private PayPalConfiguration payPalConfiguration;
    private PaymentType paymentType;
    private PayPalConfirmInterface confirmInterface;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //
        // Set executive
        //

        executive = new ContainerExecutive(this);

        //
        // Set sliding menu view
        //

        menuFragment = new LeftSideMenuFragment();
        menuFragment.containerExecutive = executive;
        setBehindContentView(R.layout.sliding_menu_frame);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.menu_frame, menuFragment)
                .commit();

        //
        // Set content view
        //

        contentFragment = new QuickPayFragment();
        ((QuickPayFragment) contentFragment).setPayPalPaymentInterface(this);
        setContentView(R.layout.activity_container);
        switchContent(contentFragment, ContainerExecutive.SideMenuItem.QuickPay);

        //
        // Configure Slide Menu
        //

        SlidingMenu menu = getSlidingMenu();
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.side_menu_shadow_size);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffset((int) (Common.screenSize(this).x * 0.35));
        menu.setFadeDegree(0.35f);

        //
        // Set PayPal Config
        //

        if (AppData.payPalInfo.getMode().equalsIgnoreCase("sandbox")) {
            CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
        } else {
            CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
        }
        payPalConfiguration = new PayPalConfiguration().environment(CONFIG_ENVIRONMENT)
                .clientId(AppData.payPalInfo.getClientId())
                .merchantName("YLC, Inc.").merchantPrivacyPolicyUri(
                        Uri.parse("https://www.example.com/privacy"))
                .merchantUserAgreementUri(
                        Uri.parse("https://www.example.com/legal"));

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, payPalConfiguration);
        startService(intent);
    }

    public void switchContent(BaseFragment fragment, ContainerExecutive.SideMenuItem item) {
        contentFragment = fragment;
        contentFragment.containerExecutive = executive;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
        getSlidingMenu().showContent();
    }

    @Override
    public void selectSideMenuItem(ContainerExecutive.SideMenuItem item) {
        BaseFragment newContent = null;
        BillingDisplayContext displayContext;
        switch (item) {
            case QuickPay:
                newContent = new QuickPayFragment();
                ((QuickPayFragment) newContent).setPayPalPaymentInterface(this);
                break;
            case Account:
                newContent = new AccountFragment();
                break;
            case CreditCard:
                newContent = new CreditCardFragment();
                break;
            case BillingInvoice:
                displayContext = new BillingDisplayContext();
                displayContext.getFilterRequest().setBillingType(Invoice);
                newContent = BillingFragment.newInstance(displayContext);
                break;
            case BillingPayments:
                displayContext = new BillingDisplayContext();
                displayContext.getFilterRequest().setBillingType(Payment);
                newContent = BillingFragment.newInstance(displayContext);
                break;
            case LogOut:
                AppData.user = null;
                showActivity(LoginActivity.class, Constants.ANIMATION_RIGHT_TO_LEFT, true);
                break;
        }
        if (newContent != null) {
            menuFragment.setHighLightItem(item);
            switchContent(newContent, item);
        }
    }

    @Override
    public void showSideMenu(ContainerExecutive.SideMenuItem item) {
        getSlidingMenu().showMenu();
    }

    @Override
    public void replaceAddAmount() {
        AddAmountFragment fragment = new AddAmountFragment();
        fragment.setPayPalPaymentInterface(this);
        fragment.containerExecutive = executive;
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(R.id.container, fragment)
                .commit();
    }

    @Override
    public void replaceRefund(RefundRequest request) {
        RefundFragment fragment = new RefundFragment();
        fragment.setGetRefundListRequest(request);
        fragment.containerExecutive = executive;
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                .replace(R.id.container, fragment)
                .commit();
    }

    @Override
    public void removeChildFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.container, contentFragment)
                .commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        System.out.println(confirm.toJSONObject().toString(4));
                        JSONObject confirmObject = confirm.toJSONObject();
                        System.out.println(confirm.getPayment().toJSONObject()
                                .toString(4));
                        String paymentId = confirm.getProofOfPayment().getPaymentId();
                        confirmInterface.proceedPayPalConfirm(paymentId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                System.out.println("The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                System.out
                        .println("An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data
                        .getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject()
                                .toString(4));
                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);
                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample",
                                "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    @Override
    public void proceedPayPalPayment(PaymentType paymentType, String amount, PayPalConfirmInterface confirmInterface) {
        PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(amount), "USD",
                "YLC", PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(ContainerActivity.this,
                PaymentActivity.class);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
        this.paymentType = paymentType;
        this.confirmInterface = confirmInterface;
    }
}
