package com.youlocalcall.quickpay.executive.billing;

import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.billing.BillingFilterRequest;

/**
 * Created by Michael on 6/21/17.
 */

public class BillingFilterExecutive extends CommonExecutive {

    public static Integer FilterOperatorValues[] = {1, 4, 5, 6};
    public static Integer InvoiceTypeFilterValues[] = {0, 3, 4, 5, 17};
    public static Integer PaymentTypeFilterValues[] = {0, 1, 2, 10, 11, 12, 15};

    private BillingFilterDisplay display;
    private BillingFilterRequest displayContext;

    public BillingFilterExecutive(BillingFilterDisplay display) {
        super(display);
        this.display = display;
    }

    //
    // Refresh Layout and Load Views
    //

    public void displayDidLoad(BillingFilterRequest context) {
        displayContext = context;
        display.loadDisplay(displayContext);
    }

    public void refreshLayout(BillingFilterRequest context) {
        displayContext = context;
        display.refreshLayout(displayContext);
    }

    // Event Handler

    public void didPressViewButton() {
        displayContext.setExpanded(false);
        display.refreshLayout(displayContext);
    }

    public void didPressExpandButton() {
        boolean expanded = displayContext.isExpanded();
        displayContext.setExpanded(!expanded);
        display.refreshLayout(displayContext);
    }

    public interface BillingFilterDisplay extends CommonExecutive.CommonDisplay {
        void refreshLayout(BillingFilterRequest context);

        void loadDisplay(BillingFilterRequest context);

        BillingFilterRequest getFilterRequest();
    }
}
