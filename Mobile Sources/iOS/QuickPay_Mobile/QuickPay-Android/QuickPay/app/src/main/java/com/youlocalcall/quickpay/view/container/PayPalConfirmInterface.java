package com.youlocalcall.quickpay.view.container;

public interface PayPalConfirmInterface {
    void proceedPayPalConfirm(String paymentId);
}
