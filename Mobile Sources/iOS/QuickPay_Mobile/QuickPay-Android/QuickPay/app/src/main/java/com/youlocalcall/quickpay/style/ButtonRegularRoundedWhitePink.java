package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Michael on 6/20/17.
 */

public class ButtonRegularRoundedWhitePink extends ButtonRegularRounded {
    public ButtonRegularRoundedWhitePink(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        setRoundedBackground(BaseColors.YLC_PINK, -1, 0);
        setTextColor(BaseColors.YLC_WHITE);
    }
}
