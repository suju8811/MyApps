package com.youlocalcall.quickpay.model.account.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class State extends Country {

    @SerializedName("stateID")
    @Expose
    private Integer stateId;

    @SerializedName("stateName")
    @Expose
    private String stateName;

    @SerializedName("stateAbbr")
    @Expose
    private String stateAbbreviation;

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateAbbreviation() {
        return stateAbbreviation;
    }

    public void setStateAbbreviation(String stateAbbreviation) {
        this.stateAbbreviation = stateAbbreviation;
    }

    State copyState() {
        State state = new State();
        state.setId(getId());
        state.setName(getName());
        state.setAbbreviation(getAbbreviation());
        state.setCode(getCode());
        state.stateId = stateId;
        state.stateName = stateName;
        state.stateAbbreviation = stateAbbreviation;
        return state;
    }
}
