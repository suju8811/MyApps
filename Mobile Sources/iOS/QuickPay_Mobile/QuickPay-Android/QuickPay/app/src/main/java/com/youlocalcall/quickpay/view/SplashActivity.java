package com.youlocalcall.quickpay.view;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.common.Constants;
import com.youlocalcall.quickpay.view.account.LoginActivity;

public class SplashActivity extends BaseActivity implements Constants {

    final int ACTION_GO_TO_LOGIN = 1;
    final int SPLASH_DELAY_TIME = 2500;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == ACTION_GO_TO_LOGIN) {
                showActivity(LoginActivity.class, Constants.ANIMATION_BOTTOM_TO_UP, true);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mHandler.sendEmptyMessageDelayed(ACTION_GO_TO_LOGIN, SPLASH_DELAY_TIME);
    }
}
