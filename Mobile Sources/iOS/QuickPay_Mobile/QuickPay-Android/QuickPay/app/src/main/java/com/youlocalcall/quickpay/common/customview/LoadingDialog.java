package com.youlocalcall.quickpay.common.customview;

import android.app.ProgressDialog;
import android.content.Context;

import com.youlocalcall.quickpay.R;

/**
 * Created by Michael on 6/21/17.
 */

public class LoadingDialog extends ProgressDialog {

    Context mContext;
    String mMessage;

    public LoadingDialog(Context context, String message) {
        super(context, R.style.TransparentProgressDialog);

        mContext = context;
        mMessage = message;

        setCancelable(false);
        setMessage(message);

    }
}
