package com.youlocalcall.quickpay.model.payment.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.youlocalcall.quickpay.model.CommonResponse;

/**
 * Created by Michael on 6/27/17.
 */

public class PaymentProfileResponse extends CommonResponse {

    @SerializedName("result")
    @Expose

    private PaymentProfileResponseResult result;

    public PaymentProfileResponseResult getResult() {
        return result;
    }

    public void setResult(PaymentProfileResponseResult result) {
        this.result = result;
    }
}
