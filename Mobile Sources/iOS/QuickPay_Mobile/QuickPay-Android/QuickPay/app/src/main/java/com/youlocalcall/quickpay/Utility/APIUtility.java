package com.youlocalcall.quickpay.Utility;

import com.youlocalcall.quickpay.model.account.AccountService;
import com.youlocalcall.quickpay.model.billing.BillingService;
import com.youlocalcall.quickpay.model.payment.PaymentService;

public class APIUtility {
//    static final String BASE_URL = "http://208.73.233.211:8080/YLC/";
    static final String BASE_URL = "https://www.yourlocalcall.com:8443/YLC/";

    public static AccountService getAccountService() {
        return RetrofitClient.getClient(BASE_URL).create(AccountService.class);
    }

    public static BillingService getBillingService() {
        return RetrofitClient.getClient(BASE_URL).create(BillingService.class);
    }

    public static PaymentService getPaymentService() {
        return RetrofitClient.getClient(BASE_URL).create(PaymentService.class);
    }

    public interface APIResponse<T> {
        void onResponse(T response);
    }
}
