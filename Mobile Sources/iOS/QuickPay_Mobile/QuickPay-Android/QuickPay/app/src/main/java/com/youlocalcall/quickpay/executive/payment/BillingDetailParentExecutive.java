package com.youlocalcall.quickpay.executive.payment;

import com.youlocalcall.quickpay.executive.CommonExecutive;

/**
 * Created by Michael on 6/28/17.
 */

public class BillingDetailParentExecutive extends CommonExecutive {

    protected BillingDetailExecutive childExecutive;

    public BillingDetailParentExecutive(CommonDisplay display) {
        super(display);
    }

    public void displayDidLoad() {
    }
}
