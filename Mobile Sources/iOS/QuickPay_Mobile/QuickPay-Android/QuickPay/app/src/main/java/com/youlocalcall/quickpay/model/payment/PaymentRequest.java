package com.youlocalcall.quickpay.model.payment;

import com.youlocalcall.quickpay.model.account.BaseRequest;

/**
 * Created by Michael on 6/20/17.
 */

public class PaymentRequest extends BaseRequest {
    private Integer inmateId = null;
    private String totalAmount = "";
    private String paymentProfileID = "";
    private String cardHolder = "";
    private String cvc = "";
    private String cardNumber = "";
    private String expiration = "";

    public Integer getInmateId() {
        return inmateId;
    }

    public void setInmateId(Integer inmateId) {
        this.inmateId = inmateId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPaymentProfileID() {
        return paymentProfileID;
    }

    public void setPaymentProfileID(String paymentProfileID) {
        this.paymentProfileID = paymentProfileID;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }
}
