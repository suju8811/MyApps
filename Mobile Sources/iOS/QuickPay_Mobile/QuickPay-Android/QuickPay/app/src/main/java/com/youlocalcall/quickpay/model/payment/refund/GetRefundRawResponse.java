package com.youlocalcall.quickpay.model.payment.refund;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.youlocalcall.quickpay.model.CommonResponse;

/**
 * Created by Michael on 6/27/17.
 */

public class GetRefundRawResponse extends CommonResponse {

    @SerializedName("result")
    @Expose

    GetRefundListResponse result;

    public GetRefundListResponse getResult() {
        return result;
    }

    public void setResult(GetRefundListResponse result) {
        this.result = result;
    }
}
