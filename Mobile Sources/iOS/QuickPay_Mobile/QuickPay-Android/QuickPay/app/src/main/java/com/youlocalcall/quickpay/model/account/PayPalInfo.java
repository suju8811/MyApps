package com.youlocalcall.quickpay.model.account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PayPalInfo {

    @SerializedName("mode")
    @Expose
    private String mode;

    @SerializedName("clientID")
    @Expose
    private String clientId;

    @SerializedName("secretKey")
    @Expose
    private String secretKey;

    public String getMode() {
        return mode;
    }

    public String getClientId() {
        return clientId;
    }

    public String getSecretKey() {
        return secretKey;
    }
}
