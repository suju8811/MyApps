package com.youlocalcall.quickpay.executive.payment;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.Utility.StringUtility;
import com.youlocalcall.quickpay.common.AppData;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.payment.PaymentAmountRequest;
import com.youlocalcall.quickpay.model.payment.confirm.PayPalConfirmRequest;
import com.youlocalcall.quickpay.view.container.PayPalConfirmInterface;
import com.youlocalcall.quickpay.view.container.PayPalPaymentInterface;
import com.youlocalcall.quickpay.view.container.PaymentType;

public class AddPrepayAmountExecutive extends BillingDetailParentExecutive implements PayPalConfirmInterface {

    private AddPrepayAmountDisplay display;
    private PayPalPaymentInterface payPalPaymentInterface;

    public AddPrepayAmountExecutive(AddPrepayAmountDisplay display,
                                    PayPalPaymentInterface payPalPaymentInterface) {
        super(display);
        this.display = display;
        this.payPalPaymentInterface = payPalPaymentInterface;
    }

    // MARK: - Event Handler

    public void displayDidLoad() {

        //
        // Load person address context
        //

        childExecutive.loadDisplay();
    }

    public void didPressSubmitButton() {
        display.showWarningMessage();
    }

    // MARK: - Helper functions

    public void proceedAddAmount() {
        PaymentAmountRequest request = display.getPaymentAmountRequest();
        if (request == null) {
            return;
        }
        if (request.isPayPal()) {
            proceedPrepayByPayPal(request);
        } else {
            proceedPrepayByCreditCard(request);
        }
    }

    private void proceedPrepayByCreditCard(PaymentAmountRequest request) {
        int validateString = validateCreditCardPrepay(request);
        if (validateString != R.string.input_validate) {
            display.showError(validateString);
            return;
        }
        if (!request.isUpdate()) {
            request.getPaymentProfile().setPaymentProfileID(AppData.paymentProfile.getPaymentProfileID());
        }
        display.showLoading(true);
        request.post(new APIUtility.APIResponse<CommonResponse>() {
            @Override
            public void onResponse(CommonResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.showError(StringUtility.html2String(response.getMessage()));
            }
        }, this);
    }

    private void proceedPrepayByPayPal(PaymentAmountRequest request) {

        int validateString = validateCreditCardPrepay(request);
        if (validateString != R.string.input_validate) {
            display.showError(validateString);
            return;
        }
        payPalPaymentInterface.proceedPayPalPayment(PaymentType.QuickPay, request.getTotalPrice(), this);
    }

    private int validateCreditCardPrepay(PaymentAmountRequest request) {
        if (request.getTotalPrice().isEmpty()) {
            return R.string.empty_total_amount;
        }
        if (request.isUpdate()) {
            return R.string.input_validate;
        }
        if (request.isPayPal()) {
            return R.string.input_validate;
        }
        String cardHolder = request.getPaymentProfile().getCardHolderName();
        if (cardHolder == null || cardHolder.isEmpty()) {
            return R.string.empty_card_holder_name;
        }
        String cvc = request.getPaymentProfile().getCvc();
        if (cvc == null || cvc.isEmpty()) {
            return R.string.empty_cvc;
        }
        String cardNumber = request.getPaymentProfile().getCardNumber();
        if (cardNumber == null || cardNumber.isEmpty()) {
            return R.string.empty_card_number;
        }
        String expiration = request.getPaymentProfile().getExpiration();
        if (expiration == null || expiration.isEmpty()) {
            return R.string.empty_expiration;
        }
        String phone = request.getPaymentProfile().getPhoneNumber();
        if (phone == null || phone.isEmpty()) {
            return R.string.empty_phone_number;
        }
        if (!StringUtility.isValidPhone(phone)) {
            return R.string.invalid_phone;
        }
        String email = request.getPaymentProfile().getEmail();
        if (email == null || email.isEmpty()) {
            return R.string.empty_email;
        }
        if (!StringUtility.isValidEmail(email)) {
            return R.string.invalid_email;
        }
        String countryName = request.getPaymentProfile().getPersonAddress().getCountry().getName();
        if (countryName == null || countryName.isEmpty()) {
            return R.string.empty_country;
        }
        String address = request.getPaymentProfile().getPersonAddress().getBillingAddress();
        if (address == null || address.isEmpty()) {
            return R.string.empty_billing_address;
        }
        String city = request.getPaymentProfile().getPersonAddress().getCity();
        if (city == null || city.isEmpty()) {
            return R.string.empty_city;
        }
        String stateName = request.getPaymentProfile().getPersonAddress().getState().getStateName();
        if (stateName == null || stateName.isEmpty()) {
            return R.string.empty_state;
        }
        String zipCode = request.getPaymentProfile().getPersonAddress().getZipCode();
        if (zipCode == null || zipCode.isEmpty()) {
            return R.string.empty_zip_code;
        }
        return R.string.input_validate;
    }

    @Override
    public void proceedPayPalConfirm(String paymentId) {
        PayPalConfirmRequest request = display.getPayPalConfirmRequest(paymentId);
        display.showLoading(true);
        request.prePayConfirm(new APIUtility.APIResponse<CommonResponse>() {
            @Override
            public void onResponse(CommonResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.showError(StringUtility.html2String(response.getMessage()));
            }
        }, this);
    }

    public interface AddPrepayAmountDisplay extends CommonDisplay {
        void showWarningMessage();
        PaymentAmountRequest getPaymentAmountRequest();

        PayPalConfirmRequest getPayPalConfirmRequest(String paymentId);
    }
}
