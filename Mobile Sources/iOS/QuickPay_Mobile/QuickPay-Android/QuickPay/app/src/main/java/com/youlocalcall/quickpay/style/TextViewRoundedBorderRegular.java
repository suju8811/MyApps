package com.youlocalcall.quickpay.style;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Michael on 6/20/17.
 */

public class TextViewRoundedBorderRegular extends TextViewCommon {
    public TextViewRoundedBorderRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        BaseFontStyles.applyArialRegularFont(this);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }
}
