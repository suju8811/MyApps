package com.youlocalcall.quickpay.executive.account;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.Utility.StringUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.account.signup.SignUpRequest;

/**
 * Created by Michael on 6/21/17.
 */

public class SignUpExecutive extends CommonExecutive {
    SignUpDisplay display;

    public SignUpExecutive(SignUpDisplay display) {
        super(display);
        this.display = display;
    }

    public void signUp() {
        SignUpRequest request = display.getSignUpRequest();
        if (request == null) {
            return;
        }

        int validateString = validateRequest(request);
        if (validateString != R.string.input_validate) {
            display.showError(validateString);
            return;
        }

        display.displayVerificationMethodSelectMessage(request);
    }

    public void sendSignUpRequest(SignUpRequest request, boolean isPhone) {
        request.setIsPhone(isPhone ? 1 : 0);
        display.showLoading(true);
        request.post(new APIUtility.APIResponse<CommonResponse>() {
            @Override
            public void onResponse(CommonResponse response) {
                display.showLoading(false);
                if (response == null) {
                    return;
                }
                display.showError(response.getMessage());
                display.signedUp(response.getMessage());
            }
        }, this);
    }

    int validateRequest(SignUpRequest request) {
        if (request.getUserName().isEmpty()) {
            return R.string.empty_user_name;
        }
        if (request.getPassword().isEmpty()) {
            return R.string.empty_password;
        }
        if (!request.getPassword().equals(request.getConfirmPassword())) {
            return R.string.password_mismatch;
        }
        if (request.getEmail().isEmpty() &&
                request.getPhoneNumber().isEmpty()) {
            return R.string.empty_phone_and_email;
        }
        if (!request.getEmail().isEmpty() &&
                !StringUtility.isValidEmail(request.getEmail())) {
            return R.string.invalid_email;
        }

        if (!request.getPhoneNumber().isEmpty() &&
                !StringUtility.isValidPhone(request.getPhoneNumber())) {
            return R.string.invalid_phone;
        }
        if (!request.isAcceptTerms()) {
            return R.string.accept_terms_warning;
        }
        return R.string.input_validate;
    }

    public interface SignUpDisplay extends CommonExecutive.CommonDisplay {
        void signedUp(String message);

        SignUpRequest getSignUpRequest();

        void displayVerificationMethodSelectMessage(SignUpRequest request);
    }
}
