package com.youlocalcall.quickpay.view.billing;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.executive.billing.BillingDisplayContext;
import com.youlocalcall.quickpay.executive.billing.BillingExecutive;
import com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingActor;
import com.youlocalcall.quickpay.model.billing.BillingRequest;
import com.youlocalcall.quickpay.model.billing.BillingResponse;
import com.youlocalcall.quickpay.model.billing.BillingResponseArray;
import com.youlocalcall.quickpay.style.SearchViewRounded;
import com.youlocalcall.quickpay.style.TextViewBold;
import com.youlocalcall.quickpay.style.TextViewRegular;
import com.youlocalcall.quickpay.style.TextViewRoundedBlueRegular;
import com.youlocalcall.quickpay.view.BaseFragment;

import static com.youlocalcall.quickpay.view.billing.BillingContentFragment.BillingItemType.BillingContent;
import static com.youlocalcall.quickpay.view.billing.BillingContentFragment.BillingItemType.BillingHeader;

public class BillingContentFragment extends BaseFragment implements View.OnClickListener {
    private static final String ARG_DISPLAY_CONTEXT = "displayContext";
    private BillingDisplayContext displayContext;
    private BillingFilterFragment filterFragment;
    private BillingExecutive billingExecutive;
    private SearchViewRounded searchView;
    private RecyclerView billingRecyclerView;
    private BillingsRecyclerViewAdapter billingsAdapter = new BillingsRecyclerViewAdapter();
    private BillingResponseArray billingResponseArray = new BillingResponseArray();

    public BillingContentFragment() {
    }

    public static BillingContentFragment newInstance(BillingDisplayContext displayContext) {
        BillingContentFragment fragment = new BillingContentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DISPLAY_CONTEXT, new Gson().toJson(displayContext));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String contextJSON = getArguments().getString(ARG_DISPLAY_CONTEXT);
            displayContext = new Gson().fromJson(contextJSON, BillingDisplayContext.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_billing_content, container, false);

        //
        // Create child fragments
        //

        filterFragment = BillingFilterFragment.newInstance(displayContext.getFilterRequest());
        filterFragment.setBillingExecutive(billingExecutive);
        filterFragment.setBillingActor(displayContext.getActor());
        placeContent(R.id.filter_container, filterFragment);

        //
        // Set Content
        //

        searchView = (SearchViewRounded) view.findViewById(R.id.searchView);
        billingRecyclerView = (RecyclerView) view.findViewById(R.id.billingList);
        billingRecyclerView.setAdapter(billingsAdapter);

        //
        // Set Event Handler
        //

        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                billingExecutive.loadBilling();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                View view = filterFragment.getView();
                if (view == null) {
                    return false;
                }
                billingExecutive.loadBilling();
                return true;
            }
        });

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
        }
    }

    public BillingRequest getBillingRequest() {
        BillingRequest billingRequest = new BillingRequest();
        billingRequest.getSearch().setValue(searchView.getQuery().toString());
        billingRequest.setFilterRequest(filterFragment.getFilterRequest());
        billingRequest.setBillingType(displayContext.getFilterRequest().getBillingType());
        billingRequest.setBillingActor(displayContext.getActor());
        return billingRequest;
    }

    public BillingFilterFragment getFilterFragment() {
        return filterFragment;
    }

    public void setBillingExecutive(BillingExecutive billingExecutive) {
        this.billingExecutive = billingExecutive;
    }

    public void reloadBillingList(BillingResponseArray responseArray) {
        this.billingResponseArray = responseArray;
        billingsAdapter.notifyDataSetChanged();
    }

    enum BillingItemType {
        BillingHeader(0),
        BillingContent(1);

        private int value;

        BillingItemType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public class BillingsRecyclerViewAdapter extends RecyclerView.Adapter<BillingsRecyclerViewAdapter.ViewHolder> {

        @Override
        public int getItemViewType(int position) {
            return BillingContent.value;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;
            if (viewType == BillingHeader.value) {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_billing_header, parent, false);
                view.setTag(BillingHeader);
            } else {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_billing_content, parent, false);
                view.setTag(BillingContent);
            }
            return new BillingsRecyclerViewAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final BillingResponse response = billingResponseArray.getData().get(position);
            holder.dateTextView.setText(response.getPaymentDate());
            holder.typeTextView.setText(response.getPaymentType());
            holder.cardNumberTextView.setText(response.getLast4Digits());
            if (holder.amountRoundTextView != null) {
                holder.amountRoundTextView.setText(response.getPaymentAmount());
            }
            holder.inmateTextView.setText(response.getInmateName());
            holder.descriptionTextView.setText(response.getDescription());
            holder.refundTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    billingExecutive.gotoRefund(response.getId());
                }
            });
        }

        @Override
        public int getItemCount() {
            return billingResponseArray.getData().size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final TextViewBold dateTextView;
            final TextViewRegular typeTextView;
            final TextViewRegular cardNumberTextView;
            final TextViewRegular amountRegularTextView;
            final TextViewRegular amountRoundTextView;
            final TextViewRegular inmateTextView;
            final TextViewRegular descriptionTextView;
            final TextViewBold refundTextView;

            ViewHolder(View view) {
                super(view);

                dateTextView = (TextViewBold) view.findViewById(R.id.dateTextView);
                typeTextView = (TextViewRegular) view.findViewById(R.id.typeTextView);
                cardNumberTextView = (TextViewRegular) view.findViewById(R.id.cardNumberTextView);
                if (view.getTag() == BillingHeader) {
                    amountRegularTextView = (TextViewRegular) view.findViewById(R.id.amountTextView);
                    amountRoundTextView = null;
                } else {
                    amountRegularTextView = null;
                    amountRoundTextView = (TextViewRegular) view.findViewById(R.id.amountTextView);
                }
                inmateTextView = (TextViewRegular) view.findViewById(R.id.inmateTextView);
                inmateTextView.setVisibility(displayContext.getActor() == BillingActor.Inmate ? View.VISIBLE : View.GONE);
                descriptionTextView = (TextViewRegular) view.findViewById(R.id.descriptionTextView);
                refundTextView = (TextViewBold) view.findViewById(R.id.refundTextView);
                refundTextView.setVisibility(displayContext.getFilterRequest().getBillingType() == BillingExecutive.BillingType.Payment ? View.VISIBLE : View.GONE);
            }
        }
    }
}