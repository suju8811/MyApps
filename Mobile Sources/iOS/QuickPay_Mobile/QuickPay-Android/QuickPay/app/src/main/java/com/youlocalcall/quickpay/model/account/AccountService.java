package com.youlocalcall.quickpay.model.account;

import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.account.Inmate.InmateByIdResponse;
import com.youlocalcall.quickpay.model.account.Inmate.InmateResponse;
import com.youlocalcall.quickpay.model.account.address.CountryResponse;
import com.youlocalcall.quickpay.model.account.address.GetStateResponse;
import com.youlocalcall.quickpay.model.account.login.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Michael on 6/23/17.
 */

public interface AccountService {

    @FormUrlEncoded
    @POST("customer/sign_in_direct.do")
    Call<LoginResponse> login(@Field("username") String userName,
                              @Field("password") String password,
                              @Field("is_mobile") Integer isMobile);

    @FormUrlEncoded
    @POST("customer/forget_password.do")
    Call<CommonResponse> forgot(@Field("email") String userName);

    @FormUrlEncoded
    @POST("customer/sign_up.do")
    Call<CommonResponse> signUp(@Field("username") String userName,
                                @Field("password") String password,
                                @Field("email_address") String email,
                                @Field("phone_number") String phone,
                                @Field("is_mobile") Integer isMobile,
                                @Field("is_phone") Integer isPhone);

    @FormUrlEncoded
    @POST("customer/resend_activation.do")
    Call<CommonResponse> resendActivation(@Field("username") String userName,
                                          @Field("is_phone") Integer isPhone);

    @FormUrlEncoded
    @POST("customer/activate_user.do")
    Call<CommonResponse> activate(@Field("username") String userName,
                                  @Field("password") String password,
                                  @Field("activation_code") String activationCode,
                                  @Field("is_mobile") Integer isMobile);

    @POST("address/get_country.do")
    Call<CountryResponse> getCountries();

    @FormUrlEncoded
    @POST("address/get_state.do")
    Call<GetStateResponse> getState(@Field("country") Integer country);

    @FormUrlEncoded
    @POST("inmate/get.do")
    Call<InmateByIdResponse> getInmateById(@Field("id") Integer id,
                                           @Field("from") String from);

    @FormUrlEncoded
    @POST("customer/update_password.do")
    Call<CommonResponse> updatePassword(@Field("old_password") String oldPassword,
                                        @Field("new_password") String newPassword,
                                        @Field("is_mobile") Integer isMobile);

    @FormUrlEncoded
    @POST("customer/update_profile.do")
    Call<CommonResponse> updateProfile(@Field("first_name") String firstName,
                                       @Field("middle_name") String middleName,
                                       @Field("last_name") String lastName,
                                       @Field("email_address") String email,
                                       @Field("contact_number") String phone,
                                       @Field("country") String country,
                                       @Field("address_1") String address1,
                                       @Field("address_2") String address2,
                                       @Field("city") String city,
                                       @Field("state") String state,
                                       @Field("zip_code") String zipCode,
                                       @Field("is_mobile") Integer isMobile);

    @FormUrlEncoded
    @POST("inmate/search.do")
    Call<InmateResponse> searchInmates(@Field("first_name") String firstName,
                                       @Field("last_name") String lastName,
                                       @Field("bop_number") String bopNumber);

    @FormUrlEncoded
    @POST("inmate/add_by_customer.do")
    Call<InmateResponse> addInmate(@Field("first_name") String firstName,
                                   @Field("last_name") String lastName,
                                   @Field("bop_number") String bopNumber);
}
