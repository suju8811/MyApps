package com.youlocalcall.quickpay.view.billing;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.executive.billing.BillingExecutive;
import com.youlocalcall.quickpay.executive.billing.BillingFilterExecutive;
import com.youlocalcall.quickpay.model.billing.BillingFilterRequest;
import com.youlocalcall.quickpay.style.EditTextFieldCommon;
import com.youlocalcall.quickpay.style.SpinnerRoundedBorderRegular;
import com.youlocalcall.quickpay.style.TextViewRegular;
import com.youlocalcall.quickpay.style.TextViewRoundedBorderRegular;
import com.youlocalcall.quickpay.view.BaseFragment;

import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingActor.Inmate;
import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingType.Invoice;
import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.BillingType.Payment;
import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.DateType.Current;
import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.DateType.From;
import static com.youlocalcall.quickpay.executive.billing.BillingExecutive.DateType.To;

public class BillingFilterFragment extends BaseFragment implements View.OnClickListener,
        BillingFilterExecutive.BillingFilterDisplay {

    private static final String ARG_DISPLAY_CONTEXT = "filterRequest";

    private BillingFilterRequest filterRequest;
    private BillingFilterExecutive executive;
    private BillingExecutive billingExecutive;
    private BillingExecutive.BillingActor billingActor;

    //
    // Controls
    //

    private View filterContentView;
    private ImageView headerArrowImageView;
    private TextViewRegular dateTitleTextView;
    private SpinnerRoundedBorderRegular dateOperatorSpinner;
    private TextViewRoundedBorderRegular dateButton;
    private View doubleDateView;
    private TextViewRoundedBorderRegular dateStartButton;
    private TextViewRoundedBorderRegular dateEndButton;
    private TextViewRegular typeTitleTextView;
    private SpinnerRoundedBorderRegular typeSpinner;
    private TextViewRegular amountTitleTextView;
    private SpinnerRoundedBorderRegular amountOperatorSpinner;
    private EditTextFieldCommon amountEditText;
    private View doubleAmountView;
    private EditTextFieldCommon amountStartEditText;
    private EditTextFieldCommon amountEndEditText;
    private View inmateBalanceView;
    private SpinnerRoundedBorderRegular balanceOperatorSpinner;
    private EditTextFieldCommon balanceEditText;
    private View doubleBalanceView;
    private EditTextFieldCommon balanceStartEditText;
    private EditTextFieldCommon balanceEndEditText;

    public BillingFilterFragment() {
    }

    public static BillingFilterFragment newInstance(BillingFilterRequest filterRequest) {
        BillingFilterFragment fragment = new BillingFilterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DISPLAY_CONTEXT, new Gson().toJson(filterRequest));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String contextJSON = getArguments().getString(ARG_DISPLAY_CONTEXT);
            filterRequest = new Gson().fromJson(contextJSON, BillingFilterRequest.class);
        }
        executive = new BillingFilterExecutive(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_billing_filter, container, false);

        //
        // Set Content
        //

        filterContentView = view.findViewById(R.id.filterContentView);
        headerArrowImageView = (ImageView) view.findViewById(R.id.headerArrowImageView);
        dateTitleTextView = (TextViewRegular) view.findViewById(R.id.dateTitleTextView);
        dateOperatorSpinner = (SpinnerRoundedBorderRegular) view.findViewById(R.id.dateOperatorSpinner);
        dateButton = (TextViewRoundedBorderRegular) view.findViewById(R.id.dateButton);
        doubleDateView = view.findViewById(R.id.doubleDateView);
        dateStartButton = (TextViewRoundedBorderRegular) view.findViewById(R.id.dateStartButton);
        dateEndButton = (TextViewRoundedBorderRegular) view.findViewById(R.id.dateEndButton);
        typeTitleTextView = (TextViewRegular) view.findViewById(R.id.typeTitleTextView);
        typeSpinner = (SpinnerRoundedBorderRegular) view.findViewById(R.id.typeSpinner);
        amountTitleTextView = (TextViewRegular) view.findViewById(R.id.amountTitleTextView);
        amountOperatorSpinner = (SpinnerRoundedBorderRegular) view.findViewById(R.id.amountOperatorSpinner);
        amountEditText = (EditTextFieldCommon) view.findViewById(R.id.amountEditText);
        doubleAmountView = view.findViewById(R.id.doubleAmountView);
        amountStartEditText = (EditTextFieldCommon) view.findViewById(R.id.amountStartEditText);
        amountEndEditText = (EditTextFieldCommon) view.findViewById(R.id.amountEndEditText);
        inmateBalanceView = view.findViewById(R.id.inmateBalanceView);
        balanceOperatorSpinner = (SpinnerRoundedBorderRegular) view.findViewById(R.id.balanceOperatorSpinner);
        balanceEditText = (EditTextFieldCommon) view.findViewById(R.id.balanceEditText);
        doubleBalanceView = view.findViewById(R.id.doubleBalanceView);
        balanceStartEditText = (EditTextFieldCommon) view.findViewById(R.id.balanceStartEditText);
        balanceEndEditText = (EditTextFieldCommon) view.findViewById(R.id.balanceEndEditText);

        //
        // Set Event Handler
        //

        view.findViewById(R.id.expandButton).setOnClickListener(this);
        dateButton.setOnClickListener(this);
        dateStartButton.setOnClickListener(this);
        dateEndButton.setOnClickListener(this);
        view.findViewById(R.id.viewButton).setOnClickListener(this);

        //
        // Load and refresh display
        //

        billingExecutive.reloadFilter(executive, billingActor);
        billingExecutive.loadBilling();

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.expandButton:
                executive.didPressExpandButton();
                break;
            case R.id.viewButton:
                executive.didPressViewButton();
                billingExecutive.loadBilling();
                break;
            case R.id.dateButton:
                billingExecutive.didPressDateButton(executive, Current);
                break;
            case R.id.dateStartButton:
                billingExecutive.didPressDateButton(executive, From);
                break;
            case R.id.dateEndButton:
                billingExecutive.didPressDateButton(executive, To);
                break;
        }
    }

    @Override
    public void refreshLayout(BillingFilterRequest context) {
        filterRequest = context;

        //
        // Layout Header
        //

        if (context.isExpanded()) {
            filterContentView.setVisibility(View.VISIBLE);
            headerArrowImageView.setImageResource(R.drawable.ic_arrow_up);
        } else {
            filterContentView.setVisibility(View.GONE);
            headerArrowImageView.setImageResource(R.drawable.ic_arrow_bottom);
        }

        //
        // Layout Content
        //

        if (billingActor == Inmate) {
            inmateBalanceView.setVisibility(View.VISIBLE);
        } else {
            inmateBalanceView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void loadDisplay(BillingFilterRequest context) {
        filterRequest = context;

        //
        // Configure Drop Down
        //

        ArrayAdapter<String> dateOperatorAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.filter_operators));
        dateOperatorSpinner.setAdapter(dateOperatorAdapter);
        dateOperatorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                dateButton.setVisibility(i == 3 ? View.GONE : View.VISIBLE);
                doubleDateView.setVisibility(i == 3 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        int spinnerIndex = 0;
        for (int index = 0; index < BillingFilterExecutive.FilterOperatorValues.length; index++) {
            if (BillingFilterExecutive.FilterOperatorValues[index] == context.getDateOption()) {
                spinnerIndex = index;
                break;
            }
        }
        dateOperatorSpinner.setSelection(spinnerIndex);
        dateButton.setText(context.getDate());
        dateStartButton.setText(context.getDateStart());
        dateEndButton.setText(context.getDateEnd());

        int typeArrayResource = R.array.invoice_type_filters;
        if (filterRequest.getBillingType() == Payment) {
            typeArrayResource = R.array.payment_type_filters;
        }
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(typeArrayResource));
        typeSpinner.setAdapter(typeAdapter);
        spinnerIndex = 0;
        Integer typeValues[] = filterRequest.getBillingType() == Payment ? BillingFilterExecutive.PaymentTypeFilterValues : BillingFilterExecutive.InvoiceTypeFilterValues;
        for (int index = 0; index < typeValues.length; index++) {
            if (String.valueOf(typeValues[index]).equals(context.getType())) {
                spinnerIndex = index;
                break;
            }
        }
        typeSpinner.setSelection(spinnerIndex);

        ArrayAdapter<String> amountOperatorAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.filter_operators));
        amountOperatorSpinner.setAdapter(amountOperatorAdapter);
        amountOperatorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                amountEditText.setVisibility(i == 3 ? View.GONE : View.VISIBLE);
                doubleAmountView.setVisibility(i == 3 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        spinnerIndex = 0;
        for (int index = 0; index < BillingFilterExecutive.FilterOperatorValues.length; index++) {
            if (BillingFilterExecutive.FilterOperatorValues[index] == context.getAmountOption()) {
                spinnerIndex = index;
                break;
            }
        }
        amountOperatorSpinner.setSelection(spinnerIndex);
        amountEditText.setText(context.getAmount());
        amountStartEditText.setText(context.getAmountStart());
        amountEndEditText.setText(context.getAmountEnd());

        ArrayAdapter<String> balanceOperatorAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.filter_operators));
        balanceOperatorSpinner.setAdapter(balanceOperatorAdapter);
        balanceOperatorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                balanceEditText.setVisibility(i == 3 ? View.GONE : View.VISIBLE);
                doubleBalanceView.setVisibility(i == 3 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        spinnerIndex = 0;
        for (int index = 0; index < BillingFilterExecutive.FilterOperatorValues.length; index++) {
            if (BillingFilterExecutive.FilterOperatorValues[index] == context.getInmateBalanceOption()) {
                spinnerIndex = index;
                break;
            }
        }
        balanceOperatorSpinner.setSelection(spinnerIndex);
        balanceEditText.setText(context.getInmateBalance());
        balanceStartEditText.setText(context.getInmateBalanceStart());
        balanceEndEditText.setText(context.getInmateBalanceEnd());

        //
        // Set labels
        //

        dateTitleTextView.setText(context.getBillingType().toString() + " Date:");
        dateButton.setVisibility(dateOperatorSpinner.getSelectedItemPosition() == 3 ? View.GONE : View.VISIBLE);
        doubleDateView.setVisibility(dateOperatorSpinner.getSelectedItemPosition() == 3 ? View.VISIBLE : View.GONE);
        typeTitleTextView.setText(context.getBillingType().toString() + " Type:");
        amountTitleTextView.setText(context.getBillingType().toString() + " Amount:");
        amountEditText.setVisibility(amountOperatorSpinner.getSelectedItemPosition() == 3 ? View.GONE : View.VISIBLE);
        doubleAmountView.setVisibility(amountOperatorSpinner.getSelectedItemPosition() == 3 ? View.VISIBLE : View.GONE);
        balanceEditText.setVisibility(amountOperatorSpinner.getSelectedItemPosition() == 3 ? View.GONE : View.VISIBLE);
        doubleBalanceView.setVisibility(amountOperatorSpinner.getSelectedItemPosition() == 3 ? View.VISIBLE : View.GONE);
    }

    @Override
    public BillingFilterRequest getFilterRequest() {
        BillingFilterRequest request = new BillingFilterRequest();
        request.setDate(dateButton.getText().toString());
        request.setDateStart(dateStartButton.getText().toString());
        request.setDateEnd(dateEndButton.getText().toString());
        request.setDateOption(BillingFilterExecutive.FilterOperatorValues[dateOperatorSpinner.getSelectedItemPosition()]);
        int paymentTypeIndex = typeSpinner.getSelectedItemPosition();
        if (paymentTypeIndex == 0) {
            request.setType("");
        } else {
            if (filterRequest.getBillingType() == Invoice) {
                request.setType(String.valueOf(BillingFilterExecutive.InvoiceTypeFilterValues[paymentTypeIndex]));
            } else {
                request.setType(String.valueOf(BillingFilterExecutive.PaymentTypeFilterValues[paymentTypeIndex]));
            }
        }
        request.setAmount(amountEditText.getText().toString());
        request.setAmountStart(amountStartEditText.getText().toString());
        request.setAmountEnd(amountEndEditText.getText().toString());
        request.setAmountOption(BillingFilterExecutive.FilterOperatorValues[amountOperatorSpinner.getSelectedItemPosition()]);
        request.setInmateBalance(balanceEditText.getText().toString());
        request.setInmateBalanceStart(balanceStartEditText.getText().toString());
        request.setInmateBalanceEnd(balanceEndEditText.getText().toString());
        request.setInmateBalanceOption(BillingFilterExecutive.FilterOperatorValues[balanceOperatorSpinner.getSelectedItemPosition()]);
        request.setExpanded(filterRequest.isExpanded());
        return request;
    }

    public void setBillingExecutive(BillingExecutive billingExecutive) {
        this.billingExecutive = billingExecutive;
    }

    public void setBillingActor(BillingExecutive.BillingActor billingActor) {
        this.billingActor = billingActor;
    }
}
