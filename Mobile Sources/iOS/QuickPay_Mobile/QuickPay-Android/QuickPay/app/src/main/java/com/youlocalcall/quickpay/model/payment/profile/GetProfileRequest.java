package com.youlocalcall.quickpay.model.payment.profile;

import com.youlocalcall.quickpay.R;
import com.youlocalcall.quickpay.Utility.APIUtility;
import com.youlocalcall.quickpay.executive.CommonExecutive;
import com.youlocalcall.quickpay.model.CommonResponse;
import com.youlocalcall.quickpay.model.account.BaseRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Michael on 6/27/17.
 */

public class GetProfileRequest extends BaseRequest {

    public void post(final APIUtility.APIResponse<CommonResponse> apiResponse, final CommonExecutive executive) {
        paymentService.loadPaymentProfile().enqueue(new Callback<PaymentProfileResponse>() {
            public void onResponse(Call<PaymentProfileResponse> call, Response<PaymentProfileResponse> response) {
                if (!executive.validateResponse(response)) {
                    apiResponse.onResponse(null);
                    return;
                }
                apiResponse.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<PaymentProfileResponse> call, Throwable t) {
                executive.display.showError(R.string.request_failed);
                apiResponse.onResponse(null);
            }
        });
    }
}
