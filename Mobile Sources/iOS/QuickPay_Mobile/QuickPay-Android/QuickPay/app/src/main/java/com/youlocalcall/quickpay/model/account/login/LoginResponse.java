package com.youlocalcall.quickpay.model.account.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.youlocalcall.quickpay.model.CommonResponse;

/**
 * Created by Michael on 6/23/17.
 */

public class LoginResponse extends CommonResponse {

    @SerializedName("result")
    @Expose

    private LoginResponseResult result;

    public LoginResponseResult getResult() {
        return result;
    }

    public void setResult(LoginResponseResult result) {
        this.result = result;
    }
}
