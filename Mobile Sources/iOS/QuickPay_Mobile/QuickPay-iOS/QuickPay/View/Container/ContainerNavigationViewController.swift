//
//  ContainerNavigationViewController.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/30/17.
//
//

import UIKit

class ContainerNavigationViewController: UINavigationController, UINavigationControllerDelegate {

    var containerExecutive: ContainerExecutive?
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - UINavigation Controller Delegate
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController is BaseViewController {
            (viewController as! BaseViewController).containerExecutive = containerExecutive
        }
    }
    
    // MARK: - Helper Method
    
    func popToTopViewController() {
        if topViewController is QuickPayHomeViewController {
            return
        }
        
        for viewController in viewControllers {
            if viewController is QuickPayHomeViewController {
                popToViewController(viewController, animated: false)
                return
            }
        }
    }
}
