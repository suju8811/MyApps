//
//  ContainerViewController.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/30/17.
//
//

import UIKit

class ContainerViewController:
    UIViewController,
    ContainerDisplay {

    private static let EmbeddedNavigationSegueID = "EmbeddedNagivation"
    private static let LeftSideMenuNavigationSegueID = "LeftSideNavigationSegue"
    private static let GotoAccountSegueID = "gotoAccountSegue"
    private static let GotoCreditCardSegueID = "gotoCreditCardSegue"
    public static let GotoBillingInvoicesSegueID = "gotoBillingInvoicesSegue"
    public static let GotoBillingPaymentsSegueID = "gotoBillingPaymentsSegue"
    
    var containerNavigationViewController: ContainerNavigationViewController?
    let executive = ContainerExecutive()
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Actions
    
    func gotoViewController(_ item: SideMenuItemIndex) {
        if item != .Billing {
            containerNavigationViewController?.popToTopViewController()
        }
        switch item {
        case .Account:
            containerNavigationViewController?.topViewController?.performSegue(withIdentifier: ContainerViewController.GotoAccountSegueID, sender: nil)
            break
        case .CreditCard:
            containerNavigationViewController?.topViewController?.performSegue(withIdentifier: ContainerViewController.GotoCreditCardSegueID, sender: nil)
            break
        case .Invoices:
            containerNavigationViewController?.topViewController?.performSegue(withIdentifier: ContainerViewController.GotoBillingInvoicesSegueID, sender: nil)
            break
        case .Payments:
            containerNavigationViewController?.topViewController?.performSegue(withIdentifier: ContainerViewController.GotoBillingPaymentsSegueID, sender: nil)
            break
        case .Logout:
            AppData.sharedInstance.user = nil
            navigationController?.popViewController(animated: false)
            break
        default:
            break
        }
    }
    
    // MARK: - Segue Preparation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        executive.display = self
        if segue.identifier == ContainerViewController.EmbeddedNavigationSegueID {
            containerNavigationViewController = segue.destination as? ContainerNavigationViewController
            containerNavigationViewController?.containerExecutive = executive
        }
        else if segue.identifier == ContainerViewController.LeftSideMenuNavigationSegueID {
            (segue.destination as! LeftSideMenuContainerViewController).containerExecutive = executive
        }
    }
    
    // MARK: - Container Display Delegate
    
    func showLeftSideMenu() {
        performSegue(withIdentifier: ContainerViewController.LeftSideMenuNavigationSegueID, sender: nil)
    }
}
