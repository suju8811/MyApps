//
//  SplashViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 5/9/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

private let GotoAccountSegueID = "GotoAccount"
private let GotoHomeSegueID = "GotoHome"

class SplashViewController: UIViewController {
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        perform(#selector(SplashViewController.gotoAccount), with: nil, afterDelay: 2.0)
    }
    
    // MARK: - Helper
    
    func gotoAccount() {
        performSegue(withIdentifier: GotoAccountSegueID, sender: nil)
    }
}
