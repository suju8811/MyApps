//
//  InmatesTableViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 5/12/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

private let HeaderCellReuseIdentifier = "InmatesTableHeaderCell"
private let CellReuseIdentifier = "InmatesTableCell"

class InmatesTableViewController: UITableViewController {
    
    var executive: PaymentHomeExecutive?
    var selectedInmate: Inmate?
    
    var inmates: [Inmate] = [Inmate].init() {
        didSet {
            tableView.reloadData()
            if !inmates.isEmpty {
                let indexPath = IndexPath(row: 1, section: 0)
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .top)
                selectedInmate = inmates.first
            }
        }
    }
    
    // MARK: - UIView Lifecyle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = InmatesTableViewStyle.EstimatedHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        executive?.adjustTableHeight()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if inmates.isEmpty {
            return 0
        }
        return inmates.count + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: HeaderCellReuseIdentifier, for: indexPath)
            (cell as! InmatesTableViewHeaderCell).setContent()
        }
        else {
            cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier, for: indexPath)
            let inmate = inmates[indexPath.row - 1]
            (cell as! InmatesTableViewCell).setContent(inmate)
        }
        return cell
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row != 0) {
            selectedInmate = inmates[indexPath.row - 1];
        }
    }
}
