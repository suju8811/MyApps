//
//  QuickPayHomeViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 5/12/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit
import DLRadioButton
import DropDown

class QuickPayHomeViewController: InputBaseViewController, PaymentHomeDisplay, PayPalPaymentDelegate {

    private static let EnmatesTableEmbeddedSegue = "InmatesTableEmbedded"
    
    @IBOutlet var searchButtonsTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var firstNameTextField: TextFieldRegular16!
    @IBOutlet weak var lastNameTextField: TextFieldRegular16!
    @IBOutlet weak var bopNumberTextField: TextFieldRegular16!
    @IBOutlet weak var addInmateButton: ButtonRegular16Rounded!
    @IBOutlet weak var searchButtonsView: UIView!
    @IBOutlet weak var inmatesView: ViewCommonContent!
    @IBOutlet var inmateTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalAmountTextField: TextFieldRegular16!
    @IBOutlet weak var payViaCreditCardBeforeButton: DLRadioButton!
    @IBOutlet weak var payViaCreditCardButton: DLRadioButton!
    @IBOutlet weak var payViaPayPalButton: DLRadioButton!
    @IBOutlet var topCreditCardToBeforeConstraint: NSLayoutConstraint!
    @IBOutlet var topCreditCardToSuperConstraint: NSLayoutConstraint!
    @IBOutlet var creditCardView: UIView!
    @IBOutlet var creditCardTopConstraint: NSLayoutConstraint!
    @IBOutlet var useEmailCheckButton: ButtonRoundedGrayBorder!
    @IBOutlet var useAddressCheckButton: ButtonRoundedGrayBorder!
    @IBOutlet var emailTextField: TextFieldRegular16!
    @IBOutlet var phoneNumberTextField: TextFieldRegular16!
    @IBOutlet var cardHolderTextField: TextFieldRegular16!
    @IBOutlet weak var cardNumberTextField: TextFieldRegular16!
    @IBOutlet var cvcTextField: TextFieldRegular16!
    @IBOutlet var expirationTextField: TextFieldRegular16!
    @IBOutlet weak var countryLabel: LabelRoundedBorderRegular16!
    @IBOutlet weak var countryDropDownButton: UIButton!
    @IBOutlet var billingAddressTextField: TextFieldRegular16!
    @IBOutlet var cityTextField: TextFieldRegular16!
    @IBOutlet weak var stateLabel: LabelRoundedBorderRegular16!
    @IBOutlet weak var stateDropDownButton: UIButton!
    @IBOutlet var zipcodeTextField: TextFieldRegular16!
    @IBOutlet var bottomScrollViewConstraint: NSLayoutConstraint!
    
    var executive: PaymentHomeExecutive?
    var inmatesTableViewController: InmatesTableViewController?
    var paymentMethod: PaymentMethod = .CreditCard
    var environment:String = PayPalEnvironmentSandbox {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    var payPalConfig = PayPalConfiguration()
    var controlsPlaced = false
    let countryDropDown = DropDown()
    let stateDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.countryDropDown,
            self.stateDropDown
        ]
    }()
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        // Configure Paypal
        //
        
        var productionClientID = ""
        var sandboxClientID = ""
        let currentUser = AppData.sharedInstance.user
        guard let paypalInfo = currentUser?.paypalInfo, let clientID = paypalInfo.clientId, let paypalMode = paypalInfo.mode else {
            return
        }
        if paypalMode == .Production {
            productionClientID = clientID
        }
        else {
            sandboxClientID = clientID
        }
        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: productionClientID,
                                                                PayPalEnvironmentSandbox: sandboxClientID])
        payPalConfig.acceptCreditCards = false
        payPalConfig.merchantName = "YLC, Inc."
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        payPalConfig.payPalShippingAddressOption = .payPal;
        AppData.sharedInstance.payPalConfig = payPalConfig
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        hideKeyboardWhenTappedAround()
        bottomConstraint = bottomScrollViewConstraint
        
        //
        // Load Display
        //
        
        executive?.displayDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let currentUser = AppData.sharedInstance.user
        guard let paypalInfo = currentUser?.paypalInfo, let paypalMode = paypalInfo.mode else {
            return
        }
        if paypalMode == .Production {
            environment = PayPalEnvironmentProduction
        }
        else {
            environment = PayPalEnvironmentSandbox
        }
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if !controlsPlaced {
            showAddButton(false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        controlsPlaced = true
    }
    
    // MAKR: - Segue Preparation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == QuickPayHomeViewController.EnmatesTableEmbeddedSegue {
            inmatesTableViewController = segue.destination as? InmatesTableViewController
            executive = PaymentHomeExecutive.init(self)
            inmatesTableViewController?.executive = executive
        }
        if segue.identifier == ContainerViewController.GotoBillingInvoicesSegueID {
            let billingViewController = segue.destination as? BillingViewController
            let customerDisplayContext = BillingDisplayContext()
            customerDisplayContext.actor = .Customer
            customerDisplayContext.filterContext.type = .Invoice
            billingViewController?.customerDisplayContext = customerDisplayContext
            let inmateDisplayContext = BillingDisplayContext()
            inmateDisplayContext.actor = .Inmate
            inmateDisplayContext.filterContext.type = .Invoice
            billingViewController?.inmateDisplayContext = inmateDisplayContext
        }
        if segue.identifier == ContainerViewController.GotoBillingPaymentsSegueID {
            let billingViewController = segue.destination as? BillingViewController
            let customerDisplayContext = BillingDisplayContext()
            customerDisplayContext.actor = .Customer
            customerDisplayContext.filterContext.type = .Payment
            billingViewController?.customerDisplayContext = customerDisplayContext
            let inmateDisplayContext = BillingDisplayContext()
            inmateDisplayContext.actor = .Inmate
            inmateDisplayContext.filterContext.type = .Payment
            billingViewController?.inmateDisplayContext = inmateDisplayContext
        }
    }
    
    // MARK: - Actions
    
    @IBAction func didPressSideMenuButton(_ sender: Any) {
        containerExecutive?.didPressSideMenu(.QuickPay)
    }
    
    @IBAction func didPressSearchButton(_ sender: Any) {
        executive?.didPressSearchButton()
    }
    
    @IBAction func didPressAddInmateButton(_ sender: Any) {
        executive?.didPressAddInmateButton()
    }
    
    @IBAction func didPressPayViaCreditBefore(_ sender: Any) {
        executive?.didSelectPaymentMethod(.CreditCardBefore)
    }
    
    @IBAction func didPressPayViaCreditButton(_ sender: Any) {
        executive?.didSelectPaymentMethod(.CreditCard)
    }
    
    @IBAction func didPressPayViaPayPalButton(_ sender: Any) {
        executive?.didSelectPaymentMethod(.PayPal)
    }
    
    @IBAction func didPressUsePersonEmail(_ sender: Any) {
        executive?.didSelectPersonEmail(!useEmailCheckButton.isSelected)
    }
    
    @IBAction func didPressUsePersonAddress(_ sender: Any) {
        executive?.didSelectPersonAddress(!useAddressCheckButton.isSelected)
    }
    
    @IBAction func didPressCountryDropDownButton(_ sender: Any) {
        countryDropDown.show()
    }
    
    @IBAction func didPressStateDropDownButton(_ sender: Any) {
        stateDropDown.show()
    }
    
    @IBAction func didPressSubmitButton(_ sender: Any) {
        executive?.didPressSubmitButton()
    }
    
    // MARK: - Payment Home Display Delegate
    
    func loadDisplay(_ context: QuickPayDisplayContext) {
        
        //
        // Load default value
        //
        
        if let paymentProfileID = context.paymentProfile.paymentProfileID, !paymentProfileID.isEmpty {
            topCreditCardToSuperConstraint.isActive = false
            topCreditCardToBeforeConstraint.isActive = true
            payViaCreditCardBeforeButton.isHidden = false
            payViaCreditCardBeforeButton.isSelected = true
            if let cardNumber = context.paymentProfile.cardNumber {
                payViaCreditCardBeforeButton.setTitle("Pay via Credit Card \(cardNumber)", for: .normal)
            }
            selectPaymentMethod(.CreditCardBefore)
        }
        else {
            topCreditCardToSuperConstraint.isActive = true
            topCreditCardToBeforeConstraint.isActive = false
            payViaCreditCardBeforeButton.isHidden = true
            payViaCreditCardButton.isSelected = true
            selectPaymentMethod(.CreditCard)
        }
        emailTextField.text = context.paymentProfile.email
        emailTextField.isEnabled = !context.paymentProfile.usePersonEmail
        useEmailCheckButton.isSelected = context.paymentProfile.usePersonEmail
        phoneNumberTextField.text = context.paymentProfile.phoneNumber
        cardHolderTextField.text = context.paymentProfile.cardHolderName
        cardNumberTextField.text = context.paymentProfile.cardNumber
        cvcTextField.text = context.paymentProfile.cvc
        expirationTextField.text = context.paymentProfile.expiration
        useAddressCheckButton.isSelected = context.paymentProfile.usePersonAddress
        if let selectedCountry = context.countryState.selectedCountry {
            countryLabel.text = AppData.sharedInstance.countries[selectedCountry].name
        }
        countryDropDownButton.isEnabled = !context.paymentProfile.usePersonAddress
        billingAddressTextField.text = context.paymentProfile.personAddress.billingAddress
        billingAddressTextField.isEnabled = !context.paymentProfile.usePersonAddress
        cityTextField.text = context.paymentProfile.personAddress.city
        cityTextField.isEnabled = !context.paymentProfile.usePersonAddress
        if let selectedState = context.countryState.selectedState {
            stateLabel.text = context.countryState.states[selectedState].name
        }
        stateDropDownButton.isEnabled = !context.paymentProfile.usePersonAddress
        zipcodeTextField.text = context.paymentProfile.personAddress.zipCode
        
        //
        // Configure Drop Down
        //
        
        countryDropDown.anchorView = countryDropDownButton
        countryDropDown.bottomOffset = CGPoint(x: 0, y: countryDropDownButton.bounds.height)
        countryDropDown.dataSource = AppData.sharedInstance.countries.map({$0.name ?? ""})
        countryDropDown.selectionAction = { [unowned self] (index, item) in
            self.countryLabel.text = item
            self.executive?.didSelectCountry(index)
        }
        countryDropDown.selectRow(at: context.countryState.selectedCountry)
        
        stateDropDown.anchorView = stateDropDownButton
        stateDropDown.bottomOffset = CGPoint(x: 0, y: stateDropDownButton.bounds.height)
        reloadStateDropDown(context)
        
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
    }
    
    func reloadStateDropDown(_ context: QuickPayDisplayContext) {
        stateDropDown.dataSource = context.countryState.states.map({$0.name ?? ""})
        stateDropDown.selectionAction = { [unowned self] (index, item) in
            self.stateLabel.text = item
        }
        stateDropDown.selectRow(at: context.countryState.selectedState)
        stateLabel.text = ""
        if let stateIndex = context.countryState.selectedState {
            stateLabel.text = context.countryState.states[stateIndex].name
        }
    }
    
    func getInmatesSearchRequest() -> Inmate {
        let inmate = Inmate()
        inmate.firstName = firstNameTextField.text
        inmate.lastName = lastNameTextField.text
        inmate.BOPNumber = bopNumberTextField.text
        return inmate
    }
    
    func getPaymentRequest() -> PaymentRequest {
        var paymentRequest: PaymentRequest
        let currentUser = AppData.sharedInstance.user
        if paymentMethod == .PayPal {
            paymentRequest = PaymentRequest()
        }
        else {
            paymentRequest = CreditCardPaymentRequest()
        }
        if let inmatedId = currentUser?.inmateId {
            paymentRequest.inmateId = inmatedId
        }
        else {
            paymentRequest.inmateId = inmatesTableViewController?.selectedInmate?.id
        }
        if let totalAmountText = totalAmountTextField.text {
            paymentRequest.totalAmount = totalAmountText
        }
        if let cardHolder = cardHolderTextField.text {
            paymentRequest.cardHolder = cardHolder
        }
        if let cardNumber = cardNumberTextField.text {
            paymentRequest.cardNumber = cardNumber
        }
        if let cvc = cvcTextField.text {
            paymentRequest.cvc = cvc
        }
        if let expiration = expirationTextField.text {
            paymentRequest.expiration = expiration
        }
        if paymentMethod != .PayPal {
            if let email = emailTextField.text {
                (paymentRequest as! CreditCardPaymentRequest).email = email
            }
            if let phoneNumber = phoneNumberTextField.text {
                (paymentRequest as! CreditCardPaymentRequest).phoneNumber = phoneNumber
            }
            (paymentRequest as! CreditCardPaymentRequest).usePersonEmail = useEmailCheckButton.isSelected
            if let country = countryLabel.text {
                (paymentRequest as! CreditCardPaymentRequest).country = country
            }
            if let billingAddress = billingAddressTextField.text {
                (paymentRequest as! CreditCardPaymentRequest).billingAdress = billingAddress
            }
            (paymentRequest as! CreditCardPaymentRequest).usePersonAddress = useAddressCheckButton.isSelected
            if let city = cityTextField.text {
                (paymentRequest as! CreditCardPaymentRequest).city = city
            }
            if let state = stateLabel.text {
                (paymentRequest as! CreditCardPaymentRequest).state = state
            }
            if let zipCode = zipcodeTextField.text {
                (paymentRequest as! CreditCardPaymentRequest).zipCode = zipCode
            }
            if paymentMethod == .CreditCard {
                (paymentRequest as! CreditCardPaymentRequest).isUpdate = true
            }
        }
        return paymentRequest
    }
    
    func refreshSearchResult(_ inmates: [Inmate]) {
        inmatesTableViewController?.inmates = inmates
    }
    
    func adjustTableHeight() {
        if let tableView = inmatesTableViewController?.tableView {
            tableViewHeightConstraint.constant = tableView.contentSize.height
        }
    }
    
    func selectPaymentMethod(_ paymentMethod: PaymentMethod) {
        self.paymentMethod = paymentMethod
        if paymentMethod == .CreditCard {
            creditCardView.isHidden = false
            creditCardTopConstraint.constant = 0
        }
        else {
            creditCardView.isHidden = true
            creditCardTopConstraint.constant = -creditCardView.bounds.height
        }
    }
    
    func usePersonEmail(_ isUse: Bool) {
        useEmailCheckButton.isSelected = isUse
        emailTextField.isEnabled = !isUse
        if isUse {
            emailTextField.text = AppData.sharedInstance.user?.commonInfo.email
        }
    }
    
    func usePersonAddress(_ isUse: Bool) {
        useAddressCheckButton.isSelected = isUse
        let currentUser = AppData.sharedInstance.user
        countryLabel.isEnabled = !isUse
        if isUse {
            countryLabel.text = currentUser?.personAddress.country.name
            var index: Index?
            if let countryName = currentUser?.personAddress.country.name {
                index = countryDropDown.dataSource.index(of: countryName)
            }
            countryDropDown.selectRow(at: index)
        }
        billingAddressTextField.isEnabled = !isUse
        if isUse {
            billingAddressTextField.text = currentUser?.personAddress.billingAddress
        }
        cityTextField.isEnabled = !isUse
        if isUse {
            cityTextField.text = currentUser?.personAddress.city
        }
        stateLabel.isEnabled = !isUse
        if isUse {
            stateLabel.text = currentUser?.personAddress.state.name
            var index: Index?
            if let stateName = currentUser?.personAddress.state.name {
                index = stateDropDown.dataSource.index(of: stateName)
            }
            stateDropDown.selectRow(at: index)
        }
        zipcodeTextField.isEnabled = !isUse
        if isUse {
            zipcodeTextField.text = currentUser?.personAddress.zipCode
        }
        executive?.didSelectCountry(countryDropDown.indexForSelectedRow)
    }
    
    func showPaypalViewController(_ payment: PayPalPayment) {
        let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
        present(paymentViewController!, animated: true, completion: nil)
    }
    
    func showAddButton(_ isShow: Bool) {
        addInmateButton.isHidden = !isShow
        if isShow {
            inmateTrailingConstraint.constant = MainControlSpacing
        }
        else {
            inmateTrailingConstraint.constant = -addInmateButton.bounds.width
        }
    }
    
    func enableSearchView(_ isEnable: Bool, inmate: Inmate) {
        var constraintConstant: CGFloat
        firstNameTextField.isEnabled = isEnable
        lastNameTextField.isEnabled = isEnable
        bopNumberTextField.isEnabled = isEnable
        searchButtonsView.isHidden = !isEnable
        inmatesView.isHidden = !isEnable
        if isEnable {
            constraintConstant = MainControlSpacing
        }
        else {
            constraintConstant = -searchButtonsView.bounds.height - inmatesView.bounds.height - MainControlSpacing
            firstNameTextField.text = inmate.firstName
            lastNameTextField.text = inmate.lastName
            bopNumberTextField.text = inmate.BOPNumber
        }
        searchButtonsTopConstraint.constant = constraintConstant
    }
    
    // MARK: - PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            self.executive?.proceedPaypalConfirm(completedPayment.confirmation as NSDictionary)
        })
    }
}
