//
//  InmatesTableViewCell.swift
//  QuickPay
//
//  Created by Michael Lee on 5/12/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

class InmatesTableViewHeaderCell: UITableViewCell {
    
    @IBOutlet weak var firstNameLabel: LabelBold14!
    @IBOutlet weak var lastNameLabel: LabelBold14!
    @IBOutlet weak var bopNumberLabel: LabelBold14!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setContent() {
        firstNameLabel.text = "First Name"
        lastNameLabel.text = "Last Name"
        bopNumberLabel.text = "BOP Number"
    }
}

class InmatesTableViewCell: UITableViewCell {

    @IBOutlet weak var firstNameLabel: LabelRegular14!
    @IBOutlet weak var lastNameLabel: LabelRegular14!
    @IBOutlet weak var bopNumberLabel: LabelRegular14!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setContent(_ inmate: Inmate) {
        firstNameLabel.text = inmate.firstName
        lastNameLabel.text = inmate.lastName
        bopNumberLabel.text = inmate.BOPNumber
    }
}
