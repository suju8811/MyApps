//
//  LeftSideMenuTableViewCell.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/23/17.
//
//

import UIKit

class LeftSideMenuTableViewCell: UITableViewCell {

    public var item: LeftSideMenuItem?
    
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var itemLabel: LabelRegular16!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setDisplay(_ item: LeftSideMenuItem, selectedItemIndex: SideMenuItemIndex) {
        self.item = item
        
        if item.index == selectedItemIndex {
            itemLabel.textColor = QuickPayBlue
            itemImageView.image = item.selectImage
        }
        else {
            itemLabel.textColor = QuickPayGray4
            itemImageView.image = item.normalImage
        }
        itemLabel.text = item.title
    }
}
