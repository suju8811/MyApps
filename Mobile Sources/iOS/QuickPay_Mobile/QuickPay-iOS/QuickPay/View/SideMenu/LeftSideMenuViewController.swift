//
//  LeftSideMenuViewController.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/22/17.
//
//

import UIKit
import SideMenu

class LeftSideMenuViewController:
    UIViewController,
    LeftSideMenuDisplay {
    
    private static let TableViewItemSegue = "EmbededTableViewItemSegue"
    
    var leftSideMenuTableViewController: LeftSideMenuTableViewController?
    
    var executive = LeftSideMenuExecutive()
    var containerExecutive: ContainerExecutive!
    
    @IBOutlet weak var userNameLabel: LabelRegular20!
    @IBOutlet weak var emailLabel: LabelRegular14!
    
    // MARK: - UIView Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        executive.display = self
        leftSideMenuTableViewController?.prepareForDisplay(self, executive: executive)
        executive.displayDidLoad()
    }
    
    // MARK: - Display
    
    func reloadItems(_ items: [LeftSideMenuItem]!) {
        let currentUser = AppData.sharedInstance.user
        userNameLabel.text = currentUser?.commonInfo.name
        if let email = currentUser?.commonInfo.email {
            emailLabel.text = email
        }
        else {
            emailLabel.text = currentUser?.commonInfo.phone
        }
        leftSideMenuTableViewController?.leftSideMenuItems = items
    }
    
    func selectedSideMenuItem(_ item: LeftSideMenuItem) {
        containerExecutive.gotoViewController(item.index)
        if item.index != .Billing {
            dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Segue Preparation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == LeftSideMenuViewController.TableViewItemSegue {
            leftSideMenuTableViewController = segue.destination as? LeftSideMenuTableViewController
        }
    }
}
