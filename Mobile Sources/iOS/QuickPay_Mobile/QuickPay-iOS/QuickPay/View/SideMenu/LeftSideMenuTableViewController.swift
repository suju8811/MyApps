//
//  LeftSideMenuTableViewController.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/23/17.
//
//

import UIKit

private let itemReuseIdentifier = "LeftSideMenuItemCell"
private let separatorReuseIdentifier = "LeftSideMenuSeparatorCell"

class LeftSideMenuTableViewController: UITableViewController {

    var leftSideMenuExecutive: LeftSideMenuExecutive!
    var leftSideMenuDisplay: LeftSideMenuDisplay!
    var leftSideMenuItems: [LeftSideMenuItem]? {
        didSet {
            tableView.reloadData()
            let indexPath = IndexPath(row: 0, section: 0)
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .bottom)
        }
    }
    var selectedItemIndex = SideMenuItemIndex.QuickPay
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearsSelectionOnViewWillAppear = false
        tableView.estimatedRowHeight = LeftSideMenuStyle.EstimatedHeight
    }
    
    // MARK: - Prepare for display and executive
    
    func prepareForDisplay(_ display: LeftSideMenuDisplay, executive: LeftSideMenuExecutive) {
        leftSideMenuDisplay = display
        leftSideMenuExecutive = executive
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if leftSideMenuItems == nil {
            return 0
        }
        return leftSideMenuItems!.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell

        let item = leftSideMenuItems?[indexPath.row]
        if item?.type == .Item {
            cell = tableView.dequeueReusableCell(withIdentifier: itemReuseIdentifier, for: indexPath)
            (cell as! LeftSideMenuTableViewCell).setDisplay(item!, selectedItemIndex: selectedItemIndex)
        }
        else {
            cell = tableView.dequeueReusableCell(withIdentifier: separatorReuseIdentifier, for: indexPath)
        }

        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = leftSideMenuItems?[indexPath.row]
        leftSideMenuExecutive.selectedSideMenuItem(item!)
    }
}
