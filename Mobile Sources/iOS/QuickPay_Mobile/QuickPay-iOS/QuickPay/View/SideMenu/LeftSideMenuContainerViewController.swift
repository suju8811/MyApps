//
//  ContainerViewController.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/22/17.
//
//

import UIKit
import SideMenu

class LeftSideMenuContainerViewController: UISideMenuNavigationController, UINavigationControllerDelegate {
    
    var containerExecutive: ContainerExecutive?
    
    var appScreenRect: CGRect {
        let appWindowRect = UIApplication.shared.keyWindow?.bounds ?? UIWindow().bounds
        return appWindowRect
    }
    
    override func awakeFromNib() {
        SideMenuManager.menuPresentMode = .viewSlideInOut
        SideMenuManager.menuFadeStatusBar = true
        SideMenuManager.menuAnimationBackgroundColor = UIColor.clear
        SideMenuManager.menuAnimationPresentDuration = 0.45
        SideMenuManager.menuAnimationDismissDuration = 0.45
        SideMenuManager.menuWidth = appScreenRect.width * 0.65
        delegate = self
        
        super.awakeFromNib()
    }
    
    // MARK: - UINavigationController Delegate
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController is LeftSideMenuViewController {
            (viewController as! LeftSideMenuViewController).containerExecutive = containerExecutive
            (viewController as! LeftSideMenuViewController).leftSideMenuTableViewController?.selectedItemIndex = (containerExecutive?.selectedSideMenuIndex)!
        }
    }
}
