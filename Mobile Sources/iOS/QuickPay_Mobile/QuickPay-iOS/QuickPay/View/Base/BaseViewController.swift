//
//  BaseViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 5/20/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var containerExecutive: ContainerExecutive?
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Common Display Delegate
    
    func showLoading(_ isShow: Bool) {
        if isShow {
            KAlert.showLoading(view)
        }
        else {
            KAlert.hideLoading(view)
        }
    }
    
    func showError(_ message: String) {
        present(KAlert.notifyAlert(withTitle: kAppName, message: message, buttonTitle: OKString), animated: true, completion: nil)
    }
    
    func showSideMenu(_ sideMenuItemIndex: SideMenuItemIndex) {
        containerExecutive?.didPressSideMenu(sideMenuItemIndex)
    }
}
