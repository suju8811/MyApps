//
//  InputBaseViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 5/21/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

class InputBaseViewController: BaseViewController {

    weak var bottomConstraint: NSLayoutConstraint?
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(InputBaseViewController.resizeForKeyboard(_:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(InputBaseViewController.resizeForKeyboard(_:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // MARK: - UIKeyboardNotification
    
    func resizeForKeyboard(_ notification: NSNotification) {
        let up = notification.name == Notification.Name.UIKeyboardWillShow
        let userInfo = notification.userInfo
        let animationDuration = userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        let keyboardEndFrame = userInfo?[UIKeyboardFrameEndUserInfoKey] as! CGRect
        if (up) {
            bottomConstraint?.constant = keyboardEndFrame.height
            UIView.animate(withDuration: animationDuration, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (finished) in
            })
        }
        else {
            bottomConstraint?.constant = 0
            UIView.animate(withDuration: animationDuration, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (finished) in
            })
        }
    }
}
