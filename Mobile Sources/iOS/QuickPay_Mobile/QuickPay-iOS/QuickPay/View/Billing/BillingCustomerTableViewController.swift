//
//  BillingCustomerTableViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 6/8/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

class BillingCustomerTableViewController: UITableViewController {

    private static let cellReuseIdentifier = "BillingCustomerTableViewCell"
    var displayContext = BillingDisplayContext()
    var billingExecutive: BillingExecutive?
    var billingResponseArray = BillingResponseArray() {
        didSet {
            tableView.reloadData()
        }
    }
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = CGFloat(144.0)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return billingResponseArray.data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BillingCustomerTableViewController.cellReuseIdentifier, for: indexPath) as! BillingCustomerTableViewCell
        let billingResponse = billingResponseArray.data[indexPath.row]
        cell.setContent(billingResponse, exectuive: billingExecutive, type: displayContext.filterContext.type)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = .zero
        }
        
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))  {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins))  {
            cell.layoutMargins = .zero
        }
    }
}
