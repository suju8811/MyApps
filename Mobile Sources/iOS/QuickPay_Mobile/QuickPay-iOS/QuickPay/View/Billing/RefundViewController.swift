//
//  RefundViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 6/16/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

fileprivate let RefundCellReuseIdentifier = "RefundCell"
fileprivate let TotalRefundCellReuseIdentifier = "RefundTotalCell"

class RefundViewController: BaseViewController, RefundDisplay {

    private static let UnwindToBillingSegue = "UnwindToBillingSegue"
    var getRefundListRequest = RefundRequest()
    var getRefundListResponse = GetRefundListResponse()
    var executive: RefundExecutive?
    
    @IBOutlet var unavailableRefundLabel: LabelRegular18!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var transactionLabel: LabelRegular14!
    @IBOutlet var buttonView: UIView!
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = CGFloat(144.0)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        executive = RefundExecutive(self)
        executive?.displayDidLoad(getRefundListRequest)
    }
    
    // MARK: - Actions
    
    @IBAction func didPressBackButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didPressRefundButton(_ sender: Any) {
        executive?.didPressRefundButton(getRefundListRequest)
    }
    
    // MARK: - Refund Display Delegate
    
    func loadRefundList(_ refundListResponse: GetRefundListResponse) {
        getRefundListResponse = refundListResponse
        transactionLabel.text = refundListResponse.transaction
        if let isSettle = refundListResponse.isSettle, isSettle == 0 {
            buttonView.isHidden = true
            unavailableRefundLabel.text = RefundExecutive.UnavailableRefund
        }
        else {
            buttonView.isHidden = false
            unavailableRefundLabel.text = ""
        }
        
        tableView.reloadData()
    }
    
    func showResultMessage(_ message: String) {
        present(KAlert.alert(withTitle: kAppName, message: message, leftButton: OKString, rightButton: nil, leftBlock: { (action) in
            self.performSegue(withIdentifier: RefundViewController.UnwindToBillingSegue, sender: nil)
        }, rightBlock: nil), animated: true, completion: nil)
    }
}

extension RefundViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getRefundListResponse.payments.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        let totalCount = getRefundListResponse.payments.count
        if indexPath.row == totalCount {
            cell = tableView.dequeueReusableCell(withIdentifier: TotalRefundCellReuseIdentifier, for: indexPath) as! RefundTotalCell
            (cell as! RefundTotalCell).setContent(getRefundListResponse)
        }
        else {
            let payment = getRefundListResponse.payments[indexPath.row]
            cell = tableView.dequeueReusableCell(withIdentifier: RefundCellReuseIdentifier, for: indexPath) as! RefundCell
            (cell as! RefundCell).setContent(payment)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = .zero
        }
        
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))  {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins))  {
            cell.layoutMargins = .zero
        }
    }
}
