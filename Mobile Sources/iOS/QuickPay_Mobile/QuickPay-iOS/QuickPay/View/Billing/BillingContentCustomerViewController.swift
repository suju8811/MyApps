//
//  BillingContentCustomerViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 6/8/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

fileprivate let customerDataColumns = ["payment_date", "payment_type", "payment_amount", "transaction_id", "name_on_card", "last_4_digits", "description", "additional"]

class BillingContentCustomerViewController: UIViewController, UISearchBarDelegate {
    
    private static let BllingContentFilterEmbeddedSegue = "BillingContentFilterEmbeddedSegue"
    private static let BillingCustomerTableEmbeddedSegue = "BillingCustomerTableEmbeddedSegue"
    
    var displayContext = BillingDisplayContext()
    var filterViewController: BillingContentFilterViewController?
    var billingCustomerTableViewController: BillingCustomerTableViewController?
    var billingExecutive: BillingExecutive? {
        didSet {
            filterViewController?.billingExecutive = billingExecutive
        }
    }
    @IBOutlet var searchBar: UISearchBar!
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    // MARK: - UISearchBar Delegate
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        billingExecutive?.loadBilling()
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        billingExecutive?.loadBilling()
        view.endEditing(true)
    }
    
    // MARK: - Segue Preparation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == BillingContentCustomerViewController.BllingContentFilterEmbeddedSegue {
            filterViewController = segue.destination as? BillingContentFilterViewController
            filterViewController?.displayContext.actor = .Customer
            filterViewController?.displayContext.filterContext.type = displayContext.filterContext.type
        }
        if segue.identifier == BillingContentCustomerViewController.BillingCustomerTableEmbeddedSegue {
            billingCustomerTableViewController = segue.destination as? BillingCustomerTableViewController
            billingCustomerTableViewController?.displayContext = displayContext
        }
    }
    
    func getBillingRequest() -> BillingRequest {
        let billingRequest = BillingRequest()
        for dataColumn in customerDataColumns {
            let column = BillingCommonRequest()
            column.data = dataColumn
            billingRequest.columns.append(column)
        }
        billingRequest.search.value = searchBar.text ?? ""
        if let filterRequest = filterViewController?.getFilterRequest() {
            billingRequest.filterRequest = filterRequest
        }
        billingRequest.billingType = displayContext.filterContext.type
        billingRequest.billingActor = .Customer
        return billingRequest
    }
}
