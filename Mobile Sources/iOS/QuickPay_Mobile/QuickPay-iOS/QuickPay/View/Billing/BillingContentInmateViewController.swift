//
//  BillingContentInmateViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 6/8/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

fileprivate let inmateDataColumns = ["payment_date", "payment_type", "payment_amount", "transaction_id", "name_on_card", "last_4_digits", "description", "additional"]

class BillingContentInmateViewController: UIViewController, UISearchBarDelegate {
    
    private static let BllingContentFilterEmbeddedSegue = "BillingContentFilterEmbeddedSegue"
    private static let BillingInmateTableEmbeddedSegue = "BillingInmateTableEmbeddedSegue"
    
    var displayContext = BillingDisplayContext()
    var filterViewController: BillingContentFilterViewController?
    var billingExecutive: BillingExecutive? {
        didSet {
            filterViewController?.billingExecutive = billingExecutive
        }
    }
    var billingInmateTableViewController: BillingInmateTableViewController?
    @IBOutlet var searchBar: UISearchBar!
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    // MARK: - UISearchBar Delegate
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        billingExecutive?.loadBilling()
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        billingExecutive?.loadBilling()
        view.endEditing(true)
    }
    
    // MARK: - Segue Preparation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == BillingContentInmateViewController.BllingContentFilterEmbeddedSegue {
            filterViewController = segue.destination as? BillingContentFilterViewController
            filterViewController?.displayContext.actor = .Inmate
            filterViewController?.displayContext.filterContext.type = displayContext.filterContext.type
        }
        if segue.identifier == BillingContentInmateViewController.BillingInmateTableEmbeddedSegue {
            billingInmateTableViewController = segue.destination as? BillingInmateTableViewController
            billingInmateTableViewController?.displayContext = displayContext
        }
    }
    
    func getBillingRequest() -> BillingRequest {
        let billingRequest = BillingRequest()
        for dataColumn in inmateDataColumns {
            let column = BillingCommonRequest()
            column.data = dataColumn
            billingRequest.columns.append(column)
        }
        billingRequest.search.value = searchBar.text ?? ""
        if let filterRequest = filterViewController?.getFilterRequest() {
            billingRequest.filterRequest = filterRequest
        }
        billingRequest.billingType = displayContext.filterContext.type
        billingRequest.billingActor = .Inmate
        return billingRequest
    }
}
