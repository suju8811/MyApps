//
//  RefundCell.swift
//  QuickPay
//
//  Created by Michael Lee on 6/16/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

class RefundCell: UITableViewCell {

    @IBOutlet var descriptionLabel: LabelRegular12!
    @IBOutlet var priceLabel: LabelRegular12!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setContent(_ getRefundResponse: GetRefundResponse) {
        descriptionLabel.text = getRefundResponse.description
        if let paymentAmount = getRefundResponse.paymentAmount {
            priceLabel.text = "$ \(paymentAmount)"
        }
    }
}

class RefundTotalCell: UITableViewCell {
    
    @IBOutlet var amountLabel: LabelRegular13!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setContent(_ getRefundListResponse: GetRefundListResponse) {
        amountLabel.text = getRefundListResponse.price
    }
}
