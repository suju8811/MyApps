//
//  BillingInmateTableViewCell.swift
//  QuickPay
//
//  Created by Michael Lee on 6/15/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

class BillingInmateTableViewCell: UITableViewCell {

    @IBOutlet var dateView: UIView!
    @IBOutlet var dateLabel: LabelBold16!
    @IBOutlet var typeLabel: LabelBold18!
    @IBOutlet var cardNumberLabel: LabelRegular18!
    @IBOutlet var amountLabel: LabelRegular18!
    @IBOutlet var inmateNameLabel: LabelRegular18!
    @IBOutlet var descriptionLabel: LabelRegular18!
    @IBOutlet var refundLabel: LabelBold18!
    @IBOutlet var refundView: UIView!
    
    var billingData: BillingResponse?
    var billingExecutive: BillingExecutive?
    var billingType: BillingType?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapPrepayLabel = UITapGestureRecognizer(target: self, action: #selector(BillingCustomerTableViewCell.didTapPrepayLabel(_:)))
        refundView.isUserInteractionEnabled = true
        refundView.addGestureRecognizer(tapPrepayLabel)
        dateView.layer.borderWidth = 1.0
        dateView.layer.borderColor = QuickPayGreen.cgColor
    }
    
    func setContent(_ data: BillingResponse, exectuive: BillingExecutive?, type: BillingType) {
        billingData = data
        billingExecutive = exectuive
        dateLabel.text = data.paymentDate
        typeLabel.text = data.paymentType
        amountLabel.text = data.paymentAmount
        inmateNameLabel.text = data.inmateName
        descriptionLabel.text = data.description
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        refreshConstraints()
    }
    
    func refreshConstraints() {
        guard let billingType = billingType else {
            return
        }
        if (billingType == .Payment) {
            refundLabel.text = "  $  "
        }
        else {
            refundLabel.text = ""
        }
    }
    
    func didTapPrepayLabel(_ sender: UIGestureRecognizer) {
        billingExecutive?.gotoRefund(billingData?.id)
    }
}
