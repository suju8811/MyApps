//
//  BillingContentFilterViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 6/8/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit
import DropDown

class BillingContentFilterViewController: BaseViewController, BillingFilterDisplay {

    private static let arrowUpImageName = "ic_arrow_up"
    private static let arrowDownImageName = "ic_arrow_bottom"
    
    var displayContext = BillingDisplayContext()
    var executive: BillingFilterExecutive?
    var billingExecutive: BillingExecutive?
    
    @IBOutlet var headerBottomConstraint: NSLayoutConstraint!
    @IBOutlet var contentBottomConstraint: NSLayoutConstraint!
    @IBOutlet var headerArrowImageView: UIImageView!
    @IBOutlet var inmateBallanceView: UIView!
    @IBOutlet var amountBottomToButtonConstraint: NSLayoutConstraint!
    @IBOutlet var amountBottomToBalanceConstraint: NSLayoutConstraint!
    @IBOutlet var contentView: UIView!
    @IBOutlet var dateTitleLabel: LabelRegular12!
    @IBOutlet var dateOperatorLabel: LabelRoundedBorderRegular12!
    @IBOutlet var dateDropDownButton: UIButton!
    @IBOutlet var singleDateView: UIView!
    @IBOutlet var dateLabel: LabelRoundedBorderRegular12!
    @IBOutlet var doubleDateView: UIView!
    @IBOutlet var dateFromLabel: LabelRoundedBorderRegular12!
    @IBOutlet var dateToLabel: LabelRoundedBorderRegular12!
    @IBOutlet var typeTitleLabel: LabelRegular12!
    @IBOutlet var typeDropDownButton: UIButton!
    @IBOutlet var typeLabel: LabelRoundedBorderRegular12!
    @IBOutlet var amountTitleLabel: LabelRegular12!
    @IBOutlet var amountDropDownButton: UIButton!
    @IBOutlet var amountOperatorLabel: LabelRoundedBorderRegular12!
    @IBOutlet var singleAmountView: UIView!
    @IBOutlet var amountTextField: TextFieldRegular12!
    @IBOutlet var doubleAmountView: UIView!
    @IBOutlet var amountFromTextField: TextFieldRegular12!
    @IBOutlet var amountToTextField: TextFieldRegular12!
    @IBOutlet var balanceTitleLabel: LabelRegular12!
    @IBOutlet var balanceDropDownButton: UIButton!
    @IBOutlet var balanceOperatorLabel: LabelRoundedBorderRegular12!
    @IBOutlet var singleBalanceView: UIView!
    @IBOutlet var balanceTextField: TextFieldRegular12!
    @IBOutlet var doubleBalanceView: UIView!
    @IBOutlet var balanceFromTextField: TextFieldRegular12!
    @IBOutlet var balanceToTextField: TextFieldRegular12!
    
    let dateOperatorDropDown = DropDown()
    let typeDropDown = DropDown()
    let amountOperatorDropDown = DropDown()
    let balanceOperatorDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.dateOperatorDropDown,
            self.typeDropDown,
            self.amountOperatorDropDown,
            self.balanceOperatorDropDown
        ]
    }()
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.translatesAutoresizingMaskIntoConstraints = false
        executive = BillingFilterExecutive(self)
        executive?.displayDidLoad(displayContext.filterContext)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        executive?.refreshLayout(displayContext.filterContext)
    }
    
    // MARK: - Actions
    
    @IBAction func didPressViewButton(_ sender: Any) {
        executive?.didPressViewButton()
        billingExecutive?.loadBilling()
    }
    
    @IBAction func didPressDateOperatorButton(_ sender: Any) {
        dateOperatorDropDown.show()
    }
    
    @IBAction func didPressDateValueButton(_ sender: Any) {
        billingExecutive?.didPressDateButton(.Current)
    }
    
    @IBAction func didPressFromDateButton(_ sender: Any) {
        billingExecutive?.didPressDateButton(.From)
    }
    
    @IBAction func didPressToDateButton(_ sender: Any) {
        billingExecutive?.didPressDateButton(.To)
    }
    
    @IBAction func didPressTypeButton(_ sender: Any) {
        typeDropDown.show()
    }
    
    @IBAction func didPressAmountOperatorButton(_ sender: Any) {
        amountOperatorDropDown.show()
    }
    
    @IBAction func didPressBalanceOperatorButton(_ sender: Any) {
        balanceOperatorDropDown.show()
    }
    
    @IBAction func didPressExpandButton(_ sender: Any) {
        executive?.didPressExpandButton()
    }
    
    // MARK: - Billing Filter Display Delegate
    
    func refreshLayout(_ context: BillingFilterDisplayContext) {
        displayContext.filterContext = context
        
        //
        // Layout Header
        //
        
        if displayContext.filterContext.expanded {
            headerBottomConstraint.isActive = false
            contentBottomConstraint.isActive = true
            headerArrowImageView.image = UIImage.init(named: BillingContentFilterViewController.arrowUpImageName)
        }
        else {
            headerBottomConstraint.isActive = true
            contentBottomConstraint.isActive = false
            headerArrowImageView.image = UIImage.init(named: BillingContentFilterViewController.arrowDownImageName)
        }
        
        //
        // Layout Content
        //
        
        if displayContext.actor == .Inmate {
            inmateBallanceView.isHidden = false
            amountBottomToButtonConstraint.isActive = false
            amountBottomToBalanceConstraint.isActive = true
        }
        else {
            inmateBallanceView.isHidden = true
            amountBottomToButtonConstraint.isActive = true
            amountBottomToBalanceConstraint.isActive = false
        }
    }
    
    func loadDisplay(_ context: BillingFilterDisplayContext) {
        displayContext.filterContext = context
        
        //
        // Configure Drop Down
        //
        
        dateOperatorDropDown.anchorView = dateDropDownButton
        dateOperatorDropDown.bottomOffset = CGPoint(x: 0, y: dateDropDownButton.bounds.height)
        dateOperatorDropDown.dataSource = BillingFilterExecutive.FilterOperators.map({$0.rawValue})
        dateOperatorDropDown.selectionAction = { [unowned self] (index, item) in
            self.dateOperatorLabel.text = item
            self.singleDateView.isHidden = item == OperatorType.Between.rawValue
            self.doubleDateView.isHidden = item != OperatorType.Between.rawValue
        }
        dateOperatorDropDown.selectRow(at: context.dateOperatorIndex)
        
        typeDropDown.anchorView = typeDropDownButton
        typeDropDown.bottomOffset = CGPoint(x: 0, y: typeDropDownButton.bounds.height)
        if displayContext.filterContext.type == .Payment {
            typeDropDown.dataSource = BillingFilterExecutive.PaymentTypeFilters.map({$0.rawValue})
        }
        else {
            typeDropDown.dataSource = BillingFilterExecutive.InvoiceTypeFilters.map({$0.rawValue})
        }
        typeDropDown.selectionAction = { [unowned self] (index, item) in
            self.typeLabel.text = item
        }
        typeDropDown.selectRow(at: context.typeIndex)
        
        amountOperatorDropDown.anchorView = amountDropDownButton
        amountOperatorDropDown.bottomOffset = CGPoint(x: 0, y: amountDropDownButton.bounds.height)
        amountOperatorDropDown.dataSource = BillingFilterExecutive.FilterOperators.map({$0.rawValue})
        amountOperatorDropDown.selectionAction = { [unowned self] (index, item) in
            self.amountOperatorLabel.text = item
            self.singleAmountView.isHidden = item == OperatorType.Between.rawValue
            self.doubleAmountView.isHidden = item != OperatorType.Between.rawValue
        }
        amountOperatorDropDown.selectRow(at: context.amountOperatorIndex)
        
        balanceOperatorDropDown.anchorView = balanceDropDownButton
        balanceOperatorDropDown.bottomOffset = CGPoint(x: 0, y: balanceDropDownButton.bounds.height)
        balanceOperatorDropDown.dataSource = BillingFilterExecutive.FilterOperators.map({$0.rawValue})
        balanceOperatorDropDown.selectionAction = { [unowned self] (index, item) in
            self.balanceOperatorLabel.text = item
            self.singleBalanceView.isHidden = item == OperatorType.Between.rawValue
            self.doubleBalanceView.isHidden = item != OperatorType.Between.rawValue
        }
        balanceOperatorDropDown.selectRow(at: context.balanceOperatorIndex)
        
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        
        //
        // Set labels
        //
        
        let dateOperator = BillingFilterExecutive.FilterOperators[context.dateOperatorIndex]
        dateTitleLabel.text = context.type.rawValue + " Date:"
        dateOperatorLabel.text = dateOperator.rawValue
        dateLabel.text = context.dateFormatted
        dateFromLabel.text = context.dateFromFormatted
        dateToLabel.text = context.dateToFormatted
        singleDateView.isHidden = dateOperator == .Between
        doubleDateView.isHidden = dateOperator != .Between
        typeTitleLabel.text = context.type.rawValue + " Type:"
        if displayContext.filterContext.type == .Payment {
            typeLabel.text = BillingFilterExecutive.PaymentTypeFilters[context.typeIndex].rawValue
        }
        else {
            typeLabel.text = BillingFilterExecutive.InvoiceTypeFilters[context.typeIndex].rawValue
        }
        let amountOperator = BillingFilterExecutive.FilterOperators[context.amountOperatorIndex]
        amountTitleLabel.text = context.type.rawValue + " Amount:"
        amountOperatorLabel.text = amountOperator.rawValue
        amountTextField.text = context.amountValue
        amountFromTextField.text = context.amountFromValue
        amountToTextField.text = context.amountToValue
        singleAmountView.isHidden = amountOperator == .Between
        doubleAmountView.isHidden = amountOperator != .Between
        let balanceOperator = BillingFilterExecutive.FilterOperators[context.balanceOperatorIndex]
        balanceOperatorLabel.text = balanceOperator.rawValue
        balanceTextField.text = context.balanceValue
        balanceFromTextField.text = context.balanceFromValue
        balanceToTextField.text = context.balanceToValue
        singleBalanceView.isHidden = balanceOperator == .Between
        doubleBalanceView.isHidden = balanceOperator != .Between
    }
    
    func getFilterRequest() -> BillingFilterRequest {
        let filterRequest = BillingFilterRequest()
        filterRequest.date = dateLabel.text ?? ""
        filterRequest.dateStart = dateFromLabel.text ?? ""
        filterRequest.dateEnd = dateToLabel.text ?? ""
        filterRequest.dateOption = BillingFilterExecutive.FilterOperatorValues[dateOperatorDropDown.indexForSelectedRow ?? 0]
        if let paymentType = typeDropDown.indexForSelectedRow,
            paymentType != 0 {
            if displayContext.filterContext.type == .Invoice {
                filterRequest.type = "\(BillingFilterExecutive.InvoiceTypeFilterValues[paymentType])"
            }
            else {
                filterRequest.type = "\(BillingFilterExecutive.PaymentTypeFilterValues[paymentType])"
            }
        }
        else {
            filterRequest.type = ""
        }
        filterRequest.amount = amountTextField.text ?? ""
        filterRequest.amountStart = amountFromTextField.text ?? ""
        filterRequest.amountEnd = amountToTextField.text ?? ""
        filterRequest.amountOption = BillingFilterExecutive.FilterOperatorValues[amountOperatorDropDown.indexForSelectedRow ?? 0]
        filterRequest.inmateBalance = balanceTextField.text ?? ""
        filterRequest.inmateBalanceStart = balanceFromTextField.text ?? ""
        filterRequest.inmateBalanceEnd = balanceToTextField.text ?? ""
        filterRequest.inmateBalanceOption = BillingFilterExecutive.FilterOperatorValues[balanceOperatorDropDown.indexForSelectedRow ?? 0]
        return filterRequest
    }
    
    func getFilterDisplayContext() -> BillingFilterDisplayContext {
        let filterDisplayContext = BillingFilterDisplayContext()
        filterDisplayContext.type = displayContext.filterContext.type
        filterDisplayContext.dateOperatorIndex = dateOperatorDropDown.indexForSelectedRow!
        filterDisplayContext.dateValue = displayContext.filterContext.dateValue
        filterDisplayContext.dateFormatted = displayContext.filterContext.dateFormatted
        filterDisplayContext.dateFromValue = displayContext.filterContext.dateFromValue
        filterDisplayContext.dateFromFormatted = displayContext.filterContext.dateFromFormatted
        filterDisplayContext.dateToValue = displayContext.filterContext.dateToValue
        filterDisplayContext.dateToFormatted = displayContext.filterContext.dateToFormatted
        filterDisplayContext.typeIndex = typeDropDown.indexForSelectedRow!
        filterDisplayContext.amountOperatorIndex = amountOperatorDropDown.indexForSelectedRow!
        filterDisplayContext.amountValue = amountTextField.text!
        filterDisplayContext.amountFromValue = amountFromTextField.text!
        filterDisplayContext.amountToValue = amountToTextField.text!
        filterDisplayContext.balanceOperatorIndex = balanceOperatorDropDown.indexForSelectedRow!
        filterDisplayContext.balanceValue = balanceTextField.text!
        filterDisplayContext.balanceFromValue = balanceFromTextField.text!
        filterDisplayContext.balanceToValue = balanceToTextField.text!
        filterDisplayContext.expanded = displayContext.filterContext.expanded
        return filterDisplayContext
    }
}
