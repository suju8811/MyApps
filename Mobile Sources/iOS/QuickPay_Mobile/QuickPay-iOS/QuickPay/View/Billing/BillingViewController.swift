//
//  BillingViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 6/4/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

class BillingViewController: InputBaseViewController, BillingDisplay {
    
    private static let BllingContentCustomerEmbeddedSegue = "BillingConentCustomerEmbeddedSegeu"
    private static let BllingContentInmateEmbeddedSegue = "BillingConentInmateEmbeddedSegeu"
    private static let GotoRefundSegue = "GotoRefundSegue"
    
    @IBOutlet weak var titleLabel: LabelRegular32!
    @IBOutlet weak var subTitleLabel: LabelRegular16!
    @IBOutlet weak var customerButton: ButtonRegular14Tab!
    @IBOutlet weak var inmateButton: ButtonRegular14Tab!
    @IBOutlet weak var customerContainerView: UIView!
    @IBOutlet weak var inmateContainerView: UIView!
    
    var selectedActor: BillingActor = .Customer
    var customerDisplayContext = BillingDisplayContext()
    var inmateDisplayContext = BillingDisplayContext()
    var executive: BillingExecutive?
    
    var customerViewController: BillingContentCustomerViewController?
    var inmateViewController: BillingContentInmateViewController?
    var commonDatePickerViewController: CommonDatePickerViewController?
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        commonDatePickerViewController = UIStoryboard(name: CommonDatePickerStoryboardID, bundle: nil).instantiateInitialViewController() as? CommonDatePickerViewController
        executive = BillingExecutive(self)
        commonDatePickerViewController?.executive = executive
        executive?.displayDidLoad(customerDisplayContext, inmateContext: inmateDisplayContext)
        customerViewController?.billingExecutive = executive
        inmateViewController?.billingExecutive = executive
    }
    
    // MARK: - Segue Preparation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == BillingViewController.BllingContentCustomerEmbeddedSegue {
            customerViewController = segue.destination as? BillingContentCustomerViewController
            customerViewController?.displayContext = customerDisplayContext
        }
        if segue.identifier == BillingViewController.BllingContentInmateEmbeddedSegue {
            inmateViewController = segue.destination as? BillingContentInmateViewController
            inmateViewController?.displayContext = inmateDisplayContext
        }
        if segue.identifier == BillingViewController.GotoRefundSegue {
            let refundViewController = segue.destination as? RefundViewController
            refundViewController?.getRefundListRequest = sender as! RefundRequest
        }
    }
    // MARK: - Actions
    
    @IBAction func didPressSideMenuButton(_ sender: Any) {
        if customerDisplayContext.filterContext.type == .Invoice {
            containerExecutive?.didPressSideMenu(.Invoices)
        }
        else {
            containerExecutive?.didPressSideMenu(.Payments)
        }
    }
    
    @IBAction func didPressCustomerButton(_ sender: Any) {
        executive?.didPressCusomterButton()
    }
    
    @IBAction func didPressInmateButton(_ sender: Any) {
        executive?.didPressInmateButton()
    }
    
    @IBAction func prepareForUnwindToBilling(_ segue: UIStoryboardSegue) {
        executive?.loadBilling()
    }
    
    // MARK: - Billing Display Delegate
    
    func refreshDisplay(_ customerContext: BillingDisplayContext, inmateContext: BillingDisplayContext, selectedActor: BillingActor) {
        customerDisplayContext = customerContext
        inmateDisplayContext = inmateContext
        self.selectedActor = selectedActor
        
        //
        // Set title and sub title
        //
        
        titleLabel.text = customerDisplayContext.title
        subTitleLabel.text = customerDisplayContext.subtitle
        
        //
        // Set tab
        //
        
        customerButton.isSelected = selectedActor == .Customer
        inmateButton.isSelected = selectedActor == .Inmate
        
        //
        // Switch View
        //
        
        customerContainerView.isHidden = true
        inmateContainerView.isHidden = true
        if selectedActor == .Customer {
            customerContainerView.isHidden = false
        }
        else {
            inmateContainerView.isHidden = false
        }
        
        //
        // Filter view
        //
        
        customerViewController?.filterViewController?.loadDisplay(customerDisplayContext.filterContext)
        inmateViewController?.filterViewController?.loadDisplay(inmateDisplayContext.filterContext)
    }
    
    func showCalendar(_ selectedDate: Date, dateType: DateType) {
        commonDatePickerViewController?.currentDate = selectedDate
        commonDatePickerViewController?.dateType = dateType
        presentTransparentViewController(commonDatePickerViewController!, animated: true)
    }
    
    func getBillingRequest(_ actor: BillingActor?) -> BillingRequest {
        var currentActor: BillingActor
        if let actor = actor {
            currentActor = actor
        }
        else {
            currentActor = selectedActor
        }
        switch currentActor {
        case .Customer:
            if let customerBillingRequest = customerViewController?.getBillingRequest() {
                return customerBillingRequest
            }
            break
        case .Inmate:
            if let inmateBillingRequest = inmateViewController?.getBillingRequest() {
                return inmateBillingRequest
            }
            break
        }
        return BillingRequest()
    }
    
    func getCustomerFilterDisplayContext() -> BillingFilterDisplayContext {
        if let customerFilterDisplayContext = customerViewController?.filterViewController?.getFilterDisplayContext() {
            return customerFilterDisplayContext
        }
        return BillingFilterDisplayContext()
    }
    
    func getInmateFilterDisplayContext() -> BillingFilterDisplayContext {
        if let inmateFilterDisplayContext = inmateViewController?.filterViewController?.getFilterDisplayContext() {
            return inmateFilterDisplayContext
        }
        return BillingFilterDisplayContext()
    }
    
    func reloadBillingTable(_ actor: BillingActor?, billingResponseArray: BillingResponseArray) {
        var currentActor: BillingActor
        if let actor = actor {
            currentActor = actor
        }
        else {
            currentActor = selectedActor
        }
        switch currentActor {
        case .Customer:
            customerViewController?.billingCustomerTableViewController?.displayContext = customerDisplayContext
            customerViewController?.billingCustomerTableViewController?.billingExecutive = executive
            customerViewController?.billingCustomerTableViewController?.billingResponseArray = billingResponseArray
            break
        case .Inmate:
            inmateViewController?.billingInmateTableViewController?.displayContext = inmateDisplayContext
            inmateViewController?.billingInmateTableViewController?.billingExecutive = executive
            inmateViewController?.billingInmateTableViewController?.billingResponseArray = billingResponseArray
            break
        }
    }
    
    func gotoRefund(_ getRefundListRequest: RefundRequest) {
        performSegue(withIdentifier: BillingViewController.GotoRefundSegue, sender: getRefundListRequest)
    }
}
