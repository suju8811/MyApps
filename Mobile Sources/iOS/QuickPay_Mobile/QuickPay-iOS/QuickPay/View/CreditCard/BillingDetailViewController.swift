//
//  BillingDetailViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 6/3/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit
import DLRadioButton

class BillingDetailViewController: BaseViewController, BillingDetailDisplay {
    
    private static let CreditCardInfoEmbeddedSegue = "CreditCardInfoEmbeddedSegue"
    var creditCardInfoViewController: CreditCardInfoViewController?
    var parentExecutive: CommonExecutive?
    var paymentMethod: PaymentMethod = .CreditCard
    var executive: BillingDetailExecutive?
    
    @IBOutlet var topCreditCardToSuperConstraint:
    NSLayoutConstraint!
    @IBOutlet var topCreditCardToBeforeConstraint: NSLayoutConstraint!
    @IBOutlet var payViaCreditCardBeforeButton: DLRadioButton!
    @IBOutlet var payViaCreditCardButton: DLRadioButton!
    @IBOutlet var creditCardContainerView: UIView!
    @IBOutlet var creditCardTopConstraint: NSLayoutConstraint!
    
    // MAKR: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        executive = BillingDetailExecutive(self)
    }
    
    
    // MARK: - Prepare for segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == BillingDetailViewController.CreditCardInfoEmbeddedSegue {
            creditCardInfoViewController = segue.destination as? CreditCardInfoViewController
            creditCardInfoViewController?.view.translatesAutoresizingMaskIntoConstraints = false
            creditCardInfoViewController?.parentDisplay = parentExecutive?.display
        }
    }
    
    // MARK: - Actions
    
    @IBAction func didPressPayViaCreditBefore(_ sender: Any) {
        executive?.didSelectPaymentMethod(.CreditCardBefore)
    }
    
    @IBAction func didPressPayViaCreditButton(_ sender: Any) {
        executive?.didSelectPaymentMethod(.CreditCard)
    }
    
    @IBAction func didPressPayViaPayPalButton(_ sender: Any) {
        executive?.didSelectPaymentMethod(.PayPal)
    }
    
    // MARK: - Billing Detail Display Delegate
    
    func selectPaymentMethod(_ paymentMethod: PaymentMethod) {
        self.paymentMethod = paymentMethod
        if paymentMethod == .CreditCard {
            creditCardContainerView.isHidden = false
            creditCardTopConstraint.constant = 0
        }
        else {
            creditCardContainerView.isHidden = true
            creditCardTopConstraint.constant = -creditCardContainerView.bounds.height
        }
    }
    
    func loadLayout(_ context: PaymentProfile) {
        
        //
        // Load default value
        //
        
        print("Payment profile")
        if let paymentProfileID = context.paymentProfileID, !paymentProfileID.isEmpty {
            topCreditCardToSuperConstraint.isActive = false
            topCreditCardToBeforeConstraint.isActive = true
            payViaCreditCardBeforeButton.isHidden = false
            payViaCreditCardBeforeButton.isSelected = true
            if let cardNumber = context.cardNumber {
                payViaCreditCardBeforeButton.setTitle("Pay via Credit Card \(cardNumber)", for: .normal)
            }
            selectPaymentMethod(.CreditCardBefore)
        }
        else {
            topCreditCardToSuperConstraint.isActive = true
            topCreditCardToBeforeConstraint.isActive = false
            payViaCreditCardBeforeButton.isHidden = true
            payViaCreditCardButton.isSelected = true
            selectPaymentMethod(.CreditCard)
        }
    }
    
    func loadDisplay(_ context: PaymentProfile, personAddressDisplayContext: PersonAddressDisplayContext) {
        creditCardInfoViewController?.loadDisplay(context, personAddressDisplayContext: personAddressDisplayContext)
    }
    
    func getPaymentAmountRequest() -> PaymentAmountRequest {
        let request = PaymentAmountRequest()
        if let paymentProfile = creditCardInfoViewController?.getCreditCardUpdateRequest() {
            request.paymentProfile = paymentProfile
        }
        request.isPaypal = paymentMethod == .PayPal
        request.isUpdate = paymentMethod == .CreditCard
        return request
    }
}

