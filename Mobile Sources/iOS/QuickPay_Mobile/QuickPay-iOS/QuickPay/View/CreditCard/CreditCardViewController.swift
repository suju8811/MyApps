//
//  CreditCardViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 6/2/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

class CreditCardViewController: InputBaseViewController, CreditCardDisplay {
    
    private static let CreditCardEmbeddedSegue = "CreditCardEmbedded"
    
    @IBOutlet weak var autoPaymentSwitch: UISwitch!
    
    var creditCardInfoViewController: CreditCardInfoViewController?
    var executive: CreditCardExecutive?
    
    // MARK: - UIView Lifecycle
    
    @IBOutlet var scrollViewBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        bottomConstraint = scrollViewBottomConstraint
        executive = CreditCardExecutive(self)
        executive?.displayDidLoad()
    }

    // MARK: - Actions
    
    @IBAction func didPressSideMenuButton(_ sender: Any) {
        containerExecutive?.didPressSideMenu(.CreditCard)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == CreditCardViewController.CreditCardEmbeddedSegue {
            creditCardInfoViewController = segue.destination as? CreditCardInfoViewController
            creditCardInfoViewController?.view.translatesAutoresizingMaskIntoConstraints = false
            creditCardInfoViewController?.parentDisplay = self
        }
    }
    
    @IBAction func didPressResetButton(_ sender: Any) {
        executive?.didPressResetButton()
    }
    
    @IBAction func didPressUpdateButton(_ sender: Any) {
        executive?.didPressUpdateButton()
    }
    
    // MARK: - CreditCardDisplay Context
    
    func loadDisplay(_ context: PaymentProfile, personAddressDisplayContext: PersonAddressDisplayContext) {
        autoPaymentSwitch.setOn(context.autoPayment == "1", animated: false)
        creditCardInfoViewController?.loadDisplay(context, personAddressDisplayContext: personAddressDisplayContext)
    }
    
    func getCreditCardUpdateRequest() -> PaymentProfile? {
        let request = creditCardInfoViewController?.getCreditCardUpdateRequest()
        if autoPaymentSwitch.isOn {
            request?.autoPayment = "0"
        }
        else {
            request?.autoPayment = "1"
        }
        return request
    }
}
