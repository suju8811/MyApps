//
//  CreditCardInfoViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 6/3/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit
import DropDown

class CreditCardInfoViewController: BaseViewController, CreditCardInfoDisplay {
    
    private static let PersonAddressEmbeddedSegue = "PersonAddressEmbeddedSegue"
    
    @IBOutlet var useEmailCheckButton: ButtonRoundedGrayBorder!
    @IBOutlet var useAddressCheckButton: ButtonRoundedGrayBorder!
    @IBOutlet weak var emailTextField: TextFieldRegular16!
    @IBOutlet weak var phoneTextField: PhoneNumberFieldRegular16!
    @IBOutlet weak var cardHolderTextField: TextFieldRegular16!
    @IBOutlet weak var cardNumberTextField: TextFieldRegular16!
    @IBOutlet weak var cvcTextField: TextFieldRegular16!
    @IBOutlet weak var expirationTextField: TextFieldRegular16!

    var personAddressViewController: PersonAddressViewController?
    var executive: CreditCardInfoExecutive?
    var parentDisplay: CommonDisplay?
    
    let countryDropDown = DropDown()
    let stateDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.countryDropDown,
            self.stateDropDown
        ]
    }()
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let parentDisplay = parentDisplay {
            executive = CreditCardInfoExecutive(parentDisplay)
        }
        else {
            executive = CreditCardInfoExecutive(self)
        }
    }
    
    // MARK: - Segue Preparation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == CreditCardInfoViewController.PersonAddressEmbeddedSegue {
            personAddressViewController = segue.destination as? PersonAddressViewController
            AppData.sharedInstance.paymentProfile.personAddress.addressType = .OneAddress
            personAddressViewController?.displayContext.personAddress.addressType = .OneAddress
            if let parentDisplay = parentDisplay {
                personAddressViewController?.parentDisplay = parentDisplay
            }
            else {
                personAddressViewController?.parentDisplay = self
            }
        }
    }
    
    // MARK: - Actions
    
    @IBAction func didPressUsePersonEmail(_ sender: Any) {
        executive?.didSelectPersonEmail(!useEmailCheckButton.isSelected)
    }
    
    @IBAction func didPressUsePersonAddress(_ sender: Any) {
        executive?.didSelectPersonAddress(!useAddressCheckButton.isSelected)
    }
    
    // MARK: - Credit Card Info Display Delegate
    
    func loadDisplay(_ context: PaymentProfile, personAddressDisplayContext: PersonAddressDisplayContext) {
        emailTextField.text = context.email
        useEmailCheckButton.isSelected = context.usePersonEmail
        phoneTextField.text = context.phoneNumber
        cardHolderTextField.text = context.cardHolderName
        cardNumberTextField.text = context.cardNumber
        cvcTextField.text = context.cvc
        expirationTextField.text = context.expiration
        personAddressViewController?.loadDisplay(personAddressDisplayContext)
    }
    
    func usePersonEmail(_ isUse: Bool) {
        useEmailCheckButton.isSelected = isUse
        emailTextField.isEnabled = !isUse
        if isUse {
            emailTextField.text = AppData.sharedInstance.user?.commonInfo.email
        }
    }
    
    func usePersonAddress(_ isUse: Bool) {
        useAddressCheckButton.isSelected = isUse
        personAddressViewController?.usePersonAddress(isUse)
    }
    
    func getCreditCardUpdateRequest() -> PaymentProfile {
        let request = PaymentProfile()
        request.usePersonEmail = useEmailCheckButton.isSelected
        request.email = emailTextField.text
        request.phoneNumber = phoneTextField.text
        request.cardHolderName = cardHolderTextField.text
        request.cardNumber = cardNumberTextField.text
        request.cvc = cvcTextField.text
        request.expiration = expirationTextField.text
        request.usePersonAddress = useAddressCheckButton.isSelected
        if let personAddress = personAddressViewController?.getPersonAddressInfo() {
            request.personAddress = personAddress
        }
        return request
    }
}
