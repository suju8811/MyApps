//
//  PersonAddressViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 6/5/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit
import DropDown

class PersonAddressViewController: BaseViewController, PersonAddressDisplay {
    
    @IBOutlet weak var address1View: UIView!
    @IBOutlet weak var address2View: UIView!
    @IBOutlet weak var billingAddressView: UIView!
    @IBOutlet var billingAddressTopConstraint: NSLayoutConstraint!
    @IBOutlet var billingAddressOldTopConstraint: NSLayoutConstraint!
    @IBOutlet var cityOldTopConstraint: NSLayoutConstraint!
    @IBOutlet var cityTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var countryLabel: LabelRoundedBorderRegular16!
    @IBOutlet weak var countryDropDownButton: UIButton!
    @IBOutlet weak var address1TextField: TextFieldRegular16!
    @IBOutlet weak var address2TextField: TextFieldRegular16!
    @IBOutlet var billingAddressTextField: TextFieldRegular16!
    @IBOutlet var cityTextField: TextFieldRegular16!
    @IBOutlet weak var stateLabel: LabelRoundedBorderRegular16!
    @IBOutlet weak var stateDropDownButton: UIButton!
    @IBOutlet var zipcodeTextField: TextFieldRegular16!
    
    var displayContext = PersonAddressDisplayContext()
    var executive: PersonAddressExecutive?
    var parentDisplay: CommonDisplay?
    let countryDropDown = DropDown()
    let stateDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.countryDropDown,
            self.stateDropDown
        ]
    }()
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        if let parentDisplay = parentDisplay {
            executive = PersonAddressExecutive(parentDisplay)
        }
        else {
            executive = PersonAddressExecutive(self)
        }
        executive?.personAddressDisplay = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        executive?.refreshLayout(displayContext)
    }
    
    // MARK: - Actions
    
    @IBAction func didPressCountryDropDownButton(_ sender: Any) {
        countryDropDown.show()
    }
    
    @IBAction func didPressStateDropDownButton(_ sender: Any) {
        stateDropDown.show()
    }
    
    // MARK: - PersonAddressDisplay Delegate
    
    func refreshLayout(_ context: PersonAddressDisplayContext) {
        view.layoutIfNeeded()
        displayContext = context
        if displayContext.personAddress.addressType == .OneAddress {
            address1View.isHidden = true
            address2View.isHidden = true
            billingAddressView.isHidden = false
            billingAddressOldTopConstraint.isActive = false
            billingAddressTopConstraint.isActive = true
        }
        else {
            address1View.isHidden = false
            address2View.isHidden = false
            billingAddressView.isHidden = true
            cityOldTopConstraint.isActive = false
            cityTopConstraint.isActive = true
        }
    }
    
    func loadDisplay(_ context: PersonAddressDisplayContext) {
        displayContext = context
        
        //
        // Configure Drop Down
        //
        
        countryDropDown.anchorView = countryDropDownButton
        countryDropDown.bottomOffset = CGPoint(x: 0, y: countryDropDownButton.bounds.height)
        countryDropDown.dataSource = AppData.sharedInstance.countries.map({$0.name ?? ""})
        countryDropDown.selectionAction = { [unowned self] (index, item) in
            self.countryLabel.text = item
            self.executive?.didSelectCountry(index)
        }
        countryDropDown.selectRow(at: context.countryState.selectedCountry)
        
        stateDropDown.anchorView = stateDropDownButton
        stateDropDown.bottomOffset = CGPoint(x: 0, y: stateDropDownButton.bounds.height)
        refreshDisplay(context)
        
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
    }
    
    func refreshDisplay(_ context: PersonAddressDisplayContext) {
        displayContext = context
        
        //
        // Refresh Drop Down
        //
        
        countryDropDown.selectRow(at: context.countryState.selectedCountry)
        if let selectedCountry = context.countryState.selectedCountry {
            countryLabel.text = AppData.sharedInstance.countries[selectedCountry].name
        }
        else {
            countryLabel.text = nil
        }
        reloadStateDropDown(context)
        
        //
        // Refresh Values
        //
        
        address1TextField.text = context.personAddress.address1
        address2TextField.text = context.personAddress.address2
        billingAddressTextField.text = context.personAddress.billingAddress
        cityTextField.text = context.personAddress.city
        zipcodeTextField.text = context.personAddress.zipCode
    }
    
    func reloadStateDropDown(_ context: PersonAddressDisplayContext) {
        displayContext = context
        stateDropDown.dataSource = context.countryState.states.map({$0.name ?? ""})
        stateDropDown.selectionAction = { [unowned self] (index, item) in
            self.stateLabel.text = item
        }
        stateDropDown.selectRow(at: context.countryState.selectedState)
        stateLabel.text = ""
        if let stateIndex = context.countryState.selectedState {
            stateLabel.text = context.countryState.states[stateIndex].name
        }
    }
    
    func usePersonAddress(_ isUse: Bool) {
        let currentUser = AppData.sharedInstance.user
        countryDropDownButton.isEnabled = !isUse
        if isUse {
            countryLabel.text = currentUser?.personAddress.country.name
            var index: Index?
            if let countryName = currentUser?.personAddress.country.name {
                index = countryDropDown.dataSource.index(of: countryName)
            }
            countryDropDown.selectRow(at: index)
        }
        billingAddressTextField.isEnabled = !isUse
        if isUse {
            billingAddressTextField.text = currentUser?.personAddress.address1
        }
        cityTextField.isEnabled = !isUse
        if isUse {
            cityTextField.text = currentUser?.personAddress.city
        }
        stateDropDownButton.isEnabled = !isUse
        if isUse {
            stateLabel.text = currentUser?.personAddress.state.name
            var index: Index?
            if let stateName = currentUser?.personAddress.state.name {
                index = stateDropDown.dataSource.index(of: stateName)
            }
            stateDropDown.selectRow(at: index)
        }
        zipcodeTextField.isEnabled = !isUse
        if isUse {
            zipcodeTextField.text = currentUser?.personAddress.zipCode
        }
    }
    
    func getPersonAddressInfo() -> PersonAddress {
        let personAddress = PersonAddress()
        if let selectedCountryIndex = countryDropDown.indexForSelectedRow {
            personAddress.country = AppData.sharedInstance.countries[selectedCountryIndex]
        }
        personAddress.address1 = address1TextField.text
        personAddress.address2 = address2TextField.text
        personAddress.billingAddress = billingAddressTextField.text
        personAddress.city = cityTextField.text
        if let selectedStateIndex = stateDropDown.indexForSelectedRow {
            personAddress.state = displayContext.countryState.states[selectedStateIndex]
        }
        personAddress.zipCode = zipcodeTextField.text
        return personAddress
    }
}
