//
//  AddPrepayAmountViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 6/5/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

fileprivate let PrepayWarning = "Use QuickPay to add funds to Inmate.\nAre  you sure to proceed this payment?"

class AddPrepayAmountViewController: InputBaseViewController, AddPrepayAmountDisplay, PayPalPaymentDelegate {
    
    private static let BillingDetailEmbeddedSegueID = "BillingDetailEmbeddedSegue"
    
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var priceTextField: TextFieldRegular16!
    
    var billingDetailViewController: BillingDetailViewController?
    var executive: AddPrepayAmountExecutive?
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomConstraint = scrollViewBottomConstraint
        hideKeyboardWhenTappedAround()
        executive = AddPrepayAmountExecutive(self)
        executive?.displayDidLoad()
    }
    
    // MARK: - Segue Preparation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == AddPrepayAmountViewController.BillingDetailEmbeddedSegueID {
            segue.destination.view.translatesAutoresizingMaskIntoConstraints = false
            billingDetailViewController = segue.destination as? BillingDetailViewController
            billingDetailViewController?.parentExecutive = executive
        }
    }
    
    // MARK: - Actions
    
    @IBAction func didPressBackButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didPressSubmitButton(_ sender: Any) {
        executive?.didPressSubmitButton()
    }
    
    // MARK: - Add Prepay amount display context
    
    func loadLayout(_ context: PaymentProfile) {
        billingDetailViewController?.loadLayout(context)
    }
    
    func loadDisplay(_ context: PaymentProfile, personAddressDisplayContext: PersonAddressDisplayContext) {
        billingDetailViewController?.loadDisplay(context, personAddressDisplayContext: personAddressDisplayContext)
    }
    
    func showWarningMessage() {
        present(KAlert.alert(withTitle: kAppName, message: PrepayWarning, leftButton: CancelString, rightButton: OKString, leftBlock: nil, rightBlock: { (action) in
            self.executive?.proceedAddAmount()
        }), animated: true, completion: nil)
    }
    
    func getPaymentAmountRequest() -> PaymentAmountRequest {
        if let request = billingDetailViewController?.getPaymentAmountRequest() {
            request.totalPrice = priceTextField.text ?? ""
            return request
        }
        return PaymentAmountRequest()
    }
    
    func showPaypalViewController(_ payment: PayPalPayment) {
        let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: AppData.sharedInstance.payPalConfig, delegate: self)
        present(paymentViewController!, animated: true, completion: nil)
    }
    
    // MARK: - PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            self.executive?.proceedPaypalConfirm(completedPayment.confirmation as NSDictionary)
        })
    }
}
