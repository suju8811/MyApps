//
//  LoginViewController.swift
//  ReserveIn
//
//  Created by Michael Lee on 4/1/17.
//
//

import UIKit

fileprivate let GotoActivateSegueID = "GotoActivate"
fileprivate let GotoSignupSegeueID = "GotoSignup"
fileprivate let GotoMainSegeueID = "GotoMain"

class LoginViewController: InputBaseViewController, LoginDisplay {

    @IBOutlet weak var usernameTextField: TextFieldRegular16!
    @IBOutlet weak var passwordTextField: TextFieldRegular16!
    @IBOutlet weak var bottomScrollViewConstraint: NSLayoutConstraint!
    
    var executive: LoginExecutive?
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        hideKeyboardWhenTappedAround()
        executive = LoginExecutive(self)
        bottomConstraint = bottomScrollViewConstraint
    }
    
    // MARK: - Login Display Delegate
    
    func loggedIn() {
        usernameTextField.text = ""
        passwordTextField.text = ""
        performSegue(withIdentifier: GotoMainSegeueID, sender: nil)
    }
    
    func getLoginRequest() -> User {
        let user = User()
        user.commonInfo.name = usernameTextField.text!
        user.password = passwordTextField.text!
        return user
    }
    
    func showPasswordResetSuccess(_ message: String) {
        present(KAlert.alert(withTitle: kAppName, message: message, leftButton: "OK", rightButton: nil, leftBlock: { (action) in
            
        }, rightBlock: nil), animated: true, completion: nil)
    }
    
    // MARK: - Actions
    
    @IBAction func didForgotButtonPressed(_ sender: Any) {
        present(KAlert.inputAlert(withTitle: kAppName, message: "Please Input Username", leftButton: "Cancel", rightButton: "OK", placeholder: "Please input user name", leftBlock: { (action, textField) in
        }, rightBlock: { (action, textField) in
            self.executive?.forgotPassword(userName: textField.text!)
        }), animated: true)
    }
    
    @IBAction func didSigninButtonPressed(_ sender: Any) {
        executive?.login()
    }
    
    @IBAction func didSignupButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: GotoSignupSegeueID, sender: nil)
    }
    
    @IBAction func didActivateButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: GotoActivateSegueID, sender: nil)
    }
    
}
