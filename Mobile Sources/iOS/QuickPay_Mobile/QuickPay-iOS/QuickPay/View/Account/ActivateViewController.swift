//
//  ActivateViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 5/11/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

fileprivate let GotoMainSegeueID = "GotoMain"
fileprivate let VerificationPhone = "Phone"
fileprivate let VerificationEmail = "Email"
fileprivate let VerificationMethodSelect = "Which method will you use for account verification?"

class ActivateViewController: InputBaseViewController, ActivateDisplay {
    
    @IBOutlet weak var userNameTextField: TextFieldRegular16!
    @IBOutlet weak var passwordTextField: TextFieldRegular16!
    @IBOutlet weak var activationCodeTextField: TextFieldRegular16!
    @IBOutlet weak var bottomScrollViewConstraint: NSLayoutConstraint!
    
    var executive: ActivateExecutive?
    
    // MARK: - UIView Lifecyle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        bottomConstraint = bottomScrollViewConstraint
        executive = ActivateExecutive(self)
    }
    
    // MARK: - Activate Display Delegate
    
    func activated() {
        for viewController in navigationController!.viewControllers {
            if viewController is LoginViewController {
                navigationController?.popToViewController(viewController, animated: true)
            }
        }
    }
    
    func getActivateRequest() -> ActivateRequest {
        let userInfo = User()
        userInfo.commonInfo.name = userNameTextField.text!
        userInfo.password = passwordTextField.text!
        return ActivateRequest(userInfo: userInfo, activationCode: activationCodeTextField.text!)
    }
    
    func displayVerificationMethodSelectMessage(_ parameters: [String : Any]?) {
        present(KAlert.alert(withTitle: kAppName, message: VerificationMethodSelect, leftButton: VerificationPhone, rightButton: VerificationEmail, leftBlock: { (action) in
            self.executive?.sendResendActivationRequest(parameters, isPhone: true)
        }, rightBlock: { (action) in
            self.executive?.sendResendActivationRequest(parameters, isPhone: false)
        }), animated: true, completion: nil)
    }
    
    // MARK: - Actions
    
    @IBAction func didBackButtonPressed(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didActivateButtonPressed(_ sender: Any) {
        executive?.activate()
    }
    
    @IBAction func didPressResendActivationButtonPressed(_ sender: Any) {
        executive?.resendActivation()
    }
}
