//
//  AccountViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 6/2/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

fileprivate let QuickPayRecommendMessage = "Use QuickPay to add funds"

class AccountViewController: InputBaseViewController, AccountDisplay {
    
    private static let PersonAddressEmbeddedSegue = "PersonAddressEmbeddedSegue"
    private static let GotoAddPrepayAmountSegue = "GotoAddPrepayAmount"
    
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var userNameTextField: TextFieldRegular16!
    @IBOutlet weak var firstNameTextField: TextFieldRegular16!
    @IBOutlet weak var middleNameTextField: TextFieldRegular16!
    @IBOutlet weak var lastNameTextField: TextFieldRegular16!
    @IBOutlet weak var emailTextField: TextFieldRegular16!
    @IBOutlet weak var phoneNumberTextField: PhoneNumberFieldRegular16!
    @IBOutlet weak var oldPasswordTextField: TextFieldRegular16!
    @IBOutlet weak var newPasswordTextField: TextFieldRegular16!
    @IBOutlet weak var confirmPasswordTextField: TextFieldRegular16!
    
    var personAddressViewController: PersonAddressViewController?
    var executive: AccountExecutive?
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        bottomConstraint = scrollViewBottomConstraint
        executive = AccountExecutive(self)
        if let personAddressDisplayContext = personAddressViewController?.displayContext {
            executive?.displayDidLoad(personAddressDisplayContext)
        }
    }
    
    // MARK: - Segue Preparation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == AccountViewController.PersonAddressEmbeddedSegue {
            personAddressViewController = segue.destination as? PersonAddressViewController
            AppData.sharedInstance.user?.personAddress.addressType = .TwoAddress
            personAddressViewController?.displayContext.personAddress.addressType = .TwoAddress
            personAddressViewController?.parentDisplay = self
        }
    }
    
    // MARK: - Actions
    
    @IBAction func didPressSideMenuButton(_ sender: Any) {
        containerExecutive?.didPressSideMenu(.Account)
    }
    
    @IBAction func didPressAddPrepayAmount(_ sender: Any) {
        executive?.didPressAddPrepayAmountButton()
    }
    
    @IBAction func didPressResetButton(_ sender: Any) {
        executive?.didPressResetButton()
    }
    
    @IBAction func didPressUpdateProfileButton(_ sender: Any) {
        executive?.didPressUpdateProfileButton()
    }
    
    @IBAction func didPressResetPasswordButton(_ sender: Any) {
        executive?.didPressResetPasswordButton()
    }
    
    @IBAction func didPressUpdatePasswordButton(_ sender: Any) {
        executive?.didPressUpdatePasswordButton()
    }
    
    // MARK: - Account Display Delegate
    
    func loadDisplay(_ accountDisplayContext: AccountDisplayContext) {
        personAddressViewController?.loadDisplay(accountDisplayContext.personAddressDisplayContext)
        userNameTextField.text = accountDisplayContext.commonUserInfo.name
        firstNameTextField.text = accountDisplayContext.commonUserInfo.firstName
        middleNameTextField.text = accountDisplayContext.commonUserInfo.middleName
        lastNameTextField.text = accountDisplayContext.commonUserInfo.lastName
        emailTextField.text = accountDisplayContext.commonUserInfo.email
        phoneNumberTextField.text = accountDisplayContext.commonUserInfo.phone
    }
    
    func resetPassword() {
        oldPasswordTextField.text = ""
        newPasswordTextField.text = ""
        confirmPasswordTextField.text = ""
    }
    
    func getEditProfileRequest() -> EditProfileRequest {
        let commonUserInfo = CommonUserInfo()
        commonUserInfo.name = userNameTextField.text
        commonUserInfo.firstName = firstNameTextField.text
        commonUserInfo.middleName = middleNameTextField.text
        commonUserInfo.lastName = lastNameTextField.text
        commonUserInfo.email = emailTextField.text
        commonUserInfo.phone = phoneNumberTextField.text
        let personAddressInfo = personAddressViewController?.getPersonAddressInfo()
        let editProfileRequest = EditProfileRequest(commonInfo: commonUserInfo, addressInfo: personAddressInfo ?? PersonAddress())
        return editProfileRequest
    }
    
    func getChangePasswordRequest() -> ChangePasswordRequest {
        let oldPassword = oldPasswordTextField.text ?? ""
        let newPassword = newPasswordTextField.text ?? ""
        let confirmPassword = confirmPasswordTextField.text ?? ""
        return ChangePasswordRequest(oldPassword: oldPassword, newPassword: newPassword, confirmPassword: confirmPassword)
    }
    
    func displayQuickPayRecommendAlert() {
        present(KAlert.alert(withTitle: kAppName, message: QuickPayRecommendMessage, leftButton: OKString, rightButton: nil, leftBlock: { (action) in
            self.performSegue(withIdentifier: AccountViewController.GotoAddPrepayAmountSegue, sender: nil)
        }, rightBlock: nil), animated: true, completion: nil)
    }
}
