//
//  SignupViewController.swift
//  QuickPay
//
//  Created by Michael Lee on 5/11/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

fileprivate let GotoActivateID = "GotoActivate"
fileprivate let VerificationPhone = "Phone"
fileprivate let VerificationEmail = "Email"
fileprivate let VerificationMethodSelect = "Which method will you use for account verification?"

class SignupViewController: InputBaseViewController, SignupDisplay {

    @IBOutlet weak var userNameTextField: TextFieldRegular16!
    @IBOutlet weak var passwordTextField: TextFieldRegular16!
    @IBOutlet weak var confirmPasswordTextField: TextFieldRegular16!
    @IBOutlet weak var emailTextField: TextFieldRegular16!
    @IBOutlet weak var phoneNumberTextField: TextFieldRegular16!
    @IBOutlet weak var acceptTermsButton: ButtonRoundedGrayBorder!
    @IBOutlet weak var bottomScrollViewConstraint: NSLayoutConstraint!
    
    var executive: SignUpExecutive?
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        executive = SignUpExecutive(self)
        bottomConstraint = bottomScrollViewConstraint
    }
    
    // MARK: - Signup Display Delegate
    
    func signedup(_ message: String) {
        present(KAlert.alert(withTitle: kAppName, message: message, leftButton: "OK", rightButton: nil, leftBlock: { (action) in
            self.performSegue(withIdentifier: GotoActivateID, sender: nil)
        }, rightBlock: nil), animated: true, completion: nil)
    }
    
    func getSignupRequest() -> SignupRequest {
        let userInfo = User()
        userInfo.commonInfo.name = userNameTextField.text!
        userInfo.password = passwordTextField.text!
        userInfo.commonInfo.email = emailTextField.text!
        userInfo.commonInfo.phone = phoneNumberTextField.text!
        return SignupRequest(userInfo: userInfo, confirmPassword: confirmPasswordTextField.text!, isAcceptTerms: acceptTermsButton.isSelected)
    }
    
    func displayVerificationMethodSelectMessage(_ parameters: [String : Any]?) {
        present(KAlert.alert(withTitle: kAppName, message: VerificationMethodSelect, leftButton: VerificationPhone, rightButton: VerificationEmail, leftBlock: { (action) in
            self.executive?.sendSignupRequest(parameters, isPhone: true)
        }, rightBlock: { (action) in
            self.executive?.sendSignupRequest(parameters, isPhone: false)
        }), animated: true, completion: nil)
    }
    
    // MARK: - Actions
    
    @IBAction func didPressBackButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didPressCreateAccountButton(_ sender: Any) {
        executive?.signup()
    }
    
    @IBAction func didPressCheckTermsPrivacy(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func didPressTermsPrivacyButton(_ sender: Any) {
    }
}
