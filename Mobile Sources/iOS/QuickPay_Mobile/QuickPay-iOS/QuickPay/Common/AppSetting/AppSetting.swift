//
//  AppSetting.swift
//  CrossWord
//
//  Created by Michael Lee on 11/30/16.
//  Copyright © 2016 idragon. All rights reserved.
//

import UIKit

fileprivate var appSetting: AppSetting? = nil

class AppSetting: AppSettingBase {
    
    static let instance : AppSetting = {
        
        if appSetting == nil {
            appSetting = AppSetting.init()
        }
        
        return appSetting!
    }()
    
    var userId: String? {
        didSet {
            self.set(stringVal: userId, forKey: "PhotoSaveUserId")
        }
    }
    
    var userName: String? {
        didSet {
            self.set(stringVal: userName, forKey: "PhotoSaveUsername")
        }
    }
    
    override func loadSetting() {
        userId = string(forKey: "PhotoSaveUserId", defVal: nil)
        userName = string(forKey: "PhotoSaveUsername", defVal: nil)
    }
}
