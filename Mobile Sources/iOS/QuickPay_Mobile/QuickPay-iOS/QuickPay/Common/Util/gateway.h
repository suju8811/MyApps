//
//  NetworkUtil.h
//  leaf
//
//  Created by Michael Lee on 1/22/17.
//  Copyright © 2017 Cirrus Systems. All rights reserved.
//

#include <netinet/in.h>
#include <net/if.h>

int getdefaultgateway(in_addr_t * addr);
