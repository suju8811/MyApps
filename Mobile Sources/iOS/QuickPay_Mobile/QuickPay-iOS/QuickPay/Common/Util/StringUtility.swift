//
//  StringUtil.swift
//  CrossWord
//
//  Created by Michael Lee on 1/29/17.
//  Copyright © 2017 idragon. All rights reserved.
//

import UIKit

class StringUtility: NSObject {
    
    public class func validate(email:String) -> Bool {
    
        let regex1 = "\\A[A-Za-z0-9-]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,4}\\z"
        let regex2 = "^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*"
        let test1 = NSPredicate.init(format: "SELF MATCHES %@", regex1)
        let test2 = NSPredicate.init(format: "SELF MATCHES %@", regex2)
        
        return test1.evaluate(with: email) && test2.evaluate(with: email)
    }
    
    public class func htmlToText(encodedString:String) -> String?
    {
        let encodedData = encodedString.data(using: String.Encoding.utf8)!
        do {
            return try NSAttributedString(data: encodedData, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:String.Encoding.utf8.rawValue], documentAttributes: nil).string
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
    }
    
    public class func decimal(with string: String) -> NSDecimalNumber {
        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        return formatter.number(from: string) as? NSDecimalNumber ?? 0
    }
    
    public class func validate(phone: String) -> Bool {
        let regx1 = "\\d{3}-\\d{3}-\\d{4}"
        let test = NSPredicate.init(format: "SELF MATCHES %@", regx1)
        return test.evaluate(with: phone)
    }
    
    public class func discardNoNumber(_ string: String) -> String {
        return string.components(separatedBy: CharacterSet(charactersIn: "0123456789").inverted).joined(separator:"")
    }
}
