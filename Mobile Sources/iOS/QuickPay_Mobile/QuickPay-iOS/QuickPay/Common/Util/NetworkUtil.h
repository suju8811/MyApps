//
//  NetworkUtil.h
//  leaf
//
//  Created by Michael Lee on 1/23/17.
//  Copyright © 2017 Cirrus Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkUtil : NSObject

+  (NSString *)getGatewayIP;

@end
