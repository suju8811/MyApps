//
//  DateTimeUtil.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/24/17.
//
//

import Foundation

let SecondsPerDay = 3600 * 24

class DateTimeUtil {
    
    class func dateFromString(_ string: String, format: String) -> Date? {
        let formatter = DateFormatter.init()
        formatter.dateFormat = format
        let date = formatter.date(from: string)
        return date
    }
    
    class func stringFromDate(_ date: Date, format: String) -> String? {
        let formatter = DateFormatter.init()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
}
