//
//  Client.swift
//  QuickPay
//
//  Created by Michael Lee on 3/29/17.
//
//

import Foundation
import SystemConfiguration.CaptiveNetwork
import Alamofire
import SwiftyJSON

enum RequestType {
    case Get
    case Post
    case Put
    case Delete
}

let LOG_HTTP_RESPONSE    = true

public class Client {
    
    class func sendRequest(_ type: RequestType, name: String, parameters: [String: Any]?, headers: [String: String]?, completionBlock:((Any?, String?) -> Swift.Void)? = nil) {
        sendCommonRequest(type, name: name, parameters: parameters, headers: headers) { (response, error) in
            if let error = error {
                completionBlock?(response, error.localizedDescription)
                return
            }
            guard let response = response else {
                completionBlock?(nil, "")
                return
            }
            let json = JSON(response)
            let code = json["code"].int
            if code != 0 {
                let message = json["message"].string
                completionBlock?(response, message)
                return
            }
            completionBlock?(response, nil)
        }
    }
    
    class func sendCommonRequest(_ type: RequestType, name: String, parameters: [String: Any]? = nil, headers: [String: String]? = nil, completionBlock:((Any?, Error?) -> Swift.Void)? = nil) {
        
        if currentReachabilityStatus == .notReachable {
            let error = NSError(domain: ErrorDomain, code: ErrorUnsupportedType, userInfo: [NSLocalizedDescriptionKey: "Network connection is not enabled."])
            completionBlock?(nil, error)
            return
        }
        
        var httpMethod: HTTPMethod
        switch type {
        case .Get:
            httpMethod = .get
            break
        case .Post:
            httpMethod = .post
            break
        case .Put:
            httpMethod = .put
            break
        case .Delete:
            httpMethod = .delete
            break
        }
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        let URLString = ApiBaseURL + name
        let url = URL(string: URLString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        Alamofire.request(url!, method: httpMethod, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            if LOG_HTTP_RESPONSE {
                let errorResponse: String = String.init(data: response.data!, encoding: .utf8)!
                print(errorResponse)
            }
            
            switch response.result {
            case .success(let value):
                completionBlock?(value, nil)
                break
                
            case .failure(let error):
                completionBlock?(nil, error)
                break
            }
        }
    }
}

extension Client {
    
    enum ReachabilityStatus {
        case notReachable
        case reachableViaWWAN
        case reachableViaWiFi
    }
    
    static var currentReachabilityStatus: ReachabilityStatus {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }
        
        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .notReachable
        }
        else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return .reachableViaWWAN
        }
        else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .reachableViaWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .reachableViaWiFi
        }
        else {
            return .notReachable
        }
    }
}

