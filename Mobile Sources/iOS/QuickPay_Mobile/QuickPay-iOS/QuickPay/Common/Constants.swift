//
//  Constants.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/29/17.
//
//

import Foundation

//
// App Name
//

let kAppName = "QuickPay"

//
// API BASE URL
//

//let ApiBaseURL = "https://www.yourlocalcall.com:8443/YLC/"
let ApiBaseURL = "http://208.73.233.211:8080/YLC/"

//
// Storyboard and IDs
//

let HomeStoryboard = "Home"
let MainScreenID = "MainScreen"

//
// String values
//

let OKString = "OK"
let CancelString = "Cancel"

//
// Storyboard
//

let CommonDatePickerStoryboardID = "CommonDatePicker"
