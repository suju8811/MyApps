//
//  DictionaryExtension.swift
//  QuickPay
//
//  Created by Michael Lee on 6/15/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}
