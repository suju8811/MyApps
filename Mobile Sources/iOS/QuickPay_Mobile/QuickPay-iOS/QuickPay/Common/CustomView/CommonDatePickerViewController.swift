//
//  ReservationDatePickerViewController.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/24/17.
//
//

import UIKit

enum DateType {
    case Current
    case From
    case To
}

class CommonDatePickerViewController: UIViewController {

    @IBOutlet var datePicker: UIDatePicker!
    
    var executive: CommonExecutive!
    var currentDate: Date!
    var dateType: DateType = .Current
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(CommonDatePickerViewController.viewTapped(_:)))
        view.addGestureRecognizer(gestureRecognizer)
        
        datePicker.date = currentDate
    }
    
    // MAKR: - Actions
    
    @IBAction func viewTapped(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionDone(_ sender: Any) {
        executive.didDateChanged(datePicker.date, type: dateType)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didCloseButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
