//
//  UIViewControllerExtension.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/24/17.
//
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func presentTransparentViewController(_ viewControllerToPresent: UIViewController, animated: Bool, completion: (() -> Void)? = nil) {
        
        viewControllerToPresent.modalPresentationStyle = .overCurrentContext
        present(viewControllerToPresent, animated: animated, completion: completion)
    }
}
