//
//  KAlert.swift
//  CrossWord
//
//  Created by Marcus Lee on 11/23/16.
//  Copyright © 2016 idragon. All rights reserved.
//

import UIKit
import PKHUD

class KAlert: NSObject {

    // MARK: - Create Common Alert Controller
    class func alert(withTitle: String?, message: String?, leftButton: String?, rightButton: String?, leftBlock: ((UIAlertAction) -> Swift.Void)? = nil, rightBlock: ((UIAlertAction) -> Swift.Void)? = nil) -> UIAlertController {
        
        let alertController : UIAlertController = UIAlertController.init(title: withTitle, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        if (leftButton != nil) {
            
            let cancelAction:UIAlertAction = UIAlertAction.init(title: NSLocalizedString(leftButton!, comment: "Cancel Action"), style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                
                if (leftBlock != nil) {
                    leftBlock!(action)
                }
            })
          
            alertController.addAction(cancelAction)
        }
        
        if (rightButton != nil) {
            
            let okAction:UIAlertAction = UIAlertAction.init(title: NSLocalizedString(rightButton!, comment: "OK action"), style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
                
                if (rightBlock != nil) {
                    rightBlock!(action)
                }
            })
            
            alertController.addAction(okAction)
        }
        
        return alertController;
    }
    
    // MARK: - Create Input Alert
    class func inputAlert(withTitle:String?, message:String?, leftButton:String?, rightButton:String?, placeholder: String?, leftBlock:((UIAlertAction, UITextField) -> Swift.Void)? = nil, rightBlock:((UIAlertAction, UITextField) -> Swift.Void)? = nil) -> UIAlertController {
        
        let alert:UIAlertController = UIAlertController.init(title: withTitle, message: message, preferredStyle: UIAlertControllerStyle.alert)

        let ok:UIAlertAction = UIAlertAction.init(title: leftButton, style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            
            let textField:UITextField = alert.textFields![0]
            textField.resignFirstResponder()
            
            if (leftBlock != nil) {
                leftBlock!(action, textField)
            }
        })
        
        let cancel:UIAlertAction = UIAlertAction.init(title: rightButton, style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            
            let textField:UITextField = alert.textFields![0]
            textField.resignFirstResponder()
            alert.dismiss(animated: true, completion: nil)
            
            if (rightBlock != nil) {
                rightBlock!(action, textField)
            }
        })
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        alert.addTextField { (textField) in
            textField.placeholder = placeholder
        }
        
        return alert;
    }
    
    // MARK: - Create Common ActionSheet
    class func actionSheet(withTitle:String?, message:String?, cancelButtonTitle:String?, actionBlock:((UIAlertAction) -> Swift.Void)? = nil, button:UIView, otherButtonTitles:String...) -> UIAlertController {
        
        let alertController:UIAlertController = UIAlertController.init(title: withTitle, message: message, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        for arg in otherButtonTitles {
            
            let action = UIAlertAction.init(title: NSLocalizedString(arg, comment: "OK action"), style: UIAlertActionStyle.default, handler: { (action) in
                actionBlock!(action)
            })
            alertController.addAction(action)
        }
        
        if (cancelButtonTitle != nil && cancelButtonTitle != "") {
            
            let cancelAction:UIAlertAction = UIAlertAction.init(title: NSLocalizedString(cancelButtonTitle!, comment: "Cancel Action"), style: UIAlertActionStyle.cancel, handler: nil)
            alertController.addAction(cancelAction)
        }
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad) {
            alertController.popoverPresentationController?.sourceView = button
        }
        
        return alertController
    }
    
    class func actionSheet(withTitle:String?, message:String?, cancelButtonTitle:String?, actionBlock:((UIAlertAction, Int) -> Swift.Void)? = nil, button:UIView, otherButtonTitles:[String]) -> UIAlertController {
        
        let alertController:UIAlertController = UIAlertController.init(title: withTitle, message: message, preferredStyle: UIAlertControllerStyle.actionSheet)
  
        for arg in otherButtonTitles {
            
            let action:UIAlertAction = UIAlertAction.init(title: NSLocalizedString(arg, comment: "OK Action"), style: UIAlertActionStyle.default, handler: { (action) in
                
                let index:Int = alertController.actions.index(of: action)!
                actionBlock!(action, index)
            })
            alertController.addAction(action)
        }
        
        if (cancelButtonTitle != nil && cancelButtonTitle != "") {
            
            let cancelAction:UIAlertAction = UIAlertAction.init(title: NSLocalizedString(cancelButtonTitle!, comment: "Cancel Action"), style: UIAlertActionStyle.cancel, handler: nil)
            alertController.addAction(cancelAction)
        }

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad) {
            
            alertController.popoverPresentationController?.sourceView = button
            alertController.popoverPresentationController?.sourceRect = CGRect(x:button.bounds.size.width / 2, y:button.bounds.size.height / 2, width:5, height:5)
        }
        
        return alertController
    }
    
    // MARK: - Create Simple Notification Alert Controller
    class func notifyAlert(withTitle:String?, message:String?, buttonTitle:String?) -> UIAlertController {
     
        return KAlert.alert(withTitle: withTitle, message: message, leftButton: buttonTitle, rightButton: nil)
    }
    
    // MARK: - waiting dialog
    class func showLoading(_ parentView: UIView, msg: String) {
        DispatchQueue.main.async {
            PKHUD.sharedHUD.contentView = PKHUDProgressView.init(title: msg, subtitle: nil)
            PKHUD.sharedHUD.show()
        }
    }
    
    class func showLoading(_ parentView: UIView) {
        KAlert.showLoading(parentView, msg: "Loading")
    }
    
    class func hideLoading(_ parentView:UIView) {
        DispatchQueue.main.async {
            PKHUD.sharedHUD.hide()
        }
    }

}
