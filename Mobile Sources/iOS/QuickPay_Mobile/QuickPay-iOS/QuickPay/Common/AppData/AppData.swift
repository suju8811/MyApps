//
//  AppData.swift
//  QuickPay
//
//  Created by Michael Lee on 6/6/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation

var quickPayAppDataInstance: AppData? = nil

class AppData {
    
    static let sharedInstance : AppData = {
        
        if quickPayAppDataInstance == nil {
            quickPayAppDataInstance = AppData.init()
        }
        
        return quickPayAppDataInstance!
    }()
    
    var user: User?
    var paymentProfile = PaymentProfile()
    var countries = [Country]()
    var payPalConfig = PayPalConfiguration()
}
