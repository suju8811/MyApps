//
//  Billing.swift
//  QuickPay
//
//  Created by Michael Lee on 6/14/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation
import SwiftyJSON

class BillingSearchRequest {
    var value = ""
    var regex = false
    
    /**
     *  Convert object to parameters
     *  @return parameter object as dictionary
     */
    
    func toParameters() -> [String: Any] {
        let parameters = ["value": value,
                          "regex": regex] as [String : Any]
        return parameters
    }
}

class BillingCommonRequest {
    var data: String = "payment_date"
    var name: String = ""
    var searchable: Bool = false
    var orderable: Bool = true
    var search = BillingSearchRequest()
    
    /**
     *  Convert object to parameters
     *  @return parameter object as dictionary
     */
    
    func toParameters() -> [String: Any] {
        let parameters = ["data": data,
                          "name": name,
                          "searchable": searchable,
                          "orderable": orderable,
                          "search": search.toParameters()] as [String : Any]
        return parameters
    }
}

class BillingOrderRequest {
    var column = 0
    var dir = "desc"
    
    /**
     *  Convert object to parameters
     *  @return parameter object as dictionary
     */
    
    func toParameters() -> [String: Any] {
        let parameters = ["column": column,
                          "dir": dir] as [String : Any]
        return parameters
    }
}

class BillingFilterRequest {
    var date = ""
    var dateStart = ""
    var dateEnd = ""
    var dateOption = 0
    var type = ""
    var amount = ""
    var amountStart = ""
    var amountEnd = ""
    var amountOption = 1
    var inmateBalance = ""
    var inmateBalanceStart = ""
    var inmateBalanceEnd = ""
    var inmateBalanceOption = 1
    
    /**
     *  Convert object to parameters
     *  @return parameter object as dictionary
     */
    
    func toParameters() -> [String: Any] {
        let parameters = ["payment_date": date,
                          "payment_date_start": dateStart,
                          "payment_date_end": dateEnd,
                          "payment_date_option": dateOption,
                          "payment_type": type,
                          "payment_amount": amount,
                          "payment_amount_start": amountStart,
                          "payment_amount_end": amountEnd,
                          "payment_amount_option": amountOption,
                          "inmate_balance": inmateBalance,
                          "inmate_balance_start": inmateBalanceStart,
                          "inmate_balance_end": inmateBalanceEnd,
                          "inmate_balance_option": inmateBalanceOption] as [String : Any]
        return parameters
    }
}

class BillingRequest {
    var draw = 0
    var columns = [BillingCommonRequest]()
    var order = [BillingOrderRequest]()
    var start = 0
    var length = 10
    var search = BillingSearchRequest()
    var filterRequest = BillingFilterRequest()
    var billingType: BillingType = .Payment
    var billingActor: BillingActor = .Customer
    
    /**
     *  Convert object to parameters
     *  @return parameter object as dictionary
     */
    
    func toParameters() -> [String: Any] {
        var parameters = ["draw": draw,
//                          "columns": columns.map({ $0.toParameters() }),
//                          "order": order.map({ $0.toParameters}),
                          "start": start,
                          "length": length,
                          "search": search.toParameters()] as [String : Any]
        let filterParameters = filterRequest.toParameters()
        parameters.merge(dict: filterParameters)
        return parameters
    }
    
    /**
     *  Request load billing data
     *  @param executive: Common executive object
     *  @param completionBlock: The block that is called when request is completed.
     */
    
    public func send(executive: CommonExecutive, completionBlock:@escaping ((Bool, BillingResponseArray?) -> Swift.Void)) {
        let parameters = toParameters()
        let path = "billing/\(billingType.rawValue.lowercased())/load_\(billingActor.rawValue.lowercased()).do"
        Client.sendRequest(.Post, name: path, parameters: parameters, headers: nil) { (response, error) in
            guard executive.validate(response, error: error, dataColumn: "data") != nil else {
                completionBlock(false, nil)
                return
            }
            let data = BillingResponseArray()
            data.setData(from: JSON(response!))
            completionBlock(true, data)
        }
    }
}

class BillingResponseArray {
    var recordsTotal: Int?
    var recordsFiltered: Int?
    var data = [BillingResponse]()
    
    /**
     *  Set data from JSON
     *  @param json: The json object to be parsed
     */
    
    public func setData(from json: JSON?) {
        guard let json = json else {
            return
        }
        
        recordsTotal = json["recordsTotal"].int
        recordsFiltered = json["recordsFiltered"].int
        data.removeAll()
        if let dataArray = json["data"].array {
            for billingResponseJson in dataArray {
                let billingResponse = BillingResponse()
                billingResponse.setData(from: billingResponseJson)
                data.append(billingResponse)
            }
        }
    }
}

class BillingResponse {
    
    var transactionID: String?
    var prepaidAmount: Double?
    var additional: String?
    var inmateBalance: String?
    var paymentAmount: String?
    var description: String?
    var isRemit: Int?
    var inmateName: String?
    var paymentType: String?
    var last4Digits: String?
    var isRefunded: Int?
    var paymentProfileID: String?
    var notificationAmount: Int?
    var id: Int?
    var isPaid: Int?
    var paymentDate: String? = "2017-06-02 17:42:29.0"
    var nameOnCard: String?
    
    /**
     *  Set data from JSON
     *  @param json: The json object to be parsed
     */
    
    public func setData(from json: JSON?) {
        guard let json = json else {
            return
        }
        
        transactionID = json["transaction_id"].string
        prepaidAmount = json["prepaid_amount"].double
        additional = json["additional"].string
        inmateBalance = json["inmate_balance"].string
        paymentAmount = json["payment_amount"].string
        description = json["description"].string
        isRemit = json["is_remit"].int
        inmateName = json["inmate_name"].string
        paymentType = json["payment_type"].string
        last4Digits = json["last_4_digits"].string
        isRefunded = json["is_refunded"].int
        paymentProfileID = json["payment_profile_id"].string
        notificationAmount = json["notification_amount"].int
        id = json["id"].int
        isPaid = json["is_paid"].int
        paymentDate = json["payment_date"].string
        nameOnCard = json["name_on_card"].string
    }
}
