//
//  Inmate.swift
//  QuickPay
//
//  Created by Michael Lee on 5/12/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit
import SwiftyJSON

class Inmate {
    
    var id: Int?
    var code: String?
    var locationID: Int?
    var BOPNumber: String?
    var firstName: String?
    var lastName: String?
    
    /**
     *  Set data from JSON
     *  @param json: The json object to be parsed
     */
    
    public func setData(from json: JSON?) {
        guard let json = json else {
            return
        }
        
        id = json["inmateID"].int
        code = json["inmateCode"].string
        locationID = json["locationID"].int
        BOPNumber = json["bopNumber"].string
        firstName = json["firstName"].string
        lastName = json["lastName"].string
    }
    
    /**
     *  Get inmate by id from server
     *  @param id: Inmate Id
     *  @param successBlock: The block that is called when request is completed.
     *  @param executive: Common executive object
     */
    
    public class func loadInmate(by id: Int, _ successBlock:@escaping ((Inmate?) -> Swift.Void), executive: CommonExecutive) {
        let parameters = ["id": id, "from": "frontend"] as [String : Any]
        Client.sendRequest(.Post, name: "inmate/get.do", parameters: parameters, headers: nil) { (response, error) in
            guard let result = executive.validate(response, error: error) else {
                successBlock(nil)
                return
            }
            let inmateJson = result["inmate"]
            let inmate = Inmate()
            inmate.setData(from: inmateJson)
            successBlock(inmate)
        }
    }
}
