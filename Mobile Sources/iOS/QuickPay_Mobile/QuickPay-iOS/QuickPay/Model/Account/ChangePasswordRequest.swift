//
//  ChangePasswordRequest.swift
//  QuickPay
//
//  Created by Michael Lee on 6/6/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation

//
// Change Password Request
//

class ChangePasswordRequest {
    
    let oldPassword: String
    let newPassword: String
    let confirmPassword: String
    
    /**
     *  Initialization
     *  @param oldPassword
     *  @param newPassword
     *  @param confirmPassword
     */
    
    init(oldPassword: String, newPassword: String, confirmPassword: String) {
        self.oldPassword = oldPassword
        self.newPassword = newPassword
        self.confirmPassword = confirmPassword
    }
    
    /**
     *  Request change password
     *  @param executive: Common executive object
     *  @param completionBlock: The block that is called when request is completed.
     */
    
    public func send(executive: CommonExecutive, completionBlock:@escaping ((Bool, String?) -> Swift.Void)) {
        let parameters = ["old_password": oldPassword,
                          "new_password": newPassword,
                          "is_mobile": "1"]
        Client.sendRequest(.Post, name: "customer/update_password.do", parameters: parameters, headers: nil) { (response, error) in
            guard executive.validate(response, error: error) != nil else {
                completionBlock(false, nil)
                return
            }
            guard let message = executive.messageFromResponse(response) else {
                return
            }
            completionBlock(true, message)
        }
    }
}
