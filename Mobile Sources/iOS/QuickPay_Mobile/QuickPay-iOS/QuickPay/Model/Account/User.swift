//
//  User.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/23/17.
//
//

import Foundation
import UIKit
import SwiftyJSON

//
// User
//

class User {
    
    var id: Int?
    var commonInfo = CommonUserInfo()
    var personAddress = PersonAddress()
    var inmateId: Int?
    var paypalInfo: PaypalInfo?
    var password: String?
    
    /**
     *  Set data from JSON
     *  @param json: The json object to be parsed
     */
    
    public func setData(from json: JSON?) {
        guard let json = json else {
            return
        }
        
        let userJson = json["user"]
        id = userJson["id"].int
        commonInfo.setData(from: userJson)
        personAddress.setData(from: json)
        inmateId = userJson["inmateID"].int
        if inmateId == 0 {
            inmateId = nil
        }
        if let allowCustomer = userJson["allowCustomer"].int, allowCustomer == 1 {
            inmateId = nil
        }
        paypalInfo = PaypalInfo()
        paypalInfo?.setData(from: json["paypal"])
    }
}

//
// Common User Info
//

class CommonUserInfo: NSCopying {
    
    var name: String?
    var firstName: String?
    var middleName: String?
    var lastName: String?
    var email: String?
    var phone: String?
    
    /**
     *  Set data from JSON
     *  @param json: The json object to be parsed
     */
    
    public func setData(from json: JSON?) {
        guard let json = json else {
            return
        }
        
        name = json["userName"].string
        firstName = json["firstName"].string
        middleName = json["middleName"].string
        lastName = json["lastName"].string
        email = json["email"].string
        phone = json["mobileNumber"].string
    }
    
    /**
     *  Initialize value
     */
    
    public func initialize() {
        firstName = nil
        middleName = nil
        lastName = nil
        email = nil
        phone = nil
    }
    
    /**
     *  NSCopying delegate function
     *  @param zone: NSZone
     *  @return Any object
     */
    
    public func copy(with zone: NSZone? = nil) -> Any {
        let commonUserInfo = CommonUserInfo()
        commonUserInfo.name = name
        commonUserInfo.firstName = firstName
        commonUserInfo.middleName = middleName
        commonUserInfo.lastName = lastName
        commonUserInfo.email = email
        commonUserInfo.phone = phone
        return commonUserInfo
    }
}

//
// Paypal
//

enum PaypalMode {
    case SandBox
    case Production
}

class PaypalInfo {
    var mode: PaypalMode?
    var clientId: String?
    var secretKey: String?
    
    public func setData(from json: JSON?) {
        guard let json = json else {
            return
        }
        mode = .SandBox
        if let modeString = json["mode"].string {
            if modeString != "sandbox" {
                mode = .Production
            }
        }
        clientId = json["clientID"].string
        secretKey = json["secretKey"].string
    }
}

//
// Signup Request
//

class SignupRequest {
    
    let userInfo: User
    let confirmPassword: String
    let isAcceptTerms: Bool
    
    init(userInfo: User, confirmPassword: String, isAcceptTerms: Bool) {
        self.userInfo = userInfo
        self.confirmPassword = confirmPassword
        self.isAcceptTerms = isAcceptTerms
    }
}

class ActivateRequest {
    let userInfo: User
    let activationCode: String
    
    init(userInfo: User, activationCode: String) {
        self.userInfo = userInfo
        self.activationCode = activationCode
    }
}
