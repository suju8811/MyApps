//
//  EditProfileRequest.swift
//  QuickPay
//
//  Created by Michael Lee on 6/6/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation

//
// Edit Profile Request
//

class EditProfileRequest {
    
    let commonInfo: CommonUserInfo
    let addressInfo: PersonAddress
    
    /**
     *  Initialization
     *  @param commonInfo: Common User Information
     *  @param addressInfo: Person Address Information
     */
 
    init(commonInfo: CommonUserInfo, addressInfo: PersonAddress) {
        self.commonInfo = commonInfo
        self.addressInfo = addressInfo
    }
    
    /**
     *  Request edit profile
     *  @param executive: Common executive object
     *  @param completionBlock: The block that is called when request is completed.
     */
    
    public func send(executive: CommonExecutive, completionBlock:@escaping ((Bool, String?) -> Swift.Void)) {
        
        let firstName = commonInfo.firstName ?? ""
        let middleName = commonInfo.middleName ?? ""
        let lastName = commonInfo.lastName ?? ""
        let emailAddress = commonInfo.email ?? ""
        let phone = commonInfo.phone ?? ""
        var country: String
        if let countryId = addressInfo.country.id {
            country = "\(countryId)"
        }
        else {
            country = ""
        }
        let address1 = addressInfo.address1 ?? ""
        let address2 = addressInfo.address2 ?? ""
        let city = addressInfo.city ?? ""
        var state: String
        if let stateId = addressInfo.state.id {
            state = "\(stateId)"
        }
        else {
            state = ""
        }
        let zipCode = addressInfo.zipCode ?? ""
        let parameters = ["first_name": firstName,
                          "middle_name": middleName,
                          "last_name": lastName,
                          "email_address": emailAddress,
                          "contact_number": phone,
                          "country": country,
                          "address_1": address1,
                          "address_2": address2,
                          "city": city,
                          "state": state,
                          "zip_code": zipCode,
                          "is_mobile": "1"]
        Client.sendRequest(.Post, name: "customer/update_profile.do", parameters: parameters, headers: nil) { (response, error) in
            guard executive.validate(response, error: error) != nil else {
                completionBlock(false, nil)
                return
            }
            
            let currentUser = AppData.sharedInstance.user
            currentUser?.commonInfo.firstName = firstName
            currentUser?.commonInfo.middleName = middleName
            currentUser?.commonInfo.lastName = lastName
            currentUser?.commonInfo.phone = phone
            currentUser?.personAddress.country = self.addressInfo.country
            currentUser?.personAddress.address1 = address1
            currentUser?.personAddress.address2 = address2
            currentUser?.personAddress.city = city
            currentUser?.personAddress.state = self.addressInfo.state
            currentUser?.personAddress.zipCode = zipCode
            guard let message = executive.messageFromResponse(response) else {
                return
            }
            completionBlock(true, message)
        }
    }
}
