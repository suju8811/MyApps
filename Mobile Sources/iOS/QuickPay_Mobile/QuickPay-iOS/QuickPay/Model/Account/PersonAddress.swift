//
//  PersonAddress.swift
//  QuickPay
//
//  Created by Michael Lee on 6/6/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation
import SwiftyJSON

//
// Person Address
//

enum AddressType {
    case OneAddress
    case TwoAddress
}

//
// Country
//

class Country: NSCopying {
    
    var id: Int?
    var name: String?
    var abbreviation: String?
    var code: Int?
    
    /**
     *  Set data from JSON
     *  @param json: The json object to be parsed
     */
    
    public func setData(from json: JSON?) {
        guard let json = json else {
            return
        }
        
        id = json["countryID"].int
        name = json["countryName"].string
        abbreviation = json["countryAbbr"].string
        code = json["countryCode"].int
    }
    
    /**
     *  Load countries from server
     *  @param successBlock: The block that is called when request is completed.
     *  @param executive: Common executive object
     */
    
    public class func loadCountries(_ successBlock:@escaping ((Bool) -> Swift.Void),
                                    executive: CommonExecutive) {
        Client.sendRequest(.Post, name: "address/get_country.do", parameters: nil, headers: nil) { (response, error) in
            guard let result = executive.validate(response, error: error) else {
                successBlock(false)
                return
            }
            if let resultArray = result["country"].array {
                var countries = [Country]()
                for countryJson in resultArray {
                    let country = Country()
                    country.setData(from: countryJson)
                    countries.append(country)
                }
                AppData.sharedInstance.countries = countries
                successBlock(true)
                return
            }
            successBlock(false)
        }
    }
    
    /**
     *  NSCopying delegate function
     *  @param zone: NSZone
     *  @return Any object
     */
    
    public func copy(with zone: NSZone? = nil) -> Any {
        let country = Country()
        country.id = id
        country.name = name
        country.abbreviation = abbreviation
        country.code = code
        return country
    }
}

//
// State
//

class State {
    
    var id: Int?
    var name: String?
    var abbreviation: String?
    var country: Country?
    
    /**
     *  Set data from JSON
     *  @param json: The json object to be parsed
     */
    
    public func setData(from json: JSON?) {
        guard let json = json else {
            return
        }
        
        id = json["stateID"].int
        name = json["stateName"].string?.trimmingCharacters(in: .whitespacesAndNewlines)
        abbreviation = json["stateAbbr"].string
        country = Country()
        country?.setData(from: json)
    }
    
    /**
     *  Load states for country from server
     *  @param countryId: Country Id of which states
     *  @param successBlock: The block that is called when request is completed.
     *  @param executive: Common executive object
     */
    
    public class func loadStates(_ countryId: Int,
                                 successBlock:@escaping (([State]?) -> Swift.Void),
                                 executive: CommonExecutive) {
        let parameters = ["country": countryId]
        Client.sendRequest(.Post, name: "address/get_state.do", parameters: parameters, headers: nil) { (response, error) in
            guard let result = executive.validate(response, error: error) else {
                successBlock(nil)
                return
            }
            if let resultArray = result["state"].array {
                var states = [State]()
                for stateJson in resultArray {
                    let state = State()
                    state.setData(from: stateJson)
                    states.append(state)
                }
                successBlock(states)
                return
            }
            successBlock(nil)
        }
    }
}

//
// Person Address
//

class PersonAddress: NSCopying {
    
    var addressType = AddressType.OneAddress
    var billingAddress: String?
    var address1: String?
    var address2: String?
    var country = Country()
    var state = State()
    var city: String?
    var zipCode: String?
    
    /**
     *  Set data from JSON
     *  @param json: The json object to be parsed
     */
    
    public func setData(from json: JSON?) {
        guard let json = json else {
            return
        }
        
        if let countryName = json["country"].string {
            country.name = countryName
        }
        else {
            country.setData(from: json["country"])
        }
        billingAddress = json["billingAddress"].string
        address1 = json["address1"].string
        address2 = json["address2"].string
        if let stateName = json["state"].string {
            state.name = stateName
        }
        else {
            state.setData(from: json["state"])
        }
        city = json["city"].string
        zipCode = json["zipCode"].string
    }
    
    /**
     *  NSCopying delegate function
     *  @param zone: NSZone
     *  @return Any object
     */
    
    public func copy(with zone: NSZone? = nil) -> Any {
        let personAddress = PersonAddress()
        personAddress.addressType = addressType
        personAddress.billingAddress = billingAddress
        personAddress.address1 = address1
        personAddress.address2 = address2
        personAddress.country = country.copy() as! Country
        personAddress.state = state
        personAddress.city = city
        personAddress.zipCode = zipCode
        return personAddress
    }
    
    /**
     *  Initialize values
     */
    
    public func initialize() {
        
        billingAddress = nil
        address1 = nil
        address2 = nil
        country = Country()
        state = State()
        city = nil
        zipCode = nil
    }
}
