//
//  PaymentRequest.swift
//  QuickPay
//
//  Created by Michael Lee on 5/25/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation
import SwiftyJSON

//
// Payment Request
//

class PaymentRequest {
    
    var inmateId: Int?
    var totalAmount = ""
    var paymentProfileID = ""
    var cardHolder = ""
    var cvc = ""
    var cardNumber = ""
    var expiration = ""
}

class CreditCardPaymentRequest: PaymentRequest {
    
    var phoneNumber = ""
    var email = ""
    var usePersonEmail = false
    var country = ""
    var billingAdress = ""
    var usePersonAddress = false
    var city = ""
    var state = ""
    var zipCode = ""
    var isUpdate = false
}
