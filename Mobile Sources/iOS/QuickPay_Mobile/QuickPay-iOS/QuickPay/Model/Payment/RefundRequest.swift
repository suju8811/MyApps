//
//  RefundRequest.swift
//  QuickPay
//
//  Created by Michael Lee on 6/16/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation
import SwiftyJSON

//
// Get Refund List Request
//

class RefundRequest {
    
    var from = "frontend"
    var kind = "get"
    var paymentId = 0
    var actor: BillingActor = .Customer
    
    /**
     *  Convert object to parameters
     *  @return parameter object as dictionary
     */
    
    func toParameters() -> [String: Any] {
        let parameters = ["from": from,
                          "kind": kind,
                          "payment_id": paymentId] as [String : Any]
        return parameters
    }
    
    /**
     *  Request get refund list
     *  @param executive: Common executive object
     *  @param completionBlock: The block that is called when request is completed.
     */
    
    public func get(executive: CommonExecutive, completionBlock:@escaping ((Bool, GetRefundListResponse?) -> Swift.Void)) {
        let parameters = toParameters()
        let path = "billing/refund/\(actor.rawValue.lowercased()).do"
        Client.sendRequest(.Post, name: path, parameters: parameters, headers: nil) { (response, error) in
            guard let responseJson = executive.validate(response, error: error) else {
                completionBlock(false, nil)
                return
            }
            let data = GetRefundListResponse()
            data.setData(from: responseJson)
            completionBlock(true, data)
        }
    }
    
    /**
     *  Request refund
     *  @param executive: Common executive object
     *  @param completionBlock: The block that is called when request is completed.
     */
    
    public func post(executive: CommonExecutive, completionBlock:@escaping ((Bool, String?) -> Swift.Void)) {
        let parameters = toParameters()
        let path = "billing/refund/\(actor.rawValue.lowercased()).do"
        Client.sendRequest(.Post, name: path, parameters: parameters, headers: nil) { (response, error) in
            if let message = executive.messageFromResponse(response) {
                completionBlock(true, message)
            }
            completionBlock(false, nil)
        }
    }
}

class GetRefundListResponse {
    var price: String?
    var payments = [GetRefundResponse]()
    var transaction: String?
    var isSettle: Int?
    
    /**
     *  Set data from JSON
     *  @param json: The json object to be parsed
     */
    
    public func setData(from json: JSON?) {
        guard let json = json else {
            return
        }
        
        price = json["price"].string
        payments.removeAll()
        if let paymentArray = json["payment"].array {
            for paymentJson in paymentArray {
                let payment = GetRefundResponse()
                payment.setData(from: paymentJson)
                payments.append(payment)
            }
        }
        transaction = json["transaction"].string
        isSettle = json["is_settle"].int
    }
}

class GetRefundResponse {
    var customerPaymentID: Int?
    var customerID: Int?
    var customerNumberID: Int?
    var inmatePaymentID: Int?
    var inmateID: Int?
    var inmateNumberID: Int?
    var paymentType: Int?
    var paymentAmount: Int?
    var transactionID: Int?
    var nameOnCard: String?
    var last4DigitsOfCard: String?
    var paymentDate: String?
    var isPaid: Int?
    var isRemit: Int?
    var isInvoice: Int?
    var isRefunded: Int?
    var description: String?
    var callID: Int?
    var duration: Int?
    var paymentProfileID: String?
    var paymentOption: String?
    var referralID: Int?
    var createdBy: Int?
    var createdAt: String?
    
    /**
     *  Set data from JSON
     *  @param json: The json object to be parsed
     */
    
    public func setData(from json: JSON?) {
        guard let json = json else {
            return
        }
        
        customerPaymentID = json["customerPaymentID"].int
        customerID = json["customerID"].int
        customerNumberID = json["customerNumberID"].int
        inmatePaymentID = json["inmatePaymentID"].int
        inmateID = json["inmateID"].int
        inmateNumberID = json["inmateNumberID"].int
        paymentType = json["paymentType"].int
        paymentAmount = json["paymentAmount"].int
        transactionID = json["transactionID"].int
        nameOnCard = json["nameOnCard"].string
        last4DigitsOfCard = json["last4DigitsOfCard"].string
        paymentDate = json["paymentDate"].string
        isPaid = json["isPaid"].int
        isRemit = json["isRemit"].int
        isInvoice = json["isInvoice"].int
        isRefunded = json["isRefunded"].int
        description = json["description"].string
        callID = json["callID"].int
        duration = json["duration"].int
        paymentProfileID = json["paymentProfileID"].string
        paymentOption = json["paymentOption"].string
        referralID = json["referralID"].int
        createdBy = json["createdBy"].int
        createdAt = json["createdAt"].string
    }
}


