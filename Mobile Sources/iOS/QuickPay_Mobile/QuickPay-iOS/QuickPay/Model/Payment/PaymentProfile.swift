//
//  PaymentProfile.swift
//  QuickPay
//
//  Created by Michael Lee on 6/6/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation
import SwiftyJSON

class PaymentProfile: NSCopying {
    
    var paymentProfileID: String?
    var firstName: String?
    var lastName: String?
    var cardHolderName: String?
    var cvc: String?
    var usePersonAddress = false
    var personAddress = PersonAddress()
    var cardNumber: String?
    var expiration: String?
    var email: String?
    var usePersonEmail = false
    var phoneNumber: String?
    var autoPayment: String?
    var message: String?
    var newCVC: Bool?
    
    /**
     *  NSCopying delegate function
     *  @param zone: NSZone
     *  @return Any object
     */
    
    public func copy(with zone: NSZone? = nil) -> Any {
        let paymentProfile = PaymentProfile()
        paymentProfile.paymentProfileID = paymentProfileID
        paymentProfile.firstName = firstName
        paymentProfile.lastName = lastName
        paymentProfile.cardHolderName = cardHolderName
        paymentProfile.cvc = cvc
        paymentProfile.usePersonAddress = usePersonAddress
        paymentProfile.personAddress = personAddress.copy() as! PersonAddress
        paymentProfile.cardNumber = cardNumber
        paymentProfile.expiration = expiration
        paymentProfile.email = email
        paymentProfile.usePersonEmail = usePersonEmail
        paymentProfile.phoneNumber = phoneNumber
        paymentProfile.autoPayment = autoPayment
        paymentProfile.message = message
        paymentProfile.newCVC = newCVC
        return paymentProfile
    }
    
    /**
     *  Set data from JSON
     *  @param json: The json object to be parsed
     */
    
    public func setData(from json: JSON?) {
        guard let json = json else {
            return
        }
        
        let paymentJson = json["payment"]
        let userJson = json["user"]
        paymentProfileID = paymentJson["paymentProfileID"].string
        firstName = paymentJson["firstName"].string
        lastName = paymentJson["lastName"].string
        cardHolderName = paymentJson["cardHolderName"].string
        cvc = paymentJson["cvc"].string
        let currentUser = AppData.sharedInstance.user
        currentUser?.personAddress.setData(from: userJson)
        if let isUsePersonAddress = paymentJson["usePersonAddress"].int, isUsePersonAddress == 0 {
            usePersonAddress = false
            personAddress.setData(from: paymentJson)
        }
        else {
            usePersonAddress = true
            if let userPersonAddress = currentUser?.personAddress {
                personAddress = userPersonAddress
            }
        }
        cardNumber = paymentJson["cardNumber"].string
        expiration = paymentJson["expiration"].string
        currentUser?.commonInfo.setData(from: userJson)
        if let isUsePersonEmail = paymentJson["usePersonEmail"].int, isUsePersonEmail == 0 {
            usePersonEmail = false
            email = paymentJson["email"].string
        }
        else {
            usePersonEmail = true
            email = currentUser?.commonInfo.email
        }
        phoneNumber = paymentJson["phoneNumber"].string
        autoPayment = paymentJson["autoPayment"].string
        message = paymentJson["message"].string
        newCVC = paymentJson["newCVC"].bool
    }
    
    /**
     *  Load payment profile from server
     *  @param countryId: Country Id of which states
     *  @param successBlock: The block that is called when request is completed.
     *  @param executive: Common executive object
     */
    
    public func loadPaymentProfile(_ successBlock:@escaping ((Bool) -> Swift.Void), executive: CommonExecutive) {
        Client.sendRequest(.Post, name: "payment/get_profile.do", parameters: nil, headers: nil) { (response, error) in
            guard let result = executive.validate(response, error: error) else {
                successBlock(false)
                return
            }
            self.setData(from: result)
            successBlock(true)
        }
    }
    
    /**
     *  Update payment profile
     *  @param countryId: Country Id of which states
     *  @param successBlock: The block that is called when request is completed.
     *  @param executive: Common executive object
     */
    
    public func updatePaymentProfile(_ completionBlock:@escaping ((Bool, String?) -> Swift.Void), executive: CommonExecutive) {
        let parameters = ["email_address": email!,
                          "phone_number": StringUtility.discardNoNumber(phoneNumber!),
                          "use_person_email": usePersonEmail ? "1" : "0",
                          "cardholder": cardHolderName!,
                          "cvc": cvc!,
                          "card_number": cardNumber!,
                          "expiration": expiration!,
                          "use_person_address": usePersonAddress ? "1" : "0",
                          "country": personAddress.country.name!,
                          "address": personAddress.billingAddress!,
                          "city": personAddress.city!,
                          "state": personAddress.state.name ?? "",
                          "zip_code": personAddress.zipCode!,
                          "auto_payment": autoPayment!,
                          "is_mobile": "1"]
        Client.sendRequest(.Post, name: "customer/update_creditcard.do", parameters: parameters, headers: nil) { (response, error) in
            guard executive.validate(response, error: error) != nil else {
                completionBlock(false, nil)
                return
            }
            let message = executive.messageFromResponse(response)
            completionBlock(true, message)
        }
    }
}
