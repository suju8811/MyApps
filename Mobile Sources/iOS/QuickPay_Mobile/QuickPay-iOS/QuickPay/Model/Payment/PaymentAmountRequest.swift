//
//  PaymentAmountRequest.swift
//  QuickPay
//
//  Created by Michael Lee on 6/15/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation

//
// Payment Request
//

class PaymentAmountRequest {

    var paymentProfile = PaymentProfile()
    var isPaypal = false
    var isUpdate = false
    var totalPrice = ""
    
    /**
     *  Proceed prepay by credit card
     *  @param countryId: Country Id of which states
     *  @param successBlock: The block that is called when request is completed.
     *  @param executive: Common executive object
     */
    
    public func proceedPrepayByCreditCard(_ completionBlock:@escaping ((Bool, String?) -> Swift.Void), executive: CommonExecutive) {
        let paymentProfileID = paymentProfile.paymentProfileID ?? ""
        let cardHolderName = paymentProfile.cardHolderName ?? ""
        let cvc = paymentProfile.cvc ?? ""
        let cardNumber = paymentProfile.cardNumber ?? ""
        let expiration = paymentProfile.expiration ?? ""
        let phoneNumber = StringUtility.discardNoNumber(paymentProfile.phoneNumber ?? "")
        let email = paymentProfile.email ?? ""
        let usePersonEmail = paymentProfile.usePersonEmail ? "1" : "0"
        let counrtyName = paymentProfile.personAddress.country.name ?? ""
        let billingAddress = paymentProfile.personAddress.billingAddress ?? ""
        let usePersonAddress = paymentProfile.usePersonAddress ? "1" : "0"
        let city = paymentProfile.personAddress.city ?? ""
        let stateName = paymentProfile.personAddress.state.name ?? ""
        let zipCode = paymentProfile.personAddress.zipCode ?? ""
        let parameters = ["payment_profile_id": paymentProfileID,
                          "cardholder": cardHolderName,
                          "cvc": cvc,
                          "card_number": cardNumber,
                          "expiration": expiration,
                          "is_paypal": "",
                          "total_price": totalPrice,
                          "phone_number": phoneNumber,
                          "email_address": email,
                          "use_person_email": usePersonEmail,
                          "country": counrtyName,
                          "address": billingAddress,
                          "use_person_address": usePersonAddress,
                          "city": city,
                          "state": stateName,
                          "zip_code": zipCode,
                          "is_mobile": "1"]
        Client.sendRequest(.Post, name: "payment/amount/add.do", parameters: parameters, headers: nil) { (response, error) in
            guard executive.validate(response, error: error) != nil else {
                completionBlock(false, nil)
                return
            }
            if let message = executive.messageFromResponse(response), let plainText = StringUtility.htmlToText(encodedString: message) {
                completionBlock(true, plainText)
            }
            completionBlock(true, nil)
        }
    }
    
    /**
     *  Proceed prepay by paypal
     *  @param countryId: Country Id of which states
     *  @param successBlock: The block that is called when request is completed.
     *  @param executive: Common executive object
     */
    
    public func proceedPrepayByPaypal(_ completionBlock:@escaping ((Bool, String?) -> Swift.Void), executive: CommonExecutive, paymentId: String) {
    }
}
