//
//  Style.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/22/17.
//
//

import UIKit
import REFormattedNumberField

//
// Configure Hex Color Abbreviation.
//

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: a)
    }
    
    convenience init(netHex:Int, a: CGFloat = 1.0) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff, a: a)
    }
}

//
// Arial Fonts.
//

let ArialRegular = UIFont(name: "ArialMT", size: 20)
let ArialBold = UIFont(name: "Arial-BoldMT", size: 20)
let ArialItalic = UIFont(name: "Arial-ItalicMT", size: 20)

//
// Best Fitting Fonts for buttons, labels, calendar, date bar etc
//

let MainControlSpacing = CGFloat(16)

//
// Color Palette.
//

let QuickPayWhite = UIColor(netHex: 0xFFFFFF)
let QuickPayClear = UIColor.clear
let QuickPayPink = UIColor(netHex: 0x0b0080)
let QuickPayBlue = UIColor(netHex: 0x0645AD)
let QuickPayGray1 = UIColor(netHex: 0xFAFAFA)
let QuickPayGray2 = UIColor(netHex: 0xEEEEEE)
let QuickPayGray3 = UIColor(netHex: 0xC8C8C8)
let QuickPayGray4 = UIColor(netHex: 0x646C77)
let QuickPayGreen = UIColor(netHex: 0x70A01C)

struct QuickPayStyle {
    
    //
    // Adjustable size constants.
    //
    
    static let LargeTabletHeight = CGFloat(1366)
    static let SmallTabletHeight = CGFloat(1024)
    static let BasePhoneHeight = CGFloat(667)
    static let SmallPhoneHeight = CGFloat(568)
    static let LargeTabletMultiplier = CGFloat(1.5)
    static let SmallTabletMultipler = CGFloat(1.3)
    static let SmallPhoneFontSizeSubtractionValue = CGFloat(4)
    static let ExtraSmallPhoneFontSizeSubtractionValue = CGFloat(6)
    
    //
    // Adjust font sizes based on screen size.
    //
    
    static func bestFittingFontSize(_ baseFontSize: CGFloat) -> CGFloat {
        assert(baseFontSize > ExtraSmallPhoneFontSizeSubtractionValue)
        
        let screenSize = UIScreen.main.bounds.size
        
        if (screenSize.height >= LargeTabletHeight) {
            return LargeTabletMultiplier * baseFontSize
        }
        
        if (screenSize.height >= SmallTabletHeight) {
            return SmallTabletMultipler * baseFontSize
        }
        
        if (screenSize.height >= BasePhoneHeight) {
            return baseFontSize
        }
        
        if (screenSize.height >= SmallPhoneHeight) {
            return baseFontSize - SmallPhoneFontSizeSubtractionValue
        }
        
        return baseFontSize - ExtraSmallPhoneFontSizeSubtractionValue
    }
}

//
// Customized View
//

class ViewRounded: UIView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        clipsToBounds = true
        layer.cornerRadius = 3.0
    }
}

class ViewCommonContent: UIView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        clipsToBounds = true
        backgroundColor = QuickPayGray1
        layer.borderWidth = 1.0
        layer.borderColor = QuickPayGray2.cgColor
    }
}

class ViewSeparatorGray: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = QuickPayGray1
    }
}

//
// Customized Label
//

class LabelRegular9: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(9)
    }
}

class LabelRegular10: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(10)
    }
}

class LabelRegular11: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(11)
    }
}

class LabelRegular12: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(12)
    }
}

class LabelRegular13: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(13)
    }
}

class LabelRegular14: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(14)
    }
}

class LabelRegular16: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(16)
    }
}

class LabelRegular18: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(18)
    }
}

class LabelRegular20: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(20)
    }
}

class LabelRegular32: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(32)
    }
}

class LabelBold14: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialBold
        baseFontSize = CGFloat(14)
    }
}

class LabelBold16: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialBold
        baseFontSize = CGFloat(16)
    }
}

class LabelBold18: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialBold
        baseFontSize = CGFloat(18)
    }
}

class LabelCommon: CustomFontSizeLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderWidth = 1.0
        layer.borderColor = QuickPayGray3.cgColor
        layer.cornerRadius = 2.0
    }
}

class LabelRoundedBorderRegular12: LabelCommon {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(12)
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 6)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
}

class LabelRoundedBorderRegular16: LabelCommon {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(16)
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
}

//
// Customized Button
//

class ButtonRegular12: CustomFontSizeButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        titleLabel?.font = ArialRegular
        baseFontSize = CGFloat(12)
    }
}

class ButtonRounded: CustomFontSizeButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.cornerRadius = 4.0
    }
}

class ButtonRoundedGrayBorder: ButtonRounded {
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.borderColor = QuickPayGray3.cgColor
        layer.borderWidth = 1.0
    }
}

class ButtonRegular14Tab: ButtonRounded {
    override var isSelected: Bool {
        didSet {
            inflectSelection()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setTitleColor(QuickPayPink, for: .normal)
        setTitleColor(QuickPayWhite, for: .selected)
        titleLabel?.font = ArialRegular
        baseFontSize = CGFloat(14)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        inflectSelection()
    }
    
    func inflectSelection() {
        if isSelected {
            backgroundColor = QuickPayPink
        }
        else {
            backgroundColor = QuickPayClear
        }
    }
}

class ButtonRegular14RoundedGrayBorder: ButtonRoundedGrayBorder {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        titleLabel?.font = ArialRegular
        baseFontSize = CGFloat(14)
    }
}

class ButtonRegular16RoundedGrayBorder: ButtonRoundedGrayBorder {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        titleLabel?.font = ArialRegular
        baseFontSize = CGFloat(16)
    }
}

class ButtonRegular16Rounded: ButtonRounded {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    
        titleLabel?.font = ArialRegular
        baseFontSize = CGFloat(16)
    }
}

class ButtonRegular16: CustomFontSizeButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        titleLabel?.font = ArialRegular
        baseFontSize = CGFloat(16)
    }
}

class ButtonItalic16: CustomFontSizeButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        titleLabel!.font = ArialItalic
        baseFontSize = CGFloat(16)
    }
}

//
// Customized Text Field
//

class TextFieldCommon: CustomFontSizeTextField {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderWidth = 1.0
        layer.borderColor = QuickPayGray3.cgColor
        layer.cornerRadius = 2.0
        let paddingRect = CGRect(x: 0, y: 0, width: 4.0, height: 1)
        let paddingLeftView = UIView(frame: paddingRect)
        let paddingRightView = UIView(frame: paddingRect)
        leftView = paddingLeftView
        rightView = paddingRightView
        leftViewMode = .always
        rightViewMode = .always
    }
}

class TextFieldRegular12: TextFieldCommon {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(12)
    }
}

class TextFieldRegular16: TextFieldCommon {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(16)
    }
}

class FormattedNumberFieldCommon: CustomFontSizeFormattedNumberField {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderWidth = 1.0
        layer.borderColor = QuickPayGray3.cgColor
        layer.cornerRadius = 2.0
        let paddingRect = CGRect(x: 0, y: 0, width: 4.0, height: 1)
        let paddingLeftView = UIView(frame: paddingRect)
        let paddingRightView = UIView(frame: paddingRect)
        leftView = paddingLeftView
        rightView = paddingRightView
        leftViewMode = .always
        rightViewMode = .always
    }
}

class FormattedNumberFieldRegular16: FormattedNumberFieldCommon {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(16)
    }
}

class PhoneNumberFieldRegular16: FormattedNumberFieldCommon {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        font = ArialRegular
        baseFontSize = CGFloat(16)
        format = "XXX-XXX-XXXX"
        placeholder = "XXX-XXX-XXXX"
    }
}

//
// Custom font size classes.
//

class CustomFontSizeLabel: UILabel {
    var baseFontSize: CGFloat = 12 {
        didSet {
            updateFontSize()
        }
    }
    
    func updateFontSize() {
        let fontSize = QuickPayStyle.bestFittingFontSize(baseFontSize)
        let newFont = UIFont(name: font.fontName, size: fontSize)
        font = newFont
    }
}


class CustomFontSizeButton: UIButton {
    var baseFontSize: CGFloat = 0 {
        didSet {
            updateFontSize()
        }
    }
    
    func updateFontSize() {
        let fontSize = QuickPayStyle.bestFittingFontSize(baseFontSize)
        let newFont = UIFont(name: (titleLabel?.font.fontName)!, size: fontSize)
        titleLabel!.font = newFont
    }
}

class CustomFontSizeTextField: UITextField {
    var baseFontSize: CGFloat = 0 {
        didSet {
            updateFontSize()
        }
    }
    
    func updateFontSize() {
        let fontSize = QuickPayStyle.bestFittingFontSize(baseFontSize)
        let newFont = UIFont(name: (font!.fontName), size: fontSize)
        font = newFont
    }
}

class CustomFontSizeFormattedNumberField: REFormattedNumberField {
    var baseFontSize: CGFloat = 0 {
        didSet {
            updateFontSize()
        }
    }
    
    func updateFontSize() {
        let fontSize = QuickPayStyle.bestFittingFontSize(baseFontSize)
        let newFont = UIFont(name: (font!.fontName), size: fontSize)
        font = newFont
    }
}

//
// Customized Image View
//

class CircularGrayBorderedImageView: UIImageView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        clipsToBounds = true
        layer.borderColor = QuickPayGray4.cgColor
        layer.borderWidth = 1.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2.0
    }
}

//
// Inmates Table View Style
//

class InmatesTableViewStyle {
    static let EstimatedHeight = CGFloat(144.0)
}

//
// Left Side menu style
//

class LeftSideMenuStyle {
    static let EstimatedHeight = CGFloat(144.0)
}
