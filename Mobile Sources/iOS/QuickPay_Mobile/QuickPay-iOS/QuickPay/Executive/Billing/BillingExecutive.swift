//
//  BillingExecutive.swift
//  QuickPay
//
//  Created by Michael Lee on 6/4/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation
import SwiftyJSON

//
// Display Context
//

enum BillingType: String {
    case Invoice = "Invoice"
    case Payment = "Payment"
}

enum BillingActor: String {
    case Customer = "Customer"
    case Inmate = "Inmate"
}

class BillingDisplayContext {
    var actor = BillingActor.Customer
    var title = ""
    var subtitle = ""
    var filterContext = BillingFilterDisplayContext()
}

//
// Display
//

protocol BillingDisplay: CommonDisplay {
    func refreshDisplay(_ customerContext: BillingDisplayContext, inmateContext: BillingDisplayContext, selectedActor: BillingActor)
    func showCalendar(_ selectedDate: Date, dateType: DateType)
    func getBillingRequest(_ actor: BillingActor?) -> BillingRequest
    func getCustomerFilterDisplayContext() -> BillingFilterDisplayContext
    func getInmateFilterDisplayContext() -> BillingFilterDisplayContext
    func reloadBillingTable(_ actor: BillingActor?, billingResponseArray: BillingResponseArray)
    func gotoRefund(_ getRefundListRequest: RefundRequest)
}

//
// Executive
//

class BillingExecutive: CommonExecutive {
    
    private static let InvoiceTitle = "View Invoices"
    private static let InvoiceSubTitle = "Browse All Invoices"
    private static let PaymentTitle = "View Payments"
    private static let PaymentSubTitle = "Browse All Payment Details"
    
    var customerDisplayContext = BillingDisplayContext()
    var inmateDisplayContext = BillingDisplayContext()
    var selectedActor: BillingActor = .Customer
    
    weak var billingDisplay: BillingDisplay? {
        get {
            return display as? BillingDisplay
        }
    }
    
    // MARK: - Event Handler
    
    func displayDidLoad(_ customerContext: BillingDisplayContext, inmateContext: BillingDisplayContext) {
        customerDisplayContext = customerContext
        inmateDisplayContext = inmateContext
        customerDisplayContext.title = getDisplayTitles().title
        customerDisplayContext.subtitle = getDisplayTitles().subtitle
        refreshDisplay()
        display?.showLoading(true)
        loadBilling(.Customer) { (isSuccess, billingResponseArray) in
            if !isSuccess {
                self.display?.showLoading(false)
                return
            }
            if let customerBillings = billingResponseArray {
                DispatchQueue.main.async {
                    self.billingDisplay?.reloadBillingTable(.Customer, billingResponseArray: customerBillings)
                }
            }
            self.loadBilling(.Inmate, completionBlock: { (isSuccess, billingResponseArray) in
                self.display?.showLoading(false)
                if !isSuccess {
                    return;
                }
                if let inmateBillings = billingResponseArray {
                    DispatchQueue.main.async {
                        self.billingDisplay?.reloadBillingTable(.Inmate, billingResponseArray: inmateBillings)
                    }
                }
            })
        }
    }
    
    func didPressCusomterButton() {
        selectedActor = .Customer
        if let inmateFilterDisplayContext = billingDisplay?.getInmateFilterDisplayContext() {
            inmateDisplayContext.filterContext = inmateFilterDisplayContext
        }
        refreshDisplay()
    }
    
    func didPressInmateButton() {
        selectedActor = .Inmate
        if let customerFilterDisplayContext = billingDisplay?.getCustomerFilterDisplayContext() {
            customerDisplayContext.filterContext = customerFilterDisplayContext
        }
        refreshDisplay()
    }
    
    func didPressDateButton(_ dateType: DateType) {
        var displayContext = customerDisplayContext
        if selectedActor == .Inmate {
            displayContext = inmateDisplayContext
        }
        var date = displayContext.filterContext.dateValue
        if dateType == .From {
            date = displayContext.filterContext.dateFromValue
        }
        else if dateType == .To {
            date = displayContext.filterContext.dateToValue
        }
        guard let currentDate = date else {
            billingDisplay?.showCalendar(Date(), dateType: dateType)
            return
        }
        billingDisplay?.showCalendar(currentDate, dateType: dateType)
    }
    
    override func didDateChanged(_ date: Date, type: DateType) {
        if selectedActor == .Customer {
            if let filterDisplayContext = billingDisplay?.getCustomerFilterDisplayContext() {
                customerDisplayContext.filterContext = filterDisplayContext
            }
            switch type {
            case .Current:
                customerDisplayContext.filterContext.dateValue = date
                customerDisplayContext.filterContext.dateFormatted = DateTimeUtil.stringFromDate(date, format: "MM/dd/yyyy")!
                break
            case .From:
                customerDisplayContext.filterContext.dateFromValue = date
                customerDisplayContext.filterContext.dateFromFormatted = DateTimeUtil.stringFromDate(date, format: "MM/dd/yyyy")!
                break
            case .To:
                customerDisplayContext.filterContext.dateToValue = date
                customerDisplayContext.filterContext.dateToFormatted = DateTimeUtil.stringFromDate(date, format: "MM/dd/yyyy")!
                break
            }
        }
        else {
            if let filterDisplayContext = billingDisplay?.getInmateFilterDisplayContext() {
                inmateDisplayContext.filterContext = filterDisplayContext
            }
            switch type {
            case .Current:
                inmateDisplayContext.filterContext.dateValue = date
                inmateDisplayContext.filterContext.dateFormatted = DateTimeUtil.stringFromDate(date, format: "MM/dd/yyyy")!
                break
            case .From:
                inmateDisplayContext.filterContext.dateFromValue = date
                inmateDisplayContext.filterContext.dateFromFormatted = DateTimeUtil.stringFromDate(date, format: "MM/dd/yyyy")!
                break
            case .To:
                inmateDisplayContext.filterContext.dateToValue = date
                inmateDisplayContext.filterContext.dateToFormatted = DateTimeUtil.stringFromDate(date, format: "MM/dd/yyyy")!
                break
            }
        }
        refreshDisplay()
    }
    
    // MARK: - Helper
    
    func getDisplayTitles() -> (title: String, subtitle: String) {
        var title = BillingExecutive.PaymentTitle
        var subtitle = BillingExecutive.PaymentSubTitle
        if customerDisplayContext.filterContext.type == .Invoice {
            title = BillingExecutive.InvoiceTitle
            subtitle = BillingExecutive.InvoiceSubTitle
        }
        return (title, subtitle)
    }
    
    func refreshDisplay() {
        billingDisplay?.refreshDisplay(customerDisplayContext, inmateContext: inmateDisplayContext, selectedActor: selectedActor)
    }
    
    // MARK: - Load Billings
    
    func loadBilling(_ actor: BillingActor? = nil, completionBlock:@escaping ((Bool, BillingResponseArray?) -> Swift.Void)) {
        guard let request = billingDisplay?.getBillingRequest(actor) else {
            completionBlock(false, nil)
            return
        }
        request.send(executive: self) { (isSuccess, billingResponseArray) in
            completionBlock(isSuccess, billingResponseArray)
        }
    }
    
    func loadBilling() {
        guard let request = billingDisplay?.getBillingRequest(selectedActor) else {
            return
        }
        display?.showLoading(true)
        request.send(executive: self) { (isSuccess, billingResponseArray) in
            self.display?.showLoading(false)
            if let billings = billingResponseArray {
                DispatchQueue.main.async {
                    self.billingDisplay?.reloadBillingTable(self.selectedActor, billingResponseArray: billings)
                }
            }
        }
    }
    
    func gotoRefund(_ paymentId: Int?) {
        guard let paymentId = paymentId else {
            return
        }
        let getRefundListRequest = RefundRequest()
        getRefundListRequest.paymentId = paymentId
        getRefundListRequest.actor = selectedActor
        billingDisplay?.gotoRefund(getRefundListRequest)
    }
}
