//
//  BillingFilterExecutive.swift
//  QuickPay
//
//  Created by Michael Lee on 6/12/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation

//
// Operator
//

enum OperatorType: String {
    case Equal = "="
    case GreaterThanEqual = ">="
    case LessThanEqual = "<="
    case Between = "~"
}

enum InvoiceTypeFilter: String {
    case All = "All"
    case SetupFee = "Setup Fee"
    case MonthlyFee = "Monthly Fee"
    case CallLog = "Call Log"
    case ReferralFee = "Referral Fee"
}

enum PaymentTypeFilter: String {
    case All = "All"
    case CreditCard = "CreditCard"
    case PayPal = "PayPal"
    case BP199 = "BP199"
    case FreeTrialMinutes = "Free Trial Minutes"
    case WesternUnion = "Western Union"
    case PendingBP199 = "Pending BP199"
}

class BillingFilterDisplayContext {
    var type: BillingType = .Invoice
    var dateOperatorIndex = 0
    var dateValue: Date?
    var dateFormatted = ""
    var dateFromValue: Date?
    var dateFromFormatted = ""
    var dateToValue: Date?
    var dateToFormatted = ""
    var typeIndex = 0
    var amountOperatorIndex = 0
    var amountValue = ""
    var amountFromValue = ""
    var amountToValue = ""
    var balanceOperatorIndex = 0
    var balanceValue = ""
    var balanceFromValue = ""
    var balanceToValue = ""
    var expanded = false
}

//
// Display
//

protocol BillingFilterDisplay: CommonDisplay {
    func refreshLayout(_ context: BillingFilterDisplayContext)
    func loadDisplay(_ context: BillingFilterDisplayContext)
    func getFilterRequest() -> BillingFilterRequest
}

//
// Executive
//

class BillingFilterExecutive: CommonExecutive {
    
    static let FilterOperators: [OperatorType] =
        [.Equal,
         .GreaterThanEqual,
         .LessThanEqual,
         .Between]
    static let FilterOperatorValues: [Int] =
        [1,
         4,
         5,
         6]
    
    static let InvoiceTypeFilters: [InvoiceTypeFilter] =
        [.All,
         .SetupFee,
         .MonthlyFee,
         .CallLog,
         .ReferralFee]
    static let InvoiceTypeFilterValues: [Int] =
        [0,
         3,
         4,
         5,
         17]
    
    static let PaymentTypeFilters: [PaymentTypeFilter] =
        [.All,
         .CreditCard,
         .PayPal,
         .BP199,
         .FreeTrialMinutes,
         .WesternUnion,
         .PendingBP199]
    static let PaymentTypeFilterValues: [Int] =
        [0,
         1,
         2,
         10,
         11,
         12,
         15]
    
    var displayContext = BillingFilterDisplayContext()
    
    weak var billingFilterDisplay: BillingFilterDisplay? {
        get {
            return display as? BillingFilterDisplay
        }
    }
    
    // MARK: - Refresh Layout and Load Views
    
    func displayDidLoad(_ context: BillingFilterDisplayContext) {
        displayContext = context
        billingFilterDisplay?.loadDisplay(displayContext)
    }
    
    func refreshLayout(_ context: BillingFilterDisplayContext) {
        displayContext = context
        billingFilterDisplay?.refreshLayout(displayContext)
    }
    
    // MARK: - Event Handler
    
    func didPressViewButton() {
        displayContext.expanded = false
        billingFilterDisplay?.refreshLayout(displayContext)
    }
    
    func didPressExpandButton() {
        displayContext.expanded = !displayContext.expanded
        billingFilterDisplay?.refreshLayout(displayContext)
    }
}
