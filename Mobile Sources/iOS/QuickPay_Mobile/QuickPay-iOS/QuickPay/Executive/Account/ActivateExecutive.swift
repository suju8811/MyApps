//
//  ActivateExecutive.swift
//  QuickPay
//
//  Created by Michael Lee on 5/21/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation
import SwiftyJSON

//
// Display
//

protocol ActivateDisplay: CommonDisplay {
    func activated()
    func getActivateRequest() -> ActivateRequest
    func displayVerificationMethodSelectMessage(_ parameters: [String : Any]?)
}

//
// Executive
//

class ActivateExecutive: CommonExecutive {
    
    private static let EmptyUserName = "Please input user name"
    private static let EmptyPassword = "Please input password"
    private static let EmptyActivationCode = "Please input activation code"
    
    weak var activateDisplay: ActivateDisplay? {
        get {
            return display as? ActivateDisplay
        }
    }
    var sessionUserName: String?
    
    func activate() {
        guard let request = activateDisplay?.getActivateRequest() else {
            return
        }
        if let validateString = validate(request) {
            display?.showError(validateString)
            return
        }
        guard let userName = request.userInfo.commonInfo.name, let password = request.userInfo.password else {
            return
        }
        display?.showLoading(true)
        let parameters = ["username": userName,
                          "password": password,
                          "activation_code": request.activationCode,
                          "is_mobile": "1"]
        Client.sendRequest(.Post, name: "customer/activate_user.do", parameters: parameters, headers: nil) { (response, error) in
            self.display?.showLoading(false)
            guard self.validate(response, error: error) != nil else {
                return
            }
            self.activateDisplay?.activated()
        }
    }
    
    func resendActivation() {
        if let userName = sessionUserName {
            if userName.isEmpty {
                sessionUserName = activateDisplay?.getActivateRequest().userInfo.commonInfo.name
            }
        }
        else {
            sessionUserName = activateDisplay?.getActivateRequest().userInfo.commonInfo.name
        }
        
        if let validateString = validateSessionUserName() {
            display?.showError(validateString)
            return
        }
        let parameters = ["username": sessionUserName!]
        activateDisplay?.displayVerificationMethodSelectMessage(parameters)
    }
    
    func sendResendActivationRequest(_ parameters: [String : Any]?, isPhone: Bool) {
        var newParameter = parameters
        if isPhone {
            newParameter?["is_phone"] = "1"
        }
        else {
            newParameter?["is_phone"] = "0"
        }
        display?.showLoading(true)
        Client.sendRequest(.Post, name: "customer/resend_activation.do", parameters: newParameter, headers: nil) { (response, error) in
            self.display?.showLoading(false)
            guard let message = self.messageFromResponse(response) else {
                return
            }
            self.display?.showError(message)
        }
    }
    
    func validateSessionUserName() -> String? {
        guard let userName = sessionUserName, !userName.isEmpty else {
            return ActivateExecutive.EmptyUserName
        }
        return nil
    }
    
    func validate(_ request: ActivateRequest) -> String? {
        guard let userName = request.userInfo.commonInfo.name, !userName.isEmpty else {
            return ActivateExecutive.EmptyUserName
        }
        guard let password = request.userInfo.password, !password.isEmpty else {
            return ActivateExecutive.EmptyPassword
        }
        if request.activationCode.isEmpty {
            return ActivateExecutive.EmptyActivationCode
        }
        return nil
    }
}
