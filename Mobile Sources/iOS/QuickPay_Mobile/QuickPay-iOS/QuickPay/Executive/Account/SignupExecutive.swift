//
//  SignupExecutive.swift
//  QuickPay
//
//  Created by Michael Lee on 5/20/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation

//
// Display
//

protocol SignupDisplay: CommonDisplay {
    
    func signedup(_ message: String)
    func getSignupRequest() -> SignupRequest
    func displayVerificationMethodSelectMessage(_ parameters: [String : Any]?)
}

//
// Executive
//

class SignUpExecutive: CommonExecutive {
    
    private static let EmptyUserName = "Please input user name"
    private static let EmptyPassword = "Please input password"
    private static let MismatchPasswords = "Password mismatch"
    private static let EmptyEmailAddressOrPhoneNumber = "Please input email address or phone number"
    private static let InvalidEmailAddress = "Please input valid email address"
    private static let InvalidPhoneNumber = "Please input valid phone number"
    private static let AgreeTermsAndPrivacy = "Please agree with the terms and privacy"
    
    weak var signupDisplay: SignupDisplay? {
        get {
            return display as? SignupDisplay
        }
    }
    
    func signup() {
        guard let request = signupDisplay?.getSignupRequest() else {
            return
        }
        if let validateString = validate(request) {
            display?.showError(validateString)
            return
        }
        guard let userName = request.userInfo.commonInfo.name, let password = request.userInfo.password else {
            return
        }
        let phone = StringUtility.discardNoNumber(request.userInfo.commonInfo.phone ?? "")
        let parameters = ["username": userName,
                          "password": password,
                          "email_address": request.userInfo.commonInfo.email ?? "",
                          "phone_number": phone,
                          "is_mobile": "1"]
        signupDisplay?.displayVerificationMethodSelectMessage(parameters)
    }
    
    func sendSignupRequest(_ parameters: [String: Any]?, isPhone: Bool) {
        var newParameter = parameters
        if isPhone {
            newParameter?["is_phone"] = "1"
        }
        else {
            newParameter?["is_phone"] = "0"
        }
        display?.showLoading(true)
        Client.sendRequest(.Post, name: "customer/sign_up.do", parameters: newParameter, headers: nil) { (response, error) in
            self.display?.showLoading(false)
            guard self.validate(response, error: error) != nil else {
                return
            }
            guard let message = self.messageFromResponse(response) else {
                return
            }
            self.signupDisplay?.signedup(message)
        }
    }
    
    func validate(_ request: SignupRequest) -> String? {
        var result: String?
        guard let userName = request.userInfo.commonInfo.name, !userName.isEmpty else {
            return SignUpExecutive.EmptyUserName
        }
        guard let password = request.userInfo.password, !password.isEmpty else {
            return SignUpExecutive.EmptyPassword
        }
        if password != request.confirmPassword {
            return SignUpExecutive.MismatchPasswords
        }
        guard let email = request.userInfo.commonInfo.email, let phone = request.userInfo.commonInfo.phone else {
            return SignUpExecutive.EmptyEmailAddressOrPhoneNumber
        }
        if email.isEmpty && phone.isEmpty {
            return SignUpExecutive.EmptyEmailAddressOrPhoneNumber
        }
        if !email.isEmpty && !StringUtility.validate(email: email) {
            result = SignUpExecutive.InvalidEmailAddress
        }
        if !phone.isEmpty && !StringUtility.validate(phone: phone) {
            return SignUpExecutive.InvalidPhoneNumber
        }
        else if !request.isAcceptTerms {
            result = SignUpExecutive.AgreeTermsAndPrivacy
        }
        return result
    }
}
