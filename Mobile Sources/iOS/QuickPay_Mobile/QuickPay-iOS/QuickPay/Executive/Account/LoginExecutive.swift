//
//  LoginExecutive.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/28/17.
//
//

import UIKit
import SwiftyJSON

//
// Display
//

protocol LoginDisplay: CommonDisplay {
    
    func loggedIn()
    func getLoginRequest() -> User
}

//
// Executive
//

class LoginExecutive: CommonExecutive {
    
    private static let EmptyUserName = "Please input user name"
    private static let EmptyPassword = "Please input password"
    
    weak var loginDisplay: LoginDisplay? {
        get {
            return display as? LoginDisplay
        }
    }
    
    func login() {
        guard let userInfo = loginDisplay?.getLoginRequest() else {
            return
        }
        if let validateString = validate(userInfo) {
            display?.showError(validateString)
            return
        }
        guard let userName = userInfo.commonInfo.name, let password = userInfo.password else {
            return
        }
        display?.showLoading(true)
        let parameters = ["username": userName,
                          "password": password,
                          "is_mobile": "1"]
        Client.sendRequest(.Post, name: "customer/sign_in_direct.do", parameters: parameters, headers: nil) { (response, error) in
            self.display?.showLoading(false)
            guard let result = self.validate(response, error: error) else {
                return
            }
            
            AppData.sharedInstance.user = User()
            AppData.sharedInstance.user?.setData(from: result)
            self.loginDisplay?.loggedIn()
        }
    }
    
    func forgotPassword(userName: String) {
        if let validateString = validate(userName: userName) {
            display?.showError(validateString)
            return
        }
        display?.showLoading(true)
        let parameters = ["email": userName]
        Client.sendRequest(.Post, name: "customer/forget_password.do", parameters: parameters, headers: nil) { (response, error) in
            self.display?.showLoading(false)
            guard self.validate(response, error: error) != nil else {
                return
            }
            guard let message = self.messageFromResponse(response) else {
                return
            }
            self.display?.showError(message)
        }
    }
    
    func validate(userName: String) -> String? {
        var result:String?
        if userName.isEmpty {
            result = LoginExecutive.EmptyUserName
        }
        return result
    }
    
    func validate(_ userInfo: User) -> String? {
        guard let userName = userInfo.commonInfo.name, !userName.isEmpty else {
            return LoginExecutive.EmptyUserName
        }
        guard let password = userInfo.password, !password.isEmpty else {
            return LoginExecutive.EmptyPassword
        }
        return nil
    }
}
