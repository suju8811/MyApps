//
//  AccountExecutive.swift
//  QuickPay
//
//  Created by Michael Lee on 6/6/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation
import SwiftyJSON

//
// Display
//

protocol AccountDisplay: CommonDisplay {
    func loadDisplay(_ acountDisplayContext: AccountDisplayContext)
    func resetPassword()
    func getEditProfileRequest() -> EditProfileRequest
    func getChangePasswordRequest() -> ChangePasswordRequest
    func displayQuickPayRecommendAlert()
}

//
// Executive
//

class AccountExecutive: CommonExecutive {
    
    private static let EmptyFirstName = "Please input first name"
    private static let EmptyLastName = "Please input last name"
    private static let InvalidEmail = "Please input valid email address"
    private static let EmptyCountry = "Please input country"
    private static let InvalidPhoneNumber = "Please enter valid phone number"
    private static let EmptyOldPassword = "Please input old password"
    private static let EmptyNewPassword = "Please input new password"
    private static let PasswordMismatch = "Password mismatch"
    
    weak var accountDisplay: AccountDisplay? {
        get {
            return display as? AccountDisplay
        }
    }
    
    var accountDisplayContext = AccountDisplayContext()
    
    func displayDidLoad(_ personAddressDisplayContext: PersonAddressDisplayContext) {
        self.accountDisplayContext.personAddressDisplayContext = personAddressDisplayContext
        let appData = AppData.sharedInstance
        if let personAddress = appData.user?.personAddress {
            self.accountDisplayContext.personAddressDisplayContext.personAddress = personAddress.copy() as! PersonAddress
        }
        if let commonUserInfo = appData.user?.commonInfo {
            self.accountDisplayContext.commonUserInfo = commonUserInfo.copy() as! CommonUserInfo
        }
        let country = self.accountDisplayContext.personAddressDisplayContext.personAddress.country
        if let countryName = country.name,
            let countryId = country.id {
            let countriesString = appData.countries.map({ $0.name?.lowercased() ?? "" })
            self.accountDisplayContext.personAddressDisplayContext.countryState.selectedCountry = countriesString.index(of: countryName.lowercased())
            State.loadStates(countryId, successBlock: { (states) in
                if let states = states {
                    self.accountDisplayContext.personAddressDisplayContext.countryState.states = states
                    let stateIds = states.map({ $0.id ?? -1 })
                    let state = self.accountDisplayContext.personAddressDisplayContext.personAddress.state
                    if let stateId = state.id {
                        self.accountDisplayContext.personAddressDisplayContext.countryState.selectedState = stateIds.index(of: stateId)
                    }
                }
                DispatchQueue.main.async {
                    self.accountDisplay?.loadDisplay(self.accountDisplayContext)
                }
            }, executive: self)
            return
        }
        accountDisplay?.loadDisplay(self.accountDisplayContext)
    }
    
    func didPressAddPrepayAmountButton() {
        accountDisplay?.displayQuickPayRecommendAlert()
    }
    
    func didPressResetButton() {
        accountDisplayContext.initialize()
        accountDisplay?.loadDisplay(accountDisplayContext)
    }
    
    func didPressUpdateProfileButton() {
        guard let request = accountDisplay?.getEditProfileRequest() else {
            return
        }
        if let validateString = validateEditProfile(request) {
            display?.showError(validateString)
            return
        }
        display?.showLoading(true)
        request.send(executive: self) { (isSuccess, message) in
            self.display?.showLoading(false)
            if isSuccess {
                if let message = message {
                    self.display?.showError(message)
                }
            }
        }
    }
    
    func didPressResetPasswordButton() {
        accountDisplay?.resetPassword()
    }
    
    func didPressUpdatePasswordButton() {
        guard let request = accountDisplay?.getChangePasswordRequest() else {
            return
        }
        if let validateString = validateChangePassword(request) {
            display?.showError(validateString)
            return
        }
        display?.showLoading(true)
        request.send(executive: self) { (isSuccess, message) in
            self.display?.showLoading(false)
            if isSuccess {
                if let message = message {
                    self.display?.showError(message)
                }
            }
        }
    }
    
    func validateEditProfile(_ request: EditProfileRequest) -> String? {
        guard let firstName = request.commonInfo.firstName,
            !firstName.isEmpty else {
            return AccountExecutive.EmptyFirstName
        }
        guard let lastName = request.commonInfo.lastName,
            !lastName.isEmpty else {
                return AccountExecutive.EmptyLastName
        }
        if let email = request.commonInfo.email,
            !email.isEmpty,
            !StringUtility.validate(email: email) {
            return AccountExecutive.InvalidEmail
        }
        if let phone = request.commonInfo.phone,
            !phone.isEmpty,
            !StringUtility.validate(phone: phone) {
            return AccountExecutive.InvalidPhoneNumber
        }
        guard request.addressInfo.country.id != nil else {
                return AccountExecutive.EmptyCountry
        }
        return nil
    }
    
    func validateChangePassword(_ request: ChangePasswordRequest) -> String? {
        if request.oldPassword.isEmpty {
            return AccountExecutive.EmptyOldPassword
        }
        if request.newPassword.isEmpty {
            return AccountExecutive.EmptyNewPassword
        }
        if request.newPassword != request.confirmPassword {
            return AccountExecutive.PasswordMismatch
        }
        return nil
    }
}
