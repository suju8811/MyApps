//
//  PersonAddressExecutive.swift
//  QuickPay
//
//  Created by Michael Lee on 6/5/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation
import SwiftyJSON

//
// Display Context
//

class CountryStateDisplayContext {
    
    var selectedCountry: Int?
    var selectedState: Int?
    var states = [State]()
    
    /**
     *  Initialize values
     */
    
    func initialize() {
        selectedState = nil
        selectedCountry = nil
        states.removeAll()
    }
}

class PersonAddressDisplayContext {
    
    var countryState = CountryStateDisplayContext()
    var personAddress = PersonAddress()
    
    /**
     *  Initialize values
     */
    
    func initialize() {
        countryState.initialize()
        personAddress.initialize()
    }
}

class AccountDisplayContext {
    
    var personAddressDisplayContext = PersonAddressDisplayContext()
    var commonUserInfo = CommonUserInfo()
    
    /**
     *  Initialize values
     */
    
    func initialize() {
        personAddressDisplayContext.initialize()
        commonUserInfo.initialize()
    }
}

//
// Display
//

protocol PersonAddressDisplay: CommonDisplay {
    func refreshLayout(_ context: PersonAddressDisplayContext)
    func loadDisplay(_ context: PersonAddressDisplayContext)
    func getPersonAddressInfo() -> PersonAddress
    func reloadStateDropDown(_ context: PersonAddressDisplayContext)
}

//
// Executive
//

class PersonAddressExecutive: CommonExecutive {
    
    weak var personAddressDisplay: PersonAddressDisplay?
    var context = PersonAddressDisplayContext()
    
    func refreshDisplay(_ displayContext: PersonAddressDisplayContext) {
        context = displayContext
        personAddressDisplay?.loadDisplay(context)
    }
    
    func refreshLayout(_ displayContext: PersonAddressDisplayContext) {
        context = displayContext
        personAddressDisplay?.refreshLayout(context)
    }
    
    func didSelectCountry(_ index: Int?) {
        guard let index = index,
            let countryId = AppData.sharedInstance.countries[index].id else {
                context.countryState.states.removeAll()
                context.countryState.selectedState = nil
                personAddressDisplay?.reloadStateDropDown(context)
                return
        }
        display?.showLoading(true)
        State.loadStates(countryId, successBlock: { (states) in
            self.display?.showLoading(false)
            self.context.countryState.states = states ?? [State]()
            self.context.countryState.selectedState = nil
            DispatchQueue.main.async {
                self.personAddressDisplay?.reloadStateDropDown(self.context)
            }
        }, executive: self)
    }
}
