//
//  CommonExecutive.swift
//  QuickPay
//
//  Created by Michael Lee on 5/20/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation
import SwiftyJSON

//
// Common Display
//

protocol CommonDisplay: class {
    func showLoading(_ isShow: Bool)
    func showError(_ message: String)
}

//
// Common Exectuive
//

class CommonExecutive: NSObject {
    
    weak var display: CommonDisplay?

    init(_ display: CommonDisplay) {
        self.display = display
    }
    
    func validate(_ response: Any?, error: String?, dataColumn: String = "result") -> JSON? {
        
        if let error = error {
            self.display?.showError(error)
            return nil
        }
        guard let response = response else {
            return nil
        }
        return JSON(response)[dataColumn]
    }
    
    func messageFromResponse(_ response: Any?) -> String? {
        guard let response = response else {
            return nil
        }
        return JSON(response)["message"].string
    }
    
    func didDateChanged(_ date: Date, type: DateType) {
    }
}
