//
//  ContainerExecutive.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/30/17.
//
//

import UIKit

//
// Display
//

protocol ContainerDisplay: class {
    
    func showLeftSideMenu()
    
    func gotoViewController(_ item: SideMenuItemIndex)
}

class ContainerExecutive: NSObject {
    
    weak var display: ContainerDisplay?
    var selectedSideMenuIndex: SideMenuItemIndex = .QuickPay
    
    func didPressSideMenu(_ selectedItem: SideMenuItemIndex) {
        selectedSideMenuIndex = selectedItem
        display?.showLeftSideMenu()
    }
    
    func gotoViewController(_ item: SideMenuItemIndex) {
        if (selectedSideMenuIndex == item) || (item == .Billing) {
            return
        }
        selectedSideMenuIndex = item
        display?.gotoViewController(item)
    }
}
