//
//  LeftSideMenuExecutive.swift
//  ReserveIn
//
//  Created by Michael Lee on 3/23/17.
//
//

import UIKit

//
// Side Menu Item
//

class LeftSideMenuItem {
    let type: SideMenuItemType
    let index: SideMenuItemIndex
    let title: String?
    let normalImage: UIImage?
    let selectImage: UIImage?
    
    init(type: SideMenuItemType,
         index: SideMenuItemIndex,
         title: String?,
         normalImage: UIImage?,
         selectImage: UIImage?) {
        self.type = type
        self.index = index
        self.title = title
        self.normalImage = normalImage
        self.selectImage = selectImage
    }
}

//
// Side Menu Items
//

enum SideMenuItemType {
    case Item
    case Separator
}

enum SideMenuItemIndex: Int {
    case QuickPay    = 0
    case Account
    case CreditCard
    case Billing
    case Invoices
    case Payments
    case Logout
}

//
// Display
//

protocol LeftSideMenuDisplay: class {
    
    /**
     * Reload side menu items
     */
    
    func reloadItems(_ items: [LeftSideMenuItem]!)
    
    /**
     * Select side menu item
     */
    
    func selectedSideMenuItem(_ item: LeftSideMenuItem)
}

class LeftSideMenuExecutive: NSObject {
    
    weak var display: LeftSideMenuDisplay?
    
    /**
     * Called when view did load
     */
    
    func displayDidLoad() {
        
        var leftSideMenuItems = [LeftSideMenuItem].init()
        leftSideMenuItems.append(LeftSideMenuItem.init(type: .Item, index: .QuickPay, title: "QuickPay", normalImage: UIImage.init(named: "ic_quickpay"), selectImage: UIImage.init(named: "ic_quickpay_sel")))
        leftSideMenuItems.append(LeftSideMenuItem.init(type: .Item, index: .Account, title: "Account", normalImage: UIImage.init(named: "ic_account"), selectImage: UIImage.init(named: "ic_accout_sel")))
        leftSideMenuItems.append(LeftSideMenuItem.init(type: .Item, index: .CreditCard, title: "Credit Card", normalImage: UIImage.init(named: "ic_creditcard"), selectImage: UIImage.init(named: "ic_creditcard_sel")))
        leftSideMenuItems.append(LeftSideMenuItem.init(type: .Item, index: .Billing, title: "Billing", normalImage: UIImage.init(named: "ic_billing"), selectImage: UIImage.init(named: "ic_billing_sel")))
        leftSideMenuItems.append(LeftSideMenuItem.init(type: .Item, index: .Invoices, title: "Invoices", normalImage: nil, selectImage: nil))
        leftSideMenuItems.append(LeftSideMenuItem.init(type: .Item, index: .Payments, title: "Payments", normalImage: nil, selectImage: nil))
        leftSideMenuItems.append(LeftSideMenuItem.init(type: .Item, index: .Logout, title: "Logout", normalImage: UIImage.init(named: "ic_sign_out"), selectImage: UIImage.init(named: "ic_sign_out_sel")))
        display?.reloadItems(leftSideMenuItems)
    }
    
    /**
     * Called when item selected
     */
    
    func selectedSideMenuItem(_ item: LeftSideMenuItem) {
        display?.selectedSideMenuItem(item)
    }
}
