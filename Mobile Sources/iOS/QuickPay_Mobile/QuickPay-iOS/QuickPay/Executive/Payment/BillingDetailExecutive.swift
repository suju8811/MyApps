//
//  BillingDetailExecutive.swift
//  QuickPay
//
//  Created by Michael Lee on 6/15/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation

//
// Display
//

protocol BillingDetailDisplay: CommonDisplay {
    func loadLayout(_ context: PaymentProfile)
    func selectPaymentMethod(_ paymentMethod: PaymentMethod)
}

//
// Executive
//

class BillingDetailExecutive: CommonExecutive {
    
    private static let EmptyCardHolderName = "Please input card holder name"
    private static let EmptyCardNumber = "Please input card number"
    private static let EmptyExpiration = "Please input expiration"
    private static let EmptyCVC = "Please input CVC"
    private static let EmptyPhoneNumber = "Please input phone number"
    private static let InvalidPhoneNumber = "Please input valid phone number"
    private static let EmptyEmail = "Please input email"
    private static let InvalidEmail = "Please input valid email"
    private static let EmptyCountry = "Please select country"
    private static let EmptyBillingAddress = "Please input billing address"
    private static let EmptyCity = "Please input city"
    private static let EmptyZipCode = "Please input zip code"
    
    weak var billingDetailDisplay: BillingDetailDisplay? {
        get {
            return display as? BillingDetailDisplay
        }
    }
    
    func didSelectPaymentMethod(_ paymentMethod: PaymentMethod) {
        billingDetailDisplay?.selectPaymentMethod(paymentMethod)
    }
}
