//
//  AddPrepayAmountExecutive.swift
//  QuickPay
//
//  Created by Michael Lee on 6/15/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation

//
// Display
//

protocol AddPrepayAmountDisplay: CommonDisplay {
    func loadDisplay(_ context: PaymentProfile,
                     personAddressDisplayContext: PersonAddressDisplayContext)
    func loadLayout(_ context: PaymentProfile)
    func showWarningMessage()
    func getPaymentAmountRequest() -> PaymentAmountRequest
    func showPaypalViewController(_ payment: PayPalPayment)
}

//
// Executive
//

class AddPrepayAmountExecutive: CommonExecutive {
    
    private static let EmptyTotalPrice = "Please input total amount"
    private static let EmptyCardHolderName = "Please input card holder name"
    private static let EmptyCardNumber = "Please input card number"
    private static let EmptyExpiration = "Please input expiration"
    private static let EmptyCVC = "Please input CVC"
    private static let EmptyPhoneNumber = "Please input phone number"
    private static let InvalidPhoneNumber = "Please input valid phone number"
    private static let EmptyEmail = "Please input email"
    private static let InvalidEmail = "Please input valid email"
    private static let EmptyCountry = "Please select country"
    private static let EmptyBillingAddress = "Please input billing address"
    private static let EmptyCity = "Please input city"
    private static let EmptyState = "Please input state"
    private static let EmptyZipCode = "Please input zip code"
    
    weak var addPrepayAmountDisplay: AddPrepayAmountDisplay? {
        get {
            return display as? AddPrepayAmountDisplay
        }
    }
    
    var context = PaymentProfile()
    
    // MARK: - Event Handler
    
    func displayDidLoad() {
        
        //
        // Load main information
        //
        
        let appData = AppData.sharedInstance
        context = appData.paymentProfile.copy() as! PaymentProfile
        context.personAddress.addressType = .OneAddress
        loadLayout()
        
        //
        // Load person address context
        //
        
        let personAddressDisplayContext = PersonAddressDisplayContext()
        personAddressDisplayContext.personAddress = context.personAddress
        let country = personAddressDisplayContext.personAddress.country
        if let countryName = country.name,
            let countryId = country.id {
            let countriesString = appData.countries.map({ $0.name?.lowercased() ?? "" })
            personAddressDisplayContext.countryState.selectedCountry = countriesString.index(of: countryName.lowercased())
            State.loadStates(countryId, successBlock: { (states) in
                if let states = states {
                    personAddressDisplayContext.countryState.states = states
                    let stateIds = states.map({ $0.id ?? -1 })
                    let state = personAddressDisplayContext.personAddress.state
                    if let stateId = state.id {
                        personAddressDisplayContext.countryState.selectedState = stateIds.index(of: stateId)
                    }
                }
                DispatchQueue.main.async {
                    self.addPrepayAmountDisplay?.loadDisplay(self.context, personAddressDisplayContext: personAddressDisplayContext)
                }
            }, executive: self)
            return
        }
        addPrepayAmountDisplay?.loadDisplay(context, personAddressDisplayContext: personAddressDisplayContext)
    }
    
    func didPressSubmitButton() {
        addPrepayAmountDisplay?.showWarningMessage()
    }
    
    // MARK: - Helper functions
    func loadLayout() {
        addPrepayAmountDisplay?.loadLayout(context)
    }
    
    func proceedAddAmount() {
        guard let request = addPrepayAmountDisplay?.getPaymentAmountRequest() else {
            return
        }
        if request.isPaypal {
            proceedPrepayByPaypal(request)
        }
        else {
            proceedPrepayByCreditCard(request)
        }
    }
    
    func proceedPrepayByCreditCard(_ request: PaymentAmountRequest) {
        if let validateString = validateCreditCardPrepay(request) {
            display?.showError(validateString)
            return
        }
        if !request.isUpdate {
            request.paymentProfile.paymentProfileID = AppData.sharedInstance.paymentProfile.paymentProfileID ?? ""
        }
        display?.showLoading(true)
        request.proceedPrepayByCreditCard({ (isSuccess, message) in
            self.display?.showLoading(false)
            if !isSuccess {
                return
            }
            if let message = message {
                self.display?.showError(message)
            }
        }, executive: self)
    }
    
    func proceedPrepayByPaypal(_ request: PaymentAmountRequest) {
        
        //
        // Validate request parameters
        //
        
        if let validateString = validateCreditCardPrepay(request) {
            display?.showError(validateString)
            return
        }
        
        //
        // Define payment items
        //
        
        let item1 = PayPalItem(name: "YLC 1", withQuantity: 1, withPrice: StringUtility.decimal(with: request.totalPrice), withCurrency: "USD", withSku: "Hip-0037")
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        //
        // Include payment details. It is optional
        //
        
        let shipping = NSDecimalNumber(string: "0.00")
        let tax = NSDecimalNumber(string: "0.00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        let total = subtotal.adding(shipping).adding(tax)
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "YLC Payment", intent: .sale)
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        //
        // Proceed payment
        //
        
        if (payment.processable) {
            addPrepayAmountDisplay?.showPaypalViewController(payment)
        }
        else {
            print("Payment not processalbe: \(payment)")
        }
    }
    
    func proceedPaypalConfirm(_ confirmation: NSDictionary) {
        guard let request = addPrepayAmountDisplay?.getPaymentAmountRequest(),
            let confirmationDictionary = confirmation["response"] as? NSDictionary,
            let paymentId = confirmationDictionary["id"] else {
                return
        }
        display?.showLoading(true)
        guard let userID = AppData.sharedInstance.user?.id else {
            return
        }
        let parameters = ["payment_id": paymentId]
        let url = "paypal/\(userID)/\(request.totalPrice)/customer.do"
        print("Process Start")
        Client.sendRequest(.Post, name: url, parameters: parameters, headers: nil) { (response, error) in
            print("Process End")
            self.display?.showLoading(false)
            guard self.validate(response, error: error) != nil else {
                return
            }
            if let message = self.messageFromResponse(response), let plainText = StringUtility.htmlToText(encodedString: message) {
                self.display?.showError(plainText)
            }
        }
    }
    
    func validateCreditCardPrepay(_ request: PaymentAmountRequest) -> String? {
    
        if request.totalPrice.isEmpty {
            return AddPrepayAmountExecutive.EmptyTotalPrice
        }
        if !request.isUpdate {
            return nil
        }
        if request.isPaypal {
            return nil
        }
        guard let cardHolder = request.paymentProfile.cardHolderName, !cardHolder.isEmpty else {
            return AddPrepayAmountExecutive.EmptyCardHolderName
        }
        guard let cvc = request.paymentProfile.cvc, !cvc.isEmpty else {
            return AddPrepayAmountExecutive.EmptyCVC
        }
        guard let cardNumber = request.paymentProfile.cardNumber, !cardNumber.isEmpty else {
            return AddPrepayAmountExecutive.EmptyCardNumber
        }
        guard let expiration = request.paymentProfile.expiration, !expiration.isEmpty else {
            return AddPrepayAmountExecutive.EmptyExpiration
        }
        guard let phone = request.paymentProfile.phoneNumber, !phone.isEmpty else {
            return AddPrepayAmountExecutive.EmptyPhoneNumber
        }
        if !StringUtility.validate(phone: phone) {
            return AddPrepayAmountExecutive.InvalidPhoneNumber
        }
        guard let email = request.paymentProfile.email, !email.isEmpty else {
            return AddPrepayAmountExecutive.EmptyEmail
        }
        if !StringUtility.validate(email: email) {
            return AddPrepayAmountExecutive.InvalidEmail
        }
        guard let countryName = request.paymentProfile.personAddress.country.name, !countryName.isEmpty else {
            return AddPrepayAmountExecutive.EmptyCountry
        }
        guard let address = request.paymentProfile.personAddress.billingAddress, !address.isEmpty else {
            return AddPrepayAmountExecutive.EmptyBillingAddress
        }
        guard let city = request.paymentProfile.personAddress.city, !city.isEmpty else {
            return AddPrepayAmountExecutive.EmptyCity
        }
        guard let state = request.paymentProfile.personAddress.state.name, !state.isEmpty else {
            return AddPrepayAmountExecutive.EmptyState
        }
        guard let zipCode = request.paymentProfile.personAddress.zipCode, !zipCode.isEmpty else {
            return AddPrepayAmountExecutive.EmptyZipCode
        }
        return nil
    }
}
