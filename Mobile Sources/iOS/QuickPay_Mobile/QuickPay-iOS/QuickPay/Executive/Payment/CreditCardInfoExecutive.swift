//
//  CreditCardExecutive.swift
//  QuickPay
//
//  Created by Michael Lee on 6/4/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation
import SwiftyJSON

//
// Display
//

protocol CreditCardInfoDisplay: CommonDisplay {
    func loadDisplay(_ context: PaymentProfile, personAddressDisplayContext: PersonAddressDisplayContext)
    func usePersonEmail(_ isUse: Bool)
    func usePersonAddress(_ isUse: Bool)
}

//
// Executive
//

class CreditCardInfoExecutive: CommonExecutive {
    
    weak var creditCardInfoDisplay: CreditCardInfoDisplay? {
        get {
            return display as? CreditCardInfoDisplay
        }
    }
    
    // MARK: - Event Handler
    
    func displayDidLoad() {
    }
    
    func didSelectPersonEmail(_ isSelected: Bool) {
        creditCardInfoDisplay?.usePersonEmail(isSelected)
    }
    
    func didSelectPersonAddress(_ isSelected: Bool) {
        creditCardInfoDisplay?.usePersonAddress(isSelected)
    }
}
