//
//  RefundExecutive.swift
//  QuickPay
//
//  Created by Michael Lee on 6/16/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation

//
// Display
//

protocol RefundDisplay: CommonDisplay {
    func loadRefundList(_ refundListResponse: GetRefundListResponse)
    func showResultMessage(_ message: String)
}

//
// Executive
//

class RefundExecutive: CommonExecutive {
    
    static let UnavailableRefund = "\nThis payment is still unsettlement. After 24 hours passed, You can refund it.\n"
    
    weak var refundDisplay: RefundDisplay? {
        get {
            return display as? RefundDisplay
        }
    }
    
    func displayDidLoad(_ getRefundListRequest: RefundRequest) {
        display?.showLoading(true)
        getRefundListRequest.get(executive: self) {
            (isSuccess, getRefundListResponse) in
            self.display?.showLoading(false)
            if !isSuccess {
                return
            }
            if let response = getRefundListResponse {
                DispatchQueue.main.async {
                    self.refundDisplay?.loadRefundList(response)
                }
            }
        }
    }
    
    func didPressRefundButton(_ getRefundListRequest: RefundRequest) {
        let refundRequest = RefundRequest()
        refundRequest.paymentId = getRefundListRequest.paymentId
        refundRequest.kind = "process"
        display?.showLoading(true)
        refundRequest.post(executive: self) { (isSuccess, message) in
            self.display?.showLoading(false)
            if let message = message {
                self.refundDisplay?.showResultMessage(message)
            }
        }
    }
}
