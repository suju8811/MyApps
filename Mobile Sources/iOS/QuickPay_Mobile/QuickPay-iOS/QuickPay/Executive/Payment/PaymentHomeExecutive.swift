//
//  PaymentHomeExecutive.swift
//  QuickPay
//
//  Created by Michael Lee on 5/12/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import UIKit

//
// Payment Method
//

enum PaymentMethod {
    case CreditCardBefore
    case CreditCard
    case PayPal
}

//
// Quick Pay Display Context
//

class QuickPayDisplayContext {
    
    var countryState = CountryStateDisplayContext()
    var paymentProfile = PaymentProfile()
}

//
// Display
//

protocol PaymentHomeDisplay: CommonDisplay {
    func loadDisplay(_ context: QuickPayDisplayContext)
    func selectPaymentMethod(_ paymentMethod: PaymentMethod)
    func usePersonEmail(_ isUse: Bool)
    func usePersonAddress(_ isUse: Bool)
    func refreshSearchResult(_ inmates: [Inmate])
    func getInmatesSearchRequest() -> Inmate
    func getPaymentRequest() -> PaymentRequest
    func showPaypalViewController(_ payment: PayPalPayment)
    func adjustTableHeight()
    func showAddButton(_ isShow: Bool)
    func reloadStateDropDown(_ context: QuickPayDisplayContext)
    func enableSearchView(_ isEnable: Bool, inmate: Inmate)
}

//
// Executive
//

class PaymentHomeExecutive: CommonExecutive {
    
    private static let InvalidSearchCriteria = "Please input first name or last name or BOP Number"
    private static let EmptyFirstName = "Please input first name"
    private static let EmptyLastName = "Please last name"
    private static let EmptyBOPNumber = "Please input BOP Number"
    private static let InvalidBOPNumber = "BOP number is 8-digit number"
    private static let NotSelectedInmate = "Please search and select inmate"
    private static let EmptyTotalAmount = "Please input total amount"
    private static let EmptyEmail = "Please enter email address"
    private static let InvalidEmail = "Please enter valid email address"
    private static let EmptyPhoneNumber = "Please enter phone number"
    private static let InvalidPhoneNumber = "Please enter valid phone number"
    private static let EmptyCardHolderName = "Please enter card holder name"
    private static let EmptyCardNumber = "Please enter card number"
    private static let EmptyCVC = "Please enter CVC"
    private static let EmptyExpiration = "Please enter expiration"
    private static let EmptyCountry = "Please select country"
    private static let EmptyBillingAddress = "Please enter billing address"
    private static let EmptyCity = "Please enter city"
    private static let EmptyState = "Please select state"
    private static let EmptyZipCode = "Please enter zip code"
    private static let InmateNotFound = "Inmate not found. Enter inmate information to add inmate or call 855-952-5060 for customer support."
    
    weak var paymentHomeDisplay: PaymentHomeDisplay? {
        get {
            return display as? PaymentHomeDisplay
        }
    }
    var context = QuickPayDisplayContext()
    
    func displayDidLoad() {
        let appData = AppData.sharedInstance
        display?.showLoading(true)
        Country.loadCountries({ (isSuccess) in
            if !isSuccess {
                DispatchQueue.main.async {
                    self.display?.showLoading(false)
                }
                return
            }
            appData.paymentProfile.loadPaymentProfile({ (isSuccess) in
                if !isSuccess {
                    DispatchQueue.main.async {
                        self.display?.showLoading(false)
                    }
                    return
                }
                
                self.context.paymentProfile = appData.paymentProfile
                self.context.countryState.selectedCountry = appData.paymentProfile.personAddress.country.id
                let countryStrings = appData.countries.map({ $0.name?.lowercased() ?? "" })
                if let countryName = appData.paymentProfile.personAddress.country.name {
                    self.context.countryState.selectedCountry = countryStrings.index(of: countryName.lowercased())
                }
                guard let selectedCountry = self.context.countryState.selectedCountry,
                    let countryId = appData.countries[selectedCountry].id else {
                    DispatchQueue.main.async {
                        self.display?.showLoading(false)
                        self.paymentHomeDisplay?.loadDisplay(self.context)
                    }
                    return
                }
                appData.paymentProfile.personAddress.country = appData.countries[selectedCountry]
                State.loadStates(countryId, successBlock: { (states) in
                    DispatchQueue.main.async {
                        self.display?.showLoading(false)
                        if let states = states {
                            self.context.countryState.states = states
                        }
                        else {
                            self.context.countryState.states.removeAll()
                        }
                        let stateStrings = self.context.countryState.states.map({ $0.name?.lowercased() ?? "" })
                        if let stateName = appData.paymentProfile.personAddress.state.name {
                            self.context.countryState.selectedState = stateStrings.index(of: stateName.lowercased())
                        }
                        if let selectedState = self.context.countryState.selectedState {
                            appData.paymentProfile.personAddress.state = self.context.countryState.states[selectedState]
                        }
                        self.paymentHomeDisplay?.loadDisplay(self.context)
                        if let inmateId = appData.user?.inmateId {
                            self.display?.showLoading(true)
                            Inmate.loadInmate(by: inmateId, { (inmate) in
                                self.display?.showLoading(false)
                                if let inmate = inmate {
                                    self.paymentHomeDisplay?.enableSearchView(false, inmate: inmate)
                                }
                            }, executive: self)
                        }
                    }
                }, executive: self)
            }, executive: self)
        }, executive: self)
    }
    
    func didPressSearchButton() {
        guard let request = paymentHomeDisplay?.getInmatesSearchRequest() else {
            return
        }
        if let validateString = validateSearch(request) {
            display?.showError(validateString)
            return
        }
        guard let firstName = request.firstName, let lastName = request.lastName, let bopNumber = request.BOPNumber else {
            return
        }
        display?.showLoading(true)
        let parameters = ["first_name": firstName,
                          "last_name": lastName,
                          "bop_number": bopNumber]
        Client.sendRequest(.Post, name: "inmate/search.do", parameters: parameters, headers: nil) { (response, error) in
            self.display?.showLoading(false)
            guard let result = self.validate(response, error: error) else {
                return
            }
            if let resultArray = result["inmate"].array {
                var inmates = [Inmate]()
                for inmateJson in resultArray {
                    let inmate = Inmate()
                    inmate.setData(from: inmateJson)
                    inmates.append(inmate)
                }
                if inmates.isEmpty {
                    self.display?.showError(PaymentHomeExecutive.InmateNotFound)
                    self.paymentHomeDisplay?.showAddButton(true)
                    return
                }
                
                DispatchQueue.main.async {
                    self.paymentHomeDisplay?.showAddButton(false)
                    self.paymentHomeDisplay?.refreshSearchResult(inmates)
                }
            }
        }
    }
    
    func didPressAddInmateButton() {
        guard let request = paymentHomeDisplay?.getInmatesSearchRequest() else {
            return
        }
        if let validateString = validateAdd(request) {
            display?.showError(validateString)
            return
        }
        guard let firstName = request.firstName, let lastName = request.lastName, let bopNumber = request.BOPNumber else {
            return
        }
        display?.showLoading(true)
        let parameters = ["first_name": firstName,
                          "last_name": lastName,
                          "bop_number": bopNumber]
        Client.sendRequest(.Post, name: "inmate/add_by_customer.do", parameters: parameters, headers: nil) { (response, error) in
            self.display?.showLoading(false)
            guard let result = self.validate(response, error: error) else {
                return
            }
            if let resultArray = result["inmate"].array {
                var inmates = [Inmate]()
                for inmateJson in resultArray {
                    let inmate = Inmate()
                    inmate.setData(from: inmateJson)
                    inmates.append(inmate)
                }
                DispatchQueue.main.async {
                    self.paymentHomeDisplay?.showAddButton(false)
                    self.paymentHomeDisplay?.refreshSearchResult(inmates)
                }
            }
        }
    }
    
    func didSelectPaymentMethod(_ paymentMethod: PaymentMethod) {
        paymentHomeDisplay?.selectPaymentMethod(paymentMethod)
    }
    
    func didSelectPersonEmail(_ isSelected: Bool) {
        paymentHomeDisplay?.usePersonEmail(isSelected)
    }
    
    func didSelectPersonAddress(_ isSelected: Bool) {
        paymentHomeDisplay?.usePersonAddress(isSelected)
    }
    
    func didSelectCountry(_ index: Int?) {
        guard let index = index,
            let countryId = AppData.sharedInstance.countries[index].id else {
            context.countryState.states.removeAll()
            context.countryState.selectedState = nil
            paymentHomeDisplay?.reloadStateDropDown(context)
            return
        }
        display?.showLoading(true)
        State.loadStates(countryId, successBlock: { (states) in
            self.display?.showLoading(false)
            self.context.countryState.states = states ?? [State]()
            self.context.countryState.selectedState = nil
            DispatchQueue.main.async {
                self.paymentHomeDisplay?.reloadStateDropDown(self.context)
            }
        }, executive: self)
    }
    
    func didPressSubmitButton() {
        guard let paymentRequest = paymentHomeDisplay?.getPaymentRequest() else {
            return
        }
        if paymentRequest is CreditCardPaymentRequest {
            proceedPaymentByCreditCard(paymentRequest as! CreditCardPaymentRequest)
        }
        else {
            proceedPaymentByPaypal(paymentRequest)
        }
    }
    
    func adjustTableHeight() {
        paymentHomeDisplay?.adjustTableHeight()
    }
    
    func proceedPaymentByCreditCard(_ request: CreditCardPaymentRequest) {
        if let validateString = validateCreditCardPayment(request) {
            display?.showError(validateString)
            return
        }
        guard let inmateId = request.inmateId else {
            return
        }
        if !request.isUpdate {
            request.paymentProfileID = AppData.sharedInstance.paymentProfile.paymentProfileID ?? ""
        }
        display?.showLoading(true)
        let parameters = ["inmate_id": inmateId,
                          "payment_profile_id": request.paymentProfileID,
                          "cardholder": request.cardHolder,
                          "cvc": request.cvc,
                          "card_number": request.cardNumber,
                          "expiration": request.expiration,
                          "is_paypal": "",
                          "total_price": request.totalAmount,
                          "phone_number": StringUtility.discardNoNumber(request.phoneNumber),
                          "email_address": request.email,
                          "use_person_email": request.usePersonEmail ? "1" : "0",
                          "country": request.country,
                          "address": request.billingAdress,
                          "use_person_address": request.usePersonAddress ? "1" : "0",
                          "city": request.city,
                          "state": request.state,
                          "zip_code": request.zipCode] as [String : Any]
        Client.sendRequest(.Post, name: "payment/quick/add.do", parameters: parameters, headers: nil) { (response, error) in
            self.display?.showLoading(false)
            guard self.validate(response, error: error) != nil else {
                return
            }
            if let message = self.messageFromResponse(response), let plainText = StringUtility.htmlToText(encodedString: message) {
                self.display?.showError(plainText)
            }
        }
    }
    
    func proceedPaymentByPaypal(_ request: PaymentRequest) {
        
        //
        // Validate request parameters
        //
        
        if let validateString = validatePayPalPayment(request) {
            display?.showError(validateString)
            return
        }
        
        //
        // Define payment items
        //
        
        let item1 = PayPalItem(name: "YLC 1", withQuantity: 1, withPrice: StringUtility.decimal(with: request.totalAmount), withCurrency: "USD", withSku: "Hip-0037")
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        //
        // Include payment details. It is optional
        //
        
        let shipping = NSDecimalNumber(string: "0.00")
        let tax = NSDecimalNumber(string: "0.00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        let total = subtotal.adding(shipping).adding(tax)
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "YLC Payment", intent: .sale)
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        //
        // Proceed payment
        //
        
        if (payment.processable) {
            paymentHomeDisplay?.showPaypalViewController(payment)
        }
        else {
            print("Payment not processalbe: \(payment)")
        }
    }
    
    func proceedPaypalConfirm(_ confirmation: NSDictionary) {
        guard let paymentRequest = paymentHomeDisplay?.getPaymentRequest(),
            let inmateId = paymentRequest.inmateId,
            let confirmationDictionary = confirmation["response"] as? NSDictionary,
            let paymentId = confirmationDictionary["id"] else {
            return
        }
        display?.showLoading(true)
        guard let userID = AppData.sharedInstance.user?.id else {
            return
        }
        let parameters = ["payment_id": paymentId]
        let url = "paypal/\(userID)/\(inmateId)/\(paymentRequest.totalAmount)/inmate.do"
        print("Process Start")
        Client.sendRequest(.Post, name: url, parameters: parameters, headers: nil) { (response, error) in
            print("Process End")
            self.display?.showLoading(false)
            guard self.validate(response, error: error) != nil else {
                return
            }
            if let message = self.messageFromResponse(response), let plainText = StringUtility.htmlToText(encodedString: message) {
                self.display?.showError(plainText)
            }
        }
    }
    
    func validateSearch(_ request: Inmate) -> String? {
        guard let firstName = request.firstName, let lastName = request.lastName, let BOPNumber = request.BOPNumber else {
            return PaymentHomeExecutive.InvalidSearchCriteria
        }
        if firstName.isEmpty && lastName.isEmpty && BOPNumber.isEmpty {
            return PaymentHomeExecutive.InvalidSearchCriteria
        }
        return nil
    }
    
    func validateAdd(_ request: Inmate) -> String? {
        guard let firstName = request.firstName, let lastName = request.lastName, let BOPNumber = request.BOPNumber else {
            return PaymentHomeExecutive.InvalidSearchCriteria
        }
        if firstName.isEmpty {
            return PaymentHomeExecutive.EmptyFirstName
        }
        if lastName.isEmpty {
            return PaymentHomeExecutive.EmptyLastName
        }
        if BOPNumber.isEmpty {
            return PaymentHomeExecutive.EmptyBOPNumber
        }
        if BOPNumber.characters.count != 8 {
            return PaymentHomeExecutive.InvalidBOPNumber
        }
        return nil
    }
    
    func validatePayPalPayment(_ request: PaymentRequest) -> String? {
        if request.inmateId == nil {
            return PaymentHomeExecutive.NotSelectedInmate
        }
        if request.totalAmount.isEmpty {
            return PaymentHomeExecutive.EmptyTotalAmount
        }
        return nil
    }
    
    func validateCreditCardPayment(_ request: CreditCardPaymentRequest) -> String? {
        if request.inmateId == nil {
            return PaymentHomeExecutive.NotSelectedInmate
        }
        if request.totalAmount.isEmpty {
            return PaymentHomeExecutive.EmptyTotalAmount
        }
        if !request.isUpdate {
            return nil
        }
        if request.email.isEmpty {
            return PaymentHomeExecutive.EmptyEmail
        }
        if !StringUtility.validate(email: request.email) {
            return PaymentHomeExecutive.InvalidEmail
        }
        if request.phoneNumber.isEmpty {
            return PaymentHomeExecutive.EmptyPhoneNumber
        }
        if !StringUtility.validate(phone: request.phoneNumber) {
            return PaymentHomeExecutive.InvalidPhoneNumber
        }
        if request.cardHolder.isEmpty {
            return PaymentHomeExecutive.EmptyCardHolderName
        }
        if request.cardNumber.isEmpty {
            return PaymentHomeExecutive.EmptyCardNumber
        }
        if request.expiration.isEmpty {
            return PaymentHomeExecutive.EmptyExpiration
        }
        if request.cvc.isEmpty {
            return PaymentHomeExecutive.EmptyCVC
        }
        if request.country.isEmpty {
            return PaymentHomeExecutive.EmptyCountry
        }
        if request.billingAdress.isEmpty {
            return PaymentHomeExecutive.EmptyBillingAddress
        }
        if request.city.isEmpty {
            return PaymentHomeExecutive.EmptyCity
        }
        if request.state.isEmpty {
            return PaymentHomeExecutive.EmptyState
        }
        if request.zipCode.isEmpty {
            return PaymentHomeExecutive.EmptyZipCode
        }
        return nil
    }
}
