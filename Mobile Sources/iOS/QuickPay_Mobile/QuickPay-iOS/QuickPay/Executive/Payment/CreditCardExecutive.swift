//
//  CreditCardExecutive.swift
//  QuickPay
//
//  Created by Michael Lee on 6/4/17.
//  Copyright © 2017 Michael Lee. All rights reserved.
//

import Foundation
import SwiftyJSON

//
// Display
//

protocol CreditCardDisplay: CommonDisplay {
    func loadDisplay(_ context: PaymentProfile, personAddressDisplayContext: PersonAddressDisplayContext)
    func getCreditCardUpdateRequest() -> PaymentProfile?
}

//
// Executive
//

class CreditCardExecutive: CommonExecutive {
    
    private static let EmptyCardHolderName = "Please input card holder name"
    private static let EmptyCardNumber = "Please input card number"
    private static let EmptyExpiration = "Please input expiration"
    private static let EmptyCVC = "Please input CVC"
    private static let EmptyPhoneNumber = "Please input phone number"
    private static let InvalidPhoneNumber = "Please input valid phone number"
    private static let EmptyEmail = "Please input email"
    private static let InvalidEmail = "Please input valid email"
    private static let EmptyCountry = "Please select country"
    private static let EmptyBillingAddress = "Please input billing address"
    private static let EmptyCity = "Please input city"
    private static let EmptyZipCode = "Please input zip code"
    
    weak var creditCardDisplay: CreditCardDisplay? {
        get {
            return display as? CreditCardDisplay
        }
    }
    
    // MARK: - Event Handler
    
    func displayDidLoad() {
        
        //
        // Load main information
        //
        
        let appData = AppData.sharedInstance
        appData.paymentProfile.personAddress.addressType = .OneAddress
        let context = appData.paymentProfile
        
        //
        // Load person address context
        //
        
        let personAddressDisplayContext = PersonAddressDisplayContext()
        personAddressDisplayContext.personAddress.addressType = .OneAddress
        personAddressDisplayContext.personAddress = context.personAddress.copy() as! PersonAddress
        let country = personAddressDisplayContext.personAddress.country
        if let countryName = country.name,
            let countryId = country.id {
            let countriesString = appData.countries.map({ $0.name?.lowercased() ?? "" })
            personAddressDisplayContext.countryState.selectedCountry = countriesString.index(of: countryName.lowercased())
            State.loadStates(countryId, successBlock: { (states) in
                if let states = states {
                    personAddressDisplayContext.countryState.states = states
                    let stateIds = states.map({ $0.id ?? -1 })
                    let state = personAddressDisplayContext.personAddress.state
                    if let stateId = state.id {
                        personAddressDisplayContext.countryState.selectedState = stateIds.index(of: stateId)
                    }
                }
                DispatchQueue.main.async {
                    self.creditCardDisplay?.loadDisplay(context, personAddressDisplayContext: personAddressDisplayContext)
                }
            }, executive: self)
            return
        }
        creditCardDisplay?.loadDisplay(context, personAddressDisplayContext: personAddressDisplayContext)
    }
    
    func didPressResetButton() {
        creditCardDisplay?.loadDisplay(PaymentProfile(),
                                       personAddressDisplayContext: PersonAddressDisplayContext())
    }
    
    func didPressUpdateButton() {
        guard let request = creditCardDisplay?.getCreditCardUpdateRequest() else {
            return
        }
        if let validateString = validate(request) {
            display?.showError(validateString)
            return
        }
        display?.showLoading(true)
        request.updatePaymentProfile({ (isSuccess, message) in
            self.display?.showLoading(false)
            if isSuccess {
                let paymentProfileID = AppData.sharedInstance.paymentProfile.paymentProfileID
                AppData.sharedInstance.paymentProfile = request
                AppData.sharedInstance.paymentProfile.paymentProfileID = paymentProfileID
                if let message = message,
                    !message.isEmpty {
                    self.display?.showError(message)
                }
            }
        }, executive: self)
    }
    
    func validate(_ request: PaymentProfile) -> String? {
        guard let cardHolderName = request.cardHolderName,
            !cardHolderName.isEmpty else {
                return CreditCardExecutive.EmptyCardHolderName
        }
        guard let cardNumber = request.cardNumber,
            !cardNumber.isEmpty else {
                return CreditCardExecutive.EmptyCardNumber
        }
        guard let expiration = request.expiration,
            !expiration.isEmpty else {
                return CreditCardExecutive.EmptyExpiration
        }
        guard let cvc = request.cvc,
            !cvc.isEmpty else {
                return CreditCardExecutive.EmptyCVC
        }
        guard let phoneNumber = request.phoneNumber,
            !phoneNumber.isEmpty else {
                return CreditCardExecutive.EmptyPhoneNumber
        }
        if !StringUtility.validate(phone: phoneNumber) {
            return CreditCardExecutive.InvalidPhoneNumber
        }
        guard let email = request.email,
            !email.isEmpty else {
                return CreditCardExecutive.EmptyEmail
        }
        if !StringUtility.validate(email: email) {
            return CreditCardExecutive.InvalidEmail
        }
        guard let country = request.personAddress.country.name,
            !country.isEmpty else {
                return CreditCardExecutive.EmptyCountry
        }
        guard let billingAddress = request.personAddress.billingAddress,
            !billingAddress.isEmpty else {
                return CreditCardExecutive.EmptyBillingAddress
        }
        guard let city = request.personAddress.city,
            !city.isEmpty else {
                return CreditCardExecutive.EmptyCity
        }
        guard let zipCode = request.personAddress.zipCode,
            !zipCode.isEmpty else {
                return CreditCardExecutive.EmptyZipCode
        }
        return nil
    }
}
