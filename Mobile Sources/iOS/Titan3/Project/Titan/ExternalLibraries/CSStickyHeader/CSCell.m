//
//  CSCell.m
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by Jamz Tang on 8/1/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//

#import "CSCell.h"
#import "AppContext.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation CSCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:100], [UtilManager height:10], WIDTH - [UtilManager width:100], [UtilManager height:20])];
        [_titleLabel setText:@"Sesión inicial nutrición"];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setTextColor:YELLOW_APP_COLOR];
        [_titleLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
        [_titleLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_titleLabel];
        
        /*
        _workoutImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:25], [UtilManager width:90], [UtilManager width:90])];
        [_workoutImageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
        
        [[self contentView] addSubview:_workoutImageView];
        
        
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.size.height + _titleLabel.frame.origin.y + [UtilManager height:15], _titleLabel.frame.size.width, [UtilManager height:25])];
        [_tagView setMode:TagsControlModeList];
        [[self contentView] addSubview:_tagView];
        
        _seriesLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:5], _titleLabel.frame.size.width, _titleLabel.frame.size.height)];
        [_seriesLabel setTextColor:[UIColor lightGrayColor]];
        [_seriesLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:13]];
        [_seriesLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_seriesLabel];
        
        _clockImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _seriesLabel.frame.origin.y + _seriesLabel.frame.size.height + [UtilManager height:10], [UtilManager width:20] * 0.8, [UtilManager width:20] * 0.8)];
        [_clockImageView setImage:[UIImage imageNamed:@"icon-cell-clock"]];
        
        [[self contentView] addSubview:_clockImageView];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(_clockImageView.frame.origin.x + _clockImageView.frame.size.width + [UtilManager width:5], _clockImageView.frame.origin.y, [UtilManager width:80], _clockImageView.frame.size.height)];
        [_timeLabel setTextColor:GRAY_REGISTER_FONT];
        [_timeLabel setFont:_seriesLabel.font];
        [_timeLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_timeLabel];
        
        _kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_timeLabel.frame.origin.x + _timeLabel.frame.size.width, _clockImageView.frame.origin.y, [UtilManager width:20] * 0.8, [UtilManager width:20] * 0.8)];
        [_kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-red"]];
        
        [[self contentView] addSubview:_kCalImageView];
        
        _energyLabel = [[UILabel alloc] initWithFrame:CGRectMake(_kCalImageView.frame.origin.x + _kCalImageView.frame.size.width, _kCalImageView.frame.origin.y, [UtilManager width:110], _kCalImageView.frame.size.height)];
        [_energyLabel setTextColor:RED_APP_COLOR];
        [_energyLabel setText:@"3780 kCal"];
        [_energyLabel setFont:_seriesLabel.font];
        
        [[self contentView] addSubview:_energyLabel];
         */
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., [UtilManager height:139], WIDTH, 1)];
        [_separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        
        [[self contentView] addSubview:_separatorView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
