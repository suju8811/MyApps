//
//  CSCell.h
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by Jamz Tang on 8/1/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagView.h"
#import "CADRACSwippableCell.h"

@interface CSCell : CADRACSwippableCell

@property (nonatomic, strong) UILabel *titleLabel;

/*
@property (nonatomic, strong) UIImageView *workoutImageView;


@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) UILabel *seriesLabel;
@property (nonatomic, strong) UILabel *energyLabel;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIImageView *clockImageView;
@property (nonatomic, strong) UIImageView *kCalImageView;
 */

@property (nonatomic, strong) UIView *separatorView;

@end
