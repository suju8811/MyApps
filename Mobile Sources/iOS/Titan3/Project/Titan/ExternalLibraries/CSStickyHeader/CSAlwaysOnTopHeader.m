//
//  CSAlwaysOnTopHeader.m
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by James Tang on 6/4/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//

#import "CSAlwaysOnTopHeader.h"
#import "CSStickyHeaderFlowLayoutAttributes.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "UtilManager.h"

@interface CSAlwaysOnTopHeader ()

@property (nonatomic, assign) CGSize clockImageSize;
@property (nonatomic, assign) CGSize kCalImageSize;

@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *menuButton;

@end

@implementation CSAlwaysOnTopHeader

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = frame;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self addSubview:blurEffectView];
        
        _backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:20], [UtilManager width:40]*0.8, [UtilManager width:40] * 0.8)];
        [_backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
        [_backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:_backButton];
        
        _menuButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50], [UtilManager height:20], [UtilManager width:40] * 0.8, [UtilManager width:40]*0.8)];
        [_menuButton setImage:[UIImage imageNamed:@"btn-menu-settings"] forState:UIControlStateNormal];
        [_menuButton addTarget:self action:@selector(menuAction) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:_menuButton];
        
        if(_isCreatingSuperSerie){
            [_backButton setHidden:TRUE];
            [_menuButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
        }
        
        _titleHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(_backButton.frame.origin.x + _backButton.frame.size.width, _backButton.frame.origin.y + [UtilManager height:30], WIDTH - 2 * (_backButton.frame.origin.x + _backButton.frame.size.width) - [UtilManager width:20], [UtilManager height:45])];
        //[_titleLabel setText:_currentTraining.title];
        [_titleHeaderLabel setNumberOfLines:2];
        [_titleHeaderLabel setBackgroundColor:[UIColor clearColor]];
        [_titleHeaderLabel setTextAlignment:NSTextAlignmentCenter];
        [_titleHeaderLabel setTextColor:[UIColor whiteColor]];
        [_titleHeaderLabel setFont:[UIFont fontWithName:BOLD_FONT size:21]];
        [_titleHeaderLabel setAdjustsFontSizeToFitWidth:YES];
        [_titleHeaderLabel setText:@"Texto"];
        
        [self addSubview:_titleHeaderLabel];
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake(0., _titleHeaderLabel.frame.origin.y + _titleHeaderLabel.frame.size.height + [UtilManager height:5], WIDTH, TAGVIEW_SMALL_HEIGHT)]; //andTags:tags withTagsControlMode:TagsControlModeList];
        [_tagView setMode:TagsControlModeList];
        [_tagView setIsCenter:TRUE];
        [_tagView reloadTagSubviews];
        [_tagView setUserInteractionEnabled:FALSE];
        
        [self addSubview:_tagView];
        
        _firstView = [[UIView alloc] initWithFrame:CGRectMake(0., _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:10], WIDTH/3, [UtilManager height:30])];
        [_firstView setBackgroundColor:[UIColor clearColor]];
        
        _exercisesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0., _firstView.frame.size.width, [UtilManager height:25])];
        [_exercisesLabel setTextAlignment:NSTextAlignmentRight];
        [_exercisesLabel setTextColor:GRAY_REGISTER_FONT];
        [_exercisesLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [_exercisesLabel setAdjustsFontSizeToFitWidth:YES];
        [_exercisesLabel setCenter:CGPointMake(_exercisesLabel.center.x, _firstView.frame.size.height/2)];
        
        [_firstView addSubview:_exercisesLabel];
        
        [self addSubview:_firstView];
        
        _secondView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH/3, _firstView.frame.origin.y, _firstView.frame.size.width, _firstView.frame.size.height)];
        [_secondView setBackgroundColor:[UIColor clearColor]];
        
        _clockImageSize = [UIImage imageNamed:@"icon-cell-clock"].size;
        _kCalImageSize = [UIImage imageNamed:@"icon-kcal-red"].size;
        
        _clockImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:20], _exercisesLabel.frame.origin.y, [UtilManager width:25] * 0.8, [UtilManager width:25] * 0.8)];
        [_clockImageView setImage:[UIImage imageNamed:@"icon-cell-clock"]];
        [_clockImageView setCenter:CGPointMake(_secondView.frame.size.width/2 - [UtilManager width:20], _firstView.frame.size.height/2)];
        
        [_secondView addSubview:_clockImageView];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(_clockImageView.frame.origin.x + _clockImageView.frame.size.width+ [UtilManager width:5], _exercisesLabel.frame.origin.y,_secondView.frame.size.width, _exercisesLabel.frame.size.height)];
        [_timeLabel setText:@"47 min"];
        [_timeLabel setTextAlignment:NSTextAlignmentLeft];
        [_timeLabel setFont:_exercisesLabel.font];
        [_timeLabel setTextColor:_exercisesLabel.textColor];
        [_timeLabel setAdjustsFontSizeToFitWidth:YES];
        [_timeLabel setCenter:CGPointMake(_timeLabel.center.x, _firstView.frame.size.height/2)];
        
        [_secondView addSubview:_timeLabel];
        
        [self addSubview:_secondView];
        
        _thirdView = [[UIView alloc] initWithFrame:CGRectMake(2 * WIDTH / 3, _firstView.frame.origin.y, _firstView.frame.size.width, _firstView.frame.size.height)];
        [_thirdView setBackgroundColor:[UIColor clearColor]];
        
        _kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:0], _timeLabel.frame.origin.y, [UtilManager width:20] * 0.8, [UtilManager width:20] * 0.8)];
        [_kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-red"]];
        [_kCalImageView setCenter:CGPointMake(_kCalImageView.center.x, _firstView.frame.size.height/2)];
        
        [_thirdView addSubview:_kCalImageView];
        
        _kCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(_kCalImageView.frame.origin.x + _kCalImageView.frame.size.width + [UtilManager width:5], _timeLabel.frame.origin.y, _thirdView.frame.size.width, _timeLabel.frame.size.height)];
        [_kCalLabel setTextColor:RED_APP_COLOR];
        [_kCalLabel setFont:_timeLabel.font];
        [_kCalLabel setTextAlignment:NSTextAlignmentLeft];
        [_kCalLabel setCenter:CGPointMake(_kCalLabel.center.x, _firstView.frame.size.height/2)];
        
        [_thirdView addSubview:_kCalLabel];
        
        [self addSubview:_thirdView];
    }
    
    return self;
}

- (void)setIsCreatingSuperSerie:(BOOL)isCreatingSuperSerie{
    if(isCreatingSuperSerie){
        [_backButton setHidden:TRUE];
        [_menuButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    } else {
        [_backButton setHidden:FALSE];
        [_menuButton setImage:[UIImage imageNamed:@"btn-menu-settings"] forState:UIControlStateNormal];
    }
}

- (void)didPressBackButton {
    [_delegate backTopHeaderAction];
}

- (void)menuAction{
    [_delegate menuTopHeaderAction];
}

- (void)applyLayoutAttributes:(CSStickyHeaderFlowLayoutAttributes *)layoutAttributes {

    //[self setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    if(layoutAttributes.progressiveness > 0.685){
        [_firstView setHidden:FALSE];
        [_secondView setHidden:FALSE];
        [_thirdView setHidden:FALSE];
    }
    
    if(layoutAttributes.progressiveness > 0.3766 && layoutAttributes.progressiveness<1){
        float widthText = (WIDTH - 2 * (_backButton.frame.origin.x + _backButton.frame.size.width) - [UtilManager width:20]) * layoutAttributes.progressiveness;
        
        if(widthText > [UtilManager width:180]){
            [_titleHeaderLabel setFrame:CGRectMake(_titleHeaderLabel.frame.origin.x,(_backButton.frame.origin.y + [UtilManager height:30] * layoutAttributes.progressiveness) * layoutAttributes.progressiveness, widthText, _titleHeaderLabel.frame.size.height)];
        }
        else{
            [_titleHeaderLabel setFrame:CGRectMake(_titleHeaderLabel.frame.origin.x,(MAX(_backButton.frame.origin.y * layoutAttributes.progressiveness , [UtilManager height:1]) + [UtilManager height:30] * layoutAttributes.progressiveness) * layoutAttributes.progressiveness, [UtilManager width:180], _titleHeaderLabel.frame.size.height)];
        }
        
        [_titleHeaderLabel setCenter:CGPointMake(self.center.x, _titleHeaderLabel.center.y)];
        [_titleHeaderLabel setNumberOfLines:1];
        [_titleHeaderLabel setAdjustsFontSizeToFitWidth:true];
        
        [_tagView setFrame:CGRectMake(0., _titleHeaderLabel.frame.origin.y + MAX(_titleHeaderLabel.frame.size.height * layoutAttributes.progressiveness, [UtilManager height:30]) + [UtilManager height:5], WIDTH, TAGVIEW_SMALL_HEIGHT)];
        
        [_firstView setFrame:CGRectMake(0., _tagView.frame.origin.y + _tagView.frame.size.height + MAX([UtilManager height:1] * layoutAttributes.progressiveness, 4), WIDTH/3, MAX([UtilManager height:30] * layoutAttributes.progressiveness, [UtilManager height:12]))];
        
        [_exercisesLabel setFrame:CGRectMake(0, 0., _firstView.frame.size.width - [UtilManager width:10], MAX([UtilManager height:25] * layoutAttributes.progressiveness, [UtilManager height:12]))];
        
        [_secondView setFrame:CGRectMake(WIDTH/3, _firstView.frame.origin.y, _firstView.frame.size.width, _firstView.frame.size.height)];
        
        [_clockImageView setFrame:CGRectMake([UtilManager width:20], 0, MAX(_clockImageSize.width * 0.8 * layoutAttributes.progressiveness, _clockImageSize.width * 0.4), MAX(_clockImageSize.width * 0.8 * layoutAttributes.progressiveness, _clockImageSize.width * 0.4))];
        
        [_timeLabel setFrame:CGRectMake(_clockImageView.frame.origin.x + _clockImageView.frame.size.width+ [UtilManager width:5], _exercisesLabel.frame.origin.y,_secondView.frame.size.width, _exercisesLabel.frame.size.height)];
        
        [_clockImageView setCenter:CGPointMake(_clockImageView.center.x, _timeLabel.center.y)];
        
        [_thirdView setFrame:CGRectMake(WIDTH * 2/3, _firstView.frame.origin.y, _firstView.frame.size.width, _firstView.frame.size.height)];
        
        [_kCalImageView setFrame:CGRectMake([UtilManager width:0], _timeLabel.frame.origin.y, MAX(_kCalImageSize.width * 0.8 * layoutAttributes.progressiveness, _kCalImageSize.width * 0.4), MAX(_kCalImageSize.width * 0.8 * layoutAttributes.progressiveness, _kCalImageSize.width * 0.4))];
        
        [_kCalLabel setFrame:CGRectMake(_kCalImageView.frame.origin.x + _kCalImageView.frame.size.width + [UtilManager width:5], _timeLabel.frame.origin.y, _thirdView.frame.size.width, _timeLabel.frame.size.height)];
        
        [_kCalImageView setCenter:CGPointMake(_kCalImageView.center.x, _kCalLabel.center.y)];
    }
    else{
        if(layoutAttributes.progressiveness < 0.1){
            [_firstView setHidden:true];
            [_secondView setHidden:true];
            [_thirdView setHidden:true];
        } else {
            [_firstView setHidden:false];
            [_secondView setHidden:false];
            [_thirdView setHidden:false];
        }
    }
    
    [UIView commitAnimations];
}

@end
