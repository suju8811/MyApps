//
//  CSAlwaysOnTopHeader.h
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by James Tang on 6/4/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//

#import "CSCell.h"
#import "TagView.h"

@protocol CSAlwaysOnTopHeaderDelegate;

@interface CSAlwaysOnTopHeader : UICollectionViewCell

@property (strong, nonatomic) UILabel *titleHeaderLabel;
@property (strong, nonatomic) UILabel *kCalLabel;
@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) TagView *tagView;
@property (strong, nonatomic) UILabel *exercisesLabel;
@property (strong, nonatomic) UIImageView *clockImageView;
@property (strong, nonatomic) UIImageView *kCalImageView;

@property (strong, nonatomic) UIView *firstView;
@property (strong, nonatomic) UIView *secondView;
@property (strong, nonatomic) UIView *thirdView;

@property (nonatomic, assign) BOOL isCreatingSuperSerie;

@property (nonatomic, assign) id<CSAlwaysOnTopHeaderDelegate>delegate;

@end

@protocol CSAlwaysOnTopHeaderDelegate <NSObject>

- (void)backTopHeaderAction;
- (void)menuTopHeaderAction;

@end
