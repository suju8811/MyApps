//
//  MBTwitterScroll.m
//  TwitterScroll
//
//  Created by Martin Blampied on 07/02/2015.
//  Copyright (c) 2015 MartinBlampied. All rights reserved.
//

#import "MBTwitterScroll.h"
#import "UIScrollView+TwitterCover.h"
#import "UtilManager.h"
#import "AppConstants.h"
#import "AppContext.h"

CGFloat const offset_HeaderStop = 110.0;
CGFloat const offset_B_LabelHeader = 35.0;
CGFloat const distance_W_LabelHeader = 35.0;

@implementation MBTwitterScroll

- (UIImage *)imageWithColor:(UIColor *)color{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


- (MBTwitterScroll *)initTableViewWithBackgound:(UIImage*)backgroundImage avatarImage:(UIImage *)avatarImage titleString:(NSString *)titleString subtitleString:(NSString *)subtitleString buttonTitle:(NSString *)buttonTitle {
    
    CGRect bounds = [[UIScreen mainScreen] bounds];
    self = [[MBTwitterScroll alloc] initWithFrame:bounds];
    
    [self setupView:backgroundImage avatarImage:avatarImage titleString:titleString subtitleString:subtitleString buttonTitle:buttonTitle scrollHeight:0 type:MBTableMeal];
    [self.tableView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    return self;
}

- (MBTwitterScroll *)initWithType:(MBType)type{
    CGRect bounds = [[UIScreen mainScreen] bounds];
    self = [[MBTwitterScroll alloc] initWithFrame:bounds];
    
    _selectType = type;
    
    return self;
}

- (void)setupView {
    
    self.headerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:210])];
    [self.headerScrollView setBounces:FALSE];
    [self.headerScrollView setContentSize:CGSizeMake(2 * WIDTH, self.headerScrollView.frame.size.height)];
    [self.headerScrollView setScrollEnabled:TRUE];
    [self.headerScrollView setDelegate:self];
    [self.headerScrollView setPagingEnabled:TRUE];
    [self.headerScrollView setShowsHorizontalScrollIndicator:FALSE];
    
    // Header
    CGFloat headerHeight = [UtilManager height:210];
    if (_fromCalendar) {
        headerHeight = [UtilManager height:265];
    }
    self.header = [[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x,
                                                           self.frame.origin.y,
                                                           self.frame.size.width,
                                                           headerHeight)];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, headerHeight)];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    UIImage *backgroundImage = [self imageWithColor:BLACK_APP_COLOR];
    
    self.headerImageView = [[UIImageView alloc] initWithFrame:self.header.frame];
    self.headerImageView.image = backgroundImage;
    [self.headerImageView setAlpha:0.8];
    self.headerImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.header insertSubview:self.headerImageView aboveSubview:self.headerLabel];
    self.header.clipsToBounds = YES;
    
    [self.header addSubview:self.headerImageView];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = headerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [headerView addSubview:blurEffectView];
    
    [self.header addSubview:headerView];
    
    [self addSubview:self.header];

    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0., self.headerScrollView.frame.size.height + self.headerScrollView.frame.origin.y - [UtilManager height:20], WIDTH, [UtilManager height:20])];
    [_pageControl setNumberOfPages:2];
    [_pageControl setCurrentPage:0];
    [_pageControl setBackgroundColor:[UIColor clearColor]];
    
    
    [self.header addSubview:_pageControl];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.frame];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.showsVerticalScrollIndicator = NO;
    
    // TableView Header
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.header.frame.size.height)];
    [self addSubview:self.tableView];
    
    [self addSubview:self.headerScrollView];
    
    _backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:20], [UtilManager width:40], [UtilManager width:40])];
    [_backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    _backButton.imageEdgeInsets = UIEdgeInsetsMake(5., 5., 5., 5.);
    
    [self addSubview:_backButton];
    
    _menuButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50],
                                                             [UtilManager height:30.],
                                                             [UtilManager width:40],
                                                             [UtilManager width:40])];
    
    [_menuButton setImage:[UIImage imageNamed:@"btn-menu-settings"] forState:UIControlStateNormal];
    [_menuButton addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    [_menuButton setHidden:NO];
    [_menuButton setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    [_menuButton setCenter:CGPointMake(_menuButton.center.x, _backButton.center.y)];
    
    [self addSubview:_menuButton];
    
    _showInfoButton  = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(_menuButton.frame) - [UtilManager width:50],
                                                                  [UtilManager height:30],
                                                                  [UtilManager width:40],
                                                                  [UtilManager width:40])];
    
    [_showInfoButton setImage:[UIImage imageNamed:@"btn_show_info"] forState:UIControlStateNormal];
    [_showInfoButton addTarget:self action:@selector(showInfoAction) forControlEvents:UIControlEventTouchUpInside];
    [_showInfoButton setHidden:NO];
    [_showInfoButton setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    [_showInfoButton setCenter:CGPointMake(_showInfoButton.center.x, _backButton.center.y)];
    [self addSubview:_showInfoButton];

    _userRateButton  = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(_showInfoButton.frame) - [UtilManager width:50],
                                                                  [UtilManager height:30.],
                                                                  [UtilManager width:40],
                                                                  [UtilManager width:40])];
    [_userRateButton setImage:[UIImage imageNamed:@"btn_user_rate"] forState:UIControlStateNormal];
    [_userRateButton addTarget:self action:@selector(userRateAction) forControlEvents:UIControlEventTouchUpInside];
    _userRateButton.hidden = NO;
    _userRateButton.imageEdgeInsets = UIEdgeInsetsMake(5., 5., 5., 5.);
    _userRateButton.center = CGPointMake(_userRateButton.center.x, _backButton.center.y);
    [self addSubview:_userRateButton];

    _userProfileButton  = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(_userRateButton.frame) - [UtilManager width:50],
                                                                     [UtilManager height:30.],
                                                                     [UtilManager width:40],
                                                                     [UtilManager width:40])];
    [_userProfileButton setImage:[UIImage imageNamed:@"btn_user_profile"] forState:UIControlStateNormal];
    [_userProfileButton addTarget:self action:@selector(userProfileAction) forControlEvents:UIControlEventTouchUpInside];
    _userProfileButton.hidden = NO;
    _userProfileButton.imageEdgeInsets = UIEdgeInsetsMake(5., 5., 5., 5.);
    _userProfileButton.center = CGPointMake(_userProfileButton.center.x, _backButton.center.y);
    [self addSubview:_userProfileButton];
    
    _showDetailInfoButton  = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(_userProfileButton.frame) - [UtilManager width:50],
                                                                        [UtilManager height:30.],
                                                                        [UtilManager width:40],
                                                                        [UtilManager width:40])];
    
    [_showDetailInfoButton setImage:[UIImage imageNamed:@"btn_show_detail_info"] forState:UIControlStateNormal];
    [_showDetailInfoButton addTarget:self action:@selector(showDetailInfoAction) forControlEvents:UIControlEventTouchUpInside];
    _showDetailInfoButton.hidden = NO;
    _showDetailInfoButton.imageEdgeInsets = UIEdgeInsetsMake(5., 5., 5., 5.);
    _showDetailInfoButton.center = CGPointMake(_showDetailInfoButton.center.x, _backButton.center.y);
    [self addSubview:_showDetailInfoButton];
    
    self.headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_backButton.frame),
                                                                 CGRectGetMinY(_backButton.frame) + [UtilManager height:30],
                                                                 WIDTH - 2 * CGRectGetMaxX(_backButton.frame) - [UtilManager width:20],
                                                                 [UtilManager height:45])];
    
    self.headerLabel.textAlignment = NSTextAlignmentCenter;
    self.headerLabel.numberOfLines = 2;
    self.headerLabel.backgroundColor = UIColor.clearColor;
    self.headerLabel.textAlignment = NSTextAlignmentCenter;
    self.headerLabel.textColor = UIColor.whiteColor;
    self.headerLabel.font = [UIFont fontWithName:BOLD_FONT size:21];
    self.headerLabel.adjustsFontSizeToFitWidth = YES;
    [self.headerScrollView addSubview:self.headerLabel];
    
    self.blurImages = [[NSMutableArray alloc] init];
    
    _contentTextView = [[UITextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_backButton.frame) + WIDTH,
                                                                    CGRectGetMaxY(_backButton.frame),
                                                                    CGRectGetWidth(self.headerLabel.frame),
                                                                    CGRectGetHeight(self.headerScrollView.frame) - CGRectGetMinY(_backButton.frame) - [UtilManager height:20])];
    
    _contentTextView.textAlignment = NSTextAlignmentJustified;
    _contentTextView.textColor = UIColor.whiteColor;
    _contentTextView.font = [UIFont fontWithName:REGULAR_FONT size:14];
    _contentTextView.adjustsFontForContentSizeCategory = YES;
    _contentTextView.editable = NO;
    _contentTextView.backgroundColor = UIColor.clearColor;
    [self.headerScrollView addSubview:_contentTextView];
    
    if (_selectType == MBTableWorkout) {
        self.headerLabel.text = _currentTraining.title;
        if(_currentTraining.content && _currentTraining.content.length > 0) {
            _contentTextView.text = _currentTraining.content;
        }
        else {
            _contentTextView.text = NSLocalizedString(@"Sin descripción", nil);
        }
    }
    else if (_selectType == MBTableTrainingPlan) {
        self.headerLabel.text = _currentTrainingPlan.title;
        if(_currentTrainingPlan.content && _currentTrainingPlan.content.length > 0)
            _contentTextView.text = _currentTrainingPlan.content;
        else
            _contentTextView.text = NSLocalizedString(@"Sin descripción", nil);
    } else if (_selectType == MBTableDiet){
        self.headerLabel.text = _currentDiet.title;
        if(_currentDiet.content && _currentDiet.content.length > 0)
            _contentTextView.text = _currentDiet.content;
        else
            _contentTextView.text = NSLocalizedString(@"Sin descripción", nil);
    } else if (_selectType == MBTableMeal){
        self.headerLabel.text = _currentMeal.title;
        if(_currentMeal.content && _currentMeal.content.length > 0)
            _contentTextView.text = _currentMeal.content;
        else
            _contentTextView.text = NSLocalizedString(@"Sin descripción", nil);
    }
    
    _favouriteButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(_menuButton.frame),
                                                                  CGRectGetMinY(_headerLabel.frame),
                                                                  CGRectGetWidth(_menuButton.frame),
                                                                  CGRectGetHeight(_menuButton.frame))];
    
    [_favouriteButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateNormal];
    _favouriteButton.hidden = YES;
    _favouriteButton.imageEdgeInsets = UIEdgeInsetsMake(5., 5., 5., 5.);
    
    switch (_selectType) {
        case MBTableWorkout:
            if(_currentTraining.isFavourite.boolValue)
                [_favouriteButton setHidden:FALSE];
            break;
        case MBTableTrainingPlan:
            if(_currentTrainingPlan.isFavourite.boolValue)
                [_favouriteButton setHidden:FALSE];
            break;
        case MBTableDiet:
            if(_currentDiet.isFavourite.boolValue)
                [_favouriteButton setHidden:FALSE];
            break;
        case MBTableMeal:
            if(_currentMeal.isFavourite.boolValue)
                [_favouriteButton setHidden:FALSE];
            break;
        default:
            break;
    }
    
    [self addSubview:_favouriteButton];
    
    NSArray *tags = [[NSArray alloc] init];
    
    if(_selectType == MBTableMeal) {
        tags = [Meal getMealTags:_currentMeal];
    }
    else if (_selectType == MBTableDiet) {
        tags = [Diet getTagsInDiet:_currentDiet];
    }
    else if (_selectType == MBTableNutritionPlan) {
        tags = [NutritionPlan getTagsInNutritionPlan:_currentNutritionPlan];
    }
    else if (_selectType == MBTableWorkout) {
        tags = [Training getTrainingsExercisesTags:_currentTraining];
    }
    else if (_selectType == MBTableTrainingPlan) {
        tags = [TrainingPlan getTrainingsTags:_currentTrainingPlan];
    }
    
    _tagView = [[TagView alloc] initWithFrame:CGRectMake(0., self.headerLabel.frame.origin.y + self.headerLabel.frame.size.height + [UtilManager height:5], WIDTH, TAGVIEW_SMALL_HEIGHT)]; //andTags:tags withTagsControlMode:TagsControlModeList];
    _tagView.mode = TagsControlModeList;
    _tagView.isCenter = YES;
    _tagView.tags = [tags mutableCopy];
    [_tagView reloadTagSubviews];
    _tagView.userInteractionEnabled = NO;
    
    [self.headerScrollView addSubview:_tagView];
    
    /////WORKOUT
    
    if(_selectType == MBTableWorkout) {
        _firstView = [[UIView alloc] initWithFrame:CGRectMake(0., _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:10], WIDTH/3, [UtilManager height:30])];
        [_firstView setBackgroundColor:[UIColor clearColor]];
        
        _exercisesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0., _firstView.frame.size.width, [UtilManager height:25])];
        _exercisesLabel.textAlignment = NSTextAlignmentRight;
        _exercisesLabel.textColor = GRAY_REGISTER_FONT;
        _exercisesLabel.font = [UIFont fontWithName:REGULAR_FONT_OPENSANS size:14];
        _exercisesLabel.adjustsFontSizeToFitWidth = YES;
        _exercisesLabel.center = CGPointMake(_exercisesLabel.center.x, _firstView.frame.size.height/2);
        [_firstView addSubview:_exercisesLabel];
        [self.headerScrollView addSubview:_firstView];
        
        _secondView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH/3, _firstView.frame.origin.y, _firstView.frame.size.width, _firstView.frame.size.height)];
        [_secondView setBackgroundColor:[UIColor clearColor]];
        
        _clockImageSize = [UIImage imageNamed:@"icon-cell-clock"].size;
        _kCalImageSize = [UIImage imageNamed:@"icon-kcal-red"].size;
        
        _clockImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:20], _exercisesLabel.frame.origin.y, [UtilManager width:25] * 0.8, [UtilManager width:25] * 0.8)];
        [_clockImageView setImage:[UIImage imageNamed:@"icon-cell-clock"]];
        [_clockImageView setCenter:CGPointMake(_secondView.frame.size.width/2 - [UtilManager width:20], _firstView.frame.size.height/2)];
        
        [_secondView addSubview:_clockImageView];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(_clockImageView.frame.origin.x + _clockImageView.frame.size.width+ [UtilManager width:5], _exercisesLabel.frame.origin.y,_secondView.frame.size.width, _exercisesLabel.frame.size.height)];
        [_timeLabel setText:@"47 min"];
        [_timeLabel setTextAlignment:NSTextAlignmentLeft];
        [_timeLabel setFont:_exercisesLabel.font];
        [_timeLabel setTextColor:_exercisesLabel.textColor];
        [_timeLabel setAdjustsFontSizeToFitWidth:YES];
        [_timeLabel setCenter:CGPointMake(_timeLabel.center.x, _firstView.frame.size.height/2)];
        
        [_secondView addSubview:_timeLabel];
        
        [self.headerScrollView addSubview:_secondView];
        
        _thirdView = [[UIView alloc] initWithFrame:CGRectMake(2 * WIDTH / 3, _firstView.frame.origin.y, _firstView.frame.size.width, _firstView.frame.size.height)];
        [_thirdView setBackgroundColor:[UIColor clearColor]];
        
        _kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:0], _timeLabel.frame.origin.y, [UtilManager width:20] * 0.8, [UtilManager width:20] * 0.8)];
        [_kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-red"]];
        [_kCalImageView setCenter:CGPointMake(_kCalImageView.center.x, _firstView.frame.size.height/2)];
        
        [_thirdView addSubview:_kCalImageView];
        
        _kCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(_kCalImageView.frame.origin.x + _kCalImageView.frame.size.width + [UtilManager width:5], _timeLabel.frame.origin.y, _thirdView.frame.size.width, _timeLabel.frame.size.height)];
        [_kCalLabel setTextColor:RED_APP_COLOR];
        [_kCalLabel setFont:_timeLabel.font];
        [_kCalLabel setTextAlignment:NSTextAlignmentLeft];
        [_kCalLabel setCenter:CGPointMake(_kCalLabel.center.x, _firstView.frame.size.height/2)];
        
        [_thirdView addSubview:_kCalLabel];
        
        [self.headerScrollView addSubview:_thirdView];
        
        [self reloadWorkoutValues];
    }
    else if (_selectType == MBTableTrainingPlan) {
    
        _firstView = [[UIView alloc] initWithFrame:CGRectMake(0., _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:10], WIDTH/2, [UtilManager height:30])];
        [_firstView setBackgroundColor:[UIColor clearColor]];
        
        _exercisesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0., _firstView.frame.size.width - [UtilManager width:10], [UtilManager height:25])];
        [_exercisesLabel setTextAlignment:NSTextAlignmentRight];
        [_exercisesLabel setTextColor:GRAY_REGISTER_FONT];
        [_exercisesLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [_exercisesLabel setAdjustsFontSizeToFitWidth:YES];
        [_exercisesLabel setText:NSLocalizedString(@"0 Entrenamientos", nil)];
        
        [_firstView addSubview:_exercisesLabel];
        
        [self.headerScrollView addSubview:_firstView];
        
        _secondView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH/2, _firstView.frame.origin.y, _firstView.frame.size.width, _firstView.frame.size.height)];
        [_secondView setBackgroundColor:[UIColor clearColor]];
        
        //CGSize _calendarImageSize = [UIImage imageNamed:@"icon-birth"].size;
        
        _calendarImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-birth"]];
        [_calendarImageView setFrame:CGRectMake([UtilManager width:10], 0, _calendarImageView.frame.size.width * 0.8, _calendarImageView.frame.size.height * 0.8)];
        
        [_secondView  addSubview:_calendarImageView];
        
        _daysLabel = [[UILabel alloc] initWithFrame:CGRectMake(_calendarImageView.frame.origin.x + _calendarImageView.frame.size.width + [UtilManager width:5], 0,_secondView.frame.size.width/2, _exercisesLabel.frame.size.height)];
        [_daysLabel setText:@"0 días"];
        [_daysLabel setTextAlignment:NSTextAlignmentLeft];
        [_daysLabel setFont:_exercisesLabel.font];
        [_daysLabel setTextColor:_exercisesLabel.textColor];
        [_daysLabel setAdjustsFontSizeToFitWidth:YES];
        
        [_calendarImageView setCenter:CGPointMake(_calendarImageView.center.x, _daysLabel.center.y)];
        
        [_secondView addSubview:_daysLabel];
        
        [self.headerScrollView addSubview:_secondView];
        
        [self reloadTrainingPlansValues];
        
    }
    else if(_selectType == MBTableMeal || _selectType == MBTableDiet || _selectType == MBTableNutritionPlan) {
        ////MEALS
        
        _firstView = [[UIView alloc] initWithFrame:CGRectMake(0., _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:10], WIDTH/2, [UtilManager height:20])];
        [_firstView setBackgroundColor:[UIColor clearColor]];
        
        _kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH/2 - [UtilManager width:30], 0., [UtilManager width:20] * 0.8, [UtilManager width:20] * 0.8)];
        [_kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-green"]];
        [_kCalImageView setCenter:CGPointMake(_kCalImageView.center.x, _firstView.frame.size.height/2)];
        
        [_firstView addSubview:_kCalImageView];
        
        _kCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(_kCalImageView.frame.origin.x + _kCalImageView.frame.size.width + [UtilManager width:5], 0, [UtilManager width:200], _firstView.frame.size.height)];
        [_kCalLabel setTextColor:GREEN_APP_COLOR];
        [_kCalLabel setFont:_timeLabel.font];
        [_kCalLabel setTextAlignment:NSTextAlignmentLeft];
        [_kCalLabel setCenter:CGPointMake(_kCalLabel.center.x, _firstView.frame.size.height/2)];
        
        [_firstView addSubview:_kCalLabel];
        
        [self.headerScrollView addSubview:_firstView];
        
        _proteinCircleView = [[PropertyCircleView alloc] initWithFrame:CGRectMake(self.headerLabel.frame.origin.x, _firstView.frame.origin.y + _firstView.frame.size.height + [UtilManager height:5], [UtilManager width:40], [UtilManager width:40])];
        [[_proteinCircleView layer] setBorderColor:BLUE_PERCENT_CIRCLE.CGColor];
        [[_proteinCircleView layer] setBorderWidth:1];
        [[_proteinCircleView layer] setCornerRadius:_proteinCircleView.frame.size.width/2];
        
        [[_proteinCircleView initialLabel] setText:@"P"];
        [[_proteinCircleView initialLabel] setTextColor:BLUE_PERCENT_CIRCLE];
        [[_proteinCircleView initialLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:8]];
        [[_proteinCircleView countLabel] setText:@"95 g"];
        [[_proteinCircleView countLabel] setTextColor:BLUE_PERCENT_CIRCLE];
        [[_proteinCircleView measureLabel] setTextColor:BLUE_PERCENT_CIRCLE];
        [[_proteinCircleView countLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [[_proteinCircleView measureLabel] setText:@"g"];
        [[_proteinCircleView measureLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:11]];
        
        [self.headerScrollView addSubview:_proteinCircleView];
        
        _carbsCircleView = [[PropertyCircleView alloc] initWithFrame:CGRectMake(_proteinCircleView.frame.origin.x + _proteinCircleView.frame.size.width + [UtilManager width:10], _proteinCircleView.frame.origin.y, _proteinCircleView.frame.size.width, _proteinCircleView.frame.size.height)];
        [[_carbsCircleView layer] setBorderColor:YELLOW_PERCENT_CIRCLE.CGColor];
        [[_carbsCircleView layer] setBorderWidth:1];
        [[_carbsCircleView layer] setCornerRadius:_carbsCircleView.frame.size.width/2];
        
        [[_carbsCircleView initialLabel] setText:@"C"];
        [[_carbsCircleView initialLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
        [[_carbsCircleView countLabel] setText:@"95 g"];
        [[_carbsCircleView countLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
        [[_carbsCircleView measureLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
        [[_carbsCircleView measureLabel] setText:@"g"];
        [[_carbsCircleView initialLabel] setFont:_proteinCircleView.initialLabel.font];
        [[_carbsCircleView countLabel] setFont:_proteinCircleView.countLabel.font];
        [[_carbsCircleView measureLabel] setFont:_proteinCircleView.measureLabel.font];
        
        [self.headerScrollView  addSubview:_carbsCircleView];
        
        _fatsCircleView = [[PropertyCircleView alloc] initWithFrame:CGRectMake(_carbsCircleView.frame.origin.x + _carbsCircleView.frame.size.width + [UtilManager width:10], _proteinCircleView.frame.origin.y, _proteinCircleView.frame.size.width, _proteinCircleView.frame.size.height)];
        [[_fatsCircleView layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
        [[_fatsCircleView layer] setBorderWidth:1];
        [[_fatsCircleView layer] setCornerRadius:_fatsCircleView.frame.size.width/2];
        
        [[_fatsCircleView initialLabel] setText:@"G"];
        [[_fatsCircleView initialLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
        [[_fatsCircleView countLabel] setText:@"95 g"];
        [[_fatsCircleView countLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
        [[_fatsCircleView measureLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
        [[_fatsCircleView measureLabel] setText:@"g"];
        [[_fatsCircleView initialLabel] setFont:_proteinCircleView.initialLabel.font];
        [[_fatsCircleView countLabel] setFont:_proteinCircleView.countLabel.font];
        [[_fatsCircleView measureLabel] setFont:_proteinCircleView.measureLabel.font];
        
        [self.headerScrollView addSubview:_fatsCircleView];
        
        [_carbsCircleView setCenter:CGPointMake(self.center.x, _carbsCircleView.center.y)];
        [_fatsCircleView setCenter:CGPointMake(self.center.x + _fatsCircleView.frame.size.width * 1.25, _fatsCircleView.center.y)];
        [_proteinCircleView setCenter:CGPointMake(self.center.x - _proteinCircleView.frame.size.width * 1.25, _proteinCircleView.center.y)];
    
    }
    
    if (_selectType == MBTableTrainingPlan || _selectType == MBTableNutritionPlan) {
        _simplifyButton  = [[UIButton alloc] initWithFrame:CGRectMake(_showInfoButton.frame.origin.x,
                                                                      [UtilManager height:210] - _menuButton.frame.size.height,
                                                                      _menuButton.frame.size.width,
                                                                      _menuButton.frame.size.height)];
        
        [_simplifyButton setImage:[UIImage imageNamed:@"btn-simplify-on"] forState:UIControlStateNormal];
        [_simplifyButton setImage:[UIImage imageNamed:@"btn-simplify-off"] forState:UIControlStateSelected];
        [_simplifyButton addTarget:self action:@selector(simplifyAction:) forControlEvents:UIControlEventTouchUpInside];
        _simplifyButton.hidden = NO;;
        _simplifyButton.imageEdgeInsets = UIEdgeInsetsMake(5., 5., 5., 5.);
        [self addSubview:_simplifyButton];
    }
    
    _optionButton  = [[UIButton alloc] initWithFrame:CGRectMake(_menuButton.frame.origin.x,[UtilManager height:210] - _menuButton.frame.size.height, _menuButton.frame.size.width, _menuButton.frame.size.height)];
    [_optionButton setImage:[UIImage imageNamed:@"btn-edit-on"] forState:UIControlStateNormal];
    [_optionButton addTarget:self action:@selector(optionAction) forControlEvents:UIControlEventTouchUpInside];
    [_optionButton setHidden:NO];
    [_optionButton setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    [self addSubview:_optionButton];
    
    if (_fromCalendar) {
        _showExerciseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _showExerciseButton.frame = CGRectMake(CGRectGetWidth(headerView.frame) * .15f,
                                              CGRectGetMaxY(_optionButton.frame) + [UtilManager height:5],
                                              CGRectGetWidth(headerView.frame) * .7f,
                                              [UtilManager height:35]);
        
        _showExerciseButton.layer.cornerRadius = CGRectGetHeight(_showExerciseButton.frame) / 2.f;
        _showExerciseButton.titleLabel.font = [UIFont fontWithName:REGULAR_FONT_OPENSANS size:16];
        _showExerciseButton.userInteractionEnabled = YES;
        [_showExerciseButton setTitle:@"ENTRENAR" forState:UIControlStateNormal];
        [_showExerciseButton addTarget:self action:@selector(didPressShowCalendarExercise:) forControlEvents:UIControlEventTouchUpInside];
        _showExerciseButton.backgroundColor = YELLOW_APP_COLOR;
        [self addSubview:_showExerciseButton];
    }
    
    [self.tableView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)favouriteAction{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    switch (_selectType) {
        case MBTableWorkout:{
            [realm transactionWithBlock:^{
                [_currentTraining setIsFavourite:FALSE];
            }];
        }
            break;
        case MBTableTrainingPlan:{
            [realm transactionWithBlock:^{
                [_currentTrainingPlan setIsFavourite:FALSE];
            }];
        }
        default:
            break;
    }
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [_favouriteButton setHidden:TRUE];
}

- (void)reloadWorkoutValues{
    int secs = 0;
    NSInteger kCal = 0;
    
    self.headerLabel.text = _currentTraining.title;
    
    for(TrainingExercise *exercise in _currentTraining.trainingExercises){
        secs = [TrainingExercise getExerciseTime:exercise] + secs;
        for(Serie *serie in exercise.series){
            kCal = kCal + [Serie kCalInSerie:serie];
        }
    }
    
    int minutes = secs / 60;
    
    if(minutes<60) {
        [_timeLabel setText:[NSString stringWithFormat:@"%d min",minutes]];
    }
    else{
        int hour = minutes / 60;
        minutes = minutes % 60;
    
         [_timeLabel setText:[NSString stringWithFormat:@"%d h %d min",hour,minutes]];
    }
    
    [_exercisesLabel setText:[NSString stringWithFormat:@"%lu Ejercicios",(unsigned long)_currentTraining.trainingExercises.count]];
    [_kCalLabel setText:[NSString stringWithFormat:@"%ld kCal",(long)kCal]];
    
    _tagView.tags = [Training getTrainingsExercisesTags:_currentTraining];
    [_tagView reloadTagSubviews];
}

- (void)reloadTrainingPlansValues{
    [_daysLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_currentTrainingPlan.trainingDay.count,NSLocalizedString(@"Días", nil)]];
    
    [_exercisesLabel setText:[NSString stringWithFormat:@"%d %@",[TrainingPlan numberOfTrainings:_currentTrainingPlan],NSLocalizedString(@"Entrenamientos", nil)]];
    [_headerLabel setText:_currentTrainingPlan.title];
    
    _tagView.tags = [TrainingPlan getTrainingsTags:_currentTrainingPlan];
    [_tagView reloadTagSubviews];
}

- (void)reloadMealValues{
    
    [_headerLabel setText:_currentMeal.title];
    
    [_kCalLabel setText:[NSString stringWithFormat:@"%d kCal", (int)[Meal getKCalInMeal:_currentMeal]]];
    [[_proteinCircleView countLabel] setText:[NSString stringWithFormat:@"%ld g",[Meal getMealProtein:_currentMeal]]];
    [[_carbsCircleView countLabel] setText:[NSString stringWithFormat:@"%ld g",[Meal getMealCarb:_currentMeal]]];
    [[_fatsCircleView countLabel] setText:[NSString stringWithFormat:@"%ld g",[Meal getMealFats:_currentMeal]]];
    
    _tagView.tags = [Meal getMealTags:_currentMeal];
    [_tagView reloadTagSubviews];
}

- (void)reloadDietValues {
    [_headerLabel setText:_currentDiet.title];
    
    [_kCalLabel setText:[NSString stringWithFormat:@"%d kCal", (int)[Diet getKCalInDiet:_currentDiet]]];
    [[_proteinCircleView countLabel] setText:[NSString stringWithFormat:@"%ld g",[Diet getDietProtein:_currentDiet]]];
    [[_carbsCircleView countLabel] setText:[NSString stringWithFormat:@"%ld g",[Diet getDietCarb:_currentDiet]]];
    [[_fatsCircleView countLabel] setText:[NSString stringWithFormat:@"%ld g",[Diet getDietFats:_currentDiet]]];
    
    _tagView.tags = [Diet getTagsInDiet:_currentDiet];
    [_tagView reloadTagSubviews];
}

- (void)reloadNutritionPlanValues {
    [_headerLabel setText:_currentNutritionPlan.title];
    
    if (_currentNutritionPlan.isFavourite &&
        _currentNutritionPlan.isFavourite.boolValue) {
        
        _favouriteButton.hidden = NO;
    }
    else {
        _favouriteButton.hidden = YES;
    }
    
    [_kCalLabel setText:[NSString stringWithFormat:@"%d kCal", (int)[NutritionPlan getKCalInNutritionPlan:_currentNutritionPlan]]];
    [[_proteinCircleView countLabel] setText:[NSString stringWithFormat:@"%ld g",[NutritionPlan getNutritionPlanProtein:_currentNutritionPlan]]];
    [[_carbsCircleView countLabel] setText:[NSString stringWithFormat:@"%ld g",[NutritionPlan getNutritionPlanCarb:_currentNutritionPlan]]];
    [[_fatsCircleView countLabel] setText:[NSString stringWithFormat:@"%ld g",[NutritionPlan getNutritionPlanFats:_currentNutritionPlan]]];
    
    _tagView.tags = [NutritionPlan getTagsInNutritionPlan:_currentNutritionPlan];
    [_tagView reloadTagSubviews];
}

- (void) setupView:(UIImage*)backgroundImage avatarImage:(UIImage *)avatarImage titleString:(NSString *)titleString subtitleString:(NSString *)subtitleString buttonTitle:(NSString *)buttonTitle scrollHeight:(CGFloat)height type:(MBType)type {
    
    // Header
    self.header = [[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 210)];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:210])];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = headerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [headerView addSubview:blurEffectView];
    
    [self.header addSubview:headerView];
    
    [self addSubview:self.header];
    
    /*
     //[_titleLabel setText:_currentTraining.title];
     [_titleHeaderLabel setText:@"Texto"];
     [self addSubview:_titleHeaderLabel];
     */
   
    // TableView
    self.tableView = [[UITableView alloc] initWithFrame:self.frame];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.showsVerticalScrollIndicator = NO;
        
    // TableView Header
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.header.frame.size.height)];
    [self addSubview:self.tableView];
    
    backgroundImage = [self imageWithColor:BLACK_APP_COLOR];

    self.headerImageView = [[UIImageView alloc] initWithFrame:self.header.frame];
    self.headerImageView.image = backgroundImage;
    [self.headerImageView setAlpha:0.8];
    self.headerImageView.contentMode = UIViewContentModeScaleAspectFill;
    //[self.header insertSubview:self.headerImageView aboveSubview:self.headerLabel];
    self.header.clipsToBounds = YES;
    
    self.blurImages = [[NSMutableArray alloc] init];
    
    _backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:20], [UtilManager width:40], [UtilManager width:40])];
    [_backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [_backButton setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    [self addSubview:_backButton];
    
    _menuButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 50.,[UtilManager height:30.], [UtilManager width:40], [UtilManager width:40])];
    [_menuButton setImage:[UIImage imageNamed:@"btn-menu-settings"] forState:UIControlStateNormal];
    [_menuButton addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    [_menuButton setHidden:FALSE];
    [_menuButton setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    [_menuButton setCenter:CGPointMake(_menuButton.center.x, _backButton.center.y)];
    
    [self addSubview:_menuButton];
    
    self.headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_backButton.frame),
                                                                 CGRectGetMinY(_backButton.frame) + [UtilManager height:30],
                                                                 WIDTH - 2 * CGRectGetMaxX(_backButton.frame) - [UtilManager width:20],
                                                                 [UtilManager height:45])];
    
    self.headerLabel.textAlignment = NSTextAlignmentCenter;
    self.headerLabel.text = titleString;
    self.headerLabel.numberOfLines = 2;
    self.headerLabel.backgroundColor = UIColor.clearColor;
    self.headerLabel.textAlignment = NSTextAlignmentCenter;
    self.headerLabel.textColor = UIColor.whiteColor;
    self.headerLabel.font = [UIFont fontWithName:BOLD_FONT size:21];
    self.headerLabel.adjustsFontSizeToFitWidth = YES;
    [self.headerScrollView addSubview:self.headerLabel];
    
    NSMutableArray *tags = [[NSMutableArray alloc] init];
    
    if(_selectType == MBTableMeal)
        tags = [Tag getTagsWithType:@"type_1" andCollection:@"Meals"];
    else if (MBTableWorkout)
        tags = [Tag getTagsWithType:@"type_1" andCollection:@"Meals"];
    
    _tagView = [[TagView alloc] initWithFrame:CGRectMake(0., self.headerLabel.frame.origin.y + self.headerLabel.frame.size.height + [UtilManager height:5], WIDTH, TAGVIEW_SMALL_HEIGHT)]; //andTags:tags withTagsControlMode:TagsControlModeList];
    [_tagView setMode:TagsControlModeList];
    [_tagView setIsCenter:TRUE];
    [_tagView setTags:tags];
    [_tagView reloadTagSubviews];
    [_tagView setUserInteractionEnabled:FALSE];
    
    [self addSubview:_tagView];
    
    _proteinCircleView = [[PropertyCircleView alloc] initWithFrame:CGRectMake(self.headerLabel.frame.origin.x, _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:20], [UtilManager width:60], [UtilManager width:60])];
    [[_proteinCircleView layer] setBorderColor:BLUE_PERCENT_CIRCLE.CGColor];
    [[_proteinCircleView layer] setBorderWidth:1];
    [[_proteinCircleView layer] setCornerRadius:_proteinCircleView.frame.size.width/2];
    
    [[_proteinCircleView initialLabel] setText:@"P"];
    [[_proteinCircleView initialLabel] setTextColor:BLUE_PERCENT_CIRCLE];
    [[_proteinCircleView initialLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:11]];
    [[_proteinCircleView countLabel] setText:@"95 g"];
    [[_proteinCircleView countLabel] setTextColor:BLUE_PERCENT_CIRCLE];
    [[_proteinCircleView measureLabel] setTextColor:BLUE_PERCENT_CIRCLE];
    [[_proteinCircleView countLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:21]];
    [[_proteinCircleView measureLabel] setText:@"g"];
    [[_proteinCircleView measureLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:11]];
    
    [self addSubview:_proteinCircleView];
    
    _carbsCircleView = [[PropertyCircleView alloc] initWithFrame:CGRectMake(_proteinCircleView.frame.origin.x + _proteinCircleView.frame.size.width + [UtilManager width:10], _proteinCircleView.frame.origin.y, _proteinCircleView.frame.size.width, _proteinCircleView.frame.size.height)];
    [[_carbsCircleView layer] setBorderColor:YELLOW_PERCENT_CIRCLE.CGColor];
    [[_carbsCircleView layer] setBorderWidth:1];
    [[_carbsCircleView layer] setCornerRadius:_carbsCircleView.frame.size.width/2];
    
    [[_carbsCircleView initialLabel] setText:@"C"];
    [[_carbsCircleView initialLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
    [[_carbsCircleView countLabel] setText:@"95 g"];
    [[_carbsCircleView countLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
    [[_carbsCircleView measureLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
    [[_carbsCircleView measureLabel] setText:@"g"];
    [[_carbsCircleView initialLabel] setFont:_proteinCircleView.initialLabel.font];
    [[_carbsCircleView countLabel] setFont:_proteinCircleView.countLabel.font];
    [[_carbsCircleView measureLabel] setFont:_proteinCircleView.measureLabel.font];
    
    [self  addSubview:_carbsCircleView];
    
    _fatsCircleView = [[PropertyCircleView alloc] initWithFrame:CGRectMake(_carbsCircleView.frame.origin.x + _carbsCircleView.frame.size.width + [UtilManager width:10], _proteinCircleView.frame.origin.y, _proteinCircleView.frame.size.width, _proteinCircleView.frame.size.height)];
    [[_fatsCircleView layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
    [[_fatsCircleView layer] setBorderWidth:1];
    [[_fatsCircleView layer] setCornerRadius:_fatsCircleView.frame.size.width/2];
    
    [[_fatsCircleView initialLabel] setText:@"G"];
    [[_fatsCircleView initialLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_fatsCircleView countLabel] setText:@"95 g"];
    [[_fatsCircleView countLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_fatsCircleView measureLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_fatsCircleView measureLabel] setText:@"g"];
    [[_fatsCircleView initialLabel] setFont:_proteinCircleView.initialLabel.font];
    [[_fatsCircleView countLabel] setFont:_proteinCircleView.countLabel.font];
    [[_fatsCircleView measureLabel] setFont:_proteinCircleView.measureLabel.font];
    
    [self addSubview:_fatsCircleView];
    
    [_carbsCircleView setCenter:CGPointMake(self.center.x, _carbsCircleView.center.y)];
    [_fatsCircleView setCenter:CGPointMake(self.center.x + _fatsCircleView.frame.size.width * 1.25, _fatsCircleView.center.y)];
    [_proteinCircleView setCenter:CGPointMake(self.center.x - _proteinCircleView.frame.size.width * 1.25, _proteinCircleView.center.y)];
    
    _optionButton  = [[UIButton alloc] initWithFrame:CGRectMake(_menuButton.frame.origin.x,[UtilManager height:250] - _menuButton.frame.size.height, _menuButton.frame.size.width, _menuButton.frame.size.height)];
    [_optionButton setImage:[UIImage imageNamed:@"btn-menu-settings"] forState:UIControlStateNormal];
    [_optionButton addTarget:self action:@selector(optionAction) forControlEvents:UIControlEventTouchUpInside];
    [_optionButton setHidden:FALSE];
    [_optionButton setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    [self addSubview:_optionButton];
}


- (void)didPressBackButton {
    [_delegate backButtonDidPress];
}

- (void)didPressShowCalendarExercise:(UIButton*)sender {
    [_delegate didPressShowExerciseInCalendar];
}

- (void)moreAction {
    [_delegate optionButtonDidPress];
}

- (void)simplifyAction:(UIButton*)sender {
    sender.selected = !sender.selected;
    [_delegate didPressSimplifyButton:sender];
}

- (void)optionAction {
    
    if([_optionButton.imageView.image isEqual:[UIImage imageNamed:@"btn-edit-on"]])
        [_optionButton setImage:[UIImage imageNamed:@"btn-edit-off"] forState:UIControlStateNormal];
    else
        [_optionButton setImage:[UIImage imageNamed:@"btn-edit-on"] forState:UIControlStateNormal];
    
    [_delegate editButtonDidPress];
}

- (void)showInfoAction {
    if ([_delegate respondsToSelector:@selector(didPressShowInfoButton)]) {
        [_delegate didPressShowInfoButton];
    }
}

- (void)userRateAction {
    if ([_delegate respondsToSelector:@selector(didPressUserRateButton)]) {
        [_delegate didPressUserRateButton];
    }
}

- (void)userProfileAction {
    if ([_delegate respondsToSelector:@selector(didPressUserProfileButton)]) {
        [_delegate didPressUserProfileButton];
    }
}

- (void)showDetailInfoAction {
    if ([_delegate respondsToSelector:@selector(didPressShowDetailInfoButton)]) {
        [_delegate didPressShowDetailInfoButton];
    }
}

#pragma mark UIScrollView Delegate Methods

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if(scrollView.contentOffset.x > 0)
        _pageControl.currentPage = 1;
    else
        _pageControl.currentPage = 0;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView.contentOffset.x > 0)
        _pageControl.currentPage = 1;
    else
        _pageControl.currentPage = 0;
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    CGFloat offset = self.tableView.contentOffset.y;
    
    _showInfoButton.layer.zPosition = 3;
    _userRateButton.layer.zPosition = 3;
    _userProfileButton.layer.zPosition = 3;
    _showDetailInfoButton.layer.zPosition = 3;
    
    if (offset>0) {
        _showInfoButton.hidden = YES;
        _userRateButton.hidden = YES;
        _userProfileButton.hidden = YES;
        _showDetailInfoButton.hidden = YES;
    }
    else {
        _showInfoButton.hidden = NO;
        _userRateButton.hidden = NO;
        _userProfileButton.hidden = NO;
        _showDetailInfoButton.hidden = NO;
    }
    
    [self animationForScroll:offset];
}


- (void) animationForScroll:(CGFloat) offset {
    
    NSLog(@"OFFSET %f",offset);
    
    CATransform3D headerTransform = CATransform3DIdentity;
    
    headerTransform = CATransform3DTranslate(headerTransform, 0, MAX(-offset_HeaderStop, -offset), 0);
    
    if (MAX(-distance_W_LabelHeader, offset_B_LabelHeader - offset) > 0) {
        if (offset_B_LabelHeader - offset < 0) {
            
            CATransform3D labelTransform = CATransform3DMakeTranslation(0, MAX(-distance_W_LabelHeader, offset_B_LabelHeader - offset), 0);
            self.headerLabel.layer.transform = labelTransform;
            self.headerLabel.layer.zPosition = 2;
            
            _tagView.layer.transform = labelTransform;
            _tagView.layer.zPosition = 2;
            
            if (_selectType == MBTableWorkout) {
                
                CATransform3D viewTransform = CATransform3DMakeTranslation(0, MAX(-distance_W_LabelHeader, offset_B_LabelHeader - offset), 0);
                
                _firstView.layer.transform = viewTransform;
                _firstView.layer.zPosition = 2;
                
                _secondView.layer.transform = viewTransform;
                _secondView.layer.zPosition = 2;
                
                _thirdView.layer.transform = viewTransform;
                _thirdView.layer.zPosition = 2;
                
                if(offset>80){
                    [_firstView setHidden:TRUE];
                    [_secondView setHidden:TRUE];
                    [_thirdView setHidden:TRUE];
                } else {
                    [_firstView setHidden:FALSE];
                    [_secondView setHidden:FALSE];
                    [_thirdView setHidden:FALSE];
                }
                
            }
            else if (_selectType == MBTableTrainingPlan) {
                CATransform3D viewTransform = CATransform3DMakeTranslation(0, MAX(-distance_W_LabelHeader, offset_B_LabelHeader - offset), 0);
                
                _firstView.layer.transform = viewTransform;
                _firstView.layer.zPosition = 2;
                
                _secondView.layer.transform = viewTransform;
                _secondView.layer.zPosition = 2;
                
                if (offset > [UtilManager height:80]) {
                    _firstView.hidden = YES;
                    _secondView.hidden = YES;
                    _thirdView.hidden = YES;
                }
                else {
                    _firstView.hidden = NO;
                    _secondView.hidden = NO;
                    _thirdView.hidden = NO;
                }
            }
            else {
                ////_proteinCircleView
                CATransform3D proteinTransform = CATransform3DMakeTranslation(0, MAX(-distance_W_LabelHeader, offset_B_LabelHeader - offset), 0);
                _proteinCircleView.layer.transform = proteinTransform;
                _proteinCircleView.layer.zPosition = 2;
                
                _carbsCircleView.layer.transform = proteinTransform;
                _carbsCircleView.layer.zPosition = 2;
                
                _fatsCircleView.layer.transform = proteinTransform;
                _fatsCircleView.layer.zPosition = 2;
                
                
                if(offset>80){
                    [_fatsCircleView setHidden:TRUE];
                    [_carbsCircleView setHidden:TRUE];
                    [_proteinCircleView setHidden:TRUE];
                    [_firstView setHidden:TRUE];
                    [_secondView setHidden:TRUE];
                    [_thirdView setHidden:TRUE];
                } else {
                    [_fatsCircleView setHidden:FALSE];
                    [_carbsCircleView setHidden:FALSE];
                    [_proteinCircleView setHidden:FALSE];
                    [_firstView setHidden:FALSE];
                    [_secondView setHidden:FALSE];
                    [_thirdView setHidden:FALSE];
                }
            }
        }
        self.headerScrollView.layer.transform = headerTransform;
    }
    else {
        if (_selectType == MBTableTrainingPlan) {
            if (offset > [UtilManager height:80]) {
                _firstView.hidden = YES;
                _secondView.hidden = YES;
                _thirdView.hidden = YES;
            }
            else {
                _firstView.hidden = NO;
                _secondView.hidden = NO;
                _thirdView.hidden = NO;
            }
        }
        else if(_selectType == MBTableMeal || _selectType == MBTableDiet || _selectType == MBTableNutritionPlan){
            if(offset>44){
                [_fatsCircleView setHidden:TRUE];
                [_carbsCircleView setHidden:TRUE];
                [_proteinCircleView setHidden:TRUE];
            }else{
                [_fatsCircleView setHidden:FALSE];
                [_carbsCircleView setHidden:FALSE];
                [_proteinCircleView setHidden:FALSE];
            }
            if(offset>80){
                [_firstView setHidden:TRUE];
            }else{
                [_firstView setHidden:FALSE];
            }
        
        }
    }
    
    if (offset <= offset_HeaderStop) {
    }
    else {
        self.header.layer.zPosition = 2;
        self.headerScrollView.layer.zPosition = 2;
        _menuButton.layer.zPosition = 3;
        _backButton.layer.zPosition = 3;
        _optionButton.layer.zPosition = 3;
        _simplifyButton.layer.zPosition = 3;
        _favouriteButton.layer.zPosition = 3;
        _showExerciseButton.layer.zPosition = 3;
    }
    
    if(offset>15) {
        _optionButton.hidden = YES;
        _simplifyButton.hidden = YES;
        _showExerciseButton.hidden = YES;
    }
    else {
        _optionButton.hidden = NO;
        _simplifyButton.hidden = NO;
        _showExerciseButton.hidden = NO;
    }
    
    if (self.headerImageView.image != nil) {
        [self blurWithOffset:offset];
    }
    
    self.header.layer.transform = headerTransform;
}

- (void)prepareForBlurImages
{
    CGFloat factor = 0.1;
    [self.blurImages addObject:self.headerImageView.image];
    for (NSUInteger i = 0; i < self.headerImageView.frame.size.height/10; i++) {
        [self.blurImages addObject:[self.headerImageView.image boxblurImageWithBlur:factor]];
        factor+=0.04;
    }
}


- (void) blurWithOffset:(float)offset {
    /*
    NSInteger index = offset / 10;
    if (index < 0) {
        index = 0;
    }
    else if(index >= self.blurImages.count) {
        index = self.blurImages.count - 1;
    }
    UIImage *image = self.blurImages[index];
    if (self.headerImageView.image != image) {
        [self.headerImageView setImage:image];
    }
     */
}



- (void) recievedMBTwitterScrollButtonClicked {
    [self.delegate recievedMBTwitterScrollButtonClicked];
}



- (void) recievedMBTwitterScrollEvent {
    [self.delegate recievedMBTwitterScrollEvent];
}


// Function to blur the header image (used if the header image is replaced with updateHeaderImage)
-(void)resetBlurImages {
    [self.blurImages removeAllObjects];
    [self prepareForBlurImages];
}


// Function to update the header image and blur it out appropriately
-(void)updateHeaderImage:(UIImage*)newImage {
    self.headerImageView.image = newImage;
    [self resetBlurImages];
}



-(void) dealloc {
    [self.tableView removeObserver:self forKeyPath:@"contentOffset"];
}


@end
