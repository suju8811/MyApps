//
//  MBTwitterScroll.h
//  TwitterScroll
//
//  Created by Martin Blampied on 07/02/2015.
//  Copyright (c) 2015 MartinBlampied. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyCircleView.h"
#import "Tag.h"
#import "TagView.h"
#import "Training.h"
#import "TrainingPlan.h"
#import "Meal.h"
#import "Diet.h"
#import "NutritionPlan.h"

typedef enum : NSUInteger {
    MBTableMeal,
    MBTableWorkout,
    MBTableTrainingPlan,
    MBTableDiet,
    MBTableTrainingDay,
    MBTableNutritionPlan
} MBType;


@protocol MBTwitterScrollDelegate <NSObject>

-(void)recievedMBTwitterScrollEvent;

@optional

-(void)recievedMBTwitterScrollButtonClicked;
- (void)editButtonDidPress;
- (void)backButtonDidPress;
- (void)optionButtonDidPress;
- (void)didPressShowInfoButton;
- (void)didPressUserRateButton;
- (void)didPressUserProfileButton;
- (void)didPressShowDetailInfoButton;
- (void)didPressSimplifyButton:(UIButton*)sender;
- (void)didPressShowExerciseInCalendar;
@end

@interface MBTwitterScroll : UIView <UIScrollViewDelegate, MBTwitterScrollDelegate >

- (MBTwitterScroll *)initTableViewWithBackgound:(UIImage*)backgroundImage avatarImage:(UIImage *)avatarImage titleString:(NSString *)titleString subtitleString:(NSString *)subtitleString buttonTitle:(NSString *)buttonTitle;

- (MBTwitterScroll *)initWithType:(MBType)type;
- (void)setupView;
- (void)reloadWorkoutValues;
- (void)reloadTrainingPlansValues;
- (void)reloadMealValues;
- (void)reloadDietValues;
- (void)reloadNutritionPlanValues;

-(void)updateHeaderImage:(UIImage*)newImage;

@property (strong, nonatomic) UIScrollView *headerScrollView;

@property (strong, nonatomic) UIView *header;
@property (strong, nonatomic) UILabel *headerLabel;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIImageView *headerImageView;
@property (strong, nonatomic) UIButton *headerButton;

@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) UIButton *menuButton;
@property (strong, nonatomic) UIButton *showInfoButton;
@property (strong, nonatomic) UIButton *userRateButton;
@property (strong, nonatomic) UIButton *userProfileButton;
@property (strong, nonatomic) UIButton *showDetailInfoButton;
@property (strong, nonatomic) UIButton *optionButton;
@property (strong, nonatomic) UIButton *simplifyButton;
@property (strong, nonatomic) UIButton *favouriteButton;

@property (assign, nonatomic) MBType selectType;

@property (strong, nonatomic) Training *currentTraining;
@property (strong, nonatomic) TrainingPlan *currentTrainingPlan;
@property (strong, nonatomic) NutritionPlan *currentNutritionPlan;
@property (strong, nonatomic) Meal *currentMeal;
@property (strong, nonatomic) Diet *currentDiet;
@property (strong, nonatomic) NutritionPlan *nutritionPlan;

@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) UITextView *contentTextView;

///Workout

@property (strong, nonatomic) UILabel *kCalLabel;
@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) UILabel *exercisesLabel;
@property (strong, nonatomic) UIImageView *clockImageView;
@property (strong, nonatomic) UIImageView *kCalImageView;
@property (strong, nonatomic) UIView *firstView;
@property (strong, nonatomic) UIView *secondView;
@property (strong, nonatomic) UIView *thirdView;
@property (nonatomic, assign) CGSize clockImageSize;
@property (nonatomic, assign) CGSize kCalImageSize;
@property (nonatomic, assign) BOOL fromCalendar;
@property (nonatomic, strong) UIButton * showExerciseButton;

///TrainingPlan

@property (strong, nonatomic) UILabel *daysLabel;
@property (strong, nonatomic) UIImageView *calendarImageView;

///Meal

@property (nonatomic, strong) PropertyCircleView *proteinCircleView;
@property (nonatomic, strong) PropertyCircleView *carbsCircleView;
@property (nonatomic, strong) PropertyCircleView *fatsCircleView;

@property (nonatomic, strong) TagView *tagView;

@property (nonatomic, strong) NSMutableArray *blurImages;
@property (nonatomic,strong) id<MBTwitterScrollDelegate>delegate;

@end
