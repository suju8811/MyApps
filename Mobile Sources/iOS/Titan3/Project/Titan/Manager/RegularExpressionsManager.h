//
//  RegularExpressionsManager.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface RegularExpressionsManager : NSObject

+ (BOOL)checkMail:(NSString *)checkString;
+ (BOOL)checkNIF:(NSString *)nifString;

@end
