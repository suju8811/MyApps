//
//  UtilManager.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "UtilManager.h"
#import "AppConstants.h"
#import "DeviceManager.h"

//The Mockup with Sketch is doing with iPhone 6 size
#define iPHONE_6_WIDTH 375
#define iPHONE_6_HEIGHT 667

@implementation UtilManager

+ (CGFloat)height:(CGFloat)pixels{
    return (pixels /iPHONE_6_HEIGHT) * HEIGHT;
}

+ (CGFloat)width:(CGFloat)pixels{
    return (pixels /iPHONE_6_WIDTH) * WIDTH;
}

+ (CGFloat)heightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font
{
    CGFloat result = font.pointSize + 4;
    if (text)
    {
        CGSize textSize = { widthValue, CGFLOAT_MAX };
        CGSize size;
        CGRect frame = [text boundingRectWithSize:textSize
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{ NSFontAttributeName:font }
                                          context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height+1);
        
        result = MAX(size.height, result);
    }
    return result;
}

+ (UIColor *)colorwithHexString:(NSString *)hexStr alpha:(CGFloat)alpha;
{
    unsigned int hexint = 0;
    
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    [scanner setCharactersToBeSkipped:[NSCharacterSet
                                       characterSetWithCharactersInString:@"#"]];
    [scanner scanHexInt:&hexint];
    
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];
    
    return color;
}

+ (UIAlertController *)presentAlertWithMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *acceptAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
    }];
    
    [alertController addAction:acceptAction];
    
    return alertController;
}

+ (UIAlertController *)presentAlertWithMessage:(NSString *)message andTitle:(NSString *)title {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *acceptAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
    }];
    
    [alertController addAction:acceptAction];
    
    return alertController;
}

+ (UIAlertController *)presentAlertWithMessage:(NSString *)message
                                      andTitle:(NSString *)title
                                   alertAction:(UIAlertAction*)alertAction {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:alertAction];
    
    return alertController;
}

+ (NSDictionary *)getMetadataDictionary{

    NSDictionary *metadataDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[DeviceManager uniqueIDForDevice],@"deviceID",[DeviceManager deviceName],@"deviceName",[DeviceManager deviceModel],@"deviceModel",[DeviceManager deviceLocalizedModel],@"deviceLocalizedModel",@"iOS",@"os",[DeviceManager deviceSystemName],@"deviceSystemName",[DeviceManager deviceSystemName] ,@"deviceSystemVersion",nil];
    
    return metadataDictionary;
}

+ (NSDateFormatter *)apiDateFormatter{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    return formatter;
}

+ (NSString *)uuid
{
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    return (__bridge_transfer NSString *)uuidStringRef;
}

+ (NSNumber*)floatFromString:(NSString*)string {
    if (string == nil) {
        return nil;
    }
    
    return @([string floatValue]);
}

+ (NSNumber*)intFromString:(NSString*)string {
    if (string == nil) {
        return nil;
    }
    
    return @([string intValue]);
}

+ (NSNumber*)boolFromString:(NSString*)string {
    if (string == nil) {
        return nil;
    }
    
    return @([string boolValue]);
}

@end
