//
//  ParserManager.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Training.h"
#import "User.h"

@interface ParserManager : NSObject

+ (BOOL)parseAuthenticate:(NSArray<NSDictionary*>*)responseArray;
+ (User *)parseUser:(NSDictionary *)response;
+ (BOOL)parseUserLogs:(NSArray *)response;
+ (BOOL)parseUserLog:(NSDictionary *)dataDictionary;

+ (BOOL)parseMuscles:(NSDictionary *)response;
+ (BOOL)parseExercises:(NSArray<NSDictionary*>*)responseArray;
//+ (BOOL)parseExercises:(NSDictionary *)response;
+ (BOOL)parseTrainings:(NSArray<NSDictionary*>*) responseArray;
+ (Training *)parseTraining:(NSDictionary *)response;
+ (BOOL)parseTrainingPlans:(NSArray<NSDictionary*>*)response;
+ (BOOL)parseSchedule:(NSArray<NSDictionary*>*)response;
+ (BOOL)parseConstants:(NSDictionary *)response;

+ (BOOL)parseFoods:(NSDictionary *)response;
+ (BOOL)parseAliments:(NSArray<NSDictionary*>*)response;
+ (BOOL)parseMeals:(NSArray *)response;
+ (BOOL)parseDiets:(NSArray<NSDictionary*>*)response;
+ (BOOL)parseNutritionsPlans:(NSArray<NSDictionary*>*)response;

+ (BOOL)parseTags:(NSArray *)response;

@end
