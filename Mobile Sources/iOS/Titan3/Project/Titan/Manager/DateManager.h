//
//  DateManager.h
//
//  Created by Manuel Manzanera on 10/11/16.
//  Copyright (c) 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateManager : NSObject

+ (NSDate *)dateWithString:(NSString *)dateString withDateFormat:(NSString *)dateFormatter;
+ (NSString *)stringWithDateAndFormat:(NSDate *)date;
+ (NSDateComponents *)dateComponentsWithDate:(NSDate *)date;
+ (NSString *)weekDayWithInteger:(NSInteger)weekDay;
+ (NSString *)monthWithInteger:(NSInteger)month;

+ (NSString *)stringWithDate:(NSDate *)date;

+ (NSDate *)endOfDay:(NSDate *)date;
+ (NSDate *)startOfDay:(NSDate *)date;

@end
