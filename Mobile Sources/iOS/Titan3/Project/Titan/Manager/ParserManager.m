//
//  ParserManager.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ParserManager.h"
#import "AppContext.h"
#import "User.h"
#import "UserDefaultLibrary.h"
#import "AppConstants.h"
#import "Muscle.h"
#import "Exercise.h"
#import "Food.h"
#import "Aliment.h"
#import "Meal.h"
#import "Diet.h"
#import "NutritionPlan.h"
#import "NutritionDay.h"
#import "Ingredient.h"
#import "Tag.h"
#import "TrainingExercise.h"
#import "TrainingDayContent.h"
#import "TrainingPlan.h"
#import "TrainingDay.h"
#import "Serie.h"
#import "Log.h"
#import "UtilManager.h"
#import "ValueConstant.h"
#import "JSONKit.h"
#import "AlimentAddExecutive.h"
#import "Event.h"
#import "Schedule.h"

@implementation ParserManager

+ (BOOL)parseAuthenticate:(NSArray<NSDictionary*>*)responseArray {
    
    if (responseArray == nil || responseArray.count == 0) {
        return NO;
    }
    
    NSDictionary *dataDictionary = [responseArray firstObject];
    
    [[AppContext sharedInstance] setAuthToken:[dataDictionary valueForKey:@"token"]];
    [[AppContext sharedInstance] setUserId:[dataDictionary valueForKey:@"userId"]];
    
    return YES;
}

+ (User *)parseUser:(NSDictionary *)response{
    NSDictionary *dataDictionary = [response valueForKey:@"data"];
    
    //
    NSDictionary *profileDictionary = [dataDictionary valueForKey:@"profile"];
    //
    User *currentUser = [[User alloc] init];
    //
    [currentUser setUserId:[dataDictionary valueForKey:@"userId"]];
    //
    [currentUser setName:[profileDictionary valueForKey:@"firstName"]];
    [currentUser setLastName:[profileDictionary valueForKey:@"lastName"]];
    [currentUser setGender:[profileDictionary valueForKey:@"gender"]];
    [currentUser setWeight:[profileDictionary valueForKey:@"weight"]];
    [currentUser setHeight:[profileDictionary valueForKey:@"height"]];
    [currentUser setAge:[profileDictionary valueForKey:@"age"]];
    [currentUser setBmr:[profileDictionary valueForKey:@"bmr"]];
    [currentUser setPicture:[profileDictionary valueForKey:@"picture"]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    [currentUser setBirthDate:[formatter dateFromString:[profileDictionary valueForKey:@"birthDate"]]];
    
    return currentUser;
}

+ (BOOL)parseUserLogs:(NSArray *)response {
    NSMutableArray *logsAux = [[NSMutableArray alloc] init];
    
    for(NSDictionary *logDictionary in response){
        Log *log = [[Log alloc] init];
        [log parseFromDictionary:logDictionary];
        [logsAux addObject:log];
    }
    
    [Log saveLogs:logsAux];
    
    return YES;
}

+ (BOOL)parseUserLog:(NSDictionary *)dataDictionary{
    //NSArray *dataDictionary = [response valueForKey:@"data"];
    
    NSDictionary *logDictionary = [dataDictionary valueForKey:@"data"];
    NSMutableArray *logsAux = [[NSMutableArray alloc] initWithCapacity:1];
        Log *log = [[Log alloc] init];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        
        [log setCreateAt:[formatter dateFromString:[logDictionary valueForKey:@"createdAt"]]];
        [log setUpdatedAt:[formatter dateFromString:[logDictionary valueForKey:@"updatedAt"]]];
    
        [log setRevisionDate:[formatter dateFromString:[logDictionary valueForKey:@"revisionDate"]]];
        
        [log setLogId:[logDictionary valueForKey:@"_id"]];
        [log setBack:[logDictionary valueForKey:@"chest"]];
        [log setBmr:[logDictionary valueForKey:@"bmr"]];
        [log setIgc:[logDictionary valueForKey:@"igc"]];
        [log setHeight:[logDictionary valueForKey:@"height"]];
        [log setLeftForearm:[logDictionary valueForKey:@"leftForeArm"]];
        [log setLeftThigh:[logDictionary valueForKey:@"leftThigh"]];
        [log setOwner:[logDictionary valueForKey:@"owner"]];
        [log setRightForearm:[logDictionary valueForKey:@"rightForeArm"]];
        [log setRightThigh:[logDictionary valueForKey:@"rightThigh"]];
        [log setWaist:[logDictionary valueForKey:@"waist"]];
        [log setWeight:[logDictionary valueForKey:@"weight"]];
        
        NSDictionary *basicScoresDictionary = [logDictionary valueForKey:@"basicScores"];
        
        [log setSquat:[basicScoresDictionary valueForKey:@"squatScore"]];
        [log setBenchPress:[basicScoresDictionary valueForKey:@"benchPressScore"]];
        [log setMilitaryPress:[basicScoresDictionary valueForKey:@"militaryPressScore"]];
        [log setDeadWeight:[basicScoresDictionary valueForKey:@"deadWeightScore"]];
        [log setDominated:[NSNumber numberWithInt:20]];
        
        NSArray *pictures = [logDictionary valueForKey:@"pictures"];
        if(pictures.count > 0){
            int index = 0;
            
            for(NSDictionary *pictureDic in pictures){
                
                switch (index) {
                    case 0:
                        [log setPicture1:[pictureDic valueForKey:@"picture"]];
                        break;
                    case 1:
                        [log setPicture2:[pictureDic valueForKey:@"picture"]];
                        break;
                    case 2:
                        [log setPicture3:[pictureDic valueForKey:@"picture"]];
                        break;
                    case 3:
                        [log setPicture4:[pictureDic valueForKey:@"picture"]];
                        break;
                    case 4:
                        [log setPicture5:[pictureDic valueForKey:@"picture"]];
                        break;
                    case 5:
                        [log setPicture6:[pictureDic valueForKey:@"picture"]];
                        break;
                    default:
                        break;
                }
                
                index ++;
            }
        }
    
    [logsAux addObject:log];
    
    [Log saveLogs:logsAux];
    
    
    return TRUE;
}

+ (BOOL)parseMuscles:(NSDictionary *)response{
    NSArray *dataDictionary = [response valueForKey:@"data"];
    
    NSMutableArray *muscleAux = [[NSMutableArray alloc] initWithCapacity:dataDictionary.count];
    
    for(NSDictionary *muscleDictionary in dataDictionary){
        Muscle *muscle = [[Muscle alloc] init];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        
        [muscle setMuscleId:[muscleDictionary valueForKey:@"_id"]];
        [muscle setCreatedAt:[formatter dateFromString:[muscleDictionary valueForKey:@"createdAt"]]];
        [muscle setTitle:[muscleDictionary valueForKey:@"title"]];
        [muscle setOwner:[muscleDictionary valueForKey:@"owner"]];
        
        [muscleAux addObject:muscle];
    }
    
    [Muscle saveMuscles:muscleAux];
    
    return YES;
}

+ (BOOL)parseExercises:(NSArray<NSDictionary*>*)responseArray{
    
    NSMutableArray *exerciseAux = [[NSMutableArray alloc] initWithCapacity:responseArray.count];
    
    for(NSDictionary *exerciseDictionary in responseArray){
        Exercise *exercise = [[Exercise alloc] init];
        [exercise parseFromDictionary:exerciseDictionary];
        [exerciseAux addObject:exercise];
    }
    
    [Exercise saveExercises:exerciseAux];
    
    return YES;
}

+ (BOOL)parseConstants:(NSDictionary *)response{
    NSArray *dataDictionary = [response valueForKey:@"data"];
    
    NSMutableArray *constantsAux = [[NSMutableArray alloc] initWithCapacity:dataDictionary.count];
    
    for(NSDictionary *constantDictionary in dataDictionary){
        ValueConstant *valueConstant = [[ValueConstant alloc] init];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        
        [valueConstant setValueConstantId:[constantDictionary valueForKey:@"_id"]];
        [valueConstant setCreatedAt:[formatter dateFromString:[constantDictionary valueForKey:@"createdAt"]]];
        [valueConstant setUpdatedAt:[formatter dateFromString:[constantDictionary valueForKey:@"updatedAt"]]];
        [valueConstant setValue:[constantDictionary valueForKey:@"value"]];
        [valueConstant setName:[constantDictionary valueForKey:@"title"]];
        
        [constantsAux addObject:valueConstant];
    }
    
    [ValueConstant saveConstants:constantsAux];
    
    return YES;
}

+ (BOOL)parseTrainings:(NSArray<NSDictionary*>*) responseArray {
    if (responseArray == nil || responseArray.count == 0) {
        return NO;
    }
    
    NSDictionary * responseDictionary = [responseArray firstObject];
    if (![[responseDictionary valueForKey:@"active"] boolValue]) {
        return NO;
    }
    
    NSArray<NSDictionary*>* trainings = [responseDictionary valueForKey:@"entrenamientos"];
    NSArray<NSDictionary*>* everybody = [responseDictionary valueForKey:@"todos"];
    NSArray<NSDictionary*>* favorites = [responseDictionary valueForKey:@"favoritos"];
    NSArray<NSDictionary*>* shares = [responseDictionary valueForKey:@"enviados"];
    
    NSMutableArray *trainingAux = [[NSMutableArray alloc] initWithCapacity:trainings.count];
    
    for(NSDictionary *trainingDictionary in trainings){
        Training *training = [[Training alloc] init];
        [training parseFromDictionary:trainingDictionary];
        [training setIsFavourite:@NO];
        [trainingAux addObject:training];
    }
    
    if (trainingAux.count > 0) {
        [Training saveTrainings:trainingAux];
    }
    
    trainingAux = [[NSMutableArray alloc] initWithCapacity:everybody.count];
    
    for(NSDictionary *trainingDictionary in everybody){
        Training *training = [[Training alloc] init];
        [training parseFromDictionary:trainingDictionary];
        [training setIsFavourite:@NO];
        [trainingAux addObject:training];
    }
    
    if (trainingAux.count > 0) {
        [Training saveTrainings:trainingAux];
    }
    
    trainingAux = [[NSMutableArray alloc] initWithCapacity:favorites.count];
    
    RLMRealm *realm = [AppContext currentRealm];
    for(NSDictionary *trainingDictionary in favorites){
        Training *training = [[Training alloc] init];
        [training parseFromDictionary:trainingDictionary];
        
        Training * existTraining = [Training trainingWithServerId:training.serverTrainingId];
        
        if (existTraining) {
            [realm transactionWithBlock:^{
                [existTraining setIsFavourite:[NSNumber numberWithBool:YES]];
            }];
            continue;
        }
        
        [training setIsFavourite:[NSNumber numberWithBool:YES]];
        [trainingAux addObject:training];
    }
    
    for(NSDictionary *trainingDictionary in shares){
        Training *training = [[Training alloc] init];
        [training parseFromDictionary:trainingDictionary];
        
        Training * existTraining = [Training trainingWithServerId:training.serverTrainingId];
        
        if (existTraining) {
            [realm transactionWithBlock:^{
                existTraining.isShared = @YES;
            }];
            continue;
        }
        
        training.isShared = @YES;
        [trainingAux addObject:training];
    }

    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    if (trainingAux.count > 0) {
        [Training saveTrainings:trainingAux];
    }
    
    return YES;
}

+ (Training *)parseTraining:(NSDictionary *)response{
    NSDictionary *dataDictionary = [response valueForKey:@"data"];
    
    Training *training = [[Training alloc] init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    if([Training trainingWithId:[dataDictionary valueForKey:@"trainingId"]]){

        Training *oldTraining = [Training trainingWithId:[dataDictionary valueForKey:@"trainingId"]];

        [training setTrainingId:[dataDictionary valueForKey:@"trainingId"]];
        [training setIsFavourite:oldTraining.isFavourite];

        Training *deleteTraining = [Training trainingWithId:[dataDictionary valueForKey:@"trainingId"]];
        for(TrainingExercise *trainingExercise in deleteTraining.trainingExercises){
            [Training deleteTrainingExercise:trainingExercise inTraining:training];
        }

    }
    else
        [training setTrainingId:[dataDictionary valueForKey:@"id"]];
    [training setServerTrainingId:[dataDictionary valueForKey:@"id"]];
    
    [training setCreatedAt:[formatter dateFromString:[dataDictionary valueForKey:@"createdAt"]]];
    [training setUpdatedAt:[formatter dateFromString:[dataDictionary valueForKey:@"updatedAt"]]];
    [training setTitle:[dataDictionary valueForKey:@"title"]];
    [training setOwner:[dataDictionary valueForKey:@"owner"]];
    [training setTotalEnergy:[dataDictionary valueForKey:@"totalEnergy"]];
    [training setContent:[dataDictionary valueForKey:@"content"]];
    [training setNotes:[dataDictionary valueForKey:@"note"]];
    
    [training setPrivacity:[dataDictionary valueForKey:@"privacity"]];
    
    if([dataDictionary valueForKey:@"isCopy"])
        [training setIsCopy:[dataDictionary valueForKey:@"isCopy"]];
    else
        [training setIsCopy:[NSNumber numberWithBool:FALSE]];
    
    NSArray *exercises = [dataDictionary valueForKey:@"training_exercises"];
    
    [training setTrainingExercisesCount:[NSNumber numberWithInteger:exercises.count]];
    
    if(exercises.count > 0){
        NSMutableArray *exercisesAux = [[NSMutableArray alloc] initWithCapacity:exercises.count];
        
        for(NSDictionary *trainingExerciseDictionary in exercises){
            
            TrainingExercise *trainingExercise = [[TrainingExercise alloc] init];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        
            [trainingExercise setServerTrainingId:[trainingExerciseDictionary valueForKey:@"_id"]];
            [trainingExercise setIsSelected:[NSNumber numberWithBool:FALSE]];
            
            if([trainingExerciseDictionary valueForKey:@"trainingExerciseId"])
                [trainingExercise setTrainingId:[trainingExerciseDictionary valueForKey:@"trainingExerciseId"]];
            else
                [trainingExercise setTrainingId:[UtilManager uuid]];
            [trainingExercise setCreatedAt:[formatter dateFromString:[trainingExerciseDictionary valueForKey:@"createdAt"]]];
            [trainingExercise setUpdatedAt:[formatter dateFromString:[trainingExerciseDictionary valueForKey:@"updatedAt"]]];
            [trainingExercise setOwner:[trainingExerciseDictionary valueForKey:@"owner"]];
            [trainingExercise setTotalEnergy:[trainingExerciseDictionary valueForKey:@"energy"]];
            [trainingExercise setOrder:[trainingExerciseDictionary valueForKey:@"order"]];
            [trainingExercise setRandomKey:[trainingExerciseDictionary valueForKey:@"randomKey"]];
            
            NSArray *exercises = [trainingExerciseDictionary valueForKey:@"exercise"];
            
            if(exercises.count > 0){
                NSMutableArray *exerAux = [[NSMutableArray alloc] initWithCapacity:exercises.count];
                
                for(NSString *exerciseString in exercises){
                    Exercise *exercise = [Exercise exerciseWithId:exerciseString];
                    if(exercise)
                        [exerAux addObject:exercise];
                }
                
                if(exercises.count >= 2)
                    [trainingExercise setIsSuperSerie:[NSNumber numberWithBool:TRUE]];
                
                if(exerAux.count > 0)
                    [trainingExercise.exercises addObjects:exerAux];
            }
            
            NSArray *series = [trainingExerciseDictionary valueForKey:@"series"];
            
            if(series.count > 0){
                NSMutableArray *seriesAux = [[NSMutableArray alloc] initWithCapacity:series.count];
                
                for(NSDictionary *seriesDictionary in series){
                    Serie *newSerie = [[Serie alloc] init];
                    
                    [newSerie setServerSerieId:[seriesDictionary valueForKey:@"_id"]];
                    [newSerie setSerieId:[seriesDictionary valueForKey:@"serieId"]];
                    [newSerie setCreatedAt:[formatter dateFromString:[seriesDictionary valueForKey:@"createdAt"]]];
                    [newSerie setUpdatedAt:[formatter dateFromString:[seriesDictionary valueForKey:@"updatedAt"]]];
                    [newSerie setIsSubserie:[seriesDictionary valueForKey:@"isSubserie"]];
                    [newSerie setOrder:[seriesDictionary valueForKey:@"order"]];
                    
                    Exercise *exercise = [Exercise exerciseWithId:[seriesDictionary valueForKey:@"exercise"]];
                    [newSerie setExercise:exercise];
                    
                    [newSerie setIntensityFactor:[seriesDictionary valueForKey:@"intensity"]];
                    [newSerie setOwner:[seriesDictionary valueForKey:@"owner"]];
                    [newSerie setReps:[seriesDictionary valueForKey:@"reps"]];
                    [newSerie setRest:[seriesDictionary valueForKey:@"rest"]];
                    [newSerie setWeight:[seriesDictionary valueForKey:@"weight"]];
                    [newSerie setExecTime:[seriesDictionary valueForKey:@"duration"]];
                    
                    //[newSerie setTrainingExerxise:trainingExercise];
                    
                    if([[trainingExercise.exercises objectAtIndex:0] isEqual:exercise])
                        [trainingExercise.series addObject:newSerie];
                    else
                        [trainingExercise.secondExerciseSeries addObject:newSerie];
                    
                    //[seriesAux addObject:newSerie];
                }
                //[trainingExercise.series addObjects:seriesAux];
            }
            
            [exercisesAux addObject:trainingExercise];
        }
        
        if(exercisesAux.count > 0)
            [training.trainingExercises addObjects:exercisesAux];
    }
    
    NSArray *tags = [dataDictionary valueForKey:@"tags"];
    
    if(tags.count > 0){
        NSMutableArray *tagsAux = [[NSMutableArray alloc] initWithCapacity:tags.count];
        
        for(NSString *tagsString in tags){
            Tag *tag = [Tag tagWithId:tagsString];
            [tagsAux addObject:tag];
        }
        
        if(tagsAux.count > 0)
            [training.tags addObjects:tagsAux];
    }
    
    return training;
}

+ (BOOL)parseTrainingPlans:(NSArray<NSDictionary*>*)response{
    
    if (response == nil || response.count == 0) {
        return NO;
    }
    
    if (![[[response firstObject] valueForKey:@"active"] boolValue]) {
        return NO;
    }
    
    NSMutableArray *trainingAux = [[NSMutableArray alloc] init];
    
    //
    // Add my training plans
    //
    
    NSArray *myPlanDictionary = [[response firstObject] valueForKey:@"planes"];
    
    for(NSDictionary *trainingPlanDictionary in myPlanDictionary) {
        TrainingPlan *trainingPlan = [[TrainingPlan alloc] init];
        trainingPlan.trainingPlanId =[trainingPlanDictionary valueForKey:@"id"];
        [trainingPlan parseFromDictionary:trainingPlanDictionary];
        trainingPlan.isCalendarCopy = @NO;
        [trainingAux addObject:trainingPlan];
    }
    
    //
    // Add other training plans
    //
    
    NSArray * otherPlanDictionary = [[response firstObject] valueForKey:@"todos"];
    
    for(NSDictionary *trainingPlanDictionary in otherPlanDictionary) {
        TrainingPlan *trainingPlan = [[TrainingPlan alloc] init];
        trainingPlan.trainingPlanId =[trainingPlanDictionary valueForKey:@"id"];
        [trainingPlan parseFromDictionary:trainingPlanDictionary];
        trainingPlan.isCalendarCopy = @NO;
        [trainingAux addObject:trainingPlan];
    }
    
    //
    // Add favorite training plans
    //
    
    NSArray * favoritePlanDictionary = [[response firstObject] valueForKey:@"favoritos"];
    
    for(NSDictionary *trainingPlanDictionary in favoritePlanDictionary) {
        TrainingPlan *trainingPlan = [[TrainingPlan alloc] init];
        trainingPlan.trainingPlanId =[trainingPlanDictionary valueForKey:@"id"];
        [trainingPlan parseFromDictionary:trainingPlanDictionary];
        trainingPlan.isCalendarCopy = @NO;
        trainingPlan.isFavourite = @YES;
        [trainingAux addObject:trainingPlan];
    }
    
    //
    // Add shared training plans
    //
    
    NSArray * sharedPlanDictionary = [[response firstObject] valueForKey:@"enviados"];
    
    for(NSDictionary *trainingPlanDictionary in sharedPlanDictionary) {
        TrainingPlan *trainingPlan = [[TrainingPlan alloc] init];
        trainingPlan.trainingPlanId =[trainingPlanDictionary valueForKey:@"id"];
        [trainingPlan parseFromDictionary:trainingPlanDictionary];
        trainingPlan.isCalendarCopy = @NO;
        trainingPlan.isShared = @YES;
        [trainingAux addObject:trainingPlan];
    }
    
    [TrainingPlan saveTrainingPlans:trainingAux];
    
    return YES;
}

+ (BOOL)parseSchedule:(NSArray<NSDictionary*>*)response {
    
    if (response == nil || response.count == 0) {
        return NO;
    }
    
    if (![[[response firstObject] valueForKey:@"active"] boolValue]) {
        return NO;
    }
    
    NSArray * allArray = [[response firstObject] valueForKey:@"todo"];
    for (NSDictionary * scheduleDictionary in allArray) {
        
        //
        // Save Schedule Trainings
        //
        
        NSArray *scheduleTrainingPlanDictionary = [scheduleDictionary valueForKey:@"plan_entrenamiento"];
        NSMutableArray * scheduleTrainingsAux = [[NSMutableArray alloc] init];
        
        for (NSDictionary * trainingPlanDictionary in scheduleTrainingPlanDictionary) {
            NSArray * scheduleTrainingArrayDictionary = [[[trainingPlanDictionary valueForKey:@"entrenamientos"] valueForKey:@"entrenamientos"] firstObject];
            for (NSDictionary * scheduleTrainingDictionary in scheduleTrainingArrayDictionary) {
                Training * training = [[Training alloc] init];
                [training parseFromDictionary:scheduleTrainingDictionary];
                training.trainingId = [NSString stringWithFormat:@"%@-schedule", training.trainingId];
                training.serverTrainingId = [NSString stringWithFormat:@"%@-schedule", training.serverTrainingId];
                training.isCalendarCopy = @YES;
                [scheduleTrainingsAux addObject:training];
            }
        }
        
        [Training saveTrainings:scheduleTrainingsAux];
        
        //
        // Add scheduled training plan
        //
        
        NSMutableArray *trainingPlanAux = [[NSMutableArray alloc] init];
        for (NSDictionary *trainingPlanDictionary in scheduleTrainingPlanDictionary) {
            
            //
            // Set Schedule Plan
            //
            
            TrainingPlan *trainingPlan = [[TrainingPlan alloc] init];
            trainingPlan.trainingPlanId =[trainingPlanDictionary valueForKey:@"id"];
            [trainingPlan parseFromDictionary:trainingPlanDictionary];
            trainingPlan.isCalendarCopy = @YES;
            [Schedule addTrainingPlan:trainingPlan toDate:trainingPlan.startDate];
            [trainingPlanAux addObject:trainingPlan];
        }
        
        [Schedule addTrainingPlans:trainingPlanAux];
        
        //
        // Add scheduled nutrition plan
        //
        
        NSMutableArray *nutritionPlanAux = [[NSMutableArray alloc] init];
        NSArray *scheduledNutritionPlanDictionary = [scheduleDictionary valueForKey:@"plan_nutricional"];
        
        for (NSDictionary *nutritionPlanDictionary in scheduledNutritionPlanDictionary) {
            NutritionPlan * nutritionPlan = [[NutritionPlan alloc] init];
            nutritionPlan.nutritionPlanId = [nutritionPlanDictionary valueForKey:@"id"];
            [nutritionPlan parseFromDictionary:nutritionPlanDictionary planDayKey:@"plan_dia" planDayContentKey:@"plan_dia_training"];
            nutritionPlan.isCalendarCopy = @YES;
            [nutritionPlanAux addObject:nutritionPlan];
        }
        
        [Schedule addNutritionPlans:nutritionPlanAux];
        
        //
        // Add event
        //
        
        NSMutableArray *eventAux = [[NSMutableArray alloc] init];
        NSArray *scheduledEventsDictionary = [scheduleDictionary valueForKey:@"eventos"];
        
        for (NSDictionary *eventDictionary in scheduledEventsDictionary) {
            Event * event = [[Event alloc] init];
            event.eventId = [eventDictionary valueForKey:@"id"];
            [event parseFromDictionary:eventDictionary];
            [eventAux addObject:event];
        }

        [Event saveEvents:eventAux];
    }
    
    return YES;
}

+ (BOOL)parseAliments:(NSArray<NSDictionary*>*)response{
    if (response == nil || response.count == 0) {
        return NO;
    }
    
    if (![[[response firstObject] valueForKey:@"active"] boolValue]) {
        return NO;
    }
    
    NSArray * alimentsArray  = [response valueForKey:@"alimentos"];
    NSArray * supplementArray = [response valueForKey:@"suplementos"];
    
    NSMutableArray * alimentsAux = [[NSMutableArray alloc] initWithCapacity:1];
    for (NSDictionary * alimentDictionary in [alimentsArray firstObject]) {
        Aliment * aliment = [[Aliment alloc] init];
        [aliment parseFromDictionary:alimentDictionary];
        [aliment setIsSupplement:@NO];
        [alimentsAux addObject:aliment];
    }
    
    for (NSDictionary * supplementDictionary in [supplementArray firstObject]) {
        Aliment * supplement = [[Aliment alloc] init];
        [supplement parseFromDictionary:supplementDictionary];
        [supplement setIsSupplement:@YES];
        [alimentsAux addObject:supplement];
    }
    
    [Aliment saveAliments:alimentsAux];
    
    return YES;
}

+ (BOOL)parseFoods:(NSDictionary *)response{
    NSArray *dataDictionary = [response valueForKey:@"data"];
    
    NSMutableArray *foodsAux = [[NSMutableArray alloc] initWithCapacity:dataDictionary.count];
    
    for(NSDictionary *foodDictionary in dataDictionary){
        Food *food = [[Food alloc] init];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        
        [food setFoodId:[foodDictionary valueForKey:@"_id"]];
        [food setCreatedAt:[formatter dateFromString:[foodDictionary valueForKey:@"createdAt"]]];
        [food setUpdatedAt:[formatter dateFromString:[foodDictionary valueForKey:@"updatedAt"]]];
        [food setTitle:[foodDictionary valueForKey:@"title"]];
        [food setOwner:[foodDictionary valueForKey:@"owner"]];
        [food setCalories:[foodDictionary valueForKey:@"calories"]];
        [food setCarbs:[foodDictionary valueForKey:@"carbs"]];
        [food setFats:[foodDictionary valueForKey:@"fats"]];
        [food setProteins:[foodDictionary valueForKey:@"proteins"]];
        [food setSugar:[foodDictionary valueForKey:@"sugar"]];
        [food setWater:[foodDictionary valueForKey:@"water"]];
        [food setUnsaturatedFats:[foodDictionary valueForKey:@"unsaturatedFats"]];
        [food setPolyUnsaturatedFats:[foodDictionary valueForKey:@"polyUnsaturatedFats"]];
        [food setMonoUnsaturatedFats:[foodDictionary valueForKey:@"monoUnsaturatedFats"]];
        [food setMeasureUnit:[foodDictionary valueForKey:@"measureUnit"]];
        [food setFiber:[foodDictionary valueForKey:@"fiber"]];
        [food setSodium:[foodDictionary valueForKey:@"sodium"]];
        [food setContent:[foodDictionary valueForKey:@"content"]];
        
        [foodsAux addObject:food];
    }
    
    [Food saveFoods:foodsAux];
    
    return YES;
}

+ (BOOL)parseMeals:(NSArray *)response {
    if (response == nil || response.count == 0) {
        return NO;
    }
    
    NSDictionary * responseDictionary = [response firstObject];
    if (![[responseDictionary valueForKey:@"active"] boolValue]) {
        return NO;
    }
    
    NSArray<NSDictionary*>* meals = [responseDictionary valueForKey:@"comidas"];
    NSArray<NSDictionary*>* everybody = [responseDictionary valueForKey:@"todos"];
    NSArray<NSDictionary*>* favorites = [responseDictionary valueForKey:@"favoritos"];
    NSArray<NSDictionary*>* shares = [responseDictionary valueForKey:@"enviados"];
    
    NSMutableArray *mealAux = [[NSMutableArray alloc] initWithCapacity:meals.count];
    
    //
    // Get merged meals from stroage
    //
    
//    NSMutableArray * mergedMealIds = [[NSMutableArray alloc] init];
//    NSNumber * mergedMealsCount = [UserDefaultLibrary getWithKey:@"child_meals_for_merge_count"];
//    if (mergedMealsCount) {
//        for (NSInteger index = 0; index < mergedMealsCount.integerValue; index++) {
//            NSString * mealServerIdKey = [NSString stringWithFormat:@"child_meals_for_merge_count_%d", (int)index];
//            NSString * mergedMealId = [UserDefaultLibrary getWithKey:mealServerIdKey];
//            [mergedMealIds addObject:mergedMealId];
//        }
//    }

    for (NSDictionary *mealDictionary in meals) {
        Meal *meal = [[Meal alloc] init];
        [meal parseFromDictionary:mealDictionary];
        meal.isFavourite = @NO;
//        if ([mergedMealIds containsObject:meal.serverMealId]) {
//            meal.childOfMergedMeal = @YES;
//        }
//        else {
//            meal.childOfMergedMeal = @NO;
//        }
        [mealAux addObject:meal];
    }

    if (mealAux.count > 0) {
        [Meal saveMeals:mealAux];
    }
    
    mealAux = [[NSMutableArray alloc] initWithCapacity:everybody.count];
    
    for(NSDictionary *mealDictionary in everybody){
        Meal *meal = [[Meal alloc] init];
        [meal parseFromDictionary:mealDictionary];
        [meal setIsFavourite:[NSNumber numberWithBool:NO]];
//        [meal setIsUsersOwn:[NSNumber numberWithBool:NO]];
//        if ([mergedMealIds containsObject:meal.serverMealId]) {
//            meal.childOfMergedMeal = @YES;
//        }
//        else {
//            meal.childOfMergedMeal = @NO;
//        }
        [mealAux addObject:meal];
    }
    
    if (mealAux.count > 0) {
        [Meal saveMeals:mealAux];
    }
    
    mealAux = [[NSMutableArray alloc] initWithCapacity:favorites.count];
    
    RLMRealm *realm = [AppContext currentRealm];
    for(NSDictionary *mealDictionary in favorites){
        Meal *meal = [[Meal alloc] init];
        [meal parseFromDictionary:mealDictionary];
        
        Meal * existMeal = [Meal mealWithServerId:meal.mealId];
        
        if (existMeal) {
            [realm transactionWithBlock:^{
                [existMeal setIsFavourite:[NSNumber numberWithBool:YES]];
            }];
            continue;
        }
        
        [meal setIsFavourite:[NSNumber numberWithBool:YES]];
        [mealAux addObject:meal];
    }
    
    for(NSDictionary *mealDictionary in shares){
        Meal *meal = [[Meal alloc] init];
        [meal parseFromDictionary:mealDictionary];
        
        Meal * existMeal = [Meal mealWithServerId:meal.mealId];
        
        if (existMeal) {
            [realm transactionWithBlock:^{
                existMeal.isShared = @YES;
            }];
            continue;
        }
        
        meal.isShared = @YES;
        [mealAux addObject:meal];
    }
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    if (mealAux.count > 0) {
        [Meal saveMeals:mealAux];
    }
    
    return YES;
}

+ (BOOL)parseDiets:(NSArray<NSDictionary*>*)response {
    if (response == nil || response.count == 0) {
        return NO;
    }
    
    NSDictionary * responseDictionary = [response firstObject];
    if (![[[response firstObject] valueForKey:@"active"] boolValue]) {
        return NO;
    }
    
    NSArray<NSDictionary*>* diets = [responseDictionary valueForKey:@"dietas"];
    NSArray<NSDictionary*>* everybody = [responseDictionary valueForKey:@"todos"];
    NSArray<NSDictionary*>* favorites = [responseDictionary valueForKey:@"favoritos"];
    NSArray<NSDictionary*>* shares = [responseDictionary valueForKey:@"enviados"];
    
    NSMutableArray *dietsAux = [[NSMutableArray alloc] initWithCapacity:diets.count];
    
    for(NSDictionary *dietDictionary in diets){
        Diet *diet = [[Diet alloc] init];
        [diet parseFromDictionary:dietDictionary];
        [diet setIsFavourite:[NSNumber numberWithBool:NO]];
        [dietsAux addObject:diet];
    }
    
    if (dietsAux.count > 0) {
        [Diet saveDiets:dietsAux];
    }
    
    dietsAux = [[NSMutableArray alloc] initWithCapacity:everybody.count];
    
    for(NSDictionary *dietsDictionary in everybody){
        Diet *diet = [[Diet alloc] init];
        [diet parseFromDictionary:dietsDictionary];
        [diet setIsFavourite:[NSNumber numberWithBool:NO]];
//        [diet setIsUsersOwn:@NO];
        [dietsAux addObject:diet];
    }
    
    if (dietsAux.count > 0) {
        [Diet saveDiets:dietsAux];
    }
    
    dietsAux = [[NSMutableArray alloc] initWithCapacity:favorites.count];
    
    RLMRealm *realm = [AppContext currentRealm];
    for(NSDictionary *mealDictionary in favorites){
        Diet *diet = [[Diet alloc] init];
        [diet parseFromDictionary:mealDictionary];
        
        Diet * existDiet = [Diet dietWithServerId:diet.serverDietId];
        
        if (existDiet) {
            [realm transactionWithBlock:^{
                [existDiet setIsFavourite:[NSNumber numberWithBool:YES]];
            }];
            continue;
        }
        
        [diet setIsFavourite:[NSNumber numberWithBool:YES]];
        [dietsAux addObject:diet];
    }
    
    for(NSDictionary *mealDictionary in shares){
        Diet *diet = [[Diet alloc] init];
        [diet parseFromDictionary:mealDictionary];
        
        Diet * existDiet = [Diet dietWithServerId:diet.serverDietId];
        
        if (existDiet) {
            [realm transactionWithBlock:^{
                existDiet.isShared = @YES;
            }];
            continue;
        }
        
        diet.isShared = @YES;
        [dietsAux addObject:diet];
    }
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    if (dietsAux.count > 0) {
        [Diet saveDiets:dietsAux];
    }
    
    return TRUE;
}

+ (BOOL)parseNutritionsPlans:(NSArray<NSDictionary*>*)response {
    if (response == nil || response.count == 0) {
        return NO;
    }
    
    if (![[[response firstObject] valueForKey:@"active"] boolValue]) {
        return NO;
    }
    
    NSArray *myPlanDictionary = [[response firstObject] valueForKey:@"Planes_nutricionales"];
    
    NSMutableArray *planAux = [[NSMutableArray alloc] init];
    
    //
    // Add my nutrition plans
    //
    
    for (NSDictionary *nutritionPlanDictionary in myPlanDictionary) {
        NutritionPlan * nutritionPlan = [[NutritionPlan alloc] init];
        nutritionPlan.nutritionPlanId = [nutritionPlanDictionary valueForKey:@"id"];
        [nutritionPlan parseFromDictionary:nutritionPlanDictionary];
        nutritionPlan.isCalendarCopy = @NO;
        [planAux addObject:nutritionPlan];
    }
    
    //
    // Add other nutrition plans
    //
    
    NSArray *othersPlanDictionary = [[response firstObject] valueForKey:@"todos"];
    for (NSDictionary *nutritionPlanDictionary in othersPlanDictionary) {
        NutritionPlan * nutritionPlan = [[NutritionPlan alloc] init];
        nutritionPlan.nutritionPlanId = [nutritionPlanDictionary valueForKey:@"id"];
        [nutritionPlan parseFromDictionary:nutritionPlanDictionary];
        nutritionPlan.isCalendarCopy = @NO;
        [planAux addObject:nutritionPlan];
    }
    
    //
    // Add favorite nutrition plans
    //
    
    NSArray *favoritePlanDictionary = [[response firstObject] valueForKey:@"favoritos"];
    for (NSDictionary *nutritionPlanDictionary in favoritePlanDictionary) {
        NutritionPlan * nutritionPlan = [[NutritionPlan alloc] init];
        nutritionPlan.nutritionPlanId = [nutritionPlanDictionary valueForKey:@"id"];
        [nutritionPlan parseFromDictionary:nutritionPlanDictionary];
        nutritionPlan.isCalendarCopy = @NO;
        nutritionPlan.isFavourite = @YES;
        [planAux addObject:nutritionPlan];
    }
    
    //
    // Add shared nutrition plans
    //
    
    NSArray *sharedPlanDictionary = [[response firstObject] valueForKey:@"enviados"];
    for (NSDictionary *nutritionPlanDictionary in sharedPlanDictionary) {
        NutritionPlan * nutritionPlan = [[NutritionPlan alloc] init];
        nutritionPlan.nutritionPlanId = [nutritionPlanDictionary valueForKey:@"id"];
        [nutritionPlan parseFromDictionary:nutritionPlanDictionary];
        nutritionPlan.isCalendarCopy = @NO;
        nutritionPlan.isShared = @YES;
        [planAux addObject:nutritionPlan];
    }
    
    [NutritionPlan saveNutritionsPlans:planAux];
    
    return YES;
}

+ (BOOL)parseTags:(NSArray *)response{
    NSMutableArray *tagsAux = [[NSMutableArray alloc] initWithCapacity:response.count];
    
    for(NSDictionary *tagDictionary in response){
        Tag *tag = [[Tag alloc] init];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        [tag setTagId:[tagDictionary valueForKey:@"code"]];
        [tag setCreatedAt:[formatter dateFromString:[tagDictionary valueForKey:@"timestamp"]]];
        [tag setUpdatedAt:[formatter dateFromString:[tagDictionary valueForKey:@"timestamp"]]];
        
        [tag setCollection:[tagDictionary valueForKey:@"coleccion_id"]];
        [tag setType:[tagDictionary valueForKey:@"tipo"]];
        [tag setTitle:[tagDictionary valueForKey:@"etiqueta"]];
        tag.order = @([[tagDictionary valueForKey:@"orden"] intValue]);
        [tag setOwner:[tagDictionary valueForKey:@"owner"]];
        
        [tag setStatus:@"Off"];
        
        [tagsAux addObject:tag];
    }
    
    [Tag saveTags:tagsAux];
    
    return YES;
}

@end
