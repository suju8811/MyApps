//
//  RegularExpressionsManager.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RegularExpressionsManager.h"

@implementation RegularExpressionsManager

+ (BOOL)checkMail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (BOOL)checkNIF:(NSString *)nifString{
    
    static NSString *characters = @"TRWAGMYFPDXBNJZSQVHLCKE";
    
    if ( [nifString length] == 9 || ([nifString length] && [nifString length] == 10) )
    {
        NSString *lastCharacter = [[nifString substringWithRange:NSMakeRange(8, 1)] uppercaseString];
        NSString *number        = [[nifString substringWithRange:NSMakeRange(0, 8)] uppercaseString];
        
        int index = [number intValue] % [characters length];
        
        return ( [[characters substringWithRange:NSMakeRange(index, 1)] isEqualToString:lastCharacter] ) ? YES : NO;
    }
    else
    {
        return NO;
    }

}

@end
