//
//  DateManager.m
//
//
//  Created by Manuel Manzanera on 10/11/16.
//  Copyright (c) 2016 inup. All rights reserved.
//

#import "DateManager.h"

@implementation DateManager

+ (NSDate *)dateWithString:(NSString *)dateString withDateFormat:(NSString *)dateFormatter{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:dateFormatter];
    NSDate *date = [dateFormat dateFromString:dateString];
    return date;
}

+ (NSString *)stringWithDateAndFormat:(NSDate *)date{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/YYYY HH:mm"];
    
    NSString *returnString = [dateFormat stringFromDate:date];
    
    return returnString;
}

+ (NSDateComponents *)dateComponentsWithDate:(NSDate *)date{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitDay;
    
    if(!date)
        date = [[NSDate alloc] init];
        
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:date];
    
    return dateComponents;
}

+ (NSString *)weekDayWithInteger:(NSInteger)weekDay{
    
    switch (weekDay)
    {
        case 1:
            return @"Domingo";
            break;
        case 2:
            return @"Lunes";
            break;
        case 3:
            return @"Martes";
            break;
        case 4:
            return @"Miércoles";
            break;
        case 5:
            return @"Jueves";
        case 6:
            return @"Viernes";
        case 7:
            return @"Sábado";
        default:
            break;
    }
    return @"";
}

+ (NSString *)monthWithInteger:(NSInteger)month{
    switch (month)
    {
        case 1:
            return NSLocalizedString(@"Enero", nil);
            break;
        case 2:
            return NSLocalizedString(@"Febrero", nil);
            break;
        case 3:
            return NSLocalizedString(@"Marzo", nil);
            break;
        case 4:
            return NSLocalizedString(@"Abril", nil);
            break;
        case 5:
            return NSLocalizedString(@"Mayo", nil);
            break;
        case 6:
            return NSLocalizedString(@"Junio", nil);
            break;
        case 7:
            return NSLocalizedString(@"Julio", nil);
            break;
        case 8:
            return NSLocalizedString(@"Agosto", nil);
            break;
        case 9:
            return NSLocalizedString(@"Septiembre", nil);
            break;
        case 10:
            return NSLocalizedString(@"Octubre", nil);
            break;
        case 11:
            return NSLocalizedString(@"Noviembre", nil);
            break;
        case 12:
            return NSLocalizedString(@"Diciembre", nil);
            break;
        default:
            break;
    }
    return @"";
}

+ (NSString *)stringWithDate:(NSDate *)date{
    
    NSString *dateString;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute;
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date
                                                                     toDate:[NSDate date]
                                                                    options:0];
    
    if (components.year > 0){
        if(components.year == 1)
            dateString = [NSString stringWithFormat:@"%ld %@", (long)components.year,NSLocalizedString(@"year ago", nil)];
        else
            dateString = [NSString stringWithFormat:@"%ld %@", (long)components.year,NSLocalizedString(@"years ago", nil)];
        
    } else if (components.month > 0){
        if(components.month == 1)
            dateString = [NSString stringWithFormat:@"%ld %@", (long)components.month,NSLocalizedString(@"month ago", nil)];
        else
            dateString = [NSString stringWithFormat:@"%ld %@", (long)components.month,NSLocalizedString(@"months ago", nil)];
    } else if (components.weekOfYear > 0){
        if(components.weekOfYear == 1)
            dateString = [NSString stringWithFormat:@"%ld %@", (long)components.weekOfYear,NSLocalizedString(@"week ago", nil)];
        else
            dateString = [NSString stringWithFormat:@"%ld %@", (long)components.weekOfYear,NSLocalizedString(@"weeks ago", nil)];
    } else if (components.day > 0) {
        if (components.day > 1) {
            dateString = [NSString stringWithFormat:@"%ld %@", (long)components.day,NSLocalizedString(@"days ago", nil)];
        } else {
            dateString = NSLocalizedString(@"Ayer", nil);
        }
    } else
        dateString = NSLocalizedString(@"Hace", nil);
    
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    
    NSString *hourString = [NSString stringWithFormat:@"%ld",(long)hour];
    NSString *minuteString = [NSString stringWithFormat:@"%ld",(long)minute];
    
    if(hourString.length == 1)
        hourString = [NSString stringWithFormat:@"0%@",hourString];
    if(minuteString.length == 1)
        minuteString = [NSString stringWithFormat:@"0%@",minuteString];
    
    dateString = [NSString stringWithFormat:@"%@ %@:%@",dateString,hourString,minuteString];
    
    return dateString;
}

+(NSDate *)endOfDay:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(  NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:date];
    
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    
    return [cal dateFromComponents:components];
    
}

+ (NSDate *)startOfDay:(NSDate *)date{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(  NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:date];
    
    [components setHour:00];
    [components setMinute:00];
    [components setSecond:01];
    
    return [cal dateFromComponents:components];
}

@end
