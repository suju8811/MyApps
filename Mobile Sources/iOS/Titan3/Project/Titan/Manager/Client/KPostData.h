//
//  KPostData.h
//  NewsHom
//
//  Created by Michael Lee on 8/4/16.
//  Copyright © 2016 idragonit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KPostData : NSObject

@property (nonatomic, strong)   NSData * appendData;
@property (nonatomic, strong)   NSString * field;
@property (nonatomic, strong)   NSString * fileName;
@property (nonatomic, strong)   NSString * mimeType;
@property (nonatomic, strong)   NSURL * fileURL;

@end
