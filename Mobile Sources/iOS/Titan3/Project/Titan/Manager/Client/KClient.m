#import <Foundation/Foundation.h>
#import "KClient.h"
#import "AppContext.h"

static NSString * const BasePath = @"http://titan.com.88-99-160-173.my-website-preview.com/api/titan/";
static NSString * const BasePath1 = @"http://192.168.1.102:8888/newshom/index.php/api/";

@implementation KClient

+ (instancetype)getInstance
{
    static KClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        _sharedClient = [[KClient alloc] initWithBaseURL:[NSURL URLWithString:BasePath]];
        [_sharedClient setRequestSerializer:[AFHTTPRequestSerializer serializer]];
        [_sharedClient setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    });
    
    return _sharedClient;
}

+ (instancetype)getTestInstance
{
    static KClient *_sharedTestClient = nil;
    static dispatch_once_t onceTokenTest;
    
    dispatch_once(&onceTokenTest, ^{
        
        _sharedTestClient = [[KClient alloc] initWithBaseURL:[NSURL URLWithString:BasePath1]];
        [_sharedTestClient setRequestSerializer:[AFHTTPRequestSerializer serializer]];
        [_sharedTestClient setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    });
    
    return _sharedTestClient;
}

- (void) sendRequestWithType:(REQUEST_TYPE) type
                        path:(NSString *) path
                      params:(NSDictionary *) params
                 requireAuth:(BOOL) requireAuth
                successBlock:(void (^)(id result)) successBlock
                 failedBlock:(void (^)(NSError *error)) failedBlock {
    
    [self sendRequestWithType:type
                         path:path
                       params:params
                  requireAuth:requireAuth
                  postURLData:nil
                 postFormData:nil
                 successBlock:^(id result) {
                     NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)result encoding:NSUTF8StringEncoding];
                     NSLog(@"Error Response for Path %@: %@", path, ErrorResponse);
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    successBlock(result);
                 }
                 failedBlock:^(NSError *error) {
                     
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                     NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
                     NSLog(@"Error Response for Path %@: %@", path, ErrorResponse);
                    failedBlock(error);
                 }];
}

- (void) sendRequestWithType:(REQUEST_TYPE) type
                        path:(NSString *) path
                      params:(NSDictionary *) params
                 requireAuth:(BOOL) requireAuth
                postFormData:(NSArray<KPostData*>*) postFormData
                successBlock:(void (^)(id result)) successBlock
                 failedBlock:(void (^)(NSError *error)) failedBlock {
    
    [self sendRequestWithType:type
                         path:path
                       params:params
                  requireAuth:requireAuth
                  postURLData:nil
                 postFormData:postFormData
                 successBlock:^(id result) {
                     
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    successBlock(result);
                 }
                 failedBlock:^(NSError *error) {
                     
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    failedBlock(error);
                 }];
}

- (void) sendRequestWithType:(REQUEST_TYPE) type
                        path:(NSString *) path
                      params:(NSDictionary *) params
                 requireAuth:(BOOL) requireAuth
                 postURLData:(NSArray<KPostData*>*) postURLData
                successBlock:(void (^)(id result)) successBlock
                 failedBlock:(void (^)(NSError *error)) failedBlock {
    
    [self sendRequestWithType:type
                         path:path
                       params:params
                  requireAuth:requireAuth
                  postURLData:postURLData
                 postFormData:nil
                 successBlock:^(id result) {
                     
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    successBlock(result);
                 }
                 failedBlock:^(NSError *error) {
                     
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    failedBlock(error);
                 }];
}

- (void)sendRequestWithType:(REQUEST_TYPE)type
                       path:(NSString *)path
                     params:(NSDictionary *)params
                requireAuth:(BOOL) requireAuth
                postURLData:(NSArray<KPostData*>*) postURLData
               postFormData:(NSArray<KPostData*>*) postFormData
               successBlock:(void (^)(id))successBlock
                failedBlock:(void (^)(NSError *))failedBlock {
    
    NSURLSessionDataTask * task;
    
    if (requireAuth) {
        [self.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
        [self.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    }
    
    switch (type) {
            
        case REQUEST_GET: {
            
            task = [self GET:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                if (successBlock) {
                    successBlock(responseObject);
                }
            } failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
                
                if (failedBlock) {
                    failedBlock(error);
                }
            }];
            break;
        }
        case REQUEST_POST: {
            
            task = [self POST:path parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                
                if (postFormData != nil) {
                    
                    for (NSInteger i = 0; i < postFormData.count; i++) {
                        [formData appendPartWithFileData:postFormData[i].appendData name:postFormData[i].field fileName:postFormData[i].fileName mimeType:postFormData[i].mimeType];
                    }
                }
                
                if (postURLData != nil) {
                    
                    NSError * error;
                    for (NSInteger i = 0; i < postURLData.count; i++) {
                        
                        [formData appendPartWithFileURL:postURLData[i].fileURL name:postURLData[i].field error:&error];
                        if (error != nil) {
                            NSLog(@"Form Data Append Error : %@", error.description);
                        }
                    }
                }
                
            } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                if (successBlock) {
                    successBlock(responseObject);
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                if (failedBlock) {
                    failedBlock(error);
                }
                
            }];
            
            break;
        }
        case REQUEST_DELETE: {
            
            task = [self DELETE:path parameters:params success:^(NSURLSessionDataTask * __unused task, id result) {
                
                if (successBlock) {
                    successBlock(result);
                }
            } failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
                
                if (failedBlock) {
                    failedBlock(error);
                }
            }];
            
            break;
        }
        case REQUEST_PUT: {
            
            task = [self PUT:path parameters:params success:^(NSURLSessionDataTask * __unused task, id result) {
                
                if (successBlock) {
                    successBlock(result);
                }
            } failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
                
                if (failedBlock) {
                    failedBlock(error);
                }
            }];
            
            break;
        }
        case REQUEST_HEAD: {
            
            task = [self HEAD:path parameters:params success:^(NSURLSessionDataTask *task) {
                
                if (successBlock) {
                    successBlock(nil);
                }
            } failure:^(NSURLSessionDataTask * __unused task, NSError *error) {
                
                if (failedBlock) {
                    failedBlock(error);
                }
            }];
            
            break;
        }
        default: {
            break;
        }
    }

    [task resume];
}

@end
