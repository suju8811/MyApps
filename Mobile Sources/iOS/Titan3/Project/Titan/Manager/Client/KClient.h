#import "AFHTTPSessionManager.h"
#import "KPostData.h"

typedef enum
{
    REQUEST_GET = 1,
    REQUEST_POST,
    REQUEST_HEAD,
    REQUEST_PUT,
    REQUEST_DELETE
} REQUEST_TYPE;

@interface KClient : AFHTTPSessionManager

+ (instancetype)getInstance;
+ (instancetype)getTestInstance;

- (void) sendRequestWithType:(REQUEST_TYPE) type
                        path:(NSString *) path
                      params:(NSDictionary *) params
                 requireAuth:(BOOL) requireAuth
                successBlock:(void (^)(id result)) successBlock
                 failedBlock:(void (^)(NSError *error)) failedBlock;

- (void) sendRequestWithType:(REQUEST_TYPE) type
                        path:(NSString *) path
                      params:(NSDictionary *) params
                 requireAuth:(BOOL) requireAuth
                postFormData:(NSArray<KPostData*>*) postFormData
                successBlock:(void (^)(id result)) successBlock
                 failedBlock:(void (^)(NSError *error)) failedBlock;

- (void) sendRequestWithType:(REQUEST_TYPE) type
                        path:(NSString *) path
                      params:(NSDictionary *) params
                 requireAuth:(BOOL) requireAuth
                 postURLData:(NSArray<KPostData*>*) postURLData
                successBlock:(void (^)(id result)) successBlock
                 failedBlock:(void (^)(NSError *error)) failedBlock;

- (void) sendRequestWithType:(REQUEST_TYPE) type
                        path:(NSString *) path
                      params:(NSDictionary *) params
                 requireAuth:(BOOL) requireAuth
                 postURLData:(NSArray<KPostData*>*) postURLData
                postFormData:(NSArray<KPostData*>*) postFormData
                successBlock:(void (^)(id result)) successBlock
                 failedBlock:(void (^)(NSError *error)) failedBlock;

@end
