//
//  UserDefaultLibrary.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "UserDefaultLibrary.h"

@implementation UserDefaultLibrary

+(id)getWithKey:(NSString *)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (standardUserDefaults && [standardUserDefaults objectForKey:key]!=nil)
    {
        return [standardUserDefaults objectForKey:key];
    }
    return nil;
}

+ (void)removeFromKey:(NSString *)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (standardUserDefaults && [standardUserDefaults objectForKey:key]!=nil)
        [standardUserDefaults removeObjectForKey:key];
}

+(void)setWithKey:(NSString *)key WithObject:(id)obj
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	if (standardUserDefaults)
    {
        [standardUserDefaults setObject:obj forKey:key];
        [standardUserDefaults synchronize];
	}
}

+(void)setWithdictionary:(NSDictionary *)dict
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (standardUserDefaults)
    {
        for (NSString * key in dict)
        {
            [standardUserDefaults setObject:[dict objectForKey:key] forKey:key];
        }
        [standardUserDefaults synchronize];
    }
}

@end
