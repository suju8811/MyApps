//
//  DeveiceManager.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DeviceManager : NSObject

+ (NSString *)uniqueIDForDevice;
+ (NSString *)deviceName;
+ (NSString *)deviceModel;
+ (NSString *)deviceLocalizedModel;
+ (NSString *)deviceSystemName;
+ (NSString *)deviceSystemVersion;


@end
