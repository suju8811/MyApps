//
//  UserDefaultLibrary.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaultLibrary : NSObject

+ (id)getWithKey:(NSString *)key;
+ (void)removeFromKey:(NSString *)key;
+ (void)setWithKey:(NSString *)key WithObject:(id)obj;
+ (void)setWithdictionary:(NSDictionary *)dict;

@end
