//
//  WebServiceManager.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WebServiceManagerDelegate;

typedef enum {
    WebServiceTypeAuthenticate,
    WebServiceTypeAuthenticateWithFacebook,
    WebServiceTypeRecoveryUser,
    WebServiceTypeCreateUser,
    WebServiceTypeUpdateUser,
    WebServiceTypeLogout,
    WebServiceTypeGetMuscles,
    WebServiceTypeGetExercises,
    WebServiceTypeGetTrainings,
    WebServiceTypeGetTraining,
    WebServiceTypePostTraining,
    WebServiceTypeDeleteTraining,
    WebServiceTypeUpdateTraining,
    WebServiceTypeGetTrainingsExercises,
    WebServiceTypeGetTrainingsPlans,
    WebServiceTypePostTrainingsPlans,
    WebServiceTypeUpdateTrainingPlan,
    WebServiceTypeGetFood,
    WebServiceTypeGetEats,
    WebServiceTypeTags,
    WebServiceTypeGetUsersRevision,
    WebServiceTypePostUsersRevision,
    WebServiceTypeUpdateUsersRevision,
    WebServiceTypeUploadLogImage,
    WebServiceTypePostMeal,
    WebServiceTypeDeleteMeal,
    WebServiceTypeUpdateMeal,
    WebServiceTypePostDiet,
    WebServiceTypeUpdateDiet,
    WebServiceTypeDeleteDiet,
    WebServiceTypePostNutriotionPlan,
    WebServiceTypeUpdateNutritionPlan,
    WebServiceTypeDeleteNutritionPlan,
    WebServiceTypeGetConstants,
    WebServiceTypePostNutriotionPlanCalendar,
    WebServiceTypeGetNutriotionPlanCalendar
} WebServiceType;


@interface WebServiceManager : NSObject

//// USERS

+ (void)createUserWithFacebookWithDictionary:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)recoveryUser:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)authenticateUserWithFacebook:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)authenticateUserWithTouchID:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)authenticateUser:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)authenticateUserWithData:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)logoutWithDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)createUserWithDictionary:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)updateUser:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)getUsersRevision;
+ (void)getUserRevision:(NSString *)userRevisionId andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)postUserRevision:(NSDictionary *)userRevisionDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)updateUserRevision:(NSDictionary *)userRevisionDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)deleteUserRevision:(NSString *)userRevisionId andDelegate:(id<WebServiceManagerDelegate>)delegate;
+ (void)getConstants;

////CALENDAR

+ (void)getCalendarTraining:(id<WebServiceManagerDelegate>)delegate;
+ (void)getCalendarTrainingPlan:(id<WebServiceManagerDelegate>)delegate;

+ (void)addTrainingPlanToCalendar:(NSDictionary *)params andDelegate:(id<WebServiceManagerDelegate>)delegate;

//
// Fitness
//

+ (void)getMuscles;
+ (void)getExercises;
+ (void)getExercisesWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock;
+ (void)getTrainings;
+ (void)getTrainingsWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock;
+ (void)getTrainingsWithUser:(NSString *)userId;
+ (void)getTrainingPlans;
+ (void)getTrainingPlansWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock;

//
// Foods
//

+ (void)getAliments;
+ (void)getAlimentsWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock;

+ (void)getFoods;

+ (void)getMeals;
+ (void)getMealsWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock;
+ (void)getNutrionalPlans;
+ (void)getNutrionalPlansWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock;
+ (void)getDiets;
+ (void)getDietsWithWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock;

//// LOGS
+ (void)uploadImage:(NSDictionary *)imageDictionary withDelegate:(id<WebServiceManagerDelegate>)delegate;

@end

@protocol WebServiceManagerDelegate <NSObject>

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object;
- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType;

@optional
- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object;

@end
