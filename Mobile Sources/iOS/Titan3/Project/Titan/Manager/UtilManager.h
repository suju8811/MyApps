//
//  UtilManager.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UtilManager : NSObject

+ (CGFloat)height:(CGFloat)pixels;
+ (CGFloat)width:(CGFloat)pixels;

+ (UIColor *)colorwithHexString:(NSString *)hexStr alpha:(CGFloat)alpha;
+ (CGFloat)heightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font;

+ (UIAlertController *)presentAlertWithMessage:(NSString *)message;
+ (UIAlertController *)presentAlertWithMessage:(NSString *)message
                                      andTitle:(NSString *)title;

+ (UIAlertController *)presentAlertWithMessage:(NSString *)message
                                      andTitle:(NSString *)title
                                   alertAction:(UIAlertAction*)alertAction;

+ (NSDateFormatter *)apiDateFormatter;

+ (NSDictionary *)getMetadataDictionary;
+ (NSString *)uuid;
+ (NSNumber*)floatFromString:(NSString*)string;
+ (NSNumber*)intFromString:(NSString*)string;
+ (NSNumber*)boolFromString:(NSString*)string;

@end
