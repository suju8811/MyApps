//
//  WebServiceManager.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "WebServiceManager.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "AFHTTPSessionManager.h"
#import <Crashlytics/Crashlytics.h>
#import "MRProgress.h"
#import "ParserManager.h"
#import "UtilManager.h"
#import "Aliment.h"
#import "Meal.h"
#import "TrainingPlan.h"
#import "Diet.h"
#import "NutritionPlan.h"
#import "ValueConstant.h"
#import "GetExercisesRequest.h"
#import "GetTrainingsRequest.h"
#import "GetTrainingPlansRequest.h"
#import "GetAlimentsRequest.h"
#import "GetMealsRequest.h"
#import "GetNutritionPlansRequest.h"
#import "GetDietsRequest.h"
#import "UserDefaultLibrary.h"
#import "GetSchedulesRequest.h"

//#import "User.h"


//#ifdef DEBUG

//#ifdef MAGISTER
static NSString *path = @"http://titan.com.88-99-160-173.my-website-preview.com/api/titan/";
//#else
//#endif

//#else
//static NSString *path = @"http://192.168.54.165:3000/api/v1/";
//#endif
//http://backeteor.visualtis.com:3000/

static NSString *RECOVER_PATH = @"recovery";
static NSString *AUTHENTICATE_FACEBOOK_PATH = @"login_facebook";
static NSString *AUTHENTICATE_TOUCHID_PATH = @"touchID";
static NSString *AUTHENTICATE_DATA_PATH = @"register_data";
static NSString *AUTHENTICATE_PATH = @"login";
static NSString *LOGOUT_PATH = @"logout";
static NSString *USER_FACEBOOK_PATH = @"register_facebook";
static NSString *USER_PATH = @"register";
static NSString *PUSHTOKEN_PATH = @"sendPushToken";
static NSString *FORGOTPASSWORD_PATH = @"forgotPassword";
static NSString *USERSREVISION_PATH = @"users_revision";

static NSString *MUSCLE_PATH = @"muscles";
static NSString *EXERCISES_PATH = @"exercises";
static NSString *TRAININGS_PATH = @"training";
static NSString *TRAININGS_PLANS_PATH = @"training_plan";
static NSString *FOODS_PATH = @"foods";
static NSString *MEALS_PATH = @"meals";
static NSString *DIETS_PATH = @"diets";
static NSString *NUTRITIONAL_PLAN_PATH = @"nutrition_plan";
static NSString *TAG_PATH = @"tags";
static NSString *CONSTANTS_PATH = @"constants";

@implementation WebServiceManager

#pragma mark USER

+ (void)recoveryUser:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:[NSString stringWithFormat:@"%@%@",path,RECOVER_PATH] parameters:userDictionary progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if(!jsonError){
            if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
                [delegate dataDidDownload:WebServiceTypeRecoveryUser andObject:dataDictionary];
        }else{
            if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
                [delegate dataDidDownloadWithFail:WebServiceTypeRecoveryUser andObject:NSLocalizedString(@"Error en el usario o la contraseña", nil)];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeRecoveryUser andObject:nil];
    }];
}

+ (void)authenticateUserWithFacebook:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:[NSString stringWithFormat:@"%@%@",path,AUTHENTICATE_FACEBOOK_PATH] parameters:userDictionary progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if(!jsonError && [[[dataDictionary valueForKey:@"active"] objectAtIndex:0] boolValue]){
            //[ParserManager parseAuthenticate:dataDictionary];
            if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
                [delegate dataDidDownload:WebServiceTypeAuthenticateWithFacebook andObject:nil];
        }else{
            if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
                [delegate dataDidDownloadWithFail:WebServiceTypeAuthenticateWithFacebook andObject:NSLocalizedString(@"Error en el usario o la contraseña", nil)];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeAuthenticate andObject:nil];
    }];
}

+ (void)authenticateUserWithData:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:[NSString stringWithFormat:@"%@%@",path,AUTHENTICATE_DATA_PATH] parameters:userDictionary progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:responseObject];
        
        if ([ParserManager parseAuthenticate:responseArray]) {
            
            [NSTimer scheduledTimerWithTimeInterval:0.5 repeats:FALSE block:^(NSTimer *timer){
                [AppContext saveInitialData];
            }];
            
            [UserDefaultLibrary setWithKey:IS_USER_LOGIN WithObject:[NSNumber numberWithBool:TRUE]];
            
            if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)]) {
                [delegate dataDidDownload:WebServiceTypeAuthenticate andObject:nil];
            }
        }
        else {
            if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
                [delegate dataDidDownloadWithFail:WebServiceTypeAuthenticate andObject:NSLocalizedString(@"Error en el usario o la contraseña", nil)];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeAuthenticate andObject:nil];
    }];
}

+ (void)authenticateUserWithTouchID:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:[NSString stringWithFormat:@"%@%@",path,AUTHENTICATE_TOUCHID_PATH] parameters:userDictionary progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if(!jsonError && [[[dataDictionary valueForKey:@"active"] objectAtIndex:0] boolValue]){
            //[ParserManager parseAuthenticate:dataDictionary];
            if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
                [delegate dataDidDownload:WebServiceTypeAuthenticate andObject:nil];
        }else{
            if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
                [delegate dataDidDownloadWithFail:WebServiceTypeAuthenticate andObject:NSLocalizedString(@"Error en el usario o la contraseña", nil)];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeAuthenticate andObject:nil];
    }];
}

+ (void)authenticateUser:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate{
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:[NSString stringWithFormat:@"%@%@",path,AUTHENTICATE_PATH] parameters:userDictionary progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if(!jsonError && [[[dataDictionary valueForKey:@"active"] objectAtIndex:0] boolValue]){
            //[ParserManager parseAuthenticate:dataDictionary];
            [[AppContext sharedInstance] setAuthToken:[[dataDictionary valueForKey:@"token"] objectAtIndex:0]];
            if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
                [delegate dataDidDownload:WebServiceTypeAuthenticate andObject:nil];
        }else{
            if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
                [delegate dataDidDownloadWithFail:WebServiceTypeAuthenticate andObject:NSLocalizedString(@"Error en el usario o la contraseña", nil)];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeAuthenticate andObject:nil];
    }];
}

+ (void)logoutWithDelegate:(id<WebServiceManagerDelegate>)delegate{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    NSString *urlPath = [NSString stringWithFormat:@"%@%@",path,LOGOUT_PATH];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    [manager POST:urlPath parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        NSLog(@"%@",dataDictionary);
        
        if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
            [delegate dataDidDownload:WebServiceTypeLogout andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeLogout];
    }];
}

+ (void)createUserWithFacebookWithDictionary:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager POST:[NSString stringWithFormat:@"%@%@",path,USER_FACEBOOK_PATH] parameters:userDictionary progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if(!jsonError && [[[dataDictionary valueForKey:@"active"] objectAtIndex:0] boolValue]){
            //[ParserManager parseAuthenticate:dataDictionary];
            if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
                [delegate dataDidDownload:WebServiceTypeCreateUser andObject:nil];
        }else{
            if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
                [delegate dataDidDownloadWithFail:WebServiceTypeCreateUser andObject:NSLocalizedString(@"Error en el usario o la contraseña", nil)];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeCreateUser andObject:NSLocalizedString(@"Error en el usario o la contraseña", nil)];
    }];
}

+ (void)createUserWithDictionary:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager POST:[NSString stringWithFormat:@"%@%@",path,USER_PATH] parameters:userDictionary progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if(!jsonError && [[[dataDictionary valueForKey:@"active"] objectAtIndex:0] boolValue]){
            //[ParserManager parseAuthenticate:dataDictionary];
            if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
                [delegate dataDidDownload:WebServiceTypeCreateUser andObject:dataDictionary];
        }else{
            if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
                [delegate dataDidDownloadWithFail:WebServiceTypeCreateUser andObject:NSLocalizedString(@"Error en el usario o la contraseña", nil)];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeCreateUser andObject:NSLocalizedString(@"Error en el usario o la contraseña", nil)];
    }];
}

+ (void)updateUser:(NSDictionary *)userDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    //NSError *error;
    
    //BOOL test = FALSE;
    
    //NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userDictionary
    //                                                   options:(NSJSONWritingOptions)    (test ? NSJSONWritingPrettyPrinted : 0)
    //                                                     error:&error];
    
    //NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    //NSLog(@"%@",jsonString);
    
    //NSDictionary *trainingData = [[NSDictionary alloc] initWithObjectsAndKeys:jsonString,@"userData", nil];
    
    [manager PUT:[NSString stringWithFormat:@"%@%@/%@",path,USER_PATH,[AppContext sharedInstance].userId] parameters:userDictionary success:^(NSURLSessionDataTask *task, id responseObject) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        if(!jsonError && [[dataDictionary valueForKey:@"status"] isEqualToString:@"success"]){
            if([delegate respondsToSelector:@selector(dataDidDownload:andObject:)])
                [delegate dataDidDownload:WebServiceTypeUpdateUser andObject:dataDictionary];
        }else{
            if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
                [delegate dataDidDownloadWithFail:WebServiceTypeUpdateUser andObject:dataDictionary];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeUpdateUser andObject:dataDictionary];
    }];

}

+ (void)getUsersRevision{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    [manager GET:[NSString stringWithFormat:@"%@%@",path,USERSREVISION_PATH] parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        [ParserManager parseUserLogs:(NSArray *)dataDictionary];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
}

+ (void)getUserRevision:(NSString *)userRevisionId andDelegate:(id<WebServiceManagerDelegate>)delegate{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    [manager GET:[NSString stringWithFormat:@"%@%@/%@",path,USERSREVISION_PATH,userRevisionId] parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        [ParserManager parseUserLog:dataDictionary];
        [delegate dataDidDownload:WebServiceTypeGetUsersRevision andObject:dataDictionary];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
}

+ (void)postUserRevision:(NSDictionary *)userRevisionDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    [manager POST:[NSString stringWithFormat:@"%@%@",path,USERSREVISION_PATH] parameters:userRevisionDictionary progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers| NSJSONReadingMutableLeaves error:&jsonError];
        
        if([[dataDictionary valueForKey:@"status"] isEqualToString:@"error"])
            return [delegate dataDidDownloadWithFail:WebServiceTypePostUsersRevision andObject:[dataDictionary valueForKey:@"message"]];
        else if([[dataDictionary valueForKey:@"status"] isEqualToString:@"success"])
            [WebServiceManager getUserRevision:[dataDictionary valueForKey:@"data"] andDelegate:delegate];
        else
            [delegate dataDidDownload:WebServiceTypePostUsersRevision andObject:dataDictionary];
    
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
            [delegate dataDidDownloadWithFail:WebServiceTypePostUsersRevision andObject:nil];
    }];
}

+ (void)updateUserRevision:(NSDictionary *)userRevisionDictionary andDelegate:(id<WebServiceManagerDelegate>)delegate{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    [manager PUT:[NSString stringWithFormat:@"%@%@",path,USERSREVISION_PATH] parameters:userRevisionDictionary success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers| NSJSONReadingMutableLeaves error:&jsonError];
        
        [delegate dataDidDownload:WebServiceTypePostUsersRevision andObject:dataDictionary];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
            [delegate dataDidDownloadWithFail:WebServiceTypePostUsersRevision andObject:nil];
    }];
}

+ (void)deleteUserRevision:(NSString *)userRevisionId andDelegate:(id<WebServiceManagerDelegate>)delegate{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    [manager DELETE:[NSString stringWithFormat:@"%@%@/%@",path,USERSREVISION_PATH,userRevisionId] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        NSLog(@"%@",dataDictionary);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
}

#pragma mark CALENDAR

+ (void)getCalendarTraining:(id<WebServiceManagerDelegate>)delegate{

}

+ (void)getCalendarTrainingPlan:(id<WebServiceManagerDelegate>)delegate{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    [manager GET:[NSString stringWithFormat:@"%@getCalendarTrainingPlan",path] parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers| NSJSONReadingMutableLeaves error:&jsonError];
        
        if([[dataDictionary valueForKey:@"status"] isEqualToString:@"error"])
            return [delegate dataDidDownloadWithFail:WebServiceTypeGetNutriotionPlanCalendar andObject:[dataDictionary valueForKey:@"message"]];
        else
        {
            [ParserManager parseTrainingPlans:dataDictionary];
            [delegate dataDidDownload:WebServiceTypeGetNutriotionPlanCalendar andObject:dataDictionary];
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
            [delegate dataDidDownloadWithFail:WebServiceTypeGetNutriotionPlanCalendar andObject:nil];
    }];
}

+ (void)addTrainingPlanToCalendar:(NSDictionary *)params andDelegate:(id<WebServiceManagerDelegate>)delegate{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    [manager POST:[NSString stringWithFormat:@"%@addTrainingPlanToCalendar",path] parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers| NSJSONReadingMutableLeaves error:&jsonError];
        
        if([[dataDictionary valueForKey:@"status"] isEqualToString:@"error"])
            return [delegate dataDidDownloadWithFail:WebServiceTypePostNutriotionPlanCalendar andObject:[dataDictionary valueForKey:@"message"]];
        else
            [delegate dataDidDownload:WebServiceTypePostNutriotionPlanCalendar andObject:dataDictionary];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if([delegate respondsToSelector:@selector(dataDidDownloadWithFail:andObject:)])
            [delegate dataDidDownloadWithFail:WebServiceTypePostNutriotionPlanCalendar andObject:nil];
    }];

}


#pragma mark FITNESS

+ (void)getMuscles{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    [manager GET:[NSString stringWithFormat:@"%@%@",path,MUSCLE_PATH] parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        [ParserManager parseMuscles:dataDictionary];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
}

+ (void)getExercises {
    [self getExercisesWithCompletionBlock:nil];
}

+ (void)getExercisesWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock {
    GetExercisesRequest * request = [[GetExercisesRequest alloc] initWithToken:[AppContext sharedInstance].authToken];
    
    [request get:NO
 completionBlock:^(id response, NSError *error) {
     
     if (error != nil) {
         if (completionBlock != nil) {
             completionBlock(NO);
         }
         return;
     }
     
     JSONDecoder * decoder = [[JSONDecoder alloc] init];
     NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
     [ParserManager parseExercises:responseArray];
     
     if (completionBlock != nil) {
         completionBlock(YES);
     }
 }];
}

+ (void)getTrainings {
    [self getTrainingsWithCompletionBlock:nil];
}

+ (void)getTrainingsWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock {
    GetTrainingsRequest * request = [[GetTrainingsRequest alloc] initWithToken:[AppContext sharedInstance].authToken];
    
    [request get:NO
 completionBlock:^(id response, NSError *error) {
     
     if (error != nil) {
         if (completionBlock != nil) {
             completionBlock(NO);
         }
         return;
     }
     
     JSONDecoder * decoder = [[JSONDecoder alloc] init];
     NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
     [ParserManager parseTrainings:responseArray];
     
     if (completionBlock != nil) {
         completionBlock(YES);
     }
 }];
}

+ (void)getTrainingsWithUser:(NSString *)userId {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    //NSDictionary *updateDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[Training lastUpdateDate],@"updatedAt", nil];
    
    NSString *urlQuery = @"";
    
    if([Training lastUpdateDate]){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        urlQuery = [NSString stringWithFormat:@"?updatedAt=%@&user=%@",[formatter stringFromDate:[Training lastUpdateDate]],userId];
    }
    
    [manager GET:[NSString stringWithFormat:@"%@%@%@",path,TRAININGS_PATH,urlQuery] parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        [ParserManager parseTrainings:@[dataDictionary]];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
}

+ (void)getTrainingPlans {
    [self getTrainingPlansWithCompletionBlock:nil];
}

+ (void)getTrainingPlansWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock {
    NSString * updatedAt = @"";
    if([Exercise lastUpdateDate]){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        updatedAt = [formatter stringFromDate:[Exercise lastUpdateDate]];
    }
    
    GetTrainingPlansRequest * request =
    [[GetTrainingPlansRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                         updatedAt:updatedAt];
    
    [request get:NO
 completionBlock:^(id response, NSError *error) {
     
     if (error != nil) {
         if (completionBlock != nil) {
             completionBlock(false);
         }
         return;
     }
     
     JSONDecoder * decoder = [[JSONDecoder alloc] init];
     NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
     [ParserManager parseTrainingPlans:responseArray];
     
     if (completionBlock != nil) {
         completionBlock(true);
     }
 }];
}

+ (void)getConstants{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    NSString *urlQuery = @"";
    
    if([Exercise lastUpdateDate]){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        urlQuery = [NSString stringWithFormat:@"?updatedAt=%@",[formatter stringFromDate:[ValueConstant lastUpdateDate]]];
    }
    
    [manager GET:[NSString stringWithFormat:@"%@%@%@",path,CONSTANTS_PATH,urlQuery] parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        [ParserManager parseConstants:dataDictionary];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];

}

#pragma mark FOODs

+ (void)getNutrionalPlans {
    [WebServiceManager getNutrionalPlansWithCompletionBlock: nil];
}

+ (void)getNutrionalPlansWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock {
    NSString * updatedAt = @"";
    if([NutritionPlan lastUpdateDate]){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];

        updatedAt = [formatter stringFromDate:[NutritionPlan lastUpdateDate]];
    }

    GetNutritionPlansRequest * request =
    [[GetNutritionPlansRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                          updatedAt:updatedAt];

    [request get:NO
 completionBlock:^(id response, NSError *error) {

     if (error != nil) {
         if (completionBlock != nil) {
             completionBlock(false);
         }
         return;
     }

     JSONDecoder * decoder = [[JSONDecoder alloc] init];
     NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
     [ParserManager parseNutritionsPlans:responseArray];
     if (completionBlock != nil) {
         completionBlock(true);
     }
 }];
}

+ (void)getDiets {
    [WebServiceManager getDietsWithWithCompletionBlock:nil];
}

+ (void)getDietsWithWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock {
    GetDietsRequest * request =
    [[GetDietsRequest alloc] initWithToken:[AppContext sharedInstance].authToken];
    
    [request get:NO
 completionBlock:^(id response, NSError *error) {
     
     if (error != nil) {
         if (completionBlock != nil) {
             completionBlock(NO);
         }
         return;
     }
     
     JSONDecoder * decoder = [[JSONDecoder alloc] init];
     NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
     [ParserManager parseDiets:responseArray];
     if (completionBlock != nil) {
         completionBlock(YES);
     }
 }];
}

+ (void)getAliments {
    [WebServiceManager getAlimentsWithCompletionBlock:nil];
}

+ (void)getAlimentsWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock {
    GetAlimentsRequest * request =
    [[GetAlimentsRequest alloc] initWithToken:[AppContext sharedInstance].authToken];
    
    [request get:NO
 completionBlock:^(id response, NSError *error) {
     
     if (error != nil) {
         if (completionBlock != nil) {
             completionBlock(false);
         }
         return;
     }
     
     JSONDecoder * decoder = [[JSONDecoder alloc] init];
     NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
     [ParserManager parseAliments:responseArray];
     
     if (completionBlock != nil) {
         completionBlock(true);
     }
 }];
}


+ (void)getFoods{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    [manager GET:[NSString stringWithFormat:@"%@%@",path,FOODS_PATH] parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        
        [ParserManager parseFoods:dataDictionary];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
}

+ (void)getMeals {
    [WebServiceManager getMealsWithCompletionBlock:nil];
}

+ (void)getMealsWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock {
    GetMealsRequest * request =
    [[GetMealsRequest alloc] initWithToken:[AppContext sharedInstance].authToken];
    
    [request post:NO
 completionBlock:^(id response, NSError *error) {
     
     if (error != nil) {
         if (completionBlock != nil) {
             completionBlock(false);
         }
         return;
     }
     
     JSONDecoder * decoder = [[JSONDecoder alloc] init];
     NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
     [ParserManager parseMeals:responseArray];
     if (completionBlock != nil) {
         completionBlock(true);
     }
 }];
}

+ (void)getTags {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
//    NSDictionary *updateDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[Tag lastUpdateDate],@"updatedAt", nil];
    
    NSString *urlQuery = @"";
    
    if([Tag lastUpdateDate]){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        urlQuery = [NSString stringWithFormat:@"?updatedAt=%@",[formatter stringFromDate:[Tag lastUpdateDate]]];
    }
    
    [manager GET:[NSString stringWithFormat:@"%@%@%@",path,@"tags",urlQuery] parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray * responseArray = [decoder objectWithData:responseObject];
        
        [ParserManager parseTags:responseArray];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
}

+ (void)uploadImage:(NSDictionary *)imageDictionary withDelegate:(id<WebServiceManagerDelegate>)delegate{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager.requestSerializer setValue:[AppContext sharedInstance].authToken forHTTPHeaderField:@"X-Auth-Token"];
    [manager.requestSerializer setValue:[AppContext sharedInstance].userId forHTTPHeaderField:@"X-User-Id"];
    
    [manager POST:[NSString stringWithFormat:@"%@%@",path,@"uploadImageUserRevision"] parameters:imageDictionary progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        NSError *jsonError;
        NSDictionary *dataDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&jsonError];
        NSLog(@"%@",dataDictionary);
        
        [delegate dataDidDownload:WebServiceTypeUploadLogImage andObject:nil];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [delegate dataDidDownloadWithFail:WebServiceTypeUploadLogImage andObject:nil];
    }];
}

@end
