//
//  DeveiceManager.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DeviceManager.h"
#import "AppConstants.h"
#import <CommonCrypto/CommonDigest.h>

@implementation DeviceManager

+ (NSString*)uniqueIDForDevice
{
    NSString* uniqueIdentifier = nil;
    uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    return uniqueIdentifier;
}

+ (NSString *)deviceName{
    return [[UIDevice currentDevice] name];
}

+ (NSString *)deviceModel{
    return [[UIDevice currentDevice] model];
}

+ (NSString *)deviceLocalizedModel{
    return [[UIDevice currentDevice] localizedModel];
}

+ (NSString *)deviceSystemName{
    return [[UIDevice currentDevice] systemName];
}

+ (NSString *)deviceSystemVersion{
    return [[UIDevice currentDevice] systemVersion];
}

@end
