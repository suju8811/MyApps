//
//  DataManager.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DataManager.h"


@implementation DataManager

/*
+ (NSArray *)getFamilies{
    
    Family *firstFamily = [[Family alloc] init];
    [firstFamily setFamilyId:@"1"];
    [firstFamily setTitle:NSLocalizedString(@"LÍNEA FACIAL", nil)];
    [firstFamily setSubtitle:NSLocalizedString(@"Antiarrugas, antiedad, hidratantes, desmaquillantes ...", nil)];
    [firstFamily setImage:@"image-home-facial"];
    [firstFamily setHexColor:@"FFC48F"];
    
    Family *secondFamily = [[Family alloc] init];
    [secondFamily setFamilyId:@"2"];
    [secondFamily setTitle:NSLocalizedString(@"SERUM Y CONTORNO DE OJOS", nil)];
    [secondFamily setSubtitle:NSLocalizedString(@"Reafirmante, aislante facial, contorno de ojos y labios ...", nil)];
    [secondFamily setImage:@"image-home-serum"];
    [secondFamily setHexColor:@"fabe13"];
    
    
    Family *thirdFamily = [[Family alloc] init];
    [thirdFamily setFamilyId:@"3"];
    [thirdFamily setTitle:NSLocalizedString(@"LÍNEA CORPORAL", nil)];
    [thirdFamily setSubtitle:NSLocalizedString(@"Crema hidratante, anticelulíticos, crema de manos, pies", nil)];
    [thirdFamily setImage:@"image-home-body"];
    [thirdFamily setHexColor:@"AECA5D"];
    
 
    Family *fourFamily = [[Family alloc] init];
    [fourFamily setFamilyId:[NSNumber numberWithInteger:1]];
    [fourFamily setTitle:NSLocalizedString(@"PROMOCIONES", nil)];
    [fourFamily setSubtitle:NSLocalizedString(@"Packs, descuentos, ofertas ...", nil)];
    [fourFamily setImage:@"image-promos"];
    [fourFamily setHexColor:@"74C0D8"];
 
    return @[firstFamily, secondFamily, thirdFamily, fourFamily];
}

+ (NSArray *)getPharmacies{
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Pharmacy" ofType:@"plist"]];
    NSArray *pharmacies = [dictionary objectForKey:@"Farmacias"];
    NSMutableArray *pharmaciesSave = [[NSMutableArray alloc] init];
    for(NSDictionary *pharmacyDictionary in pharmacies){
        Pharmacy *pharmacy = [[Pharmacy alloc] init];
        
        [pharmacy setPharmacyId:[pharmacyDictionary valueForKey:@"pharmacyId"]];
        [pharmacy setTitle:[pharmacyDictionary valueForKey:@"title"]];
        [pharmacy setSummary:[pharmacyDictionary valueForKey:@"summary"]];
        [pharmacy setImage:[pharmacyDictionary valueForKey:@"image"]];
        [pharmacy setLogo:[pharmacyDictionary valueForKey:@"logo"]];
        [pharmacy setAddress:[pharmacyDictionary valueForKey:@"address"]];
        [pharmacy setPhone:[pharmacyDictionary valueForKey:@"phone"]];
        [pharmacy setEmail:[pharmacyDictionary valueForKey:@"email"]];
        [pharmacy setLatitude:[pharmacyDictionary valueForKey:@"latitude"]];
        [pharmacy setLongitude:[pharmacyDictionary valueForKey:@"longitude"]];
        [pharmacy setProvinceCode:[pharmacyDictionary valueForKey:@"provinceCode"]];
        [pharmacy setCity:[pharmacyDictionary valueForKey:@"city"]];
        
        [pharmaciesSave addObject:pharmacy];
    }
    
    return pharmaciesSave;
}

+ (NSArray *)getProductsWithFamilyId:(NSNumber *)familyId{
    if(familyId.intValue == 1)
        return [DataManager lineaFacial];
    else if (familyId.intValue == 2)
        return [DataManager lineaSerum];
    else if (familyId.intValue == 3)
        return [DataManager lineaCorporal];
    else{
        
        NSLog(@"%lu",(unsigned long)[[DataManager lineaFacial] count]);
        NSLog(@"%lu",(unsigned long)[[DataManager lineaSerum] count]);
        NSLog(@"%lu",(unsigned long)[[DataManager lineaCorporal] count]);
        
        NSMutableArray *allProdcuts = [[NSMutableArray alloc] initWithArray:[DataManager lineaFacial]];
        [allProdcuts addObjectsFromArray:[DataManager lineaSerum]];
        [allProdcuts addObjectsFromArray:[DataManager lineaCorporal]];
        
        return allProdcuts;
    }
    
    return nil;
}

+ (NSArray *)lineaCorporal{
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"LineaCorporal" ofType:@"plist"]];
    NSArray *products = [dictionary objectForKey:@"Products"];
    NSMutableArray *productLineaCorporal = [[NSMutableArray alloc] init];
    for(NSDictionary *productDictionary in products){
        Product *product = [[Product alloc] init];
        [product setProductId:[productDictionary valueForKey:@"productId"]];
        [product setName:NSLocalizedString([productDictionary valueForKey:@"name"], nil)];
        [product setSummary:NSLocalizedString([productDictionary valueForKey:@"summary"], nil)];
        [product setImage:[productDictionary valueForKey:@"image"]];
        [product setPrice:[productDictionary valueForKey:@"price"]];
        
        [productLineaCorporal addObject:product];
    }
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"price"
                                                                 ascending:FALSE];
    [productLineaCorporal sortUsingDescriptors:@[descriptor]];
    
    return productLineaCorporal;
}

+ (NSArray *)lineaFacial{
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"LineaFacial" ofType:@"plist"]];
    NSArray *products = [dictionary objectForKey:@"Products"];
    NSMutableArray *productLineaCorporal = [[NSMutableArray alloc] init];
    for(NSDictionary *productDictionary in products){
        Product *product = [[Product alloc] init];
        [product setProductId:[productDictionary valueForKey:@"productId"]];
        [product setName:NSLocalizedString([productDictionary valueForKey:@"name"], nil)];
        [product setSummary:NSLocalizedString([productDictionary valueForKey:@"summary"], nil)];
        [product setImage:[productDictionary valueForKey:@"image"]];
        [product setPrice:[productDictionary valueForKey:@"price"]];
        
        [productLineaCorporal addObject:product];
    }
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"price"
                                                                 ascending:FALSE];
    [productLineaCorporal sortUsingDescriptors:@[descriptor]];
    
    return productLineaCorporal;
}

+ (NSArray *)lineaSerum{
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"LineaSerum" ofType:@"plist"]];
    NSArray *products = [dictionary objectForKey:@"Products"];
    NSMutableArray *productLineaCorporal = [[NSMutableArray alloc] init];
    for(NSDictionary *productDictionary in products){
        Product *product = [[Product alloc] init];
        [product setProductId:[productDictionary valueForKey:@"productId"]];
        [product setName:NSLocalizedString([productDictionary valueForKey:@"name"], nil)];
        [product setSummary:NSLocalizedString([productDictionary valueForKey:@"summary"], nil)];
        [product setImage:[productDictionary valueForKey:@"image"]];
        [product setPrice:[productDictionary valueForKey:@"price"]];
        
        [productLineaCorporal addObject:product];
    }
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"price"
                                                                 ascending:FALSE];
    [productLineaCorporal sortUsingDescriptors:@[descriptor]];
    
    return productLineaCorporal;
}

+ (NSArray *)getProvincias{
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Province" ofType:@"plist"]];
    NSArray *provinces = [dictionary objectForKey:@"Provincias"];
    NSMutableArray *provincesSave = [[NSMutableArray alloc] init];
    for(NSDictionary *provinceDictionary in provinces){
        Province *province = [[Province alloc] init];
        [province setNameKey:[provinceDictionary valueForKey:@"nameKey"]];
        [province setName:[provinceDictionary valueForKey:@"name"]];
        
        
        
        [provincesSave addObject:province];
    }
    
    return provincesSave;
}

+ (NSArray *)getProvinciasWithPharmacies{
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Province" ofType:@"plist"]];
    NSArray *provinces = [dictionary objectForKey:@"Provincias"];
    NSMutableArray *provincesSave = [[NSMutableArray alloc] init];
    for(NSDictionary *provinceDictionary in provinces){
        Province *province = [[Province alloc] init];
        [province setNameKey:[provinceDictionary valueForKey:@"nameKey"]];
        [province setName:[provinceDictionary valueForKey:@"name"]];
        
        [provincesSave addObject:province];
    }
    
    return provincesSave;
}
*/

@end
