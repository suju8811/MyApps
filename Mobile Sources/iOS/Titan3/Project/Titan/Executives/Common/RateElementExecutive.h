//
//  RateElementExecutive.h
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseExecutive.h"
#import "BaseElement.h"

@interface RateElementDisplayContext : NSObject

@property (nonatomic, assign) float rate;
@property (nonatomic, strong) NSString * note;
@property (nonatomic, strong) BaseElement * element;

- (id)initWithRate:(float)rate
              note:(NSString*)note
           element:(BaseElement*)element;

@end

@protocol RateElementDisplay <BaseDisplay>

- (void)loadDisplay;
- (RateElementDisplayContext*)displayContext;
- (void)didRateElement;

@end

@interface RateElementExecutive : NSObject

- (id)initWithDisplay:(id<RateElementDisplay>)display;
- (void)displayDidLoad;
- (void)didPressOKButton;

@end
