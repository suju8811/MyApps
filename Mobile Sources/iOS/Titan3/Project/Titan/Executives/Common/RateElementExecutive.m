//
//  RateElementExecutive.m
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RateElementExecutive.h"
#import "RateElementRequest.h"
#import "TrainingPlan.h"
#import "Training.h"
#import "NutritionPlan.h"
#import "Diet.h"
#import "Meal.h"
#import "AppContext.h"
#import "WebServiceManager.h"

@interface RateElementExecutive()

@property id<RateElementDisplay> display;

@end

@implementation RateElementExecutive

- (id)initWithDisplay:(id<RateElementDisplay>)display {
    
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)displayDidLoad {
    [_display loadDisplay];
}

- (void)didPressOKButton {
    RateElementDisplayContext * context = [_display displayContext];
    
    NSInteger elementType;
    NSString * elementId;
    
    //
    // Get element type and id
    //
    
    if ([context.element isKindOfClass:[Training class]]) {
        elementType = 1;
        elementId = ((Training*)context.element).serverTrainingId;
    }
    else if ([context.element isKindOfClass:[TrainingPlan class]]) {
        elementType = 2;
        elementId = ((TrainingPlan*)context.element).serverTrainingPlanId;
    }
    else if ([context.element isKindOfClass:[Meal class]]) {
        elementType = 5;
        elementId = ((Meal*)context.element).serverMealId;
    }
    else if ([context.element isKindOfClass:[Diet class]]) {
        elementType = 6;
        elementId = ((Diet*)context.element).serverDietId;
    }
    else if ([context.element isKindOfClass:[NutritionPlan class]]) {
        elementType = 7;
        elementId = ((NutritionPlan*)context.element).serverNutritionPlanId;
    }
    else {
        elementType = -1;
    }
    
    NSAssert(elementType != -1, @"Unknown element type");
    
    //
    // Post request
    //
    
    RateElementRequest * request = [[RateElementRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                        rate:context.rate
                                                                        note:context.note
                                                                        type:elementType
                                                                   elementId:elementId];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            [_display showMessage:@"Error al calificar la aplicación"];
            return;
        }
        
        [_display showLoading:@"Subiendo ..."];
        [self reloadElements:elementType];
    }];
}

- (void)reloadElements:(NSInteger)elementType {
    switch (elementType) {
        case 1: {
            [WebServiceManager getTrainingsWithCompletionBlock:^(BOOL isSuccess) {
                [_display hideLoading];
                [_display didRateElement];
            }];
        }
        case 2: {
            [WebServiceManager getTrainingPlansWithCompletionBlock:^(BOOL isSuccess) {
                [_display hideLoading];
                [_display didRateElement];
            }];
        }
            break;
        case 5: {
            [WebServiceManager getMealsWithCompletionBlock:^(BOOL isSuccess) {
                [_display hideLoading];
                [_display didRateElement];
            }];
        }
            break;
        case 6: {
            [WebServiceManager getDietsWithWithCompletionBlock:^(BOOL isSuccess) {
                [_display hideLoading];
                [_display didRateElement];
            }];
        }
            break;
        case 7: {
            [WebServiceManager getNutrionalPlansWithCompletionBlock:^(BOOL isSuccess) {
                [_display hideLoading];
                [_display didRateElement];
            }];
        }
            break;
            
        default:
            break;
    }
}

@end

@implementation RateElementDisplayContext

- (id)initWithRate:(float)rate
              note:(NSString*)note
           element:(BaseElement*)element {
    
    self = [super init];
    
    _rate = rate;
    _note = note;
    _element = element;
    
    return self;
    
}

@end
