//
//  RateElementListExecutive.h
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseExecutive.h"
#import "BaseElement.h"

@protocol RateElementListDisplay <BaseDisplay>

- (void)loadRateInfos:(RLMArray<RateInfo*>*)rateInfos;

@end

@interface RateElementListExecutive : NSObject

- (id)initWithDisplay:(id<RateElementListDisplay>)display
              element:(BaseElement*)element;

- (void)reloadList;

@end
