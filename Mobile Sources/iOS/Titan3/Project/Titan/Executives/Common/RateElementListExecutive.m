//
//  RateElementListExecutive.m
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RateElementListExecutive.h"

@interface RateElementListExecutive()

@property id<RateElementListDisplay> display;
@property BaseElement * element;

@end

@implementation RateElementListExecutive

- (id)initWithDisplay:(id<RateElementListDisplay>)display
              element:(BaseElement*)element {
    
    self = [super init];
    
    _display = display;
    _element = element;
    
    return self;
}

- (void)reloadList {
    RLMArray<RateInfo*> * rateInfos = _element.rateInfos;
    [_display loadRateInfos:rateInfos];
}

@end
