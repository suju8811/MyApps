//
//  BaseExecutive.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BaseDisplay <NSObject>

@optional
- (void)showLoading:(NSString*)title;
- (void)hideLoading;
- (void)showMessage:(NSString*)message;

@end

@interface BaseExecutive : NSObject

@end
