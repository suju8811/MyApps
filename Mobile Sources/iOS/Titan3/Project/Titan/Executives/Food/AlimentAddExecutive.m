//
//  AlimentAddExecutive.m
//  Titan
//
//  Created by Marcus Lee on 25/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "AlimentAddExecutive.h"
#import "CreateAlimentRequest.h"
#import "EditAlimentRequest.h"
#import "AppContext.h"

@interface AlimentAddExecutive()

@property id<AlimentAddDisplay> display;

@end

@implementation AlimentAddExecutive

- (id)initWithDisplay:(id<AlimentAddDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)displayDidLoad {
    [_display loadDisplay];
}

- (NSArray*)pickerListForTag:(AlimentPickerType)pickerType {
    switch (pickerType) {
        case AlimentPickerTypeMeasureUnit:
            return @[@"GR", @"ML"];
        case AlimentPickerTypeLabel1:
            return TAG1_ARRAY_FOR_ALIMENT;
        case AlimentPickerTypeLabel2:
            return TAG2_ARRAY_FOR_ALIMENT;
        default:
            return @[];
    }
}

- (void)createAlimentWithImage:(UIImage*)image {
    Aliment * aliment = [_display alimentFromDisplay];
    CreateAlimentRequest * request = [[CreateAlimentRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                         aliment:aliment
                                                                           image:image];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display didPostedAliment:aliment];
    }];
}

- (void)editAlimentWithImage:(UIImage*)image {
    Aliment * aliment = [_display alimentFromDisplay];
    EditAlimentRequest * request = [[EditAlimentRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                     aliment:aliment
                                                                       image:image];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display didPostedAliment:aliment];
    }];
}

@end
