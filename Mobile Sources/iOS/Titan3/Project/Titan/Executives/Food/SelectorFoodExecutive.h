//
//  SelectorFoodExecutive.h
//  Titan
//
//  Created by Marcus Lee on 1/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"

typedef enum {
    SelectorFoodTypeMeal,
    SelectorFoodTypeMealSingle,
    SelectorFoodTypeDiet,
    SelectorFoodTypeDietSingle,
    SelectorFoodTypeNutritionPlan,
    SelectorFoodTypeNutritionPlanSingle,
    SelectorFoodTypeNutritionPlanCalendar,
} SelectorFoodType;

@protocol SelectorFoodDisplay <BaseDisplay>

- (void)dismissWithConfirm;
- (NSArray*)selectedItems;

@end

@interface SelectorFoodExecutive : NSObject

- (id)initWithDisplay:(id<SelectorFoodDisplay>)display
         selectorType:(SelectorFoodType)selectorType;

- (void)didPressOKButton;

@end
