//
//  DietAddExecutive.m
//  Titan
//
//  Created by Marcus Lee on 27/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DietAddExecutive.h"
#import "AppContext.h"
#import "CreateDietRequest.h"
#import "EditDietRequest.h"

@interface DietAddExecutive()

@property id<DietAddDisplay> display;

@end

@implementation DietAddExecutive

- (id)initWithDisplay:(id<DietAddDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)createDiet {
    Diet * newDiet = [_display dietFromDisplay];
    CreateDietRequest * request = [[CreateDietRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                      diet:newDiet];
    
    [_display showLoading:@"Creando Dieta"];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display didCreatedDiet:newDiet];
    }];
}

- (void)editDiet {
    Diet * editDiet = [_display dietFromDisplay];
    EditDietRequest * request = [[EditDietRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                  diet:editDiet];
    
    [_display showLoading:@"Actualizando ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display didCreatedDiet:editDiet];
    }];
}

@end
