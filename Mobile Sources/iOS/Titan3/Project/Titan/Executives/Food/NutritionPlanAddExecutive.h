//
//  NutritionPlanAddExecutive.h
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NutritionPlan.h"
#import "BaseExecutive.h"

@interface NutritionPlanAddDisplayContext : NSObject

@property NSString * title;
@property NSString * content;
@property NSString * note;
@property NSMutableArray * selectedTags;
@property NSString * privacy;

@end

@protocol NutritionPlanAddDisplay <BaseDisplay>

- (NutritionPlan*)nutritionPlanFromDisplay;
- (void)didPostNutritionPlan:(NutritionPlan*)nutritionPlan;
- (void)didUpdateNutritionPlan:(NutritionPlan*)nutritionPlan;

@end

@interface NutritionPlanAddExecutive : NSObject

- (id)initWithDisplay:(id<NutritionPlanAddDisplay>)display;

- (void)createNutritionPlan:(NutritionPlan*)nutritionPlan;

- (void)updateNutritionPlan:(NutritionPlan*)nutritionPlan;

@end
