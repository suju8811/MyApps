//
//  FoodExecutive.h
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"
#import "NutritionPlan.h"
#import "Meal.h"
#import "Diet.h"
#import "Aliment.h"

typedef enum {
    FoodSelectTypePlan = 0,
    FoodSelectTyeDiet,
    FoodSelectTypeMeal,
    FoodSelectTypeAliment
} FoodSelectType;


typedef enum{
    MealSelectTypeUser,
    MealSelectTypeAdmin,
    MealSelectTypeFavourite,
    MealSelectTypeShared
    
} MealSelectType;

typedef enum{
    DietSelectTypeUser,
    DietSelectTypeAdmin,
    DietSelectTypeFavourite,
    DietSelectTypeShared
    
} DietSelectType;

typedef enum{
    NutritionPlanSelectTypeUser,
    NutritionPlanSelectTypeAdmin,
    NutritionPlanSelectTypeFavourite,
    NutritionPlanSelectTypeShared
} NutritionPlanSelectType;

@protocol FoodDisplay <BaseDisplay>

- (void)didDeletedNutritionPlan:(NutritionPlan*)nutritionPlan;
- (void)didDeletedMeal:(Meal*)meal;
- (void)didDeleteDiet:(Diet*)diet;
- (void)didDeleteAliment:(Aliment*)aliment;
- (void)reloadItems;

- (void)didLoadItems:(RLMResults*)items
         forFoodType:(FoodSelectType)foodType;

@end

@interface FoodExecutive : NSObject

- (id)initWithDisplay:(id<FoodDisplay>)display;
- (void)deleteNutritionPlan:(NutritionPlan*)nutritionPlan;
- (void)deleteMeal:(Meal*)meal;
- (void)deleteDiet:(Diet*)meal;
- (void)deleteAliment:(Aliment*)aliment;
- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2;
- (void)shareContent:(RLMObject*)content;
- (void)addNutritionPlan:(NutritionPlan*)nutritionPlan
                  toDate:(NSDate*)date;

- (void)loadItemsForFoodSelectType:(FoodSelectType)fitnessType
                    mealSelectType:(MealSelectType)mealSelectType
                    dietSelectType:(DietSelectType)dietSelectType
           nutritionPlanSelectType:(NutritionPlanSelectType)nutritionPlanSelectType
               alimentSegmentState:(NSInteger)segmentState
                        searchText:(NSString*)searchText;

@end
