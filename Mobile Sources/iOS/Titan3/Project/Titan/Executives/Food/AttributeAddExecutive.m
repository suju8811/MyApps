//
//  AttributeExecutive.m
//  Titan
//
//  Created by Marcus Lee on 2/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "AttributeAddExecutive.h"

@interface AttributeAddExecutive()

@property id<AttributeAddDisplay> display;

@end

@implementation AttributeAddExecutive

- (id)initWithDisplay:(id<AttributeAddDisplay>)display {
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)didPressOKButton {
    FoodAttribute * attribute = [_display attributeFromDisplay];
    
    if (attribute.name.length == 0) {
        [_display showMessage:@"Por favor ingrese el nombre del atributo"];
        return;
    }
    
    [_display didConfirmAdd:attribute];
}

@end
