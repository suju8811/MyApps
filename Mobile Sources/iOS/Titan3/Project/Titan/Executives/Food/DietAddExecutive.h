//
//  DietAddExecutive.h
//  Titan
//
//  Created by Marcus Lee on 27/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"
#import "Diet.h"

@protocol DietAddDisplay <BaseDisplay>

- (Diet*)dietFromDisplay;
- (void)didCreatedDiet:(Diet*)diet;

@end

@interface DietAddExecutive : NSObject

- (id)initWithDisplay:(id<DietAddDisplay>)display;
- (void)createDiet;
- (void)editDiet;

@end
