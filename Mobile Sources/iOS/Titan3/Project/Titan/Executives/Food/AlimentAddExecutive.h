//
//  AlimentAddExecutive.h
//  Titan
//
//  Created by Marcus Lee on 25/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseExecutive.h"
#import "Aliment.h"

#define TAG1_ARRAY_FOR_ALIMENT @[@"Proteínas", @"Grasas", @"Carbohidratos", @"Otro"]
#define TAG2_ARRAY_FOR_ALIMENT @[@"Fruta", @"Frutos secos", @"Verduras y Hortalizas", @"Cereales y Legumbres", @"Aceites y Grasas", @"Lacteos", @"Azúcares y Dulces", @"Carnes", @"Pescados y Mariscos", @"Sushi", @"Huevos", @"Salsas y Especias", @"Bollería", @"Repostería", @"Aperitivos", @"Bebidas"]

typedef enum : NSUInteger {
    AlimentPickerTypeMeasureUnit = 100,
    AlimentPickerTypeLabel1,
    AlimentPickerTypeLabel2
} AlimentPickerType;

@protocol AlimentAddDisplay <BaseDisplay>

- (void)loadDisplay;
- (Aliment*)alimentFromDisplay;
- (void)didPostedAliment:(Aliment*)aliment;

@end

@interface AlimentAddExecutive : NSObject

- (id)initWithDisplay:(id<AlimentAddDisplay>)display;

- (void)displayDidLoad;

- (NSArray*)pickerListForTag:(AlimentPickerType)pickerType;

- (void)createAlimentWithImage:(UIImage*)image;

- (void)editAlimentWithImage:(UIImage*)image;

@end
