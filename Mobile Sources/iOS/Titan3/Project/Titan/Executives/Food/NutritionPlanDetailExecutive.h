//
//  NutritionPlanDetailExecutive.h
//  Titan
//
//  Created by Marcus Lee on 28/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NutritionPlan.h"
#import "BaseExecutive.h"

@protocol NutritionPlanDetailDisplay <BaseDisplay>

- (void)didUpdateNutritionPlan:(NutritionPlan*)nutritionPlan;

- (void)reloadAllElements;

@end

@interface NutritionPlanDetailExecutive : NSObject

- (id)initWithDisplay:(id<NutritionPlanDetailDisplay>)display;

- (void)updateNutritionPlan:(NutritionPlan*)trainingPlan;

- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2;

- (void)shareContent:(RLMObject*)content;

- (void)addNutritionPlan:(NutritionPlan*)nutritionPlan
                  toDate:(NSDate*)date;

@end
