//
//  DietDetailExecutive.h
//  Titan
//
//  Created by Marcus Lee on 30/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Diet.h"
#import "BaseExecutive.h"

@protocol DietDetailDisplay <BaseDisplay>

- (void)didUpdatedDiet:(Diet*)meal;
- (void)didUpdatedMeal:(Meal*)meal;
- (void)reloadAllElements;

@end

@interface DietDetailExecutive : NSObject

- (id)initWithDisplay:(id<DietDetailDisplay>)display;

- (void)updateDiet:(Diet*)diet;

- (void)updateMeal:(Meal*)meal;

- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2;

- (void)shareContent:(RLMObject*)content;

- (void)updateStartTime:(NSDate*)startTime
             ofDietMeal:(DietMeal*)dietMeal
                 ofDiet:(Diet*)diet;

@end
