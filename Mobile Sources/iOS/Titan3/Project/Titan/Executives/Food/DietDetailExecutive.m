//
//  DietDetailExecutive.m
//  Titan
//
//  Created by Marcus Lee on 30/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DietDetailExecutive.h"
#import "EditDietRequest.h"
#import "EditMealRequest.h"
#import "AppContext.h"
#import "UIImage+AFNetworking.h"
#import "SendContentRequest.h"
#import "ShareContentRequest.h"
#import "DateTimeUtil.h"

@interface DietDetailExecutive()

@property (nonatomic, strong) id<DietDetailDisplay> display;

@end

@implementation DietDetailExecutive

- (id)initWithDisplay:(id<DietDetailDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)updateMeal:(Meal*)meal {
    UIImage * mealImage;
    
    if (meal.image) {
        NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString: meal.image]];
        if (imageData) {
            mealImage = [UIImage safeImageWithData:imageData];
        }
    }
    
    EditMealRequest * request = [[EditMealRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                  meal:meal
                                                                 image:mealImage];
    
    [_display showLoading:@"Actualizando ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display didUpdatedMeal:meal];
    }];
}

- (void)updateDiet:(Diet*)diet {
    
    EditDietRequest * request = [[EditDietRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                  diet:diet];
    
    [_display showLoading:@"Actualizando ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display didUpdatedDiet:diet];
    }];
}

- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2 {
    SendContentRequest * request = [[SendContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                       token2:token2
                                                                    rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadAllElements];
    }];
}

- (void)shareContent:(RLMObject*)content {
    ShareContentRequest * request = [[ShareContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                      rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadAllElements];
    }];
}

- (void)updateStartTime:(NSDate*)startTime
             ofDietMeal:(DietMeal*)dietMeal
                 ofDiet:(Diet*)diet {
    
    RLMRealm *realm = [AppContext currentRealm];
    
    NSString * startTimeString = [DateTimeUtil stringFromDate:startTime
                                                       format:@"HH:mm:ss"];
    
    [realm transactionWithBlock:^{
        dietMeal.startTime = startTimeString;
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [self updateDiet:diet];
}

@end
