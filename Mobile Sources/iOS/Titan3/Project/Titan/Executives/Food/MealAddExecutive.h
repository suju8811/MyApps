//
//  MealAddExecutive.h
//  Titan
//
//  Created by Marcus Lee on 26/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseExecutive.h"
#import "Meal.h"

@protocol MealAddDisplay <BaseDisplay>

- (Meal*)mealFromDisplay;
- (void)didCreateMeal:(Meal*)meal;

@end

@interface MealAddExecutive : NSObject

- (id)initWithDisplay:(id<MealAddDisplay>)display;
- (void)createMealWithImage:(UIImage*)image;
- (void)editMealWithImage:(UIImage*)image;

@end
