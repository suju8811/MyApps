//
//  NutritionPlanAddExecutive.m
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "NutritionPlanAddExecutive.h"
#import "CreateNutritionPlanRequest.h"
#import "EditNutritionPlanRequest.h"
#import "AppContext.h"
#import "JSONKit.h"

@interface NutritionPlanAddExecutive()

@property id<NutritionPlanAddDisplay> display;

@end

@implementation NutritionPlanAddExecutive

- (id)initWithDisplay:(id<NutritionPlanAddDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)createNutritionPlan:(NutritionPlan*)nutritionPlan {
    CreateNutritionPlanRequest * request
    = [[CreateNutritionPlanRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                          nutritionPlan:nutritionPlan];
    
    [_display showLoading:@"Creando Plan"];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        if (responseArray == nil || responseArray.count == 0) {
            [_display showMessage:@"Failed to create training plan"];
            return;
        }
        
        if (![[[responseArray firstObject] valueForKey:@"active"] boolValue]) {
            [_display showMessage:@"Failed to create training plan"];
            return;
        }
        
        [_display didPostNutritionPlan:nutritionPlan];
    }];
}

- (void)updateNutritionPlan:(NutritionPlan*)nutritionPlan {
    EditNutritionPlanRequest * request
    = [[EditNutritionPlanRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                               nutritionPlan:nutritionPlan];

    [_display showLoading:@"Actualizando ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }

        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        if (responseArray == nil || responseArray.count == 0) {
            [_display showMessage:@"Failed to update training plan"];
            return;
        }

        if (![[[responseArray firstObject] valueForKey:@"active"] boolValue]) {
            [_display showMessage:@"Failed to update training plan"];
            return;
        }

        [_display didUpdateNutritionPlan:nutritionPlan];
    }];
}

@end
