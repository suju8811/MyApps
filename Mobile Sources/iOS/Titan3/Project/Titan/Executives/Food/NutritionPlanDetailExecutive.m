//
//  NutritionPlanDetailExecutive.m
//  Titan
//
//  Created by Marcus Lee on 28/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "NutritionPlanDetailExecutive.h"
#import "EditNutritionPlanRequest.h"
#import "SendContentRequest.h"
#import "ShareContentRequest.h"
#import "AppContext.h"
#import "Schedule.h"

@interface NutritionPlanDetailExecutive()

@property id<NutritionPlanDetailDisplay> display;

@end

@implementation NutritionPlanDetailExecutive

- (id)initWithDisplay:(id<NutritionPlanDetailDisplay>)display {
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)updateNutritionPlan:(NutritionPlan*)nutritionPlan {
    
    EditNutritionPlanRequest * request
    = [[EditNutritionPlanRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                        nutritionPlan:nutritionPlan];
    
    [_display showLoading:@"Editando"];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        if (responseArray == nil || responseArray.count == 0) {
            [_display showMessage:@"Failed to update nutrition plan"];
            return;
        }
        
        if (![[[responseArray firstObject] valueForKey:@"active"] boolValue]) {
            [_display showMessage:@"Failed to update nutrition plan"];
            return;
        }
        
        [_display didUpdateNutritionPlan:nutritionPlan];
    }];
}

- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2 {
    SendContentRequest * request = [[SendContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                       token2:token2
                                                                    rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadAllElements];
    }];
}

- (void)shareContent:(RLMObject*)content {
    ShareContentRequest * request = [[ShareContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                      rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadAllElements];
    }];
}

- (void)addNutritionPlan:(NutritionPlan*)nutritionPlan
                  toDate:(NSDate*)date {
    
    //
    // Check the content for date
    //
    
    NSString * checkResult = [Schedule checkContentForDate:date];
    if (checkResult) {
        [_display showMessage:checkResult];
        return;
    }
    
    //
    // Register nutrition plan
    //
    
    if ([Schedule addNutritionPlan:nutritionPlan
                            toDate:date]) {
        
        [_display showMessage:@"plan de nutrición se registra con éxito."];
    }
}

@end
