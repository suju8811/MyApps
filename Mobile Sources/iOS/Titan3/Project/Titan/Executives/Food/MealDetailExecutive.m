//
//  MealDetailExecutive.m
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "MealDetailExecutive.h"
#import "EditMealRequest.h"
#import "SendContentRequest.h"
#import "ShareContentRequest.h"
#import "AppContext.h"

@interface MealDetailExecutive()

@property (nonatomic, strong) id<MealDetailDisplay> display;

@end

@implementation MealDetailExecutive

- (id)initWithDisplay:(id<MealDetailDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)updateMeal:(Meal*)meal
         withImage:(UIImage*)image {
    
    EditMealRequest * request = [[EditMealRequest alloc] initWithToken:[AppContext sharedInstance].authToken meal:meal
                                                                 image:image];
    
    [_display showLoading:@"Actualizando ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display didUpdatedMeal:meal];
    }];
}

- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2 {
    SendContentRequest * request = [[SendContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                       token2:token2
                                                                    rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadAllElements];
    }];
}

- (void)shareContent:(RLMObject*)content {
    ShareContentRequest * request = [[ShareContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                      rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadAllElements];
    }];
}

@end
