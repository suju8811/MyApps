//
//  SelectorFoodExecutive.m
//  Titan
//
//  Created by Marcus Lee on 1/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SelectorFoodExecutive.h"

@interface SelectorFoodExecutive()

@property id<SelectorFoodDisplay> display;
@property (nonatomic, assign) SelectorFoodType selectorType;

@end

@implementation SelectorFoodExecutive

- (id)initWithDisplay:(id<SelectorFoodDisplay>)display
         selectorType:(SelectorFoodType)selectorType {
    
    self = [super init];
    
    _display = display;
    _selectorType = selectorType;
    
    return self;
}

- (void)didPressOKButton {
    if ([self validateItems] != nil) {
        [_display showMessage:[self validateItems]];
        return;
    }
    
    [_display dismissWithConfirm];
}

- (NSString*)validateItems {
    NSArray * items = [_display selectedItems];
    if (items != nil
        && items.count > 0) {
        return nil;
    }
    
    NSString * invalidMessage = nil;
    switch (_selectorType) {
        case SelectorFoodTypeNutritionPlanSingle:
            invalidMessage = @"Por favor, seleccione el dieta que quiere sustituir por otro";
            break;
        case SelectorFoodTypeNutritionPlan:
            invalidMessage = @"Debes introducir por lo menos una dieta";
            break;
        case SelectorFoodTypeDietSingle:
            invalidMessage = @"Por favor, seleccione el comida que quiere sustituir por otro";
            break;
        case SelectorFoodTypeDiet:
            invalidMessage = @"Debes introducir por lo menos una comida";
            break;
        case SelectorFoodTypeMeal:
            invalidMessage = @"Debes introducir por lo menos un alimento";
            break;
        case SelectorFoodTypeMealSingle:
            invalidMessage = @"Por favor, seleccione el alimento que quiere sustituir por otro";
            break;
        default:
            break;
    }
    
    return invalidMessage;
}

@end
