//
//  OptionFoodExecutive.h
//  Titan
//
//  Created by Marcus Lee on 27/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"
#import "Meal.h"
#import "NutritionPlan.h"

@protocol OptionFoodDisplay <BaseDisplay>

- (void)didDeletedAliment: (Aliment*)aliment;

- (void)didDuplicateMeal:(Meal*)meal;

- (void)didDeletedMeal:(Meal*)meal;

- (void)didFavoriteMeal:(Meal*)meal;

- (void)didFavoriteDiet:(Diet*)diet;

- (void)didDuplicatedDiet:(Diet*)diet;

- (void)didDeletedDiet:(Diet*)diet;

- (void)didUpdateNutritionPlan:(NutritionPlan*)plan;

- (void)didDuplicateNutritionPlan:(NutritionPlan*)plan;

- (void)didFavoriteNutritionPlan:(NutritionPlan*)plan;

- (void)didDeletedNutritionPlan:(NutritionPlan*)plan;

- (void)dismissDisplay;

@end

@interface OptionFoodExecutive : NSObject

- (id)initWithDisplay:(id<OptionFoodDisplay>)display;

- (void)deleteAliment: (Aliment*)aliment;

- (void)duplicateMeal:(Meal*)meal;

- (void)favoriteMeal:(Meal*)meal;

- (void)deleteMeal:(Meal*)meal;

- (void)duplicateDiet:(Diet*)diet;

- (void)deleteDiet:(Diet*)diet;

- (void)favoriteDiet:(Diet*)diet;

- (void)updateNutritionPlan:(NutritionPlan*)plan;

- (void)duplicateNutritionPlan:(NutritionPlan*)plan;

- (void)favoriteNutritionPlan:(NutritionPlan*)plan;

- (void)deleteNutritionPlan:(NutritionPlan*)plan;

@end
