//
//  MealAddExecutive.m
//  Titan
//
//  Created by Marcus Lee on 26/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "MealAddExecutive.h"
#import "CreateMealRequest.h"
#import "EditMealRequest.h"
#import "AppContext.h"

@interface MealAddExecutive()

@property id<MealAddDisplay> display;

@end

@implementation MealAddExecutive

- (id)initWithDisplay:(id<MealAddDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)createMealWithImage:(UIImage*)image {
    Meal * newMeal = [_display mealFromDisplay];
    CreateMealRequest * request = [[CreateMealRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                      meal:newMeal
                                                                     image:image];
    
    [_display showLoading:@"Creando Comida"];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display didCreateMeal:newMeal];
    }];
}

- (void)editMealWithImage:(UIImage*)image {
    Meal * newMeal = [_display mealFromDisplay];
    EditMealRequest * request = [[EditMealRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                      meal:newMeal
                                                                     image:image];
    
    [_display showLoading:@"Actualizando ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display didCreateMeal:newMeal];
    }];
}

@end
