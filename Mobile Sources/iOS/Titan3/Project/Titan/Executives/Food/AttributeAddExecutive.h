//
//  AttributeExecutive.h
//  Titan
//
//  Created by Marcus Lee on 2/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"
#import "FoodAttribute.h"

@protocol AttributeAddDisplay <BaseDisplay>

- (FoodAttribute*)attributeFromDisplay;

- (void)didConfirmAdd:(FoodAttribute*)attribute;

@end

@interface AttributeAddExecutive : NSObject

- (id)initWithDisplay:(id<AttributeAddDisplay>)display;

- (void)didPressOKButton;

@end
