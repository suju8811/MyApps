//
//  OptionFoodExecutive.m
//  Titan
//
//  Created by Marcus Lee on 27/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OptionFoodExecutive.h"
#import "AppContext.h"
#import "CreateMealRequest.h"
#import "DeleteMealRequest.h"
#import "FavoriteMealRequest.h"
#import "CreateDietRequest.h"
#import "DeleteDietRequest.h"
#import "FavoriteDietRequest.h"
#import "CreateNutritionPlanRequest.h"
#import "DeleteNutritionPlanRequest.h"
#import "FavoriteNutritionPlanRequest.h"
#import "UIImage+AFNetworking.h"
#import "DeleteAlimentRequest.h"
#import "DeleteSupplementRequest.h"

@interface OptionFoodExecutive()

@property id<OptionFoodDisplay> display;

@end

@implementation OptionFoodExecutive

- (id)initWithDisplay:(id<OptionFoodDisplay>)display {
    
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)deleteAliment: (Aliment*)aliment {
    BaseRequest * request;
    if (aliment.isSupplement != nil &&
        aliment.isSupplement.boolValue) {
        
        request = [[DeleteSupplementRequest alloc] initWithSupplementId:aliment.supplementId
                                                                  token:[AppContext sharedInstance].authToken];
    }
    else {
        request = [[DeleteAlimentRequest alloc] initWithAlimentId:aliment.alimentId
                                                            token:[AppContext sharedInstance].authToken];
    }
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [Aliment deleteAliment:aliment];
        [_display didDeletedAliment:aliment];
    }];
}

- (void)duplicateMeal:(Meal*)meal {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm transactionWithBlock:^{
        [meal setTitle:[NSString stringWithFormat:@"%@ %@", meal.title, NSLocalizedString(@"Copia", nil)]];
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [_display showLoading:@"Duplicando ..."];
    NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:meal.image]];
    UIImage * image = nil;
    if (imageData != nil) {
        image = [UIImage safeImageWithData:imageData];
    }
    
    CreateMealRequest * request =
    [[CreateMealRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                        meal:meal
                                       image:image];
    
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display didDuplicateMeal:meal];
    }];
}

- (void)favoriteMeal:(Meal*)meal {
    NSNumber * favoriteValue;
    
    if(meal.isFavourite != nil && meal.isFavourite.boolValue){
        favoriteValue = [NSNumber numberWithBool:NO];
    }
    else {
        favoriteValue = [NSNumber numberWithBool:YES];
    }
    
    FavoriteMealRequest * request =
    [[FavoriteMealRequest alloc] initWithMealId:meal.serverMealId
                                          token:[AppContext sharedInstance].authToken
                                     isFavorite:favoriteValue.boolValue];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            [meal setIsFavourite:favoriteValue];
        }];
        
        [realm beginWriteTransaction];
        NSError *transactionError;
        [realm commitWriteTransaction:&transactionError];
        if(transactionError){
            NSLog(@"%@",error.description);
        }
        
        [_display didFavoriteMeal:meal];
    }];
}

- (void)deleteMeal:(Meal*)meal {
    DeleteMealRequest * request =
    [[DeleteMealRequest alloc] initWithMealId:meal.serverMealId
                                        token:[AppContext sharedInstance].authToken];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [Meal deleteMeal:meal];
        [_display didDeletedMeal:meal];
    }];
}

- (void)duplicateDiet:(Diet*)diet {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm transactionWithBlock:^{
        [diet setTitle:[NSString stringWithFormat:@"%@ %@", diet.title, NSLocalizedString(@"Copia", nil)]];
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [_display showLoading:@"Duplicando ..."];

    CreateDietRequest * request =
    [[CreateDietRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                        diet:diet];
    
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display didDuplicatedDiet:diet];
    }];
}

- (void)deleteDiet:(Diet*)diet {
    DeleteDietRequest * request =
    [[DeleteDietRequest alloc] initWithDietId:diet.serverDietId
                                        token:[AppContext sharedInstance].authToken];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [Diet deleteDiet:diet];
        [_display didDeletedDiet:diet];
    }];
}

- (void)favoriteDiet:(Diet*)diet {
    NSNumber * favoriteValue;
    
    if(diet.isFavourite != nil && diet.isFavourite.boolValue){
        favoriteValue = [NSNumber numberWithBool:NO];
    }
    else {
        favoriteValue = [NSNumber numberWithBool:YES];
    }
    
    FavoriteDietRequest * request =
    [[FavoriteDietRequest alloc] initWithDietId:diet.serverDietId
                                          token:[AppContext sharedInstance].authToken
                                     isFavorite:favoriteValue.boolValue];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            [diet setIsFavourite:favoriteValue];
        }];
        
        [realm beginWriteTransaction];
        NSError *transactionError;
        [realm commitWriteTransaction:&transactionError];
        if(transactionError){
            NSLog(@"%@",error.description);
        }
        
        [_display didFavoriteDiet:diet];
    }];
}

- (void)updateNutritionPlan:(NutritionPlan*)plan {
}

- (void)duplicateNutritionPlan:(NutritionPlan*)nutritionPlan {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm transactionWithBlock:^{
        [nutritionPlan setTitle:[NSString stringWithFormat:@"%@ %@", nutritionPlan.title, NSLocalizedString(@"Copia", nil)]];
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error) {
        NSLog(@"%@",error.description);
    }
    
    CreateNutritionPlanRequest * request
    = [[CreateNutritionPlanRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                          nutritionPlan:nutritionPlan];
    
    [_display showLoading:@"Duplicando ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        if (responseArray == nil || responseArray.count == 0) {
            [_display showMessage:@"Failed to create training plan"];
            return;
        }
        
        if (![[[responseArray firstObject] valueForKey:@"active"] boolValue]) {
            [_display showMessage:@"Failed to create training plan"];
            return;
        }
        
        [_display didDuplicateNutritionPlan:nutritionPlan];
    }];
}

- (void)favoriteNutritionPlan:(NutritionPlan*)plan {
    NSNumber * favoriteValue;
    
    if(plan.isFavourite != nil && plan.isFavourite.boolValue){
        favoriteValue = [NSNumber numberWithBool:NO];
    }
    else {
        favoriteValue = [NSNumber numberWithBool:YES];
    }
    
    FavoriteNutritionPlanRequest * request =
    [[FavoriteNutritionPlanRequest alloc] initWithNutritionPlanId:plan.serverNutritionPlanId
                                                   token:[AppContext sharedInstance].authToken
                                              isFavorite:favoriteValue.boolValue];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            [plan setIsFavourite:favoriteValue];
        }];
        
        [realm beginWriteTransaction];
        NSError *transactionError;
        [realm commitWriteTransaction:&transactionError];
        if(transactionError){
            NSLog(@"%@",error.description);
        }
        
        [_display didFavoriteNutritionPlan:plan];
    }];
}

- (void)deleteNutritionPlan:(NutritionPlan*)plan {
    DeleteNutritionPlanRequest * request =
    [[DeleteNutritionPlanRequest alloc] initWithNutritionPlanId:plan.serverNutritionPlanId
                                                          token:[AppContext sharedInstance].authToken];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [NutritionPlan deleteNutritionPlan:plan];
        [_display didDeletedNutritionPlan:plan];
    }];
}

@end
