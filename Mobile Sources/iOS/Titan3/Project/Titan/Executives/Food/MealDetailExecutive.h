//
//  MealDetailExecutive.h
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Meal.h"
#import "BaseExecutive.h"

@protocol MealDetailDisplay <BaseDisplay>

- (void)didUpdatedMeal:(Meal*)meal;

- (void)reloadAllElements;

@end

@interface MealDetailExecutive : NSObject

- (id)initWithDisplay:(id<MealDetailDisplay>)display;

- (void)updateMeal:(Meal*)meal
         withImage:(UIImage*)image;

- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2;

- (void)shareContent:(RLMObject*)content;

@end
