//
//  FoodExecutive.m
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FoodExecutive.h"
#import "DeleteNutritionPlanRequest.h"
#import "DeleteMealRequest.h"
#import "DeleteDietRequest.h"
#import "DeleteAlimentRequest.h"
#import "DeleteSupplementRequest.h"
#import "AppContext.h"
#import "AppConstants.h"
#import "ShareContentRequest.h"
#import "SendContentRequest.h"
#import "SchedulePlan.h"
#import "Event.h"
#import "UtilManager.h"
#import "Schedule.h"
#import "WebServiceManager.h"

@interface FoodExecutive()

@property id<FoodDisplay> display;

@end

@implementation FoodExecutive

- (id)initWithDisplay:(id<FoodDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)loadItemsForFoodSelectType:(FoodSelectType)fitnessType
                    mealSelectType:(MealSelectType)mealSelectType
                    dietSelectType:(DietSelectType)dietSelectType
           nutritionPlanSelectType:(NutritionPlanSelectType)nutritionPlanSelectType
               alimentSegmentState:(NSInteger)segmentState
                        searchText:(NSString*)searchText {
    
    if (mealSelectType == MealSelectTypeShared) {
        [WebServiceManager getMealsWithCompletionBlock:^(BOOL isSuccess) {
            [self loadLocalItemsForFoodSelectType:fitnessType
                                   mealSelectType:mealSelectType
                                   dietSelectType:dietSelectType
                          nutritionPlanSelectType:nutritionPlanSelectType
                              alimentSegmentState:segmentState searchText:searchText];
        }];
        return;
    }
    
    if (dietSelectType == DietSelectTypeShared) {
        [WebServiceManager getDietsWithWithCompletionBlock:^(BOOL isSuccess) {
            [self loadLocalItemsForFoodSelectType:fitnessType
                                   mealSelectType:mealSelectType
                                   dietSelectType:dietSelectType
                          nutritionPlanSelectType:nutritionPlanSelectType
                              alimentSegmentState:segmentState searchText:searchText];
        }];
        return;
    }
    
    if (nutritionPlanSelectType == NutritionPlanSelectTypeShared) {
        [WebServiceManager getNutrionalPlansWithCompletionBlock:^(BOOL isSuccess) {
            [self loadLocalItemsForFoodSelectType:fitnessType
                                   mealSelectType:mealSelectType
                                   dietSelectType:dietSelectType
                          nutritionPlanSelectType:nutritionPlanSelectType
                              alimentSegmentState:segmentState searchText:searchText];
        }];
        return;
    }
    
    [self loadLocalItemsForFoodSelectType:fitnessType
                           mealSelectType:mealSelectType
                           dietSelectType:dietSelectType
                  nutritionPlanSelectType:nutritionPlanSelectType
                      alimentSegmentState:segmentState
                               searchText:searchText];
    
}

- (void)loadLocalItemsForFoodSelectType:(FoodSelectType)foodType
                         mealSelectType:(MealSelectType)mealSelectType
                         dietSelectType:(DietSelectType)dietSelectType
                nutritionPlanSelectType:(NutritionPlanSelectType)nutritionPlanSelectType
                    alimentSegmentState:(NSInteger)segmentState
                             searchText:(NSString*)searchText {
    
    RLMResults *items;
    switch (foodType) {
        case FoodSelectTypePlan:
            if (nutritionPlanSelectType == NutritionPlanSelectTypeUser) {
                items = [NutritionPlan getMyNutritionPlansWithKeyword:searchText];
            }
            else if (nutritionPlanSelectType == NutritionPlanSelectTypeFavourite) {
                items = [NutritionPlan getFavouritesNutritionPlansWithKeyword:searchText];
            }
            else if (nutritionPlanSelectType == NutritionPlanSelectTypeAdmin) {
                items = [NutritionPlan getOtherNutritionPlansWithKeyword:searchText];
            }
            else if (nutritionPlanSelectType == NutritionPlanSelectTypeShared) {
                items = [NutritionPlan getSharedPlansNutritionPlansWithKeyword:searchText];
            }
            break;
        case FoodSelectTyeDiet:
            if (dietSelectType == DietSelectTypeUser) {
                items = [Diet getMyDietsWithKeyword:searchText];
            }
            else if (dietSelectType == DietSelectTypeAdmin) {
                items = [Diet getOtherDietsWithKeyword:searchText];
            }
            else if (dietSelectType == DietSelectTypeFavourite) {
                items = [Diet getFavouritesDietsWithKeyword:searchText];
            }
            else {
                items = [Diet getSharedDietsWithKeyword:searchText];
            }
            break;
        case FoodSelectTypeMeal:
            if (mealSelectType == MealSelectTypeUser) {
                items = [Meal getMyMealsWithKeyword:searchText];
            }
            else if (mealSelectType == MealSelectTypeAdmin) {
                items = [Meal getOtherMealsWithKeyword:searchText];
            }
            else if (mealSelectType == MealSelectTypeFavourite) {
                items = [Meal getFavouritesMealsWithKeyword:searchText];
            }
            else {
                items = [Meal getSharedMealsWithKeyword:searchText];
            }
            break;
        case FoodSelectTypeAliment:{
            if(segmentState == 0) {
                items = [Aliment getAlimentsWithKeyword:searchText];
            }
            else {
                items = [Aliment getSupplementsWithKeyword:searchText];
            }
            break;
        }
        default:
            break;
    }
    
    [_display didLoadItems:items forFoodType:foodType];
}

- (void)deleteNutritionPlan:(NutritionPlan*)nutritionPlan {
    DeleteNutritionPlanRequest * request =
    [[DeleteNutritionPlanRequest alloc] initWithNutritionPlanId:nutritionPlan.serverNutritionPlanId
                                                          token:[AppContext sharedInstance].authToken];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [NutritionPlan deleteNutritionPlan:nutritionPlan];
        [_display didDeletedNutritionPlan:nutritionPlan];
    }];
}

- (void)deleteMeal:(Meal*)meal {
    DeleteMealRequest * request
    = [[DeleteMealRequest alloc] initWithMealId:meal.serverMealId
                                          token:[AppContext sharedInstance].authToken];
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
       
        [Meal deleteMeal:meal];
        [_display didDeletedMeal:meal];
    }];
}

- (void)deleteDiet:(Diet*)diet {
    DeleteDietRequest * request
    = [[DeleteDietRequest alloc] initWithDietId:diet.serverDietId
                                          token:[AppContext sharedInstance].authToken];
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [Diet deleteDiet:diet];
        [_display didDeleteDiet:diet];
    }];
}

- (void)deleteAliment:(Aliment*)aliment {
    BaseRequest * request;
    if (aliment.isSupplement != nil &&
        aliment.isSupplement.boolValue) {
        
        request = [[DeleteSupplementRequest alloc] initWithSupplementId:aliment.supplementId
                                                                  token:[AppContext sharedInstance].authToken];
    }
    else {
        request = [[DeleteAlimentRequest alloc] initWithAlimentId:aliment.alimentId
                                                            token:[AppContext sharedInstance].authToken];
    }
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
    
        [Aliment deleteAliment:aliment];
        [_display didDeleteAliment:aliment];
    }];
}

- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2 {
    SendContentRequest * request = [[SendContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                       token2:token2
                                                                    rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadItems];
    }];
}

- (void)shareContent:(RLMObject*)content {
    ShareContentRequest * request = [[ShareContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                      rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadItems];
    }];
}

- (void)addNutritionPlan:(NutritionPlan*)nutritionPlan
                  toDate:(NSDate*)date {
    
    //
    // Check the content for date
    //
    
    NSString * checkResult = [Schedule checkContentForDate:date];
    if (checkResult) {
        [_display showMessage:checkResult];
        return;
    }
    
    //
    // Register nutrition plan
    //

    if ([Schedule addNutritionPlan:nutritionPlan
                            toDate:date]) {
        
        [_display showMessage:@"plan de nutrición se registra con éxito."];
    }
}

@end
