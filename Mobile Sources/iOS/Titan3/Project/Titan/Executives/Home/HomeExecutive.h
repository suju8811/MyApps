//
//  HomeExecutive.h
//  Titan
//
//  Created by Marcus Lee on 18/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseExecutive.h"
#import "SchedulePlan.h"

@protocol HomeDisplay <BaseDisplay>

- (void)presentTrainingPlanDetailView:(SchedulePlan*)trainingPlan;

- (void)presentNutritionPlanDetailView:(SchedulePlan*)schedulePlan;

- (void)presentTrainingDetailView:(Training*)training ofPlan:(TrainingPlan*)trainingPlan;

- (void)presentReposeView:(TrainingPlan*)repose;

- (void)reloadHomeView;

@end

@interface HomeExecutive : NSObject

- (id)initWithDisplay:(id<HomeDisplay>)display;

- (void)didPressTrainingPlanOfFitnessCell:(NSDate*)date;

- (void)saveTrainingPlan:(TrainingPlan*)trainingPlan
                  toDate:(NSDate*)date;

- (void)didPressNutritionPlanOfFitnessCell:(NSDate*)date;

- (void)saveNutritionPlan:(NutritionPlan*)nutritionPlan
                   toDate:(NSDate*)date;

- (void)didPressShowTrainingOfFitnessCell:(NSDate*)date;

@end
