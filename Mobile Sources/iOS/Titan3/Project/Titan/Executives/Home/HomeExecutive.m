//
//  HomeExecutive.m
//  Titan
//
//  Created by Marcus Lee on 18/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "HomeExecutive.h"
#import "Schedule.h"

@interface HomeExecutive()

@property id<HomeDisplay> display;

@end

@implementation HomeExecutive

- (id)initWithDisplay:(id<HomeDisplay>)display {
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)didPressTrainingPlanOfFitnessCell:(NSDate*)dateToCheck {
    NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:dateToCheck options:0];
    SchedulePlan * scheuldePlan = [SchedulePlan getScheduleTrainingPlanWithScheduleDate:date];
    if (scheuldePlan == nil ||
        scheuldePlan.plan == nil) {
        
        [_display showMessage:@"no hay plan de entrenamiento para este día"];
        return;
    }
     
    [_display presentTrainingPlanDetailView:scheuldePlan];
}

- (void)saveTrainingPlan:(TrainingPlan*)trainingPlan
                  toDate:(NSDate*)date {
    
    if ([Schedule addTrainingPlan:trainingPlan
                           toDate:date]) {
        
        [_display reloadHomeView];
    }
}

- (void)didPressNutritionPlanOfFitnessCell:(NSDate*)dateToCheck {
    NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:dateToCheck options:0];
    SchedulePlan * scheuldePlan = [SchedulePlan getScheduleNutritionPlanWithScheduleDate:date];
    if (scheuldePlan == nil ||
        scheuldePlan.nutritionPlan == nil) {
        
        [_display showMessage:@"no hay un plan de nutrición para este día"];
        return;
    }
    
    [_display presentNutritionPlanDetailView:scheuldePlan];
}

- (void)saveNutritionPlan:(NutritionPlan*)nutritionPlan
                   toDate:(NSDate*)date {
    
    if ([Schedule addNutritionPlan:nutritionPlan
                            toDate:date]) {
        
        [_display reloadHomeView];
    }
}

- (void)didPressShowTrainingOfFitnessCell:(NSDate *)dateToCheck {
    NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:dateToCheck options:0];
    
    //
    // Manage training plan
    //
    
    TrainingPlan * trainingPlan = [Schedule trainingPlanForDate:date];
    TrainingPlan * repose = [Schedule reposePlanForDate:date];
    
    //
    // Get training plan or repose for date
    //
    
    TrainingPlan * considerTrainingPlan;
    if (trainingPlan) {
        considerTrainingPlan = trainingPlan;
    }
    else if (repose) {
        considerTrainingPlan = repose;
    }
    
    //
    // Go to repose
    //
    
    if (considerTrainingPlan.isRepose.boolValue) {
        [_display presentReposeView:considerTrainingPlan];
        return;
    }
    
    //
    // Go to training
    //
    
    Training * training = [[considerTrainingPlan.trainingDay firstObject].trainingDayContent firstObject].training;
    if (training) {
        [_display presentTrainingDetailView:training ofPlan:considerTrainingPlan];
    }
}

@end
