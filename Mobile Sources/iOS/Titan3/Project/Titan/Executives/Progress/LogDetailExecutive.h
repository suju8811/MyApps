//
//  LogDetailExecutive.h
//  Titan
//
//  Created by Marcus Lee on 29/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseExecutive.h"
#import "Log.h"

@protocol LogDetailDisplay <BaseDisplay>

- (Log*)logFromDisplay;
- (void)dismissWithLog:(Log*)log;

@end

@interface LogDetailExecutive : NSObject

- (id)initWithDisplay:(id<LogDetailDisplay>)display;
- (void)didPressOKButton;

@end
