//
//  SiluetStressDetailExecutive.h
//  Titan
//
//  Created by Marcus Lee on 24/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"

typedef enum : NSUInteger {
    SiluetStressImageTypeFront      = 0,
    SiluetStressImageTypeBack       = 1
} SiluetStressImageType;

@protocol SiluetStressImageDisplay <BaseDisplay>

- (void)gotoSiluetStressDetailViewController:(NSArray*)exerciseArray
                                   imageType:(SiluetStressImageType)imageType
                                       index:(NSInteger)index
                                    bodyPart:(NSString*)bodyPart;

@end

@interface SiluetStressImageExecutive : NSObject

- (id)initWithDisplay:(id<SiluetStressImageDisplay>)display
            imageType:(SiluetStressImageType)imageType;

- (void)didPressBodyPart:(SiluetStressImageType)imageType
                   index:(NSInteger)index;

@end
