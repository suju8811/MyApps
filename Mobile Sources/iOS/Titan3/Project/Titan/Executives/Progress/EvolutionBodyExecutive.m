//
//  EvolutionBodyExecutive.m
//  Titan
//
//  Created by Marcus Lee on 3/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "EvolutionBodyExecutive.h"
#import "DateTimeUtil.h"
#import "SchedulePlan.h"
#import <PNColor.h>

@implementation EvolutionBodyDisplayContext

- (id)init {
    self = [super init];
    
    _maxLogDate = [Log getMaxValueWithKey:@"revisionDate"];
    _minLogDate = [Log getMinValueWithKey:@"revisionDate"];
    
    if (!self.startDate) {
        self.startDate = _minLogDate.revisionDate;
    }
    
    if (!self.endDate) {
        self.endDate = _maxLogDate.revisionDate;
    }
    
    return self;
}

@end

@interface EvolutionBodyExecutive()

@end

@implementation EvolutionBodyExecutive

- (void)displayDidLoad {
    self.displayContext = [[EvolutionBodyDisplayContext alloc] init];
    [self.display configureDisplay];
    [self reloadDisplay];
}

- (void)reloadDisplay {
    [self loadDisplayData];
    [self.display relocateControls:self.displayContext];
    [self.display loadDisplay:self.displayContext];
    [self drawChart];
}

- (BOOL)didPressDatePicker:(NSString*)accessibilityLabel {
    if ([super didPressDatePicker:accessibilityLabel]) {
        return YES;
    }
    
    if (self.displayContext.calendarIndex == EvolutionCalendarTypeDate) {
        Log *maxLogDate = [Log getMaxValueWithKey:@"revisionDate"];
        Log *minLogDate = [Log getMinValueWithKey:@"revisionDate"];
        
        NSDate *maxDate = maxLogDate.revisionDate;
        NSDate *minDate = minLogDate.revisionDate;
        
        if (!maxDate) {
            maxDate = [NSDate date];
        }
        if (!minDate) {
            minDate = [NSDate date];
        }
        
        if (!self.displayContext.startDate) {
            self.displayContext.startDate = minDate;
        }
        if (!self.displayContext.endDate) {
            self.displayContext.endDate = maxDate;
        }
        
        NSDate * selectedDate;
        if ([accessibilityLabel isEqualToString:@"startDate"]) {
            selectedDate = self.displayContext.startDate;
        }
        else {
            selectedDate = self.displayContext.endDate;
        }
        
        [self.display presentDatePicker:accessibilityLabel
                           selectedDate:selectedDate
                                maxDate:maxDate
                                minDate:minDate];
        
    }
    
    return YES;
}

- (void)didPressAccpetDate:(NSString*)accessibilityLabel
              selectedDate:(NSDate*)selectedDate {
    
    if ([accessibilityLabel isEqualToString:@"startDate"]) {
        self.displayContext.startDate = selectedDate;
    }
    else {
        self.displayContext.endDate = selectedDate;
    }
    
    if ([self.displayContext.startDate compare:self.displayContext.endDate] == NSOrderedDescending) {
        self.displayContext.endDate = self.displayContext.startDate;
    }
    
    NSString * dateString;
    if ([accessibilityLabel isEqualToString:@"startDate"]) {
        dateString = [DateTimeUtil stringFromDate:self.displayContext.startDate format:@"yyyy-MM-dd"];
    }
    else {
        dateString = [DateTimeUtil stringFromDate:self.displayContext.endDate format:@"yyyy-MM-dd"];
    }
    
    [self.display acceptDatePicker:accessibilityLabel dateString:dateString];
    [self reloadDisplay];
}

- (void)didPressComparePicture {
    EvolutionBodyDisplayContext * displayContext = (EvolutionBodyDisplayContext*)self.displayContext;
    if (displayContext &&
        displayContext.maxLogDate) {
        
        [self.display presentComparePictureView:displayContext.minLogDate
                                         maxLog:displayContext.maxLogDate];
        
        return;
    }
    
    [self.display showMessage:@"Debe existir un log inicial y otro final"];
}

- (void)didPressCompareValue:(NSString*)identifierString {
    if ([identifierString isEqualToString:@"maxWeight"]) {
        NSNumber *maxWeight = [self.displayContext.logs maxOfProperty:@"weight"];
        self.displayContext.focusedLogs = [Log getLogsWithProperty:@"weight" value:maxWeight];
    }
    else if ([identifierString isEqualToString:@"minWeight"]) {
        NSNumber *minWeight = [self.displayContext.logs minOfProperty:@"weight"];
        self.displayContext.focusedLogs = [Log getLogsWithProperty:@"weight" value:minWeight];
    }
    
    [self drawChart];
}

- (void)didPressCalendarIndex:(NSInteger)calendarIndex {
    if (self.displayContext.calendarIndex == calendarIndex) {
        return;
    }
    
    self.displayContext.calendarIndex = calendarIndex;
    
    //
    // There is not dots tab when the user selects "Fecha".
    // So it selects first selection "peso" when the user selects "Fecha" and current selection is "Dots"
    //
    
    if (calendarIndex == EvolutionCalendarTypeDate &&
        self.displayContext.chartSelectionIndex == EvolutionChartDataDots) {
        
        self.displayContext.chartSelectionIndex = EvolutionChartDataWeight;
    }
    
    //
    // Load display
    //
    
    [self reloadDisplay];
}

- (void)didPressDotOfChart:(NSInteger)pointIndex {
    if (self.displayContext.chartSelectionIndex != EvolutionChartDataDots) {
        return;
    }
    
    if (self.displayContext.calendarIndex != EvolutionCalendarTypePlans) {
        Log * log = self.displayContext.logs[pointIndex];
        [self.display presentLogDetailViewController:log];
    }
    else {
        SchedulePlan * schedulePlan = self.displayContext.plans[pointIndex];
        if (schedulePlan.plan) {
            [self.display presentPlanDetailViewController:schedulePlan.plan];
        }
        else {
            [self.display presentPlanDetailViewController:schedulePlan.nutritionPlan];
        }
    }
}

- (void)loadDisplayData {
    
    //
    // Get log data
    //
    
    if (self.displayContext.calendarIndex == EvolutionCalendarTypeDate) {
        self.displayContext.logs = [Log getLogsBetweenDates:self.displayContext.startDate
                                             andEndDate:self.displayContext.endDate];
        
    }
    else {
        if (self.displayContext.startDateLog && self.displayContext.endDateLog) {
            self.displayContext.logs = [Log getLogsBetweenDates:self.displayContext.startDateLog.revisionDate
                                             andEndDate:self.displayContext.endDateLog.revisionDate];
        }
        else {
            self.displayContext.logs = nil;
        }
    }
    
    EvolutionBodyDisplayContext * displayContext = (EvolutionBodyDisplayContext*)self.displayContext;
    if (self.displayContext.logs.count > 0) {
        displayContext.maxLogDate = [self.displayContext.logs lastObject];
        displayContext.minLogDate = [self.displayContext.logs firstObject];
    }
    else {
        displayContext.maxLogDate = nil;
        displayContext.minLogDate = nil;
    }
    
    //
    // Get plans
    //
    
    if (self.displayContext.startDatePlan && self.displayContext.endDatePlan) {
        if (self.displayContext.segmentState == EvolutionChartTabTrainingPlan) {
            self.displayContext.plans = [SchedulePlan getScheduleTrainingPlanBetweenDate:self.displayContext.startDatePlan.scheduleDate
                                                                             andDate:self.displayContext.endDatePlan.scheduleDate];
        }
        else {
            self.displayContext.plans = [SchedulePlan getScheduleNutritionPlanBetweenDate:self.displayContext.startDatePlan.scheduleDate
                                                                              andDate:self.displayContext.endDatePlan.scheduleDate];
            
        }
    }
    else {
        self.displayContext.plans = nil;
    }
}

- (UIColor*)chartColor {
    switch (self.displayContext.chartSelectionIndex) {
        case EvolutionChartDataWeight:
            return PNFreshGreen;
        case EvolutionChartDataIMG:
            return PNBlue;
        case EvolutionChartDataTMB:
            return PNTwitterColor;
        case EvolutionChartDataKCal:
            return PNLightGreen;
            break;
        case EvolutionChartDataDots:
            return PNLightBlue;
    }
    
    return PNWhite;
}

- (void)drawChart {
    
    NSMutableArray * xLabels = [[NSMutableArray alloc] init];
    NSArray * yLabels;
    
    if (self.displayContext.calendarIndex == EvolutionCalendarTypePlans) {
        for(SchedulePlan * schedulePlan in self.displayContext.plans) {
            [xLabels addObject:[DateTimeUtil stringFromDate:schedulePlan.scheduleDate format:@"MM-dd"]];
        }
        
        yLabels = [self yLabelsForPlans];
    }
    else  {
        for(Log * log in self.displayContext.logs) {
            [xLabels addObject:[DateTimeUtil stringFromDate:log.revisionDate format:@"MM-dd"]];
        }
        
        yLabels = [self yLabelsForLogs:self.displayContext.logs];
    }
    
    NSMutableArray * focusedIndices = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index < self.displayContext.logs.count; index++) {
        Log * iterateLog = self.displayContext.logs[index];
        for (Log * focusedLog in self.displayContext.focusedLogs) {
            if ([iterateLog.logId isEqualToString:focusedLog.logId]) {
                [focusedIndices addObject:@(index)];
                break;
            }
        }
    }
    
    [self.display drawChart:xLabels
         selectionIndex:self.displayContext.chartSelectionIndex
                  color:[self chartColor]
                yValues:yLabels
         focusedIndices:focusedIndices];
}

- (NSArray*)yLabelsForPlans {
    NSMutableArray * yLabels = [[NSMutableArray alloc] init];
    
    switch (self.displayContext.chartSelectionIndex) {
        case EvolutionChartDataWeight: {
            for (SchedulePlan * schedulePlan in self.displayContext.plans) {
                [yLabels addObject:@5];
            }
        }
            break;
        case EvolutionChartDataIMG: {
            for (SchedulePlan * schedulePlan in self.displayContext.plans) {
                [yLabels addObject:@5];
            }
        }
            break;
        case EvolutionChartDataTMB: {
            for (SchedulePlan * schedulePlan in self.displayContext.plans) {
                [yLabels addObject:@5];
            }
        }
            break;
        case EvolutionChartDataKCal: {
            for (SchedulePlan * schedulePlan in self.displayContext.plans) {
                [yLabels addObject:@5];
            }
        }
            break;
        case EvolutionChartDataDots: {
            for (SchedulePlan * schedulePlan in self.displayContext.plans) {
                [yLabels addObject:@5];
            }
        }
            break;
            
        default:
            break;
    }
    
    return yLabels;
}

- (NSArray*)yLabelsForLogs:(RLMResults*)logs {
    NSMutableArray * yLabels = [[NSMutableArray alloc] init];
    
    switch (self.displayContext.chartSelectionIndex) {
        case EvolutionChartDataWeight: {
            for (Log * log in logs) {
                [yLabels addObject:log.weight];
            }
        }
            break;
        case EvolutionChartDataIMG: {
            for (Log * log in logs) {
                [yLabels addObject:log.igc];
            }
        }
            break;
        case EvolutionChartDataTMB: {
            for (Log * log in logs) {
                [yLabels addObject:log.rm];
            }
        }
            break;
        case EvolutionChartDataKCal: {
            for (Log * log in logs) {
                [yLabels addObject:log.bmr];
            }
        }
            break;
        case EvolutionChartDataDots: {
            if (logs) {
                for (NSInteger index = 0; index < logs.count; index++) {
                    [yLabels addObject:@10];
                }
            }
        }
            break;
            
        default:
            break;
    }
    
    return yLabels;
}

@end
