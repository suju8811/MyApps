//
//  TrainingExercisesTableExecutive.m
//  Titan
//
//  Created by Marcus Lee on 9/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "TrainingExercisesTableExecutive.h"
#import "Schedule.h"
#import "SchedulePlan.h"
#import "DateTimeUtil.h"
#import "GetProgress2Request.h"
#import "AppContext.h"

@interface TrainingExercisesTableExecutive()

@property id<TrainingExercisesTableDisplay> display;

@end

@implementation TrainingExercisesTableExecutive

- (id)initWithDisplay:(id<TrainingExercisesTableDisplay>)display
            startDate:(NSDate*)startDate
              endDate:(NSDate*)endDate {
    
    self = [super init];
    
    _display = display;
    _startDate = startDate;
    _endDate = endDate;
    
    return self;
}

- (void)displayDidLoad {
    
    GetProgress2Request * request = [[GetProgress2Request alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                         date1:_startDate
                                                                         date2:_endDate];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        NSArray * exerciseArray = [[responseArray firstObject] valueForKey:@"ejercicios"];
        if (!exerciseArray || exerciseArray.count <= 0) {
            [_display showMessage:@"No hay ejercicio."];
            return;
        }
    }];
    
//    NSMutableArray * trainingExercises = [[NSMutableArray alloc] init];
//    NSMutableArray * exerciseDates = [[NSMutableArray alloc] init];
//    NSInteger index;
//
//    NSArray * trainingSchedules = [Schedule trainingSchedulesBetweenDate:_startDate date:_endDate];
//    for (SchedulePlan * schedulePlan in trainingSchedules) {
//        index = 0;
//        for (TrainingDay * trainingDay in schedulePlan.plan.trainingDay) {
//            NSDate * exerciseDate = [DateTimeUtil addDays:index toDate:schedulePlan.scheduleDate];
//            index ++;
//            for (TrainingDayContent * trainingDayContent in trainingDay.trainingDayContent) {
//                for (TrainingExercise * trainingExercise in trainingDayContent.training.trainingExercises) {
//                    [trainingExercises addObject:trainingExercise];
//                    [exerciseDates addObject:exerciseDate];
//                }
//            }
//        }
//    }
//
//    [_display reloadTable:trainingExercises
//            exerciseDates:exerciseDates];
    
}

@end
