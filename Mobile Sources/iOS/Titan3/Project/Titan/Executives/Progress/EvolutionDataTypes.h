//
//  EvolutionDataTypes.h
//  Titan
//
//  Created by Marcus Lee on 7/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#ifndef EvolutionDataTypes_h
#define EvolutionDataTypes_h

typedef enum : NSUInteger {
    EvolutionChartTabTrainingPlan       = 0,
    EvolutionChartTabNutritionPlan      = 1
} EvolutionChartTab;

typedef enum : NSUInteger {
    EvolutionChartDataWeight    = 0,
    EvolutionChartDataIMG       = 1,
    EvolutionChartDataTMB       = 2,
    EvolutionChartDataKCal      = 3,
    EvolutionChartDataDots      = 4
} EvolutionChartData;

typedef enum : NSUInteger {
    EvolutionCalendarTypeDate   = 0,
    EvolutionCalendarTypeLogs   = 1,
    EvolutionCalendarTypePlans  = 2
} EvolutionCalendarType;

#endif /* EvolutionDataTypes_h */
