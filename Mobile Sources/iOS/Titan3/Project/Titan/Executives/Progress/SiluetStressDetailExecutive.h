//
//  SiluetStressDetailExecutive.h
//  Titan
//
//  Created by Marcus Lee on 25/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "BaseExecutive.h"
#import "SiluetStressImageExecutive.h"
#import "DivisionPercentage.h"

@interface SiluetStressDetailDisplayContext : NSObject

@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * divisionPercentage;
@property (nonatomic, strong) NSString * difficulty;
@property (nonatomic, strong) NSString * frequency;
@property (nonatomic, strong) NSString * basic;
@property (nonatomic, strong) NSString * specific;
@property (nonatomic, strong) NSString * series;

@end

@protocol SiluetStressDetailDisplay <BaseDisplay>

- (void)loadDisplay:(SiluetStressDetailDisplayContext*)displayContext;

@end

@interface SiluetStressDetailExecutive : NSObject

- (id)initWithDisplay:(id<SiluetStressDetailDisplay>)display
                title:(NSString*)title
            imageType:(SiluetStressImageType)imageType
           imageIndex:(NSInteger)index
            exercises:(NSArray*)exercises
   divisionPercentage:(DivisionPercentage*)divisionPercentage;

- (void)displayDidLoad;

@end
