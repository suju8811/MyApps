//
//  ProgressExecutive.m
//  Titan
//
//  Created by Marcus Lee on 28/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ProgressExecutive.h"
#import "CreateLogRequest.h"
#import "DeleteLogRequest.h"
#import "AppContext.h"
#import "Progress.h"

@interface ProgressExecutive()

@property id<ProgressDisplay> display;

@end

@implementation ProgressExecutive

- (id)initWithDisplay:(id<ProgressDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)didPressFooterIndex:(NSInteger)index {
    ProgressSelectType progressSelectType = ProgressSelectTypeStatistix;
    switch (index) {
        case 0:
            progressSelectType = ProgressSelectTypeStatistix;
            break;
        case 1:
            progressSelectType = ProgressSelectTypeLogs;
            break;
        case 2:
            progressSelectType = ProgressSelectTypeRecords;
            break;
        default:
            break;
    }
    
    [_display reloadProgressView:progressSelectType];
}

- (void)createLog:(Log*)log image:(UIImage*)image {
    CreateLogRequest * request = [[CreateLogRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                     log:log
                                                                   image:image];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        if (error != nil) {
            [_display hideLoading];
            [_display showMessage:@"Error al calificar la aplicación"];
            return;
        }
        
        [Progress getLogsWithCompletionBlock:^(BOOL isSuccess) {
            [_display hideLoading];
            [_display reloadLogsTable];
        }];
    }];
}

- (void)updateLog:(Log*)log image:(UIImage*)image {
}

- (void)deleteLog:(Log *)log {
    DeleteLogRequest * request = [[DeleteLogRequest alloc] initWithLogId:log.logId
                                                                   token:[AppContext sharedInstance].authToken];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            [_display showMessage:@"Error al calificar la aplicación"];
            return;
        }
        
        [Log deleteLog:log];
        [_display reloadLogsTable];
    }];
    
}

@end
