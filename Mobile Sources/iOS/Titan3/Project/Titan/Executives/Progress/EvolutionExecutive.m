//
//  EvolutionExecutive.m
//  Titan
//
//  Created by Marcus Lee on 7/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "EvolutionExecutive.h"

@implementation EvolutionDisplayContext

- (id)init {
    self = [super init];
    
    _segmentState = 0;
    _logs = [Log getLogs];
    
    _segmentState = 0;
    _calendarIndex = 0;
    _chartSelectionIndex = 0;
    
    return self;
}

@end

@implementation EvolutionExecutive

- (id)initWithDisplay:(id<EvolutionDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)displayDidLoad {
}

- (BOOL)didPressDatePicker:(NSString*)accessibilityLabel {
    if (self.displayContext.calendarIndex == EvolutionCalendarTypeLogs) {
        if ([accessibilityLabel isEqualToString:@"startDate"]) {
            [self.display presentLogsSelectorViewController:self.displayContext.startDateLog
                                              accessibility:accessibilityLabel];
            
        }
        else {
            [self.display presentLogsSelectorViewController:self.displayContext.endDateLog
                                              accessibility:accessibilityLabel];
            
        }
        
        return YES;
    }
    else if (self.displayContext.calendarIndex == EvolutionCalendarTypePlans) {
        if ([accessibilityLabel isEqualToString:@"startDate"]) {
            [self.display presentPlanSelectorViewController:self.displayContext.startDatePlan
                                               trainingPlan:self.displayContext.segmentState == EvolutionChartTabTrainingPlan
                                              accessibility:accessibilityLabel];
            
        }
        else {
            [self.display presentPlanSelectorViewController:self.displayContext.endDatePlan
                                               trainingPlan:self.displayContext.segmentState == EvolutionChartTabTrainingPlan
                                              accessibility:accessibilityLabel];
            
        }
        
        return YES;
    }
    
    return NO;
}

- (void)didPressCalendarIndex:(NSInteger)calendarIndex {
}

- (void)didPressDotOfChart:(NSInteger)pointIndex {    
}

- (void)reloadDisplay {    
}

- (void)didPressAccpetDate:(NSString*)accessibilityLabel
              selectedDate:(NSDate*)selectedDate {    
}

- (void)didSelectLog:(Log*)log
       accessibility:(NSString*)accessibilityLabel {
    
    if ([accessibilityLabel isEqualToString:@"startDate"]) {
        if (self.displayContext.endDateLog &&
            self.displayContext.endDateLog.revisionDate < log.revisionDate) {
            
            [self.display showMessage:@"Seleccione registro anterior que registro final"];
            return;
        }
        
        self.displayContext.startDateLog = log;
    }
    else {
        if (self.displayContext.startDateLog &&
            self.displayContext.startDateLog.revisionDate > log.revisionDate) {
            
            [self.display showMessage:@"Seleccione registro posterior que registro inicial"];
            return;
        }
        
        self.displayContext.endDateLog = log;
    }
    
    [self reloadDisplay];
}

- (void)didSelectPlan:(SchedulePlan*)schedulePlan
        accessibility:(NSString*)accessibilityLabel {
    
    if ([accessibilityLabel isEqualToString:@"startDate"]) {
        if (self.displayContext.endDatePlan &&
            self.displayContext.endDatePlan.scheduleDate < schedulePlan.scheduleDate) {
            
            [self.display showMessage:@"Por favor seleccione el plan anterior que el plan final"];
            return;
        }
        
        self.displayContext.startDatePlan = schedulePlan;
    }
    else {
        if (self.displayContext.startDatePlan &&
            self.displayContext.startDatePlan.scheduleDate > schedulePlan.scheduleDate) {
            
            [self.display showMessage:@"Seleccione un plan posterior al plan inicial"];
            return;
        }
        
        self.displayContext.endDatePlan = schedulePlan;
    }
    
    [self reloadDisplay];
}

- (void)didPressSegment:(NSInteger)segmentState {
    self.displayContext.segmentState = segmentState;
    [self reloadDisplay];
}

- (void)didPressChartSelectionButton:(NSInteger)chartSelectionIndex {
    if (self.displayContext.chartSelectionIndex == chartSelectionIndex) {
        return;
    }
    
    self.displayContext.chartSelectionIndex = chartSelectionIndex;
    [self reloadDisplay];
}

@end
