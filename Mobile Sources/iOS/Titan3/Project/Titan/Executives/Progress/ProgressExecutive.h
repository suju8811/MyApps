//
//  ProgressExecutive.h
//  Titan
//
//  Created by Marcus Lee on 28/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseExecutive.h"
#import "Log.h"
#import <UIKit/UIKit.h>

typedef enum {
    ProgressSelectTypeStatistix = 0,
    ProgressSelectTypeLogs,
    ProgressSelectTypeRecords,
} ProgressSelectType;

@protocol ProgressDisplay <BaseDisplay>

- (void)hideRightNavigationButton:(BOOL)isHide;
- (void)reloadProgressView:(ProgressSelectType)selectType;
- (void)reloadLogsTable;

@end

@interface ProgressExecutive : NSObject

- (id)initWithDisplay:(id<ProgressDisplay>)display;
- (void)didPressFooterIndex:(NSInteger)index;

- (void)createLog:(Log*)log image:(UIImage*)image;
- (void)updateLog:(Log*)log image:(UIImage*)image;
- (void)deleteLog:(Log*)log;

@end
