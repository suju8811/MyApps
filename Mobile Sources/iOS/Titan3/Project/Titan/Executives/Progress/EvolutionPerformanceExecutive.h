//
//  EvolutionPerformanceExecutive.h
//  Titan
//
//  Created by Marcus Lee on 7/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "EvolutionExecutive.h"
#import <UIKit/UIKit.h>
#import "EvolutionDataTypes.h"
#import "TrainingExercise.h"

@interface EvolutionPerformanceDisplayContext : EvolutionDisplayContext

@property (nonatomic, strong) SchedulePlan * minTrainingSchedule;
@property (nonatomic, strong) SchedulePlan * maxTrainingSchedule;

@property (nonatomic, strong) SchedulePlan *endTrainingSchedule;
@property (nonatomic, strong) SchedulePlan *startTrainingSchedule;

@property (nonatomic, strong) RLMResults * trainingSchedules;
@property (nonatomic, strong) TrainingExercise * trainingExercise;

@end

@protocol EvolutionPerformanceDisplay <EvolutionDisplay>

- (void)presentTrainingExerciseSelectorView:(EvolutionPerformanceDisplayContext*)displayContext;

@end

@interface EvolutionPerformanceExecutive : EvolutionExecutive

- (void)didPressComparePicture;
- (void)didPressCompareValue:(NSString*)identifierString;

- (void)didSelectTrainingExercise:(TrainingExercise*)trainingExercise;

@end
