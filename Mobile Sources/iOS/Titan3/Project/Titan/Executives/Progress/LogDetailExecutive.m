//
//  LogDetailExecutive.m
//  Titan
//
//  Created by Marcus Lee on 29/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "LogDetailExecutive.h"

@interface LogDetailExecutive()

@property id<LogDetailDisplay> display;

@end

@implementation LogDetailExecutive

- (id)initWithDisplay:(id<LogDetailDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)didPressOKButton {
    Log * log = [_display logFromDisplay];
    [_display dismissWithLog:log];
}

@end
