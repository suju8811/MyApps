//
//  SiluetStressDetailExecutive.m
//  Titan
//
//  Created by Marcus Lee on 24/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "SiluetStressImageExecutive.h"
#import "GetProgress3Request.h"
#import "AppContext.h"
#import "JSONKit.h"
#import "TrainingExercise.h"

@interface SiluetStressImageExecutive()

@property id<SiluetStressImageDisplay> display;
@property (nonatomic, assign) SiluetStressImageType imageType;

@end

@implementation SiluetStressImageExecutive

- (id)initWithDisplay:(id<SiluetStressImageDisplay>)display
            imageType:(SiluetStressImageType)imageType {
    
    self = [super init];
    
    _display = display;
    _imageType = imageType;
    
    return self;
}

- (void)didPressBodyPart:(SiluetStressImageType)imageType
                   index:(NSInteger)index {
    
    if (index == -1) {
        return;
    }
    
    NSString * bodyPart;
    if (imageType == SiluetStressImageTypeFront) {
        switch (index) {
            case 0:
                bodyPart = @"Biceps";
                break;
            case 1:
                bodyPart = @"Cuadriceps";
                break;
            case 2:
                bodyPart = @"Pecho";
                break;
            case 3:
                bodyPart = @"Triceps";
                break;
            default:
                break;
        }
    }
    else {
        switch (index) {
            case 0:
                bodyPart = @"Antebrazo";
                break;
            case 1:
                bodyPart = @"Espalada";
                break;
            case 2:
                bodyPart = @"Femoral";
                break;
            case 3:
                bodyPart = @"Gemelos";
                break;
            case 4:
                bodyPart = @"Gluteos";
                break;
            case 5:
                bodyPart = @"Hombros";
                break;
            case 6:
                bodyPart = @"Lumbar";
                break;
            case 7:
                bodyPart = @"Trapecio";
                break;
                
            default:
                break;
        }
    }
    
    GetProgress3Request * request = [[GetProgress3Request alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                      bodyPart:bodyPart];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        NSArray * exercisesDictionary = [[responseArray firstObject] valueForKey:@"ejercicios"];
        NSMutableArray * exercises = [[NSMutableArray alloc] init];
        for (NSDictionary * exerciseDictionary in exercisesDictionary) {
            TrainingExercise * trainingExercise = [[TrainingExercise alloc] init];
            [trainingExercise parseFromDictionary:exerciseDictionary];
            [exercises addObject:trainingExercise];
        }
        
        [_display gotoSiluetStressDetailViewController:exercises
                                             imageType:imageType
                                                 index:index
                                              bodyPart:bodyPart];
        
    }];
    
}

@end
