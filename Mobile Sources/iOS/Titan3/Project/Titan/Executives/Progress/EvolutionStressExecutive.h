//
//  EvolutionStressExecutive.h
//  Titan
//
//  Created by Marcus Lee on 9/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "EvolutionExecutive.h"
#import "Log.h"
#import "SchedulePlan.h"
#import <UIKit/UIKit.h>
#import "EvolutionDataTypes.h"
#import "DivisionPercentage.h"

@interface EvolutionStressDisplayContext : EvolutionDisplayContext

@property (nonatomic, strong) Log *minLogDate;
@property (nonatomic, strong) Log *maxLogDate;

@end

@protocol EvolutionStressDisplay <EvolutionDisplay>

- (void)didLoadDivisionPercentage:(DivisionPercentage*)divisionPercentage;

@end

@interface EvolutionStressExecutive : EvolutionExecutive

- (void)didPressComparePicture;
- (void)didPressCompareValue:(NSString*)identifierString;
- (void)loadDivisionPercentage;

@end
