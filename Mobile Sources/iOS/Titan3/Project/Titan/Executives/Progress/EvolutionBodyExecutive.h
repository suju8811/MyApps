//
//  EvolutionBodyExecutive.h
//  Titan
//
//  Created by Marcus Lee on 3/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "EvolutionExecutive.h"
#import "Log.h"
#import "SchedulePlan.h"
#import <UIKit/UIKit.h>
#import "EvolutionDataTypes.h"

@interface EvolutionBodyDisplayContext : EvolutionDisplayContext

@property (nonatomic, strong) Log *minLogDate;
@property (nonatomic, strong) Log *maxLogDate;

@end

@protocol EvolutionBodyDisplay <EvolutionDisplay>

@end

@interface EvolutionBodyExecutive : EvolutionExecutive

- (void)didPressComparePicture;
- (void)didPressCompareValue:(NSString*)identifierString;

@end
