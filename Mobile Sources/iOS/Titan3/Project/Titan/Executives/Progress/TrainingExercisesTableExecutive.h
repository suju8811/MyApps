//
//  TrainingExercisesTableExecutive.h
//  Titan
//
//  Created by Marcus Lee on 9/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "BaseExecutive.h"

@protocol TrainingExercisesTableDisplay <BaseDisplay>

- (void)reloadTable:(NSArray*)trainingExercises
      exerciseDates:(NSArray*)exerciseDates;

@end

@interface TrainingExercisesTableExecutive : NSObject

@property (nonatomic, strong) NSDate * startDate;
@property (nonatomic, strong) NSDate * endDate;

- (id)initWithDisplay:(id<TrainingExercisesTableDisplay>)display
            startDate:(NSDate*)startDate
              endDate:(NSDate*)endDate;

- (void)displayDidLoad;

@end
