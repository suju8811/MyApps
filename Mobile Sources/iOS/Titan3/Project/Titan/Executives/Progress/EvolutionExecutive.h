//
//  EvolutionExecutive.h
//  Titan
//
//  Created by Marcus Lee on 7/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "BaseExecutive.h"
#import "Log.h"
#import "SchedulePlan.h"
#import <UIKit/UIKit.h>
#import "EvolutionDataTypes.h"

@interface EvolutionDisplayContext : NSObject

@property (nonatomic, assign) NSInteger segmentState;
@property (nonatomic, strong) RLMResults *logs;
@property (nonatomic, strong) NSArray *plans;

@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) NSDate *startDate;

@property (nonatomic, strong) Log *endDateLog;
@property (nonatomic, strong) Log *startDateLog;

@property (nonatomic, strong) SchedulePlan * endDatePlan;
@property (nonatomic, strong) SchedulePlan * startDatePlan;
@property (nonatomic, strong) RLMResults * focusedLogs;

@property (nonatomic, assign) NSInteger calendarIndex;
@property (nonatomic, assign) NSInteger chartSelectionIndex;

@end

@protocol EvolutionDisplay <BaseDisplay>

- (void)loadDisplay:(EvolutionDisplayContext*)displayContext;
- (void)relocateControls:(EvolutionDisplayContext*)displayContext;
- (void)loadDateViews:(EvolutionDisplayContext*)displayContext;
- (void)drawChart:(NSArray *)xLabels
   selectionIndex:(NSInteger)chartSelectionIndex
            color:(UIColor*)chartColor
          yValues:(NSArray*)yValues
   focusedIndices:(NSArray*)focusedIndices;

- (void)acceptDatePicker:(NSString*)accessibilityLabel
              dateString:(NSString*)dateString;

- (void)presentComparePictureView:(Log*)minLogDate
                           maxLog:(Log*)maxLogDate;

- (void)presentLogDetailViewController:(Log*)log;
- (void)presentPlanDetailViewController:(RLMObject*)plan;
- (void)presentLogsSelectorViewController:(Log*)log
                            accessibility:(NSString*)accessibilityLabel;

- (void)presentPlanSelectorViewController:(SchedulePlan*)plan
                             trainingPlan:(BOOL)trainingPlan
                            accessibility:(NSString*)accessibilityLabel;

- (void)configureDisplay;

- (void)presentDatePicker:(NSString*)accesibilityLabel
             selectedDate:(NSDate*)selectedDate
                  maxDate:(NSDate*)maxDate
                  minDate:(NSDate*)minDate;

@end

@interface EvolutionExecutive : NSObject

@property id<EvolutionDisplay> display;
@property EvolutionDisplayContext * displayContext;

- (id)initWithDisplay:(id<EvolutionDisplay>)display;
- (void)displayDidLoad;
- (void)reloadDisplay;
- (BOOL)didPressDatePicker:(NSString*)accessibilityLabel;
- (void)didPressAccpetDate:(NSString*)accessibilityLabel
              selectedDate:(NSDate*)selectedDate;

- (void)didPressCalendarIndex:(NSInteger)calendarIndex;
- (void)didPressDotOfChart:(NSInteger)pointIndex;
- (void)didSelectLog:(Log*)log
       accessibility:(NSString*)accessibilityLabel;

- (void)didSelectPlan:(SchedulePlan*)schedulePlan
        accessibility:(NSString*)accessibilityLabel;

- (void)didPressSegment:(NSInteger)segmentState;
- (void)didPressChartSelectionButton:(NSInteger)chartSelectionIndex;

@end
