//
//  SiluetStressDetailExecutive.m
//  Titan
//
//  Created by Marcus Lee on 25/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "SiluetStressDetailExecutive.h"
#import "Exercise.h"
#import "TrainingExercise.h"

@interface SiluetStressDetailExecutive()

@property id<SiluetStressDetailDisplay> display;
@property NSString * title;
@property NSArray * exercises;
@property (nonatomic, assign) NSInteger imageIndex;
@property (nonatomic, assign) SiluetStressImageType imageType;
@property (nonatomic, strong) DivisionPercentage * divisionPercentage;

@end

@implementation SiluetStressDetailExecutive

- (id)initWithDisplay:(id<SiluetStressDetailDisplay>)display
                title:(NSString*)title
            imageType:(SiluetStressImageType)imageType
           imageIndex:(NSInteger)index
            exercises:(NSArray*)exercises
   divisionPercentage:(DivisionPercentage*)divisionPercentage {

    self = [super init];
    
    _display = display;
    _title = title;
    _imageType = imageType;
    _imageIndex = index;
    _exercises = exercises;
    _divisionPercentage = divisionPercentage;
    
    return self;
}

- (void)displayDidLoad {
    SiluetStressDetailDisplayContext * displayContext = [[SiluetStressDetailDisplayContext alloc] init];
    displayContext.title = _title;
    
    //
    // Calculate division percentage
    //
    
    float divisionPercentage = 0;
    if (_imageType == SiluetStressImageTypeFront) {
        switch (_imageIndex) {
            case 0:
                divisionPercentage = _divisionPercentage.biceps;
                break;
            case 1:
                divisionPercentage = _divisionPercentage.quadriceps;
                break;
            case 2:
                divisionPercentage = _divisionPercentage.chest;
                break;
            case 3:
                divisionPercentage = _divisionPercentage.triceps;
                break;
            default:
                break;
        }
    }
    else if (_imageType == SiluetStressImageTypeBack) {
        switch (_imageIndex) {
            case 0:
                divisionPercentage = _divisionPercentage.forearm;
                break;
            case 1:
                divisionPercentage = _divisionPercentage.back;
                break;
            case 2:
                divisionPercentage = _divisionPercentage.twin;
                break;
            case 3:
                divisionPercentage = _divisionPercentage.gluteo;
                break;
            case 4:
                divisionPercentage = _divisionPercentage.shoulders;
                break;
            case 5:
                divisionPercentage = _divisionPercentage.lumbares;
                break;
            case 6:
                divisionPercentage = _divisionPercentage.trapecios;
                break;
            default:
                break;
        }
    }
    
    displayContext.divisionPercentage = [NSString stringWithFormat:@"%g%%", roundf(divisionPercentage * 100)/100.f];
    
    //
    // Calculate difficulty
    //
    
    int difficultySum = 0;
    for (Exercise * exercise in _exercises) {
        difficultySum = difficultySum + exercise.weightDifficulty.intValue;
    }
    
    int difficulty = 0;
    if (_exercises.count > 0) {
        difficulty = difficulty / _exercises.count;
    }
    
    NSString * difficultyString = @"Baja";
    if (difficulty >= 3 &&
        difficulty <= 4) {
        difficultyString = @"Media";
    }
    else if (difficulty > 4) {
        difficultyString = @"Alta";
    }
    displayContext.difficulty = difficultyString;
    
    //
    // Calculate frequency
    //
    
    if (divisionPercentage >= 0 && divisionPercentage <= 10) {
        displayContext.frequency = @"Baja";
    }
    else if (divisionPercentage >= 11 && divisionPercentage <= 41) {
        displayContext.frequency = @"Media";
    }
    else if (divisionPercentage >= 42 && divisionPercentage <= 100) {
        displayContext.frequency = @"Alta";
    }
    
    //
    // Calculate basic
    //
    
    int basicCount = 0;
    for (Exercise * exercise in _exercises) {
        if ([exercise.borl isEqualToString:@"básico"]) {
            basicCount = basicCount + 1;
        }
    }
    float basicPercentage = 0;
    if (_exercises.count > 0) {
        basicPercentage = (float)basicCount / (float)_exercises.count;
    }
    
    displayContext.basic = [NSString stringWithFormat:@"%g%%", roundf(basicPercentage * 100)/100.f];
    
    //
    // Calculate specific
    //
    
    int specificCount = 0;
    for (Exercise * exercise in _exercises) {
        if ([exercise.borl isEqualToString:@"especifico"]) {
            specificCount = specificCount + 1;
        }
    }
    float specificPercentage = 0;
    if (_exercises.count > 0) {
        specificPercentage = (float)specificPercentage / (float)_exercises.count;
    }
    
    displayContext.specific = [NSString stringWithFormat:@"%g%%", roundf(specificPercentage * 100)/100.f];
    
    //
    // Calculate series
    //
    
    NSInteger series = 0;
    for (TrainingExercise * trainingExercise in _exercises) {
        series = series + trainingExercise.series.count;
    }
    displayContext.series = [NSString stringWithFormat:@"%d", (int)series];
    
    [_display loadDisplay:displayContext];
}

@end

@implementation SiluetStressDetailDisplayContext

@end
