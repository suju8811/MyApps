//
//  NotificationSettingExecutive.m
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "NotificationSettingExecutive.h"
#import "AppSetting.h"

@interface NotificationSettingExecutive()

@property id<NotificationSettingDisplay> display;

@end

@implementation NotificationSettingExecutive

- (id)initWithDisplay:(id<NotificationSettingDisplay>)display {
    
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)displayDidLoad {
    NotificationSetting * notificationSetting = [AppSetting sharedInstance].notificationSetting;
    [_display loadDisplay:notificationSetting];
}

- (void)didPressOKButton {
    NotificationSetting * notificationSetting = [_display notificationSettingFromDisplay];
    [AppSetting sharedInstance].notificationSetting = notificationSetting;
    [_display didSaveNotificationSetting];
}

@end
