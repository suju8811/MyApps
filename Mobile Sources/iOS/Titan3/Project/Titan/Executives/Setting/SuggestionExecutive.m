//
//  SuggestionExecutive.m
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SuggestionExecutive.h"
#import "AppSetting.h"
#import "SubmitSuggestionRequest.h"
#import "AppContext.h"

@interface SuggestionExecutive()

@property id<SuggestionDisplay> display;

@end

@implementation SuggestionExecutive

- (id)initWithDisplay:(id<SuggestionDisplay>)display {
    
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)displayDidLoad {
    Suggestion * suggestion = [[Suggestion alloc] init];
    [_display loadDisplay:suggestion];
}

- (void)didPressOKButton {
    Suggestion * suggestion = [_display suggestionFromDisplay];
    if ([self checkSuggestion:suggestion] != nil) {
        [_display showMessage:[self checkSuggestion:suggestion]];
        return;
    }

    SubmitSuggestionRequest * request = [[SubmitSuggestionRequest alloc] initWithToken:[AppContext sharedInstance].authToken suggestion:suggestion];
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            [_display showMessage:@"Error al calificar la aplicación"];
            return;
        }
        
        [_display didSuggestionSubmitted];
    }];
}

- (NSString*)checkSuggestion:(Suggestion*)suggestion {
    if (suggestion.title == nil ||
        suggestion.title.length == 0) {
        return @"Por favor ingrese";
    }
    return nil;
}

@end
