//
//  RateExecutive.h
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseExecutive.h"

@protocol RateDisplay <BaseDisplay>

- (CGFloat)rateFromDisplay;
- (void)didRateApp;

@end

@interface RateExecutive : NSObject

- (id)initWithDisplay:(id<RateDisplay>)display;
- (void)didPressOKButton;

@end
