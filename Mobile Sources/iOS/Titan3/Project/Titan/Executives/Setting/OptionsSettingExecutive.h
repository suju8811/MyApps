//
//  OptionsSettingExecutive.h
//  Titan
//
//  Created by Marcus Lee on 9/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"

@protocol OptionsSettingDisplay <BaseDisplay>

@end

@interface OptionsSettingExecutive : NSObject

- (id)initWithDisplay:(id<OptionsSettingDisplay>)display;

@end
