//
//  SuggestionExecutive.h
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"
#import "Suggestion.h"

@protocol SuggestionDisplay <BaseDisplay>

- (void)loadDisplay:(Suggestion*)suggestion;
- (Suggestion*)suggestionFromDisplay;
- (void)didSuggestionSubmitted;

@end

@interface SuggestionExecutive : NSObject

- (id)initWithDisplay:(id<SuggestionDisplay>)display;
- (void)displayDidLoad;
- (void)didPressOKButton;

@end
