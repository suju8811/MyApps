//
//  ProfileExecutive.m
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ProfileExecutive.h"

@interface ProfileExecutive()

@property (nonatomic, strong) id<ProfileDisplay> display;

@end

@implementation ProfileExecutive

- (id)initWithDisplay:(id<ProfileDisplay>)display {
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)didPressSubmitButton {
    UpdateProfileRequest * request = [_display requestFromDisplay];
    if ([self checkRequest:request] != nil) {
        [_display showMessage:[self checkRequest:request]];
        return;
    }
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
            return;
        }
        
        [_display didUpdateProfile];
    }];
}

- (void)displayDidLoad {
    [_display reloadDisplay];
}

- (NSString*)checkRequest:(UpdateProfileRequest*)request {
    NSString * error = nil;
    
    if (request.photo == nil) {
        error = @"Añade una foto";
    }
    else if (request.dateOfBirth == nil) {
        error = @"Añade tu fecha de nacimiento";
    }
    else if (request.gender == nil) {
        error = @"Añade tu género";
    }
    
    return error;
}

@end
