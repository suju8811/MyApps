//
//  OptionsSettingExecutive.m
//  Titan
//
//  Created by Marcus Lee on 9/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OptionsSettingExecutive.h"

@interface OptionsSettingExecutive()

@property id<OptionsSettingDisplay> display;

@end

@implementation OptionsSettingExecutive

- (id)initWithDisplay:(id<OptionsSettingDisplay>)display {
    
    self = [super init];
    
    _display = display;
    
    return self;
}

@end
