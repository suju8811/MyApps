//
//  ProfileExecutive.h
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseExecutive.h"
#import "UpdateProfileRequest.h"

@protocol ProfileDisplay <BaseDisplay>

- (UpdateProfileRequest*)requestFromDisplay;
- (void)didUpdateProfile;
- (void)reloadDisplay;

@end

@interface ProfileExecutive : NSObject

- (id)initWithDisplay:(id<ProfileDisplay>)display;
- (void)displayDidLoad;
- (void)didPressSubmitButton;

@end
