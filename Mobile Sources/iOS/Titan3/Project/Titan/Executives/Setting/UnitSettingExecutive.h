//
//  UnitSettingExecutive.h
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"
#import "UnitSetting.h"

@protocol UnitSettingDisplay <BaseDisplay>

- (void)loadDisplay:(UnitSetting*)unitSetting;
- (UnitSetting*)unitSettingFromDisplay;
- (void)didSaveUnitSetting;

@end

@interface UnitSettingExecutive : NSObject

- (id)initWithDisplay:(id<UnitSettingDisplay>)display;
- (void)displayDidLoad;
- (void)didPressOKButton;

@end
