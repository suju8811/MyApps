//
//  NotificationSettingExecutive.h
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"
#import "NotificationSetting.h"

@protocol NotificationSettingDisplay <BaseDisplay>

- (void)loadDisplay:(NotificationSetting*)notificationSetting;
- (NotificationSetting*)notificationSettingFromDisplay;
- (void)didSaveNotificationSetting;

@end

@interface NotificationSettingExecutive : NSObject

- (id)initWithDisplay:(id<NotificationSettingDisplay>)display;
- (void)displayDidLoad;
- (void)didPressOKButton;

@end
