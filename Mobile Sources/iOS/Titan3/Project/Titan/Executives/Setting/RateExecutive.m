//
//  RateExecutive.m
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RateExecutive.h"
#import "RateAppRequest.h"
#import "AppContext.h"

@interface RateExecutive()

@property id<RateDisplay> display;

@end

@implementation RateExecutive

- (id)initWithDisplay:(id<RateDisplay>)display {
    
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)didPressOKButton {
    CGFloat rateValue = [_display rateFromDisplay];
    RateAppRequest * request = [[RateAppRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                              number:rateValue];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            [_display showMessage:@"Error al calificar la aplicación"];
            return;
        }
        
        [_display didRateApp];
    }];
}
@end
