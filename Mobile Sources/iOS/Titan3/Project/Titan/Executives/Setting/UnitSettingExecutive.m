//
//  UnitSettingExecutive.m
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "UnitSettingExecutive.h"
#import "AppSetting.h"

@interface UnitSettingExecutive()

@property id<UnitSettingDisplay> display;

@end

@implementation UnitSettingExecutive

- (id)initWithDisplay:(id<UnitSettingDisplay>)display {
    
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)displayDidLoad {
    UnitSetting * unitSetting = [AppSetting sharedInstance].unitSetting;
    [_display loadDisplay:unitSetting];
}

- (void)didPressOKButton {
    UnitSetting * unitSetting = [_display unitSettingFromDisplay];
    [AppSetting sharedInstance].unitSetting = unitSetting;
    [_display didSaveUnitSetting];
}

@end
