//
//  EventExecutive.h
//  Titan
//
//  Created by Marcus Lee on 14/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseExecutive.h"
#import "Event.h"

@protocol EventDisplay <BaseDisplay>

- (void)loadDisplay;

- (Event*)eventFromDisplay;

- (void)didCreateEvent;

@end

@interface EventExecutive : NSObject

- (id)initWithDisplay:(id<EventDisplay>)display;

- (void)displayDidLoad;

- (void)didPressOKButton;

- (NSArray*)repeatMenus;

@end
