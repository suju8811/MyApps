//
//  CalendarDayInfoExecutive.m
//  Titan
//
//  Created by Marcus Lee on 26/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CalendarDayInfoExecutive.h"
#import "DateTimeUtil.h"

@interface CalendarDayInfoExecutive()

@property id<CalendarDayInfoDisplay> display;

@end

@implementation CalendarDayInfoExecutive

- (id)initWithDisplay:(id<CalendarDayInfoDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)displayDidLoad {
    [_display loadDisplay];
}

- (void)reloadCalendarData:(NSDate*)selectedDate {
    NSDictionary * calendarData = [Schedule calendarDataForDate:selectedDate];
    [_display reloadCalendarData:calendarData];
}

- (void)didPressAddCalendarInfo:(CalendarInfoType)calendarInfoType {
    switch (calendarInfoType) {
        case CalendarInfoTypeTrainingPlan:
            [_display presentTrainingPlanSelectorViewController];
            break;
        case CalendarInfoTypeNutritionPlan:
            [_display presentNutritionPlanSelectorViewController];
            break;
        case CalendarInfoTypeEvent:
            [_display presentEventViewController:nil];
            break;
        case CalendarInfoTypeRepose:
            [_display presentReposeViewController:nil];
        default:
            break;
    }
}

- (void)didPressCalendarInfo:(id)calendarInfo
                selectedDate:(NSDate*)selectedDate {
    
    if ([calendarInfo isKindOfClass:[SchedulePlan class]]) {
        SchedulePlan * schedulePlan = (SchedulePlan*)calendarInfo;
        if (schedulePlan.plan) {
            NSInteger dayOffset = [DateTimeUtil daysBetween:schedulePlan.scheduleDate and:selectedDate];
            Training * training = [schedulePlan.plan.trainingDay[dayOffset].trainingDayContent firstObject].training;
            [_display presentTrainingDetailViewController:training ofPlan:schedulePlan.plan];
        }
        else if (schedulePlan.nutritionPlan) {
            [_display presentNutritionPlanDetailViewController:schedulePlan];
        }
    }
    else if ([calendarInfo isKindOfClass:[Event class]]) {
        Event * event = (Event*)calendarInfo;
        [_display presentEventViewController:event];
    }
    else if ([calendarInfo isKindOfClass:[TrainingPlan class]]) {
        TrainingPlan * repose = (TrainingPlan*)calendarInfo;
        [_display presentReposeViewController:repose];
    }
}

- (void)saveTrainingPlan:(TrainingPlan*)trainingPlan
                  toDate:(NSDate*)date {
    
    [_display showLoading:@"Subiendo ..."];
    [Schedule addTrainingPlan:trainingPlan toDate:date completionBlock:^(BOOL isSuccess) {
        [_display hideLoading];
        if (!isSuccess) {
            [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
            return;
        }
        
        NSDictionary * calendarData = [Schedule calendarDataForDate:date];
        [_display reloadCalendarData:calendarData];
    }];
}

- (void)saveNutritionPlan:(NutritionPlan*)nutritionPlan
                   toDate:(NSDate*)date {
    
    [_display showLoading:@"Subiendo ..."];
    [Schedule addNutritionPlan:nutritionPlan toDate:date completionBlock:^(BOOL isSuccess) {
        [_display hideLoading];
        if (!isSuccess) {
            [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
            return;
        }
        
        NSDictionary * calendarData = [Schedule calendarDataForDate:date];
        [_display reloadCalendarData:calendarData];
    }];
}

@end
