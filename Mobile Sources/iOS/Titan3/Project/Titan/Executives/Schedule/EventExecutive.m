//
//  EventExecutive.m
//  Titan
//
//  Created by Marcus Lee on 14/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "EventExecutive.h"
#import "CreateEventRequest.h"
#import "AppContext.h"
#import "Schedule.h"
#import "DateTimeUtil.h"

@interface EventExecutive()

@property id<EventDisplay> display;

@end

@implementation EventExecutive

- (id)initWithDisplay:(id<EventDisplay>)display {
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)displayDidLoad {
    [_display loadDisplay];
}

- (void)didPressOKButton {
    Event * event = [_display eventFromDisplay];
    if (event.title == nil ||
        event.title.length == 0) {
        
        [_display showMessage:@"Por favor ingrese el título"];
        return;
    }
    
    [_display showLoading:@"Subiendo ..."];
    [Schedule createEvent:event
          completionBlock:^(id response, NSError *error) {
              [_display hideLoading];
              if (error != nil) {
                  [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
                  return;
              }
              
              if (event.repeat.intValue > 0) {
                  Event * duplicatedEvent = [Event duplicateEvent:event];
                  duplicatedEvent.startDate = [DateTimeUtil addDays:event.repeat.intValue
                                                   toDate:event.startDate];
                  duplicatedEvent.selectedDate = [DateTimeUtil addDays:event.repeat.intValue
                                                      toDate:event.selectedDate];
                  
                  [_display showLoading:@"Subiendo ..."];
                  [Schedule createEvent:duplicatedEvent completionBlock:^(id response, NSError *error) {
                      [_display hideLoading];
                      if (error != nil) {
                          [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
                          return;
                      }
                      [_display didCreateEvent];
                  }];
              }
              else {
                  [_display didCreateEvent];
              }
          }];
}

- (NSArray*)repeatMenus {
    return @[
        @"Nunca",
        @"Al día siguiente (next day)",
        @"Dentro de 2 días (in 2 days)",
        @"Dentro de 3 días (in 3 days)",
        @"Dentro de 4 días (in 4 days)",
        @"Dentro de 5 días (in 5 days)",
        @"Dentro de 6 días (in 6 days)",
        @"Dentro de 7 días (in 7 days)"
        ];
}
@end
