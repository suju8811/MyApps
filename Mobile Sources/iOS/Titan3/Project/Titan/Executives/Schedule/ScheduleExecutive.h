//
//  ScheduleExecutive.h
//  Titan
//
//  Created by Marcus Lee on 14/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseExecutive.h"
#import "Event.h"
#import "OptionItem.h"
#import "SchedulePlan.h"
#import "Log.h"

@protocol ScheduleDisplay <BaseDisplay>

- (void)selectDate:(NSDate*)date;
- (void)presentEventViewController:(Event*)event;
- (void)presentReposeViewController:(TrainingPlan*)repose;
- (void)presentLogViewController:(Log*)log;
- (void)presentPopupViewControllerWithTag:(NSInteger)tag
                              optionItems:(NSArray<OptionItem*>*) optionItems
                               withObject:(RLMObject*)object;

- (void)presentTrainingPlanDetailViewController:(SchedulePlan*)schedulePlan;
- (void)presentTrainingDetailViewController:(Training*)training;
- (void)presentNutritionPlanDetailViewController:(SchedulePlan*)schedulePlan;
- (void)presentTrainingPlanSelectorViewController;
- (void)presentNutritionPlanSelectorViewController;
- (void)presentCalendarDayInfoPopup:(NSDictionary*)calendarData date:(NSDate*)selectedDate;
- (void)reloadCalendar;

@end

@interface ScheduleExecutive : NSObject

- (id)initWithDisplay:(id<ScheduleDisplay>)display;

- (void)didPressTodayButton:(NSInteger)segmentState;

- (BOOL)didPressDate:(NSDate*)selectedDate
           atSegment:(NSInteger)segmentState;

- (void)didPressPopupMenuTag:(NSInteger)tag
                       index:(NSInteger)index
                      object:(RLMObject*)object;

- (void)didPressCalendarOption:(NSInteger)index;

- (void)didSelectTrainingPlan:(TrainingPlan*)trainingPlan
                       atDate:(NSDate*)date;

- (void)saveTrainingPlan:(TrainingPlan*)trainingPlan
                  toDate:(NSDate*)date;

- (void)didSelectNutritionPlan:(NutritionPlan*)nutritionPlan
                        atDate:(NSDate*)date;

- (void)saveNutritionPlan:(NutritionPlan*)nutritionPlan
                   toDate:(NSDate*)date;

- (void)didChangeCurrentPage:(NSDate*)currentPage;

- (void)createLog:(Log*)log image:(UIImage*)image;

- (void)updateLog:(Log*)log image:(UIImage*)image;

@end
