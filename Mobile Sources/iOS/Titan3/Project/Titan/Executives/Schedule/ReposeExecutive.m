//
//  ReposeExecutive.m
//  Titan
//
//  Created by Marcus Lee on 14/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ReposeExecutive.h"
#import "CreateTrainingPlanRequest.h"
#import "AppContext.h"
#import "SchedulePlan.h"

@interface ReposeExecutive()

@property id<ReposeDisplay> display;

@end

@implementation ReposeExecutive

- (id)initWithDisplay:(id<ReposeDisplay>)display {
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)displayDidLoad {
    [_display loadDisplay];
}

- (void)didPressOKButton {
    TrainingPlan * repose = [_display reposeFromDisplay];
    if (repose.title == nil ||
        repose.title.length == 0) {
        
        [_display showMessage:@"Por favor ingrese el título"];
        return;
    }
    
    if (repose.endDate == nil) {
        [_display showMessage:@"por favor selecciona la fecha de finalización"];
        return;
    }
    
    CreateTrainingPlanRequest * request
    = [[CreateTrainingPlanRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                  trainingPlan:repose];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
            return;
        }
        
        [self performPostpone:repose];
        [_display didCreateRepose:repose];
    }];
}

- (void)performPostpone:(TrainingPlan*)repose {
    
    //
    // Postpone training plan
    //
    
    RLMRealm *realm = [AppContext currentRealm];
    
    NSDate * iteratorDate = repose.startDate;
    while ([iteratorDate compare:repose.endDate] != NSOrderedDescending) {
        SchedulePlan * schedulePlan = [SchedulePlan getScheduleTrainingPlanWithScheduleDate:iteratorDate];
        NSInteger stepDays = 1;
        if (schedulePlan != nil) {
            [realm transactionWithBlock:^{
                schedulePlan.scheduleDate = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                                                     value:1
                                                                                    toDate:repose.endDate
                                                                                   options:0];
            }];
            
            stepDays = schedulePlan.plan.trainingDay.count;
        }
        
        iteratorDate = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                           value:stepDays
                                                          toDate:iteratorDate
                                                         options:0];
    }
    
    //
    // Postpone nutrition plan
    //
    
    if (repose.isFavourite == nil ||
        !repose.isFavourite.boolValue) {
        
        return;
    }
    
    iteratorDate = repose.startDate;
    while ([iteratorDate compare:repose.endDate] != NSOrderedDescending) {
        SchedulePlan * schedulePlan = [SchedulePlan getScheduleNutritionPlanWithScheduleDate:iteratorDate];
        NSInteger stepDays = 1;
        if (schedulePlan != nil) {
            [realm transactionWithBlock:^{
                schedulePlan.scheduleDate = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                                                     value:1
                                                                                    toDate:repose.endDate
                                                                                   options:0];
            }];
            stepDays = schedulePlan.nutritionPlan.nutritionDays.count;
        }
        
        iteratorDate = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                                value:stepDays
                                                               toDate:iteratorDate
                                                              options:0];
    }
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
}

@end
