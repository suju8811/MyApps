//
//  CalendarDayInfoExecutive.h
//  Titan
//
//  Created by Marcus Lee on 26/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseExecutive.h"
#import "Schedule.h"
#import "SchedulePlan.h"

@protocol CalendarDayInfoDisplay <BaseDisplay>

- (void)loadDisplay;
- (void)reloadCalendarData:(NSDictionary*)calendarData;
- (void)presentTrainingPlanSelectorViewController;
- (void)presentNutritionPlanSelectorViewController;
- (void)presentTrainingDetailViewController:(Training*)training ofPlan:(TrainingPlan*)trainingPlan;
- (void)presentTrainingPlanDetailViewController:(SchedulePlan*)schedulePlan;
- (void)presentNutritionPlanDetailViewController:(SchedulePlan*)schedulePlan;
- (void)presentEventViewController:(Event *)event;
- (void)presentReposeViewController:(TrainingPlan*)repose;

@end

@interface CalendarDayInfoExecutive : NSObject

- (id)initWithDisplay:(id<CalendarDayInfoDisplay>)display;

- (void)displayDidLoad;

- (void)reloadCalendarData:(NSDate*)selectedDate;

- (void)didPressAddCalendarInfo:(CalendarInfoType)calendarInfoType;

- (void)didPressCalendarInfo:(id)calendarInfo
                selectedDate:(NSDate*)selectedDate;

- (void)saveTrainingPlan:(TrainingPlan*)trainingPlan
                  toDate:(NSDate*)date;

- (void)saveNutritionPlan:(NutritionPlan*)nutritionPlan
                   toDate:(NSDate*)date;

@end
