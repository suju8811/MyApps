//
//  ScheduleExecutive.m
//  Titan
//
//  Created by Marcus Lee on 14/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ScheduleExecutive.h"
#import "Schedule.h"
#import "DateTimeUtil.h"
#import "CreateLogRequest.h"
#import "Progress.h"
#import "AppContext.h"
#import "ChangeCalendarRequest.h"

#define TRAINING_PLAN_ADD_OPTION_TAG        100
#define NUTRITION_PLAN_ADD_OPTION_TAG       200

@interface ScheduleExecutive()

@property id<ScheduleDisplay> display;
@property SchedulePlan * selectedSchedulePlan;
@property NSDate * selectedDate;
@property (nonatomic, strong) NSDate * minDate;
@property (nonatomic, strong) NSDate * maxDate;

@end

@implementation ScheduleExecutive

- (id)initWithDisplay:(id<ScheduleDisplay>)display {
    
    self = [super init];
    _display = display;
    _minDate = [DateTimeUtil addMonths:-1 toDate:[NSDate date]];
    _maxDate = [DateTimeUtil addMonths:1 toDate:[NSDate date]];;
    return self;
}

- (void)didPressTodayButton:(NSInteger)segmentState {
    [self didPressDate:[NSDate date]
             atSegment:segmentState];
    
    [_display reloadCalendar];
}

- (BOOL)didPressDate:(NSDate*)selectedDate
           atSegment:(NSInteger)segmentState {
    
    NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:selectedDate options:0];
    
    _selectedDate = date;
    [_display selectDate:date];
    
    NSDictionary * calendarData = [Schedule calendarDataForDate:_selectedDate];

    if ([Schedule hasCalendarData:date]) {
        [_display presentCalendarDayInfoPopup:calendarData date:_selectedDate];
        return YES;
    }
    
    return NO;
}

- (void)changeCalendar:(BOOL)isDelete object:(RLMObject*)rlmObject scheduleDate:(NSDate*)scheduleDate {
    NSDate * date2;
    NSInteger days = 0;
    
    if ([rlmObject isKindOfClass:[TrainingPlan class]]) {
        days = ((TrainingPlan*)rlmObject).trainingDay.count;
        date2 = [DateTimeUtil addDays:days toDate:scheduleDate];
    }
    else if ([rlmObject isKindOfClass:[NutritionPlan class]]) {
        days = ((NutritionPlan*)rlmObject).nutritionDays.count;
        date2 = [DateTimeUtil addDays:days toDate:scheduleDate];
    }
    
    ChangeCalendarRequest * request = [[ChangeCalendarRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                            delete:isDelete
                                                                             date1:scheduleDate
                                                                             date2:date2
                                                                              days:days];
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        if (error != nil) {
            [_display hideLoading];
            [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
            return;
        }
        
        NSInteger currentYear = [DateTimeUtil yearOfDate:_selectedDate];
        NSInteger currentMonth = [DateTimeUtil monthOfDate:_selectedDate];
        
        [Schedule getSchedulesWithYear:[NSString stringWithFormat:@"%d", (int)currentYear]
                                 month:[NSString stringWithFormat:@"%d", (int)currentMonth]
                       completionBlock:^(BOOL isSuccess) {
                           [_display hideLoading];
                           [_display reloadCalendar];
                       }];
    }];
}

- (void)didPressPopupMenuTag:(NSInteger)tag
                       index:(NSInteger)index
                      object:(RLMObject*)object {

    if (index == 0) { // Delete
        if (tag == TRAINING_PLAN_ADD_OPTION_TAG) { // Training Plan
            [self saveTrainingPlan:((TrainingPlan*)object)
                            toDate:_selectedDate
                   completionBlock:^(BOOL isSuccess) {
                       
                       [self changeCalendar:YES object:object scheduleDate:_selectedDate];
            }];
        }
        else if (tag == NUTRITION_PLAN_ADD_OPTION_TAG) { // Nutrition Plan
            [self saveNutritionPlan:((NutritionPlan*)object)
                             toDate:_selectedDate
                    completionBlock:^(BOOL isSuccess) {
                        
                        [self changeCalendar:YES object:object scheduleDate:_selectedDate];
                    }];
        }
    }
    else if (index == 1) { //Postpone
        if (tag == TRAINING_PLAN_ADD_OPTION_TAG) { // Training Plan
            [self saveTrainingPlan:((TrainingPlan*)object)
                            toDate:_selectedDate
                   completionBlock:^(BOOL isSuccess) {
                       
                       [self changeCalendar:NO object:object scheduleDate:_selectedDate];
                   }];
        }
        else if (tag == NUTRITION_PLAN_ADD_OPTION_TAG) { // Nutrition Plan
            [self saveNutritionPlan:((NutritionPlan*)object)
                             toDate:_selectedDate
                    completionBlock:^(BOOL isSuccess) {
                        
                        [self changeCalendar:NO object:object scheduleDate:_selectedDate];
                    }];
        }
    }
}

- (void)didPressCalendarOption:(NSInteger)index {
    if (index == 0) { // Training Plan
        [_display presentTrainingPlanSelectorViewController];
    }
    else if (index == 1) { // Nutrition Plan
        SchedulePlan * schedulePlan = [SchedulePlan getScheduleNutritionPlanWithScheduleDate:_selectedDate];
        if (schedulePlan != nil) {
            [_display presentPopupViewControllerWithTag:NUTRITION_PLAN_ADD_OPTION_TAG
                                            optionItems:@[[[OptionItem alloc] initWithTitle:@"ELIMINAR" image:[UIImage imageNamed:@"icon-delete"]],
                                                          [[OptionItem alloc] initWithTitle:@"DESPLAZAR" image:[UIImage imageNamed:@"icon-edit"]]]
                                             withObject:schedulePlan.nutritionPlan];
            return;
        }
        
        [_display presentNutritionPlanSelectorViewController];
    }
    else if (index == 2) { // Repose
        [_display presentReposeViewController:nil];
    }
    else if (index == 3) { // Event
        [_display presentEventViewController:nil];
    }
    else if (index == 4) { // Registro
        [_display presentLogViewController:nil];
    }
}

- (void)didSelectTrainingPlan:(TrainingPlan*)trainingPlan
                       atDate:(NSDate*)date {
    
    //
    // Check if there is training plan at days
    //
    
    
    if ([Schedule checkHaveTraininigContentInDays:trainingPlan.trainingDay.count fromDate:date]) {
        [_display presentPopupViewControllerWithTag:TRAINING_PLAN_ADD_OPTION_TAG
                                        optionItems:@[[[OptionItem alloc] initWithTitle:@"ELIMINAR" image:[UIImage imageNamed:@"icon-delete"]],
                                                      [[OptionItem alloc] initWithTitle:@"DESPLAZAR" image:[UIImage imageNamed:@"icon-edit"]]]
                                         withObject:trainingPlan];
        
        return;
    }
    
    
    [self saveTrainingPlan:trainingPlan toDate:date];
}

- (void)saveTrainingPlan:(TrainingPlan *)trainingPlan
                  toDate:(NSDate *)date
         completionBlock:(void (^)(BOOL isSuccess)) saveCompletionBlock {
    
    [_display showLoading:@"Subiendo ..."];
    [Schedule addTrainingPlan:trainingPlan toDate:date completionBlock:^(BOOL isSuccess) {
        [_display hideLoading];
        if (!isSuccess) {
            [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
            return;
        }
        
        saveCompletionBlock(isSuccess);
    }];
}

- (void)saveTrainingPlan:(TrainingPlan*)trainingPlan
                  toDate:(NSDate*)date {
    
    [self saveTrainingPlan:trainingPlan
                    toDate:date
           completionBlock:^(BOOL isSuccess) {
               
        [_display reloadCalendar];
    }];
}

- (void)didSelectNutritionPlan:(NutritionPlan*)nutritionPlan
                        atDate:(NSDate*)date {
    
    //
    // Check if there is training plan at days
    //
    
    if ([Schedule checkHaveNutritionContentInDays:nutritionPlan.nutritionDays.count fromDate:date]) {
        [_display presentPopupViewControllerWithTag:TRAINING_PLAN_ADD_OPTION_TAG
                                        optionItems:@[[[OptionItem alloc] initWithTitle:@"ELIMINAR" image:[UIImage imageNamed:@"icon-delete"]],
                                                      [[OptionItem alloc] initWithTitle:@"DESPLAZAR" image:[UIImage imageNamed:@"icon-edit"]]]
                                         withObject:nutritionPlan];
        
        return;
    }
    
    [self saveNutritionPlan:nutritionPlan toDate:date];
}

- (void)saveNutritionPlan:(NutritionPlan*)nutritionPlan
                   toDate:(NSDate*)date
          completionBlock:(void (^)(BOOL isSuccess)) saveCompletionBlock {
    
    [_display showLoading:@"Subiendo ..."];
    [Schedule addNutritionPlan:nutritionPlan toDate:date completionBlock:^(BOOL isSuccess) {
        [_display hideLoading];
        if (!isSuccess) {
            [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
            return;
        }
       
        saveCompletionBlock(isSuccess);
    }];
}

- (void)saveNutritionPlan:(NutritionPlan*)nutritionPlan
                   toDate:(NSDate*)date {
    
    [self saveNutritionPlan:nutritionPlan
                     toDate:date
            completionBlock:^(BOOL isSuccess) {
                
        [_display reloadCalendar];
    }];
}

- (void)didChangeCurrentPage:(NSDate*)currentPage {
    NSDate * currentMaxDate = [DateTimeUtil addMonths:2 toDate:currentPage];
    if ([currentPage compare:_minDate] != NSOrderedAscending &&
        [currentMaxDate compare:_maxDate] != NSOrderedDescending) {
        
        return;
    }
    
    NSInteger currentMonth = 0;
    NSInteger currentYear = 0;
    
    if ([currentPage compare:_minDate] == NSOrderedAscending) {
        _minDate = currentPage;
        currentMonth = [DateTimeUtil monthOfDate:currentPage];
        currentYear = [DateTimeUtil yearOfDate:currentPage];
    }
    
    if ([currentMaxDate compare:_maxDate] == NSOrderedDescending) {
        _maxDate = currentMaxDate;
        currentMonth = [DateTimeUtil monthOfDate:currentMaxDate];
        currentYear = [DateTimeUtil yearOfDate:currentMaxDate];
    }
    
    [Schedule getSchedulesWithYear:[NSString stringWithFormat:@"%d", (int)currentYear]
                             month:[NSString stringWithFormat:@"%d", (int)currentMonth]
                   completionBlock:^(BOOL isSuccess) {

                       [_display reloadCalendar];
    }];
}

- (void)createLog:(Log*)log image:(UIImage*)image {
    CreateLogRequest * request = [[CreateLogRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                     log:log
                                                                   image:image];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        if (error != nil) {
            [_display hideLoading];
            [_display showMessage:@"Error al calificar la aplicación"];
            return;
        }
        
        [Progress getLogsWithCompletionBlock:^(BOOL isSuccess) {
            [_display hideLoading];
            [_display reloadCalendar];
        }];
    }];
}

- (void)updateLog:(Log*)log image:(UIImage*)image {
}

@end
