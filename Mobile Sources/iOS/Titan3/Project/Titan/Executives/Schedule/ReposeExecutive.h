//
//  ReposeExecutive.h
//  Titan
//
//  Created by Marcus Lee on 14/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrainingPlan.h"
#import "BaseExecutive.h"

@protocol ReposeDisplay <BaseDisplay>

- (void)loadDisplay;

- (TrainingPlan*)reposeFromDisplay;

- (void)didCreateRepose:(TrainingPlan*)repose;

@end

@interface ReposeExecutive : NSObject

- (id)initWithDisplay:(id<ReposeDisplay>)display;

- (void)displayDidLoad;

- (void)didPressOKButton;

@end
