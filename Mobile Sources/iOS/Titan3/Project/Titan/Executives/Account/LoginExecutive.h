//
//  LoginExecutive.h
//  Titan
//
//  Created by Marcus Lee on 18/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginRequest.h"
#import "BaseExecutive.h"

@protocol LoginDisplay <BaseDisplay>

- (LoginRequest*)getLoginRequest;
- (void)didLoginSuccess;

@end

@interface LoginExecutive : NSObject

- (id)initWithDisplay:(id<LoginDisplay>)display;

- (void)didPressLoginButton;

@end
