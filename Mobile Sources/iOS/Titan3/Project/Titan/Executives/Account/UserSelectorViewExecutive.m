//
//  UserSelectorViewExecutive.m
//  Titan
//
//  Created by Marcus Lee on 8/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "UserSelectorViewExecutive.h"
#import "GetUsersMetaRequest.h"
#import "JSONKit.h"
#import "UserMeta.h"

@interface UserSelectorViewExecutive()

@property id<UserSelectorViewDisplay> display;

@end

@implementation UserSelectorViewExecutive

- (id)initWithDisplay:(id<UserSelectorViewDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)didPressOKButton {
    if ([self validateItems] != nil) {
        [_display showMessage:[self validateItems]];
        return;
    }
    
    [_display dismissWithConfirm];
}

- (NSString*)validateItems {
    NSArray * items = [_display selectedItems];
    if (items != nil
        && items.count > 0) {
        return nil;
    }
    
    NSString * invalidMessage = @"Seleccione por lo menos un usuario";
    return invalidMessage;
}

- (void)displayDidLoad {
    GetUsersMetaRequest * request = [[GetUsersMetaRequest alloc] init];
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        NSDictionary * responseDictionary = [responseArray firstObject];
        if (![[responseDictionary valueForKey:@"active"] boolValue]) {
            return;
        }
        
        NSArray<NSDictionary*> *usersDictionary = [responseDictionary valueForKey:@"usuarios"];
        NSMutableArray * items = [[NSMutableArray alloc] init];
        for (NSDictionary * userDictionary in usersDictionary) {
            UserMeta * userMeta = [[UserMeta alloc] init];
            [userMeta parseFromDictionary:userDictionary];
            [items addObject:userMeta];
        }
        
        [_display didLoadItems:items];
    }];
}

@end
