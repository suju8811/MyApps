//
//  UserSelectorViewExecutive.h
//  Titan
//
//  Created by Marcus Lee on 8/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"

@protocol UserSelectorViewDisplay <BaseDisplay>

- (void)dismissWithConfirm;
- (void)didLoadItems:(NSArray*)items;
- (NSArray*)selectedItems;

@end

@interface UserSelectorViewExecutive : NSObject

- (id)initWithDisplay:(id<UserSelectorViewDisplay>)display;

- (void)displayDidLoad;

- (void)didPressOKButton;

@end
