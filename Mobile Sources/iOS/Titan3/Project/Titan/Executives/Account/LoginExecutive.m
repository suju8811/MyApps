//
//  LoginExecutive.m
//  Titan
//
//  Created by Marcus Lee on 18/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "LoginExecutive.h"
#import <Parse/Parse.h>
#import "Titan-Swift.h"
#import "LoginChatRequest.h"

@interface LoginExecutive()

@property id<LoginDisplay> display;

@end

@implementation LoginExecutive

- (id)initWithDisplay:(id<LoginDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)didPressLoginButton {
    LoginRequest * request = [_display getLoginRequest];
    
    [_display showLoading:@"Login ..."];
    [request loginWithCompletionBlock:^(BOOL isSuccess, NSString *error) {
        if (!isSuccess) {
            [_display hideLoading];
            [_display showMessage:@"No puedes loguearte sin registrarte."];
            return;
        }
        
        [PFUser logInWithUsernameInBackground:request.username
                                     password:request.password
                                        block:^(PFUser * _Nullable user, NSError * _Nullable error) {
                                            
            if (user != nil) {
                [self performChatLoginWithUserName:request.username
                                          password:request.password
                                   CompletionBlock:^(BOOL isSuccess, NSString *error) {
                                       
                                       [_display hideLoading];
                                       if (!isSuccess) {
                                           [_display showMessage:@"No puedes loguearte sin registrarte."];
                                           return;
                                       }
                                       
                                       [_display didLoginSuccess];
                }];
            }
            else {
                [_display hideLoading];
                NSLog(@"Failed to login to parse: %@", error.localizedDescription);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_display showMessage:@"No puedes loguearte sin registrarte."];
                });
            }
        }];
    }];
}

- (void)performChatLoginWithUserName:(NSString*)username
                            password:(NSString*)password
                     CompletionBlock:(void (^)(BOOL isSuccess, NSString * error)) completionBlock {
    
    LoginChatRequest * request = [[LoginChatRequest alloc] initWithUserName:username password:password];
    
    [request postWithCompletionBlock:^(id response, NSString * errorMessage) {
        completionBlock(errorMessage == nil, errorMessage);
    }];
}

@end
