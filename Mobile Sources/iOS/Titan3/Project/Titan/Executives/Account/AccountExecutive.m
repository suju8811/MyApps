//
//  AccountExecutive.m
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "AccountExecutive.h"
#import "ChangeUserNameRequest.h"
#import "ChangePasswordRequest.h"
#import "ChangeEmailRequest.h"
#import "DeleteAccountRequest.h"
#import "AppContext.h"

@interface AccountExecutive()

@property (nonatomic, strong) id<AccountDisplay> display;

@end

@implementation AccountExecutive

- (id)initWithDisplay:(id<AccountDisplay>)display {
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)changeUserName:(NSString*)userName {
    if (userName.length == 0) {
        [_display showMessage:NSLocalizedString(@"Introduce tu nombre de usuario", nil)];
    }
    
    ChangeUserNameRequest * request = [[ChangeUserNameRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                          userName:userName];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
            return;
        }
        
        [_display didChangeUserName:userName];
    }];
}

- (void)changeEmail:(NSString*)email {
    if (email.length == 0) {
        [_display showMessage:NSLocalizedString(@"Introduce tu email", nil)];
    }
    
    ChangeEmailRequest * request = [[ChangeEmailRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                       email:email];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
            return;
        }
        
        [_display didChangeEmail:email];
    }];
}

- (void)changePassword:(NSString*)password {
    if (password.length == 0) {
        [_display showMessage:NSLocalizedString(@"Introduce tu password", nil)];
    }
    
    ChangePasswordRequest * request = [[ChangePasswordRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                          password:password];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
            return;
        }
        
        [_display didChangePassword:password];
    }];
}

- (void)deleteAccount {
    DeleteAccountRequest * request = [[DeleteAccountRequest alloc] initWithToken:[AppContext sharedInstance].authToken];
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
            return;
        }
        
        [_display didDeleteAccount];
    }];
}

@end
