//
//  AccountExecutive.h
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"

@protocol AccountDisplay <BaseDisplay>

- (void)didChangeUserName:(NSString*)userName;
- (void)didChangeEmail:(NSString*)email;
- (void)didChangePassword:(NSString*)password;
- (void)didDeleteAccount;

@end

@interface AccountExecutive : NSObject

- (id)initWithDisplay:(id<AccountDisplay>)display;

- (void)changeUserName:(NSString*)userName;
- (void)changeEmail:(NSString*)email;
- (void)changePassword:(NSString*)password;
- (void)deleteAccount;

@end
