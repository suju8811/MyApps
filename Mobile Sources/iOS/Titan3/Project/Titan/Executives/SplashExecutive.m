//
//  SplashExecutive.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SplashExecutive.h"
#import "LoginRequest.h"
#import "UserDefaultLibrary.h"
#import "AppConstants.h"
#import "AppContext.h"
#import <Parse/Parse.h>
#import "Titan-Swift.h"
#import "AuthorizeChatRequest.h"

@interface SplashExecutive()

@property id<SplashDisplay> display;

@end

@implementation SplashExecutive

- (id)initWithDisplay:(id<SplashDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)displayDidLoad {
    if ([UserDefaultLibrary getWithKey:USER_NAME] == nil ||
        [UserDefaultLibrary getWithKey:USER_PASSWORD] == nil) {
        
        [_display presentLoginViewController];
        return;
    }
    
    NSString * userName = [UserDefaultLibrary getWithKey:USER_NAME];
    NSString * userPassword = [UserDefaultLibrary getWithKey:USER_PASSWORD];
    LoginRequest * request =
        [[LoginRequest alloc] initWithUserName:userName
                                      password:userPassword];
    
    [request loginWithCompletionBlock:^(BOOL isSuccess, NSString *error) {
        if (isSuccess) {
            [PFUser logInWithUsernameInBackground:userName password:userPassword block:^(PFUser * _Nullable user, NSError * _Nullable error) {
                if (user != nil) {
                    [self performChatLoginWithCompletionBlock:^(BOOL isSuccess, NSString *error) {
                        if (isSuccess) {
                            [_display presentHomeViewController];
                        }
                        else {
                            [_display presentLoginViewController];
                        }
                    }];
                }
                else {
                    NSLog(@"Failed to login to parse: %@", error.localizedDescription);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_display presentLoginViewController];
                    });
                }
            }];

            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_display presentLoginViewController];
        });
    }];
}

- (void)performChatLoginWithCompletionBlock:(void (^)(BOOL isSuccess, NSString * error)) completionBlock {
    [[iApp sharedInstance] readSettings];
    
    NSLog(@"---- Chat Configuration ----");
    NSLog(@"id = %d", (int)[[iApp sharedInstance] getId]);
    NSLog(@"access_token = %@", [[iApp sharedInstance] getAccessToken]);
    NSLog(@"username = %@", [[iApp sharedInstance] getUsername]);
    NSLog(@"fullname = %@", [[iApp sharedInstance] getFullname]);
    NSLog(@"email = %@", [[iApp sharedInstance] getEmail]);
    NSLog(@"---- Chat Configuration End ----");
    
    if ([[iApp sharedInstance] getId] != 0 &&
        [Helper isInternetAvailable]) {
        
        AuthorizeChatRequest * request = [[AuthorizeChatRequest alloc] initWithAccountId:[[iApp sharedInstance] getId]
                                                                             accessToken:[[iApp sharedInstance] getAccessToken]
                                                                                fcmRegId:[[iApp sharedInstance] getFcmRegId]];
        [request postWithCompletionBlock:^(id response, NSString * errorMessage) {
            completionBlock(errorMessage == nil, errorMessage);
        }];
        
    }
    else {
        completionBlock(NO, @"Error is occured");
    }
}

@end
