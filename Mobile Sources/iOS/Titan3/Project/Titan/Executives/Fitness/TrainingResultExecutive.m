//
//  TrainingResultExecutive.m
//  Titan
//
//  Created by Marcus Lee on 27/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingResultExecutive.h"

@interface TrainingResultExecutive()

@property id<TrainingResultDisplay> display;

@end

@implementation TrainingResultExecutive

- (id)initWithDisplay:(id<TrainingResultDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)displayDidLoad {
    [_display loadDisplay];
}

@end
