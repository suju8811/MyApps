//
//  TrainingAddExecutive.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingAddExecutive.h"
#import "CreateTrainingRequest.h"
#import "EditTrainingRequest.h"
#import "AppContext.h"

typedef enum : NSUInteger {
    TrainingOperationCreate,
    TrainingOperationEdit,
    TrainingOperationSelect
} TrainingOperationType;

@interface TrainingAddExecutive()

@property id<TrainingAddDisplay> display;

@end

@implementation TrainingAddExecutive

- (id)initWithDisplay:(id<TrainingAddDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)postTraining:(Training*)training {
    if (![self checkFormFields:training]) {
        return;
    }
    
    [self setDisplayContextValueForTraining:training];
//    [self saveTrainingToRealm:training];
    
    CreateTrainingRequest * request =
        [[CreateTrainingRequest alloc] initWithTraining:[AppContext sharedInstance].authToken
                                             training:training];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [self setTagState:NO forTraining:training];
        
        [_display didTrainingPost:training];
    }];
}

- (void)updateTraining:(Training*)training {
    if (![self checkFormFields:training]) {
        return;
    }
    
    [self updateWithDisplayContextValueForTraining:training];
    
    EditTrainingRequest * request =
    [[EditTrainingRequest alloc] initWithTraining:[AppContext sharedInstance].authToken
                                         training:training];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        
        [self setTagState:NO forTraining:training];
        [self saveTrainingToRealm:training];
        [_display didTrainingUpdate:training];
    }];
}

- (void)saveTrainingToRealm:(Training*)training {
    RLMRealm *realm = [AppContext currentRealm];
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:training];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
}

- (void)setDisplayContextValueForTraining:(Training*)training {
    TrainingAddDisplayContext * context = _display.displayContext;
    
    [training setTitle:context.title];
    [training setContent:context.content];
    [training setDesc:context.content];
    [training.tags addObjects:context.selectedTags];
    [training setNotes:context.note];
    [training setPunctation:context.note];
    [training setPrivacity:context.privacy];
}

- (void)updateWithDisplayContextValueForTraining:(Training*)training {
    TrainingAddDisplayContext * context = _display.displayContext;
    
    RLMRealm *realm = [AppContext currentRealm];
    [realm transactionWithBlock:^{
        [training setTitle:context.title];
        [training setContent:context.content];
        [training setDesc:context.content];
        [training.tags addObjects:context.selectedTags];
        [training setNotes:context.note];
        [training setPunctation:context.note];
        [training setPrivacity:context.privacy];
    }];
}

- (void)setTagState:(BOOL)state
        forTraining:(Training*)training {
    
    NSString * stateValue = state ? @"On" : @"Off";
    RLMRealm *realm = [AppContext currentRealm];
    
    for(Tag *tag in training.tags){
        [realm transactionWithBlock:^{
            [tag setStatus:stateValue];
        }];
    }
    
    TrainingAddDisplayContext * context = [_display displayContext];
    for(Tag *tag in context.selectedTags){
        [realm transactionWithBlock:^{
            [tag setStatus:stateValue];
        }];
    }
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
}

- (BOOL)checkFormFields:(Training*)training {
    TrainingAddDisplayContext * context = [_display displayContext];
    
    if(context.title.length == 0) {
        [_display showMessage:@"El campo nombre es oblicatorio"];
        return NO;
    }
    
    if (training.trainingExercises.count == 0) {
//        [_display showMessage:@"Debe añadir algún ejercicio"];
//        return NO;
    }
    
    return TRUE;
}

@end

@implementation TrainingAddDisplayContext

@end
