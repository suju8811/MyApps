//
//  ExcercisesExecutive.m
//  Titan
//
//  Created by Marcus Lee on 17/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ExcercisesExecutive.h"

@interface ExcercisesExecutive()

@property id<ExcercisesDisplay> display;

@end

@implementation ExcercisesExecutive

- (id)initWithDisplay:(id<ExcercisesDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)loadItemsForTraining:(Training*)training
        exerciseSegmentState:(NSInteger)segmentState {
    
    RLMArray * trainingExcercises = training.trainingExercises;
    NSMutableArray * filteredExercises = [[NSMutableArray alloc] init];
    NSString * selectedType = segmentState == 0 ? @"anaerobico" : @"aerobico";
    
    for (TrainingExercise * trainingExercise in trainingExcercises) {
        for (Exercise * exercise in trainingExercise.exercises) {
            if ([exercise.type.lowercaseString isEqualToString:selectedType]) {
                [filteredExercises addObject:exercise];
            }
        }
    }
    
    [_display didLoadExercises:filteredExercises];
}

@end
