//
//  FitnessExecutive.m
//  Titan
//
//  Created by Marcus Lee on 15/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FitnessExecutive.h"
#import "DeletePlanRequest.h"
#import "DeleteTrainingRequest.h"
#import "AppContext.h"
#import "SendContentRequest.h"
#import "ShareContentRequest.h"
#import "Schedule.h"
#import "WebServiceManager.h"

@interface FitnessExecutive()

@property id<FitnessDisplay> display;

@end

@implementation FitnessExecutive

- (id)initWithDisplay:(id<FitnessDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)loadItemsForFitnessType:(FitnessSelectType)fitnessType
              workoutSelectType:(WorkoutSelectType)workoutType
         trainingPlanSelectType:(TrainingPlanSelectType)trainingPlanType
           exerciseSegmentState:(NSInteger)segmentState
                     searchText:(NSString*)searchText {
    
    if (workoutType == WorkoutSelectTypeShared) {
        if (fitnessType == FitnessSelectTypePlan) {
            [WebServiceManager getTrainingPlansWithCompletionBlock:^(BOOL isSuccess) {
                [self loadItemsFromLocalForFitnessType:fitnessType
                                     workoutSelectType:workoutType
                                trainingPlanSelectType:trainingPlanType
                                  exerciseSegmentState:segmentState
                                            searchText:searchText];
            }];
        }
        else if (fitnessType == FitnessSelectTyeWorkout) {
            [WebServiceManager getTrainingsWithCompletionBlock:^(BOOL isSuccess) {
                [self loadItemsFromLocalForFitnessType:fitnessType
                                     workoutSelectType:workoutType
                                trainingPlanSelectType:trainingPlanType
                                  exerciseSegmentState:segmentState
                                            searchText:searchText];
            }];
        }
        else if (fitnessType == FitnessSelectTypeExercise) {
            [WebServiceManager getExercisesWithCompletionBlock:^(BOOL isSuccess) {
                [self loadItemsFromLocalForFitnessType:fitnessType
                                     workoutSelectType:workoutType
                                trainingPlanSelectType:trainingPlanType
                                  exerciseSegmentState:segmentState
                                            searchText:searchText];
            }];
        }
        
        return;
    }
    
    [self loadItemsFromLocalForFitnessType:fitnessType
                         workoutSelectType:workoutType
                    trainingPlanSelectType:trainingPlanType
                      exerciseSegmentState:segmentState
                                searchText:searchText];
}

- (void)loadItemsFromLocalForFitnessType:(FitnessSelectType)fitnessType
                       workoutSelectType:(WorkoutSelectType)workoutType
                  trainingPlanSelectType:(TrainingPlanSelectType)trainingPlanType
                    exerciseSegmentState:(NSInteger)segmentState
                              searchText:(NSString*)searchText {
    
    RLMResults *items;
    switch (fitnessType) {
        case FitnessSelectTypePlan:
            if(trainingPlanType == TrainingPlanSelectTypeUser) {
                items = [TrainingPlan getMyTrainingPlansWithKeyword:searchText];
            }
            else if(trainingPlanType == TrainingPlanSelectTypeFavourite) {
                items = [TrainingPlan getFavouritesTrainnigPlansWithKeyword:searchText];
            }
            else if(trainingPlanType == TrainingPlanSelectTypeAdmin) {
                items = [TrainingPlan getOtherTrainingPlansWithKeyword:searchText];
            }
            else if(trainingPlanType == TrainingPlanSelectTypeShared) {
                items = [TrainingPlan getSharedTrainnigPlansWithKeyword:searchText];
            }
            break;
        case FitnessSelectTyeWorkout:{
            if (workoutType == WorkoutSelectTypeUser) {
                items = [Training getMyTrainingsWithKeyword:searchText];
            }
            else if (workoutType == WorkoutSelectTypeAdmin) {
                items = [Training getOtherTrainingsWithKeyword:searchText];
            }
            else if (workoutType == WorkoutSelectTypeFavourite) {
                items = [Training getFavouritesTrainingsWithKeyword:searchText];
            }
            else {
                items = [Training getSharedTrainingsWithKeyword:searchText];
            }
        }
            break;
        case FitnessSelectTypeExercise:{
            if(segmentState == 0) {
                items = [Exercise getAnaerobicExercisesWithKeyword:searchText];
            }
            else {
                items = [Exercise getAerobicsExercisesWithKeyword:searchText];
            }
            break;
        }
        default:
            break;
    }
    
    [_display didLoadItems:items forFitnessType:fitnessType];
}

- (void)didPressHeaderSection:(NSInteger)sectionIndex {
    
}

- (void)deleteTraining:(Training*)training {
    DeleteTrainingRequest * request =
    [[DeleteTrainingRequest alloc] initWithTrainingId:training.serverTrainingId
                                                token:[AppContext sharedInstance].authToken];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [Training deleteTraining:training];
        [_display didDeletedTraining:training];
    }];
}

- (void)deleteTrainingPlan:(TrainingPlan*)trainingPlan {
    DeletePlanRequest * request =
    [[DeletePlanRequest alloc] initWithPlanId:trainingPlan.serverTrainingPlanId
                                        token:[AppContext sharedInstance].authToken];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [TrainingPlan deleteTrainingPlan:trainingPlan];
        [_display didDeletedTrainingPlan:trainingPlan];
    }];
}

- (void)setAllTagsOff {
    
    NSString * stateValue = @"Off";
    RLMRealm *realm = [AppContext currentRealm];
    
    NSArray * tags = [Tag getAllTags];
    for(Tag *tag in tags){
        [realm transactionWithBlock:^{
            [tag setStatus:stateValue];
        }];
    }
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
}

- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2 {
    SendContentRequest * request = [[SendContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                       token2:token2
                                                                    rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadItems];
    }];
}

- (void)shareContent:(RLMObject*)content {
    ShareContentRequest * request = [[ShareContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                      rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadItems];
    }];
}

- (void)addTrainingPlan:(TrainingPlan*)trainingPlan
                 toDate:(NSDate*)date {
    
    //
    // Check the content for date
    //
    
    NSString * checkResult = [Schedule checkContentForDate:date];
    if (checkResult) {
        [_display showMessage:checkResult];
        return;
    }
    
    //
    // Register training plan
    //
    
    if ([Schedule addTrainingPlan:trainingPlan
                           toDate:date]) {
        
        [_display showMessage:@"plan de entrenamiento se registra con éxito."];
    }
}

@end
