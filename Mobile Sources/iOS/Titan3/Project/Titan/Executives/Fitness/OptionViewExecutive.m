//
//  OptionViewExecutive.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OptionViewExecutive.h"
#import "CreateTrainingRequest.h"
#import "FavoriteTrainingRequest.h"
#import "DeleteTrainingRequest.h"
#import "CreateTrainingPlanRequest.h"
#import "AppContext.h"
#import "FavoritePlanRequest.h"
#import "DeletePlanRequest.h"
#import "EditTrainingPlanRequest.h"

@interface OptionViewExecutive()

@property id<OptionViewDisplay> display;

@end

@implementation OptionViewExecutive

- (id)initWithDisplay:(id<OptionViewDisplay>)display {
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)duplicateTraining:(Training*)training {
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm transactionWithBlock:^{
        [training setTitle:[NSString stringWithFormat:@"%@ %@",training.title, NSLocalizedString(@"Copia", nil)]];
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    CreateTrainingRequest * request =
    [[CreateTrainingRequest alloc] initWithTraining:[AppContext sharedInstance].authToken
                                         training:training];
    
    [_display showLoading:@"Guardando ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [Training deleteTraining:training];
        [_display didDuplicateTraining:training];
    }];
}

- (void)favoriteTraining:(Training*)training {
    NSNumber * favoriteValue;
    
    if(training.isFavourite != nil && training.isFavourite.boolValue){
        favoriteValue = [NSNumber numberWithBool:NO];
    }
    else {
        favoriteValue = [NSNumber numberWithBool:YES];
    }
    
    FavoriteTrainingRequest * request =
    [[FavoriteTrainingRequest alloc] initWithTrainingId:training.serverTrainingId token:[AppContext sharedInstance].authToken isFavorite:favoriteValue.boolValue];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            [training setIsFavourite:favoriteValue];
        }];
        
        [realm beginWriteTransaction];
        NSError *transactionError;
        [realm commitWriteTransaction:&transactionError];
        if(transactionError){
            NSLog(@"%@",error.description);
        }
        
        [_display dismissDisplay];
    }];
}

- (void)deleteTraining:(Training*)training {
    DeleteTrainingRequest * request =
    [[DeleteTrainingRequest alloc] initWithTrainingId:training.serverTrainingId
                                                token:[AppContext sharedInstance].authToken];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        
        [Training deleteTraining:training];
        [_display didDeletedTraining:training];
    }];
}

- (void)updateTrainingPlan:(TrainingPlan*)trainingPlan {
    
    EditTrainingPlanRequest * request
    = [[EditTrainingPlanRequest alloc] initWithTraining:[AppContext sharedInstance].authToken
                                   trainingPlan:trainingPlan];
    
    [_display showLoading:@"Editando"];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        if (responseArray == nil || responseArray.count == 0) {
            [_display showMessage:@"Failed to update training plan"];
            return;
        }
        
        if (![[[responseArray firstObject] valueForKey:@"active"] boolValue]) {
            [_display showMessage:@"Failed to update training plan"];
            return;
        }
        
        [_display didUpdateTrainingPlan:trainingPlan];
    }];
}

- (void)duplicateTrainingPlan:(TrainingPlan*)trainingPlan {
    
    CreateTrainingPlanRequest * request
    = [[CreateTrainingPlanRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                     trainingPlan:trainingPlan];
    
    [_display showLoading:@"Subiendo..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        if (responseArray == nil || responseArray.count == 0) {
            [_display showMessage:@"Failed to create training plan"];
            return;
        }
        
        if (![[[responseArray firstObject] valueForKey:@"active"] boolValue]) {
            [_display showMessage:@"Failed to create training plan"];
            return;
        }
        
        [_display didDuplicateTrainingPlan:trainingPlan];
    }];
}

- (void)favoriteTrainingPlan:(TrainingPlan*)trainingPlan {
    NSNumber * favoriteValue;
    
    if(trainingPlan.isFavourite != nil && trainingPlan.isFavourite.boolValue){
        favoriteValue = [NSNumber numberWithBool:NO];
    }
    else {
        favoriteValue = [NSNumber numberWithBool:YES];
    }
    
    FavoritePlanRequest * request =
    [[FavoritePlanRequest alloc] initWithPlanId:trainingPlan.serverTrainingPlanId token:[AppContext sharedInstance].authToken isFavorite:favoriteValue.boolValue];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }

//        [_display didUpdateTrainingPlan:trainingPlan];
        RLMRealm *realm = [AppContext currentRealm];

        [realm transactionWithBlock:^{
            [trainingPlan setIsFavourite:favoriteValue];
        }];

        [realm beginWriteTransaction];
        NSError *transactionError;
        [realm commitWriteTransaction:&transactionError];
        if(transactionError){
            NSLog(@"%@",error.description);
        }

        [_display dismissDisplay];
    }];
}

- (void)deleteTrainingPlan:(TrainingPlan*)trainingPlan {
    DeletePlanRequest * request =
    [[DeletePlanRequest alloc] initWithPlanId:trainingPlan.serverTrainingPlanId
                                        token:[AppContext sharedInstance].authToken];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [TrainingPlan deleteTrainingPlan:trainingPlan];
        [_display didDeletedTrainingPlan:trainingPlan];
    }];
}

@end
