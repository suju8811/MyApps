//
//  SeriesExecutive.m
//  Titan
//
//  Created by Marcus Lee on 20/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SeriesExecutive.h"
#import "AppContext.h"

@interface SeriesExecutive()

@property (nonatomic, strong) id<SeriesDisplay> display;
@property (nonatomic, strong) NSArray<Serie*>* series;
@property (nonatomic, assign) NSInteger currentExecutingIndex;
@property (nonatomic, assign) NSInteger currentExecutingSeconds;
@property (nonatomic, assign) NSInteger order;

@end

@implementation SeriesExecutive

- (id)initWithDisplay:(id<SeriesDisplay>)display
               series:(NSArray<Serie*>*)series
                order:(NSInteger)order {
    
    self = [super init];
    
    _display = display;
    _series = series;
    _currentExecutingIndex = 0;
    _currentExecutingSeconds  = 0;
    _order = order;
    
    return self;
}

- (void)startExecuting {
    if (_currentExecutingIndex >= _series.count) {
        _currentExecutingIndex = _series.count - 1;
        [_display didCompletedExecutingSeries:_order];
        return;
    }
    
    RLMRealm * realm = [AppContext currentRealm];
//    for (Serie * serie in _series) {
//        [realm transactionWithBlock:^{
//            serie.executingStatus = @0;
//        }];
//    }
    
    Serie * executingSerie = _series[_currentExecutingIndex];
    [realm transactionWithBlock:^{
        executingSerie.executingStatus = @1; // Counting down execution time
        if (executingSerie.execTime.intValue - 1 >= 0) {
            executingSerie.execTime = @(executingSerie.execTime.intValue - 1);
        }
        else {
            if (executingSerie.rest.intValue - 1 >= 0) {
                executingSerie.rest = @(executingSerie.rest.intValue - 1);
                executingSerie.executingStatus = @3; // Counting down rest
            }
            else {
                executingSerie.executingStatus = @4; // Completed
                _currentExecutingIndex ++;
            }
        }
    }];

    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];

    if(error){
        NSLog(@"%@",error.description);
    }
    
    [_display reloadSeries];
    [self performSelector:@selector(startExecuting) withObject:nil afterDelay:1.f];
}

- (void)pauseExecuting {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startExecuting) object:nil];
    
    RLMRealm *realm = [AppContext currentRealm];
    Serie * executingSerie = _series[_currentExecutingIndex];
    [realm transactionWithBlock:^{
        executingSerie.executingStatus = @2;
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if (error) {
        NSLog(@"%@",error.description);
    }
    
    [_display reloadSeries];
}

- (void)dismissToParent {
    [self pauseExecuting];
    
    [_display dismissDisplayWithOrder:_order
                           serieIndex:_currentExecutingIndex
                              seconds:_currentExecutingSeconds];
}

@end
