//
//  WorkoutDetailExecutive.m
//  Titan
//
//  Created by Marcus Lee on 17/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "WorkoutDetailExecutive.h"
#import "EditTrainingRequest.h"
#import "AppContext.h"
#import "SendContentRequest.h"
#import "ShareContentRequest.h"
#import "DoneTrainingRequest.h"

@interface WorkoutDetailExecutive()

@property id<WorkoutDetailDisplay> display;

@end

@implementation WorkoutDetailExecutive

- (id)initWithDisplay:(id<WorkoutDetailDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)updateTraining:(Training*)training {
    
    EditTrainingRequest * request =
    [[EditTrainingRequest alloc] initWithTraining:[AppContext sharedInstance].authToken
                                         training:training];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        
        [_display didTrainingUpdate:training];
    }];
    
}

- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2 {
    SendContentRequest * request = [[SendContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                       token2:token2
                                                                    rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadAllFitness];
    }];
}

- (void)shareContent:(RLMObject*)content {
    ShareContentRequest * request = [[ShareContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                      rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadAllFitness];
    }];
}

- (void)completeExercise:(TrainingExercise*)trainingExercise
                training:(Training*)currentTraining
                 forPlan:(TrainingPlan*)trainingPlan {
    
    _executingIndex = _executingIndex + 1;
    [_display realodTableView];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm transactionWithBlock:^{
        trainingExercise.done = @YES;
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
        return;
    }
        
    if (_executingIndex < currentTraining.trainingExercises.count) {
        [_display didPressShowExerciseInCalendar];
    }
    else {
        
        //
        // Request complete training
        //
        
        DoneTrainingRequest * request = [[DoneTrainingRequest alloc] initWithToken:[AppContext sharedInstance].authToken trainingId:currentTraining.trainingId];
        [_display showLoading:@"Subiendo ..."];
        [request post:YES completionBlock:^(id response, NSError *error) {
            [_display hideLoading];
            if (error != nil) {
                [_display showMessage:@"Ha habido un error, inténtelo de nuevo"];
                return;
            }
            
            [_display presentTrainingResultViewController];
        }];
    }
}

@end
