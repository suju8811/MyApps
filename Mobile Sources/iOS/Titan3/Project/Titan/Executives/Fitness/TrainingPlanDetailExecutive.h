//
//  TrainingPlanDetailExecutive.h
//  Titan
//
//  Created by Marcus Lee on 14/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrainingPlan.h"
#import "BaseExecutive.h"

@protocol TrainingPlanDetailDisplay <BaseDisplay>

- (void)didUpdateTrainingPlan:(TrainingPlan*)trainingPlan;

- (void)reloadAllFitness;

@end

@interface TrainingPlanDetailExecutive : NSObject

- (id)initWithDisplay:(id<TrainingPlanDetailDisplay>)display;

- (void)updateTrainingPlan:(TrainingPlan*)trainingPlan;

- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2;

- (void)shareContent:(RLMObject*)content;

- (void)addTrainingPlan:(TrainingPlan*)trainingPlan
                 toDate:(NSDate*)date;

- (void)updateTrainingDayConent:(TrainingDayContent*)trainingDayContent
                 ofTrainingPlan:(TrainingPlan*)trainingPlan
                      startTime:(NSDate*)startTime
                        endTime:(NSDate*)endTime;

@end
