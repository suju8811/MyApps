//
//  ExcercisesExecutive.h
//  Titan
//
//  Created by Marcus Lee on 17/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"
#import "Training.h"

@protocol ExcercisesDisplay <BaseDisplay>

- (void)didLoadExercises:(NSArray*)exercises;

@end

@interface ExcercisesExecutive : NSObject

- (id)initWithDisplay:(id<ExcercisesDisplay>)display;

- (void)loadItemsForTraining:(Training*)training
        exerciseSegmentState:(NSInteger)segmentState;

@end
