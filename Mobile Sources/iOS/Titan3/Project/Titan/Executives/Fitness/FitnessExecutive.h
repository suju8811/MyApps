//
//  FitnessExecutive.h
//  Titan
//
//  Created by Marcus Lee on 15/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"
#import "TrainingPlan.h"

#define ADMIN_OWNER_ID @"pY3QN58pqefLYYX3y"

typedef enum {
    FitnessSelectTypePlan   = 0,
    FitnessSelectTyeWorkout,
    FitnessSelectTypeExercise,
} FitnessSelectType;

typedef enum{
    WorkoutSelectTypeUser,
    WorkoutSelectTypeAdmin,
    WorkoutSelectTypeFavourite,
    WorkoutSelectTypeShared
    
} WorkoutSelectType;

typedef enum{
    TrainingPlanSelectTypeUser,
    TrainingPlanSelectTypeAdmin,
    TrainingPlanSelectTypeFavourite,
    TrainingPlanSelectTypeShared
    
} TrainingPlanSelectType;

@protocol FitnessDisplay <BaseDisplay>

- (void)didLoadItems:(RLMResults*)items
      forFitnessType:(FitnessSelectType)fitnessType;

- (void)didDeletedTrainingPlan:(TrainingPlan*)trainingPlan;

- (void)didDeletedTraining:(Training*)training;

- (void)reloadItems;

@end

@interface FitnessExecutive : NSObject

- (id)initWithDisplay:(id<FitnessDisplay>)display;

- (void)loadItemsForFitnessType:(FitnessSelectType)fitnessType
              workoutSelectType:(WorkoutSelectType)workoutType
         trainingPlanSelectType:(TrainingPlanSelectType)trainingPlanType
           exerciseSegmentState:(NSInteger)segmentState
                     searchText:(NSString*)searchText;

- (void)didPressHeaderSection:(NSInteger)sectionIndex;

- (void)deleteTraining:(Training*)training;

- (void)deleteTrainingPlan:(TrainingPlan*)trainingPlan;

- (void)setAllTagsOff;

- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2;

- (void)shareContent:(RLMObject*)content;

- (void)addTrainingPlan:(TrainingPlan*)trainingPlan
                 toDate:(NSDate*)date;

@end
