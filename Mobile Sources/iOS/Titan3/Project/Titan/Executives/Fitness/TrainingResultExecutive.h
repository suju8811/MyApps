//
//  TrainingResultExecutive.h
//  Titan
//
//  Created by Marcus Lee on 27/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseExecutive.h"

@protocol TrainingResultDisplay <BaseDisplay>

- (void)loadDisplay;

@end

@interface TrainingResultExecutive : NSObject

- (id)initWithDisplay:(id<TrainingResultDisplay>)display;
- (void)displayDidLoad;

@end
