//
//  TrainingPlanDetailExecutive.m
//  Titan
//
//  Created by Marcus Lee on 14/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingPlanDetailExecutive.h"
#import "EditTrainingPlanRequest.h"
#import "SendContentRequest.h"
#import "ShareContentRequest.h"
#import "AppContext.h"
#import "Schedule.h"
#import "DateTimeUtil.h"

@interface TrainingPlanDetailExecutive()

@property id<TrainingPlanDetailDisplay> display;

@end

@implementation TrainingPlanDetailExecutive

- (id)initWithDisplay:(id<TrainingPlanDetailDisplay>)display {
    self = [super init];
    
    _display = display;
    
    return self;
}

- (void)updateTrainingPlan:(TrainingPlan*)trainingPlan {
    
    EditTrainingPlanRequest * request
    = [[EditTrainingPlanRequest alloc] initWithTraining:[AppContext sharedInstance].authToken
                                   trainingPlan:trainingPlan];
    
    [_display showLoading:@"Editando"];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        if (responseArray == nil || responseArray.count == 0) {
            [_display showMessage:@"Failed to update training plan"];
            return;
        }
        
        if (![[[responseArray firstObject] valueForKey:@"active"] boolValue]) {
            [_display showMessage:@"Failed to update training plan"];
            return;
        }
        
        [_display didUpdateTrainingPlan:trainingPlan];
    }];
}

- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2 {
    SendContentRequest * request = [[SendContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                       token2:token2
                                                                    rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadAllFitness];
    }];
}

- (void)shareContent:(RLMObject*)content {
    ShareContentRequest * request = [[ShareContentRequest alloc] initWithToken1:[AppContext sharedInstance].authToken
                                                                      rlmObject:content];
    
    [_display showLoading:@"Subiendo ..."];
    [request post:YES completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        [_display reloadAllFitness];
    }];
}

- (void)addTrainingPlan:(TrainingPlan*)trainingPlan
                 toDate:(NSDate*)date {
    
    //
    // Check the content for date
    //
    
    NSString * checkResult = [Schedule checkContentForDate:date];
    if (checkResult) {
        [_display showMessage:checkResult];
        return;
    }
    
    //
    // Register training plan
    //
    
    if ([Schedule addTrainingPlan:trainingPlan
                           toDate:date]) {
        
        [_display showMessage:@"plan de entrenamiento se registra con éxito."];
    }
}

- (void)updateTrainingDayConent:(TrainingDayContent*)trainingDayContent
                 ofTrainingPlan:(TrainingPlan*)trainingPlan
                      startTime:(NSDate*)startTime
                        endTime:(NSDate*)endTime {
    
    if ([startTime compare:endTime] == NSOrderedDescending ||
        [startTime compare:endTime] == NSOrderedSame) {
        
        [_display showMessage:@"la hora de inicio debe ser anterior a la hora de finalización"];
        return;
    }
    
    RLMRealm *realm = [AppContext currentRealm];
    
    NSString * startTimeString = [DateTimeUtil stringFromDate:startTime
                                                       format:@"HH:mm:ss"];
    NSString * endTimeString = [DateTimeUtil stringFromDate:endTime
                                                     format:@"HH:mm:ss"];
    
    [realm transactionWithBlock:^{
        trainingDayContent.start = startTimeString;
        trainingDayContent.end = endTimeString;
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [self updateTrainingPlan:trainingPlan];
}

@end
