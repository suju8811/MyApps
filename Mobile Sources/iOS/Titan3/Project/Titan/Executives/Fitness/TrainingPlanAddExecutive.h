//
//  TrainingPlanAddExecutive.h
//  Titan
//
//  Created by Marcus Lee on 10/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"
#import "TrainingPlan.h"
#import "BaseExecutive.h"

@interface TrainingPlanAddDisplayContext : NSObject

@property NSString * title;
@property NSString * content;
@property NSString * note;
@property NSMutableArray * selectedTags;
@property NSString * privacy;

@end

@protocol TrainingPlanAddDisplay <BaseDisplay>

- (TrainingPlanAddDisplayContext*)displayContext;
- (void)didPostTrainingPlan:(TrainingPlan*)trainingPlan;
- (void)didUpdateTrainingPlan:(TrainingPlan*)trainingPlan;

@end

@interface TrainingPlanAddExecutive : BaseRequest

- (id)initWithDisplay:(id<TrainingPlanAddDisplay>)display;

- (void)postTrainingPlan:(TrainingPlan*)trainingPlan;

- (void)updateTrainingPlan:(TrainingPlan*)trainingPlan;

- (void)setTagState:(BOOL)state
    forTrainingPlan:(TrainingPlan*)trainingPlan;

@end
