//
//  SeriesExecutive.h
//  Titan
//
//  Created by Marcus Lee on 20/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseExecutive.h"
#import "Serie.h"

@protocol SeriesDisplay <BaseDisplay>

- (void)reloadSeries;

- (void)didCompletedExecutingSeries:(NSInteger)order;

- (void)dismissDisplayWithOrder:(NSInteger)order
                     serieIndex:(NSInteger)serieIndex
                        seconds:(NSInteger)seconds;

@end

@interface SeriesExecutive : NSObject

- (id)initWithDisplay:(id<SeriesDisplay>)display
               series:(NSArray<Serie*>*)series
                order:(NSInteger)order;

- (void)startExecuting;

- (void)pauseExecuting;

- (void)dismissToParent;

@end
