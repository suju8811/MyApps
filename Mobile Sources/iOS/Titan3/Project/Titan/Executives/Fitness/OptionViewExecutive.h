//
//  OptionViewExecutive.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseExecutive.h"
#import "Training.h"
#import "TrainingPlan.h"

@protocol OptionViewDisplay <BaseDisplay>

- (void)didDuplicateTraining:(Training*)training;

- (void)didDeletedTraining:(Training*)training;

- (void)didUpdateTrainingPlan:(TrainingPlan*)training;

- (void)didDuplicateTrainingPlan:(TrainingPlan*)training;

- (void)didDeletedTrainingPlan:(TrainingPlan*)training;

- (void)dismissDisplay;

@end

@interface OptionViewExecutive : NSObject

- (id)initWithDisplay:(id<OptionViewDisplay>)display;

- (void)duplicateTraining:(Training*)training;

- (void)favoriteTraining:(Training*)training;

- (void)deleteTraining:(Training*)training;

- (void)updateTrainingPlan:(TrainingPlan*)trainingPlan;

- (void)duplicateTrainingPlan:(TrainingPlan*)trainingPlan;

- (void)favoriteTrainingPlan:(TrainingPlan*)trainingPlan;

- (void)deleteTrainingPlan:(TrainingPlan*)trainingPlan;

@end
