//
//  WorkoutDetailExecutive.h
//  Titan
//
//  Created by Marcus Lee on 17/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrainingPlan.h"
#import "Training.h"
#import "BaseExecutive.h"

@protocol WorkoutDetailDisplay <BaseDisplay>

- (void)didTrainingUpdate:(Training*)training;

- (void)reloadAllFitness;

- (void)realodTableView;

- (void)presentTrainingResultViewController;

- (void)didPressShowExerciseInCalendar;

@end

@interface WorkoutDetailExecutive : NSObject

@property (nonatomic, assign) NSInteger executingIndex;

- (id)initWithDisplay:(id<WorkoutDetailDisplay>)display;

- (void)updateTraining:(Training*)training;

- (void)sendContent:(RLMObject*)content toToken:(NSString*)token2;

- (void)shareContent:(RLMObject*)content;

- (void)completeExercise:(TrainingExercise*)trainingExercise
                training:(Training*)currentTraining
                 forPlan:(TrainingPlan*)trainingPlan;

@end
