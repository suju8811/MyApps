//
//  TrainingAddExecutive.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Training.h"
#import "BaseExecutive.h"

@interface TrainingAddDisplayContext : NSObject

@property NSString * title;
@property NSString * content;
@property NSString * note;
@property NSArray * selectedTags;
@property NSString * privacy;

@end

@protocol TrainingAddDisplay <BaseDisplay>

- (TrainingAddDisplayContext*)displayContext;
- (void)didTrainingPost:(Training*)training;
- (void)didTrainingUpdate:(Training*)training;

@end

@interface TrainingAddExecutive : NSObject

@property Training * currentTraining;

- (id)initWithDisplay:(id<TrainingAddDisplay>)display;

- (void)postTraining:(Training*)training;

- (void)updateTraining:(Training*)training;

- (void)setTagState:(BOOL)state
        forTraining:(Training*)training;

@end
