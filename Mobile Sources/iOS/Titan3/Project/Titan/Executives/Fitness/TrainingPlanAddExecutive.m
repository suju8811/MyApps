//
//  TrainingPlanAddExecutive.m
//  Titan
//
//  Created by Marcus Lee on 10/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingPlanAddExecutive.h"
#import "AppContext.h"
#import "CreateTrainingPlanRequest.h"
#import "EditTrainingPlanRequest.h"
#import "JSONKit.h"

@interface TrainingPlanAddExecutive()

@property id<TrainingPlanAddDisplay> display;

@end

@implementation TrainingPlanAddExecutive

- (id)initWithDisplay:(id<TrainingPlanAddDisplay>)display {
    self = [super init];
    _display = display;
    return self;
}

- (void)postTrainingPlan:(TrainingPlan*)trainingPlan {
    CreateTrainingPlanRequest * request
    = [[CreateTrainingPlanRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                     trainingPlan:trainingPlan];
    
    [_display showLoading:@"Creando Plan"];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        if (responseArray == nil || responseArray.count == 0) {
            [_display showMessage:@"Failed to create training plan"];
            return;
        }
        
        if (![[[responseArray firstObject] valueForKey:@"active"] boolValue]) {
            [_display showMessage:@"Failed to create training plan"];
            return;
        }
        
        [_display didPostTrainingPlan:trainingPlan];
    }];
}

- (void)updateTrainingPlan:(TrainingPlan*)trainingPlan {
    EditTrainingPlanRequest * request
    = [[EditTrainingPlanRequest alloc] initWithTraining:[AppContext sharedInstance].authToken
                                   trainingPlan:trainingPlan];
    
    [_display showLoading:@"Editando"];
    [request post:NO completionBlock:^(id response, NSError *error) {
        [_display hideLoading];
        if (error != nil) {
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        if (responseArray == nil || responseArray.count == 0) {
            [_display showMessage:@"Failed to update training plan"];
            return;
        }
        
        if (![[[responseArray firstObject] valueForKey:@"active"] boolValue]) {
            [_display showMessage:@"Failed to update training plan"];
            return;
        }
        
        [_display didUpdateTrainingPlan:trainingPlan];
    }];
}

- (void)setTagState:(BOOL)state
    forTrainingPlan:(TrainingPlan*)trainingPlan {
    
    NSString * stateValue = state ? @"On" : @"Off";
    RLMRealm *realm = [AppContext currentRealm];
    
    for(Tag *tag in trainingPlan.tags){
        [realm transactionWithBlock:^{
            [tag setStatus:stateValue];
        }];
    }
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
}

@end

@implementation TrainingPlanAddDisplayContext

@end
