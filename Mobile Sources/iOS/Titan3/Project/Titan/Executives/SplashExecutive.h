//
//  SplashExecutive.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SplashDisplay <NSObject>

- (void)presentLoginViewController;

- (void)presentHomeViewController;

@end

@interface SplashExecutive : NSObject

- (void)displayDidLoad;

- (id)initWithDisplay:(id<SplashDisplay>)display;

@end
