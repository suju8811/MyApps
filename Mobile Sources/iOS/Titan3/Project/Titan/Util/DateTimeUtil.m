//
//  DateTimeUtil.m
//  Titan
//
//  Created by Marcus Lee on 16/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DateTimeUtil.h"

@implementation DateTimeUtil

+ (NSInteger)yearOfDate:(NSDate *)date {
    if (!date) return NSNotFound;
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *component = [calendar components:NSCalendarUnitYear fromDate:date];
    return component.year;
}

+ (NSInteger)monthOfDate:(NSDate *)date {
    if (!date) return NSNotFound;
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *component = [calendar components:NSCalendarUnitMonth
                                              fromDate:date];
    return component.month;
}

+ (NSInteger)dayOfDate:(NSDate *)date {
    if (!date) return NSNotFound;
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *component = [calendar components:NSCalendarUnitDay
                                              fromDate:date];
    return component.day;
}

+ (NSDate*)addDays:(NSInteger)days toDate:(NSDate*)date {
    return
    [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                             value:days
                                            toDate:date
                                           options:0];
}

+ (NSDate*)addMonths:(NSInteger)months toDate:(NSDate*)date {
    return
    [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitMonth
                                             value:months
                                            toDate:date
                                           options:0];
}

+ (NSDate*)addYears:(NSInteger)years toDate:(NSDate*)date {
    return
    [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitYear
                                             value:years
                                            toDate:date
                                           options:0];
}

+ (NSInteger)daysBetween:(NSDate *)dt1 and:(NSDate *)dt2 {
    NSUInteger unitFlags = NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:unitFlags fromDate:dt1 toDate:dt2 options:0];
    return components.day;
}

+ (NSString*)stringFromDate:(NSDate*)date
                     format:(NSString*)format {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    return [formatter stringFromDate:date];
}

+ (NSDate*)dateFromString:(NSString*)dateString
                   format:(NSString*)format {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    return [formatter dateFromString:dateString];
}

+ (NSString*)spanishStringFromDate:(NSDate*)date
                            format:(NSString*)format {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = format;
    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];
    return [formatter stringFromDate:date];
}

@end
