//
//  Schedule.h
//  Titan
//
//  Created by Marcus Lee on 16/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NutritionPlan.h"
#import "TrainingPlan.h"
#import "Event.h"

typedef enum : NSUInteger {
    CalendarInfoTypeNone,
    CalendarInfoTypeTrainingPlan,
    CalendarInfoTypeNutritionPlan,
    CalendarInfoTypeEvent,
    CalendarInfoTypeRepose
} CalendarInfoType;

@interface Schedule : NSObject

+ (void)getSchedules;
+ (void)getSchedulesWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock;
+ (void)getSchedulesWithYear:(NSString*)year
                       month:(NSString*)month
             completionBlock:(void (^)(BOOL isSuccess)) completionBlock;

+ (NSString*)checkContentForDate:(NSDate*)date;

+ (BOOL)addTrainingPlans:(NSArray*)trainingPlans;

+ (BOOL)addTrainingPlan:(TrainingPlan*)trainingPlan
                 toDate:(NSDate*)date;

+ (void)addTrainingPlan:(TrainingPlan*)trainingPlan
                 toDate:(NSDate*)date
        completionBlock:(void (^)(BOOL isSuccess)) completionBlock;

+ (BOOL)addNutritionPlans:(NSArray*)nutritionPlans;

+ (BOOL)addNutritionPlan:(NutritionPlan*)nutritionPlan
                  toDate:(NSDate*)date;

+ (void)addNutritionPlan:(NutritionPlan*)nutritionPlan
                  toDate:(NSDate*)date
         completionBlock:(void (^)(BOOL isSuccess)) completionBlock;

+ (void)createEvent:(Event*)event
    completionBlock:(void (^)(id response, NSError * error)) completionBlock;

+ (TrainingPlan*)trainingPlanForDate:(NSDate*)dateToCheck;

+ (TrainingPlan*)reposePlanForDate:(NSDate*)dateToCheck;

+ (NutritionPlan*)nutritionPlanForDate:(NSDate*)dateToCheck;

+ (BOOL)checkHaveTraininigContentInDays:(NSInteger)days
                               fromDate:(NSDate*)dateToCheck;

+ (BOOL)checkHaveNutritionContentInDays:(NSInteger)days
                               fromDate:(NSDate*)dateToCheck;

+ (NSDictionary*)calendarDataForDate:(NSDate*)selectedDate;

+ (BOOL)hasCalendarData:(NSDate*)dateToCheck;

+ (NSArray*)trainingSchedulesBetweenDate:(NSDate*)fromDate date:(NSDate*)toDate;

@end
