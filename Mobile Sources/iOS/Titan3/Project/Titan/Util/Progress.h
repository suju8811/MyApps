//
//  Progress.h
//  Titan
//
//  Created by Marcus Lee on 29/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Progress : NSObject

+ (void)getLogs;
+ (void)getLogsWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock;

@end
