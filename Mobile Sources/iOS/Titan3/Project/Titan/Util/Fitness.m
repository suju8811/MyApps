//
//  Fitness.m
//  Titan
//
//  Created by Marcus Lee on 27/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Fitness.h"

@implementation Fitness

+ (TrainingExerciseStatus)trainingExerciseStatus:(TrainingExercise*)trainingExercise {
    
    TrainingExerciseStatus status = TrainingExerciseStatusNone;
    for (NSInteger index = 0; index < trainingExercise.series.count; index++) {
        Serie * serie = trainingExercise.series[index];
        if (serie.executingStatus.intValue == 4) {
            if (index == trainingExercise.series.count - 1) {
                status = TrainingExerciseStatusCompleted;
                break;
            }
        }
    }
    
    return status;
}

+ (TrainingStatus)trainingStatus:(Training*)training {
    for (TrainingExercise * trainingExercise in training.trainingExercises) {
        TrainingExerciseStatus trainingExerciseStatus = [Fitness trainingExerciseStatus:trainingExercise];
        if (trainingExerciseStatus == TrainingExerciseStatusNone) {
            return TrainingStatusNone;
        }
        if (trainingExerciseStatus == TrainingExerciseStatusExecuting) {
            return TrainingStatusExecuting;
        }
    }
    
    return TrainingStatusCompleted;
}

@end
