//
//  Schedule.m
//  Titan
//
//  Created by Marcus Lee on 16/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Schedule.h"
#import "SchedulePlan.h"
#import "UtilManager.h"
#import "Event.h"
#import "AppContext.h"
#import "CreateEventRequest.h"
#import "DateTimeUtil.h"
#import "GetSchedulesRequest.h"
#import "CreateTrainingPlanRequest.h"
#import "CreateNutritionPlanRequest.h"
#import "ParserManager.h"

@implementation Schedule

+ (void)getSchedules {
    [Schedule getSchedulesWithCompletionBlock:nil];
}

+ (void)getSchedulesWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock {
    [Schedule getSchedulesWithYear:nil month:nil completionBlock:completionBlock];
}

+ (void)getSchedulesWithYear:(NSString*)year
                       month:(NSString*)month
             completionBlock:(void (^)(BOOL isSuccess)) completionBlock {
    
    GetSchedulesRequest * request;
    if (year == nil || month == nil) {
        request =
        [[GetSchedulesRequest alloc] initWithToken:[AppContext sharedInstance].authToken];
    }
    else {
        request =
        [[GetSchedulesRequest alloc] initWithToken:[AppContext sharedInstance].authToken year:year month:month];
    }
    
    [request post:NO
  completionBlock:^(id response, NSError *error) {
      
      if (error != nil) {
          if (completionBlock != nil) {
              completionBlock(NO);
          }
          return;
      }
      
      JSONDecoder * decoder = [[JSONDecoder alloc] init];
      NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
      [ParserManager parseSchedule:responseArray];
      if (completionBlock != nil) {
          completionBlock(YES);
      }
  }];
}

+ (NSString*)checkContentForDate:(NSDate*)dateToCheck {

    NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:dateToCheck options:0];
    
    //
    // Check if the training plan is already registered.
    //
    
    SchedulePlan * schedulePlan = [SchedulePlan getScheduleTrainingPlanWithScheduleDate:date];
    if (schedulePlan != nil) {
        return @"el plan ya está registrado para este día";
    }
    
    //
    // Check if the nutrition plan is already registered.
    //
    
    schedulePlan = [SchedulePlan getScheduleNutritionPlanWithScheduleDate:date];
    if (schedulePlan != nil) {
        return @"el plan ya está registrado para este día";
    }
    
    //
    // Check if the event is already registered
    //
    
    Event * event = [Event getEventWithScheduleDate:date];
    if (event) {
        return @"el evento ya está registrado para este día";
    }
    
    //
    // Check if the repose is already registered
    //
    
    TrainingPlan * repose = [SchedulePlan getReposeWithScheduleDate:date];
    if (repose) {
        return @"el reposo ya está registrado para este día";
    }
    
    return nil;
}

+ (BOOL)addTrainingPlans:(NSArray*)trainingPlans {
    NSMutableArray * schedulePlanAux = [[NSMutableArray alloc] init];
    for (TrainingPlan * trainingPlan in trainingPlans) {
        SchedulePlan * schedulePlan = [[SchedulePlan alloc] init];
        NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:trainingPlan.startDate options:0];
        
        schedulePlan.schedulePlanId = [UtilManager uuid];
        schedulePlan.scheduleDate = date;
        schedulePlan.plan = trainingPlan;
        [schedulePlanAux addObject:schedulePlan];
    }
    
    return [SchedulePlan saveSchedulePlans:schedulePlanAux];
}

+ (void)addTrainingPlan:(TrainingPlan*)trainingPlan
                 toDate:(NSDate*)date
        completionBlock:(void (^)(BOOL isSuccess)) completionBlock {
    
    TrainingPlan * copiedTrainingPlan = [TrainingPlan copyTrainingPlan:trainingPlan];
    copiedTrainingPlan.startDate = date;
    copiedTrainingPlan.endDate = [DateTimeUtil addDays:copiedTrainingPlan.trainingDay.count toDate:date];
    copiedTrainingPlan.isCalendarCopy = @YES;
    
    CreateTrainingPlanRequest * request
    = [[CreateTrainingPlanRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                  trainingPlan:copiedTrainingPlan];
    
    [request post:NO completionBlock:^(id response, NSError *error) {
        if (error != nil) {
            completionBlock(NO);
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        if (responseArray == nil || responseArray.count == 0) {
            completionBlock(NO);
            return;
        }
        
        if (![[[responseArray firstObject] valueForKey:@"active"] boolValue]) {
            completionBlock(NO);
            return;
        }
        
        NSInteger currentYear = [DateTimeUtil yearOfDate:date];
        NSInteger currentMonth = [DateTimeUtil monthOfDate:date];
        [Schedule getSchedulesWithYear:[NSString stringWithFormat:@"%d", (int)currentYear]
                                 month:[NSString stringWithFormat:@"%d", (int)currentMonth]
                       completionBlock:^(BOOL isSuccess) {
                           completionBlock(isSuccess);
                       }];
    }];
}

+ (BOOL)addTrainingPlan:(TrainingPlan*)trainingPlan
                 toDate:(NSDate*)dateToAdd {
    
    NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:dateToAdd options:0];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    SchedulePlan * schedulePlan = [[SchedulePlan alloc] init];
    schedulePlan.schedulePlanId = [UtilManager uuid];
    schedulePlan.scheduleDate = date;
    schedulePlan.plan = trainingPlan;
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:schedulePlan];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if (error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (void)addNutritionPlan:(NutritionPlan*)nutritionPlan
                  toDate:(NSDate*)date
         completionBlock:(void (^)(BOOL isSuccess)) completionBlock {
    
    NutritionPlan * copiedNutritionPlan = [NutritionPlan duplicateNutritionPlan:nutritionPlan];
    copiedNutritionPlan.startDate = date;
    copiedNutritionPlan.endDate = [DateTimeUtil addDays:copiedNutritionPlan.nutritionDays.count toDate:date];
    copiedNutritionPlan.isCalendarCopy = @YES;
    
    CreateNutritionPlanRequest * request
    = [[CreateNutritionPlanRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                          nutritionPlan:copiedNutritionPlan];
    
    [request post:NO completionBlock:^(id response, NSError *error) {
        if (error != nil) {
            completionBlock(NO);
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        if (responseArray == nil || responseArray.count == 0) {
            completionBlock(NO);
            return;
        }
        
        if (![[[responseArray firstObject] valueForKey:@"active"] boolValue]) {
            completionBlock(NO);
            return;
        }
        
        NSInteger currentYear = [DateTimeUtil yearOfDate:date];
        NSInteger currentMonth = [DateTimeUtil monthOfDate:date];
        [Schedule getSchedulesWithYear:[NSString stringWithFormat:@"%d", (int)currentYear]
                                 month:[NSString stringWithFormat:@"%d", (int)currentMonth]
                       completionBlock:^(BOOL isSuccess) {
                           completionBlock(isSuccess);
        }];
    }];
}

+ (BOOL)addNutritionPlans:(NSArray*)nutritionPlans {
    NSMutableArray * schedulePlanAux = [[NSMutableArray alloc] init];
    for (NutritionPlan * nutritionPlan in nutritionPlans) {
        SchedulePlan * schedulePlan = [[SchedulePlan alloc] init];
        NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:nutritionPlan.startDate options:0];
        
        schedulePlan.schedulePlanId = [UtilManager uuid];
        schedulePlan.scheduleDate = date;
        schedulePlan.nutritionPlan = nutritionPlan;
        [schedulePlanAux addObject:schedulePlan];
    }
    
    return [SchedulePlan saveSchedulePlans:schedulePlanAux];
}

+ (BOOL)addNutritionPlan:(NutritionPlan*)nutritionPlan
                  toDate:(NSDate*)dateToAdd {
    
    NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:dateToAdd options:0];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    SchedulePlan * schedulePlan = [[SchedulePlan alloc] init];
    schedulePlan.schedulePlanId = [UtilManager uuid];
    schedulePlan.scheduleDate = date;
    schedulePlan.nutritionPlan = nutritionPlan;
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:schedulePlan];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (void)createEvent:(Event*)event
    completionBlock:(void (^)(id response, NSError * error)) completionBlock {
    
    CreateEventRequest * request = [[CreateEventRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                                       event:event];
    
    [request post:YES completionBlock:^(id response, NSError *error) {
        completionBlock(response, error);
        
        NSInteger currentYear = [DateTimeUtil yearOfDate:event.startDate];
        NSInteger currentMonth = [DateTimeUtil monthOfDate:event.startDate];
        [Schedule getSchedulesWithYear:[NSString stringWithFormat:@"%d", (int)currentYear]
                                 month:[NSString stringWithFormat:@"%d", (int)currentMonth]
                       completionBlock:^(BOOL isSuccess) {
                           completionBlock(response, error);
                       }];
    }];
}

+ (TrainingPlan*)trainingPlanForDate:(NSDate*)dateToCheck {
    NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:dateToCheck options:0];
    SchedulePlan * schedulePlan = [SchedulePlan getScheduleTrainingPlanWithScheduleDate:date];
    if (schedulePlan == nil) {
        return nil;
    }
    
    return schedulePlan.plan;
}

+ (TrainingPlan*)reposePlanForDate:(NSDate*)dateToCheck {
    NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:dateToCheck options:0];
    TrainingPlan * trainingPlan = [SchedulePlan getReposeWithScheduleDate:date];
    return trainingPlan;
}

+ (NutritionPlan*)nutritionPlanForDate:(NSDate*)dateToCheck {
    NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:dateToCheck options:0];
    SchedulePlan * schedulePlan = [SchedulePlan getScheduleNutritionPlanWithScheduleDate:date];
    if (schedulePlan == nil) {
        return nil;
    }
    
    return schedulePlan.nutritionPlan;
}

+ (BOOL)checkHaveTraininigContentInDays:(NSInteger)days
                      fromDate:(NSDate*)dateToCheck {
    
    NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:dateToCheck options:0];
    for (NSInteger day = 0; day <= days; day++) {
        NSDate * iteratorDate = [DateTimeUtil addDays:day toDate:date];
        TrainingPlan * repose = [SchedulePlan getReposeWithScheduleDate:iteratorDate];
        if (repose != nil) {
            return YES;
        }
        
        TrainingPlan * trainingPlan = [Schedule trainingPlanForDate:iteratorDate];
        if (trainingPlan != nil) {
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL)checkHaveNutritionContentInDays:(NSInteger)days
                               fromDate:(NSDate*)dateToCheck {
    
    NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:dateToCheck options:0];
    for (NSInteger day = 0; day <= days; day++) {
        NSDate * iteratorDate = [DateTimeUtil addDays:day toDate:date];
        TrainingPlan * repose = [SchedulePlan getReposeWithScheduleDate:iteratorDate];
        if (repose != nil) {
            return YES;
        }
        
        SchedulePlan * nutritionPlan = [SchedulePlan getScheduleNutritionPlanWithScheduleDate:iteratorDate];
        if (nutritionPlan != nil) {
            return YES;
        }
    }
    
    return NO;
}

+ (NSDictionary*)calendarDataForDate:(NSDate*)selectedDate {
    NSMutableDictionary * calendarData = [[NSMutableDictionary alloc] init];
    
    //
    // Get training plan
    //
    
    SchedulePlan * trainingPlanSchedule = [SchedulePlan getScheduleTrainingPlanWithScheduleDate:selectedDate];
    if (trainingPlanSchedule != nil) {
        [calendarData setValue:trainingPlanSchedule forKey:@"TrainingPlan"];
    }
    else {
        [calendarData setValue:@"empty" forKey:@"TrainingPlan"];
    }
    
    //
    // Get nutrition plan
    //
    
    SchedulePlan * nutritionPlanSchedule = [SchedulePlan getScheduleNutritionPlanWithScheduleDate:selectedDate];
    if (nutritionPlanSchedule != nil) {
        [calendarData setValue:nutritionPlanSchedule forKey:@"NutritionPlan"];
    }
    else {
        [calendarData setValue:@"empty" forKey:@"NutritionPlan"];
    }
    
    //
    // Get events
    //
    
    Event * event = [Event getEventWithScheduleDate:selectedDate];
    if (event) {
        [calendarData setValue:event forKey:@"Event"];
    }
    else {
        [calendarData setValue:@"empty" forKey:@"Event"];
    }
    
    //
    // Get repose
    //
    
    TrainingPlan * repose = [SchedulePlan getReposeWithScheduleDate:selectedDate];
    if (repose) {
        [calendarData setValue:repose forKey:@"Repose"];
    }
    else {
        [calendarData setValue:@"empty" forKey:@"Repose"];
    }
    
    return calendarData;
}

+ (BOOL)hasCalendarData:(NSDate*)dateToCheck {
    NSDate * date = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:dateToCheck options:0];
    NSDictionary * calendarData = [Schedule calendarDataForDate:date];
    
    NSArray * dataKeys = [calendarData allKeys];
    for (NSString * dataKey in dataKeys) {
        if (![[calendarData valueForKey:dataKey] isKindOfClass:[NSString class]]) {
            return YES;
        }
    }
    
    return NO;
}

+ (NSArray*)trainingSchedulesBetweenDate:(NSDate*)fromDate date:(NSDate*)toDate {
    NSMutableArray * trainingSchedules = [[NSMutableArray alloc] init];
    
    RLMResults<SchedulePlan *> *schedulePlans = [SchedulePlan objectsWhere:@"scheduleDate >= %@ OR scheduleDate <= %@", fromDate, toDate];
    for (SchedulePlan * schedulePlan in schedulePlans) {
        if (schedulePlan.plan) {
            [trainingSchedules addObject:schedulePlan];
        }
    }
    
    return trainingSchedules;
}

@end
