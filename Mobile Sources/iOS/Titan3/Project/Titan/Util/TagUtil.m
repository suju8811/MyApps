//
//  TagUtil.m
//  Titan
//
//  Created by Marcus Lee on 30/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TagUtil.h"
#import "GetTagsRequest.h"
#import "AppContext.h"
#import "JSONKit.h"
#import "Tag.h"

@implementation TagUtil

+ (void)getTags {
    [TagUtil getTagsWithCompletionBlock:nil];
}

+ (void)getTagsWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock {
    GetTagsRequest * request = [[GetTagsRequest alloc] initWithToken:[AppContext sharedInstance].authToken];
    
    [request get:NO
 completionBlock:^(id response, NSError *error) {
     
     if (error != nil) {
         if (completionBlock != nil) {
             completionBlock(NO);
         }
         return;
     }
     
     JSONDecoder * decoder = [[JSONDecoder alloc] init];
     NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
     [TagUtil parseTags:responseArray];
     
     if (completionBlock != nil) {
         completionBlock(NO);
     }
 }];
}


+ (void)parseTags:(NSArray<NSDictionary*>*)responseArray {
    NSMutableArray * tagsAux = [[NSMutableArray alloc] init];
    
    for(NSDictionary * tagDictionary in responseArray){
        Tag * tag = [[Tag alloc] init];
        [tag parseFromDictionary:tagDictionary];
        [tagsAux addObject:tag];
    }
    
    [Tag saveTags:tagsAux];
}

+ (NSMutableArray *)getTagsWithFitnessSelectorType:(SelectorType)selectorType {
    NSString * type;
    switch (selectorType) {
        case SelectorTypeExercise:
        case SelectorTypeExerciseSingle:
            type = @"1";
            break;
        case SelectorTypeTrainings:
        case SelectorTypeTrainingSingle:
        case SelectorTypeTrainingDay:
            type = @"2";
            break;
        case SelectorTypeTrainingPlan:
            type = @"3";
        default:
            type = @"";
            break;
    }
    
    RLMResults<Tag *> *tags = [Tag objectsWhere:[NSString stringWithFormat:@"type = '%@'", type]];
    
    NSMutableArray *tagsArray = [[NSMutableArray alloc] initWithCapacity:tags.count];
    
    for(Tag *tag in tags){
        [tagsArray addObject:tag];
    }
    
    return tagsArray;
}

+ (NSMutableArray *)getTagsWithFoodSelectorType:(SelectorFoodType)selectorType {
    NSString * type;
    switch (selectorType) {
        case SelectorFoodTypeMeal:
        case SelectorFoodTypeMealSingle:
            type = @"4";
            break;
        case SelectorFoodTypeDiet:
        case SelectorFoodTypeDietSingle:
            type = @"6";
            break;
        case SelectorFoodTypeNutritionPlan:
        case SelectorFoodTypeNutritionPlanSingle:
            type = @"7";
            break;
        case SelectorFoodTypeNutritionPlanCalendar:
            type = @"8";
        default:
            type = @"";
            break;
    }
    
    RLMResults<Tag *> *tags = [Tag objectsWhere:[NSString stringWithFormat:@"type = '%@'", type]];
    
    NSMutableArray *tagsArray = [[NSMutableArray alloc] initWithCapacity:tags.count];
    
    for(Tag *tag in tags){
        [tagsArray addObject:tag];
    }
    
    return tagsArray;
}

@end
