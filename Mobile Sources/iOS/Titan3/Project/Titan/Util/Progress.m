//
//  Progress.m
//  Titan
//
//  Created by Marcus Lee on 29/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Progress.h"
#import "ParserManager.h"
#import "GetLogRequest.h"
#import "AppContext.h"
#import "JSONKit.h"

@implementation Progress

+ (void)getLogs {
    [Progress getLogsWithCompletionBlock:nil];
}

+ (void)getLogsWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock {
    
    GetLogRequest * request = [[GetLogRequest alloc] initWithToken:[AppContext sharedInstance].authToken];
    
    [request get:NO
 completionBlock:^(id response, NSError *error) {
     
     if (error != nil) {
         return;
     }
     
     JSONDecoder * decoder = [[JSONDecoder alloc] init];
     NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
     [ParserManager parseUserLogs:responseArray];
     
     if (completionBlock != nil) {
         completionBlock(NO);
     }
 }];
}

@end
