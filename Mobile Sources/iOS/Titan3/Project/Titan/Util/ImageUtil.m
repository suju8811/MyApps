//
//  ImageUtil.m
//  Titan
//
//  Created by Marcus Lee on 11/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "ImageUtil.h"

@implementation ImageUtil

+ (UIColor*)RGBAFromImage:(UIImage*)image
                      atx:(int)xp
                      atY:(int)yp
               renderSize:(CGSize)renderSize {
    
    CGImageRef imageRef = [image CGImage];
//    NSUInteger width = CGImageGetWidth(imageRef);
//    NSUInteger height = CGImageGetHeight(imageRef);
    NSUInteger renderWidth = renderSize.width;
    NSUInteger renderHeight = renderSize.height;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char * rawData = (unsigned char*) calloc(renderHeight * renderWidth * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * renderWidth;
    NSUInteger bitsPerComponenet = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, renderWidth, renderHeight,
                                                 bitsPerComponenet, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, renderWidth, renderHeight), imageRef);
    CGContextRelease(context);
    
    int byteIndex = (int)((bytesPerRow * yp) + xp * bytesPerPixel);
    CGFloat red = (rawData[byteIndex] * 1.0) / 255.0;
    CGFloat green = (rawData[byteIndex + 1] * 1.0) / 255.0;
    CGFloat blue = (rawData[byteIndex + 2] * 1.0) / 255.0;
    CGFloat alpha = (rawData[byteIndex + 3] * 1.0) / 255.0;
    byteIndex += 4;
    UIColor * color = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
    
//    NSLog(@"width: %i height:%i Color:%i", width, height, [color description]);
    
    free(rawData);
    
    return color;
}

@end
