//
//  DateTimeUtil.h
//  Titan
//
//  Created by Marcus Lee on 16/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateTimeUtil : NSObject

+ (NSInteger)yearOfDate:(NSDate *)date;
+ (NSInteger)monthOfDate:(NSDate *)date;
+ (NSInteger)dayOfDate:(NSDate *)date;

+ (NSDate*)addDays:(NSInteger)days toDate:(NSDate*)date;
+ (NSDate*)addMonths:(NSInteger)months toDate:(NSDate*)date;
+ (NSDate*)addYears:(NSInteger)years toDate:(NSDate*)date;

+ (NSInteger)daysBetween:(NSDate *)dt1 and:(NSDate *)dt2;
+ (NSString*)stringFromDate:(NSDate*)date
                     format:(NSString*)format;

+ (NSDate*)dateFromString:(NSString*)dateString
                   format:(NSString*)format;

+ (NSString*)spanishStringFromDate:(NSDate*)date
                     format:(NSString*)format;

@end
