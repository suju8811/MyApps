//
//  Fitness.h
//  Titan
//
//  Created by Marcus Lee on 27/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingPlan.h"

typedef enum : NSUInteger {
    TrainingExerciseStatusNone,
    TrainingExerciseStatusCompleted,
    TrainingExerciseStatusExecuting
} TrainingExerciseStatus;

typedef enum : NSUInteger {
    TrainingStatusNone,
    TrainingStatusCompleted,
    TrainingStatusExecuting
} TrainingStatus;

@interface Fitness : NSObject

+ (TrainingExerciseStatus)trainingExerciseStatus:(TrainingExercise*)trainingExercise;

+ (TrainingStatus)trainingStatus:(Training*)training;

@end
