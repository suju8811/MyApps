//
//  TagUtil.h
//  Titan
//
//  Created by Marcus Lee on 30/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SelectorTableViewController.h"
#import "SelectorFoodExecutive.h"

@interface TagUtil : NSObject

+ (void)getTags;
+ (void)getTagsWithCompletionBlock:(void (^)(BOOL isSuccess)) completionBlock;
+ (NSMutableArray *)getTagsWithFitnessSelectorType:(SelectorType)selectorType;
+ (NSMutableArray *)getTagsWithFoodSelectorType:(SelectorFoodType)selectorType;

@end
