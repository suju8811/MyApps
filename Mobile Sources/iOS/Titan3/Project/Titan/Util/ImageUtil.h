//
//  ImageUtil.h
//  Titan
//
//  Created by Marcus Lee on 11/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageUtil : NSObject

+ (UIColor*)RGBAFromImage:(UIImage*)image
                      atx:(int)xp
                      atY:(int)yp
               renderSize:(CGSize)renderSize;

@end
