//
//  ReposeViewController.h
//  Titan
//
//  Created by Marcus Lee on 14/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ParentViewController.h"
#import "TrainingPlan.h"

@interface ReposeViewController : ParentViewController

//
// Variables
//

@property (nonatomic, strong) NSDate * selectedDate;
@property (nonatomic, strong) TrainingPlan * repose;

//
// IBOutlets
//

@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIView *typeView;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *endDateButton;
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;
@property (weak, nonatomic) IBOutlet UISwitch *postponeSwitch;

@end
