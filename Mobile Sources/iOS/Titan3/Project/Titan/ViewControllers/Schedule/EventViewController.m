//
//  EventViewController.m
//  Titan
//
//  Created by Marcus Lee on 14/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "EventViewController.h"
#import "EventExecutive.h"
#import "UtilManager.h"

@interface EventViewController () <EventDisplay, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) EventExecutive * executive;

@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UIButton *acceptButton;

@property (nonatomic, strong) UIPickerView *repeatPicker;
@property (nonatomic, assign) NSInteger repeatMode;
@property (nonatomic, assign) NSInteger eventType;
@property (nonatomic, strong) NSDate *startDate;

@end

@implementation EventViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _repeatMode = 0;
    _eventType = 0;
    _startDate = _selectedDate;
    if (_event) {
        _repeatMode = _event.repeat.integerValue;
        _startDate = _event.startDate;
        _eventType = _event.type.integerValue;
    }
    
    _executive = [[EventExecutive alloc] initWithDisplay:self];
    [_executive displayDidLoad];
}

#pragma mark - Actions

- (IBAction)didPressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - EventDisplay

- (void)loadDisplay {
    [_titleTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    _titleTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Título", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName:[UIColor grayColor],
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]
                                                 }
     ];
    
    _noteTextView.keyboardAppearance = UIKeyboardAppearanceDark;
    _noteTextView.layer.borderWidth = 1;
    _noteTextView.layer.borderColor = BLACK_HEADER_APP_COLOR.CGColor;
    _noteTextView.text = NSLocalizedString(@"Notas", nil);
    
    UIToolbar *toolbarWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetTextView)];
    [barButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarWeigth setItems:[NSArray arrayWithObjects:flexibleWeigthSpace, barButtonItem, nil]];
    _noteTextView.inputAccessoryView = toolbarWeigth;
    
    [self loadValues];
}

- (Event*)eventFromDisplay {
    if (_event) {
        RLMRealm *realm = [AppContext currentRealm];
        [realm transactionWithBlock:^{
            [self setPropertiesToEvent:_event];
        }];
        
        [realm beginWriteTransaction];
        NSError *error;
        [realm commitWriteTransaction:&error];
        if(error) {
            NSLog(@"%@",error.description);
        }
        return _event;
    }
    else {
        Event * event = [[Event alloc] init];
        event.eventId = [UtilManager uuid];
        [self setPropertiesToEvent:event];
        return event;
    }
}

- (void)didCreateEvent {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setPropertiesToEvent:(Event*)event {
    event.title = _titleTextField.text;
    event.type = @(_eventType);
    event.note = _noteTextView.text;
    event.startDate = _startDate;
    event.selectedDate = _selectedDate;
    event.repeat = @(_repeatMode);
    event.days = @1;
}

- (void)loadValues {
    if (_event) {
        _titleTextField.text = _event.title;
        _okButton.hidden = YES;
        _titleTextField.userInteractionEnabled = NO;
        _typeView.userInteractionEnabled = NO;
        _dateSelectButton.userInteractionEnabled = NO;
        _repeatButton.userInteractionEnabled = NO;
        _noteTextView.text = _event.note;
        _noteTextView.userInteractionEnabled = NO;
    }
    
    [self selectEventTypeButton];
    
    
    if(_startDate) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];
        formatter.dateFormat = @"MMM, d E yyyy HH:mm";
        _dateLabel.text = [formatter stringFromDate:_startDate];
    }
}

#pragma mark - Actions

- (IBAction)didPressSelectDateButton:(id)sender {
    [self.view endEditing:YES];
    
    if(_datePicker) {
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - 216, self.view.frame.size.width, 216)];
    _datePicker.date = _startDate;
    _datePicker.datePickerMode = UIDatePickerModeTime;
    _datePicker.backgroundColor = UIColor.whiteColor;
    _datePicker.userInteractionEnabled = YES;
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_datePicker.frame.size.width - 80., _datePicker.frame.origin.y, 80., 24.)];
    [_acceptButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptDatePickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_datePicker];
    [self.view addSubview:_acceptButton];
}

- (void)acceptDatePickerAction {
    
    if(_acceptButton && _datePicker) {
        NSCalendar * calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:_datePicker.date];
        _startDate = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:components.hour minute:components.minute second:0 ofDate:_startDate options:0];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];
        formatter.dateFormat = @"MMM, d E yyyy HH:mm";
        _dateLabel.text = [formatter stringFromDate:_startDate];
        
        [_acceptButton removeFromSuperview];
        _acceptButton = nil;
        
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
}
- (IBAction)didPressRepeatButton:(id)sender {
    [self.view endEditing:YES];
    
    if(_repeatPicker) {
        [_repeatPicker removeFromSuperview];
        _repeatPicker = nil;
    }
    
    _repeatPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - 216, self.view.frame.size.width, 216)];
    [_repeatPicker setBackgroundColor:[UIColor whiteColor]];
    [_repeatPicker setUserInteractionEnabled:YES];
    [_repeatPicker setDelegate:self];
    [_repeatPicker setDataSource:self];
    [_repeatPicker selectRow:_repeatMode inComponent:0 animated:YES];
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_repeatPicker.frame.size.width - 80., _repeatPicker.frame.origin.y, 80., 24.)];
    [_acceptButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptPickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_repeatPicker];
    [self.view addSubview:_acceptButton];
}

- (void)acceptPickerAction {
    [self.view endEditing:YES];
    
    _repeatMode = [_repeatPicker selectedRowInComponent:0];
    
    [_repeatButton setTitle:[NSString stringWithFormat:@"%@ >",
                             [_executive repeatMenus][_repeatMode]]
                   forState:UIControlStateNormal];
    
    [_repeatPicker removeFromSuperview];
    _repeatPicker = nil;
    
    [_acceptButton removeFromSuperview];
    _acceptButton = nil;
}

- (IBAction)didPressTypeButton:(UIButton *)sender {
    if (_eventType == sender.tag) {
        return;
    }
    
    _eventType = sender.tag;
    [self selectEventTypeButton];
}

- (IBAction)didPressOKButton:(id)sender {
    [_executive didPressOKButton];
}

- (void)selectEventTypeButton {
    UIImageView * imageView;
    UIImage * image;
    UILabel * typeLabel;
    for (NSInteger tagIndex = 1; tagIndex <= 4; tagIndex++) {
        imageView = (UIImageView*)[_typeView viewWithTag:tagIndex * 10];
        image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        imageView.tintColor = UIColor.whiteColor;
        imageView.image = image;
        
        typeLabel = (UILabel*)[_typeView viewWithTag:tagIndex * 100];
        typeLabel.textColor = UIColor.whiteColor;
    }
    
    imageView = (UIImageView*)[_typeView viewWithTag:(_eventType + 1)*10];
    image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    imageView.tintColor = UIColor.yellowColor;
    imageView.image = image;
    
    typeLabel = (UILabel*)[_typeView viewWithTag:(_eventType + 1) * 100];
    typeLabel.textColor = UIColor.yellowColor;
}

#pragma mark - UIPickerViewDelegate and UIPickerViewDataSource

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_executive repeatMenus].count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [_executive repeatMenus][row];
}

#pragma mark - Delegate For Text Field and Text View

- (void)resetTextView {
    [_noteTextView resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
    
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    if([[textView text] isEqualToString:NSLocalizedString(@"Notas", nil)]) {
        [_noteTextView setText:@""];
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

@end
