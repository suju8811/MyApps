//
//  ReposeViewController.m
//  Titan
//
//  Created by Marcus Lee on 14/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ReposeViewController.h"
#import "ReposeExecutive.h"
#import "DateTimeUtil.h"
#import "WebServiceManager.h"

@interface ReposeViewController () <ReposeDisplay>

@property (nonatomic, strong) ReposeExecutive * executive;

@property (nonatomic, assign) NSInteger reposeType;

@property (nonatomic, strong) UIButton *acceptButton;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) NSDate *endDate;

@end

@implementation ReposeViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _reposeType = 0;
    if (_repose) {
        TrainingDay * reposeDay = [_repose.trainingDay firstObject];
        _reposeType = reposeDay.restDay.integerValue;
        _selectedDate = _repose.startDate;
        _endDate = _repose.endDate;
    }
    
    _executive = [[ReposeExecutive alloc] initWithDisplay:self];
    [_executive displayDidLoad];
}

#pragma mark - Actions

- (IBAction)didPressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didPressOKButton:(id)sender {
    [_executive didPressOKButton];
}

- (IBAction)didPressEndDateButton:(id)sender {
    [self.view endEditing:YES];
    
    if(_datePicker) {
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - 216, self.view.frame.size.width, 216)];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    if (_endDate) {
        [_datePicker setDate:_endDate];
    }
    else {
        [_datePicker setDate:_selectedDate];
    }
    
    if (_selectedDate) {
        _datePicker.minimumDate = _selectedDate;
    }
    else {
        _datePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:-60*60*24*365 * 75];
    }
    _datePicker.maximumDate = [NSDate dateWithTimeIntervalSinceNow:60*60*24*365 * 18];
    
    _datePicker.backgroundColor = [UIColor whiteColor];
    _datePicker.userInteractionEnabled = YES;
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_datePicker.frame.size.width - 80., _datePicker.frame.origin.y, 80., 24.)];
    [_acceptButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptDatePickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_datePicker];
    [self.view addSubview:_acceptButton];
}

- (void)acceptDatePickerAction {
    if(_acceptButton && _datePicker) {
        if ([_datePicker.date compare:_selectedDate] == NSOrderedAscending) {
            [self showMessage:@"La fecha de finalización debe ser posterior a la fecha seleccionada"];
            return;
        }
        
        _endDate = _datePicker.date;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];
        dateFormatter.dateFormat = @"MMM, d E yyyy";
        _endDateLabel.text = [dateFormatter stringFromDate:_endDate];
        
        [_acceptButton removeFromSuperview];
        _acceptButton = nil;
        
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
}

- (IBAction)didPressTypeButton:(UIButton *)sender {
    if (_reposeType == sender.tag) {
        return;
    }
    
    _reposeType = sender.tag;
    [self selectReposeTypeButton];
}

- (void)selectReposeTypeButton {
    UIImageView * imageView;
    UIImage * image;
    UILabel * typeLabel;
    for (NSInteger tagIndex = 1; tagIndex <= 4; tagIndex++) {
        imageView = (UIImageView*)[_typeView viewWithTag:tagIndex * 10];
        image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        imageView.tintColor = UIColor.whiteColor;
        imageView.image = image;
        
        typeLabel = (UILabel*)[_typeView viewWithTag:tagIndex * 100];
        typeLabel.textColor = UIColor.whiteColor;
    }
    
    imageView = (UIImageView*)[_typeView viewWithTag:(_reposeType + 1)*10];
    image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    imageView.tintColor = UIColor.yellowColor;
    imageView.image = image;
    
    typeLabel = (UILabel*)[_typeView viewWithTag:(_reposeType + 1) * 100];
    typeLabel.textColor = UIColor.yellowColor;
}

#pragma mark - ReposeDisplay

- (void)loadDisplay {
    [_titleTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    _titleTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Título", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName:[UIColor grayColor],
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]
                                                 }
     ];
    
    _noteTextView.keyboardAppearance = UIKeyboardAppearanceDark;
    _noteTextView.layer.borderWidth = 1;
    _noteTextView.layer.borderColor = BLACK_HEADER_APP_COLOR.CGColor;
    _noteTextView.text = NSLocalizedString(@"Notas", nil);
    
    UIToolbar *toolbarWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetTextView)];
    [barButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarWeigth setItems:[NSArray arrayWithObjects:flexibleWeigthSpace, barButtonItem, nil]];
    _noteTextView.inputAccessoryView = toolbarWeigth;
    
    [self loadValues];
}

- (void)didCreateRepose:(TrainingPlan *)repose {
    [WebServiceManager getTrainingPlansWithCompletionBlock:^(BOOL isSuccess) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (TrainingPlan*)reposeFromDisplay {
    if (_repose) {
        RLMRealm *realm = [AppContext currentRealm];
        [realm transactionWithBlock:^{
            [self setPropertiesToRepose:_repose];
        }];
        
        [realm beginWriteTransaction];
        NSError *error;
        [realm commitWriteTransaction:&error];
        if(error){
            NSLog(@"%@",error.description);
        }
        return _repose;
    }
    else {
        TrainingPlan * repose = [[TrainingPlan alloc] init];
        [self setPropertiesToRepose:repose];
        return repose;
    }
}

- (void)setPropertiesToRepose:(TrainingPlan*)repose {
    repose.title = [NSString stringWithFormat:@"@@%@", _titleTextField.text];
    repose.startDate = _selectedDate;
    repose.endDate = _endDate;
    
    if (_endDate) {
        NSInteger duration = [DateTimeUtil daysBetween:_selectedDate and:_endDate];
        for (NSInteger index = 0; index <= duration; index++) {
            TrainingDay * trainingDay = [[TrainingDay alloc] init];
            trainingDay.restDay = _reposeType == 0 ? @0 : @1;
            trainingDay.order = @(index);
            [repose.trainingDay addObject:trainingDay];
        }
    }
    
    repose.notes = _noteTextView.text;
    repose.desc = _noteTextView.text;
    repose.isFavourite = @(_postponeSwitch.on);
}

- (void)loadValues {
    if (_repose) {
        _titleTextField.text = _repose.title;
        _okButton.hidden = YES;
        _titleTextField.userInteractionEnabled = NO;
        _typeView.userInteractionEnabled = NO;
        _endDateButton.userInteractionEnabled = NO;
        _postponeSwitch.userInteractionEnabled = NO;
        _noteTextView.text = _repose.desc;
        _noteTextView.userInteractionEnabled = NO;
        if (_repose.isFavourite) {
            [_postponeSwitch setOn:_repose.isFavourite.boolValue];
        }
    }
    
    [self selectReposeTypeButton];
    
    if(_selectedDate) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];
        formatter.dateFormat = @"MMM, d E yyyy";
        _dateLabel.text = [formatter stringFromDate:_selectedDate];
    }
    
    if (_endDate) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];
        dateFormatter.dateFormat = @"MMM, d E yyyy";
        _endDateLabel.text = [dateFormatter stringFromDate:_endDate];
    }
}

#pragma mark - Delegate For Text Field and Text View

- (void)resetTextView {
    [_noteTextView resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
    
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    if([[textView text] isEqualToString:NSLocalizedString(@"Notas", nil)]) {
        [_noteTextView setText:@""];
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

@end
