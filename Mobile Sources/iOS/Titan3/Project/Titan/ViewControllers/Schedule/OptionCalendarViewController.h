//
//  OptionCalendarViewController.h
//  Titan
//
//  Created by Marcus Lee on 14/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ParentViewController.h"

@protocol OptionCalendarDelegate<NSObject>

- (void)didSelectItemForCalendarOptionIndex:(NSInteger)index;

@end


@interface OptionCalendarViewController : ParentViewController

@property (nonatomic, strong) id<OptionCalendarDelegate> delegate;
@property (nonatomic, strong) NSDate * selectedDate;

@end
