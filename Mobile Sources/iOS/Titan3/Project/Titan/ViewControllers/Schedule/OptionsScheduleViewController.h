//
//  SelectorScheduleViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 26/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

typedef enum : NSUInteger {
    ScheduleTypeNutrition,
    ScheduleTypePlan
} ScheduleType;

@protocol OptionScheduleDelegate<NSObject>

- (void)didSelectSchedule:(ScheduleType)scheduleType;

@end

@interface OptionsScheduleViewController : ParentViewController

@property (nonatomic, strong) id<OptionScheduleDelegate> delegate;

@end
