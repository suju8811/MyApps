//
//  SelectorScheduleViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 26/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OptionsScheduleViewController.h"
#import "OptionsCell.h"
#import "MRProgress.h"
#import "AlertCustomView.h"
#import "AppDelegate.h"
#import "NutritionPlanAddViewController.h"
#import "NutritionalListViewController.h"
#import "PlanListViewController.h"
#import "SelectorTableViewController.h"

@interface OptionsScheduleViewController ()<UITableViewDelegate, UITableViewDataSource, AlertCustomViewDelegate>

@property (nonatomic, strong) UITableView *optionsTableView;
@property (nonatomic, strong) AlertCustomView *alertCustomView;

@end

@implementation OptionsScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)configureView{
    
    [[self view] setBackgroundColor:BLACK_APP_COLOR];
    //[self.view addSubview:[self headerView]];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _optionsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, HEIGHT) style:UITableViewStylePlain];
    
    [_optionsTableView setDelegate:self];
    [_optionsTableView setDataSource:self];
    [_optionsTableView registerClass:[OptionsCell class] forCellReuseIdentifier:@"optionCell"];
    [_optionsTableView setBackgroundColor:[UIColor clearColor]];
    [_optionsTableView setTableHeaderView:[self headerView]];
    [_optionsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.view addSubview:_optionsTableView];
    
    [self footerView];
}

- (UIView *)headerView{
    UIView *headerView  = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:100])];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:20], WIDTH - [UtilManager width:30], [UtilManager height:60])];
    [titleLabel setText:@"Sample"];
    [titleLabel setText:NSLocalizedString(@"Añadir opción al día seleccionado", nil)];
    
    
    [titleLabel setTextColor:YELLOW_APP_COLOR];
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (void)footerView{
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    [closeButton setTitle:NSLocalizedString(@"Cerrar", nil) forState:UIControlStateNormal];
    [[closeButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:19]];
    [closeButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:closeButton];
}


- (void)dismissView{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
 
    if (indexPath.row == 0) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [_delegate didSelectSchedule:ScheduleTypePlan];
    }
    else if (indexPath.row == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [_delegate didSelectSchedule:ScheduleTypeNutrition];
    }
}

#pragma mark UITableView Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.08 * HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.08 * HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OptionsCell *cell = [_optionsTableView dequeueReusableCellWithIdentifier:@"optionCell"];
    
    if(indexPath.row == 0){
        [[cell iconImageView] setImage:[UIImage imageNamed:@"icon-edit"]];
        [[cell titleLabel] setText:NSLocalizedString(@"Añadir Plan de Entrenamiento", nil)];
    }else if (indexPath.row == 1){
        [[cell iconImageView] setImage:[UIImage imageNamed:@"icon-edit"]];
        [[cell titleLabel] setText:NSLocalizedString(@"Añadir Plan de Nutrición", nil)];
    }else if (indexPath.row == 2){
        [[cell iconImageView] setImage:[UIImage imageNamed:@"icon-edit"]];
        [[cell titleLabel] setText:NSLocalizedString(@"Ver plan de entrenamiento", nil)];
    }else if (indexPath.row == 3){
        [[cell iconImageView] setImage:[UIImage imageNamed:@"icon-edit"]];
        [[cell titleLabel] setText:NSLocalizedString(@"Ver plan de nutrición", nil)];
    }
    
    return cell;
    
}

#pragma mark AlertCustomViewDelegate

- (void)dismissAlertViewSelected{
    [_alertCustomView removeFromSuperview];
}

- (void)cancelAlertViewDidselected  {
    [_alertCustomView removeFromSuperview];
}

- (void)confirmAlertViewDidSelected{
    
    if(_alertCustomView.textField.text){
        
    } else
    [_alertCustomView removeFromSuperview];
}

@end
