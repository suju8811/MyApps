//
//  EventViewController.h
//  Titan
//
//  Created by Marcus Lee on 14/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ParentViewController.h"
#import "Event.h"

@interface EventViewController : ParentViewController

//
// Outlets
//

@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;

@property (weak, nonatomic) IBOutlet UIView *typeView;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;
@property (weak, nonatomic) IBOutlet UIButton *repeatButton;
@property (weak, nonatomic) IBOutlet UIButton *dateSelectButton;

//
// Delegated variables
//

@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) Event * event;

@end
