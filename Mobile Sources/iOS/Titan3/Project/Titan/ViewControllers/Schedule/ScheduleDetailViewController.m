//
//  ScheduleDetailViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 11/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ScheduleDetailViewController.h"

@interface ScheduleDetailViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *dayTableView;

@end

@implementation ScheduleDetailViewController

static float headerHeigth = 50.;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
    _dayTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., NAV_HEIGHT, WIDTH, HEIGHT- NAV_HEIGHT) style:UITableViewStylePlain];
    [_dayTableView setDataSource:self];
    [_dayTableView setDelegate:self];
    [_dayTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [_dayTableView setBackgroundColor:[UIColor clearColor]];
    [_dayTableView setOpaque:NO];
    
    
    [self.view addSubview:_dayTableView];
}

#pragma mark UITableViewDataSource Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return [UtilManager height:headerHeigth];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return [UtilManager height:headerHeigth];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH,[UtilManager height:headerHeigth])];
    
    UILabel *dayLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:5], WIDTH - [UtilManager width:20], [UtilManager height:30])];
    [dayLabel setText:@"15 de Marzo"];
    [dayLabel setTextColor:YELLOW_APP_COLOR];
    [dayLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:19]];
    [dayLabel setBackgroundColor:[UIColor clearColor]];
    [headerView addSubview:dayLabel];
    
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(dayLabel.frame.origin.x,[UtilManager height:headerHeigth]- 1, dayLabel.frame.size.width, 1)];
    [separatorView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [headerView addSubview:separatorView];
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    return cell;
}

#pragma mark UITableViewDelegate Methods



@end
