//
//  PlanTitleCell.m
//  Titan
//
//  Created by Marcus Lee on 14/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "PlanTitleCell.h"
#import "UtilManager.h"
#import "AppConstants.h"

@implementation PlanTitleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                [UtilManager height:10],
                                                                [UtilManager width:300],
                                                                [UtilManager height:20])];
        
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.textColor = YELLOW_APP_COLOR;
        [self.contentView addSubview:_titleLabel];
        
        _daysLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_titleLabel.frame),
                                                                CGRectGetMaxY(_titleLabel.frame) + [UtilManager height:5],
                                                                CGRectGetWidth(_titleLabel.frame),
                                                                CGRectGetHeight(_titleLabel.frame))];
        
        _daysLabel.textAlignment = NSTextAlignmentLeft;
        _daysLabel.textColor = YELLOW_APP_COLOR;
        [self.contentView addSubview:_daysLabel];
        
        _actionButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:45],
                                                                   _titleLabel.frame.origin.y,
                                                                   [UtilManager width:40],
                                                                   [UtilManager width:40])];
        
        [self.contentView addSubview:_actionButton];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, [UtilManager height:79], _titleLabel.frame.size.width, 1)];
        [_separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        [_separatorView setAlpha:0.2];
        
        [[self contentView] addSubview:_separatorView];
        
        _checkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:25], 0, [UtilManager width:18], [UtilManager width:18])];
        [_checkImageView setImage:[UIImage imageNamed:@"img-check"]];
        [_checkImageView setCenter:CGPointMake(_checkImageView.center.x, exerciseCellHeight/2)];
        [_checkImageView setHidden:true];
        
        [[self contentView] addSubview:_checkImageView];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
