//
//  PlanTitleCell.h
//  Titan
//
//  Created by Marcus Lee on 14/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlanTitleCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *daysLabel;

@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, strong) UIButton *actionButton;

@property (nonatomic, assign) NSInteger index;

@property (nonatomic, strong) UIImageView *checkImageView;

@end
