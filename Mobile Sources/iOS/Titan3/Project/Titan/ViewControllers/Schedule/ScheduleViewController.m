//
//  ScheduleViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 9/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ScheduleViewController.h"
#import "ScheduleDetailViewController.h"
#import "LogDetailViewController.h"
#import <EventKit/EventKit.h>
#import "TrainingPlan.h"
#import "Log.h"
#import "UIImageView+AFNetworking.h"
#import "DateManager.h"
#import "ProgressCell.h"
#import "OptionsScheduleViewController.h"
#import "OptionCalendarViewController.h"
#import "SelectorTableViewController.h"
#import "SelectorFoodViewController.h"
#import "SchedulePlan.h"

#import "FSCalendar.h"
#import "ReposeViewController.h"
#import "EventViewController.h"
#import "TrainingPlanDetailViewController.h"
#import "NutritionPlanDetailViewController.h"
#import "OptionPopupViewController.h"
#import "WorkoutDetailViewController.h"
#import "CalendarDayPopupViewController.h"
#import "OptionItem.h"
#import "Meal.h"
#import "UIViewController+Present.h"

#import "ScheduleExecutive.h"

#define EVENT_VIEW_STORYBOARD_ID                        @"EventViewStoryboardID"
#define OPTION_POPUP_STORY_BOARD_ID                     @"OptionPopupStoryboardID"
#define CALENDAR_DAY_INFO_POPUP_STORYBOARD_ID           @"CalendarDayInfoPopupStoryboard"
#define CALENDAR_DATE_SELECT_FILL_COLOR                 [[UIColor yellowColor] colorWithAlphaComponent:0.8]

@interface ScheduleViewController ()<FSCalendarDataSource,FSCalendarDelegate,FSCalendarDelegateAppearance, UITableViewDelegate, UITableViewDataSource, SelectorTableViewDelegate, OptionScheduleDelegate, OptionCalendarDelegate, SelectorFoodViewDelegate, OptionPopupViewDelegate, CalendarDayPopupViewControllerDelegate, ScheduleDisplay, LogDetailViewControllerDelegate>

@property (weak, nonatomic) FSCalendar *calendar;
@property (strong, nonatomic) NSCalendar *gregorian;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSDate *minimumDate;
@property (strong, nonatomic) NSDate *maximumDate;
@property (strong, nonatomic) NSCache *cache;
@property (strong, nonatomic) NSArray<EKEvent *> *events;
@property (assign, nonatomic) BOOL showsEvents;

@property (nonatomic, strong) UITableView *itemsTableView;

@property (nonatomic, strong) RLMResults *logs;

@property (nonatomic, strong) UISegmentedControl * segmentControl;
@property (nonatomic, assign) NSInteger segmentState;
@property (nonatomic, strong) UIView *segmentControlView;
@property (nonatomic, strong) UIButton * todayButton;
@property (nonatomic, strong) ScheduleExecutive * executive;

- (void)todayItemClicked:(id)sender;
- (void)eventItemClicked:(id)sender;

- (void)loadCalendarEvents;
- (NSArray<EKEvent *> *)eventsForDate:(NSDate *)date;

@end

@implementation ScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    self.calendar.accessibilityIdentifier = @"calendar";
    
    _showsEvents = YES;
    _segmentState = 0;
    
    [self loadCalendarEvents];
    [self configureView];
    
    _executive = [[ScheduleExecutive alloc] initWithDisplay:self];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.calendar.frame = CGRectMake([UtilManager width:10],
                                     CGRectGetMaxY(self.navigationController.navigationBar.frame),
                                     WIDTH - [UtilManager width:20],
                                     self.view.bounds.size.height - CGRectGetMaxY(self.navigationController.navigationBar.frame));
    self.calendar.backgroundColor = UIColor.clearColor;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPressAddScheduleButton) name:NAV_RIGHT1_ACTION object:nil];
    [_calendar reloadData];
}

- (void)viewWillDisappear:(BOOL)animated  {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NAV_RIGHT1_ACTION object:nil];
}

- (void)configureView{
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    FSCalendar *calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0,
                                                                        self.navigationController.navigationBar.frame.size.height + NAV_HEIGHT,
                                                                        self.view.bounds.size.width,
                                                                        self.view.bounds.size.height-self.navigationController.navigationBar.frame.size.height)];
    
    calendar.backgroundColor = [UIColor clearColor];
    calendar.dataSource = self;
    calendar.delegate = self;
    calendar.pagingEnabled = NO; // important
    calendar.allowsMultipleSelection = FALSE;
    calendar.firstWeekday = 2;
    calendar.placeholderType = FSCalendarPlaceholderTypeFillHeadTail;
    calendar.appearance.caseOptions = FSCalendarCaseOptionsWeekdayUsesSingleUpperCase|FSCalendarCaseOptionsHeaderUsesUpperCase;
    
    calendar.appearance.todayColor = GRAY_REGISTER_FONT;
    calendar.appearance.headerTitleColor = YELLOW_APP_COLOR;
    calendar.appearance.eventSelectionColor = YELLOW_APP_COLOR;
    calendar.appearance.weekdayTextColor = [UIColor whiteColor];
    calendar.appearance.titleTodayColor = BLACK_APP_COLOR;
    calendar.appearance.eventDefaultColor = [UIColor whiteColor];
    calendar.appearance.selectionColor = YELLOW_APP_COLOR;
    calendar.appearance.titleDefaultColor = [UIColor whiteColor];
    
    [self.view addSubview:calendar];
    
    self.calendar = calendar;
    
    [self segmentControlHeaderView];
    
//    _itemsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., HEIGHT, WIDTH, HEIGHT/2) style:UITableViewStylePlain];
//    [_itemsTableView setDataSource:self];
//    [_itemsTableView setDelegate:self];
//    [_itemsTableView registerClass:[ProgressCell class] forCellReuseIdentifier:@"progressCell"];
//    [_itemsTableView setBackgroundColor:BLACK_APP_COLOR];
//    [_itemsTableView setBounces:FALSE];
//
//    [self.view addSubview:_itemsTableView];
}

- (void)segmentControlHeaderView {
    _segmentControlView = [[UIView alloc] initWithFrame:CGRectMake(0., NAV_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    [_segmentControlView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = _segmentControlView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //[exerciseHeaderView addSubview:blurEffectView];
    
    _segmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Entrenamiento", nil),NSLocalizedString(@"Nutrición", nil)]];
    [_segmentControl setSelectedSegmentIndex:_segmentState];
    [_segmentControl addTarget:self action:@selector(changeCalendarView:) forControlEvents:UIControlEventValueChanged];
    [_segmentControl setCenter:CGPointMake(_segmentControlView.center.x, [UtilManager height:25])];
    [_segmentControl setTintColor:YELLOW_APP_COLOR];
    
    [_segmentControlView addSubview:_segmentControl];
    
    _todayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _todayButton.frame = CGRectMake(WIDTH - [UtilManager width:60], 0, [UtilManager width:40], [UtilManager height:40]);
    _todayButton.center = CGPointMake(_todayButton.center.x, _segmentControl.center.y);
    _todayButton.backgroundColor = UIColor.clearColor;
    [_todayButton setTitle:@"Hoy" forState:UIControlStateNormal];
    [_todayButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [_todayButton.titleLabel setFont:[UIFont fontWithName:BOLD_FONT size:18]];
    [_todayButton addTarget:self action:@selector(didPressTodayButton:) forControlEvents:UIControlEventTouchUpInside];
    [_segmentControlView addSubview:_todayButton];
    
    [self.view addSubview:_segmentControlView];
}

- (void)didPressAddScheduleButton {
    if (_calendar.selectedDate == nil) {
        [self showMessage:@"Por favor seleccione la fecha"];
        return;
    }
    
    OptionCalendarViewController * optionCalendarViewController = [[OptionCalendarViewController alloc] init];
    optionCalendarViewController.delegate = self;
    optionCalendarViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionCalendarViewController.selectedDate = _calendar.selectedDate;
    [self.navigationController presentViewController:optionCalendarViewController animated:YES completion:nil];
}

- (void)eventItemClicked:(id)sender{
    
}

#pragma mark - Action
- (void)changeCalendarView:(id)sender {
    _segmentState = _segmentControl.selectedSegmentIndex;
    [_calendar reloadData];
}

#pragma mark - FSCalendarDataSource

- (NSDate *)minimumDateForCalendar:(FSCalendar *)calendar {
    return self.minimumDate;
}

- (NSDate *)maximumDateForCalendar:(FSCalendar *)calendar {
    return self.maximumDate;
}

- (NSString *)calendar:(FSCalendar *)calendar subtitleForDate:(NSDate *)date {
    return nil;
}

#pragma mark - FSCalendarDelegate

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition {
    [_executive didPressDate:date
                   atSegment:_segmentState];
    
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar {
    [_executive didChangeCurrentPage:calendar.currentPage];
}

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated {
}

//- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date
//{
//    RLMResults *results = [TrainingPlan getTrainingPlansInDate:date];
//
//    if(results.count > 0)
//        return results.count;
//
//    RLMResults *logsInDate = [Log getLogsInDate:date];
//
//    if(logsInDate.count > 0)
//        return logsInDate.count;
//
//    if (!self.showsEvents) return 0;
//    if (!self.events) return 0;
//    NSArray<EKEvent *> *events = [self eventsForDate:date];
//    return events.count;
//}

- (NSArray<UIColor *> *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance eventDefaultColorsForDate:(NSDate *)date
{
    if (!self.showsEvents) return nil;
    if (!self.events) return nil;
    //NSArray<EKEvent *> *events = [self eventsForDate:date];
    
    RLMResults *results = [TrainingPlan getTrainingPlansInDate:date];
    
    if(results.count > 0)
        return @[YELLOW_APP_COLOR];
    
    RLMResults *logsInDate = [Log getLogsInDate:date];
    
    if(logsInDate.count > 0)
        return @[BLUE_PERCENT_CIRCLE];
    
    return nil;
}

#pragma mark FSCalendare Appearance Methods


/**
 * Asks the delegate for a fill color in unselected state for the specific date.
 */
- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date {
    
    SchedulePlan * schedulePlan = [SchedulePlan getScheduleTrainingPlanWithScheduleDate:date];
    if (schedulePlan != nil) {
        return CALENDAR_DATE_SELECT_FILL_COLOR;
    }
    
    schedulePlan = [SchedulePlan getScheduleNutritionPlanWithScheduleDate:date];
    if (schedulePlan != nil) {
        return CALENDAR_DATE_SELECT_FILL_COLOR;
    }
    
    Event * event = [Event getEventWithScheduleDate:date];
    if (event) {
        return CALENDAR_DATE_SELECT_FILL_COLOR;
    }
    
    TrainingPlan * repose = [SchedulePlan getReposeWithScheduleDate:date];
    if (repose) {
        return CALENDAR_DATE_SELECT_FILL_COLOR;
    }
    
    return nil;
}

/**
 * Asks the delegate for a fill color in selected state for the specific date.
 */
//- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillSelectionColorForDate:(NSDate *)date{

//}

/**
 * Asks the delegate for day text color in unselected state for the specific date.
 */
//- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date{

//}

/**
 * Asks the delegate for day text color in selected state for the specific date.
 */
//- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance titleSelectionColorForDate:(NSDate *)date{

//}

/**
 * Asks the delegate for subtitle text color in unselected state for the specific date.
 */
//- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance subtitleDefaultColorForDate:(NSDate *)date{

//}

/**
 * Asks the delegate for subtitle text color in selected state for the specific date.
 */
//- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance subtitleSelectionColorForDate:(NSDate *)date{

//}

/**
 * Asks the delegate for event colors for the specific date.
 */
//- (nullable NSArray<UIColor *> *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance eventDefaultColorsForDate:(NSDate *)date{

//}

/**
 * Asks the delegate for multiple event colors in selected state for the specific date.
 */
//- (nullable NSArray<UIColor *> *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance eventSelectionColorsForDate:(NSDate *)date{
//}

/**
 * Asks the delegate for a border color in unselected state for the specific date.
 */
- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance borderDefaultColorForDate:(NSDate *)date {
    RLMResults *results = [TrainingPlan getTrainingPlansInDate:date];
    
    if(results.count > 0)
        return nil;
    else
        return nil;
}

/**
 * Asks the delegate for a border color in selected state for the specific date.
 */
//- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance borderSelectionColorForDate:(NSDate *)date{

//}

/**
 * Asks the delegate for an offset for day text for the specific date.
 */
//- (CGPoint)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance titleOffsetForDate:(NSDate *)date{
//}

/**
 * Asks the delegate for an offset for subtitle for the specific date.
 */
//- (CGPoint)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance subtitleOffsetForDate:(NSDate *)date;

/**
 * Asks the delegate for an offset for image for the specific date.
 */
//- (CGPoint)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance imageOffsetForDate:(NSDate *)date;

/**
 * Asks the delegate for an offset for event dots for the specific date.
 */
//- (CGPoint)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance eventOffsetForDate:(NSDate *)date;


/**
 * Asks the delegate for a border radius for the specific date.
 */
//- (CGFloat)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance borderRadiusForDate:(NSDate *)date;

#pragma mark - Private methods

- (void)loadCalendarEvents {
    [self.calendar reloadData];
}

- (NSArray<EKEvent *> *)eventsForDate:(NSDate *)date
{
    NSArray<EKEvent *> *events = [self.cache objectForKey:date];
    if ([events isKindOfClass:[NSNull class]]) {
        return nil;
    }
    NSArray<EKEvent *> *filteredEvents = [self.events filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(EKEvent * _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        return [evaluatedObject.occurrenceDate isEqualToDate:date];
    }]];
    if (filteredEvents.count) {
        [self.cache setObject:filteredEvents forKey:date];
    } else {
        [self.cache setObject:[NSNull null] forKey:date];
    }
    return filteredEvents;
}

#pragma marks UITableViewDelegateMethods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
}

#pragma marks UITableViewDataSourceMethods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _logs.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [UtilManager height:170];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [UtilManager height:170];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return TAB_BAR_HEIGHT * 0.6;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return TAB_BAR_HEIGHT * 0.6;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, TAB_BAR_HEIGHT * 0.6)];
    
    [headerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 40.,[UtilManager height:10.], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [backButton setImage:[UIImage imageNamed:@"btn-down"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    if(_logs.count > 0) {
        [backButton setHidden:NO];
    }
    else {
        [backButton setHidden:YES];
    }
    
    [backButton setCenter:CGPointMake(backButton.center.x, headerView.center.y)];
    
    [headerView addSubview:backButton];
    
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ProgressCell *progressCell = [_itemsTableView dequeueReusableCellWithIdentifier:@"progressCell"];
    
    Log *currentLog = [_logs objectAtIndex:indexPath.row];

    [progressCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute;
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units fromDate:currentLog.revisionDate];
    
    [[progressCell dateLabel] setText:[NSString stringWithFormat:@"%ld %@ %ld",(long)components.day,[DateManager monthWithInteger:components.month],(long)components.year]];
    [[progressCell weightLabel] setText:[NSString stringWithFormat:@"%d Kg",currentLog.weight.intValue]];
    [[progressCell percentLabel] setText:[NSString stringWithFormat:@"%d %%",currentLog.igc.intValue]];
    
    NSURLRequest *homeImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture1]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
    NSURLRequest *exercise1ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture2]]
                                                           cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                       timeoutInterval:60];
    NSURLRequest *exercise2ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture3]]
                                                           cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                       timeoutInterval:60];
    NSURLRequest *exercise3ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture4]]
                                                           cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                       timeoutInterval:60];
    NSURLRequest *exercise4ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture5]]
                                                           cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                       timeoutInterval:60];
    
    __weak UIImageView *progressHomeImageView = progressCell.homeImageView;
    
    [[progressCell homeImageView] setImageWithURLRequest:homeImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [progressHomeImageView setImage:image];
        [[progressHomeImageView layer] setMasksToBounds:YES];
        [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        
        [progressHomeImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        [[progressHomeImageView layer] setMasksToBounds:YES];
        [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    }];
    
    __weak UIImageView *firstImageView = progressCell.exercise1ImageView;
    
    [[progressCell exercise1ImageView] setImageWithURLRequest:exercise1ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [firstImageView setImage:image];
        [[firstImageView layer] setMasksToBounds:YES];
        [firstImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        [firstImageView setImage:nil];
    }];
    
    __weak UIImageView *secondImageView = progressCell.exercise2ImageView;
    
    [[progressCell exercise2ImageView] setImageWithURLRequest:exercise2ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [secondImageView setImage:image];
        [[secondImageView layer] setMasksToBounds:YES];
        [secondImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        [secondImageView setImage:nil];
    }];
    
    __weak UIImageView *thirdImageView = progressCell.exercise3ImageView;
    
    [[progressCell exercise3ImageView] setImageWithURLRequest:exercise3ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [thirdImageView setImage:image];
        [[thirdImageView layer] setMasksToBounds:YES];
        [thirdImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        [thirdImageView setImage:nil];
    }];
    
    __weak UIImageView *fourImageView = progressCell.exercise4ImageView;
    
    [[progressCell exercise4ImageView] setImageWithURLRequest:exercise4ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [fourImageView setImage:image];
        [[fourImageView layer] setMasksToBounds:YES];
        [fourImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        [fourImageView setImage:nil];
    }];
    
    return progressCell;
}

- (void)didPressTodayButton:(UIButton*)sender {
    [_executive didPressTodayButton:_segmentState];
}

- (void)didPressBackButton {
    [UIView animateWithDuration:0.5 animations:^{
        [_itemsTableView setFrame:CGRectMake(0., HEIGHT, WIDTH, _itemsTableView.frame.size.height)];
    } completion:^(BOOL finished){
        NSLog(@"Items PresentView");
    }];
}

#pragma mark - Option Schedule Delegate

- (void)didSelectSchedule:(ScheduleType)scheduleType {

    SelectorTableViewController * selectorTableViewController = [[SelectorTableViewController alloc] init];
    [selectorTableViewController setDelegate:self];
    [selectorTableViewController setIsOnlyOneSelector:YES];
    [selectorTableViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    switch (scheduleType) {
        case ScheduleTypePlan:
        {
            [selectorTableViewController setSelectorType:SelectorTypeTrainingPlan];
            [self presentViewController:selectorTableViewController animated:YES completion:nil];
        }
            break;
        case ScheduleTypeNutrition:
            break;
        default:
            break;
    }
}

#pragma mark - Selector Table View Delegate

- (void)didSelectTrainingPlan:(TrainingPlan*)trainingPlan {
    [_executive didSelectTrainingPlan:trainingPlan atDate:_calendar.selectedDate];
    
}

- (void)didPressNutritionPlanForCalendar:(NutritionPlan*)nutritionPlan {
    [_executive didSelectNutritionPlan:nutritionPlan atDate:_calendar.selectedDate];
}

#pragma mark - OptionCalendarDelegate

- (void)didSelectItemForCalendarOptionIndex:(NSInteger)index {
    [_executive didPressCalendarOption:index];
}

#pragma mark - OptionPopupViewDelegate

- (void)optionPopupViewController:(OptionPopupViewController*)optionPopupViewController
              didPressItemAtIndex:(NSInteger)index
                       withObject:(RLMObject*)object {
    
    [_executive didPressPopupMenuTag:optionPopupViewController.view.tag
                               index:index
                              object:object];
    
}

#pragma mark - ScheduleDisplay

- (void)presentTrainingPlanSelectorViewController {
    SelectorTableViewController * selectorTableViewController = [[SelectorTableViewController alloc] init];
    selectorTableViewController.delegate = self;
    selectorTableViewController.isOnlyOneSelector = YES;
    selectorTableViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    selectorTableViewController.selectorType = SelectorTypeTrainingPlan;
    [self presentViewController:selectorTableViewController animated:YES completion:nil];
}

- (void)presentTrainingDetailViewController:(Training*)training {
    WorkoutDetailViewController *trainingDetailViewController = [[WorkoutDetailViewController alloc] init];
    trainingDetailViewController.currentTraining = training;
    trainingDetailViewController.fromCalendar = YES;
    [self.navigationController pushViewController:trainingDetailViewController animated:YES];
}

- (void)presentNutritionPlanSelectorViewController {
    SelectorFoodViewController * selectorFoodViewController = [[SelectorFoodViewController alloc] init];
    selectorFoodViewController.delegate = self;
    selectorFoodViewController.isOnlyOneSelector = YES;
    selectorFoodViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    selectorFoodViewController.selectorType = SelectorFoodTypeNutritionPlanCalendar;
    [self presentViewController:selectorFoodViewController animated:YES completion:nil];
}

- (void)presentEventViewController:(Event *)event {
    UIStoryboard * scheduleStoryboard = [UIStoryboard storyboardWithName:@"Schedule" bundle:nil];
    EventViewController * eventViewController = [scheduleStoryboard instantiateViewControllerWithIdentifier:EVENT_VIEW_STORYBOARD_ID];
    eventViewController.selectedDate = _calendar.selectedDate;
    eventViewController.event = event;
    [self.navigationController pushViewController:eventViewController animated:YES];
}

- (void)presentReposeViewController:(TrainingPlan*)repose {
    UIStoryboard * scheduleStoryboard = [UIStoryboard storyboardWithName:@"Schedule" bundle:nil];
    ReposeViewController * reposeViewController = [scheduleStoryboard instantiateInitialViewController];
    reposeViewController.selectedDate = _calendar.selectedDate;
    reposeViewController.repose = repose;
    [self.navigationController pushViewController:reposeViewController animated:YES];
}

- (void)presentLogViewController:(Log *)log {
    LogDetailViewController *logDetailViewController = [[LogDetailViewController alloc] init];
    logDetailViewController.isEditing = log != nil;
    logDetailViewController.selectLog = log;
    logDetailViewController.delegate = self;
    [self.navigationController pushViewController:logDetailViewController animated:YES];
}

- (void)presentPopupViewControllerWithTag:(NSInteger)tag
                              optionItems:(NSArray<OptionItem*>*) optionItems
                               withObject:(RLMObject *)object {
    
    UIStoryboard * scheduleStoryboard = [UIStoryboard storyboardWithName:@"Schedule" bundle:nil];
    OptionPopupViewController * popupViewController = [scheduleStoryboard instantiateViewControllerWithIdentifier:OPTION_POPUP_STORY_BOARD_ID];
    popupViewController.view.tag = tag;
    popupViewController.delegate = self;
    popupViewController.optionItems = optionItems;
    popupViewController.rlmObject = object;
    [self.navigationController presentViewController:popupViewController animated:YES completion:nil];
}

- (void)selectDate:(NSDate*)date {
    NSMutableArray *selectedDates = [[NSMutableArray alloc] initWithObjects:date, nil];
    [self.calendar setSelectedDates:selectedDates];
    [self.calendar selectDate:date scrollToDate:YES];
}

- (void)presentTrainingPlanDetailViewController:(SchedulePlan*)schedulePlan {
    TrainingPlanDetailViewController *trainingDetailViewController = [[TrainingPlanDetailViewController alloc] init];
    trainingDetailViewController.trainingPlan = schedulePlan.plan;
    trainingDetailViewController.fromCalendar = 1;
    trainingDetailViewController.schedulePlan = schedulePlan;
    [self.navigationController pushViewController:trainingDetailViewController animated:YES];
}

- (void)presentNutritionPlanDetailViewController:(SchedulePlan*)schedulePlan {
    NutritionPlanDetailViewController * nutritionPlanDetailViewController = [[NutritionPlanDetailViewController alloc] init];
    nutritionPlanDetailViewController.currentNutritionPlan = schedulePlan.nutritionPlan;
    nutritionPlanDetailViewController.fromCalendar = 1;
    nutritionPlanDetailViewController.schedulePlan = schedulePlan;
    nutritionPlanDetailViewController.selectedDate = _calendar.selectedDate;
    [self.navigationController pushViewController:nutritionPlanDetailViewController animated:YES];
}

- (void)presentCalendarDayInfoPopup:(NSDictionary*)calendarData date:(NSDate*)selectedDate {
    UIStoryboard * commonStoryboard = [UIStoryboard storyboardWithName:@"Common" bundle:nil];
    CalendarDayPopupViewController * calendarDayInfoPopupViewController = [commonStoryboard instantiateViewControllerWithIdentifier:CALENDAR_DAY_INFO_POPUP_STORYBOARD_ID];
    calendarDayInfoPopupViewController.calendarData = calendarData;
    calendarDayInfoPopupViewController.selectedDate = selectedDate;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:calendarDayInfoPopupViewController];
    navigationController.view.backgroundColor = UIColor.clearColor;
    [self presentTransparentViewController:navigationController animated:NO completion:nil];
}

- (void)reloadCalendar {
    [self.calendar reloadData];
}

#pragma mark - LogDetailViewControllerDelegate

- (void)logDetailViewController:(LogDetailViewController*)logDetailViewController
              didPressCreateLog:(Log*)log
                          image:(UIImage*)image {
    
    log.revisionDate = _calendar.selectedDate;
    [_executive createLog:log image:image];
}

- (void)logDetailViewController:(LogDetailViewController*)logDetailViewController
              didPressUpdateLog:(Log*)log
                          image:(UIImage*)image {
    
    [_executive updateLog:log image:image];
}

@end
