//
//  ScheduleDetailViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 11/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

@interface ScheduleDetailViewController : ParentViewController

@property (nonatomic, strong) NSDate *selectedDate;

@end
