//
//  OptionCalendarViewController.m
//  Titan
//
//  Created by Marcus Lee on 14/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OptionCalendarViewController.h"
#import "OptionsCell.h"
#import "MRProgress.h"
#import "AlertCustomView.h"

@interface OptionCalendarViewController ()<UITableViewDelegate, UITableViewDataSource, AlertCustomViewDelegate>

@property (nonatomic, strong) UITableView *optionsTableView;
@property (nonatomic, strong) AlertCustomView *alertCustomView;

@end

@implementation OptionCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    CGRect tableViewFrame = _optionsTableView.frame;
    tableViewFrame.size.height = _optionsTableView.contentSize.height;
    _optionsTableView.frame = tableViewFrame;
    _optionsTableView.center = CGPointMake(_optionsTableView.center.x,
                                           (HEIGHT - TAB_BAR_HEIGHT)/2.f);
}

- (void)configureView{
    
    [[self view] setBackgroundColor:BLACK_APP_COLOR];
    //[self.view addSubview:[self headerView]];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _optionsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT) style:UITableViewStylePlain];
    _optionsTableView.backgroundColor = UIColor.redColor;
    _optionsTableView.delegate = self;
    _optionsTableView.dataSource = self;
    [_optionsTableView registerClass:[OptionsCell class] forCellReuseIdentifier:@"optionCell"];
    _optionsTableView.backgroundColor = [UIColor clearColor];
    _optionsTableView.tableHeaderView = [self headerView];
    _optionsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:_optionsTableView];
    
    [self footerView];
}

- (UIView *)headerView{
    UIView *headerView  = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:100])];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:20], WIDTH - [UtilManager width:30], [UtilManager height:60])];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = UIColor.whiteColor;
    titleLabel.font = [UIFont fontWithName:REGULAR_FONT size:19];
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es"];
    [formatter setDateFormat:@"E - d MMMM yyyy"];
    
    titleLabel.text = [formatter stringFromDate:_selectedDate];
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (void)footerView{
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    [closeButton setTitle:NSLocalizedString(@"Cancelar", nil) forState:UIControlStateNormal];
    [closeButton.titleLabel setFont:[UIFont fontWithName:BOLD_FONT size:19]];
    [closeButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:closeButton];
}


- (void)dismissView{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [_delegate didSelectItemForCalendarOptionIndex:indexPath.row];
    }];
}

#pragma mark UITableView Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.08 * HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.08 * HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OptionsCell *cell = [_optionsTableView dequeueReusableCellWithIdentifier:@"optionCell"];
    
    if(indexPath.row == 0) {
        cell.iconImageView.image = [UIImage imageNamed:@"icon-fitness"];
        cell.titleLabel.text = NSLocalizedString(@"Plan de Entrenamiento", nil);
    }
    else if (indexPath.row == 1) {
        cell.iconImageView.image = [UIImage imageNamed:@"icon-food"];
        cell.titleLabel.text = NSLocalizedString(@"Plan de Nutrición", nil);
    }
    else if (indexPath.row == 2) {
        cell.iconImageView.image = [UIImage imageNamed:@"icon-edit"];
        cell.titleLabel.text = NSLocalizedString(@"Repeso", nil);
    }
    else if (indexPath.row == 3) {
        cell.iconImageView.image = [UIImage imageNamed:@"icon-edit"];
        cell.titleLabel.text = NSLocalizedString(@"Evento", nil);
    }
    else if (indexPath.row == 4) {
        cell.iconImageView.image = [UIImage imageNamed:@"icon-edit"];
        cell.titleLabel.text = @"Registro";
    }
    
    return cell;
}

#pragma mark AlertCustomViewDelegate

- (void)dismissAlertViewSelected{
    [_alertCustomView removeFromSuperview];
}

- (void)cancelAlertViewDidselected  {
    [_alertCustomView removeFromSuperview];
}

- (void)confirmAlertViewDidSelected{
    
    if(_alertCustomView.textField.text) {
        
    }
    else {
        [_alertCustomView removeFromSuperview];
    }
}

@end
