//
//  CustomSettingsCell.swift
//
//  Created by qode on 28.01.17.
//  Copyright © 2017 ll rights reserved.
//

import UIKit

class CustomSettingsCell: UITableViewCell {

    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    
    
    override func prepareForReuse() {
        
        super.prepareForReuse()
    }
}
