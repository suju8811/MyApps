//
//  DateCell.swift
//
//  Created by qode on 28.01.17.
//  Copyright © 2017 ll rights reserved.
//

import UIKit

class DateCell: UITableViewCell {
    
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var dateView: UITextField!

    
    override func prepareForReuse() {
        
        super.prepareForReuse()
    }
}
