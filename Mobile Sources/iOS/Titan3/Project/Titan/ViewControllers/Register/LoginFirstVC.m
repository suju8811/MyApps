//
//  LoginFirstVC.m
//  Titan
//
//  Created by MacOS on 10/30/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "LoginFirstVC.h"
#import "UserDefaultLibrary.h"
#import "AppDelegate.h"
#import "WebServiceManager.h"
#import "ParentViewController.h"
#import "MRProgress.h"

#import "MenuHomeViewController.h"
#import "REFrostedViewController.h"

@interface LoginFirstVC ()

@end

@implementation LoginFirstVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureView{
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UILabel *_titleDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:120], WIDTH - 2*[UtilManager width:10], UITEXTFIELD_HEIGHT)];
                                                                                
    [_titleDescriptionLabel setTextColor:[UIColor whiteColor]];
    [_titleDescriptionLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleDescriptionLabel setText:NSLocalizedString(@"BIENVENIDO A LA COMUNIDAD DEL HIERRO - TITAN", nil)];
    [_titleDescriptionLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    _titleDescriptionLabel.numberOfLines = 2;
    [_titleDescriptionLabel setAdjustsFontSizeToFitWidth:YES];
    [_titleDescriptionLabel setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:_titleDescriptionLabel];
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleDescriptionLabel.frame.origin.x, _titleDescriptionLabel.frame.origin.y + _titleDescriptionLabel.frame.size.height, _titleDescriptionLabel.frame.size.width, [UtilManager height:160])];
    [descriptionLabel setFont:[UIFont fontWithName:LIGHT_FONT size:12]];
    descriptionLabel.numberOfLines = 10;
    [descriptionLabel setText:NSLocalizedString(@"TITAN TE OFRECE TODAS LAS HERRAMIENTAS Y COMPAÑEROS DE BATALLA PARA ALCANZAR TUS METAS EN EL MUNDO DE LOS HIERROS.TITAN ES TAN POTENTE QUE NO PODEMOS EXPLICARTE TODO LO QUE PUEDES HACER CON ELLA EN UN PAR DE LINEAS Y TAMPOCO PRETENDEMOS AGOBIARTE. POR ELLO, HEMOS IDO MÁS ALLÁ.EN NUESTRO CANAL DE YOUTUBE ENCONTRARÁS TUTORIALES DONDE PUEDES APRENDER MUY RÁPIDO Y CON TODO LUJO DE DETALLES COMO SACARLE EL MÁXIMO PARTIDO A CADA HERRAMIENTA DE TITAN. NO DUDES EN VISITARLO.", nil)];
    [descriptionLabel setBackgroundColor:[UIColor clearColor]];
    [descriptionLabel setAdjustsFontSizeToFitWidth:YES];
    [descriptionLabel setTextColor:[UIColor whiteColor]];
    
    [self.view addSubview:descriptionLabel];
    
    UIButton *youtubeButton = [[UIButton alloc] initWithFrame:CGRectMake(descriptionLabel.frame.size.width*0.5 -  [UtilManager height:80], descriptionLabel.frame.origin.y + descriptionLabel.frame.size.height, [UtilManager height:160], LOGIN_BUTTON_HEIGHT)];
    [youtubeButton setBackgroundImage:[UIImage imageNamed:@"btn_youtube"] forState:UIControlStateNormal];
    [youtubeButton addTarget:self action:@selector(youtubeVideo) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:youtubeButton];
    
    UILabel *bottomdescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(descriptionLabel.frame.origin.x, youtubeButton.frame.origin.y + youtubeButton.frame.size.height, descriptionLabel.frame.size.width, [UtilManager height:160])];
    [bottomdescriptionLabel setFont:[UIFont fontWithName:LIGHT_FONT size:12]];
    bottomdescriptionLabel.numberOfLines = 10;
    [bottomdescriptionLabel setText:NSLocalizedString(@"TAMBIÉN ESTAMOS A TU ENTERA DISPOSICIÓN A TRAVÉS DEL BUZÓN DE “DUDAS Y SUGERENCIAS” DONDE PUEDES APORTAR TU GRANITO DE ARENA PARA AYUDARNOS A CRECER O DEJAR QUE NOSOTROS TE APORTEMOS EL NUESTRO PARA MEJORAR TU EXPERIENCIA COMO COMPAÑERO DE BATALLA.", nil)];
    [bottomdescriptionLabel setBackgroundColor:[UIColor clearColor]];
    [bottomdescriptionLabel setAdjustsFontSizeToFitWidth:YES];
    [bottomdescriptionLabel setTextColor:[UIColor whiteColor]];
    
    [self.view addSubview:bottomdescriptionLabel];
    
    UIButton *touchButton = [[UIButton alloc] initWithFrame:CGRectMake(bottomdescriptionLabel.frame.origin.x, bottomdescriptionLabel.frame.origin.y + bottomdescriptionLabel.frame.size.height, bottomdescriptionLabel.frame.size.width, LOGIN_BUTTON_HEIGHT)];
    [touchButton setBackgroundColor:YELLOW_APP_COLOR];
    [touchButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [touchButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [[touchButton titleLabel] setFont:[UIFont fontWithName:BOLD_DOLCE_FONT size:21]];
    [touchButton setTitle:NSLocalizedString(@"VAMOS! LIGHT WEIGHT!", nil) forState:UIControlStateNormal];
    [touchButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:touchButton];
    [self headerView];
}

- (void)headerView{
    
    UIView *_containerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_HEIGHT)];
    [_containerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIImageView *_icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:18], [UtilManager height:30], [UtilManager width:30.], [UtilManager height:30])];
    [_icon setBackgroundColor:[UIColor clearColor]];
    [_icon setContentMode:UIViewContentModeScaleAspectFit];
    [_containerView addSubview:_icon];
    
    UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, _containerView.frame.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [_titleLabel setTextColor:[UIColor whiteColor]];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setText:NSLocalizedString(@"BIENVENIDO", nil)];
    [_titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [_titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [_containerView addSubview:_titleLabel];
    
    UIButton *_backButton = [[UIButton alloc] initWithFrame:CGRectMake([[UIApplication sharedApplication] statusBarFrame].size.width - [UtilManager width:40],[[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], [UtilManager width:30], [UtilManager width:30])];
    [_backButton setImage:[UIImage imageNamed:@"btn_close"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(backToAction) forControlEvents:UIControlEventTouchUpInside];
    [_backButton setHidden:FALSE];
    [_backButton setCenter:CGPointMake(_backButton.center.x, NAV_HEIGHT/2 + [UtilManager height:5])];
    [_containerView addSubview:_backButton];
    
    [[self view] addSubview:_containerView];
}

- (void) didPressBackButton {
    
    LoginSecondVC *loginViewController = [[LoginSecondVC alloc] init];
    [self.navigationController pushViewController:loginViewController animated:YES];
}

- (void) backToAction
{
    MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:homeViewController.view
                      duration:1.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
     }];
}

- (void) youtubeVideo
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.youtube.com"]];
}
@end
