//
//  RegisterNavController.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RegisterNavController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "InitViewController.h"

#import "AppConstants.h"
#import "UtilManager.h"

@interface RegisterNavController ()<UINavigationControllerDelegate>

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *icon;

@end

@implementation RegisterNavController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setNavigationBarHidden:YES];
    [self setDelegate:self];
    
    _containerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_HEIGHT)];
    [_containerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    _icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:18], [UtilManager height:30], [UtilManager width:30.], [UtilManager height:30])];
    [_icon setBackgroundColor:[UIColor clearColor]];
    [_icon setContentMode:UIViewContentModeScaleAspectFit];
    [_containerView addSubview:_icon];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, _containerView.frame.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [_titleLabel setTextColor:[UIColor whiteColor]];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setText:NSLocalizedString(@"INICIAR SESIÓN", nil)];
    [_titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [_titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [_containerView addSubview:_titleLabel];
    
    _backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:30.], [UtilManager width:30], [UtilManager width:30])];
    [_backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [_backButton setHidden:TRUE];
    [_backButton setCenter:CGPointMake(_backButton.center.x, NAV_HEIGHT/2 + [UtilManager height:5])];
    [_containerView addSubview:_backButton];
    [_containerView setHidden:TRUE];
    
    [[self view] addSubview:_containerView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)didPressBackButton {
    [self popViewControllerAnimated:YES];
}

#pragma mark UINavigationControllerDelegate Methods

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    //if([viewController isKindOfClass:[InitViewController class]]){
        [_containerView setHidden:TRUE];
    //}
    
    //if([viewController isKindOfClass:[LoginViewController class]]){
    //    [_containerView setHidden:FALSE];
    //    [_backButton setHidden:FALSE];
    //    [_titleLabel setText:NSLocalizedString(@"INICIAR SESIÓN", nil)];
    //}
    //if([viewController isKindOfClass:[RegisterViewController class]]){
    //    [_containerView setHidden:FALSE];
    //    [_backButton setHidden:FALSE];
    //    [_titleLabel setText:NSLocalizedString(@"CREAR CUENTA", nil)];
    //}
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
}




@end
