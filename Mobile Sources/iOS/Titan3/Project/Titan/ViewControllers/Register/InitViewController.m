//
//  InitViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "InitViewController.h"
#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "TutorialViewController.h"

#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"

#import "PageControl.h"

@interface InitViewController ()<UIPageViewControllerDataSource, UIPageViewControllerDelegate, PageControlDelegate, UIScrollViewDelegate>

@property (nonatomic) NSInteger index;
@property (nonatomic) NSInteger currentScreenID;
//@property (nonatomic, strong) PageControl *pageControl;
@property (nonatomic, strong) UIPageViewController *pageViewController;

@property (nonatomic, strong) UIScrollView *tutorailScrollView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, assign) BOOL userMove;

@end

@implementation InitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _userMove = FALSE;
    
    _index = 1;
    
    [self configureView];    
    [NSTimer scheduledTimerWithTimeInterval:4 repeats:YES block:^(NSTimer *timer){
        
        if(_userMove) {
            _userMove = FALSE;
        }
        else {
            CGFloat pageWidth = CGRectGetWidth(_tutorailScrollView.frame);
            NSUInteger page = floor((_tutorailScrollView.contentOffset.x - pageWidth / 4) / pageWidth) + 1;
            
            if(page == 3) {
                page = 0;
                [_tutorailScrollView setContentOffset:CGPointMake(0., 0.) animated:FALSE];
            }
            else {
                page++;
                [_tutorailScrollView setContentOffset:CGPointMake(WIDTH * page, 0) animated:YES];
                
            }
            
            self.pageControl.currentPage = page;
        
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
    
    FLAnimatedImageView *animatedImageView = [[FLAnimatedImageView alloc] initWithFrame:self.view.frame];
    
    animatedImageView.contentMode = UIViewContentModeScaleAspectFill;
    animatedImageView.clipsToBounds = YES;
    
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"video" withExtension:@"gif"];
    NSData *data1 = [NSData dataWithContentsOfURL:url1];
    animatedImageView.animatedImage = [FLAnimatedImage animatedImageWithGIFData:data1];
    
    [self.view addSubview:animatedImageView];
    
    UIImage *logoImage = [UIImage imageNamed:@"img-logo"];
    
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:logoImage];
    [logoImageView setCenter:CGPointMake(self.view.center.x, HEIGHT/4)];
    
    [self.view addSubview:logoImageView];
    
    /*
    NSURL *url2 = [NSURL URLWithString:@"https://cloud.githubusercontent.com/assets/1567433/10417835/1c97e436-7052-11e5-8fb5-69373072a5a0.gif"];
    [self loadAnimatedImageWithURL:url2 completion:^(FLAnimatedImage *animatedImage) {
        animatedImageView.animatedImage = animatedImage;
        animatedImageView.userInteractionEnabled = YES;
    }];
     
     */
    /*
    _pageViewController = [[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    [_pageViewController.view setFrame:CGRectMake(0., NAV_HEIGHT, WIDTH, HEIGHT - NAV_HEIGHT)];
    
    TutorialViewController *tutorialViewController = [[TutorialViewController alloc] init];
    [tutorialViewController setIndex:[NSNumber numberWithInt:1]];
    
    [_pageViewController setViewControllers:@[tutorialViewController] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished){
        
    }];
    
    [_pageViewController setDelegate:self];
    [_pageViewController setDataSource:self];
    
    [self addChildViewController:_pageViewController];
    [_pageViewController didMoveToParentViewController:self];
    
    [self.view addSubview:_pageViewController.view];
    
    _pageControl = [[PageControl alloc] initWithFrame:CGRectMake(100.,40., 170., 17.)];
    [_pageControl setBackgroundColor:[UIColor clearColor]];
    [_pageControl setCenter:CGPointMake(_pageViewController.view.center.x, self.view.frame.size.height - [UtilManager height:180])];
    
    [_pageControl setDelegate:self];
    
    [_pageControl setNumberOfPages:4];
    [_pageControl setCurrentPage:1];
    
    _index = 1;
    
    [_pageViewController.view addSubview:_pageControl];
    */
    
    _tutorailScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, HEIGHT/2, WIDTH, HEIGHT/2)];
    [_tutorailScrollView setContentSize:CGSizeMake(WIDTH * 4, HEIGHT/2)];
    [_tutorailScrollView setShowsHorizontalScrollIndicator:FALSE];
    [_tutorailScrollView setBackgroundColor:[UIColor clearColor]];
    _tutorailScrollView.pagingEnabled = YES;
    _tutorailScrollView.showsHorizontalScrollIndicator = NO;
    _tutorailScrollView.showsVerticalScrollIndicator = NO;
    _tutorailScrollView.scrollsToTop = NO;
    _tutorailScrollView.delegate = self;
    
    [self initScrollView];
    
    [self.view addSubview:_tutorailScrollView];
    
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:60], [UtilManager height:20])];
    [_pageControl setCenter:CGPointMake(self.view.center.x, HEIGHT - [UtilManager height:90])];
    [_pageControl setCurrentPage:1];
    [_pageControl setNumberOfPages:4];
    [_pageControl setPageIndicatorTintColor:[UIColor grayColor]];
    [_pageControl setCurrentPageIndicatorTintColor:[UIColor whiteColor]];
    
    [self.view addSubview:_pageControl];
    
    UIButton *loginButton = [[UIButton alloc] initWithFrame:CGRectMake(0., HEIGHT - [UtilManager height:60], WIDTH/2, [UtilManager height:60])];
    [loginButton addTarget:self action:@selector(presentLoginViewController) forControlEvents:UIControlEventTouchUpInside];
    [loginButton setBackgroundColor:BLACK_APP_COLOR];
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[loginButton titleLabel] setFont:[UIFont fontWithName:BOLD_DOLCE_FONT size:21]];
    [loginButton setTitle:@"INICIAR SESIÓN" forState:UIControlStateNormal];
    //[[loginButton titleLabel] setFont:[UIFont boldSystemFontOfSize:15]]; TrainingPlanAddViewController
    
    [self.view addSubview:loginButton];
   
    UIButton *registerButton = [[UIButton alloc] initWithFrame:CGRectMake(loginButton.frame.size.width, loginButton.frame.origin.y, loginButton.frame.size.width, loginButton.frame.size.height)];
    [registerButton addTarget:self action:@selector(presentRegisterViewController) forControlEvents:UIControlEventTouchUpInside];
    [registerButton setBackgroundColor:YELLOW_APP_COLOR];
    [registerButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [[registerButton titleLabel] setFont:[UIFont fontWithName:BOLD_DOLCE_FONT size:21]];
    //[[registerButton titleLabel] setFont:[UIFont boldSystemFontOfSize:15]];
    [registerButton setTitle:@"CREAR CUENTA" forState:UIControlStateNormal];
    
    [self.view addSubview:registerButton];
    
}

// at the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = CGRectGetWidth(_tutorailScrollView.frame);
    NSUInteger page = floor((_tutorailScrollView.contentOffset.x - pageWidth / 4) / pageWidth) + 1;
    self.pageControl.currentPage = page;
    
    // a possible optimization would be to unload the views+controllers which are no longer visible
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _userMove = TRUE;
}

- (void)initScrollView{
    
    for (int i=0 ; i<4 ; i++){
    
        UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(i * WIDTH + [UtilManager width:20], [UtilManager height:60], WIDTH - [UtilManager width:40], [UtilManager height:60])];
        [_titleLabel setTextColor:[UIColor whiteColor]];
        [_titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_titleLabel setFont:[UIFont fontWithName:BOLD_DOLCE_FONT size:21]];
        
        [_titleLabel setAlpha:0.8];
        
        [_tutorailScrollView addSubview:_titleLabel];
        
        UITextView *_summaryTextView = [[UITextView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x,_titleLabel.frame.origin.y + _titleLabel.frame.size.height, _titleLabel.frame.size.width, [UtilManager height:200])];
        [_summaryTextView setTextAlignment:NSTextAlignmentJustified];
        [_summaryTextView setTextColor:[UIColor whiteColor]];
        [_summaryTextView setBackgroundColor:[UIColor clearColor]];
        [_summaryTextView setEditable:NO];
        [_summaryTextView setSelectable:YES];
        [_summaryTextView setDataDetectorTypes:UIDataDetectorTypeAll];
        [_summaryTextView setFont:[UIFont fontWithName:BOLD_FONT size:16]];
        
        [_summaryTextView setAlpha:0.8];
        
        [_tutorailScrollView addSubview:_summaryTextView];
        
        switch (i) {
            case 0:
                [_titleLabel setText:[NSLocalizedString(@"Diseña", nil) uppercaseString]];
                [_summaryTextView setText:NSLocalizedString(@"Crea tus planes de entrenamiento y nutricicón a medida", nil)];
                break;
            case 1:
                [_titleLabel setText:[NSLocalizedString(@"Entrena", nil) uppercaseString]];
                [_summaryTextView setText:NSLocalizedString(@"Añádelos al calendario e introduce tus marcas", nil)];
                break;
            case 2:
                [_titleLabel setText:[NSLocalizedString(@"Analiza", nil) uppercaseString]];
                [_summaryTextView setText:NSLocalizedString(@"Estudia tus resultados y programa nuevos objetivos", nil)];
                break;
            case 3:
                [_titleLabel setText:[NSLocalizedString(@"Comparte", nil) uppercaseString]];
                [_summaryTextView setText:NSLocalizedString(@"Aproveche la comunidad para compartir planes de entrenamiento, nutrición y resultados", nil)];
                break;
            default:
                break;
        }
    }
}

- (void)loadAnimatedImageWithURL:(NSURL *const)url completion:(void (^)(FLAnimatedImage *animatedImage))completion
{
    NSString *const filename = url.lastPathComponent;
    NSString *const diskPath = [NSHomeDirectory() stringByAppendingPathComponent:filename];
    
    NSData * __block animatedImageData = [[NSFileManager defaultManager] contentsAtPath:diskPath];
    FLAnimatedImage * __block animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:animatedImageData];
    
    if (animatedImage) {
        if (completion) {
            completion(animatedImage);
        }
    } else {
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            animatedImageData = data;
            animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:animatedImageData];
            if (animatedImage) {
                if (completion) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(animatedImage);
                    });
                }
                [data writeToFile:diskPath atomically:YES];
            }
        }] resume];
    }
}

- (void)presentLoginViewController{
    LoginViewController *loginViewController = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:loginViewController animated:YES];
}

- (void)presentRegisterViewController{
    RegisterViewController *registerViewController = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:registerViewController animated:YES];
}

#pragma mark UIPageViewDataSource

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    //TutorialViewController *introViewController = [previousViewControllers objectAtIndex:0];
    //[_pageControl setCurrentPage:introViewController.index.integerValue];
}

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers
{
    
}

/*
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(TutorialViewController *)viewController
{
    if(_index>1)
        _index --;
    [_pageControl setCurrentPage:_index];
    if(viewController.index.integerValue > 1)
    {
        TutorialViewController *introViewController = [[TutorialViewController alloc] init];
        [introViewController setIndex:[NSNumber numberWithInteger:_index]];
        [introViewController reloadTexts];
        
        return introViewController;
    }
    else
        return nil;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(TutorialViewController *)viewController
{
    if(_index<4)
        _index ++;
    [_pageControl setCurrentPage:_index];
    if(viewController.index.integerValue<4)
    {
        TutorialViewController *introViewController = [[TutorialViewController alloc] init];
        [introViewController setIndex:[NSNumber numberWithInteger:_index+1]];
        [introViewController reloadTexts];
        
        return introViewController;
    }
    else
    {
        //_index = viewController.index.integerValue;
        return nil;
    }
}

*/

- (void)pageControlPageDidChange:(PageControl *)pageControl{
    NSLog(@"%ld",(long)pageControl.currentPage);
}

@end
