//
//  LoginViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FingerPrintVC.h"
#import "LoginFirstVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "ParentViewController.h"
#import "RecoveryViewController.h"

@interface LoginViewController : ParentViewController

@end
