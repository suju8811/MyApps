//
//  FingerPrintVC.m
//  Titan
//
//  Created by MacOS on 10/29/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FingerPrintVC.h"
#import <LocalAuthentication/LocalAuthentication.h>

@interface FingerPrintVC ()

@end

@implementation FingerPrintVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self headerView];
    [self getTouchID];
}

- (void)headerView{
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, HEIGHT)];
    bgImageView.image = [UIImage imageNamed:@"bg_touch"];
    [self.view addSubview:bgImageView];
    
    UIView *_containerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_HEIGHT)];
    [_containerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIImageView *_icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:18], [UtilManager height:30], [UtilManager width:30.], [UtilManager height:30])];
    [_icon setBackgroundColor:[UIColor clearColor]];
    [_icon setContentMode:UIViewContentModeScaleAspectFit];
    [_containerView addSubview:_icon];
    
    UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, _containerView.frame.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [_titleLabel setTextColor:[UIColor whiteColor]];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setText:NSLocalizedString(@"HUELLA", nil)];
    [_titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [_titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [_containerView addSubview:_titleLabel];
    
    UIButton *_backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:30.], [UtilManager width:30], [UtilManager width:30])];
    [_backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [_backButton setHidden:FALSE];
    [_backButton setCenter:CGPointMake(_backButton.center.x, NAV_HEIGHT/2 + [UtilManager height:5])];
    [_containerView addSubview:_backButton];
    
    [[self view] addSubview:_containerView];
}

- (void)didPressBackButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getTouchID
{
    LAContext *context = [[LAContext alloc] init];
    
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:@"Are you the device owner?" reply:^(BOOL success, NSError *error) {
            
            if (error) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:@"There was a problem verifying your identity."
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
                [alert show];
                return;
            }
            
            if (success) {
                
                [MRProgressOverlayView showOverlayAddedTo:self.view title:NSLocalizedString(@"Login ...", nil) mode:MRProgressOverlayViewModeIndeterminate animated:TRUE];
                [WebServiceManager authenticateUserWithTouchID:[self loginDictionary] andDelegate:self];
                
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:@"You are not the device owner."
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
        }];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Your device cannot authenticate using TouchID."
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
}

- (NSDictionary *)loginDictionary{
    NSDictionary *loginDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"touch_id_string",@"touch_id", nil];
    return loginDictionary;
}

#pragma mark WebServiceManager Delegate

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        
        MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
        
        [NSTimer scheduledTimerWithTimeInterval:0.5 repeats:FALSE block:^(NSTimer *timer){
            [WebServiceManager getExercises];
            [WebServiceManager getTrainingsWithUser:ADMIN_OWNER_ID];
            [WebServiceManager getTrainingsWithUser:[[[AppContext sharedInstance] currentUser] userId]];
            //[WebServiceManager getFoods];
            [WebServiceManager getAliments];
            [WebServiceManager getMeals];
            [WebServiceManager getNutrionalPlans];
            [WebServiceManager getDiets];
            [WebServiceManager getUserRevision:[[AppContext sharedInstance] userId] andDelegate:self];
            [WebServiceManager getConstants];
            [WebServiceManager getCalendarTrainingPlan:self];
        }];
        
        if (webServiceType == WebServiceTypeAuthenticate) {
            
            //[UserDefaultLibrary setWithKey:IS_USER_LOGIN WithObject:[NSNumber numberWithBool:TRUE]];
        }
        
        [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                            toView:homeViewController.view
                          duration:1.5
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        completion:^(BOOL finished)
         {
             [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
         }];
    }];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        if (webServiceType == WebServiceTypeAuthenticate){
            [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"error de inicio de sesion.", YES)] animated:YES completion:^{
                
            }];
        }
    }];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    
}

@end
