//
//  TutorialViewcontrollerViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 2/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TutorialViewController : UIViewController

@property (nonatomic, strong) NSNumber *index;
@property (nonatomic, strong) UITextView *summaryTextView;
@property (nonatomic, strong) UILabel *titleLabel;

- (void)reloadTexts;

@end
