//
//  RegisterDataVC.m
//  Titan
//
//  Created by MacOS on 10/29/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RegisterDataVC.h"

@interface RegisterDataVC ()
{
    BOOL isMale;
    int currentHeight;
}
@property (nonatomic, strong) UITextField *txtDate;
@property (nonatomic, strong) UITextField *txtAltura;
@property (nonatomic, strong) UITextField *txtPeso;
@property (nonatomic, strong) UIButton *btnMale;
@property (nonatomic, strong) UIButton *btnFemale;
@property (nonatomic, strong) UIButton *btnComplete;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UIPickerView *datePicker1;
@property (nonatomic, retain) UILabel *dateNewLabel;
@end

@implementation RegisterDataVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureView];
}

- (void)configureView{
    
    currentHeight =  120;
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:80], WIDTH - [UtilManager width:40], UITEXTFIELD_HEIGHT)];
    [dateLabel setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
    [dateLabel setText:NSLocalizedString(@"DINOS QUE EDAD TIENES :", nil)];
    [dateLabel setTextColor:[UIColor whiteColor]];
    
    _txtDate = [[UITextField alloc] initWithFrame:CGRectMake(dateLabel.frame.origin.x, dateLabel.frame.origin.y + dateLabel.frame.size.height, WIDTH - [UtilManager width:40], UITEXTFIELD_HEIGHT)];
    [_txtDate setBorderStyle:UITextBorderStyleNone];
    [[_txtDate layer] setMasksToBounds:YES];
    [_txtDate setTextColor:[UIColor whiteColor]];
    [_txtDate setKeyboardType:UIKeyboardTypeEmailAddress];
    [_txtDate setReturnKeyType:UIReturnKeyDone];
    [_txtDate setDelegate:self];
    [_txtDate setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_txtDate setLeftViewMode:UITextFieldViewModeAlways];
    [_txtDate setPlaceholder:NSLocalizedString(@"YYY-MM-DD", nil)];
    [_txtDate setAutocorrectionType:UITextAutocorrectionTypeNo];
    [_txtDate setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_txtDate setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    _txtDate.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"YYYY-MM-DD", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
    ];
    
    _datePicker=[[UIDatePicker alloc]init];
    _datePicker.datePickerMode=UIDatePickerModeDate;
    [_txtDate setInputView:_datePicker];
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(ShowSelectedDate)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
    [self.txtDate setInputAccessoryView:toolBar];
    
    UIView *separatorDateView = [[UIView alloc] initWithFrame:CGRectMake(_txtDate.frame.origin.x, _txtDate.frame.origin.y + _txtDate.frame.size.height, _txtDate.frame.size.width, 1)];
    [separatorDateView setBackgroundColor:GRAY_REGISTER_FONT];
    
    _dateNewLabel = [[UILabel alloc] initWithFrame:CGRectMake(separatorDateView.frame.origin.x, separatorDateView.frame.origin.y + separatorDateView.frame.size.height, separatorDateView.frame.size.width, 9)];
    [_dateNewLabel setFont:[UIFont fontWithName:REGULAR_FONT size:8]];
    [_dateNewLabel setText:NSLocalizedString(@"CONFIRMACIÓN FECHA DE NACIMENTO", nil)];
    [_dateNewLabel setTextColor:[UIColor whiteColor]];
    
    [self.view addSubview:dateLabel];
    [self.view addSubview:_txtDate];
    [self.view addSubview:separatorDateView];
    [self.view addSubview:_dateNewLabel];
    
    UILabel *alturaLabel = [[UILabel alloc] initWithFrame:CGRectMake(_dateNewLabel.frame.origin.x, _dateNewLabel.frame.origin.y + _dateNewLabel.frame.size.height + [UtilManager height:20], _dateNewLabel.frame.size.width, UITEXTFIELD_HEIGHT)];
    [alturaLabel setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
    [alturaLabel setText:NSLocalizedString(@"NECESITAMOS SABER TU ALTURA :", nil)];
    [alturaLabel setTextColor:[UIColor whiteColor]];
    
    _txtAltura = [[UITextField alloc] initWithFrame:CGRectMake(alturaLabel.frame.origin.x, alturaLabel.frame.origin.y + alturaLabel.frame.size.height, WIDTH - [UtilManager width:40], UITEXTFIELD_HEIGHT)];
    [_txtAltura setBorderStyle:UITextBorderStyleNone];
    [[_txtAltura layer] setMasksToBounds:YES];
    [_txtAltura setTextColor:[UIColor whiteColor]];
    [_txtAltura setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [_txtAltura setReturnKeyType:UIReturnKeyDone];
    [_txtAltura setDelegate:self];
    [_txtAltura setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_txtAltura setLeftViewMode:UITextFieldViewModeAlways];
    [_txtAltura setPlaceholder:NSLocalizedString(@"ALTURA", nil)];
    [_txtAltura setAutocorrectionType:UITextAutocorrectionTypeNo];
    [_txtAltura setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_txtAltura setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    _txtAltura.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"ALTURA", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
     ];
    
    _datePicker1=[[UIPickerView alloc]init];
    _datePicker1.dataSource = self;
    _datePicker1.delegate = self;
    [_txtAltura setInputView:_datePicker1];
    
    UIToolbar *toolBar1=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar1 setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn1=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(ShowSelectedDate1)];
    UIBarButtonItem *space1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar1 setItems:[NSArray arrayWithObjects:space1,doneBtn1, nil]];
    [_txtAltura setInputAccessoryView:toolBar1];
    
    UIView *separatorAlturaView = [[UIView alloc] initWithFrame:CGRectMake(_txtAltura.frame.origin.x, _txtAltura.frame.origin.y + _txtAltura.frame.size.height, _txtAltura.frame.size.width, 1)];
    [separatorAlturaView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self.view addSubview:alturaLabel];
    [self.view addSubview:_txtAltura];
    [self.view addSubview:separatorAlturaView];
    
    UILabel *pesoLabel = [[UILabel alloc] initWithFrame:CGRectMake(separatorAlturaView.frame.origin.x, separatorAlturaView.frame.origin.y + separatorAlturaView.frame.size.height + [UtilManager height:20], separatorAlturaView.frame.size.width, UITEXTFIELD_HEIGHT)];
    [pesoLabel setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
    [pesoLabel setText:NSLocalizedString(@"NECESITAMOS SABER TU PESO :", nil)];
    [pesoLabel setTextColor:[UIColor whiteColor]];
    
    _txtPeso = [[UITextField alloc] initWithFrame:CGRectMake(pesoLabel.frame.origin.x, pesoLabel.frame.origin.y + pesoLabel.frame.size.height, WIDTH - [UtilManager width:40], UITEXTFIELD_HEIGHT)];
    [_txtPeso setBorderStyle:UITextBorderStyleNone];
    [[_txtPeso layer] setMasksToBounds:YES];
    [_txtPeso setTextColor:[UIColor whiteColor]];
    [_txtPeso setKeyboardType:UIKeyboardTypeEmailAddress];
    [_txtPeso setReturnKeyType:UIReturnKeyDone];
    [_txtPeso setDelegate:self];
    [_txtPeso setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [_txtPeso setLeftViewMode:UITextFieldViewModeAlways];
    [_txtPeso setPlaceholder:NSLocalizedString(@"PESO", nil)];
    [_txtPeso setAutocorrectionType:UITextAutocorrectionTypeNo];
    [_txtPeso setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_txtPeso setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    _txtPeso.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"PESO", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
     ];
    
    UIView *separatorPesoView = [[UIView alloc] initWithFrame:CGRectMake(_txtPeso.frame.origin.x, _txtPeso.frame.origin.y + _txtPeso.frame.size.height, _txtPeso.frame.size.width, 1)];
    [separatorPesoView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self.view addSubview:pesoLabel];
    [self.view addSubview:_txtPeso];
    [self.view addSubview:separatorPesoView];
    
    _btnMale = [[UIButton alloc] initWithFrame:CGRectMake(separatorPesoView.frame.origin.x + [UtilManager height:60], separatorPesoView.frame.origin.y + separatorPesoView.frame.size.height + [UtilManager height:40], [UtilManager height:60], [UtilManager height:60])];
    [_btnMale setImage:[UIImage imageNamed:@"icon_male_sel"] forState:UIControlStateNormal];
    [_btnMale addTarget:self action:@selector(maleAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnMale];
    
    _btnFemale = [[UIButton alloc] initWithFrame:CGRectMake(_btnMale.frame.origin.x + [UtilManager height:150], separatorPesoView.frame.origin.y + separatorPesoView.frame.size.height + [UtilManager height:40], [UtilManager height:60], [UtilManager height:60])];
    [_btnFemale setImage:[UIImage imageNamed:@"icon_female_desel"] forState:UIControlStateNormal];
    [_btnFemale addTarget:self action:@selector(femaleAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnFemale];
    
    isMale = YES;
    
    _btnComplete = [[UIButton alloc] initWithFrame:CGRectMake(separatorPesoView.frame.origin.x, _btnMale.frame.origin.y + _btnMale.frame.size.height + [UtilManager height:40], separatorPesoView.frame.size.width, [UtilManager height:60])];
    [_btnComplete setBackgroundColor:YELLOW_APP_COLOR];
    [_btnComplete setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_btnComplete setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [[_btnComplete titleLabel] setFont:[UIFont fontWithName:BOLD_DOLCE_FONT size:21]];
    [_btnComplete setTitle:NSLocalizedString(@"LISTO", nil) forState:UIControlStateNormal];
    [_btnComplete addTarget:self action:@selector(completeAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnComplete];
    
    [self headerView];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 80;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%ld cm", 120 + row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    currentHeight = (int)(120 + row);
}

-(void)ShowSelectedDate1
{
    _txtAltura.text = [NSString stringWithFormat:@"%d",currentHeight];
     [_txtAltura resignFirstResponder];
}

-(void)ShowSelectedDate
{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    _txtDate.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:_datePicker.date]];
    [_txtDate resignFirstResponder];
    
    _dateNewLabel.text = [NSString stringWithFormat:@"%ld AÑOS",(long)[self age]];
}

- (NSInteger)age {
    
    NSDate *today = [NSDate date];
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                       components:NSYearCalendarUnit
                                       fromDate:_datePicker.date
                                       toDate:today
                                       options:0];
    return ageComponents.year;
}

- (void)headerView{
    UIView *_containerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_HEIGHT)];
    [_containerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIImageView *_icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:18], [UtilManager height:30], [UtilManager width:30.], [UtilManager height:30])];
    [_icon setBackgroundColor:[UIColor clearColor]];
    [_icon setContentMode:UIViewContentModeScaleAspectFit];
    [_containerView addSubview:_icon];
    
    UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, _containerView.frame.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [_titleLabel setTextColor:[UIColor whiteColor]];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setText:NSLocalizedString(@"COMPLETA TU PERFIL", nil)];
    [_titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [_titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [_containerView addSubview:_titleLabel];
    
    [[self view] addSubview:_containerView];
}

- (void) maleAction
{
    if (!isMale) {
        [_btnMale setImage:[UIImage imageNamed:@"icon_male_sel"] forState:UIControlStateNormal];
        [_btnFemale setImage:[UIImage imageNamed:@"icon_female_desel"] forState:UIControlStateNormal];
        isMale = YES;
    }
}

- (void) femaleAction
{
    if (isMale) {
        [_btnMale setImage:[UIImage imageNamed:@"icon_male_desel"] forState:UIControlStateNormal];
        [_btnFemale setImage:[UIImage imageNamed:@"icon_female_sel"] forState:UIControlStateNormal];
        isMale = NO;
    }
}

- (void) completeAction
{
    if ([_txtDate.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"¡alerta!" message:@"Por favor rellene todos los campos." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    if ([_txtAltura.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"¡alerta!" message:@"Por favor rellene todos los campos." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    if ([_txtPeso.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"¡alerta!" message:@"Por favor rellene todos los campos." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    [MRProgressOverlayView showOverlayAddedTo:self.view title:NSLocalizedString(@"Registering datas ...", nil) mode:MRProgressOverlayViewModeIndeterminate animated:TRUE];
    [WebServiceManager authenticateUserWithData:[self loginDictionary] andDelegate:self];
}

- (NSDictionary *)loginDictionary{
    
    NSDictionary *loginDictionary = [NSDictionary dictionary];
    NSString *deliveryString = [NSString stringWithFormat:@"%@",_tokenString];
    NSMutableArray* words = [NSMutableArray arrayWithArray:[deliveryString componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    [words removeLastObject];
    [words removeObjectAtIndex:0];
   
    deliveryString = [words componentsJoinedByString:@""];
    [deliveryString stringByReplacingOccurrencesOfString:@"{" withString:@""];
    [deliveryString stringByReplacingOccurrencesOfString:@"}" withString:@""];
    
    if (isMale) {
        loginDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:_txtDate.text,@"fecha",_txtAltura.text,@"altura",_txtPeso.text,@"peso", @"1",@"genero",deliveryString, @"token", nil];
    }
    else
    {
        loginDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:_txtDate.text,@"fecha",_txtAltura.text,@"altura",_txtPeso.text,@"peso", @"2",@"genero",deliveryString , @"token", nil];
    }
    
    return loginDictionary;
}

#pragma mark UITextFieldDelegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        LoginFirstVC *loginViewController = [[LoginFirstVC alloc] init];
        [self.navigationController pushViewController:loginViewController animated:YES];
    }];
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        LoginFirstVC *loginViewController = [[LoginFirstVC alloc] init];
        [self.navigationController pushViewController:loginViewController animated:YES];
    }];
    
    [UtilManager presentAlertWithMessage:NSLocalizedString(@"Registrar éxito.", nil)];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        LoginFirstVC *loginViewController = [[LoginFirstVC alloc] init];
        [self.navigationController pushViewController:loginViewController animated:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
