//
//  RecoveryViewController.m
//  Titan
//
//  Created by MacOS on 10/20/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RecoveryViewController.h"
#import "MenuHomeViewController.h"
#import "REFrostedViewController.h"

#import "UserDefaultLibrary.h"
#import "AppDelegate.h"
#import "WebServiceManager.h"
#import "MRProgress.h"

@interface RecoveryViewController ()<UITextFieldDelegate, WebServiceManagerDelegate>

@property (nonatomic, strong) UITextField *userTextField;

@property (nonatomic, strong) UIButton *sessionButton;

@end

@implementation RecoveryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureView{
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    _userTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:180], WIDTH - [UtilManager width:40], UITEXTFIELD_HEIGHT)];
    [_userTextField setBorderStyle:UITextBorderStyleNone];
    [[_userTextField layer] setMasksToBounds:YES];
    [_userTextField setTextColor:[UIColor whiteColor]];
    [_userTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_userTextField setReturnKeyType:UIReturnKeyDone];
    [_userTextField setDelegate:self];
    [_userTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_userTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_userTextField setPlaceholder:NSLocalizedString(@"EMAIL", nil)];
    [_userTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [_userTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_userTextField setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    _userTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"EMAIL", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
     ];
    
    UIView *leftUserView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40], _userTextField.frame.size.height)];
    [leftUserView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *userImageView = [[UIImageView alloc] initWithFrame:leftUserView.bounds];
    [userImageView setImage:[UIImage imageNamed:@"icon-mail"]];
    [userImageView setContentMode:UIViewContentModeCenter];
    
    [leftUserView addSubview:userImageView];
    [_userTextField setLeftView:leftUserView];
    
    [self.view addSubview:_userTextField];
    
    UIView *separatorUserView = [[UIView alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _userTextField.frame.origin.y + _userTextField.frame.size.height, _userTextField.frame.size.width, 1)];
    [separatorUserView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self.view addSubview:separatorUserView];
    
    [_userTextField setDelegate:self];
    [self.view addSubview:_userTextField];
    
    UIView *separatorPasswordView = [[UIView alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _userTextField.frame.origin.y + _userTextField.frame.size.height, _userTextField.frame.size.width, 1)];
    [separatorPasswordView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self.view addSubview:separatorPasswordView];
    
    _sessionButton = [[UIButton alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _userTextField.frame.origin.y + _userTextField.frame.size.height + [UtilManager height:40], _userTextField.frame.size.width, [UtilManager height:60])];
    [_sessionButton setBackgroundColor:YELLOW_APP_COLOR];
    [_sessionButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_sessionButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [[_sessionButton titleLabel] setFont:[UIFont fontWithName:BOLD_DOLCE_FONT size:21]];
    [_sessionButton setTitle:NSLocalizedString(@"RECUPERACIÓN", nil) forState:UIControlStateNormal];
    [_sessionButton addTarget:self action:@selector(recoveryAction) forControlEvents:UIControlEventTouchUpInside];
    //[_sessionButton setEnabled:FALSE];
    
    [self.view addSubview:_sessionButton];
    [self headerView];
}

- (void)headerView{
    UIView *_containerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_HEIGHT)];
    [_containerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIImageView *_icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:18], [UtilManager height:30], [UtilManager width:30.], [UtilManager height:30])];
    [_icon setBackgroundColor:[UIColor clearColor]];
    [_icon setContentMode:UIViewContentModeScaleAspectFit];
    [_containerView addSubview:_icon];
    
    UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, _containerView.frame.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [_titleLabel setTextColor:[UIColor whiteColor]];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setText:NSLocalizedString(@"RECUPERACIÓN", nil)];
    [_titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [_titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [_containerView addSubview:_titleLabel];
    
    UIButton *_backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:30.], [UtilManager width:30], [UtilManager width:30])];
    [_backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [_backButton setHidden:FALSE];
    [_backButton setCenter:CGPointMake(_backButton.center.x, NAV_HEIGHT/2 + [UtilManager height:5])];
    [_containerView addSubview:_backButton];
    
    [[self view] addSubview:_containerView];
}

- (void)didPressBackButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)recoveryAction{
    
    if(_userTextField.text.length == 0){
        [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"Debe introducir un email", nil)] animated:YES completion:^{
            
        }];
    }
    else
    {
        [MRProgressOverlayView showOverlayAddedTo:self.view title:NSLocalizedString(@"Recoverying ...", nil) mode:MRProgressOverlayViewModeIndeterminate animated:TRUE];
        [WebServiceManager recoveryUser:[self recoverDictionary] andDelegate:self];
    }
}

- (NSDictionary *)recoverDictionary{
    NSDictionary *recoverDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:_userTextField.text,@"email", nil];
    return recoverDictionary;
}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
  
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    //[self checkEnableSessionButton];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark WebServiceManager Delegate

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        
        if([[[object valueForKey:@"active"] objectAtIndex:0] boolValue])
            [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"Email enviado.", nil)] animated:YES completion:^{
                
            }];
        else
            [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"El correo electrónico no existe.", YES)] animated:YES completion:^{
                
            }];
        
    }];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        
    }];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        
    }];
}

@end
