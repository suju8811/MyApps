//
//  TutorialViewcontrollerViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 2/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TutorialViewController.h"
#import "UtilManager.h"
#import "AppConstants.h"

@interface TutorialViewController ()

@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureView{
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], 0., WIDTH - [UtilManager width:40], [UtilManager height:30])];
    [_titleLabel setTextColor:[UIColor whiteColor]];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setCenter:CGPointMake(self.view.center.x, self.view.center.y + [UtilManager height:30])];
    [_titleLabel setFont:[UIFont fontWithName:BOLD_DOLCE_FONT size:21]];
    
    [_titleLabel setAlpha:0.8];
    
    [self.view addSubview:_titleLabel];
    
    _summaryTextView = [[UITextView alloc] initWithFrame:CGRectMake(0,_titleLabel.frame.origin.y + _titleLabel.frame.size.height, _titleLabel.frame.size.width, [UtilManager height:200])];
    [_summaryTextView setCenter:CGPointMake(self.view.center.x, _summaryTextView.center.y)];
    [_summaryTextView setTextAlignment:NSTextAlignmentJustified];
    [_summaryTextView setTextColor:[UIColor whiteColor]];
    [_summaryTextView setBackgroundColor:[UIColor clearColor]];
    [_summaryTextView setEditable:NO];
    [_summaryTextView setSelectable:YES];
    [_summaryTextView setDataDetectorTypes:UIDataDetectorTypeAll];
    [_summaryTextView setFont:[UIFont fontWithName:BOLD_FONT size:16]];

    [_summaryTextView setAlpha:0.8];
    
    [self.view addSubview:_summaryTextView];
    
    [self reloadTexts];
}

- (void)reloadTexts{
    switch (_index.intValue) {
        case 1:
            [_titleLabel setText:[NSLocalizedString(@"Diseña", nil) uppercaseString]];
            [_summaryTextView setText:NSLocalizedString(@"Crea tus planes de entrenamiento y nutricicón a medida", nil)];
            break;
        case 2:
            [_titleLabel setText:[NSLocalizedString(@"Entrena", nil) uppercaseString]];
            [_summaryTextView setText:NSLocalizedString(@"Añádelos al calendario e introduce tus marcas", nil)];
            break;
        case 3:
            [_titleLabel setText:[NSLocalizedString(@"Analiza", nil) uppercaseString]];
            [_summaryTextView setText:NSLocalizedString(@"Estudia tus resultados y programa nuevos objetivos", nil)];
            break;
        case 4:
            [_titleLabel setText:[NSLocalizedString(@"Comparte", nil) uppercaseString]];
            [_summaryTextView setText:NSLocalizedString(@"Aproveche la comunidad para compartir planes de entrenamiento, nutrición y resultados", nil)];
            break;
        default:
            [_titleLabel setText:[NSLocalizedString(@"Entrena", nil) uppercaseString]];
            [_summaryTextView setText:NSLocalizedString(@"Añádelos al calendario e introduce tus marcas", nil)];
            break;
    }
}

@end
