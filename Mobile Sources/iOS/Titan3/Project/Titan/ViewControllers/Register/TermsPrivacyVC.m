//
//  TermsPrivacyVC.m
//  Titan
//
//  Created by MacOS on 10/30/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TermsPrivacyVC.h"

@interface TermsPrivacyVC ()

@end

@implementation TermsPrivacyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureView];
}

- (void)configureView{
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:120], WIDTH - 2*[UtilManager width:10], [UtilManager height:160])];
    [descriptionLabel setFont:[UIFont fontWithName:LIGHT_FONT size:15]];
    descriptionLabel.numberOfLines = 10;
    [descriptionLabel setText:NSLocalizedString(@"Bla bla bla bla bla bla bla bla bla bla.", nil)];
    [descriptionLabel setBackgroundColor:[UIColor clearColor]];
    [descriptionLabel setAdjustsFontSizeToFitWidth:YES];
    [descriptionLabel setTextColor:[UIColor whiteColor]];
    
    [self.view addSubview:descriptionLabel];
    
    [self headerView];
}

- (void)headerView{
    
    UIView *_containerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_HEIGHT)];
    [_containerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIImageView *_icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:18], [UtilManager height:30], [UtilManager width:30.], [UtilManager height:30])];
    [_icon setBackgroundColor:[UIColor clearColor]];
    [_icon setContentMode:UIViewContentModeScaleAspectFit];
    [_containerView addSubview:_icon];
    
    UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, _containerView.frame.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [_titleLabel setTextColor:[UIColor whiteColor]];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setText:NSLocalizedString(@"TÉRMINOS Y CONDICIONES", nil)];
    [_titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [_titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [_containerView addSubview:_titleLabel];
    
    UIButton *_backButton = [[UIButton alloc] initWithFrame:CGRectMake([[UIApplication sharedApplication] statusBarFrame].size.width - [UtilManager width:40],[[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], [UtilManager width:30], [UtilManager width:30])];
    [_backButton setImage:[UIImage imageNamed:@"btn_close"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [_backButton setHidden:FALSE];
    [_backButton setCenter:CGPointMake(_backButton.center.x, NAV_HEIGHT/2 + [UtilManager height:5])];
    [_containerView addSubview:_backButton];
    
    [[self view] addSubview:_containerView];
}

- (void) didPressBackButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
