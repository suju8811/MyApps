//
//  RegisterViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RegisterViewController.h"
#import "MenuHomeViewController.h"

#import "MRProgress.h"
#import "WebServiceManager.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "Titan-Swift.h"
#import "SignupChatRequest.h"

@interface RegisterViewController ()<UITextFieldDelegate, WebServiceManagerDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITextField *mailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UITextField *userTextField;
@property (nonatomic, strong) UITextField *birthTextField;
@property (nonatomic, strong) UITextField *genderTextField;
@property (nonatomic, strong) UIButton *saveButton;

@property (nonatomic, strong) UIButton *acceptButton;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) NSDate *selectedDate;

@property (nonatomic, strong) UIButton *sessionButton;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    [self configureView];
}

- (void)configureView{
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.,0.,WIDTH , HEIGHT)];
    [_scrollView setBackgroundColor:[UIColor clearColor]];
    
    //////// EMAIL //////
    
    _mailTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:140], WIDTH - [UtilManager width:40], UITEXTFIELD_HEIGHT)];
    [_mailTextField setBorderStyle:UITextBorderStyleNone];
    [[_mailTextField layer] setMasksToBounds:YES];
    [_mailTextField setTextColor:[UIColor whiteColor]];
    [_mailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_mailTextField setReturnKeyType:UIReturnKeyDone];
    [_mailTextField setDelegate:self];
    [_mailTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_mailTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_mailTextField setPlaceholder:NSLocalizedString(@"EMAIL", nil)];
    [_mailTextField setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    [_mailTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    _mailTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"EMAIL", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
     ];
    
    UIView *leftMailView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40], _mailTextField.frame.size.height)];
    [leftMailView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *mailImageView = [[UIImageView alloc] initWithFrame:leftMailView.bounds];
    [mailImageView setImage:[UIImage imageNamed:@"icon-mail"]];
    [mailImageView setContentMode:UIViewContentModeCenter];
    
    [leftMailView addSubview:mailImageView];
    [_mailTextField setLeftView:leftMailView];
    
    [_scrollView addSubview:_mailTextField];
    
    UIView *separatorMailView = [[UIView alloc] initWithFrame:CGRectMake(_mailTextField.frame.origin.x, _mailTextField.frame.origin.y + _mailTextField.frame.size.height, _mailTextField.frame.size.width, 1)];
    [separatorMailView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [_scrollView addSubview:separatorMailView];
    
    //////// USER //////
    
    _userTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:20], _mailTextField.frame.origin.y + _mailTextField.frame.size.height + [UtilManager height:10], WIDTH - [UtilManager width:40], UITEXTFIELD_HEIGHT)];
    [_userTextField setBorderStyle:UITextBorderStyleNone];
    [[_userTextField layer] setMasksToBounds:YES];
    [_userTextField setTextColor:[UIColor whiteColor]];
    [_userTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_userTextField setReturnKeyType:UIReturnKeyDone];
    [_userTextField setDelegate:self];
    [_userTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_userTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_userTextField setPlaceholder:NSLocalizedString(@"USUARIO", nil)];
    [_userTextField setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    [_userTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    _userTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"USUARIO", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
     ];
    
    UIView *leftUserView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40], _userTextField.frame.size.height)];
    [leftUserView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *userImageView = [[UIImageView alloc] initWithFrame:leftUserView.bounds];
    [userImageView setImage:[UIImage imageNamed:@"icon-user"]];
    [userImageView setContentMode:UIViewContentModeCenter];
    
    [leftUserView addSubview:userImageView];
    [_userTextField setLeftView:leftUserView];
    
    [_scrollView addSubview:_userTextField];
    
    UIView *separatorUserView = [[UIView alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _userTextField.frame.origin.y + _userTextField.frame.size.height, _userTextField.frame.size.width, 1)];
    [separatorUserView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [_scrollView addSubview:separatorUserView];
    
    //////// PASSWORD //////
    
    _passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _userTextField.frame.origin.y + _userTextField.frame.size.height + [UtilManager height:10], _userTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
    [_passwordTextField setBorderStyle:UITextBorderStyleNone];
    [_passwordTextField setTextColor:[UIColor whiteColor]];
    [[_passwordTextField layer] setMasksToBounds:YES];
    [_passwordTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_passwordTextField setReturnKeyType:UIReturnKeyDone];
    [_passwordTextField setSecureTextEntry:YES];
    [_passwordTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_passwordTextField setRightViewMode:UITextFieldViewModeAlways];
    [_passwordTextField setPlaceholder:NSLocalizedString(@"CONTRASEÑA", nil)];
    [_passwordTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_passwordTextField setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    _passwordTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"CONTRASEÑA", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
     ];
    
    UIView *leftPasswordView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40], _passwordTextField.frame.size.height)];
    [leftPasswordView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *passwordImageView = [[UIImageView alloc] initWithFrame:leftPasswordView.bounds];
    [passwordImageView setImage:[UIImage imageNamed:@"icon-password"]];
    [passwordImageView setContentMode:UIViewContentModeCenter];
    
    [leftPasswordView addSubview:passwordImageView];
    
    [_passwordTextField setLeftView:leftPasswordView];
    
    [_passwordTextField setDelegate:self];
    [_scrollView addSubview:_passwordTextField];
    
    UIView *separatorPasswordView = [[UIView alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _passwordTextField.frame.origin.y + _passwordTextField.frame.size.height, _userTextField.frame.size.width, 1)];
    [separatorPasswordView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [_scrollView addSubview:separatorPasswordView];
    
    //////// BIRTH //////
    
    _birthTextField = [[UITextField alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _passwordTextField.frame.origin.y + _passwordTextField.frame.size.height + [UtilManager height:10], _userTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
    [_birthTextField setBorderStyle:UITextBorderStyleNone];
    [_birthTextField setTextColor:[UIColor whiteColor]];
    [[_birthTextField layer] setMasksToBounds:YES];
    [_birthTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_birthTextField setReturnKeyType:UIReturnKeyDone];
    [_birthTextField setSecureTextEntry:FALSE];
    [_birthTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_birthTextField setRightViewMode:UITextFieldViewModeAlways];
    [_birthTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_birthTextField setPlaceholder:NSLocalizedString(@"FECHA DE NACIMIENTO", nil)];
    [_birthTextField setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    _birthTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"FECHA DE NACIMIENTO", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
     ];
    
    UIView *leftBirthView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40], _passwordTextField.frame.size.height)];
    [leftBirthView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *birthImageView = [[UIImageView alloc] initWithFrame:leftPasswordView.bounds];
    [birthImageView setImage:[UIImage imageNamed:@"icon-birth"]];
    [birthImageView setContentMode:UIViewContentModeCenter];
    
    [leftBirthView addSubview:birthImageView];
    
    [_birthTextField setLeftView:leftBirthView];
    [_birthTextField setDelegate:self];
    
    //[_scrollView addSubview:_birthTextField];
    
    UIView *separatorBirthView = [[UIView alloc] initWithFrame:CGRectMake(_birthTextField.frame.origin.x, _birthTextField.frame.origin.y + _birthTextField.frame.size.height, _birthTextField.frame.size.width, 1)];
    [separatorBirthView setBackgroundColor:GRAY_REGISTER_FONT];
    
    //[_scrollView addSubview:separatorBirthView];
    
    //////// GENDER //////
    
    _genderTextField = [[UITextField alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _passwordTextField.frame.origin.y + _passwordTextField.frame.size.height + [UtilManager height:10], _userTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
    [_genderTextField setBorderStyle:UITextBorderStyleNone];
    [_genderTextField setTextColor:[UIColor whiteColor]];
    [[_genderTextField layer] setMasksToBounds:YES];
    [_genderTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_genderTextField setReturnKeyType:UIReturnKeyDone];
    [_genderTextField setSecureTextEntry:FALSE];
    [_genderTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_genderTextField setRightViewMode:UITextFieldViewModeAlways];
    [_genderTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_genderTextField setPlaceholder:NSLocalizedString(@"GÉNERO", nil)];
    [_genderTextField setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    _genderTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"GÉNERO", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:21.0]
                                                 }
     ];
    
    UIView *leftGenderView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40], _passwordTextField.frame.size.height)];
    [leftGenderView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *genderImageView = [[UIImageView alloc] initWithFrame:leftPasswordView.bounds];
    [genderImageView setImage:[UIImage imageNamed:@"icon-gender"]];
    [genderImageView setContentMode:UIViewContentModeCenter];
    
    [leftGenderView addSubview:genderImageView];
    
    [_genderTextField setLeftView:leftGenderView];
    [_genderTextField setDelegate:self];
    
    //[_scrollView addSubview:_genderTextField];
    
    UIView *separatorGenderView = [[UIView alloc] initWithFrame:CGRectMake(_birthTextField.frame.origin.x, _genderTextField.frame.origin.y + _genderTextField.frame.size.height, _birthTextField.frame.size.width, 1)];
    [separatorGenderView setBackgroundColor:GRAY_REGISTER_FONT];
    
    //[_scrollView addSubview:separatorGenderView];
    
    _sessionButton = [[UIButton alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _genderTextField.frame.origin.y + _genderTextField.frame.size.height + [UtilManager height:40], _userTextField.frame.size.width, LOGIN_BUTTON_HEIGHT)];
    [_sessionButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [_sessionButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [[_sessionButton titleLabel] setFont:[UIFont fontWithName:BOLD_DOLCE_FONT size:21]];
    [_sessionButton setBackgroundColor:YELLOW_APP_COLOR];
    [_sessionButton setTitle:NSLocalizedString(@"CREAR CUENTA", nil) forState:UIControlStateNormal];
    [_sessionButton addTarget:self action:@selector(didPressRegisterButton) forControlEvents:UIControlEventTouchUpInside];
    
    [_scrollView addSubview:_sessionButton];
    
    UILabel *orLabel = [[UILabel alloc] initWithFrame:CGRectMake(_sessionButton.frame.origin.x, _sessionButton.frame.origin.y + _sessionButton.frame.size.height, _sessionButton.frame.size.width, _sessionButton.frame.size.height)];
    [orLabel setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
    [orLabel setText:NSLocalizedString(@"o", nil)];
    [orLabel setTextAlignment:NSTextAlignmentCenter];
    [orLabel setTextColor:[UIColor whiteColor]];
    
    [_scrollView addSubview:orLabel];
    
    UIButton *registerButton = [[UIButton alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, orLabel.frame.origin.y + orLabel.frame.size.height, _userTextField.frame.size.width, LOGIN_BUTTON_HEIGHT)];
    [registerButton setBackgroundColor:BLUE_FACEBOOK_FONT];
    [registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[registerButton titleLabel] setFont:[UIFont fontWithName:BOLD_DOLCE_FONT size:21]];
    [registerButton setTitle:NSLocalizedString(@"CREAR CUENTA CON FACEBOOK", nil) forState:UIControlStateNormal];
    [registerButton addTarget:self action:@selector(facebookAction) forControlEvents:UIControlEventTouchUpInside];
    
    [_scrollView addSubview:registerButton];
    
    UIButton *registerLabel = [[UIButton alloc] initWithFrame:CGRectMake(0, registerButton.frame.origin.y + registerButton.frame.size.height + [UtilManager height:25], WIDTH, registerButton.frame.size.height)];
    [registerLabel setTitle:NSLocalizedString(@"ACEPTO LOS TÉRMINOS Y CONDICIONES DE USO", nil) forState:UIControlStateNormal];
    [[registerLabel titleLabel] setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
    [registerLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [registerLabel addTarget:self action:@selector(termsAction) forControlEvents:UIControlEventTouchUpInside];
    
    [_scrollView addSubview:registerLabel];
    
    [self.view addSubview:_scrollView];
    
    [self headerView];
}

- (void)headerView{
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIView *_containerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_HEIGHT)];
    [_containerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIImageView *_icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:18], [UtilManager height:30], [UtilManager width:30.], [UtilManager height:30])];
    [_icon setBackgroundColor:[UIColor clearColor]];
    [_icon setContentMode:UIViewContentModeScaleAspectFit];
    [_containerView addSubview:_icon];
    
    UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, _containerView.frame.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [_titleLabel setTextColor:[UIColor whiteColor]];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setText:NSLocalizedString(@"CREAR CUENTA", nil)];
    [_titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [_titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [_containerView addSubview:_titleLabel];
    
    UIButton *_backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:30.], [UtilManager width:30], [UtilManager width:30])];
    [_backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [_backButton setHidden:FALSE];
    [_backButton setCenter:CGPointMake(_backButton.center.x, NAV_HEIGHT/2 + [UtilManager height:5])];
    [_containerView addSubview:_backButton];
    [_containerView setHidden:FALSE];
    
    [[self view] addSubview:_containerView];
}

- (void)didPressBackButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didPressRegisterButton {
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([emailTest evaluateWithObject:_mailTextField.text] == NO) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"¡alerta!" message:@"Nombre de usuario vacío." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if (_userTextField.text.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"¡alerta!" message:@"Por favor ingrese una dirección de correo electrónico válida." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if (_passwordTextField.text.length < 8) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"¡alerta!" message:@"La contraseña debe tener más de 8 caracteres." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [UserDefaultLibrary setWithKey:USER_PASSWORD WithObject:_passwordTextField.text];
        [UserDefaultLibrary setWithKey:USER_NAME WithObject:_userTextField.text];
        [MRProgressOverlayView showOverlayAddedTo:self.view title:NSLocalizedString(@"Registrando ...", nil) mode:MRProgressOverlayViewModeIndeterminate animated:YES];
        [WebServiceManager createUserWithDictionary:[self userDictionary] andDelegate:self];
    }
}

- (void)termsAction{
    TermsPrivacyVC *homeViewController = [[TermsPrivacyVC alloc] init];
    [self.navigationController pushViewController:homeViewController animated:YES];
}

- (void)facebookAction{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile",@"email"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    NSLog(@"Process error");
                                } else if (result.isCancelled) {
                                    NSLog(@"Cancelled");
                                } else {
                                    
                                    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                                    [parameters setValue:@"id,name,email" forKey:@"fields"];
                                    
                                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
                                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                                  id result, NSError *error) {
                                         
                                         [MRProgressOverlayView showOverlayAddedTo:self.view title:NSLocalizedString(@"Signup ...", nil) mode:MRProgressOverlayViewModeIndeterminate animated:TRUE];
                                         [WebServiceManager createUserWithFacebookWithDictionary:[self loginFacebookDictionary:[result objectForKey:@"name"] email:[result objectForKey:@"email"]] andDelegate:self];
                                     }];
                                }
                            }];
}

- (NSDictionary *)loginFacebookDictionary:(NSString*) username email: (NSString*) email{
    NSDictionary *loginFacebookDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:username,@"username",email,@"email", nil];
    return loginFacebookDictionary;
}

- (void)hideKeyBoards{
    [_userTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [_mailTextField resignFirstResponder];
    [_genderTextField resignFirstResponder];
    [_birthTextField resignFirstResponder];
}

- (void)presentDatePickerView
{
    [self hideKeyBoards];
    
    if(_datePicker)
    {
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - 216, self.view.frame.size.width, 216)];
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    [_datePicker setDate:[NSDate dateWithTimeIntervalSinceNow:-60*60*24*365 * 18]];
    [_datePicker setMaximumDate:[NSDate dateWithTimeIntervalSinceNow:-60*60*24*365 * 18]];
    [_datePicker setMinimumDate:[NSDate dateWithTimeIntervalSinceNow:-60*60*24*365 * 75]];
    [_datePicker setBackgroundColor:BLACK_APP_COLOR];
    
    [_datePicker setValue:YELLOW_APP_COLOR forKey:@"textColor"];
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_datePicker.frame.size.width - 80., _datePicker.frame.origin.y, 80., 24.)];
    [_acceptButton setTitleColor:GRAY_REGISTER_FONT forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptDatePickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_datePicker];
    [self.view addSubview:_acceptButton];
}

- (void)presentGenderSelector{
    
    [self hideKeyBoards];
    
    UIAlertController *genderController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"Seleccione un género", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *maleAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Male", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [_genderTextField setText:NSLocalizedString(@"MALE", nil)];
        [_scrollView setContentOffset:CGPointMake(0., 0.) animated:YES];
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }];
    
    UIAlertAction *femaleAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Female", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [_genderTextField setText:NSLocalizedString(@"FEMALE", nil)];
        [_scrollView setContentOffset:CGPointMake(0., 0.) animated:YES];
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }];
    
    [genderController addAction:maleAction];
    [genderController addAction:femaleAction];
    
    [self presentViewController:genderController animated:YES completion:^{
    
    }];
    
}

- (void)acceptDatePickerAction{
    
     [_scrollView setContentOffset:CGPointMake(0., 0.) animated:YES];
    
    if(_acceptButton && _datePicker){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSString *dateString = [dateFormatter stringFromDate:_datePicker.date];
        [_birthTextField setText:dateString];
        
        _selectedDate = _datePicker.date;
                
        [_acceptButton removeFromSuperview];
        _acceptButton = nil;
        
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
}

- (BOOL)checkFields{
    
    return TRUE;
}

- (NSDictionary *)userDictionary{
    NSDictionary *userDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:_mailTextField.text,@"email",_userTextField.text,@"username",_passwordTextField.text,@"password", nil];
    return userDictionary;
}

- (void)presentHomeViewController{
    MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:homeViewController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
     }];
}

#pragma mark ScrollViewdelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if([textField isEqual:_passwordTextField])
        [_passwordTextField setSecureTextEntry:YES];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:_passwordTextField])
        [textField resignFirstResponder];
    if([textField isEqual:_userTextField])
        [textField resignFirstResponder];
    if([textField isEqual:_mailTextField])
        [textField resignFirstResponder];
    if([textField isEqual:_birthTextField])
        [textField resignFirstResponder];
    if([textField isEqual:_genderTextField])
        [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self acceptDatePickerAction];
    
    if([textField isEqual:_birthTextField]){
        [_scrollView setContentOffset:CGPointMake(0., [UtilManager height:92]) animated:YES];
        [self presentDatePickerView];
        return FALSE;
    }
    
    if([textField isEqual:_genderTextField]){
        [_scrollView setContentOffset:CGPointMake(0., [UtilManager height:92]) animated:YES];
        [self presentGenderSelector];
        return FALSE;
    }

    /*
    if([textField isEqual:_emailTextField])
        [_scrollView setContentOffset:CGPointMake(0., [UtilManager height:15]) animated:YES];
    if([textField isEqual:_phoneTextField])
        [_scrollView setContentOffset:CGPointMake(0., [UtilManager height:255]) animated:YES];
    if([textField isEqual:_nifTextField])
        [_scrollView setContentOffset:CGPointMake(0., [UtilManager height:335]) animated:YES];
    if([_scrollView isEqual:_passwordTextField])
        [_scrollView setContentOffset:CGPointMake(0., [UtilManager height:475]) animated:YES];
    if([_scrollView isEqual:_repeatPasswordTextField])
        [_scrollView setContentOffset:CGPointMake(0, [UtilManager height:555]) animated:YES];
    */
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength;
    
    newLength = [textField.text length] + [string length] - range.length;
    
    if([textField isEqual:_passwordTextField])
        return (newLength > 8) ? NO : YES;
    if([textField isEqual:_mailTextField])
        return (newLength > 100) ? NO : YES;
    
    /*
    if([textField isEqual:_phoneTextField])
        return (newLength > 9) ? NO : YES;
    if([textField isEqual:_nifTextField])
        return (newLength > 9) ? NO : YES;
    
    if([textField isEqual:_repeatPasswordTextField])
        return (newLength > 8) ? NO : YES;
    */
    
    return YES;
}

#pragma mark WebServiceManagerDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        
    }];
    
    [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"Registro fallido.", YES)] animated:YES completion:^{
        
    }];
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{

    NSString * token = [object valueForKey:@"token"];
    
    PFUser * userForSignUp = [[PFUser alloc] init];
    userForSignUp.username = _userTextField.text;
    userForSignUp.email = _mailTextField.text;
    userForSignUp.password = _passwordTextField.text;
    userForSignUp[@"fullName"] = _userTextField.text;
    
    [userForSignUp signUpInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
            if (error == nil) {
                [self performChatSignUpWithCompletionBlock:^(BOOL isSuccess, NSString *error) {
                    if (!isSuccess) {
                        [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"Registro fallido.", YES)] animated:YES completion:nil];
                        return;
                    }
                    
                    RegisterDataVC *loginViewController = [[RegisterDataVC alloc] init];
                    loginViewController.tokenString = token;
                    [self.navigationController pushViewController:loginViewController animated:YES];
                }];
            }
            else {
                NSLog(@"Failed to signup to parse: %@", error.localizedDescription);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"Registro fallido.", YES)] animated:YES completion:nil];
                });
            }
        }];
    }];
}

- (void)performChatSignUpWithCompletionBlock:(void (^)(BOOL isSuccess, NSString * error)) completionBlock {
    SignupChatRequest * request = [[SignupChatRequest alloc] initWithUserName:_userTextField.text
                                                                     fullname:_userTextField.text
                                                                     password:_passwordTextField.text
                                                                        email:_mailTextField.text];
    
    [request postWithCompletionBlock:^(id response, NSString * errorMessage) {
        completionBlock(errorMessage == nil, errorMessage);
    }];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
    
    }];
    
    [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"Registro fallido.", YES)] animated:YES completion:^{
        
    }];
}

@end
