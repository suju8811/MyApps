//
//  TermsPrivacyVC.h
//  Titan
//
//  Created by MacOS on 10/30/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDefaultLibrary.h"
#import "AppDelegate.h"
#import "WebServiceManager.h"
#import "ParentViewController.h"
#import "MRProgress.h"

#import "MenuHomeViewController.h"
#import "REFrostedViewController.h"

@interface TermsPrivacyVC : UIViewController

@end
