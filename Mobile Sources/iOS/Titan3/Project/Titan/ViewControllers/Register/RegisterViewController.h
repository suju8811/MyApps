//
//  RegisterViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterDataVC.h"
#import "TermsPrivacyVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "ParentViewController.h"

@interface RegisterViewController : ParentViewController

@end
