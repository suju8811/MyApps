//
//  LoginViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "LoginViewController.h"
#import "MenuHomeViewController.h"
#import "REFrostedViewController.h"

#import "UserDefaultLibrary.h"
#import "AppDelegate.h"
#import "WebServiceManager.h"
#import "MRProgress.h"
#import "LoginExecutive.h"
#import <Parse/Parse.h>

@interface LoginViewController () <UITextFieldDelegate, LoginDisplay, WebServiceManagerDelegate>

@property (nonatomic, strong) UITextField *userTextField;
@property (nonatomic, strong) UITextField *passwordTextField;

@property (nonatomic, strong) UIButton *sessionButton;
@property (nonatomic, strong) LoginExecutive * executive;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    
    _executive = [[LoginExecutive alloc] initWithDisplay:self];
}

- (void)configureView{
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    _userTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:150], WIDTH - [UtilManager width:40], UITEXTFIELD_HEIGHT)];
    [_userTextField setBorderStyle:UITextBorderStyleNone];
    [[_userTextField layer] setMasksToBounds:YES];
    [_userTextField setTextColor:[UIColor whiteColor]];
    [_userTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_userTextField setReturnKeyType:UIReturnKeyDone];
    [_userTextField setDelegate:self];
    [_userTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_userTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_userTextField setPlaceholder:NSLocalizedString(@"USUARIO", nil)];
    [_userTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [_userTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_userTextField setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    _userTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"USUARIO", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
     ];
    
    UIView *leftUserView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40], _userTextField.frame.size.height)];
    [leftUserView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *userImageView = [[UIImageView alloc] initWithFrame:leftUserView.bounds];
    [userImageView setImage:[UIImage imageNamed:@"icon-user"]];
    [userImageView setContentMode:UIViewContentModeCenter];
    
    [leftUserView addSubview:userImageView];
    [_userTextField setLeftView:leftUserView];
    
    [self.view addSubview:_userTextField];
    
    UIView *separatorUserView = [[UIView alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _userTextField.frame.origin.y + _userTextField.frame.size.height, _userTextField.frame.size.width, 1)];
    [separatorUserView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self.view addSubview:separatorUserView];
    
    _passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _userTextField.frame.origin.y + _userTextField.frame.size.height + [UtilManager height:10], _userTextField.frame.size.width, UITEXTFIELD_HEIGHT)];
    [_passwordTextField setBorderStyle:UITextBorderStyleNone];
    [_passwordTextField setTextColor:[UIColor whiteColor]];
    [[_passwordTextField layer] setMasksToBounds:YES];
    [_passwordTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_passwordTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_passwordTextField setReturnKeyType:UIReturnKeyDone];
    [_passwordTextField setSecureTextEntry:YES];
    [_passwordTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_passwordTextField setRightViewMode:UITextFieldViewModeAlways];
    [_passwordTextField setPlaceholder:NSLocalizedString(@"CONTRASEÑA", nil)];
    [_passwordTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [_passwordTextField setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    _passwordTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"CONTRASEÑA", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
     ];
    
    UIView *leftPasswordView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40], _passwordTextField.frame.size.height)];
    [leftPasswordView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *passwordImageView = [[UIImageView alloc] initWithFrame:leftPasswordView.bounds];
    [passwordImageView setImage:[UIImage imageNamed:@"icon-password"]];
    [passwordImageView setContentMode:UIViewContentModeCenter];
    
    [leftPasswordView addSubview:passwordImageView];
    
    [_passwordTextField setLeftView:leftPasswordView];
    
    [_passwordTextField setDelegate:self];
    [self.view addSubview:_passwordTextField];
    
    UIView *separatorPasswordView = [[UIView alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _passwordTextField.frame.origin.y + _passwordTextField.frame.size.height, _userTextField.frame.size.width, 1)];
    [separatorPasswordView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self.view addSubview:separatorPasswordView];
    
    _sessionButton = [[UIButton alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _passwordTextField.frame.origin.y + _passwordTextField.frame.size.height + [UtilManager height:40], _userTextField.frame.size.width, [UtilManager height:60])];
    [_sessionButton setBackgroundColor:YELLOW_APP_COLOR];
    [_sessionButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_sessionButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [[_sessionButton titleLabel] setFont:[UIFont fontWithName:BOLD_DOLCE_FONT size:21]];
    [_sessionButton setTitle:NSLocalizedString(@"INICIAR SESIÓN", nil) forState:UIControlStateNormal];
    [_sessionButton addTarget:self action:@selector(didPressLoginButton) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_sessionButton];
    
    UILabel *orLabel = [[UILabel alloc] initWithFrame:CGRectMake(_sessionButton.frame.origin.x, _sessionButton.frame.origin.y + _sessionButton.frame.size.height, _sessionButton.frame.size.width, _sessionButton.frame.size.height)];
    [orLabel setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
    [orLabel setText:NSLocalizedString(@"o", nil)];
    [orLabel setTextAlignment:NSTextAlignmentCenter];
    [orLabel setTextColor:[UIColor whiteColor]];
    
    [self.view addSubview:orLabel];
    
    UIButton *registerButton = [[UIButton alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, orLabel.frame.origin.y + orLabel.frame.size.height, _userTextField.frame.size.width, LOGIN_BUTTON_HEIGHT)];
    [registerButton setBackgroundColor:BLUE_FACEBOOK_FONT];
    [registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[registerButton titleLabel] setFont:[UIFont fontWithName:BOLD_DOLCE_FONT size:21]];
    [registerButton setTitle:NSLocalizedString(@"INICIAR SESIÓN CON FACEBOOK", nil) forState:UIControlStateNormal];
    [registerButton addTarget:self action:@selector(facebookAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:registerButton];
    
    UILabel *orLabelThashi = [[UILabel alloc] initWithFrame:CGRectMake(registerButton.frame.origin.x, registerButton.frame.origin.y + registerButton.frame.size.height, registerButton.frame.size.width, registerButton.frame.size.height)];
    [orLabelThashi setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
    [orLabelThashi setText:NSLocalizedString(@"o", nil)];
    [orLabelThashi setTextAlignment:NSTextAlignmentCenter];
    [orLabelThashi setTextColor:[UIColor whiteColor]];
    
    [self.view addSubview:orLabelThashi];
    
    UIButton *touchButton = [[UIButton alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, orLabelThashi.frame.origin.y + orLabelThashi.frame.size.height, _userTextField.frame.size.width, [UtilManager height:60])];
    [touchButton setBackgroundColor:YELLOW_APP_COLOR];
    [touchButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [touchButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [[touchButton titleLabel] setFont:[UIFont fontWithName:BOLD_DOLCE_FONT size:20]];
    [touchButton setTitle:NSLocalizedString(@"INICIAR SESIÓN HUELLA DACTILAR", nil) forState:UIControlStateNormal];
    [touchButton addTarget:self action:@selector(touchLogin) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:touchButton];
    
    UILabel *forguetLabel = [[UILabel alloc] initWithFrame:CGRectMake(orLabelThashi.frame.origin.x, touchButton.frame.origin.y + touchButton.frame.size.height + 10, orLabelThashi.frame.size.width, LOGIN_BUTTON_HEIGHT)];
    [forguetLabel setTextAlignment:NSTextAlignmentCenter];
    [forguetLabel setText:NSLocalizedString(@"¿Has olvidado tu contraseña?", nil)];
    [forguetLabel setFont:[UIFont fontWithName:REGULAR_FONT size:15]];
    [forguetLabel setTextColor:[UIColor whiteColor]];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(forguetAction)];
    [forguetLabel setUserInteractionEnabled:YES];
    [forguetLabel addGestureRecognizer:tapGesture];
    
    [self.view addSubview:forguetLabel];
    
    [self headerView];
}

- (void)headerView{
    UIView *_containerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_HEIGHT)];
    [_containerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIImageView *_icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:18], [UtilManager height:30], [UtilManager width:30.], [UtilManager height:30])];
    [_icon setBackgroundColor:[UIColor clearColor]];
    [_icon setContentMode:UIViewContentModeScaleAspectFit];
    [_containerView addSubview:_icon];
    
    UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, _containerView.frame.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [_titleLabel setTextColor:[UIColor whiteColor]];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setText:NSLocalizedString(@"INICIAR SESIÓN", nil)];
    [_titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [_titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [_containerView addSubview:_titleLabel];
    
    UIButton *_backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:30.], [UtilManager width:30], [UtilManager width:30])];
    [_backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [_backButton setHidden:FALSE];
    [_backButton setCenter:CGPointMake(_backButton.center.x, NAV_HEIGHT/2 + [UtilManager height:5])];
    [_containerView addSubview:_backButton];
    
    [[self view] addSubview:_containerView];
}

- (void)didPressBackButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)facebookAction{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             
             NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
             [parameters setValue:@"id,name,email" forKey:@"fields"];
             
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
              startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                           id result, NSError *error) {
                  
                  [MRProgressOverlayView showOverlayAddedTo:self.view title:NSLocalizedString(@"Login ...", nil) mode:MRProgressOverlayViewModeIndeterminate animated:TRUE];
                  [WebServiceManager authenticateUserWithFacebook:[self loginFacebookDictionary:[result objectForKey:@"name"] email:[result objectForKey:@"email"]] andDelegate:self];
              }];
         }
     }];
}

- (NSDictionary *)loginFacebookDictionary:(NSString*) username email: (NSString*) email{
    NSDictionary *loginFacebookDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:username,@"username",email,@"email", nil];
    return loginFacebookDictionary;
}

- (void)touchLogin {
    FingerPrintVC *loginViewController = [[FingerPrintVC alloc] init];
    [self.navigationController pushViewController:loginViewController animated:YES];
}

- (void)didPressLoginButton {
    [_executive didPressLoginButton];
    
}

- (void)forguetAction{
    
    RecoveryViewController *recoveryViewController = [[RecoveryViewController alloc] init];
    [self.navigationController pushViewController:recoveryViewController animated:YES];
}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark WebServiceManager Delegate

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    [PFUser logInWithUsernameInBackground:_userTextField.text
                                 password:_passwordTextField.text
                                    block:^(PFUser * _Nullable user, NSError * _Nullable error) {
                                        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
                                            if (user != nil) {
                                                
                                                    MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
                                                
                                                    [NSTimer scheduledTimerWithTimeInterval:0.5 repeats:FALSE block:^(NSTimer *timer){
                                                        [AppContext saveInitialData];
                                                    }];
                                                
                                                    if (webServiceType == WebServiceTypeAuthenticate) {
                                                        
                                                        [UserDefaultLibrary setWithKey:IS_USER_LOGIN WithObject:[NSNumber numberWithBool:TRUE]];
                                                        [UserDefaultLibrary setWithKey:USER_PASSWORD WithObject:_passwordTextField.text];
                                                        [UserDefaultLibrary setWithKey:USER_NAME WithObject:_userTextField.text];
                                                    }
                                                
                                                    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                                                                        toView:homeViewController.view
                                                                      duration:1.5
                                                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                                                    completion:^(BOOL finished)
                                                     {
                                                         [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
                                                     }];
                                            }
                                            else {
                                                NSLog(@"Failed to login to parse: %@", error.localizedDescription);
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"No puedes loguearte sin registrarte.", YES)] animated:YES completion:nil];
                                                });
                                            }
                                        }];
                                    }];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:nil];
    
    [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"No puedes loguearte sin registrarte.", YES)] animated:YES completion:nil];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        
    }];
    
    [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"No puedes loguearte sin registrarte.", YES)] animated:YES completion:^{
            
    }];
}

#pragma mark - LoginDisplay Delegate

- (LoginRequest*)getLoginRequest {
    LoginRequest * loginRequest = [[LoginRequest alloc] initWithUserName:_userTextField.text
                                                                password:_passwordTextField.text];
    
    return loginRequest;
}

- (void)didLoginSuccess {
    
    MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:homeViewController.view
                      duration:1.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
     }];
}

@end
