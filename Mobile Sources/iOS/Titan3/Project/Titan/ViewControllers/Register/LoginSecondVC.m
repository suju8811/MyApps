//
//  LoginSecondVC.m
//  Titan
//
//  Created by MacOS on 10/30/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "LoginSecondVC.h"
#import "UserDefaultLibrary.h"
#import "AppDelegate.h"
#import "WebServiceManager.h"
#import "ParentViewController.h"
#import "MRProgress.h"

#import "MenuHomeViewController.h"
#import "REFrostedViewController.h"

@interface LoginSecondVC ()

@end

@implementation LoginSecondVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureView{
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:120], WIDTH - 2*[UtilManager width:10], [UtilManager height:160])];
    [descriptionLabel setFont:[UIFont fontWithName:LIGHT_FONT size:12]];
    descriptionLabel.numberOfLines = 10;
    [descriptionLabel setText:NSLocalizedString(@"SENTIMOS INSISTIR, PERO TE RECOMENDAMOS QUE LE ECHES UN VISTAZO AL PRIMER VIDEO-TUTORIAL AL QUE HEMOS LLAMADO: “PRIMEROS PASOS”. TE ENSEÑARÁ MUY RÁPIDO COMO EMPEZAR A MANEJARTE CON TITAN.", nil)];
    [descriptionLabel setBackgroundColor:[UIColor clearColor]];
    [descriptionLabel setAdjustsFontSizeToFitWidth:YES];
    [descriptionLabel setTextColor:[UIColor whiteColor]];
    
    [self.view addSubview:descriptionLabel];
    
    UIButton *playButton = [[UIButton alloc] initWithFrame:CGRectMake(descriptionLabel.frame.size.width/2 - [UtilManager height:60] , descriptionLabel.frame.origin.y + descriptionLabel.frame.size.height + [UtilManager height:40], [UtilManager height:120], [UtilManager height:120])];
    
    [playButton setImage:[UIImage imageNamed:@"btn_player"] forState:UIControlStateNormal];
    [playButton addTarget:self action:@selector(onPlayAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:playButton];
    
    [self headerView];
}

- (void) onPlayAction
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.youtube.com"]];
}

- (void)headerView{
    
    UIView *_containerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_HEIGHT)];
    [_containerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIImageView *_icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:18], [UtilManager height:30], [UtilManager width:30.], [UtilManager height:30])];
    [_icon setBackgroundColor:[UIColor clearColor]];
    [_icon setContentMode:UIViewContentModeScaleAspectFit];
    [_containerView addSubview:_icon];
    
    UILabel *_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, _containerView.frame.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [_titleLabel setTextColor:[UIColor whiteColor]];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setText:NSLocalizedString(@"PRIMEROS PASOS", nil)];
    [_titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [_titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [_containerView addSubview:_titleLabel];
    
    UIButton *_backButton = [[UIButton alloc] initWithFrame:CGRectMake([[UIApplication sharedApplication] statusBarFrame].size.width - [UtilManager width:40],[[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], [UtilManager width:30], [UtilManager width:30])];
    [_backButton setImage:[UIImage imageNamed:@"btn_close"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [_backButton setHidden:FALSE];
    [_backButton setCenter:CGPointMake(_backButton.center.x, NAV_HEIGHT/2 + [UtilManager height:5])];
    [_containerView addSubview:_backButton];
    
    [[self view] addSubview:_containerView];
}

- (void) didPressBackButton {
    
    MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:homeViewController.view
                      duration:1.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
     }];
}

@end
