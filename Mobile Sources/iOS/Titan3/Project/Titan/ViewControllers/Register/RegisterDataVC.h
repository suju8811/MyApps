//
//  RegisterDataVC.h
//  Titan
//
//  Created by MacOS on 10/29/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginFirstVC.h"
#import "UserDefaultLibrary.h"
#import "AppDelegate.h"
#import "WebServiceManager.h"
#import "ParentViewController.h"
#import "MRProgress.h"

#import "MenuHomeViewController.h"
#import "REFrostedViewController.h"

@interface RegisterDataVC : UIViewController<WebServiceManagerDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, retain) NSString *tokenString;
@end
