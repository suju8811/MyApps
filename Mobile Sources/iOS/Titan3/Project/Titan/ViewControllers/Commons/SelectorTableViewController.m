//
//  SelectorTableViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 14/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SelectorTableViewController.h"
#import "UIImageView+AFNetworking.h"
#import "SearchTableViewController.h"
#import "SeriesViewController.h"
#import "SelectorCell.h"
#import "FitnessCell.h"
#import "Exercise.h"
#import "Training.h"
#import "SelectTagView.h"
#import "ExerciseCell.h"
#import "PlanTitleCell.h"
#import "TagUtil.h"

@interface SelectorTableViewController ()<UITableViewDataSource, UITableViewDelegate, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate,SelectTagViewDelegate>

@property (nonatomic, strong) UIView *segmentControlView;
@property (nonatomic, strong) UISegmentedControl *segmentControl;
@property (nonatomic) NSInteger segmentState;

@property (nonatomic, strong) UITableView *selectorTableView;
@property (nonatomic, strong) UITableView *selectedTableView;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) UIView *searchContainerView;
@property (nonatomic, strong) SearchTableViewController *searchTableViewController;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) UILabel *selectExercisesLabel;
@property (nonatomic, strong) NSMutableArray *tags;
@property (nonatomic, strong) NSArray *totalItems;
@property (nonatomic, strong) UIButton *actionButton;
@property (nonatomic, strong) UIButton *okButton;

@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;

@end

@implementation SelectorTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _segmentState = 0;
    
    if (_selectorType == SelectorTypeTrainingPlan) {
        _items = [TrainingPlan getMyTrainingPlans];
        
        _searchResults = [[NSMutableArray alloc] initWithCapacity:_items.count];
        for (TrainingPlan * trainingPlan in _items) {
            [_searchResults addObject:trainingPlan];
        }
    }
    else if (_selectorType == SelectorTypeTrainingSingle) {
        _items = [Training getMyTrainings];
        
        _searchResults = [[NSMutableArray alloc] init];
        _selectTrainings = [[NSMutableArray alloc] initWithCapacity:0];
        
        if (_selectTrainingPlan) {
            for(Training *training in _items){
                if (![training.serverTrainingId isEqualToString:_selectTrainingDayContent.training.serverTrainingId]) {
                    [_searchResults addObject:training];
                }
            }
        }
    }
    else if (_selectorType == SelectorTypeTrainings) {
        _items = [Training getMyTrainings];
        
        _searchResults = [[NSMutableArray alloc] initWithCapacity:_items.count];
        _selectTrainings = [[NSMutableArray alloc] initWithCapacity:0];
        
        for(Training *training in _items){
            [_searchResults addObject:training];
        }
    }
    else if (_selectorType == SelectorTypeExerciseSingle) {
        _items = [Exercise getAnaerobicExercises];
        _searchResults = [[NSMutableArray alloc] init];
        _selectsExercises = [[NSMutableArray alloc] init];
        
        if (_selectTraining) {
            TrainingExercise * replacingTrainingExercise = _selectTraining.trainingExercises[_selectedFitnessIndex];
            Exercise * replacingExercise = [replacingTrainingExercise.exercises firstObject];
            for (Exercise * exerciseItem in _items) {
                if (![replacingExercise.exerciseId isEqualToString:exerciseItem.exerciseId]) {
                    [_searchResults addObject:exerciseItem];
                }
            }
        }
    }
    else {
        _items = [Exercise getAnaerobicExercises];
        _searchResults = [[NSMutableArray alloc] init];
        _selectsExercises = [[NSMutableArray alloc] initWithCapacity:0];
        RLMRealm *realm = [AppContext currentRealm];
        if(_selectTraining){
            for(TrainingExercise *trainingExercise in _selectTraining.trainingExercises){
                
                for(Exercise *exercise in trainingExercise.exercises){
                    for(Exercise *exerciseItem in _items)
                    {
                        if([exercise isEqual:exerciseItem]){
                            [realm transactionWithBlock:^{
                                [exercise setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                            }];
                            [_selectsExercises addObject:exercise];
                        }
                    }
                }
            }
        }
        
        [realm beginWriteTransaction];
        NSError *error;
        [realm commitWriteTransaction:&error];
        if(error){
            NSLog(@"%@",error.description);
        }
        
        for(Exercise *exercise in _items) {
            [_searchResults addObject:exercise];
        }
    }
    
    _totalItems = [[NSMutableArray alloc] initWithArray:_searchResults];
    
    [self configureView];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];

    if (_selectorType == SelectorTypeTrainingPlan) {
        return;
    }
    
    [Exercise deselectAllExercises];
    [Training deselectAllExercises];
    [Tag deselectAllTags];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // restore the searchController's active state
    if (self.searchControllerWasActive) {
        self.searchController.active = self.searchControllerWasActive;
        _searchControllerWasActive = NO;
        
        if (self.searchControllerSearchFieldWasFirstResponder) {
            [self.searchController.searchBar becomeFirstResponder];
            _searchControllerSearchFieldWasFirstResponder = NO;
        }
    }
}

- (void)configureView{
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_HEIGHT)];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    _searchContainerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:44], [UtilManager height:22], WIDTH - [UtilManager width:88], [UtilManager height:40])];
    [_searchContainerView addSubview:self.searchController.searchBar];
    [_searchContainerView setBackgroundColor:[UIColor clearColor]];
    self.searchController.searchBar.frame = _searchContainerView.frame;
    [self.searchController.searchBar setCenter:CGPointMake(_searchContainerView.frame.size.width/2,_searchContainerView.frame.size.height/2)];
    
    [headerView addSubview:_searchContainerView];
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:40], [UtilManager height:20], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [okButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
    [okButton setCenter:CGPointMake(okButton.center.x, _searchContainerView.center.y)];
    
    [headerView addSubview:okButton];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                      [UtilManager height:20.f],
                                                                      [UtilManager width:40] * 0.8f,
                                                                      [UtilManager width:40] * 0.8f)];
    
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    backButton.hidden = NO;
    backButton.center = CGPointMake(backButton.center.x, _searchContainerView.center.y);
    [headerView addSubview:backButton];
    [self.view addSubview:headerView];
    
    //
    // Add segment control view
    //
    
    CGFloat segmentControlHeight = (_selectorType == SelectorTypeExercise || _selectorType == SelectorTypeExerciseSingle) ? TAB_BAR_HEIGHT : 0;
    _segmentControlView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                   CGRectGetMaxY(headerView.frame),
                                                                   WIDTH,
                                                                   segmentControlHeight)];
    
    _segmentControlView.backgroundColor = UIColor.clearColor;
    _segmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Anaeróbicos", nil),
                                                                  NSLocalizedString(@"Aeróbicos", nil)]];
    
    _segmentControl.selectedSegmentIndex = _segmentState;
    [_segmentControl addTarget:self action:@selector(changeExercisesView:) forControlEvents:UIControlEventValueChanged];
    _segmentControl.center = CGPointMake(_segmentControlView.center.x, [UtilManager height:25]);
    _segmentControl.tintColor = YELLOW_APP_COLOR;
    [_segmentControlView addSubview:_segmentControl];
    [self.view addSubview:_segmentControlView];
    
    //
    // Add table view
    //
    
    _selectorTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.f,
                                                                       CGRectGetMaxY(_segmentControlView.frame),
                                                                       WIDTH,
                                                                       HEIGHT - [UtilManager height:40] - NAV_HEIGHT)
                                                      style:UITableViewStylePlain];
    
    [_selectorTableView registerClass:[FitnessCell class] forCellReuseIdentifier:@"fitnessCell"];
    [_selectorTableView registerClass:[ExerciseCell class] forCellReuseIdentifier:@"exerciseCell"];
    [_selectorTableView registerClass:[PlanTitleCell class] forCellReuseIdentifier:@"planTitleCell"];
    _selectorTableView.delegate = self;
    _selectorTableView.dataSource =  self;
    _selectorTableView.multipleTouchEnabled = YES;
    _selectorTableView.bounces = NO;
    _selectorTableView.backgroundColor = UIColor.clearColor;
    _selectorTableView.allowsMultipleSelection = YES;
    _selectorTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_selectorTableView];
    
    /**
     **/
    
    _selectedTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, HEIGHT/3) style:UITableViewStylePlain];
    [_selectedTableView setDelegate:self];
    [_selectedTableView setDataSource:self];
    [_selectedTableView setMultipleTouchEnabled:YES];
    [_selectedTableView setBounces:NO];
    [_selectedTableView setAllowsMultipleSelection:TRUE];
    [_selectedTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_selectedTableView registerClass:[FitnessCell class] forCellReuseIdentifier:@"fitnessCell"];
    [_selectedTableView registerClass:[ExerciseCell class] forCellReuseIdentifier:@"exerciseCell"];
    [_selectedTableView setBackgroundColor:BLACK_APP_COLOR];
    
    [self.view addSubview:_selectedTableView];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    
    self.searchController.searchResultsUpdater = self;
    [_searchController setDelegate:self];
    [self.searchController.searchBar sizeToFit];
    self.searchController.searchBar.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    [self.searchController.searchBar setSearchBarStyle:UISearchBarStyleMinimal];
    [self.searchController.searchBar setBarTintColor:BLACK_APP_COLOR];
    [self.searchController.searchBar setKeyboardAppearance:UIKeyboardAppearanceDark];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]
     setTintColor:[UIColor whiteColor]];
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]
     setTintColor:[UIColor whiteColor]];
    [self.searchController.searchBar setTintColor:[UIColor whiteColor]];
    
    for (UIView *subView in self.searchController.searchBar.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                searchBarTextField.textColor = [UIColor whiteColor];
                break;
            }
        }
    }
    
    //_selectorTableView.tableHeaderView = headerView;
    
    NSMutableArray *scopeButtonTitles = [[NSMutableArray alloc] init];
    [scopeButtonTitles addObject:NSLocalizedString(@"All", @"Search display controller All button.")];
    
    self.definesPresentationContext = TRUE;
}

- (void)didPressOKButton {
    
    if (_selectorType == SelectorTypeTrainingPlan) {
        
        if (_selectedSinglePlan == nil) {
            [self showMessage:@"Please select at least a training plan"];
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:^{
                [_delegate didSelectTrainingPlan:_selectedSinglePlan];
            }];
        });
        
        
        RLMRealm *realm = [AppContext currentRealm];
        [realm transactionWithBlock:^{
            for (TrainingPlan * plan in _items) {
                [plan setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
            }
        }];
        return;
    }
    else if (_selectorType == SelectorTypeExerciseSingle ||
             _selectorType == SelectorTypeTrainingSingle) {
        
        _presentWorkoutDetailForReplacing = YES;
    }
    else {
        _presentWorkoutDetail = YES;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didPressBackButton {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSMutableArray *)getReturnData
{
    NSMutableArray *dataSelected = [[NSMutableArray alloc] init];
    
    return dataSelected;
}

- (UIView *)headerView{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:80])];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50], [UtilManager height:20], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [okButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
    
    [headerView addSubview:okButton];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:20.], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton setHidden:FALSE];
    [headerView addSubview:backButton];
    
    return headerView;
}

- (void)reloadResults{
    _searchResults = [[NSMutableArray alloc] initWithCapacity:_items.count];
    RLMRealm *realm = [AppContext currentRealm];
    
    if(_selectorType == SelectorTypeExercise){
        for(Exercise *exercise in _items){
            for(Exercise *selectExercise in _selectsExercises)
            {
                if([selectExercise isEqual:exercise])
                {
                    [realm transactionWithBlock:^{
                        [exercise setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                    }];
                    break;
                }
            }
            [_searchResults addObject:exercise];
        }
    }else if(_selectorType == SelectorTypeTrainings){
        
        for(Training *training in _items){
            for(Training *selectTraining in _selectTrainings){
                if([selectTraining isEqual:training]){
                    [realm transactionWithBlock:^{
                        [training setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                    }];
                    break;
                }
            }
            [_searchResults addObject:training];
        }
    }
}

- (void)openSelectedView{
    [_selectedTableView reloadData];
    
    [UIView animateWithDuration:0.5 animations:^{
        if(_selectorType == SelectorTypeExercise) {
            if(_selectsExercises.count > 0 && _selectedTableView.frame.origin.y == HEIGHT - TAB_BAR_HEIGHT){
                [_selectedTableView setFrame:CGRectMake(0., HEIGHT -HEIGHT/3, WIDTH, _selectedTableView.frame.size.height)];
                [_actionButton setImage:[UIImage imageNamed:@"btn-down"] forState:UIControlStateNormal];
            }else /*if (_selectedTableView.frame.origin.y == HEIGHT - HEIGHT/3)*/{
                [_selectedTableView setFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, _selectedTableView.frame.size.height)];
                [_actionButton setImage:[UIImage imageNamed:@"btn-up"] forState:UIControlStateNormal];
            }
        } else {
            if(_selectTrainings.count > 0 && _selectedTableView.frame.origin.y == HEIGHT - TAB_BAR_HEIGHT){
                [_selectedTableView setFrame:CGRectMake(0., HEIGHT -HEIGHT/3, WIDTH, _selectedTableView.frame.size.height)];
                [_actionButton setImage:[UIImage imageNamed:@"btn-down"] forState:UIControlStateNormal];
            }else /*if (_selectedTableView.frame.origin.y == HEIGHT - HEIGHT/3)*/{
                [_selectedTableView setFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, _selectedTableView.frame.size.height)];
                [_actionButton setImage:[UIImage imageNamed:@"btn-up"] forState:UIControlStateNormal];
            }
        }
    } completion:^(BOOL finished){
    
    }];
}

#pragma mark SelectTagDelegate

- (void)actionDidPushReturnTags:(NSMutableArray *)selectedTags{
    
    NSMutableArray *auxSearch = [[NSMutableArray alloc] initWithCapacity:_items.count];
    
    if(selectedTags.count > 0){
        for(Tag *tag in selectedTags){
            
            if(_selectorType == SelectorTypeTrainings){
            
            }else{
                RLMResults *firstMuscles = [Exercise objectsWhere:@"tag1 == %@",tag.tagId];
                RLMResults *secondMuscle = [Exercise objectsWhere:@"tag2 == %@",tag.tagId];
                RLMResults *thirdMuscle = [Exercise objectsWhere:@"tag3 == %@",tag.tagId];
                
                for(Exercise *exercise in firstMuscles){
                    if(![auxSearch containsObject:exercise])
                        [auxSearch addObject:exercise];
                }
                
                for(Exercise *exercise in secondMuscle){
                    if(![auxSearch containsObject:exercise])
                        [auxSearch addObject:exercise];
                }
                
                for(Exercise *exercise in thirdMuscle){
                    if(![auxSearch containsObject:exercise])
                        [auxSearch addObject:exercise];
                }
            }
            
            _searchResults = [auxSearch mutableCopy];
        }
        
        
        [_selectorTableView reloadData];
    } else {
        [self reloadResults];
        [_selectorTableView reloadData];
    }
    
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [_searchController.searchBar resignFirstResponder];
    
    if(tableView == _selectorTableView) {
        if (_selectorType == SelectorTypeTrainingPlan) {
            TrainingPlan * plan = [_searchResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            if (plan.isSelected) {
                [realm transactionWithBlock:^{
                    [plan setValue:@NO forKey:@"isSelected"];
                }];
                _selectedSinglePlan = nil;
            }else{
                [realm transactionWithBlock:^{
                    [plan setValue:@YES forKey:@"isSelected"];
                }];
                _selectedSinglePlan = plan;
            }
        }
        else if(_selectorType == SelectorTypeExercise) {
            Exercise *exercise = [_searchResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            if(exercise.isSelected){
                [realm transactionWithBlock:^{
                    [exercise setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
                }];
                [_selectsExercises removeObject:exercise];
            }else{
                [realm transactionWithBlock:^{
                    [exercise setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                }];
                [_selectsExercises addObject:exercise];
            }
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectsExercises.count,NSLocalizedString(@"Ejercicios seleccionados", nil)]];
        }
        else if (_selectorType == SelectorTypeExerciseSingle) {
            Exercise *replacingExercise = [_searchResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            [_selectsExercises removeAllObjects];
            for (Exercise * exercise in _searchResults) {
                [realm transactionWithBlock:^{
                    [exercise setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
                }];
            }
            
            [_selectsExercises addObject:replacingExercise];
            [realm transactionWithBlock:^{
                [replacingExercise setValue:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
            }];
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectsExercises.count,NSLocalizedString(@"Ejercicios seleccionados", nil)]];
        }
        else if (_selectorType == SelectorTypeTrainingSingle) {
            Training * replacingTraining = [_searchResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            [_selectTrainings removeAllObjects];
            for (Training * training in _searchResults) {
                [realm transactionWithBlock:^{
                    [training setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
                }];
            }
            
            [_selectTrainings addObject:replacingTraining];
            [realm transactionWithBlock:^{
                [replacingTraining setValue:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
            }];
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectTrainings.count,NSLocalizedString(@"Entrenamientos seleccionados", nil)]];
        }
        else {
            Training *training = [_searchResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            if(training.isSelected.boolValue){
                [realm transactionWithBlock:^{
                    [training setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
                }];
                [_selectTrainings removeObject:training];
            }else{
                [realm transactionWithBlock:^{
                    [training setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                }];
                [_selectTrainings addObject:training];
            }
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectTrainings.count,NSLocalizedString(@"Entrenamientos seleccionados", nil)]];
        }
    } else {
        if(_selectorType == SelectorTypeExercise){
            
            Exercise *exercise = [_selectsExercises objectAtIndex:indexPath.row];
            
            Exercise *searchExercise;
            
            for(Exercise *exer in _searchResults){
                if([exer isEqual:exercise])
                    searchExercise = exer;
            }
            
            RLMRealm *realm = [AppContext currentRealm];
            
            if(exercise.isSelected){
                [realm transactionWithBlock:^{
                    [searchExercise setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
                }];
                [_selectsExercises removeObject:exercise];
            }else{
                [realm transactionWithBlock:^{
                    [searchExercise setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                }];
                [_selectsExercises addObject:exercise];
            }
            
            [_selectorTableView reloadData];
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectsExercises.count,NSLocalizedString(@"Ejercicios seleccionados", nil)]];
        } else{
            Training *training = [_selectTrainings objectAtIndex:indexPath.row];
            
            Training *searchTraining;
            
            for(Training *exer in _searchResults){
                if([exer isEqual:searchTraining])
                    searchTraining = exer;
            }
            
            RLMRealm *realm = [AppContext currentRealm];
            
            if(searchTraining.isSelected.boolValue){
                [realm transactionWithBlock:^{
                    [searchTraining setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
                }];
                [_selectTrainings removeObject:searchTraining];
            }else{
                [realm transactionWithBlock:^{
                    [searchTraining setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                }];
                [_selectTrainings addObject:training];
            }
            
            [_selectorTableView reloadData];
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectTrainings.count,NSLocalizedString(@"Entrenamientos seleccionados", nil)]];
        }
    }
    
    [tableView reloadData];
}

#pragma mark UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(tableView == _selectorTableView) {
        return _searchResults.count;
    }
    else{
        if(_selectorType == SelectorTypeExercise) {
            return _selectsExercises.count;
        }
        else {
            return _selectTrainings.count;
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    if(_selectorType == SelectorTypeExercise)
        return exerciseCellHeight;
    else
        return planCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_selectorType == SelectorTypeTrainingPlan) {
        return planTitleCellHeight;
    }
    if(_selectorType == SelectorTypeExercise) {
        return exerciseCellHeight;
    }
    else {
        return planCellHeight;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _selectorTableView) {
        if (_selectorType == SelectorTypeTrainingPlan) {
            PlanTitleCell *planTitleCell = [_selectorTableView dequeueReusableCellWithIdentifier:@"planTitleCell"];
            TrainingPlan * plan = [_searchResults objectAtIndex:indexPath.row];
            planTitleCell.titleLabel.text = plan.title;
            planTitleCell.daysLabel.text = [NSString stringWithFormat:@"%d días", (int)plan.trainingDay.count];
            
            if(plan.isSelected) {
                [[planTitleCell checkImageView] setHidden:NO];
            }
            else {
                [[planTitleCell checkImageView] setHidden:YES];
            }
            
            return planTitleCell;
        }
        else if(_selectorType == SelectorTypeExercise ||
                _selectorType == SelectorTypeExerciseSingle){
            
            ExerciseCell *exerciseCell = [_selectorTableView dequeueReusableCellWithIdentifier:@"exerciseCell"];
            
            Exercise *exercise = [_searchResults objectAtIndex:indexPath.row];
            [[exerciseCell titleLabel] setText:exercise.title];
            
            NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
            
            if(exercise.tag1 && exercise.tag1.length > 0){
                Tag *tag1 = [Tag tagWithId:exercise.tag1];
                if(tag1)
                    [tags addObject:tag1];
            }
            if(exercise.tag2 && exercise.tag2.length > 0){
                Tag *tag2 = [Tag tagWithId:exercise.tag2];
                if(tag2)
                    [tags addObject:tag2];
            }
            if(exercise.tag3 && exercise.tag3.length > 0){
                Tag *tag3 = [Tag tagWithId:exercise.tag3];
                if(tag3)
                    [tags addObject:tag3];
            }
            
            [[exerciseCell tagView] setTags:tags];
            [[exerciseCell tagView] reloadTagSubviews];
            
            UIImageView *imageView = exerciseCell.exerciseImageView;
            
            NSURLRequest *exerciseImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_EXERCISES,exercise.picture]]
                                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                              timeoutInterval:60];
            
            [exerciseCell.exerciseImageView setImageWithURLRequest:exerciseImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-small"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                [imageView setImage:image];
                [imageView setContentMode:UIViewContentModeScaleAspectFill];
                [[imageView layer] setMasksToBounds:YES];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                NSLog(@"%@",error);
                [imageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
            }];

            [exerciseCell setHidden:FALSE];
            
            if(exercise.isSelected)
                [[exerciseCell checkImageView] setHidden:FALSE];
            else
                [[exerciseCell checkImageView] setHidden:TRUE];
            
            return exerciseCell;
        }
        else {
            
            FitnessCell *fitnessCell = [_selectedTableView dequeueReusableCellWithIdentifier:@"fitnessCell"];
            
            Training *training = [_searchResults objectAtIndex:indexPath.row];
            
            NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
            
            if(training.tags){
                for(Tag *tag in training.tags){
                    //[tag setTitle:tag.title];
                    [tags addObject:tag];
                }
            }
            
            int secs = 0;
            
            for(TrainingExercise *trainingExercise in training.trainingExercises){
                secs = [TrainingExercise getExerciseTime:trainingExercise] + secs;
            }
            
            //if(secs / 60 >= 1)
            //{
            int minutes = secs / 60;
            //     int seconds = secs % 60;
            
            [fitnessCell.timeLabel setText:[NSString stringWithFormat:@"%d min",minutes]];
            // }else
            //     [cell.timeLabel setText:[NSString stringWithFormat:@"%d seg.",secs]];
            
            [[fitnessCell tagView] setTags:tags];
            [[fitnessCell tagView] reloadTagSubviews];
            
            [[fitnessCell calendarImageView] setHidden:TRUE];
            [[fitnessCell daysLabel] setHidden:TRUE];
            [[fitnessCell titleLabel] setText:training.title];
            [[fitnessCell contentLabel] setText:training.content];
            
            if(training.isSelected.boolValue)
                [[fitnessCell checkImageView] setHidden:FALSE];
            else
                [[fitnessCell checkImageView] setHidden:TRUE];
            
            [[fitnessCell menuButton] setHidden:TRUE];
            
            if(training.trainingExercises.count == 0)
            {
            }
            
            long count = 0;
            
            if(training.trainingExercises.count >= training.trainingExercisesCount.intValue)
                count = training.trainingExercises.count ;
            else
                count = training.trainingExercisesCount.intValue;
            
            [[fitnessCell exercisesLabel] setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)count,NSLocalizedString(@"Ejercicios", nil)]];
            
            return fitnessCell;
        }
    }
    else {
        if(_selectorType == SelectorTypeExercise){
            ExerciseCell *exerciseCell = [_selectedTableView dequeueReusableCellWithIdentifier:@"exerciseCell"];
            
            Exercise *exercise = [_selectsExercises objectAtIndex:indexPath.row];
            [[exerciseCell titleLabel] setText:exercise.title];
            
            NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
            
            if(exercise.tag1 && exercise.tag1.length > 0){
                Tag *tag1 = [Tag tagWithId:exercise.tag1];
                if(tag1)
                    [tags addObject:tag1];
            }
            if(exercise.tag2 && exercise.tag2.length > 0){
                Tag *tag2 = [Tag tagWithId:exercise.tag2];
                if(tag2)
                    [tags addObject:tag2];
            }
            if(exercise.tag3 && exercise.tag3.length > 0){
                Tag *tag3 = [Tag tagWithId:exercise.tag3];
                if(tag3)
                    [tags addObject:tag3];
            }
            
            [[exerciseCell tagView] setTags:tags];
            [[exerciseCell tagView] reloadTagSubviews];
            
            UIImageView *imageView = exerciseCell.exerciseImageView;
            
            NSURLRequest *exerciseImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_EXERCISES,exercise.picture]]
                                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                              timeoutInterval:60];
            
            [exerciseCell.exerciseImageView setImageWithURLRequest:exerciseImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-small"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                [imageView setImage:image];
                [imageView setContentMode:UIViewContentModeScaleAspectFill];
                [[imageView layer] setMasksToBounds:YES];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                NSLog(@"%@",error);
                [imageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
            }];
            
            
            [exerciseCell setHidden:FALSE];
            
            if(exercise.isSelected) {
                exerciseCell.checkImageView.hidden = NO;
            }
            else {
                exerciseCell.checkImageView.hidden = YES;
            }
            
            exerciseCell.backgroundColor = BLACK_HEADER_APP_COLOR;
            exerciseCell.contentView.backgroundColor = BLACK_HEADER_APP_COLOR;
            return exerciseCell;
        }
        else {
            
            FitnessCell *fitnessCell = [_selectorTableView dequeueReusableCellWithIdentifier:@"fitnessCell"];
            
            Training *training = [_searchResults objectAtIndex:indexPath.row];
            
            NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
            
            for(Tag *tag in training.tags){
                //[tag setTitle:tag.title];
                [tags addObject:tag];
            }
            
            int secs = 0;
            
            for(TrainingExercise *trainingExercise in training.trainingExercises){
                secs = [TrainingExercise getExerciseTime:trainingExercise] + secs;
            }
            
            //if(secs / 60 >= 1)
            //{
            int minutes = secs / 60;
            //     int seconds = secs % 60;
            [fitnessCell.timeLabel setText:[NSString stringWithFormat:@"%d min",minutes]];
            // }else
            //     [cell.timeLabel setText:[NSString stringWithFormat:@"%d seg.",secs]];
            
            [[fitnessCell tagView] setTags:tags];
            [[fitnessCell tagView] reloadTagSubviews];
            
            [[fitnessCell calendarImageView] setHidden:TRUE];
            [[fitnessCell daysLabel] setHidden:TRUE];
            [[fitnessCell titleLabel] setText:training.title];
            [[fitnessCell contentLabel] setText:training.content];
            
            if(training.isSelected)
            [[fitnessCell checkImageView] setHidden:FALSE];
            else
            [[fitnessCell checkImageView] setHidden:TRUE];
            
            [[fitnessCell menuButton] setHidden:TRUE];
            [[fitnessCell checkImageView] setHidden:FALSE];
            
            
            if(training.trainingExercises.count == 0)
            {
            }
            
            long count = 0;
            
            if(training.trainingExercises.count >= training.trainingExercisesCount.intValue)
            count = training.trainingExercises.count ;
            else
            count = training.trainingExercisesCount.intValue;
            
            [[fitnessCell exercisesLabel] setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)count,NSLocalizedString(@"Ejercicios", nil)]];
            
            [fitnessCell setBackgroundColor:BLACK_HEADER_APP_COLOR];
            [[fitnessCell contentView] setBackgroundColor:BLACK_HEADER_APP_COLOR];
            
            return fitnessCell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if([tableView isEqual:_selectedTableView])
        return [UtilManager height:50];
    else
        return [UtilManager height:50];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    if([tableView isEqual:_selectedTableView])
        return [UtilManager height:50];
    else
        return [UtilManager height:50];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:50])];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    if(tableView == _selectedTableView){
        [headerView setBackgroundColor:BLACK_APP_COLOR];
        _selectExercisesLabel = [[UILabel alloc] initWithFrame:headerView.bounds];
        [_selectExercisesLabel setTextAlignment:NSTextAlignmentCenter];
        [_selectExercisesLabel setTextColor:YELLOW_APP_COLOR];
        if(_selectorType == SelectorTypeExercise)
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectsExercises.count,NSLocalizedString(@"Ejercicios seleccionados", nil)]];
        else
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectTrainings.count,NSLocalizedString(@"Entrenamientos seleccionados", nil)]];
        [_selectExercisesLabel setUserInteractionEnabled:FALSE];
        
        
        [headerView addSubview:_selectExercisesLabel];
        
        //UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openSelectedView)];
        //[_selectExercisesLabel addGestureRecognizer:tapGesture];
        
        if(!_actionButton){
            _actionButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:20.], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
            [_actionButton setImage:[UIImage imageNamed:@"btn-up"] forState:UIControlStateNormal];
        }
        
        [_actionButton addTarget:self action:@selector(openSelectedView) forControlEvents:UIControlEventTouchUpInside];
        [_actionButton setCenter:CGPointMake(_actionButton.center.x, headerView.frame.size.height/2)];
        
        _okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:40], [UtilManager height:20], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
        [_okButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [_okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
        [_okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
        [_okButton setCenter:CGPointMake(_okButton.center.x, headerView.frame.size.height/2)];
        
        [headerView addSubview:_actionButton];
        
        //[headerView addSubview:_okButton];
        
        UITapGestureRecognizer *openGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openSelectedView)];
        [headerView addGestureRecognizer:openGesture];
        
        UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(openSelectedView)];
        [swipeGesture setDirection:UISwipeGestureRecognizerDirectionUp];
        
        [headerView addGestureRecognizer:swipeGesture];
        
        UISwipeGestureRecognizer *swipeDownGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(openSelectedView)];
        [swipeDownGesture setDirection:UISwipeGestureRecognizerDirectionDown];
        
        [headerView addGestureRecognizer:swipeDownGesture];
        
    }else{
        
        _tags = [TagUtil getTagsWithFitnessSelectorType:_selectorType];
        SelectTagView *selectTagView = [[SelectTagView alloc] initWithFrame:CGRectMake(0., [UtilManager height:10], WIDTH, [UtilManager height:30]) andTags:_tags];
        [selectTagView setDelegate:self];
        [headerView setBackgroundColor:BLACK_APP_COLOR];
        [headerView addSubview:selectTagView];
    }
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:50])];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    return footerView;
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    // update the filtered array based on the search text
    NSString *searchText = searchController.searchBar.text;
    
    // strip out all the leading and trailing spaces
    NSString *strippedString = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // break up the search terms (separated by spaces)
    NSArray *searchItems = nil;
    if (strippedString.length > 0) {
        searchItems = [strippedString componentsSeparatedByString:@" "];
    }
    
    NSPredicate *finalPredicate;
    for (NSString *searchString in searchItems) {
        
        // Below we use NSExpression represent expressions in our predicates.
        // NSPredicate is made up of smaller, atomic parts: two NSExpressions (a left-hand value and a right-hand value)
        
        // name field matching
        //finalPredicate = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@",searchString];
        finalPredicate = [NSPredicate predicateWithFormat:@"title CONTAINS[c] %@",searchString];
        
    }
    
    if(finalPredicate){
        _searchResults = [NSMutableArray arrayWithArray:[_totalItems filteredArrayUsingPredicate:finalPredicate]];
    }
    if(searchText.length == 0)
        [self reloadResults];
    
    //searchResults = [[searchResults filteredArrayUsingPredicate:finalPredicate] mutableCopy];
    // hand over the filtered results to our search results table
    self.definesPresentationContext = TRUE;
    
    [_selectorTableView reloadData];
}


#pragma mark - UISearchBarDelegate

// Workaround for bug: -updateSearchResultsForSearchController: is not called when scope buttons change
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
    [self updateSearchResultsForSearchController:self.searchController];
}


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    //[_searchContainerView setFrame:CGRectMake(0, 10, WIDTH , 40)];
    [_searchContainerView setBackgroundColor:[UIColor clearColor]];
    //[_searchContainerView removeFromSuperview];
    //[self.view addSubview:_searchContainerView];
    //[self.view bringSubviewToFront:_searchContainerView];
    //self.searchController.searchBar.frame = _searchContainerView.frame;
    //[self.searchController.searchBar setCenter:CGPointMake(_searchContainerView.frame.size.width/2,_searchContainerView.frame.size.height/2)];
    
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    //[_searchContainerView setFrame:CGRectMake([UtilManager width:44], 10, WIDTH - [UtilManager width:88], 40)];
    [_searchContainerView setBackgroundColor:[UIColor clearColor]];
    //self.searchController.searchBar.frame = _searchContainerView.frame;
    //[self.searchController.searchBar setCenter:CGPointMake(_searchContainerView.frame.size.width/2,_searchContainerView.frame.size.height/2)];
}


#pragma mark - Content Filtering


#pragma mark

- (void)willPresentSearchController:(UISearchController *)searchController{
    dispatch_async(dispatch_get_main_queue(), ^{
        //searchController.searchResultsController.view.hidden = NO;
        //[[searchController searchBar] setFrame:CGRectMake(0., 0., searchController.searchBar.frame.size.width, searchController.searchBar.frame.size.height)];
    });
}

- (void)didPresentSearchController:(UISearchController *)searchController{
    searchController.searchResultsController.view.hidden = NO;
}

- (void)willDismissSearchController:(UISearchController *)searchController{
    dispatch_async(dispatch_get_main_queue(), ^{
        //searchController.searchResultsController.view.hidden = YES;
        //[self.searchController.searchBar setFrame:CGRectMake(0., NAV_HEIGHT -  self.searchController.searchBar.frame.size.height, WIDTH, self.searchController.searchBar.frame.size.height)];
    });
}

- (void)didDismissSearchController:(UISearchController *)searchController{
    
}

- (void)presentSearchController:(UISearchController *)searchController{
    
}

#pragma mark - UIStateRestoration

// we restore several items for state restoration:
//  1) Search controller's active state,
//  2) search text,
//  3) first responder

NSString *const ViewControllerTitleKey = @"ViewControllerTitleKey";
NSString *const SearchControllerIsActiveKey = @"SearchControllerIsActiveKey";
NSString *const SearchBarTextKey = @"SearchBarTextKey";
NSString *const SearchBarIsFirstResponderKey = @"SearchBarIsFirstResponderKey";

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    [super encodeRestorableStateWithCoder:coder];
    
    // encode the view state so it can be restored later
    
    // encode the title
    [coder encodeObject:self.title forKey:ViewControllerTitleKey];
    
    UISearchController *searchController = self.searchController;
    
    // encode the search controller's active state
    BOOL searchDisplayControllerIsActive = searchController.isActive;
    [coder encodeBool:searchDisplayControllerIsActive forKey:SearchControllerIsActiveKey];
    
    // encode the first responser status
    if (searchDisplayControllerIsActive) {
        [coder encodeBool:[searchController.searchBar isFirstResponder] forKey:SearchBarIsFirstResponderKey];
    }
    
    // encode the search bar text
    [coder encodeObject:searchController.searchBar.text forKey:SearchBarTextKey];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    [super decodeRestorableStateWithCoder:coder];
    
    // restore the title
    self.title = [coder decodeObjectForKey:ViewControllerTitleKey];
    
    // restore the active state:
    // we can't make the searchController active here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerWasActive = [coder decodeBoolForKey:SearchControllerIsActiveKey];
    
    // restore the first responder status:
    // we can't make the searchController first responder here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerSearchFieldWasFirstResponder = [coder decodeBoolForKey:SearchBarIsFirstResponderKey];
    
    // restore the text in the search field
    self.searchController.searchBar.text = [coder decodeObjectForKey:SearchBarTextKey];
}

- (void)changeExercisesView:(UISegmentedControl *)segmentControl{
    UISegmentedControl *currentSegmentControl = segmentControl;

    if (currentSegmentControl.selectedSegmentIndex == _segmentState) {
        return;
    }
    
    switch (currentSegmentControl.selectedSegmentIndex) {
        case 1:
            _items = [Exercise getAerobicsExercises];
            break;
        case 0:
            _items = [Exercise getAnaerobicExercises];
            break;
        default:
            break;
    }
    
    _segmentState = currentSegmentControl.selectedSegmentIndex;
    
    if (_selectorType == SelectorTypeExerciseSingle) {
        _searchResults = [[NSMutableArray alloc] init];
        
        if (_selectTraining) {
            TrainingExercise * replacingTrainingExercise = _selectTraining.trainingExercises[_selectedFitnessIndex];
            Exercise * replacingExercise = [replacingTrainingExercise.exercises firstObject];
            for (Exercise * exerciseItem in _items) {
                if (![replacingExercise.exerciseId isEqualToString:exerciseItem.exerciseId]) {
                    [_searchResults addObject:exerciseItem];
                }
            }
        }
    }
    else {
        _searchResults = [[NSMutableArray alloc] init];
        
        for(Exercise *exercise in _items) {
            [_searchResults addObject:exercise];
        }
    }
    
    _totalItems = [[NSMutableArray alloc] initWithArray:_searchResults];
    
    [_selectorTableView reloadData];
}

@end
