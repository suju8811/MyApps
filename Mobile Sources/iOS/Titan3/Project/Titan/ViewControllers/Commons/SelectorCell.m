//
//  SelectorCell.m
//  Titan
//
//  Created by Manuel Manzanera on 14/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SelectorCell.h"
#import "UtilManager.h"
#import "AppConstants.h"

@implementation SelectorCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        
        [self setBackgroundColor:[UIColor clearColor]];
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:5], [UtilManager width:300], [UtilManager height:30])];
        [_titleLabel setTextColor:YELLOW_APP_COLOR];
        [_titleLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        
        [[self contentView] addSubview:_titleLabel];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, [UtilManager height:39], WIDTH - [UtilManager width:20], 1)];
        [_separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        
        [[self contentView] addSubview:_separatorView];
        
        _checkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:25], 0, [UtilManager width:18], [UtilManager width:18])];
        [_checkImageView setImage:[UIImage imageNamed:@"img-check"]];
        [_checkImageView setCenter:CGPointMake(_checkImageView.center.x, self.contentView.center.y)];
        
        [[self contentView] addSubview:_checkImageView];
    }

    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
