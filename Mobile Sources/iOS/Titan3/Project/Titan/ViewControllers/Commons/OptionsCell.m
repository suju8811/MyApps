//
//  OptionsCell.m
//  Titan
//
//  Created by Manuel Manzanera on 8/6/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OptionsCell.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation OptionsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:25], [UtilManager height:10], [UtilManager width:30], [UtilManager height:30])];
        [_iconImageView setBackgroundColor:[UIColor clearColor]];
        [_iconImageView setContentMode:UIViewContentModeScaleAspectFit];
        [_iconImageView setCenter:CGPointMake(_iconImageView.center.x, 0.04 * HEIGHT)];
        
        [[self contentView] addSubview:_iconImageView];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_iconImageView.frame.origin.x + _iconImageView.frame.size.width + [UtilManager width:15], 0., [UtilManager width:258], 44)];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setTextColor:GRAY_REGISTER_FONT];
        [_titleLabel setCenter:CGPointMake(_titleLabel.center.x, _iconImageView.center.y)];
        [_titleLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:21]];
        [_titleLabel setBackgroundColor:[UIColor clearColor]];
        
        [[self contentView] addSubview:_titleLabel];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:10], 0.08 * HEIGHT - 1, WIDTH  - [UtilManager width:20], 1)];
        [_separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        [_separatorView setAlpha:0.2];
        [[self contentView] addSubview:_separatorView];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:NO];
    
    if (selected) {
        _titleLabel.textColor = YELLOW_APP_COLOR;
        _iconImageView.image = [_iconImageView.image  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        _iconImageView.tintColor = YELLOW_APP_COLOR;
    }
    else {
        _titleLabel.textColor = GRAY_REGISTER_FONT;
        _iconImageView.image = [_iconImageView.image  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        _iconImageView.tintColor = GRAY_REGISTER_FONT;
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    
//    if(highlighted){
//        [_titleLabel setTextColor:YELLOW_APP_COLOR];
//        _iconImageView.image = [_iconImageView.image  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//        [_iconImageView setTintColor:YELLOW_APP_COLOR];
//    }
}


@end
