//
//  SearchTableViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 22/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SearchTableViewController.h"
#import "UtilManager.h"
#import "AppConstants.h"
#import "SelectorCell.h"
#import "Exercise.h"

@interface SearchTableViewController ()

@end

@implementation SearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[self view] setBackgroundColor:BLACK_APP_COLOR];
    
    [self setAutomaticallyAdjustsScrollViewInsets:FALSE];
    [[self tableView] registerClass:[UITableViewCell class] forCellReuseIdentifier:@"searchCell"];
    [[self tableView] registerClass:[SelectorCell class] forCellReuseIdentifier:@"searchCell"];
    [[self tableView] setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [[self tableView] setMultipleTouchEnabled:YES];
    
    [[self tableView] setDelegate:self];
    [[self tableView] setDataSource:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _filteredProducts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectorCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"searchCell"];
    
    Exercise *exercise = [_filteredProducts objectAtIndex:indexPath.row];
    [[cell titleLabel] setText:exercise.title];
    
    return cell;
}

#pragma mark UITableViewCell Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

@end
