//
//  SelectorFoodViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 10/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SelectorFoodViewController.h"
#import "UIImageView+AFNetworking.h"
#import "SearchTableViewController.h"
#import "PlanFoodViewCell.h"
#import "AlimentCell.h"
#import "MealCell.h"
#import "DietCell.h"
#import "SelectTagView.h"
#import "Ingredient.h"
#import "TagUtil.h"

@interface SelectorFoodViewController () <UITableViewDataSource, UITableViewDelegate, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate, SelectTagViewDelegate, SelectorFoodDisplay>

@property (nonatomic, strong) UIView *segmentControlView;
@property (nonatomic, strong) UISegmentedControl *segmentControl;
@property (nonatomic) NSInteger segmentState;
@property (nonatomic, strong) UITableView *selectorTableView;
@property (nonatomic, strong) UITableView *selectedTableView;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) SearchTableViewController *searchTableViewController;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) UILabel *selectExercisesLabel;
@property (nonatomic, strong) NSMutableArray *tags;
@property (nonatomic, strong) UIButton *actionButton;
@property (nonatomic, strong) UIButton *okButton;
@property (nonatomic, strong) NSArray *totalItems;

@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;

@property (nonatomic, strong) SelectorFoodExecutive * executive;

@end

@implementation SelectorFoodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _segmentState = 0;
    
    if (_selectorType == SelectorFoodTypeMealSingle) {
        _items = [Aliment getAliments];
        
        Aliment * replacingAliment;
        if(_selectMeal) {
            Ingredient * selectedIngredient = _selectMeal.ingredients[_selectedFoodIndex];
            replacingAliment = selectedIngredient.aliment;
        }
        
        _searchResults = [[NSMutableArray alloc] initWithCapacity:_items.count];
        _selectResults = [[NSMutableArray alloc] initWithCapacity:0];
        
        for(Aliment *aliment in _items) {
            if (replacingAliment != nil &&
                ![aliment.alimentId isEqualToString:replacingAliment.alimentId]) {
                
                [_searchResults addObject:aliment];
            }
        }
    }
    else if (_selectorType == SelectorFoodTypeMeal) {
        
        _items = [Aliment getAliments];
        
        _searchResults = [[NSMutableArray alloc] initWithCapacity:_items.count];
        _selectResults = [[NSMutableArray alloc] initWithCapacity:0];
        
        for(Aliment *aliment in _items) {
            [_searchResults addObject:aliment];
        }
        
        if(_selectMeal) {
            
            RLMRealm *realm = [AppContext currentRealm];
            
            for(Ingredient *ingredient in _selectMeal.ingredients){
                Aliment *aliment = ingredient.aliment;
                
                [realm transactionWithBlock:^{
                    [aliment setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                }];
                [_selectResults addObject:aliment];
            }
        }
        
    }
    else if (_selectorType == SelectorFoodTypeDietSingle) {
        _searchResults = [[NSMutableArray alloc] init];
        _selectResults = [[NSMutableArray alloc] init];
        
        _items = [Meal getMyMeals];
        
        Meal * replacingMeal = _selectMeal;
        if (_selectDiet) {
            replacingMeal = [_selectDiet.dietMeals[_selectedFoodIndex].meals firstObject];
        }
        
        for (Meal * meal in _items) {
            if (replacingMeal != nil &&
                ![replacingMeal.serverMealId isEqualToString:meal.serverMealId]) {
                
                [_searchResults addObject:meal];
            }
        }
    }
    else if (_selectorType == SelectorFoodTypeDiet) {
        _searchResults = [[NSMutableArray alloc] init];
        _selectResults = [[NSMutableArray alloc] init];
        
        if (_selectDiet) {
            RLMRealm *realm = [AppContext currentRealm];
            
            for (DietMeal * dietMeal in _selectDiet.dietMeals) {
                [realm transactionWithBlock:^{
                    [[dietMeal.meals firstObject] setValue:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
                }];
                [_selectResults addObject:[dietMeal.meals firstObject]];
            }
        }
        
        _items = [Meal getMyMeals];
        
        for(Meal *meal in _items){
            [_searchResults addObject:meal];
        }
    }
    else if (_selectorType == SelectorFoodTypeNutritionPlanSingle) {
        _searchResults = [[NSMutableArray alloc] init];
        _selectResults = [[NSMutableArray alloc] init];
        
        _items = [Diet getMyDiets];
        
        if (_nutritionPlan) {
            NutritionDay * replacingNutritionDay = _nutritionPlan.nutritionDays[_selectedFoodIndex];
            Diet * replacingDiet = replacingNutritionDay.diet;
            for (Diet * diet in _items) {
                if (replacingDiet != nil &&
                    ![replacingDiet.serverDietId isEqualToString:diet.serverDietId]) {
                    
                    [_searchResults addObject:diet];
                }
            }
        }
    }
    else if (_selectorType == SelectorFoodTypeNutritionPlan){
    
        _items = [Diet getMyDiets];
        
        _searchResults = [[NSMutableArray alloc] initWithCapacity:_items.count];
        _selectResults = [[NSMutableArray alloc] initWithCapacity:0];
        
        RLMRealm *realm = [AppContext currentRealm];
        for(Diet *diet in _items){
            [realm transactionWithBlock:^{
                [diet setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
            }];
            [_searchResults addObject:diet];
        }
    }
    else if (_selectorType == SelectorFoodTypeNutritionPlanCalendar) {
        _searchResults = [[NSMutableArray alloc] init];
        _selectResults = [[NSMutableArray alloc] init];
        
        _items = [NutritionPlan getMyNutritionPlans];
        for (NutritionPlan * plan in _items) {
            [_searchResults addObject:plan];
        }
    }
    
    _totalItems = [[NSMutableArray alloc] initWithArray:_searchResults];
    
    [self configureView];
    
    _executive = [[SelectorFoodExecutive alloc] initWithDisplay:self
                                                   selectorType:_selectorType];
}

- (void)configureView {
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _selectorTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.,[UtilManager height:20], WIDTH, HEIGHT - [UtilManager height:40]) style:UITableViewStylePlain];
    [_selectorTableView registerClass:[PlanFoodViewCell class] forCellReuseIdentifier:@"planCell"];
    [_selectorTableView registerClass:[AlimentCell class] forCellReuseIdentifier:@"alimentCell"];
    [_selectorTableView registerClass:[MealCell class] forCellReuseIdentifier:@"foodCell"];
    [_selectorTableView registerClass:[DietCell class] forCellReuseIdentifier:@"dietCell"];
    _selectorTableView.delegate = self;
    _selectorTableView.dataSource = self;
    _selectorTableView.multipleTouchEnabled = YES;
    _selectorTableView.bounces = NO;
    _selectorTableView.backgroundColor = UIColor.clearColor;
    _selectorTableView.allowsMultipleSelection = YES;
    _selectorTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _selectorTableView.tableHeaderView = _searchController.searchBar;
    [self.view addSubview:_selectorTableView];
    
    _selectedTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.f,
                                                                       HEIGHT - TAB_BAR_HEIGHT,
                                                                       WIDTH,
                                                                       HEIGHT/3)
                                                      style:UITableViewStylePlain];
    
    _selectedTableView.delegate = self;
    _selectedTableView.dataSource = self;
    _selectedTableView.multipleTouchEnabled = YES;
    _selectedTableView.bounces = NO;
    _selectedTableView.allowsMultipleSelection = YES;
    _selectedTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_selectedTableView registerClass:[AlimentCell class] forCellReuseIdentifier:@"alimentCell"];
    [_selectedTableView registerClass:[MealCell class] forCellReuseIdentifier:@"foodCell"];
    [_selectorTableView registerClass:[DietCell class] forCellReuseIdentifier:@"dietCell"];
    _selectedTableView.backgroundColor = BLACK_APP_COLOR;
    [self.view addSubview:_selectedTableView];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate = self;
    [self.searchController.searchBar sizeToFit];
    self.searchController.searchBar.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchController.searchBar.barTintColor = BLACK_APP_COLOR;
    self.searchController.searchBar.keyboardAppearance = UIKeyboardAppearanceDark;
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]
     setTintColor:[UIColor whiteColor]];
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]
     setTintColor:[UIColor whiteColor]];
    self.searchController.searchBar.tintColor = UIColor.whiteColor;
    
    for (UIView *subView in self.searchController.searchBar.subviews) {
        for (UIView *secondLevelSubview in subView.subviews) {
            if ([secondLevelSubview isKindOfClass:[UITextField class]]) {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                searchBarTextField.textColor = [UIColor whiteColor];
                break;
            }
        }
    }
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, WIDTH, [UtilManager height:60])];
    headerView.backgroundColor = UIColor.clearColor;
    UIView *searchContainerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:44],
                                                                           [UtilManager height:10],
                                                                           WIDTH - [UtilManager width:88],
                                                                           [UtilManager height:40])];
    
    [searchContainerView addSubview:self.searchController.searchBar];
    searchContainerView.backgroundColor = UIColor.clearColor;
    self.searchController.searchBar.frame = searchContainerView.frame;
    self.searchController.searchBar.center = CGPointMake(searchContainerView.frame.size.width/2,
                                                         searchContainerView.frame.size.height/2);
    
    [headerView addSubview:searchContainerView];
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:40],
                                                                    [UtilManager height:20],
                                                                    [UtilManager width:40] * 0.8f,
                                                                    [UtilManager width:40] * 0.8f)];
    
    [okButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
    okButton.center = CGPointMake(okButton.center.x, searchContainerView.center.y);
    [headerView addSubview:okButton];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                      [UtilManager height:20.f],
                                                                      [UtilManager width:40] * 0.8f,
                                                                      [UtilManager width:40] * 0.8f)];
    
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    backButton.hidden = NO;
    backButton.center = CGPointMake(backButton.center.x, searchContainerView.center.y);
    [headerView addSubview:backButton];
    
    //
    // Add segment control view
    //
    
    CGFloat segmentControlHeight = (_selectorType == SelectorFoodTypeMeal || _selectorType == SelectorFoodTypeMealSingle) ? TAB_BAR_HEIGHT : 0;
    _segmentControlView = [[UIView alloc] initWithFrame:CGRectMake(0.f,
                                                                   CGRectGetMaxY(searchContainerView.frame),
                                                                   WIDTH,
                                                                   segmentControlHeight)];

    _segmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Alimentos", nil),
                                                                  NSLocalizedString(@"Suplementos", nil)]];
    
    _segmentControl.selectedSegmentIndex = _segmentState;
    [_segmentControl addTarget:self action:@selector(changeAlimentsView:) forControlEvents:UIControlEventValueChanged];
    _segmentControl.backgroundColor = BLACK_APP_COLOR;
    _segmentControl.center = CGPointMake(_segmentControlView.center.x,
                                         CGRectGetHeight(_segmentControlView.frame)/2);

    _segmentControl.tintColor = YELLOW_APP_COLOR;
    [_segmentControlView addSubview:_segmentControl];
    if (_selectorType == SelectorFoodTypeMeal) {
        _segmentControlView.hidden = NO;
    }
    else {
        _segmentControlView.hidden = YES;
    }
    
    [headerView addSubview:_segmentControlView];
    
    CGRect headerViewFrame = headerView.frame;
    headerViewFrame.size.height = CGRectGetMaxY(_segmentControlView.frame) + [UtilManager height:10];
    headerView.frame = headerViewFrame;
    
    _selectorTableView.tableHeaderView = headerView;
    
    NSMutableArray *scopeButtonTitles = [[NSMutableArray alloc] init];
    [scopeButtonTitles addObject:NSLocalizedString(@"All", @"Search display controller All button.")];
    
    self.definesPresentationContext = YES;
}

- (void)reloadResults {
    _searchResults = [[NSMutableArray alloc] initWithCapacity:_items.count];
    RLMRealm *realm = [AppContext currentRealm];
    
    if(_selectorType == SelectorFoodTypeMeal){
        for(Aliment *aliment in _items){
            for(Aliment *selectAliment in _selectResults)
            {
                if([selectAliment isEqual:aliment])
                {
                    [realm transactionWithBlock:^{
                        [aliment setValue:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
                    }];
                    break;
                }
            }
            [_searchResults addObject:aliment];
        }
    }
    else if (_selectorType == SelectorFoodTypeDiet){
        for(Meal *meal in _items){
            for(Meal *selectMeal in _selectResults)
            {
                if([selectMeal isEqual:meal])
                {
                    [realm transactionWithBlock:^{
                        [meal setValue:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
                    }];
                    break;
                }
            }
            [_searchResults addObject:meal];
        }
    }
    else if (_selectorType == SelectorFoodTypeNutritionPlan){
        for(Diet *diet in _items){
            for(Diet *selectDiet in _selectResults)
            {
                if([selectDiet isEqual:diet])
                {
                    [realm transactionWithBlock:^{
                        [diet setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                    }];
                    break;
                }
            }
            [_searchResults addObject:diet];
        }
    }
}

- (void)openSelectedView{
    [_selectedTableView reloadData];
    
    [UIView animateWithDuration:0.5 animations:^{
        if (_selectorType == SelectorFoodTypeMeal) {
            if (_selectResults.count > 0 &&
                _selectedTableView.frame.origin.y == HEIGHT - TAB_BAR_HEIGHT) {
                
                _selectedTableView.frame = CGRectMake(0.f,
                                                      HEIGHT -HEIGHT/3,
                                                      WIDTH,
                                                      _selectedTableView.frame.size.height);
                
                [_actionButton setImage:[UIImage imageNamed:@"btn-down"] forState:UIControlStateNormal];
            }else /*if (_selectedTableView.frame.origin.y == HEIGHT - HEIGHT/3)*/{
                [_selectedTableView setFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, _selectedTableView.frame.size.height)];
                [_actionButton setImage:[UIImage imageNamed:@"btn-up"] forState:UIControlStateNormal];
            }
        } else {
            if(_selectResults.count > 0 && _selectedTableView.frame.origin.y == HEIGHT - TAB_BAR_HEIGHT){
                [_selectedTableView setFrame:CGRectMake(0., HEIGHT -HEIGHT/3, WIDTH, _selectedTableView.frame.size.height)];
                [_actionButton setImage:[UIImage imageNamed:@"btn-down"] forState:UIControlStateNormal];
            }else /*if (_selectedTableView.frame.origin.y == HEIGHT - HEIGHT/3)*/{
                [_selectedTableView setFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, _selectedTableView.frame.size.height)];
                [_actionButton setImage:[UIImage imageNamed:@"btn-up"] forState:UIControlStateNormal];
            }
        }
    } completion:^(BOOL finished){
        
    }];
}

- (void)didPressBackButton {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didPressOKButton {
    [_executive didPressOKButton];
}

#pragma mark - SelectTagDelegate

- (void)actionDidPushReturnTags:(NSMutableArray *)selectedTags{
    
    NSMutableArray *auxSearch = [[NSMutableArray alloc] initWithCapacity:_items.count];
    
    if(selectedTags.count > 0){
        for(Tag *tag in selectedTags){
            
            if(_selectorType == SelectorFoodTypeMeal){
                
            }
            _searchResults = [auxSearch mutableCopy];
        }
        
        
        [_selectorTableView reloadData];
    } else {
        [self reloadResults];
        [_selectorTableView reloadData];
    }
    
}


#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [_searchController.searchBar resignFirstResponder];
    
    if(tableView == _selectorTableView){
        if (_selectorType == SelectorFoodTypeMealSingle) {
            Aliment *selectingAliment = [_searchResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [_selectResults removeAllObjects];
            for (Aliment * aliment in _searchResults) {
                [realm transactionWithBlock:^{
                    [aliment setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
                }];
            }
            
            if(selectingAliment.isSelected.boolValue){
                [realm transactionWithBlock:^{
                    [selectingAliment setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
                }];
                [_selectResults removeObject:selectingAliment];
            }
            else {
                [realm transactionWithBlock:^{
                    [selectingAliment setValue:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
                }];
                [_selectResults addObject:selectingAliment];
            }
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Ingredientes seleccionados", nil)]];
        }
        else if (_selectorType == SelectorFoodTypeMeal) {
            Aliment *aliment = [_searchResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            if(aliment.isSelected.boolValue){
                [realm transactionWithBlock:^{
                    [aliment setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
                }];
                [_selectResults removeObject:aliment];
            }else{
                [realm transactionWithBlock:^{
                    [aliment setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                }];
                [_selectResults addObject:aliment];
            }
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Ingredientes seleccionados", nil)]];
        }
        else if (_selectorType == SelectorFoodTypeDietSingle){
            Meal *selectingMeal = [_searchResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [_selectResults removeAllObjects];
            for (Meal * meal in _searchResults) {
                [realm transactionWithBlock:^{
                    [meal setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
                }];
            }
            
            if(selectingMeal.isSelected.boolValue){
                [realm transactionWithBlock:^{
                    [selectingMeal setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
                }];
                [_selectResults removeObject:selectingMeal];
            }
            else {
                [realm transactionWithBlock:^{
                    [selectingMeal setValue:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
                }];
                [_selectResults addObject:selectingMeal];
            }
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Comidas seleccionados", nil)]];
        }
        else if (_selectorType == SelectorFoodTypeDiet){
            
            Meal *meal = [_searchResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            if(meal.isSelected.boolValue){
                [realm transactionWithBlock:^{
                    [meal setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
                }];
                [_selectResults removeObject:meal];
            }else{
                [realm transactionWithBlock:^{
                    [meal setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                }];
                [_selectResults addObject:meal];
            }
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Comidas seleccionados", nil)]];
        }
        else if (_selectorType == SelectorFoodTypeNutritionPlanSingle){
            Diet *selectingDiet = [_searchResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [_selectResults removeAllObjects];
            for (Diet * diet in _searchResults) {
                [realm transactionWithBlock:^{
                    [diet setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
                }];
            }
            
            if(selectingDiet.isSelected.boolValue){
                [realm transactionWithBlock:^{
                    [selectingDiet setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
                }];
                [_selectResults removeObject:selectingDiet];
            }
            else {
                [realm transactionWithBlock:^{
                    [selectingDiet setValue:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
                }];
                [_selectResults addObject:selectingDiet];
            }
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Comidas seleccionados", nil)]];
        }
        else if (_selectorType == SelectorFoodTypeNutritionPlan){
            
            Diet *diet = [_searchResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            if(diet.isSelected.boolValue){
                [realm transactionWithBlock:^{
                    [diet setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
                }];
                [_selectResults removeObject:diet];
            }else{
                [realm transactionWithBlock:^{
                    [diet setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                }];
                [_selectResults addObject:diet];
            }
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Dietas seleccionadas", nil)]];
        }
        else if (_selectorType == SelectorFoodTypeNutritionPlanCalendar) {
            NutritionPlan * selectingNutritionPlan = [_searchResults objectAtIndex:indexPath.row];
            BOOL isSelected = selectingNutritionPlan.isSelected && selectingNutritionPlan.isSelected.boolValue;
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [_selectResults removeAllObjects];
            for (NutritionPlan * nutritionPlan in _searchResults) {
                [realm transactionWithBlock:^{
                    nutritionPlan.isSelected = @NO;
                }];
            }
            
            if (isSelected) {
                [realm transactionWithBlock:^{
                    selectingNutritionPlan.isSelected = @NO;
                }];
                [_selectResults removeObject:selectingNutritionPlan];
            }
            else {
                [realm transactionWithBlock:^{
                    selectingNutritionPlan.isSelected = @YES;
                }];
                [_selectResults addObject:selectingNutritionPlan];
            }
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Nutricion seleccionados", nil)]];
        }
    } else {
    
        if(_selectorType == SelectorFoodTypeMeal){
            Aliment *aliment = [_selectResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            if(aliment.isSelected.boolValue){
                [realm transactionWithBlock:^{
                    [aliment setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
                }];
                [_selectResults removeObject:aliment];
            }else{
                [realm transactionWithBlock:^{
                    [aliment setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                }];
                [_selectResults addObject:aliment];
            }
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Ingredientes seleccionados", nil)]];
        }else if (_selectorType == SelectorFoodTypeDiet){
            
            Meal *meal = [_selectResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            if(meal.isSelected.boolValue){
                [realm transactionWithBlock:^{
                    [meal setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
                }];
                [_selectResults removeObject:meal];
            }else{
                [realm transactionWithBlock:^{
                    [meal setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                }];
                [_selectResults addObject:meal];
            }
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Comidas seleccionados", nil)]];
        }
        else if (_selectorType == SelectorFoodTypeNutritionPlan) {
            
            Diet *diet = [_selectResults objectAtIndex:indexPath.row];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            if(diet.isSelected.boolValue){
                [realm transactionWithBlock:^{
                    [diet setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
                }];
                [_selectResults removeObject:diet];
            }else{
                [realm transactionWithBlock:^{
                    [diet setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                }];
                [_selectResults addObject:diet];
            }
            
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Dietas seleccionadas", nil)]];
        }
    }
    
    [_selectedTableView reloadData];
    [_selectorTableView reloadData];
}

#pragma mark UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == _selectorTableView)
        return _searchResults.count;
    else
        return _selectResults.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    if(_selectorType == SelectorFoodTypeMeal)
        return alimentCellHeigtht;
    else
        return dietCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_selectorType == SelectorFoodTypeMeal) {
        return alimentCellHeigtht;
    }
    else if (_selectorType == SelectorFoodTypeNutritionPlanCalendar) {
        return planFooodCellHeight;
    }
    else {
        return dietCellHeight;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _selectorTableView) {
        if (_selectorType == SelectorFoodTypeMeal ||
            _selectorType == SelectorFoodTypeMealSingle) {
            
            AlimentCell *alimentCell = [_selectorTableView dequeueReusableCellWithIdentifier:@"alimentCell"];
            Aliment *aliment = [_searchResults objectAtIndex:indexPath.row];
            
            [[alimentCell titleLabel] setText:aliment.title];
            
            UIImageView *imageView = alimentCell.foodImageView;
            
            NSURLRequest *foodImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_FOOD,aliment.image]]
                                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                          timeoutInterval:60];
            
            [alimentCell.imageView setImageWithURLRequest:foodImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                [imageView setImage:image];
                [imageView setContentMode:UIViewContentModeScaleAspectFit];
                [[imageView layer] setMasksToBounds:YES];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                NSLog(@"%@",error);
                [imageView setImage:[UIImage imageNamed:@"img-placeholder"]];
            }];
            
            NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
            
            Tag *aliment1Tag = aliment.tag1;
            Tag *alimentTag2 = aliment.tag2;
            
            if(aliment1Tag) {
                [tags addObject:aliment1Tag];
            }
            if(alimentTag2) {
                [tags addObject:alimentTag2];
            }
            
            alimentCell.tagView.tags = tags;
            [alimentCell.tagView reloadTagSubviews];
            
            alimentCell.kCalLabel.text = [NSString stringWithFormat:@"%ld kCals",(long)aliment.calories.intValue];
            alimentCell.proteinCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g", (long)aliment.proteins.intValue];
            alimentCell.carbsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g", (long)aliment.carbs.intValue];
            alimentCell.fatsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g",(long)aliment.fats.intValue];
            alimentCell.weightLabel.text = [NSString stringWithFormat:@"100 %@",aliment.measureUnit];
            

            if (aliment.isSelected.boolValue) {
                alimentCell.checkImageView.hidden = NO;
            }
            else {
                alimentCell.checkImageView.hidden = YES;
            }
            
            return alimentCell;
        }
        else if(_selectorType == SelectorFoodTypeDiet ||
                 _selectorType == SelectorFoodTypeDietSingle){
            
            MealCell *cell = [_selectorTableView dequeueReusableCellWithIdentifier:@"foodCell"];
            
            Meal *meal = [_searchResults objectAtIndex:indexPath.row];
            [[cell titleLabel] setText:meal.title];
            
            NSInteger kCals = 0;
            NSInteger proteins = 0;
            NSInteger carbs= 0;
            NSInteger fats = 0;
            
            for(Ingredient *ingredient in meal.ingredients){
                
                kCals = ingredient.aliment.calories.integerValue + kCals;
                proteins = ingredient.aliment.proteins.integerValue + proteins;
                carbs = ingredient.aliment.carbs.integerValue + carbs;
                fats = ingredient.aliment.fats.integerValue + fats;
            }
            
            NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:meal.tags.count];
            
            for(Tag *tag in meal.tags){
                [tags addObject:tag];
            }
            
            cell.tagView.tags = tags;
            [cell.tagView reloadTagSubviews];
            cell.tagView.hidden = NO;
            
            cell.kCalLabel.text = [NSString stringWithFormat:@"%ld kCals", (long)kCals];
            cell.proteinCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g", (long)proteins];
            cell.carbsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g", (long)carbs];
            cell.fatsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g",(long)fats];
            
            if (meal.isSelected!= nil
                && meal.isSelected.boolValue) {
                
                cell.checkImageView.hidden = NO;
            }
            else {
                cell.checkImageView.hidden = YES;
            }
            cell.infoButton.hidden = YES;
            
            return cell;
        }
        else if (_selectorType == SelectorFoodTypeNutritionPlan ||
                 _selectorType == SelectorFoodTypeNutritionPlanSingle) {
            
            DietCell *cell = [_selectorTableView dequeueReusableCellWithIdentifier:@"dietCell"];
            
            Diet *diet = [_searchResults objectAtIndex:indexPath.row];
            cell.titleLabel.text = diet.title;
            cell.currentIndex = indexPath.row;
            cell.infoButton.hidden = YES;
            
            if(diet.isSelected.boolValue) {
                cell.checkImageView.hidden = NO;
            }
            else {
                cell.checkImageView.hidden = YES;
            }
            
            return cell;
        }
        else if (_selectorType == SelectorFoodTypeNutritionPlanCalendar) {
            
            PlanFoodViewCell * cell = [_selectorTableView dequeueReusableCellWithIdentifier:@"planCell"];
            
            NutritionPlan * plan = [_searchResults objectAtIndex:indexPath.row];
            
            cell.titleLabel.text = plan.title;
            
            cell.contentLabel.text = plan.content;
            if(plan.nutritionDays.count > 0) {
                cell.daysLabel.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)plan.nutritionDays.count, NSLocalizedString(@"días", nil)];
            }
            
            cell.detailInfoButton.hidden = YES;
            cell.menuButton.hidden = YES;
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.exercisesLabel.text =  [NSString stringWithFormat:@"%d %@",(int)plan.nutritionDays.count, NSLocalizedString(@"dietas", nil)];
            [cell setCurrentIndex:indexPath.row];
            
            if(plan.isSelected.boolValue) {
                cell.checkImageView.hidden = NO;
            }
            else {
                cell.checkImageView.hidden = YES;
            }
            
            return cell;
        }
        
    } else{
        if (_selectorType == SelectorFoodTypeMeal ||
           _selectorType == SelectorFoodTypeMealSingle) {
            
            AlimentCell *alimentCell = [_selectorTableView dequeueReusableCellWithIdentifier:@"alimentCell"];
            
            Aliment *aliment = [_selectResults objectAtIndex:indexPath.row];
            alimentCell.titleLabel.text = aliment.title;
            
            UIImageView *imageView = alimentCell.foodImageView;
            NSURLRequest *foodImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_FOOD,aliment.image]]
                                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                          timeoutInterval:60];
            
            [alimentCell.imageView setImageWithURLRequest:foodImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                [imageView setImage:image];
                [imageView setContentMode:UIViewContentModeScaleAspectFit];
                [[imageView layer] setMasksToBounds:YES];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                NSLog(@"%@",error);
                [imageView setImage:[UIImage imageNamed:@"img-placeholder"]];
            }];
            
            NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
            
            Tag *aliment1Tag = aliment.tag1;
            Tag *alimentTag2 = aliment.tag2;
            
            if(aliment1Tag) {
                [tags addObject:aliment1Tag];
            }
            if(alimentTag2) {
                [tags addObject:alimentTag2];
            }
            
            alimentCell.tagView.tags = tags;
            [alimentCell.tagView reloadTagSubviews];
            
            alimentCell.kCalLabel.text = [NSString stringWithFormat:@"%ld kCals", (long)aliment.calories.intValue];
            alimentCell.proteinCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g", (long)aliment.proteins.intValue];
            alimentCell.carbsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g", (long)aliment.carbs.intValue];
            alimentCell.fatsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g", (long)aliment.fats.intValue];
            alimentCell.weightLabel.text = [NSString stringWithFormat:@"100 %@",aliment.measureUnit];
            
            if (aliment.isSelected.boolValue) {
                alimentCell.checkImageView.hidden = NO;
            }
            else {
                alimentCell.checkImageView.hidden = YES;
            }
            
            return alimentCell;
        }
        else if(_selectorType == SelectorFoodTypeDiet ||
                  _selectorType == SelectorFoodTypeDietSingle) {
            MealCell *cell = [_selectorTableView dequeueReusableCellWithIdentifier:@"foodCell"];
            
            Meal *meal = [_selectResults objectAtIndex:indexPath.row];
            [[cell titleLabel] setText:meal.title];
            
            NSInteger kCals = 0;
            NSInteger proteins = 0;
            NSInteger carbs= 0;
            NSInteger fats = 0;
            
            for(Ingredient *ingredient in meal.ingredients){
                
                kCals = ingredient.aliment.calories.integerValue + kCals;
                proteins = ingredient.aliment.proteins.integerValue + proteins;
                carbs = ingredient.aliment.carbs.integerValue + carbs;
                fats = ingredient.aliment.fats.integerValue + fats;
            }
            
            NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:meal.tags.count];
            
            for(Tag *tag in meal.tags){
                [tags addObject:tag];
            }
            
            cell.tagView.tags = tags;
            [cell.tagView reloadTagSubviews];
            cell.tagView.hidden = NO;
            
            cell.kCalLabel.text = [NSString stringWithFormat:@"%ld kCals", (long)kCals];
            cell.proteinCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g",(long)proteins];
            cell.carbsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g",(long)carbs];
            cell.fatsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g",(long)fats];
            
            if(meal.isSelected.boolValue) {
                cell.checkImageView.hidden = NO;
            }
            else {
                cell.checkImageView.hidden = YES;
            }
            cell.infoButton.hidden = YES;
            return cell;
        }
        else if (_selectorType == SelectorFoodTypeNutritionPlan ||
                 _selectorType == SelectorFoodTypeNutritionPlanSingle) {
            
            DietCell *cell = [_selectorTableView dequeueReusableCellWithIdentifier:@"dietCell"];
            
            Diet *diet = [_selectResults objectAtIndex:indexPath.row];
            cell.titleLabel.text = diet.title;
            cell.currentIndex = indexPath.row;
            cell.infoButton.hidden = YES;
            
            if (diet.isSelected.boolValue) {
                cell.checkImageView.hidden = NO;
            }
            else {
                cell.checkImageView.hidden = YES;
            }
            
            return cell;
        }
        else if (_selectorType == SelectorFoodTypeNutritionPlanCalendar) {
            
            PlanFoodViewCell * cell = [_selectorTableView dequeueReusableCellWithIdentifier:@"planCell"];
            
            NutritionPlan * plan = [_selectResults objectAtIndex:indexPath.row];
            
            cell.titleLabel.text = plan.title;
            
            cell.contentLabel.text = plan.content;
            if(plan.nutritionDays.count > 0) {
                cell.daysLabel.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)plan.nutritionDays.count, NSLocalizedString(@"días", nil)];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.detailInfoButton.hidden = YES;
            cell.menuButton.hidden = YES;
            
            cell.exercisesLabel.text =  [NSString stringWithFormat:@"%d %@",(int)plan.nutritionDays.count, NSLocalizedString(@"dietas", nil)];
            [cell setCurrentIndex:indexPath.row];
            
            if (plan.isSelected.boolValue) {
                cell.checkImageView.hidden = NO;
            }
            else {
                cell.checkImageView.hidden = YES;
            }
            
            return cell;
        }
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:50])];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    if(tableView == _selectedTableView){
        [headerView setBackgroundColor:BLACK_APP_COLOR];
        _selectExercisesLabel = [[UILabel alloc] initWithFrame:headerView.bounds];
        [_selectExercisesLabel setTextAlignment:NSTextAlignmentCenter];
        [_selectExercisesLabel setTextColor:YELLOW_APP_COLOR];
        if(_selectorType == SelectorFoodTypeMeal ||
           _selectorType == SelectorFoodTypeMealSingle)
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Ingredientes seleccionados", nil)]];
        else if(_selectorType == SelectorFoodTypeDiet ||
                _selectorType == SelectorFoodTypeDietSingle)
            [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Comidas seleccionadas", nil)]];
        [_selectExercisesLabel setUserInteractionEnabled:FALSE];
        
        [headerView addSubview:_selectExercisesLabel];
        
        //UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openSelectedView)];
        //[_selectExercisesLabel addGestureRecognizer:tapGesture];
        
        if(!_actionButton){
            _actionButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:20.], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
            [_actionButton setImage:[UIImage imageNamed:@"btn-up"] forState:UIControlStateNormal];
        }
        
        [_actionButton addTarget:self action:@selector(openSelectedView) forControlEvents:UIControlEventTouchUpInside];
        [_actionButton setCenter:CGPointMake(_actionButton.center.x, headerView.frame.size.height/2)];
        
        _okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:40], [UtilManager height:20], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
        [_okButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [_okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
        [_okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
        [_okButton setCenter:CGPointMake(_okButton.center.x, headerView.frame.size.height/2)];
        
        [headerView addSubview:_actionButton];
        
        //[headerView addSubview:_okButton];
        
        UITapGestureRecognizer *openGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openSelectedView)];
        [headerView addGestureRecognizer:openGesture];
        
        UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(openSelectedView)];
        [swipeGesture setDirection:UISwipeGestureRecognizerDirectionUp];
        
        [headerView addGestureRecognizer:swipeGesture];
        
        UISwipeGestureRecognizer *swipeDownGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(openSelectedView)];
        [swipeDownGesture setDirection:UISwipeGestureRecognizerDirectionDown];
        
        [headerView addGestureRecognizer:swipeDownGesture];
        
    }
    else {
        _tags = [TagUtil getTagsWithFoodSelectorType:_selectorType];
        
        SelectTagView *selectTagView = [[SelectTagView alloc] initWithFrame:CGRectMake(0., [UtilManager height:10], WIDTH, [UtilManager height:30]) andTags:_tags];
        [selectTagView setDelegate:self];
        [headerView setBackgroundColor:BLACK_APP_COLOR];
        [headerView addSubview:selectTagView];
    }
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:50])];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    return footerView;
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    // update the filtered array based on the search text
    NSString *searchText = searchController.searchBar.text;
    
    // strip out all the leading and trailing spaces
    NSString *strippedString = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // break up the search terms (separated by spaces)
    NSArray *searchItems = nil;
    if (strippedString.length > 0) {
        searchItems = [strippedString componentsSeparatedByString:@" "];
    }
    
    NSPredicate *finalPredicate;
    for (NSString *searchString in searchItems) {
        
        // Below we use NSExpression represent expressions in our predicates.
        // NSPredicate is made up of smaller, atomic parts: two NSExpressions (a left-hand value and a right-hand value)
        
        // name field matching
        //finalPredicate = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@",searchString];
        finalPredicate = [NSPredicate predicateWithFormat:@"title CONTAINS[c] %@",searchString];
        
    }
    
    if(finalPredicate)
        _searchResults = [NSMutableArray arrayWithArray:[_totalItems filteredArrayUsingPredicate:finalPredicate]];
    if(searchText.length == 0)
        [self reloadResults];
    
    //searchResults = [[searchResults filteredArrayUsingPredicate:finalPredicate] mutableCopy];
    // hand over the filtered results to our search results table
    self.definesPresentationContext = TRUE;
    
    [_selectorTableView reloadData];
}


#pragma mark - UISearchBarDelegate

// Workaround for bug: -updateSearchResultsForSearchController: is not called when scope buttons change
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
    [self updateSearchResultsForSearchController:self.searchController];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}


#pragma mark - Content Filtering


#pragma mark

- (void)willPresentSearchController:(UISearchController *)searchController{
    dispatch_async(dispatch_get_main_queue(), ^{
        //searchController.searchResultsController.view.hidden = NO;
        //[[searchController searchBar] setFrame:CGRectMake(0., 0., searchController.searchBar.frame.size.width, searchController.searchBar.frame.size.height)];
    });
}

- (void)didPresentSearchController:(UISearchController *)searchController{
    searchController.searchResultsController.view.hidden = NO;
}

- (void)willDismissSearchController:(UISearchController *)searchController{
    dispatch_async(dispatch_get_main_queue(), ^{
        //searchController.searchResultsController.view.hidden = YES;
        //[self.searchController.searchBar setFrame:CGRectMake(0., NAV_HEIGHT -  self.searchController.searchBar.frame.size.height, WIDTH, self.searchController.searchBar.frame.size.height)];
    });
}

- (void)didDismissSearchController:(UISearchController *)searchController{
    
}

- (void)presentSearchController:(UISearchController *)searchController{
    
}
#pragma mark - UIStateRestoration

// we restore several items for state restoration:
//  1) Search controller's active state,
//  2) search text,
//  3) first responder

NSString *const ViewControllerTitleKeyFood = @"ViewControllerTitleKeyFood";
NSString *const SearchControllerIsActiveKeyFood = @"SearchControllerIsActiveKeyFood";
NSString *const SearchBarTextKeyFood = @"SearchBarTextKeyFood";
NSString *const SearchBarIsFirstResponderKeyFood = @"SearchBarIsFirstResponderKeyFood";

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    [super encodeRestorableStateWithCoder:coder];
    
    // encode the view state so it can be restored later
    
    // encode the title
    [coder encodeObject:self.title forKey:ViewControllerTitleKeyFood];
    
    UISearchController *searchController = self.searchController;
    
    // encode the search controller's active state
    BOOL searchDisplayControllerIsActive = searchController.isActive;
    [coder encodeBool:searchDisplayControllerIsActive forKey:SearchControllerIsActiveKeyFood];
    
    // encode the first responser status
    if (searchDisplayControllerIsActive) {
        [coder encodeBool:[searchController.searchBar isFirstResponder] forKey:SearchBarIsFirstResponderKeyFood];
    }
    
    // encode the search bar text
    [coder encodeObject:searchController.searchBar.text forKey:SearchBarTextKeyFood];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    [super decodeRestorableStateWithCoder:coder];
    
    // restore the title
    self.title = [coder decodeObjectForKey:ViewControllerTitleKeyFood];
    
    // restore the active state:
    // we can't make the searchController active here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerWasActive = [coder decodeBoolForKey:SearchControllerIsActiveKeyFood];
    
    // restore the first responder status:
    // we can't make the searchController first responder here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerSearchFieldWasFirstResponder = [coder decodeBoolForKey:SearchBarIsFirstResponderKeyFood];
    
    // restore the text in the search field
    self.searchController.searchBar.text = [coder decodeObjectForKey:SearchBarTextKeyFood];
}

#pragma mark - SelectorFoodDisplay Delegate

- (void)dismissWithConfirm {
    if (_selectorType == SelectorFoodTypeNutritionPlanCalendar) {
        NutritionPlan * plan = [_selectResults firstObject];
        
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            plan.isSelected = @NO;
        }];
        
        [self dismissViewControllerAnimated:YES completion:^{
            [_delegate didPressNutritionPlanForCalendar:plan];
        }];
        return;
    }
    
    if (_selectorType == SelectorFoodTypeDietSingle &&
        _selectMeal) {
        Meal * mergingMeal = [_selectResults firstObject];
        [_delegate didPressOKForMergeFood:mergingMeal];
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    
    if (_selectorType == SelectorFoodTypeMealSingle ||
        _selectorType == SelectorFoodTypeDietSingle ||
        _selectorType == SelectorFoodTypeNutritionPlanSingle) {
        self.presentDetailForReplacingFood = YES;
    }
    
    self.presentMealDetail = YES;
    self.presentNutritionPlanDetail = YES;
    self.presentDietDetail = YES;
    [self dismissViewControllerAnimated:YES completion:^{
        self.presentDetailForReplacingFood = NO;
        self.presentMealDetail = NO;
        self.presentDietDetail = NO;
    }];
}

- (NSArray*)selectedItems {
    return _selectResults;
}

- (void)changeAlimentsView:(UISegmentedControl *)segmentControl {
    UISegmentedControl *currentSegmentControl = segmentControl;
    if (_segmentState == currentSegmentControl.selectedSegmentIndex) {
        return;
    }
    
    _segmentState = currentSegmentControl.selectedSegmentIndex;
    switch (_segmentState) {
        case 0:
            _items = [Aliment getAliments];
            break;
        case 1:
            _items = [Aliment getSupplements];
            break;
            
        default:
            break;
    }
    
    if (_selectorType == SelectorFoodTypeMealSingle) {
        Aliment * replacingAliment;
        if (_selectMeal) {
            Ingredient * selectedIngredient = _selectMeal.ingredients[_selectedFoodIndex];
            replacingAliment = selectedIngredient.aliment;
        }
        
        _searchResults = [[NSMutableArray alloc] init];
        
        for(Aliment *aliment in _items) {
            if (replacingAliment != nil &&
                ![aliment.alimentId isEqualToString:replacingAliment.alimentId]) {
                
                [_searchResults addObject:aliment];
            }
        }
    }
    else if (_selectorType == SelectorFoodTypeMeal) {
        _searchResults = [[NSMutableArray alloc] init];
        
        for(Aliment *aliment in _items) {
            [_searchResults addObject:aliment];
        }
    }
    
    _totalItems = [[NSMutableArray alloc] initWithArray:_searchResults];
    
    [_selectorTableView reloadData];
}

@end
