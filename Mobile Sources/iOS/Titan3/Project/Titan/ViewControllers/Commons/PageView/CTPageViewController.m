//
//  CTPageViewController.m
//  CtxNet
//
//  Created by Shi Li on 9/6/16.
//  Copyright © 2016 ngv-group. All rights reserved.
//

#import "CTPageViewController.h"

@interface CTPageViewController () <UIPageViewControllerDataSource>

@end

@implementation CTPageViewController

- (id)init {
    
    self = [super init];
    _childViewControllers = [[NSArray<UIViewController*> alloc] init];
    return self;
}

- (void)setChildViewControllers:(NSArray<UIViewController *> *)childViewControllers {

    _childViewControllers = childViewControllers;
    
    for (NSInteger i = 0; i < childViewControllers.count; i++) {
        
        [self addChildViewController:childViewControllers[i]];
        childViewControllers[i].view.tag = i;
    }
    
    UIViewController *initialViewController = childViewControllers[0];
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    [self setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.dataSource = self;
}

- (NSUInteger)getCurrentPage {
    
    UIViewController * curViewController = [self.viewControllers objectAtIndex:0];
    NSUInteger index = curViewController.view.tag;
    return index;
}

- (void)gotoPage:(NSInteger)index animated:(BOOL)animated {
    
    NSUInteger curPage = [self getCurrentPage];
    UIPageViewControllerNavigationDirection animationDirection = curPage > index ? UIPageViewControllerNavigationDirectionReverse : UIPageViewControllerNavigationDirectionForward;
    [self gotoPage:index direction:animationDirection animated:animated];
}

- (void)gotoPage:(NSInteger)index direction:(UIPageViewControllerNavigationDirection)transitionDirection animated:(BOOL)animated {
    
    __weak CTPageViewController* pvcw = self;
    UIViewController * page = _childViewControllers[index];
    [self setViewControllers:@[page]
                   direction:transitionDirection
                    animated:animated completion:^(BOOL finished) {
                        
                        UIPageViewController* pvcs = pvcw;
                        if (!pvcs) return;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [pvcs setViewControllers:@[page]
                                          direction:transitionDirection
                                           animated:animated completion:nil];
                        });
                    }];
}

#pragma mark -
#pragma mark - UIPageViewController Datasource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [self getCurrentPage];
    
    if (index == 0) {
        return nil;
    }
    
    index--;
    
    return [_childViewControllers objectAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [self getCurrentPage];
    
    index++;
    
    if (index == _childViewControllers.count) {
        return nil;
    }
    
    return [_childViewControllers objectAtIndex:index];
}

@end
