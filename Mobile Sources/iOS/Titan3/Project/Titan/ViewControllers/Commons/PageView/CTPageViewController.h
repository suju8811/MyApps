//
//  CTPageViewController.h
//  CtxNet
//
//  Created by Shi Li on 9/6/16.
//  Copyright © 2016 ngv-group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTPageViewController : UIPageViewController

@property (nonatomic, strong) NSArray<UIViewController*>* childViewControllers;
- (NSUInteger)getCurrentPage;
- (void)gotoPage:(NSInteger)index direction:(UIPageViewControllerNavigationDirection)transitionDirection animated:(BOOL)animated;
- (void)gotoPage:(NSInteger)index animated:(BOOL)animated;

@end
