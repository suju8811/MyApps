//
//  PictureViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 7/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "PictureViewController.h"
#import "UIImageView+AFNetworking.h"

@interface PictureViewController ()

@end

@implementation PictureViewController

- (id)initWithImageId:(NSString *)image{
    
    self = [super init];
    
    if(self){
        _imageId = image;
    }
    
    return self;

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [_pictureVisorViewcontroller  scrollViewDidScroll:_pictureVisorViewcontroller.picScrollView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    
    //UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backToGallery)];
    //[tapGesture setNumberOfTapsRequired:1];
    //[self.view addGestureRecognizer:tapGesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
   
    _picImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    
    __weak UIImageView *progressHomeImageView = _picImageView;
    
    __weak PictureVisorViewController *viewcontroller = _pictureVisorViewcontroller;
    
    NSURLRequest *homeImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,_imageId]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
    
    [_picImageView setImageWithURLRequest:homeImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [progressHomeImageView setImage:image];
        [[progressHomeImageView layer] setMasksToBounds:YES];
        [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
        
        [viewcontroller scrollViewDidScroll:viewcontroller.picScrollView];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        
        [progressHomeImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        [[progressHomeImageView layer] setMasksToBounds:YES];
        [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    }];
    
    [self.view addSubview:_picImageView];
}

@end
