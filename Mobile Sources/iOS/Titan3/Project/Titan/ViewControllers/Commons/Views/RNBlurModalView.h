

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

extern NSString * const kRNBlurDidShowNotification;
extern NSString * const kRNBlurDidHidewNotification;

@protocol RNBlurModalViewDelegate<NSObject>

- (void)didPressOKButton;

@end

@interface RNBlurModalView : UIView

@property (assign, readonly) BOOL isVisible;

@property (assign) CGFloat animationDuration;
@property (assign) CGFloat animationDelay;
@property (assign) UIViewAnimationOptions animationOptions;
@property (nonatomic, strong) id<RNBlurModalViewDelegate> delegate;

- (id)initWithViewController:(UIViewController*)viewController view:(UIView*)view;
- (id)initTableViewController:(UIViewController*)viewController view:(UIView*)view;
- (id)initWithViewController:(UIViewController*)viewController title:(NSString*)title message:(NSString*)message;
- (id)initWithParentView:(UIView*)parentView view:(UIView*)view;
- (id)initWithParentView:(UIView*)parentView title:(NSString*)title message:(NSString*)message;
- (id)initWithView:(UIView*)view;
- (id)initWithTitle:(NSString*)title message:(NSString*)message;
- (id)initWithViewControllerAnimationLoading:(UIViewController *)viewController;

- (void)show;
- (void)showWithDuration:(CGFloat)duration delay:(NSTimeInterval)delay options:(UIViewAnimationOptions)options completion:(void (^)(void))completion;

- (void)didPressOKButton;
- (void)hide;
- (void)hideWithDuration:(CGFloat)duration delay:(NSTimeInterval)delay options:(UIViewAnimationOptions)options completion:(void (^)(void))completion;

@end
