//
//  SplashViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 25/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SplashViewController.h"
#import "InitViewController.h"
#import "AppDelegate.h"
#import "RegisterNavController.h"
#import "MenuHomeViewController.h"
#import "UserDefaultLibrary.h"
#import "WebServiceManager.h"
#import "MRProgress.h"
#import "SplashExecutive.h"

@interface SplashViewController ()<SplashDisplay>

@property SplashExecutive * executive;

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    
    _executive = [[SplashExecutive alloc] initWithDisplay:self];
    [_executive displayDidLoad];
}

- (void)configureView{
    
    UIImageView *splashImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [splashImageView setImage:[UIImage imageNamed:@"splash"]];
    
    [self.view addSubview:splashImageView];
    
    [_executive displayDidLoad];
}

#pragma mark - Splash Display Delegate

- (void)presentLoginViewController{
    
    InitViewController *loginViewController = [[InitViewController alloc] init];
    RegisterNavController *signNavController = [[RegisterNavController alloc] initWithRootViewController:loginViewController];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:signNavController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:signNavController];
     }];
}


- (void)presentHomeViewController{
    
    MenuHomeViewController *homeViewController = [[MenuHomeViewController alloc] init];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:homeViewController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:homeViewController];
     }];
}

@end
