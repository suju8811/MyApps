//
//  SelectorFoodViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 10/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import <Realm/Realm.h>
#import "Meal.h"
#import "Diet.h"
#import "Aliment.h"
#import "NutritionPlan.h"
#import "SelectorFoodExecutive.h"

@protocol SelectorFoodViewDelegate;

@interface SelectorFoodViewController : ParentViewController

@property (nonatomic, assign) id<SelectorFoodViewDelegate>delegate;
@property (nonatomic, assign) BOOL isOnlyOneSelector;
@property (nonatomic, strong) RLMResults *items;
@property (nonatomic, strong) Meal *selectMeal;
@property (nonatomic, strong) Diet *selectDiet;
@property (nonatomic, strong) NutritionPlan *nutritionPlan;
@property (nonatomic, assign) SelectorFoodType selectorType;
@property (nonatomic, strong) NSMutableArray *selectResults;

@property (nonatomic, assign) NSInteger selectedFoodIndex;
@property (nonatomic, assign) BOOL presentMealDetail;
@property (nonatomic, assign) BOOL presentDetailForReplacingFood;
@property (nonatomic, assign) BOOL presentDietDetail;
@property (nonatomic, assign) BOOL presentNutritionPlanDetail;

@end

@protocol SelectorFoodViewDelegate <NSObject>

@optional
- (void)returnData:(NSMutableArray *)returnData withType:(SelectorFoodType)selectorType;
- (void)didPressOKForMergeFood:(RLMObject*)rlmObject;
- (void)didPressNutritionPlanForCalendar:(NutritionPlan*)nutritionPlan;

@end
