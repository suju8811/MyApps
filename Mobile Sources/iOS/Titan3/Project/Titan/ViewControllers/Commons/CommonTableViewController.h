//
//  CommonTableViewController.h
//  Titan
//
//  Created by Marcus Lee on 25/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonTableViewController : UITableViewController

//
// Navigation Bar
//

@property (nonatomic, assign) BOOL isHideCustomNavigation;

//
// BaseDisplay Delegate Methods
//

- (void)showLoading:(NSString*)title;
- (void)hideLoading;
- (void)showMessage:(NSString*)message;
- (void)showMessage:(NSString *)message alertAction:(UIAlertAction*)alertAction;

@end
