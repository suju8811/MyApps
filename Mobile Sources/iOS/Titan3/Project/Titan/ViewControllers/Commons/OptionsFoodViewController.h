//
//  OptionsFoodViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 10/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

#import "Meal.h"
#import "Diet.h"
#import "NutritionPlan.h"
#import "NutritionDay.h"

typedef enum {
    OptionFoodTypeAliment,
    OptionFoodTypeMeal,
    OptionFoodTypeDiet,
    OptionFoodTypeNutritionPlan
} OptionFoodType;

@class OptionsFoodViewController;

@protocol OptionsFoodViewControllerDelegate <NSObject>

- (void)editDiet:(Diet *)diet;
- (void)editNutritionPlan:(NutritionPlan *)nutritionPlan;

@optional
- (void)didDeletedMeal:(Meal*)meal;
- (void)didPressEditFood:(RLMObject*)rlmObject;
- (void)didPressDuplicateFood:(RLMObject*)rlmObject;
- (void)didPressFavoriteFood:(RLMObject*)rlmObject;
- (void)didPressDeleteFood:(RLMObject*)rlmObject;
- (void)didPressReplaceDiet:(RLMObject*)rlmObject;
- (void)didPressReplicateFood:(RLMObject*)rlmObject count:(NSInteger)count;
- (void)didPressReplicateTodo;
- (void)didPressFusionFood:(RLMObject*)rlmObject;
- (void)didPressSendFood:(RLMObject*)rlmObject;
- (void)didPressShareFood:(RLMObject*)rlmObject;
- (void)didPressAddToCalendar:(RLMObject*)rlmObject;
- (void)optionsFoodViewController:(OptionsFoodViewController*)optionsFoodViewController didPressTimePicker:(RLMObject*)rlmObject;

@end

@interface OptionsFoodViewController : ParentViewController

@property (nonatomic, strong) Aliment *currentAliment;
@property (nonatomic, strong) Meal *currentMeal;
@property (strong, nonatomic) DietMeal * currentDietMeal;
@property (nonatomic, strong) Diet *currentDiet;
@property (nonatomic, strong) NutritionPlan *currentNutritionPlan;
@property (nonatomic, assign) id<OptionsFoodViewControllerDelegate>delegate;
@property (nonatomic, assign) OptionFoodType optionFoodType;
@property (nonatomic, assign) BOOL fromDetailOfParent;
@property (nonatomic, assign) NSInteger fromCalendar;

@end
