//
//  PictureVisorViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 7/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "PictureVisorViewController.h"
#import "PictureViewController.h"
#import "UIImageView+AFNetworking.h"

@interface PictureVisorViewController ()

@end

@implementation PictureVisorViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (int i=0; i<_picImages.count ; i++)
    {
        [controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
    
    [self configureView];
    
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:FALSE block:^(NSTimer *timer){
    
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    ContainerViewController *containerViewController = (ContainerViewController *)self.navigationController;
//    
//    [UIView animateWithDuration:1. animations:^{
//        [[containerViewController slideButton] setAlpha:1.];
//        [[containerViewController backButton] setAlpha:1.];
//    }];
    //[[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    ContainerViewController *containerViewController = (ContainerViewController *)self.navigationController;
//    
//    [UIView animateWithDuration:1. animations:^{
//        [[containerViewController slideButton] setAlpha:0.];
//        [[containerViewController backButton] setAlpha:0.];
//    }];
    
    //[[UIApplication sharedApplication]setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
}

- (void)configureView
{
    UIView *blackBackground = [[UIView alloc] initWithFrame:self.view.frame];
    [blackBackground setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:blackBackground];
    
    _picScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, HEIGHT)];
    
    [_picScrollView setContentSize:CGSizeMake(WIDTH * _picImages.count, HEIGHT)];
    
    [_picScrollView setPagingEnabled:YES];
    [_picScrollView setShowsHorizontalScrollIndicator:NO];
    [_picScrollView setShowsVerticalScrollIndicator:NO];
    [_picScrollView setScrollsToTop:YES];
    [_picScrollView setDelegate:self];
    [_picScrollView setBackgroundColor:[UIColor clearColor]];
    
    [self loadScrollViewWithPage:_picNumber-1];
    [self loadScrollViewWithPage:_picNumber+1];
    [self loadScrollViewWithPage:_picNumber];
   
    [_picScrollView setContentOffset:CGPointMake(0,0)];
    [self.view addSubview:_picScrollView];
}

//-------------------------------SCROLL VIEW---------------------------------//

- (void)loadScrollViewWithPage:(int)page
{
    if (page < 0)
        return;
    if (page >= [_picImages count])
        return;
    
    PictureViewController * pictureViewController = [_viewControllers objectAtIndex:page];
    if ((NSNull *)pictureViewController == [NSNull null])
    {
        NSLog(@"%@",[_picImages objectAtIndex:page]);
        pictureViewController = [[PictureViewController alloc]initWithImageId:[_picImages objectAtIndex:page]];
        [pictureViewController setPage:page];
        [pictureViewController setPictureVisorViewcontroller:self];
        //[[pictureViewController picImageView] setImage:[_picImages objectAtIndex:page]];
        [_viewControllers replaceObjectAtIndex:page withObject:pictureViewController];
    }
    if (!pictureViewController.view.superview)
    {
        CGRect frame = _picScrollView.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        
        frame.origin.x = WIDTH * page;
        
        pictureViewController.view.frame = frame;
        [_picScrollView addSubview:pictureViewController.view];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    CGFloat pageWidth = _picScrollView.frame.size.width;
    int page = floor((_picScrollView.contentOffset.x - pageWidth / 2) / pageWidth)+1;
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page + 1];
    [self loadScrollViewWithPage:page];
}

@end
