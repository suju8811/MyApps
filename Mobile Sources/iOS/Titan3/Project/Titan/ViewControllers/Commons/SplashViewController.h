//
//  SplashViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 25/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

@interface SplashViewController : ParentViewController

@end
