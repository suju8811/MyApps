//
//  OptionsFoodViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 10/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OptionsFoodViewController.h"
#import "WebServiceManager.h"
#import "MRProgress.h"
#import "OptionsCell.h"
#import "OptionFoodExecutive.h"
#import "FoodViewController.h"
#import "AlertCustomView.h"

@interface OptionsFoodViewController () <UITableViewDelegate, UITableViewDataSource, OptionFoodDisplay, AlertCustomViewDelegate>

@property (nonatomic, strong) UITableView *optionsTableView;
@property (nonatomic, strong) Meal *duplicateMeal;
@property (nonatomic, strong) Diet *duplicateDiet;
@property (nonatomic, strong) NutritionPlan *duplicateNutritionPlan;
@property (nonatomic, strong) OptionFoodExecutive *executive;
@property (nonatomic, strong) AlertCustomView *alertCustomView;

@end

@implementation OptionsFoodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    
    _executive = [[OptionFoodExecutive alloc] initWithDisplay:self];
}

- (void)configureView{
    
    [[self view] setBackgroundColor:BLACK_APP_COLOR];
    //[self.view addSubview:[self headerView]];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _optionsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, HEIGHT) style:UITableViewStylePlain];
    
    [_optionsTableView setDelegate:self];
    [_optionsTableView setDataSource:self];
    [_optionsTableView registerClass:[OptionsCell class] forCellReuseIdentifier:@"optionCell"];
    [_optionsTableView setBackgroundColor:[UIColor clearColor]];
    [_optionsTableView setTableHeaderView:[self headerView]];
    [_optionsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.view addSubview:_optionsTableView];
    
    [self footerView];
}

- (UIView *)headerView{
    UIView *headerView  = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:100])];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:20], WIDTH - [UtilManager width:30], [UtilManager height:60])];
    [titleLabel setText:@"Sample"];
    
    
    switch (_optionFoodType) {
        case OptionFoodTypeMeal:
            titleLabel.text = _currentMeal.title;
            break;
        case OptionFoodTypeDiet:
            titleLabel.text = _currentDiet.title;
            break;
        case OptionFoodTypeNutritionPlan:
            titleLabel.text = _currentNutritionPlan.title;
            break;
        default:
            titleLabel.text = NSLocalizedString(@"Añadir opción", nil);
            break;
    }
    
    [titleLabel setTextColor:YELLOW_APP_COLOR];
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (void)footerView{
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    [closeButton setTitle:NSLocalizedString(@"Cerrar", nil) forState:UIControlStateNormal];
    [[closeButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:19]];
    [closeButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:closeButton];
}

- (void)dismissView{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView setUserInteractionEnabled:FALSE];
    
    if (_fromCalendar == 1) {
        switch (indexPath.row) {
            case 0: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressSendFood:_currentNutritionPlan];
                }];
            }
                break;
            case 1: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressShareFood:_currentNutritionPlan];
                }];
            }
                break;
            default:
                break;
        }
    }
    else if (_optionFoodType == OptionFoodTypeAliment) {
        switch (indexPath.row) {
            case 0: { // edit
                [self dismissViewControllerAnimated:YES completion:^{
                    if ([_delegate respondsToSelector:@selector(didPressEditFood:)]) {
                        [_delegate didPressEditFood:_currentAliment];
                    }
                }];
            }
                break;
            case 1: // delete
                if ([_delegate respondsToSelector:@selector(didPressDeleteFood:)]) {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressDeleteFood:_currentAliment];
                    }];
                }
                else {
                    [_executive deleteMeal:_currentMeal];
                }
                break;
            case 2: {
                if (_fromDetailOfParent) {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressReplaceDiet:_currentMeal];
                    }];
                }
                else {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressSendFood:_currentAliment];
                    }];
                }
            }
                break;
            case 3:
                if (_fromDetailOfParent) {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressSendFood:_currentAliment];
                    }];
                }
                else {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressShareFood:_currentAliment];
                    }];
                }
            case 4: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressShareFood:_currentAliment];
                }];
            }
                break;
            default:
                break;
        }
    }
    else if (_optionFoodType == OptionFoodTypeMeal) {
        switch (indexPath.row) {
            case 0: { // edit
                [self dismissViewControllerAnimated:YES completion:^{
                    if ([_delegate respondsToSelector:@selector(didPressEditFood:)]) {
                        [_delegate didPressEditFood:_currentMeal];
                    }
                }];
            }
                break;
            case 1: // duplicate
                _duplicateMeal = [Meal duplicateMeal:_currentMeal];
                [_executive duplicateMeal:_duplicateMeal];
                break;
            case 2:
                [_executive favoriteMeal:_currentMeal];
                break;
            case 3:
                if ([_delegate respondsToSelector:@selector(didPressDeleteFood:)]) {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressDeleteFood:_currentMeal];
                    }];
                }
                else {
                    [_executive deleteMeal:_currentMeal];
                }
                break;
            case 4: {
                if (_fromDetailOfParent) {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressReplaceDiet:_currentMeal];
                    }];
                }
                else {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressFusionFood:_currentMeal];
                    }];
                }
            }
                break;
            case 5:{
                if (_fromDetailOfParent) {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressFusionFood:_currentMeal];
                    }];
                }
                else {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressSendFood:_currentMeal];
                    }];
                }
            }
                break;
            case 6:{
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressShareFood:_currentMeal];
                }];
            }
                break;
            case 7: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate optionsFoodViewController:self didPressTimePicker:_currentDietMeal];
                }];
            }
                break;
            default:
                break;
        }
    }
    else if (_optionFoodType == OptionFoodTypeDiet){
        switch (indexPath.row) {
            case 0: {
                if ([_delegate respondsToSelector:@selector(didPressEditFood:)]) {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressEditFood:_currentDiet];
                    }];
                }
                else {
                    [self dismissView];
                    [_delegate editDiet:_currentDiet];
                }
            }
                break;
            case 1:
                _duplicateDiet = [Diet duplicateDiet:_currentDiet];
                [_executive duplicateDiet:_duplicateDiet];
                break;
            case 2:
                [_executive favoriteDiet:_currentDiet];
                break;
            case 3: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressDeleteFood:_currentDiet];
                }];
            }
                break;
            case 4: {
                if (_fromDetailOfParent) {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressReplaceDiet:_currentDiet];
                    }];
                }
                else {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressSendFood:_currentDiet];
                    }];
                }
            }
                break;
            case 5:
                if (_fromDetailOfParent) {
                    _alertCustomView = [[AlertCustomView alloc] initWithFrameAndTextField:CGRectMake(0., 0., WIDTH, HEIGHT)];
                    [[_alertCustomView titleLabel] setText:NSLocalizedString(@"Número de Réplicas", nil)];
                    [_alertCustomView setTag:3];
                    [[_alertCustomView textField] setText:@"1"];
                    _alertCustomView.delegate = self;
                    
                    [_alertCustomView setCenter:self.view.center];
                    
                    [self.view addSubview:_alertCustomView];
                }
                else {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressShareFood:_currentDiet];
                    }];
                }
                break;
            case 6: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressReplicateTodo];
                }];
            }
                break;
            default:
                break;
        }
    }
    else if (_optionFoodType == OptionFoodTypeNutritionPlan) {
        switch (indexPath.row) {
            case 0:
                [self dismissView];
                if ([_delegate respondsToSelector:@selector(didPressEditFood:)]) {
                    [_delegate didPressEditFood:_currentNutritionPlan];
                }
                break;
            case 1:
            {
                _duplicateNutritionPlan = [NutritionPlan duplicateNutritionPlan:_currentNutritionPlan];
                [_executive duplicateNutritionPlan:_duplicateNutritionPlan];
            }
                break;
            case 2:
                [_executive favoriteNutritionPlan:_currentNutritionPlan];
                break;
            case 3:
                [_executive deleteNutritionPlan:_currentNutritionPlan];
                break;
            case 4: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressSendFood:_currentNutritionPlan];
                }];
            }
                break;
            case 5: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressShareFood:_currentNutritionPlan];
                }];
            }
                break;
            case 6: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressAddToCalendar:_currentNutritionPlan];
                }];
            }
                break;
            default:
                break;
        }
    }
}

#pragma mark UITableView Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.08 * HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.08 * HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_fromCalendar == 1) {
        return 2;
    }
    
    if (_optionFoodType == OptionFoodTypeNutritionPlan) {
        return 7;
    }
    
    if (_optionFoodType == OptionFoodTypeMeal) {
        if (_fromDetailOfParent) {
            return 8;
        }
        
        return 7;
    }
    
    if(_optionFoodType == OptionFoodTypeDiet &&
       _fromDetailOfParent) {
        
        return 7;
    }
    
    if (_optionFoodType == OptionFoodTypeAliment) {
        if (_fromDetailOfParent) {
            return 5;
        }
        
        return 4;
    }
    
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OptionsCell *cell = [_optionsTableView dequeueReusableCellWithIdentifier:@"optionCell"];
    
    if (_fromCalendar == 1) {
        
        switch (indexPath.row) {
            case 0:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                cell.titleLabel.text = NSLocalizedString(@"Enviar", nil);
                break;
            case 1:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                cell.titleLabel.text = NSLocalizedString(@"Compartir", nil);
                break;
            default:
                break;
        }
    }
    else if (_optionFoodType == OptionFoodTypeNutritionPlan) {
        switch (indexPath.row) {
            case 0: // edit
                cell.iconImageView.image = [UIImage imageNamed:@"icon-edit"];
                cell.titleLabel.text = NSLocalizedString(@"Editar", nil);
                break;
            case 1: // duplicate
                cell.iconImageView.image = [UIImage imageNamed:@"icon-duplicate"];
                cell.titleLabel.text = NSLocalizedString(@"Duplicar", nil);
                break;
            case 2: // favorite
                cell.iconImageView.image = [UIImage imageNamed:@"icon-fav-off"];
                if (_currentNutritionPlan.isFavourite.boolValue) {
                    cell.titleLabel.text = NSLocalizedString(@"Deshacer Favorito", nil);
                }
                else {
                    cell.titleLabel.text = NSLocalizedString(@"Añadir a Favorito", nil);
                }
                break;
            case 3: // delete
                cell.iconImageView.image = [UIImage imageNamed:@"icon-delete"];
                cell.titleLabel.text = NSLocalizedString(@"Eliminar", nil);
                break;
            case 4:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                cell.titleLabel.text = @"Enviar";
                break;
            case 5:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                cell.titleLabel.text = @"Compartir";
                break;
            case 6:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-calendar"];
                cell.titleLabel.text = NSLocalizedString(@"Añadir a Calendario", nil);
                break;
            default:
                break;
        }
    }
    else if (_optionFoodType == OptionFoodTypeDiet) {
        
        switch (indexPath.row) {
            case 0: // edit
                cell.iconImageView.image = [UIImage imageNamed:@"icon-edit"];
                cell.titleLabel.text = NSLocalizedString(@"Editar", nil);
                break;
            case 1: // duplicate
                cell.iconImageView.image = [UIImage imageNamed:@"icon-duplicate"];
                cell.titleLabel.text = NSLocalizedString(@"Duplicar", nil);
                break;
            case 2: // favorite
                cell.iconImageView.image = [UIImage imageNamed:@"icon-fav-off"];
                
                if (_optionFoodType == OptionFoodTypeMeal) {
                    if (_currentMeal.isFavourite.boolValue) {
                        cell.titleLabel.text = NSLocalizedString(@"Deshacer Favorito", nil);
                    }
                    else {
                        cell.titleLabel.text = NSLocalizedString(@"Añadir a Favorito", nil);
                    }
                }
                else if (_optionFoodType == OptionFoodTypeDiet) {
                    if (_currentDiet.isFavourite.boolValue) {
                        cell.titleLabel.text = NSLocalizedString(@"Deshacer Favorito", nil);
                    }
                    else {
                        cell.titleLabel.text = NSLocalizedString(@"Añadir a Favorito", nil);
                    }
                }
                else if (_optionFoodType == OptionFoodTypeNutritionPlan) {
                    if (_currentNutritionPlan.isFavourite.boolValue) {
                        cell.titleLabel.text = NSLocalizedString(@"Deshacer Favorito", nil);
                    }
                    else {
                        cell.titleLabel.text = NSLocalizedString(@"Añadir a Favorito", nil);
                    }
                }
                break;
            case 3: // delete
                cell.iconImageView.image = [UIImage imageNamed:@"icon-delete"];
                cell.titleLabel.text = NSLocalizedString(@"Eliminar", nil);
                break;
            case 4:
                if (_fromDetailOfParent) {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon_replace_gray"];
                    if (_optionFoodType == OptionFoodTypeDiet) {
                        cell.titleLabel.text = @"Sustituir dieta";
                    }
                    else {
                        cell.titleLabel.text = @"Sustituir comida";
                    }
                }
                else {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                    cell.titleLabel.text = @"Enviar";
                }
                break;
            case 5:
                if (_fromDetailOfParent) {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon-duplicate"];
                    cell.titleLabel.text = NSLocalizedString(@"Replicar", nil);
                }
                else {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                    cell.titleLabel.text = @"Compartir";
                }
                break;
            case 6:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-duplicate"];
                cell.titleLabel.text = NSLocalizedString(@"Replicar todo", nil);
                break;
            default:
                break;
        }
    }
    else if (_optionFoodType == OptionFoodTypeMeal) {
        
        switch (indexPath.row) {
            case 0:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-edit"];
                cell.titleLabel.text = NSLocalizedString(@"Editar", nil);
                break;
            case 1:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-duplicate"];
                cell.titleLabel.text = NSLocalizedString(@"Duplicar", nil);
                break;
            case 2:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-fav-off"];
                if (_currentMeal.isFavourite.boolValue) {
                    cell.titleLabel.text = NSLocalizedString(@"Deshacer Favorito", nil);
                }
                else {
                    cell.titleLabel.text = NSLocalizedString(@"Añadir a Favorito", nil);
                }
                break;
            case 3:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-delete"];
                cell.titleLabel.text = NSLocalizedString(@"Eliminar", nil);
                break;
            case 4:
                if (_fromDetailOfParent) {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon_replace_gray"];
                    if (_optionFoodType == OptionFoodTypeDiet) {
                        cell.titleLabel.text = @"Sustituir dieta";
                    }
                    else {
                        cell.titleLabel.text = @"Sustituir comida";
                    }
                }
                else {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon_replace_gray"];
                    if (_currentMeal.mergedMeals &&
                        _currentMeal.mergedMeals.count > 0) {
                        
                        cell.titleLabel.text = NSLocalizedString(@"Defusionar", nil);
                    }
                    else {
                        cell.titleLabel.text = NSLocalizedString(@"Fusionar", nil);
                    }
                }
                break;
            case 5:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                cell.titleLabel.text = @"Enviar";
                break;
            case 6:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                cell.titleLabel.text = @"Compartir";
                break;
            case 7:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-calendar"];
                cell.titleLabel.text = NSLocalizedString(@"Añadir hora", nil);
            default:
                break;
        }
    }
    else if (_optionFoodType == OptionFoodTypeAliment) {
        switch (indexPath.row) {
            case 0:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-edit"];
                cell.titleLabel.text = NSLocalizedString(@"Editar", nil);
                break;
            case 1:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-delete"];
                cell.titleLabel.text = NSLocalizedString(@"Eliminar", nil);
                break;
            case 2:
                if (_fromDetailOfParent) {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon_replace_gray"];
                    cell.titleLabel.text = NSLocalizedString(@"Sustituir alimentos", nil);
                }
                else {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                    cell.titleLabel.text = NSLocalizedString(@"Enviar", nil);
                }
                break;
            case 3:
                if (_fromDetailOfParent) {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                    cell.titleLabel.text = NSLocalizedString(@"Enviar", nil);
                }
                else {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                    cell.titleLabel.text = NSLocalizedString(@"Compartir", nil);
                }
                break;
            case 4:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                cell.titleLabel.text = NSLocalizedString(@"Compartir", nil);
                break;
            default:
                break;
        }
    }
    
    return cell;
}

#pragma mark - OptionFoodDisplay Delegate

- (void)didDeletedAliment: (Aliment*)aliment {
    [WebServiceManager getAlimentsWithCompletionBlock:^(BOOL isSuccess) {
        [self dismissView];
    }];
}

- (void)didDuplicateMeal:(Meal*)meal {
    [WebServiceManager getMealsWithCompletionBlock:^(BOOL isSuccess) {
        [self dismissDisplay];
        if ([_delegate respondsToSelector:@selector(didPressDuplicateFood:)]) {
            [_delegate didPressDuplicateFood:meal];
        }
    }];
}

- (void)didFavoriteMeal:(Meal*)meal {
    [WebServiceManager getMealsWithCompletionBlock:^(BOOL isSuccess) {
        if ([_delegate respondsToSelector:@selector(didPressFavoriteFood:)]) {
            [_delegate didPressFavoriteFood:meal];
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (void)didDeletedMeal:(Meal*)meal {
    [self dismissViewControllerAnimated:YES completion:^{
        [_delegate didDeletedMeal:meal];
    }];
}

- (void)didDuplicatedDiet:(Diet*)diet {
    [WebServiceManager getDietsWithWithCompletionBlock:^(BOOL isSuccess) {
        [self dismissDisplay];
        if ([_delegate respondsToSelector:@selector(didPressDuplicateFood:)]) {
            [_delegate didPressDuplicateFood:diet];
        }
    }];
}

- (void)didDeletedDiet:(Diet*)diet {
    [self dismissViewControllerAnimated:YES completion:^{
//        [_delegate editDiet:diet];
    }];
}

- (void)didFavoriteDiet:(Diet*)diet {
    [WebServiceManager getDietsWithWithCompletionBlock:^(BOOL isSuccess) {
        [self dismissDisplay];
        if ([_delegate respondsToSelector:@selector(didPressFavoriteFood:)]) {
            [_delegate didPressFavoriteFood:diet];
        }
    }];
}

- (void)didUpdateNutritionPlan:(NutritionPlan*)plan {
}

- (void)didDuplicateNutritionPlan:(NutritionPlan*)plan {
    [WebServiceManager getNutrionalPlansWithCompletionBlock:^(BOOL isSuccess) {
        [self dismissDisplay];
        if ([_delegate respondsToSelector:@selector(didPressDuplicateFood:)]) {
            [_delegate didPressDuplicateFood:plan];
        }
    }];
}

- (void)didFavoriteNutritionPlan:(NutritionPlan*)plan {
    [WebServiceManager getNutrionalPlansWithCompletionBlock:^(BOOL isSuccess) {
        [self dismissDisplay];
        if ([_delegate respondsToSelector:@selector(didPressFavoriteFood:)]) {
            [_delegate didPressFavoriteFood:plan];
        }
    }];
}

- (void)didDeletedNutritionPlan:(NutritionPlan*)plan {
    if ([_delegate respondsToSelector:@selector(didPressDeleteFood:)]) {
        [_delegate didPressDeleteFood:plan];
    }
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)dismissDisplay {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - AlertCustomViewDelegate Methods

- (void)cancelAlertViewDidselected {
    [_alertCustomView removeFromSuperview];
}

- (void)confirmAlertViewDidSelected {
    
    if(_alertCustomView.textField.text) {
        NSInteger replies = _alertCustomView.textField.text.intValue;
        [_alertCustomView removeFromSuperview];
        [self dismissViewControllerAnimated:YES completion:^{
            [_delegate didPressReplicateFood:_currentDiet count:replies];
        }];
    }
    else {
        [_alertCustomView removeFromSuperview];
    }
}

- (void)dismissAlertViewSelected {
    [_alertCustomView removeFromSuperview];
}

@end
