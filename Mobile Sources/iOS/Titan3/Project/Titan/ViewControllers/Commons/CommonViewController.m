//
//  CommonViewController.m
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CommonViewController.h"
#import "MRProgress.h"
#import "UtilManager.h"
#import "RateElementListViewController.h"

#define RATE_ELEMENT_LIST_VIEW_SEGUE_ID     @"RateElementListStoryboardID"

@interface CommonViewController ()

@end

@implementation CommonViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - BaseDisplay Delegate Functions

- (void)showLoading:(NSString*)title {
    NSString * localizedTitle = @"";
    if (title != nil) {
        localizedTitle = NSLocalizedString(title, nil);
    }
    
    [MRProgressOverlayView showOverlayAddedTo:self.view
                                        title:localizedTitle
                                         mode:MRProgressOverlayViewModeCheckmark
                                     animated:YES];
}

- (void)hideLoading {
    [MRProgressOverlayView dismissOverlayForView:self.view animated:NO];
}

- (void)showMessage:(NSString*)message {
    [self presentViewController:[UtilManager presentAlertWithMessage:@""
                                                            andTitle:NSLocalizedString(message, nil)]
                       animated:YES
                     completion:nil];
}

- (void)showMessage:(NSString *)message alertAction:(UIAlertAction*)alertAction {
    [self presentViewController:[UtilManager presentAlertWithMessage:@"" andTitle:NSLocalizedString(message, nil) alertAction:alertAction]
                       animated:YES
                     completion:nil];
}

#pragma mark - Present common view

- (void)presentUserRateView:(BaseElement*)element {
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Common" bundle:nil];
    RateElementListViewController * rateElementListViewController = [storyboard instantiateViewControllerWithIdentifier:RATE_ELEMENT_LIST_VIEW_SEGUE_ID];
    rateElementListViewController.isHideCustomNavigation = YES;
    rateElementListViewController.element = element;
    [self.navigationController pushViewController:rateElementListViewController animated:YES];
}

@end
