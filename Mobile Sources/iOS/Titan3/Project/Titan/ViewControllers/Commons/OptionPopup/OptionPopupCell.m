//
//  OptionPopupCell.m
//  Titan
//
//  Created by Marcus Lee on 15/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OptionPopupCell.h"

@implementation OptionPopupCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
