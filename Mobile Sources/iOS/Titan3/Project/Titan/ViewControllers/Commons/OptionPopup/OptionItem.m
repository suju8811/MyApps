//
//  OptionItem.m
//  Titan
//
//  Created by Marcus Lee on 15/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OptionItem.h"

@implementation OptionItem

- (id)initWithTitle:(NSString*)title
              image:(UIImage*)image {

    self = [super init];
    
    _title = title;
    _image = image;
    
    return self;
}

@end
