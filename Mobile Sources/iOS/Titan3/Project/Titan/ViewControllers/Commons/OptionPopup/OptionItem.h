//
//  OptionItem.h
//  Titan
//
//  Created by Marcus Lee on 15/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OptionItem : NSObject

@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) UIImage * image;

- (id)initWithTitle:(NSString*)title
              image:(UIImage*)image;

@end
