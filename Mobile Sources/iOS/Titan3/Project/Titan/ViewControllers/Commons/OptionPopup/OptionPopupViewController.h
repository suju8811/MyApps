//
//  OptionPopupViewController.h
//  Titan
//
//  Created by Marcus Lee on 15/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ParentViewController.h"
#import "OptionItem.h"

@class OptionPopupViewController;

@protocol OptionPopupViewDelegate

- (void)optionPopupViewController:(OptionPopupViewController*)optionPopupViewController
              didPressItemAtIndex:(NSInteger)index
                       withObject:(RLMObject*)object;

@end

@interface OptionPopupViewController : ParentViewController

//
// Outlets
//

@property (weak, nonatomic) IBOutlet UITableView *tableView;

//
// Variables
//

@property (nonatomic, strong) NSArray<OptionItem*>* optionItems;
@property (nonatomic, strong) id<OptionPopupViewDelegate> delegate;
@property (nonatomic, strong) NSString * optionTitle;
@property (nonatomic, strong) RLMObject * rlmObject;

@end
