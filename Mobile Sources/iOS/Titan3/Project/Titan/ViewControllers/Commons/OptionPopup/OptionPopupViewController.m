//
//  OptionPopupViewController.m
//  Titan
//
//  Created by Marcus Lee on 15/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OptionPopupViewController.h"
#import "OptionPopupCell.h"

@interface OptionPopupViewController ()

@end

@implementation OptionPopupViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView.tableFooterView = [[UIView alloc] init];
    _tableView.tableHeaderView = [self tableHeaderView];
}

#pragma mark - Actions

- (IBAction)didPressCancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDatasource and UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 0.08 * HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 0.08 * HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _optionItems.count;
}

- (UIView*)tableHeaderView {
    UIView *headerView
    = [[UIView alloc] initWithFrame:CGRectMake(0.,
                                               0.,
                                               WIDTH,
                                               [UtilManager height:100])];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:20], WIDTH - [UtilManager width:30], [UtilManager height:60])];
    titleLabel.text = _optionTitle;
    titleLabel.textColor = YELLOW_APP_COLOR;
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OptionPopupCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"optionCell"];
    OptionItem * optionItem = _optionItems[indexPath.row];
    
    cell.imageView.image = optionItem.image;
    cell.titleLabel.text = optionItem.title;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self dismissViewControllerAnimated:YES completion:^{
        [_delegate optionPopupViewController:self didPressItemAtIndex:indexPath.row withObject:_rlmObject];
    }];
}

@end
