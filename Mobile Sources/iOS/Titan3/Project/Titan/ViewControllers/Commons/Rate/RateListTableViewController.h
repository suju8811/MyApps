//
//  RateListTableViewController.h
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseElement.h"

@interface RateListTableViewController : UITableViewController

@property (nonatomic, strong) RLMArray<RateInfo*>* rateInfos;

@end
