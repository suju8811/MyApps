//
//  RateElementListViewController.h
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CommonViewController.h"
#import "RateListTableViewController.h"

@interface RateElementListViewController : CommonViewController

//
// Outlets
//

@property (nonatomic, strong) RateListTableViewController * rateListTableViewController;

//
// Variables
//

@property (nonatomic, strong) BaseElement * element;


@end
