//
//  RateListCell.m
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RateListCell.h"

@implementation RateListCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setContent:(RateInfo *)rateInfo {
    _userNameLabel.text = rateInfo.userName;
    _note.text = rateInfo.opinion;
    _rateView.value = [rateInfo.rate floatValue];
}

@end
