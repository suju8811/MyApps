//
//  RateListTableViewController.m
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RateListTableViewController.h"
#import "RateListCell.h"

#define RATE_LIST_CELL_REUSE_IDENTIFIER         @"RateListCell"

@interface RateListTableViewController ()

@end

@implementation RateListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 144.f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _rateInfos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RateListCell *cell = [tableView dequeueReusableCellWithIdentifier:RATE_LIST_CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
    RateInfo * rateInfo = _rateInfos[indexPath.row];
    [cell setContent:rateInfo];
    return cell;
}

#pragma mark - Reload Values

- (void)setRateInfos:(RLMArray<RateInfo*>*)rateInfos {
    _rateInfos = rateInfos;
    [self.tableView reloadData];
}

@end
