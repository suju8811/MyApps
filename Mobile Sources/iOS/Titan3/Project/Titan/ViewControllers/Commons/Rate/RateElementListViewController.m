//
//  RateElementListViewController.m
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RateElementListViewController.h"
#import "RateElementViewController.h"
#import "RateElementListExecutive.h"
#import "AppContext.h"

#define RATING_LIST_TABLE_EMBEDDED_SEGUE    @"RateListTableEmbeddedSegue"
#define ADD_USER_RATE_SEGUE_ID              @"AddUserRateSegueID"

@interface RateElementListViewController () <RateElementListDisplay>

@property (nonatomic, strong) RateElementListExecutive * executive;

@end

@implementation RateElementListViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    _executive = [[RateElementListExecutive alloc] initWithDisplay:self
                                                           element:_element];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [_executive reloadList];
}

#pragma mark - Segue Preparation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:RATING_LIST_TABLE_EMBEDDED_SEGUE]) {
        _rateListTableViewController = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:ADD_USER_RATE_SEGUE_ID]) {
        ((RateElementViewController*)segue.destinationViewController).isHideCustomNavigation = YES;
        ((RateElementViewController*)segue.destinationViewController).element = _element;
    }
}

#pragma mark - Actions

- (IBAction)didPressAddButton:(id)sender {
    NSString * owner = [_element valueForKey:@"owner"];
    if ([owner isEqualToString:[AppContext sharedInstance].currentUser.name]) {
        [self showMessage:@"No puedes calificar tu propio elemento"];
        return;
    }
    
    [self performSegueWithIdentifier:ADD_USER_RATE_SEGUE_ID sender:nil];
}

- (IBAction)didPressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - RateElementListDisplay

- (void)loadRateInfos:(RLMArray<RateInfo*> *)rateInfos {
    _rateListTableViewController.rateInfos = rateInfos;
}

@end
