//
//  RateElementViewController.m
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RateElementViewController.h"
#import "RateElementExecutive.h"
#import "AppConstants.h"
#import "UtilManager.h"

@interface RateElementViewController () <RateElementDisplay>

@property (nonatomic, strong) RateElementExecutive * executive;

@end

@implementation RateElementViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _executive = [[RateElementExecutive alloc] initWithDisplay:self];
    [_executive displayDidLoad];
}

#pragma mark - RateElementDisplay

- (void)loadDisplay {
    _noteTextView.keyboardAppearance = UIKeyboardAppearanceDark;
    _noteTextView.layer.borderWidth = 1;
    _noteTextView.layer.borderColor = BLACK_HEADER_APP_COLOR.CGColor;
    _noteTextView.text = NSLocalizedString(@"Notas", nil);
    
    UIToolbar *toolbarWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetTextView)];
    [barButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarWeigth setItems:[NSArray arrayWithObjects:flexibleWeigthSpace, barButtonItem, nil]];
    _noteTextView.inputAccessoryView = toolbarWeigth;
}

- (RateElementDisplayContext*)displayContext {
    return [[RateElementDisplayContext alloc] initWithRate:_starRatingView.value
                                                      note:_noteTextView.text
                                                   element:_element];
    
}

#pragma mark - Actions

- (IBAction)didPressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didPressOKButton:(id)sender {
    [_executive didPressOKButton];
}

- (void)didRateElement {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Delegate For Text Field and Text View

- (void)resetTextView {
    [_noteTextView resignFirstResponder];
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
    
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    if([[textView text] isEqualToString:NSLocalizedString(@"Notas", nil)]) {
        [_noteTextView setText:@""];
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

@end
