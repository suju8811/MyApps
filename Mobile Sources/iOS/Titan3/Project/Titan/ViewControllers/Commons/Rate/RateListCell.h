//
//  RateListCell.h
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HCSStarRatingView.h>
#import "RateInfo.h"

@interface RateListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *rateView;
@property (weak, nonatomic) IBOutlet UILabel *note;

- (void)setContent:(RateInfo*)rateInfo;

@end
