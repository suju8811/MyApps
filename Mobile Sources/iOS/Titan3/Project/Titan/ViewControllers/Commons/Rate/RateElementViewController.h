//
//  RateElementViewController.h
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CommonViewController.h"
#import <HCSStarRatingView.h>
#import "BaseElement.h"

@interface RateElementViewController : CommonViewController

//
// Variables
//

@property (nonatomic, strong) BaseElement * element;

//
// Outlets
//

@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatingView;
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;

@end
