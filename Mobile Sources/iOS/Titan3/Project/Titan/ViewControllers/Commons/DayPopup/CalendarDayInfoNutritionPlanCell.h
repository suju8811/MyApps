//
//  CalendarDayInfoNutritionPlanCell.h
//  Titan
//
//  Created by Marcus Lee on 26/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CalendarDayInfoCell.h"

@interface CalendarDayInfoNutritionPlanCell : CalendarDayInfoCell

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *kCalLabel;

@end
