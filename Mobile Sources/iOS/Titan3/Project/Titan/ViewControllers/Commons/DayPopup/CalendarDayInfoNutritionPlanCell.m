//
//  CalendarDayInfoNutritionPlanCell.m
//  Titan
//
//  Created by Marcus Lee on 26/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CalendarDayInfoNutritionPlanCell.h"
#import "SchedulePlan.h"

@implementation CalendarDayInfoNutritionPlanCell

- (void)setContent:(id)calendarInfo selectedDate:(NSDate *)selectedDate dataTye:(CalendarInfoType)calendarInfoType delegate:(id<CalendarDayInfoCellDelegate>)delegate {
    
    [super setContent:calendarInfo selectedDate:selectedDate dataTye:calendarInfoType delegate:delegate];
    
    SchedulePlan * schedulePlan = (SchedulePlan*)calendarInfo;
    NutritionPlan * nutritionPlan = schedulePlan.nutritionPlan;
    _titleLabel.text = nutritionPlan.title;

    float kCal = 0;
    for (NutritionDay * nutritionDay in nutritionPlan.nutritionDays) {
        kCal = kCal + [Diet getKCalInDiet:nutritionDay.diet];
    }
    
    _kCalLabel.text = [NSString stringWithFormat:@"%d kCal", (int)kCal];
}

@end
