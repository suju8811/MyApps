//
//  CalendarDayInfoNoDataCell.m
//  Titan
//
//  Created by Marcus Lee on 26/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CalendarDayInfoNoDataCell.h"
#import "SchedulePlan.h"

@implementation CalendarDayInfoNoDataCell

- (void)setContent:(id)calendarInfo selectedDate:(NSDate *)selectedDate dataTye:(CalendarInfoType)calendarInfoType delegate:(id<CalendarDayInfoCellDelegate>)delegate {
    
    [super setContent:calendarInfo
         selectedDate:selectedDate
              dataTye:calendarInfoType
             delegate:delegate];
    
    switch (self.calendarInfoType) {
        case CalendarInfoTypeTrainingPlan:
            _descriptionLabel.text = @"No hay plan de entrenamiento";
            break;
        case CalendarInfoTypeNutritionPlan:
            _descriptionLabel.text = @"No hay un plan de nutrición";
            break;
        case CalendarInfoTypeEvent:
            _descriptionLabel.text = @"No hay evento";
            break;
        case CalendarInfoTypeRepose:
            _descriptionLabel.text = @"No hay reposo";
            break;
        default:
            break;
    }
}

- (IBAction)didPressAddButton:(id)sender {
    [self.delegate didPressAddInfo:self.calendarInfoType];
}

@end
