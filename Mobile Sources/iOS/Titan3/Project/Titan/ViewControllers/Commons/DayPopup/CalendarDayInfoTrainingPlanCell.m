//
//  CalendarDayInfoTrainingPlanCell.m
//  Titan
//
//  Created by Marcus Lee on 26/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CalendarDayInfoTrainingPlanCell.h"
#import "SchedulePlan.h"
#import "DateTimeUtil.h"

@implementation CalendarDayInfoTrainingPlanCell

- (void)setContent:(id)calendarInfo selectedDate:(NSDate *)selectedDate dataTye:(CalendarInfoType)calendarInfoType delegate:(id<CalendarDayInfoCellDelegate>)delegate {
    
    [super setContent:calendarInfo
         selectedDate:selectedDate
              dataTye:calendarInfoType
             delegate:delegate];
    
    SchedulePlan * schedulePlan = (SchedulePlan*)calendarInfo;
    
    NSInteger dayOffset = [DateTimeUtil daysBetween:schedulePlan.scheduleDate and:selectedDate];
    Training * training = [schedulePlan.plan.trainingDay[dayOffset].trainingDayContent firstObject].training;
    
    _titleLabel.text = training.title;
    
    int secs = 0;
    NSInteger kCal = 0;
    
    for(TrainingExercise *trainingExercise in training.trainingExercises){
        secs = [TrainingExercise getExerciseTime:trainingExercise] + secs;
        
        for(Serie *serie in trainingExercise.series){
            kCal = kCal + [Serie kCalInSerie:serie];
        }
    }
    
    _kCalLabel.text = [NSString stringWithFormat:@"%ld kCal",kCal];
    
    int minutes = secs / 60;
    
    if(minutes<60) {
        _durationLabel.text = [NSString stringWithFormat:@"%d min",minutes];
    }
    else {
        int hour = minutes/60;
        minutes = minutes/60;
        _durationLabel.text = [NSString stringWithFormat:@"%d h %d min",hour,minutes];
    }
}

@end
