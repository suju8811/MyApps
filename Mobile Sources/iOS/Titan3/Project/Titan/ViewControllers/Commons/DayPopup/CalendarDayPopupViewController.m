//
//  CalendarDayPopupViewController.m
//  Titan
//
//  Created by Marcus Lee on 25/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CalendarDayPopupViewController.h"
#import "CalendarDayInfoTableViewController.h"
#import "CalendarDayInfoExecutive.h"
#import "DateTimeUtil.h"
#import "TrainingPlanDetailViewController.h"
#import "WorkoutDetailViewController.h"
#import "NutritionPlanDetailViewController.h"
#import "EventViewController.h"
#import "ReposeViewController.h"
#import "SelectorFoodViewController.h"
#import "SelectorTableViewController.h"

#define EVENT_VIEW_STORYBOARD_ID                        @"EventViewStoryboardID"
#define CALENDAR_DAY_INFO_TABLE_EMBEDDED_SEGUE  @"CalendarDayInfoTableEmbedded"

@interface CalendarDayPopupViewController () <CalendarDayInfoDisplay, CalendarDayInfoTableViewControllerDelegate, SelectorTableViewDelegate, SelectorFoodViewDelegate>

@property (nonatomic, strong) CalendarDayInfoTableViewController * calendarDayInfoTableViewController;
@property (nonatomic, strong) CalendarDayInfoExecutive * executive;

@end

@implementation CalendarDayPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;

    //
    // Add Tab Gesture Recognizer
    //
    
    UITapGestureRecognizer * tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapTouchView:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [_touchView addGestureRecognizer:tapGestureRecognizer];
    
    //
    // Add swipe down gesture recognizer
    //
    
    UISwipeGestureRecognizer * swipeDownGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeDown:)];
    swipeDownGestureRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [_swipeDownView addGestureRecognizer:swipeDownGestureRecognizer];
    
    //
    // Load executive
    //
    
    _executive = [[CalendarDayInfoExecutive alloc] initWithDisplay:self];
    [_executive displayDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_executive reloadCalendarData:_selectedDate];
    
    _touchView.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    _touchView.hidden = YES;
}

#pragma mark - Segue Prepartion

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:CALENDAR_DAY_INFO_TABLE_EMBEDDED_SEGUE]) {
        _calendarDayInfoTableViewController = segue.destinationViewController;
        _calendarDayInfoTableViewController.delegate = self;
        _calendarDayInfoTableViewController.selectedDate = _selectedDate;
    }
}

#pragma mark - Actions

- (void)didSwipeDown:(UISwipeGestureRecognizer*)swipeGestureRecognizer {
    if (swipeGestureRecognizer.direction == UISwipeGestureRecognizerDirectionDown) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)didTapTouchView:(UITapGestureRecognizer*)tapGestureRecognizer {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - CalendarDayInfoTableViewControllerDelegate

- (void)didPressAddCalendarInfo:(CalendarInfoType)calendarInfoType {
    [_executive didPressAddCalendarInfo:calendarInfoType];
}

- (void)didPressCalendarInfo:(id)info {
    [_executive didPressCalendarInfo:info selectedDate:_selectedDate];
}

#pragma mark - CalendarDayInfoDisplay

- (void)loadDisplay {
    
    //
    // Show date
    //
    
    _dayLabel.text = [DateTimeUtil spanishStringFromDate:_selectedDate format:@"EEEE - dd MMM yyyy"];
}

- (void)reloadCalendarData:(NSDictionary *)calendarData {
    _calendarData = calendarData;
    _calendarDayInfoTableViewController.calendarData = _calendarData;
}

- (void)presentTrainingPlanSelectorViewController {
    SelectorTableViewController * selectorTableViewController = [[SelectorTableViewController alloc] init];
    selectorTableViewController.delegate = self;
    selectorTableViewController.isOnlyOneSelector = YES;
    selectorTableViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    selectorTableViewController.selectorType = SelectorTypeTrainingPlan;
    [self presentViewController:selectorTableViewController animated:YES completion:nil];
}

- (void)presentNutritionPlanSelectorViewController {
    SelectorFoodViewController * selectorFoodViewController = [[SelectorFoodViewController alloc] init];
    selectorFoodViewController.delegate = self;
    selectorFoodViewController.isOnlyOneSelector = YES;
    selectorFoodViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    selectorFoodViewController.selectorType = SelectorFoodTypeNutritionPlanCalendar;
    [self presentViewController:selectorFoodViewController animated:YES completion:nil];
}

- (void)presentTrainingDetailViewController:(Training*)training ofPlan:(TrainingPlan*)trainingPlan {
    WorkoutDetailViewController *trainingDetailViewController = [[WorkoutDetailViewController alloc] init];
    trainingDetailViewController.currentTraining = training;
    trainingDetailViewController.currentTrainingPlan = trainingPlan;
    trainingDetailViewController.fromCalendar = YES;
    [self.navigationController pushViewController:trainingDetailViewController animated:YES];
}

- (void)presentTrainingPlanDetailViewController:(SchedulePlan*)schedulePlan {
    TrainingPlanDetailViewController *trainingDetailViewController = [[TrainingPlanDetailViewController alloc] init];
    trainingDetailViewController.trainingPlan = schedulePlan.plan;
    trainingDetailViewController.fromCalendar = 1;
    trainingDetailViewController.schedulePlan = schedulePlan;
    [self.navigationController pushViewController:trainingDetailViewController animated:YES];
}

- (void)presentNutritionPlanDetailViewController:(SchedulePlan*)schedulePlan {
    NutritionPlanDetailViewController * nutritionPlanDetailViewController = [[NutritionPlanDetailViewController alloc] init];
    nutritionPlanDetailViewController.currentNutritionPlan = schedulePlan.nutritionPlan;
    nutritionPlanDetailViewController.fromCalendar = 1;
    nutritionPlanDetailViewController.schedulePlan = schedulePlan;
    nutritionPlanDetailViewController.selectedDate = _selectedDate;
    [self.navigationController pushViewController:nutritionPlanDetailViewController animated:YES];
}

- (void)presentEventViewController:(Event *)event {
    UIStoryboard * scheduleStoryboard = [UIStoryboard storyboardWithName:@"Schedule" bundle:nil];
    EventViewController * eventViewController = [scheduleStoryboard instantiateViewControllerWithIdentifier:EVENT_VIEW_STORYBOARD_ID];
    eventViewController.selectedDate = _selectedDate;
    eventViewController.event = event;
    [self.navigationController pushViewController:eventViewController animated:YES];
}

- (void)presentReposeViewController:(TrainingPlan*)repose {
    UIStoryboard * scheduleStoryboard = [UIStoryboard storyboardWithName:@"Schedule" bundle:nil];
    ReposeViewController * reposeViewController = [scheduleStoryboard instantiateInitialViewController];
    reposeViewController.selectedDate = _selectedDate;
    reposeViewController.repose = repose;
    [self.navigationController pushViewController:reposeViewController animated:YES];
}

#pragma mark - Selector Table View Delegate

- (void)didSelectTrainingPlan:(TrainingPlan*)trainingPlan {
    [_executive saveTrainingPlan:trainingPlan
                          toDate:_selectedDate];
    
}

- (void)didPressNutritionPlanForCalendar:(NutritionPlan*)nutritionPlan {
    [_executive saveNutritionPlan:nutritionPlan
                           toDate:_selectedDate];
    
}

@end
