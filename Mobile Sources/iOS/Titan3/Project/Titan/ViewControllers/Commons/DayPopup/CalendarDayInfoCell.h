//
//  CalendarDayInfoCell.h
//  Titan
//
//  Created by Marcus Lee on 27/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"

@protocol CalendarDayInfoCellDelegate

- (void)didPressAddInfo:(CalendarInfoType)calendarInfoType;

@end

@interface CalendarDayInfoCell : UITableViewCell

@property (nonatomic, strong) id<CalendarDayInfoCellDelegate> delegate;
@property (nonatomic, strong) NSDate * selectedDate;
@property (nonatomic, strong) id calendarInfo;
@property (nonatomic, assign) CalendarInfoType calendarInfoType;

- (void)setContent:(id)calendarInfo
      selectedDate:(NSDate*)selectedDate
           dataTye:(CalendarInfoType)calendarInfoType
          delegate:(id<CalendarDayInfoCellDelegate>)delegate;

@end
