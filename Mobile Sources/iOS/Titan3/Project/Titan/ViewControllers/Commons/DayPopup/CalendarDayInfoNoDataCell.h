//
//  CalendarDayInfoNoDataCell.h
//  Titan
//
//  Created by Marcus Lee on 26/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"
#import "CalendarDayInfoCell.h"

@interface CalendarDayInfoNoDataCell : CalendarDayInfoCell

//
// Outlets
//

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
