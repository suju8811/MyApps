//
//  CalendarDayInfoCell.m
//  Titan
//
//  Created by Marcus Lee on 27/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CalendarDayInfoCell.h"

@implementation CalendarDayInfoCell

- (void)setContent:(id)calendarInfo
      selectedDate:(NSDate*)selectedDate
           dataTye:(CalendarInfoType)calendarInfoType
          delegate:(id<CalendarDayInfoCellDelegate>)delegate {
    
    _calendarInfo = calendarInfo;
    _selectedDate = selectedDate;
    _calendarInfoType = calendarInfoType;
    _delegate = delegate;
}

@end
