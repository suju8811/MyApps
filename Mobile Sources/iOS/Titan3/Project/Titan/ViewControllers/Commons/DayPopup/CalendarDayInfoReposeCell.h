//
//  CalendarDayInfoReposeCell.h
//  Titan
//
//  Created by Marcus Lee on 27/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CalendarDayInfoCell.h"

@interface CalendarDayInfoReposeCell : CalendarDayInfoCell

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *noteLabel;

@end
