//
//  CalendarDayInfoTableViewController.m
//  Titan
//
//  Created by Marcus Lee on 26/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CalendarDayInfoTableViewController.h"
#import "TitanStyle.h"
#import "CalendarDayInfoNutritionPlanCell.h"
#import "CalendarDayInfoTrainingPlanCell.h"
#import "CalendarDayInfoEventCell.h"
#import "CalendarDayInfoNoDataCell.h"

@interface CalendarDayInfoTableViewController () <CalendarDayInfoCellDelegate>

@end

@implementation CalendarDayInfoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 144.f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)setCalendarData:(NSDictionary *)calendarData {
    _calendarData = calendarData;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource and UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSArray * dataKeys = [_calendarData allKeys];
    return dataKeys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGRectGetHeight([TitanStyle headerSectionView:nil].frame);
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSArray * dataKeys = [_calendarData allKeys];
    NSString * dataKey = dataKeys[section];
    if ([dataKey isEqualToString:@"TrainingPlan"]) {
        return [TitanStyle headerSectionView:[UIImage imageNamed:@"icon-fitness"]];
    }
    else if ([dataKey isEqualToString:@"NutritionPlan"]) {
        return [TitanStyle headerSectionView:[UIImage imageNamed:@"icon-food"]];
    }
    else {
        return [TitanStyle headerSectionView:[UIImage imageNamed:@"icon-calendar"]];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray * dataKeys = [_calendarData allKeys];
    NSString * datakey = dataKeys[indexPath.section];
    id datum = [_calendarData valueForKey:datakey];
    CalendarInfoType calendarInfoType;
    NSString * cellReuseIdentifier;
    
    if ([datakey isEqualToString:@"TrainingPlan"]) {
        calendarInfoType = CalendarInfoTypeTrainingPlan;
        if ([datum isKindOfClass:[NSString class]]) {
            cellReuseIdentifier = @"CalendarDayInfoNoDataCell";
        }
        else {
            cellReuseIdentifier = @"CalendarDayInfoTrainingCell";
        }
    }
    else if ([datakey isEqualToString:@"NutritionPlan"]) {
        calendarInfoType = CalendarInfoTypeNutritionPlan;
        if ([datum isKindOfClass:[NSString class]]) {
            cellReuseIdentifier = @"CalendarDayInfoNoDataCell";
        }
        else {
            cellReuseIdentifier = @"CalendarDayInfoNutritionPlanCell";
        }
    }
    else if ([datakey isEqualToString:@"Event"]) {
        calendarInfoType = CalendarInfoTypeEvent;
        if ([datum isKindOfClass:[NSString class]]) {
            cellReuseIdentifier = @"CalendarDayInfoNoDataCell";
        }
        else {
            cellReuseIdentifier = @"CalendarDayInfoEventCell";
        }
    }
    else if ([datakey isEqualToString:@"Repose"]) {
        calendarInfoType = CalendarInfoTypeRepose;
        if ([datum isKindOfClass:[NSString class]]) {
            cellReuseIdentifier = @"CalendarDayInfoNoDataCell";
        }
        else {
            cellReuseIdentifier = @"CalendarDayInfoReposeCell";
        }
    }
    else {
        calendarInfoType = CalendarInfoTypeNone;
        cellReuseIdentifier = nil;
        NSAssert(YES, @"Unknown calendar information type");
    }
    
    CalendarDayInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier forIndexPath:indexPath];
    [cell setContent:datum
        selectedDate:_selectedDate
             dataTye:calendarInfoType
            delegate:self];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger section = indexPath.section;
    
    NSArray * dataKeys = [_calendarData allKeys];
    NSString * dataKey = dataKeys[section];
    
    NSData * datum = [_calendarData valueForKey:dataKey];
    if ([datum isKindOfClass:[NSString class]]) {
        return;
    }
    
    [_delegate didPressCalendarInfo:datum];
}

#pragma mark - CalendarDayInfoCellDelegate

- (void)didPressAddInfo:(CalendarInfoType)calendarInfoType {
    [_delegate didPressAddCalendarInfo:calendarInfoType];
}

@end
