//
//  CalendarDayPopupViewController.h
//  Titan
//
//  Created by Marcus Lee on 25/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@protocol CalendarDayPopupViewControllerDelegate
@end

@interface CalendarDayPopupViewController : CommonViewController

//
// Variables
//

@property (nonatomic, strong) NSDictionary * calendarData;
@property (nonatomic, strong) NSDate * selectedDate;

//
// Outlets
//

@property (weak, nonatomic) IBOutlet UIView *swipeDownView;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UIView *touchView;


@end
