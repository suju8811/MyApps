//
//  CalendarDayInfoTableViewController.h
//  Titan
//
//  Created by Marcus Lee on 26/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"

@protocol CalendarDayInfoTableViewControllerDelegate

- (void)didPressAddCalendarInfo:(CalendarInfoType)calendarInfoType;
- (void)didPressCalendarInfo:(id)info;

@end

@interface CalendarDayInfoTableViewController : UITableViewController

@property (nonatomic, strong) NSDictionary * calendarData;
@property (nonatomic, strong) NSDate * selectedDate;
@property (nonatomic, strong) id<CalendarDayInfoTableViewControllerDelegate> delegate;

@end
