//
//  CalendarDayInfoEventCell.m
//  Titan
//
//  Created by Marcus Lee on 26/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CalendarDayInfoEventCell.h"
#import "DateTimeUtil.h"

@implementation CalendarDayInfoEventCell

- (void)setContent:(id)calendarInfo selectedDate:(NSDate *)selectedDate dataTye:(CalendarInfoType)calendarInfoType delegate:(id<CalendarDayInfoCellDelegate>)delegate {
    
    [super setContent:calendarInfo selectedDate:selectedDate dataTye:calendarInfoType delegate:delegate];
    
    Event * event = (Event*)calendarInfo;
    _timeLabel.text = [DateTimeUtil stringFromDate:event.startDate format:@"hh:mm"];
    _titleLabel.text = event.title;
    _noteLabel.text = event.note;
}

@end
