//
//  CalendarDayInfoTrainingPlanCell.h
//  Titan
//
//  Created by Marcus Lee on 26/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarDayInfoCell.h"

@interface CalendarDayInfoTrainingPlanCell : CalendarDayInfoCell

//
// Outlet
//

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *kCalLabel;

@end
