//
//  CalendarDayInfoReposeCell.m
//  Titan
//
//  Created by Marcus Lee on 27/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CalendarDayInfoReposeCell.h"

@implementation CalendarDayInfoReposeCell

- (void)setContent:(id)calendarInfo selectedDate:(NSDate *)selectedDate dataTye:(CalendarInfoType)calendarInfoType delegate:(id<CalendarDayInfoCellDelegate>)delegate {
    
    [super setContent:calendarInfo selectedDate:selectedDate dataTye:calendarInfoType delegate:delegate];
    
    TrainingPlan * repose = (TrainingPlan*)calendarInfo;
    _titleLabel.text = repose.title;
    _noteLabel.text = repose.notes;
}

@end
