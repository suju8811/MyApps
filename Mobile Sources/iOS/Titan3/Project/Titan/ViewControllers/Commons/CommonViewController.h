//
//  CommonViewController.h
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseElement.h"

@interface CommonViewController : UIViewController

//
// Navigation Bar
//

@property (nonatomic, assign) BOOL isHideCustomNavigation;

//
// BaseDisplay Delegate Methods
//

- (void)showLoading:(NSString*)title;
- (void)hideLoading;
- (void)showMessage:(NSString*)message;
- (void)showMessage:(NSString *)message alertAction:(UIAlertAction*)alertAction;

//
// Common Present Methods
//

- (void)presentUserRateView:(BaseElement*)element;

@end
