//
//  SelectorCell.h
//  Titan
//
//  Created by Manuel Manzanera on 14/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectorCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, strong) UIImageView *checkImageView;

@end
