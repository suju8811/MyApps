//
//  OptionsCell.h
//  Titan
//
//  Created by Manuel Manzanera on 8/6/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionsCell : UITableViewCell

@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, assign) BOOL isSelected;

@end
