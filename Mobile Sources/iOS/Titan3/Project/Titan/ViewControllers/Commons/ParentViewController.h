//
//  ParentViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 25/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppContext.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import <Realm/Realm.h>
#import "CommonViewController.h"


@interface ParentViewController : CommonViewController

- (void)popToViewController:(UIViewController*)viewController;
- (void)didSearchButtonClicked:(UISearchBar*)searchBar;
- (void)didSearchBar:(UISearchBar*)searchBar TextChange:(NSString*)searchText;

@end
