//
//  ParentViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 25/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ParentViewController.h"
#import "TransparentView.h"
#import "FunctionsTableViewController.h"
#import "MenuHomeViewController.h"
#import "InitViewController.h"
#import "SplashViewController.h"
#import "LoginViewController.h"
#import "AlimentAddViewController.h"
#import "AttributeAddViewController.h"
#import "UserSelectorViewController.h"
#import "RateViewController.h"
#import "NotificationSettingViewController.h"
#import "HelpViewController.h"
#import "UnitSettingViewController.h"
#import "SuggestionViewController.h"
#import "ReposeViewController.h"
#import "EventViewController.h"
#import "OptionPopupViewController.h"

@interface ParentViewController ()

@property (nonatomic, strong) UIView *panView;
@property (nonatomic, strong) TransparentView *transparentView;

@end

@implementation ParentViewController

- (void)loadView {
    [super loadView];
    if ([self isKindOfClass:[AlimentAddViewController class]] ||
        [self isKindOfClass:[AttributeAddViewController class]] ||
        [self isKindOfClass:[RateViewController class]] ||
        [self isKindOfClass:[NotificationSettingViewController class]] ||
        [self isKindOfClass:[HelpViewController class]] ||
        [self isKindOfClass:[UnitSettingViewController class]] ||
        [self isKindOfClass:[SuggestionViewController class]] ||
        [self isKindOfClass:[ReposeViewController class]] ||
        [self isKindOfClass:[EventViewController class]] ||
        [self isKindOfClass:[OptionPopupViewController class]]) {
        
        return;
    }
    
    UIView *loadView = [[UIView alloc] initWithFrame:CGRectMake(0., 0.,[[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    [loadView setBackgroundColor:[UIColor clearColor]];
    
    [self setView:loadView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setAutomaticallyAdjustsScrollViewInsets:FALSE];
    
    /*
    if(![self isKindOfClass:[FunctionsTableViewController class]] && ![self isKindOfClass:[MenuHomeViewController class]] && ![self isKindOfClass:[SplashViewController class]] && ![self isKindOfClass:[InitViewController class]] && ![self isKindOfClass:[LoginViewController class]]){
        NSLog(@"%@",self.description);
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showBlurView) name:@"willShowMenuViewController" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideBlurView) name:@"didHideMenuViewController" object:nil];
    }*/
    
    BOOL showPanGesture = YES;
    
    if ([self isKindOfClass:[AlimentAddViewController class]] ||
        [self isKindOfClass:[AttributeAddViewController class]] ||
        [self isKindOfClass:[RateViewController class]] ||
        [self isKindOfClass:[NotificationSettingViewController class]] ||
        [self isKindOfClass:[HelpViewController class]] ||
        [self isKindOfClass:[UnitSettingViewController class]] ||
        [self isKindOfClass:[SuggestionViewController class]] ||
        [self isKindOfClass:[ReposeViewController class]] ||
        [self isKindOfClass:[EventViewController class]] ||
        [self isKindOfClass:[OptionPopupViewController class]]) {
        
        showPanGesture = NO;
    }
    
    if (!showPanGesture) {
        return;
    }
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
    [panGesture setEnabled:TRUE];
    
    _panView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH * 0.4, HEIGHT)];
    [_panView setBackgroundColor:[UIColor clearColor]];
    [_panView addGestureRecognizer:panGesture];
    
    [self.view addSubview:_panView];
    
    
    //[self.view addGestureRecognizer:panGesture];
}

- (void)dealloc{
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"willShowMenuViewController" object:nil];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"didHideMenuViewController" object:nil];
}

- (void)showBlurView{
    //[self.view addSubview:_transparentView];
}

- (void)hideBlurView{
    //[_transparentView removeFromSuperview];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    /*
    if(![self isKindOfClass:[FunctionsTableViewController class]] && ![self isKindOfClass:[MenuHomeViewController class]] && ![self isKindOfClass:[SplashViewController class]] && ![self isKindOfClass:[InitViewController class]] && ![self isKindOfClass:[LoginViewController class]])
        [self.view addSubview:_transparentView];
     */
}

- (void)panGestureAction:(UIPanGestureRecognizer *)recognizer{

    CGPoint translation = [recognizer translationInView:self.view];
    
    //CGPoint velocity = [recognizer velocityInView:self.view];
    if(translation.x > 80)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTIF_MENU_SLIDE" object:nil];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didSearchButtonClicked:(UISearchBar*)searchBar {    
}

- (void)didSearchBar:(UISearchBar*)searchBar TextChange:(NSString*)searchText {    
}

@end
