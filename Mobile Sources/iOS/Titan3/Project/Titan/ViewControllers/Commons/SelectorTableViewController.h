//
//  SelectorTableViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 14/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import <Realm/Realm.h>
#import "Training.h"
#import "TrainingExercise.h"
#import "TrainingPlan.h"
#import "TrainingDay.h"

typedef enum {
    SelectorTypeExercise,
    SelectorTypeExerciseSingle,
    SelectorTypeTrainings,
    SelectorTypeTrainingSingle,
    SelectorTypeTrainingDay,
    SelectorTypeTrainingPlan
} SelectorType;

@protocol SelectorTableViewDelegate;

@interface SelectorTableViewController : ParentViewController

@property (nonatomic, assign) id<SelectorTableViewDelegate>delegate;
@property (nonatomic, assign) BOOL isOnlyOneSelector;
@property (nonatomic, strong) RLMResults *items;
@property (nonatomic, strong) Training *selectTraining;
@property (nonatomic, strong) TrainingPlan *selectTrainingPlan;
@property (nonatomic, strong) TrainingDay *selectTrainingDay;
@property (nonatomic, strong) TrainingDayContent *selectTrainingDayContent;
@property (nonatomic, strong) TrainingExercise *selectTrainingExercise;
@property (nonatomic, strong) NSMutableArray *selectsExercises;
@property (nonatomic, strong) TrainingPlan *selectedSinglePlan;
@property (nonatomic, strong) NSMutableArray *selectTrainings;
@property (nonatomic, assign) BOOL presentWorkoutDetail;
@property (nonatomic, assign) BOOL presentWorkoutDetailForReplacing;
@property (nonatomic, assign) BOOL presentTraininPlanDetail;
@property (nonatomic, assign) SelectorType selectorType;
@property (nonatomic, assign) NSInteger selectedFitnessIndex;

@end

@protocol SelectorTableViewDelegate <NSObject>

@optional
- (void)returnData:(NSMutableArray *)returnData withType:(SelectorType)selectorType;
- (void)didSelectTrainingPlan:(TrainingPlan*)trainingPlan;

@end
