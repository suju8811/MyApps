//
//  OptionsViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 8/6/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OptionsViewController.h"
#import "WebServiceManager.h"
#import "OptionsCell.h"
#import "MRProgress.h"
#import "AlertCustomView.h"
#import "AppDelegate.h"
#import "OptionViewExecutive.h"

@interface OptionsViewController ()<UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate, AlertCustomViewDelegate, OptionViewDisplay>

@property (nonatomic, strong) UITableView *optionsTableView;
@property (nonatomic, strong) TrainingPlan *duplicateTrainingPlan;
@property (nonatomic, strong) AlertCustomView *alertCustomView;

@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) UIView *containerPickerView;
@property (nonatomic, strong) UIDatePicker *datePicker;

@property (nonatomic, strong) OptionViewExecutive *executive;

@end

@implementation OptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    
    _executive = [[OptionViewExecutive alloc] initWithDisplay:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)configureView{
    
    [[self view] setBackgroundColor:BLACK_APP_COLOR];
    //[self.view addSubview:[self headerView]];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _optionsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, HEIGHT) style:UITableViewStylePlain];
    
    [_optionsTableView setDelegate:self];
    [_optionsTableView setDataSource:self];
    [_optionsTableView registerClass:[OptionsCell class] forCellReuseIdentifier:@"optionCell"];
    [_optionsTableView setBackgroundColor:[UIColor clearColor]];
    [_optionsTableView setTableHeaderView:[self headerView]];
    [_optionsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.view addSubview:_optionsTableView];
    
    [self footerView];
}

- (UIView *)headerView{
    UIView *headerView  = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:100])];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:20], WIDTH - [UtilManager width:30], [UtilManager height:60])];
    [titleLabel setText:@"Sample"];
    
    
    switch (_optionType) {
        case OptionTypeExercise:
            [titleLabel setText:_trainingExercise.title];
            break;
        case OptionTypeWorkout:
            [titleLabel setText:_currentTraining.title];
            break;
        case OptionTypeTrainings:
            [titleLabel setText:_currentTrainingPlan.title];
            break;
        default:
            [titleLabel setText:NSLocalizedString(@"Añadir opción al día seleccionado", nil)];
            break;
    }
    
    [titleLabel setTextColor:YELLOW_APP_COLOR];
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (void)footerView{
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    [closeButton setTitle:NSLocalizedString(@"Cerrar", nil) forState:UIControlStateNormal];
    [[closeButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:19]];
    [closeButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(dismissDisplay) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:closeButton];
}

- (void)presentDatePicker:(id)sender{
    
    if(_datePicker)
    {
        [_datePicker removeFromSuperview];
        _datePicker = nil;
        
        [_containerPickerView removeFromSuperview];
        _containerPickerView = nil;
    }
    
    _containerPickerView = [[UIView alloc] initWithFrame:CGRectMake(0., HEIGHT - [UtilManager height:206], WIDTH, [UtilManager height:206])];
    [_containerPickerView setBackgroundColor:[UIColor clearColor]];
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0., [UtilManager height:44], self.view.frame.size.width, [UtilManager height:162])];
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    [_datePicker setBackgroundColor:BLACK_APP_COLOR];
    [_datePicker setDate:[NSDate date]];
    [_datePicker setMinimumDate:[NSDate date]];
    [_datePicker setMaximumDate:[NSDate dateWithTimeIntervalSinceNow:24 * 60 * 60 * 100]];
    
    [_datePicker setUserInteractionEnabled:YES];
    [_datePicker setValue:YELLOW_APP_COLOR forKey:@"textColor"];
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,WIDTH,[UtilManager height:44])];
    [toolBar setBarStyle:UIBarStyleBlackTranslucent];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil)
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(acceptDatePickerAction:)];
    
    UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancelar", nil)
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(cancelDatePickerAction:)];
    
    [barButtonDone setTintColor:GRAY_REGISTER_FONT];
    toolBar.items = @[barButtonDone,barButtonCancel];
    [_containerPickerView addSubview:_datePicker];
    [_containerPickerView addSubview:toolBar];
    
    [self.view addSubview:_containerPickerView];
    //[self.view addSubview:_acceptButton];
}

- (void)cancelDatePickerAction:(id)sender{
    NSLog(@"");
    [_containerPickerView removeFromSuperview];
    _containerPickerView = nil;
    
    [_datePicker removeFromSuperview];
    _datePicker = nil;
}

- (void)acceptDatePickerAction:(id)sender{
    
    /*
    _startDate = _datePicker.date;
   
    [_containerPickerView removeFromSuperview];
    _containerPickerView = nil;
    
    [_datePicker removeFromSuperview];
    _datePicker = nil;
    
    NSMutableDictionary *paramDictionary = [[NSMutableDictionary alloc] init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    [paramDictionary setObject:_currentTrainingPlan.serverTrainingPlanId forKey:@"trainingPlan"];
    [paramDictionary setObject:[formatter stringFromDate:_startDate] forKey:@"day"];
    
    _duplicateTrainingPlan = [TrainingPlan duplicateTrainingPlan:_currentTrainingPlan];
    [_duplicateTrainingPlan setOwner:[[[AppContext sharedInstance] currentUser] userId]];
    [_duplicateTrainingPlan setTitle:[NSString stringWithFormat:@"%@",_currentTrainingPlan.title]];
    [_duplicateTrainingPlan setContent:_currentTrainingPlan.content];
    [_duplicateTrainingPlan setStartDate:[NSDate date]];
    [_duplicateTrainingPlan setIsCalendarCopy:[NSNumber numberWithBool:TRUE]];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addObject:_duplicateTrainingPlan];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [MRProgressOverlayView showOverlayAddedTo:self.view title:@"Añadiendo a calendario ..." mode:MRProgressOverlayViewModeIndeterminate animated:YES];
    [WebServiceManager addTrainingPlanToCalendar:paramDictionary andDelegate:self];
     */
    
    NSLog(@"");
    [_containerPickerView removeFromSuperview];
    _containerPickerView = nil;
    
    [_datePicker removeFromSuperview];
    _datePicker = nil;
    
    [UtilManager presentAlertWithMessage:@"Opción implementada a falta de detallar workflow"];
}


#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView setUserInteractionEnabled:FALSE];
    
    if(_optionType == OptionTypeTrainings) {
        switch (indexPath.row) {
            case 0:
                [self dismissDisplay];
                [_delegate editTrainingPlan:_currentTrainingPlan];
                break;
            case 1:
            {
                _duplicateTrainingPlan = [TrainingPlan copyTrainingPlan:_currentTrainingPlan];
                [_executive duplicateTrainingPlan:_duplicateTrainingPlan];
            }
                break;
            case 2:
            {
                if (_fromCalendar == 1) {
                    [self dismissViewControllerAnimated:YES completion:^{
                        [_delegate didPressDeleteFitness:_currentTrainingPlan];
                    }];
                }
                else {
                    [_executive favoriteTrainingPlan:_currentTrainingPlan];
                }
            }
                break;
            case 3:
                [_executive deleteTrainingPlan:_currentTrainingPlan];
                break;
            case 4: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressAddToCalendar:_currentTrainingPlan];
                }];
            }
                break;
            case 5: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressSendFood:_currentTrainingPlan];
                }];
            }
                break;
            case 6:{
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressShareFood:_currentTrainingPlan];
                }];
            }
                break;
            default:
                break;
        }
    } else if (_optionType == OptionTypeWorkout){
        switch (indexPath.row) {
            case 0:
                [self dismissDisplay];
                [_delegate editTraining:_currentTraining];
                break;
            case 1:
            {
                Training *copyTraining = [Training copyTraining:_currentTraining];
                [_executive duplicateTraining:copyTraining];
            }
                break;
            case 2:
                [_executive favoriteTraining:_currentTraining];
                break;
            case 3:
                [_executive deleteTraining:_currentTraining];
                break;
            case 4: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressSendFood:_currentTraining];
                }];
            }
                break;
            case 5:{
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressShareFood:_currentTraining];
                }];
            }
                break;
            default:
                break;
        }
    }
    else if (_optionType == OptionTypeExercise) {
        switch (indexPath.row) {
            case 0:
                if(_trainingExercise.exercises.count > 1) {
                    [_delegate removeSuperSerie];
                }
                else {
                    [_delegate setSuperSerie];
                }
                
                [self dismissDisplay];
                break;
            case 1: {
                TrainingExercise *trainingExercise = [TrainingExercise duplicateTrainingExercise:_trainingExercise];
                
                RLMRealm *realm = [AppContext currentRealm];
                
                [realm transactionWithBlock:^{
                    [[_currentTraining trainingExercises] addObject:trainingExercise];
                }];
                
                [realm beginWriteTransaction];
                
                NSError *error;
                [realm commitWriteTransaction:&error];
                
                if(error){
                    NSLog(@"%@",error.description);
                }
                
                [self dismissDisplay];
            }
                break;
            case 2:
                if(_currentTraining.trainingExercises.count > 1) {
                    if([Training canDeleteTraining:_currentTraining]){
                        [Training deleteTrainingExercise:_trainingExercise inTraining:_currentTraining];
                    }else
                        [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"No tienes permisos para eliminar este Entrenamiento", nil)] animated:TRUE completion:^{
                            
                        }];
                    
                } else {
                    [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"Un entrenamiento debe tener mínimo un ejercicio. Borre el entrenamiento si lo desea.", nil) andTitle:@""] animated:YES completion:^{
                    
                    }];
                }
                [self dismissDisplay];
                break;
            case 3: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressReplace:_trainingExercise];
                }];
            }
                break;
            case 4: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressTimePicker:_trainingExercise];
                }];
            }
                break;
            default:
                break;
        }
    }
    else if (_optionType  == OptionTypeTrainingDay) {
        switch (indexPath.row) {
            case 0:
        
                [self dismissDisplay];
                [_delegate addWorkoutsToTrainingDay:_trainingDay];
                
                break;
            case 1:
            {
                _alertCustomView = [[AlertCustomView alloc] initWithFrameAndTextField:CGRectMake(0., 0., WIDTH, HEIGHT)];
                [[_alertCustomView titleLabel] setText:NSLocalizedString(@"Número de Réplicas", nil)];
                [_alertCustomView setTag:3];
                [[_alertCustomView textField] setText:@"1"];
                [_alertCustomView setDelegate:self];
                
                [_alertCustomView setCenter:self.view.center];
                
                [self.view addSubview:_alertCustomView];
            }
                break;
            
            case 2:{
                if(_trainingDay.trainingDayContent.count > 0)
                    _trainingDayContent = [_trainingDay.trainingDayContent firstObject];
                Training *editTraining = _trainingDayContent.training;
                [self dismissDisplay];
                [_delegate editWorkOut:editTraining];
            }
                break;
            case 3:
                [self deleteTrainingPlan];
                break;
            case 4: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressReplace:_trainingDay];
                }];
            }
                break;
            case 5: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressReplicateTodo];
                }];
            }
                break;
            case 6: {
                [self dismissViewControllerAnimated:YES completion:^{
                    [_delegate didPressTimePicker:_trainingDay];
                }];
            }
                break;
            default:
                break;
        }
    }
}

- (void) deleteTrainingPlan {
    if([TrainingPlan canDeleteTrainingPlan:_currentTrainingPlan]) {
        [TrainingPlan deleteTrainingDayContent:_trainingDayContent inTrainingDay:_trainingDay];
//        [TrainingPlan deleteTrainingDay:_trainingDay inTrainingPlan:_currentTrainingPlan];
        [_executive updateTrainingPlan:_currentTrainingPlan];
    }
    else {
        [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"No tienes permisos para eliminar este Plan de Entrenamiento", nil)] animated:TRUE completion:^{
            
        }];
    }
}
#pragma mark UITableView Datasource Methods

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.08 * HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.08 * HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(_optionType == OptionTypeExercise) {
        if (_fromDetailOfParent) {
            return 4;
        }
        else {
            return 3;
        }
    }
    else if (_optionType == OptionTypeTrainings) {
        if (_fromCalendar == 1) {
            return 3;
        }
        return 7;
    }
    else if (_optionType == OptionTypeTrainingDay &&
             _fromDetailOfParent) {
        return 7;
    }
    else {
        NSInteger returnValue;
        if (_currentTraining != nil &&
            [_currentTraining.owner isEqualToString:[AppContext sharedInstance].currentUser.name]) {
            returnValue = 6;
        }
        else {
            returnValue = 3;
        }
        
        if (_fromDetailOfParent) {
            returnValue = returnValue + 1;
        }
        
        return returnValue;
    }
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OptionsCell *cell = [_optionsTableView dequeueReusableCellWithIdentifier:@"optionCell"];
    if(_optionType == OptionTypeExercise){
        switch (indexPath.row) {
            case 0:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-edit"];
                
                if(_trainingExercise.exercises.count > 1) {
                    cell.titleLabel.text = NSLocalizedString(@"Eliminar SuperSerie", nil);
                }
                else {
                    cell.titleLabel.text = NSLocalizedString(@"Añadir SuperSerie", nil);
                }
                break;
            case 1:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-duplicate"];
                cell.titleLabel.text = NSLocalizedString(@"Duplicar", nil);
                break;
            case 2:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-delete"];
                cell.titleLabel.text = NSLocalizedString(@"Eliminar", nil);
                break;
            case 3:
                cell.iconImageView.image = [UIImage imageNamed:@"icon_replace_gray"];
                cell.titleLabel.text = NSLocalizedString(@"Sustituir ejercicio", nil);
                break;
            default:
                break;
        }
    } else if (_optionType == OptionTypeTrainingDay) {
        switch (indexPath.row) {
            case 0:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-edit"];
                cell.titleLabel.text = NSLocalizedString(@"Añadir Extra", nil);
                break;
            case 1:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-duplicate"];
                cell.titleLabel.text = NSLocalizedString(@"Replicar", nil);
                break;
            case 2:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-edit"];
                cell.titleLabel.text = NSLocalizedString(@"Editar", nil);
                break;
            case 3:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-delete"];
                cell.titleLabel.text = NSLocalizedString(@"Eliminar", nil);
                break;
            case 4:
                cell.iconImageView.image = [UIImage imageNamed:@"icon_replace_gray"];
                cell.titleLabel.text = NSLocalizedString(@"Sustituir entrenamiento", nil);
                break;
            case 5:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-duplicate"];
                cell.titleLabel.text = NSLocalizedString(@"Replicar todo", nil);
                break;
            case 6:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-calendar"];
                cell.titleLabel.text = NSLocalizedString(@"Añadir hora", nil);
                break;
            default:
                break;
        }
    } else {
        switch (indexPath.row) {
            case 0:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-edit"];
                cell.titleLabel.text = NSLocalizedString(@"Editar", nil);
                break;
            case 1:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-duplicate"];
                cell.titleLabel.text = NSLocalizedString(@"Duplicar", nil);
                break;
            case 2:
                if (_optionType == OptionTypeTrainings &&
                    _fromCalendar == 1) {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon-delete"];
                    cell.titleLabel.text = NSLocalizedString(@"Eliminar", nil);
                }
                else {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon-fav-off"];
                    
                    if (_optionType == OptionTypeTrainings) {
                        if(_currentTrainingPlan.isFavourite) {
                            cell.titleLabel.text = NSLocalizedString(@"Deshacer Favorito", nil);
                        }
                        else {
                            cell.titleLabel.text = NSLocalizedString(@"Añadir a Favorito", nil);
                        }
                    } else if (_optionType == OptionTypeWorkout) {
                        if(_currentTraining.isFavourite.boolValue) {
                            cell.titleLabel.text = NSLocalizedString(@"Deshacer Favorito", nil);
                        }
                        else {
                            cell.titleLabel.text = NSLocalizedString(@"Añadir a Favorito", nil);
                        }
                    }
                }
                
                break;
            case 3:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-delete"];
                cell.titleLabel.text = NSLocalizedString(@"Eliminar", nil);
                break;
            case 4:
                if (_currentTraining != nil &&
                    [_currentTraining.owner isEqualToString:[AppContext sharedInstance].currentUser.name]) {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                    cell.titleLabel.text = @"Enviar";
                }
                else {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon-calendar"];
                    cell.titleLabel.text = NSLocalizedString(@"Añadir a calendario", nil);
                }
                break;
            case 5:
                if (_currentTraining != nil &&
                    [_currentTraining.owner isEqualToString:[AppContext sharedInstance].currentUser.name]) {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                    cell.titleLabel.text = @"Compartir";
                }
                else {
                    cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                    cell.titleLabel.text = @"Enviar";
                }
                break;
            case 6:
                cell.iconImageView.image = [UIImage imageNamed:@"icon-comunity-off"];
                cell.titleLabel.text = @"Compartir";
                break;
            default:
                break;
        }
    }
    
    return cell;
    
}

#pragma mark WebServiceDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        [self dismissDisplay];
    }];
}
    
- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    if(webServiceType == WebServiceTypeGetTraining){
        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
            [self dismissDisplay];
        }];
    }else if(webServiceType == WebServiceTypeUpdateTraining){
        NSDictionary *workoutDictionary = (NSDictionary *)object;
        
        NSDictionary *dataDictionary = [workoutDictionary valueForKey:@"data"];
        
        RLMRealm *realm = [AppContext currentRealm];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        
        [realm transactionWithBlock:^{
            [_currentTraining setServerTrainingId:[dataDictionary valueForKey:@"_id"]];
            [_currentTraining setOwner:[dataDictionary valueForKey:@"owner"]];
            [_currentTraining setCreatedAt:[formatter dateFromString:[dataDictionary valueForKey:@"createdAt"]]];
            [_currentTraining setUpdatedAt:[formatter dateFromString:[dataDictionary valueForKey:@"updatedAt"]]];
        }];
        
        NSArray *trainingExercises = [dataDictionary valueForKey:@"training_exercises"];
        
        int order = 0;
        
        for(NSString *trainingExerciseServerId in trainingExercises){
            
            [realm transactionWithBlock:^{
                [[[_currentTraining trainingExercises] objectAtIndex:order] setServerTrainingId:trainingExerciseServerId];
            }];
            
            order++;
        }
        
        [realm beginWriteTransaction];
        [realm addOrUpdateObject:_currentTraining];
        
        NSError *error;
        [realm commitWriteTransaction:&error];
        if(error){
            NSLog(@"%@",error.description);
        }
    }
}

#pragma mark AlertCustomViewDelegate

- (void)dismissAlertViewSelected{
    [_alertCustomView removeFromSuperview];
}

- (void)cancelAlertViewDidselected  {
    [_alertCustomView removeFromSuperview];
}

- (void)confirmAlertViewDidSelected{
    
    if(_alertCustomView.textField.text){
        
        int replies = _alertCustomView.textField.text.intValue;
        
        int order = (int)_currentTrainingPlan.trainingDay.count;
        
        for(int i=0 ;i<replies; i++){
            TrainingDay *copyTrainingDay = [TrainingDay copyTrainingDay:_trainingDay];
            copyTrainingDay.order = [NSNumber numberWithInt:order];
            order++;
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                [_currentTrainingPlan.trainingDay addObject:copyTrainingDay];
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
        }
        
        [_executive updateTrainingPlan:_currentTrainingPlan];
    } else
        [_alertCustomView removeFromSuperview];
}

#pragma mark - Option View Display Delegate

- (void)didDuplicateTraining:(Training *)training {
    [WebServiceManager getTrainingsWithCompletionBlock:^(BOOL isSuccess) {
        [self dismissDisplay];
    }];
}

- (void)didDeletedTraining:(Training *)training {
    if([_delegate respondsToSelector:@selector(removeWorkout)]) {
        [_delegate removeWorkout];
    }
    
    [self dismissDisplay];
}

- (void)dismissDisplay {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didUpdateTrainingPlan:(TrainingPlan *)training {
    [WebServiceManager getTrainingPlansWithCompletionBlock:^(BOOL isSuccess) {
        [self dismissDisplay];
    }];
}

- (void)didDuplicateTrainingPlan:(TrainingPlan*)training {
    [WebServiceManager getTrainingPlansWithCompletionBlock:^(BOOL isSuccess) {
        [self dismissDisplay];
    }];
}

- (void)didDeletedTrainingPlan:(TrainingPlan*)training {
    if([_delegate respondsToSelector:@selector(removeTrainingPlan)]) {
            [_delegate removeTrainingPlan];
    }
    
    [self dismissDisplay];
}

@end
