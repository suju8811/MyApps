//
//  TextFieldCell.h
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TextFieldCellDelegate;

@interface TextFieldCell : UITableViewCell

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, assign) id<TextFieldCellDelegate>delegate;

@end

@protocol TextFieldCellDelegate <NSObject>

@end
