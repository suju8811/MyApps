//
//  TextFieldCell.m
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TextFieldCell.h"

#import "AppContext.h"
#import "AppConstants.h"
#import "UtilManager.h"

@interface TextFieldCell () <UITextFieldDelegate>

@end

@implementation TextFieldCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        
        [self setBackgroundColor:[UIColor clearColor]];
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        
        _textField  =[[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:5] , WIDTH - [UtilManager width:20], UITEXTFIELD_HEIGHT)];
        [_textField setDelegate:self];
        [_textField setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]];
        [_textField setTextColor:GRAY_REGISTER_FONT];
        
        [[self contentView] addSubview:_textField];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(_textField.frame.origin.x, _textField.frame.origin.y + _textField.frame.size.height , WIDTH - [UtilManager width:20], 1)];
        [_separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        
        [[self contentView] addSubview:_separatorView];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
