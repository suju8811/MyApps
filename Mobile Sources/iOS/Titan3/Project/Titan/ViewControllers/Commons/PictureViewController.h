//
//  PictureViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 7/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "PictureVisorViewController.h"

@interface PictureViewController : ParentViewController

@property (nonatomic) int page;
@property (nonatomic, strong) UIImageView *picImageView;
@property (nonatomic, strong) NSString *imageId;
@property (nonatomic, assign) PictureVisorViewController *pictureVisorViewcontroller;

- (id)initWithImageId:(NSString *)image;

@end
