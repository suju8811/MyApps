//
//  CommonTableViewController.m
//  Titan
//
//  Created by Marcus Lee on 25/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "CommonTableViewController.h"
#import "MRProgress.h"
#import "UtilManager.h"

@interface CommonTableViewController ()

@end

@implementation CommonTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

#pragma mark - BaseDisplay Delegate Functions

- (void)showLoading:(NSString*)title {
    NSString * localizedTitle = @"";
    if (title != nil) {
        localizedTitle = NSLocalizedString(title, nil);
    }
    
    [MRProgressOverlayView showOverlayAddedTo:self.view
                                        title:localizedTitle
                                         mode:MRProgressOverlayViewModeCheckmark
                                     animated:YES];
}

- (void)hideLoading {
    [MRProgressOverlayView dismissOverlayForView:self.view animated:NO];
}

- (void)showMessage:(NSString*)message {
    [self presentViewController:[UtilManager presentAlertWithMessage:@""
                                                            andTitle:NSLocalizedString(message, nil)]
                       animated:YES
                     completion:nil];
}

- (void)showMessage:(NSString *)message alertAction:(UIAlertAction*)alertAction {
    [self presentViewController:[UtilManager presentAlertWithMessage:@"" andTitle:NSLocalizedString(message, nil) alertAction:alertAction]
                       animated:YES
                     completion:nil];
}

@end
