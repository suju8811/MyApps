//
//  SearchTableViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 22/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Realm/Realm.h>

@interface SearchTableViewController : UITableViewController

@property (nonatomic, strong) RLMResults *filteredProducts;

@end
