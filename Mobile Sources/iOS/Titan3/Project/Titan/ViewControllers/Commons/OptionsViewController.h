//
//  OptionsViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 8/6/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ParentViewController.h"
#import "TrainingPlan.h"
#import "Training.h"

typedef enum {
    OptionTypeWorkout,
    OptionTypeTrainings,
    OptionTypeExercise,
    OptionTypeSchedule,
    OptionTypeTrainingDay
} OptionType;

@protocol OptionsViewControllerDelegate <NSObject>

- (void)editTrainingPlan:(TrainingPlan *)trainingPlan;
- (void)editTraining:(Training *)training;
- (void)setSuperSerie;
- (void)removeSuperSerie;
- (void)removeWorkout;
- (void)removeTrainingPlan;

///// TrainingDays

- (void)addWorkoutsToTrainingDay:(TrainingDay *)trainingDay;
- (void)editWorkOut:(Training *)selectWorkout;

@optional
- (void)didPressReplace:(RLMObject*)rlmObject;
- (void)didPressReplicateTodo;
- (void)didPressSendFood:(RLMObject*)rlmObject;
- (void)didPressShareFood:(RLMObject*)rlmObject;
- (void)didPressTimePicker:(RLMObject*)rlmObject;
- (void)didPressDeleteFitness:(RLMObject*)rlmObject;
- (void)didPressAddToCalendar:(RLMObject*)rlmObject;

@end

@interface OptionsViewController : ParentViewController

@property (nonatomic, assign) OptionType optionType;
@property (nonatomic, strong) TrainingPlan *currentTrainingPlan;
@property (nonatomic, strong) Training *currentTraining;
@property (nonatomic, strong) TrainingExercise *trainingExercise;
@property (nonatomic, strong) TrainingDay *trainingDay;
@property (nonatomic, strong) TrainingDayContent *trainingDayContent;
@property (nonatomic, assign) id<OptionsViewControllerDelegate>delegate;
@property (nonatomic, assign) BOOL fromDetailOfParent;
@property (nonatomic, assign) NSInteger fromCalendar;

@end
