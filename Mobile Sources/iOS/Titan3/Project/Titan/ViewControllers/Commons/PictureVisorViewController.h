//
//  PictureVisorViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 7/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

@interface PictureVisorViewController : ParentViewController <UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *picScrollView;
@property (nonatomic, strong) NSMutableArray *viewControllers;
@property (nonatomic, strong) NSArray *picImages;
@property (nonatomic) int picNumber;

@end
