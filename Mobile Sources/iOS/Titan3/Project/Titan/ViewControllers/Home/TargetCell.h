//
//  TargetCell.h
//  Titan
//
//  Created by Manuel Manzanera on 10/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TargetCell : UITableViewCell

@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIImageView *photoView;
@property (nonatomic, strong) UILabel *weightLabel;
@property (nonatomic, strong) UILabel *percentLabel;

@end
