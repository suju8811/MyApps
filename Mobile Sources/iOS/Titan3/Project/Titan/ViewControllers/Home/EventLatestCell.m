//
//  EventLatestCell.m
//  Titan
//
//  Created by Marcus Lee on 19/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "EventLatestCell.h"
#import "DateTimeUtil.h"

@implementation EventLatestCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)setContentWithTodayEvent:(Event*)todayEvent
                   andNextEvents:(NSArray<Event*>*)nextEvents {
    
    _todayEvent = todayEvent;
    _nextEvents = nextEvents;
    
    for (UIView * eventView in _eventViews) {
        eventView.hidden = YES;
    }
    
    if (_nextEvents == nil ||
        _nextEvents.count == 0) {
        
        _descriptionLabel.hidden = NO;
    }
    else {
        _descriptionLabel.hidden = YES;
    }
    
    if (_todayEvent) {
        _eventViews[0].hidden = NO;
        _todayEventTitleLabel.text = _todayEvent.title;
        _todayEventDescriptionLabel.text = _todayEvent.note;
    }
    
    for (NSInteger index = 0; index < _nextEvents.count; index ++) {
        _nextEventsTitleLabels[index].text = _nextEvents[index].title;
        _nextEventsDescriptionsLabels[index].text = _nextEvents[index].note;
        _nextEventsDaysLabels[index].text = [DateTimeUtil spanishStringFromDate:_nextEvents[index].selectedDate format:@"d MMMM yyyy"];
        _eventViews[index + 1].hidden = NO;
    }
}

- (IBAction)didPressAddEventButton:(id)sender {
    if ([_delegate respondsToSelector:@selector(eventLatestCell:didPressAddEventAtIndex:)]) {
        [_delegate eventLatestCell:self didPressAddEventAtIndex:0];
    }
}

@end
