//
//  EventLatestCell.h
//  Titan
//
//  Created by Marcus Lee on 19/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

@class EventLatestCell;

@protocol EventLatestCellDelegate<NSObject>

- (void)eventLatestCell:(EventLatestCell*)cell didPressAddEventAtIndex:(NSInteger)index;

@end

@interface EventLatestCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *todayEventTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *todayEventDescriptionLabel;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray<UILabel*> *nextEventsTitleLabels;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray<UILabel*> *nextEventsDescriptionsLabels;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray<UILabel*> *nextEventsDaysLabels;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray<UIView*> *eventViews;

@property (nonatomic, strong) Event * todayEvent;
@property (nonatomic, strong) NSArray<Event*> * nextEvents;
@property (weak, nonatomic) IBOutlet UIButton *addEventButton;

@property (nonatomic, strong) id<EventLatestCellDelegate> delegate;

- (void)setContentWithTodayEvent:(Event*)todayEvent
                   andNextEvents:(NSArray<Event*>*)nextEvents;

@end
