//
//  TrainFoodCell.h
//  Titan
//
//  Created by Manuel Manzanera on 9/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircleProgressBar.h"

@class FitnessFoodCell;

@protocol FitnessFoodCellDelegate<NSObject>

- (void)fitnessFoodCell:(FitnessFoodCell*)cell didPressTrainingAtIndex:(NSInteger)index;
- (void)fitnessFoodCell:(FitnessFoodCell*)cell didPressAddTrainingPlanAtIndex:(NSInteger)index;
- (void)fitnessFoodCell:(FitnessFoodCell*)cell didPressAddReposeAtIndex:(NSInteger)index;

- (void)fitnessFoodCell:(FitnessFoodCell*)cell didPressNutritionPlanAtIndex:(NSInteger)index;
- (void)fitnessFoodCell:(FitnessFoodCell*)cell didPressAddNutritionPlanAtIndex:(NSInteger)index;
- (void)fitnessFoodCell:(FitnessFoodCell*)cell didPressShowTrainingAtIndex:(NSInteger)index;

@end

@interface FitnessFoodCell : UITableViewCell

@property (nonatomic, strong) UIView *trainView;
@property (nonatomic, strong) UIView *foodView;

@property (nonatomic, strong) UILabel *fitnessHourLabel;
@property (nonatomic, strong) UILabel *foodHourLabel;

@property (nonatomic, strong) UILabel *fitnessValueLabel;
@property (nonatomic, strong) UILabel *foodValueLabel;

@property (nonatomic, strong) UIImageView * clockImageView;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIImageView * kCalImageView;
@property (nonatomic, strong) UILabel *fitnessKCalLabel;

@property (nonatomic, strong) UIImageView * addTrainingPlanImageView;
@property (nonatomic, strong) UILabel * addTrainingPlanLabel;
@property (nonatomic, strong) UIButton * addTrainingPlanButton;

@property (nonatomic, strong) UIImageView * addReposeImageView;
@property (nonatomic, strong) UILabel * addReposeLabel;
@property (nonatomic, strong) UIButton * addReposeButton;

@property (nonatomic, strong) UILabel *balanceValueLabel;

@property (nonatomic, strong) UIImageView * kCalFoodImageView;
@property (nonatomic, strong) UILabel *foodKCalLabel;
@property (nonatomic, strong) UILabel *diferencekCalLabel;

@property (nonatomic, strong) UIImageView * addNutritionPlanImageView;
@property (nonatomic, strong) UILabel * addNutritionPlanLabel;
@property (nonatomic, strong) UIButton * addNutritionPlanButton;

@property (nonatomic, strong) CircleProgressBar *fitnessCircleProgress;
@property (nonatomic, strong) CircleProgressBar *foodCircleProgress;

@property (nonatomic, strong) UILabel *fitnessDayLabel;
@property (nonatomic, strong) UILabel *foodDayLabel;

@property (nonatomic, strong) UILabel *balanceFitnessLabel;
@property (nonatomic, strong) UILabel *balanceFoodLabel;

@property (nonatomic, strong) UIView *separatorCellView;
@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, strong) id<FitnessFoodCellDelegate> delegate;

- (void)setContentWithDate:(NSDate*)date;

@end
