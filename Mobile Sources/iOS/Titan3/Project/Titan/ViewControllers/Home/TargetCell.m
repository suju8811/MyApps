//
//  TargetCell.m
//  Titan
//
//  Created by Manuel Manzanera on 10/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TargetCell.h"

#import "AppContext.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation TargetCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self) {
    
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:10], WIDTH - [UtilManager width:20], [UtilManager height:200])];
        
        [containerView setBackgroundColor:[UIColor blackColor]];
        
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:5], [UtilManager width:140], [UtilManager height:25])];
        [_dateLabel setTextColor:[UIColor whiteColor]];
        [_dateLabel setFont:[UIFont fontWithName:REGULAR_FONT size:24]];
        [_dateLabel setText:@"10 Octubre 2017"];
        
        [containerView addSubview:_dateLabel];
        
        
        [[self contentView] addSubview:containerView];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
