//
//  HomeViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "HomeViewController.h"
#import "UIImageView+AFNetworking.h"
#import "FitnessFoodCell.h"
#import "LogDetailViewController.h"
#import "TargetCell.h"
#import "ProgressCell.h"
#import "DateManager.h"
#import "Log.h"
#import "HomeExecutive.h"
#import "TrainingPlanDetailViewController.h"
#import "NutritionPlanDetailViewController.h"
#import "ReposeViewController.h"
#import "OptionPopupViewController.h"
#import "SelectorFoodViewController.h"
#import "SelectorTableViewController.h"
#import "EventViewController.h"
#import "EventLatestCell.h"
#import "WorkoutDetailViewController.h"

#define EVENT_VIEW_STORYBOARD_ID            @"EventViewStoryboardID"

@interface HomeViewController ()<UITableViewDelegate, UITableViewDataSource, FitnessFoodCellDelegate, HomeDisplay, SelectorFoodViewDelegate, SelectorTableViewDelegate, EventLatestCellDelegate>

@property (nonatomic, strong) UITableView *homeTableView;

@property (nonatomic, strong) HomeExecutive * executive;

@property (nonatomic, strong) Event * todayEvent;
@property (nonatomic, strong) NSArray<Event*> * nextEvents;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    
    _executive = [[HomeExecutive alloc] initWithDisplay:self];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NAV_RIGHT1_ACTION object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPressMessageButton) name:NAV_RIGHT1_ACTION object:nil];
    
    _todayEvent = [Event getEventWithScheduleDate:[NSDate date]];
    _nextEvents = [Event getLatestEvents:4];
    
    [_homeTableView reloadData];
}

- (void)configureView{
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _homeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., NAV_HEIGHT, WIDTH,HEIGHT - NAV_HEIGHT) style:UITableViewStylePlain];
    _homeTableView.delegate = self;
    _homeTableView.dataSource = self;
    [_homeTableView registerClass:[FitnessFoodCell class] forCellReuseIdentifier:@"trainFoodCell"];
    [_homeTableView registerClass:[ProgressCell class] forCellReuseIdentifier:@"targetCell"];
    _homeTableView.backgroundColor = [UIColor clearColor];
    _homeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(rightSwipe)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    
    [_homeTableView addGestureRecognizer:recognizer];
    
    [self.view addSubview:_homeTableView];
}
    
- (void)rightSwipe {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTIF_MENU_SLIDE" object:nil];
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    
    if(indexPath.row == 1){
        LogDetailViewController *logDetailViewController = [[LogDetailViewController alloc] init];
        
        Log *currentLog = [Log getLastLog];
        [logDetailViewController setSelectLog:currentLog];
    
        [self.navigationController pushViewController:logDetailViewController animated:YES];
    }
    
}

#pragma mark - Actions

- (void)didPressMessageButton {
    UIStoryboard * chatStoryboard = [UIStoryboard storyboardWithName:@"Content" bundle:nil];
    UITabBarController * chatTabBarController = [chatStoryboard instantiateViewControllerWithIdentifier:@"TabController"];
    
    [self.navigationController pushViewController:chatTabBarController animated:YES];
}

#pragma mark UITableViewDataSource Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0) {
        FitnessFoodCell *cell = [tableView dequeueReusableCellWithIdentifier:@"trainFoodCell"];
        cell.currentIndex = indexPath.row;
        cell.delegate = self;
        [cell setContentWithDate:[NSDate date]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    else if (indexPath.row == 1) {
        ProgressCell *progressCell = [tableView dequeueReusableCellWithIdentifier:@"targetCell"];
        
        Log *currentLog = [Log getLastLog];
        
        if (currentLog) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
            
            NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
            NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute;
            
            NSDateComponents *components = [[NSCalendar currentCalendar] components:units fromDate:currentLog.revisionDate];
            
            [[progressCell dateLabel] setText:[NSString stringWithFormat:@"%ld %@ %ld",components.day,[DateManager monthWithInteger:components.month],components.year]];
            [[progressCell weightLabel] setText:[NSString stringWithFormat:@"%d Kg",currentLog.weight.intValue]];
            [[progressCell percentLabel] setText:[NSString stringWithFormat:@"%d %%",currentLog.igc.intValue]];
            
            __weak UIImageView *progressHomeImageView = progressCell.homeImageView;
            
            [[progressCell homeImageView] setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture1]]] placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                
                [progressHomeImageView setImage:image];
                [[progressHomeImageView layer] setMasksToBounds:YES];
                [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFill];
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                
                [progressHomeImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
                [[progressHomeImageView layer] setMasksToBounds:YES];
                [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
            
            }];
            
            __weak UIImageView *firstImageView = progressCell.exercise1ImageView;
            
            [[progressCell exercise1ImageView] setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture2]]] placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                
                [firstImageView setImage:image];
                [[firstImageView layer] setMasksToBounds:YES];
                [firstImageView setContentMode:UIViewContentModeScaleAspectFill];
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                [firstImageView setImage:nil];
            }];
            
            __weak UIImageView *secondImageView = progressCell.exercise2ImageView;
            
            [[progressCell exercise2ImageView] setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture3]]] placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                
                [secondImageView setImage:image];
                [[secondImageView layer] setMasksToBounds:YES];
                [secondImageView setContentMode:UIViewContentModeScaleAspectFill];
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                [secondImageView setImage:nil];
            }];
            
            __weak UIImageView *thirdImageView = progressCell.exercise3ImageView;
            
            [[progressCell exercise3ImageView] setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture3]]] placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                
                [thirdImageView setImage:image];
                [[thirdImageView layer] setMasksToBounds:YES];
                [thirdImageView setContentMode:UIViewContentModeScaleAspectFill];
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                [thirdImageView setImage:nil];
            }];
            
            __weak UIImageView *fourImageView = progressCell.exercise4ImageView;
            
            [[progressCell exercise4ImageView] setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture4]]] placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                
                [fourImageView setImage:image];
                [[fourImageView layer] setMasksToBounds:YES];
                [fourImageView setContentMode:UIViewContentModeScaleAspectFill];
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                [fourImageView setImage:nil];
            }];
        }
        
        [progressCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return progressCell;
    }
    else {
        EventLatestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventLatestCell"];
        if (!cell) {
            [tableView registerNib:[UINib nibWithNibName:@"EventLatestCell" bundle:nil] forCellReuseIdentifier:@"EventLatestCell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"EventLatestCell"];
        }
        [cell setContentWithTodayEvent:_todayEvent
                         andNextEvents:_nextEvents];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:
            return [UtilManager height:400];
            break;
        case 1:
            return [UtilManager height:170];
            break;
        default:
            return UITableViewAutomaticDimension;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            return [UtilManager height:400];
            break;
        case 1:
            return [UtilManager height:170];
            break;
        default:
            return [UtilManager height:144];
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return [UtilManager height:10];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return [UtilManager height:10];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:10])];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    return headerView;
}

#pragma mark - FitnessFoodCellDelegate

- (void)fitnessFoodCell:(FitnessFoodCell *)cell didPressTrainingAtIndex:(NSInteger)index {
    [_executive didPressTrainingPlanOfFitnessCell:[NSDate date]];
}

- (void)fitnessFoodCell:(FitnessFoodCell *)cell didPressAddTrainingPlanAtIndex:(NSInteger)index {
    SelectorTableViewController * selectorTableViewController = [[SelectorTableViewController alloc] init];
    selectorTableViewController.delegate = self;
    selectorTableViewController.isOnlyOneSelector = YES;
    selectorTableViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    selectorTableViewController.selectorType = SelectorTypeTrainingPlan;
    [self presentViewController:selectorTableViewController animated:YES completion:nil];
}

- (void)fitnessFoodCell:(FitnessFoodCell *)cell didPressAddReposeAtIndex:(NSInteger)index {
    UIStoryboard * scheduleStoryboard = [UIStoryboard storyboardWithName:@"Schedule" bundle:nil];
    ReposeViewController * reposeViewController = [scheduleStoryboard instantiateInitialViewController];
    reposeViewController.selectedDate = [NSDate date];
    reposeViewController.repose = nil;
    [self.navigationController pushViewController:reposeViewController animated:YES];
}

- (void)fitnessFoodCell:(FitnessFoodCell *)cell didPressNutritionPlanAtIndex:(NSInteger)index {
    [_executive didPressNutritionPlanOfFitnessCell:[NSDate date]];
}

- (void)fitnessFoodCell:(FitnessFoodCell *)cell didPressAddNutritionPlanAtIndex:(NSInteger)index {
    SelectorFoodViewController * selectorFoodViewController = [[SelectorFoodViewController alloc] init];
    selectorFoodViewController.delegate = self;
    selectorFoodViewController.isOnlyOneSelector = YES;
    selectorFoodViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    selectorFoodViewController.selectorType = SelectorFoodTypeNutritionPlanCalendar;
    [self presentViewController:selectorFoodViewController animated:YES completion:nil];
}

- (void)fitnessFoodCell:(FitnessFoodCell *)cell didPressShowTrainingAtIndex:(NSInteger)index {
    [_executive didPressShowTrainingOfFitnessCell:[NSDate date]];
}


#pragma mark - Selector Table View Delegate

- (void)didSelectTrainingPlan:(TrainingPlan*)trainingPlan {
    [_executive saveTrainingPlan:trainingPlan
                          toDate:[NSDate date]];
    
}

- (void)didPressNutritionPlanForCalendar:(NutritionPlan*)nutritionPlan {
    [_executive saveNutritionPlan:nutritionPlan
                           toDate:[NSDate date]];
    
}

#pragma mark - HomeDisplay

- (void)presentTrainingPlanDetailView:(SchedulePlan *)schedulePlan {
    TrainingPlanDetailViewController *trainingDetailViewController = [[TrainingPlanDetailViewController alloc] init];
    trainingDetailViewController.trainingPlan = schedulePlan.plan;
    trainingDetailViewController.fromCalendar = 1;
    trainingDetailViewController.schedulePlan = schedulePlan;
    [self.navigationController pushViewController:trainingDetailViewController animated:YES];
}

- (void)presentNutritionPlanDetailView:(SchedulePlan*)schedulePlan {
    NutritionPlanDetailViewController * nutritionPlanDetailViewController = [[NutritionPlanDetailViewController alloc] init];
    nutritionPlanDetailViewController.currentNutritionPlan = schedulePlan.nutritionPlan;
    nutritionPlanDetailViewController.fromCalendar = 1;
    nutritionPlanDetailViewController.schedulePlan = schedulePlan;
    nutritionPlanDetailViewController.selectedDate = [NSDate date];
    [self.navigationController pushViewController:nutritionPlanDetailViewController animated:YES];
}

- (void)presentTrainingDetailView:(Training*)training ofPlan:(TrainingPlan*)trainingPlan {
    WorkoutDetailViewController *trainingDetailViewController = [[WorkoutDetailViewController alloc] init];
    trainingDetailViewController.currentTraining = training;
    trainingDetailViewController.currentTrainingPlan = trainingPlan;
    trainingDetailViewController.fromCalendar = YES;
    [self.navigationController pushViewController:trainingDetailViewController animated:YES];
}

- (void)presentReposeView:(TrainingPlan *)repose {
    UIStoryboard * scheduleStoryboard = [UIStoryboard storyboardWithName:@"Schedule" bundle:nil];
    ReposeViewController * reposeViewController = [scheduleStoryboard instantiateInitialViewController];
    reposeViewController.selectedDate = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:[NSDate date] options:0];
    reposeViewController.repose = repose;
    [self.navigationController pushViewController:reposeViewController animated:YES];
}

- (void)reloadHomeView {
    [_homeTableView reloadData];
}

#pragma mark - EventLatestCellDelegate

- (void)eventLatestCell:(EventLatestCell *)cell didPressAddEventAtIndex:(NSInteger)index {
    UIStoryboard * scheduleStoryboard = [UIStoryboard storyboardWithName:@"Schedule" bundle:nil];
    EventViewController * eventViewController = [scheduleStoryboard instantiateViewControllerWithIdentifier:EVENT_VIEW_STORYBOARD_ID];
    NSDate * today = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:[NSDate date] options:0];
    eventViewController.selectedDate = today;
    [self.navigationController pushViewController:eventViewController animated:YES];
}

@end
