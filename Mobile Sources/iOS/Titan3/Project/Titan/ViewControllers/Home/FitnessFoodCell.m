//
//  TrainFoodCell.m
//  Titan
//
//  Created by Manuel Manzanera on 9/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FitnessFoodCell.h"
#import "AppContext.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "CircleProgressBar.h"
#import "Schedule.h"
#import "DateTimeUtil.h"
#import "SchedulePlan.h"

@interface FitnessFoodCell()

@end

@implementation FitnessFoodCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self) {
        
        [self setBackgroundColor:[UIColor clearColor]];
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        
        /////// FITNESS /////////
        
        _trainView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:5], (WIDTH - [UtilManager width:30])/2, [UtilManager height:300])];
        [_trainView setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *fitnessContentImageView = [[UIImageView alloc] initWithFrame:_trainView.bounds];
        [fitnessContentImageView setImage:[UIImage imageNamed:@"img-home-cell"]];
        [fitnessContentImageView setContentMode:UIViewContentModeScaleToFill];
        [[fitnessContentImageView layer] setMasksToBounds:YES];
        
        [_trainView addSubview:fitnessContentImageView];
        
        UIView *separatorFitnessView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:4], fitnessContentImageView.frame.size.height/2, fitnessContentImageView.frame.size.width - [UtilManager width:8], 1)];
        [separatorFitnessView setBackgroundColor:BLACK_HEADER_APP_COLOR];
        [_trainView addSubview:separatorFitnessView];
        
        _fitnessHourLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:14], [UtilManager height:10], [UtilManager width:80], [UtilManager height:46])];
        [_fitnessHourLabel setFont:[UIFont fontWithName:LIGHT_FONT size:36]];
        [_fitnessHourLabel setAdjustsFontSizeToFitWidth:YES];
        [_fitnessHourLabel setTextColor:[UIColor whiteColor]];
        [_fitnessHourLabel setText:@"17:30"];
        
        [_trainView addSubview:_fitnessHourLabel];
        
        UIImageView *fitnessIconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(fitnessContentImageView.frame.size.width + fitnessContentImageView.frame.origin.x - [UtilManager width:50], _fitnessHourLabel.frame.origin.y, [UtilManager width:40] * 0.8, [UtilManager height:46] * 0.8)];
        [fitnessIconImageView setContentMode:UIViewContentModeScaleAspectFit];
        [fitnessIconImageView setImage:[UIImage imageNamed:@"icon-cell-fitness"]];
        [fitnessIconImageView setCenter:CGPointMake(fitnessIconImageView.center.x, _fitnessHourLabel.center.y)];
        
        [_trainView addSubview:fitnessIconImageView];
        
        _fitnessValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(_fitnessHourLabel.frame.origin.x, _fitnessHourLabel.frame.origin.y + _fitnessHourLabel.frame.size.height, (WIDTH - [UtilManager width:30])/2 - [UtilManager width:14], [UtilManager height:36])];
        [_fitnessValueLabel setNumberOfLines:2];
        [_fitnessValueLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [_fitnessValueLabel setTextColor:GRAY_REGISTER_FONT];
        [_fitnessValueLabel setTextColor:[UIColor whiteColor]];
        [_fitnessValueLabel setText:@"Piernas &\nAbdominales 2A"];
        [_fitnessValueLabel setAdjustsFontSizeToFitWidth:YES];
        [_fitnessValueLabel setBackgroundColor:[UIColor clearColor]];
        
        [_trainView addSubview:_fitnessValueLabel];
        
        UIButton * showTrainingButton = [UIButton buttonWithType:UIButtonTypeCustom];
        showTrainingButton.frame = CGRectMake(0,
                                              0,
                                              CGRectGetWidth(fitnessContentImageView.frame),
                                              CGRectGetMaxY(_fitnessValueLabel.frame));

        [showTrainingButton addTarget:self action:@selector(didPressShowTrainingButton:) forControlEvents:UIControlEventTouchUpInside];
        [_trainView addSubview:showTrainingButton];

        _clockImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_fitnessHourLabel.frame.origin.x,
                                                                        CGRectGetMaxY(_fitnessValueLabel.frame) + [UtilManager height:6],
                                                                        [UtilManager width:20] * 0.8,
                                                                        [UtilManager width:20] * 0.8)];
        
        _clockImageView.image = [UIImage imageNamed:@"icon-cell-clock"];
        
        [_trainView addSubview:_clockImageView];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(_clockImageView.frame.origin.x + _clockImageView.frame.size.width + [UtilManager width:5], _clockImageView.frame.origin.y, [UtilManager width:80], _clockImageView.frame.size.height)];
        [_timeLabel setText:@"52 Min"];
        [_timeLabel setTextColor:GRAY_REGISTER_FONT];
        [_timeLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:12]];
        
        [_trainView addSubview:_timeLabel];
        
        _kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_trainView.frame.size.width/2 + [UtilManager width:10], _timeLabel.frame.origin.y, [UtilManager width:20] * 0.8, [UtilManager width:20] * 0.8)];
        _kCalImageView.image = [UIImage imageNamed:@"icon-kcal-red"];
        
        [_trainView addSubview:_kCalImageView];
        
        _fitnessKCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(_kCalImageView.frame.origin.x + _kCalImageView.frame.size.width + [UtilManager width:5], _kCalImageView.frame.origin.y, [UtilManager width:80], _clockImageView.frame.size.height)];
        [_fitnessKCalLabel setText:@"426 kCal"];
        [_fitnessKCalLabel setTextColor:RED_APP_COLOR];
        [_fitnessKCalLabel setFont:_timeLabel.font];
        
        [_trainView addSubview:_fitnessKCalLabel];
        
        _addTrainingPlanImageView = [[UIImageView alloc] initWithFrame:_clockImageView.frame];
        _addTrainingPlanImageView.image = [UIImage imageNamed:@"icn-info-off"];
        _addTrainingPlanImageView.hidden = YES;
        [_trainView addSubview:_addTrainingPlanImageView];
        
        _addTrainingPlanLabel = [[UILabel alloc] initWithFrame:_timeLabel.frame];
        _addTrainingPlanLabel.text = @"Add plan";
        _addTrainingPlanLabel.textColor = GRAY_REGISTER_FONT;
        _addTrainingPlanLabel.font = [UIFont fontWithName:REGULAR_FONT_OPENSANS size:12];
        _addTrainingPlanLabel.hidden = YES;
        [_trainView addSubview:_addTrainingPlanLabel];
        
        _addTrainingPlanButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _addTrainingPlanButton.frame = CGRectMake(CGRectGetMinX(_addTrainingPlanImageView.frame),
                                                  CGRectGetMinY(_addTrainingPlanImageView.frame),
                                                  CGRectGetMaxX(_addTrainingPlanLabel.frame),
                                                  CGRectGetMaxY(_addTrainingPlanLabel.frame));
        
        _addTrainingPlanButton.hidden = YES;
        [_addTrainingPlanButton addTarget:self action:@selector(didPressAddTrainingPlanButton:) forControlEvents:UIControlEventTouchUpInside];
        [_trainView addSubview:_addTrainingPlanButton];
        
        _addReposeImageView = [[UIImageView alloc] initWithFrame:_kCalImageView.frame];
        _addReposeImageView.image = [UIImage imageNamed:@"icn-info-off"];
        _addReposeImageView.hidden = YES;
        [_trainView addSubview:_addReposeImageView];
        
        _addReposeLabel = [[UILabel alloc] initWithFrame:_fitnessKCalLabel.frame];
        _addReposeLabel.text = @"Reposo";
        _addReposeLabel.textColor = GRAY_REGISTER_FONT;
        _addReposeLabel.font = [UIFont fontWithName:REGULAR_FONT_OPENSANS size:12];
        _addReposeLabel.hidden = YES;
        [_trainView addSubview:_addReposeLabel];
        
        _addReposeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _addReposeButton.frame = CGRectMake(CGRectGetMinX(_addReposeImageView.frame),
                                            CGRectGetMinY(_addReposeImageView.frame),
                                            CGRectGetMaxX(_addReposeLabel.frame),
                                            CGRectGetMaxY(_addReposeLabel.frame));
        
        _addReposeButton.hidden = YES;
        [_addReposeButton addTarget:self action:@selector(didPressAddReposeButton:) forControlEvents:UIControlEventTouchUpInside];
        [_trainView addSubview:_addReposeButton];
        
        [separatorFitnessView setFrame:CGRectMake(separatorFitnessView.frame.origin.x, _kCalImageView.frame.origin.y + _kCalImageView.frame.size.height + [UtilManager height:15], separatorFitnessView.frame.size.width, 1)];
        
        //[[_dialChart LabelCenterCurrent] setBackgroundColor:BLACK_HEADER_APP_COLOR];
        //[_trainView addSubview:_dialChart];
        
        _fitnessCircleProgress = [[CircleProgressBar alloc]initWithFrame:CGRectMake(0., 0.,fitnessContentImageView.frame.size.width - [UtilManager width:40], fitnessContentImageView.frame.size.width - [UtilManager width:40])];
        [_fitnessCircleProgress setProgressBarProgressColor:YELLOW_APP_COLOR];
        [_fitnessCircleProgress setProgressBarTrackColor:[UIColor clearColor]];
        [_fitnessCircleProgress setProgressBarWidth:10];
        [_fitnessCircleProgress setProgress:0.0 animated:YES];
        [_fitnessCircleProgress setBackgroundColor:[UIColor clearColor]];
        [_fitnessCircleProgress setCenter:CGPointMake(fitnessContentImageView.center.x, _trainView.center.y + [UtilManager height:60])];
        [_fitnessCircleProgress setStartAngle:-90];
        
        // Hint View Customization
        /*
        [_fitnessCircleProgress setHintViewSpacing:(customized ? 10.0f : 0)];
        [_fitnessCircleProgress setHintViewBackgroundColor:(customized ? [UIColor colorWithWhite:1.000 alpha:0.800] : nil)];
        [_fitnessCircleProgress setHintTextFont:(customized ? [UIFont fontWithName:@"AvenirNextCondensed-Heavy" size:40.0f] : nil)];
        [_fitnessCircleProgress setHintTextColor:(customized ? [UIColor blackColor] : nil)];
        [_fitnessCircleProgress setHintTextGenerationBlock:(customized ? ^NSString *(CGFloat progress) {
            return [NSString stringWithFormat:@"%.0f / 255", progress * 255];
        } : nil)];
        
        [_fitnessCircleProgress setHintTextGenerationBlock:^NSString *(CGFloat progress) {
            return [NSString stringWithFormat:@"%.0f / 255", progress * 255];
        }];
        */
         
        [_trainView addSubview:_fitnessCircleProgress];
        
        _fitnessDayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:60], [UtilManager height:20])];
        [_fitnessDayLabel setText:NSLocalizedString(@"Día 0", nil)];
        [_fitnessDayLabel setCenter:CGPointMake(_fitnessCircleProgress.center.x, _fitnessCircleProgress.center.y - [UtilManager height:33])];
        [_fitnessDayLabel setTextAlignment:NSTextAlignmentCenter];
        [_fitnessDayLabel setTextColor:[UIColor whiteColor]];
        [_fitnessDayLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:15]];
        
        [_trainView addSubview:_fitnessDayLabel];
        
        UIButton * trainingViewButton = [UIButton buttonWithType:UIButtonTypeCustom];
        trainingViewButton.frame = CGRectMake(0,
                                              CGRectGetHeight(fitnessContentImageView.frame) / 2,
                                              fitnessContentImageView.frame.size.width,
                                              CGRectGetHeight(fitnessContentImageView.frame) / 2);
        
        [trainingViewButton addTarget:self action:@selector(didSelectTraining:) forControlEvents:UIControlEventTouchUpInside];
        [_trainView addSubview:trainingViewButton];
        
        [self.contentView addSubview:_trainView];
        
        //////// FOOD
        
        _foodView = [[UIView alloc] initWithFrame:CGRectMake(_trainView.frame.origin.x + _trainView.frame.size.width + [UtilManager width:10], _trainView.frame.origin.y, _trainView.frame.size.width, _trainView.frame.size.height)];
        [_foodView setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *foodContentImageView = [[UIImageView alloc] initWithFrame:_foodView.bounds];
        [foodContentImageView setImage:[UIImage imageNamed:@"img-home-cell"]];
        [foodContentImageView setContentMode:UIViewContentModeScaleToFill];
        [[foodContentImageView layer] setMasksToBounds:YES];
        
        [_foodView addSubview:foodContentImageView];
        
        UIView *separatorFoodView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:4], separatorFitnessView.frame.origin.y, foodContentImageView.frame.size.width - [UtilManager width:8], 1)];
        [separatorFoodView setBackgroundColor:BLACK_HEADER_APP_COLOR];
        [_foodView addSubview:separatorFoodView];
        
        _foodHourLabel = [[UILabel alloc] initWithFrame:CGRectMake(_fitnessHourLabel.frame.origin.x, _fitnessHourLabel.frame.origin.y, _fitnessHourLabel.frame.size.width, _fitnessHourLabel.frame.size.height)];
        [_foodHourLabel setFont:[UIFont fontWithName:LIGHT_FONT size:36]];
        [_foodHourLabel setAdjustsFontSizeToFitWidth:YES];
        [_foodHourLabel setTextColor:[UIColor whiteColor]];
        [_foodHourLabel setText:@"21:30"];
        
        [_foodView addSubview:_foodHourLabel];
        
        UIImageView *foodIconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(foodContentImageView.frame.size.width + foodContentImageView.frame.origin.x - [UtilManager width:50], fitnessIconImageView.frame.origin.y, fitnessIconImageView.frame.size.width, fitnessIconImageView.frame.size.height)];
        [foodIconImageView setContentMode:UIViewContentModeScaleAspectFit];
        [foodIconImageView setImage:[UIImage imageNamed:@"icon-bat"]];
        [foodIconImageView setCenter:CGPointMake(foodIconImageView.center.x, _foodHourLabel.center.y)];
        
        [_foodView addSubview:foodIconImageView];
        
        _foodValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(_foodHourLabel.frame.origin.x, _fitnessValueLabel.frame.origin.y, [UtilManager width:140], _fitnessValueLabel.frame.size.height)];
        [_foodValueLabel setNumberOfLines:2];
        [_foodValueLabel setFont:_fitnessValueLabel.font];
        [_foodValueLabel setTextColor:_fitnessValueLabel.textColor];
        [_foodValueLabel setText:@"Pollo &\nArroz al Curry"];
        [_foodValueLabel setAdjustsFontSizeToFitWidth:YES];
        [_foodValueLabel setBackgroundColor:[UIColor clearColor]];
        
        [_foodView addSubview:_foodValueLabel];
        
        _kCalFoodImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_kCalImageView.frame.origin.x, _kCalImageView.frame.origin.y, _kCalImageView.frame.size.width, _kCalImageView.frame.size.height)];
        [_kCalFoodImageView setImage:[UIImage imageNamed:@"icon-kcal-green"]];
        
        [_foodView addSubview:_kCalFoodImageView];
        
        _foodKCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(_fitnessKCalLabel.frame.origin.x, _fitnessKCalLabel.frame.origin.y, [UtilManager width:80], _clockImageView.frame.size.height)];
        [_foodKCalLabel setText:@"678 kCal"];
        [_foodKCalLabel setTextColor:GREEN_APP_COLOR];
        [_foodKCalLabel setFont:_fitnessKCalLabel.font];
        [_foodKCalLabel setCenter:CGPointMake(_foodKCalLabel.center.x, _kCalFoodImageView.center.y)];
        
        [_foodView addSubview:_foodKCalLabel];
        
        _foodCircleProgress = [[CircleProgressBar alloc]initWithFrame:CGRectMake(0., 0.,fitnessContentImageView.frame.size.width - [UtilManager width:40], fitnessContentImageView.frame.size.width - [UtilManager width:40])];
        [_foodCircleProgress setProgressBarProgressColor:YELLOW_APP_COLOR];
        [_foodCircleProgress setProgressBarTrackColor:[UIColor clearColor]];
        [_foodCircleProgress setProgressBarWidth:10];
        [_foodCircleProgress setProgress:0.0 animated:YES];
        [_foodCircleProgress setBackgroundColor:[UIColor clearColor]];
        [_foodCircleProgress setCenter:CGPointMake(foodContentImageView.center.x, _fitnessCircleProgress.center.y)];
        [_foodCircleProgress setStartAngle:-90];
        
        [_foodView addSubview:_foodCircleProgress];
        /*  PNCircleChart
        XMCircleTypeView
         */

        _foodDayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:60], [UtilManager height:20])];
        [_foodDayLabel setText:NSLocalizedString(@"Día 0", nil)];
        [_foodDayLabel setCenter:CGPointMake(_foodCircleProgress.center.x, _foodCircleProgress.center.y - [UtilManager height:33])];
        [_foodDayLabel setTextAlignment:NSTextAlignmentCenter];
        [_foodDayLabel setTextColor:[UIColor whiteColor]];
        [_foodDayLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:15]];
        
        [_foodView addSubview:_foodDayLabel];
        
        UIButton * nutritionPlanViewButton = [UIButton buttonWithType:UIButtonTypeCustom];
        nutritionPlanViewButton.frame = CGRectMake(CGRectGetMinX(foodContentImageView.frame),
                                              CGRectGetHeight(foodContentImageView.frame) / 2,
                                              foodContentImageView.frame.size.width,
                                              CGRectGetHeight(foodContentImageView.frame) / 2);
        
        [nutritionPlanViewButton addTarget:self action:@selector(didSelectNutritionPlan:) forControlEvents:UIControlEventTouchUpInside];
        [_foodView addSubview:nutritionPlanViewButton];
        
        _addNutritionPlanImageView = [[UIImageView alloc] initWithFrame:_kCalFoodImageView.frame];
        _addNutritionPlanImageView.image = [UIImage imageNamed:@"icn-info-off"];
        _addNutritionPlanImageView.hidden = YES;
        [_foodView addSubview:_addNutritionPlanImageView];
        
        _addNutritionPlanLabel = [[UILabel alloc] initWithFrame:_foodKCalLabel.frame];
        _addNutritionPlanLabel.text = @"Add plan";
        _addNutritionPlanLabel.textColor = GRAY_REGISTER_FONT;
        _addNutritionPlanLabel.font = [UIFont fontWithName:REGULAR_FONT_OPENSANS size:12];
        _addNutritionPlanLabel.hidden = YES;
        [_foodView addSubview:_addNutritionPlanLabel];
        
        _addNutritionPlanButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _addNutritionPlanButton.frame = CGRectMake(CGRectGetMinX(_addNutritionPlanImageView.frame),
                                            CGRectGetMinY(_addNutritionPlanImageView.frame),
                                            CGRectGetMaxX(_addNutritionPlanLabel.frame),
                                            CGRectGetMaxY(_addNutritionPlanLabel.frame));
        
        _addNutritionPlanButton.hidden = YES;
        [_addNutritionPlanButton addTarget:self action:@selector(didPressAddNutritionPlanButton:) forControlEvents:UIControlEventTouchUpInside];
        [_foodView addSubview:_addNutritionPlanButton];
        [[self contentView] addSubview:_foodView];
        
        ////// BALANCE /////
        
        UILabel *balanceLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20],
                                                                          CGRectGetHeight(foodContentImageView.frame) + [UtilManager height:20],
                                                                          WIDTH - [UtilManager width:40],
                                                                          [UtilManager height:30])];
        [balanceLabel setText:NSLocalizedString(@"BALANCE", nil)];
        [balanceLabel setTextColor:[UIColor whiteColor]];
        [balanceLabel setFont:[UIFont fontWithName:REGULAR_FONT size:18]];
        //[balanceLabel setFont:_fitnessHourLabel.font];
        
        [self.contentView addSubview:balanceLabel];
        
        UIView *balanceSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(balanceLabel.frame.origin.x, balanceLabel.frame.origin.y + balanceLabel.frame.size.height, balanceLabel.frame.size.width, 1)];
        [balanceSeparatorView setBackgroundColor:GRAY_REGISTER_FONT];
        [self.contentView addSubview:balanceSeparatorView];
        
        _diferencekCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(balanceLabel.frame.origin.x,  balanceSeparatorView.frame.origin.y + [UtilManager height:7], balanceLabel.frame.size.width + [UtilManager width:120], balanceLabel.frame.size.height)];
        [_diferencekCalLabel setFont:[UIFont fontWithName:LIGHT_FONT size:34]];
        [_diferencekCalLabel setText:@"+240"];
        [_diferencekCalLabel setTextColor:GREEN_APP_COLOR];
        [_diferencekCalLabel setHidden:TRUE];
        [_diferencekCalLabel setBackgroundColor:[UIColor clearColor]];
        [_diferencekCalLabel setTextAlignment:NSTextAlignmentCenter];
        [_diferencekCalLabel setCenter:CGPointMake(2 * WIDTH / 3, _diferencekCalLabel.center.y)];
        
        [self.contentView addSubview:_diferencekCalLabel];
        
        _balanceValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(balanceLabel.frame.origin.x, balanceSeparatorView.frame.origin.y + [UtilManager height:7],balanceLabel.frame.size.width,balanceLabel.frame.size.height)];
        [_balanceValueLabel setText:@"kCal"];
        [_balanceValueLabel setTextColor:GRAY_REGISTER_FONT];
        [_balanceValueLabel setFont:[UIFont fontWithName:LIGHT_FONT_OPENSANS size:34]];
        [_balanceValueLabel setUserInteractionEnabled:YES];
        
        
        [self.contentView addSubview:_balanceValueLabel];
        
        _balanceFitnessLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., _balanceValueLabel.frame.origin.y,[UtilManager width:120], _balanceValueLabel.frame.size.height)];
        [_balanceFitnessLabel setFont:[UIFont fontWithName:LIGHT_FONT size:34]];
        [_balanceFitnessLabel setTextAlignment:NSTextAlignmentCenter];
        [_balanceFitnessLabel setTextColor:RED_APP_COLOR];
        [_balanceFitnessLabel setCenter:CGPointMake(WIDTH/2, _balanceValueLabel.center.y)];
        [_balanceFitnessLabel setText:@"3540"];
        [_balanceFitnessLabel setUserInteractionEnabled:YES];
        
        
        [self.contentView addSubview:_balanceFitnessLabel];
        
        _balanceFoodLabel = [[UILabel alloc] initWithFrame:CGRectMake(balanceSeparatorView.frame.origin.x, _balanceValueLabel.frame.origin.y, balanceSeparatorView.frame.size.width, _balanceFitnessLabel.frame.size.height)];
        [_balanceFoodLabel setTextAlignment:NSTextAlignmentRight];
        [_balanceFoodLabel setTextColor:GREEN_APP_COLOR];
        [_balanceFoodLabel setFont:_balanceFitnessLabel.font];
        [_balanceFoodLabel setText:@"3780"];
        
        [self.contentView addSubview:_balanceFoodLabel];
        
        UIButton *differenceButton = [[UIButton alloc] initWithFrame:CGRectMake(balanceSeparatorView.frame.origin.x, balanceLabel.frame.origin.y, balanceSeparatorView.frame.size.width, _balanceFoodLabel.frame.origin.y + _balanceFoodLabel.frame.size.height)];
        [differenceButton addTarget:self action:@selector(diferenceAction) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView addSubview:differenceButton];
        
    }
    
    return self;
}

- (void)diferenceAction{
    if(_diferencekCalLabel.isHidden){
        [_balanceFoodLabel setText:@""];
        [_balanceFitnessLabel setText:@""];
        [_diferencekCalLabel setHidden:FALSE];
    } else {
        [_balanceFoodLabel setText:@"3780"];
        [_balanceFitnessLabel setText:@"3540"];
        [_diferencekCalLabel setHidden:TRUE];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)didSelectTraining:(UIButton*)sender {
    if ([_delegate respondsToSelector:@selector(fitnessFoodCell:didPressTrainingAtIndex:)]) {
        [_delegate fitnessFoodCell:self didPressTrainingAtIndex:_currentIndex];
    }
}

- (void)didSelectNutritionPlan:(UIButton*)sender {
    if ([_delegate respondsToSelector:@selector(fitnessFoodCell:didPressNutritionPlanAtIndex:)]) {
        [_delegate fitnessFoodCell:self didPressNutritionPlanAtIndex:_currentIndex];
    }
}

- (void)didPressAddTrainingPlanButton:(UIButton*)sender {
    if ([_delegate respondsToSelector:@selector(fitnessFoodCell:didPressAddTrainingPlanAtIndex:)]) {
        [_delegate fitnessFoodCell:self didPressAddTrainingPlanAtIndex:_currentIndex];
    }
}

- (void)didPressAddReposeButton:(UIButton*)sender {
    if ([_delegate respondsToSelector:@selector(fitnessFoodCell:didPressAddReposeAtIndex:)]) {
        [_delegate fitnessFoodCell:self didPressAddReposeAtIndex:_currentIndex];
    }
}

- (void)didPressAddNutritionPlanButton:(UIButton*)sender {
    if ([_delegate respondsToSelector:@selector(fitnessFoodCell:didPressAddNutritionPlanAtIndex:)]) {
        [_delegate fitnessFoodCell:self didPressAddNutritionPlanAtIndex:_currentIndex];
    }
}

- (void)setContentWithDate:(NSDate*)date {
    
    //
    // Manage training plan
    //
    
    NSDate * dayWithoutTime = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:date options:0];
    SchedulePlan * trainingSchedule = [SchedulePlan getScheduleTrainingPlanWithScheduleDate:dayWithoutTime];
    TrainingPlan * trainingPlan = [Schedule trainingPlanForDate:date];
    TrainingPlan * repose = [Schedule reposePlanForDate:date];
    
    //
    // Get training plan or repose for date
    //
    
    TrainingPlan * considerTrainingPlan;
    if (trainingPlan) {
        considerTrainingPlan = trainingPlan;
    }
    else if (repose) {
        considerTrainingPlan = repose;
    }
    
    //
    // Show values of trainig plan or repose accordingly
    //
    
    if (considerTrainingPlan == nil) {
        if ([Schedule checkHaveTraininigContentInDays:7 fromDate:date]) {
            _fitnessHourLabel.text = @"Hoy";
        }
        else {
            _fitnessHourLabel.text = @"periodo inactivo";
        }
        _fitnessValueLabel.text = @"Vaya!, parece que no tienes ningún plan de entrenamiento programado compañero, añade uno";
        
        //
        // Show 2 icons and hide duration and calories label
        //
        
        _addTrainingPlanImageView.hidden = NO;
        _addTrainingPlanLabel.hidden = NO;
        _addTrainingPlanButton.hidden = NO;
        
        _addReposeImageView.hidden = NO;
        _addReposeLabel.hidden = NO;
        _addReposeButton.hidden = NO;
        
        _clockImageView.hidden = YES;
        _timeLabel.hidden = YES;
        
        _kCalImageView.hidden = YES;
        _fitnessKCalLabel.hidden = YES;
    }
    else {
        NSDate * scheduleDate = trainingSchedule.scheduleDate;
        NSInteger offset = [DateTimeUtil daysBetween:scheduleDate and:dayWithoutTime];
        
        //
        // Show training day content
        //
        
        TrainingDayContent * trainingDayContent = [considerTrainingPlan.trainingDay[offset].trainingDayContent firstObject];
        Training * training = trainingDayContent.training;
        
        if (trainingDayContent.start == nil ||
            trainingDayContent.start.length == 0) {
            _fitnessHourLabel.text = @"Hoy";
        }
        else {
            NSDate * startDate = [DateTimeUtil dateFromString:trainingDayContent.start format:@"HH:mm:ss"];
            _fitnessHourLabel.text = [DateTimeUtil stringFromDate:startDate format:@"HH:mm"];
        }
        
        if (considerTrainingPlan.isRepose.boolValue) {
            _fitnessValueLabel.text = considerTrainingPlan.title;
        }
        else {
            _fitnessValueLabel.text = training.title;
        }
        
        //
        // Show progress
        //
        
        _fitnessDayLabel.text = [NSString stringWithFormat:@"Día %d", (int)offset + 1];
        CGFloat trainingProgress = 0;
        if (considerTrainingPlan.trainingDay.count > 0) {
            trainingProgress = (float)(offset + 1) / (float)considerTrainingPlan.trainingDay.count;
        }
        [_fitnessCircleProgress setProgress:trainingProgress animated:YES];
        
        //
        // Hide 2 icons and show duration and calories label
        //
        
        _addTrainingPlanImageView.hidden = YES;
        _addTrainingPlanLabel.hidden = YES;
        _addTrainingPlanButton.hidden = YES;
        
        _addReposeImageView.hidden = YES;
        _addReposeLabel.hidden = YES;
        _addReposeButton.hidden = YES;
    
        _clockImageView.hidden = NO;
        _timeLabel.hidden = NO;
        
        _kCalImageView.hidden = NO;
        _fitnessKCalLabel.hidden = NO;
    }
    
    //
    // Manage nutrition plan
    //
    
    SchedulePlan * nutritionSchedule = [SchedulePlan getScheduleNutritionPlanWithScheduleDate:dayWithoutTime];
    NutritionPlan * nutritionPlan = [Schedule nutritionPlanForDate:date];
    
    //
    // Show values of trainig plan or repose accordingly
    //
    
    if (nutritionPlan == nil) {
        if ([Schedule checkHaveNutritionContentInDays:7 fromDate:date]) {
            _foodHourLabel.text = @"Hoy";
        }
        else {
            _foodHourLabel.text = @"periodo inactivo";
        }
        _foodValueLabel.text = @"Vaya!, parece que no tienes ningún plan de entrenamiento programado compañero, añade uno";
        
        //
        // Show 2 icons and hide duration and calories label
        //
        
        _addNutritionPlanImageView.hidden = NO;
        _addNutritionPlanLabel.hidden = NO;
        _addNutritionPlanButton.hidden = NO;
        
        _kCalFoodImageView.hidden = YES;
        _foodKCalLabel.hidden = YES;
    }
    else {
        NSDate * scheduleDate = nutritionSchedule.scheduleDate;
        NSInteger offset = [DateTimeUtil daysBetween:scheduleDate and:dayWithoutTime];
        
        //
        // Show training day content
        //
        
        NutritionDay * nutritionDay = [nutritionPlan.nutritionDays objectAtIndex:offset];
        
        if (nutritionDay.start == nil ||
            nutritionDay.start.length == 0) {
            _foodHourLabel.text = @"Hoy";
        }
        else {
            NSDate * startDate = [DateTimeUtil dateFromString:nutritionDay.start format:@"HH:mm:ss"];
            _foodHourLabel.text = [DateTimeUtil stringFromDate:startDate format:@"HH:mm"];
        }
        
        _foodValueLabel.text = nutritionDay.diet.title;
        
        //
        // Show progress
        //
        
        _foodDayLabel.text = [NSString stringWithFormat:@"Día %d", (int)offset + 1];
        CGFloat nutritionProgress = 0;
        if (nutritionPlan.nutritionDays.count > 0) {
            nutritionProgress = (float)(offset + 1) / (float)nutritionPlan.nutritionDays.count;
        }
        [_foodCircleProgress setProgress:nutritionProgress animated:YES];
        
        //
        // Hide 2 icons and show duration and calories label
        //
        
        _addNutritionPlanImageView.hidden = YES;
        _addNutritionPlanLabel.hidden = YES;
        _addNutritionPlanButton.hidden = YES;
        
        _kCalFoodImageView.hidden = NO;
        _foodKCalLabel.hidden = NO;
    }
}

- (void)didPressShowTrainingButton:(UIButton*)sender {
    if ([_delegate respondsToSelector:@selector(fitnessFoodCell:didPressShowTrainingAtIndex:)]) {
        [_delegate fitnessFoodCell:self didPressShowTrainingAtIndex:_currentIndex];
    }
}

@end
