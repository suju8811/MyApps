//
//  OptionsSettingViewController.h
//  Titan
//
//  Created by Marcus Lee on 9/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ParentViewController.h"

@protocol OptionsSettingViewControllerDelegate <NSObject>

- (void)didSelectSettingItemAtIndex:(NSInteger)index;

@end

@interface OptionsSettingViewController : ParentViewController

@property (nonatomic, assign) id<OptionsSettingViewControllerDelegate> delegate;

@end
