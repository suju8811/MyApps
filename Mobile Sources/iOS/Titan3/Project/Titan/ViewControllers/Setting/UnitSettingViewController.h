//
//  UnitSettingViewController.h
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ParentViewController.h"

#define UNIT_SETTING_STORY_BOARD_ID     @"UnitSettingStoryboardID"

@interface UnitSettingViewController : ParentViewController

@end
