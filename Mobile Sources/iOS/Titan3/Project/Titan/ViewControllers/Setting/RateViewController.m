//
//  RateViewController.m
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RateViewController.h"
#import <HCSStarRatingView.h>
#import "RateExecutive.h"

@interface RateViewController () <RateDisplay>

@property (strong, nonatomic) IBOutlet HCSStarRatingView *starRatingView;

@property (strong, nonatomic) RateExecutive * executive;

@end

@implementation RateViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _executive = [[RateExecutive alloc] initWithDisplay:self];
}

#pragma mark - Actions

- (IBAction)didPressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didPressOKButton:(id)sender {
    [_executive didPressOKButton];
}

#pragma mark - RateDisplay

- (CGFloat)rateFromDisplay {
    return _starRatingView.value;
}

- (void)didRateApp {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
