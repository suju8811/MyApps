//
//  UnitSettingViewController.m
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "UnitSettingViewController.h"
#import "UnitSettingExecutive.h"

@interface UnitSettingViewController () <UnitSettingDisplay>

@property (nonatomic, strong) UnitSettingExecutive * executive;
@property (nonatomic, strong) UnitSetting * unitSetting;

//
// IBOutlets
//

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextField *weightTextField;
@property (strong, nonatomic) IBOutlet UITextField *heightTextField;
@property (strong, nonatomic) IBOutlet UITextField *tmbTextField;

@end

@implementation UnitSettingViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _executive = [[UnitSettingExecutive alloc] initWithDisplay:self];
    [_executive displayDidLoad];
}

#pragma mark - Actions

- (IBAction)didPressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didPressOKButton:(id)sender {
    [_executive didPressOKButton];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == _weightTextField) {
        [self presentWeightUnitPicker];
        return NO;
    }
    else if (textField == _heightTextField) {
        [self presentHeightUnitPicker];
        return NO;
    }
    else if (textField == _tmbTextField) {
        [self presentTMBUnitPicker];
        return NO;
    }
    
    return YES;
}

- (void)presentWeightUnitPicker {
    [self.view endEditing:YES];
    
    UIAlertController *weightController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"Seleccione un peso unidades", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *kgAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Kg", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        _unitSetting.weightUnit = 0;
        [self loadDisplay:_unitSetting];
    }];
    
    UIAlertAction *poundsAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Pounds", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        _unitSetting.weightUnit = 1;
        [self loadDisplay:_unitSetting];
        
    }];
    
    [weightController addAction:kgAction];
    [weightController addAction:poundsAction];
    
    [self presentViewController:weightController animated:YES completion:nil];
}

- (void)presentHeightUnitPicker {
    [self.view endEditing:YES];
    
    UIAlertController *heightController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"Seleccione un altura unidades", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cmAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cm", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        _unitSetting.heightUnit = 0;
        [self loadDisplay:_unitSetting];
    }];
    
    UIAlertAction *inchAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"inch", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        _unitSetting.heightUnit = 1;
        [self loadDisplay:_unitSetting];
    }];
    
    [heightController addAction:cmAction];
    [heightController addAction:inchAction];
    
    [self presentViewController:heightController animated:YES completion:nil];
}

- (void)presentTMBUnitPicker {
    [self.view endEditing:YES];
    
    UIAlertController *tmbController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"Seleccione un tasa metabólica basal)", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *harrisAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Harris-Benedict", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        _unitSetting.tmbUnit = 0;
        [self loadDisplay:_unitSetting];
    }];
    
    UIAlertAction *katchAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Katch Mcardle", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        _unitSetting.tmbUnit = 1;
        [self loadDisplay:_unitSetting];
    }];
    
    UIAlertAction *faoAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"FAO-WHO-UNU", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        _unitSetting.tmbUnit = 2;
        [self loadDisplay:_unitSetting];
    }];
    
    [tmbController addAction:harrisAction];
    [tmbController addAction:katchAction];
    [tmbController addAction:faoAction];
    
    [self presentViewController:tmbController animated:YES completion:nil];
}

#pragma mark - UnitSettingDisplay

- (void)loadDisplay:(UnitSetting*)unitSetting {
    _unitSetting = unitSetting;
    
    switch (_unitSetting.weightUnit) {
        case 0:
            _weightTextField.text = @"kg";
            break;
        case 1:
            _weightTextField.text = @"pounds";
            break;
        default:
            break;
    }
    
    switch (_unitSetting.heightUnit) {
        case 0:
            _heightTextField.text = @"cm";
            break;
        case 1:
            _heightTextField.text = @"inch";
            break;
        default:
            break;
    }
    
    switch (_unitSetting.tmbUnit) {
        case 0:
            _tmbTextField.text = @"Harris-Benedict";
            break;
        case 1:
            _tmbTextField.text = @"Katch Mcardle";
            break;
        case 2:
            _tmbTextField.text = @"FAO-WHO-UNU";
            break;
        default:
            break;
    }
}

- (UnitSetting*)unitSettingFromDisplay {
    return _unitSetting;
}

- (void)didSaveUnitSetting {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
