//
//  SuggestionViewController.m
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SuggestionViewController.h"
#import "UploadMediaView.h"
#import "Suggestion.h"
#import "SuggestionExecutive.h"

@interface SuggestionViewController () <UploadMediaViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, SuggestionDisplay>

@property (nonatomic, strong) UploadMediaView *uploadMediaView;
@property (nonatomic, strong) UIImagePickerController *picker;
@property (nonatomic, strong) Suggestion * suggestion;

@property (strong, nonatomic) IBOutlet UITextField *suggstionOrDoubtTextField;
@property (strong, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) IBOutlet UITextView *descTextView;
@property (strong, nonatomic) IBOutlet UIImageView *uploadedImageView;
@property (nonatomic, strong) SuggestionExecutive * executive;

@end

@implementation SuggestionViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _titleTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Titulo", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName:[UIColor grayColor],
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]
                                      }
     ];
     
    _descTextView.layer.borderWidth = 1;
    _descTextView.layer.borderColor = BLACK_HEADER_APP_COLOR.CGColor;
    _descTextView.text = NSLocalizedString(@"Descripción", nil);
    _descTextView.backgroundColor = [UIColor clearColor];
    
    _uploadedImageView.layer.borderWidth = 1;
    _uploadedImageView.layer.borderColor = YELLOW_APP_COLOR.CGColor;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentImagePickView)];
    [_uploadedImageView addGestureRecognizer:tapGesture];
    
    _executive = [[SuggestionExecutive alloc] initWithDisplay:self];
    [_executive displayDidLoad];
}

#pragma mark - Actions

- (IBAction)didPressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didPressOKButton:(id)sender {
    [_executive didPressOKButton];
}

- (void)presentImagePickView{
    if(_uploadMediaView)
        _uploadMediaView = nil;
    _uploadMediaView = [[UploadMediaView alloc] initWithFrame:self.view.bounds];
    [_uploadMediaView setDelegate:self];
    //[_uploadMediaView setAccessibilityLabel:tapGesture.accessibilityLabel];
    [self.view addSubview:_uploadMediaView];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == _suggstionOrDoubtTextField) {
        [self presentSuggestionOrDoubtPicker];
        return NO;
    }
    
    return YES;
}

- (void)presentSuggestionOrDoubtPicker {
    [self.view endEditing:YES];
    
    UIAlertController * suggestionController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"Seleccione un doubt / suggestion", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *doubtAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Doubt", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        _suggstionOrDoubtTextField.text = @"Doubt";
        _suggestion.suggstionOrDoubt = 0;
    }];
    
    UIAlertAction *suggestionAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Suggestion", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        _suggstionOrDoubtTextField.text = @"Suggestion";
        _suggestion.suggstionOrDoubt = 1;
    }];
    
    [suggestionController addAction:doubtAction];
    [suggestionController addAction:suggestionAction];
    
    [self presentViewController:suggestionController animated:YES completion:nil];
}

#pragma mark UITextViewDelegate Methods

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
    return true;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    if([[textView text] isEqualToString:NSLocalizedString(@"Descripción", nil)]) {
        [_descTextView setText:@""];
    }
    
    return  true;
}

#pragma mark - UploadMediaViewDelegate

- (void)galleryDidselected {
    _picker = [[UIImagePickerController alloc] init];
    [_picker setAccessibilityLabel:_uploadMediaView.accessibilityLabel];
    [_uploadMediaView removeFromSuperview];
    [_picker setDelegate:self];
    [_picker setAllowsEditing:YES];
    [_picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:_picker animated:YES completion:nil];
}

- (void)cameraDidSelected {
    _picker = [[UIImagePickerController alloc] init];
    [_picker setAccessibilityLabel:_uploadMediaView.accessibilityLabel];
    [_uploadMediaView removeFromSuperview];
    [_picker setAllowsEditing:YES];
    [_picker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [_picker setDelegate:self];
    
    [self presentViewController:_picker animated:YES completion:nil];
}

- (void)dismissViewSelected {
    [_uploadMediaView removeFromSuperview];
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    _suggestion.image = [info objectForKey:UIImagePickerControllerEditedImage];
    _uploadedImageView.image = _suggestion.image;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - SuggestionDisplay

- (void)loadDisplay:(Suggestion*)suggestion {
    _suggestion = suggestion;
    if (_suggestion.suggstionOrDoubt == 0) {
        _suggstionOrDoubtTextField.text = @"Doubt";
    }
    else {
        _suggstionOrDoubtTextField.text = @"Suggestion";
    }
    
    _titleTextField.text = _suggestion.title;
    if (_suggestion.desc.length > 0) {
        _descTextView.text = _suggestion.desc;
    }
    
    _uploadedImageView.image = _suggestion.image;
}

- (Suggestion*)suggestionFromDisplay {
    _suggestion.title = _titleTextField.text;
    _suggestion.desc = _descTextView.text;
    return _suggestion;
}

- (void)didSuggestionSubmitted {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
