//
//  NotificationSettingViewController.h
//  Titan
//
//  Created by Marcus Lee on 11/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ParentViewController.h"

#define NOTIFICATION_SETTING_STORYBOARD_ID @"NotificationSettingStoryboardID"

@interface NotificationSettingViewController : ParentViewController

@end
