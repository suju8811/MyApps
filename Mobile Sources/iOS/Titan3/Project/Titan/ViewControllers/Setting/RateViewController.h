//
//  RateViewController.h
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

#define RATE_APP_VIEW_STORY_BOARD_ID @"RateAppStoryboardID"

@interface RateViewController : ParentViewController

@end
