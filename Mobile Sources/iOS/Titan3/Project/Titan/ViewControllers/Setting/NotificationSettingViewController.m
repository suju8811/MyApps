
#import "NotificationSettingViewController.h"
#import "NotificationSettingExecutive.h"

@interface NotificationSettingViewController () <NotificationSettingDisplay>

@property (nonatomic, strong) NotificationSettingExecutive * executive;

//
//  IBOutlet
//

@property (strong, nonatomic) IBOutlet UISwitch *generalSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *soundSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *trainingInformativeSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *trainingReminderSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *trainingAlertSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *nutritionReminderSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *nutritionAlertSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *communitySwitch;

@end

@implementation NotificationSettingViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _executive = [[NotificationSettingExecutive alloc] initWithDisplay:self];
    [_executive displayDidLoad];
}

#pragma mark  - Actions

- (IBAction)didPressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didPressOKButton:(id)sender {
    [_executive didPressOKButton];
}

#pragma mark - NotificationSettingDisplay

- (void)loadDisplay:(NotificationSetting*)notificationSetting {
    _generalSwitch.on = notificationSetting.general;
    _soundSwitch.on = notificationSetting.sound;
    
    _trainingInformativeSwitch.on = notificationSetting.trainingInformative;
    _trainingReminderSwitch.on = notificationSetting.trainingReminder;
    _trainingAlertSwitch.on = notificationSetting.trainingAlerts;
    
    _nutritionReminderSwitch.on = notificationSetting.nutritionReminder;
    _nutritionAlertSwitch.on = notificationSetting.nutritionAlerts;
    
    _communitySwitch.on = notificationSetting.community;
}

- (NotificationSetting*)notificationSettingFromDisplay {
    NotificationSetting * notificationSetting = [[NotificationSetting alloc] init];
    notificationSetting.general = _generalSwitch.on;
    notificationSetting.sound = _soundSwitch.on;
    
    notificationSetting.trainingInformative = _trainingInformativeSwitch.on;
    notificationSetting.trainingReminder = _trainingReminderSwitch.on;
    notificationSetting.trainingAlerts = _trainingAlertSwitch.on;
    
    notificationSetting.nutritionReminder = _nutritionReminderSwitch.on;
    notificationSetting.nutritionAlerts = _nutritionAlertSwitch.on;
    
    notificationSetting.community = _communitySwitch.on;
    
    return notificationSetting;
}

- (void)didSaveNotificationSetting {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
