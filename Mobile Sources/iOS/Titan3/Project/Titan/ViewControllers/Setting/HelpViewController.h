//
//  HelpViewController.h
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ParentViewController.h"

#define HELP_STORYBOARD_ID @"HelpStoryboardID"

@interface HelpViewController : ParentViewController

@end
