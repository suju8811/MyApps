//
//  SuggestionViewController.h
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ParentViewController.h"

#define SUGGESTION_STORY_BOARD_ID     @"SuggestionStoryboardID"

@interface SuggestionViewController : ParentViewController

@end
