//
//  OptionsSettingViewController.m
//  Titan
//
//  Created by Marcus Lee on 9/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "OptionsSettingViewController.h"
#import "OptionsSettingExecutive.h"
#import "OptionsCell.h"
#import "ProfileViewController.h"
#import "AccountViewController.h"
#import "RateViewController.h"
#import "NotificationSettingViewController.h"
#import "UnitSettingViewController.h"
#import "HelpViewController.h"
#import "SuggestionViewController.h"

@interface OptionsSettingViewController () <UITableViewDelegate, UITableViewDataSource, OptionsSettingDisplay>

@property (nonatomic, strong) UITableView *optionsTableView;
@property (nonatomic, strong) OptionsSettingExecutive *executive;

@end

@implementation OptionsSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    
    _executive = [[OptionsSettingExecutive alloc] initWithDisplay:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [_optionsTableView deselectRowAtIndexPath:[_optionsTableView indexPathForSelectedRow] animated:YES];
}

- (void)configureView{
    self.navigationController.navigationBarHidden = YES;
    self.view.backgroundColor = BLACK_APP_COLOR;
    //[self.view addSubview:[self headerView]];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _optionsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, HEIGHT) style:UITableViewStylePlain];
    
    [_optionsTableView setDelegate:self];
    [_optionsTableView setDataSource:self];
    [_optionsTableView registerClass:[OptionsCell class] forCellReuseIdentifier:@"optionCell"];
    [_optionsTableView setBackgroundColor:[UIColor clearColor]];
    [_optionsTableView setTableHeaderView:[self headerView]];
    [_optionsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.view addSubview:_optionsTableView];
    
    [self footerView];
}

- (UIView *)headerView{
    UIView *headerView  = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:100])];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:20], WIDTH - [UtilManager width:30], [UtilManager height:60])];
    [titleLabel setText:@"Ajuste"];
    
    [titleLabel setTextColor:YELLOW_APP_COLOR];
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (void)footerView{
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    [closeButton setTitle:NSLocalizedString(@"Cerrar", nil) forState:UIControlStateNormal];
    [[closeButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:19]];
    [closeButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:closeButton];
}

- (void)dismissView{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIStoryboard * settingStoryboard = [UIStoryboard storyboardWithName:@"Setting" bundle:nil];
    
    if (indexPath.row == 0) {
        ProfileViewController *profileViewController = [[ProfileViewController alloc] init];
        [profileViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        [self.navigationController pushViewController:profileViewController animated:YES];
    }
    else if (indexPath.row == 1) {
        AccountViewController *accountViewController = [[AccountViewController alloc] init];
        [accountViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        [self.navigationController pushViewController:accountViewController animated:YES];
    }
    else if (indexPath.row == 2) {
        NotificationSettingViewController * notificationSettingViewController = [settingStoryboard instantiateViewControllerWithIdentifier:NOTIFICATION_SETTING_STORYBOARD_ID];
        [self.navigationController pushViewController:notificationSettingViewController animated:YES];
    }
    else if (indexPath.row == 3) {
        UnitSettingViewController * unitSettingViewController = [settingStoryboard instantiateViewControllerWithIdentifier:UNIT_SETTING_STORY_BOARD_ID];
        [self.navigationController pushViewController:unitSettingViewController animated:YES];
    }
    else if (indexPath.row == 4) {
        HelpViewController * helpViewController = [settingStoryboard instantiateViewControllerWithIdentifier:HELP_STORYBOARD_ID];
        [self.navigationController pushViewController:helpViewController animated:YES];
    }
    else if (indexPath.row == 5) {
        NSString * message = @"Titan3 - World is the Training. \n\n https://itunes.apple.com/al/app/titan3/id912399912?mt=8";
        NSArray * arrayOfActivityItems = [NSArray arrayWithObjects:message, nil];
        UIActivityViewController * activityViewController = [[UIActivityViewController alloc] initWithActivityItems:arrayOfActivityItems applicationActivities:nil];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            activityViewController.modalPresentationStyle = UIModalPresentationPopover;
        }
        
        [self presentViewController:activityViewController animated:YES completion:nil];
    }
    else if (indexPath.row == 6) {
        RateViewController * rateViewController = [settingStoryboard instantiateViewControllerWithIdentifier:RATE_APP_VIEW_STORY_BOARD_ID];
        [self.navigationController pushViewController:rateViewController animated:YES];
    }
    else if (indexPath.row == 7) {
        SuggestionViewController * suggestionViewController = [settingStoryboard instantiateViewControllerWithIdentifier:SUGGESTION_STORY_BOARD_ID];
        [self.navigationController pushViewController:suggestionViewController animated:YES];
    }
    else if (indexPath.row == 8) {
        NSURL * url = [NSURL URLWithString:@"https://youtube.com"];
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 0.08 * HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 0.08 * HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OptionsCell *cell = [_optionsTableView dequeueReusableCellWithIdentifier:@"optionCell"];
    
    switch (indexPath.row) {
        case 0: // profile
            [[cell iconImageView] setImage:[UIImage imageNamed:@"icon-edit"]];
            [[cell titleLabel] setText:NSLocalizedString(@"Perfil", nil)];
            break;
        case 1: // account
            [[cell iconImageView] setImage:[UIImage imageNamed:@"icon-edit"]];
            [[cell titleLabel] setText:NSLocalizedString(@"Cuenta", nil)];
            break;
        case 2: // notifications
            [[cell iconImageView] setImage:[UIImage imageNamed:@"icon-edit"]];
            [[cell titleLabel] setText:NSLocalizedString(@"Notificaciones", nil)];
            break;
        case 3: // unit and formulas
            [[cell iconImageView] setImage:[UIImage imageNamed:@"icon-edit"]];
            [[cell titleLabel] setText:NSLocalizedString(@"Unidades y Fórmulas", nil)];
            break;
        case 4: // unit and formulas
            [[cell iconImageView] setImage:[UIImage imageNamed:@"icon-edit"]];
            [[cell titleLabel] setText:NSLocalizedString(@"Ayuda", nil)];
            break;
        case 5: // tell a friend
            [[cell iconImageView] setImage:[UIImage imageNamed:@"icon-edit"]];
            [[cell titleLabel] setText:NSLocalizedString(@"Contarle a un amigo", nil)];
            break;
        case 6: // rate TITAN
            [[cell iconImageView] setImage:[UIImage imageNamed:@"icon-edit"]];
            [[cell titleLabel] setText:NSLocalizedString(@"Valora TITAN", nil)];
            break;
        case 7: // Send directive message
            [[cell iconImageView] setImage:[UIImage imageNamed:@"icon-edit"]];
            [[cell titleLabel] setText:NSLocalizedString(@"Buzón dudas y sugerencias", nil)];
            break;
        case 8: // Tutorial
            [[cell iconImageView] setImage:[UIImage imageNamed:@"icon-edit"]];
            [[cell titleLabel] setText:NSLocalizedString(@"Tutorial", nil)];
            break;
        default:
            break;
    }
    
    return cell;
}

@end
