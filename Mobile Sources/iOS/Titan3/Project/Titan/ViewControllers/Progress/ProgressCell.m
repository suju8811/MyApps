//
//  ProgressCell.m
//  Titan
//
//  Created by Manuel Manzanera on 26/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ProgressCell.h"

#import "AppContext.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation ProgressCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self) {
    
        self.contentView.backgroundColor = UIColor.clearColor;
        self.backgroundColor = UIColor.clearColor;
        
        _containerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:15],
                                                                  [UtilManager height:5],
                                                                  WIDTH - [UtilManager width:30],
                                                                  [UtilManager height:160])];
        
        UIView *containerBackgroundView = [[UIView alloc] initWithFrame:_containerView.frame];
        containerBackgroundView.backgroundColor = UIColor.blackColor;
        containerBackgroundView.alpha = 0.65f;
        
        [self.contentView addSubview:containerBackgroundView];
        
        _containerView.backgroundColor = UIColor.clearColor;
        
        _siluetImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_containerView.frame.size.width - [UtilManager width:230],
                                                                         0,
                                                                         [UtilManager width:230],
                                                                         _containerView.frame.size.height)];
        
        _siluetImageView.contentMode = UIViewContentModeScaleAspectFit;
        if ([[AppContext sharedInstance].currentUser.gender isEqualToString:@"female"]) {
            _siluetImageView.image = [UIImage imageNamed:@"img-profile-woman"];
        }
        else {
            _siluetImageView.image = [UIImage imageNamed:@"img-prof-man"];
        }
        
        _siluetImageView.layer.masksToBounds = YES;
        [_containerView addSubview:_siluetImageView];
        
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:15],
                                                               [UtilManager height:10],
                                                               _containerView.frame.size.width * 0.6 - [UtilManager width:8],
                                                               [UtilManager height:20])];
        
        _dateLabel.text = @"10 OCTUBRE 2017";
        _dateLabel.font = [UIFont fontWithName:REGULAR_FONT size:21];
        _dateLabel.textColor = UIColor.whiteColor;
        _dateLabel.adjustsFontSizeToFitWidth = YES;
        
        [_containerView addSubview:_dateLabel];
        
        _homeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_dateLabel.frame.origin.x,
                                                                       _dateLabel.frame.origin.y + _dateLabel.frame.size.height + [UtilManager height:12],
                                                                       [UtilManager width:105],
                                                                       [UtilManager width:105])];
        
        _homeImageView.image = [UIImage imageNamed:@"img-placeholder-big"];
        [_containerView addSubview:_homeImageView];
        
        _exercise1ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_homeImageView.frame.origin.x + _homeImageView.frame.size.width + [UtilManager width:7],
                                                                            _homeImageView.frame.origin.y,
                                                                            [UtilManager width:50],
                                                                            [UtilManager width:50])];
        
        _exercise1ImageView.backgroundColor = UIColor.clearColor;
        _exercise1ImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [_containerView addSubview:_exercise1ImageView];
        
        _exercise2ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_exercise1ImageView.frame) + [UtilManager width:4],
                                                                            CGRectGetMinY(_exercise1ImageView.frame),
                                                                            CGRectGetWidth(_exercise1ImageView.frame),
                                                                            CGRectGetHeight(_exercise1ImageView.frame))];
        
        _exercise2ImageView.backgroundColor = UIColor.clearColor;
        _exercise2ImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [_containerView addSubview:_exercise2ImageView];
        
        _exercise3ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(_exercise1ImageView.frame),
                                                                            CGRectGetMaxY(_exercise1ImageView.frame) + [UtilManager height:4],
                                                                            CGRectGetWidth(_exercise1ImageView.frame),
                                                                            CGRectGetHeight(_exercise1ImageView.frame))];
        
        _exercise3ImageView.backgroundColor = UIColor.clearColor;
        _exercise3ImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [_containerView addSubview:_exercise3ImageView];
        
        _exercise4ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(_exercise2ImageView.frame),
                                                                            CGRectGetHeight(_exercise2ImageView.frame) + [UtilManager height:4],
                                                                            CGRectGetWidth(_exercise1ImageView.frame),
                                                                            CGRectGetHeight(_exercise1ImageView.frame))];
        
        _exercise4ImageView.backgroundColor = UIColor.clearColor;
        _exercise4ImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [_containerView addSubview:_exercise4ImageView];
        
        /*
        _exercise6ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_exercise3ImageView.frame.origin.x, _exercise3ImageView.frame.origin.y + _exercise3ImageView.frame.size.height + [UtilManager height:5], _exercise1ImageView.frame.size.width,_exercise1ImageView.frame.size.height)];
        [_exercise6ImageView setBackgroundColor:[UIColor clearColor]];
        
        [_containerView addSubview:_exercise6ImageView];
        */
        
        _weightLabel = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:120],
                                                                 [UtilManager height:30],
                                                                 [UtilManager height:60],
                                                                 [UtilManager height:60])];
        
        _weightLabel.textAlignment = NSTextAlignmentCenter;
        _weightLabel.textColor = BLUE_WEIGTH_PERCENT_CIRCLE;
        _weightLabel.layer.cornerRadius = _weightLabel.frame.size.width / 2;
        _weightLabel.layer.borderWidth = 1;
        _weightLabel.layer.borderColor = BLUE_WEIGTH_PERCENT_CIRCLE.CGColor;
        _weightLabel.text = @"93,5 Kg";
        _weightLabel.font = [UIFont fontWithName:LIGHT_FONT_OPENSANS size:15];
        _weightLabel.adjustsFontSizeToFitWidth = YES;
        
        [_containerView addSubview:_weightLabel];
        
        _percentLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_weightLabel.frame),
                                                                  CGRectGetMaxY(_weightLabel.frame) + [UtilManager height:10],
                                                                  [UtilManager height:45],
                                                                  [UtilManager height:45])];
        
        _percentLabel.textAlignment = NSTextAlignmentCenter;
        _percentLabel.textColor = RED_APP_COLOR;
        _percentLabel.layer.cornerRadius = _percentLabel.frame.size.width / 2;
        _percentLabel.layer.borderWidth = 1;
        _percentLabel.layer.borderColor = RED_APP_COLOR.CGColor;
        _percentLabel.text = @"13,45%";
        _percentLabel.font = [UIFont fontWithName:LIGHT_FONT_OPENSANS size:13];
        _percentLabel.adjustsFontSizeToFitWidth = YES;
        _percentLabel.center = CGPointMake(_weightLabel.center.x, _percentLabel.center.y);
        
        [_containerView addSubview:_percentLabel];
        
        _checkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50],
                                                                        0,
                                                                        [UtilManager width:18],
                                                                        [UtilManager width:18])];
        
        [self.contentView addSubview:_containerView];
        
        _checkImageView.image = [UIImage imageNamed:@"img-check"];
        _checkImageView.center = CGPointMake(_checkImageView.center.x, planCellHeight/2);
        _checkImageView.hidden = YES;
        [self.contentView addSubview:_checkImageView];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
