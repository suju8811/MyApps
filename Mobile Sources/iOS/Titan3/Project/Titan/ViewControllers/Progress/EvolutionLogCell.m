//
//  EvolutionLogCell.m
//  Titan
//
//  Created by Manuel Manzanera on 3/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "EvolutionLogCell.h"
#import "AppContext.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation EvolutionLogCell

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
        
        _containerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:5], frame.size.width - [UtilManager width:20], [UtilManager height:140])];
        [_containerView setBackgroundColor:[UIColor blackColor]];
        
        _homeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0.,[UtilManager height:140],[UtilManager height:140])];
        [_homeImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        
        [_containerView addSubview:_homeImageView];
        
        _exercise1ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_homeImageView.frame.origin.x + _homeImageView.frame.size.width + [UtilManager width:5], [UtilManager height:5], [UtilManager width:65],[UtilManager width:65] )];
        [_exercise1ImageView setBackgroundColor:[UIColor clearColor]];
        
        [_containerView addSubview:_exercise1ImageView];
        
        _exercise2ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_exercise1ImageView.frame.origin.x + _exercise1ImageView.frame.size.width + [UtilManager width:5], _exercise1ImageView.frame.origin.y, _exercise1ImageView.frame.size.width, _exercise1ImageView.frame.size.height)];
        [_exercise2ImageView setBackgroundColor:[UIColor clearColor]];
        
        [_containerView addSubview:_exercise2ImageView];
        
        //        _exercise3ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_exercise2ImageView.frame.origin.x + _exercise2ImageView.frame.size.width + [UtilManager width:5], _exercise1ImageView.frame.origin.y, _exercise1ImageView.frame.size.width, _exercise1ImageView.frame.size.height)];
        //        [_exercise3ImageView setBackgroundColor:[UIColor clearColor]];
        //
        //        [_containerView addSubview:_exercise3ImageView];
        
        _exercise3ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_exercise1ImageView.frame.origin.x, _exercise1ImageView.frame.origin.y + _exercise1ImageView.frame.size.height + [UtilManager height:5], _exercise1ImageView.frame.size.width,_exercise1ImageView.frame.size.height)];
        [_exercise3ImageView setBackgroundColor:[UIColor clearColor]];
        
        [_containerView addSubview:_exercise3ImageView];
        
        _exercise4ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_exercise2ImageView.frame.origin.x, _exercise2ImageView.frame.origin.y + _exercise2ImageView.frame.size.height + [UtilManager height:5], _exercise1ImageView.frame.size.width,_exercise1ImageView.frame.size.height)];
        [_exercise4ImageView setBackgroundColor:[UIColor clearColor]];
        
        [_containerView addSubview:_exercise4ImageView];
        
        [self addSubview:_containerView];
        
    }
    return self;
}

@end
