//
//  EvolutionPerformanceViewController.h
//  Titan
//
//  Created by Marcus Lee on 7/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EvolutionPerformanceExecutive.h"
#import "EvolutionViewController.h"

@interface EvolutionPerformanceViewController : EvolutionViewController

@property (nonatomic, strong) UIView * compareView;
@property (nonatomic, strong) UIImageView *siluetImageView;
@property (nonatomic, strong) UIView * separatorSecondView;

@end
