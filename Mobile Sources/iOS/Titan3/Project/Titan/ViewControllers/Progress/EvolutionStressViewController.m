//
//  EvolutionStressViewController.m
//  Titan
//
//  Created by Marcus Lee on 9/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "EvolutionStressViewController.h"
#import "ComparePicsViewController.h"
#import "UIImageView+AFNetworking.h"
#import "GalleryView.h"
#import "EvolutionLogCell.h"
#import "Log.h"
#import "DateTimeUtil.h"
#import "SchedulePlan.h"
#import "YellowBlackSwitchButton.h"
#import "EvolutionStressExecutive.h"
#import "CTPageViewController.h"
#import "SiluetStressImageViewController.h"

@interface EvolutionStressViewController () <GalleryDataSource, GalleryDelegate, UITextFieldDelegate, EvolutionStressDisplay, UIPageViewControllerDelegate, SiluetStressImageViewControllerDelegate>

@property (nonatomic, strong) GalleryView * galleryView;

@property (nonatomic, strong) UIImageView *firstLogImageView;
@property (nonatomic, strong) UIImageView *lastLogImageView;

@property (nonatomic, strong) RLMResults * logs;
@property (nonatomic, strong) RLMResults * plans;

///////
@property (nonatomic, strong) UILabel *weightMinLabel;
@property (nonatomic, strong) UILabel *weightMaxLabel;
@property (nonatomic, strong) UILabel *weightAveLabel;
@property (nonatomic, strong) UILabel *weightDifLabel;

@property (nonatomic, strong) UILabel *imgMinLabel;
@property (nonatomic, strong) UILabel *imgMaxLabel;
@property (nonatomic, strong) UILabel *imgAveLabel;
@property (nonatomic, strong) UILabel *imgDifLabel;

@property (nonatomic, strong) UILabel *bmrMinLabel;
@property (nonatomic, strong) UILabel *bmrMaxLabel;
@property (nonatomic, strong) UILabel *bmrAveLabel;
@property (nonatomic, strong) UILabel *bmrDifLabel;

//
// Siluet
//

@property (nonatomic, strong) UIImageView * foreamImageView;
@property (nonatomic, strong) CTPageViewController * siluetPageViewController;
@property (nonatomic, strong) SiluetStressImageViewController * siluetFrontImageViewController;
@property (nonatomic, strong) SiluetStressImageViewController * siluetBackImageViewController;

@end

@implementation EvolutionStressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(optionsMenu) name:NAV_RIGHT1_ACTION object:nil];
    
    self.executive = [[EvolutionStressExecutive alloc] initWithDisplay:self];
    [self.executive displayDidLoad];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NAV_RIGHT1_ACTION object:nil];
}

- (UIView *)headerView {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, WIDTH, NAV_HEIGHT)];
    headerView.backgroundColor = UIColor.clearColor;
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                      [UtilManager height:20],
                                                                      [UtilManager width:40]*0.8,
                                                                      [UtilManager width:40] * 0.8)];
    
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:backButton];
    
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50],
                                                                      [UtilManager height:20],
                                                                      [UtilManager width:40] * 0.8,
                                                                      [UtilManager width:40] * 0.8)];
    
    [menuButton setImage:[UIImage imageNamed:@"btn-menu-settings"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(menuAction) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:menuButton];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(backButton.frame),
                                                                    CGRectGetMinY(backButton.frame) + [UtilManager height:30],
                                                                    WIDTH - 2 * CGRectGetMaxX(backButton.frame),
                                                                    [UtilManager height:40])];
    
    titleLabel.text = NSLocalizedString(@"EVOLUCIÓN", nil);
    
    titleLabel.numberOfLines = 1;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = UIColor.whiteColor;
    titleLabel.font = [UIFont fontWithName:BOLD_FONT size:20];
    titleLabel.adjustsFontSizeToFitWidth = YES;
    
    titleLabel.center = CGPointMake(self.view.center.x, backButton.center.y);
    
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (void)menuAction {
}

- (void)configureCompareView {
    
    _galleryView = [[GalleryView alloc] initWithFrame:CGRectMake(0,
                                                                 [UtilManager height:310],
                                                                 WIDTH,
                                                                 [UtilManager height:180])];
    
    _galleryView.type = GalleryTypeLinear;
    _galleryView.delegate = self;
    _galleryView.dataSource = self;
    _galleryView.backgroundColor = UIColor.clearColor;
    
    //[self.scrollView addSubview:_galleryView];
    
    _firstLogImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.f,
                                                                       CGRectGetMaxY(self.chartView.frame) + [UtilManager height:10],
                                                                       [UtilManager height:140],
                                                                       [UtilManager height:140])];
    
    _firstLogImageView.center = CGPointMake(WIDTH/4, _firstLogImageView.center.y);
    _firstLogImageView.contentMode = UIViewContentModeScaleAspectFit;
    _firstLogImageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *firstImageGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didPressComparePicture:)];
    [_firstLogImageView addGestureRecognizer:firstImageGesture];
    [self.scrollView addSubview:_firstLogImageView];
    
    _lastLogImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.f,
                                                                      CGRectGetMinY(_firstLogImageView.frame),
                                                                      CGRectGetWidth(_firstLogImageView.frame),
                                                                      CGRectGetHeight(_firstLogImageView.frame))];
    
    _lastLogImageView.center = CGPointMake(WIDTH*3/4, _lastLogImageView.center.y);
    _lastLogImageView.contentMode = UIViewContentModeScaleAspectFit;
    _lastLogImageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *lastImageGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didPressComparePicture:)];
    [_lastLogImageView addGestureRecognizer:lastImageGesture];
    [self.scrollView addSubview:_lastLogImageView];
    
    _compareView = [[UIView alloc] initWithFrame:CGRectMake(0.f,
                                                            CGRectGetMaxY(_lastLogImageView.frame) + [UtilManager height:10],
                                                            WIDTH,
                                                            [UtilManager height:120])];
    
    UILabel *minLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:120],
                                                                  [UtilManager height:5],
                                                                  [UtilManager width:55],
                                                                  [UtilManager height:20])];
    
    minLabel.text = NSLocalizedString(@"Min", nil);
    minLabel.textColor = UIColor.whiteColor;
    minLabel.font = [UIFont fontWithName:REGULAR_FONT_OPENSANS size:15];
    minLabel.textAlignment = NSTextAlignmentCenter;
    
    [_compareView addSubview:minLabel];
    
    UIView *separatorMinTitleView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(minLabel.frame),
                                                                             CGRectGetMaxY(minLabel.frame) + 5,
                                                                             CGRectGetWidth(minLabel.frame) - [UtilManager width:25],
                                                                             1)];
    
    separatorMinTitleView.center = CGPointMake(minLabel.center.x, separatorMinTitleView.center.y);
    separatorMinTitleView.backgroundColor = UIColor.whiteColor;
    [_compareView addSubview:separatorMinTitleView];
    
    UILabel *maxLabel  = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(minLabel.frame) + [UtilManager width:10],
                                                                   CGRectGetMinY(minLabel.frame),
                                                                   CGRectGetWidth(minLabel.frame),
                                                                   CGRectGetHeight(minLabel.frame))];
    
    maxLabel.text = NSLocalizedString(@"Max", nil);
    maxLabel.textColor = minLabel.textColor;
    maxLabel.font = minLabel.font;
    maxLabel.textAlignment = NSTextAlignmentCenter;
    [_compareView addSubview:maxLabel];
    
    UIView *separatorMaxTitleView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(maxLabel.frame),
                                                                             CGRectGetMaxY(maxLabel.frame) + 5,
                                                                             CGRectGetWidth(separatorMinTitleView.frame),
                                                                             1)];
    
    separatorMaxTitleView.center = CGPointMake(maxLabel.center.x, separatorMaxTitleView.center.y);
    separatorMaxTitleView.backgroundColor = UIColor.whiteColor;
    [_compareView addSubview:separatorMaxTitleView];
    
    UILabel *aveLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(maxLabel.frame) + [UtilManager width:10],
                                                                  CGRectGetMinY(maxLabel.frame),
                                                                  CGRectGetWidth(maxLabel.frame),
                                                                  CGRectGetHeight(maxLabel.frame))];
    
    aveLabel.text = NSLocalizedString(@"Ave", nil);
    aveLabel.textColor = UIColor.whiteColor;
    aveLabel.font = minLabel.font;
    aveLabel.textAlignment = NSTextAlignmentCenter;
    [_compareView addSubview:aveLabel];
    
    UIView *separatorAveTitleView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(aveLabel.frame),
                                                                             CGRectGetMaxY(aveLabel.frame) + 5,
                                                                             CGRectGetWidth(separatorMaxTitleView.frame),
                                                                             1)];
    
    separatorAveTitleView.center = CGPointMake(aveLabel.center.x, separatorAveTitleView.center.y);
    separatorAveTitleView.backgroundColor = UIColor.whiteColor;
    [_compareView addSubview:separatorAveTitleView];
    
    UILabel *difLabel  = [[UILabel alloc]
                          initWithFrame:CGRectMake(CGRectGetMaxX(aveLabel.frame) + [UtilManager width:10],
                                                   CGRectGetMinY(minLabel.frame),
                                                   CGRectGetWidth(minLabel.frame),
                                                   CGRectGetHeight(minLabel.frame))];
    
    difLabel.text = NSLocalizedString(@"Dif", nil);
    difLabel.textColor = UIColor.whiteColor;
    difLabel.textAlignment = NSTextAlignmentCenter;
    difLabel.font = minLabel.font;
    [_compareView addSubview:difLabel];
    
    UIView *separatorDifTitleView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(difLabel.frame),
                                                                             CGRectGetMaxY(difLabel.frame) + 5,
                                                                             CGRectGetWidth(separatorMaxTitleView.frame),
                                                                             1)];
    
    separatorDifTitleView.center = CGPointMake(difLabel.center.x, separatorDifTitleView.center.y);
    separatorDifTitleView.backgroundColor = UIColor.whiteColor;
    [_compareView addSubview:separatorDifTitleView];
    
    UIView *separatorTitleView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                          CGRectGetMaxY(difLabel.frame) + 5,
                                                                          WIDTH - [UtilManager width:20],
                                                                          1)];
    
    separatorTitleView.backgroundColor = UIColor.whiteColor;
    
    //[compareView addSubview:separatorTitleView];
    
    UILabel *weightLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                     CGRectGetMaxY(separatorTitleView.frame),
                                                                     [UtilManager width:145],
                                                                     [UtilManager height:30])];
    
    weightLabel.text = NSLocalizedString(@"Weight (Kg)", nil);
    weightLabel.textColor = PNFreshGreen;
    weightLabel.font = minLabel.font;
    [_compareView addSubview:weightLabel];
    
    _weightMinLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(minLabel.frame),
                                                                CGRectGetMinY(weightLabel.frame),
                                                                CGRectGetWidth(minLabel.frame),
                                                                CGRectGetHeight(weightLabel.frame))];
    
    _weightMinLabel.textColor = GRAY_REGISTER_FONT;
    _weightMinLabel.font = minLabel.font;
    _weightMinLabel.textAlignment = NSTextAlignmentCenter;
    _weightMinLabel.adjustsFontSizeToFitWidth = YES;
    _weightMinLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer * minWeightTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didPressCompareValue:)];
    minWeightTapGesture.accessibilityLabel = @"minWeight";
    [_weightMinLabel addGestureRecognizer:minWeightTapGesture];
    [_compareView addSubview:_weightMinLabel];
    
    _weightMaxLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(maxLabel.frame),
                                                                CGRectGetMinY(weightLabel.frame),
                                                                CGRectGetWidth(minLabel.frame),
                                                                CGRectGetHeight(weightLabel.frame))];
    
    _weightMaxLabel.textColor = _weightMinLabel.textColor;
    _weightMaxLabel.font = minLabel.font;
    _weightMaxLabel.textAlignment = _weightMinLabel.textAlignment;
    _weightMaxLabel.adjustsFontSizeToFitWidth = YES;
    _weightMaxLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer * maxWeightTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didPressCompareValue:)];
    maxWeightTapGesture.accessibilityLabel = @"maxWeight";
    [_weightMaxLabel addGestureRecognizer:maxWeightTapGesture];
    
    [_compareView addSubview:_weightMaxLabel];
    
    _weightAveLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(aveLabel.frame),
                                                                CGRectGetMinY(weightLabel.frame),
                                                                CGRectGetWidth(minLabel.frame),
                                                                CGRectGetHeight(weightLabel.frame))];
    
    _weightAveLabel.textColor = _weightMinLabel.textColor;
    _weightAveLabel.font = minLabel.font;
    _weightAveLabel.textAlignment = _weightMinLabel.textAlignment;
    _weightAveLabel.adjustsFontSizeToFitWidth = YES;
    float aveWeightLog = [Log getAverageValueWithKey:@"weight"];
    _weightAveLabel.text = [NSString stringWithFormat:@"%.0f", aveWeightLog];
    [_compareView addSubview:_weightAveLabel];
    
    UIView *verticalSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_weightAveLabel.frame) + [UtilManager width:5],
                                                                             CGRectGetMinY(_weightAveLabel.frame) + [UtilManager height:10],
                                                                             1,
                                                                             [UtilManager height:80])];
    
    verticalSeparatorView.backgroundColor = UIColor.whiteColor;
    [_compareView addSubview:verticalSeparatorView];
    
    _weightDifLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(difLabel.frame),
                                                                CGRectGetMinY(weightLabel.frame),
                                                                CGRectGetWidth(minLabel.frame),
                                                                CGRectGetHeight(weightLabel.frame))];
    
    _weightDifLabel.textColor = _weightMinLabel.textColor;
    _weightDifLabel.font = minLabel.font;
    _weightDifLabel.textAlignment = _weightMinLabel.textAlignment;
    _weightDifLabel.adjustsFontSizeToFitWidth = YES;
    
    [_compareView addSubview:_weightDifLabel];
    
    UIView *separatorWeigthView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                           CGRectGetMaxY(_weightDifLabel.frame) + 5,
                                                                           WIDTH - [UtilManager width:20],
                                                                           1)];
    
    separatorWeigthView.backgroundColor = UIColor.whiteColor;
    
    //[compareView addSubview:separatorWeigthView];
    
    UILabel *imgLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                  CGRectGetMaxY(separatorWeigthView.frame),
                                                                  CGRectGetWidth(weightLabel.frame),
                                                                  CGRectGetHeight(weightLabel.frame))];
    
    imgLabel.text = NSLocalizedString(@"IGC (%)", nil);
    imgLabel.textColor = PNTwitterColor;
    imgLabel.font = minLabel.font;
    [_compareView addSubview:imgLabel];
    
    _imgMinLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(minLabel.frame),
                                                             CGRectGetMinY(imgLabel.frame),
                                                             CGRectGetWidth(minLabel.frame),
                                                             CGRectGetHeight(weightLabel.frame))];
    
    _imgMinLabel.textColor = _weightMinLabel.textColor;
    _imgMinLabel.font = minLabel.font;
    _imgMinLabel.textAlignment = _weightMinLabel.textAlignment;
    _imgMinLabel.adjustsFontSizeToFitWidth = YES;
    [_compareView addSubview:_imgMinLabel];
    
    _imgMaxLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(maxLabel.frame),
                                                             CGRectGetMinY(imgLabel.frame),
                                                             CGRectGetWidth(minLabel.frame),
                                                             CGRectGetHeight(weightLabel.frame))];
    
    _imgMaxLabel.textColor = _weightMinLabel.textColor;
    _imgMaxLabel.font = minLabel.font;
    _imgMaxLabel.textAlignment = _weightMinLabel.textAlignment;
    _imgMaxLabel.adjustsFontSizeToFitWidth = YES;
    [_compareView addSubview:_imgMaxLabel];
    
    _imgAveLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(aveLabel.frame),
                                                             CGRectGetMinY(imgLabel.frame),
                                                             CGRectGetWidth(minLabel.frame),
                                                             CGRectGetHeight(weightLabel.frame))];
    
    _imgAveLabel.textColor = _weightMinLabel.textColor;
    _imgAveLabel.font = minLabel.font;
    _imgAveLabel.textAlignment = _weightMinLabel.textAlignment;
    _imgAveLabel.adjustsFontSizeToFitWidth = YES;
    [_compareView addSubview:_imgAveLabel];
    
    _imgDifLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(difLabel.frame),
                                                             CGRectGetMinY(imgLabel.frame),
                                                             CGRectGetWidth(minLabel.frame),
                                                             CGRectGetHeight(weightLabel.frame))];
    
    _imgDifLabel.textColor = UIColor.whiteColor;
    _imgDifLabel.text = @"-";
    _imgDifLabel.font = minLabel.font;
    _imgDifLabel.textAlignment = _weightMinLabel.textAlignment;
    _imgDifLabel.adjustsFontSizeToFitWidth = YES;
    [_compareView addSubview:_imgDifLabel];
    
    UIView *separatorBMRView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                        CGRectGetMaxY(_imgDifLabel.frame) + 5,
                                                                        WIDTH - [UtilManager width:20],
                                                                        1)];
    
    separatorBMRView.backgroundColor = UIColor.whiteColor;
    //[compareView addSubview:separatorBMRView];
    
    UILabel *bmrLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                  CGRectGetMaxY(separatorBMRView.frame),
                                                                  CGRectGetWidth(weightLabel.frame),
                                                                  CGRectGetHeight(weightLabel.frame))];
    
    bmrLabel.text = NSLocalizedString(@"BMR (Kcal)", nil);
    bmrLabel.textColor = PNYellow;
    bmrLabel.font = minLabel.font;
    [_compareView addSubview:bmrLabel];
    
    _bmrMinLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(minLabel.frame),
                                                             CGRectGetMinY(bmrLabel.frame),
                                                             CGRectGetWidth(minLabel.frame),
                                                             CGRectGetHeight(weightLabel.frame))];
    
    _bmrMinLabel.textColor = _weightMinLabel.textColor;
    _bmrMinLabel.font = minLabel.font;
    _bmrMinLabel.textAlignment = _weightMinLabel.textAlignment;
    _bmrMinLabel.adjustsFontSizeToFitWidth = YES;
    [_compareView addSubview:_bmrMinLabel];
    
    _bmrMaxLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(maxLabel.frame),
                                                             CGRectGetMinY(bmrLabel.frame),
                                                             CGRectGetWidth(minLabel.frame),
                                                             CGRectGetHeight(weightLabel.frame))];
    
    _bmrMaxLabel.textColor = _weightMinLabel.textColor;
    _bmrMaxLabel.font = minLabel.font;
    _bmrMaxLabel.textAlignment = _weightMinLabel.textAlignment;
    _bmrMaxLabel.adjustsFontSizeToFitWidth = YES;
    [_compareView addSubview:_bmrMaxLabel];
    
    _bmrAveLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(aveLabel.frame),
                                                             CGRectGetMinY(bmrLabel.frame),
                                                             CGRectGetWidth(minLabel.frame),
                                                             CGRectGetHeight(weightLabel.frame))];
    
    _bmrAveLabel.text = @"-";
    _bmrAveLabel.textColor = _weightMinLabel.textColor;
    _bmrAveLabel.font = minLabel.font;
    _bmrAveLabel.textAlignment = _weightMinLabel.textAlignment;
    _bmrAveLabel.adjustsFontSizeToFitWidth = YES;
    [_compareView addSubview:_bmrAveLabel];
    
    _bmrDifLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(difLabel.frame),
                                                             CGRectGetMinY(bmrLabel.frame),
                                                             CGRectGetWidth(minLabel.frame),
                                                             CGRectGetHeight(weightLabel.frame))];
    
    _bmrDifLabel.textColor = UIColor.whiteColor;
    _bmrDifLabel.font = minLabel.font;
    _bmrDifLabel.textAlignment = _weightMinLabel.textAlignment;
    _bmrDifLabel.adjustsFontSizeToFitWidth = YES;
    
    [_compareView addSubview:_bmrDifLabel];
    [self.scrollView addSubview:_compareView];
}

- (void)configureSiluetView {
    
    _separatorSecondView = [[UIView alloc]
                            initWithFrame:CGRectMake([UtilManager width:10],
                                                     CGRectGetMaxY(_compareView.frame) + [UtilManager height:20],
                                                     WIDTH - [UtilManager width:20],
                                                     1)];
    
    [self.scrollView addSubview:_separatorSecondView];

    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Progress" bundle:nil];
    _siluetFrontImageViewController = [storyboard instantiateViewControllerWithIdentifier:@"SiluetStressImageStoryboardID"];
    _siluetFrontImageViewController.imageType = SiluetStressImageTypeFront;
    _siluetFrontImageViewController.delegate = self;
    _siluetBackImageViewController = [storyboard instantiateViewControllerWithIdentifier:@"SiluetStressImageStoryboardID"];
    _siluetBackImageViewController.delegate = self;
    _siluetBackImageViewController.imageType = SiluetStressImageTypeBack;
    
    _siluetPageViewController = [[CTPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    _siluetPageViewController.delegate = self;
    _siluetPageViewController.childViewControllers = @[_siluetFrontImageViewController, _siluetBackImageViewController];
    
    [self addChildViewController:_siluetPageViewController];
    UIView * siluetContentView = [[UIView alloc]
                        initWithFrame:CGRectMake(0.f,
                                                 CGRectGetMinY(_separatorSecondView.frame) + [UtilManager height:10],
                                                 WIDTH,
                                                 [UtilManager height:375])];
    
    [siluetContentView addSubview:_siluetPageViewController.view];
    [self.scrollView addSubview:siluetContentView];
    self.scrollView.contentSize = CGSizeMake(WIDTH, CGRectGetMaxY(siluetContentView.frame) + [UtilManager height:30]);
}

- (void)optionsMenu {
    
}

#pragma mark - Actions
- (void)didPressBackButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didPressComparePicture:(UITapGestureRecognizer*)sender {
    [((EvolutionStressExecutive*)self.executive) didPressComparePicture];
}

- (void)didPressCompareValue:(UITapGestureRecognizer*)sender {
    [((EvolutionStressExecutive*)self.executive) didPressCompareValue:sender.accessibilityLabel];
}

#pragma mark GalleryViewDataSource methods

- (NSUInteger)numberOfItemsInGallery:(GalleryView *)Gallery{
    return _logs.count;
}

- (NSUInteger)numberOfPlaceholdersInGallery:(GalleryView *)Gallery {
    return 0;
}

- (CGFloat)gallery:(GalleryView *)Gallery valueForOption:(GalleryOption)option withDefault:(CGFloat)value {
    switch (option){
        case GalleryOptionWrap:
            return true;
        case GalleryOptionFadeMin:
            return -0.2;
        case GalleryOptionFadeMax:
            return 0.2;
        case GalleryOptionFadeRange:
            return 1.5;
        default:
            break;
    }
    return value * 1.05;
}


- (UIView *)gallery:(GalleryView *)Gallery viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view{
    
    EvolutionLogCell *evolutionView = (EvolutionLogCell *)view;
    Log *currentLog = [_logs objectAtIndex:index];
    
    if(evolutionView == nil)
        evolutionView = [[EvolutionLogCell alloc]initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:10], [UtilManager width:280], [UtilManager height:140])];
    
    __weak UIImageView *progressHomeImageView = evolutionView.homeImageView;
    
    NSURLRequest *homeImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture1]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
    
    [[evolutionView homeImageView] setImageWithURLRequest:homeImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [progressHomeImageView setImage:image];
        [[progressHomeImageView layer] setMasksToBounds:YES];
        [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        
        [progressHomeImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        [[progressHomeImageView layer] setMasksToBounds:YES];
        [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    }];
    
    __weak UIImageView *firstImageView = evolutionView.exercise1ImageView;
    
    NSURLRequest *exercise1ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture2]]
                                                           cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                       timeoutInterval:60];
    
    [[evolutionView exercise1ImageView] setImageWithURLRequest:exercise1ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [firstImageView setImage:image];
        [[firstImageView layer] setMasksToBounds:YES];
        [firstImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        [firstImageView setImage:nil];
        [[firstImageView layer] setMasksToBounds:YES];
        [firstImageView setContentMode:UIViewContentModeScaleAspectFit];
    }];
    
    __weak UIImageView *secondImageView = evolutionView.exercise2ImageView;
    
    NSURLRequest *exercise2ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture3]]
                                                           cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                       timeoutInterval:60];
    
    [[evolutionView exercise2ImageView] setImageWithURLRequest:exercise2ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [secondImageView setImage:image];
        [[secondImageView layer] setMasksToBounds:YES];
        [secondImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        [secondImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        [[secondImageView layer] setMasksToBounds:YES];
        [secondImageView setContentMode:UIViewContentModeScaleAspectFit];
    }];
    
    __weak UIImageView *thirdImageView = evolutionView.exercise3ImageView;
    
    NSURLRequest *exercise3ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture4]]
                                                           cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                       timeoutInterval:60];
    
    [[evolutionView exercise3ImageView] setImageWithURLRequest:exercise3ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [thirdImageView setImage:image];
        [[thirdImageView layer] setMasksToBounds:YES];
        [thirdImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        [thirdImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        [[thirdImageView layer] setMasksToBounds:YES];
        [thirdImageView setContentMode:UIViewContentModeScaleAspectFit];
    }];
    
    __weak UIImageView *fourImageView = evolutionView.exercise4ImageView;
    
    NSURLRequest *exercise4ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture5]]
                                                           cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                       timeoutInterval:60];
    
    [[evolutionView exercise4ImageView] setImageWithURLRequest:exercise4ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [fourImageView setImage:image];
        [[fourImageView layer] setMasksToBounds:YES];
        [fourImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        [fourImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        [[fourImageView layer] setMasksToBounds:YES];
        [fourImageView setContentMode:UIViewContentModeScaleAspectFit];
    }];
    
    return evolutionView;
}

#pragma mark GalleryViewDelegate methods

- (void)gallery:(GalleryView *)Gallery didSelectItemAtIndex:(NSInteger)index {
}

- (void)galleryDidScroll:(GalleryView *)Gallery {
}

#pragma mark - EvolutionDisplay

- (void)configureDisplay {
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    
    [self.scrollView addSubview:[self headerView]];
    
    [self configureDateViews];
    [self configureSegmentControlView];
    [self configureChartView];
    [self configureCompareView];
    [self configureSiluetView];
    
    [self.view addSubview:self.scrollView];
}

- (void)loadDisplay:(EvolutionStressDisplayContext *)displayContext {
    _logs = displayContext.logs;
    self.segmentControl.selectedSegmentIndex = displayContext.segmentState;
    
    //
    // Show dates
    //
    
    [self loadDateViews:displayContext];
    
    //
    // Draw chart
    //
    
    //    [self drawChart:displayContext];
    
    //
    // Set first and last image view
    //
    
    __weak UIImageView *progressHomeImageView = _firstLogImageView;
    
    NSURLRequest *homeImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS, displayContext.minLogDate.picture1]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
    
    [_firstLogImageView setImageWithURLRequest:homeImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [progressHomeImageView setImage:image];
        [[progressHomeImageView layer] setMasksToBounds:YES];
        [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        
        [progressHomeImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        [[progressHomeImageView layer] setMasksToBounds:YES];
        [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    }];
    
    __weak UIImageView *lastImageView = _lastLogImageView;
    
    NSURLRequest *lastImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS, displayContext.maxLogDate.picture1]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
    
    [_lastLogImageView setImageWithURLRequest:lastImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [lastImageView setImage:image];
        [[lastImageView layer] setMasksToBounds:YES];
        [lastImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        [lastImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        [[lastImageView layer] setMasksToBounds:YES];
        [lastImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    }];
    
    [_galleryView reloadData];
    [self loadCompareView:displayContext];
}

- (void)didLoadDivisionPercentage:(DivisionPercentage *)divisionPercentage {
    _siluetBackImageViewController.percentage = divisionPercentage;
    _siluetFrontImageViewController.percentage = divisionPercentage;
}

- (void)loadCompareView:(EvolutionStressDisplayContext*)displayContext {
    
    RLMResults * logs = displayContext.logs;
    if (displayContext.logs.count > 0) {
        
        NSNumber *max = [logs maxOfProperty:@"weight"];
        NSNumber *min = [logs minOfProperty:@"weight"];
        
        _weightMinLabel.text = [NSString stringWithFormat:@"%@", [logs minOfProperty:@"weight"]];
        _weightMaxLabel.text = [NSString stringWithFormat:@"%@",[logs maxOfProperty:@"weight"]];
        _weightAveLabel.text = [NSString stringWithFormat:@"%.0f",[[logs averageOfProperty:@"weight"] floatValue]];
        
        if(max.floatValue > min.floatValue) {
            _weightDifLabel.textColor = GREEN_APP_COLOR;
            _weightDifLabel.text = [NSString stringWithFormat:@"+%.0f",max.floatValue - min.floatValue];
        }
        else {
            _weightDifLabel.textColor = RED_APP_COLOR;
            _weightDifLabel.text = [NSString stringWithFormat:@"-%.0f",max.floatValue - min.floatValue];
        }
        
        NSNumber *maxBMR = [logs maxOfProperty:@"bmr"];
        NSNumber *minBMR = [logs minOfProperty:@"bmr"];
        
        _bmrMinLabel.text = [NSString stringWithFormat:@"%@",[logs minOfProperty:@"bmr"]];
        _bmrMaxLabel.text = [NSString stringWithFormat:@"%@",[logs maxOfProperty:@"bmr"]];
        _bmrAveLabel.text = [NSString stringWithFormat:@"%.0f",[[logs averageOfProperty:@"bmr"] floatValue]];
        
        if(maxBMR.floatValue > minBMR.floatValue) {
            _bmrDifLabel.textColor = GREEN_APP_COLOR;
            _bmrDifLabel.text = [NSString stringWithFormat:@"+%.0f",maxBMR.floatValue - minBMR.floatValue];
        }
        else {
            _bmrDifLabel.textColor = RED_APP_COLOR;
            _bmrDifLabel.text = [NSString stringWithFormat:@"-%.0f",maxBMR.floatValue - minBMR.floatValue];
        }
        
        NSNumber *maxIGC = [logs maxOfProperty:@"igc"];
        NSNumber *minIGC = [logs minOfProperty:@"igc"];
        
        if(maxIGC.floatValue > minIGC.floatValue) {
            _imgDifLabel.textColor = GREEN_APP_COLOR;
            _imgDifLabel.text = [NSString stringWithFormat:@"+%.0f",maxIGC.floatValue - minIGC.floatValue];
        }
        else {
            _imgDifLabel.textColor = RED_APP_COLOR;
            _imgDifLabel.text = [NSString stringWithFormat:@"-%.0f",maxIGC.floatValue - minIGC.floatValue];
        }
        
        _imgMinLabel.text = [NSString stringWithFormat:@"%@",[logs minOfProperty:@"igc"]];
        _imgMaxLabel.text = [NSString stringWithFormat:@"%@",[logs maxOfProperty:@"igc"]];
        _imgAveLabel.text = [NSString stringWithFormat:@"%.0f",[[logs averageOfProperty:@"igc"] floatValue]];
        
        __weak UIImageView *progressHomeImageView = _firstLogImageView;
        
        NSURLRequest *homeImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS, displayContext.minLogDate.picture1]]
                                                          cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                      timeoutInterval:60];
        
        [_firstLogImageView setImageWithURLRequest:homeImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            
            [progressHomeImageView setImage:image];
            [[progressHomeImageView layer] setMasksToBounds:YES];
            [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            
            [progressHomeImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
            [[progressHomeImageView layer] setMasksToBounds:YES];
            [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        }];
        __weak UIImageView *lastImageView = _lastLogImageView;
        
        NSURLRequest *lastImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS, displayContext.maxLogDate.picture1]]
                                                          cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                      timeoutInterval:60];
        
        [_lastLogImageView setImageWithURLRequest:lastImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            
            [lastImageView setImage:image];
            [[lastImageView layer] setMasksToBounds:YES];
            [lastImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            [lastImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
            [[lastImageView layer] setMasksToBounds:YES];
            [lastImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        }];
    }
}

- (void)relocateControls:(EvolutionStressDisplayContext *)displayContext {
    
    //
    // Relocate segments
    //
    
    CGRect segmentControlFrame = self.segmentControlView.frame;
    if (displayContext.chartSelectionIndex == EvolutionChartDataDots &&
        displayContext.calendarIndex == EvolutionCalendarTypePlans) {
        
        segmentControlFrame.size.height = TAB_BAR_HEIGHT;
    }
    else {
        segmentControlFrame.size.height = 0;
    }
    self.segmentControlView.frame = segmentControlFrame;
    
    //
    // Relocate chart view
    //
    
    self.chartView.frame = CGRectMake(0,
                                      CGRectGetMaxY(self.segmentControlView.frame),
                                      SCREEN_WIDTH,
                                      [UtilManager height:170]);
    
    //
    // Relocate chart selection
    //
    
    CGFloat chartSelectionWidth = SCREEN_WIDTH * .8f;
    NSInteger chartSelections = self.chartDataButtons.count;
    if (displayContext.calendarIndex == EvolutionCalendarTypeDate) {
        chartSelections = self.chartDataButtons.count - 1;
    }
    
    for (NSInteger index = 0; index < self.chartDataButtons.count; index++) {
        YellowBlackSwitchButton * chartDataButton = self.chartDataButtons[index];
        CGRect chartDataButtonFrame = chartDataButton.frame;
        chartDataButtonFrame.size.width = chartSelectionWidth / chartSelections;
        chartDataButtonFrame.origin.x = index * CGRectGetWidth(chartDataButtonFrame);
        chartDataButton.frame = chartDataButtonFrame;
    }
    
    //
    // Relocate image view
    //
    
    _firstLogImageView.frame = CGRectMake(0.f,
                                          CGRectGetMaxY(self.chartView.frame) + [UtilManager height:10],
                                          [UtilManager height:140],
                                          [UtilManager height:140]);
    
    _firstLogImageView.center = CGPointMake(WIDTH/4, _firstLogImageView.center.y);
    
    _lastLogImageView.frame = CGRectMake(0.f,
                                         CGRectGetMinY(_firstLogImageView.frame),
                                         CGRectGetWidth(_firstLogImageView.frame),
                                         CGRectGetHeight(_firstLogImageView.frame));
    
    _lastLogImageView.center = CGPointMake(WIDTH*3/4, _lastLogImageView.center.y);
    _lastLogImageView.contentMode = UIViewContentModeScaleAspectFit;
    _lastLogImageView.userInteractionEnabled = YES;
    
    _compareView.frame = CGRectMake(0.f,
                                    CGRectGetMaxY(_lastLogImageView.frame) + [UtilManager height:10],
                                    WIDTH,
                                    [UtilManager height:120]);
    
    _separatorSecondView.frame = CGRectMake([UtilManager width:10],
                                            CGRectGetMaxY(_compareView.frame) + [UtilManager height:20],
                                            WIDTH - [UtilManager width:20],
                                            1);
    
    _siluetFrontImageView.frame = CGRectMake(0.f,
                                        CGRectGetMinY(_separatorSecondView.frame) + [UtilManager height:10],
                                        [UtilManager width:215],
                                        [UtilManager height:375]);
    
    _siluetFrontImageView.center = CGPointMake(self.scrollView.center.x, _siluetFrontImageView.center.y);
}

- (void)loadDateViews:(EvolutionStressDisplayContext *)displayContext {
    if (displayContext.calendarIndex == EvolutionCalendarTypeDate) {
        self.startDateLabel.text = [DateTimeUtil stringFromDate:displayContext.startDate
                                                         format:@"yyyy-MM-dd"];
        
        self.endDateLabel.text = [DateTimeUtil stringFromDate:displayContext.endDate
                                                       format:@"yyyy-MM-dd"];
    }
    else if (displayContext.calendarIndex == EvolutionCalendarTypeLogs) {
        if (displayContext.startDateLog) {
            self.startDateLabel.text = [DateTimeUtil stringFromDate:displayContext.startDateLog.revisionDate
                                                             format:@"yyyy-MM-dd"];
        }
        else {
            self.startDateLabel.text = @"Comienzo Log";
        }
        
        if (displayContext.endDateLog) {
            self.endDateLabel.text = [DateTimeUtil stringFromDate:displayContext.endDateLog.revisionDate
                                                           format:@"yyyy-MM-dd"];
        }
        else {
            self.endDateLabel.text = @"Finalizacion Log";
        }
    }
    else if (displayContext.calendarIndex == EvolutionCalendarTypePlans) {
        if (displayContext.startDatePlan) {
            self.startDateLabel.text = [DateTimeUtil stringFromDate:displayContext.startDatePlan.scheduleDate
                                                             format:@"yyyy-MM-dd"];
        }
        else {
            self.startDateLabel.text = @"Comienzo Plan";
        }
        
        if (displayContext.endDatePlan) {
            self.endDateLabel.text = [DateTimeUtil stringFromDate:displayContext.endDatePlan.scheduleDate
                                                           format:@"yyyy-MM-dd"];
        }
        else {
            self.endDateLabel.text = @"Finalizacion Plan";
        }
    }
}

- (void)acceptDatePicker:(NSString*)accessibilityLabel
              dateString:(NSString*)dateString {
    
    if ([accessibilityLabel isEqualToString:@"startDate"]) {
        self.startDateLabel.text = dateString;
    }
    else {
        self.endDateLabel.text = dateString;
    }
    
    [self.containerPickerView removeFromSuperview];
    self.containerPickerView = nil;
    
    [self.acceptButton removeFromSuperview];
    self.acceptButton = nil;
    
    [self.datePicker removeFromSuperview];
    self.datePicker = nil;
    
    [self.executive reloadDisplay];
}

- (void)presentComparePictureView:(Log*)minLogDate
                           maxLog:(Log*)maxLogDate {
    
    ComparePicsViewController *comparePicsViewController = [[ComparePicsViewController alloc] init];
    comparePicsViewController.firstLog = minLogDate;
    comparePicsViewController.lastLog = maxLogDate;
    [self presentViewController:comparePicsViewController animated:YES completion:nil];
}

#pragma mark - CTPageViewControllerDelegate

- (void)pageViewController:(CTPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
}

#pragma mark - SiluetStressImageViewControllerDelegate

- (void)siluetStressImageViewController:(SiluetStressImageViewController *)siluetStressImageViewController didPressGotoButton:(BOOL)isForward {
    NSUInteger currentPage = [_siluetPageViewController getCurrentPage];
    if (isForward) {
        currentPage = currentPage + 1;
    }
    else {
        currentPage = currentPage - 1;
    }
    
    [_siluetPageViewController gotoPage:currentPage animated:YES];
}

@end
