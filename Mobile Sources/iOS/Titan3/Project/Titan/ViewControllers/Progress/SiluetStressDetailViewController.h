//
//  SiluetStressDetailViewController.h
//  Titan
//
//  Created by Marcus Lee on 11/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "CommonViewController.h"
#import "SiluetStressImageViewController.h"

@interface SiluetStressDetailViewController : CommonViewController

@property (nonatomic, assign) SiluetStressImageType imageType;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) NSString * bodyPart;
@property (nonatomic, strong) NSArray * exercises;
@property (nonatomic, strong) DivisionPercentage * divisionPercentage;

//
// Outlets
//

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *divisionPercentageLabel;
@property (weak, nonatomic) IBOutlet UILabel *difficultyLabel;
@property (weak, nonatomic) IBOutlet UILabel *frequencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *basicLabel;
@property (weak, nonatomic) IBOutlet UILabel *specificLabel;
@property (weak, nonatomic) IBOutlet UILabel *seriesLabel;

@end
