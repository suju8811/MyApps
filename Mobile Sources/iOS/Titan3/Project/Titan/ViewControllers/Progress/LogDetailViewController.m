//
//  LogDetailViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 2/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "LogDetailViewController.h"
#import "PictureVisorViewController.h"
#import "CustomInputView.h"
#import "RNBlurModalView.h"
#import "UploadMediaView.h"
#import "WebServiceManager.h"
#import "MRProgress.h"
#import "UIImageView+AFNetworking.h"
#import "MenuContainerNavController.h"
#import "GalleryView.h"
#import "LogImageCarouselCell.h"
#import "DateManager.h"

#define TEST_BACKGROUND_COLOR   UIColor.clearColor

@interface LogDetailViewController () <UITextFieldDelegate, UIImagePickerControllerDelegate, CustomInputViewDelegate, UploadMediaViewDelegate, UINavigationControllerDelegate, WebServiceManagerDelegate, UITextFieldDelegate,GalleryDelegate, GalleryDataSource, UIPickerViewDelegate, UIPickerViewDataSource, LogDetailDisplay>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIPageControl *pageControl;

@property (nonatomic, strong) UILabel *weightLabel;
@property (nonatomic, strong) UILabel *bfiLabel;
@property (nonatomic, strong) UILabel *bmrLabel;
@property (nonatomic, strong) UITextField *rmTextField;
@property (nonatomic, strong) UITextField *bmrTextfield;

@property (nonatomic, strong) UIImageView *squatImageView;
@property (nonatomic, strong) UIImageView *benchPressImageView;
@property (nonatomic, strong) UIImageView *militaryPressImageView;
@property (nonatomic, strong) UIImageView *deadWeightImageView;
@property (nonatomic, strong) UIImageView *dominatedImageView;

@property (nonatomic, strong) UITextField *squatTextFieled;         //Sentadilla
@property (nonatomic, strong) UITextField *benchPresstextField;     //Press Banca
@property (nonatomic, strong) UITextField *militaryPressTextField;  //Press Militar
@property (nonatomic, strong) UITextField *deadWeightTextField;     //Peso Muerto
@property (nonatomic, strong) UITextField *dominatedTextField;      //Dominadas

@property (nonatomic, strong) UIImageView *siluetImageView;

@property (nonatomic, strong) UITextField *neckTextField;               //Neck
@property (nonatomic, strong) UITextField *backTextField;               //Back
@property (nonatomic, strong) UITextField *leftBicepTextField;          //Left Bicep
@property (nonatomic, strong) UITextField *rightBicepTextField;         //Right Bicep
@property (nonatomic, strong) UITextField *hipTextField;                //Hip
@property (nonatomic, strong) UITextField *waistTextField;            //cintura
@property (nonatomic, strong) UITextField *leftForearmTextField;      //antebrazo izquierdo
@property (nonatomic, strong) UITextField *rightForearmTextField;     //antebrazo derecho
@property (nonatomic, strong) UITextField *leftThighTextField;        //muslo izquierdo
@property (nonatomic, strong) UITextField *rightThighTextField;       //muslo derecho
@property (nonatomic, strong) UITextField *leftLegTextField;        //muslo izquierdo
@property (nonatomic, strong) UITextField *rightLegTextField;       //muslo derecho

@property (nonatomic, strong) CustomInputView *customInputView;
@property (nonatomic, strong) RNBlurModalView *modalView;

@property (nonatomic, strong) UploadMediaView *uploadMediaView;
@property (nonatomic, strong) UIImage *logImage;
@property (nonatomic, strong) UIImagePickerController *picker;

@property (nonatomic, strong) NSMutableArray *images;

@property (nonatomic, strong) Log *currentLog;
@property (nonatomic, strong) GalleryView *galleryView;

@property (nonatomic, strong) UIImageView *mainImageView;
@property (nonatomic, strong) UIImageView *secondImageView;
@property (nonatomic, strong) UIImageView *thirdImageView;
@property (nonatomic, strong) UIImageView *fourImageView;
@property (nonatomic, strong) UIImageView *fiveImageView;

@property (nonatomic, strong) UIView *layerView;

@property (nonatomic, strong) UIView *containerPickerView;
@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) UIButton *acceptButton;
@property (nonatomic, assign) CustomInputType customInputType;

@property (nonatomic, assign) NSInteger imagesUploads;

@property (nonatomic, strong) LogDetailExecutive * executive;

@end

@implementation LogDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _images = [[NSMutableArray alloc] initWithCapacity:0];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addLog) name:NAV_RIGHT1_ACTION object:nil];
    
    MenuContainerNavController *menuContainer = (MenuContainerNavController *)self.navigationController;
    if(_selectLog) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        
        NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
        NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute;
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:units fromDate:_selectLog.revisionDate];
        
        [[menuContainer titleLabel] setText:[NSString stringWithFormat:@"%ld %@ %ld",(long)components.day,[DateManager monthWithInteger:components.month],(long)components.year]];

        if(_selectLog.picture1)
            [_images addObject:_selectLog.picture1];
        if(_selectLog.picture2)
            [_images addObject:_selectLog.picture2];
        if(_selectLog.picture3)
            [_images addObject:_selectLog.picture3];
        if(_selectLog.picture4)
            [_images addObject:_selectLog.picture4];
        if(_selectLog.picture5)
            [_images addObject:_selectLog.picture5];
        
        MenuContainerNavController *menuNavController = (MenuContainerNavController *)self.navigationController;
        [[menuNavController rightButton1] setTitle:@"" forState:UIControlStateNormal];
        [[menuNavController rightButton1] setHidden:YES];
    }
    
    [self configureView];
    
    _executive = [[LogDetailExecutive alloc] initWithDisplay:self];;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(_selectLog){
        MenuContainerNavController *menuNavController = (MenuContainerNavController *)self.navigationController;
        [[menuNavController rightButton1] setTitle:@"" forState:UIControlStateNormal];
        [[menuNavController rightButton1] setHidden:YES];
    }
}

- (void)configureView{
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, HEIGHT)];
    
    [_scrollView addSubview:[self headerView]];
    
    _galleryView = [[GalleryView alloc] initWithFrame:CGRectMake(0,NAV_HEIGHT + [UtilManager height:20], WIDTH,WIDTH * 0.5625)];
    [_galleryView setDelegate:self];
    [_galleryView setDataSource:self];
    [_galleryView setBackgroundColor:[UIColor clearColor]];
    
    [_scrollView addSubview:_galleryView];
    
    if(_selectLog){
        if(_images.count == 0){
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0., _galleryView.frame.origin.y, [UtilManager width:240], _galleryView.frame.size.height)];
            [imageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
            [imageView setCenter:CGPointMake(self.view.center.x, imageView.center.y)];
            
            [_galleryView removeFromSuperview];
            [_scrollView addSubview:imageView];
        }
    }
    
    //_mainImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:10], NAV_HEIGHT + [UtilManager height:20], [UtilManager width:300], [UtilManager height:180])];
    _mainImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0., _galleryView.frame.origin.y + _galleryView.frame.size.height, WIDTH/5, WIDTH/5)];
    [_mainImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
    [_mainImageView setTag:1];
    [_mainImageView setUserInteractionEnabled:TRUE];
    UITapGestureRecognizer *mainTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentPicMainView:)];
    [mainTap setAccessibilityLabel:@"1"];
    [_mainImageView addGestureRecognizer:mainTap];
    
    [_scrollView addSubview:_mainImageView];
    
    _secondImageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH/5, _mainImageView.frame.origin.y,WIDTH/5, WIDTH/5)];
    [_secondImageView setImage:[UIImage imageNamed:@""]];
    [_secondImageView setTag:2];
    [_secondImageView setUserInteractionEnabled:TRUE];
    UITapGestureRecognizer *secondTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentPicMainView:)];
    [secondTap setAccessibilityLabel:@"2"];
    [_secondImageView addGestureRecognizer:secondTap];
    
    [_scrollView addSubview:_secondImageView];
    
    _thirdImageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH * 2/5, _mainImageView.frame.origin.y, WIDTH/5, WIDTH/5)];
    [_thirdImageView setUserInteractionEnabled:TRUE];
    [_thirdImageView setImage:[UIImage imageNamed:@""]];
    [_thirdImageView setTag:3];
    UITapGestureRecognizer *thirdTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentPicMainView:)];
    [thirdTap setAccessibilityLabel:@"3"];
    [_thirdImageView addGestureRecognizer:thirdTap];
    
    [_scrollView addSubview:_thirdImageView];
    
    _fourImageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH * 3/5, _mainImageView.frame.origin.y, WIDTH/5, WIDTH/5)];
    [_fourImageView setUserInteractionEnabled:TRUE];
    [_fourImageView setImage:[UIImage imageNamed:@""]];
    [_fourImageView setTag:4];
    UITapGestureRecognizer *fourTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentPicMainView:)];
    [fourTap setAccessibilityLabel:@"4"];
    [_fourImageView addGestureRecognizer:fourTap];
    
    [_scrollView addSubview:_fourImageView];
    
    _fiveImageView = [[UIImageView alloc] initWithFrame:CGRectMake(4 * WIDTH / 5, _mainImageView.frame.origin.y, WIDTH/5, WIDTH/5)];
    [_fiveImageView setUserInteractionEnabled:TRUE];
    [_fiveImageView setImage:[UIImage imageNamed:@""]];
    UITapGestureRecognizer *fiveTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentPicMainView:)];
    [fiveTap setAccessibilityLabel:@"5"];
    [_fiveImageView addGestureRecognizer:fiveTap];
    
    [_scrollView addSubview:_fiveImageView];
    
    _layerView = [[UIView alloc] initWithFrame:_mainImageView.frame];
    [_layerView setBackgroundColor:[UIColor clearColor]];
    [[_layerView layer] setBorderWidth:2];
    [[_layerView layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    if(_selectLog) {
        
        [_scrollView addSubview:_layerView];
        
        if(_selectLog.picture1) {
            NSData * imageData = [[NSData alloc] initWithBase64EncodedString:_selectLog.picture1 options:0];
            _mainImageView.image = [UIImage imageWithData:imageData];
        }
        if(_selectLog.picture2){
            __weak UIImageView *progressHomeImageView = _secondImageView;
            
            NSURLRequest *homeImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,_selectLog.picture2]]
                                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                          timeoutInterval:60];
            
            [_secondImageView setImageWithURLRequest:homeImageRequest placeholderImage:[UIImage imageNamed:@""] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                
                [progressHomeImageView setImage:image];
                [[progressHomeImageView layer] setMasksToBounds:YES];
                [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFill];
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                
                [progressHomeImageView setImage:[UIImage imageNamed:@""]];
                [[progressHomeImageView layer] setMasksToBounds:YES];
                [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
                
            }];
        }
        if(_selectLog.picture3){
            __weak UIImageView *progressHomeImageView = _thirdImageView;
            
            NSURLRequest *homeImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,_selectLog.picture3]]
                                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                          timeoutInterval:60];
            
            [_thirdImageView setImageWithURLRequest:homeImageRequest placeholderImage:[UIImage imageNamed:@""] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                
                [progressHomeImageView setImage:image];
                [[progressHomeImageView layer] setMasksToBounds:YES];
                [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFill];
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                
                [progressHomeImageView setImage:[UIImage imageNamed:@""]];
                [[progressHomeImageView layer] setMasksToBounds:YES];
                [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
                
            }];
        }
        if(_selectLog.picture4){
            __weak UIImageView *progressHomeImageView = _fourImageView;
            
            NSURLRequest *homeImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,_selectLog.picture4]]
                                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                          timeoutInterval:60];
            
            [_fourImageView setImageWithURLRequest:homeImageRequest placeholderImage:[UIImage imageNamed:@""] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                
                [progressHomeImageView setImage:image];
                [[progressHomeImageView layer] setMasksToBounds:YES];
                [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFill];
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                
                [progressHomeImageView setImage:[UIImage imageNamed:@""]];
                [[progressHomeImageView layer] setMasksToBounds:YES];
                [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
                
            }];
        }
        if(_selectLog.picture5){
            __weak UIImageView *progressHomeImageView = _fiveImageView;
            
            NSURLRequest *homeImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,_selectLog.picture5]]
                                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                          timeoutInterval:60];
            
            [_fiveImageView setImageWithURLRequest:homeImageRequest placeholderImage:[UIImage imageNamed:@""] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                
                [progressHomeImageView setImage:image];
                [[progressHomeImageView layer] setMasksToBounds:YES];
                [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFill];
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                
                [progressHomeImageView setImage:[UIImage imageNamed:@""]];
                [[progressHomeImageView layer] setMasksToBounds:YES];
                [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
                
            }];
        }
        
    }
    else {
        [_mainImageView setUserInteractionEnabled:YES];
    }
     
    
    UIView *pageContainerView = [[UIView alloc] initWithFrame:CGRectMake(0., _galleryView.frame.size.height - [UtilManager height:30], WIDTH, [UtilManager height:30])];
    [pageContainerView setBackgroundColor:[UIColor blackColor]];
    [pageContainerView setAlpha:0.6];
    
    [_galleryView addSubview:pageContainerView];
    
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0., _galleryView.frame.size.height - [UtilManager height:30], WIDTH, [UtilManager height:30])];
    [_pageControl setNumberOfPages:_images.count];
    [_pageControl setCurrentPage:0];
    [_pageControl setBackgroundColor:[UIColor clearColor]];
    
    [_galleryView addSubview:_pageControl];
    
    UIView *separatorGalleryView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:5], _fiveImageView.frame.size.height + _fiveImageView.frame.origin.y + [UtilManager height:12], WIDTH - [UtilManager width:10], 1)];
    [separatorGalleryView setBackgroundColor:GRAY_REGISTER_FONT];
    
    //[_scrollView addSubview:separatorGalleryView];
    
    UIView *weightView = [[UILabel alloc] initWithFrame:CGRectMake(0.f,
                                                                   separatorGalleryView.frame.origin.y + [UtilManager height:10],
                                                                   [UtilManager width:105],
                                                                   [UtilManager width:105])];
    
    weightView.layer.borderColor = BLUE_WEIGTH_PERCENT_CIRCLE.CGColor;
    weightView.layer.borderWidth = 2;
    weightView.center = CGPointMake(WIDTH/3, weightView.center.y);
    weightView.layer.cornerRadius = weightView.frame.size.width/2;
    
    [_scrollView addSubview:weightView];
    
    _weightLabel = [[UILabel alloc] initWithFrame:CGRectMake(weightView.frame.origin.x,
                                                             weightView.frame.origin.y,
                                                             weightView.frame.size.width,
                                                             weightView.frame.size.height * 0.8)];
    
    if(_selectLog.weight) {
        _weightLabel.text = [NSString stringWithFormat:@"%.0f",_selectLog.weight.floatValue];
    }
    else {
        if ([AppContext sharedInstance].currentUser.weight) {
            _weightLabel.text = [NSString stringWithFormat:@"%.0f",[AppContext sharedInstance].currentUser.weight.floatValue];
            if(!_currentLog) {
                _currentLog = [[Log alloc] init];
            }
            _currentLog.weight = [AppContext sharedInstance].currentUser.weight;
            _currentLog.bmr = [AppContext sharedInstance].currentUser.bmr;
        }
        else {
            [_weightLabel setText:@"-"];
        }
    }
    
    [_weightLabel setFont:[UIFont fontWithName:LIGHT_FONT_OPENSANS size:33]];
    [_weightLabel setNumberOfLines:1];
    [_weightLabel setTextColor:BLUE_WEIGTH_PERCENT_CIRCLE];
    [_weightLabel setTextAlignment:NSTextAlignmentCenter];
    [_weightLabel setUserInteractionEnabled:YES];
    [_weightLabel setAccessibilityLabel:@"weight"];
    
    UILabel *kgLabel = [[UILabel alloc] initWithFrame:CGRectMake(_weightLabel.frame.origin.x,
                                                                 _weightLabel.frame.origin.y + [UtilManager height:30],
                                                                 _weightLabel.frame.size.width,
                                                                 _weightLabel.frame.size.height - [UtilManager height:30])];
    
    kgLabel.textColor = _weightLabel.textColor;
    kgLabel.textAlignment = NSTextAlignmentCenter;
    kgLabel.font = [UIFont fontWithName:LIGHT_FONT_OPENSANS size:16];
    kgLabel.text = @"\nKg";
    kgLabel.numberOfLines = 2;
    kgLabel.backgroundColor = UIColor.clearColor;
    
    [_scrollView addSubview:kgLabel];
    
    UITapGestureRecognizer *weightGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(weightInputValue)];
    [_weightLabel addGestureRecognizer:weightGestureRecognizer];
    
    [_scrollView addSubview:_weightLabel];
    
    UIView *bfiView = [[UILabel alloc] initWithFrame:CGRectMake(weightView.frame.origin.x + weightView.frame.size.width + [UtilManager width:10],
                                                                separatorGalleryView.frame.origin.y + [UtilManager height:10],
                                                                [UtilManager width:70],
                                                                [UtilManager width:70])];
    
    bfiView.layer.borderColor = RED_APP_COLOR.CGColor;
    bfiView.layer.borderWidth = 2;
    bfiView.center = CGPointMake(WIDTH * 2/3, weightView.center.y);
    bfiView.layer.cornerRadius = bfiView.frame.size.width/2;
    [_scrollView addSubview:bfiView];
    
    _bfiLabel = [[UILabel alloc] initWithFrame:CGRectMake(bfiView.frame.origin.x,
                                                          bfiView.frame.origin.y,
                                                          bfiView.frame.size.width,
                                                          bfiView.frame.size.height * 0.8)];
    
    _bfiLabel.font = [UIFont fontWithName:LIGHT_FONT_OPENSANS size:22];
    if(_selectLog.igc) {
        [_bfiLabel setText:[NSString stringWithFormat:@"%.0f",_selectLog.igc.floatValue]];
    }
    else if (_currentLog.igc) {
        [_bfiLabel setText:[NSString stringWithFormat:@"%.0f",_currentLog.igc.floatValue]];
    }
    else {
        [_bfiLabel setText:@"-%"];
    }
    
    _bfiLabel.numberOfLines = 1;
    _bfiLabel.adjustsFontSizeToFitWidth = YES;
    _bfiLabel.textColor = RED_APP_COLOR;
    _bfiLabel.textAlignment = NSTextAlignmentCenter;
    _bfiLabel.userInteractionEnabled = YES;
    _bfiLabel.accessibilityLabel = @"bfi";
    
    UILabel *percentLabel = [[UILabel alloc] initWithFrame:CGRectMake(_bfiLabel.frame.origin.x,
                                                                      _bfiLabel.frame.origin.y + [UtilManager height:20],
                                                                      _bfiLabel.frame.size.width,
                                                                      _bfiLabel.frame.size.height - [UtilManager height:15])];
    
    percentLabel.textColor = _bfiLabel.textColor;
    percentLabel.textAlignment = NSTextAlignmentCenter;
    percentLabel.font = [UIFont fontWithName:LIGHT_FONT_OPENSANS size:12];
    percentLabel.text = @"\n%";
    percentLabel.numberOfLines = 2;
    percentLabel.backgroundColor = UIColor.clearColor;
    
    [_scrollView addSubview:percentLabel];
    
    UITapGestureRecognizer *bfiGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(igcInputValue)];
    [_bfiLabel addGestureRecognizer:bfiGestureRecognizer];
    
    [_scrollView addSubview:_bfiLabel];
    
    UIView *separatorFirstView = [[UIView alloc] initWithFrame:CGRectMake(separatorGalleryView.frame.origin.x,
                                                                          weightView.frame.origin.y + weightView.frame.size.height + [UtilManager height:20],
                                                                          separatorGalleryView.frame.size.width,
                                                                          1)];
    
    separatorFirstView.backgroundColor = separatorGalleryView.backgroundColor;
    
    //[_scrollView addSubview:separatorFirstView];
    
    UILabel *tmbLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                  CGRectGetMinY(separatorFirstView.frame) + [UtilManager height:10],
                                                                  [UtilManager width:40],
                                                                  [UtilManager height:24])];
    
    tmbLabel.text = NSLocalizedString(@"TMB", nil);
    tmbLabel.textAlignment = NSTextAlignmentLeft;
    tmbLabel.textColor = GRAY_REGISTER_FONT;
    tmbLabel.font = [UIFont fontWithName:REGULAR_FONT size:16];
    [_scrollView addSubview:tmbLabel];
    
    _bmrLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(tmbLabel.frame),
                                                          CGRectGetMinY(separatorFirstView.frame) - [UtilManager height:10],
                                                          [UtilManager width:120],
                                                          [UtilManager height:60])];
    
    _bmrLabel.textColor = UIColor.whiteColor;
    _bmrLabel.text = NSLocalizedString(@"TMB", nil);
    _bmrLabel.textAlignment = NSTextAlignmentLeft;
    _bmrLabel.font = [UIFont fontWithName:LIGHT_FONT_OPENSANS size:23];
    _bmrLabel.textColor = [UIColor whiteColor];
    _bmrLabel.adjustsFontSizeToFitWidth = YES;
    if (_selectLog.weight) {
        _bmrLabel.text = [NSString stringWithFormat:@"%.2f kCal",_selectLog.bmr.floatValue];
    }
    else {
        if(_currentLog.bmr) {
            _bmrLabel.text = [NSString stringWithFormat:@"%.2f kCal",_currentLog.bmr.floatValue];
        }
        else {
            _bmrLabel.text = [NSString stringWithFormat:@" - kCal"];
        }
    }
    
    [_scrollView addSubview:_bmrLabel];
    
    UIButton *infoButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_bmrLabel.frame) + [UtilManager width:10],
                                                                      CGRectGetMinY(_bmrLabel.frame),
                                                                      CGRectGetHeight(_bmrLabel.frame) * 0.6,
                                                                      CGRectGetHeight(_bmrLabel.frame) * 0.6)];
    
    [infoButton setImage:[UIImage imageNamed:@"btn-info"] forState:UIControlStateNormal];
    [infoButton addTarget:self action:@selector(infoAction) forControlEvents:UIControlEventTouchUpInside];
    infoButton.center = CGPointMake(infoButton.center.x, tmbLabel.center.y);
    
    [_scrollView addSubview:infoButton];
    
    UILabel *rmTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(infoButton.frame) + [UtilManager width:10],
                                                                  CGRectGetMinY(infoButton.frame),
                                                                  [UtilManager width:40],
                                                                  [UtilManager height:24])];
    
    rmTitleLabel.text = NSLocalizedString(@"RM", nil);
    rmTitleLabel.textAlignment = NSTextAlignmentLeft;
    rmTitleLabel.textColor = GRAY_REGISTER_FONT;
    rmTitleLabel.font = [UIFont fontWithName:REGULAR_FONT size:16];
    rmTitleLabel.center = CGPointMake(rmTitleLabel.center.x, infoButton.center.y);
    [_scrollView addSubview:rmTitleLabel];
    
    _rmTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(rmTitleLabel.frame),
                                                                 CGRectGetMinY(separatorFirstView.frame) - [UtilManager height:10],
                                                                 [UtilManager width:120],
                                                                 [UtilManager height:60])];
    
    _rmTextField.delegate = self;
    _rmTextField.borderStyle = UITextBorderStyleNone;
    _rmTextField.textAlignment = NSTextAlignmentCenter;
    _rmTextField.font = [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18];
    _rmTextField.textColor = BLUE_PERCENT_CIRCLE;
    _rmTextField.tintColor = UIColor.whiteColor;
    _rmTextField.textAlignment = NSTextAlignmentCenter;
    _rmTextField.keyboardType = UIKeyboardTypeDecimalPad;
    _rmTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _rmTextField.accessibilityLabel = @"rm";
    _rmTextField.adjustsFontSizeToFitWidth = YES;
    _rmTextField.text = NSLocalizedString(@"RM", nil);
    
    if (_selectLog) {
        if(_selectLog.rm) {
            _rmTextField.text = [NSString stringWithFormat:@"%.2f RM", _selectLog.rm.floatValue];
        }
        else {
            _rmTextField.text = [NSString stringWithFormat:@" - RM"];
        }
    }
    
    [_scrollView addSubview:_rmTextField];
    
    UIView *bmrSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(separatorGalleryView.frame.origin.x,
                                                                        _bmrLabel.frame.origin.y + _bmrLabel.frame.size.height + [UtilManager height:15],
                                                                        separatorGalleryView.frame.size.width,
                                                                        separatorGalleryView.frame.size.height)];
    
    bmrSeparatorView.backgroundColor = separatorGalleryView.backgroundColor;
    
    [_scrollView addSubview:bmrSeparatorView];
    
    _squatImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_bmrLabel.frame.origin.x,
                                                                    bmrSeparatorView.frame.origin.y + [UtilManager height:20],
                                                                    [UtilManager width:70],
                                                                    [UtilManager height:70])];
    
    _squatImageView.image = [UIImage imageNamed:@"img-main-exercise-1"];
    _squatImageView.center = CGPointMake(WIDTH / 4, _squatImageView.center.y);
    
    [_scrollView addSubview:_squatImageView];
    
    _squatTextFieled = [[UITextField alloc] initWithFrame:CGRectMake(_squatImageView.frame.origin.x,
                                                                     _squatImageView.frame.origin.y + _squatImageView.frame.size.height + [UtilManager height:10],
                                                                     _squatImageView.frame.size.width,
                                                                     UITEXTFIELD_HEIGHT)];
    
    _squatTextFieled.delegate = self;
    _squatTextFieled.borderStyle = UITextBorderStyleNone;
    _squatTextFieled.textAlignment = NSTextAlignmentCenter;
    _squatTextFieled.font = [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18];
    _squatTextFieled.textColor = BLUE_PERCENT_CIRCLE;
    _squatTextFieled.tintColor = UIColor.whiteColor;
    _squatTextFieled.textAlignment = NSTextAlignmentCenter;
    _squatTextFieled.keyboardType = UIKeyboardTypeDecimalPad;
    _squatTextFieled.keyboardAppearance = UIKeyboardAppearanceDark;
    _squatTextFieled.accessibilityLabel = @"squat";
    _squatTextFieled.adjustsFontSizeToFitWidth = YES;
    [_scrollView addSubview:_squatTextFieled];
    
    _benchPressImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_squatImageView.frame) + [UtilManager width:10],
                                                                         CGRectGetMinY(_squatImageView.frame),
                                                                         CGRectGetWidth(_squatImageView.frame),
                                                                         CGRectGetHeight(_squatImageView.frame))];
    
    _benchPressImageView.image = [UIImage imageNamed:@"img-main-exercise-2"];
    _benchPressImageView.center = CGPointMake(WIDTH/2, _benchPressImageView.center.y);
    
    [_scrollView addSubview:_benchPressImageView];
    
    _benchPresstextField = [[UITextField alloc] initWithFrame:CGRectMake(_benchPressImageView.frame.origin.x,
                                                                         CGRectGetMaxY(_benchPressImageView.frame) + [UtilManager height:10],
                                                                         CGRectGetWidth(_squatImageView.frame),
                                                                         UITEXTFIELD_HEIGHT)];
    
    _benchPresstextField.delegate = self;
    _benchPresstextField.borderStyle = UITextBorderStyleNone;
    _benchPresstextField.textColor = _squatTextFieled.textColor;
    _benchPresstextField.font = _squatTextFieled.font;
    _benchPresstextField.textAlignment = _squatTextFieled.textAlignment;
    _benchPresstextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _benchPresstextField.keyboardType = _squatTextFieled.keyboardType;
    _benchPresstextField.accessibilityLabel = @"bench";
    _benchPresstextField.adjustsFontSizeToFitWidth = YES;
   
    [_scrollView addSubview:_benchPresstextField];
    
    _militaryPressImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_benchPressImageView.frame) + [UtilManager width:10],
                                                                            CGRectGetMinY(_squatImageView.frame),
                                                                            CGRectGetWidth(_squatImageView.frame),
                                                                            CGRectGetHeight(_benchPressImageView.frame))];
    
    _militaryPressImageView.image = [UIImage imageNamed:@"img-main-exercise-3"];
    _militaryPressImageView.center = CGPointMake(WIDTH * 3 / 4, _militaryPressImageView.center.y);
    
    [_scrollView addSubview:_militaryPressImageView];
    
    _militaryPressTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMinX(_militaryPressImageView.frame),
                                                                            CGRectGetMaxY(_militaryPressImageView.frame) + [UtilManager height:10],
                                                                            CGRectGetWidth(_squatImageView.frame),
                                                                            UITEXTFIELD_HEIGHT)];
    
    _militaryPressTextField.delegate = self;;
    _militaryPressTextField.borderStyle = UITextBorderStyleNone;
    _militaryPressTextField.textColor = _squatTextFieled.textColor;
    _militaryPressTextField.font = _squatTextFieled.font;
    _militaryPressTextField.textAlignment = _squatTextFieled.textAlignment;
    _militaryPressTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _militaryPressTextField.keyboardType = _squatTextFieled.keyboardType;
    _militaryPressTextField.accessibilityLabel = @"militaryPress";
    _militaryPressTextField.adjustsFontSizeToFitWidth = YES;
    
    [_scrollView addSubview:_militaryPressTextField];
    
    _deadWeightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(_squatImageView.frame),
                                                                         CGRectGetMaxY(_squatTextFieled.frame) + [UtilManager height:10],
                                                                         CGRectGetWidth(_squatImageView.frame),
                                                                         CGRectGetHeight(_squatImageView.frame))];
    
    _deadWeightImageView.center = CGPointMake(WIDTH/3, _deadWeightImageView.center.y);
    _deadWeightImageView.image = [UIImage imageNamed:@"img-main-exercise-4"];
    
    [_scrollView addSubview:_deadWeightImageView];
    
    _deadWeightTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMinX(_deadWeightImageView.frame),
                                                                         CGRectGetMaxY(_deadWeightImageView.frame) + [UtilManager height:10],
                                                                         CGRectGetWidth(_squatImageView.frame),
                                                                         UITEXTFIELD_HEIGHT)];
                                                                         
    _deadWeightTextField.delegate = self;
    _deadWeightTextField.borderStyle = UITextBorderStyleNone;
    _deadWeightTextField.textColor = _squatTextFieled.textColor;
    _deadWeightTextField.font = _squatTextFieled.font;
    _deadWeightTextField.textAlignment = _squatTextFieled.textAlignment;
    _deadWeightTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _deadWeightTextField.keyboardType = _squatTextFieled.keyboardType;
    _deadWeightTextField.accessibilityLabel = @"deadWeight";
    _deadWeightTextField.adjustsFontSizeToFitWidth = YES;
    
    [_scrollView addSubview:_deadWeightTextField];
    
    _dominatedImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_deadWeightImageView.frame) + [UtilManager width:10],
                                                                        CGRectGetMinY(_deadWeightImageView.frame),
                                                                        CGRectGetWidth(_deadWeightImageView.frame),
                                                                        CGRectGetHeight(_squatImageView.frame))];
    
    _dominatedImageView.center = CGPointMake(WIDTH * 2 / 3, _deadWeightImageView.center.y);
    _dominatedImageView.image = [UIImage imageNamed:@"img-main-exercise-5"];
    
    [_scrollView addSubview:_dominatedImageView];
    
    _dominatedTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMinX(_dominatedImageView.frame),
                                                                        CGRectGetMaxY(_dominatedImageView.frame) + [UtilManager height:10],
                                                                        CGRectGetWidth(_squatImageView.frame),
                                                                        UITEXTFIELD_HEIGHT)];
    
    _dominatedTextField.delegate = self;
    _dominatedTextField.borderStyle = UITextBorderStyleNone;
    _dominatedTextField.textColor = _squatTextFieled.textColor;
    _dominatedTextField.font = _squatTextFieled.font;
    _dominatedTextField.textAlignment = _squatTextFieled.textAlignment;
    _dominatedTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _dominatedTextField.keyboardType = _squatTextFieled.keyboardType;
    _dominatedTextField.accessibilityLabel = @"dominated";
    _dominatedTextField.adjustsFontSizeToFitWidth = YES;
    
    [_scrollView addSubview:_dominatedTextField];
    
    UIView *separatorSecondView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(separatorFirstView.frame),
                                                                           CGRectGetMaxY(_dominatedTextField.frame) + [UtilManager height:20],
                                                                           CGRectGetWidth(separatorFirstView.frame),
                                                                           CGRectGetHeight(separatorFirstView.frame))];
    
    separatorSecondView.backgroundColor = separatorFirstView.backgroundColor;
    
    [_scrollView addSubview:separatorSecondView];
    
    _siluetImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.f,
                                                                     CGRectGetMinY(separatorSecondView.frame) + [UtilManager height:10],
                                                                     [UtilManager width:215],
                                                                     [UtilManager height:375])];
    
    _siluetImageView.image = [UIImage imageNamed:@"img-siluet-empty-text"];
    _siluetImageView.center = CGPointMake(self.view.center.x, _siluetImageView.center.y);
    _siluetImageView.userInteractionEnabled = YES;
    
    [_scrollView addSubview:_siluetImageView];
    
    _neckTextField = [[UITextField alloc] initWithFrame:CGRectMake(0,
                                                                    CGRectGetHeight(_siluetImageView.frame) * 185/1119,
                                                                    CGRectGetWidth(_siluetImageView.frame) * 0.25,
                                                                    CGRectGetHeight(_siluetImageView.frame) * 0.048)];
    
    _neckTextField.textColor = UIColor.whiteColor;
    _neckTextField.font = _squatTextFieled.font;
    _neckTextField.textAlignment = NSTextAlignmentCenter;
    _neckTextField.adjustsFontSizeToFitWidth = YES;
    _neckTextField.delegate = self;
    _neckTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _neckTextField.keyboardType = _squatTextFieled.keyboardType;
    _neckTextField.accessibilityLabel = @"neck";
    _neckTextField.adjustsFontSizeToFitWidth = YES;
    _neckTextField.backgroundColor = TEST_BACKGROUND_COLOR;
    _neckTextField.center = CGPointMake(CGRectGetWidth(_siluetImageView.bounds) / 2.f, _neckTextField.center.y);
    
    [_siluetImageView addSubview:_neckTextField];
    
    _backTextField = [[UITextField alloc] initWithFrame:CGRectMake(0,
                                                                   CGRectGetHeight(_siluetImageView.frame) * 296/1119,
                                                                   CGRectGetWidth(_siluetImageView.frame) * 0.25,
                                                                   CGRectGetHeight(_neckTextField.frame))];
    
    _backTextField.textColor = UIColor.whiteColor;
    _backTextField.font = _squatTextFieled.font;
    _backTextField.textAlignment = NSTextAlignmentCenter;
    _backTextField.adjustsFontSizeToFitWidth = YES;
    _backTextField.delegate = self;
    _backTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _backTextField.keyboardType = _squatTextFieled.keyboardType;
    _backTextField.accessibilityLabel = @"back";
    _backTextField.adjustsFontSizeToFitWidth = YES;
    _backTextField.backgroundColor = TEST_BACKGROUND_COLOR;
    _backTextField.center = CGPointMake(CGRectGetWidth(_siluetImageView.bounds) / 2.f, _backTextField.center.y);
    
    [_scrollView addSubview:_dominatedTextField];
    
    [_siluetImageView addSubview:_backTextField];
    
    _leftBicepTextField = [[UITextField alloc] initWithFrame:CGRectMake(0,
                                                                        CGRectGetHeight(_siluetImageView.frame) * 340/1119,
                                                                        CGRectGetWidth(_siluetImageView.frame) * 0.25,
                                                                        CGRectGetHeight(_neckTextField.frame))];
    
    _leftBicepTextField.textColor = UIColor.whiteColor;
    _leftBicepTextField.font = _squatTextFieled.font;
    _leftBicepTextField.textAlignment = NSTextAlignmentCenter;
    _leftBicepTextField.adjustsFontSizeToFitWidth = YES;
    _leftBicepTextField.delegate = self;
    _leftBicepTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _leftBicepTextField.keyboardType = _squatTextFieled.keyboardType;
    _leftBicepTextField.accessibilityLabel = @"leftBicep";
    _leftBicepTextField.adjustsFontSizeToFitWidth = YES;
    _leftBicepTextField.backgroundColor = TEST_BACKGROUND_COLOR;
    _leftBicepTextField.center = CGPointMake(CGRectGetWidth(_siluetImageView.bounds) * 190/641, _leftBicepTextField.center.y);
    
    [_siluetImageView addSubview:_leftBicepTextField];
    
    _rightBicepTextField = [[UITextField alloc] initWithFrame:CGRectMake(0,
                                                                         CGRectGetHeight(_siluetImageView.frame) * 340/1119,
                                                                         CGRectGetWidth(_siluetImageView.frame) * 0.25,
                                                                         CGRectGetHeight(_neckTextField.frame))];
    
    _rightBicepTextField.textColor = UIColor.whiteColor;
    _rightBicepTextField.font = _squatTextFieled.font;
    _rightBicepTextField.textAlignment = NSTextAlignmentCenter;
    _rightBicepTextField.adjustsFontSizeToFitWidth = YES;
    _rightBicepTextField.delegate = self;
    _rightBicepTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _rightBicepTextField.keyboardType = _squatTextFieled.keyboardType;
    _rightBicepTextField.accessibilityLabel = @"rightBicep";
    _rightBicepTextField.adjustsFontSizeToFitWidth = YES;
    _rightBicepTextField.backgroundColor = TEST_BACKGROUND_COLOR;
    _rightBicepTextField.center = CGPointMake(CGRectGetWidth(_siluetImageView.bounds) * 458/641, _rightBicepTextField.center.y);
    
    [_siluetImageView addSubview:_rightBicepTextField];
    
    _leftForearmTextField = [[UITextField alloc] initWithFrame:CGRectMake(0,
                                                                          CGRectGetHeight(_siluetImageView.frame) * 420/1119,
                                                                          CGRectGetWidth(_siluetImageView.frame) * 0.25,
                                                                          CGRectGetHeight(_neckTextField.frame))];
    
    _leftForearmTextField.backgroundColor = TEST_BACKGROUND_COLOR;
    _leftForearmTextField.textColor = UIColor.whiteColor;
    _leftForearmTextField.font = _squatTextFieled.font;
    _leftForearmTextField.textAlignment = NSTextAlignmentCenter;
    _leftForearmTextField.adjustsFontSizeToFitWidth = YES;
    _leftForearmTextField.delegate = self;
    _leftForearmTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _leftForearmTextField.keyboardType = _squatTextFieled.keyboardType;
    _leftForearmTextField.accessibilityLabel = @"leftForeArm";
    _leftForearmTextField.adjustsFontSizeToFitWidth = YES;
    _leftForearmTextField.center = CGPointMake(CGRectGetWidth(_siluetImageView.bounds) * 147/641, _leftForearmTextField.center.y);
    
    [_siluetImageView addSubview:_leftForearmTextField];
    
    _rightForearmTextField = [[UITextField alloc] initWithFrame:CGRectMake(0,
                                                                           CGRectGetHeight(_siluetImageView.frame) * 420/1119,
                                                                           CGRectGetWidth(_siluetImageView.frame) * 0.25,
                                                                           CGRectGetHeight(_neckTextField.frame))];
    
    _rightForearmTextField.backgroundColor = TEST_BACKGROUND_COLOR;
    _rightForearmTextField.textColor = UIColor.whiteColor;
    _rightForearmTextField.font = _squatTextFieled.font;
    _rightForearmTextField.textAlignment = NSTextAlignmentCenter;
    _rightForearmTextField.delegate = self;
    _rightForearmTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _rightForearmTextField.keyboardType = _squatTextFieled.keyboardType;
    _rightForearmTextField.accessibilityLabel = @"rightForeArm";
    _rightForearmTextField.adjustsFontSizeToFitWidth = YES;
    _rightForearmTextField.center = CGPointMake(CGRectGetWidth(_siluetImageView.bounds) * 500/641, _rightForearmTextField.center.y);
    
    [_siluetImageView addSubview:_rightForearmTextField];
    
    _hipTextField = [[UITextField alloc] initWithFrame:CGRectMake(0,
                                                                  CGRectGetHeight(_siluetImageView.frame) * 481/1119,
                                                                  CGRectGetWidth(_siluetImageView.frame) * 0.25,
                                                                  CGRectGetHeight(_neckTextField.frame))];
    
    _hipTextField.backgroundColor = TEST_BACKGROUND_COLOR;
    _hipTextField.textColor = UIColor.whiteColor;
    _hipTextField.font = _squatTextFieled.font;
    _hipTextField.textAlignment = NSTextAlignmentCenter;
    _hipTextField.delegate = self;
    _hipTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _hipTextField.keyboardType = _squatTextFieled.keyboardType;
    _hipTextField.accessibilityLabel = @"hip";
    _hipTextField.adjustsFontSizeToFitWidth = YES;
    _hipTextField.center = CGPointMake(CGRectGetWidth(_siluetImageView.bounds) / 2.f, _hipTextField.center.y);
    
    [_siluetImageView addSubview:_hipTextField];
    
    _waistTextField  = [[UITextField alloc] initWithFrame:CGRectMake(0,
                                                                     CGRectGetHeight(_siluetImageView.frame) * 438/1119,
                                                                     CGRectGetWidth(_siluetImageView.frame) * 0.25,
                                                                     CGRectGetHeight(_neckTextField.frame))];
    
    _waistTextField.textColor = UIColor.whiteColor;
    _waistTextField.font = _squatTextFieled.font;
    _waistTextField.textAlignment = NSTextAlignmentCenter;
    _waistTextField.adjustsFontSizeToFitWidth = YES;
    _waistTextField.delegate = self;
    _waistTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _waistTextField.keyboardType = _squatTextFieled.keyboardType;
    _waistTextField.accessibilityLabel = @"waist";
    _waistTextField.adjustsFontSizeToFitWidth = YES;
    _waistTextField.backgroundColor = TEST_BACKGROUND_COLOR;
    _waistTextField.center = CGPointMake(CGRectGetWidth(_siluetImageView.bounds) / 2.f, _waistTextField.center.y);
    
    [_scrollView addSubview:_dominatedTextField];
    
    [_siluetImageView addSubview:_waistTextField];
    
    _leftThighTextField = [[UITextField alloc] initWithFrame:CGRectMake(0,
                                                                        CGRectGetHeight(_siluetImageView.frame) * 613/1119,
                                                                        CGRectGetWidth(_siluetImageView.frame) * 0.25,
                                                                        CGRectGetHeight(_neckTextField.frame))];
    
    _leftThighTextField.backgroundColor = TEST_BACKGROUND_COLOR;
    _leftThighTextField.textColor = UIColor.whiteColor;
    _leftThighTextField.font = _squatTextFieled.font;
    _leftThighTextField.textAlignment = NSTextAlignmentCenter;
    _leftThighTextField.adjustsFontSizeToFitWidth = YES;
    _leftThighTextField.delegate = self;
    _leftThighTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _leftThighTextField.keyboardType = _squatTextFieled.keyboardType;
    _leftThighTextField.accessibilityLabel = @"leftThigh";
    _leftThighTextField.adjustsFontSizeToFitWidth = YES;
    _leftThighTextField.center = CGPointMake(CGRectGetWidth(_siluetImageView.bounds) * 263 / 641, _leftThighTextField.center.y);
    
    [_siluetImageView addSubview:_leftThighTextField];
    
    _rightThighTextField = [[UITextField alloc] initWithFrame:CGRectMake(0,
                                                                         CGRectGetHeight(_siluetImageView.frame) * 613/1119,
                                                                         CGRectGetWidth(_siluetImageView.frame) * 0.25,
                                                                         CGRectGetHeight(_neckTextField.frame))];
    
    _rightThighTextField.backgroundColor = TEST_BACKGROUND_COLOR;
    _rightThighTextField.textColor = UIColor.whiteColor;
    _rightThighTextField.font = _squatTextFieled.font;
    _rightThighTextField.textAlignment = NSTextAlignmentCenter;
    _rightThighTextField.adjustsFontSizeToFitWidth = YES;
    _rightThighTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _rightThighTextField.keyboardType = _squatTextFieled.keyboardType;
    _rightThighTextField.accessibilityLabel = @"rightThigh";
    _rightThighTextField.adjustsFontSizeToFitWidth = YES;
    _rightThighTextField.delegate = self;
    _rightThighTextField.center = CGPointMake(CGRectGetWidth(_siluetImageView.bounds) * 414 / 641, _rightThighTextField.center.y);
    
    [_siluetImageView addSubview:_rightThighTextField];
    
    _leftLegTextField = [[UITextField alloc] initWithFrame:CGRectMake(0,
                                                                      CGRectGetHeight(_siluetImageView.frame) * 838/1119,
                                                                      CGRectGetWidth(_siluetImageView.frame) * 0.25,
                                                                      CGRectGetHeight(_neckTextField.frame))];
    
    _leftLegTextField.backgroundColor = TEST_BACKGROUND_COLOR;
    _leftLegTextField.textColor = UIColor.whiteColor;
    _leftLegTextField.font = _squatTextFieled.font;
    _leftLegTextField.textAlignment = NSTextAlignmentCenter;
    _leftLegTextField.adjustsFontSizeToFitWidth = YES;
    _leftLegTextField.delegate = self;
    _leftLegTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _leftLegTextField.keyboardType = _squatTextFieled.keyboardType;
    _leftLegTextField.accessibilityLabel = @"leftLeg";
    _leftLegTextField.adjustsFontSizeToFitWidth = YES;
    _leftLegTextField.center = CGPointMake(CGRectGetWidth(_siluetImageView.bounds) * 234 / 641, _leftLegTextField.center.y);
    
    [_siluetImageView addSubview:_leftLegTextField];
    
    _rightLegTextField = [[UITextField alloc] initWithFrame:CGRectMake(0,
                                                                      CGRectGetHeight(_siluetImageView.frame) * 838/1119,
                                                                      CGRectGetWidth(_siluetImageView.frame) * 0.25,
                                                                      CGRectGetHeight(_neckTextField.frame))];
    
    _rightLegTextField.backgroundColor = TEST_BACKGROUND_COLOR;
    _rightLegTextField.textColor = UIColor.whiteColor;
    _rightLegTextField.font = _squatTextFieled.font;
    _rightLegTextField.textAlignment = NSTextAlignmentCenter;
    _rightLegTextField.adjustsFontSizeToFitWidth = YES;
    _rightLegTextField.delegate = self;
    _rightLegTextField.keyboardAppearance = UIKeyboardAppearanceDark;
    _rightLegTextField.keyboardType = _squatTextFieled.keyboardType;
    _rightLegTextField.accessibilityLabel = @"rightLeg";
    _rightLegTextField.adjustsFontSizeToFitWidth = YES;
    _rightLegTextField.center = CGPointMake(CGRectGetWidth(_siluetImageView.bounds) * 416 / 641, _rightLegTextField.center.y);
    
    [_siluetImageView addSubview:_rightLegTextField];
    
    if(_selectLog) {
        _neckTextField.text = [NSString stringWithFormat:@"%d cm.", _selectLog.neck.intValue];
        _backTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.back.intValue];
        _leftBicepTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.leftBicep.intValue];
        _rightBicepTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.rightBicep.intValue];
        _leftForearmTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.leftForearm.intValue];
        _rightForearmTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.rightForearm.intValue];
        _hipTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.hip.intValue];
        _waistTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.waist.intValue];
        _leftThighTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.leftThigh.intValue];
        _rightThighTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.rightThigh.intValue];
        _leftLegTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.leftLeg.intValue];
        _rightLegTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.rightLeg.intValue];
        [_squatTextFieled setText:[NSString stringWithFormat:@"%d Kg",_selectLog.squat.intValue]];
        [_benchPresstextField setText:[NSString stringWithFormat:@"%d Kg",_selectLog.benchPress.intValue]];
        [_militaryPressTextField setText:[NSString stringWithFormat:@"%d Kg",_selectLog.militaryPress.intValue]];
        [_dominatedTextField setText:[NSString stringWithFormat:@"%d Kg",_selectLog.dominated.intValue]];
        [_deadWeightTextField setText:[NSString stringWithFormat:@"%d Kg",_selectLog.deadWeight.intValue]];
    }
    else if (_currentLog) {
        _neckTextField.text = [NSString stringWithFormat:@"%d cm.", _currentLog.neck.intValue];
        _backTextField.text = [NSString stringWithFormat:@"%d cm.",_currentLog.back.intValue];
        _leftBicepTextField.text = [NSString stringWithFormat:@"%d cm.",_currentLog.leftBicep.intValue];
        _rightBicepTextField.text = [NSString stringWithFormat:@"%d cm.",_currentLog.rightBicep.intValue];
        _leftForearmTextField.text = [NSString stringWithFormat:@"%d cm.",_currentLog.leftForearm.intValue];
        _rightForearmTextField.text = [NSString stringWithFormat:@"%d cm.",_currentLog.rightForearm.intValue];
        _hipTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.hip.intValue];
        _waistTextField.text = [NSString stringWithFormat:@"%d cm.",_currentLog.waist.intValue];
        _leftThighTextField.text = [NSString stringWithFormat:@"%d cm.",_currentLog.leftThigh.intValue];
        _rightThighTextField.text = [NSString stringWithFormat:@"%d cm.",_currentLog.rightThigh.intValue];
        _leftLegTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.leftLeg.intValue];
        _rightLegTextField.text = [NSString stringWithFormat:@"%d cm.",_selectLog.rightLeg.intValue];
        [_squatTextFieled setText:[NSString stringWithFormat:@"%d Kg",_currentLog.squat.intValue]];
        [_benchPresstextField setText:[NSString stringWithFormat:@"%d Kg",_currentLog.benchPress.intValue]];
        [_militaryPressTextField setText:[NSString stringWithFormat:@"%d Kg",_currentLog.militaryPress.intValue]];
        [_dominatedTextField setText:[NSString stringWithFormat:@"%d Kg",_currentLog.dominated.intValue]];
        [_deadWeightTextField setText:[NSString stringWithFormat:@"%d Kg",_currentLog.deadWeight.intValue]];
    }
    
    UIButton *addExerciseButton = [[UIButton alloc] initWithFrame:CGRectMake(0.f,
                                                                             CGRectGetMaxY(_siluetImageView.frame) + [UtilManager height:40],
                                                                             WIDTH,
                                                                             [UtilManager height:80])];
    if(_selectLog) {
        addExerciseButton.hidden = YES;
    }
    else {
        [addExerciseButton setTitle:NSLocalizedString(@"AÑADIR LOG", nil) forState:UIControlStateNormal];
    }
    
    addExerciseButton.titleLabel.font = [UIFont fontWithName:BOLD_FONT size:18];
    [addExerciseButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [addExerciseButton addTarget:self action:@selector(addLog) forControlEvents:UIControlEventTouchUpInside];
    addExerciseButton.backgroundColor = BLACK_HEADER_APP_COLOR;
    
    //[_scrollView addSubview:addExerciseButton];
    _scrollView.contentSize = CGSizeMake(WIDTH, CGRectGetMinY(addExerciseButton.frame));
    
    [self.view addSubview:_scrollView];
}

- (UIView *)headerView {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH,NAV_HEIGHT)];
    
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:20], [UtilManager width:40]*0.8, [UtilManager width:40] * 0.8)];
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    
    [headerView addSubview:backButton];
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50],
                                                                    [UtilManager height:20],
                                                                    [UtilManager width:40] * 0.8,
                                                                    [UtilManager width:40]*0.8)];
    
    [okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
    
    [headerView addSubview:okButton];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(backButton.frame.origin.x + backButton.frame.size.width, backButton.frame.origin.y + [UtilManager height:30], WIDTH - 2 * (backButton.frame.origin.x + backButton.frame.size.width), [UtilManager height:40])];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute;
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units fromDate:_selectLog.revisionDate];
    
    if(_selectLog)
        [titleLabel setText:[NSString stringWithFormat:@"%ld %@ %ld",(long)components.day,[DateManager monthWithInteger:components.month],(long)components.year]];
    else
        [titleLabel setText:NSLocalizedString(@"AÑADIR LOG", nil)];
    
    [titleLabel setNumberOfLines:1];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setFont:[UIFont fontWithName:BOLD_FONT size:20]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    [titleLabel setCenter:CGPointMake(self.view.center.x, backButton.center.y)];
    
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (void)didPressBackButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didPressOKButton {
    [_executive didPressOKButton];
}

- (void)weightInputValue {
    if(!_selectLog) {
        
        [self setCustomInputType:CustomInputTypeWeight];
        [self presentPickerView];
        /*
        _customInputView = [[CustomInputView alloc] initWithFrame:CGRectMake(0., HEIGHT - [UtilManager height:340], [UtilManager width:280], [UtilManager height:340])];
        [_customInputView setDelegate:self];
        
        [_customInputView setCustomInputType:CustomInputTypeWeight];
        [[_customInputView titleLabel] setText:NSLocalizedString(@"Peso:", nil)];
        
        _modalView  = [[RNBlurModalView alloc] initWithViewController:self view:_customInputView];
        
        [_modalView show];
         */
    }
}

- (void)igcInputValue {
    if(!_selectLog){
        [self setCustomInputType:CustomInputTypeIGC];
        [self presentPickerView];
    }
}

- (void)inputValue:(id)sender{
    UIButton *selectButton = (UIButton *)sender;
    if(!_selectLog){
        
        _customInputView = [[CustomInputView alloc] initWithFrame:CGRectMake(0., HEIGHT - [UtilManager height:340], [UtilManager width:280], [UtilManager height:340])];
        [_customInputView setCenter:CGPointMake(self.view.center.x,[UtilManager height:100])];
        [_customInputView setDelegate:self];
        
        if([selectButton.accessibilityLabel isEqualToString:@"back"]){
            [[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su pecho:", nil)];
            [_customInputView setCustomInputType:CustomInputTypeBack];
        } else if([selectButton.accessibilityLabel isEqualToString:@"waist"]){
            [[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su cintura:", nil)];
            [_customInputView setCustomInputType:CustomInputTypeWaist];
        }else if([selectButton.accessibilityLabel isEqualToString:@"leftArm"]){
            [[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su brazo izquierdo:", nil)];
            [_customInputView setCustomInputType:CustomInputTypeleftArm];
        }else if([selectButton.accessibilityLabel isEqualToString:@"rightArm"]){
            [[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su brazo derecho:", nil)];
            [_customInputView setCustomInputType:CustomInputTypeRightArm];
        }else if([selectButton.accessibilityLabel isEqualToString:@"leftForearm"]){
            [[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su antebrazo izquierdo:", nil)];
            [_customInputView setCustomInputType:CustomInputTypeLeftForeArm];
        }else if([selectButton.accessibilityLabel isEqualToString:@"rightForearm"]){
            [[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su antebrazo derecho:", nil)];
            [_customInputView setCustomInputType:CustomInputTypeRightForeArm];
        }else if([selectButton.accessibilityLabel isEqualToString:@"leftThigh"]){
            [[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su muslo izquierdo:", nil)];
            [_customInputView setCustomInputType:CustomInputTypeLeftThigh];
        }else if([selectButton.accessibilityLabel isEqualToString:@"rightThigh"]){
            [[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su muslo derecho:", nil)];
            [_customInputView setCustomInputType:CustomInputTypeRightThigh];
        }
        
        _modalView  = [[RNBlurModalView alloc] initWithViewController:self view:_customInputView];
        [_modalView show];
    }
}

- (void)presentPicView{
    
    //UITapGestureRecognizer *tapGesture = (UITapGestureRecognizer *)sender;
    
    if(_uploadMediaView)
        _uploadMediaView = nil;
    _uploadMediaView = [[UploadMediaView alloc] initWithFrame:self.view.bounds];
    [_uploadMediaView setDelegate:self];
    //[_uploadMediaView setAccessibilityLabel:tapGesture.accessibilityLabel];
    [self.view addSubview:_uploadMediaView];
}

- (void)addLog{
    if([self checkFields]){
        [MRProgressOverlayView showOverlayAddedTo:self.view title:NSLocalizedString(@"Subiendo Log", nil) mode:MRProgressOverlayViewModeIndeterminate animated:YES];
        [WebServiceManager postUserRevision:[self logDictionary] andDelegate:self];
    }
}

- (BOOL)checkFields{
    
    if(!_currentLog.weight){
        [self presentViewController:[UtilManager presentAlertWithMessage:@"Introduzca un peso"] animated:TRUE completion:^{
        }];
        
        return FALSE;
    }
    if(![[AppContext sharedInstance] currentUser].height){
        [self presentViewController:[UtilManager presentAlertWithMessage:@"Debe introducir una altura en su perfil"] animated:TRUE completion:^{
        
        }];
        
        return FALSE;
    }
    if(![[AppContext sharedInstance] currentUser].gender){
        [self presentViewController:[UtilManager presentAlertWithMessage:@"Debe elegir un sexo en su perfil"] animated:TRUE completion:^{
            
        }];
        
        return FALSE;
    }
    if(![[AppContext sharedInstance] currentUser].age){
        [self presentViewController:[UtilManager presentAlertWithMessage:@"Debe seleccionar una edad en su perfil"] animated:TRUE completion:^{
            
        }];
        
        return FALSE;
    }
    if(!_currentLog.igc){
        [self presentViewController:[UtilManager presentAlertWithMessage:@"Introduzca un IGC"] animated:TRUE completion:^{
        }];
        
        return FALSE;
    }
    
    return TRUE;
}

- (NSDictionary *)logDictionary{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSDictionary *logDictionary;
    
    NSMutableArray *pictures;
    
    if(_images.count > 0){
        
        pictures = [[NSMutableArray alloc] initWithCapacity:_images.count];
        
        for(UIImage *image in _images){
            NSData *imageData = UIImageJPEGRepresentation(image, 1.);
            NSString *encodeString = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
            NSDictionary *pictureDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:encodeString,@"picture", nil];
            [pictures addObject:pictureDictionary];
        }
    }
    _imagesUploads = _images.count;
    
    if(!_currentLog.dominated)
        _currentLog.dominated = [NSNumber numberWithInt:0];
    if(!_currentLog.squat)
        _currentLog.squat = [NSNumber numberWithInt:0];
    if(!_currentLog.benchPress)
        _currentLog.benchPress = [NSNumber numberWithInt:0];
    if(!_currentLog.militaryPress)
        _currentLog.militaryPress = [NSNumber numberWithInt:0];
    if(!_currentLog.deadWeight)
        _currentLog.deadWeight = [NSNumber numberWithInt:0];
    
    NSDictionary *dominatedDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:1],@"reps",_currentLog.dominated,@"weight", nil];
    
    NSDictionary *basicScoresDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:_currentLog.squat,@"squat",_currentLog.benchPress,@"benchPress",_currentLog.militaryPress,@"militaryPress", _currentLog.deadWeight,@"deadWeight",dominatedDictionary,@"dominated",nil];
    
    if(pictures.count == 0)
        logDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[NSDate date],@"revisionDate",[NSNumber numberWithFloat:_currentLog.back.floatValue],@"back",[NSNumber numberWithFloat:_currentLog.igc.floatValue],@"igc",[NSNumber numberWithFloat:_currentLog.leftBicep.floatValue],@"leftArm",[NSNumber numberWithFloat:_currentLog.leftForearm.floatValue],@"leftForeArm",[NSNumber numberWithFloat:_currentLog.leftThigh.floatValue],@"leftThigh",[NSNumber numberWithFloat:_currentLog.rightBicep.floatValue],@"rightArm",[NSNumber numberWithFloat:_currentLog.rightForearm.floatValue],@"rightForeArm",[NSNumber numberWithFloat:_currentLog.rightThigh.floatValue],@"rightThigh",[NSNumber numberWithFloat:_currentLog.waist.floatValue],@"waist",[NSNumber numberWithFloat:_currentLog.weight.floatValue],@"weight",[[AppContext sharedInstance] currentUser].userId,@"owner",basicScoresDictionary,@"basicScores", _currentLog.bmr,@"bmr",nil];
    else
        logDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[NSDate date],@"revisionDate",[NSNumber numberWithFloat:_currentLog.back.floatValue],@"back",[NSNumber numberWithFloat:_currentLog.igc.floatValue],@"igc",[NSNumber numberWithFloat:_currentLog.leftBicep.floatValue],@"leftArm",[NSNumber numberWithFloat:_currentLog.leftForearm.floatValue],@"leftForeArm",[NSNumber numberWithFloat:_currentLog.leftThigh.floatValue],@"leftThigh",[NSNumber numberWithFloat:_currentLog.rightBicep.floatValue],@"rightArm",[NSNumber numberWithFloat:_currentLog.rightForearm.floatValue],@"rightForeArm",[NSNumber numberWithFloat:_currentLog.rightThigh.floatValue],@"rightThigh",[NSNumber numberWithFloat:_currentLog.waist.floatValue],@"waist",[NSNumber numberWithFloat:_currentLog.weight.floatValue],@"weight",[[AppContext sharedInstance] currentUser].userId,@"owner",basicScoresDictionary,@"basicScores",_currentLog.bmr,@"bmr", nil];
    
    return logDictionary;
}

- (void)presentPicMainView:(id)sender{
    UITapGestureRecognizer *tapGesture = (UITapGestureRecognizer *)sender;
    NSLog(@"%@",tapGesture.accessibilityLabel);
    NSInteger tapIndex = tapGesture.accessibilityLabel.intValue;
    [_galleryView scrollToItemAtIndex:tapIndex-1 animated:TRUE];
    
    [UIView animateWithDuration:0.5 animations:^{
        [_layerView setFrame:CGRectMake((tapIndex - 1)*WIDTH/5, _layerView.frame.origin.y, _layerView.frame.size.width, _layerView.frame.size.height)];
    } completion:^(BOOL finished){
        
    }];
    
}

- (void)presentPickerView {
    
    //[self hideKeyBoards];
    
    if(_pickerView)
    {
        [_containerPickerView removeFromSuperview];
        _containerPickerView = nil;
        
        [_pickerView removeFromSuperview];
        _pickerView = nil;
        
        [_acceptButton removeFromSuperview];
        _acceptButton = nil;
    }
    
    _containerPickerView = [[UIView alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - [UtilManager height:206], WIDTH, [UtilManager height:206])];
    [_containerPickerView setBackgroundColor:[UIColor clearColor]];
    
    _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0., [UtilManager height:44], self.view.frame.size.width, [UtilManager height:162])];
    [_pickerView setBackgroundColor:BLACK_APP_COLOR];
    
    [_pickerView setUserInteractionEnabled:YES];
    [_pickerView setDelegate:self];
    [_pickerView setDataSource:self];
    _pickerView.showsSelectionIndicator = YES;
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,WIDTH,[UtilManager height:44])];
    [toolBar setBarStyle:UIBarStyleBlackTranslucent];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil)
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(acceptPickerAction)];
    [barButtonDone setTintColor:GRAY_REGISTER_FONT];
    toolBar.items = @[barButtonDone];
    [_containerPickerView addSubview:_pickerView];
    [_containerPickerView addSubview:toolBar];
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_pickerView.frame.size.width - 80., _pickerView.frame.origin.y, 80., 24.)];
    [_acceptButton setTitleColor:GRAY_REGISTER_FONT forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptPickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_containerPickerView];
}

- (void)acceptPickerAction{
    NSInteger rowSelected = [_pickerView selectedRowInComponent:0];
    
    switch (_customInputType) {
        case CustomInputTypeWeight:
            _weightLabel.text = [NSString stringWithFormat:@"%ld", rowSelected + 30];
            _bmrLabel.text = [NSString stringWithFormat:@" %.0f kCal", [User calculateBMRWithWeight:@([_weightLabel.text integerValue])
                                                                                              andAge:[AppContext sharedInstance].currentUser.age
                                                                                           andGender:[AppContext sharedInstance].currentUser.gender
                                                                                          andHeight:[AppContext sharedInstance].currentUser.height]];
            break;
        case CustomInputTypeIGC:
            _bfiLabel.text = [NSString stringWithFormat:@"%ld", rowSelected + 3];
            break;
        case CustomInputTypeSquat:
            _squatTextFieled.text = [NSString stringWithFormat:@"%ld Kg",(long)rowSelected];
            [_currentLog setSquat:[NSNumber numberWithInteger:rowSelected]];
            break;
        case CustomInputTypeBenchPress:
            _benchPresstextField.text = [NSString stringWithFormat:@"%ld Kg",(long)rowSelected];
            [_currentLog setBenchPress:[NSNumber numberWithInteger:rowSelected]];
            break;
        case CustomInputTypeMilitaryPress:
            _militaryPressTextField.text = [NSString stringWithFormat:@"%ld Kg",(long)rowSelected];
            [_currentLog setMilitaryPress:[NSNumber numberWithInteger:rowSelected]];
            break;
        case CustomInputTypeDeadWeight:
            _deadWeightTextField.text = [NSString stringWithFormat:@"%ld Kg",(long)rowSelected];
            [_currentLog setDeadWeight:[NSNumber numberWithInteger:rowSelected]];
            break;
        case CustomInputTypeDominated:
            _dominatedTextField.text = [NSString stringWithFormat:@"%ld rep.",(long)rowSelected];
            [_currentLog setDominated:[NSNumber numberWithInteger:rowSelected]];
            break;
        case CustomInputTypeBack:
            _backTextField.text = [NSString stringWithFormat:@"%ld cm",(long)rowSelected];
            [_currentLog setBack:[NSNumber numberWithInteger:rowSelected]];
            break;
        case CustomInputTypeWaist:
            _waistTextField.text = [NSString stringWithFormat:@"%ld cm",(long)rowSelected];
            [_currentLog setWaist:[NSNumber numberWithInteger:rowSelected]];
            break;
        case CustomInputTypeLeftForeArm:
            _leftForearmTextField.text = [NSString stringWithFormat:@"%ld cm",(long)rowSelected];
            [_currentLog setLeftForearm:[NSNumber numberWithInteger:rowSelected]];
            break;
        case CustomInputTypeRightForeArm:
            _rightForearmTextField.text = [NSString stringWithFormat:@"%ld cm",(long)rowSelected];
             [_currentLog setRightForearm:[NSNumber numberWithInteger:rowSelected]];
            break;
        case CustomInputTypeLeftThigh:
            _leftThighTextField.text = [NSString stringWithFormat:@"%ld cm",(long)rowSelected];
             [_currentLog setLeftThigh:[NSNumber numberWithInteger:rowSelected]];
            break;
        case CustomInputTypeRightThigh:
            _rightThighTextField.text= [NSString stringWithFormat:@"%ld cm",(long)rowSelected];
            [_currentLog setRightThigh:[NSNumber numberWithInteger:rowSelected]];
            break;
        default:
            //_inputTextField.text = [NSString stringWithFormat:@"%ld cm",(long)rowSelected];
            break;
    }
    
    [_containerPickerView removeFromSuperview];
    _containerPickerView = nil;
    
    [_pickerView  removeFromSuperview];
    _pickerView = nil;
    
    [_acceptButton removeFromSuperview];
    _acceptButton = nil;
}

- (void)infoAction{
    [self presentViewController:[UtilManager presentAlertWithMessage:@"Opción en desarrollo"] animated:YES completion:^{
    
    }];
}

#pragma mark CustomInputViewdelegate MEthods

- (void)confirmWithText:(NSString *)text andCustomInputType:(CustomInputType)customInpuType{
    
    if(!_currentLog)
        _currentLog = [[Log alloc] init];
    switch (customInpuType) {
        case CustomInputTypeBack:
        {
            [_backTextField setText:[NSString stringWithFormat:@"%@ cm.",text]];
            [_currentLog setBack:[NSNumber numberWithInteger:text.integerValue]];
        }
            break;
        case CustomInputTypeWaist:
        {
            [_waistTextField setText:[NSString stringWithFormat:@"%@ cm.",text]];
            [_currentLog setWaist:[NSNumber numberWithInteger:text.integerValue]];
        }
            break;
        case CustomInputTypeLeftForeArm:{
            [_leftForearmTextField setText:[NSString stringWithFormat:@"%@ cm.",text]];
            [_currentLog setLeftForearm:[NSNumber numberWithInteger:text.integerValue]];
        }
            break;
        case CustomInputTypeRightForeArm:{
            [_rightForearmTextField setText:[NSString stringWithFormat:@"%@ cm.",text]];
            [_currentLog setRightForearm:[NSNumber numberWithInteger:text.integerValue]];
        }
            break;
        case CustomInputTypeLeftThigh:{
            [_leftThighTextField setText:[NSString stringWithFormat:@"%@ cm.",text]];
            [_currentLog setLeftThigh:[NSNumber numberWithInteger:text.integerValue]];
        }
            break;
        case CustomInputTypeRightThigh:{
            [_rightThighTextField setText:[NSString stringWithFormat:@"%@ cm.",text]];
            [_currentLog setRightThigh:[NSNumber numberWithInteger:text.integerValue]];
        }
            break;
        case CustomInputTypeWeight:{
            [_weightLabel setText:[NSString stringWithFormat:@"%@",text]];
            [_currentLog setWeight:[NSNumber numberWithFloat:text.floatValue]];
            [_bmrLabel setText:[NSString stringWithFormat:@" %.2f kCal",[[User calculateBMRWithWeight:[NSNumber numberWithInt:text.intValue] andAge:[[AppContext sharedInstance] currentUser].age andGender:[[AppContext sharedInstance] currentUser].gender andHeight:[[AppContext sharedInstance] currentUser].height] floatValue]]];
            [_currentLog setBmr:[NSNumber numberWithFloat:[[User calculateBMRWithWeight:[NSNumber numberWithInt:text.intValue] andAge:[[AppContext sharedInstance] currentUser].age andGender:[[AppContext sharedInstance] currentUser].gender andHeight:[[AppContext sharedInstance] currentUser].height] floatValue]]];
        }
        break;
        case CustomInputTypeIGC:{
            [_bfiLabel setText:[NSString stringWithFormat:@"%@",text]];
            [_currentLog setPercent:[NSNumber numberWithFloat:text.floatValue]];
        }
            break;
        case CustomInputTypeSquat:{
            [_squatTextFieled setText:[NSString stringWithFormat:@"%@\nKg",text]];
            [_currentLog setSquat:[NSNumber numberWithFloat:text.floatValue]];
        }
            break;
        case CustomInputTypeBenchPress:{
            [_benchPresstextField setText:[NSString stringWithFormat:@"%@\nKg",text]];
            [_currentLog setBenchPress:[NSNumber numberWithFloat:text.floatValue]];
        }
            break;
        case CustomInputTypeMilitaryPress:{
            [_militaryPressTextField setText:[NSString stringWithFormat:@"%@\nKg",text]];
            [_currentLog setMilitaryPress:[NSNumber numberWithFloat:text.floatValue]];
        }
            break;
        case CustomInputTypeDeadWeight:{
            [_deadWeightTextField setText:[NSString stringWithFormat:@"%@\nKg",text]];
            [_currentLog setDeadWeight:[NSNumber numberWithFloat:text.floatValue]];
        }
            break;
        case CustomInputTypeDominated:{
            [_dominatedTextField setText:[NSString stringWithFormat:@"%@\nKg.",text]];
            [_currentLog setDominated:[NSNumber numberWithFloat:text.floatValue]];
        }
            break;
        default:
            break;
    }
    
    [_modalView didPressOKButton];
}

#pragma mark UploadViewDelegateMethods

- (void)dismissViewSelected{
    [_uploadMediaView removeFromSuperview];
}

- (void)galleryDidselected{
    _picker = [[UIImagePickerController alloc] init];
    [_picker setAccessibilityLabel:_uploadMediaView.accessibilityLabel];
    [_uploadMediaView removeFromSuperview];
    [_picker setDelegate:self];
    [_picker setAllowsEditing:YES];
    [_picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:_picker animated:YES completion:^{
        
    }];
}

- (void)cameraDidSelected{
    _picker = [[UIImagePickerController alloc] init];
    [_picker setAccessibilityLabel:_uploadMediaView.accessibilityLabel];
    [_uploadMediaView removeFromSuperview];
    [_picker setAllowsEditing:YES];
    [_picker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [_picker setDelegate:self];
    
    [self presentViewController:_picker animated:YES completion:^{
        
    }];
}

- (void)resetView:(id)sender{
    
    UIButton *textFieldButton = (UIButton *)sender;
    
    if ([textFieldButton.accessibilityLabel isEqualToString:@"rm"]) {
        _rmTextField.text = [NSString stringWithFormat:@"%@ RM", _rmTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"squat"]) {
        _squatTextFieled.text = [NSString stringWithFormat:@"%@ Kg",_squatTextFieled.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"bench"]) {
        _benchPresstextField.text = [NSString stringWithFormat:@"%@ Kg",_benchPresstextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"militaryPress"]) {
        _militaryPressTextField.text = [NSString stringWithFormat:@"%@ Kg",_militaryPressTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"deadWeight"]) {
        _deadWeightTextField.text = [NSString stringWithFormat:@"%@ Kg",_deadWeightTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"dominated"]) {
        _dominatedTextField.text = [NSString stringWithFormat:@"%@ Kg",_dominatedTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"neck"]) {
        _neckTextField.text = [NSString stringWithFormat:@"%@ cm", _neckTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"leftBicep"]) {
        _leftBicepTextField.text = [NSString stringWithFormat:@"%@ cm", _leftBicepTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"rightBicep"]) {
        _rightBicepTextField.text = [NSString stringWithFormat:@"%@ cm", _rightBicepTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"leftForeArm"]) {
        _leftForearmTextField.text = [NSString stringWithFormat:@"%@ cm",_leftForearmTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"rightForeArm"]) {
        _rightForearmTextField.text = [NSString stringWithFormat:@"%@ cm",_rightForearmTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"hip"]) {
        _hipTextField.text = [NSString stringWithFormat:@"%@ cm", _hipTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"back"]) {
        _backTextField.text = [NSString stringWithFormat:@"%@ cm",_backTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"waist"]) {
        _waistTextField.text = [NSString stringWithFormat:@"%d cm", [_waistTextField.text intValue]];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"leftThigh"]) {
        _leftThighTextField.text = [NSString stringWithFormat:@"%@ cm",_leftThighTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"rightThigh"]) {
        _rightThighTextField.text = [NSString stringWithFormat:@"%@ cm",_rightThighTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"leftLeg"]) {
        _leftLegTextField.text = [NSString stringWithFormat:@"%@ cm", _leftLegTextField.text];
    }
    else if ([textFieldButton.accessibilityLabel isEqualToString:@"rightLeg"]) {
        _rightLegTextField.text = [NSString stringWithFormat:@"%@ cm", _rightLegTextField.text];
    }

    [self.view endEditing:YES];
}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(_selectLog) {
        return NO;
    }

    if([textField isEqual:_bmrTextfield]) {
        return NO;
    }
    
    UIToolbar *toolbarWaist = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarWaist.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleSpaceWaist = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *barButtonItemWaist = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resetView:)];
    [barButtonItemWaist setAccessibilityLabel:textField.accessibilityLabel];
    [barButtonItemWaist setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barButtonItemWaist setTintColor:GRAY_REGISTER_FONT];
    [toolbarWaist setItems:[NSArray arrayWithObjects:flexibleSpaceWaist, barButtonItemWaist, nil]];
    textField.inputAccessoryView = toolbarWaist;
    
    [_scrollView addSubview:_dominatedTextField];
    
    if(!_selectLog) {
        /*
        _customInputView = [[CustomInputView alloc] initWithFrame:CGRectMake(0., HEIGHT - [UtilManager height:340], [UtilManager width:280], [UtilManager height:340])];
        [_customInputView setCenter:CGPointMake(self.view.center.x,[UtilManager height:100])];
        [_customInputView setDelegate:self];
        
        if([textField isEqual:_chestTextField]){
            [self setCustomInputType:CustomInputTypeChest];
            //[[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su pecho:", nil)];
            //[_customInputView setCustomInputType:CustomInputTypeChest];
        }else if([textField isEqual:_waistTextField]){
            [self setCustomInputType:CustomInputTypeWaist];
            //[[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su cintura:", nil)];
            //[_customInputView setCustomInputType:CustomInputTypeWaist];
        }else if([textField isEqual:_leftArmTextField]){
            [self setCustomInputType:CustomInputTypeleftArm];
            //[[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su brazo izquierdo:", nil)];
            //[_customInputView setCustomInputType:CustomInputTypeleftArm];
        }else if([textField isEqual:_rightArmTextField]){
            [self setCustomInputType:CustomInputTypeRightArm];
            //[[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su brazo derecho:", nil)];
            //[_customInputView setCustomInputType:CustomInputTypeRightArm];
        }else if([textField isEqual:_leftForearmTextField]){
            [self setCustomInputType:CustomInputTypeLeftForeArm];
            //[[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su antebrazo izquierdo:", nil)];
            //[_customInputView setCustomInputType:CustomInputTypeLeftForeArm];
        }else if([textField isEqual:_rightForearmTextField]){
            [self setCustomInputType:CustomInputTypeRightForeArm];
            //[[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su antebrazo derecho:", nil)];
            //[_customInputView setCustomInputType:CustomInputTypeRightForeArm];
        }else if([textField isEqual:_leftThighTextField]){
            //[[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su muslo izquierdo:", nil)];
            //[_customInputView setCustomInputType:CustomInputTypeLeftThigh];
            [self setCustomInputType:CustomInputTypeLeftThigh];
        }else if([textField isEqual:_rightThighTextField]){
            //[[_customInputView titleLabel] setText:NSLocalizedString(@"Medida de su muslo derecho:", nil)];
            //[_customInputView setCustomInputType:CustomInputTypeRightThigh];
            [self setCustomInputType:CustomInputTypeRightThigh];
        } else if([textField isEqual:_squatTextFieled]){
            //[[_customInputView titleLabel] setText:NSLocalizedString(@"Sentadilla", nil)];
            //[_customInputView setCustomInputType:CustomInputTypeSquat];
            [self setCustomInputType:CustomInputTypeSquat];
        } else if([textField isEqual:_benchPresstextField]){
            //[[_customInputView titleLabel] setText:NSLocalizedString(@"Press Banca", nil)];
            //[_customInputView setCustomInputType:CustomInputTypeBenchPress];
            [self setCustomInputType:CustomInputTypeBenchPress];
        } else if([textField isEqual:_militaryPressTextField]){
            //[[_customInputView titleLabel] setText:NSLocalizedString(@"Press Militar", nil)];
            //[_customInputView setCustomInputType:CustomInputTypeMilitaryPress];
            [self setCustomInputType:CustomInputTypeMilitaryPress];
        } else if([textField isEqual:_deadWeightTextField]){
            //[[_customInputView titleLabel] setText:NSLocalizedString(@"Peso muerto", nil)];
            //[_customInputView setCustomInputType:CustomInputTypeDeadWeight];
            [self setCustomInputType:CustomInputTypeDeadWeight];
        } else if ([textField isEqual:_dominatedTextField]){
            //[[_customInputView titleLabel] setText:NSLocalizedString(@"Dominadas", nil)];
            //[_customInputView setCustomInputType:CustomInputTypeDominated];
            [self setCustomInputType:CustomInputTypeDominated];
        }
        
        //_modalView  = [[RNBlurModalView alloc] initWithViewController:self view:_customInputView];
        //[_modalView show];
        
        //[self presentPickerView];
         */
    }
    
    return TRUE;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [textField selectAll:self];
    [UIMenuController sharedMenuController].menuVisible = NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    if([string isEqualToString:@"."] && textField.text.length == 0)
        return FALSE;
    
    NSUInteger newLength;
    
    newLength = [textField.text length] + [string length] - range.length;
    
    return (newLength > 3) ? NO : YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return TRUE;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return TRUE;
}


#pragma mark UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    _logImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    NSInteger photoInteger = picker.accessibilityLabel.integerValue;
    
    if(_images.count == 0)
        [_images addObject:_logImage];
    else if(_galleryView.currentItemIndex >= _images.count)
        [_images addObject:_logImage];
    else
        [_images replaceObjectAtIndex:_galleryView.currentItemIndex withObject:_logImage];
    [_galleryView reloadData];
    [_pageControl setCurrentPage:_galleryView.currentItemIndex];
    if(_images.count < 5)
        [_pageControl setNumberOfPages:_images.count + 1];
    
    /*
    if(_images.count == 0)
        [_images addObject:_logImage];
    else if (_images.count < photoInteger)
        [_images addObject:_logImage];
    else
        [_images replaceObjectAtIndex:photoInteger-1 withObject:_logImage];
    
    switch (photoInteger) {
        case 1:
            [_mainImageView setImage:_logImage];
            [_secondImageView setUserInteractionEnabled:TRUE];
            break;
        case 2:
            [_secondImageView setImage:_logImage];
            [_thirdImageView setUserInteractionEnabled:TRUE];
            break;
        case 3:
            [_thirdImageView setImage:_logImage];
            [_fourImageView setUserInteractionEnabled:TRUE];
            break;
        case 4:
            [_fourImageView setImage:_logImage];
            [_fiveImageView setUserInteractionEnabled:TRUE];
            break;
        case 5:
            [_fiveImageView setImage:_logImage];
            break;
        default:
            break;
    }
     */
    
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark GalleryViewDataSource methods

- (NSUInteger)numberOfItemsInGallery:(GalleryView *)Gallery{
    
    if(_selectLog)
        return _images.count;
    else{
        if(_images.count == 0)
            return 1;
        else if (_images.count == 5)
            return 5;
        else
            return _images.count + 1;
    }
}

- (NSUInteger)numberOfPlaceholdersInGallery:(GalleryView *)Gallery
{
    return 0;
}

- (CGFloat)gallery:(GalleryView *)Gallery valueForOption:(GalleryOption)option withDefault:(CGFloat)value
{
    switch (option){
        case GalleryOptionWrap:
            return true;
        case GalleryOptionFadeMin:
            return -0.2;
        case GalleryOptionFadeMax:
            return 0.2;
        case GalleryOptionFadeRange:
            return 1.5;
        default:
            break;
    }
    return value * 1.05;
}


- (UIView *)gallery:(GalleryView *)Gallery viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view{
    
    LogImageCarouselCell *logImageCarouselCell = (LogImageCarouselCell *)view;
    
    if(logImageCarouselCell == nil){
    
        logImageCarouselCell = [[LogImageCarouselCell alloc] initWithFrame:CGRectMake([UtilManager width:30], [UtilManager height:0], WIDTH, WIDTH * 0.5625)];
        [[logImageCarouselCell logImageView] setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        UITapGestureRecognizer *imageTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentPicView)];
        if(!_selectLog)
            [logImageCarouselCell addGestureRecognizer:imageTapGesture];
        
        if(_selectLog){
            __weak UIImageView *imageView = logImageCarouselCell.logImageView;
            
            NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,[_images objectAtIndex:index]]]
                                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                          timeoutInterval:60];
            
            [logImageCarouselCell.logImageView setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                [imageView setImage:image];
                [imageView setContentMode:UIViewContentModeScaleAspectFill];
                [[imageView layer] setMasksToBounds:YES];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                [imageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
            }];
            
        }else{
            
            if(_images.count != 0 && index < _images.count)
                [[logImageCarouselCell logImageView] setImage:[_images objectAtIndex:index]];
            else
                [[logImageCarouselCell logImageView]  setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        }
    }else{
        if(_selectLog){
            
            __weak UIImageView *imageView = logImageCarouselCell.logImageView;
            
            [logImageCarouselCell.logImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,[_images objectAtIndex:index]]]] placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                [imageView setImage:image];
                [imageView setContentMode:UIViewContentModeScaleAspectFill];
                [[imageView layer] setMasksToBounds:YES];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                [imageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
            }];
            
        }else{
            
            if(_images.count != 0 && index < _images.count)
                [[logImageCarouselCell logImageView] setImage:[_images objectAtIndex:index]];
            else
                [[logImageCarouselCell logImageView]  setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        }
        
    }
    
    [logImageCarouselCell setIndex:index];
    
    return logImageCarouselCell;
}

#pragma mark UIPickerDataSource Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (_customInputType) {
        case CustomInputTypeWeight:
            return 470;
            break;
        case CustomInputTypeIGC:
            return 72;
            break;
        default:
            return 500;
            break;
    }
    return 470;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title;
    
    if(!_currentLog)
        _currentLog = [[Log alloc] init];
    switch (_customInputType) {
        case CustomInputTypeWeight:
            title = [NSString stringWithFormat:@"%ld Kg",row + 30];
            break;
        case CustomInputTypeIGC:
            title = [NSString stringWithFormat:@"%ld",row + 3];
            break;
        case CustomInputTypeSquat:
            title = [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        case CustomInputTypeBenchPress:
            title = [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        case CustomInputTypeMilitaryPress:
            title = [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        case CustomInputTypeDeadWeight:
            title = [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        case CustomInputTypeDominated:
            title = [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        default:
            title = [NSString stringWithFormat:@"%ld cm",(long)row];
            break;
    }
    NSAttributedString *attString =
    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:YELLOW_APP_COLOR}];
    
    return attString;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(!_currentLog)
        _currentLog = [[Log alloc] init];
    switch (_customInputType) {
        case CustomInputTypeWeight:
            return [NSString stringWithFormat:@"%ld Kg",row + 30];
            break;
        case CustomInputTypeIGC:
            return [NSString stringWithFormat:@"%ld",row + 3];
            break;
        case CustomInputTypeSquat:
            return [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        case CustomInputTypeBenchPress:
            return [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        case CustomInputTypeMilitaryPress:
            return [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        case CustomInputTypeDeadWeight:
            return [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        case CustomInputTypeDominated:
            return [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        default:
            return [NSString stringWithFormat:@"%ld cm",(long)row];
            break;
    }
    return [NSString stringWithFormat:@"%ld Kg",row + 30];
}

#pragma mark GalleryViewDelegate methods

- (void)gallery:(GalleryView *)Gallery didSelectItemAtIndex:(NSInteger)index
{
    PictureVisorViewController  *pictureVisorViewController = [[PictureVisorViewController alloc] init];
    [pictureVisorViewController setPicImages:_images];
    [pictureVisorViewController setPicNumber:(int)_images.count];
    [self.navigationController pushViewController:pictureVisorViewController animated:YES];
}

- (void)galleryDidScroll:(GalleryView *)Gallery{
    [_pageControl setCurrentPage:_galleryView.currentItemIndex];
    
    [UIView animateWithDuration:0.5 animations:^{
        [_layerView setFrame:CGRectMake((_galleryView.currentItemIndex)*WIDTH/5, _layerView.frame.origin.y, _layerView.frame.size.width, _layerView.frame.size.height)];
    } completion:^(BOOL finished){
        
    }];
}

- (void)galleryDidEndDecelerating:(GalleryView *)Gallery{
    NSLog(@"TTT");
}

- (void)galleryDidEndScrollingAnimation:(GalleryView *)Gallery{
    [UIView animateWithDuration:0.5 animations:^{
        [_layerView setFrame:CGRectMake((_galleryView.currentItemIndex)*WIDTH/5, _layerView.frame.origin.y, _layerView.frame.size.width, _layerView.frame.size.height)];
    } completion:^(BOOL finished){
        
    }];
}

#pragma mark WebServiceDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        
    }];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    if(webServiceType != WebServiceTypeUploadLogImage)
        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    else{
        _imagesUploads --;
        
        if(_imagesUploads == 0)
            [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
    }
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    if(webServiceType != WebServiceTypeUploadLogImage){
    
        NSDictionary *returnDictionary = (NSDictionary *)object;
        NSDictionary *logDictionary = [returnDictionary valueForKey:@"data"];
        
        if(_images.count >= 1 && _imagesUploads != 0){
            int index = 0;
            for(UIImage *image in _images){
                
                NSMutableDictionary *pictureDictionary  = [[NSMutableDictionary alloc] init];
                
                if(index == 0)
                    [pictureDictionary setValue:[NSNumber numberWithBool:TRUE] forKey:@"featured"];
                
                NSData *imageData = UIImageJPEGRepresentation(image, 1.);
                NSString *encodeString = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
                [pictureDictionary setValue:encodeString forKey:@"picture"];
                [pictureDictionary setValue:[logDictionary valueForKey:@"_id"] forKey:@"userRevisionId"];
                
                [WebServiceManager uploadImage:pictureDictionary withDelegate:self];
            
                index ++;
            }
        }else {
            [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
    } else {
        _imagesUploads --;
        
        if(_imagesUploads == 0){
            Log *lastLog = [Log getLastLog];
            
            [WebServiceManager getUserRevision:lastLog.logId andDelegate:self];
        }
    }
}

#pragma mark - LogDetailDisplay

- (Log*)logFromDisplay {
    Log * log;
    if (_isEditing) {
        log = _currentLog;
    }
    else {
        log = [Log duplicateLog:_currentLog];
    }
    
    log.weight = @([_weightLabel.text floatValue]);
    log.igc = @([_bfiLabel.text floatValue]);
    log.bmr = [User calculateBMRWithWeight:@([_weightLabel.text integerValue])
                                    andAge:[AppContext sharedInstance].currentUser.age
                                 andGender:[AppContext sharedInstance].currentUser.gender
                                 andHeight:[AppContext sharedInstance].currentUser.height];
    
    log.rm = @([_rmTextField.text floatValue]);
    if ([AppContext sharedInstance].currentUser.height) {
        log.height = [AppContext sharedInstance].currentUser.height;
    }
    else {
        log.height = @175;
    }
    
    log.neck = @([_neckTextField.text intValue]);
    log.back = @([_backTextField.text intValue]);
    log.leftBicep = @([_leftBicepTextField.text intValue]);
    log.rightBicep = @([_rightBicepTextField.text intValue]);
    log.leftForearm = @([_leftForearmTextField.text intValue]);
    log.rightForearm = @([_rightForearmTextField.text intValue]);
    log.hip = @([_hipTextField.text intValue]);
    log.waist = @([_waistTextField.text intValue]);
    log.leftThigh = @([_leftThighTextField.text intValue]);
    log.rightThigh = @([_rightThighTextField.text intValue]);
    log.leftLeg = @([_leftLegTextField.text intValue]);
    log.rightLeg = @([_rightLegTextField.text intValue]);
    
    return log;
}

- (void)dismissWithLog:(Log*)log {
    if (_isEditing) {
        [_delegate logDetailViewController:self
                         didPressUpdateLog:log
                                     image:_mainImageView.image];
    }
    else {
        [_delegate logDetailViewController:self
                         didPressCreateLog:log
                                     image:_mainImageView.image];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
