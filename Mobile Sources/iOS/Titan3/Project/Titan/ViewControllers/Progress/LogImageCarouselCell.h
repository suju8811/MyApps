//
//  LogImageCarouselCell.h
//  Titan
//
//  Created by Manuel Manzanera on 26/4/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogImageCarouselCell : UIView

@property (nonatomic, strong) UIImageView *logImageView;
@property (nonatomic, assign) NSInteger index;

@end
