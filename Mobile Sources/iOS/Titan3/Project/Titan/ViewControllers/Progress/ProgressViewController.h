//
//  ProgressViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ProgressExecutive.h"
#import "ParentViewController.h"

@interface ProgressViewController : ParentViewController

@property (nonatomic, assign) ProgressSelectType selectType;

@end
