//
//  ComparePicsViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 8/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "Log.h"

@interface ComparePicsViewController : ParentViewController

@property (nonatomic, strong) Log *firstLog;
@property (nonatomic, strong) Log *lastLog;

@end
