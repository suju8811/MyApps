//
//  EvolutionStressViewController.h
//  Titan
//
//  Created by Marcus Lee on 9/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "EvolutionViewController.h"
#import "EvolutionStressViewController.h"

@interface EvolutionStressViewController : EvolutionViewController

@property (nonatomic, strong) UIView * compareView;
@property (nonatomic, strong) UIImageView *siluetFrontImageView;
@property (nonatomic, strong) UIImageView *siluetBackImageView;
@property (nonatomic, strong) UIView * separatorSecondView;

@end
