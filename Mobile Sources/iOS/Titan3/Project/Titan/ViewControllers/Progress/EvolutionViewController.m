//
//  EvolutionViewController.m
//  Titan
//
//  Created by Marcus Lee on 7/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "EvolutionViewController.h"
#import "SchedulePlanTableViewController.h"
#import "LogsTableViewController.h"
#import "TrainingPlanDetailViewController.h"
#import "NutritionPlanDetailViewController.h"

#import "YelloFilledCircleView.h"
#import "LogDetailViewController.h"
#import "RoundYellowBorderView.h"
#import "YellowBlackSwitchButton.h"

@interface EvolutionViewController () <LogsTableViewControllerDelegate, SchedulePlanTableViewControllerDelegate, PNChartDelegate>

@end

@implementation EvolutionViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)configureDateViews {
    _datesView = [[UIView alloc] initWithFrame:CGRectMake(0.f,
                                                          NAV_HEIGHT + [UtilManager height:10],
                                                          WIDTH,
                                                          [UtilManager height:60])];
    
    YelloFilledCircleView *circleSeparatorView
    = [[YelloFilledCircleView alloc] initWithFrame:CGRectMake(0.f,
                                                              0.f,
                                                              [UtilManager height:6.5],
                                                              [UtilManager height:6.5])];
    
    [circleSeparatorView setCenter:CGPointMake(self.view.center.x - [UtilManager width:25], 0)];
    
    [_datesView addSubview:circleSeparatorView];
    
    _startDateLabel = [[LabelRegular18Yellow alloc] initWithFrame:CGRectMake([UtilManager width:0],
                                                                             [UtilManager height:0],
                                                                             CGRectGetMinX(circleSeparatorView.frame) - [UtilManager width:10],
                                                                             [UtilManager height:25])];
    
    _startDateLabel.textAlignment = NSTextAlignmentRight;
    _startDateLabel.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *startDateGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didPressDatePicker:)];
    startDateGesture.accessibilityLabel = @"startDate";
    [_startDateLabel addGestureRecognizer:startDateGesture];
    
    [_datesView addSubview:_startDateLabel];
    
    _endDateLabel = [[LabelRegular18Yellow alloc] initWithFrame:CGRectMake(CGRectGetMaxX(circleSeparatorView.frame) + [UtilManager width:10],
                                                                           CGRectGetMinY(_startDateLabel.frame),
                                                                           CGRectGetWidth(_startDateLabel.frame),
                                                                           [UtilManager height:25])];
    
    _endDateLabel.textAlignment =  NSTextAlignmentLeft;
    _endDateLabel.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *endDateGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didPressDatePicker:)];
    [endDateGesture setAccessibilityLabel:@"endDate"];
    [_endDateLabel addGestureRecognizer:endDateGesture];
    
    [circleSeparatorView setCenter:CGPointMake(circleSeparatorView.center.x, _startDateLabel.center.y)];
    
    [_datesView addSubview:_endDateLabel];
    
    UIButton * calendarButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(_datesView.frame) - [UtilManager width:50],
                                                                           0.f,
                                                                           [UtilManager width:35],
                                                                           [UtilManager width:35])];
    
    [calendarButton setImage:[[UIImage imageNamed:@"icon-calendar"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [calendarButton setTintColor:YELLOW_APP_COLOR];
    [calendarButton setCenter:CGPointMake(calendarButton.center.x, _endDateLabel.center.y)];
    [calendarButton addTarget:self action:@selector(didPressCalendarButton) forControlEvents:UIControlEventTouchUpInside];
    
    [_datesView addSubview:calendarButton];
    
    [_scrollView addSubview:_datesView];
}

- (void)configureSegmentControlView {
    _segmentControlView = [[UIView alloc] initWithFrame:CGRectMake(0.f,
                                                                   CGRectGetMaxY(self.datesView.frame),
                                                                   WIDTH,
                                                                   TAB_BAR_HEIGHT)];
    
    _segmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Plan de entrenamiento", nil),
                                                                  NSLocalizedString(@"Plan de nutrición", nil)]];
    
    [_segmentControl addTarget:self action:@selector(changeChartView:) forControlEvents:UIControlEventValueChanged];
    _segmentControl.backgroundColor = BLACK_APP_COLOR;
    _segmentControl.center = CGPointMake(_segmentControlView.center.x,
                                         CGRectGetHeight(_segmentControlView.frame)/2);
    
    _segmentControl.tintColor = YELLOW_APP_COLOR;
    _segmentControlView.clipsToBounds = YES;
    [_segmentControlView addSubview:_segmentControl];
    
    [self.scrollView addSubview:_segmentControlView];
}

- (void)configureChartView {
    _chartView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                          CGRectGetMaxY(_segmentControlView.frame),
                                                          SCREEN_WIDTH,
                                                          [UtilManager height:170])];
    
    //
    // Add Chart view
    //
    
    _lineChart = [[PNLineChart alloc] initWithFrame:CGRectMake(0,
                                                               0,
                                                               CGRectGetWidth(_chartView.frame),
                                                               CGRectGetHeight(_chartView.frame) - [UtilManager width:40])];
    _lineChart.backgroundColor = UIColor.clearColor;
    _lineChart.xLabelColor = UIColor.whiteColor;
    _lineChart.yLabelColor = UIColor.whiteColor;
    _lineChart.displayAnimated = NO;
    _lineChart.showSmoothLines = YES;
    [_chartView addSubview:_lineChart];
    
    //
    // Add data selection buttons
    //
    
    [self configureChartSelectionView];
    
    //
    // Add Chart View to view
    //
    
    [self.scrollView addSubview:_chartView];
}

- (void)configureChartSelectionView {
    RoundYellowBorderView * selectionButtonsView = [[RoundYellowBorderView alloc] initWithFrame:CGRectMake(5,
                                                                                                                                            CGRectGetMaxY(_lineChart.frame),
                                                                                                                                            SCREEN_WIDTH * .8f,
                                                                                                                                            [UtilManager height:30])];
    
    selectionButtonsView.center = CGPointMake(self.view.center.x, selectionButtonsView.center.y);
    NSArray * buttonsTitle = @[@"PESO", @"IMG", @"TMB", @"KCal", @"Dots"];
    _chartDataButtons = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index < buttonsTitle.count; index++) {
        YellowBlackSwitchButton * switchButton = [[YellowBlackSwitchButton alloc] initWithFrame:CGRectMake(index * CGRectGetWidth(selectionButtonsView.frame) / buttonsTitle.count,
                                                                                                           0,
                                                                                                           CGRectGetWidth(selectionButtonsView.frame) / buttonsTitle.count,
                                                                                                           CGRectGetHeight(selectionButtonsView.frame))];
        
        [switchButton setTitle:buttonsTitle[index] forState:UIControlStateNormal];
        [switchButton setTitle:buttonsTitle[index] forState:UIControlStateSelected];
        switchButton.tag = index;
        [switchButton addTarget:self action:@selector(didPressSwitchChartButton:) forControlEvents:UIControlEventTouchUpInside];
        [_chartDataButtons addObject:switchButton];
        [selectionButtonsView addSubview:switchButton];
    }
    
    [_chartView addSubview:selectionButtonsView];
}

#pragma mark - Actions

- (void)changeChartView:(UISegmentedControl *)segmentControl {
    UISegmentedControl *currentSegmentControl = segmentControl;
    [_executive didPressSegment:currentSegmentControl.selectedSegmentIndex];
}

- (void)didPressSwitchChartButton:(UIButton*)sender {
    [_executive didPressChartSelectionButton:sender.tag];
}

- (void)didPressAcceptDate:(UIButton*)sender {
    [self.executive didPressAccpetDate:sender.accessibilityLabel
                          selectedDate:self.datePicker.date];
}

- (void)didPressDatePicker:(UITapGestureRecognizer*)sender {
    [_executive didPressDatePicker:sender.accessibilityLabel];
}

- (void)didPressCalendarButton {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"Seleccione el tipo que desea", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSArray * calendarOptions = @[@"Fechas", @"Logs", @"Planes"];
    for (NSInteger index = 0; index < calendarOptions.count; index++) {
        UIAlertAction * alertAction = [UIAlertAction actionWithTitle:NSLocalizedString(calendarOptions[index], nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self.executive didPressCalendarIndex:index];
        }];
        
        [alertController addAction:alertAction];
    }
    
    UIAlertAction *cancelAlertAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancelar", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    
    [alertController addAction:cancelAlertAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - EvolutionDisplay

- (void)presentDatePicker:(NSString*)accesibilityLabel
             selectedDate:(NSDate*)selectedDate
                  maxDate:(NSDate*)maxDate
                  minDate:(NSDate*)minDate {
    
    [_datePicker setAccessibilityLabel:accesibilityLabel];
    
    if (_datePicker) {
        [_datePicker removeFromSuperview];
        _datePicker = nil;
        
        [_acceptButton removeFromSuperview];
        _acceptButton = nil;
        
        [_containerPickerView removeFromSuperview];
        _containerPickerView = nil;
    }
    
    _containerPickerView = [[UIView alloc] initWithFrame:CGRectMake(0.f,
                                                                    HEIGHT - [UtilManager height:206],
                                                                    WIDTH,
                                                                    [UtilManager height:206])];
    
    _containerPickerView.backgroundColor = UIColor.clearColor;
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.f,
                                                                 [UtilManager height:44],
                                                                 CGRectGetWidth(self.view.frame),
                                                                 [UtilManager height:162])];
    
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _datePicker.backgroundColor = BLACK_APP_COLOR;
    _datePicker.maximumDate = maxDate;
    _datePicker.minimumDate = minDate;
    _datePicker.date = selectedDate;
    _datePicker.userInteractionEnabled = YES;
    [_datePicker setValue:YELLOW_APP_COLOR forKey:@"textColor"];
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0.f, 0.f, WIDTH, [UtilManager height:44])];
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil)
                                                                      style:UIBarButtonItemStyleDone
                                                                     target:self
                                                                     action:@selector(didPressAcceptDate:)];
    
    barButtonDone.accessibilityLabel = accesibilityLabel;
    barButtonDone.tintColor = GRAY_REGISTER_FONT;
    toolBar.items = @[barButtonDone];
    [_containerPickerView addSubview:_datePicker];
    [_containerPickerView addSubview:toolBar];
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(_datePicker.frame) - 80.f,
                                                               CGRectGetMinY(_datePicker.frame),
                                                               80.f,
                                                               24.)];
    
    [_acceptButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(didPressAcceptDate:) forControlEvents:UIControlEventTouchUpInside];
    _acceptButton.accessibilityLabel = accesibilityLabel;
    
    [self.view addSubview:_containerPickerView];
}

#pragma mark - EvolutionDisplay

- (void)presentLogsSelectorViewController:(Log*)log
                            accessibility:(NSString*)accessibilityLabel {
    
    LogsTableViewController * logsSelectorViewController = [[LogsTableViewController alloc] init];
    logsSelectorViewController.view.accessibilityLabel = accessibilityLabel;
    logsSelectorViewController.delegate = self;
    [self presentViewController:logsSelectorViewController animated:YES completion:nil];
}

- (void)presentPlanSelectorViewController:(SchedulePlan*)plan
                             trainingPlan:(BOOL)trainingPlan
                            accessibility:(NSString*)accessibilityLabel {
    
    SchedulePlanTableViewController * planSelectorViewController = [[SchedulePlanTableViewController alloc] init];
    planSelectorViewController.view.accessibilityLabel = accessibilityLabel;
    planSelectorViewController.delegate = self;
    if (trainingPlan) {
        planSelectorViewController.schedulePlanType = SchedulePlanTypeTrainingPlan;
    }
    else {
        planSelectorViewController.schedulePlanType = SchedulePlanTypeNutritionPlan;
    }
    [self presentViewController:planSelectorViewController animated:YES completion:nil];
}

- (void)presentLogDetailViewController:(Log *)log {
    LogDetailViewController *logDetailViewController = [[LogDetailViewController alloc] init];
    logDetailViewController.isEditing = YES;
    logDetailViewController.selectLog = log;
    [self.navigationController pushViewController:logDetailViewController animated:YES];
}

- (void)presentPlanDetailViewController:(RLMObject*)plan {
    if ([plan isKindOfClass:[TrainingPlan class]]) {
        TrainingPlanDetailViewController * trainingDetailViewController = [[TrainingPlanDetailViewController alloc] init];
        trainingDetailViewController.trainingPlan = (TrainingPlan*)plan;
        [self.navigationController pushViewController:trainingDetailViewController animated:YES];
    }
    else if ([plan isKindOfClass:[NutritionPlan class]]) {
        NutritionPlanDetailViewController * nutritionPlanDetailViewController = [[NutritionPlanDetailViewController alloc] init];
        nutritionPlanDetailViewController.currentNutritionPlan = (NutritionPlan*)plan;
        [self.navigationController pushViewController:nutritionPlanDetailViewController animated:YES];
    }
}

- (void)drawChart:(NSArray *)xLabels
   selectionIndex:(NSInteger)chartSelectionIndex
            color:(UIColor*)chartColor
          yValues:(NSArray*)yValues
   focusedIndices:(NSArray *)focusedIndices {
    
    //
    // Show chart data selection
    //
    
    for (UIButton * switchButton in _chartDataButtons) {
        if (switchButton.tag == chartSelectionIndex) {
            switchButton.selected = YES;
        }
        else {
            switchButton.selected = NO;
        }
    }
    
    //
    // Draw chart
    //
    if (!xLabels || xLabels.count == 0) {
        _lineChart.hidden = YES;
        return;
    }
    
    _lineChart.hidden = NO;
    PNLineChartData * chartData = [[PNLineChartData alloc] init];
    _lineChart.xLabels = [xLabels copy];
    _lineChart.delegate = self;
    _lineChart.showCoordinateAxis = YES;
    chartData.itemCount = xLabels.count;
    chartData.inflexionPointStyle = PNLineChartPointStyleCircle;
    chartData.color = chartColor;
    chartData.getData = ^(NSUInteger index) {
        PNLineChartDataItem * lineChartDataItem = [PNLineChartDataItem dataItemWithY:[yValues[index] intValue]];
        lineChartDataItem.inflectionPointColor = chartColor;
        lineChartDataItem.inflectionWidth = 5;
        for (NSNumber * focusedIndex in focusedIndices) {
            if (focusedIndex.integerValue == index) {
                lineChartDataItem.inflectionPointColor = UIColor.redColor;
                lineChartDataItem.inflectionWidth = 8;
                break;
            }
        }
        return lineChartDataItem;
    };
    
    _lineChart.chartData = @[chartData];
    [_lineChart strokeChart];
}

#pragma mark - LogsTableViewControllerDelegate

- (void)logsTableViewController:(LogsTableViewController*)logsTableViewController
                   didSelectLog:(Log*)log
                  accessibility:(NSString *)accessibilityLabel {
    
    [_executive didSelectLog:log accessibility:accessibilityLabel];
}

#pragma mark - SchedulePlanTableViewControllerDelegate

- (void)schedulePlanTableViewController:(SchedulePlanTableViewController*)schedulePlanTableViewController
                  didSelectSchedulePlan:(SchedulePlan*)schedulePlan
                          accessibility:(NSString*)accessibilityLabel {
    
    [_executive didSelectPlan:schedulePlan accessibility:accessibilityLabel];
}

#pragma mark PNChart Delegate MEthods

- (void)userClickedOnLinePoint:(CGPoint )point lineIndex:(NSInteger)lineIndex {
    [self.executive didPressDotOfChart:lineIndex];
}

- (void)userClickedOnLineKeyPoint:(CGPoint)point lineIndex:(NSInteger)lineIndex pointIndex:(NSInteger)pointIndex {
    //    [self.executive didPressDotOfChart:pointIndex];
}

@end
