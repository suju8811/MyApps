//
//  LogImageCarouselCell.m
//  Titan
//
//  Created by Manuel Manzanera on 26/4/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "LogImageCarouselCell.h"
#import "UtilManager.h"
#import "AppContext.h"
#import "AppConstants.h"

@implementation LogImageCarouselCell

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        
        [self setBackgroundColor:[UIColor clearColor]];
        
        _logImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,WIDTH, WIDTH * 0.5625)];
        [_logImageView setContentMode:UIViewContentModeScaleAspectFill];
        [[_logImageView layer] setMasksToBounds:YES];
        
        [self addSubview:_logImageView];
        
    }
    
    return self;
}

@end
