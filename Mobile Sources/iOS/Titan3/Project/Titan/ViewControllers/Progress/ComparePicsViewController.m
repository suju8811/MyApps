//
//  ComparePicsViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 8/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ComparePicsViewController.h"
#import "UIImageView+AFNetworking.h"
#import "GalleryView.h"
#import "LogImageCarouselCell.h"

@interface ComparePicsViewController ()<GalleryDelegate, GalleryDataSource>

@property (nonatomic, strong) GalleryView *firstGalleryView;
@property (nonatomic, strong) GalleryView *lastGalleryView;
@property (nonatomic, strong) UIImageView *firstImageView;
@property (nonatomic, strong) UIImageView *lastImageView;

@property (nonatomic, strong) NSMutableArray *firstImages;
@property (nonatomic, strong) NSMutableArray *lastImages;

@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation ComparePicsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _firstImages = [[NSMutableArray alloc] initWithCapacity:0];
    _lastImages = [[NSMutableArray alloc] initWithCapacity:0];
    
    if(_firstLog.picture1)
        [_firstImages addObject:_firstLog.picture1];
    if(_firstLog.picture2)
        [_firstImages addObject:_firstLog.picture2];
    if(_firstLog.picture3)
        [_firstImages addObject:_firstLog.picture3];
    if(_firstLog.picture4)
        [_firstImages addObject:_firstLog.picture4];
    if(_firstLog.picture5)
        [_firstImages addObject:_firstLog.picture5];
    if(_firstLog.picture6)
        [_firstImages addObject:_firstLog.picture6];
    
    if(_lastLog.picture1)
        [_lastImages addObject:_lastLog.picture1];
    if(_lastLog.picture2)
        [_lastImages addObject:_lastLog.picture2];
    if(_lastLog.picture3)
        [_lastImages addObject:_lastLog.picture3];
    if(_lastLog.picture4)
        [_lastImages addObject:_lastLog.picture4];
    if(_lastLog.picture5)
        [_lastImages addObject:_lastLog.picture5];
    if(_lastLog.picture6)
        [_lastImages addObject:_lastLog.picture6];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, HEIGHT)];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 50.,[UtilManager height:30.], [UtilManager width:40], [UtilManager width:40])];
    [backButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton setHidden:FALSE];
    [_scrollView addSubview:backButton];
    
    _firstGalleryView = [[GalleryView alloc] initWithFrame:CGRectMake(0, NAV_HEIGHT, WIDTH, [UtilManager height:80])];
    [_firstGalleryView setType:GalleryTypeLinear];
    [_firstGalleryView setDelegate:self];
    [_firstGalleryView setDataSource:self];
    [_firstGalleryView setBackgroundColor:[UIColor clearColor]];
    
    [_scrollView addSubview:_firstGalleryView];
    
    _firstImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:10], _firstGalleryView.frame.origin.y + _firstGalleryView.frame.size.height + [UtilManager height:10], WIDTH - [UtilManager width:20], [UtilManager height:260])];
    [_firstImageView setContentMode:UIViewContentModeScaleAspectFit];
   
    __weak UIImageView *progressHomeImageView = _firstImageView;
    
    NSURLRequest *homeImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,_firstLog.picture1]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
    
    [_firstImageView setImageWithURLRequest:homeImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [progressHomeImageView setImage:image];
        [[progressHomeImageView layer] setMasksToBounds:YES];
        [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        
        [progressHomeImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        [[progressHomeImageView layer] setMasksToBounds:YES];
        [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    }];
    
    [_scrollView addSubview:_firstImageView];
    
    _lastImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_firstImageView.frame.origin.x, _firstImageView.frame.size.height + [UtilManager height:10] + _firstImageView.frame.origin.y,_firstImageView.frame.size.width, _firstImageView.frame.size.height)];
    [_lastImageView setContentMode:UIViewContentModeScaleAspectFit];
    
    __weak UIImageView *lastImageView = _lastImageView;
    
    NSURLRequest *lastImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,_lastLog.picture1]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
    
    [_lastImageView setImageWithURLRequest:lastImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [lastImageView setImage:image];
        [[lastImageView layer] setMasksToBounds:YES];
        [lastImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        
        [lastImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        [[lastImageView layer] setMasksToBounds:YES];
        [lastImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    }];
    
    [_scrollView addSubview:_lastImageView];
    
    _lastGalleryView = [[GalleryView alloc] initWithFrame:CGRectMake(0, _lastImageView.frame.origin.y + _lastImageView.frame.size.height, WIDTH, [UtilManager height:80])];
    [_lastGalleryView setType:GalleryTypeLinear];
    [_lastGalleryView setDelegate:self];
    [_lastGalleryView setDataSource:self];
    [_lastGalleryView setBackgroundColor:[UIColor clearColor]];
    
    [_scrollView addSubview:_lastGalleryView];
    
    [_scrollView setContentSize:CGSizeMake(WIDTH, _lastGalleryView.frame.origin.y + _lastGalleryView.frame.size.height + [UtilManager height:80])];
    
    [self.view addSubview:_scrollView];
    
}


#pragma mark GalleryViewDataSource methods

- (NSUInteger)numberOfItemsInGallery:(GalleryView *)Gallery{
    if(Gallery == _firstGalleryView)
        return _firstImages.count;
    else
        return _lastImages.count;
        
    return 5;
}

- (NSUInteger)numberOfPlaceholdersInGallery:(GalleryView *)Gallery
{
    return 0;
}

- (CGFloat)gallery:(GalleryView *)Gallery valueForOption:(GalleryOption)option withDefault:(CGFloat)value
{
    switch (option){
        case GalleryOptionWrap:
            return true;
        case GalleryOptionFadeMin:
            return -0.2;
        case GalleryOptionFadeMax:
            return 0.2;
        case GalleryOptionFadeRange:
            return 1.5;
        default:
            break;
    }
    return value * 1.05;
}


- (UIView *)gallery:(GalleryView *)Gallery viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view{
    
    LogImageCarouselCell *logImageCarouselCell = (LogImageCarouselCell *)view;
    
    if(logImageCarouselCell == nil)
        logImageCarouselCell = [[LogImageCarouselCell alloc] initWithFrame:CGRectMake([UtilManager width:30], [UtilManager height:0],[UtilManager width:60], [UtilManager height:60])];
    
    [[logImageCarouselCell logImageView] setFrame:CGRectMake(0., 0.,[UtilManager height:60], [UtilManager height:60])];
    [[logImageCarouselCell logImageView] setImage:[UIImage imageNamed:@"img-placeholder-big"]];
    
    if([Gallery isEqual:_firstGalleryView]){
        __weak UIImageView *imageView = logImageCarouselCell.logImageView;
        
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,[_firstImages objectAtIndex:index]]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        
        [logImageCarouselCell.logImageView setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            [imageView setImage:image];
            [imageView setContentMode:UIViewContentModeScaleAspectFill];
            [[imageView layer] setMasksToBounds:YES];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            [imageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        }];
    }else {
    
        __weak UIImageView *imageView = logImageCarouselCell.logImageView;
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,[_lastImages objectAtIndex:index]]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        
        [logImageCarouselCell.logImageView setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            [imageView setImage:image];
            [imageView setContentMode:UIViewContentModeScaleAspectFill];
            [[imageView layer] setMasksToBounds:YES];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            [imageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        }];
    
    }
    
    
    [logImageCarouselCell setIndex:index];
    
    return logImageCarouselCell;

}

#pragma mark GalleryViewDelegate methods

- (void)gallery:(GalleryView *)Gallery didSelectItemAtIndex:(NSInteger)index
{
    if(Gallery == _firstGalleryView){
        __weak UIImageView *progressHomeImageView = _firstImageView;
        
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,[_firstImages objectAtIndex:index]]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        
        [_firstImageView setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            
            [progressHomeImageView setImage:image];
            [[progressHomeImageView layer] setMasksToBounds:YES];
            [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            
            [progressHomeImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
            [[progressHomeImageView layer] setMasksToBounds:YES];
            [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        }];
    } else {
    
        __weak UIImageView *progressHomeImageView = _lastImageView;
        
        NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,[_lastImages objectAtIndex:index]]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        
        [_lastImageView setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            
            [progressHomeImageView setImage:image];
            [[progressHomeImageView layer] setMasksToBounds:YES];
            [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            
            [progressHomeImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
            [[progressHomeImageView layer] setMasksToBounds:YES];
            [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        }];
    }
}

- (void)galleryDidScroll:(GalleryView *)Gallery{
    
}

#pragma mark PNChart Delegate MEthods

- (void)userClickedOnLinePoint:(CGPoint )point lineIndex:(NSInteger)lineIndex{
    
}

- (void)userClickedOnLineKeyPoint:(CGPoint )point lineIndex:(NSInteger)lineIndex andPointIndex:(NSInteger)pointIndex{
    
}


- (void)didPressBackButton {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
