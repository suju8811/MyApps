//
//  ProgressViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ProgressViewController.h"
#import "EvolutionBodyViewController.h"
#import "EvolutionPerformanceViewController.h"
#import "EvolutionStressViewController.h"
#import "MenuContainerNavController.h"
#import "UIImageView+AFNetworking.h"
#import "WebServiceManager.h"
#import "ProgressCell.h"
#import "StatistixCell.h"
#import "DateManager.h"
#import "Log.h"
#import "MenuContainerNavController.h"
#import "LogDetailViewController.h"

#define StatistixCellHeight [UtilManager height:170]

@interface ProgressViewController () <UITableViewDelegate, UITableViewDataSource, ProgressDisplay, LogDetailViewControllerDelegate>

@property (nonatomic, strong) UITableView *progressTableView;

@property (nonatomic, strong) UIButton *statistixButton;
@property (nonatomic, strong) UILabel *statistixLabel;
@property (nonatomic, strong) UIButton *logsButton;
@property (nonatomic, strong) UILabel *logsLabel;
@property (nonatomic, strong) UIButton *recordsButton;

@property (nonatomic, strong) RLMResults *items;

@property (nonatomic, strong) MenuContainerNavController *menuContainerNavController;

@property (nonatomic, strong) ProgressExecutive * executive;

@end

@implementation ProgressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _menuContainerNavController = (MenuContainerNavController *)self.navigationController;
    
    _items = [Log getLogs];
    _selectType = ProgressSelectTypeStatistix;
    
    [self configureView];
    
    _executive = [[ProgressExecutive alloc] initWithDisplay:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPressAddButton) name:NAV_RIGHT1_ACTION object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter ] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(_selectType == ProgressSelectTypeStatistix) {
        [self hideRightNavigationButton:YES];
    }
    else {
        [self hideRightNavigationButton:NO];
        _items = [Log getLogs];
        [_progressTableView reloadData];
    }
}

- (void)configureView {
    self.view.backgroundColor = BLACK_APP_COLOR;
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _progressTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., NAV_HEIGHT, WIDTH, HEIGHT - NAV_HEIGHT - FOOTER_BAR_HEIGHT) style:UITableViewStylePlain];
    [_progressTableView registerClass:[ProgressCell class] forCellReuseIdentifier:@"progressCell"];
    [_progressTableView registerClass:[StatistixCell class] forCellReuseIdentifier:@"statistixCell"];
    _progressTableView.delegate = self;
    _progressTableView.dataSource = self;
    _progressTableView.backgroundColor = UIColor.clearColor;
    _progressTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(rightSwipe)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [_progressTableView addGestureRecognizer:recognizer];
    
    [self.view addSubview:_progressTableView];
    [self.view addSubview:[self footerView]];
}

- (void)rightSwipe{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTIF_MENU_SLIDE" object:nil];
}

- (UIView *)footerView{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., HEIGHT - FOOTER_BAR_HEIGHT, WIDTH, FOOTER_BAR_HEIGHT)];
    [footerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = footerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //[footerView addSubview:blurEffectView];
    
    _statistixButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., TAB_BAR_HEIGHT * 0.6, TAB_BAR_HEIGHT * 0.6)];
    //[_statistixButton setTitle:NSLocalizedString(@"EVOLUCIÓN", nil) forState:UIControlStateNormal];
    //[_statistixButton setTitleColor:GRAY_REGISTER_FONT forState:UIControlStateNormal];
    //[_statistixButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateHighlighted];
    //[_statistixButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateSelected];
    //[[_statistixButton titleLabel] setFont:[UIFont fontWithName:REGULAR_FONT size:24]];
    [_statistixButton setImage:[UIImage imageNamed:@"icon-footer-stat"] forState:UIControlStateNormal];
    [_statistixButton setImage:[_statistixButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    [_statistixButton setTintColor:YELLOW_APP_COLOR];
    [_statistixButton setSelected:TRUE];
    [_statistixButton setCenter:CGPointMake(WIDTH/3, footerView.frame.size.height * 0.4)];
    [_statistixButton setTag:0];
    [_statistixButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_statistixButton];
    
    _statistixLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., _statistixButton.frame.origin.y + _statistixButton.frame.size.height + [UtilManager height:5], WIDTH/2, [UtilManager height:10])];
    [_statistixLabel setTextAlignment:NSTextAlignmentCenter];
    [_statistixLabel setCenter:CGPointMake(_statistixButton.center.x, _statistixLabel.center.y)];
    [_statistixLabel setText:NSLocalizedString(@"Estadísticas", nil)];
    [_statistixLabel setFont:[UIFont fontWithName:BOLD_FONT size:9]];
    [_statistixLabel setTextColor:YELLOW_APP_COLOR];
    
    [footerView addSubview:_statistixLabel];
    
    UIButton *footerStatistixButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., WIDTH/2, footerView.frame.size.height)];
    [footerStatistixButton setBackgroundColor:[UIColor clearColor]];
    [footerStatistixButton setTag:_statistixButton.tag];
    [footerStatistixButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [footerView addSubview:footerStatistixButton];
    
    //tableViewCell.icon.image = [tableViewCell.icon.image  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    //[tableViewCell.icon setTintColor:YELLOW_APP_COLOR];
    
    _logsButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH/2, 0., _statistixButton.frame.size.width, _statistixButton.frame.size.height)];
   // [_logsButton setTitle:NSLocalizedString(@"LOGS", nil) forState:UIControlStateNormal];
   // [_logsButton setTitleColor:GRAY_REGISTER_FONT forState:UIControlStateNormal];
   // [_logsButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateHighlighted];
   // [_logsButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateSelected];
   // [[_logsButton titleLabel] setFont:_statistixButton.titleLabel.font];
    [_logsButton setCenter:CGPointMake(2 * WIDTH / 3, _statistixButton.center.y)];
    [_logsButton setImage:[UIImage imageNamed:@"icon-footer-log"] forState:UIControlStateNormal];
    [_logsButton setImage:[_logsButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    [_logsButton setTintColor:YELLOW_APP_COLOR];
    
    [_logsButton setTag:1];
    [_logsButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_logsButton];
    
    _logsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., _statistixLabel.frame.origin.y, WIDTH/2, [UtilManager height:10])];
    [_logsLabel setTextAlignment:NSTextAlignmentCenter];
    [_logsLabel setCenter:CGPointMake(_logsButton.center.x, _statistixLabel.center.y)];
    [_logsLabel setText:NSLocalizedString(@"Registro", nil)];
    [_logsLabel setFont:_statistixLabel.font];
    [_logsLabel setTextColor:[UIColor whiteColor]];
    
    [footerView addSubview:_logsLabel];
    
    _recordsButton = [[UIButton alloc] initWithFrame:CGRectMake(2*WIDTH/3, 0., WIDTH/3, TAB_BAR_HEIGHT)];
    [_recordsButton setTitle:NSLocalizedString(@"RECORDS", nil) forState:UIControlStateNormal];
    [_recordsButton setTitleColor:GRAY_REGISTER_FONT forState:UIControlStateNormal];
    [_recordsButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateHighlighted];
    [_recordsButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateSelected];
    [_recordsButton setTag:2];
    [_recordsButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    //[footerView addSubview:_recordsButton];
    
    UIButton *footerLogsButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH/2, 0., WIDTH/2, footerView.frame.size.height)];
    [footerLogsButton setBackgroundColor:[UIColor clearColor]];
    [footerLogsButton setTag:1];
    [footerLogsButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [footerView addSubview:footerLogsButton];
    
    return footerView;
}

- (void)footerAction:(id)sender {
    UIButton *touchButton = (UIButton *)sender;
    
    [_executive didPressFooterIndex:touchButton.tag];
}

- (void)didPressAddButton {
    LogDetailViewController *logDetailViewController = [[LogDetailViewController alloc] init];
    logDetailViewController.isEditing = NO;
    logDetailViewController.delegate = self;
    [self.navigationController pushViewController:logDetailViewController animated:YES];
}

#pragma mark - Progress Display

- (void)hideRightNavigationButton:(BOOL)isHide {
    MenuContainerNavController *menuContainerNavController = (MenuContainerNavController *)self.navigationController;
    menuContainerNavController.rightButton1.hidden = isHide;
}

- (void)reloadProgressView:(ProgressSelectType)selectType {
    _selectType = selectType;
    switch (selectType) {
        case ProgressSelectTypeStatistix:
            _statistixButton.selected = YES;
            _logsButton.selected = NO;
            _recordsButton.selected = NO;
            _statistixLabel.textColor = YELLOW_APP_COLOR;
            _logsLabel.textColor = UIColor.whiteColor;
            _menuContainerNavController.titleLabel.text = NSLocalizedString(@"ESTADÍSTICAS", nil);
            [self hideRightNavigationButton:YES];
            break;
        case ProgressSelectTypeLogs:
            _statistixButton.selected = NO;
            _logsButton.selected = YES;
            _recordsButton.selected = NO;
            _statistixLabel.textColor = UIColor.whiteColor;
            _logsLabel.textColor = YELLOW_APP_COLOR;
            _menuContainerNavController.titleLabel.text = NSLocalizedString(@"REGISTRO", nil);
            [self hideRightNavigationButton:NO];
            break;
        case ProgressSelectTypeRecords:
            _statistixButton.selected = NO;
            _logsButton.selected = NO;
            _recordsButton.selected = YES;
            break;
        default:
            break;
    }
    
    [_progressTableView reloadData];
}

- (void)reloadLogsTable {
    [_executive didPressFooterIndex:_selectType];
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(_selectType == ProgressSelectTypeLogs) {
    
        Log *currentLog = [_items objectAtIndex:indexPath.row];
    
        LogDetailViewController *logDetailViewController = [[LogDetailViewController alloc] init];
        [logDetailViewController setSelectLog:currentLog];
        [self.navigationController pushViewController:logDetailViewController animated:YES];
    }
    else {
        UIViewController * evolutionViewController;
        switch (indexPath.row) {
            case 0:
                evolutionViewController = [[EvolutionBodyViewController alloc] init];
                break;
            case 1:
                evolutionViewController = [[EvolutionPerformanceViewController alloc] init];
                break;
            case 2:
                evolutionViewController = [[EvolutionStressViewController alloc] init];
                break;
            default:
                break;
        }
        if (evolutionViewController) {
            [self.navigationController pushViewController:evolutionViewController animated:YES];
        }
    }
}

#pragma mark UITableDatasource Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(_selectType == ProgressSelectTypeLogs) {
        Log *currentLog = [_items objectAtIndex:indexPath.row];
    
        ProgressCell *progressCell = [tableView dequeueReusableCellWithIdentifier:@"progressCell"];
    
        [progressCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
        NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
        NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute;
    
        NSDateComponents *components = [[NSCalendar currentCalendar] components:units fromDate:currentLog.revisionDate];
    
        [[progressCell dateLabel] setText:[NSString stringWithFormat:@"%ld %@ %ld",(long)components.day,[DateManager monthWithInteger:components.month],(long)components.year]];
        [[progressCell weightLabel] setText:[NSString stringWithFormat:@"%d Kg",currentLog.weight.intValue]];
        [[progressCell percentLabel] setText:[NSString stringWithFormat:@"%d %%",currentLog.igc.intValue]];
        
        NSURLRequest *homeImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture1]]
                                                          cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                      timeoutInterval:60];
        NSURLRequest *exercise1ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture2]]
                                                               cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                           timeoutInterval:60];
        NSURLRequest *exercise2ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture3]]
                                                               cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                           timeoutInterval:60];
        NSURLRequest *exercise3ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture4]]
                                                               cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                           timeoutInterval:60];
        NSURLRequest *exercise4ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture5]]
                                                               cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                           timeoutInterval:60];
        
        __weak UIImageView *progressHomeImageView = progressCell.homeImageView;
        
        [[progressCell homeImageView] setImageWithURLRequest:homeImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            
            [progressHomeImageView setImage:image];
            [[progressHomeImageView layer] setMasksToBounds:YES];
            [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            
            [progressHomeImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
            [[progressHomeImageView layer] setMasksToBounds:YES];
            [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        }];
        
        __weak UIImageView *firstImageView = progressCell.exercise1ImageView;
        
        [[progressCell exercise1ImageView] setImageWithURLRequest:exercise1ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            
            [firstImageView setImage:image];
            [[firstImageView layer] setMasksToBounds:YES];
            [firstImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            [firstImageView setImage:nil];
        }];
        
        __weak UIImageView *secondImageView = progressCell.exercise2ImageView;
        
        [[progressCell exercise2ImageView] setImageWithURLRequest:exercise2ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            
            [secondImageView setImage:image];
            [[secondImageView layer] setMasksToBounds:YES];
            [secondImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            [secondImageView setImage:nil];
        }];
        
        __weak UIImageView *thirdImageView = progressCell.exercise3ImageView;
        
        [[progressCell exercise3ImageView] setImageWithURLRequest:exercise3ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            
            [thirdImageView setImage:image];
            [[thirdImageView layer] setMasksToBounds:YES];
            [thirdImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            [thirdImageView setImage:nil];
        }];
        
        __weak UIImageView *fourImageView = progressCell.exercise4ImageView;
        
        [[progressCell exercise4ImageView] setImageWithURLRequest:exercise4ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            
            [fourImageView setImage:image];
            [[fourImageView layer] setMasksToBounds:YES];
            [fourImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            [fourImageView setImage:nil];
        }];
        return progressCell;
    } else {
        StatistixCell *statistixCell = [tableView dequeueReusableCellWithIdentifier:@"statistixCell"];
        
        switch (indexPath.row) {
            case 0:
                [[statistixCell titleLabel] setText:NSLocalizedString(@"BODY", nil)];
                [[statistixCell evoImageView] setImage:[UIImage imageNamed:@""]];
                break;
            case 1:
                [[statistixCell titleLabel] setText:NSLocalizedString(@"PERFORMANCE", nil)];
                [[statistixCell evoImageView] setImage:[UIImage imageNamed:@""]];
                break;
                
            case 2:
                [[statistixCell titleLabel] setText:NSLocalizedString(@"STRESS", nil)];
                [[statistixCell evoImageView] setImage:[UIImage imageNamed:@"img-body-muscles"]];
                break;
            default:
                break;
        }
        
        
        [statistixCell setBackgroundColor:[UIColor clearColor]];
        [[statistixCell contentView] setBackgroundColor:[UIColor clearColor]];
        [statistixCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return statistixCell;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(_selectType == ProgressSelectTypeStatistix)
        return 3;
    else
        return _items.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_selectType == ProgressSelectTypeLogs)
        return [UtilManager height:170];
    return StatistixCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_selectType == ProgressSelectTypeLogs)
        return [UtilManager height:170];
    return StatistixCellHeight;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(_selectType == ProgressSelectTypeLogs) {
        return YES;
    }
    else {
        return NO;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(_selectType == ProgressSelectTypeLogs)
    {
        if(_items.count == 0)
            return _progressTableView.frame.size.height;
    }
    
    return [UtilManager height:10];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    
    if(_selectType == ProgressSelectTypeLogs)
    {
        if(_items.count == 0)
            return _progressTableView.frame.size.height;
    }
    
    return [UtilManager height:10];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return TAB_BAR_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return TAB_BAR_HEIGHT;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, TAB_BAR_HEIGHT)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    return footerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, _progressTableView.frame.size.height)];
    
    if(_items.count == 0 && _selectType == ProgressSelectTypeLogs){
        UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:10], WIDTH - [UtilManager width:20], [UtilManager height:80])];
        [infoLabel setTextAlignment:NSTextAlignmentCenter];
        [infoLabel setNumberOfLines:0];
        [infoLabel setText:NSLocalizedString(@"Pulse el botón + para añadir un nuevo Log", nil)];
        [infoLabel setFont:[UIFont fontWithName:REGULAR_FONT size:21]];
        [infoLabel setTextColor:[UIColor whiteColor]];
        
        [headerView addSubview:infoLabel];
    }
    
    return headerView;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        Log * currentLog = [_items objectAtIndex:indexPath.row];
        [_executive deleteLog:currentLog];
    }
}

#pragma mark - LogDetailViewControllerDelegate

- (void)logDetailViewController:(LogDetailViewController*)logDetailViewController
              didPressCreateLog:(Log*)log
                          image:(UIImage*)image {
    
    [_executive createLog:log image:image];
}

- (void)logDetailViewController:(LogDetailViewController*)logDetailViewController
              didPressUpdateLog:(Log*)log
                          image:(UIImage*)image {
    
    [_executive updateLog:log image:image];
}

@end
