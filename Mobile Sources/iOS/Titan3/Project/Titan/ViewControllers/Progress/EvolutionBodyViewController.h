//
//  EvolutionViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 19/4/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "EvolutionBodyExecutive.h"
#import "EvolutionViewController.h"

@interface EvolutionBodyViewController : EvolutionViewController

@property (nonatomic, strong) UIView * compareView;
@property (nonatomic, strong) UIImageView *siluetImageView;
@property (nonatomic, strong) UIView * separatorSecondView;

@end
