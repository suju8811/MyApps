//
//  EvolutionLogCell.h
//  Titan
//
//  Created by Manuel Manzanera on 3/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EvolutionLogCell : UIView

@property (nonatomic, strong) UIImageView *exercise1ImageView;
@property (nonatomic, strong) UIImageView *exercise2ImageView;
@property (nonatomic, strong) UIImageView *exercise3ImageView;
@property (nonatomic, strong) UIImageView *exercise4ImageView;
@property (nonatomic, strong) UIImageView *homeImageView;

@property (nonatomic, strong) UIView *containerView;

@end
