//
//  LogDetailViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 2/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "LogDetailExecutive.h"

@class LogDetailViewController;

@protocol LogDetailViewControllerDelegate

- (void)logDetailViewController:(LogDetailViewController*)logDetailViewController
              didPressCreateLog:(Log*)log
                          image:(UIImage*)image;

- (void)logDetailViewController:(LogDetailViewController*)logDetailViewController
              didPressUpdateLog:(Log*)log
                          image:(UIImage*)image;

@end

@interface LogDetailViewController : ParentViewController

@property (nonatomic, assign) BOOL isEditing;
@property (nonatomic, strong) Log *selectLog;
@property (nonatomic, strong) id<LogDetailViewControllerDelegate> delegate;

@end
