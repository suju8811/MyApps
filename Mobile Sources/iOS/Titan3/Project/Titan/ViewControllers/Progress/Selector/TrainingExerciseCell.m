//
//  TrainingExerciseCell.m
//  Titan
//
//  Created by Marcus Lee on 9/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "TrainingExerciseCell.h"
#import "DateTimeUtil.h"

@implementation TrainingExerciseCell

- (void)setContent:(TrainingExercise*)trainingExercise
              date:(NSDate*)date {
    _dateLabel.text = [DateTimeUtil stringFromDate:date format:@"yyyy-MM-dd"];
    _titleLabel.text = [trainingExercise.exercises firstObject].title;
    _contentLabel.text = trainingExercise.content;
}

@end
