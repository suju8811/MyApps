//
//  TrainingExerciseCell.h
//  Titan
//
//  Created by Marcus Lee on 9/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"

@interface TrainingExerciseCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

- (void)setContent:(TrainingExercise*)trainingExercise
              date:(NSDate*)date;

@end
