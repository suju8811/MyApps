//
//  LogsTableViewController.h
//  Titan
//
//  Created by Marcus Lee on 5/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Log.h"

@class LogsTableViewController;

@protocol LogsTableViewControllerDelegate <NSObject>

- (void)logsTableViewController:(LogsTableViewController*)logsTableViewController
                   didSelectLog:(Log*)log
                  accessibility:(NSString *)accessibilityLabel;

@end

@interface LogsTableViewController : UITableViewController

@property (nonatomic, strong) id<LogsTableViewControllerDelegate> delegate;

@end
