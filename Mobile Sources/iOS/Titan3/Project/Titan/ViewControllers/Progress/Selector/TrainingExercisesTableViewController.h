//
//  TrainingExercisesTableViewController.h
//  Titan
//
//  Created by Marcus Lee on 9/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "CommonTableViewController.h"
#import "SchedulePlan.h"

@class TrainingExercisesTableViewController;

@protocol TrainingExercisesTableViewControllerDelegate

- (void)trainingExercisesTableViewController:(TrainingExercisesTableViewController*)trainingExercisesTableViewController trainingExercise:(TrainingExercise*)trainingExercise;

@end

@interface TrainingExercisesTableViewController : CommonTableViewController

@property (nonatomic, strong) NSDate * startDate;
@property (nonatomic, strong) NSDate * endDate;
@property (nonatomic, strong) id<TrainingExercisesTableViewControllerDelegate> delegate;

@end
