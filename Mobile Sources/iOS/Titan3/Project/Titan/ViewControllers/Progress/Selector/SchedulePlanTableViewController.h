//
//  SchedulePlanTableViewController.h
//  Titan
//
//  Created by Marcus Lee on 5/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "CommonTableViewController.h"
#import "SchedulePlan.h"

typedef enum : NSUInteger {
    SchedulePlanTypeTrainingPlan    = 0,
    SchedulePlanTypeNutritionPlan   = 1
} SchedulePlanType;

@class SchedulePlanTableViewController;

@protocol SchedulePlanTableViewControllerDelegate

- (void)schedulePlanTableViewController:(SchedulePlanTableViewController*)schedulePlanTableViewController
                  didSelectSchedulePlan:(SchedulePlan*)schedulePlan
                          accessibility:(NSString*)accessibilityLabel;

@end

@interface SchedulePlanTableViewController : CommonTableViewController

@property (nonatomic, assign) SchedulePlanType schedulePlanType;
@property (nonatomic, strong) id<SchedulePlanTableViewControllerDelegate> delegate;

@end
