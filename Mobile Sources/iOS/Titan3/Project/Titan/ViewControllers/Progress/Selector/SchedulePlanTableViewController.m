//
//  SchedulePlanTableViewController.m
//  Titan
//
//  Created by Marcus Lee on 5/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "SchedulePlanTableViewController.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "PlanFoodViewCell.h"
#import "PlanTitleCell.h"
#import "AppContext.h"

@interface SchedulePlanTableViewController ()

@property (nonatomic, strong) NSArray * items;
@property (nonatomic, strong) SchedulePlan * selectedSchedulePlan;

@end

@implementation SchedulePlanTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = BLACK_APP_COLOR;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    [self.tableView registerClass:[PlanFoodViewCell class] forCellReuseIdentifier:@"planCell"];
    [self.tableView registerClass:[PlanTitleCell class] forCellReuseIdentifier:@"planTitleCell"];
    
    if (_schedulePlanType == SchedulePlanTypeTrainingPlan) {
        _items = [SchedulePlan getScheduleTrainingPlans];
    }
    else {
        _items = [SchedulePlan getScheduleNutritionPlans];
    }
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_schedulePlanType == SchedulePlanTypeTrainingPlan) {
        return planTitleCellHeight;
    }
    
    return planFooodCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SchedulePlan * plan = [_items objectAtIndex:indexPath.row];
    BOOL isSelected = plan.isSelected && plan.isSelected.boolValue;
    
    if (_schedulePlanType == SchedulePlanTypeTrainingPlan) {
        PlanTitleCell *planTitleCell = [tableView dequeueReusableCellWithIdentifier:@"planTitleCell"];
        [planTitleCell.titleLabel setText:plan.plan.title];
        
        if (isSelected) {
            planTitleCell.checkImageView.hidden = NO;
        }
        else {
            planTitleCell.checkImageView.hidden = YES;
        }
        
        planTitleCell.backgroundColor = UIColor.clearColor;
        
        return planTitleCell;
    }
    else {
        PlanFoodViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"planCell"];
        NutritionPlan * nutritionPlan = plan.nutritionPlan;
        
        cell.titleLabel.text = nutritionPlan.title;
        
        cell.contentLabel.text = nutritionPlan.content;
        if(nutritionPlan.nutritionDays.count > 0) {
            cell.daysLabel.text = [NSString stringWithFormat:@"%lu %@",
                                   (unsigned long)nutritionPlan.nutritionDays.count,
                                   NSLocalizedString(@"días", nil)];
        }
        
        cell.detailInfoButton.hidden = YES;
        cell.menuButton.hidden = YES;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.exercisesLabel.text = [NSString stringWithFormat:@"%d %@",(int)nutritionPlan.nutritionDays.count, NSLocalizedString(@"dietas", nil)];
        [cell setCurrentIndex:indexPath.row];
        
        if (isSelected) {
            cell.checkImageView.hidden = NO;
        }
        else {
            cell.checkImageView.hidden = YES;
        }
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SchedulePlan * plan = [_items objectAtIndex:indexPath.row];
    RLMRealm *realm = [AppContext currentRealm];
    
    for (SchedulePlan * schedulePlan in _items) {
        [realm transactionWithBlock:^{
            schedulePlan.isSelected = @NO;
        }];
    }
    
    BOOL isSelected = plan.isSelected && plan.isSelected.boolValue;
    if (isSelected) {
//        [realm transactionWithBlock:^{
//            plan.isSelected = @NO;
//        }];
        _selectedSchedulePlan = plan;
    }
    else {
//        [realm transactionWithBlock:^{
//            plan.isSelected = @YES;
//        }];
        _selectedSchedulePlan = plan;
    }

    [self dismissViewControllerAnimated:YES completion:^{
        [_delegate schedulePlanTableViewController:self
                             didSelectSchedulePlan:_selectedSchedulePlan
                                     accessibility:self.view.accessibilityLabel];
    }];
}

@end
