//
//  LogsTableViewController.m
//  Titan
//
//  Created by Marcus Lee on 5/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "LogsTableViewController.h"
#import "Log.h"
#import "ProgressCell.h"
#import "DateManager.h"
#import "AppConstants.h"
#import "UIImageView+AFNetworking.h"
#import "AppContext.h"
#import "UtilManager.h"

@interface LogsTableViewController ()

@property (nonatomic, strong) RLMResults * items;
@property (nonatomic, strong) Log * selectedLog;

@end

@implementation LogsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[ProgressCell class] forCellReuseIdentifier:@"progressCell"];
    
    _items = [Log getLogs];
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = BLACK_APP_COLOR;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [UtilManager height:170];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Log *currentLog = [_items objectAtIndex:indexPath.row];
    
    ProgressCell *progressCell = [tableView dequeueReusableCellWithIdentifier:@"progressCell"];
    
    [progressCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute;
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units fromDate:currentLog.revisionDate];
    
    [[progressCell dateLabel] setText:[NSString stringWithFormat:@"%ld %@ %ld",(long)components.day,[DateManager monthWithInteger:components.month],(long)components.year]];
    [[progressCell weightLabel] setText:[NSString stringWithFormat:@"%d Kg",currentLog.weight.intValue]];
    [[progressCell percentLabel] setText:[NSString stringWithFormat:@"%d %%",currentLog.igc.intValue]];
    
    NSURLRequest *homeImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", PATH_IMAGE_LOGS,currentLog.picture1]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
    NSURLRequest *exercise1ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", PATH_IMAGE_LOGS,currentLog.picture2]]
                                                           cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                       timeoutInterval:60];
    NSURLRequest *exercise2ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", PATH_IMAGE_LOGS,currentLog.picture3]]
                                                           cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                       timeoutInterval:60];
    NSURLRequest *exercise3ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", PATH_IMAGE_LOGS,currentLog.picture4]]
                                                           cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                       timeoutInterval:60];
    NSURLRequest *exercise4ImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_LOGS,currentLog.picture5]]
                                                           cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                       timeoutInterval:60];
    
    __weak UIImageView *progressHomeImageView = progressCell.homeImageView;
    
    [[progressCell homeImageView] setImageWithURLRequest:homeImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [progressHomeImageView setImage:image];
        [[progressHomeImageView layer] setMasksToBounds:YES];
        [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        
        [progressHomeImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
        [[progressHomeImageView layer] setMasksToBounds:YES];
        [progressHomeImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    }];
    
    __weak UIImageView *firstImageView = progressCell.exercise1ImageView;
    
    [[progressCell exercise1ImageView] setImageWithURLRequest:exercise1ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [firstImageView setImage:image];
        [[firstImageView layer] setMasksToBounds:YES];
        [firstImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        [firstImageView setImage:nil];
    }];
    
    __weak UIImageView *secondImageView = progressCell.exercise2ImageView;
    
    [[progressCell exercise2ImageView] setImageWithURLRequest:exercise2ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [secondImageView setImage:image];
        [[secondImageView layer] setMasksToBounds:YES];
        [secondImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        [secondImageView setImage:nil];
    }];
    
    __weak UIImageView *thirdImageView = progressCell.exercise3ImageView;
    
    [[progressCell exercise3ImageView] setImageWithURLRequest:exercise3ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [thirdImageView setImage:image];
        [[thirdImageView layer] setMasksToBounds:YES];
        [thirdImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        [thirdImageView setImage:nil];
    }];
    
    __weak UIImageView *fourImageView = progressCell.exercise4ImageView;
    
    [[progressCell exercise4ImageView] setImageWithURLRequest:exercise4ImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-big"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        
        [fourImageView setImage:image];
        [[fourImageView layer] setMasksToBounds:YES];
        [fourImageView setContentMode:UIViewContentModeScaleAspectFit];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        [fourImageView setImage:nil];
    }];
    
    BOOL isSelected = currentLog.isSelected && currentLog.isSelected.boolValue;
    if (isSelected) {
        progressCell.checkImageView.hidden = NO;
    }
    else {
        progressCell.checkImageView.hidden = YES;
    }
    
    progressCell.backgroundColor = UIColor.clearColor;
    progressCell.contentView.backgroundColor = UIColor.clearColor;
    return progressCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Log * selectingLog = [_items objectAtIndex:indexPath.row];
    RLMRealm *realm = [AppContext currentRealm];
    
    for (Log * log in _items) {
        [realm transactionWithBlock:^{
            log.isSelected = @NO;
        }];
    }
    
    BOOL isSelected = selectingLog.isSelected && selectingLog.isSelected.boolValue;
    if (isSelected) {
//        [realm transactionWithBlock:^{
//            selectingLog.isSelected = @NO;
//        }];
        
        _selectedLog = selectingLog;
    }
    else {
//        [realm transactionWithBlock:^{
//            selectingLog.isSelected = @YES;
//        }];
        
        _selectedLog = selectingLog;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        [_delegate logsTableViewController:self
                              didSelectLog:_selectedLog
                             accessibility:self.view.accessibilityLabel];
    }];
}

@end
