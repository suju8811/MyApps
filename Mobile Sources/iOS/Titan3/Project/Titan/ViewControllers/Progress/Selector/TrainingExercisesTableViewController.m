//
//  TrainingExercisesTableViewController.m
//  Titan
//
//  Created by Marcus Lee on 9/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "TrainingExercisesTableViewController.h"
#import "TrainingExerciseCell.h"
#import "TrainingExercisesTableExecutive.h"

@interface TrainingExercisesTableViewController () <TrainingExercisesTableDisplay>

@property (nonatomic, strong) NSArray * trainingExercises;
@property (nonatomic, strong) NSArray * trainingDates;
@property (nonatomic, strong) TrainingExercisesTableExecutive * executive;

@end

@implementation TrainingExercisesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 144.f;
    
    _executive = [[TrainingExercisesTableExecutive alloc] initWithDisplay:self startDate:_startDate endDate:_endDate];
    [_executive displayDidLoad];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _trainingExercises.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TrainingExerciseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TrainingExerciseCell" forIndexPath:indexPath];
    
    NSDate * trainingDate = _trainingDates[indexPath.row];
    TrainingExercise * trainingExercise = _trainingExercises[indexPath.row];
    [cell setContent:trainingExercise date:trainingDate];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TrainingExercise * trainingExercise = _trainingExercises[indexPath.row];
    [self dismissViewControllerAnimated:YES completion:^{
        [_delegate trainingExercisesTableViewController:self trainingExercise:trainingExercise];
    }];
}

#pragma mark - TrainingExercisesTableDisplay

- (void)reloadTable:(NSArray *)trainingExercises exerciseDates:(NSArray *)exerciseDates {
    
    _trainingExercises = trainingExercises;
    _trainingDates = exerciseDates;
    
    [self.tableView reloadData];
}

- (void)showMessage:(NSString *)message {
    UIAlertAction *acceptAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Okay", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [super showMessage:message alertAction:acceptAction];
}

@end
