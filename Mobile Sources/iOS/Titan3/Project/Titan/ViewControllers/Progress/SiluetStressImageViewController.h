//
//  SiluetStressImageViewController.h
//  Titan
//
//  Created by Marcus Lee on 10/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DivisionPercentage.h"
#import "SiluetStressImageExecutive.h"
#import "CommonViewController.h"

@class SiluetStressImageViewController;

@protocol SiluetStressImageViewControllerDelegate <NSObject>

- (void)siluetStressImageViewController:(SiluetStressImageViewController*)siluetStressImageViewController didPressGotoButton:(BOOL)isForward;

@end

@interface SiluetStressImageViewController : CommonViewController

@property (nonatomic, assign) SiluetStressImageType imageType;
@property (nonatomic, assign) id<SiluetStressImageViewControllerDelegate> delegate;
@property (nonatomic, strong) DivisionPercentage * percentage;

//
// Outlets
//

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;
@property (weak, nonatomic) IBOutlet UIButton *backwardButton;
@property (weak, nonatomic) IBOutlet UIButton *forwardButton;

@property (weak, nonatomic) IBOutlet UIImageView *forearmImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
@property (weak, nonatomic) IBOutlet UIImageView *femoralImageView;
@property (weak, nonatomic) IBOutlet UIImageView *twinImageView;
@property (weak, nonatomic) IBOutlet UIImageView *gluteoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *shouldersImageView;
@property (weak, nonatomic) IBOutlet UIImageView *lumbareImageView;
@property (weak, nonatomic) IBOutlet UIImageView *trapeciosImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bicepsImageView;
@property (weak, nonatomic) IBOutlet UIImageView *quadricepImageView;
@property (weak, nonatomic) IBOutlet UIImageView *chestImageView;
@property (weak, nonatomic) IBOutlet UIImageView *tricepImageView;


@end
