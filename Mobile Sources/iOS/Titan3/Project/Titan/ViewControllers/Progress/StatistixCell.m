//
//  StatistixCell.m
//  Titan
//
//  Created by Manuel Manzanera on 19/4/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "StatistixCell.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation StatistixCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        
        UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:10], WIDTH - [UtilManager width:40], [UtilManager height:150])];
        
        UIView *containerBackgroundView = [[UIView alloc] initWithFrame:containerView.frame];
        [containerBackgroundView setBackgroundColor:[UIColor blackColor]];
        [containerBackgroundView setAlpha:0.65];
        
        [[self contentView] addSubview:containerBackgroundView];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:30], [UtilManager height:10], [UtilManager width:220], [UtilManager height:40])];
        [_titleLabel setText:@"Label"];
        [_titleLabel setTextColor:[UIColor whiteColor]];
        [_titleLabel setBackgroundColor:[UIColor clearColor]];
        [_titleLabel setFont:[UIFont fontWithName:REGULAR_FONT size:21]];
        
        [[self contentView] addSubview:_titleLabel];
        
        _evoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(containerBackgroundView.frame.size.width - [UtilManager width:140],[UtilManager height:10], [UtilManager width:150], [UtilManager height:150])];
        [_evoImageView setImage:[UIImage imageNamed:@"img-evo"]];
        [_evoImageView setContentMode:UIViewContentModeScaleAspectFill];
        
        
        [[self contentView] addSubview:_evoImageView];
        
        
    }

    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
