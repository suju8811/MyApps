//
//  SiluetStressImageViewController.m
//  Titan
//
//  Created by Marcus Lee on 10/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "SiluetStressImageViewController.h"
#import "ImageUtil.h"
#import "SiluetStressDetailViewController.h"
#import "MRProgress.h"

@interface SiluetStressImageViewController () <SiluetStressImageDisplay>

@property (nonatomic, strong) SiluetStressImageExecutive * executive;

@end

@implementation SiluetStressImageViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer * frontGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didPressFrontBodyElement:)];
    [_tricepImageView addGestureRecognizer:frontGestureRecognizer];
    
    UITapGestureRecognizer * backGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didPressBackBodyElement:)];
    [_trapeciosImageView addGestureRecognizer:backGestureRecognizer];
    
    _executive = [[SiluetStressImageExecutive alloc] initWithDisplay:self
                                                           imageType:_imageType];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self refreshDisplay];
}

- (void)refreshDisplay {
    if (_imageType == SiluetStressImageTypeBack) {
        _mainImageView.image = [UIImage imageNamed:@"img_stress_siluet_back"];
        _forwardButton.hidden = YES;
        _backwardButton.hidden = NO;
    }
    else {
        _mainImageView.image = [UIImage imageNamed:@"img_stress_siluet_front"];
        _forwardButton.hidden = NO;
        _backwardButton.hidden = YES;
    }
    
    BOOL isHideBackImage = _imageType != SiluetStressImageTypeBack;
    _forearmImageView.hidden = isHideBackImage;
    _backImageView.hidden = isHideBackImage;
    _femoralImageView.hidden = isHideBackImage;
    _twinImageView.hidden = isHideBackImage;
    _gluteoImageView.hidden = isHideBackImage;
    _shouldersImageView.hidden = isHideBackImage;
    _lumbareImageView.hidden = isHideBackImage;
    _trapeciosImageView.hidden = isHideBackImage;
    
    BOOL isHideFrontImage = _imageType != SiluetStressImageTypeFront;
    _bicepsImageView.hidden = isHideFrontImage;
    _quadricepImageView.hidden = isHideFrontImage;
    _chestImageView.hidden = isHideFrontImage;
    _tricepImageView.hidden = isHideFrontImage;
    
    if (_percentage) {
        NSString * image = @"img_forearm_back_";
        if (_percentage.forearm >= 0 && _percentage.forearm <= 10) {
            _forearmImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@green", image]];
        }
        else if (_percentage.forearm >= 11 && _percentage.forearm <= 41) {
            _forearmImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@yellow", image]];
        }
        else if (_percentage.forearm >= 42 && _percentage.forearm <= 100) {
            _forearmImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@red", image]];
        }
        image = @"img_back_back_";
        if (_percentage.back >= 0 && _percentage.back <= 10) {
            _backImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@green", image]];
        }
        else if (_percentage.back >= 11 && _percentage.back <= 41) {
            _backImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@yellow", image]];
        }
        else if (_percentage.back >= 42 && _percentage.back <= 100) {
            _backImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@red", image]];
        }
        image = @"img_femoral_back_";
        if (_percentage.femoral >= 0 && _percentage.femoral <= 10) {
            _femoralImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@green", image]];
        }
        else if (_percentage.femoral >= 11 && _percentage.femoral <= 41) {
            _femoralImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@yellow", image]];
        }
        else if (_percentage.femoral >= 42 && _percentage.femoral <= 100) {
            _femoralImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@red", image]];
        }
        image = @"img_twin_back_";
        if (_percentage.twin >= 0 && _percentage.twin <= 10) {
            _twinImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@green", image]];
        }
        else if (_percentage.twin >= 11 && _percentage.twin <= 41) {
            _twinImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@yellow", image]];
        }
        else if (_percentage.twin >= 42 && _percentage.twin <= 100) {
            _twinImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@red", image]];
        }
        image = @"img_gluteo_back_";
        if (_percentage.gluteo >= 0 && _percentage.gluteo <= 10) {
            _gluteoImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@green", image]];
        }
        else if (_percentage.gluteo >= 11 && _percentage.gluteo <= 41) {
            _gluteoImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@yellow", image]];
        }
        else if (_percentage.gluteo >= 42 && _percentage.gluteo <= 100) {
            _gluteoImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@red", image]];
        }
        image = @"img_shoulders_back_";
        if (_percentage.shoulders >= 0 && _percentage.shoulders <= 10) {
            _shouldersImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@green", image]];
        }
        else if (_percentage.shoulders >= 11 && _percentage.shoulders <= 41) {
            _shouldersImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@yellow", image]];
        }
        else if (_percentage.shoulders >= 42 && _percentage.shoulders <= 100) {
            _shouldersImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@red", image]];
        }
        image = @"img_lumbares_back_";
        if (_percentage.lumbares >= 0 && _percentage.lumbares <= 10) {
            _lumbareImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@green", image]];
        }
        else if (_percentage.lumbares >= 11 && _percentage.lumbares <= 41) {
            _lumbareImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@yellow", image]];
        }
        else if (_percentage.lumbares >= 42 && _percentage.lumbares <= 100) {
            _lumbareImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@red", image]];
        }
        image = @"img_trapecios_back_";
        if (_percentage.trapecios >= 0 && _percentage.trapecios <= 10) {
            _trapeciosImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@green", image]];
        }
        else if (_percentage.trapecios >= 11 && _percentage.trapecios <= 41) {
            _trapeciosImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@yellow", image]];
        }
        else if (_percentage.trapecios >= 42 && _percentage.trapecios <= 100) {
            _trapeciosImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@red", image]];
        }
        image = @"img_biceps_front_";
        if (_percentage.biceps >= 0 && _percentage.biceps <= 10) {
            _bicepsImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@green", image]];
        }
        else if (_percentage.biceps >= 11 && _percentage.biceps <= 41) {
            _bicepsImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@yellow", image]];
        }
        else if (_percentage.biceps >= 42 && _percentage.biceps <= 100) {
            _bicepsImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@red", image]];
        }
        image = @"img_quadriceps_front_";
        if (_percentage.quadriceps >= 0 && _percentage.quadriceps <= 10) {
            _quadricepImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@green", image]];
        }
        else if (_percentage.quadriceps >= 11 && _percentage.quadriceps <= 41) {
            _quadricepImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@yellow", image]];
        }
        else if (_percentage.quadriceps >= 42 && _percentage.quadriceps <= 100) {
            _quadricepImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@red", image]];
        }
        image = @"img_chest_front_";
        if (_percentage.chest >= 0 && _percentage.chest <= 10) {
            _chestImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@green", image]];
        }
        else if (_percentage.chest >= 11 && _percentage.chest <= 41) {
            _chestImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@yellow", image]];
        }
        else if (_percentage.chest >= 42 && _percentage.chest <= 100) {
            _chestImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@red", image]];
        }
        image = @"img_triceps_front_";
        if (_percentage.triceps >= 0 && _percentage.triceps <= 10) {
            _tricepImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@green", image]];
        }
        else if (_percentage.triceps >= 11 && _percentage.triceps <= 41) {
            _tricepImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@yellow", image]];
        }
        else if (_percentage.triceps >= 42 && _percentage.triceps <= 100) {
            _tricepImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@red", image]];
        }
    }
}

- (void)setPercentage:(DivisionPercentage *)percentage {
    _percentage = percentage;
    [self refreshDisplay];
}

#pragma mark - Actions

- (IBAction)didPressBackwardButton:(id)sender {
    [_delegate siluetStressImageViewController:self didPressGotoButton:NO];
}

- (IBAction)didPressForwardButton:(id)sender {
    [_delegate siluetStressImageViewController:self didPressGotoButton:YES];
}

- (void)didPressFrontBodyElement:(UITapGestureRecognizer*)tapGestureRecognizer {
    CGPoint touchPoint = [tapGestureRecognizer locationInView:_tricepImageView];
    
    NSArray * frontImageList = @[@"img_biceps_front_green",
                                 @"img_quadriceps_front_green",
                                 @"img_chest_front_green",
                                 @"img_triceps_front_green"];
    
    NSInteger touchIndex = [self touchIndexFromImageList:frontImageList touchPoint:touchPoint];
    [self didTouchSiluetImage:_imageType index:touchIndex];
    
}

- (NSInteger)touchIndexFromImageList:(NSArray*)imageList touchPoint:(CGPoint)touchPoint {
    CGFloat red = 0.f, green = 0.f, blue = 0.f, alpha = 0.f;
    for (NSInteger index = 0; index < imageList.count; index++) {
        UIImage * image = [UIImage imageNamed:imageList[index]];
        UIColor * color = [ImageUtil RGBAFromImage:image atx:touchPoint.x atY:touchPoint.y renderSize:_tricepImageView.bounds.size];
        [color getRed:&red green:&green blue:&blue alpha:&alpha];
        if (alpha != 0) {
            return index;
        }
    }
    
    return -1;
}

- (void)didPressBackBodyElement:(UITapGestureRecognizer*)tapGestureRecognizer {
    NSLog(@"Did tap back body");
    CGPoint touchPoint = [tapGestureRecognizer locationInView:_trapeciosImageView];
    NSLog(@"Touch Point : %@", NSStringFromCGPoint(touchPoint));
    
    NSArray * backImageList = @[@"img_forearm_back_green",
                                @"img_back_back_green",
                                @"img_femoral_back_green",
                                @"img_twin_back_green",
                                @"img_gluteo_back_green",
                                @"img_shoulders_back_green",
                                @"img_lumbares_back_green",
                                @"img_trapecios_back_green"];
    
    NSInteger touchIndex = [self touchIndexFromImageList:backImageList touchPoint:touchPoint];
    [self didTouchSiluetImage:_imageType index:touchIndex];
}

- (void)didTouchSiluetImage:(SiluetStressImageType)imageType
                      index:(NSInteger)index {
    
    [_executive didPressBodyPart:imageType index:index];
}

- (void)gotoSiluetStressDetailViewController:(NSArray*)exerciseArray
                                   imageType:(SiluetStressImageType)imageType
                                       index:(NSInteger)index
                                    bodyPart:(NSString*)bodyPart {
    
    SiluetStressDetailViewController * detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SiluetStressDetailStoryboardID"];
    detailViewController.imageType = imageType;
    detailViewController.index = index;
    detailViewController.bodyPart = bodyPart;
    detailViewController.exercises = exerciseArray;
    detailViewController.divisionPercentage = _percentage;
    detailViewController.isHideCustomNavigation = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

- (void)showLoading:(NSString *)title {
    NSString * localizedTitle = @"";
    if (title != nil) {
        localizedTitle = NSLocalizedString(title, nil);
    }
    
    [MRProgressOverlayView showOverlayAddedTo:self.navigationController.view
                                        title:localizedTitle
                                         mode:MRProgressOverlayViewModeCheckmark
                                     animated:YES];
}

- (void)hideLoading {
    [MRProgressOverlayView dismissOverlayForView:self.navigationController.view animated:NO];
}

@end
