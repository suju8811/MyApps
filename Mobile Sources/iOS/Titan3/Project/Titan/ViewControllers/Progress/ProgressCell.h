//
//  ProgressCell.h
//  Titan
//
//  Created by Manuel Manzanera on 26/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressCell : UITableViewCell

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIImageView *exercise1ImageView;
@property (nonatomic, strong) UIImageView *exercise2ImageView;
@property (nonatomic, strong) UIImageView *exercise3ImageView;
@property (nonatomic, strong) UIImageView *exercise4ImageView;
@property (nonatomic, strong) UIImageView *exercise5ImageView;
@property (nonatomic, strong) UIImageView *exercise6ImageView;

@property (nonatomic, strong) UIImageView *homeImageView;

@property (nonatomic, strong) UIImageView *siluetImageView;
@property (nonatomic, strong) UILabel *weightLabel;
@property (nonatomic, strong) UILabel *percentLabel;
@property (nonatomic, strong) UIImageView *checkImageView;

@end
