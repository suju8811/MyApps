//
//  SiluetStressDetailViewController.m
//  Titan
//
//  Created by Marcus Lee on 11/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "SiluetStressDetailViewController.h"
#import "SiluetStressDetailExecutive.h"

@interface SiluetStressDetailViewController () <SiluetStressDetailDisplay>

@property (nonatomic, strong) SiluetStressDetailExecutive * executive;

@end

@implementation SiluetStressDetailViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _executive = [[SiluetStressDetailExecutive alloc] initWithDisplay:self
                                                                title:_bodyPart
                                                            imageType:_imageType
                                                           imageIndex:_index
                                                            exercises:_exercises
                                                   divisionPercentage:_divisionPercentage];
    
    [_executive displayDidLoad];
}

- (void)configureDisplay {
}

#pragma mark - SiluetStressDetailDisplay

- (void)loadDisplay:(SiluetStressDetailDisplayContext *)displayContext {
    _titleLabel.text = [NSString stringWithFormat:@"Detalle %@", displayContext.title];
    _divisionPercentageLabel.text = displayContext.divisionPercentage;
    _difficultyLabel.text = displayContext.difficulty;
    _frequencyLabel.text = displayContext.frequency;
    _basicLabel.text = displayContext.basic;
    _specificLabel.text = displayContext.specific;
    _seriesLabel.text = displayContext.series;
}

#pragma mark - Actions

- (IBAction)didPressBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
