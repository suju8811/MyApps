//
//  EvolutionViewController.h
//  Titan
//
//  Created by Marcus Lee on 7/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "LabelRegular18Yellow.h"
#import "EvolutionExecutive.h"
#import "PNChart.h"

@interface EvolutionViewController : ParentViewController

@property (nonatomic, strong) EvolutionExecutive * executive;

//
// Views
//

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView * datesView;
@property (nonatomic, strong) LabelRegular18Yellow *startDateLabel;
@property (nonatomic, strong) LabelRegular18Yellow *endDateLabel;

@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UIButton *acceptButton;
@property (nonatomic, strong) UIView *containerPickerView;

@property (nonatomic, strong) UIView *segmentControlView;
@property (nonatomic, strong) UISegmentedControl *segmentControl;

@property (nonatomic, strong) UIView * chartView;
@property (nonatomic, strong) PNLineChart * lineChart;
@property (nonatomic, strong) NSMutableArray * chartDataButtons;

- (void)didPressAcceptDate:(UIButton*)sender;
- (void)configureDateViews;
- (void)configureSegmentControlView;
- (void)configureChartView;
- (void)configureChartSelectionView;

@end
