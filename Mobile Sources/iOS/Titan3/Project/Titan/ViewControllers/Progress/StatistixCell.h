//
//  StatistixCell.h
//  Titan
//
//  Created by Manuel Manzanera on 19/4/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatistixCell : UITableViewCell

@property (nonatomic, strong) UIImageView *evoImageView;
@property (nonatomic, strong) UILabel *titleLabel;

@end
