//
//  AlertCustomView.m
//  Amii
//
//  Created by Manuel Manzanera on 22/12/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "AlertCustomView.h"
#import "AppContext.h"
#import "AppConstants.h"
#import "UtilManager.h"

@interface AlertCustomView ()<UITextViewDelegate, UITextFieldDelegate>

@end

@implementation AlertCustomView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
        
        UIView *backgroundView = [[UIView alloc] initWithFrame:self.frame];
        [backgroundView setBackgroundColor:[UIColor blackColor]];
        [backgroundView setAlpha:0.5];
        [backgroundView setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer *dismissTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissView)];
        [backgroundView addGestureRecognizer:dismissTap];
        
        [self addSubview:backgroundView];
        
        UIView *selectPictureView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH - [UtilManager width:50], [UtilManager height:190])];
        [selectPictureView setBackgroundColor:[UIColor whiteColor]];
        [[selectPictureView layer] setCornerRadius:8];
        
        [selectPictureView setCenter:CGPointMake(self.center.x, self.center.y)];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:20], selectPictureView.frame.size.width - [UtilManager width:20], [UtilManager height:32])];
        [_titleLabel setText: NSLocalizedString(@"Log out", nil)];
        [_titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_titleLabel setFont:[UIFont fontWithName:BOLD_FONT size:30]];
        [_titleLabel setTextColor:YELLOW_APP_COLOR];
        [selectPictureView addSubview:_titleLabel];
        
        _subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], _titleLabel.frame.size.height + _titleLabel.frame.origin.y + [UtilManager height:10], _titleLabel.frame.size.width, [UtilManager height:65])];
        [_subtitleLabel setText:NSLocalizedString(@"¿Estas seguro de querer \n cerrar la sesión?", nil)];
        [_subtitleLabel setNumberOfLines:2];
        [_subtitleLabel setTextAlignment:NSTextAlignmentCenter];
        [_subtitleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:20]];
        [_subtitleLabel setTextColor:GRAY_REGISTER_FONT];
        [_subtitleLabel setTextColor:[UIColor colorWithRed:68/255 green:68/255 blue:68/255 alpha:1]];
        [selectPictureView addSubview:_subtitleLabel];
        
        _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:30], _subtitleLabel.frame.origin.y + _subtitleLabel.frame.size.height + [UtilManager height:17], [UtilManager width:120], [UtilManager height:38])];
        [[_cancelButton layer] setCornerRadius:8];
        [_cancelButton setBackgroundColor:GRAY_REGISTER_FONT];
        [_cancelButton setTitle:NSLocalizedString(@"Cancelar", nil) forState:UIControlStateNormal];
        [[_cancelButton titleLabel] setFont:[UIFont fontWithName:SEMIBOLD_FONT size:20]];
        [_cancelButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
        
        [selectPictureView addSubview:_cancelButton];
        
        _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_cancelButton.frame.origin.x + _cancelButton.frame.size.width + [UtilManager width:30], _cancelButton.frame.origin.y, _cancelButton.frame.size.width, _cancelButton.frame.size.height)];
        [[_acceptButton layer] setCornerRadius:8];
        [_acceptButton setBackgroundColor:GRAY_REGISTER_FONT];
        [_acceptButton setTitle:NSLocalizedString(@"Confirmar", nil) forState:UIControlStateNormal];
        [[_acceptButton titleLabel] setFont:[UIFont fontWithName:SEMIBOLD_FONT size:20]];
        [_acceptButton addTarget:self action:@selector(acceptAction) forControlEvents:UIControlEventTouchUpInside];
        
        [selectPictureView addSubview:_acceptButton];
        
        [self addSubview:selectPictureView];
    }
    
    return self;
}

- (id)initWithFrameAndTextField:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
        
        UIView *backgroundView = [[UIView alloc] initWithFrame:self.frame];
        [backgroundView setBackgroundColor:[UIColor blackColor]];
        [backgroundView setAlpha:0.5];
        [backgroundView setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer *dismissTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissView)];
        [backgroundView addGestureRecognizer:dismissTap];
        
        [self addSubview:backgroundView];
        
        UIView *selectPictureView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH - [UtilManager width:50], [UtilManager height:180])];
        [selectPictureView setBackgroundColor:[UIColor whiteColor]];
        [[selectPictureView layer] setCornerRadius:8];
        
        [selectPictureView setCenter:CGPointMake(self.center.x, self.center.y)];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:20], selectPictureView.frame.size.width - [UtilManager width:20], [UtilManager height:32])];
        [_titleLabel setText: NSLocalizedString(@"Log out", nil)];
        [_titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_titleLabel setFont:[UIFont fontWithName:BOLD_FONT size:30]];
        [_titleLabel setTextColor:YELLOW_APP_COLOR];
        [selectPictureView addSubview:_titleLabel];
        
        _subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], _titleLabel.frame.size.height + _titleLabel.frame.origin.y + [UtilManager height:10], _titleLabel.frame.size.width, [UtilManager height:65])];
        [_subtitleLabel setText:NSLocalizedString(@"¿Estas seguro de querer \n cerrar la sesión?", nil)];
        [_subtitleLabel setNumberOfLines:2];
        [_subtitleLabel setTextAlignment:NSTextAlignmentCenter];
        [_subtitleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:20]];
        [_subtitleLabel setTextColor:GRAY_REGISTER_FONT];
        [_subtitleLabel setTextColor:[UIColor colorWithRed:68/255 green:68/255 blue:68/255 alpha:1]];
        //[selectPictureView addSubview:_subtitleLabel];
        
        _textField = [[UITextField alloc] initWithFrame:CGRectMake(_subtitleLabel.frame.origin.x, _titleLabel.frame.size.height + _titleLabel.frame.origin.y, _subtitleLabel.frame.size.width, UITEXTFIELD_HEIGHT)];
        [_textField setDelegate:self];
        [_textField setTextAlignment:NSTextAlignmentCenter];
        [_textField setKeyboardType:UIKeyboardTypeDecimalPad];
        [_textField setKeyboardAppearance:UIKeyboardAppearanceDark];
        
        [selectPictureView addSubview:_textField];
        
        _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:30], _textField.frame.origin.y + _textField.frame.size.height + [UtilManager height:17], [UtilManager width:120], [UtilManager height:38])];
        [[_cancelButton layer] setCornerRadius:8];
        [_cancelButton setBackgroundColor:GRAY_REGISTER_FONT];
        [_cancelButton setTitle:NSLocalizedString(@"Cancelar", nil) forState:UIControlStateNormal];
        [[_cancelButton titleLabel] setFont:[UIFont fontWithName:SEMIBOLD_FONT size:20]];
        [_cancelButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
        
        [selectPictureView addSubview:_cancelButton];
        
        _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_cancelButton.frame.origin.x + _cancelButton.frame.size.width + [UtilManager width:30], _cancelButton.frame.origin.y, _cancelButton.frame.size.width, _cancelButton.frame.size.height)];
        [[_acceptButton layer] setCornerRadius:8];
        [_acceptButton setBackgroundColor:GRAY_REGISTER_FONT];
        [_acceptButton setTitle:NSLocalizedString(@"Confirmar", nil) forState:UIControlStateNormal];
        [[_acceptButton titleLabel] setFont:[UIFont fontWithName:SEMIBOLD_FONT size:20]];
        [_acceptButton addTarget:self action:@selector(acceptAction) forControlEvents:UIControlEventTouchUpInside];
        
        [selectPictureView addSubview:_acceptButton];
        
        [self addSubview:selectPictureView];
    }
    
    return self;
}


- (void)cancelAction{
    if(_delegate)
        [_delegate cancelAlertViewDidselected];
}

- (void)acceptAction{
    if(_delegate)
        [_delegate confirmAlertViewDidSelected];
}

- (void)dismissView{
    if(_delegate)
        [_delegate dismissAlertViewSelected];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSUInteger newLength;
    
    newLength = [textField.text length] + [string length] - range.length;
    
    if([string isEqualToString:@"."])
        return FALSE;
    
    return (newLength > 1) ? NO : YES;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if(range.location + 1 > 2)
    {
        return FALSE;
    }
    
    
    return YES;
}



@end
