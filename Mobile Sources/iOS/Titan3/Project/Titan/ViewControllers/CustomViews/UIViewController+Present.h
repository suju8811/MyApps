//
//  UIViewController+Present.h
//  NewsHom
//
//  Created by LMAN on 5/8/16.
//  Copyright © 2016 idragonit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIViewController (Present)
- (void) presentTransparentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion;
@end
