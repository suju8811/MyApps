//
//  InfoViewController.m
//  Titan
//
//  Created by Marcus Lee on 6/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "InfoPopupViewController.h"
#import "TrainingPlan.h"
#import "Training.h"
#import "NutritionPlan.h"
#import "Diet.h"
#import "Meal.h"

@interface InfoPopupViewController ()

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation InfoPopupViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (_rlmObject == nil) {
        return;
    }
    
    if ([_rlmObject isKindOfClass:[NutritionPlan class]]) {
        NutritionPlan * nutritionPlan = (NutritionPlan*)_rlmObject;
        _titleLabel.text = nutritionPlan.title;
        _descriptionLabel.text = nutritionPlan.content;
    }
    
    if ([_rlmObject isKindOfClass:[Diet class]]) {
        Diet * diet = (Diet*)_rlmObject;
        _titleLabel.text = diet.title;
        _descriptionLabel.text = diet.content;
    }
    
    if ([_rlmObject isKindOfClass:[Meal class]]) {
        Meal * meal = (Meal*)_rlmObject;
        _titleLabel.text = meal.title;
        _descriptionLabel.text = meal.content;
    }
    
    if ([_rlmObject isKindOfClass:[TrainingPlan class]]) {
        TrainingPlan * trainingPlan = (TrainingPlan*)_rlmObject;
        _titleLabel.text = trainingPlan.title;
        _descriptionLabel.text = trainingPlan.content;
    }
    
    if ([_rlmObject isKindOfClass:[Training class]]) {
        Training * training = (Training*)_rlmObject;
        _titleLabel.text = training.title;
        _descriptionLabel.text = training.desc;
    }
}

#pragma mark - Actions
- (IBAction)didPressOKButton:(id)sender {
    [self.view removeFromSuperview];
}



@end
