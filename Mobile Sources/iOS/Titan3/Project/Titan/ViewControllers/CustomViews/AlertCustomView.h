//
//  AlertCustomView.h
//  Amii
//
//  Created by Manuel Manzanera on 22/12/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AlertCustomViewDelegate <NSObject>

- (void)cancelAlertViewDidselected;
- (void)confirmAlertViewDidSelected;
- (void)dismissAlertViewSelected;

@end

@interface AlertCustomView : UIView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subtitleLabel;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *acceptButton;
@property (nonatomic, assign) id<AlertCustomViewDelegate>delegate;
@property (nonatomic, strong) UITextField *textField;

- (id)initWithFrameAndTextField:(CGRect)frame;

@end
