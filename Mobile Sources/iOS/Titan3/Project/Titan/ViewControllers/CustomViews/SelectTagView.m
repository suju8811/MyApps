//
//  SelectTagView.m
//  Titan
//
//  Created by Manuel Manzanera on 9/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SelectTagView.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "Tag.h"
#import "AppContext.h"

#define numberOfRowsInFile 4

@interface SelectTagView()<TagViewDelegate>


@end

@implementation SelectTagView

- (id)initWithFrame:(CGRect)frame andTags:(NSMutableArray *)tags{

    self = [super initWithFrame:frame];
    
    _selectedTags = [[NSMutableArray alloc] initWithCapacity:tags.count];
    
    for(Tag *tag in tags){
        if([tag.status isEqualToString:@"On"])
            [_selectedTags addObject:tag];
    }
    
    if(self){
        UIScrollView *tagScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.,[UtilManager height:10.], WIDTH,[UtilManager height:80.])];
        
        [tagScrollView setBackgroundColor:[UIColor clearColor]];
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake([UtilManager width:0],0.,WIDTH, TAGVIEW_SMALL_HEIGHT)];
        
        [_tagView setIsCenter:TRUE];
        _tagView.tags = [tags mutableCopy];
        [_tagView reloadTagSubviews];
        [_tagView setTagsBackgroundColor:[UIColor yellowColor]];
        [_tagView setTagsTextColor:[UIColor colorWithRed:0 green:0 blue:98/255 alpha:1]];
        [_tagView setTagsDeleteButtonColor:[UIColor whiteColor]];
        _tagView.mode = TagsControlModeList;
        [_tagView setTapDelegate:self];
        
        [tagScrollView addSubview:_tagView];
        
        [self addSubview:tagScrollView];
    
    }
    
    return self;
}

- (void)tagView:(TagView *)tagView tagAtIndex:(NSInteger)index{
    for(NSInteger i = 0; i<=tagView.tags.count ; i++){
        if(index == i){
            Tag *pushTag = [tagView.tags objectAtIndex:index];
            
            if([_selectedTags containsObject:pushTag])
                [_selectedTags removeObject:pushTag];
            else
                [_selectedTags addObject:pushTag];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                if([pushTag.status isEqualToString:@"Off"])
                    [pushTag setStatus:@"On"];
                else
                    [pushTag setStatus:@"Off"];
            }];
            
            [tagView.tags replaceObjectAtIndex:index withObject:pushTag];
        }
    }
    
    [tagView reloadTagSubviews];
    if(_delegate)
        [_delegate actionDidPushReturnTags:_selectedTags];
}

- (void)setSelectedTags:(NSMutableArray *)selectedTags{

    _selectedTags = selectedTags;
    
    for(Tag *tag in selectedTags){
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            [tag setStatus:@"On"];
        }];
    }
    
    [_tagView reloadTagSubviews];
    
}


@end
