//
//  SelectTagView.h
//  Titan
//
//  Created by Manuel Manzanera on 9/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagView.h"

@protocol SelectTagViewDelegate;

@interface SelectTagView : UIView

@property (nonatomic, strong) NSMutableArray *selectedTags;
@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, assign) id<SelectTagViewDelegate>delegate;

- (id)initWithFrame:(CGRect)frame andTags:(NSMutableArray *)tags;

@end

@protocol SelectTagViewDelegate <NSObject>

- (void)actionDidPushReturnTags:(NSMutableArray *)selectedTags;

@end
