//
//  LabelRegular18Yellow.m
//  Titan
//
//  Created by Marcus Lee on 3/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "LabelRegular18Yellow.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation LabelRegular18Yellow

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    [self configureStyle];
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    [self configureStyle];
    return self;
}

- (void)configureStyle {
    self.textColor = YELLOW_APP_COLOR;
    self.font = [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18];
}

@end
