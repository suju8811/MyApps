//
//  TitanStyle.m
//  Titan
//
//  Created by Marcus Lee on 26/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TitanStyle.h"

@implementation TitanStyle

+ (UIView*)headerSectionView:(UIImage*)image {
    UIView * headerSectionView = [[UIView alloc] init];
    CGFloat totalWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    headerSectionView.backgroundColor = UIColor.clearColor;
    
    //
    // Set result view's frame
    //
    
    CGFloat marginHorizontal = 8.f;
    CGFloat marginVertical = 8.f;
    CGFloat imageSize = 24.f;
    headerSectionView.frame = CGRectMake(0, 0, totalWidth, imageSize + marginHorizontal + marginVertical);
    
    //
    // Add image view
    //
    
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(headerSectionView.frame) / 2.f,
                                                                            CGRectGetHeight(headerSectionView.frame) / 2.f,
                                                                            imageSize,
                                                                            imageSize)];
    
    imageView.center = CGPointMake(CGRectGetWidth(headerSectionView.frame) / 2.f, CGRectGetHeight(headerSectionView.frame) / 2.f);
    imageView.image = image;
    [headerSectionView addSubview:imageView];
    
    //
    //  Add left and right line view
    //
    
    UIView * leftLineView = [[UIView alloc] initWithFrame:CGRectMake(marginHorizontal,
                                                                     CGRectGetHeight(headerSectionView.frame) / 2.f,
                                                                     CGRectGetMinX(imageView.frame) - marginHorizontal * 2,
                                                                     1)];
                             
    leftLineView.backgroundColor = UIColor.lightGrayColor;
    [headerSectionView addSubview:leftLineView];
    
    UIView * rightLineView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame) + marginHorizontal,
                                                              CGRectGetHeight(headerSectionView.frame) / 2.f,
                                                              CGRectGetWidth(leftLineView.frame),
                                                              1)];
    
    rightLineView.backgroundColor = UIColor.lightGrayColor;
    [headerSectionView addSubview:rightLineView];
    
    return  headerSectionView;
}

@end
