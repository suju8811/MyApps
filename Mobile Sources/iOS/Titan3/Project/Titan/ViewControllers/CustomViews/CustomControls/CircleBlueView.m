//
//  CircleGreenView.m
//  Titan
//
//  Created by Marcus Lee on 11/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "CircleBlueView.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation CircleBlueView

- (void)layoutSubviews {
    [super layoutSubviews];
    self.layer.borderColor = BLUE_WEIGTH_PERCENT_CIRCLE.CGColor;
    self.layer.borderWidth = 1.f;
    self.layer.cornerRadius = CGRectGetHeight(self.bounds) / 2.f;
}

@end
