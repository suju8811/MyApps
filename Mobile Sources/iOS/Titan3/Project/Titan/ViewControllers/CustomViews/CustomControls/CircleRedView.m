//
//  CircleRedView.m
//  Titan
//
//  Created by Marcus Lee on 11/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "CircleRedView.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation CircleRedView

- (void)layoutSubviews {
    [super layoutSubviews];
    self.layer.borderColor = RED_APP_COLOR.CGColor;
    self.layer.borderWidth = 1.f;
    self.layer.cornerRadius = CGRectGetHeight(self.bounds) / 2.f;
}

@end
