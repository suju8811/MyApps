//
//  YellowBlackSwitchButton.m
//  Titan
//
//  Created by Marcus Lee on 4/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "YellowBlackSwitchButton.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation YellowBlackSwitchButton

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    [self configureStyle];
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    [self configureStyle];
    return self;
}

- (void)configureStyle {
    self.backgroundColor = UIColor.clearColor;
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = 1.f;
    self.layer.borderColor = YELLOW_APP_COLOR.CGColor;
    [self setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [self setTitleColor:YELLOW_APP_COLOR forState:UIControlStateSelected];
    self.titleLabel.font = [UIFont fontWithName:REGULAR_FONT_OPENSANS size:15];
}

- (void)setSelected:(BOOL)selected {
    if (selected) {
        self.backgroundColor = YELLOW_APP_COLOR;
    }
    else {
        self.backgroundColor = UIColor.clearColor;
    }
}

@end
