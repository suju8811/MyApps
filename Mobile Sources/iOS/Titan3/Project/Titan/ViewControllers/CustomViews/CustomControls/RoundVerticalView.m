//
//  RoundVerticalView.m
//  Titan
//
//  Created by Marcus Lee on 25/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RoundVerticalView.h"

@implementation RoundVerticalView

- (void)layoutSubviews {
    [super layoutSubviews];
    self.layer.cornerRadius = CGRectGetHeight(self.bounds) / 2.f;
}

@end
