//
//  YelloFilledCircleView.m
//  Titan
//
//  Created by Marcus Lee on 3/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "YelloFilledCircleView.h"
#import "UtilManager.h"
#import "AppConstants.h"

@implementation YelloFilledCircleView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    [self configureStyle];
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    [self configureStyle];
    return self;
}

- (void)configureStyle {
    self.backgroundColor = YELLOW_APP_COLOR;
    self.layer.masksToBounds = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.layer.cornerRadius = CGRectGetHeight(self.frame) / 2.f;
}

@end
