//
//  TitanStyle.h
//  Titan
//
//  Created by Marcus Lee on 26/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TitanStyle : NSObject

+ (UIView*)headerSectionView:(UIImage*)image;

@end
