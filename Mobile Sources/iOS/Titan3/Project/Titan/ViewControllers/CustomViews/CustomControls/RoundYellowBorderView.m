//
//  RoundYellowBorderView.m
//  Titan
//
//  Created by Marcus Lee on 4/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "RoundYellowBorderView.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation RoundYellowBorderView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    [self configureStyle];
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    [self configureStyle];
    return self;
}

- (void)configureStyle {
    self.clipsToBounds = YES;
    self.layer.borderColor = YELLOW_APP_COLOR.CGColor;
    self.layer.borderWidth = 1.f;
    self.layer.cornerRadius = 4.f;
}

@end
