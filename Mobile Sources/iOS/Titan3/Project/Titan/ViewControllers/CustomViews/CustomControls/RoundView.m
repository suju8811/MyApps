//
//  RoundView.m
//  Titan
//
//  Created by Marcus Lee on 27/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RoundView.h"

@implementation RoundView

- (void)layoutSubviews {
    [super layoutSubviews];
    self.layer.cornerRadius = 4.f;
}

@end
