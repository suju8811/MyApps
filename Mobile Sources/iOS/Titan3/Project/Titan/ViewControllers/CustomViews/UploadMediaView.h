//
//  UploadMediaView.h
//  Amii
//
//  Created by Manuel Manzanera on 20/11/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UploadMediaViewDelegate <NSObject>

- (void)galleryDidselected;
- (void)cameraDidSelected;
- (void)dismissViewSelected;

@end

@interface UploadMediaView : UIView

@property (nonatomic, strong) UIButton *galleryButton;
@property (nonatomic, strong) UIButton *cameraButton;

@property (nonatomic, assign) id<UploadMediaViewDelegate>delegate;

@end
