//
//  GalleryView.h
//  loqiva
//
//  Created by Manuel Manzanera on 28/4/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Availability.h>
#undef weak_delegate
#undef __weak_delegate

#define weak_delegate unsafe_unretained
#define __weak_delegate __unsafe_unretained

#import <QuartzCore/QuartzCore.h>

typedef CGRect NSRect;
typedef CGSize NSSize;

#import <UIKit/UIKit.h>

typedef enum
{
    GalleryTypeLinear = 0,
    GalleryTypeCustom
}
GalleryType;

typedef enum
{
    GalleryOptionWrap = 0,
    GalleryOptionShowBackfaces,
    GalleryOptionOffsetMultiplier,
    GalleryOptionVisibleItems,
    GalleryOptionCount,
    GalleryOptionArc,
    GalleryOptionAngle,
    GalleryOptionRadius,
    GalleryOptionTilt,
    GalleryOptionSpacing,
    GalleryOptionFadeMin,
    GalleryOptionFadeMax,
    GalleryOptionFadeRange
}
GalleryOption;

@protocol GalleryDataSource, GalleryDelegate;

@interface GalleryView : UIView

#ifdef __i386__
{
@private
    
    id<GalleryDelegate> __weak_delegate _delegate;
    id<GalleryDataSource> __weak_delegate _dataSource;
    GalleryType _type;
    CGFloat _perspective;
    NSInteger _numberOfItems;
    NSInteger _numberOfPlaceholders;
    NSInteger _numberOfPlaceholdersToShow;
    NSInteger _numberOfVisibleItems;
    UIView *_contentView;
    NSMutableDictionary *_itemViews;
    NSMutableSet *_itemViewPool;
    NSMutableSet *_placeholderViewPool;
    NSInteger _previousItemIndex;
    CGFloat _itemWidth;
    CGFloat _scrollOffset;
    CGFloat _offsetMultiplier;
    CGFloat _startVelocity;
    NSTimer __unsafe_unretained *_timer;
    BOOL _decelerating;
    BOOL _scrollEnabled;
    CGFloat _decelerationRate;
    BOOL _bounces;
    CGSize _contentOffset;
    CGSize _viewpointOffset;
    CGFloat _startOffset;
    CGFloat _endOffset;
    NSTimeInterval _scrollDuration;
    NSTimeInterval _startTime;
    BOOL _scrolling;
    CGFloat _previousTranslation;
    BOOL _centerItemWhenSelected;
    BOOL _wrapEnabled;
    BOOL _dragging;
    BOOL _didDrag;
    CGFloat _scrollSpeed;
    CGFloat _bounceDistance;
    NSTimeInterval _toggleTime;
    CGFloat _toggle;
    BOOL _stopAtItemBoundary;
    BOOL _scrollToItemBoundary;
    BOOL _vertical;
    BOOL _ignorePerpendicularSwipes;
    NSInteger _animationDisableCount;
}
#endif

@property (nonatomic, weak_delegate) IBOutlet id<GalleryDataSource> dataSource;
@property (nonatomic, weak_delegate) IBOutlet id<GalleryDelegate> delegate;
@property (nonatomic, assign) GalleryType type;
@property (nonatomic, assign) CGFloat perspective;
@property (nonatomic, assign) CGFloat decelerationRate;
@property (nonatomic, assign) CGFloat scrollSpeed;
@property (nonatomic, assign) CGFloat bounceDistance;
@property (nonatomic, assign, getter = isScrollEnabled) BOOL scrollEnabled;
@property (nonatomic, assign, getter = isVertical) BOOL vertical;
@property (nonatomic, readonly, getter = isWrapEnabled) BOOL wrapEnabled;
@property (nonatomic, assign) BOOL bounces;
@property (nonatomic, assign) CGFloat scrollOffset;
@property (nonatomic, readonly) CGFloat offsetMultiplier;
@property (nonatomic, assign) CGSize contentOffset;
@property (nonatomic, assign) CGSize viewpointOffset;
@property (nonatomic, readonly) NSInteger numberOfItems;
@property (nonatomic, readonly) NSInteger numberOfPlaceholders;
@property (nonatomic, assign) NSInteger currentItemIndex;
@property (nonatomic, strong, readonly) UIView *currentItemView;
@property (nonatomic, strong, readonly) NSArray *indexesForVisibleItems;
@property (nonatomic, readonly) NSInteger numberOfVisibleItems;
@property (nonatomic, strong, readonly) NSArray *visibleItemViews;
@property (nonatomic, readonly) CGFloat itemWidth;
@property (nonatomic, strong, readonly) UIView *contentView;
@property (nonatomic, readonly) CGFloat toggle;
@property (nonatomic, assign) BOOL stopAtItemBoundary;
@property (nonatomic, assign) BOOL scrollToItemBoundary;
@property (nonatomic, assign) BOOL ignorePerpendicularSwipes;
@property (nonatomic, assign) BOOL centerItemWhenSelected;
@property (nonatomic, readonly, getter = isDragging) BOOL dragging;
@property (nonatomic, readonly, getter = isDecelerating) BOOL decelerating;
@property (nonatomic, readonly, getter = isScrolling) BOOL scrolling;

- (void)scrollByOffset:(CGFloat)offset duration:(NSTimeInterval)duration;
- (void)scrollToOffset:(CGFloat)offset duration:(NSTimeInterval)duration;
- (void)scrollByNumberOfItems:(NSInteger)itemCount duration:(NSTimeInterval)duration;
- (void)scrollToItemAtIndex:(NSInteger)index duration:(NSTimeInterval)duration;
- (void)scrollToItemAtIndex:(NSInteger)index animated:(BOOL)animated;

- (UIView *)itemViewAtIndex:(NSInteger)index;
- (NSInteger)indexOfItemView:(UIView *)view;
- (NSInteger)indexOfItemViewOrSubview:(UIView *)view;
- (CGFloat)offsetForItemAtIndex:(NSInteger)index;

- (void)removeItemAtIndex:(NSInteger)index animated:(BOOL)animated;
- (void)insertItemAtIndex:(NSInteger)index animated:(BOOL)animated;
- (void)reloadItemAtIndex:(NSInteger)index animated:(BOOL)animated;

- (void)reloadData;

@end

@protocol GalleryDataSource <NSObject>

- (NSUInteger)numberOfItemsInGallery:(GalleryView *)Gallery;
- (UIView *)gallery:(GalleryView *)Gallery viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view;

@optional

- (NSUInteger)numberOfPlaceholdersInGallery:(GalleryView *)Gallery;
- (UIView *)gallery:(GalleryView *)Gallery placeholderViewAtIndex:(NSUInteger)index reusingView:(UIView *)view;

@end


@protocol GalleryDelegate <NSObject>
@optional

- (void)galleryWillBeginScrollingAnimation:(GalleryView *)Gallery;
- (void)galleryDidEndScrollingAnimation:(GalleryView *)Gallery;
- (void)galleryDidScroll:(GalleryView *)Gallery;
- (void)galleryCurrentItemIndexDidChange:(GalleryView *)Gallery;
- (void)galleryWillBeginDragging:(GalleryView *)Gallery;
- (void)galleryDidEndDragging:(GalleryView *)Gallery willDecelerate:(BOOL)decelerate;
- (void)galleryWillBeginDecelerating:(GalleryView *)Gallery;
- (void)galleryDidEndDecelerating:(GalleryView *)Gallery;

- (BOOL)gallery:(GalleryView *)Gallery shouldSelectItemAtIndex:(NSInteger)index;
- (void)gallery:(GalleryView *)Gallery didSelectItemAtIndex:(NSInteger)index;

- (CGFloat)galleryItemWidth:(GalleryView *)Gallery;
- (CATransform3D)gallery:(GalleryView *)Gallery itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform;
- (CGFloat)gallery:(GalleryView *)Gallery valueForOption:(GalleryOption)option withDefault:(CGFloat)value;

- (void)galleryScrollY:(CGFloat)distance;


@end
