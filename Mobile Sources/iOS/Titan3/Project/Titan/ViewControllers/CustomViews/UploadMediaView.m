//
//  UploadMediaView.m
//  Amii
//
//  Created by Manuel Manzanera on 20/11/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "UploadMediaView.h"
#import "AppContext.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation UploadMediaView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
        
        UIView *backgroundView = [[UIView alloc] initWithFrame:self.frame];
        [backgroundView setBackgroundColor:[UIColor blackColor]];
        [backgroundView setAlpha:0.5];
        [backgroundView setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer *dismissTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissView)];
        [backgroundView addGestureRecognizer:dismissTap];
        
        [self addSubview:backgroundView];
        
        UIView *selectPictureView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH - [UtilManager width:50], [UtilManager height:245])];
        [selectPictureView setBackgroundColor:[UIColor whiteColor]];
        [[selectPictureView layer] setCornerRadius:8];

        [selectPictureView setCenter:CGPointMake(self.center.x, self.center.y)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., [UtilManager height:20], selectPictureView.frame.size.width, [UtilManager height:37])];
        [titleLabel setText:NSLocalizedString(@"Imagen", nil)];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setFont:[UIFont fontWithName:BOLD_FONT size:30]];
        [titleLabel setTextColor:YELLOW_APP_COLOR];
        [selectPictureView addSubview:titleLabel];
        
        UILabel *selectLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., titleLabel.frame.size.height + titleLabel.frame.origin.y + [UtilManager height:15], titleLabel.frame.size.width, titleLabel.frame.size.height)];
        [selectLabel setText:NSLocalizedString(@"Selecciona tu imagen desde:", nil)];
        [selectLabel setTextAlignment:NSTextAlignmentCenter];
        [selectLabel setFont:[UIFont fontWithName:LIGHT_FONT size:20]];
        [selectLabel setTextColor: [UIColor whiteColor]];
        [selectLabel setTextColor:[UIColor colorWithRed:68/255 green:68/255 blue:68/255 alpha:1]];
        [selectPictureView addSubview:selectLabel];
        
        UIImageView *cameraImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:64], selectLabel.frame.origin.y + selectLabel.frame.size.height + [UtilManager height:15], [UtilManager width:60], [UtilManager height:45])];
        [cameraImageView setContentMode:UIViewContentModeScaleAspectFit];
        [cameraImageView setImage:[UIImage imageNamed:@"image-camera"]];
        
        [selectPictureView addSubview:cameraImageView];
        
        _cameraButton = [[UIButton alloc] initWithFrame:CGRectMake(cameraImageView.frame.origin.x, cameraImageView.frame.origin.y + cameraImageView.frame.size.height + [UtilManager height:22], [UtilManager width:100], [UtilManager height:33])];
        [[_cameraButton layer] setCornerRadius:8];
        [_cameraButton setBackgroundColor:[UIColor lightGrayColor]];
        [_cameraButton setTitle:NSLocalizedString(@"Cámara", nil) forState:UIControlStateNormal];
        [[_cameraButton titleLabel] setFont:[UIFont fontWithName:SEMIBOLD_FONT size:20]];
        [_cameraButton setCenter:CGPointMake(cameraImageView.center.x, _cameraButton.center.y)];
        [_cameraButton addTarget:self action:@selector(uploadPictureFromCamara) forControlEvents:UIControlEventTouchUpInside];
        
        [selectPictureView addSubview:_cameraButton];
        
        UIImageView *galleryImageView = [[UIImageView alloc] initWithFrame:CGRectMake(selectPictureView.frame.size.width - cameraImageView.frame.size.width - [UtilManager width:61], cameraImageView.frame.origin.y- [UtilManager height:13], [UtilManager width:70], [UtilManager height:70])];
        [galleryImageView setImage:[UIImage imageNamed:@"image-gallery"]];
        [galleryImageView setContentMode:UIViewContentModeScaleAspectFit];
        
        [selectPictureView addSubview:galleryImageView];
        
        _galleryButton = [[UIButton alloc] initWithFrame:CGRectMake(galleryImageView.frame.origin.x, _cameraButton.frame.origin.y, [UtilManager width:100], [UtilManager height:33])];
        [[_galleryButton layer] setCornerRadius:8];
        [_galleryButton setBackgroundColor:[UIColor lightGrayColor]];
        [_galleryButton setTitle:NSLocalizedString(@"Galería", nil) forState:UIControlStateNormal];
        [_galleryButton setCenter:CGPointMake(galleryImageView.center.x, _galleryButton.center.y)];
        [[_galleryButton titleLabel] setFont:[UIFont fontWithName:SEMIBOLD_FONT size:20]];
        [_galleryButton addTarget:self action:@selector(uploadPictureFromGallery) forControlEvents:UIControlEventTouchUpInside];
        
        [selectPictureView addSubview:_galleryButton];
        
        [self addSubview:selectPictureView];
    }
    
    return self;
}

- (void)uploadPictureFromCamara{
    if(_delegate)
        [_delegate cameraDidSelected];
}

- (void)uploadPictureFromGallery{
    if(_delegate)
        [_delegate galleryDidselected];
}

- (void)dismissView{
    if(_delegate)
        [_delegate dismissViewSelected];
}



@end
