//
//  UIViewController+Present.m
//  NewsHom
//
//  Created by LMAN on 5/8/16.
//  Copyright © 2016 idragonit. All rights reserved.
//

#import "UIViewController+Present.h"

@implementation UIViewController (Present)
- (void)presentTransparentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion
{
//    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
//        [self presentIOS7TransparentController:viewControllerToPresent withCompletion:completion];
//
//    }
//    else {
        viewControllerToPresent.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        [self presentViewController:viewControllerToPresent animated:flag completion:completion];
//    }
}

-(void)presentIOS7TransparentController:(UIViewController *)viewControllerToPresent withCompletion:(void(^)(void))completion
{
    UIViewController *presentingVC = self;
    UIViewController *root = self;
    while (root.parentViewController) {
        root = root.parentViewController;
    }
    UIModalPresentationStyle orginalStyle = root.modalPresentationStyle;
    root.modalPresentationStyle = UIModalPresentationCurrentContext;
    [presentingVC presentViewController:viewControllerToPresent animated:YES completion:^{
        root.modalPresentationStyle = orginalStyle;
    }];
}
@end
