//
//  InfoViewController.h
//  Titan
//
//  Created by Marcus Lee on 6/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

@protocol InfoPopupViewControllerDelegate <NSObject>

- (void)didPressOKButtonOfInfoPopUp;

@end

@interface InfoPopupViewController : UIViewController

@property (nonatomic, strong) RLMObject * rlmObject;

@end
