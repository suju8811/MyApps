//
//  MenuHomeViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "MenuHomeViewController.h"
#import "MenuContainerNavController.h"
#import "HomeViewController.h"
#import "ProfileViewController.h"
#import "FunctionsTableViewController.h"

@interface MenuHomeViewController ()<REFrostedViewControllerDelegate>

@end

@implementation MenuHomeViewController

@synthesize functionTableViewController, containerViewController, container;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    HomeViewController *homeViewcontroller = [[HomeViewController alloc] init];
    FunctionsTableViewController *functionsTableViewController = [[FunctionsTableViewController alloc] init];
    containerViewController = [[MenuContainerNavController alloc] initWithRootViewController:homeViewcontroller];
    
    //container = [SideMenuContainerViewController containerWithCenterViewController:containerViewController leftMenuViewController:functionsTableViewController rightMenuViewController:nil];
    //[container setPanMode:SideMenuPanModeCenterViewController];
    
    [containerViewController setContainerDelegate:self];
    
    //[self.view addSubview:container.view];
    
    self.frostedViewController = [[REFrostedViewController alloc] initWithContentViewController:containerViewController menuViewController:functionsTableViewController];
    self.frostedViewController.direction = REFrostedViewControllerDirectionLeft;
    self.frostedViewController.liveBlurBackgroundStyle = REFrostedViewControllerLiveBackgroundStyleDark;
    self.frostedViewController.liveBlur = true;
    self.frostedViewController.delegate = self;
    self.frostedViewController.panGestureEnabled = TRUE;
    
    [functionsTableViewController setFrostedViewController:self.frostedViewController];
    [functionsTableViewController setMenuContainerNavController:containerViewController];
    
    [self.view addSubview:self.frostedViewController.view];
    
    [container setRightMenuWidth:0. animated:FALSE];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentProfileViewController) name:@"NOTIF_PROFILE_PRESENT" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(slideActionDidSelected) name:@"NOTIF_MENU_SLIDE" object:nil];
    
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWillShow{
}

- (void)keyboardWillHide{

}


#pragma mark ContainerViewControllerDelegate Methods

- (void)slideActionDidSelected
{
    /*
    if(container.menuState == SideMenuStateLeftMenuOpen)
    {
        [container setMenuState:SideMenuStateClosed completion:^{
            
        }];
    }
    else
    {
        //[functionTableViewController.functionTableView reloadData];
        [container setMenuState:SideMenuStateLeftMenuOpen completion:^{
            
        }];
    }
     */
    
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}

- (void)presentProfileViewController{
    
    ProfileViewController *profileViewController = [[ProfileViewController alloc] init];
    [profileViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    [self presentViewController:profileViewController animated:YES completion:nil];
}


#pragma mark FrostedViewController Delegate Methods

- (void)frostedViewController:(REFrostedViewController *)frostedViewController didRecognizePanGesture:(UIPanGestureRecognizer *)recognizer
{
    NSLog(@"Recognizer");
}

- (void)frostedViewController:(REFrostedViewController *)frostedViewController willShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willShowMenuViewController");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"willShowMenuViewController" object:nil];
}

- (void)frostedViewController:(REFrostedViewController *)frostedViewController didShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didShowMenuViewController");
}

- (void)frostedViewController:(REFrostedViewController *)frostedViewController willHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willHideMenuViewController");
}

- (void)frostedViewController:(REFrostedViewController *)frostedViewController didHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didHideMenuViewController");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"didHideMenuViewController" object:nil];
}

@end
