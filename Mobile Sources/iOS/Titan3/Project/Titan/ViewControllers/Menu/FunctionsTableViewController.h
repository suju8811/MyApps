//
//  FunctionsTableViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "REFrostedViewController.h"

@class MenuContainerNavController;

@protocol FunctionsTableViewDelegate;

@interface FunctionsTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *functionTableView;
@property (nonatomic, strong) MenuContainerNavController *menuContainerNavController;
@property (nonatomic, assign) id<FunctionsTableViewDelegate>delegate;
@property (nonatomic, strong) REFrostedViewController *frostedViewController;

@end

@protocol FunctionsTableViewDelegate <NSObject>

- (void)presentFunctionViewController:(UIViewController *)viewController;

@end
