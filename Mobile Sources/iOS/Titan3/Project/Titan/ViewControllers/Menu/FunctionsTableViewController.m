//
//  FunctionsTableViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FunctionsTableViewController.h"

#import "SideMenuContainerViewController.h"
#import "UIViewController+SideMenuAdditions.h"
#import "UIViewController+REFrostedViewController.h"
#import "UIImageView+AFNetworking.h"

#import "UtilManager.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "MenuCell.h"
#import "AppDelegate.h"
#import "RegisterNavController.h"
#import "InitViewController.h"
#import "MenuContainerNavController.h"
#import "RegisterNavController.h"
#import "HomeViewController.h"
#import "ScheduleViewController.h"
#import "FitnessViewController.h"
#import "LoginViewController.h"
#import "FoodViewController.h"
#import "ProgressViewController.h"
#import "UserDefaultLibrary.h"
#import "ProfileViewController.h"
#import "AccountViewController.h"
#import "OptionsSettingViewController.h"

#import "AppContext.h"
#import "WebServiceManager.h"
#import "RateViewController.h"

@interface FunctionsTableViewController () <WebServiceManagerDelegate, OptionsSettingViewControllerDelegate>

@property (nonatomic, strong) NSArray *sections;
@property (nonatomic, strong) NSArray *icons;
@property (nonatomic, assign) NSInteger rowSelected;

@end

@implementation FunctionsTableViewController

@synthesize sections, functionTableView, menuContainerNavController, delegate;

- (void)viewDidLoad{
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentRegisterSaleViewController:) name:@"REGISTER_SALE_NOTIFICATION" object:nil];
    
    [super viewDidLoad];
    
    if ([AppContext sharedInstance].isClient) {
        if ([AppContext sharedInstance].isAuthenticateClient) {
            sections = @[NSLocalizedString(@"INICIO", nil),NSLocalizedString(@"CALENDARIO", nil),NSLocalizedString(@"ENTRENAMIENTO", nil),NSLocalizedString(@"NUTRICIÓN", nil),NSLocalizedString(@"PROGRESO", nil),NSLocalizedString(@"COMUNIDAD", nil)];
        }
        else {
            sections = @[NSLocalizedString(@"INICIO", nil),NSLocalizedString(@"CALENDARIO", nil),NSLocalizedString(@"ENTRENAMIENTO", nil),NSLocalizedString(@"NUTRICIÓN", nil),NSLocalizedString(@"PROGRESO", nil),NSLocalizedString(@"COMUNIDAD", nil)];
        }
    }
    else {
        sections = @[NSLocalizedString(@"INICIO", nil),NSLocalizedString(@"CALENDARIO", nil),NSLocalizedString(@"ENTRENAMIENTO", nil),NSLocalizedString(@"NUTRICIÓN", nil),NSLocalizedString(@"PROGRESO", nil),NSLocalizedString(@"COMUNIDAD", nil)];
    }
    
    _icons = @[[UIImage imageNamed:@"icon-home"],[UIImage imageNamed:@"icon-calendar"],[UIImage imageNamed:@"icon-fitness"],[UIImage imageNamed:@"icon-food"],[UIImage imageNamed:@"icon-progress"],[UIImage imageNamed:@"icon-comunity-off"]];
    
    [self configureView];
}

- (void)settings {
    OptionsSettingViewController * optionSettingsViewController = [[OptionsSettingViewController alloc] init];
    UINavigationController * settingNavigationViewController = [[UINavigationController alloc] initWithRootViewController:optionSettingsViewController];
    [settingNavigationViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    optionSettingsViewController.delegate = self;
    [self presentViewController:settingNavigationViewController animated:YES completion:nil];
}

- (void)logout {
    //[WebServiceManager logoutWithDelegate:self];
    
    [UserDefaultLibrary removeFromKey:IS_USER_LOGIN];
    [UserDefaultLibrary removeFromKey:USER_NAME];
    [UserDefaultLibrary removeFromKey:USER_PASSWORD];
    
    [[AppContext sharedInstance] setCurrentUser:nil];
    
    InitViewController *loginViewController = [[InitViewController alloc] init];
    RegisterNavController *signNavController = [[RegisterNavController alloc] initWithRootViewController:loginViewController];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:signNavController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:signNavController];
     }];
}

- (void)configureView
{
    UIView *backgroundAlphaView = [[UIView alloc] initWithFrame:CGRectMake(0., 0, WIDTH * 0.8, self.view.frame.size.height)];
    [backgroundAlphaView setBackgroundColor:[UIColor blackColor]];
    [backgroundAlphaView setAlpha:0.65];
    
    [self.view addSubview:backgroundAlphaView];
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    _rowSelected = 0;
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20.], [UtilManager height:29], WIDTH, [UtilManager height:32])];
    [nameLabel setTextColor:[UIColor whiteColor]];
    [self.view addSubview:nameLabel];
    
    functionTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., 0, FUNCTION_VIEWCONTROLLER_WIDTH, self.view.frame.size.height)];
    [functionTableView setBackgroundColor:[UIColor clearColor]];
    [functionTableView setBounces:FALSE];
    [functionTableView setDelegate:self];
    [functionTableView setDataSource:self];
    [functionTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [functionTableView setScrollEnabled:FALSE];
    [functionTableView setBounces:FALSE];
    [functionTableView registerClass:[MenuCell class] forCellReuseIdentifier:@"cell"];
    
    [self.view addSubview:functionTableView];
    
    UIView *tapView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH * 0.8,0, WIDTH * 0.2,HEIGHT)];
    [tapView setBackgroundColor:[UIColor clearColor]];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture addTarget:self action:@selector(didPressBackButton)];
    
    [tapView addGestureRecognizer:tapGesture];
    
    [self.view addSubview:tapView];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 50.,[UtilManager height:30.], [UtilManager width:40], [UtilManager width:40])];
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton setHidden:FALSE];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    [backButton setCenter:CGPointMake(backButton.center.x, NAV_HEIGHT/2 + [UtilManager height:5])];
    
    //[self.view  addSubview:backButton];
}

- (void)didPressBackButton {
    [_frostedViewController hideMenuViewControllerWithCompletionHandler:^{
        NSLog(@"BACK ACTION");
    }];
}

#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [[tableViewCell nameLabel] setText:[sections objectAtIndex:indexPath.row]];
    [[tableViewCell icon] setImage:[_icons objectAtIndex:indexPath.row]];
    
    if(indexPath.row == _rowSelected)
    {
        [[tableViewCell nameLabel] setTextColor:YELLOW_APP_COLOR];
        tableViewCell.icon.image = [tableViewCell.icon.image  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [tableViewCell.icon setTintColor:YELLOW_APP_COLOR];
    } else 
        [[tableViewCell nameLabel] setTextColor:GRAY_REGISTER_FONT];
    
    [tableViewCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return tableViewCell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return sections.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:10])];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    UIImage *image = [UIImage imageNamed:@"img-logo"];
    UIImageView *imageView  =[[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:40], image.size.width * 0.8, image.size.height * 0.8)];
    [imageView setImage:image];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    //[headerView addSubview:imageView];
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:310])];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:50], [UtilManager height:20], [UtilManager height:180], [UtilManager height:180])];
    [[profileImageView layer] setCornerRadius:profileImageView.frame.size.width/2];
    [[profileImageView layer] setMasksToBounds:YES];
    
    UIImageView *imageView = profileImageView;
    
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE,[[[AppContext sharedInstance] currentUser] picture]]]
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    
    [profileImageView setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-small"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        [imageView setImage:image];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [[imageView layer] setCornerRadius:imageView.frame.size.width/2];
        [[imageView layer] setMasksToBounds:YES];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        NSLog(@"%@",error);
        [imageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
    }];
    
    [profileImageView setCenter:CGPointMake(WIDTH * 0.4, profileImageView.center.y)];
    
    [footerView addSubview:profileImageView];
    
    UILabel *userLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], profileImageView.frame.origin.y + profileImageView.frame.size.height + [UtilManager height:10], [UtilManager width:160], [UtilManager height:25])];
    [userLabel setCenter:CGPointMake(profileImageView.center.x, userLabel.center.y)];
    [userLabel setTextColor:YELLOW_APP_COLOR];
    [userLabel setFont:[UIFont fontWithName:REGULAR_FONT size:17]];
    [userLabel setTextAlignment:NSTextAlignmentCenter];
    [userLabel setText:@"NOMBRE DE USUARIO"];
    
    NSString *name = [[AppContext sharedInstance] currentUser].name;
    NSString *surname = [[AppContext sharedInstance] currentUser].lastName;
    
    NSString *labelName;
    
    if(name.length > 0 && surname.length >0)
        labelName = [NSString stringWithFormat:@"%@ %@",name,surname];
    else if (name.length > 0)
        labelName = [NSString stringWithFormat:@"%@",name];
    else if (surname.length > 0)
        labelName = [NSString stringWithFormat:@"%@",surname];
    else
        labelName = @"";
    
    [userLabel setText:labelName];
    [userLabel setAdjustsFontSizeToFitWidth:YES];
    
    [footerView addSubview:userLabel];
    
    UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(userLabel.frame.origin.x, userLabel.frame.origin.y + userLabel.frame.size.height, userLabel.frame.size.width, [UtilManager height:18])];
    [emailLabel setTextAlignment:NSTextAlignmentCenter];
    [emailLabel setTextColor:[UIColor whiteColor]];
    [emailLabel setText:[UserDefaultLibrary getWithKey:USER_NAME]];
    [emailLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    [emailLabel setAdjustsFontSizeToFitWidth:YES];
    
    [footerView addSubview:emailLabel];
    
    UIButton *settingsButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], emailLabel.frame.origin.y + emailLabel.frame.size.height + [UtilManager height:15], [UtilManager width:40], [UtilManager height:40])];
    [settingsButton setImage:[UIImage imageNamed:@"btn-settings"] forState:UIControlStateNormal];
    [settingsButton addTarget:self action:@selector(settings) forControlEvents:UIControlEventTouchUpInside];
    
    [footerView addSubview:settingsButton];
    
    UIButton *logoutButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH * 0.8 - [UtilManager width:50], settingsButton.frame.origin.y, settingsButton.frame.size.width, settingsButton.frame.size.height)];
    [logoutButton setImage:[UIImage imageNamed:@"btn-logout"] forState:UIControlStateNormal];
    [logoutButton addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    
    NSLog(@"%f",logoutButton.frame.origin.y + logoutButton.frame.size.height);
    
    [footerView addSubview:logoutButton];

    return footerView;
    
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section
{
    return [UtilManager height:30];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [UtilManager height:30];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.08 * HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.08 * HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return [UtilManager height:310];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return [UtilManager height:310];
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if([[sections objectAtIndex:indexPath.row] isEqualToString:NSLocalizedString(@"INICIO", nil)]) {
        [self presentHomeViewController:nil];
    }
    else if([[sections objectAtIndex:indexPath.row] isEqualToString:NSLocalizedString(@"CALENDARIO", nil)]) {
        [self presentScheduleViewController:nil];
    }
    else if([[sections objectAtIndex:indexPath.row] isEqualToString:NSLocalizedString(@"ENTRENAMIENTO", nil)]) {
        [self presentFitnessViewController:nil];
    }
    else if([[sections objectAtIndex:indexPath.row] isEqualToString:NSLocalizedString(@"NUTRICIÓN", nil)]) {
        [self presentFoodViewController:nil];
    }
    else if([[sections objectAtIndex:indexPath.row] isEqualToString:NSLocalizedString(@"PROGRESO", nil)]) {
        [self presentProgressViewController:nil];
    }
    else if([[sections objectAtIndex:indexPath.row] isEqualToString:NSLocalizedString(@"COMUNIDAD", nil)]) {
        [self presentCommunityViewcontroller:nil];
    }
    else if([[sections objectAtIndex:indexPath.row] isEqualToString:NSLocalizedString(@"CERRAR SESIÓN", nil)]) {
         [self logout];
    }
    else if([[sections objectAtIndex:indexPath.row] isEqualToString:NSLocalizedString(@"INICIAR SESIÓN", nil)]) {
        [self logout];
    }
    
    _rowSelected = indexPath.row;
    
    [tableView reloadData];
}


- (void)dismissAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)presentHomeViewController:(NSNotification *)notification{
    HomeViewController *homeViewController = [[HomeViewController alloc] init];
    
    NSArray *controllers;
    controllers = [NSArray arrayWithObject:homeViewController];
    
    menuContainerNavController.viewControllers = controllers;
    //[self.menuContainerViewController setMenuState:SideMenuStateClosed];
    
    _rowSelected = 0;
    
    self.frostedViewController.contentViewController = menuContainerNavController;
    [self.frostedViewController hideMenuViewControllerWithCompletionHandler:nil];
}

- (void)presentScheduleViewController:(NSNotification *)notification{
 
    ScheduleViewController *scheduleViewController = [[ScheduleViewController alloc] init];
    
    _rowSelected = 1;
    
    NSArray *controllers;
    controllers = [NSArray arrayWithObject:scheduleViewController];
    menuContainerNavController.viewControllers = controllers;
    
    self.frostedViewController.contentViewController = menuContainerNavController;
    [self.frostedViewController hideMenuViewControllerWithCompletionHandler:nil];
    
}

- (void)presentFitnessViewController:(NSNotification *)notification{
    FitnessViewController *fitnessViewController = [[FitnessViewController alloc] init];
    
    _rowSelected = 2;
    
    NSArray *controllers;
    controllers = [NSArray arrayWithObject:fitnessViewController];
    menuContainerNavController.viewControllers = controllers;
    //[self.menuContainerViewController setMenuState:SideMenuStateClosed];
    
    self.frostedViewController.contentViewController = menuContainerNavController;
    [self.frostedViewController hideMenuViewControllerWithCompletionHandler:nil];
}

- (void)presentFoodViewController:(NSNotification *)notification{
    
    FoodViewController *foodViewController = [[FoodViewController alloc] init];
    
    _rowSelected = 3;
    
    NSArray *controllers;
    controllers = [NSArray arrayWithObject:foodViewController];
    menuContainerNavController.viewControllers = controllers;
    //[self.menuContainerViewController setMenuState:SideMenuStateClosed];
    
    self.frostedViewController.contentViewController = menuContainerNavController;
    [self.frostedViewController hideMenuViewControllerWithCompletionHandler:nil];
}

- (void)presentProgressViewController:(NSNotification *)notification{
    ProgressViewController *progressViewController = [[ProgressViewController alloc] init];
    
    NSArray *controllers;
    controllers = [NSArray arrayWithObject:progressViewController];
    menuContainerNavController.viewControllers = controllers;
    //[self.menuContainerViewController setMenuState:SideMenuStateClosed];
    
    _rowSelected = 4;
    
    self.frostedViewController.contentViewController = menuContainerNavController;
    [self.frostedViewController hideMenuViewControllerWithCompletionHandler:nil];
}

- (void)presentCommunityViewcontroller:(NSNotification *)notification {
    UIStoryboard * communityStoryboard = [UIStoryboard storyboardWithName:@"Community" bundle:nil];
    UITabBarController * communityTabBarController = [communityStoryboard instantiateInitialViewController];
    
//    NSArray *controllers;
//    controllers = [NSArray arrayWithObject:communityTabBarController];
//    menuContainerNavController.viewControllers = controllers;
    
    [menuContainerNavController pushViewController:communityTabBarController animated:YES];
    
    _rowSelected = 5;
    
//    self.frostedViewController.contentViewController = menuContainerNavController;
    [self.frostedViewController hideMenuViewControllerWithCompletionHandler:nil];
}

- (BOOL)prefersStatusBarHidden{
    return false;
}

#pragma mark WebServiceManagerDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{

}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{

}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
}

#pragma mark - OptionsSettingViewControllerDelegate Methods

- (void)didSelectSettingItemAtIndex:(NSInteger)index {
    UIStoryboard * settingStoryboard = [UIStoryboard storyboardWithName:@"Setting" bundle:nil];
    
    if (index == 0) {
        ProfileViewController *profileViewController = [[ProfileViewController alloc] init];
        [profileViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        [_frostedViewController.contentViewController presentViewController:profileViewController animated:YES completion:nil];
    }
    else if (index == 1) {
        AccountViewController *accountViewController = [[AccountViewController alloc] init];
        [accountViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        [_frostedViewController.contentViewController presentViewController:accountViewController animated:YES completion:nil];
    }
    else if (index == 5) {
        NSString * message = @"Titan3 - World is the Training. \n\n https://itunes.apple.com/al/app/titan3/id912399912?mt=8";
        NSArray * arrayOfActivityItems = [NSArray arrayWithObjects:message, nil];
        UIActivityViewController * activityViewController = [[UIActivityViewController alloc] initWithActivityItems:arrayOfActivityItems applicationActivities:nil];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            activityViewController.modalPresentationStyle = UIModalPresentationPopover;
        }
        
        [_frostedViewController.contentViewController presentViewController:activityViewController animated:YES completion:nil];
    }
    else if (index == 6) {
        RateViewController * rateViewController = [settingStoryboard instantiateViewControllerWithIdentifier:RATE_APP_VIEW_STORY_BOARD_ID];
        [menuContainerNavController pushViewController:rateViewController animated:YES];
    }
    
    [_frostedViewController hideMenuViewController];
}

@end
