//
//  MenuCell.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "MenuCell.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation MenuCell

@synthesize nameLabel, icon;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        
        [self setBackgroundColor:[UIColor clearColor]];
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        
        icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:25], [UtilManager height:10], [UtilManager width:30], [UtilManager height:30])];
        [icon setBackgroundColor:[UIColor clearColor]];
        [icon setContentMode:UIViewContentModeScaleAspectFit];
        [icon setCenter:CGPointMake(icon.center.x, 0.04 * HEIGHT)];
        
        [[self contentView] addSubview:icon];
        
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(icon.frame.origin.x + icon.frame.size.width + [UtilManager width:15], 0., [UtilManager width:258], 44)];
        [nameLabel setTextAlignment:NSTextAlignmentLeft];
        [nameLabel setTextColor:GRAY_REGISTER_FONT];
        [nameLabel setCenter:CGPointMake(nameLabel.center.x, icon.center.y)];
        [nameLabel setFont:[UIFont fontWithName:REGULAR_FONT size:21]];
        [nameLabel setBackgroundColor:[UIColor clearColor]];
        
        [[self contentView] addSubview:nameLabel];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:10], 0.08 * HEIGHT - 1, WIDTH * 0.8 - [UtilManager width:20], 1)];
        [_separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        [[self contentView] addSubview:_separatorView];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:FALSE animated:FALSE];
    
    if(animated){
        [nameLabel setTextColor:YELLOW_APP_COLOR];
        icon.image = [icon.image  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [icon setTintColor:YELLOW_APP_COLOR];
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    
    if(highlighted){
        [nameLabel setTextColor:YELLOW_APP_COLOR];
        icon.image = [icon.image  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [icon setTintColor:YELLOW_APP_COLOR];
    }
}

@end
