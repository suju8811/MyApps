//
//  UIViewController+SideMenuAdditions.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "UIViewController+SideMenuAdditions.h"
#import "SideMenuContainerViewController.h"

@implementation UIViewController (SideMenuAdditions)

@dynamic menuContainerViewController;

- (SideMenuContainerViewController *)menuContainerViewController
{
    id containerView = self;
    while (![containerView isKindOfClass:[SideMenuContainerViewController class]] && containerView)
    {
        if ([containerView respondsToSelector:@selector(parentViewController)])
            containerView = [containerView parentViewController];
        if ([containerView respondsToSelector:@selector(splitViewController)] && !containerView)
            containerView = [containerView splitViewController];
    }
    return containerView;
}

@end
