//
//  MenuHomeViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FunctionsTableViewController.h"
#import "MenuContainerNavController.h"
#import "SideMenuContainerViewController.h"
#import "REFrostedViewController.h"

#import "ParentViewController.h"

@interface MenuHomeViewController : ParentViewController <MenuContainerNavControllerDelegate>

@property (nonatomic, strong) SideMenuContainerViewController *container;
@property (nonatomic, strong) MenuContainerNavController *containerViewController;
@property (nonatomic, strong) FunctionsTableViewController *functionTableViewController;
@property (nonatomic, strong) REFrostedViewController *frostedViewController;

@end
