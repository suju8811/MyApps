//
//  MenuCell.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, assign) BOOL isSelected;

@end
