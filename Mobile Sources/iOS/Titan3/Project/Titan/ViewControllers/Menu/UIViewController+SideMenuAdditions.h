//
//  UIViewController+SideMenuAdditions.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SideMenuContainerViewController;

@interface UIViewController (SideMenuAdditions)

@property(nonatomic,readonly,retain) SideMenuContainerViewController *menuContainerViewController;

@end
