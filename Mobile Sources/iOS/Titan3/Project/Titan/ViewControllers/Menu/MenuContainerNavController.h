//
//  MenuContainerNavController.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"

@protocol MenuContainerNavControllerDelegate;


@interface MenuContainerNavController : UINavigationController <UINavigationControllerDelegate>

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIButton *slideButton;
@property (nonatomic, strong) UIButton *rightButton1;
@property (nonatomic, strong) UIButton *rightButton2;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UIButton *notificationButton;
@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, strong) UIView *searchContainerView;
@property (nonatomic, strong) UISearchBar * searchBar;

@property (nonatomic, weak) id<MenuContainerNavControllerDelegate>containerDelegate;

@property (nonatomic, strong) UIViewController * currentViewController;

- (void)showSearchBar:(BOOL)isShow;

@end

@protocol MenuContainerNavControllerDelegate <NSObject>

- (void)slideActionDidSelected;

@end
