//
//  MenuContainerNavController.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "MenuContainerNavController.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "UtilManager.h"

#import "HomeViewController.h"
#import "ScheduleViewController.h"
#import "ScheduleDetailViewController.h"
#import "ProfileViewController.h"
#import "FitnessViewController.h"
#import "FoodViewController.h"
#import "ProgressViewController.h"
#import "ExerciseDetailViewController.h"
#import "LogDetailViewController.h"
#import "PictureVisorViewController.h"
#import "EvolutionBodyViewController.h"
#import "EvolutionPerformanceViewController.h"
#import "MealDetailViewController.h"
#import "TrainingPlanDetailViewController.h"
#import "TrainingAddViewController.h"
#import "TrainingPlanAddViewController.h"
#import "SelectorTableViewController.h"
#import "SelectorFoodViewController.h"
#import "WorkoutDetailViewController.h"
#import "SeriesViewController.h"
#import "DietDetailViewController.h"
#import "NutritionPlanDetailViewController.h"
#import "CommunityViewController.h"
#import "ExercisesViewController.h"
#import "RateViewController.h"
#import "ReposeViewController.h"
#import "EventViewController.h"

#import "UIViewController+REFrostedViewController.h"

#import "TransparentView.h"
#import "CommonViewController.h"

@interface MenuContainerNavController () <UISearchBarDelegate>

@property (nonatomic, assign) CGFloat spaceBar;
@property (nonatomic, strong) TransparentView *transparentView;

@end

@implementation MenuContainerNavController

@synthesize containerView;
@synthesize slideButton, rightButton1, rightButton2, titleLabel, icon;
@synthesize separatorView, spaceBar, searchContainerView;
@synthesize containerDelegate, notificationButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationBar] setHidden:YES];
    [self setDelegate:self];
    
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.interactivePopGestureRecognizer.enabled = NO;
    }

    containerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, NAV_HEIGHT)];
    [containerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    icon = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:18], [UtilManager height:30], [UtilManager width:30.], [UtilManager height:30])];
    [icon setBackgroundColor:[UIColor clearColor]];
    [icon setContentMode:UIViewContentModeScaleAspectFit];
    [containerView addSubview:icon];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, containerView.frame.size.height - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setText:NSLocalizedString(@"INICIO", nil)];
    [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [containerView addSubview:titleLabel];
    
    slideButton  = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], 0, [UtilManager width:40], [UtilManager height:40])];
    [slideButton setImage:[UIImage imageNamed:@"btn-menu"] forState:UIControlStateNormal];
    [slideButton addTarget:self action:@selector(slideAction) forControlEvents:UIControlEventTouchUpInside];
    [slideButton setCenter:CGPointMake(slideButton.center.x, titleLabel.center.y)];
    [slideButton setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    [slideButton setBackgroundColor:[UIColor clearColor]];
    
    [containerView addSubview:slideButton];
    
    //[titleLabel setCenter:CGPointMake(titleLabel.center.x, slideButton.center.y)];
    
    notificationButton = [[UIButton alloc] initWithFrame:CGRectMake(slideButton.frame.origin.x - [UtilManager width:30.] - [UtilManager width:4.], [UtilManager height:30], [UtilManager width:30.],[UtilManager height:40])];
    [notificationButton setImage:[UIImage imageNamed:@"btn-notification"] forState:UIControlStateNormal];
    [notificationButton addTarget:self action:@selector(notificationAction) forControlEvents:UIControlEventTouchUpInside];
    [notificationButton setCenter:CGPointMake(notificationButton.center.x, slideButton.center.y)];
    [notificationButton setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    
    rightButton1 = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:40],
                                                              [UtilManager height:30.],
                                                              [UtilManager width:40],
                                                              [UtilManager width:40])];
    
    [rightButton1 setImage:[UIImage imageNamed:@"btn-msg"] forState:UIControlStateNormal];
    [rightButton1 addTarget:self action:@selector(didPressRight1Button) forControlEvents:UIControlEventTouchUpInside];
    [rightButton1 setHidden:NO];
    [rightButton1 setBackgroundColor:[UIColor clearColor]];
    [rightButton1 setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    [rightButton1 setCenter:CGPointMake(rightButton1.center.x, slideButton.center.y)];
    [containerView addSubview:rightButton1];
    
    rightButton2 = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:35] * 2,
                                                              [UtilManager height:30.],
                                                              [UtilManager width:40],
                                                              [UtilManager width:40])];
    
    [rightButton2 setImage:[UIImage imageNamed:@"icon_search"] forState:UIControlStateNormal];
    [rightButton2 addTarget:self action:@selector(didPressRight2Button) forControlEvents:UIControlEventTouchUpInside];
    [rightButton2 setHidden:NO];
    [rightButton2 setBackgroundColor:[UIColor clearColor]];
    [rightButton2 setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    [rightButton2 setCenter:CGPointMake(rightButton2.center.x, slideButton.center.y)];
    [containerView addSubview:rightButton2];
    rightButton2.hidden = YES;
    
    spaceBar = self.view.frame.size.width - separatorView.frame.origin.x - separatorView.frame.size.width;
    
    separatorView = [[UIView alloc] initWithFrame:CGRectMake(0.,[UtilManager height:79], WIDTH, 1.)];
    [separatorView setBackgroundColor:GRAY_REGISTER_FONT];
    //[containerView addSubview:separatorView];
    
    [[self view] addSubview:containerView];
    
    _transparentView = [[TransparentView alloc] initWithFrame:self.view.frame];
    //[self.view addSubview:transparentView];
    
    //
    // Search View
    //
    
    self.searchBar = [[UISearchBar alloc] init];
    
    self.searchBar.delegate = self;
    self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    
    [self.searchBar setBarTintColor:BLACK_APP_COLOR];
    [self.searchBar setKeyboardAppearance:UIKeyboardAppearanceDark];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]
     setTintColor:[UIColor whiteColor]];
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]
     setTintColor:[UIColor whiteColor]];
    self.searchBar.tintColor =[UIColor whiteColor];
    
    for (UIView *subView in self.searchBar.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                searchBarTextField.textColor = [UIColor whiteColor];
                break;
            }
        }
    }
    
    searchContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH - CGRectGetWidth(rightButton1.frame), [UtilManager height:40])];
    searchContainerView.center = CGPointMake((WIDTH - CGRectGetWidth(rightButton1.frame)) / 2.f,
                                             CGRectGetMinY(slideButton.frame) + CGRectGetHeight(slideButton.frame) / 2.f);
    
    [searchContainerView addSubview:self.searchBar];
    [searchContainerView setBackgroundColor:[UIColor clearColor]];
    self.searchBar.frame = searchContainerView.frame;
    [self.searchBar setCenter:CGPointMake(searchContainerView.frame.size.width/2,searchContainerView.frame.size.height/2)];
    
    [self.view addSubview:searchContainerView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showBlurView) name:@"willShowMenuViewController" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideBlurView) name:@"didHideMenuViewController" object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[containerView setHidden:FALSE];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"willShowMenuViewController" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didHideMenuViewController" object:nil];
}

- (void)showBlurView{
    [self.view addSubview:_transparentView];
}

- (void)hideBlurView{
    [_transparentView removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)slideAction
{
    if([self.visibleViewController isKindOfClass:[LogDetailViewController class ]] ||
       [self.visibleViewController isKindOfClass:[EvolutionViewController class]] ||
       [self.visibleViewController isKindOfClass:[PictureVisorViewController class]]) {
        
        [self popViewControllerAnimated:NO];
    }else if ([self.visibleViewController isKindOfClass:[TrainingAddViewController class]])
        [self popViewControllerAnimated:NO];
    else if([containerDelegate respondsToSelector:@selector(slideActionDidSelected)])
        [containerDelegate slideActionDidSelected];
    
}

#pragma mark -
#pragma mark Gesture recognizer

- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender
{
    // Dismiss keyboard (optional)
    
//    [self.view endEditing:YES];
//    [self.frostedViewController.view endEditing:YES];
//    
//    // Present the view controller
//    //
//    [self.frostedViewController panGestureRecognized:sender];
}


- (void)notificationAction{
   
}

- (void)didPressRight1Button {
    
    if([self.visibleViewController isKindOfClass:[HomeViewController class]] ||
       [self.visibleViewController isKindOfClass:[FitnessViewController class]] ||
       [self.visibleViewController isKindOfClass:[ProgressViewController class]] ||
       [self.visibleViewController isKindOfClass:[TrainingAddViewController class]] ||
       [self.visibleViewController isKindOfClass:[LogDetailViewController class]] ||
       [self.visibleViewController isKindOfClass:[EvolutionViewController class]] ||
       [self.visibleViewController isKindOfClass:[ScheduleViewController class]] ||
       [self.visibleViewController isKindOfClass:[FoodViewController class]]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NAV_RIGHT1_ACTION object:nil];
    }else if([self.visibleViewController isKindOfClass:[WorkoutDetailViewController class]]){
        [self popToViewController:[[self viewControllers] objectAtIndex:0] animated:YES];
    }
    else {
        [self popViewControllerAnimated:NO];
    }
}

- (void)didPressRight2Button {
    
    if([self.visibleViewController isKindOfClass:[FitnessViewController class]] ||
       [self.visibleViewController isKindOfClass:[ProgressViewController class]] ||
       [self.visibleViewController isKindOfClass:[TrainingAddViewController class]] ||
       [self.visibleViewController isKindOfClass:[LogDetailViewController class]] ||
       [self.visibleViewController isKindOfClass:[EvolutionViewController class]] ||
       [self.visibleViewController isKindOfClass:[ScheduleViewController class]] ||
       [self.visibleViewController isKindOfClass:[FoodViewController class]]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NAV_RIGHT2_ACTION object:nil];
    }
    else if([self.visibleViewController isKindOfClass:[WorkoutDetailViewController class]]){
        [self popToViewController:[[self viewControllers] objectAtIndex:0] animated:YES];
    }
    else {
        [self popViewControllerAnimated:NO];
    }
}

- (void)profileAction:(NSNotification *)notification{
    
    /*
     */
}

- (void)presentNotificationViewController:(NSNotification *)notification{
    
}

- (void)presentWebViewController:(NSNotification *)notification{
}

#pragma mark UINavigationControllerDelegate Methods

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    _currentViewController = viewController;
    [self showSearchBar:NO];
    containerView.hidden = NO;
    
    rightButton2.hidden = YES;
    
    if (![viewController isKindOfClass:[ExercisesViewController class]]) {
        containerView.hidden = NO;
        rightButton1.hidden = NO;
        [rightButton1 setImage:[UIImage imageNamed:@"btn-msg"] forState:UIControlStateNormal];
        [slideButton setImage:[UIImage imageNamed:@"btn-menu"] forState:UIControlStateNormal];
        separatorView.hidden = NO;
    }
    
    if ([viewController isKindOfClass:[RateViewController class]] ||
        [viewController isKindOfClass:[UITabBarController class]] ||
        [viewController isKindOfClass:[ReposeViewController class]] ||
        [viewController isKindOfClass:[EventViewController class]]) {
        
        containerView.hidden = YES;
    }
    
    if([viewController isKindOfClass:[HomeViewController class]]){
        titleLabel.text = NSLocalizedString(@"INICIO", nil);
    }
    
    if([viewController isKindOfClass:[ScheduleViewController class]]){
        [titleLabel setText:NSLocalizedString(@"CALENDARIO", nil)];
        [rightButton1 setHidden:NO];
        [rightButton1 setImage:[UIImage imageNamed:@"btn-add"] forState:UIControlStateNormal];
    }
    
    if([viewController isKindOfClass:[ScheduleDetailViewController class]]){
        [titleLabel setText:NSLocalizedString(@"CALENDARIO", nil)];
        [rightButton1 setHidden:NO];
        [rightButton1 setImage:[UIImage imageNamed:@"btn-menu"] forState:UIControlStateNormal];
    }
    if([viewController isKindOfClass:[FitnessViewController class]]){
        [rightButton1 setHidden:NO];
        [rightButton1 setImage:[UIImage imageNamed:@"icon_search"] forState:UIControlStateNormal];
        [rightButton2 setImage:[UIImage imageNamed:@"btn-add"] forState:UIControlStateNormal];
        rightButton2.hidden = NO;
    }
    if([viewController isKindOfClass:[FoodViewController class]]){
        [rightButton1 setHidden:NO];
        [rightButton1 setImage:[UIImage imageNamed:@"icon_search"] forState:UIControlStateNormal];
        [rightButton2 setImage:[UIImage imageNamed:@"btn-add"] forState:UIControlStateNormal];
        rightButton2.hidden = NO;
    }
    if([viewController isKindOfClass:[ProgressViewController class]]){
        [rightButton1 setHidden:NO];
        [titleLabel setText:NSLocalizedString(@"ESTADÍSTICAS", nil)];
        [rightButton1 setImage:[UIImage imageNamed:@"btn-add"] forState:UIControlStateNormal];
    }
    
    if([viewController isKindOfClass:[LogDetailViewController class]]){
        [titleLabel setText:NSLocalizedString(@"LOG", nil)];
        
        [containerView setHidden:YES];
        
        [rightButton1 setImage:nil forState:UIControlStateNormal];
        [rightButton1 setTitle:@"OK" forState:UIControlStateNormal];
        [rightButton1 setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [rightButton1 setHidden:NO];
        [slideButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    }
    if([viewController isKindOfClass:[PictureVisorViewController class]]){
        //[titleLabel setText:NSLocalizedString(@"LOG", nil)];
        [rightButton1 setImage:nil forState:UIControlStateNormal];
        [rightButton1 setTitle:@"OK" forState:UIControlStateNormal];
        [rightButton1 setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [rightButton1 setHidden:TRUE];
        [slideButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    }
    
    if ([viewController isKindOfClass:[EvolutionViewController class]]) {
        [titleLabel setText:NSLocalizedString(@"", nil)];
        [rightButton1 setHidden:FALSE];
        [rightButton1 setImage:[UIImage imageNamed:@"btn-menu-settings"] forState:UIControlStateNormal];
        [slideButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    }
    
    if([viewController isKindOfClass:[ExerciseDetailViewController class]]){
        [titleLabel setText:NSLocalizedString(@"Detalle de ejercicio", nil)];
        [rightButton1 setHidden:false];
         [rightButton1 setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    }
    
    if([viewController isKindOfClass:[TrainingAddViewController class]]){
        [titleLabel setText:NSLocalizedString(@"NUEVO ENTRENAMIENTO", nil)];
        [rightButton1 setHidden:YES];
        [rightButton1 setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
        [slideButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    }
    
    if([viewController isKindOfClass:[TrainingPlanAddViewController class]]){
        [titleLabel setText:NSLocalizedString(@"PLAN DE ENTRENAMIENTO", nil)];
        [rightButton1 setHidden:true];
        [rightButton1 setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
        [slideButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    }
    
    if ([viewController isKindOfClass:[SeriesViewController class]]) {
        [titleLabel setText:@"SERIES"];
        rightButton1.hidden = NO;
         [rightButton1 setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    }
    
    if ([viewController isKindOfClass:[MealDetailViewController class]] ||
        [viewController isKindOfClass:[TrainingPlanDetailViewController class]] ||
        [viewController isKindOfClass:[SelectorTableViewController class]] ||
        [viewController isKindOfClass:[WorkoutDetailViewController class]] ||
        [viewController isKindOfClass:[LogDetailViewController class]] ||
        [viewController isKindOfClass:[EvolutionViewController class]] ||
        [viewController isKindOfClass:[SelectorFoodViewController class]] ||
        [viewController isKindOfClass:[NutritionPlanDetailViewController class]] ||
        [viewController isKindOfClass:[DietDetailViewController class]] ||
        [viewController isKindOfClass:[CommunityViewController class]]) {
        
        [containerView setHidden:YES];
    }
    
    if ([viewController isKindOfClass:[CommonViewController class]]) {
        if (((CommonViewController*)viewController).isHideCustomNavigation) {
            containerView.hidden = YES;
        }
    }
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
}

- (void)showSearchBar:(BOOL)isShow {
    titleLabel.hidden = isShow;
    slideButton.hidden = isShow;
    rightButton1.hidden = NO;
    if (isShow) {
        [rightButton1 setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    }
    else {
        [rightButton1 setImage:[UIImage imageNamed:@"icon_search"] forState:UIControlStateNormal];
        [self.view endEditing:YES];
    }
    rightButton2.hidden = isShow;
    searchContainerView.hidden = !isShow;
}

#pragma mark FunctionsTableViewController Delegate MEthods

- (void)presentFunctionViewController:(UIViewController *)viewController{
    NSArray *controllers = [NSArray arrayWithObject:viewController];
    [self setViewControllers:controllers];
}

#pragma mark - UISearchBar Delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.view endEditing:YES];
    
    [self showSearchBar:NO];
    
    if ([_currentViewController isKindOfClass:[ParentViewController class]]) {
        [((ParentViewController*)_currentViewController) didSearchButtonClicked:searchBar];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([_currentViewController isKindOfClass:[ParentViewController class]]) {
        [((ParentViewController*)_currentViewController) didSearchBar:searchBar TextChange:searchText];
    }
}

@end
