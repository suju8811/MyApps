//
//  CommunityViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 27/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CommunityViewController.h"
#import "UIImageView+AFNetworking.h"
#import <MXSegmentedPager/MXSegmentedPager.h>
#import "AppConstants.h"
#import "AppContext.h"
#import "UtilManager.h"

@interface CommunityViewController ()<MXSegmentedPagerDelegate, MXSegmentedPagerDataSource, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) MXSegmentedPager  *segmentedPager;

@property (nonatomic, strong) UIView *profileView;
@property (nonatomic, strong) UIImageView *profileImageView;
@property (nonatomic, strong) UILabel *followersLabel;
@property (nonatomic, strong) UILabel *followersCountLabel;
@property (nonatomic, strong) UILabel *followsLabel;
@property (nonatomic, strong) UILabel *followsCountLabel;
@property (nonatomic, strong) UILabel *nameLabel;


@end

@implementation CommunityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[self view] setBackgroundColor:BLACK_APP_COLOR];
    
    _segmentedPager = [[MXSegmentedPager alloc] init];
    _segmentedPager.delegate    = self;
    _segmentedPager.dataSource  = self;
    [self.view addSubview:self.segmentedPager];
    
    [self configureProfileView];
    
    // Parallax Header
    self.segmentedPager.parallaxHeader.view = _profileView;
    self.segmentedPager.parallaxHeader.mode = MXParallaxHeaderModeFill;
    self.segmentedPager.parallaxHeader.height = 250;
    self.segmentedPager.parallaxHeader.minimumHeight = NAV_HEIGHT + [UtilManager height:40];
    
    // Segmented Control customization
    self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedPager.segmentedControl.backgroundColor = [UIColor whiteColor];
    self.segmentedPager.segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
    self.segmentedPager.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor orangeColor]};
    self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedPager.segmentedControl.selectionIndicatorColor = [UIColor orangeColor];
    
    self.segmentedPager.segmentedControlEdgeInsets = UIEdgeInsetsMake(12, 12, 12, 12);
}

- (void)configureProfileView{
    _profileView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager width:240])];
    
    _profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:20], NAV_HEIGHT + [UtilManager height:20], [UtilManager width:83], [UtilManager width:83])];
    [_profileImageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
    [[_profileImageView layer] setCornerRadius:_profileImageView.frame.size.width/2];
    [[_profileImageView layer] setMasksToBounds:YES];
    
    UIImageView *imageView = _profileImageView;
    
    [_profileImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE,[[[AppContext sharedInstance] currentUser] picture]]]] placeholderImage:[UIImage imageNamed:@"img-placeholder-small"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        [imageView setImage:image];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [[imageView layer] setBorderWidth:1];
        [[imageView layer] setBorderColor:YELLOW_APP_COLOR.CGColor];
        [[imageView layer] setCornerRadius:imageView.frame.size.width/2];
        [[imageView layer] setMasksToBounds:YES];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        NSLog(@"%@",error);
        [imageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
    }];
    
    [_profileImageView setCenter:CGPointMake(self.view.center.x, _profileImageView.center.y)];
    [_profileImageView setUserInteractionEnabled:YES];
    
    [self.view addSubview:_profileImageView];
    
    [_profileView setBackgroundColor:[UIColor clearColor]];
    
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, _profileImageView.frame.origin.y + _profileImageView.frame.size.height, WIDTH, [UtilManager height:30])];
    [_nameLabel setAdjustsFontSizeToFitWidth:FALSE];
    [_nameLabel setTextAlignment:NSTextAlignmentCenter];
    [_nameLabel setText:[[[AppContext sharedInstance] currentUser] name]];
    [_nameLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:20]];
    
    [self.view addSubview:_nameLabel];
    
}

- (void)viewWillLayoutSubviews {
    self.segmentedPager.frame = (CGRect){
        .origin = CGPointZero,
        .size   = self.view.frame.size
    };
    [super viewWillLayoutSubviews];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark <MXSegmentedPagerDelegate>

- (CGFloat)heightForSegmentedControlInSegmentedPager:(MXSegmentedPager *)segmentedPager {
    return 30.f;
}

- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didSelectViewWithTitle:(NSString *)title {
    NSLog(@"%@ page selected.", title);
}

- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didScrollWithParallaxHeader:(MXParallaxHeader *)parallaxHeader {
    NSLog(@"progress %f", parallaxHeader.progress);
}

#pragma mark <MXSegmentedPagerDataSource>

- (NSInteger)numberOfPagesInSegmentedPager:(MXSegmentedPager *)segmentedPager {
    return 4;
}

- (NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager titleForSectionAtIndex:(NSInteger)index {
    return @[@"Table", @"Web", @"Text", @"Custom"][index];
}

- (UIView *)segmentedPager:(MXSegmentedPager *)segmentedPager viewForPageAtIndex:(NSInteger)index {
    return nil;
}


@end
