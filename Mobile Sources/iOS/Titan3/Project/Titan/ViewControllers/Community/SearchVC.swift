/*-------------------------------------
 
 - Photofeed -
 
 created by FV iMAGINATION @2016
 All Rights Reserved
 
 -------------------------------------*/


import UIKit
import Parse
import GoogleMobileAds
import AudioToolbox

class SearchVC: UIViewController,
UISearchBarDelegate,
UITableViewDelegate,
UITableViewDataSource
//GADBannerViewDelegate
{


    /* Views */
    @IBOutlet weak var postsTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //Ad banners properties
    //var adMobBannerView = GADBannerView()
    
    
    
    /* Variables */
    var postsArray = [PFObject]()
    
    
    
    
override func viewDidAppear(_ animated: Bool) {
    UIApplication.shared.applicationIconBadgeNumber = 0
}
    
override func viewDidLoad() {
        super.viewDidLoad()
    
    var image = UIImage(named: "color.png") as! UIImage
    
    self.navigationController?.navigationBar.setBackgroundImage(image , for: .default)
    // Setup views on iPad
    if UIDevice.current.userInterfaceIdiom == .pad {
        postsTableView.frame = CGRect(x: 0, y: 0, width: 460, height: view.frame.size.height - 100)
        postsTableView.center = view.center
    }
    
    
    // Init ad banners
  //  initAdMobBanner()
    
    // Call query for latest posts
    queryPosts("")
    self.title = "Popular"
}

    
    
   
@IBAction func popularButt(_ sender: AnyObject) {
   // self.title = "Popular"
    //queryPosts("")
}

    
// MARK: - SEARCH BUTTON
@IBAction func searchButt(_ sender: AnyObject) {
    showSearchBar()
}

    
    
// MARK: - QUERY POSTS
func queryPosts(_ text:String) {
    postsArray.removeAll()
    showHUD()
    
    let query = PFQuery(className: POSTS_CLASSE_NAME)
    if text != "" {
        let keywords = text.lowercased().components(separatedBy: " ") as [String]
        query.whereKey(POSTS_KEYWORDS, containedIn: keywords)
        query.order(byDescending: "createdAt")
        self.title = "Search"
        
    } else {
        query.limit = 50
        query.order(byDescending: POSTS_LIKES)
    }

    query.whereKey(POSTS_IS_REPORTED, equalTo: false)

    query.findObjectsInBackground { (objects, error)-> Void in
        if error == nil {
            self.postsArray = objects!
            // Reload TableView
            self.postsTableView.reloadData()
            self.hideHUD()
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
            self.hideHUD()
    }}
}
    
    
    
// MARK: - SEARCH ACTIONS
func showSearchBar() {
    UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: {
        self.searchBar.frame.origin.y = 64
    }, completion: { (finished: Bool) in
        self.searchBar.becomeFirstResponder()
    })
}
func hideSearchBar() {
    UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: {
        self.searchBar.frame.origin.y = -60
    }, completion: { (finished: Bool) in
        self.searchBar.resignFirstResponder()
    })
}
    
// MARK: - SEARCH BAR DELEGATES
func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    queryPosts(searchBar.text!)
    hideSearchBar()
}
func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchBar.text = ""
    hideSearchBar()
}
    
    
 
    
    
// MARK: - TABLEVIEW DELEGATES
func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return postsArray.count
}
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
        
    var postsClass = PFObject(className: POSTS_CLASSE_NAME)
    postsClass = postsArray[(indexPath as NSIndexPath).row]
    
    // Get userPointer
    let userPointer = postsClass[POSTS_USER_POINTER] as! PFUser
    userPointer.fetchIfNeededInBackground { (user, error) in
        
        // Get User's data
        cell.fullnameLabel.text = "\(userPointer[USER_FULLNAME]!)"
        
        cell.avatarImage.image = UIImage(named: "logo")
        let avatarImage = userPointer[USER_AVATAR] as? PFFile
        avatarImage?.getDataInBackground { (imageData, error) -> Void in
            if error == nil {
                if let imageData = imageData {
                    cell.avatarImage.image = UIImage(data:imageData)
        }}}
        
        let date = postsClass.createdAt
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM dd yyyy"
        let dateStr = dateFormat.string(from: date!)
        cell.dateLabel.text = dateStr
        
        
        // Get Post's data
        if postsClass[POSTS_CITY] != nil { cell.cityLabel.text = "\(postsClass[POSTS_CITY]!)"
        } else { cell.cityLabel.text = "N/A" }
        cell.postLabel.text = "\(postsClass[POSTS_TEXT]!)"
        if postsClass[POSTS_LIKES] != nil { cell.likesLabel.text = "\(postsClass[POSTS_LIKES]!)"
        } else { cell.likesLabel.text = "0" }
        
        let imageFile = postsClass[POSTS_IMAGE] as? PFFile
        imageFile?.getDataInBackground { (imageData, error) -> Void in
            if error == nil {
                if let imageData = imageData {
                    cell.postImage.image = UIImage(data:imageData)
        }}}
        
        
        // Assign tags to buttons
        cell.showUserOutlet.tag = (indexPath as NSIndexPath).row
        
        
        // Cell layout
        let randomColor = Int(arc4random() % UInt32(postColorsArray.count))
        cell.postView.backgroundColor = postColorsArray[randomColor]
        cell.avatarImage.layer.cornerRadius = cell.avatarImage.bounds.size.width/2
        cell.avatarImage.layer.borderColor = postColorsArray[randomColor].cgColor
        cell.avatarImage.layer.borderWidth = 1
    }
        

return cell
}
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 200
}
    
    
// MARK: -  CELL HAS BEEN TAPPED -> SHOW POST DETAILS
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    var postsClass = PFObject(className: POSTS_CLASSE_NAME)
    postsClass = postsArray[(indexPath as NSIndexPath).row]
    
    let pdVC = storyboard?.instantiateViewController(withIdentifier: "PostDetails") as! PostDetails
    pdVC.postObj = postsClass
    navigationController?.pushViewController(pdVC, animated: true)
}


    
 
// MARK: - USER AVATAR BUTTON
@IBAction func userAvatarButt(_ sender: AnyObject) {
    let butt = sender as! UIButton
    
    var postsClass = PFObject(className: POSTS_CLASSE_NAME)
    postsClass = postsArray[butt.tag] 
    
    let userPointer = postsClass[POSTS_USER_POINTER] as! PFUser
    userPointer.fetchIfNeededInBackground { (user, error) in
        let oupVC = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfile") as! OtherUserProfile
        oupVC.userObj = userPointer
        self.navigationController?.pushViewController(oupVC, animated: true)
    }
}

    
 
    
    
    
    
// MARK: - ADMOB BANNER METHODS
  /*  func initAdMobBanner() {
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: 320, height: 50))
        adMobBannerView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: 320, height: 50)
        adMobBannerView.adUnitID = ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        let request = GADRequest()
        adMobBannerView.load(request)
    }
    
    
    // Hide the banner
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        
        banner.frame = CGRect(x: 0, y: self.view.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
    }
    
    // Show the banner
    func showBanner(_ banner: UIView) {
        var h: CGFloat = 0
        // iPhone X
        if UIScreen.main.bounds.size.height == 812 { h = 84
        } else { h = 48 }
        
        UIView.beginAnimations("showBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2,
                              y: view.frame.size.height - banner.frame.size.height - h,
                              width: banner.frame.size.width, height: banner.frame.size.height);
        UIView.commitAnimations()
        banner.isHidden = false
    }
    */


    
    // AdMob banner available
  /*  func adViewDidReceiveAd(_ view: GADBannerView) {
        print("AdMob loaded!")
        showBanner(adMobBannerView)
    }
    
    // NO AdMob banner available
    func adView(_ view: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("AdMob Can't load ads right now, they'll be available later \n\(error)")
        hideBanner(adMobBannerView)
    }*/
    
    
    

    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
