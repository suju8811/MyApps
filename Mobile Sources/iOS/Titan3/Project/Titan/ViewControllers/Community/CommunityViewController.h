//
//  CommunityViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 27/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXSegmentedPagerController.h"
#import "ParentViewController.h"

@interface CommunityViewController : ParentViewController

@end
