/*-------------------------------------
 
 - Photofeed -
 
 created by FV iMAGINATION @2016
 All Rights Reserved
 
 -------------------------------------*/

import UIKit
import Parse
import GoogleMobileAds
import AudioToolbox


class OtherUserProfile: UIViewController,
UITableViewDelegate,
UITableViewDataSource
//GADBannerViewDelegate
{

    /* Views */
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var aboutMeTxt: UITextView!
    
    @IBOutlet weak var userPostsTableView: UITableView!
    
    @IBOutlet weak var followOutlet: UIButton!
    
    @IBOutlet weak var followersOutlet: UIButton!
    @IBOutlet weak var followingOutlet: UIButton!
    
    //Ad banners properties
    var adMobBannerView = GADBannerView()
    
    
    
    
    /* Variables */
    var userObj = PFUser()
    var postsArray = [PFObject]()
    var followArray = [PFObject]()
    var followersArray = [PFObject]()
    var followingArray = [PFObject]()
    
    
    
    
    
override func viewDidLoad() {
        super.viewDidLoad()
    var image = UIImage(named: "color.png") as! UIImage
    
    self.navigationController?.navigationBar.setBackgroundImage(image , for: .default)
    self.edgesForExtendedLayout = UIRectEdge()
//    self.title = "\(userObj[USER_FULLNAME]!)"
    self.title = userObj.username
    
    // Setup views on iPad
    if UIDevice.current.userInterfaceIdiom == .pad {
        userPostsTableView.frame = CGRect(x: 0, y: 234, width: 460, height: view.frame.size.height-234 - 50)
        userPostsTableView.center.x = view.center.x
    }
    
    // Round views corners
    avatarImage.layer.cornerRadius = avatarImage.bounds.size.width/2
    avatarImage.layer.borderColor = postColorsArray[0].cgColor
    avatarImage.layer.borderWidth = 1
    

    // Initialize a REPORT USER BarButton Item
    let butt = UIButton(type: .custom)
    butt.adjustsImageWhenHighlighted = false
    butt.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
    butt.setBackgroundImage(UIImage(named: "reportButt"), for: .normal)
    butt.addTarget(self, action: #selector(reportButton), for: .touchUpInside)
    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: butt)

    
    
    // Show users details
    fullnameLabel.text = "\(userObj[USER_FULLNAME]!)"
    if userObj[USER_ABOUT_ME] != nil { aboutMeTxt.text = "\(userObj[USER_ABOUT_ME]!)"
    } else { aboutMeTxt.text = "" }
    
    // Get avatar image
    avatarImage.image = UIImage(named: "logo")
    let imageFile = userObj[USER_AVATAR] as? PFFile
    imageFile?.getDataInBackground(block: { (imageData, error) -> Void in
        if error == nil {
            if let imageData = imageData {
                self.avatarImage.image = UIImage(data:imageData)
    } } })
    
    // Get avatar image
    coverImage.backgroundColor = postColorsArray[0]
    let coverFile = userObj[USER_COVER_IMAGE] as? PFFile
    coverFile?.getDataInBackground(block: { (imageData, error) -> Void in
        if error == nil {
            if let imageData = imageData {
                self.coverImage.image = UIImage(data:imageData)
    } } })

    
    // Init ad banners
    //initAdMobBanner()
    
    
    // Call queries
    queryUserPosts()
    queryFollow()
    queryFollowers()
}

  
    
    
// MARK: - SHOW USER POSTS
func queryUserPosts() {
    postsArray.removeAll()
    
    let query = PFQuery(className: POSTS_CLASSE_NAME)
    query.whereKey(POSTS_USER_POINTER, equalTo: userObj)
    
    query.whereKey(POSTS_IS_REPORTED, equalTo: false)
    
    query.findObjectsInBackground { (objects, error)-> Void in
        if error == nil {
            self.postsArray = objects!
            // Reload TableView
            self.userPostsTableView.reloadData()
            
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
    }}
}
    
    
// MARK: - TABLEVIEW DELEGATES
func numberOfSections(in tableView: UITableView) -> Int {
        return 1
}
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return postsArray.count
}
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
    let randomColor = Int(arc4random() % UInt32(postColorsArray.count))

    var postsClass = PFObject(className: POSTS_CLASSE_NAME)
    postsClass = postsArray[(indexPath as NSIndexPath).row]
    
    let date = postsClass.createdAt
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = "MMM dd yyyy"
    let dateStr = dateFormat.string(from: date!)
    cell.dateLabel.text = dateStr
    
    
    if postsClass[POSTS_CITY] != nil { cell.cityLabel.text = "\(postsClass[POSTS_CITY]!)"
    } else { cell.cityLabel.text = "N/A" }
    cell.postLabel.text = "\(postsClass[POSTS_TEXT]!)"
    if postsClass[POSTS_LIKES] != nil { cell.likesLabel.text = "\(postsClass[POSTS_LIKES]!)"
    } else { cell.likesLabel.text = "0" }
    
    let imageFile = postsClass[POSTS_IMAGE] as? PFFile
    imageFile?.getDataInBackground { (imageData, error) -> Void in
        if error == nil {
            if let imageData = imageData {
                cell.postImage.image = UIImage(data:imageData)
    }}}
    
        
    // Cell layout
    cell.postView.backgroundColor = postColorsArray[randomColor]
   
        
return cell
}
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 200
}
    
    
// MARK: -  CELL HAS BEEN TAPPED -> SHOW POST DETAILS
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    var postsClass = PFObject(className: POSTS_CLASSE_NAME)
    postsClass = postsArray[(indexPath as NSIndexPath).row]
    
    let pdVC = storyboard?.instantiateViewController(withIdentifier: "PostDetails") as! PostDetails
    pdVC.postObj = postsClass
    navigationController?.pushViewController(pdVC, animated: true)
}

    
    
    
// MARK: - QUERY FOLLOWERS
func queryFollowers() {
    followersArray.removeAll()
        
    let query = PFQuery(className: FOLLOW_CLASS_NAME)
    query.whereKey(FOLLOW_IS_FOLLOWING, equalTo: userObj)
    query.countObjectsInBackground { (amount, error) -> Void in
        if error == nil {
            self.followersOutlet.setTitle("\(amount)\nfollowers", for: .normal)
            self.queryFollowing()
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
    }}
}
    
// MARK: - QUERY FOLLOWING
func queryFollowing() {
    followingArray.removeAll()
        
    let query = PFQuery(className: FOLLOW_CLASS_NAME)
    query.whereKey(FOLLOW_A_USER, equalTo: userObj)
    query.countObjectsInBackground { (amount, error) -> Void in
        if error == nil {
            self.followingOutlet.setTitle("\(amount)\nfollowing", for: .normal)
                
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
    }}
}

    
    

    
    
// MARK: - CHECK IF YOUR FOLLOWING THIS USER
func queryFollow() {
        followArray.removeAll()
        let query = PFQuery(className: FOLLOW_CLASS_NAME)
        query.whereKey(FOLLOW_A_USER, equalTo: PFUser.current()!)
        query.whereKey(FOLLOW_IS_FOLLOWING, equalTo: userObj)
        query.findObjectsInBackground { (objects, error)-> Void in
            if error == nil {
                self.followArray = objects!
                
                // YOU'RE ALREADY FOLLOWING THIS USER
                if self.followArray.count > 0 {
                    self.followOutlet.setTitle("unfollow", for: .normal)
                    
                    
                // YOU'RE NOT FOLLOWING THIS USER
                } else if self.followArray.count == 0 {
                    self.followOutlet.setTitle("follow", for: .normal)  }
                
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
            }
            
            self.followOutlet.setBackgroundImage(UIImage(named: "\(self.followOutlet.titleLabel!.text!)"), for: .normal)
        }
}
    
    
// MARK: - FOLLOW BUTTON
@IBAction func followButt(_ sender: AnyObject) {
        let butt = sender as! UIButton
        
        var followClass = PFObject(className: FOLLOW_CLASS_NAME)
        let currentUser = PFUser.current()
        
        // UNFOLLOW THIS USER
        if butt.titleLabel!.text == "unfollow" {
            followClass = followArray[0]
            followClass.deleteInBackground {(success, error) -> Void in
                if error == nil {
                    butt.setTitle("follow", for: .normal)
                    butt.setBackgroundImage(UIImage(named: "follow"), for: .normal)
            } }
            
            
        // FOLLOW THIS USER
        } else if butt.titleLabel!.text == "follow" {
            // Save follower and followed
            followClass[FOLLOW_A_USER] = currentUser
            followClass[FOLLOW_IS_FOLLOWING] = userObj
            
            // Saving block
            followClass.saveInBackground(block: { (success, error) -> Void in
                if error == nil {
                    butt.setTitle("unfollow", for: .normal)
                    butt.setBackgroundImage(UIImage(named: "unfollow"), for: .normal)
            
                    
                    // Send Push notification
                    let pushStr = "\(PFUser.current()![USER_FULLNAME]!) started following you"
                    
                    let data = [ "badge" : "Increment",
                                 "alert" : pushStr,
                                 "sound" : "bingbong.aiff"
                    ]
                    let request = [
                                "someKey" : self.userObj.objectId!,
                                "data" : data
                        ] as [String : Any]
                    PFCloud.callFunction(inBackground: "push", withParameters: request as [String : Any], block: { (results, error) in
                        if error == nil {
                            print ("\nPUSH SENT TO: \(self.userObj[USER_USERNAME]!)\nMESSAGE: \(pushStr)\n")
                        } else {
                            print ("\(error!.localizedDescription)")
                    }})
                    

                    
                    
                    // Save Activity
                    let activityClass = PFObject(className: ACTIVITY_CLASS_NAME)
                    activityClass[ACTIVITY_CURRENT_USER] = self.userObj
                    activityClass[ACTIVITY_OTHER_USER] = PFUser.current()!
                    activityClass[ACTIVITY_TEXT] = "\(PFUser.current()![USER_FULLNAME]!) started following you"
                    activityClass.saveInBackground()
            }})
        }
}
    


    
// MARK: - SHOW FOLLOWERS BUTTON
@IBAction func showFollowersButt(_ sender: AnyObject) {
    let butt = sender as! UIButton
            
    let fVC = storyboard?.instantiateViewController(withIdentifier: "Follow") as! Follow
    if butt.tag == 0 { fVC.checkFollowers = true
    } else { fVC.checkFollowers = false }
    fVC.fUser = userObj
    
    navigationController?.pushViewController(fVC, animated: true)
}
    

    
    
    
    
// MARK: - REPORT ABUSIVE USER
@objc func reportButton() {
    
    let alert = UIAlertController(title: "Reporting a User",
        message: "Tell us a bit about the reason you're reporting this User:",
        preferredStyle: .alert)
    
    // REPORT ACTION
    let ok = UIAlertAction(title: "Report", style: .default, handler: { (action) -> Void in
        let textField = alert.textFields!.first
        self.showHUD()
        
        
        let request = [
            "userId" : self.userObj.objectId!,
            "reportMessage" : textField!.text!
        ] as [String : Any]
        
        PFCloud.callFunction(inBackground: "reportUser", withParameters: request as [String : Any], block: { (results, error) in
            if error == nil {
                print ("\(self.userObj[USER_FULLNAME]!) has been reported!")
                
                self.simpleAlert("Thanks for reporting this User, we'll check it out withint 24 hours!")
                self.hideHUD()
                
                // Automatically Report all posts of this User
                var postsArr = [PFObject]()
                let query = PFQuery(className: POSTS_CLASSE_NAME)
                query.whereKey(POSTS_USER_POINTER, equalTo: self.userObj)
                query.findObjectsInBackground { (objects, error)-> Void in
                    if error == nil {
                        postsArr = objects!
                        
                        for i in 0..<postsArr.count {
                            var pObj = PFObject(className: POSTS_CLASSE_NAME)
                            pObj = postsArr[i]
                            
                            pObj[POSTS_IS_REPORTED] = true
                            pObj[POSTS_REPORT_MESSAGE] = "*Reported automatically after User reporting"
                            pObj.saveInBackground()
                        }
                }}

                
            // error in Cloud Code
            } else {
                print ("\(error!.localizedDescription)")
                self.hideHUD()
        }})
        
        
    })// end REPORT ACTION
    
    
    // Cancel action
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in })
    
    // Add textField
    alert.addTextField { (textField: UITextField) -> Void in }
    
    alert.addAction(ok)
    alert.addAction(cancel)
    present(alert, animated: true, completion: nil)
    
}
    
    
    
    
    
    
    
    
    
    
    
// MARK: - ADMOB BANNER METHODS
  /*  func initAdMobBanner() {
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: 320, height: 50))
        adMobBannerView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: 320, height: 50)
        adMobBannerView.adUnitID = ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        let request = GADRequest()
        adMobBannerView.load(request)
    }
    
    
    // Hide the banner
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        
        banner.frame = CGRect(x: 0, y: self.view.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
        
    }
    
    // Show the banner
    func showBanner(_ banner: UIView) {
        var h: CGFloat = 0
        // iPhone X
        if UIScreen.main.bounds.size.height == 812 { h = 20
        } else { h = 0 }
        
        UIView.beginAnimations("showBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2,
                              y: view.frame.size.height - banner.frame.size.height - h,
                              width: banner.frame.size.width, height: banner.frame.size.height);
        UIView.commitAnimations()
        banner.isHidden = false
    }
    

    
    // AdMob banner available
    func adViewDidReceiveAd(_ view: GADBannerView) {
        print("AdMob loaded!")
        showBanner(adMobBannerView)
    }
    
    // NO AdMob banner available
    func adView(_ view: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("AdMob Can't load ads right now, they'll be available later \n\(error)")
        hideBanner(adMobBannerView)
    }
    
    */
    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
