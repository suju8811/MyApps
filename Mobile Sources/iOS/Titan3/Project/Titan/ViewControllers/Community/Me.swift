/*-------------------------------------
 
 - Photofeed -
 
 created by FV iMAGINATION @2016
 All Rights Reserved
 
 -------------------------------------*/


import UIKit
import Parse


class Me: UIViewController,
UITableViewDataSource,
UITableViewDelegate
{

    /* Views */
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var aboutMeTxt: UITextView!
    
    @IBOutlet weak var editProfileOutlet: UIButton!
    @IBOutlet weak var segControl: UISegmentedControl!
    
    @IBOutlet weak var postsTableView: UITableView!
    
    @IBOutlet weak var followersOutlet: UIButton!
    @IBOutlet weak var followingOutlet: UIButton!
    
    @IBOutlet weak var activityOutlet: UIButton!
    
    
    
    /* Variables */
    var postsArray = [PFObject]()
    var likesArray = [PFObject]()
    var followersArray = [PFObject]()
    var followingArray = [PFObject]()
    
    
    
   
// MARK: - SHOW USER DETAILS & QUERY MY POSTS
override func viewDidAppear(_ animated: Bool) {
    
    UIApplication.shared.applicationIconBadgeNumber = 0
    
    var image = UIImage(named: "color.png") as! UIImage
    
    self.navigationController?.navigationBar.setBackgroundImage(image , for: .default)
    let currentUser = PFUser.current()!
    if let userFullName = currentUser[USER_FULLNAME] {
        fullNameLabel.text = "\(userFullName)"
    }
    else {
        fullNameLabel.text = currentUser.username
    }
        
    avatarImage.image = UIImage(named: "logo")
    let imageFile = currentUser[USER_AVATAR] as? PFFile
    imageFile?.getDataInBackground(block: { (imageData, error) -> Void in
            if error == nil {
                if let imageData = imageData {
                    self.avatarImage.image = UIImage(data:imageData)
                } } })
    avatarImage.layer.cornerRadius = avatarImage.bounds.size.width/2
    avatarImage.layer.borderColor = postColorsArray[0].cgColor
    avatarImage.layer.borderWidth = 1
    
    let coverFile = currentUser[USER_COVER_IMAGE] as? PFFile
    coverFile?.getDataInBackground(block: { (imageData, error) -> Void in
        if error == nil {
            if let imageData = imageData {
                self.coverImage.image = UIImage(data:imageData)
    } } })
    
    if currentUser[USER_ABOUT_ME] != nil { aboutMeTxt.text = "\(currentUser[USER_ABOUT_ME]!)"
    } else { aboutMeTxt.text = "" }
    
    
    // Round views corners
    editProfileOutlet.layer.cornerRadius = 5
    activityOutlet.layer.cornerRadius = 5
    
    
    // Call queries
    segControl.selectedSegmentIndex = 0
    queryMyPosts()
    queryFollowers()
    queryFollowing()
}
    
override func viewDidLoad() {
        super.viewDidLoad()

    // Setup views on iPad
    if UIDevice.current.userInterfaceIdiom == .pad {
        postsTableView.frame = CGRect(x: 0, y: 314, width: 460, height: view.frame.size.height-314 - 50)
        postsTableView.center.x = view.center.x
    }
}
  

    
// MARK: - CALL QUERIES BY SEGMENTED CONTROL CHANGE
@IBAction func segmentedChanged(_ sender: UISegmentedControl) {
    if sender.selectedSegmentIndex == 0 {
        queryMyPosts()
    } else {
        queryMyLikes()
    }
}
    

    
    
// MARK: - QUERY MY POSTS
func queryMyPosts() {
    postsArray.removeAll()
    likesArray.removeAll()
    postsTableView.reloadData()
    segControl.selectedSegmentIndex = 0
    showHUD()
    
    let query = PFQuery(className: POSTS_CLASSE_NAME)
    query.whereKey(POSTS_USER_POINTER, equalTo: PFUser.current()!)
    query.order(byDescending: "createdAt")
    query.findObjectsInBackground { (objects, error)-> Void in
            if error == nil {
                self.postsArray = objects!
                // Reload TableView
                self.postsTableView.reloadData()
                self.hideHUD()
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
                self.hideHUD()
            } }
}
    
    
 
// MARK: - QUERY MY LIKES
func queryMyLikes() {
    postsArray.removeAll()
    likesArray.removeAll()
    postsTableView.reloadData()
    showHUD()
    
    let query = PFQuery(className: LIKES_CLASS_NAME)
    query.whereKey(LIKES_LIKED_BY, equalTo: PFUser.current()!)
    query.order(byDescending: "createdAt")
    query.findObjectsInBackground { (objects, error)-> Void in
        if error == nil {
            self.likesArray = objects!
            self.postsTableView.reloadData()
            self.hideHUD()
            
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
            self.hideHUD()
    }}
}
    
 
    
    
// MARK: - QUERY FOLLOWERS
func queryFollowers() {
    followersArray.removeAll()
        
    let query = PFQuery(className: FOLLOW_CLASS_NAME)
    query.whereKey(FOLLOW_IS_FOLLOWING, equalTo: PFUser.current()!)
    query.countObjectsInBackground { (amount, error) -> Void in
        if error == nil {
            self.followersOutlet.setTitle("\(amount)\nfollowers", for: .normal)
            self.queryFollowing()
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
    }}
}
    
// MARK: - QUERY FOLLOWING
func queryFollowing() {
    followingArray.removeAll()
        
    let query = PFQuery(className: FOLLOW_CLASS_NAME)
    query.whereKey(FOLLOW_A_USER, equalTo: PFUser.current()!)
    query.countObjectsInBackground { (amount, error) -> Void in
        if error == nil {
            self.followingOutlet.setTitle("\(amount)\nfollowing", for: .normal)
            
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
    }}
}


    
    
// MARK: - TABLEVIEW DELEGATES
func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    var rows = 0
    if segControl.selectedSegmentIndex == 0 { rows = postsArray.count
    } else { rows = likesArray.count }
    
return rows
}
    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
    let randomColor = Int(arc4random() % UInt32(postColorsArray.count))

    
    // SHOW MY POSTS
    if segControl.selectedSegmentIndex == 0 {
        var postsClass = PFObject(className: POSTS_CLASSE_NAME)
        postsClass = postsArray[(indexPath as NSIndexPath).row]
        
        // Get userPointer
        let userPointer = postsClass[POSTS_USER_POINTER] as! PFUser
        userPointer.fetchIfNeededInBackground(block: { (user, error) in
            
            // Gest post data
            let date = postsClass.createdAt
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "MMM dd yyyy"
            let dateStr = dateFormat.string(from: date!)
            cell.dateLabel.text = dateStr
            
            if postsClass[POSTS_CITY] != nil { cell.cityLabel.text = "\(postsClass[POSTS_CITY]!)"
            } else { cell.cityLabel.text = "N/A" }
            
            cell.postLabel.text = "\(postsClass[POSTS_TEXT]!)"
            
            if postsClass[POSTS_LIKES] != nil { cell.likesLabel.text = "\(postsClass[POSTS_LIKES]!)"
            } else { cell.likesLabel.text = "0" }
            
            let imageFile = postsClass[POSTS_IMAGE] as? PFFile
            imageFile?.getDataInBackground { (imageData, error) -> Void in
                if error == nil {
                    if let imageData = imageData {
                        cell.postImage.image = UIImage(data:imageData)
            }}}
            
            cell.fullnameLabel.text = ""
            cell.showUserOutlet.isEnabled = false
            cell.avatarImage.image = nil
            cell.avatarImage.layer.borderWidth = 0
        })
        
        
        
    // SHOW MY LIKES
    } else {
        var likesClass = PFObject(className: LIKES_CLASS_NAME)
        likesClass = likesArray[(indexPath as NSIndexPath).row]
        
        // Get postPointer
        let postPointer = likesClass[LIKES_POST_LIKED] as! PFObject
        postPointer.fetchIfNeededInBackground(block: { (post, error) in
            
        // Get userPointer
        let userPointer = postPointer[POSTS_USER_POINTER] as! PFUser
        userPointer.fetchIfNeededInBackground(block: { (user, error) in
            
            if postPointer[POSTS_IS_REPORTED] as! Bool == false {
                
                // Get User's data
                cell.fullnameLabel.text = "\(userPointer[USER_FULLNAME]!)"
                
                cell.avatarImage.image = UIImage(named: "logo")
                let avatarImage = userPointer[USER_AVATAR] as? PFFile
                avatarImage?.getDataInBackground { (imageData, error) -> Void in
                    if error == nil {
                        if let imageData = imageData {
                            cell.avatarImage.image = UIImage(data:imageData)
                }}}
                cell.avatarImage.layer.cornerRadius = cell.avatarImage.bounds.size.width/2
                cell.avatarImage.layer.borderColor = postColorsArray[randomColor].cgColor
                cell.avatarImage.layer.borderWidth = 1
                
                // Gest post data
                let date = postPointer.createdAt
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = "MMM dd yyyy"
                let dateStr = dateFormat.string(from: date!)
                cell.dateLabel.text = dateStr
                
                if postPointer[POSTS_CITY] != nil { cell.cityLabel.text = "\(postPointer[POSTS_CITY]!)"
                } else { cell.cityLabel.text = "N/A" }
                
                cell.postLabel.text = "\(postPointer[POSTS_TEXT]!)"
                
                if postPointer[POSTS_LIKES] != nil { cell.likesLabel.text = "\(postPointer[POSTS_LIKES]!)"
                } else { cell.likesLabel.text = "0" }
                
                let imageFile = postPointer[POSTS_IMAGE] as? PFFile
                imageFile?.getDataInBackground { (imageData, error) -> Void in
                    if error == nil {
                        if let imageData = imageData {
                            cell.postImage.image = UIImage(data:imageData)
                }}}
                
                
                // Assign tags to buttons
                cell.showUserOutlet.tag = (indexPath as NSIndexPath).row
                cell.showUserOutlet.isEnabled = true

                
                // Cell layout
                cell.postView.backgroundColor = postColorsArray[randomColor]
        
                
                
                
            // THIS POST IS REPORTED
            } else {
                cell.fullnameLabel.text = "THIS POST IS REPORTED"
                cell.avatarImage.image = UIImage(named: "logo")
                cell.dateLabel.text = ""
                cell.cityLabel.text = ""
                cell.postLabel.text = ""
                cell.likesLabel.text = ""
                cell.postImage.image = UIImage(named: "logo")
            }
            
        
        })
    
    })
    
        
    }
    
    
return cell
}
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 200
}
    

// MARK: -  CELL HAS BEEN TAPPED -> SHOW POST DETAILS
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    var postsClass = PFObject(className: POSTS_CLASSE_NAME)

    // MY POSTS
    if segControl.selectedSegmentIndex == 0 {
        postsClass = PFObject(className: POSTS_CLASSE_NAME)
        postsClass = postsArray[(indexPath as NSIndexPath).row]
        
        let pdVC = self.storyboard?.instantiateViewController(withIdentifier: "PostDetails") as! PostDetails
        pdVC.postObj = postsClass
        self.navigationController?.pushViewController(pdVC, animated: true)

       
    // LIKED POSTS
    } else {
        var likesClass = PFObject(className: LIKES_CLASS_NAME)
        likesClass = likesArray[(indexPath as NSIndexPath).row]
        postsClass = likesClass[LIKES_POST_LIKED] as! PFObject
        postsClass.fetchInBackground(block: { (object, error) in
            if error == nil {
                if postsClass[POSTS_IS_REPORTED] as! Bool == false {
                    let pdVC = self.storyboard?.instantiateViewController(withIdentifier: "PostDetails") as! PostDetails
                    pdVC.postObj = postsClass
                    self.navigationController?.pushViewController(pdVC, animated: true)
                
                // POST HAS BEEN REPORTED, NO ACCESS!
                } else { self.simpleAlert("This Post has been reported, you can't access its details.") }
        }})
    }
    

}


// MARK: - DELETE POST BY SWIPING THE CELL LEFT
func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    var canEdit = Bool()
    if segControl.selectedSegmentIndex == 0 { canEdit = true
    } else { canEdit = false }
    
return canEdit
}
func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == UITableViewCellEditingStyle.delete {
            
        var postsClass = PFObject(className: POSTS_CLASSE_NAME)
        postsClass = postsArray[(indexPath as NSIndexPath).row]
            
        // DELETE ALL LIKES OF THIS RECIPE (if any)
        likesArray.removeAll()
        let query = PFQuery(className: LIKES_CLASS_NAME)
        query.whereKey(LIKES_POST_LIKED, equalTo: postsClass)
        query.findObjectsInBackground { (objects, error)-> Void in
            if error == nil {
                self.likesArray = objects!
                    
                DispatchQueue.main.async(execute: {
                    if self.likesArray.count > 0 {
                        for i in 0..<self.likesArray.count {
                            var likesClass = PFObject(className: LIKES_CLASS_NAME)
                            likesClass = self.likesArray[i]
                            likesClass.deleteInBackground()
                        }
                    }
                })
                    
            // THEN DELETE THE POST
            postsClass.deleteInBackground {(success, error) -> Void in
                if error == nil {
                    self.postsArray.remove(at: (indexPath as NSIndexPath).row)
                    tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                            
                } else {
                    self.simpleAlert("\(error!.localizedDescription)")
            }}
        }}}
}

    
    
    
    
// MARK: - USER AVATAR BUTTON
@IBAction func userAvatarButt(_ sender: AnyObject) {
    let butt = sender as! UIButton
        
    var likesClass = PFObject(className: LIKES_CLASS_NAME)
    likesClass = likesArray[butt.tag]
    
    // Get postPointer
    let postsPointer = likesClass[LIKES_POST_LIKED] as! PFObject
    postsPointer.fetchIfNeededInBackground { (post, error) in
        
        // Get userPointer
        let userPointer = postsPointer[POSTS_USER_POINTER] as! PFUser
        userPointer.fetchIfNeededInBackground { (user, error) in
            let oupVC = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfile") as! OtherUserProfile
            oupVC.userObj = userPointer
            self.navigationController?.pushViewController(oupVC, animated: true)
        }
    }
    
    
}
    
    
    
    
// MARK: - SHOW ACTIVITIES BUTTON
@IBAction func activityButt(_ sender: AnyObject) {
    let aVC = storyboard?.instantiateViewController(withIdentifier: "ActivityVC") as! ActivityVC
    navigationController?.pushViewController(aVC, animated: true)
}
    
    
    
    
// MARK: - EDIT PROFILE BUTTON
@IBAction func editProfileButt(_ sender: AnyObject) {
    let epVC = storyboard?.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
    navigationController?.pushViewController(epVC, animated: true)
}

    
    
// MARK: - SHOW FOLLOWERS BUTTON
@IBAction func showFollowersButt(_ sender: AnyObject) {
    let butt = sender as! UIButton
    
    let fVC = storyboard?.instantiateViewController(withIdentifier: "Follow") as! Follow
    if butt.tag == 0 { fVC.checkFollowers = true
    } else { fVC.checkFollowers = false }
    fVC.fUser = PFUser.current()!
    
    navigationController?.pushViewController(fVC, animated: true)
}
    
    
    
    
// MARK: - LOGOUT BUTTON
@IBAction func logoutButt(_ sender: AnyObject) {
    let alert = UIAlertController(title: APP_NAME,
        message: "Are you sure you want to logout?",
        preferredStyle: UIAlertControllerStyle.alert)
    
    let ok = UIAlertAction(title: "Logout", style: UIAlertActionStyle.default, handler: { (action) -> Void in
        self.showHUD()
        
        PFUser.logOutInBackground { (error) -> Void in
            if error == nil {
                // Show the Login screen
                let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
                self.present(loginVC, animated: true, completion: nil)
            }
            self.hideHUD()
        }
    })
    
    let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) -> Void in })
    alert.addAction(ok); alert.addAction(cancel)
    present(alert, animated: true, completion: nil)
}
    
    
    
    
    
    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
