/*-------------------------------------
 
 - Photofeed -
 
 created by FV iMAGINATION @2016
 All Rights Reserved
 
 -------------------------------------*/


import UIKit
import Parse
//import GoogleMobileAds
import AudioToolbox


// MARK: - POST CELL
class PostCell: UITableViewCell {
    /* Views */
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var postView: UIView!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    
    @IBOutlet weak var showUserOutlet: UIButton!
    
}




// MARK: - HOME CONTROLLER
class Home: UIViewController,
UITableViewDelegate,
UITableViewDataSource,
UISearchBarDelegate
//GADBannerViewDelegate
{

    /* Views */
    @IBOutlet weak var postsTableView: UITableView!
    @IBOutlet weak var noPostsLabel: UILabel!
    
    //Ad banners properties
   // var adMobBannerView = GADBannerView()
    
    
    
    /* Variables */
    var postsArray = [PFObject]()
    var followArray = [PFObject]()
    
    
    
    
override func viewDidAppear(_ animated: Bool) {
    
    UIApplication.shared.applicationIconBadgeNumber = 0
    
    
    if PFUser.current() == nil {
        let loginVC = storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
        present(loginVC, animated: true, completion: nil)
    
    } else {
    
        // Associate the device with a user for Push Notifications
        let installation = PFInstallation.current()
        installation?["username"] = PFUser.current()!.username
        installation?["userID"] = PFUser.current()!.objectId!
        installation?.saveInBackground(block: { (succ, error) in
            if error == nil {
                print("PUSH REGISTERED FOR: \(PFUser.current()!.username!)")
        }})
    }
}
    
override func viewDidLoad() {
        super.viewDidLoad()
 
   // Set logo on NavigationBar
//    navigationItem.titleView = UIImageView(image: UIImage(named: "logoNavBar"))
    var image = UIImage(named: "color.png") as! UIImage
    
    self.navigationController?.navigationBar.setBackgroundImage(image , for: .default)
    noPostsLabel.center = view.center
    
    // Setup views on iPad
    if UIDevice.current.userInterfaceIdiom == .pad {
        postsTableView.frame = CGRect(x: 0, y: 0, width: 460, height: view.frame.size.height - 100)
        postsTableView.center = view.center
    }
    
    
    // Call query
    if PFUser.current() != nil { queryPostsOfFollowing() }
    
    
    // Init ad banners
   // initAdMobBanner()
}

    
// MARK: - REFRESH BUTTON
@IBAction func refreshButt(_ sender: AnyObject) {
     queryPostsOfFollowing()
}

    @IBAction func didPressBackButton(_ sender: Any) {
        _ = tabBarController?.navigationController?.popViewController(animated: true)
    }
    
// QUERY POSTS FROM USERS YOU'RE FOLLOWING
func queryPostsOfFollowing() {
    postsArray.removeAll()
    followArray.removeAll()
    noPostsLabel.isHidden = true
    showHUD()
    
    
    let query = PFQuery(className: FOLLOW_CLASS_NAME)
    query.whereKey(FOLLOW_A_USER, equalTo: PFUser.current()!)
    query.findObjectsInBackground { (objects, error)-> Void in
        if error == nil {
            self.followArray = objects!
            
            // You're following someone:
            if self.followArray.count > 0 {
                
                for i in 0..<self.followArray.count {
                    
                    DispatchQueue.main.async(execute: {
                        
                        var followClass = PFObject(className: FOLLOW_CLASS_NAME)
                        followClass = self.followArray[i]
                        
                        // Get userPointer
                        let userPointer = followClass[FOLLOW_IS_FOLLOWING] as! PFUser
                        userPointer.fetchIfNeededInBackground(block: { (user, error) in
                            
                            if userPointer[USER_IS_REPORTED] as! Bool == false {
                                let query = PFQuery(className: POSTS_CLASSE_NAME)
                                query.whereKey(POSTS_USER_POINTER, equalTo: userPointer)
                                query.whereKey(POSTS_IS_REPORTED, equalTo: false)
                                query.order(byDescending: "createdAt")
                                query.findObjectsInBackground { (objects, error)-> Void in
                                    if error == nil {
                                        if let objects = objects  {
                                            for post in objects {
                                                self.postsArray.append(post)
                                        }}
                                    
                                    // Reload TableView (if there are some posts)
                                    self.postsTableView.reloadData()
                                    self.hideHUD()
                                }}
                            }
                        })
                        
                        
                    })// end DISPATCH_ASYNC
                    
                    
                }// end FOR LOOP
                
                
            // No following: Show noPostsLabel
            } else if self.followArray.count == 0 {
                self.noPostsLabel.isHidden = false
                self.hideHUD()
            }
            
            
        // Error
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
            self.hideHUD()
    }}
}

    
    
// MARK: - TABLEVIEW DELEGATES
func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return postsArray.count
}
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
    
    var postsClass = PFObject(className: POSTS_CLASSE_NAME)
    postsClass = postsArray[(indexPath as NSIndexPath).row] 
    
    // Get userPointer
    let userPointer = postsClass[POSTS_USER_POINTER] as! PFUser
    userPointer.fetchIfNeededInBackground { (user, error) in
        
        // Get User's data
        cell.fullnameLabel.text = "\(userPointer[USER_FULLNAME]!)"
        
        cell.avatarImage.image = UIImage(named: "logo")
        let avatarFile = userPointer[USER_AVATAR] as? PFFile
        avatarFile?.getDataInBackground { (imageData, error) -> Void in
            if error == nil {
                if let imageData = imageData {
                    cell.avatarImage.image = UIImage(data:imageData)
        }}}
        
        cell.postLabel.text = "\(postsClass[POSTS_TEXT]!)"
        
        
        // Get Post's data
        let postDate = postsClass.createdAt
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM dd yyyy"
        cell.dateLabel.text = dateFormat.string(from: postDate!)
        
        if postsClass[POSTS_CITY] != nil { cell.cityLabel.text = "\(postsClass[POSTS_CITY]!)"
        } else { cell.cityLabel.text = "N/A" }
        
        if postsClass[POSTS_LIKES] != nil { cell.likesLabel.text = "\(postsClass[POSTS_LIKES]!)"
        } else { cell.likesLabel.text = "0" }
        let imageFile = postsClass[POSTS_IMAGE] as? PFFile
        imageFile?.getDataInBackground { (imageData, error) -> Void in
            if error == nil {
                if let imageData = imageData {
                    cell.postImage.image = UIImage(data:imageData)
        }}}
        
        
        // Assign tags to buttons
        cell.showUserOutlet.tag = (indexPath as NSIndexPath).row
        
        
        // Cell layout
        let randomColor = Int(arc4random() % UInt32(postColorsArray.count))
        cell.postView.backgroundColor = postColorsArray[randomColor]
        cell.avatarImage.layer.cornerRadius = cell.avatarImage.bounds.size.width/2
        cell.avatarImage.layer.borderColor = postColorsArray[randomColor].cgColor
        cell.avatarImage.layer.borderWidth = 1
    }
    
    
    
return cell
}
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 200
}
    
    
// MARK: -  CELL HAS BEEN TAPPED -> SHOW POSTS DETAILS
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    var postsClass = PFObject(className: POSTS_CLASSE_NAME)
    postsClass = postsArray[(indexPath as NSIndexPath).row]

    let pdVC = storyboard?.instantiateViewController(withIdentifier: "PostDetails") as! PostDetails
    pdVC.postObj = postsClass
    navigationController?.pushViewController(pdVC, animated: true)
}

    
    
    

    
// MARK: - USER AVATAR BUTTON
@IBAction func userAvatarButt(_ sender: AnyObject) {
    let butt = sender as! UIButton
    
    var postsClass = PFObject(className: POSTS_CLASSE_NAME)
    postsClass = postsArray[butt.tag]
    
    let userPointer = postsClass[POSTS_USER_POINTER] as! PFUser
    userPointer.fetchIfNeededInBackground { (user, error) in
        let oupVC = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfile") as! OtherUserProfile
        oupVC.userObj = userPointer
        self.navigationController?.pushViewController(oupVC, animated: true)
    }
}

    
    
    
    
    
 
// MARK: - ADMOB BANNER METHODS
/*func initAdMobBanner() {
       adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: 320, height: 50))
        adMobBannerView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: 320, height: 50)
        adMobBannerView.adUnitID = ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        let request = GADRequest()
        adMobBannerView.load(request)
    }*/
    
    
    // Hide the banner
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        
        banner.frame = CGRect(x: 0, y: self.view.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
        
    }
    
    // Show the banner
    func showBanner(_ banner: UIView) {
        var h: CGFloat = 0
        // iPhone X
        if UIScreen.main.bounds.size.height == 812 { h = 84
        } else { h = 48 }
        
        UIView.beginAnimations("showBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2,
                              y: view.frame.size.height - banner.frame.size.height - h,
                              width: banner.frame.size.width, height: banner.frame.size.height);
        UIView.commitAnimations()
        banner.isHidden = false
    }
    

    
    // AdMob banner available
   /* func adViewDidReceiveAd(_ view: GADBannerView) {
        print("AdMob loaded!")
        showBanner(adMobBannerView)
    }
    
    // NO AdMob banner available
    func adView(_ view: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("AdMob Can't load ads right now, they'll be available later \n\(error)")
        hideBanner(adMobBannerView)
    }
    */

    
    


override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

