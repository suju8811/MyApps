//
//  CommunityTabBar.swift
//  Titan
//
//  Created by Marcus Lee on 13/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

import UIKit

class CommunityTabBar: UITabBar {

    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = 60
        tintColor = UIColor.yellow
        return sizeThatFits
    }

}
