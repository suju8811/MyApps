//
//  SeriesViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 14/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SeriesViewController.h"
#import "SeriesCell.h"
#import "Serie.h"
#import <Realm/Realm.h>
#import "SeriesInputView.h"
#import "RNBlurModalView.h"
#import "TagView.h"
#import "Tag.h"
#import "Muscle.h"
#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"
#import "UIImageView+AFNetworking.h"
#import "Exercise.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import "SeriesExecutive.h"

@interface SeriesViewController ()<UITableViewDelegate, UITableViewDataSource, SeriesInputViewDelegate, SeriesCellDelegate, AVPlayerViewControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIScrollViewDelegate, SeriesDisplay>

@property (nonatomic, strong) UITableView *serieTableView;
@property (nonatomic, strong) NSMutableArray<Serie*> *series;
@property (nonatomic, strong) NSMutableArray *secondExercisesSeries;
@property (nonatomic, strong) RNBlurModalView *modalView;
@property (nonatomic, strong) UIImageView *exerciseImageView;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *kCalLabel;
@property (nonatomic, assign) BOOL isEditing;
@property (nonatomic, strong) UITextView *contentLabel;

@property (nonatomic, strong) UIView *containerPickerView;
@property (nonatomic, strong) UIPickerView *timePickerView;
@property (nonatomic, strong) Exercise *mainExercise;
@property (nonatomic, strong) Exercise *secondExercise;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, assign) NSInteger serieIndex;
@property (nonatomic, strong) UIPageControl *pageControl;

@property (nonatomic, assign) BOOL isEmpty;
@property (nonatomic, assign) NSInteger timeColumn;

@property (nonatomic, strong) SeriesExecutive * executive;

@end

@implementation SeriesViewController

static inline UIViewAnimationOptions animationOptionsWithCurve(UIViewAnimationCurve curve) {
    return (UIViewAnimationOptions)curve << 16;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _series = [[NSMutableArray alloc] initWithCapacity:_currentExercise.series.count];
    
    if(_currentExercise.isSuperSerie.boolValue) {
        _secondExercisesSeries = [[NSMutableArray alloc] initWithCapacity:_currentExercise.secondExerciseSeries.count];
        for(Serie *serie in _currentExercise.secondExerciseSeries)
            [_secondExercisesSeries addObject:serie];
        _secondExercise = [_currentExercise.exercises objectAtIndex:1];
    }
    
    _isEditing = NO;
    
    for(Serie *serie in _currentExercise.series) {
        [_series addObject:serie];
    }
    
    _mainExercise = [_currentExercise.exercises objectAtIndex:0];
    
    [self configureView];

    _executive = [[SeriesExecutive alloc] initWithDisplay:self
                                                   series:_series
                                                    order:_index];
    
    if (_performTraining) {
        [_executive performSelector:@selector(startExecuting) withObject:nil afterDelay:1.f];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWillShow:(NSNotification*)notification {
    [self moveControls:notification up:YES];
}

- (void)keyboardWillBeHidden:(NSNotification*)notification {
    [self moveControls:notification up:NO];
}

- (void)moveControls:(NSNotification*)notification up:(BOOL)up {
    if((self.view.frame.origin.y == 0 && up) || !up){
        NSDictionary* userInfo = [notification userInfo];
        CGRect newFrame = [self getNewControlsFrame:userInfo up:up];
    
        [self animateControls:userInfo withFrame:newFrame];
    }
}

- (CGRect)getNewControlsFrame:(NSDictionary*)userInfo up:(BOOL)up
{
    CGRect kbFrame = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbFrame = [self.view convertRect:kbFrame fromView:nil];
    
    CGRect newFrame = self.view.frame;
    
    if(_currentExercise.isSuperSerie.boolValue){
        if(_serieIndex == 0 || !up)
            newFrame.origin.y = 20;
        else if (_serieIndex < 3 && up)
            newFrame.origin.y = _serieIndex * -[UtilManager height:130];
        else
            newFrame.origin.y += kbFrame.size.height * (up ? -1 : 1);
    }else{
        if(_serieIndex == 0 || !up)
            newFrame.origin.y = 0;
        else if (_serieIndex < 4 && up)
            newFrame.origin.y = _serieIndex * -[UtilManager height:65];
        else
            newFrame.origin.y += kbFrame.size.height * (up ? -1 : 1);
    }
    
    
    
    return newFrame;
}

- (void)animateControls:(NSDictionary*)userInfo withFrame:(CGRect)newFrame {
    NSTimeInterval duration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve animationCurve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:animationOptionsWithCurve(animationCurve)
                     animations:^{
                         self.view.frame = newFrame;
                     }
                     completion:^(BOOL finished){}];
}

- (void)configureView {
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    UIView *firstSeparatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:30], WIDTH - [UtilManager width:20], 1)];
    [firstSeparatorView setBackgroundColor:GRAY_REGISTER_FONT];
    //[self.view addSubview:firstSeparatorView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(firstSeparatorView.frame.origin.x, firstSeparatorView.frame.origin.y, firstSeparatorView.frame.size.width, [UtilManager height:50])];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setText:_currentExercise.title];
    [titleLabel setFont:[UIFont fontWithName:BOLD_FONT size:21]];
    
    //[self.view addSubview:titleLabel];
    
    UIView *lastSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(firstSeparatorView.frame.origin.x, titleLabel.frame.origin.y + titleLabel.frame.size.height, titleLabel.frame.size.width, 1)];
    [lastSeparatorView setBackgroundColor:GRAY_REGISTER_FONT];
    //[self.view addSubview:lastSeparatorView];
    
    if(_currentExercise){
    
        _serieTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., [UtilManager height:0], WIDTH, HEIGHT) style:UITableViewStylePlain];
        [_serieTableView setBackgroundColor:[UIColor clearColor]];
        [_serieTableView setDelegate:self];
        [_serieTableView setDataSource:self];
        [_serieTableView registerClass:[SeriesCell class] forCellReuseIdentifier:@"seriesCell"];
        [_serieTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_serieTableView setBounces:FALSE];
        [_serieTableView setTableHeaderView:[self headerView]];
        
        [self.view addSubview:_serieTableView];
        [self.view addSubview:[self footerView]];
    } else {
        
        UIView *headerView = [self headerView];
        [self.view addSubview:headerView];
        
        CGFloat contentHeight = [UtilManager heightForText:_exercise.content havingWidth:WIDTH - [UtilManager width:40] andFont:[UIFont fontWithName:REGULAR_FONT size:16]];
        
        _contentLabel = [[UITextView alloc] initWithFrame:CGRectMake([UtilManager width:20], headerView.frame.origin.y +headerView.frame.size.height  + [UtilManager height:10], WIDTH - [UtilManager width:40], contentHeight + [UtilManager height:25])];
        //[_contentLabel setNumberOfLines:0];
        [_contentLabel setText:_exercise.content];
        [_contentLabel setTextColor:[UIColor whiteColor]];
        [_contentLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
        [_contentLabel setTextAlignment:NSTextAlignmentJustified];
        [_contentLabel setEditable:FALSE];
        [_contentLabel setBackgroundColor:[UIColor clearColor]];
        
        [self.view addSubview:_contentLabel];
    }
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50], [UtilManager height:20], [UtilManager width:40] * 0.7, [UtilManager width:40] * 0.7)];
    [okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:okButton];
}

- (BOOL)checkUserValues {
    NSInteger weight = [AppContext sharedInstance].weight;
    if (weight <= 0) {
        [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"Debe introducir un peso en su perfil", nil)] animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

- (void)reloadTimeLabel {
    
    int secs = 0;
    int tRep = 2;
    
    NSInteger kCal = 0;
    
    [self checkUserValues];
    
    if([[[_currentExercise.exercises objectAtIndex:0] type] isEqualToString:@"aerobic"]){
        for (Serie *serie in _series){
            secs = serie.execTime.intValue + serie.rest.intValue + secs;
        
            if([Serie kCalInSerie:serie]>0)
                kCal = kCal + [Serie kCalInSerie:serie];
        }
    }else{
        for (Serie *serie in _series){
            secs = serie.reps.intValue * tRep + serie.rest.intValue + secs;
            
            if(serie.reps.intValue > 0 && serie.weight.intValue>0){
                if([Serie kCalInSerie:serie]>0)
                    kCal = kCal + [Serie kCalInSerie:serie];
            }
        }
    }
    
    if(secs / 60 >= 1)
    {
        int minutes = secs / 60;
        int seconds = secs % 60;
        [_timeLabel setText:[NSString stringWithFormat:@"%d min %d s",minutes,seconds]];
    }else
        [_timeLabel setText:[NSString stringWithFormat:@"%d seg.",secs]];
    
    [_kCalLabel setText:[NSString stringWithFormat:@"%ld kCal",(long)kCal]];
}

- (UIView *)headerView{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.,0.,WIDTH,[UtilManager height:360])];
    
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:35], WIDTH, [UtilManager height:30])];
    if(_currentExercise){
        Exercise *exercise = [_currentExercise.exercises objectAtIndex:0];
        if(_currentExercise.isSuperSerie.boolValue){
            
            Exercise *secondExercise = [_currentExercise.exercises objectAtIndex:1];
            [titleLabel setText:[NSString stringWithFormat:@"%@+%@",exercise.title,secondExercise.title]];
        }else
            [titleLabel setText:[NSString stringWithFormat:@"%@",exercise.title]];
        
    }else if (_exercise)
        [titleLabel setText:_exercise.title];
    [titleLabel setFont:[UIFont fontWithName:BOLD_FONT size:21]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    [headerView addSubview:titleLabel];
    
    TagView *tagView = [[TagView alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.size.height + titleLabel.frame.origin.y + [UtilManager height:10], titleLabel.frame.size.width, TAGVIEW_SMALL_HEIGHT)];
    [tagView setIsCenter:TRUE];
    [tagView setCenter:CGPointMake(self.view.center.x, tagView.center.y)];
    [tagView setMode:TagsControlModeList];
    
    NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
    
    if(_currentExercise){
        if(_mainExercise.tag1 && _mainExercise.tag1.length > 0){
            Tag *tag1 = [Tag tagWithId:_mainExercise.tag1];
            if(tag1)
                [tags addObject:tag1];
        }
        if(_mainExercise.tag2 && _mainExercise.tag2.length > 0){
            Tag *tag2 = [Tag tagWithId:_mainExercise.tag2];
            if(tag2)
                [tags addObject:tag2];
        }
        if(_mainExercise.tag3 && _mainExercise.tag3.length > 0){
            Tag *tag3 = [Tag tagWithId:_mainExercise.tag3];
            if(tag3)
                [tags addObject:tag3];
        }
    } else if(_exercise){
        if(_exercise.tag1 && _exercise.tag1.length > 0){
            Tag *tag1 = [Tag tagWithId:_exercise.tag1];
            if(tag1)
                [tags addObject:tag1];
        }
        if(_exercise.tag2 && _exercise.tag2.length > 0){
            Tag *tag2 = [Tag tagWithId:_exercise.tag2];
            if(tag2)
                [tags addObject:tag2];
        }
        if(_exercise.tag3 && _exercise.tag3.length > 0){
            Tag *tag3 = [Tag tagWithId:_exercise.tag3];
            if(tag3)
                [tags addObject:tag3];
        }
    }
    
    [tagView setTags:tags];
    [tagView reloadTagSubviews];
    
    [headerView addSubview:tagView];
    
    UIImageView *clockImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.center.x - [UtilManager width:85], tagView.frame.origin.y + tagView.frame.size.height + [UtilManager height:8], [UtilManager width:25] * 0.8, [UtilManager width:25] * 0.8)];
    [clockImageView setImage:[UIImage imageNamed:@"icon-cell-clock"]];
    
    if(_currentExercise)
        [headerView addSubview:clockImageView];
    
    _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.center.x - [UtilManager width:60], clockImageView.frame.origin.y, [UtilManager width:55], clockImageView.frame.size.height)];
    [_timeLabel setText:@"0 min"];
    [_timeLabel setTextColor:GRAY_REGISTER_FONT];
    [_timeLabel setTextAlignment:NSTextAlignmentLeft];
    [_timeLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
    [_timeLabel setAdjustsFontSizeToFitWidth:YES];
    
    if(_currentExercise)
        [headerView addSubview:_timeLabel];
    
    UIImageView *kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_timeLabel.frame.origin.x + _timeLabel.frame.size.width, _timeLabel.frame.origin.y + [UtilManager width:5], [UtilManager width:20] * 0.8, [UtilManager width:20] * 0.8)];
    [kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-red"]];
    [kCalImageView setCenter:CGPointMake(self.view.center.x + [UtilManager width:5], clockImageView.center.y)];
    
    if(_currentExercise)
        [headerView addSubview:kCalImageView];
    
    _kCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(kCalImageView.frame.origin.x + kCalImageView.frame.size.width + 5, _timeLabel.frame.origin.y, [UtilManager width:95], _timeLabel.frame.size.height)];
    [_kCalLabel setTextColor:RED_APP_COLOR];
    [_kCalLabel setText:@"3780 kCal"];
    [_kCalLabel setText:[NSString stringWithFormat:@"%d kCal",_mainExercise.energy.intValue]];
    [_kCalLabel setFont:_timeLabel.font];
    
    if(_currentExercise)
        [headerView addSubview:_kCalLabel];
    
    [self reloadTimeLabel];
    
    _exerciseImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:20],_kCalLabel.frame.origin.y + _kCalLabel.frame.size.height + [UtilManager width:5], WIDTH - [UtilManager width:40], [UtilManager height:210])];
    [_exerciseImageView setImage:[UIImage imageNamed:@"img-placeholder-big"]];
    
    //if(_currentExercise)
    //    [headerView addSubview:_exerciseImageView];
    
    UIImageView *imageView = _exerciseImageView;
    
    NSURLRequest *request;
    
    if(_currentExercise)
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_EXERCISES,_mainExercise.picture]]];
    else
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_EXERCISES,_exercise.picture]]];
    
    [_exerciseImageView setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"img-placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        [imageView setImage:image];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [[imageView layer] setMasksToBounds:YES];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        //NSLog(@"%@",error);
        [imageView setImage:[UIImage imageNamed:@"img-placeholder"]];
    }];
    
    _scrollView = [[UIScrollView alloc] initWithFrame:_exerciseImageView.frame];
    [_scrollView setPagingEnabled:YES];
    [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width * 2, _scrollView.frame.size.height)];
    [_scrollView setDelegate:self];
    
    AVPlayerViewController *avplayerViewController = [[AVPlayerViewController alloc] init];
    avplayerViewController.view.frame = CGRectMake(0., 0., _exerciseImageView.frame.size.width, _exerciseImageView.frame.size.height);
    /*[avplayerViewController setShowsPlaybackControls:YES];
    
    NSString *urlPath = [[NSBundle mainBundle] pathForResource:@"exercise-sample" ofType:@"mp4"];
    
    [avplayerViewController setPlayer:[AVPlayer playerWithURL:[NSURL fileURLWithPath:urlPath]]];
    [avplayerViewController setDelegate:self];
    [avplayerViewController setShowsPlaybackControls:FALSE];
    [[avplayerViewController player]  pause];
    [[avplayerViewController player] play];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoError:) name:@"AVPlayerItemDidPlayToEndTimeNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoError:) name:@"AVPlayerItemDidPlayToEndTimeNotification" object:nil];
    //AVPlayerItemFailedToPlayToEndTimeErrorKey
    */
    
    FLAnimatedImageView *animatedImageView = [[FLAnimatedImageView alloc] initWithFrame:avplayerViewController.view.frame];
    
    animatedImageView.contentMode = UIViewContentModeScaleAspectFit;
    animatedImageView.clipsToBounds = YES;
    
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"exercise" withExtension:@"gif"];
    NSData *data1 = [NSData dataWithContentsOfURL:url1];
    animatedImageView.animatedImage = [FLAnimatedImage animatedImageWithGIFData:data1];
    
    UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(avplayerViewController.view.frame.size.width, 0., _scrollView.frame.size.width, _scrollView.frame.size.height)];
    [contentLabel setBackgroundColor:[UIColor clearColor]];
    [contentLabel setNumberOfLines:0];
    [contentLabel setText:_mainExercise.content];
    [contentLabel setTextColor:[UIColor whiteColor]];
    [contentLabel setFont:[UIFont fontWithName:LIGHT_FONT_OPENSANS size:16]];
    [contentLabel setTextAlignment:NSTextAlignmentJustified];
    
    [_scrollView addSubview:animatedImageView];
    [_scrollView addSubview:contentLabel];
    
    [headerView addSubview:_scrollView];
    
    if(_currentExercise){
    
        UIView *pageContainerView = [[UIView alloc] initWithFrame:CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.size.height - [UtilManager height:30] + _scrollView.frame.origin.y, _scrollView.frame.size.width, [UtilManager height:30])];
        [pageContainerView setBackgroundColor:[UIColor blackColor]];
        [pageContainerView setAlpha:0.6];
        //[headerView addSubview:pageContainerView];
        
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0., _scrollView.frame.size.height + _scrollView.frame.origin.y - [UtilManager height:10], WIDTH, [UtilManager height:20])];
        [_pageControl setNumberOfPages:2];
        [_pageControl setCurrentPage:0];
        [_pageControl setBackgroundColor:[UIColor clearColor]];
        
        [headerView addSubview:_pageControl];
    }
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(_exerciseImageView.frame.origin.x, _exerciseImageView.frame.origin.y + _exerciseImageView.frame.size.height + [UtilManager height:5],_exerciseImageView.frame.size.width,1)];
    [separatorView setBackgroundColor:GRAY_REGISTER_FONT];
    //[headerView addSubview:separatorView];
    
    return headerView;
}

- (void)videoError:(NSNotification *)notification{
    NSLog(@"%@",notification);
}

- (UIView *)footerView{
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.,HEIGHT - [UtilManager height:50], WIDTH, [UtilManager height:50])];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    UIButton *moreButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40] * 0.8, [UtilManager width:40]* 0.8)];
    [moreButton setImage:[UIImage imageNamed:@"btn-add"] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(didPressAddButton) forControlEvents:UIControlEventTouchUpInside];
    [moreButton setCenter:CGPointMake(self.view.center.x, footerView.frame.size.height/2)];
    
    [footerView addSubview:moreButton];
    
    /*
    UIButton *saveButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., WIDTH/2, footerView.frame.size.height)];
    [saveButton setTitle:NSLocalizedString(@"GUARDAR", nil) forState:UIControlStateNormal];
    [saveButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [[saveButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:23]];
    [saveButton addTarget:self action:@selector(saveAction) forControlEvents:UIControlEventTouchUpInside];
    [saveButton setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    [footerView addSubview:saveButton];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH/2, 0., WIDTH/2, footerView.frame.size.height)];
    [cancelButton setTitle:NSLocalizedString(@"CANCELAR", nil) forState:UIControlStateNormal];
    [cancelButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [[cancelButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:23]];
    [cancelButton addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    [footerView addSubview:cancelButton];
    
    */ 
     
    return footerView;
}

- (void)didPressOKButton {
    if (_performTraining) {
        [_executive dismissToParent];
        return;
    }

    NSMutableArray * tempSeries = [_series mutableCopy];
    
    for(Serie *serie in _series) {
        if ([Serie isEmptyRepsSerie:serie]) {
            if ([tempSeries containsObject:serie]) {
                [tempSeries removeObject:serie];
            }
            
            NSString *serieId = serie.serieId;
            [Serie deleteSerieWithId:serieId];
        }
    }
    
    _series = tempSeries;
    
    if(_series.count == 0) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        if(_currentExercise.isSuperSerie.boolValue) {
            [_delegate backCurrentSeries:_series andSSeries:_secondExercisesSeries inIndex:_index];
        }
        else {
            [_delegate backCurrentSeries:_series inIndex:_index];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)cancelAction {
}

- (void)didPressAddButton {
    
    //
    // Create new serie
    //
    
    Serie *newSerie = [[Serie alloc] init];
    newSerie.exercise = [_currentExercise.exercises firstObject];
    newSerie.serieId = [UtilManager uuid];
    newSerie.createdAt = [NSDate date];
    newSerie.updatedAt = [NSDate date];
    newSerie.isSubserie = @NO;
    newSerie.owner = [AppContext sharedInstance].currentUser.name;
    
    //
    // Create second serie
    //
    
    Serie *secondSerie;
    if (_currentExercise.isSuperSerie.boolValue) {
        secondSerie = [[Serie alloc] init];
        secondSerie.exercise = [_currentExercise.exercises objectAtIndex:1];
        secondSerie.serieId = [UtilManager uuid];
        secondSerie.createdAt = [NSDate date];
        secondSerie.updatedAt = [NSDate date];
        secondSerie.isSubserie = @NO;
        secondSerie.owner = [AppContext sharedInstance].currentUser.name;
        secondSerie.intensityFactor = [NSNumber numberWithInt:2];
    }
    
    //
    // Add serie
    //
    
    if(_series.count > 0) {
        Serie *lastSerie = [_series lastObject];
        newSerie.reps = lastSerie.reps;
        newSerie.rest = lastSerie.rest;
        newSerie.weight = lastSerie.weight;
        newSerie.order = @([_series lastObject].order.intValue + 1);
        newSerie.intensityFactor = lastSerie.intensityFactor;
        newSerie.execTime = lastSerie.execTime;
        
        if(_currentExercise.isSuperSerie.boolValue) {
            lastSerie = [_secondExercisesSeries lastObject];
            secondSerie.reps = lastSerie.reps;
            secondSerie.reps = lastSerie.rest;
            secondSerie.weight = lastSerie.weight;
            secondSerie.order = @([_series lastObject].order.intValue + 1);
        }
    }
    else {
        newSerie.reps = @0;
        newSerie.rest = @30;
        newSerie.weight = @([AppContext sharedInstance].weight);
        newSerie.order = @1;
        newSerie.execTime = @30;
        newSerie.intensityFactor = @0;

        if(_currentExercise.isSuperSerie.boolValue) {
            secondSerie.reps = @0;
            secondSerie.rest = @0;
            secondSerie.weight = @0;
            secondSerie.order = @1;;
            secondSerie.execTime = @0;
            secondSerie.intensityFactor = @0;
        }
    }
    
    [Serie addSerie:newSerie];
    
    if(_currentExercise.isSuperSerie.boolValue) {
        [Serie addSerie:secondSerie];
        [_secondExercisesSeries addObject:secondSerie];
    }
    
    [_series addObject:newSerie];
    
    //
    // Reload table
    //
    
    [_serieTableView reloadData];
    [self reloadTimeLabel];
}
    
- (void)addSubSerieTo:(Serie *)serie {
    
    //
    // Create serie to be child of the serie
    //
    
    Serie *newSerie = [[Serie alloc] init];
    newSerie.exercise = [_currentExercise.exercises firstObject];
    newSerie.serieId = [UtilManager uuid];
    newSerie.createdAt = [NSDate date];
    newSerie.updatedAt = [NSDate date];
    newSerie.isSubserie = @YES;
    newSerie.owner = [AppContext sharedInstance].currentUser.name;
    newSerie.reps = serie.reps;
    newSerie.rest = serie.rest;
    newSerie.weight = serie.weight;
    newSerie.order = serie.order;
    newSerie.intensityFactor = @2;
    
    //
    // Insert sub serie
    //
    
    int countIndex = 0;
    int startIndex = -1;
    int auxIndex = 0;
    
    for(Serie *serieIndex in _series)  {
        if(serieIndex.order.intValue == serie.order.intValue) {
            countIndex ++;
            if(startIndex < 0) {
                startIndex = auxIndex;
            }
        }
        auxIndex++;
    }
    
    [Serie addSerie:newSerie];
    [_series insertObject:newSerie atIndex:startIndex + countIndex];
    
    //
    // Reload table
    //
    
    [_serieTableView reloadData];
}

- (void)presentPickerViewAtIndex:(NSInteger)index{
    
    if(_timePickerView) {
        [self hideTimePicker];
    }
    
    _containerPickerView = [[UIView alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - [UtilManager height:206], WIDTH, [UtilManager height:206])];
    [_containerPickerView setBackgroundColor:[UIColor clearColor]];
    
    _timePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0., [UtilManager height:44], self.view.frame.size.width, [UtilManager height:162])];
    [_timePickerView setBackgroundColor:BLACK_APP_COLOR];
    
    [_timePickerView setUserInteractionEnabled:YES];
    [_timePickerView setDelegate:self];
    [_timePickerView setDataSource:self];
    _timePickerView.showsSelectionIndicator = YES;
    
    /*
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,WIDTH,[UtilManager height:44])];
    [toolBar setBarStyle:UIBarStyleBlackTranslucent];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil)
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(acceptPickerAction)];
    
    [barButtonDone setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      YELLOW_APP_COLOR, NSForegroundColorAttributeName,nil]
                          forState:UIControlStateNormal];
    [barButtonDone setTintColor:YELLOW_APP_COLOR];
    toolBar.items = @[barButtonDone];
     */
    
    UIView *toolBarDescView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:44])];
    [toolBarDescView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIButton *doneDescButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:100],0. , [UtilManager width:90], [UtilManager height:35])];
    [doneDescButton addTarget:self action:@selector(acceptPickerAction) forControlEvents:UIControlEventTouchUpInside];
    [doneDescButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [doneDescButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [toolBarDescView addSubview:doneDescButton];
    
    [_containerPickerView addSubview:_timePickerView];
    [_containerPickerView addSubview:toolBarDescView];
    
    [self.view addSubview:_containerPickerView];
}

- (void)acceptPickerAction {
    NSInteger min = [_timePickerView selectedRowInComponent:0];
    NSInteger seg = [_timePickerView selectedRowInComponent:1] * 5;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_serieIndex inSection:0];
    SeriesCell *serieCell = [_serieTableView cellForRowAtIndexPath:indexPath];
    
    NSString *minutes;
    NSString *seconds;
    
    //if(min<10)
    //    minutes = [NSString stringWithFormat:@"0%ld",(long)min];
    //else
        minutes = [NSString stringWithFormat:@"%ld",(long)min];
    
    if(seg<10)
        seconds = [NSString stringWithFormat:@"0%ld",(long)seg];
    else
        seconds = [NSString stringWithFormat:@"%ld",(long)seg];
    
    if(_timeColumn !=1) {
        [[serieCell descTextField] setText:[NSString stringWithFormat:@"%@:%@",minutes,seconds]];
    }
    else {
        [[serieCell repTextField] setText:[NSString stringWithFormat:@"%@:%@",minutes,seconds]];
    }
    
    Serie *currentSerie = [_series objectAtIndex:indexPath.row];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    if(_currentExercise.isSuperSerie.boolValue)
    {
        Serie *secondSerie = [_secondExercisesSeries objectAtIndex:indexPath.row];
        [realm transactionWithBlock:^{
            [secondSerie setUpdatedAt:[NSDate date]];
//            if(_timeColumn != 1)
//                [secondSerie setRest:[NSNumber numberWithInteger:min * 60 + seg]];
//            else
                [currentSerie setExecTime:[NSNumber numberWithInteger:min * 60 + seg]];
        }];
        
        [realm beginWriteTransaction];
        [realm addOrUpdateObject:secondSerie];
        NSError *error;
        [realm commitWriteTransaction:&error];
        if(error)
            NSLog(@"%@",error.description);
        
    }
    
    [realm transactionWithBlock:^{
        [currentSerie setUpdatedAt:[NSDate date]];
//        if(_timeColumn != 1)
//            [currentSerie setRest:[NSNumber numberWithInteger:min * 60 + seg]];
//        else
            [currentSerie setExecTime:[NSNumber numberWithInteger:min * 60 + seg]];
    }];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:currentSerie];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error) {
        NSLog(@"%@",error.description);
    }
    
    [self reloadTimeLabel];
    
    [self hideTimePicker];
}

- (void)hideTimePicker{
    [_containerPickerView removeFromSuperview];
    _containerPickerView = nil;
    
    [_timePickerView  removeFromSuperview];
    _timePickerView = nil;
}


#pragma mark UIScrollView Delegate Methods

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if(scrollView.contentOffset.x > 0)
        _pageControl.currentPage = 1;
    else
        _pageControl.currentPage = 0;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView.contentOffset.x > 0)
        _pageControl.currentPage = 1;
    else
        _pageControl.currentPage = 0;

}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    /*
    Serie *currentSeries = [_series objectAtIndex:indexPath.row];
    
    SeriesInputView *seriesInputView = [[SeriesInputView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:280], [UtilManager height:240])];
    [seriesInputView setCenter:CGPointMake(self.view.center.x,[UtilManager height:120])];
    [seriesInputView setDelegate:self];
    
    _isEditing = TRUE;
    
    [seriesInputView setIndex:indexPath.row + 1];
    [[seriesInputView restTextField] setText:[NSString stringWithFormat:@"%ld",(long)currentSeries.rest.integerValue]];
    [[seriesInputView weightTextField] setText:[NSString stringWithFormat:@"%ld",(long)currentSeries.weight.integerValue]];
    [[seriesInputView repsTextField] setText:[NSString stringWithFormat:@"%ld",(long)currentSeries.reps.integerValue]];
    
    _modalView = [[RNBlurModalView alloc] initWithParentView:self.view view:seriesInputView];
    [_modalView show];
     */
}
    

- (nullable NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(3_0) __TVOS_PROHIBITED{
    return NSLocalizedString(@"Delete", nil);
}

- (nullable NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(8_0) __TVOS_PROHIBITED {
    
    Serie *currentSerie = [_series objectAtIndex:indexPath.row];
    
    
    UITableViewRowAction *moreAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"SubSerie", nil)  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        // maybe show an action sheet with more options
        [self addSubSerieTo:currentSerie];
        [_serieTableView setEditing:NO];
    }];
    moreAction.backgroundColor = [UIColor lightGrayColor];
    
    UITableViewRowAction *blurAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Blur" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [_serieTableView setEditing:NO];
    }];
    blurAction.backgroundEffect = [UIVibrancyEffect effectForBlurEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Delete", nil)  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [_series removeObjectAtIndex:indexPath.row];
        [_serieTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [Serie deleteSerie:currentSerie];
        
//        int newOrder = 1;
//
//        for(Serie *serie in _series){
//
//            RLMRealm *realm = [AppContext currentRealm];
//
//            [realm transactionWithBlock:^{
//                [serie setOrder:[NSNumber numberWithInt:newOrder]];
//            }];
//
//            [realm beginWriteTransaction];
//            NSError *error;
//            [realm commitWriteTransaction:&error];
//            if(error)
//                NSLog(@"%@",error.description);
//
//            if(!serie.isSubserie.boolValue)
//                newOrder ++;
//        }
        
        [_serieTableView reloadData];
    }];
    
    if(currentSerie.isSubserie.boolValue || _currentExercise.isSuperSerie.boolValue)
        return @[deleteAction];
    else
        return @[deleteAction, moreAction];
    
    
}

#pragma mark UITableViewDatasource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _series.count;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return [UtilManager height:50];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return [UtilManager height:50];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //return [UtilManager height:360];
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    //return [UtilManager height:360];
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_currentExercise.isSuperSerie.boolValue)
        return [UtilManager height:55] * 2;
    return [UtilManager height:65];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_currentExercise.isSuperSerie.boolValue)
        return [UtilManager height:55] * 2;
    return [UtilManager height:65];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.,HEIGHT - [UtilManager height:50], WIDTH, [UtilManager height:50])];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    return footerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [self headerView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SeriesCell *seriesCell = [_serieTableView dequeueReusableCellWithIdentifier:@"seriesCell"];
    Serie *currentSerie = [_series objectAtIndex:indexPath.row];
    
    seriesCell.trainingExercise = _currentExercise;
    if (_currentExercise.isSuperSerie.boolValue) {
        Serie *secondExerciseSerie = [_secondExercisesSeries objectAtIndex:indexPath.row];
        seriesCell.secondExerciseSerie = secondExerciseSerie;
    }
    
    seriesCell.index = indexPath.row;
    seriesCell.delegate = self;
    [seriesCell setContentWithSerie:currentSerie];
    return seriesCell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView beginUpdates];
    NSUInteger row = [indexPath row];
    [_series removeObjectAtIndex:row];
    
    int newOrder = 1;
    
    for(Serie *serie in _series){
        
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            [serie setOrder:[NSNumber numberWithInt:newOrder]];
        }];
        
        [realm beginWriteTransaction];
        NSError *error;
        [realm commitWriteTransaction:&error];
        if(error)
            NSLog(@"%@",error.description);
        
        if(!serie.isSubserie.boolValue)
            newOrder ++;
    }
    
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [tableView endUpdates];
    
    [tableView reloadData];
    
    [self reloadTimeLabel];
}
    

#pragma mark - SeriesCellDelegateMethods

- (void)seriesData:(NSDictionary *)seriesDictinary indIndex:(NSInteger)currentIndex{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    if(_currentExercise.isSuperSerie.boolValue){
        Serie *secondSerie = [_secondExercisesSeries objectAtIndex:currentIndex];
    
        [realm transactionWithBlock:^{
            [secondSerie setUpdatedAt:[NSDate date]];
            if([secondSerie.exercise.type isEqualToString:@"aerobic"]){
                //[secondSerie setExecTime:[seriesDictinary valueForKey:@"repsSS"]];
                [secondSerie setIntensityFactor:[seriesDictinary valueForKey:@"weightSS"]];
            }
            else if ([secondSerie.exercise.energyType isEqualToString:@"reps"]){
                [secondSerie setReps:[seriesDictinary valueForKey:@"repsSS"]];
                [secondSerie setWeight:[seriesDictinary valueForKey:@"weightSS"]];
            }
            else{
                [secondSerie setExecTime:[seriesDictinary valueForKey:@"repsSS"]];
                [secondSerie setWeight:[seriesDictinary valueForKey:@"weightSS"]];
            }
            
        }];
    
    }

    Serie *currentSerie = [_series objectAtIndex:currentIndex];
    
    [realm transactionWithBlock:^{
        [currentSerie setUpdatedAt:[NSDate date]];
        
        if([currentSerie.exercise.type isEqualToString:@"aerobic"]){
            //[currentSerie setExecTime:[seriesDictinary valueForKey:@"reps"]];
            [currentSerie setIntensityFactor:[seriesDictinary valueForKey:@"weight"]];
        }
        else if ([currentSerie.exercise.energyType isEqualToString:@"reps"]){
            [currentSerie setReps:[seriesDictinary valueForKey:@"reps"]];
            [currentSerie setWeight:[seriesDictinary valueForKey:@"weight"]];
        }
        else{
            [currentSerie setReps:[seriesDictinary valueForKey:@"reps"]];
//            [currentSerie setExecTime:[seriesDictinary valueForKey:@"reps"]];
            [currentSerie setWeight:[seriesDictinary valueForKey:@"weight"]];
        }
    }];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:currentSerie];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error)
        NSLog(@"%@",error.description);

    [_series replaceObjectAtIndex:currentIndex withObject:currentSerie];
    
    [_serieTableView reloadData];
    
    [self reloadTimeLabel];
}

- (void)textEndShowInIndex:(NSInteger)currentIndex{
    [self hideTimePicker];
}

- (void)textBeginShowInIndex:(NSInteger)currentIndex{
    _serieIndex = currentIndex;
}

- (void)timeDidPushAtIndex:(NSInteger)index andColumns:(NSInteger)column{
    
    _timeColumn = column;
    _serieIndex = index;
    if(index<0)
        NSLog(@"Bajar scroll si lo hubiera");
    else
        [self presentPickerViewAtIndex:index];
        
}

- (void)seriesCell:(SeriesCell *)cell didPressStopButton:(UIButton *)sender index:(NSInteger)index {
    if (sender.isSelected) {
        [_executive pauseExecuting];
    }
    else {
        [_executive startExecuting];
    }
}

#pragma mark SeriesInputDelegate

- (void)getSeriesData:(NSDictionary *)seriesData{
    
    [_modalView didPressOKButton];
    
    if(_isEditing){
        
        NSNumber *index = [seriesData valueForKey:@"index"];
        
        Serie *currentSerie = [_series objectAtIndex:index.intValue - 1];
        
        [currentSerie setUpdatedAt:[NSDate date]];
        [currentSerie setReps:[seriesData valueForKey:@"reps"]];
        [currentSerie setRest:[seriesData valueForKey:@"rest"]];
        [currentSerie setWeight:[seriesData valueForKey:@"weight"]];
        [currentSerie setSerieId:currentSerie.serieId];
        [currentSerie setIntensityFactor:[NSNumber numberWithInt:2]];
        
        [_series replaceObjectAtIndex:index.intValue-1 withObject:currentSerie];
    
    } else {
        Serie *newSerie = [[Serie alloc] init];
        
        [newSerie setExercise:[_currentExercise.exercises objectAtIndex:0]];
        
        [newSerie setCreatedAt:[NSDate date]];
        [newSerie setUpdatedAt:[NSDate date]];
        
        [newSerie setOwner:[[AppContext sharedInstance] currentUser].userId];
        [newSerie setReps:[seriesData valueForKey:@"reps"]];
        [newSerie setRest:[seriesData valueForKey:@"rest"]];
        [newSerie setWeight:[seriesData valueForKey:@"weight"]];
        [newSerie setIntensityFactor:[NSNumber numberWithInt:2]];
        
        [_series addObject:newSerie];
    }
    
    [_serieTableView reloadData];
}

#pragma mark AVPlayerViewController

- (void)playerViewControllerWillStartPictureInPicture:(AVPlayerViewController *)playerViewController{
    
}

/*!
	@method		playerViewControllerDidStartPictureInPicture:
	@param		playerViewController
 The player view controller.
	@abstract	Delegate can implement this method to be notified when Picture in Picture did start.
 */
- (void)playerViewControllerDidStartPictureInPicture:(AVPlayerViewController *)playerViewController{

}

/*!
	@method		playerViewController:failedToStartPictureInPictureWithError:
	@param		playerViewController
 The player view controller.
	@param		error
 An error describing why it failed.
	@abstract	Delegate can implement this method to be notified when Picture in Picture failed to start.
 */
- (void)playerViewController:(AVPlayerViewController *)playerViewController failedToStartPictureInPictureWithError:(NSError *)error{

}

/*!
	@method		playerViewControllerWillStopPictureInPicture:
	@param		playerViewController
 The player view controller.
	@abstract	Delegate can implement this method to be notified when Picture in Picture will stop.
 */
- (void)playerViewControllerWillStopPictureInPicture:(AVPlayerViewController *)playerViewController{

}

/*!
	@method		playerViewControllerDidStopPictureInPicture:
	@param		playerViewController
 The player view controller.
	@abstract	Delegate can implement this method to be notified when Picture in Picture did stop.
 */
- (void)playerViewControllerDidStopPictureInPicture:(AVPlayerViewController *)playerViewController{

}

/*!
	@method		playerViewControllerShouldAutomaticallyDismissAtPictureInPictureStart:
	@param		playerViewController
 The player view controller.
	@abstract	Delegate can implement this method and return NO to prevent player view controller from automatically being dismissed when Picture in Picture starts.
 */
- (BOOL)playerViewControllerShouldAutomaticallyDismissAtPictureInPictureStart:(AVPlayerViewController *)playerViewController
{
    return TRUE;
}

/*!
	@method		playerViewController:restoreUserInterfaceForPictureInPictureStopWithCompletionHandler:
	@param		playerViewController
 The player view controller.
	@param		completionHandler
 The completion handler the delegate needs to call after restore.
	@abstract	Delegate can implement this method to restore the user interface before Picture in Picture stops.
 */
- (void)playerViewController:(AVPlayerViewController *)playerViewController restoreUserInterfaceForPictureInPictureStopWithCompletionHandler:(void (^)(BOOL restored))completionHandler{

}

#pragma mark UIPickerDataSource Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component == 1)
        return 60/5;
    return 60;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    /// Set line break mode
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    /// Set text alignment
    //NSTextAlignmentLeft;//NSTextAlignmentRight;// NSTextAlignmentNatural;//  NSTextAlignmentLeft;

    
    switch (component) {
        case 0:{
            title = [NSString stringWithFormat:@"%ld min",(long)row];
            paragraphStyle.alignment = NSTextAlignmentCenter;
            break;
        }
        default:{
            if(row<2)
                title = [NSString stringWithFormat:@"0%ld sec",(long)row * 5];
            else
                title = [NSString stringWithFormat:@"%ld sec",(long)row * 5];
            paragraphStyle.alignment = NSTextAlignmentCenter;
            break;
        }
    }
    NSAttributedString *attString =
    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor lightTextColor],NSParagraphStyleAttributeName:paragraphStyle}];
    
    return attString;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return [NSString stringWithFormat:@"%ld min",(long)row];
            break;
        default:
            return [NSString stringWithFormat:@"%ld sec",(long)row * 5];
            break;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return [UtilManager width:80];
}

#pragma mark - SeriesDisplay

- (void)reloadSeries {
    [_serieTableView reloadData];
}


- (void)didCompletedExecutingSeries:(NSInteger)order {
    [self dismissViewControllerAnimated:YES completion:^{
        if (_workoutDetailDelegate &&
            [_workoutDetailDelegate respondsToSelector:@selector(didCompleteExecutingExercise:)]) {
            
            [_workoutDetailDelegate didCompleteExecutingExercise:order];
        }
    }];
}

- (void)dismissDisplayWithOrder:(NSInteger)order
                     serieIndex:(NSInteger)serieIndex
                        seconds:(NSInteger)seconds {
    
    if (_workoutDetailDelegate &&
        [_workoutDetailDelegate respondsToSelector:@selector(didPauseExecutingExercise:)]) {
        
        [self dismissViewControllerAnimated:YES completion:^{
            [_workoutDetailDelegate didPauseExecutingExercise:order];
        }];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
