//
//  TimeCell.h
//  Titan
//
//  Created by Manuel Manzanera on 5/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSCell.h"

@protocol TimeCellDelegate <NSObject>

@end

@interface TimeCell : UITableViewCell

@property (nonatomic, strong) UILabel *timeLabel;

@end
