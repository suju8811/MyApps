//
//  FitnessCell.m
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FitnessCell.h"

#import "AppConstants.h"
#import "AppContext.h"
#import "UtilManager.h"

@implementation FitnessCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        /*
         img-workout-cell
         */
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:10], [UtilManager width:300], [UtilManager height:20])];
        [_titleLabel setText:@"Sesión inicial nutrición"];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setFont:[UIFont fontWithName:SEMIBOLD_FONT size:15]];
        [_titleLabel setTextColor:YELLOW_APP_COLOR];
        
        [[self contentView] addSubview:_titleLabel];
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.size.height + _titleLabel.frame.origin.y + [UtilManager height:5], _titleLabel.frame.size.width, TAGVIEW_SMALL_HEIGHT)];
        [_tagView setMode:TagsControlModeList];
        [_tagView setUserInteractionEnabled:FALSE];
        
        [[self contentView] addSubview:_tagView];
        
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x,_tagView.frame.origin.y + _tagView.frame.size.height, _titleLabel.frame.size.width, [UtilManager height:35])];
        [_contentLabel setTextColor:[UIColor whiteColor]];
        [_contentLabel setNumberOfLines:2];
        [_contentLabel setAdjustsFontSizeToFitWidth:YES];
        [_contentLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [_contentLabel setText:@"Lorem ipsum dolor sit amet consecaquat at sidspld cdsvdoigds   pdogiopdisg podigopdig posdig psdogids pogik ñlfkgñlfk g..."];
        [_contentLabel setTextColor:[UIColor lightGrayColor]];
        //[[self contentView] addSubview:_contentLabel];
        
        _calendarImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-birth"]];
        [_calendarImageView setCenter:CGPointMake(self.contentView.center.x, _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:25])];
        
        [[self contentView] addSubview:_calendarImageView];
        
        _exercisesLabel = [[UILabel alloc] initWithFrame:CGRectMake(_contentLabel.frame.origin.x,_calendarImageView.frame.origin.y - [UtilManager height:8] , [UtilManager width:120], [UtilManager height:20])];
        [_exercisesLabel setText:@"5 Ejercicios"];
        [_exercisesLabel setTextColor:GRAY_REGISTER_FONT];
        [_exercisesLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        
        [[self contentView] addSubview:_exercisesLabel];
        
        _daysLabel = [[UILabel alloc] initWithFrame:CGRectMake(_calendarImageView.frame.origin.x + _calendarImageView.frame.size.width + [UtilManager width:5], _calendarImageView.frame.origin.y, [UtilManager width:200], [UtilManager height:20])];
        [_daysLabel setText:@"21 días"];
        [_daysLabel setTextColor:GRAY_REGISTER_FONT];
        [_daysLabel setFont:_exercisesLabel.font];
        
        UIImageView *clockImageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH/3 + [UtilManager width:20], _exercisesLabel.frame.origin.y, [UtilManager width:25] * 0.8, [UtilManager width:25] * 0.8)];
        [clockImageView setImage:[UIImage imageNamed:@"icon-cell-clock"]];
        
        [self.contentView addSubview:clockImageView];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(clockImageView.frame.origin.x + clockImageView.frame.size.width + [UtilManager height:5], _exercisesLabel.frame.origin.y,[UtilManager width:60], _exercisesLabel.frame.size.height)];
        [_timeLabel setText:@"47 min"];
        [_timeLabel setTextAlignment:NSTextAlignmentLeft];
        [_timeLabel setFont:_exercisesLabel.font];
        [_timeLabel setTextColor:_exercisesLabel.textColor];
        [_timeLabel setAdjustsFontSizeToFitWidth:YES];
        [_timeLabel setTextColor:GRAY_REGISTER_FONT];
        [_timeLabel setFont:_exercisesLabel.font];
        [clockImageView setFrame:CGRectMake(_timeLabel.frame.origin.x - [UtilManager width:30], clockImageView.frame.origin.y, clockImageView.frame.size.width, clockImageView.frame.size.height)];
        
        [self.contentView addSubview:_timeLabel];
        
        UIImageView *kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_timeLabel.frame.origin.x + _timeLabel.frame.size.width + [UtilManager width:5], _timeLabel.frame.origin.y, [UtilManager width:20] * 0.8, [UtilManager width:20] * 0.8)];
        [kCalImageView setCenter:CGPointMake(2 * WIDTH / 3, clockImageView.center.y)];
        [kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-red"]];
        
        [self.contentView addSubview:kCalImageView];
        
        _kCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(kCalImageView.frame.origin.x + kCalImageView.frame.size.width + 5, _timeLabel.frame.origin.y, _timeLabel.frame.size.width, _timeLabel.frame.size.height)];
        [_kCalLabel setTextColor:RED_APP_COLOR];
        [_kCalLabel setText:[NSString stringWithFormat:@"%d kCal",0]];
        [_kCalLabel setFont:_exercisesLabel.font];
        
        [self.contentView addSubview:_kCalLabel];

        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., [UtilManager height:79],WIDTH, 1)];
        [_separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        
        [[self contentView] addSubview:_separatorView];
        
        _menuButton = [[UIButton alloc] initWithFrame:CGRectMake(self.contentView.frame.size.width, 0, [UtilManager width:35], [UtilManager width:35])];
        [_menuButton addTarget:self action:@selector(menuAction) forControlEvents:UIControlEventTouchUpInside];
        [_menuButton setImage:[UIImage imageNamed:@"btn-options"] forState:UIControlStateNormal];
        [_menuButton setCenter:CGPointMake(_menuButton.center.x, planCellHeight/2)];
        
        [[self contentView] addSubview:_menuButton];
        
        _checkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:25], 0, [UtilManager width:18], [UtilManager width:18])];
        [_checkImageView setImage:[UIImage imageNamed:@"img-check"]];
        [_checkImageView setCenter:CGPointMake(_checkImageView.center.x, self.contentView.center.y)];
        [_checkImageView setHidden:TRUE];
        
        [[self contentView] addSubview:_checkImageView];
        
    }
    
    return self;
}


- (void)menuAction{
    [_delegate optionsTrainingDidPushInIndex:_currentIndex];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
