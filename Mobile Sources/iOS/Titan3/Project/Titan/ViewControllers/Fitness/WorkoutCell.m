//
//  TrainingExerciseCell.m
//  Titan
//
//  Created by Manuel Manzanera on 23/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "WorkoutCell.h"
#import "UtilManager.h"
#import "AppContext.h"
#import "AppConstants.h"

@implementation WorkoutCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        _workoutImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:10], [UtilManager width:100], [UtilManager width:100])];
        [_workoutImageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
        
        [[self contentView] addSubview:_workoutImageView];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_workoutImageView.frame.origin.x + _workoutImageView.frame.size.width + [UtilManager width:10], [UtilManager height:10], WIDTH - (_workoutImageView.frame.origin.x + _workoutImageView.frame.size.width + [UtilManager width:30]), [UtilManager height:20])];
        
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setTextColor:YELLOW_APP_COLOR];
        [_titleLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
        [_titleLabel setAdjustsFontSizeToFitWidth:YES];
         
         [[self contentView] addSubview:_titleLabel];
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.size.height + self.titleLabel.frame.origin.y + [UtilManager height:5], self.titleLabel.frame.size.width, TAGVIEW_SMALL_HEIGHT)];
        [_tagView setMode:TagsControlModeList];
        [_tagView setUserInteractionEnabled:NO];
        [self.contentView addSubview:_tagView];
        
        _seriesLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.titleLabel.frame.origin.x, _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:5], self.titleLabel.frame.size.width, self.titleLabel.frame.size.height)];
        [_seriesLabel setTextColor:[UIColor lightGrayColor]];
        [_seriesLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:13]];
        [_seriesLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_seriesLabel];
        
        _clockImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.titleLabel.frame.origin.x, _seriesLabel.frame.origin.y + _seriesLabel.frame.size.height + [UtilManager height:10], [UtilManager width:20] * 0.8, [UtilManager width:20] * 0.8)];
        [_clockImageView setImage:[UIImage imageNamed:@"icon-cell-clock"]];
        
        [[self contentView] addSubview:_clockImageView];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(_clockImageView.frame.origin.x + _clockImageView.frame.size.width + [UtilManager width:5], _clockImageView.frame.origin.y, [UtilManager width:80], _clockImageView.frame.size.height)];
        [_timeLabel setTextColor:GRAY_REGISTER_FONT];
        [_timeLabel setFont:_seriesLabel.font];
        [_timeLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_timeLabel];
        
        _kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_timeLabel.frame.origin.x + _timeLabel.frame.size.width, _clockImageView.frame.origin.y, [UtilManager width:20] * 0.8, [UtilManager width:20] * 0.8)];
        [_kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-red"]];
        
        [[self contentView] addSubview:_kCalImageView];
        
        _energyLabel = [[UILabel alloc] initWithFrame:CGRectMake(_kCalImageView.frame.origin.x + _kCalImageView.frame.size.width, _kCalImageView.frame.origin.y, [UtilManager width:110], _kCalImageView.frame.size.height)];
        [_energyLabel setTextColor:RED_APP_COLOR];
        [_energyLabel setText:@"3780 kCal"];
        [_energyLabel setFont:_seriesLabel.font];
        
        [[self contentView] addSubview:_energyLabel];
        
        _menuButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:40], 0, [UtilManager width:35], [UtilManager width:35])];
        [_menuButton addTarget:self action:@selector(menuAction) forControlEvents:UIControlEventTouchUpInside];
        [_menuButton setImage:[UIImage imageNamed:@"btn-options"] forState:UIControlStateNormal];
        [_menuButton setCenter:CGPointMake(_menuButton.center.x, _workoutImageView.center.y)];
        
        [[self contentView] addSubview:_menuButton];
        
        //[[self contentView] addSubview:_separatorView];
        
        _checkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:25], 0, [UtilManager width:18], [UtilManager width:18])];
        [_checkImageView setImage:[UIImage imageNamed:@"img-check"]];
        [_checkImageView setCenter:CGPointMake(_checkImageView.center.x, self.contentView.center.y)];
        [_checkImageView setHidden:TRUE];
        
        [[self contentView] addSubview:_checkImageView];
        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
        [panGesture setEnabled:TRUE];
        
        _panView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH * 0.3, self.contentView.frame.size.height)];
        [_panView setBackgroundColor:[UIColor clearColor]];
        [_panView setUserInteractionEnabled:TRUE];
        [_panView addGestureRecognizer:panGesture];
        
        [[self contentView] addSubview:_panView];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(0., [UtilManager height:119], WIDTH, 1)];
        [_separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        [_separatorView setAlpha:0.2];
        
        [[self contentView] addSubview:_separatorView];
        
        [self.separatorView setFrame:CGRectMake(0., [UtilManager height:119], WIDTH, 1)];
    }
    
    return self;
}

- (void)didTransitionToState:(UITableViewCellStateMask)state{

}

//- (void)willTransitionToState:(UITableViewCellStateMask)state{
//    [super willTransitionToState:state];
//    if ((state & UITableViewCellStateShowingDeleteConfirmationMask) == UITableViewCellStateShowingDeleteConfirmationMask)
//    {
//        for (UIView *subview in self.subviews)
//        {
//            if ([NSStringFromClass([subview class]) isEqualToString:@"UITableViewCellDeleteConfirmationControl"])
//            {
//                UIImageView *deleteBtn = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 64, 33)];
//                [deleteBtn setBackgroundColor:[UIColor yellowColor]];
//                [[subview.subviews objectAtIndex:0] addSubview:deleteBtn];
//            }
//        }
//    }
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)menuAction {
    if (self.exerciseStatus != -1) {
        return;
    }
    
    [_delegate optionsExerciseDidPushInIndex:_currentIndex];
}

- (void)setExerciseStatus:(NSInteger)exerciseStatus {
    _exerciseStatus = exerciseStatus;
    if (_exerciseStatus <= 0) {
        [_menuButton setImage:[UIImage imageNamed:@"btn-options"] forState:UIControlStateNormal];
    }
    else if (_exerciseStatus == 1) {
        [_menuButton setImage:[UIImage imageNamed:@"btn-check"] forState:UIControlStateNormal];
    }
    else if (_exerciseStatus == 2) {
        [_menuButton setImage:[UIImage imageNamed:@"icon_lock"] forState:UIControlStateNormal];
    }
}

- (void)panGestureAction:(UIGestureRecognizer*)gestureRecognizer {
}

@end
