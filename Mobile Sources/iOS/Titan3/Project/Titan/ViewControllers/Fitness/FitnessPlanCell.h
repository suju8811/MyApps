//
//  FitnessPlanCell.h
//  Titan
//
//  Created by Manuel Manzanera on 7/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TagView.h"


@protocol FitnessPlanCellDelegate;

@interface FitnessPlanCell : UITableViewCell

@property (nonatomic, strong) UIView *containerView;

@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *exercisesLabel;
@property (nonatomic, strong) UILabel *daysLabel;
@property (nonatomic, strong) UIView *separatorView;

@property (nonatomic, strong) UIView *panView;

@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, assign) id<FitnessPlanCellDelegate>delegate;

@end

@protocol FitnessPlanCellDelegate <NSObject>

- (void)optionsDidPushInIndex:(NSInteger)index;

@end
