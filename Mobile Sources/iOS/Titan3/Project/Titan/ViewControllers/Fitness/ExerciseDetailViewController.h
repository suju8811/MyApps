//
//  ExerciseDetailViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 27/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "RNBlurModalView.h"
#import "Exercise.h"

@interface ExerciseDetailViewController : ParentViewController

@property (nonatomic, strong) Exercise *exercise;
@property (nonatomic, strong) RNBlurModalView *modalView;

@end
