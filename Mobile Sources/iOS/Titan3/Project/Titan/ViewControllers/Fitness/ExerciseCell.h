//
//  ExerciseCell.h
//  Titan
//
//  Created by Manuel Manzanera on 20/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagView.h"

@interface ExerciseCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, strong) UIButton *actionButton;
@property (nonatomic, strong) UILabel *infoLabel;

@property (nonatomic, strong) UIImageView *exerciseImageView;

@property (nonatomic, assign) NSInteger index;

@property (nonatomic, strong) UIImageView *checkImageView;

@end
