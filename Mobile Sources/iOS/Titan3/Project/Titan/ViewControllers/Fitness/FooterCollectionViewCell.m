//
//  FooterCollectionViewCell.m
//  Titan
//
//  Created by Manuel Manzanera on 5/6/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FooterCollectionViewCell.h"

@implementation FooterCollectionViewCell

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
    }
    
    return self;
}

@end
