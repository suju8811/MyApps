//
//  SeriesViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 14/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "TrainingExercise.h"
#import "Exercise.h"
#import "WorkoutDetailViewController.h"

@protocol SeriesViewControllerDelegate;

@interface SeriesViewController : ParentViewController

@property (nonatomic, strong) TrainingExercise *currentExercise;
@property (nonatomic, assign) id<SeriesViewControllerDelegate> delegate;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) Exercise *exercise;
@property (nonatomic, assign) BOOL performTraining;
@property (nonatomic, strong) id<WorkoutDetailViewControllerDelegate> workoutDetailDelegate;

@end

@protocol SeriesViewControllerDelegate <NSObject>

- (void)backCurrentSeries:(NSMutableArray *)series inIndex:(NSInteger)index;
- (void)backCurrentSeries:(NSMutableArray *)series andSSeries:(NSMutableArray *)sseries inIndex:(NSInteger)index;
- (void)didCompleteExecute:(NSInteger)index;

@end
