//
//  SeriesInputView.m
//  Titan
//
//  Created by Manuel Manzanera on 30/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SeriesInputView.h"

#import "UtilManager.h"
#import "AppConstants.h"
#import "AppContext.h"

@interface SeriesInputView ()<UITextFieldDelegate>

@end

@implementation SeriesInputView

- (id)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if(self){

        [self setBackgroundColor:BLACK_HEADER_APP_COLOR];
        
        UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], 0., frame.size.width - [UtilManager width:20], [UtilManager height:60])];
        [infoLabel setText:NSLocalizedString(@"Introduzca los datos para la serie", nil)];
        [infoLabel setTextAlignment:NSTextAlignmentCenter];
        [infoLabel setTextColor:[UIColor whiteColor]];
        [infoLabel setFont:[UIFont fontWithName:BOLD_FONT size:16]];
        [infoLabel setNumberOfLines:2];
        
        [self addSubview:infoLabel];
        
        _restTextField = [[UITextField alloc] initWithFrame:CGRectMake(infoLabel.frame.origin.x, infoLabel.frame.origin.y + infoLabel.frame.size.height, infoLabel.frame.size.width, UITEXTFIELD_HEIGHT)];
        [_restTextField setTextColor:[UIColor whiteColor]];
        [_restTextField setKeyboardType:UIKeyboardTypeNumberPad];
        CALayer *border = [CALayer layer];
        CGFloat borderWidth = 1;
        border.borderColor = [UIColor whiteColor].CGColor;
        border.frame = CGRectMake(0, _restTextField.frame.size.height - borderWidth, _restTextField.frame.size.width, _restTextField.frame.size.height);
        border.borderWidth = borderWidth;
        [_restTextField.layer addSublayer:border];
        _restTextField.layer.masksToBounds = YES;
        [_restTextField setDelegate:self];
        _restTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Rest", nil)
         attributes:@{
                      NSForegroundColorAttributeName:[UIColor whiteColor],
                      NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]
         }
         ];
        [_restTextField setTextAlignment:NSTextAlignmentCenter];
        
        [self addSubview:_restTextField];
        
        _weightTextField = [[UITextField alloc] initWithFrame:CGRectMake(_restTextField.frame.origin.x, _restTextField.frame.origin.y + _restTextField.frame.size.height + [UtilManager height:4], infoLabel.frame.size.width, UITEXTFIELD_HEIGHT)];
        [_weightTextField setTextColor:[UIColor whiteColor]];
        [_weightTextField setKeyboardType:UIKeyboardTypeNumberPad];
        [_weightTextField setDelegate:self];
        CALayer *borderWeight = [CALayer layer];
        borderWeight.borderColor = [UIColor whiteColor].CGColor;
        borderWeight.frame = CGRectMake(0, _weightTextField.frame.size.height - borderWidth, _weightTextField.frame.size.width, _weightTextField.frame.size.height);
        borderWeight.borderWidth = borderWidth;
        [_weightTextField.layer addSublayer:borderWeight];
        _weightTextField.layer.masksToBounds = YES;
        _weightTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Peso", nil)
                                                                               attributes:@{
                                                                                            NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                                            NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]
                                                                                            }
                                                ];
        [_weightTextField setTextAlignment:NSTextAlignmentCenter];
        
        [self addSubview:_weightTextField];
        
        _repsTextField = [[UITextField alloc] initWithFrame:CGRectMake(_weightTextField.frame.origin.x, _weightTextField.frame.origin.y + _weightTextField.frame.size.height + [UtilManager height:4], infoLabel.frame.size.width, UITEXTFIELD_HEIGHT)];
        [_repsTextField setTextColor:[UIColor whiteColor]];
        [_repsTextField setKeyboardType:UIKeyboardTypeNumberPad];
        [_repsTextField setDelegate:self];
        [_repsTextField setTextColor:[UIColor whiteColor]];
        [_repsTextField setKeyboardType:UIKeyboardTypeNumberPad];
        [_repsTextField setDelegate:self];
        CALayer *borderReps = [CALayer layer];
        borderReps.borderColor = [UIColor whiteColor].CGColor;
        borderReps.frame = CGRectMake(0, _weightTextField.frame.size.height - borderWidth, _weightTextField.frame.size.width, _weightTextField.frame.size.height);
        borderReps.borderWidth = borderWidth;
        [_repsTextField.layer addSublayer:borderReps];
        _repsTextField.layer.masksToBounds = YES;
        _repsTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Repeticiones", nil)
                                                                                 attributes:@{
                                                                                              NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                                              NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]
                                                                                              }
                                                  ];
        [_repsTextField setTextAlignment:NSTextAlignmentCenter];
        
        [self addSubview:_repsTextField];
        
        
        UIButton *acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(0., _repsTextField.frame.origin.y + _repsTextField.frame.size.height + [UtilManager height:8], [UtilManager width:100], [UtilManager height:30])];
        [acceptButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
        [acceptButton addTarget:self action:@selector(acceptAction) forControlEvents:UIControlEventTouchUpInside];
        [acceptButton setCenter:CGPointMake(self.center.x, acceptButton.center.y)];
        
        [self addSubview:acceptButton];
    }
    
    return self;
}

- (void)acceptAction{
    if([self checkFields])
        [_delegate getSeriesData:[self serieData]];
}

- (NSDictionary *)serieData{
    
    NSDictionary *serieDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:_restTextField.text.integerValue],@"rest",[NSNumber numberWithInteger:_weightTextField.text.integerValue],@"weight",[NSNumber numberWithInteger:_repsTextField.text.integerValue],@"reps",[NSNumber numberWithInteger:_index],@"index", nil];
    
    return serieDictionary;
}

- (BOOL)checkFields{
    
    if(_restTextField.text.length == 0 || _weightTextField.text.length == 0 || _repsTextField.text.length == 0){
        [UtilManager presentAlertWithMessage:NSLocalizedString(@"Rellene todos los cmapos", nil)];
        return FALSE;
    }
    
    
    return TRUE;
}

#pragma mark UITextFieldDelegate Methods

@end
