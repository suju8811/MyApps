//
//  FitnessPlanCell.m
//  Titan
//
//  Created by Manuel Manzanera on 7/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FitnessPlanCell.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "UtilManager.h"

@implementation FitnessPlanCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        _containerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20], [UtilManager height:10], WIDTH - [UtilManager width:40], [UtilManager height:130])];
        
        UIView *containerBackgroundView = [[UIView alloc] initWithFrame:_containerView.frame];
        [containerBackgroundView setBackgroundColor:[UIColor blackColor]];
        [containerBackgroundView setAlpha:0.85];
        
        //[[self contentView] addSubview:containerBackgroundView];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:_containerView.frame];
        [imageView setImage:[UIImage imageNamed:@"img-workout-cell"]];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        
        [[self contentView] addSubview:imageView];
        
        //[_containerView setBackgroundColor:[UIColor clearColor]];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:25], [UtilManager height:80], [UtilManager width:300], [UtilManager height:15])];
        [_titleLabel setText:@"Sesión inicial nutrición"];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setTextColor:YELLOW_APP_COLOR];
        [_titleLabel setFont:[UIFont fontWithName:BOLD_FONT size:12]];
        
        [[self contentView] addSubview:_titleLabel];
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.size.height + _titleLabel.frame.origin.y, _titleLabel.frame.size.width, TAGVIEW_SMALL_HEIGHT)];
        [_tagView setMode:TagsControlModeList];
        [_tagView setUserInteractionEnabled:FALSE];
        
        [[self contentView] addSubview:_tagView];
        
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x,_tagView.frame.origin.y + _tagView.frame.size.height, _titleLabel.frame.size.width, [UtilManager height:25])];
        [_contentLabel setNumberOfLines:1];
        [_contentLabel setAdjustsFontSizeToFitWidth:YES];
        [_contentLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:11]];
        [_contentLabel setText:@"Lorem ipsum dolor sit amet consecaquat at sidspld cdsvdoigds   pdogiopdisg podigopdig posdig psdogids pogik ñlfkgñlfk g..."];
        [_contentLabel setTextColor:[UIColor whiteColor]];
        
        [[self contentView] addSubview:_contentLabel];
        
        UIImageView *calendarImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-birth"]];
        [calendarImageView setFrame:CGRectMake(calendarImageView.frame.origin.x, calendarImageView.frame.origin.y, calendarImageView.frame.size.width * 0.8, calendarImageView.frame.size.height * 0.8)];
        [calendarImageView setCenter:CGPointMake(self.contentView.center.x, _contentLabel.frame.origin.y + _contentLabel.frame.size.height)];
        
        [[self contentView] addSubview:calendarImageView];
        
        _exercisesLabel = [[UILabel alloc] initWithFrame:CGRectMake(_contentLabel.frame.origin.x,calendarImageView.frame.origin.y , [UtilManager width:140], [UtilManager height:20])];
        [_exercisesLabel setText:@"5 Entrenamientos"];
        [_exercisesLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:9]];
        [_exercisesLabel setTextColor:GRAY_REGISTER_FONT];
        
        [[self contentView] addSubview:_exercisesLabel];
        
        _daysLabel = [[UILabel alloc] initWithFrame:CGRectMake(calendarImageView.frame.origin.x + calendarImageView.frame.size.width + [UtilManager width:5], calendarImageView.frame.origin.y, [UtilManager width:200], [UtilManager height:20])];
        [_daysLabel setText:@"21 días"];
        [_daysLabel setFont:_exercisesLabel.font];
        [_daysLabel setTextColor:_exercisesLabel.textColor];
        
        [[self contentView] addSubview:_daysLabel];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _daysLabel.frame.origin.y + _daysLabel.frame.size.height, _titleLabel.frame.size.width, 1)];
        [_separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        
        UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(self.contentView.frame.size.width, [UtilManager height:10], [UtilManager width:35], [UtilManager width:35])];
        [menuButton addTarget:self action:@selector(menuAction) forControlEvents:UIControlEventTouchUpInside];
        [menuButton setImage:[UIImage imageNamed:@"btn-options"] forState:UIControlStateNormal];
        
        [[self contentView] addSubview:menuButton];
        
        //[[self contentView] addSubview:_separatorView];
        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
        [panGesture setEnabled:TRUE];
        
        _panView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH * 0.3, self.contentView.frame.size.height)];
        [_panView setBackgroundColor:[UIColor clearColor]];
        [_panView setUserInteractionEnabled:TRUE];
        [_panView addGestureRecognizer:panGesture];
        
        [[self contentView] addSubview:_panView];
    }
    
    return self;
}

- (void)menuAction{
    [_delegate optionsDidPushInIndex:_currentIndex];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)panGestureAction:(UIPanGestureRecognizer *)recognizer{
    
    //CGPoint velocity = [recognizer velocityInView:self];
    CGPoint translation = [recognizer translationInView:_panView];
    if(translation.x > 60)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTIF_MENU_SLIDE" object:nil];
    }
}

@end
