//
//  TrainingResultViewController.m
//  Titan
//
//  Created by Marcus Lee on 27/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingResultViewController.h"
#import "TrainingResultExecutive.h"
#import "Tag.h"
#import "TagView.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "Fitness.h"

@interface TrainingResultViewController () <TrainingResultDisplay>

@property (nonatomic, strong) TagView * tagView;
@property (nonatomic, strong) TrainingResultExecutive * executive;

@end

@implementation TrainingResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _executive = [[TrainingResultExecutive alloc] initWithDisplay:self];
    [_executive displayDidLoad];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    _tagView = [[TagView alloc] initWithFrame:CGRectMake(0, 0, _tagContainerView.frame.size.width, TAGVIEW_SMALL_HEIGHT)];
    [_tagView reloadTagSubviews];

    _tagViewHeightConstraint.constant = CGRectGetHeight(_tagView.bounds);

    [self.view layoutIfNeeded];
}

#pragma mark - Actions

- (IBAction)didPressShare:(id)sender {
}

- (IBAction)didPressClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TrainingResultDisplay

- (void)loadDisplay {
    
    //
    // Show status
    //
    
    TrainingStatus trainingStatus = [Fitness trainingStatus:_training];
    if (trainingStatus == TrainingStatusCompleted) {
        _noteLabel.text = @"Enhorabuena! Otro entrenamiento más!";
    }
    else {
        _noteLabel.text = @"Qué ha pasado?";
    }
    
    //
    // Show title and tag
    //
    
    _titleLabel.text = _training.title;
    
    _tagView = [[TagView alloc] initWithFrame:CGRectMake(0, 0, _tagContainerView.frame.size.width, TAGVIEW_SMALL_HEIGHT)];
    [_tagView setMode:TagsControlModeList];
    [_tagView setUserInteractionEnabled:NO];
    [_tagContainerView addSubview:_tagView];
    
    NSMutableArray *tags = [[NSMutableArray alloc] init];
    
    for(Tag *tagsExercise in [Training getTrainingsExercisesTags:_training]){
        [tags addObject:tagsExercise];
    }
    
    _tagView.tags = tags;
    [_tagView reloadTagSubviews];
    
    //
    // Show other information
    //
    
    _exercisesLabel.text = [NSString stringWithFormat:@"%d Ejercicios", (int)_training.trainingExercises.count];
    
    int secs = 0;
    NSInteger kCals = 0;
    
    for(TrainingExercise *exercise in _training.trainingExercises){
        secs = [TrainingExercise getExerciseTime:exercise] + secs;
        for(Serie *serie in exercise.series){
            kCals = kCals + [Serie kCalInSerie:serie];
        }
    }
    
    int minutes = secs / 60;
    
    if (minutes<60) {
        _durationLabel.text = [NSString stringWithFormat:@"%d min",minutes];
    }
    else {
        int hour = minutes / 60;
        minutes = minutes % 60;
        
        _durationLabel.text = [NSString stringWithFormat:@"%d h %d min",hour,minutes];
    }
    
    _kCalLabel.text = [NSString stringWithFormat:@"%ld kCal",(long)kCals];
}

@end
