//
//  TrainingResultViewController.h
//  Titan
//
//  Created by Marcus Lee on 27/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CommonViewController.h"
#import "Training.h"

@interface TrainingResultViewController : CommonViewController

//
// Variables
//

@property (nonatomic, strong) Training * training;

//
// Outlets
//

@property (weak, nonatomic) IBOutlet UILabel *noteLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *tagContainerView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tagViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *exercisesLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *kCalLabel;

@end
