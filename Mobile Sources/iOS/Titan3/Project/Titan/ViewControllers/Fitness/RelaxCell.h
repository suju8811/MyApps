//
//  RelaxCell.h
//  Titan
//
//  Created by Manuel Manzanera on 26/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "CSCell.h"
//#import "CADRACSwippableCell.h"

@interface RelaxCell : UITableViewCell

@property (nonatomic, strong) UILabel *dayLabel;
@property (nonatomic, strong) UIImageView *circleImageView;
@property (nonatomic, strong) UIView *verticalSeparatorView;

@property (nonatomic, strong) UILabel *titleRelaxLabel;
@property (nonatomic, strong) UIView *separatorRelaxView;

@end
