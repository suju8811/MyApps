//
//  TrainingDetailViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 14/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "Training.h"
#import "TrainingPlan.h"
#import "TrainingDay.h"
#import "TrainingDayContent.h"
#import "FitnessViewController.h"

@protocol WorkoutDetailViewControllerDelegate <NSObject>

- (void)didUpdatedTraining:(Training*)training;

- (void)didPauseExecutingExercise:(NSInteger)executingIndex;

- (void)didCompleteExecutingExercise:(NSInteger)executingIndex;

@end

@interface WorkoutDetailViewController : ParentViewController

@property (nonatomic, strong) TrainingPlan *currentTrainingPlan;
@property (nonatomic, strong) Training *currentTraining;
@property (nonatomic, strong) TrainingDayContent *trainingDayContent;
@property (nonatomic, strong) TrainingDay *currentTrainingDay;
@property (nonatomic, strong) FitnessViewController * fitnessViewController;
@property (nonatomic, assign) NSInteger selectedExerciseIndex;
@property (nonatomic, assign) BOOL fromCalendar;

@end
