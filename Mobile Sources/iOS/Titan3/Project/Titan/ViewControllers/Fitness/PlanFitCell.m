//
//  PlanFitCell.m
//  Titan
//
//  Created by Manuel Manzanera on 24/4/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "PlanFitCell.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "UtilManager.h"

@implementation PlanFitCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        _daysLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:10], [UtilManager width:40], [UtilManager height:30])];
        [_daysLabel setText:@"Día 1"];
        [_daysLabel setTextColor:YELLOW_APP_COLOR];
        [_daysLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:15]];
        
        [[self contentView] addSubview:_daysLabel];
        
        _circleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_daysLabel.frame.origin.x + _daysLabel.frame.size.width, _daysLabel.frame.origin.y, _daysLabel.frame.size.height, _daysLabel.frame.size.width)];
        [[_circleImageView layer] setBorderColor:YELLOW_APP_COLOR.CGColor];
        [[_circleImageView layer] setBorderWidth:8];
        [[_circleImageView layer] setCornerRadius:_daysLabel.frame.size.height/2];
        
        [[self contentView] addSubview:_circleImageView];
        
        _verticalSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, [UtilManager height:60])];
        [_verticalSeparatorView setBackgroundColor:[UIColor grayColor]];
        [_verticalSeparatorView setCenter:CGPointMake(_circleImageView.center.x, _circleImageView.frame.origin.y + _circleImageView.frame.size.height)];
        
        [[self contentView] addSubview:_verticalSeparatorView];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_circleImageView.frame.origin.x + _circleImageView.frame.size.width + [UtilManager width:20], _daysLabel.frame.origin.y, [UtilManager width:240], [UtilManager height:20])];
        [_titleLabel setText:@"Sesión inicial nutrición"];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setTextColor:YELLOW_APP_COLOR];
        [_titleLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        
        [[self contentView] addSubview:_titleLabel];
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.size.height + _titleLabel.frame.origin.y, _titleLabel.frame.size.width, TAGVIEW_SMALL_HEIGHT)];
        [_tagView setMode:TagsControlModeList];
        
        [[self contentView] addSubview:_tagView];
        
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x,_tagView.frame.origin.y + _tagView.frame.size.height, _titleLabel.frame.size.width, [UtilManager height:25])];
        [_contentLabel setTextColor:[UIColor whiteColor]];
        [_contentLabel setNumberOfLines:1];
        [_contentLabel setAdjustsFontSizeToFitWidth:YES];
        [_contentLabel setText:@"Lorem ipsum dolor sit amet consecaquat at sidspld cdsvdoigds   pdogiopdisg podigopdig posdig psdogids pogik ñlfkgñlfk g..."];
        
        [[self contentView] addSubview:_contentLabel];
        
        UIImageView *calendarImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-birth"]];
        [calendarImageView setCenter:CGPointMake(self.contentView.center.x, _contentLabel.frame.origin.y + _contentLabel.frame.size.height + [UtilManager height:2])];
        
        [[self contentView] addSubview:calendarImageView];
        
        _exercisesLabel = [[UILabel alloc] initWithFrame:CGRectMake(_contentLabel.frame.origin.x,calendarImageView.frame.origin.y , [UtilManager width:140], [UtilManager height:20])];
        [_exercisesLabel setText:@"5 Ejercicios"];
        [_exercisesLabel setTextColor:GRAY_REGISTER_FONT];
        
        //[[self contentView] addSubview:_separatorView];

    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
