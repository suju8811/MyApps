//
//  TrainingExerciseCell.h
//  Titan
//
//  Created by Manuel Manzanera on 23/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagView.h"


@protocol WorkoutDetailCelldelegate <NSObject>

- (void)optionsExerciseDidPushInIndex:(NSInteger)index;

@end

@interface WorkoutCell : UITableViewCell

@property (nonatomic, strong) UIImageView *workoutImageView;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) UILabel *seriesLabel;
@property (nonatomic, strong) UILabel *energyLabel;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIImageView *clockImageView;
@property (nonatomic, strong) UIImageView *kCalImageView;

@property (nonatomic, strong) UIButton *menuButton;
@property (nonatomic, strong) UIImageView *checkImageView;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, assign) id<WorkoutDetailCelldelegate>delegate;
@property (nonatomic, strong) UIView *panView;

@property (nonatomic, strong) UIView *separatorView;

@property (nonatomic, assign) NSInteger exerciseStatus;

@end
