//
//  SeriesCell.h
//  Titan
//
//  Created by Manuel Manzanera on 20/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainingExercise.h"

@protocol SeriesCellDelegate;

@interface SeriesCell : UITableViewCell

@property (nonatomic, strong) UIView *verticalSeparator;

@property (nonatomic, strong) UIImageView *descImageView;
@property (nonatomic, strong) UIButton * executeButton;

@property (nonatomic, strong) UILabel *indexLabel;
@property (nonatomic, strong) UITextField *repTextField;
@property (nonatomic, strong) UITextField *weigthTextField;
@property (nonatomic, strong) UITextField *descTextField;

@property (nonatomic, strong) UILabel *repLabel;
@property (nonatomic, strong) UILabel *weigthLabel;
@property (nonatomic, strong) UILabel *descLabel;

@property (nonatomic, strong) UILabel * pauseStateCounterLabel;

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) id<SeriesCellDelegate>delegate;

@property (nonatomic, assign) BOOL isSuperSerie;

@property (nonatomic, strong) UITextField *repSSTextField;
@property (nonatomic, strong) UITextField *weigthSSTextField;
@property (nonatomic, strong) UILabel *repSSLabel;
@property (nonatomic, strong) UILabel *weigthSSLabel;

@property (nonatomic, strong) TrainingExercise *trainingExercise;
@property (nonatomic, strong) Serie *secondExerciseSerie;
@property (nonatomic, assign) BOOL canExecute;

- (void)setContentWithSerie:(Serie*)serie;

@end

@protocol SeriesCellDelegate <NSObject>

- (void)seriesData:(NSDictionary *)seriesDictinary indIndex:(NSInteger)currentIndex;
- (void)textBeginShowInIndex:(NSInteger)currentIndex;
- (void)textEndShowInIndex:(NSInteger)currentIndex;
- (void)timeDidPushAtIndex:(NSInteger)index andColumns:(NSInteger)column;
- (void)seriesCell:(SeriesCell*)cell didPressStopButton:(UIButton*)sender index:(NSInteger)index;

@end
