//
//  SegmentedCollectionViewCell.h
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SegmentedCollectionViewCell : UICollectionViewCell
{

}

@property (nonatomic, strong) UIView *selectedView;
@property (nonatomic, strong) UIImageView *iconImageView;

- (void) configCellWithData:(id)data;
- (void) setCellSelected:(BOOL)selected;

@end
