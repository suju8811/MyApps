//
//  ExerciseDetailViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 27/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ExerciseDetailViewController.h"
#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"

#import "Tag.h"
#import "TagView.h"

@interface ExerciseDetailViewController ()

@property (nonatomic, strong) UITextView *contentLabel;

@end

@implementation ExerciseDetailViewController

- (void)loadView{
    UIView *loadView = [[UIView alloc] initWithFrame:CGRectMake(0., [UtilManager height:25], WIDTH, HEIGHT - [UtilManager height:50])];
    
    [self setView:loadView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
    
    [[self view] setBackgroundColor:BLACK_APP_COLOR];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [UtilManager height:70], WIDTH, [UtilManager height:30])];
    [titleLabel setText:_exercise.title];
    [titleLabel setFont:[UIFont fontWithName:BOLD_FONT size:21]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    [[self view] addSubview:titleLabel];
    
    TagView *tagView = [[TagView alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.size.height + titleLabel.frame.origin.y + [UtilManager height:10], titleLabel.frame.size.width, TAGVIEW_SMALL_HEIGHT)];
    [tagView setCenter:CGPointMake(self.view.center.x, tagView.center.y)];
    [tagView setMode:TagsControlModeList];
    
    NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
    
    if(_exercise.tag1 && _exercise.tag1.length > 0){
        Tag *tag1 = [Tag tagWithId:_exercise.tag1];
        [tags addObject:tag1];
    }
    if(_exercise.tag2 && _exercise.tag2.length > 0){
        Tag *tag2 = [Tag tagWithId:_exercise.tag2];
        [tags addObject:tag2];
    }
    if(_exercise.tag3 && _exercise.tag3.length > 0){
        Tag *tag3 = [Tag tagWithId:_exercise.tag3];
        [tags addObject:tag3];
    }
    
    [tagView setTags:tags];
    [tagView setMode:TagsControlModeList];
    [tagView setIsCenter:TRUE];
    [tagView reloadTagSubviews];
    
    [[self view] addSubview:tagView];
    
    UIImageView *clockImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:40], tagView.frame.origin.y + tagView.frame.size.height + [UtilManager height:8], [UtilManager width:25], [UtilManager width:25])];
    [clockImageView setBackgroundColor:[UIColor yellowColor]];
    
    //[[self view] addSubview:clockImageView];
    
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.center.x - [UtilManager width:110], clockImageView.frame.origin.y, [UtilManager width:100], clockImageView.frame.size.height)];
    [timeLabel setText:@"15 min"];
    [timeLabel setTextColor:GRAY_REGISTER_FONT];
    [timeLabel setTextAlignment:NSTextAlignmentLeft];
    
    //[[self view] addSubview:timeLabel];
    
    UIImageView *kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(timeLabel.frame.origin.x + timeLabel.frame.size.width, timeLabel.frame.origin.y, [UtilManager width:20], [UtilManager width:20])];
    [kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-red"]];
    [kCalImageView setCenter:CGPointMake(self.view.center.x, kCalImageView.center.y)];
    
    //[[self view] addSubview:kCalImageView];
    
    UILabel *kCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(kCalImageView.frame.origin.x + kCalImageView.frame.size.width + 5, timeLabel.frame.origin.y, timeLabel.frame.size.width, timeLabel.frame.size.height)];
    [kCalLabel setTextColor:RED_APP_COLOR];
    [kCalLabel setText:@"3780 kCal"];
    [kCalLabel setText:[NSString stringWithFormat:@"%d kCal",_exercise.energy.intValue]];
    
    //[[self view] addSubview:kCalLabel];
    
    FLAnimatedImageView *animatedImageView = [[FLAnimatedImageView alloc] initWithFrame:CGRectMake([UtilManager width:10],kCalLabel.frame.origin.y + kCalLabel.frame.size.height + [UtilManager width:5], WIDTH - [UtilManager width:20], [UtilManager height:280])];
    
    animatedImageView.contentMode = UIViewContentModeScaleAspectFit;
    animatedImageView.clipsToBounds = YES;
    
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"exercise" withExtension:@"gif"];
    NSData *data1 = [NSData dataWithContentsOfURL:url1];
    animatedImageView.animatedImage = [FLAnimatedImage animatedImageWithGIFData:data1];
    
    [self.view addSubview:animatedImageView];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(animatedImageView.frame.origin.x, animatedImageView.frame.origin.y + animatedImageView.frame.size.height + [UtilManager height:5],animatedImageView.frame.size.width,1)];
    [separatorView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self.view addSubview:separatorView];
    
    CGFloat contentHeoght = [UtilManager heightForText:_exercise.content havingWidth:animatedImageView.frame.size.width andFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    
    _contentLabel = [[UITextView alloc] initWithFrame:CGRectMake(animatedImageView.frame.origin.x, separatorView.frame.origin.y + [UtilManager height:10], animatedImageView.frame.size.width, contentHeoght)];
    //[_contentLabel setNumberOfLines:0];
    [_contentLabel setText:_exercise.content];
    [_contentLabel setTextColor:[UIColor whiteColor]];
    [_contentLabel setFont:[UIFont fontWithName:LIGHT_FONT_OPENSANS size:16]];
    [_contentLabel setTextAlignment:NSTextAlignmentJustified];
    
    [self.view addSubview:_contentLabel];
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50], [UtilManager height:10], [UtilManager width:40], [UtilManager width:40])];
    [okButton setTitle:@"OK" forState:UIControlStateNormal];
    [okButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(okAction) forControlEvents:UIControlEventTouchUpInside];
    
    //[[self view] addSubview:okButton];
}

- (void)okAction{
    //[self dismissViewControllerAnimated:YES completion:^{
    
    //}];
    
    [_modalView didPressOKButton];
}

@end
