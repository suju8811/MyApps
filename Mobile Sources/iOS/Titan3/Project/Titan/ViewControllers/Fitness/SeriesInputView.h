//
//  SeriesInputView.h
//  Titan
//
//  Created by Manuel Manzanera on 30/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SeriesInputViewDelegate <NSObject>

- (void)getSeriesData:(NSDictionary *)seriesData;

@end

@interface SeriesInputView : UIView

@property (nonatomic, assign) id<SeriesInputViewDelegate>delegate;

@property (nonatomic, strong) UITextField *restTextField;
@property (nonatomic, strong) UITextField *weightTextField;
@property (nonatomic, strong) UITextField *intensityFactorTextField;
@property (nonatomic, strong) UITextField *repsTextField;

@property (nonatomic, assign) NSInteger index;

@end
