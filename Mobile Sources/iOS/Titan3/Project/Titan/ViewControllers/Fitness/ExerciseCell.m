//
//  ExerciseCell.m
//  Titan
//
//  Created by Manuel Manzanera on 20/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ExerciseCell.h"
#import "UtilManager.h"
#import "AppConstants.h"
#import "AppContext.h"

@implementation ExerciseCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        _exerciseImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:10], [UtilManager width:50], [UtilManager width:50])];
        [_exerciseImageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
        [_exerciseImageView setContentMode:UIViewContentModeScaleAspectFit];
        //[[_exerciseImageView layer] setBorderWidth:1];
        //[[_exerciseImageView layer] setBorderColor:YELLOW_APP_COLOR.CGColor];
        //[[_exerciseImageView layer] setCornerRadius:[UtilManager width:25]];
        
        [[self contentView] addSubview:_exerciseImageView];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_exerciseImageView.frame.origin.x + _exerciseImageView.frame.size.height + [UtilManager width:10], [UtilManager height:10], [UtilManager width:300], [UtilManager height:20])];
        [_titleLabel setText:@"Sesión inicial nutrición"];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setTextColor:YELLOW_APP_COLOR];
        
        [[self contentView] addSubview:_titleLabel];
        
        _actionButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:45], _titleLabel.frame.origin.y, [UtilManager width:40], [UtilManager width:40])];
        
        [[self contentView] addSubview:_actionButton];
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.size.height + _titleLabel.frame.origin.y + [UtilManager height:15], WIDTH, TAGVIEW_SMALL_HEIGHT)];
        [_tagView setMode:TagsControlModeList];
        [_tagView setUserInteractionEnabled:FALSE];
        
        [[self contentView] addSubview:_tagView];
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, [UtilManager height:79], _titleLabel.frame.size.width, 1)];
        [_separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        [_separatorView setAlpha:0.2];
        
        [[self contentView] addSubview:_separatorView];
        
        _checkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:25], 0, [UtilManager width:18], [UtilManager width:18])];
        [_checkImageView setImage:[UIImage imageNamed:@"img-check"]];
        [_checkImageView setCenter:CGPointMake(_checkImageView.center.x, exerciseCellHeight/2)];
        [_checkImageView setHidden:true];
        
        [[self contentView] addSubview:_checkImageView];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
