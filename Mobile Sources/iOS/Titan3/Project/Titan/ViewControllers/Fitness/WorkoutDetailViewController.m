//
//  TrainingDetailViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 14/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "WorkoutDetailViewController.h"
#import "SelectorTableViewController.h"
#import "OptionsViewController.h"
#import "WorkoutCell.h"
#import "TimeCell.h"
#import "SeriesViewController.h"
#import "MenuContainerNavController.h"
#import "WebServiceManager.h"
#import "Tag.h"
#import "TagView.h"
#import "CSSectionHeader.h"
#import "WorkoutDetailCell.h"
#import "CSAlwaysOnTopHeader.h"
#import "CSStickyParallaxHeaderViewController.h"
#import "CSStickyHeaderFlowLayout.h"
#import "CSStickyHeaderFlowLayoutAttributes.h"
#import "FooterCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "RNBlurModalView.h"
#import "LXReorderableCollectionViewFlowLayout.h"
#import "MBTwitterScroll.h"
#import "TrainingAddViewController.h"
#import "WorkoutDetailExecutive.h"
#import "InfoPopupViewController.h"
#import "UserSelectorViewController.h"
#import "UserMeta.h"
#import "TrainingResultViewController.h"
#import "UIViewController+Present.h"

@interface WorkoutDetailViewController ()<UITableViewDelegate, UITableViewDataSource, SeriesViewControllerDelegate, SelectorTableViewDelegate, UIGestureRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, CSAlwaysOnTopHeaderDelegate, WorkoutDetailCelldelegate,LXReorderableCollectionViewDataSource, LXReorderableCollectionViewDelegateFlowLayout, OptionsViewControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, MBTwitterScrollDelegate, WorkoutDetailViewControllerDelegate, WorkoutDetailDisplay, UserSelectorViewControllerDelegate>

@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) MBTwitterScroll *trainingTableView;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *exercisesLabel;
@property (nonatomic, strong) UILabel *kCalLabel;
@property (nonatomic, strong) SelectorTableViewController *selectorTableViewController;
@property (nonatomic, assign) BOOL isCreatingSuperSerie;
@property (nonatomic, strong) NSMutableArray *superSerieExcercises;
@property (nonatomic, strong) UIButton *menuButton;
@property (nonatomic, strong) UIButton *backButton;

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) RNBlurModalView *modalView;

@property (nonatomic, strong) UIView *containerPickerView;
@property (nonatomic, strong) UIPickerView *timePickerView;

@property (nonatomic, strong) TrainingExercise *selectedTrainingExercise;

@property (nonatomic, assign) BOOL isWorkoutDelete;
@property (nonatomic, strong) WorkoutDetailExecutive * executive;
@property (nonatomic, strong) InfoPopupViewController * infoPopupViewController;
@property (nonatomic, strong) UserSelectorViewController * userSelectorViewController;

@property (nonatomic, strong) UIDatePicker * startDatePicker;
@property (nonatomic, strong) UIDatePicker * endDatePicker;
@property (nonatomic, strong) UIButton * acceptButton;

@end

@implementation WorkoutDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isCreatingSuperSerie = FALSE;
    _superSerieExcercises = [[NSMutableArray alloc] init];
    
    [self configureView];
    //[self configureCollectionView];
    
    _executive = [[WorkoutDetailExecutive alloc] initWithDisplay:self];
    _executive.executingIndex = 0;
    
    UIStoryboard * customViewStoryboard = [UIStoryboard storyboardWithName:@"CustomView" bundle:nil];
    _infoPopupViewController = [customViewStoryboard instantiateInitialViewController];
}

- (void)configureCollectionView{
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    CSStickyHeaderFlowLayout *flowLayout = [[CSStickyHeaderFlowLayout alloc] init];
    flowLayout.parallaxHeaderReferenceSize = CGSizeMake(WIDTH,NAV_HEIGHT);
    flowLayout.scrollDirection =  UICollectionViewScrollDirectionVertical;
    flowLayout.minimumLineSpacing = [UtilManager height:10];
    flowLayout.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, [UtilManager height:175]);
    flowLayout.parallaxHeaderMinimumReferenceSize = CGSizeMake(self.view.frame.size.width, NAV_HEIGHT);
    flowLayout.parallaxHeaderAlwaysOnTop = YES;
    
    // If we want to disable the sticky header effect
    flowLayout.disableStickyHeaders = YES;
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, HEIGHT) collectionViewLayout:flowLayout];
    
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [_collectionView registerClass:[WorkoutDetailCell class] forCellWithReuseIdentifier:@"cell"];
    [_collectionView registerClass:[TimeCell class] forCellWithReuseIdentifier:@"timeCell"];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    //[self.collectionView registerNib:self.headerNib
    //      forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader
    //             withReuseIdentifier:@"header"];
    [_collectionView registerClass:[CSAlwaysOnTopHeader class] forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader withReuseIdentifier:@"CSStickyHeaderParallaxHeader"];
    [_collectionView registerClass:[FooterCollectionViewCell class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footerView"];
    
    [self.view addSubview:_collectionView];
    
    [self.view addSubview:[self footerView]];
}

- (void)reloadLayout {
    
    CSStickyHeaderFlowLayout *layout = (id)_collectionView.collectionViewLayout;
    
    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]]) {
        layout.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, [UtilManager height:175]);
        layout.parallaxHeaderMinimumReferenceSize = CGSizeMake(self.view.frame.size.width, NAV_HEIGHT);
        layout.itemSize = CGSizeMake(self.view.frame.size.width, layout.itemSize.height);
        layout.parallaxHeaderAlwaysOnTop = YES;
        
        // If we want to disable the sticky header effect
        layout.disableStickyHeaders = YES;
    }
    
}

- (void)configureView {
    
    _trainingTableView = [[MBTwitterScroll alloc] initWithType:MBTableWorkout];
    _trainingTableView.currentTraining = _currentTraining;
    _trainingTableView.fromCalendar = _fromCalendar;
    [_trainingTableView setupView];
    
    _trainingTableView.delegate = self;
    _trainingTableView.tableView.delegate = self;
    _trainingTableView.tableView.dataSource = self;
    _trainingTableView.tableView.bounces = NO;
    [_trainingTableView.tableView registerClass:[WorkoutCell class] forCellReuseIdentifier:@"cell"];
    [_trainingTableView.tableView registerClass:[TimeCell class] forCellReuseIdentifier:@"timeCell"];
    
    [self.view addSubview:_trainingTableView];
    [self.view addSubview:[self footerView]];
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (_isWorkoutDelete) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        if (_selectorTableViewController.presentWorkoutDetailForReplacing) {
            _selectorTableViewController.presentWorkoutDetailForReplacing = NO;
            _selectorTableViewController.presentWorkoutDetail = NO;
            
            Exercise * replacingExercise = [_selectorTableViewController.selectsExercises firstObject];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                [replacingExercise setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
                [_currentTraining.trainingExercises[_selectedExerciseIndex].exercises replaceObjectAtIndex:0 withObject:replacingExercise];
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            
            [_executive updateTraining:_currentTraining];
        }
        if (_selectorTableViewController.presentWorkoutDetail) {
            _selectorTableViewController.presentWorkoutDetailForReplacing = NO;
            _selectorTableViewController.presentWorkoutDetail = NO;
            
            for(Exercise *exerciseBack in _selectorTableViewController.selectsExercises) {
                RLMRealm *realm = [AppContext currentRealm];
                
                [realm transactionWithBlock:^{
                    [exerciseBack setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
                }];
                
                [realm beginWriteTransaction];
                NSError *error;
                [realm commitWriteTransaction:&error];
                if(error){
                    NSLog(@"%@",error.description);
                }
                
                BOOL updateExercise = YES;
                
                for(TrainingExercise *trainingExercise in _currentTraining.trainingExercises){
                    for(Exercise *exercise in trainingExercise.exercises)
                        if([exercise.exerciseId isEqualToString:exerciseBack.exerciseId])
                        {
                            updateExercise = NO;
                            break;
                        }
                }
                
                if(updateExercise){
                    TrainingExercise *trainingExercise = [[TrainingExercise alloc] init];
                    
                    [trainingExercise setOrder:[NSNumber numberWithInteger:_currentTraining.trainingExercises.count]];
                    [trainingExercise.exercises addObject:exerciseBack];
                    [trainingExercise setTitle:exerciseBack.title];
                    [trainingExercise setTrainingId:[UtilManager uuid]];
                    [trainingExercise setOwner:[[[AppContext sharedInstance] currentUser] userId]];
                    
                    RLMRealm *realm = [AppContext currentRealm];
                    
                    [realm transactionWithBlock:^{
                        [_currentTraining.trainingExercises addObject:trainingExercise];
                        [exerciseBack setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
                    }];
                    
                    [realm beginWriteTransaction];
                    NSError *error;
                    [realm commitWriteTransaction:&error];
                    if(error){
                        NSLog(@"%@",error.description);
                    }
                }
            }
            
            [_executive updateTraining:_currentTraining];
            
            [_trainingTableView setCurrentTraining:_currentTraining];
            [_trainingTableView.tableView reloadData];
            [_trainingTableView reloadWorkoutValues];
            
            //[_collectionView reloadData];
            //[_trainingTableView reloadData];
            //[self createTraining];
        }
        [_trainingTableView setCurrentTraining:_currentTraining];
        [_trainingTableView.tableView reloadData];
        [_trainingTableView reloadWorkoutValues];
    }
}

- (UIView *)footerView{
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.,HEIGHT - [UtilManager height:50], WIDTH, [UtilManager height:50])];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = footerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [footerView addSubview:blurEffectView];
    
    UIButton *moreButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40] * 0.8, [UtilManager width:40]* 0.8)];
    [moreButton setImage:[UIImage imageNamed:@"btn-add"] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(presentSelectorTableViewController) forControlEvents:UIControlEventTouchUpInside];
    [moreButton setCenter:CGPointMake(self.view.center.x, footerView.frame.size.height/2)];
    
    [footerView addSubview:moreButton];

    return footerView;
}

- (void)reloadTimeLabel{
    int count = 0;
    NSInteger kCal = 0;
    
    for(TrainingExercise *exercise in _currentTraining.trainingExercises){
        for(Serie *serie in exercise.series){
            count = count + serie.rest.intValue;
            kCal = kCal + [Serie kCalInSerie:serie];
        }
    }
    //if(count / 60 >= 1)
    //{
        int minutes = count / 60;
        //int seconds = count % 60;
        [_timeLabel setText:[NSString stringWithFormat:@"%d min",minutes]];
    //}else
    //    [_timeLabel setText:[NSString stringWithFormat:@"%d seg.",count]];
    
    [_exercisesLabel setText:[NSString stringWithFormat:@"%lu Ejercicios",(unsigned long)_currentTraining.trainingExercises.count]];
    [_kCalLabel setText:[NSString stringWithFormat:@"%ld kCal",(long)kCal]];
}

- (void)didPressBackButton {
    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:0] animated:YES];
}

- (void)menuAction{
    
    if(_isCreatingSuperSerie){
        
        _isCreatingSuperSerie = FALSE;
        
        //[_trainingTableView setAllowsMultipleSelection:FALSE];
        
        if(_superSerieExcercises.count >= 2) {
            RLMRealm *realm = [AppContext currentRealm];
        
            TrainingExercise *trainingSuperSerieExercise = [[TrainingExercise alloc] init];
            
            trainingSuperSerieExercise.order = @(_currentTraining.trainingExercises.count + 1);
            //[trainingExercise.exercises addObject:exercise];
            //[trainingExercise setTitle:exercise.title];
            trainingSuperSerieExercise.owner = [AppContext sharedInstance].authToken;
            trainingSuperSerieExercise.trainingId = [UtilManager uuid];
            
            for (TrainingExercise *trainingExercise in _superSerieExcercises) {
                
                if(trainingSuperSerieExercise.title.length == 0) {
                    [trainingSuperSerieExercise setTitle:[trainingExercise.exercises objectAtIndex:0].title];
                }
                else {
                    [trainingSuperSerieExercise setTitle:[NSString stringWithFormat:@"%@+%@",trainingSuperSerieExercise.title,[trainingExercise.exercises objectAtIndex:0].title]];
                }
                [trainingSuperSerieExercise.exercises addObjects:trainingExercise.exercises];
                
                for (Serie *serie in trainingExercise.series) {
                    Serie *newSerie = [[Serie alloc] init];
                    
                    newSerie.serieId = [UtilManager uuid];
                    newSerie.owner = serie.owner;
                    newSerie.exercise = serie.exercise;
                    newSerie.intensityFactor = serie.intensityFactor;
                    newSerie.reps = serie.reps;
                    newSerie.weight = serie.weight;
                    newSerie.isSubserie = serie.isSubserie;
                    newSerie.order = serie.order;
                    newSerie.rest = serie.rest;
                    newSerie.intensityFactor = serie.intensityFactor;
                    
                    [[trainingSuperSerieExercise series] addObject:newSerie];
                }
            }
            
            [realm transactionWithBlock:^{
                [_currentTraining.trainingExercises removeAllObjects];
                [_currentTraining.trainingExercises addObject:trainingSuperSerieExercise];
            }];
            
            [_executive updateTraining:_currentTraining];
            
            [realm beginWriteTransaction];
            [realm addOrUpdateObject:_currentTraining];
            
            NSError *error;
            [realm commitWriteTransaction:&error];
            
            if (error) {
                NSLog(@"%@",error.description);
            }
            
            [_trainingTableView setCurrentTraining:_currentTraining];
            [_trainingTableView.tableView reloadData];
            [_trainingTableView reloadWorkoutValues];
        
            [_superSerieExcercises removeAllObjects];
        } else{
            [_trainingTableView setCurrentTraining:_currentTraining];
            [_trainingTableView.tableView reloadData];
            [_trainingTableView reloadWorkoutValues];
            
        }
    }
    else {
        _isCreatingSuperSerie = FALSE;
        [_trainingTableView setCurrentTraining:_currentTraining];
        [_trainingTableView.tableView reloadData];
        [_trainingTableView reloadWorkoutValues];
        
        //[self presentOptionViewController];
        /*
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"Seleccione una opción", nil) preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *addExercise = [UIAlertAction actionWithTitle:@"Añadir Ejercicio" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self presentSelectorTableViewController];
        }];
        
        UIAlertAction *addFavourite = [UIAlertAction actionWithTitle:@"Añadir a Favoritos" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                [_currentTraining setValue:[NSNumber numberWithBool:TRUE] forKey:@"isFavourite"];
            }];
            
            [realm beginWriteTransaction];
            [realm addOrUpdateObject:_currentTraining];
            
            NSError *error;
            [realm commitWriteTransaction:&error];
            
            if(error){
                NSLog(@"%@",error.description);
            }
            
        }];
        
        UIAlertAction *superSerieAction = [UIAlertAction actionWithTitle:@"SuperSerie" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            _isCreatingSuperSerie = TRUE;
            [_trainingTableView setAllowsMultipleSelectionDuringEditing:YES];
            [_trainingTableView setAllowsMultipleSelection:YES];
            [_menuButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
        }];
        
        [alertController addAction:addExercise];
        [alertController addAction:addFavourite];
        //[alertController addAction:superSerieAction];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:^{
        
        }];
         */
    }
}

- (void)presentOptionViewController {
    OptionsViewController *optionsViewController = [[OptionsViewController alloc] init];
    [optionsViewController setCurrentTraining:_currentTraining];
    [optionsViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [optionsViewController setDelegate:self];
    [self presentViewController:optionsViewController animated:YES completion:^{
    
    }];
}

- (void)presentSelectorTableViewController {
    _selectorTableViewController = [[SelectorTableViewController alloc] init];
    [_selectorTableViewController setDelegate:self];
    [_selectorTableViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [_selectorTableViewController setSelectTraining:_currentTraining];
    
    [self presentViewController:_selectorTableViewController animated:YES completion:nil];
}

- (void)startEditingIndex
{
    [_trainingTableView.tableView setEditing:YES animated:YES];
}

- (void)deleteAction:(id)sender{
    UITapGestureRecognizer *gesture = (UITapGestureRecognizer *)sender;
    NSLog(@"%@",gesture.accessibilityLabel);
    
    if(_currentTraining.trainingExercises.count > 1){
        TrainingExercise *trainingExercise = [_currentTraining.trainingExercises objectAtIndex:gesture.accessibilityLabel.intValue];
        
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            [_currentTraining.trainingExercises removeObjectAtIndex:gesture.accessibilityLabel.intValue];
        }];
        
        [realm beginWriteTransaction];
        [realm addOrUpdateObject:_currentTraining];
        [realm deleteObject:trainingExercise];
        
        NSError *error;
        [realm commitWriteTransaction:&error];
        
        if(error){
            NSLog(@"%@",error.description);
        }
        
        [_executive updateTraining:_currentTraining];
        
        [_collectionView reloadData];
    } else {
        [self presentViewController:[UtilManager presentAlertWithMessage:@"Un entrenamiento debe tener un ejercicio. Puedo borrar directamente el entrenamiento."] animated:YES completion:^{
            
        }];
    }
}


- (void)hideTimePicker{
    [_containerPickerView removeFromSuperview];
    _containerPickerView = nil;
    
    [_timePickerView  removeFromSuperview];
    _timePickerView = nil;
}

- (void)presentPickerViewAtIndex{
    
    if(_timePickerView)
        [self hideTimePicker];
    
    _containerPickerView = [[UIView alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - [UtilManager height:206], WIDTH, [UtilManager height:206])];
    [_containerPickerView setBackgroundColor:[UIColor clearColor]];
    
    _timePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0., [UtilManager height:44], self.view.frame.size.width, [UtilManager height:162])];
    [_timePickerView setBackgroundColor:BLACK_APP_COLOR];
    
    [_timePickerView setUserInteractionEnabled:YES];
    [_timePickerView setDelegate:self];
    [_timePickerView setDataSource:self];
    _timePickerView.showsSelectionIndicator = YES;
    
    /*
     UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,WIDTH,[UtilManager height:44])];
     [toolBar setBarStyle:UIBarStyleBlackTranslucent];
     UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil)
     style:UIBarButtonItemStyleDone target:self action:@selector(acceptPickerAction)];
     
     [barButtonDone setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
     YELLOW_APP_COLOR, NSForegroundColorAttributeName,nil]
     forState:UIControlStateNormal];
     [barButtonDone setTintColor:YELLOW_APP_COLOR];
     toolBar.items = @[barButtonDone];
     */
    
    UIView *toolBarDescView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:44])];
    [toolBarDescView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIButton *doneDescButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:100],0. , [UtilManager width:90], [UtilManager height:35])];
    [doneDescButton addTarget:self action:@selector(acceptPickerAction) forControlEvents:UIControlEventTouchUpInside];
    [doneDescButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [doneDescButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [toolBarDescView addSubview:doneDescButton];
    
    [_containerPickerView addSubview:_timePickerView];
    [_containerPickerView addSubview:toolBarDescView];
    
    [self.view addSubview:_containerPickerView];
}

- (void)acceptPickerAction{
    NSInteger hour = [_timePickerView selectedRowInComponent:0];
    NSInteger minutes = [_timePickerView selectedRowInComponent:1];
    
    NSString *hourString;
    NSString *minuteString;
    
    if(hour<10)
        hourString = [NSString stringWithFormat:@"0%ld",(long)hour];
    else
        hourString = [NSString stringWithFormat:@"%ld",(long)hour];
    
    if(minutes<10)
        minuteString = [NSString stringWithFormat:@"0%ld",(long)minutes];
    else
        minuteString = [NSString stringWithFormat:@"%ld",(long)minutes];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm transactionWithBlock:^{
        [_trainingDayContent setStart:[NSString stringWithFormat:@"%@:%@",hourString,minuteString]];
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [_trainingTableView.tableView reloadData];
    
    [self hideTimePicker];
}

#pragma mark - OptionsViewController Delegate Methods

- (void)setSuperSerie {
    _isCreatingSuperSerie = YES;
    
    [_trainingTableView.optionButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [_trainingTableView.menuButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    
    _trainingTableView.currentTraining = _currentTraining;
    [_trainingTableView.tableView reloadData];
    [_trainingTableView reloadWorkoutValues];
    
    //[_collectionView reloadData];
}

- (void)removeSuperSerie {
    [Training deleteSuperSerie:_selectedTrainingExercise inTraining:_currentTraining];
    [_executive updateTraining:_currentTraining];
    
    [_trainingTableView setCurrentTraining:_currentTraining];
    [_trainingTableView.tableView reloadData];
    [_trainingTableView reloadWorkoutValues];
}

- (void)editTraining:(Training *)training{
    TrainingAddViewController *trainingAddViewcontroller = [[TrainingAddViewController alloc] init];
    [trainingAddViewcontroller setEditTraining:training];
    [trainingAddViewcontroller setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [trainingAddViewcontroller setWorkoutDetailViewControllerDelegate:self];
    [self presentViewController:trainingAddViewcontroller animated:YES completion:nil];
}

- (void)removeWorkout{
    _isWorkoutDelete = TRUE;
}

- (void)didPressReplace:(RLMObject*)rlmObject {
    if (![rlmObject isKindOfClass:[TrainingExercise class]]) {
        return;
    }
    
    _selectorTableViewController = [[SelectorTableViewController alloc] init];
    _selectorTableViewController.delegate = self;
    _selectorTableViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    _selectorTableViewController.selectTraining = _currentTraining;
    _selectorTableViewController.selectorType = SelectorTypeExerciseSingle;
    
    [self presentViewController:_selectorTableViewController animated:YES completion:nil];
}

- (void)didPressSendFood:(RLMObject *)rlmObject {
    _userSelectorViewController = [[UserSelectorViewController alloc] init];
    _userSelectorViewController.rlmObject = rlmObject;
    _userSelectorViewController.delegate = self;
    _userSelectorViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:_userSelectorViewController animated:YES completion:nil];
}

- (void)didPressShareFood:(RLMObject *)rlmObject {
    [_executive shareContent:rlmObject];
}

- (void)didPressTimePicker:(RLMObject *)rlmObject {
    if (![rlmObject isKindOfClass:[TrainingExercise class]]) {
        return;
    }
    
    TrainingExercise * trainingExercise = (TrainingExercise*)rlmObject;
    
    if(_startDatePicker) {
        [_startDatePicker removeFromSuperview];
        _startDatePicker = nil;
    }
    
    _startDatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - 216, self.view.frame.size.width, 216)];
    [_startDatePicker setDatePickerMode:UIDatePickerModeDate];
    [_startDatePicker setDate:[NSDate dateWithTimeIntervalSinceNow:-60*60*24*365 * 18]];
    [_startDatePicker setMaximumDate:[NSDate dateWithTimeIntervalSinceNow:-60*60*24*365 * 18]];
    [_startDatePicker setMinimumDate:[NSDate dateWithTimeIntervalSinceNow:-60*60*24*365 * 75]];
    [_startDatePicker setBackgroundColor:[UIColor whiteColor]];
    [_startDatePicker setUserInteractionEnabled:YES];
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_startDatePicker.frame.size.width - 80,
                                                               _startDatePicker.frame.origin.y,
                                                               80.,
                                                               24)];
    
    [_acceptButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptDatePickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_startDatePicker];
    [self.view addSubview:_acceptButton];
}

- (void)acceptDatePickerAction {
    if(_startDatePicker) {
        [_startDatePicker removeFromSuperview];
        _startDatePicker = nil;
        [_acceptButton removeFromSuperview];
        _acceptButton = nil;
    }
}

#pragma mark SelectorTableViewDelegate

- (void)returnData:(NSMutableArray *)returnData withType:(SelectorType)selectorType{
    if(returnData.count > 0){
        int order = 0;
        
        for(Exercise *exercise in returnData){
            TrainingExercise *trainingExercise = [[TrainingExercise alloc] init];
            
            [trainingExercise setOrder:[NSNumber numberWithInt:order]];
            [trainingExercise.exercises addObject:exercise];
            [trainingExercise setTitle:exercise.title];
            [trainingExercise setOwner:[[AppContext sharedInstance] userId]];
            
            [_currentTraining.trainingExercises addObject:trainingExercise];
            
            order++;
        }
    }else{
        [_collectionView reloadData];
    }
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //if(_currentTrainingPlan && indexPath.row == 0)
    //{
    //    [self presentPickerViewAtIndex];
    //}else{
    TrainingExercise *trainingExercise;
    
    if (_currentTrainingPlan &&
       indexPath.row == 0) {
        
        [self presentPickerViewAtIndex];
    }
    else {
        if (_currentTrainingPlan) {
            if (indexPath.row != 0) {
                trainingExercise = [_currentTraining.trainingExercises objectAtIndex:indexPath.row - 1];
            }
        }
        else{
            trainingExercise = [_currentTraining.trainingExercises objectAtIndex:indexPath.row];
        }
        
        
        if (_isCreatingSuperSerie){
            if(_superSerieExcercises.count > 2){
                [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"Las superseries son de dos ejercicios.", nil)] animated:YES completion:^{
                    
                }];
            }else {
                RLMRealm *realm = [AppContext currentRealm];
                
                if(trainingExercise.isSelected.boolValue){
                    [realm transactionWithBlock:^{
                        [trainingExercise setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
                        [_superSerieExcercises removeObject:trainingExercise];
                    }];
                }else{
                    [realm transactionWithBlock:^{
                        [trainingExercise setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                        [_superSerieExcercises addObject:trainingExercise];
                    }];
                }
                [_trainingTableView.tableView reloadData];
            }
            
            //[_collectionView reloadData];
        }
        else {
            //if([trainingExercise.owner isEqualToString:[[AppContext sharedInstance] currentUser].userId]){
            
            SeriesViewController *seriesViewController = [[SeriesViewController alloc] init];
            [seriesViewController setCurrentExercise:trainingExercise];
            if (_currentTrainingPlan) {
                [seriesViewController setIndex:indexPath.row - 1];
            }
            else {
                [seriesViewController setIndex:indexPath.row];
            }
            
            [seriesViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [seriesViewController setDelegate:self];
            
            [self presentViewController:seriesViewController animated:YES completion:^{
                
            }];
            //} else {
            
            //    [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"No puedes editar planes de entrenamiento si no eres propietario o no tienes permisos", nil)] animated:TRUE completion:^{
            
            //    }];
            //}
        }
        //}

    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark UITableViewDataSource Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{    
//    if (_currentTrainingPlan) {
//        if (indexPath.row == 0) {
//            TimeCell *cell = [_trainingTableView.tableView dequeueReusableCellWithIdentifier:@"timeCell"];
//
//            if(_trainingDayContent.start) {
//                [[cell timeLabel] setText:_trainingDayContent.start];
//            }
//
//            return cell;
//        }
//    }
    
    
    WorkoutCell *cell = [_trainingTableView.tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (_fromCalendar) {
        if (indexPath.row == _executive.executingIndex) { // Executing
            cell.exerciseStatus = 0;
        }
        else if (indexPath.row < _executive.executingIndex) { // Completed
            cell.exerciseStatus = 1;
        }
        else { // Locked
            cell.exerciseStatus = 2;
        }
    }
    else {
        cell.exerciseStatus = -1;
    }
    
    TrainingExercise *trainingExercise;
//    if(_currentTrainingPlan) {
//        trainingExercise  = [_currentTraining.trainingExercises objectAtIndex:indexPath.row - 1];
//    }
//    else {
        trainingExercise  = [_currentTraining.trainingExercises objectAtIndex:indexPath.row];
//    }
    
    if(_isCreatingSuperSerie) {
        cell.menuButton.hidden = YES;
        
        if(trainingExercise.isSelected.boolValue) {
            cell.checkImageView.hidden = NO;
        }
        else {
            cell.checkImageView.hidden = YES;
        }
    }
    else {
        cell.menuButton.hidden = NO;
        cell.checkImageView.hidden = YES;
    }
    
    if(trainingExercise.title.length > 0) {
        cell.titleLabel.text = trainingExercise.title;
    }
    else {
        Exercise *exercise = [trainingExercise.exercises objectAtIndex:0];
        if(trainingExercise.exercises.count == 1) {
            cell.titleLabel.text = exercise.title;
        }
        else {
            Exercise *secondExercise = [trainingExercise.exercises objectAtIndex:1];
            cell.titleLabel.text = [NSString stringWithFormat:@"%@+%@",exercise.title,secondExercise.title];
        }
        
        NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
        
        if(exercise.tag1 && exercise.tag1.length > 0){
            Tag *tag1 = [Tag tagWithId:exercise.tag1];
            if(tag1) {
                [tags addObject:tag1];
            }
        }
        
        if(exercise.tag2 && exercise.tag2.length > 0) {
            Tag *tag2 = [Tag tagWithId:exercise.tag2];
            if(tag2) {
                [tags addObject:tag2];
            }
        }
        
        if(exercise.tag3 && exercise.tag3.length > 0){
            Tag *tag3 = [Tag tagWithId:exercise.tag3];
            if(tag3) {
                [tags addObject:tag3];
            }
        }
        
        UIImageView *imageView = cell.workoutImageView;
        
        NSURLRequest *exerciseImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_EXERCISES,exercise.picture]]
                                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                          timeoutInterval:60];
        
        [cell.workoutImageView setImageWithURLRequest:exerciseImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-small"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            [imageView setImage:image];
            [imageView setContentMode:UIViewContentModeScaleAspectFill];
            [[imageView layer] setMasksToBounds:YES];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            NSLog(@"%@",error);
            [imageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
        }];
        
        cell.energyLabel.text = [NSString stringWithFormat:@"%d kCal",exercise.energy.intValue];
        cell.tagView.tags = tags;
        [cell.tagView reloadTagSubviews];
    }
    
    NSMutableString *seriesString = [[NSMutableString alloc] init];
    
    NSInteger kCals = 0;
    
    if (trainingExercise.series.count > 0) {
        for(Serie *serie in trainingExercise.series){
            if(seriesString.length == 0) {
                seriesString = [NSMutableString stringWithFormat:@"%d ",serie.reps.intValue];
            }
            else {
                [seriesString appendString:[NSString stringWithFormat:@"- %d ",serie.reps.intValue]];
            }
            
            kCals = kCals + [Serie kCalInSerie:serie];
        }
        
        if(seriesString.length > 0) {
            cell.seriesLabel.text = seriesString;
        }
        
        int secs = [TrainingExercise getExerciseTime:trainingExercise];
        
        if(secs / 60 >= 1) {
            int minutes = secs / 60;
            
            if(minutes<60) {
                [cell.timeLabel setText:[NSString stringWithFormat:@"%d min",minutes]];
            }
            else {
                int hour = minutes/60;
                minutes = minutes/60;
                
                [cell.timeLabel setText:[NSString stringWithFormat:@"%d h %d min",hour,minutes]];
            }
        }
        else {
            [cell.timeLabel setText:[NSString stringWithFormat:@"0 min"]];
        }
    }
    else {
        cell.seriesLabel.text = NSLocalizedString(@"Añadir Series", nil);
        cell.timeLabel.text = [NSString stringWithFormat:@"0 min"];
    }
    
    cell.energyLabel.text = [NSString stringWithFormat:@"%ld kCal",(long)kCals];
    cell.delegate = self;
    
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    if(_currentTrainingPlan) {
//        return _currentTraining.trainingExercises.count + 1;
//    }
    
    return _currentTraining.trainingExercises.count;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_currentTrainingPlan) {
        if (indexPath.row == 0) {
            return  [UtilManager height:80];
        }
    }
    return [UtilManager height:120];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_currentTrainingPlan) {
        if (indexPath.row == 0) {
            return  [UtilManager height:80];
        }
    }
    return [UtilManager height:120];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return TAB_BAR_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return TAB_BAR_HEIGHT;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, TAB_BAR_HEIGHT)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    return footerView;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_currentTrainingPlan && indexPath.row == 0) {
        return NO;
    }
    return YES;
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    if(_currentTrainingPlan && indexPath.row == 0) {
        return NO;
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    if(_currentTrainingPlan && (destinationIndexPath.row == 0 || sourceIndexPath.row == 0 || !destinationIndexPath.row))
    {
        [tableView reloadData];
    }else{
        TrainingExercise *sourceTrainingExercise = [_currentTraining.trainingExercises objectAtIndex:sourceIndexPath.row];
        TrainingExercise *destinationTrainingExercise = [_currentTraining.trainingExercises objectAtIndex:destinationIndexPath.row];
        
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            [sourceTrainingExercise setOrder:[NSNumber numberWithInteger:destinationIndexPath.row]];
            [destinationTrainingExercise setOrder:[NSNumber numberWithInteger:sourceIndexPath.row]];
            
            [_currentTraining.trainingExercises replaceObjectAtIndex:sourceIndexPath.row withObject:destinationTrainingExercise];
            [_currentTraining.trainingExercises replaceObjectAtIndex:destinationIndexPath.row withObject:sourceTrainingExercise];
        }];
        
        [realm beginWriteTransaction];
        [realm addOrUpdateObject:_currentTraining];
        NSError *error;
        [realm commitWriteTransaction:&error];
        
        if(error){
            NSLog(@"%@",error.description);
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(_currentTrainingPlan && indexPath.row == 0) {
        return NO;
    }
    
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_currentTrainingPlan &&
        indexPath.row == 0) {
        
        return NO;
    }
    
    return YES;
}

- (nullable NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(8_0) __TVOS_PROHIBITED {
    
    TrainingExercise *trainingExercise;
    if(_currentTrainingPlan) {
        trainingExercise  = [_currentTraining.trainingExercises objectAtIndex:indexPath.row -1];
    }
    else {
        trainingExercise  = [_currentTraining.trainingExercises objectAtIndex:indexPath.row];
    }
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"Delete", nil)  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [_trainingTableView.tableView setEditing:NO];
        
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            [_currentTraining.trainingExercises removeObjectAtIndex:indexPath.row];
        }];
        
        [realm beginWriteTransaction];
        [realm addOrUpdateObject:_currentTraining];
        [realm deleteObject:trainingExercise];
        
        NSError *error;
        [realm commitWriteTransaction:&error];
        
        if(error){
            NSLog(@"%@",error.description);
        }
        
        [_executive updateTraining:_currentTraining];
        
        [_trainingTableView setCurrentTraining:_currentTraining];
        [_trainingTableView reloadWorkoutValues];
        [_trainingTableView.tableView reloadData];
    }];
    deleteAction.backgroundColor = [UIColor redColor];
    
    NSString * moreActionTitle = @"+";
    if (trainingExercise.exercises.count > 1) {
        moreActionTitle = @"-";
    }
    
    UITableViewRowAction *moreAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(moreActionTitle, nil)  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [_trainingTableView.tableView setEditing:NO];
        if (trainingExercise.exercises.count > 1) {
            [self removeSuperSerie];
        }
        else {
            [self setSuperSerie];
        }
    }];
    moreAction.backgroundColor = [UIColor lightGrayColor];
    if(_isCreatingSuperSerie) {
        return @[deleteAction];
    }
    return @[deleteAction, moreAction];
}

#pragma mark WorkoutCellDelegate Methods

- (void)optionsExerciseDidPushInIndex:(NSInteger)index{
    _selectedExerciseIndex = index;
    
    _selectedTrainingExercise = [[_currentTraining trainingExercises] objectAtIndex:index];
    
    OptionsViewController *optionsViewController = [[OptionsViewController alloc] init];
    optionsViewController.trainingExercise = _selectedTrainingExercise;
    optionsViewController.currentTraining = _currentTraining;
    optionsViewController.optionType = OptionTypeExercise;
    optionsViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsViewController.delegate = self;
    optionsViewController.fromDetailOfParent = YES;
    [self presentViewController:optionsViewController animated:YES completion:nil];
}

#pragma mark UICollectionViewDelegate Methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(_currentTrainingPlan && indexPath.row == 0) {
        [self presentPickerViewAtIndex];
    }
    else {
        TrainingExercise *trainingExercise;
        
        if(_currentTrainingPlan) {
            trainingExercise = [_currentTraining.trainingExercises objectAtIndex:indexPath.row - 1];
        }
        else {
            trainingExercise = [_currentTraining.trainingExercises objectAtIndex:indexPath.row];
        }
        
        if(_isCreatingSuperSerie){
            RLMRealm *realm = [AppContext currentRealm];
            
            if(trainingExercise.isSelected.boolValue){
                [realm transactionWithBlock:^{
                    [trainingExercise setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
                    [_superSerieExcercises removeObject:trainingExercise];
                }];
            }else{
                [realm transactionWithBlock:^{
                    [trainingExercise setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                    [_superSerieExcercises addObject:trainingExercise];
                }];
            }
            [_collectionView reloadData];
        }else{
            if([trainingExercise.owner isEqualToString:[[AppContext sharedInstance] currentUser].userId]){
                
                SeriesViewController *seriesViewController = [[SeriesViewController alloc] init];
                [seriesViewController setCurrentExercise:trainingExercise];
                if(_currentTrainingPlan) {
                    [seriesViewController setIndex:indexPath.row - 1];
                }
                else {
                    [seriesViewController setIndex:indexPath.row];
                }
                [seriesViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                [seriesViewController setDelegate:self];
                
                [self presentViewController:seriesViewController animated:YES completion:^{
                    
                }];
            } else {
                
                [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"No puedes editar planes de entrenamiento si no eres propietario o no tienes permisos", nil)] animated:TRUE completion:^{
                    
                }];
            }
        }
    }
}

#pragma mark UICollectionViewFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(_currentTrainingPlan) {
        if(indexPath.row == 0) {
            return CGSizeMake(WIDTH , [UtilManager height:80]);
        }
    }
    
    return CGSizeMake(WIDTH , [UtilManager height:120]);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake([UtilManager height:0], [UtilManager width:0], [UtilManager height:0], [UtilManager width:0]);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return [UtilManager height:1];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return [UtilManager height:1];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(0.,0.);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeMake(0.,[UtilManager height:50]);
}


#pragma mark UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    //return _currentTraining.trainingExercises.count;
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (_currentTrainingPlan) {
        return _currentTraining.trainingExercises.count + 1;
    }
    
    return _currentTraining.trainingExercises.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_currentTrainingPlan) {
        if(indexPath.row == 0){
            TimeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"timeCell" forIndexPath:indexPath];
            
            if(_trainingDayContent.start)
                [[cell timeLabel] setText:_trainingDayContent.start];
            
            return cell;
        
        }
    }

    WorkoutDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell"
                                                             forIndexPath:indexPath];
    UIView *bottomView = [[UIView alloc] init];
    bottomView.frame = (CGRect){
        .size = CGSizeMake(CGRectGetWidth(collectionView.bounds)/2, CGRectGetHeight(cell.bounds))
    };
    
    //cell.allowedDirection = CADRACSwippableCellAllowedDirectionLeft;
    //cell.revealView = bottomView;
    //[cell setDelegate:self];
    if (_currentTrainingPlan) {
        [cell setCurrentIndex:indexPath.row -1];
    }
    else {
        [cell setCurrentIndex:indexPath.row];
    }
    
    /*
    [[cell.revealViewSignal filter:^BOOL(NSNumber *isRevealed) {
        return [isRevealed boolValue];
    }] subscribeNext:^(id x) {
        [[self.collectionView visibleCells] enumerateObjectsUsingBlock:^(CustomCollectionCell *otherCell, NSUInteger idx, BOOL *stop) {
            if (otherCell != cell)
            {
                [otherCell hideRevealViewAnimated:YES];
            }
        }];
    }];
    */
    bottomView.backgroundColor =  [UIColor redColor];
    [bottomView setTag:indexPath.row];
    
    UILabel *deleteLabel = [[UILabel alloc] initWithFrame:bottomView.bounds];
    [deleteLabel setText:NSLocalizedString(@"Delete", nil)];
    [deleteLabel setTextAlignment:NSTextAlignmentCenter];
    [deleteLabel setTextColor:[UIColor whiteColor]];
    [bottomView addSubview:deleteLabel];
    
    UITapGestureRecognizer *deleteGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteAction:)];
    if (_currentTrainingPlan) {
        [deleteGesture  setAccessibilityLabel:[NSString stringWithFormat:@"%ld",(long)indexPath.row-1]];
    }
    else {
        [deleteGesture  setAccessibilityLabel:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    }
    
    [bottomView setUserInteractionEnabled:YES];
    //[bottomView addGestureRecognizer:deleteGesture];
    
    TrainingExercise *trainingExercise;
    
    if(_currentTrainingPlan) {
        trainingExercise = [_currentTraining.trainingExercises objectAtIndex:indexPath.row -1];
    }
    else {
        trainingExercise = [_currentTraining.trainingExercises objectAtIndex:indexPath.row];
    }
    
    if(_isCreatingSuperSerie) {
        [[cell menuButton] setHidden:YES];
        
        if(trainingExercise.isSelected.boolValue)
            [[cell checkImageView] setHidden:FALSE];
        else
            [[cell checkImageView] setHidden:TRUE];
    }else {
        [[cell menuButton] setHidden:FALSE];
        [[cell checkImageView] setHidden:TRUE];
    }
    
    if(trainingExercise.title.length > 0)
        [[cell titleLabel] setText:trainingExercise.title];
    else {
        Exercise *exercise = [trainingExercise.exercises objectAtIndex:0];
        [[cell titleLabel] setText:exercise.title];
        
        NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
        
        if(exercise.tag1 && exercise.tag1.length > 0){
            Tag *tag1 = [Tag tagWithId:exercise.tag1];
            if(tag1)
                [tags addObject:tag1];
        }
        if(exercise.tag2 && exercise.tag2.length > 0){
            Tag *tag2 = [Tag tagWithId:exercise.tag2];
            if(tag2)
                [tags addObject:tag2];
        }
        if(exercise.tag3 && exercise.tag3.length > 0){
            Tag *tag3 = [Tag tagWithId:exercise.tag3];
            if(tag3)
                [tags addObject:tag3];
        }
        
        UIImageView *imageView = cell.workoutImageView;
        
        NSURLRequest *exerciseImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_EXERCISES,exercise.picture]]
                                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                          timeoutInterval:60];
        
        [cell.workoutImageView setImageWithURLRequest:exerciseImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-small"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            [imageView setImage:image];
            [imageView setContentMode:UIViewContentModeScaleAspectFill];
            [[imageView layer] setMasksToBounds:YES];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            NSLog(@"%@",error);
            [imageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
        }];
        
        [[cell energyLabel] setText:[NSString stringWithFormat:@"%d kCal",exercise.energy.intValue]];
        [[cell tagView] setTags:tags];
        [[cell tagView] reloadTagSubviews];
    }
    
    NSMutableString *seriesString = [[NSMutableString alloc] init];
    
    NSInteger kCals = 0;
    
    if(trainingExercise.series.count > 0){
        for(Serie *serie in trainingExercise.series){
            if(seriesString.length == 0)
                seriesString = [NSMutableString stringWithFormat:@"%d ",serie.reps.intValue];
            else
                [seriesString appendString:[NSString stringWithFormat:@"- %d ",serie.reps.intValue]];
            
            kCals = kCals + [Serie kCalInSerie:serie];
        }
        
        if(seriesString.length > 0)
            [[cell seriesLabel] setText:seriesString];
        
        int secs = [TrainingExercise getExerciseTime:trainingExercise];
        
        if(secs / 60 >= 1)
        {
            int minutes = secs / 60;
            //int seconds = secs % 60;
            [cell.timeLabel setText:[NSString stringWithFormat:@"%d min",minutes]];
        }else
            [cell.timeLabel setText:[NSString stringWithFormat:@"0 min"]];
    } else{
        [[cell seriesLabel] setText:NSLocalizedString(@"Añadir Series", nil)];
        [cell.timeLabel setText:[NSString stringWithFormat:@"0 min"]];
    }
    
    [[cell energyLabel] setText:[NSString stringWithFormat:@"%ld kCal",(long)kCals]];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        
        CSSectionHeader *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                   withReuseIdentifier:@"sectionHeader"
                                                                          forIndexPath:indexPath];
        
        
        return cell;
        
    } else if ([kind isEqualToString:CSStickyHeaderParallaxHeader]) {
        CSAlwaysOnTopHeader *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                            withReuseIdentifier:@"CSStickyHeaderParallaxHeader"
                                                                                   forIndexPath:indexPath];
        [cell setIsCreatingSuperSerie:_isCreatingSuperSerie];
        
        NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
        
        
        //for(Tag *tag in _currentTraining.tags){
        //    [tags addObject:tag];
        //}
        
        for(Tag *tagsExercise in [Training getTrainingsExercisesTags:_currentTraining]){
            [tags addObject:tagsExercise];
        }
        
        [[cell tagView] setTags:tags];
        [[cell tagView] reloadTagSubviews];
        
        int secs = 0;
        NSInteger kCal = 0;
        
        
        for(TrainingExercise *exercise in _currentTraining.trainingExercises){
            secs = [TrainingExercise getExerciseTime:exercise] + secs;
            for(Serie *serie in exercise.series){
                kCal = kCal + [Serie kCalInSerie:serie];
            }
        }
        
        //if(secs / 60 >= 1)
        //{
            int minutes = secs / 60;
        //    int seconds = secs % 60;
            [cell.timeLabel setText:[NSString stringWithFormat:@"%d min",minutes]];
        //}else
        //    [cell.timeLabel setText:[NSString stringWithFormat:@"%d seg.",secs]];
        
        [cell.exercisesLabel setText:[NSString stringWithFormat:@"%lu Ejercicios",(unsigned long)_currentTraining.trainingExercises.count]];
        [cell.kCalLabel setText:[NSString stringWithFormat:@"%ld kCal",(long)kCal]];
        
        [cell.titleHeaderLabel setText:_currentTraining.title];
        [cell setDelegate:self];
        
        return cell;
    } else if (kind == UICollectionElementKindSectionFooter){
        
        FooterCollectionViewCell *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                   withReuseIdentifier:@"footerView"
                                                                          forIndexPath:indexPath];
        
        
        return cell;
        
    }
    return nil;
}


#pragma mark LongPressdelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return FALSE;
    //return ![_trainingTableView isEditing];
}

#pragma mark SeriesViewControllerDelegate

- (void)backCurrentSeries:(NSMutableArray *)series andSSeries:(NSMutableArray *)sseries inIndex:(NSInteger)index{
    TrainingExercise *trainingExercise = [_currentTraining.trainingExercises objectAtIndex:index];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm transactionWithBlock:^{
        [trainingExercise.series removeAllObjects];
        [trainingExercise.secondExerciseSeries removeAllObjects];
        [trainingExercise.series addObjects:series];
        [trainingExercise.secondExerciseSeries addObjects:sseries];
        [_currentTraining.trainingExercises setObject:trainingExercise atIndexedSubscript:index];
    }];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:trainingExercise];
    [realm addOrUpdateObject:_currentTraining];
    
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error)
        NSLog(@"%@",error.description);
    
    [_executive updateTraining:_currentTraining];
}

- (void)backCurrentSeries:(NSMutableArray *)series inIndex:(NSInteger)index{
    
    TrainingExercise *trainingExercise = [_currentTraining.trainingExercises objectAtIndex:index];

    RLMRealm *realm = [AppContext currentRealm];
    
    [realm transactionWithBlock:^{
        [trainingExercise.series removeAllObjects];
        [trainingExercise.series addObjects:series];
        [_currentTraining.trainingExercises setObject:trainingExercise atIndexedSubscript:index];
    }];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:trainingExercise];
    [realm addOrUpdateObject:_currentTraining];
    
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if (error) {
        NSLog(@"%@",error.description);
    }
    
    [_trainingTableView setCurrentTraining:_currentTraining];
    [_trainingTableView.tableView reloadData];
    [_trainingTableView reloadWorkoutValues];
    
    [_executive updateTraining:_currentTraining];
}

#pragma mark UIPickerDataSource Methods

#pragma mark UIPickerDataSource Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component == 1) {
        return 60;
    }
    return 24;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    /// Set line break mode
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    /// Set text alignment
    //NSTextAlignmentLeft;//NSTextAlignmentRight;// NSTextAlignmentNatural;//  NSTextAlignmentLeft;
    
    
    switch (component) {
        case 0:{
            if(row<10)
                title = [NSString stringWithFormat:@"0%ld",(long)row];
            else
                title = [NSString stringWithFormat:@"%ld",(long)row];
            paragraphStyle.alignment = NSTextAlignmentCenter;
            break;
        }
        default:{
            if(row<10)
                title = [NSString stringWithFormat:@"0%ld",(long)row];
            else
                title = [NSString stringWithFormat:@"%ld",(long)row];
            paragraphStyle.alignment = NSTextAlignmentCenter;
            break;
        }
    }
    NSAttributedString *attString =
    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor lightTextColor],NSParagraphStyleAttributeName:paragraphStyle}];
    
    return attString;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title;
    switch (component) {
        case 0:{
            if(row<10)
                title = [NSString stringWithFormat:@"0%ld",(long)row];
            else
                title = [NSString stringWithFormat:@"%ld",(long)row];
            break;
        }
        default:{
            if(row<10)
                title = [NSString stringWithFormat:@"0%ld",(long)row];
            else
                title = [NSString stringWithFormat:@"%ld",(long)row];
            break;
        }
    }
    
    return title;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return [UtilManager width:80];
}

#pragma mark CSTopHeaderViewDelegate Methods

- (void)backTopHeaderAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)menuTopHeaderAction{
    [self menuAction];
}

#pragma mark - LXReorderableCollectionViewDataSource methods

- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath willMoveToIndexPath:(NSIndexPath *)toIndexPath {
    
    NSLog(@"dldld");
    //PlayingCard *playingCard = self.deck[fromIndexPath.item];
    
    //[self.deck removeObjectAtIndex:fromIndexPath.item];
    //[self.deck insertObject:playingCard atIndex:toIndexPath.item];
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    /*
     #if LX_LIMITED_MOVEMENT == 1
    PlayingCard *playingCard = self.deck[indexPath.item];
    
    switch (playingCard.suit) {
        case PlayingCardSuitSpade:
        case PlayingCardSuitClub: {
            return YES;
        } break;
        default: {
            return NO;
        } break;
    }
#else
    return YES;
#endif
     */
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath canMoveToIndexPath:(NSIndexPath *)toIndexPath {
    /*
#if LX_LIMITED_MOVEMENT == 1
    PlayingCard *fromPlayingCard = self.deck[fromIndexPath.item];
    PlayingCard *toPlayingCard = self.deck[toIndexPath.item];
    
    switch (toPlayingCard.suit) {
        case PlayingCardSuitSpade:
        case PlayingCardSuitClub: {
            return fromPlayingCard.rank == toPlayingCard.rank;
        } break;
        default: {
            return NO;
        } break;
    }
#else
    return YES;
#endif
     */
    
    return YES;
}

#pragma mark - LXReorderableCollectionViewDelegateFlowLayout methods

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout willBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"will begin drag");
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout didBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"did begin drag");
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout willEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"will end drag");
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout didEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"did end drag");
}


#pragma mark MBTwitterScrollDelegate Methods

- (void)editButtonDidPress{
    
    if(_trainingTableView.tableView.editing)
    {
        [_trainingTableView.tableView setEditing:FALSE animated:TRUE];
        [_executive updateTraining:_currentTraining];
    }
    else
        [_trainingTableView.tableView setEditing:TRUE animated:TRUE];
}

- (void)didPressShowInfoButton {
    _infoPopupViewController.rlmObject = _currentTraining;
    [self.view addSubview:_infoPopupViewController.view];
}

- (void)backButtonDidPress{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didPressShowExerciseInCalendar {
    if (_executive.executingIndex >= _currentTraining.trainingExercises.count) {
        [self presentTrainingResultViewController];
        return;
    }
    
    TrainingExercise * trainingExercise = [_currentTraining.trainingExercises objectAtIndex:_executive.executingIndex];
    
    SeriesViewController *seriesViewController = [[SeriesViewController alloc] init];
    seriesViewController.currentExercise = trainingExercise;
    seriesViewController.index = _executive.executingIndex;
    seriesViewController.performTraining = YES;
    seriesViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    seriesViewController.delegate = self;
    seriesViewController.workoutDetailDelegate = self;
    
    [self presentViewController:seriesViewController animated:YES completion:nil];
}

- (void)didPressUserRateButton {
    NSString * trainingId = _currentTraining.serverTrainingId;
    [WebServiceManager getTrainingsWithCompletionBlock:^(BOOL isSuccess) {
        _currentTraining = [Training trainingWithServerId:trainingId];
        [self presentUserRateView:_currentTraining];
    }];
}

- (void)optionButtonDidPress {
    
    if(_isCreatingSuperSerie) {
        _isCreatingSuperSerie = NO;
        [_trainingTableView.optionButton setImage:[UIImage imageNamed:@"btn-menu-settings"] forState:UIControlStateNormal];
        [_trainingTableView.menuButton setImage:[UIImage imageNamed:@"btn-menu-settings"] forState:UIControlStateNormal];
        
        if(_superSerieExcercises.count >= 2) {
            RLMRealm *realm = [AppContext currentRealm];
            
            TrainingExercise *trainingSuperSerieExercise = [[TrainingExercise alloc] init];
            
            trainingSuperSerieExercise.order = ((Serie*)[_superSerieExcercises firstObject]).order;
            trainingSuperSerieExercise.owner = [AppContext sharedInstance].currentUser.userId;
            trainingSuperSerieExercise.trainingId = [UtilManager uuid];
            
            for(TrainingExercise *trainingExercise in _superSerieExcercises) {
                
                if(trainingSuperSerieExercise.title.length == 0) {
                    trainingSuperSerieExercise.title = [trainingExercise.exercises objectAtIndex:0].title;
                }
                else {
                    trainingSuperSerieExercise.title = [NSString stringWithFormat:@"%@+%@",trainingSuperSerieExercise.title,[trainingExercise.exercises objectAtIndex:0].title];
                }
                
                [trainingSuperSerieExercise.exercises addObjects:trainingExercise.exercises];
                trainingSuperSerieExercise.isSuperSerie = @YES;
                
                for (Serie *serie in trainingExercise.series){
                    
                    Serie *newSerie = [[Serie alloc] init];
                    
                    [newSerie setSerieId:[UtilManager uuid]];
                    [newSerie setOwner:serie.owner];
                    [newSerie setExercise:serie.exercise];
                    [newSerie setIntensityFactor:serie.intensityFactor];
                    [newSerie setReps:serie.reps];
                    [newSerie setWeight:serie.weight];
                    [newSerie setIsSubserie:serie.isSubserie];
                    [newSerie setOrder:serie.order];
                    [newSerie setRest:serie.rest];
                    [newSerie setIntensityFactor:serie.intensityFactor];
                    [newSerie setIsSuperSerie:[NSNumber numberWithBool:TRUE]];
                    
                    [[trainingSuperSerieExercise series] addObject:newSerie];
                }
            }
            
            [realm transactionWithBlock:^{
                
                for(TrainingExercise *trainingExercise in _superSerieExcercises){
                    int index = 0;
                    for(TrainingExercise *deleteTrainingExercise in _currentTraining.trainingExercises){
                        if([deleteTrainingExercise.trainingId isEqualToString:trainingExercise.trainingId]){
                            [_currentTraining.trainingExercises removeObjectAtIndex:index];
                        }
                        index++;
                    }
                }
                [_currentTraining.trainingExercises addObject:trainingSuperSerieExercise];
            }];
            
            [_executive updateTraining:_currentTraining];
            
            [realm beginWriteTransaction];
            [realm addOrUpdateObject:_currentTraining];
            
            NSError *error;
            [realm commitWriteTransaction:&error];
            
            if(error){
                NSLog(@"%@",error.description);
            }else{
                
            }
            
            _trainingTableView.currentTraining = _currentTraining;
            [_trainingTableView.tableView reloadData];
            [_trainingTableView reloadWorkoutValues];
            
            [_superSerieExcercises removeAllObjects];
        }
        else {
            _trainingTableView.currentTraining = _currentTraining;
            [_trainingTableView.tableView reloadData];
            [_trainingTableView reloadWorkoutValues];
            
            [self showMessage:@"por favor crea más de un ejercicio"];
        }
    } else {
        OptionsViewController *optionsViewController = [[OptionsViewController alloc] init];
        [optionsViewController setCurrentTraining:_currentTraining];
        [optionsViewController setOptionType:OptionTypeWorkout];
        [optionsViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        [optionsViewController setDelegate:self];
        [self presentViewController:optionsViewController animated:YES completion:nil];
    }
}

#pragma mark - WorkoutDetailViewController Delegate

- (void)didUpdatedTraining:(Training*)training {
    if (_fitnessViewController) {
        _fitnessViewController.training = training;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didPauseExecutingExercise:(NSInteger)executingIndex {
    _executive.executingIndex = executingIndex;
    [_trainingTableView.tableView reloadData];
    
    [self presentTrainingResultViewController];
}

- (void)didCompleteExecutingExercise:(NSInteger)executingIndex {
    TrainingExercise * trainingExercise = [_currentTraining.trainingExercises objectAtIndex:executingIndex];
    [_executive completeExercise:trainingExercise
                        training:_currentTraining
                         forPlan:_currentTrainingPlan];
    
}

#pragma mark - WorkoutDetailDisplay Delegate

- (void)didTrainingUpdate:(Training*)training {
    NSString * trainingId = training.serverTrainingId;
    [WebServiceManager getTrainingsWithCompletionBlock:^(BOOL isSuccess) {
        _currentTraining = [Training trainingWithServerId:trainingId];
        _trainingTableView.currentTraining = [Training trainingWithServerId:trainingId];
        [_trainingTableView.tableView reloadData];
        [_trainingTableView reloadWorkoutValues];
    }];
}

- (void)reloadAllFitness {
    [WebServiceManager getTrainingsWithCompletionBlock:^(BOOL isSuccess) {
        [WebServiceManager getTrainingPlansWithCompletionBlock:^(BOOL isSuccess) {
        }];
    }];
}

- (void)realodTableView {
    [_trainingTableView.tableView reloadData];
}

- (void)presentTrainingResultViewController {
    
    UIStoryboard * fitnessStoryboard = [UIStoryboard storyboardWithName:@"Fitness" bundle:nil];
    TrainingResultViewController * trainingResultViewController = [fitnessStoryboard instantiateInitialViewController];
    trainingResultViewController.training = _currentTraining;
    [self presentTransparentViewController:trainingResultViewController animated:YES completion:nil];
}

#pragma mark - UserSelectorViewControllerDelegate Methods

- (void)didPressOKWithSelectedResults:(NSArray*)selectedResults
                           andContent:(RLMObject*)content {
    
    UserMeta * userMeta = [selectedResults firstObject];
    [_executive sendContent:content
                    toToken:userMeta.token];
}

@end
