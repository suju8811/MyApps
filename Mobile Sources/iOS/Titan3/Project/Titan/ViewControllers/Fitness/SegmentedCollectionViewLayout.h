//
//  SegmentedCollectionViewLayout.h
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SegmentedCollectionViewLayout : UICollectionViewFlowLayout

@end
