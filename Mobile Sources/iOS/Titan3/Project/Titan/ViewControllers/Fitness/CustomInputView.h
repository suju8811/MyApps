//
//  CustomInputView.h
//  Titan
//
//  Created by Manuel Manzanera on 5/4/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {
    CustomInputTypeBack,                   //Back
    CustomInputTypeWaist,                   //Cointura
    CustomInputTypeleftArm,                 //brazo izquierdo
    CustomInputTypeRightArm,                //brazo derecho
    CustomInputTypeLeftForeArm,             //antebrazo izquierdo
    CustomInputTypeRightForeArm,            //antebrazo derecho
    CustomInputTypeLeftThigh,               //muslo izquierdo
    CustomInputTypeRightThigh,              //muslo derecho
    CustomInputTypeWeight,                  //peso
    CustomInputTypeIGC,                     //índice de masa corporal
    CustomInputTypeSquat,                   //Sentadilla
    CustomInputTypeBenchPress,              //Press Banca
    CustomInputTypeMilitaryPress,           //Press Militar
    CustomInputTypeDeadWeight,              //Peso Muerto
    CustomInputTypeDominated                //Dominadas
} CustomInputType;

@protocol CustomInputViewDelegate <NSObject>

- (void)confirmWithText:(NSString *)text andCustomInputType:(CustomInputType)customInpuType;

@end

@interface CustomInputView : UIView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextField *inputTextField;
@property (nonatomic, assign) CustomInputType customInputType;

@property (nonatomic, assign) id<CustomInputViewDelegate>delegate;

@end
