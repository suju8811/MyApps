//
//  ExercisesViewController.h
//  Titan
//
//  Created by Marcus Lee on 16/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "TrainingPlan.h"

@interface ExercisesViewController : ParentViewController

@property (nonatomic, strong) Training *training;

@end
