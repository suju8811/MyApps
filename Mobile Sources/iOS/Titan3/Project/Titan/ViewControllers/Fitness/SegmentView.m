//
//  SegmentView.m
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SegmentView.h"
#import "SegmentedCollectionViewLayout.h"
#import "SegmentedCollectionViewCell.h"
#import "AppConstants.h"
#import "UtilManager.h"

@interface SegmentView ()<UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate,SegmentViewDelegate>{
}

@property (nonatomic) float lastScrollingValue;
@property (nonatomic) BOOL isScrollingRemote;
@property (nonatomic) BOOL isLastCollectionScroll;
@property (nonatomic) ScrollingItemType currentScrollingType;

@end

@implementation SegmentView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
        
        if(_segmentViewType == SegmentViewTypeFitness)
            self.titles = @[@"icon-profile-on", @"icon-fav-off", @"icon-comunity-off", @"icon-download-off"];
        else
            self.titles = @[@"icon-profile-on", @"icon-fav-off", @"icon-download-off"];
        [self setupViews];
    }
    
    return self;
}

- (CGFloat) endOfOriginY {
    NSLog(@"END OF ORIGIN Y %f",self.frame.origin.y + self.frame.size.height);
    return self.frame.origin.y + self.frame.size.height;
}

+ (SegmentView*) viewWithListTitle:(NSArray*)listTitle {
    SegmentView *view = [[SegmentView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, HEIGHT)];
    if(view.segmentViewType == SegmentViewTypeFitness)
        view.titles = @[@"icon-profile-on", @"icon-fav-off", @"icon-comunity-off", @"icon-download-off"];
    else
        view.titles = @[@"icon-profile-on", @"icon-fav-off", @"icon-download-off"];
    [view setupViews];
    return view;
}

- (void) setupViews {
    
    self.lastScrollingValue = 0;
    self.selectedIndex = 0;
    self.currentScrollingType = ScrollingItemTypeNone;
    
    [self setupCollectionView];
    [self setupTabImageView];
}

- (float) itemWidth {
    float itemWidth = WIDTH / self.titles.count;
    return itemWidth;
}

- (void) setupTabImageView {
    _imgSelectedTab = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., [self itemWidth], self.frame.size.height)];
}

- (void) setupCollectionView {
    SegmentedCollectionViewLayout *layout = [[SegmentedCollectionViewLayout alloc] init];
    _collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
    
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    
    [_collectionView registerClass:[SegmentedCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    float itemWidth = [self itemWidth];
    
    UICollectionViewFlowLayout *flowlayout = (id)_collectionView.collectionViewLayout;
    flowlayout.itemSize = CGSizeMake(itemWidth, _collectionView.frame.size.height);
    flowlayout.minimumLineSpacing = 0;
    flowlayout.minimumInteritemSpacing = 0;
    flowlayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [flowlayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    [self addSubview:_collectionView];
    
    [_collectionView reloadData];
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.titles.count;
}

- (UICollectionViewCell*) collectionView:(UICollectionView *)collectionView_ cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SegmentedCollectionViewCell *cell = [collectionView_ dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    NSString *title = self.titles[indexPath.row];
    
    [[cell iconImageView]setImage:[UIImage imageNamed:title]];
    [cell configCellWithData:title];
    [cell setCellSelected:indexPath.row == self.selectedIndex];
    return cell;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    CGRect frame = [cell convertRect:cell.bounds toView:self];
    self.imgSelectedTab.frame = frame;
    
    [self reloadWithSelectedIndex:indexPath.row];
    
    if (self.selectedIndex > 0 && self.selectedIndex < self.titles.count - 1) {
        self.isScrollingRemote = YES;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.imgSelectedTab.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
            [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        } completion:^(BOOL finished) {
            self.isScrollingRemote = NO;
        }];
    }
    
    if([_delegate respondsToSelector:@selector(segmentView:didSelectedIndex:)]){
        [_delegate segmentView:self didSelectedIndex:indexPath.row];
    }
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.isScrollingRemote)
        return;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectedIndex inSection:0];
    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    CGRect frame = [cell convertRect:cell.bounds toView:self];
    self.imgSelectedTab.frame = frame;
    
    self.isLastCollectionScroll = YES;
}

- (void) reloadWithSelectedIndex:(NSInteger)itemIndex {
    
    switch (itemIndex) {
        case 0:
            if(_segmentViewType == SegmentViewTypeFitness)
                self.titles = @[@"icon-profile-on", @"icon-fav-off", @"icon-comunity-off", @"icon-download-off"];
            else
                self.titles = @[@"icon-profile-on", @"icon-fav-off", @"icon-download-off"];
            break;
        case 1:
            if(_segmentViewType == SegmentViewTypeFitness)
                self.titles = @[@"icon-profile-on", @"icon-fav-off", @"icon-comunity-off", @"icon-download-off"];
            else
                self.titles = @[@"icon-profile-on", @"icon-fav-off", @"icon-download-off"];
            break;
        case 2:
            if(_segmentViewType == SegmentViewTypeFitness)
                self.titles = @[@"icon-profile-on", @"icon-fav-off", @"icon-comunity-off", @"icon-download-off"];
            else
                self.titles = @[@"icon-profile-on", @"icon-fav-off", @"icon-download-off"];
            break;
        case 3:
            if(_segmentViewType == SegmentViewTypeFitness)
                self.titles = @[@"icon-profile-on", @"icon-fav-off", @"icon-comunity-off", @"icon-download-off"];
            else
                self.titles = @[@"icon-profile-on", @"icon-fav-off", @"icon-download-off"];
            break;
        default:
            break;
    }
    
    [self setupViews];
    
    self.selectedIndex = itemIndex;
    [self.collectionView reloadData];
    self.lastScrollingValue = 0;
    self.isScrollingRemote = NO;
}

- (CGRect) getInViewFrameOfCellAtIndex:(NSInteger)index {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    UICollectionViewLayoutAttributes *pose = [self.collectionView.collectionViewLayout layoutAttributesForItemAtIndexPath:indexPath];
    CGRect frame = pose.frame;
    frame = [self.collectionView convertRect:frame toView:self];
    return frame;
}

- (void) reloadSubViewFrameToCurrentPosition {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectedIndex inSection:0];
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    self.imgSelectedTab.frame = [self getInViewFrameOfCellAtIndex:self.selectedIndex];
    
    self.isLastCollectionScroll = NO;
    self.currentScrollingType = ScrollingItemTypeNone;
}

- (void) scrollingFromRemoteWithValue:(float)scrollingValue {
    if (self.isLastCollectionScroll) {
        [self reloadSubViewFrameToCurrentPosition];
    }
    
    self.isScrollingRemote = YES;
    
    //Get scroll type
    if (self.currentScrollingType == ScrollingItemTypeNone) {
        self.currentScrollingType = ScrollingItemTypeCollectionView;
        
        if (scrollingValue > 0) {
            if (self.selectedIndex == 0 || self.selectedIndex == self.titles.count - 2)
                self.currentScrollingType = ScrollingItemTypeBackgroundView;
        } else {
            if (self.selectedIndex == self.titles.count - 1 ||
                self.selectedIndex == 1) {
                self.currentScrollingType = ScrollingItemTypeBackgroundView;
            }
        }
    }
    
    //Scrolling
    if (self.currentScrollingType == ScrollingItemTypeBackgroundView) {
        CGRect frame = self.imgSelectedTab.frame;
        frame.origin.x += (scrollingValue - self.lastScrollingValue)*[self itemWidth];
        if (frame.origin.x >= 0 && frame.origin.x < self.frame.size.width - frame.size.width) {
            self.imgSelectedTab.frame = frame;
        }
    } else {
        float xOffset = self.collectionView.contentOffset.x + (scrollingValue - self.lastScrollingValue)*[self itemWidth];
        if (xOffset >= 0 && xOffset <= self.collectionView.contentSize.width - self.frame.size.width) {
            [self.collectionView setContentOffset:CGPointMake(xOffset, self.collectionView.contentOffset.y)];
        }
    }
    
    //Reset value
    self.lastScrollingValue = scrollingValue;
    self.isScrollingRemote = NO;
}


@end
