//
//  SeriesCell.m
//  Titan
//
//  Created by Manuel Manzanera on 20/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SeriesCell.h"

#import "UtilManager.h"
#import "AppConstants.h"
#import "AppContext.h"

@interface SeriesCell ()<UITextFieldDelegate>

@end

@implementation SeriesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        
        _verticalSeparator = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:5], [UtilManager height:5], [UtilManager width:3], UITEXTFIELD_HEIGHT)];
        [_verticalSeparator setBackgroundColor:YELLOW_APP_COLOR];
        
        [self.contentView addSubview:_verticalSeparator];
        
        _indexLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:15], _verticalSeparator.frame.origin.y, [UtilManager width:20], _verticalSeparator.frame.size.height)];
        [_indexLabel setTextColor:YELLOW_APP_COLOR];
        [_indexLabel setText:@"1"];
        
        [[self contentView] addSubview:_indexLabel];
        
        _repTextField = [[UITextField alloc] initWithFrame:CGRectMake(_indexLabel.frame.origin.x + _indexLabel.frame.size.width, [UtilManager height:5], [UtilManager width:55], UITEXTFIELD_HEIGHT)];
        [_repTextField setTextColor:[UIColor whiteColor]];
        [_repTextField setText:@"15"];
        [_repTextField setTextAlignment:NSTextAlignmentRight];
        [_repTextField setKeyboardType:UIKeyboardTypeDecimalPad];
        [_repTextField setUserInteractionEnabled:TRUE];
        [_repTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
        [_repTextField setDelegate:self];
        
        UIView *toolBarView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:35])];
        [toolBarView setBackgroundColor:BLACK_HEADER_APP_COLOR];
        
        UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:100],0. , [UtilManager width:90], [UtilManager height:35])];
        [doneButton addTarget:self action:@selector(resetView) forControlEvents:UIControlEventTouchUpInside];
        [doneButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
        [doneButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [toolBarView addSubview:doneButton];
        
        UIButton *failButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], 0., [UtilManager width:90], [UtilManager height:35])];
        [failButton addTarget:self action:@selector(failAction) forControlEvents:UIControlEventTouchUpInside];
        [failButton setTitle:NSLocalizedString(@"Fallo", nil) forState:UIControlStateNormal];
        [failButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [toolBarView addSubview:failButton];
        
        _repTextField.inputAccessoryView = toolBarView;
    
        [self.contentView addSubview:_repTextField];
        
        _repLabel = [[UILabel alloc] initWithFrame:CGRectMake(_repTextField.frame.origin.x + _repTextField.frame.size.width + [UtilManager width:10], _repTextField.frame.origin.y, [UtilManager width:40], _repTextField.frame.size.height)];
        [_repLabel setText:NSLocalizedString(@"reps.", nil)];
        [_repLabel setTextColor:GRAY_REGISTER_FONT];
        [_repLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [_repLabel setTextAlignment:NSTextAlignmentCenter];
        
        [self.contentView addSubview:_repLabel];
        
        _weigthTextField = [[UITextField alloc] initWithFrame:CGRectMake(_repLabel.frame.origin.x + _repLabel.frame.size.width + [UtilManager width:5],_repTextField.frame.origin.y, _repTextField.frame.size.width + [UtilManager width:20], _repTextField.frame.size.height)];
        [_weigthTextField setTextColor:[UIColor whiteColor]];
        [_weigthTextField setText:@"12"];
        [_weigthTextField setTextAlignment:NSTextAlignmentRight];
        [_weigthTextField setUserInteractionEnabled:TRUE];
        [_weigthTextField setKeyboardType:UIKeyboardTypeDecimalPad];
        [_weigthTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
        [_weigthTextField setDelegate:self];
        
        UIView *toolBarWeigthView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:35])];
        [toolBarWeigthView setBackgroundColor:BLACK_HEADER_APP_COLOR];
        
        UIButton *doneWeigthButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:100],0. , [UtilManager width:90], [UtilManager height:35])];
        [doneWeigthButton addTarget:self action:@selector(resetWeigthView) forControlEvents:UIControlEventTouchUpInside];
        [doneWeigthButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
        [doneWeigthButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [toolBarWeigthView addSubview:doneWeigthButton];
        
        UIButton *failWeightButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], 0., [UtilManager width:90], [UtilManager height:35])];
        [failWeightButton addTarget:self action:@selector(failWeightAction) forControlEvents:UIControlEventTouchUpInside];
        [failWeightButton setTitle:NSLocalizedString(@"RM", nil) forState:UIControlStateNormal];
        [failWeightButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [toolBarWeigthView addSubview:failWeightButton];
        
        _weigthTextField.inputAccessoryView = toolBarWeigthView;
        
        [self.contentView addSubview:_weigthTextField];
        
        _weigthLabel = [[UILabel alloc] initWithFrame:CGRectMake(_weigthTextField.frame.origin.x + _weigthTextField.frame.size.width , _repTextField.frame.origin.y, _repLabel.frame.size.width, _repLabel.frame.size.height)];
        [_weigthLabel setText:@"Kg"];
        [_weigthLabel setTextColor:_repLabel.textColor];
        [_weigthLabel setFont:_repLabel.font];
        [_weigthLabel setTextAlignment:NSTextAlignmentCenter];
        
        [self.contentView addSubview:_weigthLabel];
        
        _descTextField = [[UITextField alloc] initWithFrame:CGRectMake(_weigthLabel.frame.origin.x + _weigthLabel.frame.size.width + [UtilManager width:5], _repTextField.frame.origin.y, _repTextField.frame.size.width + [UtilManager width:20], _repTextField.frame.size.height)];
        [_descTextField setTextColor:[UIColor whiteColor]];
        [_descTextField setText:@"5'"];
        [_descTextField setUserInteractionEnabled:TRUE];
        [_descTextField setTextAlignment:NSTextAlignmentCenter];
        [_descTextField setKeyboardType:UIKeyboardTypeDecimalPad];
        [_descTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
        [_descTextField setAdjustsFontSizeToFitWidth:YES];
        [_descTextField setDelegate:self];
        
        UIView *toolBarDescView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:35])];
        [toolBarDescView setBackgroundColor:BLACK_HEADER_APP_COLOR];
        
        UIButton *doneDescButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:100],0. , [UtilManager width:90], [UtilManager height:35])];
        [doneDescButton addTarget:self action:@selector(resetView) forControlEvents:UIControlEventTouchUpInside];
        [doneDescButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
        [doneDescButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [toolBarDescView addSubview:doneDescButton];
        
        [self.contentView addSubview:_descTextField];
        
        _descLabel   = [[UILabel alloc] initWithFrame:CGRectMake(_descTextField.frame.origin.x + _descTextField.frame.size.width + [UtilManager width:10] , _repTextField.frame.origin.y, _weigthLabel.frame.size.width, _weigthLabel.frame.size.height)];
        [_descLabel setText:NSLocalizedString(@"Desc.", nil)];
        [_descLabel setTextColor:_weigthLabel.textColor];
        [_descLabel setFont:_repLabel.font];
        [_descLabel setTextAlignment:NSTextAlignmentCenter];
        
        //[self.contentView addSubview:_descLabel];
        
        _descImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_descTextField.frame.origin.x + _descTextField.frame.size.width + [UtilManager width:5], 0., [UtilManager width:25] * 0.8, [UtilManager width:25] * 0.8)];
        [_descImageView setImage:[UIImage imageNamed:@"icon-footer-log"]];
        [_descImageView setCenter:CGPointMake(_descImageView.center.x, _descLabel.center.y)];
        _descImageView.image = [_descImageView.image  imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_descImageView setTintColor:_weigthLabel.textColor];
        
        [self.contentView addSubview:_descImageView];
        
        _executeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _executeButton.frame = _descImageView.frame;
        UIImage * tintImage = [[UIImage imageNamed:@"icon-footer-log"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_executeButton setImage:tintImage forState:UIControlStateNormal];
        [_executeButton setImage:[UIImage imageNamed:@"btn_stop"] forState:UIControlStateSelected];
        _executeButton.tintColor = YELLOW_APP_COLOR;
        [_executeButton addTarget:self action:@selector(didPressExecuteButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView addSubview:_executeButton];
        
        _repSSTextField = [[UITextField alloc] initWithFrame:CGRectMake(_indexLabel.frame.origin.x + _indexLabel.frame.size.width, _repTextField.frame.size.height + [UtilManager height:10], [UtilManager width:45], UITEXTFIELD_HEIGHT)];
        [_repSSTextField setTextColor:[UIColor whiteColor]];
        [_repSSTextField setText:@"-"];
        [_repSSTextField setTextAlignment:NSTextAlignmentRight];
        [_repSSTextField setKeyboardType:UIKeyboardTypeDecimalPad];
        [_repSSTextField setUserInteractionEnabled:TRUE];
        [_repSSTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
        [_repSSTextField setDelegate:self];
        [_repSSTextField setHidden:TRUE];
    
        UIView *toolRepSSBarView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:35])];
        [toolRepSSBarView setBackgroundColor:BLACK_HEADER_APP_COLOR];
        
        UIButton *doneRepSSButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:100],0. , [UtilManager width:90], [UtilManager height:35])];
        [doneRepSSButton addTarget:self action:@selector(resetView) forControlEvents:UIControlEventTouchUpInside];
        [doneRepSSButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
        [doneRepSSButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [toolRepSSBarView addSubview:doneRepSSButton];
        
        UIButton *failRepSSButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], 0., [UtilManager width:90], [UtilManager height:35])];
        [failRepSSButton addTarget:self action:@selector(failAction) forControlEvents:UIControlEventTouchUpInside];
        [failRepSSButton setTitle:NSLocalizedString(@"Fallo", nil) forState:UIControlStateNormal];
        [failRepSSButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [toolRepSSBarView addSubview:failRepSSButton];
        
        _repSSTextField.inputAccessoryView = toolRepSSBarView;
        
        [self.contentView addSubview:_repSSTextField];
        
        _repSSLabel = [[UILabel alloc] initWithFrame:CGRectMake(_repTextField.frame.origin.x + _repTextField.frame.size.width + [UtilManager width:10], _repSSTextField.frame.origin.y, [UtilManager width:40], _repTextField.frame.size.height)];
        [_repSSLabel setText:NSLocalizedString(@"reps.", nil)];
        [_repSSLabel setTextColor:GRAY_REGISTER_FONT];
        [_repSSLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [_repSSLabel setTextAlignment:NSTextAlignmentCenter];
        [_repSSLabel setHidden:TRUE];
        
        [self.contentView addSubview:_repSSLabel];
        
        _weigthSSTextField = [[UITextField alloc] initWithFrame:CGRectMake(_repLabel.frame.origin.x + _repLabel.frame.size.width + [UtilManager width:5],_repSSTextField.frame.origin.y, _repTextField.frame.size.width + [UtilManager width:20], _repTextField.frame.size.height)];
        [_weigthSSTextField setTextColor:[UIColor whiteColor]];
        [_weigthSSTextField setText:@"12"];
        [_weigthSSTextField setTextAlignment:NSTextAlignmentRight];
        [_weigthSSTextField setUserInteractionEnabled:TRUE];
        [_weigthSSTextField setKeyboardType:UIKeyboardTypeDecimalPad];
        [_weigthSSTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
        [_weigthSSTextField setDelegate:self];
        
        UIView *toolBarWeigthSSView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:35])];
        [toolBarWeigthSSView setBackgroundColor:BLACK_HEADER_APP_COLOR];
        
        UIButton *doneWeigthSSButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:100],0. , [UtilManager width:90], [UtilManager height:35])];
        [doneWeigthSSButton addTarget:self action:@selector(resetWeigthView) forControlEvents:UIControlEventTouchUpInside];
        [doneWeigthSSButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
        [doneWeigthSSButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [toolBarWeigthSSView addSubview:doneWeigthSSButton];
        
        UIButton *failWeightSSButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], 0., [UtilManager width:90], [UtilManager height:35])];
        [failWeightSSButton addTarget:self action:@selector(failWeightAction) forControlEvents:UIControlEventTouchUpInside];
        [failWeightSSButton setTitle:NSLocalizedString(@"RM", nil) forState:UIControlStateNormal];
        [failWeightSSButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [toolBarWeigthView addSubview:failWeightSSButton];
        [_weigthSSTextField setHidden:TRUE];
        _weigthSSTextField.inputAccessoryView = toolBarWeigthSSView;
        
        [self.contentView addSubview:_weigthSSTextField];
        
        _weigthSSLabel = [[UILabel alloc] initWithFrame:CGRectMake(_weigthTextField.frame.origin.x + _weigthTextField.frame.size.width , _repTextField.frame.origin.y, _repLabel.frame.size.width, _repLabel.frame.size.height)];
        [_weigthSSLabel setText:@"Kg"];
        [_weigthSSLabel setTextColor:_repLabel.textColor];
        [_weigthSSLabel setFont:_repLabel.font];
        [_weigthSSLabel setTextAlignment:NSTextAlignmentCenter];
        [_weigthSSLabel setHidden:TRUE];
    
        [self.contentView addSubview:_weigthSSLabel];
        
        _pauseStateCounterLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_indexLabel.frame), CGRectGetMinY(_repLabel.frame), CGRectGetMinX(_descImageView.frame) - CGRectGetMaxX(_indexLabel.frame), CGRectGetHeight(_repLabel.frame))];
        _pauseStateCounterLabel.textColor = _repSSTextField.textColor;
        _pauseStateCounterLabel.font = _repSSTextField.font;
        _pauseStateCounterLabel.textAlignment = NSTextAlignmentCenter;
        _pauseStateCounterLabel.hidden = YES;
        
        [self.contentView addSubview:_pauseStateCounterLabel];
        
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)resetWeigthView{
    NSDictionary *serieDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:_repTextField.text.intValue],@"reps",[NSNumber numberWithInt:_repSSTextField.text.intValue],@"repsSS",[NSNumber numberWithFloat:_weigthTextField.text.floatValue],@"weight",[NSNumber numberWithFloat:_weigthSSTextField.text.floatValue],@"weightSS", nil];
    
    [_delegate seriesData:serieDictionary indIndex:_index];
    
    [_weigthTextField resignFirstResponder];
    [_descTextField resignFirstResponder];
}

- (void)resetView{
    
    NSDictionary *serieDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:_repTextField.text.intValue],@"reps",[NSNumber numberWithInt:_repSSTextField.text.intValue],@"repsSS",[NSNumber numberWithFloat:_weigthTextField.text.floatValue],@"weight",[NSNumber numberWithFloat:_weigthSSTextField.text.floatValue],@"weightSS", nil];
    
    [_delegate seriesData:serieDictionary indIndex:_index];
    [_repTextField resignFirstResponder];
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 repeats:FALSE block:^(NSTimer *timer){
        [_weigthTextField becomeFirstResponder];
    }];
    
    
}

- (void)failAction{
    [_repTextField setText:@"10"];
    [self resetView];
}

- (void)failWeightAction{
    [_weigthTextField setText:@"50"];
    [self resetView];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [_delegate textBeginShowInIndex:_index];
    
    if([[[_trainingExercise.exercises objectAtIndex:0] type] isEqualToString:@"aerobic"]){
        if([textField isEqual:_repTextField]){
            [_delegate timeDidPushAtIndex:_index andColumns:1];
            [_weigthTextField resignFirstResponder];
            return NO;
        }
    }
    
    if([textField isEqual:_descTextField]){
        [_repTextField resignFirstResponder];
        [_weigthTextField resignFirstResponder];
        [_delegate timeDidPushAtIndex:_index andColumns:2];
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [textField selectAll:self];
    [UIMenuController sharedMenuController].menuVisible = NO;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    [_delegate textEndShowInIndex:_index];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [_delegate textEndShowInIndex:_index];
    
    if(textField == _repTextField) {
        [_weigthTextField becomeFirstResponder];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason NS_AVAILABLE_IOS(10_0){
    
    NSDictionary *serieDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @(_repTextField.text.intValue), @"reps",
                                     @(_repSSTextField.text.intValue), @"repsSS",
                                     @(_weigthTextField.text.floatValue), @"weight",
                                     @(_weigthSSTextField.text.floatValue),@"weightSS", nil];
    
    [_delegate seriesData:serieDictionary indIndex:_index];
    
    if(textField == _weigthTextField){
    
    }
    if(textField == _repTextField)
        [_weigthTextField becomeFirstResponder];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSUInteger newLength;
    
    newLength = [textField.text length] + [string length] - range.length;
    
    if([textField isEqual:_repTextField] || [textField isEqual:_repSSTextField]){
        if([string isEqualToString:@"."])
            return false;
    } else if ([textField isEqual:_weigthTextField] || [textField isEqual:_weigthSSTextField]){
        if((_weigthTextField.text.length == 0 && [string isEqualToString:@"."]) || (_weigthSSTextField.text.length == 0 && [string isEqualToString:@"."]))
            return false;
        
        if(([[NSString stringWithFormat:@"%@%@",_weigthTextField.text,string] intValue]>1000)||[[NSString stringWithFormat:@"%@%@",_weigthSSTextField.text,string] intValue]>1000)
            return NO;
        
        return (newLength > 6) ? NO : YES;
    }
    
    return (newLength > 3) ? NO : YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return TRUE;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
}


- (void)setNeedsLayout{
    
    if (_isSuperSerie) {
        [_repSSTextField setHidden:FALSE];
        [_repSSLabel setHidden:FALSE];
        [_weigthSSTextField setHidden:FALSE];
        [_weigthSSLabel setHidden:FALSE];
        [_verticalSeparator setFrame:CGRectMake([UtilManager width:5], [UtilManager height:5], [UtilManager width:3], UITEXTFIELD_HEIGHT * 2)];
        [_indexLabel setFrame:CGRectMake([UtilManager width:15], _verticalSeparator.frame.origin.y, [UtilManager width:20], _verticalSeparator.frame.size.height)];
        [_descTextField setFrame:CGRectMake(_weigthLabel.frame.origin.x + _weigthLabel.frame.size.width + [UtilManager width:5], _repTextField.frame.origin.y + _repTextField.frame.size.height, _repTextField.frame.size.width + [UtilManager width:20], _repTextField.frame.size.height)];
        [_descTextField setCenter:CGPointMake(_descTextField.center.x, _repTextField.frame.origin.y + _repTextField.frame.size.height)];
        [_descImageView setCenter:CGPointMake(_descImageView.center.x, _descTextField.center.y)];
        
    }
    else {
        [_repSSTextField setHidden:TRUE];
        [_repSSLabel setHidden:TRUE];
        [_weigthSSTextField setHidden:TRUE];
        [_weigthSSLabel setHidden:TRUE];
    
        Exercise *exercise = [_trainingExercise.exercises objectAtIndex:0];
        
        if ([exercise.type isEqualToString:@"aerobic"]) {
            [_weigthLabel setText:@"Int"];
            [_repLabel setText:NSLocalizedString(@"time", nil)];
        }
        else if([exercise.energyType isEqualToString:@"reps"]) {
        }
        else {
        }
        
    }
}

- (void)didPressExecuteButton:(UIButton*)sender {
    sender.selected = !sender.selected;
    if ([_delegate respondsToSelector:@selector(seriesCell:didPressStopButton:index:)]) {
        [_delegate seriesCell:self didPressStopButton:sender index:_index];
    }
}

- (void)setContentWithSerie:(Serie *)serie {
    if(_trainingExercise.isSuperSerie.boolValue) {
        _isSuperSerie = YES;
        _repSSTextField.text = [NSString stringWithFormat:@"%d", _secondExerciseSerie.reps.intValue];
        _weigthSSTextField.text = [NSString stringWithFormat:@"%.2f", _secondExerciseSerie.weight.floatValue];
    }
    else if(serie.isSubserie.boolValue) {
        _verticalSeparator.hidden = NO;
        _indexLabel.hidden = YES;
    }
    else {
        _isSuperSerie = NO;
        _indexLabel.text = [NSString stringWithFormat:@"%ld", _index + 1];
        _verticalSeparator.hidden = NO;
        _indexLabel.hidden = NO;
    }
    
    int min = serie.execTime.intValue/60;
    int seg = serie.execTime.intValue%60;
    
    NSString *minutes;
    NSString *seconds;
    
    minutes = [NSString stringWithFormat:@"%ld",(long)min];
    
    if(seg < 10) {
        seconds = [NSString stringWithFormat:@"0%ld",(long)seg];
    }
    else {
        seconds = [NSString stringWithFormat:@"%ld",(long)seg];
    }
    
    _descTextField.text = [NSString stringWithFormat:@"%@:%@", minutes, seconds];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    _executeButton.hidden = !(serie.executingStatus.intValue == 1 || serie.executingStatus.intValue == 2);
    _descTextField.userInteractionEnabled = !(serie.executingStatus.intValue == 1 || serie.executingStatus.intValue == 2);
    if (serie.executingStatus.intValue >= 2) {
        min = serie.rest.intValue/60;
        seg = serie.rest.intValue%60;
        
        minutes = [NSString stringWithFormat:@"%ld",(long)min];
        
        if(seg < 10) {
            seconds = [NSString stringWithFormat:@"0%ld",(long)seg];
        }
        else {
            seconds = [NSString stringWithFormat:@"%ld",(long)seg];
        }
        
        _pauseStateCounterLabel.text = [NSString stringWithFormat:@"%@:%@", minutes, seconds];
        
        _repTextField.hidden = YES;
        _repLabel.hidden = YES;
        _repSSTextField.hidden = YES;
        _repSSLabel.hidden = YES;
        _weigthTextField.hidden = YES;
        _weigthLabel.hidden = YES;
        _weigthSSTextField.hidden = YES;
        _weigthSSLabel.hidden = YES;
        _descTextField.hidden = YES;
        
        if (serie.executingStatus.intValue == 2) {
            _pauseStateCounterLabel.hidden = YES;
            _executeButton.selected = YES;
        }
        else {
            _pauseStateCounterLabel.hidden = NO;
            _executeButton.selected = NO;
        }
    }
    else {
        _repTextField.hidden = NO;
        _repLabel.hidden = NO;
        _repSSTextField.hidden = NO;
        _repSSLabel.hidden = NO;
        _weigthTextField.hidden = NO;
        _weigthLabel.hidden = NO;
        _weigthSSTextField.hidden = NO;
        _weigthSSLabel.hidden = NO;
        _descTextField.hidden = NO;
        
        _pauseStateCounterLabel.hidden = YES;
        
        _executeButton.selected = NO;
    }
    
    if([[[_trainingExercise.exercises firstObject] type] isEqualToString:@"aerobico"]){
        if(serie.execTime.intValue == 0 &&
           serie.intensityFactor.intValue == 0 &&
           serie.rest.intValue == 0) {
            
            _repTextField.text = @"-";
            _weigthTextField.text = @"-";
            _descTextField.text = @"-";
        }
        else {
            min = serie.execTime.intValue/60;
            seg = serie.execTime.intValue%60;
            
            minutes = [NSString stringWithFormat:@"%ld",(long)min];
            
            if(seg<10) {
                seconds = [NSString stringWithFormat:@"0%ld",(long)seg];
            }
            else {
                seconds = [NSString stringWithFormat:@"%ld",(long)seg];
            }
            
            _repTextField.text = [NSString stringWithFormat:@"%@:%@", minutes, seconds];
            _weigthTextField.text = [NSString stringWithFormat:@"%.2f", serie.intensityFactor.floatValue];
        }
        
        _repLabel.text = @"";
        _weigthLabel.text = @" intensidad";
        CGRect weightLabelFrame = _weigthLabel.frame;
        weightLabelFrame.size.width = _repLabel.frame.size.width + [UtilManager width:50];
        _weigthLabel.frame = weightLabelFrame;
        _descTextField.hidden = YES;
    }
    else {
        if (serie.reps.intValue == 0 &&
            serie.weight.intValue == 0 &&
            serie.rest.intValue == 0) {
            
            _repTextField.text = @"-";
            _weigthTextField.text = @"-";
            _descTextField.text = @"-";
        }
        else {
            _repTextField.text = [NSString stringWithFormat:@"%d", serie.reps.intValue];
            _weigthTextField.text = [NSString stringWithFormat:@"%.2f", serie.weight.floatValue];
        }
        
        _repLabel.text = @"reps.";
        _weigthLabel.text = @"kg";
        CGRect weightLabelFrame = _weigthLabel.frame;
        weightLabelFrame.size.width = _repLabel.frame.size.width;
        _weigthLabel.frame = weightLabelFrame;
        _descTextField.hidden = NO;
    }
}

@end
