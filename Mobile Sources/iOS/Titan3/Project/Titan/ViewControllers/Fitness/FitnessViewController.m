//
//  FitnessViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FitnessViewController.h"
#import "ExerciseDetailViewController.h"
#import "TrainingPlanDetailViewController.h"
#import "TrainingAddViewController.h"
#import "TrainingPlanAddViewController.h"
#import "SeriesViewController.h"
#import "WorkoutDetailViewController.h"
#import "MenuContainerNavController.h"
#import "OptionsViewController.h"
#import "RNBlurModalView.h"
#import "WebServiceManager.h"
#import "FitnessCell.h"
#import "ExerciseCell.h"
#import "FitnessPlanCell.h"
#import "SegmentView.h"
#import "Tag.h"
#import "Exercise.h"
#import "Training.h"
#import "TrainingPlan.h"
#import "TrainingDay.h"
#import "TrainingExercise.h"
#import "TrainingDayContent.h"
#import "Muscle.h"
#import "UIImageView+AFNetworking.h"
#import "UserSelectorViewController.h"
#import "UserMeta.h"

@interface FitnessViewController () <UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate, FitnessPlanCellDelegate, OptionsViewControllerDelegate, FitnessCellDelegate, FitnessDisplay, UserSelectorViewControllerDelegate>

@property (nonatomic, strong) UITableView *fitnessTableView;
@property (nonatomic, strong) UIButton *profileButton;
@property (nonatomic, strong) UIButton *favButton;
@property (nonatomic, strong) UIButton *comunityButton;
@property (nonatomic, strong) UIButton *downloadButton;

@property (nonatomic, strong) UIButton *planButton;
@property (nonatomic, strong) UIButton *workoutButton;
@property (nonatomic, strong) UIButton *exercisesButton;

@property (nonatomic, strong) UILabel *planLabel;
@property (nonatomic, strong) UILabel *workoutLabel;
@property (nonatomic, strong) UILabel *exercisesLabel;

@property (nonatomic, strong) RLMResults *items;

@property (nonatomic, strong) UISegmentedControl *segmentControl;
@property (nonatomic) NSInteger segmentState;

@property (nonatomic, strong) UILabel *warningLabel;

@property (nonatomic, strong) UISearchController * searchController;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIView *segmentControlView;
@property (nonatomic, assign) BOOL isFromPush;

@property (nonatomic, strong) MenuContainerNavController *menuContainerNavController;

@property (nonatomic, strong) FitnessExecutive * executive;

@property (nonatomic, strong) NSString * keyword;
@property (nonatomic, strong) UserSelectorViewController * userSelectorViewController;

@property (nonatomic, strong) UIButton *acceptButton;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) NSDate *endDate;

@property (nonatomic, assign) NSInteger selectedIndex;

@end

@implementation FitnessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isFromPush = FALSE;
    
    _menuContainerNavController = (MenuContainerNavController *)self.navigationController;
    
    _workoutSelectType = WorkoutSelectTypeUser;
    _trainingPlanSelectType = TrainingPlanSelectTypeUser;
    _selectType = FitnessSelectTypePlan;
    _segmentState = 0;
    
    _items = [TrainingPlan getTrainingPlans];
    
    [self configureView];
    
    _executive = [[FitnessExecutive alloc] initWithDisplay:self];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NAV_RIGHT1_ACTION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NAV_RIGHT2_ACTION object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPressOKBUtton) name:NAV_RIGHT2_ACTION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPressSearchButton) name:NAV_RIGHT1_ACTION object:nil];
    
    [_executive setAllTagsOff];
    [_executive loadItemsForFitnessType:_selectType
                      workoutSelectType:_workoutSelectType
                 trainingPlanSelectType:_trainingPlanSelectType
                   exerciseSegmentState:_segmentState
                             searchText:_keyword];
    
//    if(_trainingPlan) {
//        [self presentTrainigPlanDetailViewController:_trainingPlan];
//    }
//    else if (_training) {
//        [self presentWorkOutViewController:_training];
//    }
}

- (void)configureView {
    
    [[self view] setBackgroundColor:BLACK_APP_COLOR];
    //[self.view addSubview:[self headerView]];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _warningLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:200], [UtilManager height:60])];
    [_warningLabel setNumberOfLines:0];
    [_warningLabel setTextColor:GRAY_REGISTER_FONT];
    [_warningLabel setFont:[UIFont fontWithName:REGULAR_FONT size:18]];
    [_warningLabel setCenter:self.view.center];
    [_warningLabel setTextAlignment:NSTextAlignmentCenter];
    
    [[self view] addSubview:_warningLabel];
    
    _fitnessTableView  = [[UITableView alloc] initWithFrame:CGRectMake(0., NAV_HEIGHT + TAB_BAR_HEIGHT , WIDTH, HEIGHT - NAV_HEIGHT - TAB_BAR_HEIGHT - FOOTER_BAR_HEIGHT) style:UITableViewStylePlain];
    [_fitnessTableView setDelegate:self];
    [_fitnessTableView setDataSource:self];
    [_fitnessTableView setBounces:NO];
    [_fitnessTableView setBackgroundColor:[UIColor clearColor]];
    [_fitnessTableView registerClass:[FitnessCell class] forCellReuseIdentifier:@"fitnessCell"];
    [_fitnessTableView registerClass:[FitnessPlanCell class] forCellReuseIdentifier:@"fitnessPlanCell"];
    [_fitnessTableView registerClass:[ExerciseCell class] forCellReuseIdentifier:@"exerciseCell"];
    [_fitnessTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(rightSwipe)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    
    [_fitnessTableView addGestureRecognizer:recognizer];
    
    [self segmentControlHeaderView];
    [self addHeaderView];
    [[self view] addSubview:_fitnessTableView];
    [self.view addSubview:[self footerView]];
}

- (void)rightSwipe {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTIF_MENU_SLIDE" object:nil];
}

- (void)addHeaderView{
    _headerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20], NAV_HEIGHT, WIDTH - [UtilManager width:40], TAB_BAR_HEIGHT)];
    
    _favButton = [[UIButton alloc] initWithFrame:CGRectMake(0., [UtilManager height:5], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [_favButton setImage:[UIImage imageNamed:@"icon-fav-off"] forState:UIControlStateNormal];
    [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateSelected];
    [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateHighlighted];
    [_favButton setCenter:CGPointMake(_headerView.frame.size.width * 3/8, TAB_BAR_HEIGHT/2)];
    [_favButton setTag:1];
    [_favButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:_favButton];
    
    _profileButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
    [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateSelected];
    [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateHighlighted];
    [_profileButton setImage:[UIImage imageNamed:@"icon-profile-off"] forState:UIControlStateNormal];
    [_profileButton setCenter:CGPointMake(_headerView.frame.size.width/8, _favButton.center.y)];
    [_profileButton setTag:0];
    [_profileButton setSelected:YES];
    [_profileButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:_profileButton];
    
    _comunityButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
    [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateSelected];
    [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateHighlighted];
    [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-off"] forState:UIControlStateNormal];
    [_comunityButton setCenter:CGPointMake(5 * _headerView.frame.size.width/8, _favButton.center.y)];
    [_comunityButton setTag:2];
    [_comunityButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:_comunityButton];
    
    _downloadButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
    [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateSelected];
    [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateHighlighted];
    [_downloadButton setImage:[UIImage imageNamed:@"icon-download-off"] forState:UIControlStateNormal];
    [_downloadButton setCenter:CGPointMake(7 * _headerView.frame.size.width/8, _favButton.center.y)];
    [_downloadButton setTag:3];
    [_downloadButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:_downloadButton];
    
    [self.view addSubview:_headerView];
}

- (void)segmentControlHeaderView {
    _segmentControlView = [[UIView alloc] initWithFrame:CGRectMake(0., NAV_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    _segmentControlView.backgroundColor = UIColor.clearColor;
    _segmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Anaeróbicos", nil),
                                                                  NSLocalizedString(@"Aeróbicos", nil)]];
    
    _segmentControl.selectedSegmentIndex = _segmentState;
    [_segmentControl addTarget:self action:@selector(changeExercisesView:) forControlEvents:UIControlEventValueChanged];
    _segmentControl.center = CGPointMake(_segmentControlView.center.x, [UtilManager height:25]);
    _segmentControl.tintColor = YELLOW_APP_COLOR;
    [_segmentControlView addSubview:_segmentControl];
    _segmentControlView.hidden = YES;
    [self.view addSubview:_segmentControlView];
}

- (UIView *)footerView{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., HEIGHT - FOOTER_BAR_HEIGHT, WIDTH, FOOTER_BAR_HEIGHT)];
    [footerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = footerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //[footerView addSubview:blurEffectView];
    
    _planButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., TAB_BAR_HEIGHT * 0.6, TAB_BAR_HEIGHT * 0.6)];
    //[_planButton setTitle:NSLocalizedString(@"PLANES", nil) forState:UIControlStateNormal];
    //[_planButton setTitleColor:GRAY_REGISTER_FONT forState:UIControlStateNormal];
    //[_planButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateHighlighted];
    //[_planButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateSelected];
    [_planButton setImage:[UIImage imageNamed:@"icon-footer-plan"] forState:UIControlStateNormal];
    [_planButton setImage:[_planButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    [_planButton setTintColor:YELLOW_APP_COLOR];
    [_planButton setSelected:TRUE];
    [_planButton setTag:0];
    [_planButton setCenter:CGPointMake(WIDTH / 6, footerView.frame.size.height * 0.4)];
    [_planButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_planButton];
    
    _planLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., _planButton.frame.origin.y + _planButton.frame.size.height + [UtilManager height:5], WIDTH/4, [UtilManager height:10])];
    [_planLabel setTextAlignment:NSTextAlignmentCenter];
    [_planLabel setCenter:CGPointMake(_planButton.center.x, _planLabel.center.y)];
    [_planLabel setText:NSLocalizedString(@"Planes", nil)];
    [_planLabel setFont:[UIFont fontWithName:BOLD_FONT size:9]];
    [_planLabel setTextColor:YELLOW_APP_COLOR];
    
    [footerView addSubview:_planLabel];
    
    _workoutButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _planButton.frame.size.width, _planButton.frame.size.height)];
    //[_workoutButton setTitle:NSLocalizedString(@"WORKOUT", nil) forState:UIControlStateNormal];
    //[_workoutButton setTitleColor:GRAY_REGISTER_FONT forState:UIControlStateNormal];
    //[_workoutButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateHighlighted];
    //[_workoutButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateSelected];
    [_workoutButton setImage:[UIImage imageNamed:@"icon-footer-workout"] forState:UIControlStateNormal];
    [_workoutButton setImage:[_workoutButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    [_workoutButton setTintColor:YELLOW_APP_COLOR];
    [_workoutButton setCenter:CGPointMake(WIDTH/2, _planButton.center.y)];
    [_workoutButton setTag:1];
    [_workoutButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_workoutButton];
    
    _workoutLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., _planLabel.frame.origin.y, WIDTH/4, [UtilManager height:10])];
    [_workoutLabel setTextAlignment:NSTextAlignmentCenter];
    [_workoutLabel setCenter:CGPointMake(_workoutButton.center.x, _planLabel.center.y)];
    [_workoutLabel setText:NSLocalizedString(@"Workouts", nil)];
    [_workoutLabel setFont:[UIFont fontWithName:BOLD_FONT size:9]];
    [_workoutLabel setTextColor:[UIColor whiteColor]];
    
    [footerView addSubview:_workoutLabel];
    
    _exercisesButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _planButton.frame.size.width, _planButton.frame.size.height)];
    //[_exercisesButton setTitle:NSLocalizedString(@"EJERCICIOS", nil) forState:UIControlStateNormal];
    //[_exercisesButton setTitleColor:GRAY_REGISTER_FONT forState:UIControlStateNormal];
    //[_exercisesButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateHighlighted];
    //[_exercisesButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateSelected];
    [_exercisesButton setImage:[UIImage imageNamed:@"icon-footer-exercise"] forState:UIControlStateNormal];
    [_exercisesButton setImage:[_exercisesButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    [_exercisesButton setTintColor:YELLOW_APP_COLOR];
    [_exercisesButton setCenter:CGPointMake(WIDTH * 5 / 6, _planButton.center.y)];
    [_exercisesButton setTag:2];
    [_exercisesButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_exercisesButton];
    
    _exercisesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., _planLabel.frame.origin.y, WIDTH/4, [UtilManager height:10])];
    [_exercisesLabel setTextAlignment:NSTextAlignmentCenter];
    [_exercisesLabel setCenter:CGPointMake(_exercisesButton.center.x, _planLabel.center.y)];
    [_exercisesLabel setText:NSLocalizedString(@"Ejercicios", nil)];
    [_exercisesLabel setFont:[UIFont fontWithName:BOLD_FONT size:9]];
    [_exercisesLabel setTextColor:[UIColor whiteColor]];
    
    [footerView addSubview:_exercisesLabel];
    
    UIButton *planOverButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., WIDTH/3, TAB_BAR_HEIGHT)];
    [planOverButton setTag:0];
    [planOverButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:planOverButton];
    
    UIButton *workoutOverButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH/3, 0., WIDTH/3, TAB_BAR_HEIGHT)];
    [workoutOverButton setTag:1];
    [workoutOverButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:workoutOverButton];
    
    UIButton *exerciseOverButton = [[UIButton alloc] initWithFrame:CGRectMake(2*WIDTH/3, 0., WIDTH/3, TAB_BAR_HEIGHT)];
    [exerciseOverButton setTag:2];
    [exerciseOverButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:exerciseOverButton];
    
    return footerView;
}

- (void)headerAction:(id)sender {
    UIButton *touchButton = (UIButton *)sender;
    
    [_profileButton setSelected:NO];
    [_favButton setSelected:NO];
    [_downloadButton setSelected:NO];
    [_comunityButton setSelected:NO];
    
    switch (touchButton.tag) {
        case 0:
            _workoutSelectType = WorkoutSelectTypeUser;
            _trainingPlanSelectType = TrainingPlanSelectTypeUser;
            [_profileButton setSelected:YES];
            break;
        case 1:
            _workoutSelectType = WorkoutSelectTypeFavourite;
            _trainingPlanSelectType = TrainingPlanSelectTypeFavourite;
            [_favButton setSelected:YES];
            break;
        case 2:
            _workoutSelectType = WorkoutSelectTypeAdmin;
            _trainingPlanSelectType = TrainingPlanSelectTypeAdmin;
            [_comunityButton setSelected:YES];
            break;
        case 3:
            _workoutSelectType = WorkoutSelectTypeShared;
            _trainingPlanSelectType = TrainingPlanSelectTypeShared;
            [_downloadButton setSelected:YES];
            break;
        default:
            break;
    }
    
    [_executive loadItemsForFitnessType:_selectType
                      workoutSelectType:_workoutSelectType
                 trainingPlanSelectType:_trainingPlanSelectType
                   exerciseSegmentState:_segmentState
                             searchText:_keyword];
}

- (void)footerAction:(id)sender{
    
    UIButton *touchButton = (UIButton *)sender;
    
    if (_selectType == touchButton.tag) {
        return;
    }
    
    _keyword = @"";
    switch (touchButton.tag) {
        case 0:
            _selectType = FitnessSelectTypePlan;
            break;
        case 1:
            _selectType = FitnessSelectTyeWorkout;
            break;
        case 2:
            _selectType = FitnessSelectTypeExercise;
        default:
            break;
    }
    
    [_executive loadItemsForFitnessType:_selectType
                      workoutSelectType:_workoutSelectType
                 trainingPlanSelectType:_trainingPlanSelectType
                   exerciseSegmentState:_segmentState
                             searchText:_keyword];
}

- (void)didPressOKBUtton {
    switch (_selectType) {
        case FitnessSelectTypePlan:
            [self presentTrainingPlanAddViewController];
            break;
        case FitnessSelectTyeWorkout:
            [self presentTrainingAddViewController];
            break;
            
        case FitnessSelectTypeExercise:
            
            break;
        default:
            break;
    }
}

- (void)didPressSearchButton {
    if (_menuContainerNavController.searchContainerView.isHidden) {
        [_menuContainerNavController showSearchBar:YES];
    }
    else {
        [_menuContainerNavController showSearchBar:NO];
    }
}

- (void)changeExercisesView:(UISegmentedControl *)segmentControl{
    UISegmentedControl *currentSegmentControl = segmentControl;
    
    switch (currentSegmentControl.selectedSegmentIndex) {
            case 1:
            _items = [Exercise getAerobicsExercises];
            break;
            case 0:
            _items = [Exercise getAnaerobicExercises];
            break;
        default:
            break;
    }
    
    if(_items.count == 0)
        [_warningLabel setText:NSLocalizedString(@"Todavía no hay ejercicios de este tipo", nil)];
    else
        [_warningLabel setText:NSLocalizedString(@"", nil)];
    _segmentState = currentSegmentControl.selectedSegmentIndex;
    [_fitnessTableView reloadData];
}

- (void)presentTrainingAddViewController {
    TrainingAddViewController *trainingAddViewcontroller = [[TrainingAddViewController alloc] init];
    trainingAddViewcontroller.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    trainingAddViewcontroller.fitnessViewController = self;
    [self presentViewController:trainingAddViewcontroller animated:YES completion:nil];
}

- (void)presentTrainingPlanAddViewController {
    TrainingPlanAddViewController *trainingPlanAddViewController = [[TrainingPlanAddViewController alloc] init];
    trainingPlanAddViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    trainingPlanAddViewController.fitnessViewController = self;
    [self presentViewController:trainingPlanAddViewController animated:YES completion:nil];
}

- (void)presentWorkOutViewController:(Training *)training {
    WorkoutDetailViewController *trainingDetailViewController = [[WorkoutDetailViewController alloc] init];
    [trainingDetailViewController setCurrentTraining:training];
    [trainingDetailViewController setFitnessViewController:self];
    [self.navigationController pushViewController:trainingDetailViewController animated:YES];
    [self setTraining:nil];
}

- (void)presentTrainigPlanDetailViewController:(TrainingPlan *)trainingPlan{
    TrainingPlanDetailViewController *trainingDetailViewController = [[TrainingPlanDetailViewController alloc] init];
    trainingDetailViewController.trainingPlan = trainingPlan;
    [self.navigationController pushViewController:trainingDetailViewController animated:YES];
    [self setTrainingPlan:nil];
}

#pragma mark OptionViewControllerDelegate Methods

- (void)editTrainingPlan:(TrainingPlan *)trainingPlan{
    TrainingPlanAddViewController *trainingPlanAddViewController = [[TrainingPlanAddViewController alloc] init];
    [trainingPlanAddViewController setCurrentTrainingPlan:trainingPlan];
    [trainingPlanAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [trainingPlanAddViewController setFitnessViewController:self];
    
    [self presentViewController:trainingPlanAddViewController animated:YES completion:nil];
}

- (void)editTraining:(Training *)training {
    
    TrainingAddViewController *trainingAddViewcontroller = [[TrainingAddViewController alloc] init];
    trainingAddViewcontroller.editTraining = training;
    trainingAddViewcontroller.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    trainingAddViewcontroller.fitnessViewController = self;
    
    [self presentViewController:trainingAddViewcontroller animated:YES completion:nil];
}

#pragma mark FitnessPlanCellDelegate Methods

- (void)optionsDidPushInIndex:(NSInteger)index {
    TrainingPlan *training = [_items objectAtIndex:index];
    
    _selectedIndex = index;
    
    OptionsViewController *optionsViewController = [[OptionsViewController alloc] init];
    optionsViewController.currentTrainingPlan = training;
    optionsViewController.optionType = OptionTypeTrainings;
    optionsViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsViewController.delegate = self;
    [self presentViewController:optionsViewController animated:YES completion:nil];
}

#pragma mark FitnessCellDelegate Methods

- (void)optionsTrainingDidPushInIndex:(NSInteger)index {
    Training *training = [_items objectAtIndex:index];
    
    OptionsViewController *optionsViewController = [[OptionsViewController alloc] init];
    optionsViewController.currentTraining = training;
    optionsViewController.optionType = OptionTypeWorkout;
    optionsViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsViewController.delegate = self;
    [self presentViewController:optionsViewController animated:YES completion:nil];
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(_selectType == FitnessSelectTyeWorkout){
        
        Training *training = [_items objectAtIndex:indexPath.row];
        [self presentWorkOutViewController:training];
    } else if(_selectType == FitnessSelectTypeExercise){
        Exercise *exercise = [_items objectAtIndex:indexPath.row];
        //ExerciseDetailViewController *exerciseDetailViewController = [[ExerciseDetailViewController alloc] init];
        //[exerciseDetailViewController setExercise:exercise];
        
        SeriesViewController *seriesViewController = [[SeriesViewController alloc] init];
        [seriesViewController setExercise:exercise];
        
        //RNBlurModalView *modalView = [[RNBlurModalView alloc] initWithViewController:self.navigationController view:exerciseDetailViewController.view];
        //[exerciseDetailViewController setModalView:modalView];
        //[modalView setUserInteractionEnabled:TRUE];
        
        //[modalView show];
        
        [self presentViewController:seriesViewController animated:YES completion:^{
        
        }];
        
        //[self.navigationController pushViewController:exerciseDetailViewController animated:YES];
    } else if(_selectType == FitnessSelectTypePlan){
        TrainingPlan *trainingPlan = [_items objectAtIndex:indexPath.row];
        [self presentTrainigPlanDetailViewController:trainingPlan];
    }
}


#pragma mark UITableViewDataSource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    /*if(_selectType == FitnessSelectTypeExercise)
        return [UtilManager height:60];
    else
        return TAB_BAR_HEIGHT;
     */
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    /*if(_selectType == FitnessSelectTypeExercise)
     return [UtilManager height:60];
     else
     return TAB_BAR_HEIGHT;
     */
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(_selectType == FitnessSelectTypeExercise){
        UIView *exerciseHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:60])];
        [exerciseHeaderView setBackgroundColor:[UIColor clearColor]];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = _headerView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [exerciseHeaderView addSubview:blurEffectView];
    
        _segmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Anaeróbicos", nil),NSLocalizedString(@"Aeróbicos", nil)]];
        [_segmentControl setSelectedSegmentIndex:_segmentState];
        [_segmentControl addTarget:self action:@selector(changeExercisesView:) forControlEvents:UIControlEventValueChanged];
        [_segmentControl setCenter:CGPointMake(exerciseHeaderView.center.x, [UtilManager height:25])];
        [_segmentControl setTintColor:YELLOW_APP_COLOR];
    
        [exerciseHeaderView addSubview:_segmentControl];
    
        return exerciseHeaderView;
    
    }else{
        if(!_headerView)
        {
            _headerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:0], 0, WIDTH, TAB_BAR_HEIGHT)];
        
            UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
            UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
            blurEffectView.frame = _headerView.bounds;
            blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            
            //[_headerView addSubview:blurEffectView];
            
            [_headerView setBackgroundColor:[UIColor clearColor]];
            
            _favButton = [[UIButton alloc] initWithFrame:CGRectMake(0., [UtilManager height:5], [UtilManager width:40] * 0.8, [UtilManager width:40]* 0.8)];
            [_favButton setImage:[UIImage imageNamed:@"icon-fav-off"] forState:UIControlStateNormal];
            [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateSelected];
            [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateHighlighted];
            [_favButton setCenter:CGPointMake(_headerView.frame.size.width * 3/8,TAB_BAR_HEIGHT/2)];
            [_favButton setTag:1];
            [_favButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [_headerView addSubview:_favButton];
            
            _profileButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
            [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateSelected];
            [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateHighlighted];
            [_profileButton setImage:[UIImage imageNamed:@"icon-profile-off"] forState:UIControlStateNormal];
            [_profileButton setCenter:CGPointMake(_headerView.frame.size.width/8, _favButton.center.y)];
            [_profileButton setTag:0];
            [_profileButton setSelected:YES];
            [_profileButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [_headerView addSubview:_profileButton];
            
            _comunityButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
            [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateSelected];
            [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateHighlighted];
            [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-off"] forState:UIControlStateNormal];
            [_comunityButton setCenter:CGPointMake(5 * _headerView.frame.size.width/8, _favButton.center.y)];
            [_comunityButton setTag:2];
            [_comunityButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [_headerView addSubview:_comunityButton];
            
            _downloadButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
            [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateSelected];
            [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateHighlighted];
            [_downloadButton setImage:[UIImage imageNamed:@"icon-download-off"] forState:UIControlStateNormal];
            [_downloadButton setCenter:CGPointMake(7 * _headerView.frame.size.width/8, _favButton.center.y)];
            [_downloadButton setTag:3];
            [_downloadButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [_headerView addSubview:_downloadButton];
        }
        
        return _headerView;
        
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_selectType == FitnessSelectTypeExercise)
        return exerciseCellHeight;
    if(_selectType == FitnessSelectTypePlan)
        return trainingPlanCellHeight;
    return planCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_selectType == FitnessSelectTypeExercise)
        return exerciseCellHeight;
    if(_selectType == FitnessSelectTypePlan)
        return trainingPlanCellHeight;
    return planCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(_selectType == FitnessSelectTypePlan){
        FitnessPlanCell *cell = [_fitnessTableView dequeueReusableCellWithIdentifier:@"fitnessPlanCell"];
        TrainingPlan *trainingPlan = [_items objectAtIndex:indexPath.row];
        
        if(trainingPlan.isFavourite != nil && trainingPlan.isFavourite.boolValue){
            NSMutableAttributedString *myString=[[NSMutableAttributedString alloc] initWithString:trainingPlan.title
                                                                                       attributes:@{
                                                                                                    NSForegroundColorAttributeName:YELLOW_APP_COLOR,
                                                                                                    NSFontAttributeName : [UIFont fontWithName:BOLD_FONT size:12]
                                                                                                    }
                                                 ];
            
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image = [UIImage imageNamed:@"icon-fav-on"];
            attachment.bounds = CGRectMake(5., -2., cell.titleLabel.frame.size.height, cell.titleLabel.frame.size.height);
            NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
            [myString appendAttributedString:attachmentString];
            cell.titleLabel.attributedText = myString;
        }
        else {
            [[cell titleLabel] setText:trainingPlan.title];
        }
        
        [[cell contentLabel] setText:trainingPlan.desc];
        if(trainingPlan.trainingDay.count > 0) {
            [[cell daysLabel] setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)trainingPlan.trainingDay.count,NSLocalizedString(@"días", nil)]];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [[cell exercisesLabel] setText:[NSString stringWithFormat:@"%d %@",[TrainingPlan numberOfTrainings:trainingPlan],NSLocalizedString(@"Entrenamientos", nil)]];
        
        NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:trainingPlan.tags.count];
        
        for(Tag *tag in trainingPlan.tags){
            [tags addObject:tag];
        }
        
        [[cell tagView] setTags:tags];
        [[cell tagView] reloadTagSubviews];
        
        [cell setDelegate:self];
        
        [cell setCurrentIndex:indexPath.row];
        
        return cell;
    }
    else if(_selectType == FitnessSelectTypeExercise){
    
        ExerciseCell *cell = [_fitnessTableView dequeueReusableCellWithIdentifier:@"exerciseCell"];
        Exercise *exercise = [_items objectAtIndex:indexPath.row];
        
        cell.titleLabel.text = exercise.title;
        NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
        
        if (exercise.tag1 && exercise.tag1.length > 0) {
            Tag *tag1 = [Tag tagWithId:exercise.tag1];
            if(tag1) {
                [tags addObject:tag1];
            }
        }
        if(exercise.tag2 && exercise.tag2.length > 0){
            Tag *tag2 = [Tag tagWithId:exercise.tag2];
            if(tag2) {
                [tags addObject:tag2];
            }
        }
        if(exercise.tag3 && exercise.tag3.length > 0){
            Tag *tag3 = [Tag tagWithId:exercise.tag3];
            if(tag3) {
                [tags addObject:tag3];
            }
        }
        
        [[cell tagView] setTags:tags];
        [[cell tagView] reloadTagSubviews];
        
        UIImageView *imageView = cell.exerciseImageView;
        
        NSURLRequest *exerciseImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_EXERCISES,exercise.picture]]
                                                          cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                      timeoutInterval:60];
        
        [cell.exerciseImageView setImageWithURLRequest:exerciseImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-small"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            [imageView setImage:image];
            [imageView setContentMode:UIViewContentModeScaleAspectFill];
            [[imageView layer] setMasksToBounds:YES];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            NSLog(@"%@",error);
            [imageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
        }];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
        
        return cell;
    } else if(_selectType == FitnessSelectTyeWorkout){
        FitnessCell *cell = [_fitnessTableView dequeueReusableCellWithIdentifier:@"fitnessCell"];
        
        [cell setCurrentIndex:indexPath.row];
        [cell setDelegate:self];
        
        Training *training = [_items objectAtIndex:indexPath.row];
        
        if(training.isFavourite.boolValue){
            NSMutableAttributedString *myString=[[NSMutableAttributedString alloc] initWithString:training.title
                                                                                       attributes:@{
                                                                                                    NSForegroundColorAttributeName:YELLOW_APP_COLOR,
                                                                                                    NSFontAttributeName : [UIFont fontWithName:SEMIBOLD_FONT size:15]
                                                                                                    }
                                                 ];
            
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image = [UIImage imageNamed:@"icon-fav-on"];
            attachment.bounds = CGRectMake(5., -2., cell.titleLabel.frame.size.height, cell.titleLabel.frame.size.height);
            NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
            [myString appendAttributedString:attachmentString];
            cell.titleLabel.attributedText = myString;
        }else
            [[cell titleLabel] setText:training.title];
        
        
        
        NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
        
        for(Tag *tag in training.tags){
            [tags addObject:tag];
        }
        
        int secs = 0;
        NSInteger kCal = 0;
        
        for(TrainingExercise *trainingExercise in training.trainingExercises){
            secs = [TrainingExercise getExerciseTime:trainingExercise] + secs;
            
            for(Serie *serie in trainingExercise.series){
                kCal = kCal + [Serie kCalInSerie:serie];
            }
        }
        
        [[cell kCalLabel] setText:[NSString stringWithFormat:@"%ld kCal",kCal]];
        
        int minutes = secs / 60;
        
        if(minutes<60)
            [cell.timeLabel setText:[NSString stringWithFormat:@"%d min",minutes]];
        else{
            int hour = minutes/60;
            minutes = minutes/60;
            
            [cell.timeLabel setText:[NSString stringWithFormat:@"%d h %d min",hour,minutes]];
        }
       
        [[cell tagView] setTags:tags];
        [[cell tagView] reloadTagSubviews]; 
        
        [[cell calendarImageView] setHidden:TRUE];
        [[cell daysLabel] setHidden:TRUE];
        [[cell contentLabel] setText:training.content];
        
        long count = 0;
        
        if(training.trainingExercises.count >= training.trainingExercisesCount.intValue)
            count = training.trainingExercises.count ;
        else
            count = training.trainingExercisesCount.intValue;
        
        [[cell exercisesLabel] setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)count,NSLocalizedString(@"Ejercicios", nil)]];
        
        return cell;
    }
    
    return nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_selectType == FitnessSelectTyeWorkout || _selectType == FitnessSelectTypePlan)
        return TRUE;
    else
        return FALSE;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_selectType == FitnessSelectTyeWorkout && _workoutSelectType != WorkoutSelectTypeUser) {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete;

}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
    
        switch (_selectType) {
            case FitnessSelectTyeWorkout:
            {
                if(_workoutSelectType == WorkoutSelectTypeUser){
                    Training *currentTraining = [_items objectAtIndex:indexPath.row];
                    [_executive deleteTraining:currentTraining];
                }
            }
                break;
            case FitnessSelectTypePlan:
            {
                if(_trainingPlanSelectType == TrainingPlanSelectTypeUser){
                    TrainingPlan *trainingPlan = [_items objectAtIndex:indexPath.row];
                    [_executive deleteTrainingPlan:trainingPlan];
                }
            }
                break;
                
            default:
                break;
        }
        
        /*
        if(_selectType == FitnessSelectTyeWorkout){
            [tableView beginUpdates];
            NSUInteger row = [indexPath row];
            NSMutableArray *itemsAux = [_items mutableCopy];
            
            [itemsAux removeObjectAtIndex:row];
            
            //_items = []
            
            _items =
            
            [_items removeObjectAtIndex:row];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView endUpdates];
        }
         */
    }
}

#pragma mark WebServiceDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{

}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    if(webServiceType == WebServiceTypeGetTraining){
        
        if(_isFromPush){
            NSDictionary *updateTrainingDic = (NSDictionary *)object;
            Training *updateTraining = [Training trainingWithId:[updateTrainingDic valueForKey:@"trainingId"]];
            
            if(!updateTraining)
                updateTraining = [Training trainingWithId:[updateTrainingDic valueForKey:@"_id"]];
        
            [self presentWorkOutViewController:updateTraining];
        }
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
}

#pragma mark - Fitness Display Delegate

- (void)didLoadItems:(RLMResults*)items
      forFitnessType:(FitnessSelectType)fitnessType {
    
    [_planButton setSelected:NO];
    [_workoutButton setSelected:NO];
    [_exercisesButton setSelected:NO];
    
    [_planLabel setTextColor:[UIColor whiteColor]];
    [_exercisesLabel setTextColor:[UIColor whiteColor]];
    [_workoutLabel setTextColor:[UIColor whiteColor]];
    
    switch (fitnessType) {
        case FitnessSelectTypePlan:
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"PLANES DE ENTRENAMIENTO", nil)];
            [_planButton setSelected:YES];
            [_headerView setHidden:NO];
            [_segmentControlView setHidden:YES];
            [_planLabel setTextColor:YELLOW_APP_COLOR];
            if(items.count == 0) {
                [_warningLabel setText:NSLocalizedString(@"Todavía no has creado ningún plan", nil)];
            }
            else {
                [_warningLabel setText:NSLocalizedString(@"", nil)];
            }
            break;
        case FitnessSelectTyeWorkout:
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"ENTRENAMIENTOS", nil)];
            [_workoutButton setSelected:YES];
            [_headerView setHidden:NO];
            [_segmentControlView setHidden:YES];
            [_workoutLabel setTextColor:YELLOW_APP_COLOR];
            if(items.count == 0)
                [_warningLabel setText:NSLocalizedString(@"Todavía no hay ningún entrenamiento", nil)];
            else
                [_warningLabel setText:NSLocalizedString(@"", nil)];
            break;
        case FitnessSelectTypeExercise:
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"EJERCICIOS", nil)];
            [_exercisesButton setSelected:YES];
            [_headerView setHidden:YES];
            [_segmentControlView setHidden:NO];
            [_exercisesLabel setTextColor:YELLOW_APP_COLOR];
            if(items.count == 0)
                [_warningLabel setText:NSLocalizedString(@"Todavía no hay ejercicios de este tipo", nil)];
            else
                [_warningLabel setText:NSLocalizedString(@"", nil)];
            break;
        default:
            break;
    }

    _items = items;
    [_fitnessTableView reloadData];
}

- (void)didDeletedTrainingPlan:(TrainingPlan*)trainingPlan {
    [WebServiceManager getTrainingPlansWithCompletionBlock:^(BOOL isSuccess) {
        [_executive loadItemsForFitnessType:_selectType
                          workoutSelectType:_workoutSelectType
                     trainingPlanSelectType:_trainingPlanSelectType
                       exerciseSegmentState:_segmentState
                                 searchText:_keyword];
    }];
}

- (void)didDeletedTraining:(Training*)training {
    [WebServiceManager getTrainingsWithCompletionBlock:^(BOOL isSuccess) {
        [_executive loadItemsForFitnessType:_selectType
                          workoutSelectType:_workoutSelectType
                     trainingPlanSelectType:_trainingPlanSelectType
                       exerciseSegmentState:_segmentState
                                 searchText:_keyword];
    }];
}

- (void)reloadItems {
    [WebServiceManager getTrainingsWithCompletionBlock:^(BOOL isSuccess) {
        [WebServiceManager getTrainingPlansWithCompletionBlock:^(BOOL isSuccess) {
            [_executive loadItemsForFitnessType:_selectType
                              workoutSelectType:_workoutSelectType
                         trainingPlanSelectType:_trainingPlanSelectType
                           exerciseSegmentState:_segmentState
                                     searchText:_keyword];
        }];
    }];
}

#pragma mark - SearchBarDelegate Methods

- (void)didSearchButtonClicked:(UISearchBar*)searchBar {
}

- (void)didSearchBar:(UISearchBar*)searchBar TextChange:(NSString*)searchText {
    _keyword = searchText;
    [_executive loadItemsForFitnessType:_selectType
                      workoutSelectType:_workoutSelectType
                 trainingPlanSelectType:_trainingPlanSelectType
                   exerciseSegmentState:_segmentState
                             searchText:_keyword];
}

#pragma mark - OptionsViewControllerDelegate

- (void)didPressAddToCalendar:(RLMObject *)rlmObject {
    if (![rlmObject isKindOfClass:[TrainingPlan class]]) {
        return;
    }
    
    [self.view endEditing:YES];
    
    if(_datePicker) {
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - 216, self.view.frame.size.width, 216)];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    
    [_datePicker setDate:[NSDate date]];
    
    _datePicker.backgroundColor = [UIColor whiteColor];
    _datePicker.userInteractionEnabled = YES;
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_datePicker.frame.size.width - 80., _datePicker.frame.origin.y, 80., 24.)];
    [_acceptButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptDatePickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_datePicker];
    [self.view addSubview:_acceptButton];
}

- (void)acceptDatePickerAction {
    if(_acceptButton && _datePicker) {
        TrainingPlan * trainingPlan = _items[_selectedIndex];
        [_executive addTrainingPlan:trainingPlan toDate:_datePicker.date];
        
        [_acceptButton removeFromSuperview];
        _acceptButton = nil;
        
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
}

- (void)didPressSendFood:(RLMObject *)rlmObject {
    _userSelectorViewController = [[UserSelectorViewController alloc] init];
    _userSelectorViewController.rlmObject = rlmObject;
    _userSelectorViewController.delegate = self;
    _userSelectorViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:_userSelectorViewController animated:YES completion:nil];
}

- (void)didPressShareFood:(RLMObject *)rlmObject {
    [_executive shareContent:rlmObject];
}

#pragma mark - UserSelectorViewControllerDelegate

- (void)didPressOKWithSelectedResults:(NSArray*)selectedResults
                           andContent:(RLMObject*)content {
    
    UserMeta * userMeta = [selectedResults firstObject];
    [_executive sendContent:content
                    toToken:userMeta.token];
}

@end
