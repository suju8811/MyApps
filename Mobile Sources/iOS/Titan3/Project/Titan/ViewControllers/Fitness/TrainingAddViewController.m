//
//  TrainingAddViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingAddViewController.h"
#import "WorkoutDetailViewController.h"
#import "SelectorTableViewController.h"
#import "SeriesViewController.h"
#import "WorkoutCell.h"
#import "MRProgress.h"
#import "AlertCustomView.h"
#import "AppDelegate.h"
#import "PYSearch.h"
#import "SelectTagView.h"
#import "TrainingAddExecutive.h"
#import "WebServiceManager.h"

@interface TrainingAddViewController ()<UITableViewDelegate, UITableViewDataSource, SelectorTableViewDelegate, UITextFieldDelegate, AlertCustomViewDelegate, SeriesViewControllerDelegate, PYSearchViewControllerDelegate, PYSearchViewControllerDataSource, UITextViewDelegate, SelectTagViewDelegate, TrainingAddDisplay>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITextField *nameTextField;
@property (nonatomic, strong) UITextView *contentTextView;
@property (nonatomic, strong) UITextView *noteTextView;
@property (nonatomic, strong) UITextField *tagsTextField;
@property (nonatomic, strong) UITextField *energyTextField;
@property (nonatomic, strong) UITextField *durationTextField;
@property (nonatomic, strong) UIButton *privacityButton;
@property (nonatomic, strong) UILabel *privacityLabel;

@property (nonatomic, strong) UITableView *trainingTableView;

@property (nonatomic, strong) UIButton *addExerciseButton;
@property (nonatomic, strong) UIButton *createTrainingButton;

@property (nonatomic, strong) TrainingExercise *trainingExercise;

@property (nonatomic, strong) SelectorTableViewController *selectorTableViewController;
@property (nonatomic, strong) AlertCustomView *alertCustomView;

@property (nonatomic, assign) NSInteger trainingSelected;
@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) SelectTagView *selectTagView;
@property (nonatomic, strong) NSMutableArray *tags;
@property (nonatomic, strong) NSMutableArray *selectedTags;

@property (nonatomic, strong) TrainingAddExecutive * executive;

@end

@implementation TrainingAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeAction) name:NAV_RIGHT1_ACTION object:nil];
    
    if(!_selectTraining && !_editTraining) {
        _selectTraining = [[Training alloc] init];
        [_selectTraining setTrainingId:[UtilManager uuid]];
    }
    
    _tags = [Tag getTagsWithType:@"type_2" andCollection:@"Training"];
    
    [self configureView];
    
    [_nameTextField becomeFirstResponder];
    
    _executive = [[TrainingAddExecutive alloc] initWithDisplay:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(_selectorTableViewController.presentWorkoutDetail){
        
        _selectorTableViewController.presentWorkoutDetail = FALSE;
        int order = 0;
        
        RLMRealm *realm = [AppContext currentRealm];
        
        NSMutableArray *trainingExercisesAux = [[NSMutableArray alloc] initWithCapacity:_selectorTableViewController.selectsExercises.count];
        
        for(Exercise *exercise in _selectorTableViewController.selectsExercises){
            TrainingExercise *trainingExercise = [[TrainingExercise alloc] init];
            
            [trainingExercise setIsSelected:[NSNumber numberWithBool:FALSE]];
            
            [trainingExercise setOrder:[NSNumber numberWithInt:order]];
            [trainingExercise.exercises addObject:exercise];
            [trainingExercise setTitle:exercise.title];
            [trainingExercise setTrainingId:[UtilManager uuid]];
            [trainingExercise setOwner:[[AppContext sharedInstance] userId]];
            
            [realm transactionWithBlock:^{
                [exercise setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            
            [trainingExercisesAux addObject:trainingExercise];
            
            order++;
        }
        
        if (_editTraining) {
            [realm transactionWithBlock:^{
                [_editTraining.trainingExercises removeAllObjects];
                [_editTraining.trainingExercises addObjects:trainingExercisesAux];
            }];
        }
        else {
            [_selectTraining.trainingExercises addObjects:trainingExercisesAux];
        }
        
        NSInteger count = [Exercise getExercises].count;
        
        [self createTraining];
    }
    else {
        [_trainingTableView reloadData];
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)configureView{
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _nameTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:10], NAV_HEIGHT, WIDTH - [UtilManager width:20], UITEXTFIELD_HEIGHT)];
    
    [_nameTextField setDelegate:self];
    [_nameTextField setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]];
    [_nameTextField setTextColor:GRAY_REGISTER_FONT];
    [_nameTextField setBackgroundColor:[UIColor clearColor]];
    [_nameTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    _nameTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Nombre", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName:[UIColor grayColor],
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]
                                                 }
    ];
    
    if(_editTraining) {
        _nameTextField.text = _editTraining.title;
    }
    
    [self.view addSubview:_nameTextField];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _nameTextField.frame.origin.y + _nameTextField.frame.size.height , WIDTH - [UtilManager width:40], 1)];
    [separatorView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self.view addSubview:separatorView];
    
    NSMutableArray *tags = [[NSMutableArray alloc] init];
    
    if(_editTraining){
        for(Tag *tag in _editTraining.tags){
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                if([tag.status isEqualToString:@"Off"])
                    [tag setStatus:@"On"];
                else
                    [tag setStatus:@"Off"];
            }];
            [tags addObject:tag];
        }
    }
    
    _tagView = [[TagView alloc] initWithFrame:CGRectMake([UtilManager width:0], separatorView.frame.origin.y + separatorView.frame.size.height + [UtilManager height:10], WIDTH, [UtilManager height:30]) andTags:tags withTagsControlMode:TagsControlModeEdit];
    [_tagView setBackgroundColor:[UIColor clearColor]];
    [_tagView setUserInteractionEnabled:YES];
    [_tagView setTags:_tags];
    [_tagView reloadTagSubviews];
    
    _selectTagView = [[SelectTagView alloc] initWithFrame:CGRectMake([UtilManager width:10], separatorView.frame.origin.y + separatorView.frame.size.height + [UtilManager height:10], WIDTH - [UtilManager width:20], [UtilManager height:30]) andTags:_tags];
    [_selectTagView setDelegate:self];
    [_tagView setDelegate:self];
    
    [self.view addSubview:_selectTagView];
    
    _contentTextView = [[UITextView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:10], _nameTextField.frame.size.width, [UtilManager height:120])];
    [_contentTextView setTextColor:_nameTextField.textColor];
    [_contentTextView setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_contentTextView setTextAlignment:NSTextAlignmentJustified];
    [_contentTextView setText:NSLocalizedString(@"Descripción", nil)];
    [_contentTextView setFont:[UIFont fontWithName:ITALIC size:18]];
    [_contentTextView setBackgroundColor:[UIColor clearColor]];
    [[_contentTextView layer] setBorderWidth:1];
    [[_contentTextView layer] setBorderColor:BLACK_HEADER_APP_COLOR.CGColor];
    
    if(_editTraining) {
        _contentTextView.text = _editTraining.desc;
    }
    
    [_contentTextView setDelegate:self];
    
    UIToolbar *toolbarWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //UIBarButtonItem *barWeigthButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resetView)];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetView)];
    [barButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarWeigth setItems:[NSArray arrayWithObjects:flexibleWeigthSpace, barButtonItem, nil]];
    _contentTextView.inputAccessoryView = toolbarWeigth;
    
    [self.view addSubview:_contentTextView];
    
    _noteTextView = [[UITextView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _contentTextView.frame.origin.y + _contentTextView.frame.size.height + [UtilManager height:10], _nameTextField.frame.size.width, [UtilManager height:180])];
    [_noteTextView setTextColor:_nameTextField.textColor];
    [_noteTextView setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_noteTextView setTextAlignment:NSTextAlignmentJustified];
    [_noteTextView setText:NSLocalizedString(@"Notas", nil)];
    [_noteTextView setFont:[UIFont fontWithName:ITALIC size:18]];
    [_noteTextView setBackgroundColor:[UIColor clearColor]];
    [[_noteTextView layer] setBorderWidth:1];
    [[_noteTextView layer] setBorderColor:BLACK_HEADER_APP_COLOR.CGColor];
    
    if(_editTraining) {
        _noteTextView.text = _editTraining.punctation;
    }
    
    [_noteTextView setDelegate:self];
    
    UIToolbar *toolbarNoteWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarNoteWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleNoteWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //UIBarButtonItem *barWeigthButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resetView)];
    UIBarButtonItem *barNoteButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetView)];
    [barNoteButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barNoteButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarNoteWeigth setItems:[NSArray arrayWithObjects:flexibleNoteWeigthSpace, barNoteButtonItem, nil]];
    _noteTextView.inputAccessoryView = toolbarNoteWeigth;
    
    [self.view addSubview:_noteTextView];
    
    _privacityButton = [[UIButton alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x,_noteTextView.frame.origin.y + _noteTextView.frame.size.height + [UtilManager height:10] , [UtilManager width:40], [UtilManager width:40])];
    [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
    [_privacityButton addTarget:self action:@selector(privacityAction) forControlEvents:UIControlEventTouchUpInside];
    
//    [self.view addSubview:_privacityButton];
    
    _privacityLabel = [[UILabel alloc] initWithFrame:CGRectMake(_privacityButton.frame.origin.x + _privacityButton.frame.size.width + [UtilManager width:10], _privacityButton.frame.origin.y, [UtilManager width:240], _privacityButton.frame.size.height)];
    [_privacityLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    [_privacityLabel setTextAlignment:NSTextAlignmentLeft];
    [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    [_privacityLabel setTextColor:[UIColor whiteColor]];
    
//    [self.view addSubview:_privacityLabel];
    
    if([_editTraining.privacity isEqualToString:@"public"]){
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    }else{
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-closed"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Privado", nil)];
    }
    
    _trainingTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., _nameTextField.frame.origin.y + _nameTextField.frame.size.height +1, WIDTH, HEIGHT - [UtilManager height:161] - _nameTextField.frame.size.height) style:UITableViewStylePlain];
    [_trainingTableView setBackgroundColor:[UIColor clearColor]];
    [_trainingTableView setDelegate:self];
    [_trainingTableView setDataSource:self];
    [_trainingTableView setBounces:FALSE];
    [_trainingTableView registerClass:[WorkoutCell class] forCellReuseIdentifier:@"exerciseCell"];
    [_trainingTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    //[self.view addSubview:_trainingTableView];
    
   //_addExerciseButton = [[UIButton alloc] initWithFrame:CGRectMake(0., HEIGHT - [UtilManager height:161], WIDTH, [UtilManager height:80])];
    _addExerciseButton = [[UIButton alloc] initWithFrame:CGRectMake(0., HEIGHT - [UtilManager height:80], [UtilManager width:240], [UtilManager height:80])];
    [_addExerciseButton setTitle:NSLocalizedString(@"AÑADIR EJERCICIOS", nil) forState:UIControlStateNormal];
    [[_addExerciseButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:18]];
    [_addExerciseButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [_addExerciseButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
    [_addExerciseButton setBackgroundColor:[UIColor clearColor]];
    [_addExerciseButton setCenter:CGPointMake(self.view.center.x, _addExerciseButton.center.y)];
    
    //[self.view addSubview:_addExerciseButton];
    
    _createTrainingButton = [[UIButton alloc] initWithFrame:CGRectMake(0., HEIGHT - [UtilManager height:80], WIDTH, [UtilManager height:80])];
    [_createTrainingButton setTitle:NSLocalizedString(@"CREAR ENTRENAMIENTO", nil) forState:UIControlStateNormal];
    [[_createTrainingButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:18]];
    [_createTrainingButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [_createTrainingButton addTarget:self action:@selector(createTraining) forControlEvents:UIControlEventTouchUpInside];
    [_createTrainingButton setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    //[self.view addSubview:_createTrainingButton];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, NAV_HEIGHT - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    if(_editTraining)
        [titleLabel setText:NSLocalizedString(@"EDITAR ENTRENAMIENTO", nil)];
    else
        [titleLabel setText:NSLocalizedString(@"NUEVO ENTRENAMIENTO", nil)];
    [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:titleLabel];
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50], [UtilManager height:20], [UtilManager width:40] * 0.88, [UtilManager width:40] * 0.8)];
    [okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
    [okButton setCenter:CGPointMake(okButton.center.x, titleLabel.center.y)];
    
    [self.view addSubview:okButton];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:20.], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton setHidden:FALSE];
    [backButton setCenter:CGPointMake(backButton.center.x, okButton.center.y)];
    
    [self.view addSubview:backButton];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didPressBackButton)];
    [swipeGesture setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:swipeGesture];
}

#pragma mark - Actions

- (void)didPressBackButton {
    
    if (_editTraining) {
        [_executive setTagState:NO forTraining:_editTraining];
    }
    
    if (_selectTraining) {
        [_executive setTagState:NO forTraining:_selectTraining];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)privacityAction{
    if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]]) {
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-closed"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Privado", nil)];
    }
    else {
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    }
}

- (void)resetView{
    [_contentTextView resignFirstResponder];
    [_noteTextView resignFirstResponder];
}

- (void)didPressOKButton{
    if(_editTraining){
//        _selectorTableViewController = [[SelectorTableViewController alloc] init];
//        [_selectorTableViewController setDelegate:self];
//        [_selectorTableViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//        [_selectorTableViewController setSelectTraining:_editTraining];
//        
//        [self presentViewController:_selectorTableViewController animated:YES completion:^{
//        }];
        [_executive updateTraining:_editTraining];
    } else {
        _selectorTableViewController = [[SelectorTableViewController alloc] init];
        [_selectorTableViewController setDelegate:self];
        [_selectorTableViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [_selectorTableViewController setSelectTraining:_selectTraining];
        
        [self presentViewController:_selectorTableViewController animated:YES completion:^{
        }];
    }
}


- (void)createTraining{
    if(_editTraining) {
        [_executive updateTraining:_editTraining];
    } else {
        [_executive postTraining:_selectTraining];
    }
}

- (void)closeAction{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark SelectTagViewDelegate Methods

- (void)actionDidPushReturnTags:(NSMutableArray *)selectedTags{
    _selectedTags = nil;
    _selectedTags = [[NSMutableArray alloc] init];
    _selectedTags = selectedTags;
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _trainingSelected = indexPath.row;
    
    //if(_alertCustomView){
    //    [_alertCustomView removeFromSuperview];
    //    _alertCustomView = nil;
    //}
    /*
    _alertCustomView = [[AlertCustomView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, HEIGHT)];
    [_alertCustomView setDelegate:self];
    
    [[_alertCustomView titleLabel] setText:NSLocalizedString(@"¿Qué desea añadir a al entrenamiento?", nil)];
    [[_alertCustomView subtitleLabel] setText:@""];
    
    [[_alertCustomView acceptButton] setTitle:NSLocalizedString(@"Serie", nil) forState:UIControlStateNormal];
    [[_alertCustomView cancelButton] setTitle:NSLocalizedString(@"Super Serie", nil) forState:UIControlStateNormal];
    
    double delayInSeconds = 0.4;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [[[AppDelegate sharedDelegate] window].rootViewController.view addSubview:_alertCustomView];
    });
     */
    TrainingExercise *trainingExercise = [_selectTraining.trainingExercises objectAtIndex:_trainingSelected];
    
    SeriesViewController *seriesViewController = [[SeriesViewController alloc] init];
    [seriesViewController setCurrentExercise:trainingExercise];
    [seriesViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [seriesViewController setDelegate:self];
    [seriesViewController setIndex:_trainingSelected];
    
    [self presentViewController:seriesViewController animated:YES completion:^{
        
    }];
    
}

#pragma mark UITableViewDataSource Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WorkoutCell * cell = [_trainingTableView dequeueReusableCellWithIdentifier:@"exerciseCell"];
    
    TrainingExercise *trainingExercise = [_selectTraining.trainingExercises objectAtIndex:indexPath.row];
    
    [[cell titleLabel] setText:trainingExercise.title];
    
    if(trainingExercise.series.count == 0)
        [[cell seriesLabel] setText:NSLocalizedString(@"Sin Series", nil)];
    else{
        NSMutableString *seriesString = [[NSMutableString alloc] init];
        
        if(trainingExercise.series.count > 0){
            for(Serie *serie in trainingExercise.series){
                if(seriesString.length == 0)
                    seriesString = [NSMutableString stringWithFormat:@"%d ",serie.reps.intValue];
                else
                    [seriesString appendString:[NSString stringWithFormat:@"- %d ",serie.reps.intValue]];
            }
            
            if(seriesString.length > 0)
                [[cell seriesLabel] setText:seriesString];
        }
    }
    
    Exercise *exercise = [trainingExercise.exercises objectAtIndex:0];
    [[cell titleLabel] setText:exercise.title];
    
    NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
    
    if(exercise.tag1 && exercise.tag1.length > 0){
        Tag *tag1 = [Tag tagWithId:exercise.tag1];
        [tags addObject:tag1];
    }
    if(exercise.tag2 && exercise.tag2.length > 0){
        Tag *tag2 = [Tag tagWithId:exercise.tag2];
        [tags addObject:tag2];
    }
    if(exercise.tag3 && exercise.tag3.length > 0){
        Tag *tag3 = [Tag tagWithId:exercise.tag3];
        [tags addObject:tag3];
    }
    
    [[cell tagView] setTags:tags];
    [[cell tagView] reloadTagSubviews];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _selectTraining.trainingExercises.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [UtilManager height:140];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [UtilManager height:140];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:60])];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH-[UtilManager width:60], [UtilManager height:20], [UtilManager width:40], [UtilManager width:40])];
    [closeButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
    [closeButton setBackgroundColor:[UIColor redColor]];
    
    return headerView;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return TRUE;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canFocusRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
         [tableView beginUpdates];
         NSUInteger row = [indexPath row];
         [[_selectTraining trainingExercises] removeObjectAtIndex:row];
         [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
         [tableView endUpdates];
    } else {
        NSLog(@"YEPAAA");
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView
targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath
       toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    return proposedDestinationIndexPath;
}

- (void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    NSInteger sourceRow = sourceIndexPath.row;
    NSInteger destRow = destinationIndexPath.row;
    //id object = [minutesItems objectAtIndex:sourceRow];
    
    //[minutesItems removeObjectAtIndex:sourceRow];
    //[minutesItems insertObject:object atIndex:destRow];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
   
}

#pragma mark UITextFieldDelegat Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}

#pragma mark UITextViewDelegate Methods

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
    return true;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if([[textView text] isEqualToString:NSLocalizedString(@"Descripción", nil)])
        [_contentTextView setText:@""];
    if([[textView text] isEqualToString:NSLocalizedString(@"Notas", nil)])
        [_noteTextView setText:@""];
    
    return  true;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    return TRUE;
}

#pragma mark - Training Add Display Delegate

- (TrainingAddDisplayContext*)displayContext {
    TrainingAddDisplayContext * context = [[TrainingAddDisplayContext alloc] init];
    
    context.title = _nameTextField.text;
    context.content = _contentTextView.text;
    context.note = _noteTextView.text;
    context.selectedTags = _selectedTags;
    if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]]) {
        context.privacy = @"public";
    }
    else {
        context.privacy = @"private";
    }
    
    return context;
}

- (void)didTrainingPost:(Training *)training {
    [WebServiceManager getTrainingsWithCompletionBlock:^(BOOL isSuccess) {
        Training * createdTraining = [Training trainingWithId:training.trainingId];
        if(_fitnessViewController) {
            [_fitnessViewController setTraining:createdTraining];
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (void)didTrainingUpdate:(Training *)training {
    if(_fitnessViewController) {
        _fitnessViewController.training = training;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        if (_workoutDetailViewControllerDelegate) {
            [_workoutDetailViewControllerDelegate didUpdatedTraining:training];
        }
    }];
}

#pragma mark SelectorTableViewDelegate Methods

- (void)returnData:(NSMutableArray *)returnData withType:(SelectorType)selectorType{
    
    if(returnData.count > 0){
        
        int order = 0;
        
        for(Exercise *exercise in returnData){
            TrainingExercise *trainingExercise = [[TrainingExercise alloc] init];
            
            [trainingExercise setOrder:[NSNumber numberWithInt:order]];
            [trainingExercise.exercises addObject:exercise];
            [trainingExercise setTitle:exercise.title];
            [trainingExercise setTrainingId:[UtilManager uuid]];
            [trainingExercise setOwner:[[AppContext sharedInstance] userId]];
            
            [_selectTraining.trainingExercises addObject:trainingExercise];
            
            order++;
        }
    }else{
        [_trainingTableView reloadData];
    }
}

#pragma mark AlertCustomViewDelegate Methods

- (void)cancelAlertViewDidselected{
    [_alertCustomView removeFromSuperview];
    _alertCustomView = nil;
}

- (void)confirmAlertViewDidSelected{
    
    [_alertCustomView removeFromSuperview];
    _alertCustomView = nil;
    
    TrainingExercise *trainingExercise = [_selectTraining.trainingExercises objectAtIndex:_trainingSelected];

    SeriesViewController *seriesViewController = [[SeriesViewController alloc] init];
    [seriesViewController setCurrentExercise:trainingExercise];
    [seriesViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [seriesViewController setDelegate:self];
    [seriesViewController setIndex:_trainingSelected];
    
    [self presentViewController:seriesViewController animated:YES completion:^{
        
    }];
}

- (void)dismissAlertViewSelected{
    [_alertCustomView removeFromSuperview];
    _alertCustomView = nil;
    
    TrainingExercise *trainingExercise = [_selectTraining.trainingExercises objectAtIndex:_trainingSelected];
}

#pragma mark SeriesViewControllerDelegate Methods

- (void)backCurrentExercise:(TrainingExercise *)currentExercise inIndex:(NSInteger)index{
    [[_selectTraining trainingExercises] replaceObjectAtIndex:index withObject:currentExercise];
    [_trainingTableView reloadData];
}

#pragma mark PYSearchDataSource Methods

//- (UITableViewCell *)searchSuggestionView:(UITableView *)searchSuggestionView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//}

/**
 Return number of rows in section.
 
 @param searchSuggestionView    view which display search suggestions
 @param section                 index of section
 @return number of rows in section
// */
//- (NSInteger)searchSuggestionView:(UITableView *)searchSuggestionView numberOfRowsInSection:(NSInteger)section{
//    
//}

/**
 Return number of sections in search suggestion view.
 
 @param searchSuggestionView    view which display search suggestions
 @return number of sections
 */
//- (NSInteger)numberOfSectionsInSearchSuggestionView:(UITableView *)searchSuggestionView{
//    return 1;
//}

/**
 Return height for row.
 
 @param searchSuggestionView    view which display search suggestions
 @param indexPath               indexPath of row
 @return height of row
 */
//- (CGFloat)searchSuggestionView:(UITableView *)searchSuggestionView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//}


@end
