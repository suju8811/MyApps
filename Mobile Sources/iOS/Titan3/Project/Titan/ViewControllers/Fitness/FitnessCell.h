//
//  FitnessCell.h
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagView.h"

@protocol FitnessCellDelegate;

@interface FitnessCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) UILabel *contentLabel;

@property (nonatomic, strong) UILabel *exercisesLabel;
@property (nonatomic, strong) UILabel *daysLabel;
@property (nonatomic, strong) UILabel *kCalLabel;
@property (nonatomic, strong) UIImageView *calendarImageView;

@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIView *separatorView;

@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) UIView *panView;
    
@property (nonatomic, strong) UIButton *menuButton;
@property (nonatomic, strong) UIImageView *checkImageView;

@property (nonatomic, assign) id<FitnessCellDelegate>delegate;

@end

@protocol FitnessCellDelegate <NSObject>

- (void)optionsTrainingDidPushInIndex:(NSInteger)index;

@end
