//
//  TimeCell.m
//  Titan
//
//  Created by Manuel Manzanera on 5/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TimeCell.h"
#import "UtilManager.h"
#import "AppContext.h"
#import "AppConstants.h"

@implementation TimeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:80])];
        [_timeLabel setTextColor:[UIColor whiteColor]];
        [_timeLabel setTextAlignment:NSTextAlignmentCenter];
        [_timeLabel setText:@"00:00"];
        [_timeLabel setBackgroundColor:[UIColor clearColor]];
        [_timeLabel setFont:[UIFont fontWithName:LIGHT_FONT size:32]];
        
        [self addSubview:_timeLabel];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:80])];
        [_timeLabel setTextColor:[UIColor whiteColor]];
        [_timeLabel setTextAlignment:NSTextAlignmentCenter];
        [_timeLabel setText:@"00:00"];
        [_timeLabel setBackgroundColor:[UIColor clearColor]];
        [_timeLabel setFont:[UIFont fontWithName:LIGHT_FONT size:32]];
        
        [self addSubview:_timeLabel];
    }
    
    return self;
}

@end
