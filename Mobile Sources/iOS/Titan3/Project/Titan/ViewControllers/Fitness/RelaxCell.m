//
//  RelaxCell.m
//  Titan
//
//  Created by Manuel Manzanera on 26/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RelaxCell.h"
#import "AppContext.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation RelaxCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        _dayLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:5], [UtilManager height:10], [UtilManager width:40], [UtilManager height:30])];
        [_dayLabel setText:@"Día 1"];
        [_dayLabel setTextColor:YELLOW_APP_COLOR];
        [_dayLabel setFont:[UIFont fontWithName:BOLD_FONT size:15]];
        [_dayLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_dayLabel];
        
        _circleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_dayLabel.frame.origin.x + _dayLabel.frame.size.width + [UtilManager width:5], _dayLabel.frame.origin.y, [UtilManager height:15], [UtilManager height:15])];
        [[_circleImageView layer] setBorderColor:YELLOW_APP_COLOR.CGColor];
        [[_circleImageView layer] setBorderWidth:3];
        [[_circleImageView layer] setCornerRadius:_circleImageView.frame.size.height/2];
        [_circleImageView setCenter:CGPointMake(_circleImageView.center.x, _dayLabel.center.y)];
        
        [[self contentView] addSubview:_circleImageView];
        
        _verticalSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(0, _circleImageView.frame.size.height + _circleImageView.frame.origin.y + [UtilManager height:5], 1, [UtilManager height:25])];
        [_verticalSeparatorView setBackgroundColor:[UIColor lightGrayColor]];
        [_verticalSeparatorView setCenter:CGPointMake(_circleImageView.center.x, _verticalSeparatorView.center.y)];
        
        [[self contentView] addSubview:_verticalSeparatorView];
        
        _titleRelaxLabel = [[UILabel alloc] initWithFrame:CGRectMake(_circleImageView.frame.origin.x + _circleImageView.frame.size.width + [UtilManager width:15], _dayLabel.frame.origin.y, WIDTH - [UtilManager width:20], [UtilManager height:55])];
        [_titleRelaxLabel setText:NSLocalizedString(@"Descanso", nil)];
        [_titleRelaxLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleRelaxLabel setTextColor:[UIColor whiteColor]];
        [_titleRelaxLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [_titleRelaxLabel setAdjustsFontSizeToFitWidth:YES];
        [_titleRelaxLabel setCenter:CGPointMake(_titleRelaxLabel.center.x, _dayLabel.center.y)];
        
        [[self contentView] addSubview:_titleRelaxLabel];
        
        _separatorRelaxView = [[UIView alloc] initWithFrame:CGRectMake(0., [UtilManager height:59], WIDTH, 1)];
        [_separatorRelaxView setBackgroundColor:GRAY_REGISTER_FONT];
        
        //[[self contentView] addSubview:_separatorRelaxView];
    
    }
    
    return self;

}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

@end
