//
//  WorkoutDetailCell.h
//  Titan
//
//  Created by Manuel Manzanera on 29/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSCell.h"

//@protocol WorkoutDetailCelldelegate <NSObject>
//
//- (void)optionsExerciseDidPushInIndex:(NSInteger)index;
//
//@end

@interface WorkoutDetailCell : CSCell

@property (nonatomic, strong) UIImageView *workoutImageView;

//@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) UILabel *seriesLabel;
@property (nonatomic, strong) UILabel *energyLabel;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIButton *menuButton;
@property (nonatomic, strong) UIImageView *checkImageView;

@property (nonatomic, strong) UIImageView *clockImageView;
@property (nonatomic, strong) UIImageView *kCalImageView;

@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) UIView *panView;
//@property (nonatomic, assign) id<WorkoutDetailCelldelegate>delegate;

@end
