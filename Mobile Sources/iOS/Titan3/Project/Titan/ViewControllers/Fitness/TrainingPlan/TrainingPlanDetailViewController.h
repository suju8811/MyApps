//
//  TrainingPlanDetailViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "TrainingPlan.h"
#import "SchedulePlan.h"

@interface TrainingPlanDetailViewController : ParentViewController

@property (nonatomic, strong) TrainingPlan *trainingPlan;
@property (nonatomic, assign) NSInteger selectedTrainingIndex;
@property (nonatomic, assign) NSInteger fromCalendar;
@property (nonatomic, strong) SchedulePlan * schedulePlan;

@end
