//
//  TrainingPlanCell.m
//  Titan
//
//  Created by Manuel Manzanera on 26/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingPlanCell.h"
#import "UtilManager.h"
#import "AppContext.h"
#import "AppConstants.h"

@implementation TrainingPlanCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.contentView.clipsToBounds = YES;
    self.clipsToBounds = YES;
    
    if(self){
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:100], [UtilManager height:10], WIDTH - [UtilManager width:100], [UtilManager height:20])];
        [_titleLabel setText:@"Sesión inicial nutrición"];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setTextColor:YELLOW_APP_COLOR];
        [_titleLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
        [_titleLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_titleLabel];
        
        
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        //[self.workoutImageView setHidden:TRUE];
        
        _dayLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:5], [UtilManager height:10], [UtilManager width:40], [UtilManager height:30])];
        [_dayLabel setText:@"Día 1"];
        [_dayLabel setTextColor:YELLOW_APP_COLOR];
        [_dayLabel setFont:[UIFont fontWithName:BOLD_FONT size:15]];
        [_dayLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_dayLabel];
        
        _circleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_dayLabel.frame.origin.x + _dayLabel.frame.size.width + [UtilManager width:5], _dayLabel.frame.origin.y, [UtilManager height:15], [UtilManager height:15])];
        [[_circleImageView layer] setBorderColor:YELLOW_APP_COLOR.CGColor];
        [[_circleImageView layer] setBorderWidth:3];
        [[_circleImageView layer] setCornerRadius:_circleImageView.frame.size.height/2];
        [_circleImageView setCenter:CGPointMake(_circleImageView.center.x, _dayLabel.center.y)];
        
        [[self contentView] addSubview:_circleImageView];
        
        _verticalSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(0, _circleImageView.frame.size.height + _circleImageView.frame.origin.y + [UtilManager height:5], 1, [UtilManager height:50])];
        [_verticalSeparatorView setBackgroundColor:[UIColor lightGrayColor]];
        [_verticalSeparatorView setCenter:CGPointMake(_circleImageView.center.x, _verticalSeparatorView.center.y)];
        
        [[self contentView] addSubview:_verticalSeparatorView];
        
        [self.titleLabel setFrame:CGRectMake(_circleImageView.frame.origin.x + _circleImageView.frame.size.width + [UtilManager width:15], _dayLabel.frame.origin.y, [UtilManager width:240], _dayLabel.frame.size.height)];
        [self.titleLabel setTextColor:[UIColor whiteColor]];
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height, WIDTH - self.titleLabel.frame.origin.x - [UtilManager width:50], TAGVIEW_SMALL_HEIGHT)]; //andTags:tags withTagsControlMode:TagsControlModeList];
        [_tagView setMode:TagsControlModeList];
        [_tagView setIsCenter:FALSE];
        [_tagView setUserInteractionEnabled:FALSE];
        [_tagView reloadTagSubviews];
        
        [self.contentView addSubview:_tagView];
        
        _exerciseLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.titleLabel.frame.origin.x, _tagView.frame.origin.y + _tagView.frame.size.height, [UtilManager width:75], [UtilManager height:25])];
        [_exerciseLabel setTextColor:GRAY_REGISTER_FONT];
        [_exerciseLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [_exerciseLabel setAdjustsFontSizeToFitWidth:YES];
        
        [self.contentView addSubview:_exerciseLabel];
        
        UIImageView *clockImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_exerciseLabel.frame.origin.x + _exerciseLabel.frame.size.width, _exerciseLabel.frame.origin.y, [UtilManager width:25] * 0.8, [UtilManager width:25] * 0.8)];
        [clockImageView setImage:[UIImage imageNamed:@"icon-cell-clock"]];
        
        [self.contentView addSubview:clockImageView];
        
        //[_exerciseLabel setCenter:CGPointMake(clockImageView.center.x - _exerciseLabel.frame.size.width, _exerciseLabel.center.y)];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(clockImageView.frame.origin.x + clockImageView.frame.size.width+ [UtilManager width:5], _exerciseLabel.frame.origin.y,[UtilManager width:50], _exerciseLabel.frame.size.height)];
        [_timeLabel setText:@"0 min"];
        [_timeLabel setTextAlignment:NSTextAlignmentLeft];
        [_timeLabel setFont:_exerciseLabel.font];
        [_timeLabel setTextColor:_exerciseLabel.textColor];
        [_timeLabel setAdjustsFontSizeToFitWidth:YES];
        
        [clockImageView setCenter:CGPointMake(clockImageView.center.x, _timeLabel.center.y)];
        
        [self.contentView addSubview:_timeLabel];
        
        UIImageView  *kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_timeLabel.frame.origin.x + _timeLabel.frame.size.width + [UtilManager width:7], _timeLabel.frame.origin.y, [UtilManager width:20] * 0.8, [UtilManager width:20] * 0.8)];
        [kCalImageView setCenter:CGPointMake(kCalImageView.center.x, clockImageView.center.y)];
        [kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-red"]];
        
        [self.contentView addSubview:kCalImageView];
        
        _kCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(kCalImageView.frame.origin.x + kCalImageView.frame.size.width, _timeLabel.frame.origin.y, [UtilManager width:60], _timeLabel.frame.size.height)];
        [_kCalLabel setTextColor:RED_APP_COLOR];
        [_kCalLabel setFont:_timeLabel.font];
        [_kCalLabel setTextAlignment:NSTextAlignmentLeft];
        [_kCalLabel setText:@"0 kCal"];
        [_kCalLabel setAdjustsFontSizeToFitWidth:YES];
        [_kCalLabel setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:_kCalLabel];
        
        _menuButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50], [UtilManager height:10], [UtilManager width:35], [UtilManager width:35])];
        [_menuButton addTarget:self action:@selector(menuAction) forControlEvents:UIControlEventTouchUpInside];
        [_menuButton setImage:[UIImage imageNamed:@"btn-options"] forState:UIControlStateNormal];
        [_menuButton setCenter:CGPointMake(_menuButton.center.x, _tagView.center.y)];
        
        [[self contentView] addSubview:_menuButton];        
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        //[self.workoutImageView setHidden:TRUE];
        
        _dayLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:5], [UtilManager height:10], [UtilManager width:40], [UtilManager height:30])];
        [_dayLabel setText:@"Día 1"];
        [_dayLabel setTextColor:YELLOW_APP_COLOR];
        [_dayLabel setFont:[UIFont fontWithName:BOLD_FONT size:15]];
        [_dayLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_dayLabel];
        
        _circleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_dayLabel.frame.origin.x + _dayLabel.frame.size.width + [UtilManager width:5], _dayLabel.frame.origin.y, [UtilManager height:15], [UtilManager height:15])];
        [[_circleImageView layer] setBorderColor:YELLOW_APP_COLOR.CGColor];
        [[_circleImageView layer] setBorderWidth:3];
        [[_circleImageView layer] setCornerRadius:_circleImageView.frame.size.height/2];
        [_circleImageView setCenter:CGPointMake(_circleImageView.center.x, _dayLabel.center.y)];
        
        [[self contentView] addSubview:_circleImageView];
        
        _verticalSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(0, _circleImageView.frame.size.height + _circleImageView.frame.origin.y + [UtilManager height:5], 1, [UtilManager height:50])];
        [_verticalSeparatorView setBackgroundColor:[UIColor lightGrayColor]];
        [_verticalSeparatorView setCenter:CGPointMake(_circleImageView.center.x, _verticalSeparatorView.center.y)];
        
        [[self contentView] addSubview:_verticalSeparatorView];
        
        [self.titleLabel setFrame:CGRectMake(_circleImageView.frame.origin.x + _circleImageView.frame.size.width + [UtilManager width:15], _dayLabel.frame.origin.y, [UtilManager width:240], _dayLabel.frame.size.height)];
        [self.titleLabel setTextColor:[UIColor whiteColor]];
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height, WIDTH - self.titleLabel.frame.origin.x - [UtilManager width:40], TAGVIEW_SMALL_HEIGHT)]; //andTags:tags withTagsControlMode:TagsControlModeList];
        [_tagView setMode:TagsControlModeList];
        [_tagView setIsCenter:FALSE];
        [_tagView setUserInteractionEnabled:FALSE];
        [_tagView reloadTagSubviews];
        
        [self.contentView addSubview:_tagView];
        
        _exerciseLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.titleLabel.frame.origin.x, _tagView.frame.origin.y + _tagView.frame.size.height, [UtilManager width:90], [UtilManager height:25])];
        [_exerciseLabel setTextColor:GRAY_REGISTER_FONT];
        [_exerciseLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [_exerciseLabel setAdjustsFontSizeToFitWidth:YES];
        
        [self.contentView addSubview:_exerciseLabel];
        
        UIImageView *clockImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_exerciseLabel.frame.origin.x + _exerciseLabel.frame.size.width, _exerciseLabel.frame.origin.y, [UtilManager width:25] * 0.8, [UtilManager width:25] * 0.8)];
        [clockImageView setCenter:CGPointMake(WIDTH/2  + [UtilManager width:25] * 0.8, clockImageView.center.y)];
        [clockImageView setImage:[UIImage imageNamed:@"icon-cell-clock"]];
        
        [self.contentView addSubview:clockImageView];
        
        //[_exerciseLabel setCenter:CGPointMake(clockImageView.center.x - _exerciseLabel.frame.size.width, _exerciseLabel.center.y)];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(clockImageView.frame.origin.x + clockImageView.frame.size.width+ [UtilManager width:5], _exerciseLabel.frame.origin.y,[UtilManager width:70], _exerciseLabel.frame.size.height)];
        [_timeLabel setText:@"0 min"];
        [_timeLabel setTextAlignment:NSTextAlignmentLeft];
        [_timeLabel setFont:_exerciseLabel.font];
        [_timeLabel setTextColor:_exerciseLabel.textColor];
        [_timeLabel setAdjustsFontSizeToFitWidth:YES];
        
        [clockImageView setCenter:CGPointMake(clockImageView.center.x, _timeLabel.center.y)];
        
        [self.contentView addSubview:_timeLabel];
        
        UIImageView  *kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_timeLabel.frame.origin.x + _timeLabel.frame.size.width, _timeLabel.frame.origin.y, [UtilManager width:20] * 0.8, [UtilManager width:20] * 0.8)];
        [kCalImageView setCenter:CGPointMake(kCalImageView.center.x, clockImageView.center.y)];
        [kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-red"]];
        
        [self.contentView addSubview:kCalImageView];
        
        _kCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(kCalImageView.frame.origin.x + kCalImageView.frame.size.width, _timeLabel.frame.origin.y, [UtilManager width:60], _timeLabel.frame.size.height)];
        [_kCalLabel setTextColor:RED_APP_COLOR];
        [_kCalLabel setFont:_timeLabel.font];
        [_kCalLabel setTextAlignment:NSTextAlignmentLeft];
        [_kCalLabel setText:@"0 kCal"];
        [_kCalLabel setAdjustsFontSizeToFitWidth:YES];
        [_kCalLabel setBackgroundColor:[UIColor clearColor]];
    
        [self.contentView addSubview:_kCalLabel];
        
        _menuButton = [[UIButton alloc] initWithFrame:CGRectMake(self.contentView.frame.size.width - [UtilManager width:50], [UtilManager height:10], [UtilManager width:35], [UtilManager width:35])];
        [_menuButton addTarget:self action:@selector(menuAction) forControlEvents:UIControlEventTouchUpInside];
        [_menuButton setImage:[UIImage imageNamed:@"btn-options"] forState:UIControlStateNormal];
        [_menuButton setCenter:CGPointMake(_menuButton.center.x, _tagView.center.y)];
        
        [[self contentView] addSubview:_menuButton];
    }
    
    return self;
}

- (void)menuAction{
    [_delegate optionTrainingDayDidPush:_currentIndex serialIndex:_serialIndex];
}

- (void)setIsSimplify:(BOOL)isSimplify {
    _isSimplify = isSimplify;
    if (_isSimplify) {
        _menuButton.center = CGPointMake(_menuButton.center.x, _titleLabel.center.y);
    }
    else {
        _menuButton.center = CGPointMake(_menuButton.center.x, _tagView.center.y);
    }
}

@end
