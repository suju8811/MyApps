//
//  TrainingPlanTopCell.m
//  Titan
//
//  Created by Manuel Manzanera on 1/6/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingPlanTopCell.h"
#import "CSStickyHeaderFlowLayoutAttributes.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "UtilManager.h"

@interface TrainingPlanTopCell ()

@property (nonatomic, assign) CGSize calendarImageSize;

@end

@implementation TrainingPlanTopCell

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = frame;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self addSubview:blurEffectView];
        
        _backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:20], [UtilManager width:40]*0.8, [UtilManager width:40] * 0.8)];
        [_backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
        [_backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:_backButton];
        
        _menuButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50], [UtilManager height:20], [UtilManager width:40] * 0.8, [UtilManager width:40]*0.8)];
        [_menuButton setImage:[UIImage imageNamed:@"btn-menu-settings"] forState:UIControlStateNormal];
        [_menuButton addTarget:self action:@selector(menuAction) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:_menuButton];
        
        _titleHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(_backButton.frame.origin.x + _backButton.frame.size.width, _backButton.frame.origin.y + [UtilManager height:30], WIDTH - 2 * (_backButton.frame.origin.x + _backButton.frame.size.width) - [UtilManager width:20], [UtilManager height:45])];
        //[_titleLabel setText:_currentTraining.title];
        [_titleHeaderLabel setNumberOfLines:2];
        [_titleHeaderLabel setBackgroundColor:[UIColor clearColor]];
        [_titleHeaderLabel setTextAlignment:NSTextAlignmentCenter];
        [_titleHeaderLabel setTextColor:[UIColor whiteColor]];
        [_titleHeaderLabel setFont:[UIFont fontWithName:BOLD_FONT size:21]];
        [_titleHeaderLabel setAdjustsFontSizeToFitWidth:YES];
        [_titleHeaderLabel setText:@"Texto"];
        
        [self addSubview:_titleHeaderLabel];
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake(0., _titleHeaderLabel.frame.origin.y + _titleHeaderLabel.frame.size.height + [UtilManager height:5], WIDTH, TAGVIEW_SMALL_HEIGHT)]; //andTags:tags withTagsControlMode:TagsControlModeList];
        [_tagView setMode:TagsControlModeList];
        [_tagView setIsCenter:TRUE];
        [_tagView reloadTagSubviews];
        
        [self addSubview:_tagView];
        
        _firstView = [[UIView alloc] initWithFrame:CGRectMake(0., _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:10], WIDTH/2, [UtilManager height:30])];
        [_firstView setBackgroundColor:[UIColor clearColor]];
        
        _exercisesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0., _firstView.frame.size.width - [UtilManager width:10], [UtilManager height:25])];
        [_exercisesLabel setTextAlignment:NSTextAlignmentRight];
        [_exercisesLabel setTextColor:GRAY_REGISTER_FONT];
        [_exercisesLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [_exercisesLabel setAdjustsFontSizeToFitWidth:YES];
        [_exercisesLabel setText:NSLocalizedString(@"0 Entrenamientos", nil)];
        
        [_firstView addSubview:_exercisesLabel];
        
        [self addSubview:_firstView];
        
        _secondView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH/2, _firstView.frame.origin.y, _firstView.frame.size.width, _firstView.frame.size.height)];
        [_secondView setBackgroundColor:[UIColor clearColor]];
        
        _calendarImageSize = [UIImage imageNamed:@"icon-birth"].size;
        
        _calendarImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-birth"]];
        [_calendarImageView setFrame:CGRectMake([UtilManager width:10], 0, _calendarImageView.frame.size.width * 0.8, _calendarImageView.frame.size.height * 0.8)];
        
        [_secondView  addSubview:_calendarImageView];
        
        _daysLabel = [[UILabel alloc] initWithFrame:CGRectMake(_calendarImageView.frame.origin.x + _calendarImageView.frame.size.width + [UtilManager width:5], 0,_secondView.frame.size.width/2, _exercisesLabel.frame.size.height)];
        [_daysLabel setText:@"0 días"];
        [_daysLabel setTextAlignment:NSTextAlignmentLeft];
        [_daysLabel setFont:_exercisesLabel.font];
        [_daysLabel setTextColor:_exercisesLabel.textColor];
        [_daysLabel setAdjustsFontSizeToFitWidth:YES];
        
        [_calendarImageView setCenter:CGPointMake(_calendarImageView.center.x, _daysLabel.center.y)];
        
        [_secondView addSubview:_daysLabel];
        
        [self addSubview:_secondView];
    }
    
    return self;
}

- (void)didPressBackButton {
    [_delegate backTopHeaderAction];
}

- (void)menuAction{
    
    [_delegate menuTopHeaderAction];
}

- (void)applyLayoutAttributes:(CSStickyHeaderFlowLayoutAttributes *)layoutAttributes {
    
    //[self setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    NSLog(@"PROGRESO %f",layoutAttributes.progressiveness);
    
    if(layoutAttributes.progressiveness > 0.685){
        [_firstView setHidden:FALSE];
        [_secondView setHidden:FALSE];
        [_thirdView setHidden:FALSE];
    }
    
    if(layoutAttributes.progressiveness > 0.3766 && layoutAttributes.progressiveness<1){
        [_exercisesLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [_exercisesLabel setHidden:FALSE];
        [_daysLabel setHidden:FALSE];
        [_calendarImageView setHidden:FALSE];
        [_tagView setHidden:FALSE];
        
        float widthText = (WIDTH - 2 * (_backButton.frame.origin.x + _backButton.frame.size.width) - [UtilManager width:20]) * layoutAttributes.progressiveness;
        
        if(widthText > [UtilManager width:180]){
            [_titleHeaderLabel setFrame:CGRectMake(_titleHeaderLabel.frame.origin.x,(_backButton.frame.origin.y + [UtilManager height:30] * layoutAttributes.progressiveness) * layoutAttributes.progressiveness, widthText, _titleHeaderLabel.frame.size.height)];
        }
        else{
            [_titleHeaderLabel setFrame:CGRectMake(_titleHeaderLabel.frame.origin.x,(MAX(_backButton.frame.origin.y * layoutAttributes.progressiveness , [UtilManager height:1]) + [UtilManager height:30] * layoutAttributes.progressiveness) * layoutAttributes.progressiveness, [UtilManager width:180], _titleHeaderLabel.frame.size.height)];
        }
        
        [_titleHeaderLabel setCenter:CGPointMake(self.center.x, _titleHeaderLabel.center.y)];
        [_titleHeaderLabel setNumberOfLines:1];
        [_titleHeaderLabel setAdjustsFontSizeToFitWidth:true];
        
        [_tagView setFrame:CGRectMake(0., _titleHeaderLabel.frame.origin.y + MAX(_titleHeaderLabel.frame.size.height * layoutAttributes.progressiveness, [UtilManager height:30]) + [UtilManager height:5], WIDTH, TAGVIEW_SMALL_HEIGHT)];
        
        [_firstView setFrame:CGRectMake(0., _tagView.frame.origin.y + _tagView.frame.size.height + MAX([UtilManager height:1] * layoutAttributes.progressiveness, 4), WIDTH/2, MAX([UtilManager height:30] * layoutAttributes.progressiveness, [UtilManager height:12]))];
        
        [_exercisesLabel setFrame:CGRectMake(0, 0., _firstView.frame.size.width - [UtilManager width:10], MAX([UtilManager height:25] * layoutAttributes.progressiveness, [UtilManager height:12]))];
        
        [_secondView setFrame:CGRectMake(WIDTH/2, _firstView.frame.origin.y, _firstView.frame.size.width, _firstView.frame.size.height)];
        
        [_calendarImageView setFrame:CGRectMake([UtilManager width:10], 0, MAX(_calendarImageSize.width * 0.8 * layoutAttributes.progressiveness, _calendarImageSize.width * 0.4), MAX(_calendarImageSize.width * 0.8 * layoutAttributes.progressiveness, _calendarImageSize.width * 0.4))];
        
        [_daysLabel setFrame:CGRectMake(_calendarImageView.frame.origin.x + _calendarImageView.frame.size.width + [UtilManager width:5], 0,_secondView.frame.size.width/2, _exercisesLabel.frame.size.height)];
        
        [_calendarImageView setCenter:CGPointMake(_calendarImageView.center.x, _daysLabel.center.y)];
        
        
    }
    else{
        //[_exercisesLabel setHidden:TRUE];
        //[_daysLabel setHidden:TRUE];
        //[_calendarImageView setHidden:TRUE];
        if(layoutAttributes.progressiveness < 0.1){
            [_firstView setHidden:true];
            [_secondView setHidden:true];
        } else {
            [_firstView setHidden:false];
            [_secondView setHidden:false];
        }
    }
    
    
    [UIView commitAnimations];
}

@end
