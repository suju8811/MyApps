//
//  TrainingPlanTopCell.h
//  Titan
//
//  Created by Manuel Manzanera on 1/6/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagView.h"

@protocol TrainingPlanTopHeaderDelegate;

@interface TrainingPlanTopCell : UICollectionViewCell

@property (strong, nonatomic) UILabel *titleHeaderLabel;
@property (strong, nonatomic) UILabel *kCalLabel;
@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) TagView *tagView;
@property (strong, nonatomic) UILabel *exercisesLabel;
@property (strong, nonatomic) UILabel *daysLabel;
@property (strong, nonatomic) UIImageView *calendarImageView;

@property (strong, nonatomic) UIView *firstView;
@property (strong, nonatomic) UIView *secondView;
@property (strong, nonatomic) UIView *thirdView;

@property UIButton *backButton;
@property UIButton *menuButton;

@property (nonatomic, assign) id<TrainingPlanTopHeaderDelegate>delegate;

@end

@protocol TrainingPlanTopHeaderDelegate <NSObject>

- (void)backTopHeaderAction;
- (void)menuTopHeaderAction;

@end
