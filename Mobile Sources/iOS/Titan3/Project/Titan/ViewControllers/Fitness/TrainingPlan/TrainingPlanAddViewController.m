//
//  TrainingPlanAddViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 14/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingPlanAddViewController.h"
#import "SelectorTableViewController.h"
#import "TrainingPlanDetailViewController.h"
#import "MRProgress.h"
#import "WebServiceManager.h"
#import "SelectTagView.h"
#import "TrainingPlanAddExecutive.h"

@interface TrainingPlanAddViewController ()<UITextFieldDelegate,UITextViewDelegate, WebServiceManagerDelegate, SelectTagViewDelegate, TrainingPlanAddDisplay>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITextField *nameTextField;
@property (nonatomic, strong) UITextView *contentTextView;
@property (nonatomic, strong) UITextView *noteTextView;
@property (nonatomic, strong) UITextField *startDateTextField;
@property (nonatomic, strong) UITextField *endDateTextField;
@property (nonatomic, strong) SelectorTableViewController *selectorTableViewController;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) UIView *containerPickerView;
@property (nonatomic, strong) TrainingPlan *trainingPlan;
@property (nonatomic, strong) UIButton *privacityButton;
@property (nonatomic, strong) UILabel *privacityLabel;

@property (nonatomic, strong) SelectTagView *selectTagView;
@property (nonatomic, strong) NSMutableArray *tags;
@property (nonatomic, strong) NSMutableArray *selectedTags;

@property (nonatomic, strong) TrainingPlanAddExecutive * executive;

@end

@implementation TrainingPlanAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _startDate = [NSDate date];
    _endDate = [NSDate date];
    
    _tags = [Tag getTagsWithType:@"type_2" andCollection:@"TrainingPlan"];
    
    [self configureView];
    [_nameTextField becomeFirstResponder];
    
    _executive = [[TrainingPlanAddExecutive alloc] initWithDisplay:self];
}

- (void)viewWillAppear:(BOOL)animated{
    
    if(_selectorTableViewController.presentWorkoutDetail){
        
        _selectorTableViewController.presentWorkoutDetail = FALSE;
        
        int dayOrder = 1;
        
        for(Training *training in _selectorTableViewController.selectTrainings){
            
            RLMRealm *realm = [AppContext currentRealm];
            
            TrainingDayContent *trainingDayContent = [[TrainingDayContent alloc] init];
            
            Training *copyTraining = [Training copyTraining:training];
            
            TrainingDay *trainingDay = [[TrainingDay alloc] init];
            
            [trainingDay setOrder:[NSNumber numberWithInt:dayOrder]];
            
            [realm transactionWithBlock:^{
                [trainingDayContent setTraining:copyTraining];
                if(copyTraining.serverTrainingId)
                    [trainingDayContent setTrainingId:copyTraining.serverTrainingId];
                else
                    [trainingDayContent setTrainingId:copyTraining.trainingId];
                [trainingDay.trainingDayContent addObject:trainingDayContent];
            }];
            
            [_trainingPlan.trainingDay addObject:trainingDay];
            
            dayOrder ++;
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
        }
        
        [_executive postTrainingPlan:_trainingPlan];
    }
}

- (void)configureView{
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _nameTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:10], NAV_HEIGHT, WIDTH - [UtilManager width:20], UITEXTFIELD_HEIGHT)];
    
    [_nameTextField setDelegate:self];
    [_nameTextField setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]];
    [_nameTextField setTextColor:GRAY_REGISTER_FONT];
    [_nameTextField setBackgroundColor:[UIColor clearColor]];
    _nameTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Nombre", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName:[UIColor grayColor],
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]
                                                 }
     ];
    [_nameTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    
    if(_currentTrainingPlan)
        [_nameTextField setText:_currentTrainingPlan.title];
    
    [self.view addSubview:_nameTextField];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _nameTextField.frame.origin.y + _nameTextField.frame.size.height , WIDTH - [UtilManager width:20], 1)];
    [separatorView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self.view addSubview:separatorView];
    
    _selectTagView = [[SelectTagView alloc] initWithFrame:CGRectMake([UtilManager width:10], separatorView.frame.origin.y + separatorView.frame.size.height + [UtilManager height:10], WIDTH - [UtilManager width:20], [UtilManager height:30]) andTags:_tags];
    
    if(_currentTrainingPlan)
    {
        _selectedTags = [[NSMutableArray alloc] initWithCapacity:_currentTrainingPlan.tags.count];
        for(Tag *tag in _currentTrainingPlan.tags){
            [_selectedTags addObject:tag];
        }
        [_selectTagView setSelectedTags:_selectedTags];
    }
    
    [_selectTagView setDelegate:self];
    
    [self.view addSubview:_selectTagView];
    
    _contentTextView = [[UITextView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _selectTagView.frame.origin.y + _selectTagView.frame.size.height + [UtilManager height:10], _nameTextField.frame.size.width, [UtilManager height:120])];
    [_contentTextView setTextColor:_nameTextField.textColor];
    [_contentTextView setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_contentTextView setTextAlignment:NSTextAlignmentJustified];
    [_contentTextView setText:NSLocalizedString(@"Descripción", nil)];
    [_contentTextView setFont:[UIFont fontWithName:ITALIC size:18]];
    [_contentTextView setBackgroundColor:[UIColor clearColor]];
    [[_contentTextView layer] setBorderWidth:1];
    [[_contentTextView layer] setBorderColor:BLACK_HEADER_APP_COLOR.CGColor];
    
    if(_currentTrainingPlan) {
        _contentTextView.text = _currentTrainingPlan.content;
    }
    
    [_contentTextView setDelegate:self];
    
    UIToolbar *toolbarWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //UIBarButtonItem *barWeigthButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resetView)];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetView)];
    [barButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarWeigth setItems:[NSArray arrayWithObjects:flexibleWeigthSpace, barButtonItem, nil]];
    _contentTextView.inputAccessoryView = toolbarWeigth;
    
    [self.view addSubview:_contentTextView];
    
    _noteTextView = [[UITextView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _contentTextView.frame.origin.y + _contentTextView.frame.size.height + [UtilManager height:10], _nameTextField.frame.size.width, [UtilManager height:180])];
    [_noteTextView setTextColor:_nameTextField.textColor];
    [_noteTextView setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_noteTextView setTextAlignment:NSTextAlignmentJustified];
    [_noteTextView setText:NSLocalizedString(@"Notas", nil)];
    [_noteTextView setFont:[UIFont fontWithName:ITALIC size:18]];
    [_noteTextView setBackgroundColor:[UIColor clearColor]];
    [[_noteTextView layer] setBorderWidth:1];
    [[_noteTextView layer] setBorderColor:BLACK_HEADER_APP_COLOR.CGColor];
    
    if(_currentTrainingPlan) {
        _noteTextView.text = _currentTrainingPlan.notes;
    }
    
    [_noteTextView setDelegate:self];
    
    UIToolbar *toolbarNoteWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarNoteWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleNoteWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //UIBarButtonItem *barWeigthButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resetView)];
    UIBarButtonItem *barNoteButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetView)];
    [barNoteButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barNoteButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarNoteWeigth setItems:[NSArray arrayWithObjects:flexibleNoteWeigthSpace, barNoteButtonItem, nil]];
    _noteTextView.inputAccessoryView = toolbarNoteWeigth;
    
    [self.view addSubview:_noteTextView];
    
    _startDateTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:10], _contentTextView.frame.origin.y + _contentTextView.frame.size.height + [UtilManager height:10], WIDTH - [UtilManager width:20], UITEXTFIELD_HEIGHT)];
    
    [_startDateTextField setDelegate:self];
    [_startDateTextField setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]];
    [_startDateTextField setTextColor:GRAY_REGISTER_FONT];
    [_startDateTextField setBackgroundColor:[UIColor clearColor]];
    _startDateTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Fecha de comienzo", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName:[UIColor grayColor],
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]
                                                 }
     ];
    [_startDateTextField setAccessibilityLabel:@"startDate"];
    
    //[self.view addSubview:_startDateTextField];
    
    _endDateTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:10], _startDateTextField.frame.origin.y + _startDateTextField.frame.size.height + [UtilManager height:10], WIDTH - [UtilManager width:20], UITEXTFIELD_HEIGHT)];
    
    [_endDateTextField setDelegate:self];
    [_endDateTextField setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]];
    [_endDateTextField setTextColor:GRAY_REGISTER_FONT];
    [_endDateTextField setBackgroundColor:[UIColor clearColor]];
    _endDateTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Fecha de finalización", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName:[UIColor grayColor],
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]
                                                 }
     ];
    
    //[self.view addSubview:_endDateTextField];
    
    _privacityButton = [[UIButton alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x,_noteTextView.frame.origin.y + _noteTextView.frame.size.height + [UtilManager height:10] , [UtilManager width:40], [UtilManager width:40])];
    [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
    [_privacityButton addTarget:self action:@selector(privacityAction) forControlEvents:UIControlEventTouchUpInside];
    
//    [self.view addSubview:_privacityButton];
    
    _privacityLabel = [[UILabel alloc] initWithFrame:CGRectMake(_privacityButton.frame.origin.x + _privacityButton.frame.size.width + [UtilManager width:10], _privacityButton.frame.origin.y, [UtilManager width:240], _privacityButton.frame.size.height)];
    [_privacityLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    [_privacityLabel setTextAlignment:NSTextAlignmentLeft];
    [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    [_privacityLabel setTextColor:[UIColor whiteColor]];
    
//    [self.view addSubview:_privacityLabel];
    
    if(_currentTrainingPlan){
        if([_currentTrainingPlan.privacity isEqualToString:@"private"]){
            [_privacityButton setImage:[UIImage imageNamed:@"img-lock-closed"] forState:UIControlStateNormal];
            [_privacityLabel setText:NSLocalizedString(@"Perfil Privado", nil)];
        }
    }
    
    UIButton *addExerciseButton = [[UIButton alloc] initWithFrame:CGRectMake(0., HEIGHT - [UtilManager height:80], WIDTH, [UtilManager height:80])];
    [addExerciseButton setTitle:NSLocalizedString(@"AÑADIR ENTRENAMIENTOS", nil) forState:UIControlStateNormal];
    [[addExerciseButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:18]];
    [addExerciseButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [addExerciseButton addTarget:self action:@selector(addTrainings) forControlEvents:UIControlEventTouchUpInside];
    [addExerciseButton setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    //if(!_currentTrainingPlan)
    //    [self.view addSubview:addExerciseButton];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, NAV_HEIGHT - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    if(_currentTrainingPlan)
        [titleLabel setText:NSLocalizedString(@"EDITAR PLAN ENTRENAMIENTO", nil)];
    else
        [titleLabel setText:NSLocalizedString(@"NUEVO PLAN ENTRENAMIENTO", nil)];
    [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:titleLabel];
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50], [UtilManager height:20], [UtilManager width:40] * 0.88, [UtilManager width:40] * 0.8)];
    [okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(addTrainings) forControlEvents:UIControlEventTouchUpInside];
    [okButton setCenter:CGPointMake(okButton.center.x, titleLabel.center.y)];
    
    [self.view addSubview:okButton];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:20.], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton setHidden:FALSE];
    [backButton setCenter:CGPointMake(backButton.center.x, okButton.center.y)];
    
    [self.view addSubview:backButton];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didPressBackButton)];
    [swipeGesture setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:swipeGesture];
}

- (void)didPressBackButton {
    [_executive setTagState:NO forTrainingPlan:_trainingPlan];
    [_executive setTagState:NO forTrainingPlan:_currentTrainingPlan];
    [self dismissViewControllerAnimated:YES completion:nil];
}
    
-  (void)setTagsToOff{
    RLMRealm *realm = [AppContext currentRealm];
    if(_currentTrainingPlan){
        for(Tag *tag in _currentTrainingPlan.tags){
            [realm transactionWithBlock:^{
                if([tag.status isEqualToString:@"Off"])
                [tag setStatus:@"Off"];
                else
                [tag setStatus:@"Off"];
            }];
        }
    }

    if(_trainingPlan){
        for(Tag *tag in _trainingPlan.tags){
            [realm transactionWithBlock:^{
                if([tag.status isEqualToString:@"Off"])
                [tag setStatus:@"Off"];
                else
                [tag setStatus:@"Off"];
            }];
        }
    }

    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
}

- (void)privacityAction{
    if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]]){
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-closed"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Privado", nil)];
    }else{
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    }
}

- (void)resetView{
    [_contentTextView resignFirstResponder];
    [_noteTextView resignFirstResponder];
}

- (void)addTrainings{
    
    if([self checkFields]){
        
        if(_currentTrainingPlan){
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                [_currentTrainingPlan setTitle:_nameTextField.text];
                [_currentTrainingPlan setContent:_contentTextView.text];
                [_currentTrainingPlan setDesc:_contentTextView.text];
                [_currentTrainingPlan setNotes:_noteTextView.text];
                [_currentTrainingPlan.tags removeAllObjects];
                [_currentTrainingPlan.tags addObjects:_selectedTags];
                if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]])
                    [_currentTrainingPlan setPrivacity:@"public"];
                else
                    [_currentTrainingPlan setPrivacity:@"private"];
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            
            [_executive updateTrainingPlan:_currentTrainingPlan];
            
        }else{
            _trainingPlan = [[TrainingPlan alloc] init];
            
            [_trainingPlan setContent:_contentTextView.text];
            [_trainingPlan setDesc:_contentTextView.text];
            [_trainingPlan setTitle:_nameTextField.text];
            [_trainingPlan setTrainingPlanId:[UtilManager uuid]];
            [_trainingPlan setOwner:[[AppContext sharedInstance] userId]];
            [_trainingPlan setNotes:_noteTextView.text];
            
            [[_trainingPlan tags] addObjects:_selectedTags];
            
            if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]])
                [_trainingPlan setPrivacity:@"public"];
            else
                [_trainingPlan setPrivacity:@"private"];
            
            [_trainingPlan setOwner:[[[AppContext sharedInstance] currentUser]userId]];
            
            _selectorTableViewController = [[SelectorTableViewController alloc] init];
            [_selectorTableViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [_selectorTableViewController setSelectorType:SelectorTypeTrainings];
            [_selectorTableViewController setSelectTrainingPlan:_trainingPlan];
            
            [self presentViewController:_selectorTableViewController animated:YES completion:^{
                
            }];
            
        }
    }
}

- (BOOL)checkFields{
    
    if(_nameTextField.text.length == 0){
        [self presentViewController:[UtilManager presentAlertWithMessage:@"" andTitle:NSLocalizedString(@"El campo nombre es oblicatorio", nil)] animated:YES completion:^{
            
        }];
        
        return FALSE;
    }
    
    return true;
}

- (void)presentDatePicker:(id)sender{
    
    UITextField *tapgesture = (UITextField *)sender;
    [_datePicker setAccessibilityLabel:tapgesture.accessibilityLabel];
    
    if(_datePicker)
    {
        [_datePicker removeFromSuperview];
        _datePicker = nil;
        
        [_containerPickerView removeFromSuperview];
        _containerPickerView = nil;
    }
    
    _containerPickerView = [[UIView alloc] initWithFrame:CGRectMake(0., HEIGHT - [UtilManager height:206], WIDTH, [UtilManager height:206])];
    [_containerPickerView setBackgroundColor:[UIColor clearColor]];
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0., [UtilManager height:44], self.view.frame.size.width, [UtilManager height:162])];
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    [_datePicker setBackgroundColor:BLACK_APP_COLOR];
    
    if([tapgesture.accessibilityLabel isEqualToString:@"startDate"])
        [_datePicker setDate:_startDate];
    else
        [_datePicker setDate:_endDate];
    
    //[_datePicker setMaximumDate:maxDate];
    //[_datePicker setMinimumDate:minDate];
    [_datePicker setUserInteractionEnabled:YES];
    [_datePicker setValue:YELLOW_APP_COLOR forKey:@"textColor"];
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,WIDTH,[UtilManager height:44])];
    [toolBar setBarStyle:UIBarStyleBlackTranslucent];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil)
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(acceptDatePickerAction:)];
    [barButtonDone setAccessibilityLabel:tapgesture.accessibilityLabel];
    [barButtonDone setTintColor:GRAY_REGISTER_FONT];
    toolBar.items = @[barButtonDone];
    [_containerPickerView addSubview:_datePicker];
    [_containerPickerView addSubview:toolBar];
    
    [self.view addSubview:_containerPickerView];
    //[self.view addSubview:_acceptButton];
}

- (void)acceptDatePickerAction:(id)sender{
    
    UIButton *dateButton = (UIButton *)sender;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *dateString = [dateFormatter stringFromDate:_datePicker.date];
    
    if([dateButton.accessibilityLabel isEqualToString:@"startDate"]){
        [_startDateTextField setText:dateString];
        _startDate = _datePicker.date;
    }else{
        [_endDateTextField setText:dateString];
        _endDate = _datePicker.date;
    }
    
    if([_startDate compare:_endDate] == NSOrderedDescending)
    {
        _endDate = _startDate;
        [_endDateTextField setText:dateString];
    }
    
    [_containerPickerView removeFromSuperview];
    _containerPickerView = nil;
    
    [_datePicker removeFromSuperview];
    _datePicker = nil;
    
    //[self reloadViews];
}


- (void)presentDetailView{
    
    [_fitnessViewController setTrainingPlan:_trainingPlan];
    [self setTagsToOff];
    
    [self dismissViewControllerAnimated:YES completion:^{
    
    }];
}
    
#pragma mark SelectTagViewDelegate Methods
    
- (void)actionDidPushReturnTags:(NSMutableArray *)selectedTags{
    _selectedTags = nil;
    _selectedTags = [[NSMutableArray alloc] init];
    _selectedTags = selectedTags;
}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if([textField isEqual:_startDateTextField] || [textField isEqual:_endDateTextField]){
        
        [self presentDatePicker:textField];
        return FALSE;
    }
    
    return YES;
}

#pragma mark UITextViewDelegate Methods

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
    return true;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if([textView isEqual:_noteTextView]){
        if([[textView text] isEqualToString:NSLocalizedString(@"Notas", nil)])
            [_noteTextView setText:@""];
    }
    
    if([textView isEqual:_contentTextView]){
        if([[textView text] isEqualToString:NSLocalizedString(@"Descripción", nil)])
            [_contentTextView setText:@""];
    }
    
    
    //if([[textView text] isEqualToString:NSLocalizedString(@"Notas", nil)])
    //    [_noteTextView setText:@""];
    
    return  true;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    return TRUE;
}

#pragma mark WebServiceManagerDelegate

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        
        NSDictionary *trainingPlanDictionary = (NSDictionary *)object;
        
        NSDictionary *dataDictionary = [trainingPlanDictionary valueForKey:@"data"];
        
        RLMRealm *realm = [AppContext currentRealm];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        
        if(webServiceType == WebServiceTypeUpdateTrainingPlan){
            [realm transactionWithBlock:^{
                [_currentTrainingPlan setServerTrainingPlanId:[dataDictionary valueForKey:@"_id"]];
                [_currentTrainingPlan setOwner:[dataDictionary valueForKey:@"owner"]];
                [_currentTrainingPlan setCreatedAt:[formatter dateFromString:[dataDictionary valueForKey:@"createdAt"]]];
                [_currentTrainingPlan setUpdatedAt:[formatter dateFromString:[dataDictionary valueForKey:@"updatedAt"]]];
            }];
            
            [realm beginWriteTransaction];
            [realm addOrUpdateObject:_currentTrainingPlan];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            
        } else {
            [_trainingPlan setServerTrainingPlanId:[dataDictionary valueForKey:@"_id"]];
            [_trainingPlan setOwner:[dataDictionary valueForKey:@"owner"]];
            [_trainingPlan setCreatedAt:[formatter dateFromString:[dataDictionary valueForKey:@"createdAt"]]];
            [_trainingPlan setUpdatedAt:[formatter dateFromString:[dataDictionary valueForKey:@"updatedAt"]]];
            [realm beginWriteTransaction];
            [realm addOrUpdateObject:_trainingPlan];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
        }
        
        [self presentDetailView];
    }];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        //[self presentDetailView];
        [self setTagsToOff];
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        //[self presentDetailView];
        [self setTagsToOff];
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        
    }];
}

#pragma mark - Training Plan Add Display Delegate

- (TrainingPlanAddDisplayContext*)displayContext {
    TrainingPlanAddDisplayContext * context = [[TrainingPlanAddDisplayContext alloc] init];
    
    context.title = _nameTextField.text;
    context.content = _contentTextView.text;
    context.note = _noteTextView.text;
    context.selectedTags = _selectedTags;
    if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]]) {
        context.privacy = @"public";
    }
    else {
        context.privacy = @"private";
    }
    
    return context;
}

- (void)didPostTrainingPlan:(TrainingPlan *)trainingPlan {
    [WebServiceManager getTrainingPlansWithCompletionBlock:^(BOOL isSuccess) {
        [_fitnessViewController setTrainingPlan:trainingPlan];
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (void)didUpdateTrainingPlan:(TrainingPlan *)trainingPlan {
    [_fitnessViewController setTrainingPlan:trainingPlan];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
