 //
//  TrainingPlanDetailViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingPlanDetailViewController.h"
#import "TrainingPlanAddViewController.h"
#import "WorkoutDetailViewController.h"
#import "SelectorTableViewController.h"

#import "WebServiceManager.h"
#import "MRProgress.h"
#import "DateTimeUtil.h"

#import "TrainingDay.h"
#import "TrainingExercise.h"
#import "TrainingDayContent.h"
#import "TrainingAddViewController.h"
#import "Exercise.h"
#import "PlanFitCell.h"
#import "Tag.h"
#import "TagView.h"
#import "TrainingPlanCell.h"

#import "ParserManager.h"

#import "RelaxCell.h"
#import "CSSectionHeader.h"
#import "CSCell.h"
#import "TrainingPlanTopCell.h"
#import "CSStickyParallaxHeaderViewController.h"
#import "CSStickyHeaderFlowLayout.h"
#import "CSStickyHeaderFlowLayoutAttributes.h"
#import "UIImageView+AFNetworking.h"

#import "OptionsViewController.h"

#import "MBTwitterScroll.h"

#import "TrainingPlanDetailExecutive.h"
#import "ExercisesViewController.h"
#import "InfoPopupViewController.h"
#import "UserSelectorViewController.h"
#import "UserMeta.h"

@interface TrainingPlanDetailViewController ()<UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, TrainingPlanTopHeaderDelegate, SelectorTableViewDelegate, TrainingPlanCellDelegate, OptionsViewControllerDelegate, MBTwitterScrollDelegate, TrainingPlanDetailDisplay, UserSelectorViewControllerDelegate>

@property (nonatomic, strong) MBTwitterScroll *trainingTableView;
@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) SelectorTableViewController *selectorTableViewController;

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, assign) BOOL isSuperSerieEditing;

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *trainingDaysIndex;
@property (nonatomic, strong) TrainingDay *selectedTrainingDay;
@property (nonatomic, strong) TrainingDayContent *selectedTrainingDayContent;
@property (nonatomic, assign) NSInteger selectTrainingDayIndex;

@property (nonatomic, assign) BOOL isRemoveTrainingPlan;

@property (nonatomic, strong) TrainingPlanDetailExecutive * executive;

@property (nonatomic, strong) InfoPopupViewController * infoPopupViewController;

@property (nonatomic, assign) BOOL isSimplify;

@property (nonatomic, strong) UserSelectorViewController * userSelectorViewController;

@property (nonatomic, strong) UIButton *acceptButton;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) NSDate *endDate;

@property (nonatomic, strong) UIDatePicker *startDatePicker;
@property (nonatomic, strong) UIDatePicker *endDatePicker;

@end

@implementation TrainingPlanDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _items = [[NSMutableArray alloc] init];
    _trainingDaysIndex = [[NSMutableArray alloc] init];
    
    _isSimplify = NO;
    
    [self reloadItems];
    
    _isSuperSerieEditing = FALSE;
    
    [self configureView];
    
    _executive = [[TrainingPlanDetailExecutive alloc] initWithDisplay:self];
    
    UIStoryboard * customViewStoryboard = [UIStoryboard storyboardWithName:@"CustomView" bundle:nil];
    _infoPopupViewController = [customViewStoryboard instantiateInitialViewController];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(_isRemoveTrainingPlan) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [self reloadItems];
        
        [_trainingTableView setCurrentTrainingPlan:_trainingPlan];
        [[_trainingTableView tableView] reloadData];
        [_trainingTableView reloadTrainingPlansValues];
        
        if (_selectorTableViewController.presentWorkoutDetailForReplacing) {
            _selectorTableViewController.presentWorkoutDetailForReplacing = NO;
            _selectorTableViewController.presentWorkoutDetail = NO;
            
            TrainingDayContent * replacingTrainingDayContent = [_items objectAtIndex:_selectedTrainingIndex];
            Training * replacingTraining = [_selectorTableViewController.selectTrainings firstObject];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                [replacingTraining setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
                replacingTrainingDayContent.training = replacingTraining;
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            
            [_executive updateTrainingPlan:_trainingPlan];
        }
        else if (_selectorTableViewController.presentWorkoutDetail) {
            _selectorTableViewController.presentWorkoutDetailForReplacing = NO;
            _selectorTableViewController.presentWorkoutDetail = NO;
            
            for(Training *training in _selectorTableViewController.selectTrainings){
                
                RLMRealm *realm = [AppContext currentRealm];
                
                TrainingDayContent *trainingDayContent = [[TrainingDayContent alloc] init];
                
                Training *copyTraining = [Training copyTraining:training];
                
                trainingDayContent.training = copyTraining;
                trainingDayContent.trainingId = copyTraining.serverTrainingId;
                trainingDayContent.isClone = @YES;
                
                if (_selectedTrainingDay) {
                    [realm transactionWithBlock:^{
                        [[_trainingPlan.trainingDay objectAtIndex:_selectTrainingDayIndex].trainingDayContent addObject:trainingDayContent];
                    }];
                    
                    [realm beginWriteTransaction];
                    [realm addOrUpdateObject:_trainingPlan];
                    NSError *error;
                    [realm commitWriteTransaction:&error];
                    if(error) {
                        NSLog(@"%@",error.description);
                    }
                }
                else {
                    TrainingDay *trainingDay = [[TrainingDay alloc] init];
                    [trainingDay.trainingDayContent addObject:trainingDayContent];
                    [realm transactionWithBlock:^{
                        [trainingDay setOrder:[NSNumber numberWithInt:(int)_trainingPlan.trainingDay.count + 1]];
                        [_trainingPlan.trainingDay addObject:trainingDay];
                    }];
                    
                    [realm beginWriteTransaction];
                    [realm addOrUpdateObject:_trainingPlan];
                    NSError *error;
                    [realm commitWriteTransaction:&error];
                    if(error){
                        NSLog(@"%@",error.description);
                    }
                }
            }
            
            [_executive updateTrainingPlan:_trainingPlan];
        }
        else {
            [_trainingTableView.tableView reloadData];
        }
    }
}

- (void)reloadItems{
    
    if (_items.count > 0) {
        [_items removeAllObjects];
        [_trainingDaysIndex removeAllObjects];
    }
    
    for (TrainingDay *trainingDay in _trainingPlan.trainingDay) {
        if (trainingDay.restDay.intValue == 1) {
            [_items addObject:trainingDay];
            [_trainingDaysIndex addObject:@YES];
        }
    
        int index = 0;
        
        for (TrainingDayContent *trainingDayContent in trainingDay.trainingDayContent) {
            [_items addObject:trainingDayContent];
            if (index == 0) {
                [_trainingDaysIndex addObject:@YES];
            }
            else {
                [_trainingDaysIndex addObject:@NO];
            }
            index ++;
        }
    }
}

- (void)reloadDaysItems {
    if (_items.count > 0) {
        [_items removeAllObjects];
        [_trainingDaysIndex removeAllObjects];
    }
    
    for (TrainingDay *trainingDay in _trainingPlan.trainingDay) {
        if (trainingDay.restDay.intValue == 1) {
            [_items addObject:trainingDay];
            [_trainingDaysIndex addObject:@YES];
        }
        
        int index = 0;
        for (TrainingDayContent *trainingDayContent in trainingDay.trainingDayContent) {
            if (index == 0) {
                [_items addObject:trainingDayContent];
                [_trainingDaysIndex addObject:@YES];
                break;
            }
            
            index ++;
        }
    }
}

- (void)configureView {
    self.view.backgroundColor = BLACK_APP_COLOR;
    
    _trainingTableView = [[MBTwitterScroll alloc] initWithType:MBTableTrainingPlan];
    _trainingTableView.currentTrainingPlan = _trainingPlan;
    [_trainingTableView setupView];
    _trainingTableView.delegate = self;
    _trainingTableView.tableView.delegate = self;
    _trainingTableView.tableView.dataSource = self;
    [_trainingTableView.tableView registerClass:[TrainingPlanCell class] forCellReuseIdentifier:@"trainingPlanCell"];
    [_trainingTableView.tableView registerClass:[RelaxCell class] forCellReuseIdentifier:@"relaxCell"];
    _trainingTableView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _trainingTableView.tableView.bounces = NO;
    
    [self.view addSubview:_trainingTableView];
    [self.view addSubview:[self footerView]];
}

- (void)configureCollectionView {
    self.view.backgroundColor = BLACK_APP_COLOR;
    
    CSStickyHeaderFlowLayout *flowLayout = [[CSStickyHeaderFlowLayout alloc] init];
    flowLayout.parallaxHeaderReferenceSize = CGSizeMake(WIDTH,NAV_HEIGHT);
    flowLayout.scrollDirection =  UICollectionViewScrollDirectionVertical;
    flowLayout.minimumLineSpacing = [UtilManager height:10];
    flowLayout.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, [UtilManager height:175]);
    flowLayout.parallaxHeaderMinimumReferenceSize = CGSizeMake(self.view.frame.size.width, NAV_HEIGHT);
    flowLayout.parallaxHeaderAlwaysOnTop = YES;
    
    // If we want to disable the sticky header effect
    flowLayout.disableStickyHeaders = YES;
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, HEIGHT - TAB_BAR_HEIGHT) collectionViewLayout:flowLayout];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    [_collectionView registerClass:[TrainingPlanCell class] forCellWithReuseIdentifier:@"cell"];
    [_collectionView registerClass:[RelaxCell class] forCellWithReuseIdentifier:@"relaxCell"];
    _collectionView.backgroundColor = UIColor.clearColor;
    //[self.collectionView registerNib:self.headerNib
    //      forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader
    //             withReuseIdentifier:@"header"];
    [_collectionView registerClass:[TrainingPlanTopCell class] forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader withReuseIdentifier:@"CSStickyHeaderParallaxHeader"];
    [self.view addSubview:_collectionView];
    [self.view addSubview:[self footerView]];
}

- (void)reloadLayout {
    CSStickyHeaderFlowLayout *layout = (id)_collectionView.collectionViewLayout;
    
    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]]) {
        layout.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, [UtilManager height:175]);
        layout.parallaxHeaderMinimumReferenceSize = CGSizeMake(self.view.frame.size.width, NAV_HEIGHT);
        layout.itemSize = CGSizeMake(self.view.frame.size.width, layout.itemSize.height);
        layout.parallaxHeaderAlwaysOnTop = YES;
        
        // If we want to disable the sticky header effect
        layout.disableStickyHeaders = YES;
    }
    
}

- (UIView *)headerView {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:200])];
    headerView.backgroundColor = BLACK_HEADER_APP_COLOR;
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                      [UtilManager height:20],
                                                                      [UtilManager width:40] * 0.8,
                                                                      [UtilManager width:40] * 0.8)];
    
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:backButton];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(backButton.frame),
                                                                    CGRectGetMaxY(backButton.frame),
                                                                    WIDTH - 2 * CGRectGetMaxX(backButton.frame),
                                                                    [UtilManager height:45])];
    
    titleLabel.text = _trainingPlan.title;
    titleLabel.numberOfLines = 2;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = UIColor.whiteColor;
    titleLabel.font = [UIFont fontWithName:LIGHT_FONT size:21];
    titleLabel.adjustsFontSizeToFitWidth = YES;
    [headerView addSubview:titleLabel];
    
    long exercisesCount = 0;
    NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (TrainingDay *trainingDay in _trainingPlan.trainingDay) {
        exercisesCount = exercisesCount + trainingDay.trainingDayContent.count;
        for (TrainingDayContent *trainingDayContent in trainingDay.trainingDayContent) {
            for (Tag *tag in trainingDayContent.training.tags) {
                BOOL checkString = NO;
                for (Tag *checkTag in tags) {
                    if ([tag.title isEqualToString:checkTag.title]) {
                        checkString = YES;
                    }
                }
                
                if(!checkString) {
                    [tags addObject:tag];
                }
            }
        }
    }
     
    _tagView = [[TagView alloc] initWithFrame:CGRectMake(0.f,
                                                         CGRectGetMaxY(titleLabel.frame) + [UtilManager height:5],
                                                         WIDTH,
                                                         [UtilManager height:30])];
    
    _tagView.tags = tags;
    [_tagView reloadTagSubviews];
    _tagView.mode = TagsControlModeList;
    _tagView.isCenter = YES;
    [headerView addSubview:_tagView];
    
    UILabel *exercisesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f,
                                                                        CGRectGetMaxY(_tagView.frame) + [UtilManager height:10],
                                                                        [UtilManager width:120],
                                                                        [UtilManager height:25])];
    
    exercisesLabel.textColor = GRAY_REGISTER_FONT;
    exercisesLabel.textAlignment = NSTextAlignmentRight;
    exercisesLabel.text = [NSString stringWithFormat:@"%ld %@",exercisesCount,NSLocalizedString(@"Ejercicios", nil)];
    [headerView addSubview:exercisesLabel];
    
    UILabel *daysLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(exercisesLabel.frame) + [UtilManager width:15],
                                                                   CGRectGetMinY(exercisesLabel.frame),
                                                                   CGRectGetWidth(exercisesLabel.frame),
                                                                   CGRectGetHeight(exercisesLabel.frame))];
    
    daysLabel.font = exercisesLabel.font;
    daysLabel.textColor = exercisesLabel.textColor;
    daysLabel.text = [NSString stringWithFormat:@"%lu %@", (unsigned long)_trainingPlan.trainingDay.count,NSLocalizedString(@"días", nil)];
    [headerView addSubview:daysLabel];
    
    return headerView;
}

- (UIView *)footerView {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.f ,HEIGHT - TAB_BAR_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    footerView.backgroundColor = BLACK_HEADER_APP_COLOR;
    
    UIButton *moreButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                      0.f,
                                                                      [UtilManager width:40] * 0.8,
                                                                      [UtilManager width:40] * 0.8)];
    
    [moreButton setImage:[UIImage imageNamed:@"btn-add"] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    moreButton.center = CGPointMake(WIDTH/4, TAB_BAR_HEIGHT/2);
    [footerView addSubview:moreButton];
    
    UILabel *relaxDay = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH/2 + [UtilManager width:5],
                                                                  0.f,
                                                                  WIDTH/2 - [UtilManager width:10],
                                                                  [UtilManager height:25])];
    
    relaxDay.textAlignment = NSTextAlignmentRight;
    relaxDay.textColor = UIColor.whiteColor;
    relaxDay.font = [UIFont fontWithName:REGULAR_FONT size:24];
    relaxDay.adjustsFontSizeToFitWidth = YES;
    relaxDay.textColor = YELLOW_APP_COLOR;
    relaxDay.text = NSLocalizedString(@"DÍA DE DESCANSO", nil);
    relaxDay.center = CGPointMake(relaxDay.center.x, moreButton.center.y);
    relaxDay.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *gestureRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addRelaxDay)];
    [relaxDay addGestureRecognizer:gestureRecog];
    [footerView addSubview:relaxDay];
    
    return footerView;
}

- (void)didPressBackButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didPressTimePicker:(RLMObject *)rlmObject {
    if (![rlmObject isKindOfClass:[TrainingDay class]]) {
        return;
    }

    [self.view endEditing:YES];
    
    if(_startDatePicker) {
        [_startDatePicker removeFromSuperview];
        _startDatePicker = nil;
    }
    
    if(_endDatePicker) {
        [_endDatePicker removeFromSuperview];
        _endDatePicker = nil;
    }
    
    TrainingDayContent * trainingDayContent = [_items objectAtIndex:_selectedTrainingIndex];
    _startDatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,
                                                                      CGRectGetHeight(self.view.frame) - 216,
                                                                      CGRectGetWidth(self.view.frame) / 2,
                                                                      216)];
    
    _startDatePicker.datePickerMode = UIDatePickerModeTime;
    NSDate * startTime = [DateTimeUtil dateFromString:trainingDayContent.start
                                               format:@"HH:mm:ss"];
    
    _startDatePicker.date = startTime ? startTime : [NSDate date];
    
    _startDatePicker.backgroundColor = UIColor.whiteColor;
    _startDatePicker.userInteractionEnabled = YES;
    
    _endDatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_startDatePicker.frame),
                                                                      CGRectGetHeight(self.view.frame) - 216,
                                                                      CGRectGetWidth(self.view.frame) / 2,
                                                                      216)];
    
    _endDatePicker.datePickerMode = UIDatePickerModeTime;
    NSDate * endTime = [DateTimeUtil dateFromString:trainingDayContent.end
                                             format:@"HH:mm:ss"];
    
    _endDatePicker.date = endTime ? endTime : [NSDate date];
    _endDatePicker.backgroundColor = UIColor.whiteColor;
    _endDatePicker.userInteractionEnabled = YES;
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_endDatePicker.frame) - 80,
                                                               CGRectGetMinY(_startDatePicker.frame),
                                                               80,
                                                               24)];
    
    [_acceptButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptDatePickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_startDatePicker];
    [self.view addSubview:_endDatePicker];
    [self.view addSubview:_acceptButton];
}

- (void)moreAction {
    _selectedTrainingDay = nil;
    [self presentSelectorTableViewController];
}

- (void)addRelaxDay {
    TrainingDay *trainingDay = [[TrainingDay alloc] init];
    trainingDay.restDay = @1;
    trainingDay.order = @(_trainingPlan.trainingDay.count + 1);
    
    RLMRealm *realm = [AppContext currentRealm];
    [realm transactionWithBlock:^{
        [_trainingPlan.trainingDay addObject:trainingDay];
    }];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:_trainingPlan];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [_executive updateTrainingPlan:_trainingPlan];
    [self reloadItems];
}

- (void)presentSelectorTableViewController {
    _selectorTableViewController = [[SelectorTableViewController alloc] init];
    _selectorTableViewController.delegate = self;
    _selectorTableViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    _selectorTableViewController.selectTrainingPlan = _trainingPlan;
    _selectorTableViewController.selectorType = SelectorTypeTrainings;
    [self presentViewController:_selectorTableViewController animated:YES completion:nil];
}

#pragma mark OptionsViewController Delegate Methods

- (void)didPressAddToCalendar:(RLMObject *)rlmObject {
    if (![rlmObject isKindOfClass:[TrainingPlan class]]) {
        return;
    }
    
    [self.view endEditing:YES];
    
    if(_datePicker) {
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.f,
                                                                 CGRectGetHeight(self.view.frame) - 216,
                                                                 CGRectGetWidth(self.view.frame),
                                                                 216)];
    
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _datePicker.date = [NSDate date];
    _datePicker.backgroundColor = UIColor.whiteColor;
    _datePicker.userInteractionEnabled = YES;
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(_datePicker.frame) - 80.f,
                                                               CGRectGetMinY(_datePicker.frame),
                                                               80.f,
                                                               24.f)];
    
    [_acceptButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptDatePickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_datePicker];
    [self.view addSubview:_acceptButton];
}

- (void)acceptDatePickerAction {
    if(_acceptButton && _datePicker) {
        TrainingPlan * trainingPlan = _trainingPlan;
        [_executive addTrainingPlan:trainingPlan toDate:_datePicker.date];
        
        [_acceptButton removeFromSuperview];
        _acceptButton = nil;
        
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
    else if (_acceptButton &&
             _startDatePicker &&
             _endDatePicker) {
        
        TrainingDayContent * trainingDayContent = [_items objectAtIndex:_selectedTrainingIndex];
        [_executive updateTrainingDayConent:trainingDayContent
                             ofTrainingPlan:_trainingPlan
                                  startTime:_startDatePicker.date
                                    endTime:_endDatePicker.date];
        
        [_acceptButton removeFromSuperview];
        _acceptButton = nil;
        
        [_startDatePicker removeFromSuperview];
        _startDatePicker = nil;
        [_endDatePicker removeFromSuperview];
        _endDatePicker = nil;
    }
}

- (void)didPressReplace:(RLMObject*)rlmObject {
    if (![rlmObject isKindOfClass:[TrainingDay class]]) {
        return;
    }
    
    _selectorTableViewController = [[SelectorTableViewController alloc] init];
    _selectorTableViewController.delegate = self;
    _selectorTableViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    _selectorTableViewController.selectTrainingPlan = _trainingPlan;
    _selectorTableViewController.selectTrainingDayContent = [_items objectAtIndex:_selectedTrainingIndex];
    _selectorTableViewController.selectorType = SelectorTypeTrainingSingle;
    
    [self presentViewController:_selectorTableViewController animated:YES completion:nil];
}

- (void)didPressDeleteFitness:(RLMObject*)rlmObject {
    if (![rlmObject isKindOfClass:[TrainingPlan class]]) {
        return;
    }

    if (_fromCalendar) {
        [SchedulePlan deleteSchedulePlan:_schedulePlan];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)addWorkoutsToTrainingDay:(TrainingDay *)trainingDay{
    _selectorTableViewController = [[SelectorTableViewController alloc] init];
    _selectorTableViewController.delegate = self;
    _selectorTableViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    _selectorTableViewController.selectTrainingPlan = _trainingPlan;
    _selectorTableViewController.selectorType = SelectorTypeTrainings;
    _selectedTrainingDay = trainingDay;
    [self presentViewController:_selectorTableViewController animated:YES completion:nil];
}

- (void)editWorkOut:(Training *)selectWorkout {
    TrainingAddViewController *trainingAddViewController = [[TrainingAddViewController alloc] init];
    trainingAddViewController.editTraining = selectWorkout;
    trainingAddViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:trainingAddViewController animated:YES completion:nil];
}

- (void)removeTrainingPlan {
    _isRemoveTrainingPlan = YES;
}

- (void)editTrainingPlan:(TrainingPlan *)trainingPlan {
    TrainingPlanAddViewController *trainingPlanAddViewController = [[TrainingPlanAddViewController alloc] init];
    trainingPlanAddViewController.currentTrainingPlan = _trainingPlan;
    trainingPlanAddViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:trainingPlanAddViewController animated:YES completion:nil];
}

- (void)didPressReplicateTodo {
    NSMutableArray<TrainingDay*>* trainingDaysAux = [[NSMutableArray alloc] init];
    NSInteger order = _trainingPlan.trainingDay.count;
    for (TrainingDay * trainingDay in _trainingPlan.trainingDay) {
        TrainingDay * copyTrainingDay = [TrainingDay copyTrainingDay:trainingDay];
        copyTrainingDay.order = [NSNumber numberWithInteger:order];
        order = order + 1;
        [trainingDaysAux addObject:copyTrainingDay];
    }
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm transactionWithBlock:^{
        [_trainingPlan.trainingDay addObjects:trainingDaysAux];
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [_executive updateTrainingPlan:_trainingPlan];
}

- (void)didPressSendFood:(RLMObject *)rlmObject {
    _userSelectorViewController = [[UserSelectorViewController alloc] init];
    _userSelectorViewController.rlmObject = rlmObject;
    _userSelectorViewController.delegate = self;
    _userSelectorViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:_userSelectorViewController animated:YES completion:nil];
}

- (void)didPressShareFood:(RLMObject *)rlmObject {
    [_executive shareContent:rlmObject];
}

#pragma mark HeaderDeelgate

- (void)backTopHeaderAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)menuTopHeaderAction {
    OptionsViewController *optionsViewController = [[OptionsViewController alloc] init];
    optionsViewController.currentTrainingPlan = _trainingPlan;
    optionsViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsViewController.delegate = self;
    [self presentViewController:optionsViewController animated:YES completion:nil];
}

#pragma mark - SelectorTableViewDelegate

- (void)returnData:(NSMutableArray *)returnData withType:(SelectorType)selectorType {
    if(returnData.count > 0) {
        int order = 0;
        
        for(Exercise *exercise in returnData) {
            TrainingExercise *trainingExercise = [[TrainingExercise alloc] init];
            trainingExercise.isSelected = @NO;
            trainingExercise.order = @(order);
            [trainingExercise.exercises addObject:exercise];
            trainingExercise.title = exercise.title;
            trainingExercise.owner = [[AppContext sharedInstance] userId];
            order++;
        }
    }
    else {
        _trainingTableView.currentTrainingPlan = _trainingPlan;
        [_trainingTableView.tableView reloadData];
        [_trainingTableView reloadTrainingPlansValues];
    }
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    _selectedTrainingIndex = indexPath.row;
    if ([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]) {
        
    }
    else {
        
        TrainingDayContent *selectTrainingDayContent = [_items objectAtIndex:indexPath.row];
        Training *training;
        training = [Training trainingWithServerId:selectTrainingDayContent.trainingId];
        
        [self presentWorkOutViewController:training];
    }
}

#pragma mark -  UITableViewDataSource Methods

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]) {
        return [UtilManager height:50];
    }
    else {
        return [UtilManager height:80];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]) {
        return [UtilManager height:50];
    }
    else {
        if (_isSimplify) {
            return [UtilManager height:40];
        }
        else {
            return [UtilManager height:80];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section {
    return TAB_BAR_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return TAB_BAR_HEIGHT;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, WIDTH, TAB_BAR_HEIGHT)];
    footerView.backgroundColor = UIColor.clearColor;
    return footerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]) {
        TrainingDay *trainingDay = [_items objectAtIndex:indexPath.row];
        if([trainingDay.restDay isEqual:@1]){
            int aux = 0;
            for(int i = 0 ; i <= indexPath.row; i++) {
                if ([[_trainingDaysIndex objectAtIndex:i] isEqual:@NO]) {
                    aux++;
                }
            }
            
            RelaxCell *relaxCell = [_trainingTableView.tableView dequeueReusableCellWithIdentifier:@"relaxCell"];
            
            //[relaxCell.dayLabel setText:[NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"Día", nil),trainingDay.order.intValue]];
            
            [relaxCell.dayLabel setText:[NSString stringWithFormat:@"%@ %ld",NSLocalizedString(@"Día", nil),indexPath.row + 1 - aux]];
            [relaxCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            //relaxCell.revealView = bottomView;
            //bottomView.backgroundColor =  [UIColor redColor];
            
            //relaxCell.allowedDirection = CADRACSwippableCellAllowedDirectionLeft;
            //relaxCell.revealView = bottomView;
            
            /*
             [relaxCell.workoutImageView setHidden:YES];
             [relaxCell.titleLabel setHidden:TRUE];
             [relaxCell.clockImageView setHidden:TRUE];
             [relaxCell.kCalImageView setHidden:TRUE];
             [relaxCell.energyLabel setHidden:TRUE];
             [relaxCell.separatorView setHidden:TRUE];*/
            
            return relaxCell;
        }
    }
    else {
        TrainingPlanCell *cell = [_trainingTableView.tableView dequeueReusableCellWithIdentifier:@"trainingPlanCell"];
        cell.serialIndex = indexPath.row;
        cell.isSimplify = _isSimplify;
        
        TrainingDayContent *trainingDayContent = [_items objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        CGFloat separatorViewHeight;
        if ([[_trainingDaysIndex objectAtIndex:indexPath.row] isEqual:@YES]) {
            cell.dayLabel.hidden = NO;
            cell.menuButton.hidden = NO;
            cell.circleImageView.hidden = NO;
            cell.isFirstDayContent = @YES;
            separatorViewHeight = [UtilManager height:55];
            if (cell.isSimplify) {
                separatorViewHeight = [UtilManager height:28];
            }
            cell.verticalSeparatorView.frame = CGRectMake(CGRectGetMinX(cell.verticalSeparatorView.frame),
                                                          CGRectGetMaxY(cell.circleImageView.frame) + [UtilManager height:5],
                                                          1,
                                                          separatorViewHeight);
            
        }
        else {
            cell.dayLabel.hidden = YES;
            cell.circleImageView.hidden = YES;
            cell.isFirstDayContent = @NO;
            separatorViewHeight = [UtilManager height:90];
            if (cell.isSimplify) {
                separatorViewHeight = [UtilManager height:40];
            }
            cell.verticalSeparatorView.frame = CGRectMake(CGRectGetMinX(cell.verticalSeparatorView.frame),
                                                          0,
                                                          1,
                                                          [UtilManager height:90]);
        }
        
        [cell setNeedsLayout];
        
        Training *training = trainingDayContent.training;
        if (!training) {
            training = [Training trainingWithServerId:trainingDayContent.trainingId];
            if (training) {
                RLMRealm *realm = [AppContext currentRealm];
                
                [realm transactionWithBlock:^{
                    [trainingDayContent setTraining:training];
                }];
                
                [realm beginWriteTransaction];
                NSError *error;
                [realm commitWriteTransaction:&error];
                if(error){
                    NSLog(@"%@",error.description);
                }
            }
        }
        
        cell.titleLabel.text = training.title;
        int order = 0;
        for (NSInteger index = 0; index <= indexPath.row; index++) {
            if ([_trainingDaysIndex[index] boolValue]) {
                order ++;
            }
        }
        
        cell.dayLabel.text = [NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"Día", nil),order];
        cell.currentIndex = order-1;
        
//        for(TrainingDay *trainingDay in _trainingPlan.trainingDay){
//            for(TrainingDayContent *dayContent in trainingDay.trainingDayContent){
//                if([dayContent.trainingId isEqual:trainingDayContent.trainingId]){
//                    //[cell.dayLabel setText:[NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"Día", nil),trainingDay.order.intValue]];
//                    [cell.dayLabel setText:[NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"Día", nil),order]];
//                    [cell setCurrentIndex:order-1];
//                    break;
//                }
//            }
//            order ++;
//        }
        
        NSInteger kCal = 0;
        
        for(TrainingExercise *trainingExercise in training.trainingExercises){
            kCal = kCal + [TrainingExercise getKCals:trainingExercise];
        }
        cell.kCalLabel.text = [NSString stringWithFormat:@"%ld kCal", (long)kCal];
        
        if (training.trainingExercises.count == 1) {
            cell.exerciseLabel.text = [NSString stringWithFormat:@"1 %@", NSLocalizedString(@"Ejercicio", nil)];
        }
        else {
            cell.exerciseLabel.text = [NSString stringWithFormat:@"%lu %@",
                                       MAX((unsigned long)training.exercises.count,
                                           training.trainingExercises.count),
                                       NSLocalizedString(@"Ejercicios", nil)];
            
        }
        
        int sec = [Training getMinutes:training];
        
        int minutes = sec / 60;
        if (minutes < 60) {
            cell.timeLabel.text = [NSString stringWithFormat:@"%d min", minutes];
        }
        else {
            int hour = minutes / 60;
            minutes = minutes % 60;
            cell.timeLabel.text = [NSString stringWithFormat:@"%d h %d min", hour, minutes];
        }
        
        NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
        for (Tag *tag in training.tags) {
            [tags addObject:tag];
        }
        
        for (Tag *tagsExercise in [Training getTrainingsExercisesTags:training]) {
            [tags addObject:tagsExercise];
        }
        cell.delegate = self;
        
        cell.tagView.tags = tags;
        [cell.tagView reloadTagSubviews];
        
        return cell;
    }
    
    return nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]) {
        return YES;
    }
    else {
        if ([[_trainingDaysIndex objectAtIndex:indexPath.row]isEqual:@YES]) {
            return YES;
        }
        else {
            return YES;
        }
    }

    return YES;
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]) {
        return YES;
    }
    else {
        if([[_trainingDaysIndex objectAtIndex:indexPath.row]isEqual:@YES]) {
            return YES;
        }
        else {
            return NO;
        }
    }
    
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    TrainingDay *sourceTrainingDay;
    TrainingDay *destinationTrainingDay;
    
    if ([[_items objectAtIndex:sourceIndexPath.row] isKindOfClass:[TrainingDay class]]) {
        sourceTrainingDay = [_items objectAtIndex:sourceIndexPath.row];
    }
    else {
        sourceTrainingDay = [_trainingPlan.trainingDay objectAtIndex:sourceIndexPath.row];
    }
    
    if([[_items objectAtIndex:destinationIndexPath.row] isKindOfClass:[TrainingDay class]]) {
        destinationTrainingDay = [_items objectAtIndex:destinationIndexPath.row];
    }
    else {
        destinationTrainingDay = [_trainingPlan.trainingDay objectAtIndex:destinationIndexPath.row];
    }
    
    if (sourceTrainingDay &&
        destinationTrainingDay) {
        
        RLMRealm *realm  = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            [_trainingPlan.trainingDay replaceObjectAtIndex:sourceIndexPath.row withObject:destinationTrainingDay];
            [_trainingPlan.trainingDay replaceObjectAtIndex:destinationIndexPath.row withObject:sourceTrainingDay];
        }];
        
        [realm beginWriteTransaction];
        NSError *error;
        [realm commitWriteTransaction:&error];
        if(error){
            NSLog(@"%@",error.description);
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]) {
        return YES;
    }
    else {
        //if([[_trainingDaysIndex objectAtIndex:indexPath.row]isEqual:[NSNumber numberWithBool:TRUE]])
            return YES;
        //else
        //    return FALSE;
    }
    
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]) {
        return YES;
    }
    else {
        //if([[_trainingDaysIndex objectAtIndex:indexPath.row]isEqual:[NSNumber numberWithBool:TRUE]])
            return YES;
        //else
        //    return FALSE;
    }
    
    return YES;
}

- (nullable NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(8_0) __TVOS_PROHIBITED {
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"Delete", nil)  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        [_trainingTableView.tableView setEditing:NO];
        
        if (_items.count > 1) {
            if ([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]) {
                RLMRealm *realm  = [AppContext currentRealm];
                
                [realm transactionWithBlock:^{
                    [_trainingPlan.trainingDay removeObjectAtIndex:indexPath.row];
                }];
                
                [realm beginWriteTransaction];
                NSError *error;
                [realm commitWriteTransaction:&error];
                if(error){
                    NSLog(@"%@",error.description);
                }
            }
            else {
                int trainingDayIndex = 0;
                int count = 0;
                
                for (TrainingDay *trainingDay in _trainingPlan.trainingDay) {
                    if (trainingDay.restDay.intValue == 1) {
                        if (count == indexPath.row) {
                            RLMRealm *realm  = [AppContext currentRealm];
                            
                            [realm transactionWithBlock:^{
                                [_trainingPlan.trainingDay removeObjectAtIndex:trainingDayIndex];
                            }];
                            
                            [realm beginWriteTransaction];
                            NSError *error;
                            [realm commitWriteTransaction:&error];
                            if (error){
                                NSLog(@"%@",error.description);
                            }
                            break;
                        }
                        count ++;
                        trainingDayIndex ++;
                    }
                    else {
                        if(count + trainingDay.trainingDayContent.count < indexPath.row) {
                            count = count + (int)trainingDay.trainingDayContent.count;
                            trainingDayIndex ++;
                        }
                        else if (count + trainingDay.trainingDayContent.count >= indexPath.row) {
                            if (trainingDay.trainingDayContent.count == 1) {
                                RLMRealm *realm  = [AppContext currentRealm];
                                
                                [realm transactionWithBlock:^{
                                    [_trainingPlan.trainingDay removeObjectAtIndex:trainingDayIndex];
                                }];
                                
                                [realm beginWriteTransaction];
                                NSError *error;
                                [realm commitWriteTransaction:&error];
                                if (error) {
                                    NSLog(@"%@",error.description);
                                }
                                break;
                            }
                            else {
                                int trainingDayContentIndex = 0;
                                for (TrainingDayContent *trainingDayContent in trainingDay.trainingDayContent) {
                                    if (count == indexPath.row) {
                                        RLMRealm *realm  = [AppContext currentRealm];
                                        
                                        [realm transactionWithBlock:^{
                                            [[[_trainingPlan.trainingDay objectAtIndex:trainingDayIndex] trainingDayContent] removeObjectAtIndex:trainingDayContentIndex];
                                        }];
                                        
                                        [realm beginWriteTransaction];
                                        NSError *error;
                                        [realm commitWriteTransaction:&error];
                                        if (error) {
                                            NSLog(@"%@",error.description);
                                        }
                                    }
                                    else {
                                        trainingDayContentIndex ++;
                                        count ++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            [self reloadItems];
            [_trainingTableView.tableView reloadData];
            
            [_executive updateTrainingPlan:_trainingPlan];
            
        }
        else {
            [self presentViewController:[UtilManager presentAlertWithMessage:@"Un entrenamiento debe tener un ejercicio. Puedo borrar directamente el entrenamiento."] animated:YES completion:^{
            }];
        }
    }];
    deleteAction.backgroundColor = UIColor.redColor;
    
    UITableViewRowAction *moreAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"+", nil)  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        [_trainingTableView.tableView setEditing:NO];
        int order = 0;
        for (NSInteger index = 0; index <= indexPath.row; index++) {
            if ([_trainingDaysIndex[index] boolValue]) {
                order ++;
            }
        }
        
        TrainingDay *trainingDay = [_trainingPlan.trainingDay objectAtIndex:order - 1];
        [self addWorkoutsToTrainingDay:trainingDay];
    }];
    moreAction.backgroundColor = UIColor.lightGrayColor;
    
    return @[deleteAction, moreAction];
}

#pragma mark - TrainingCellDelegateMethods

- (void)optionTrainingDayDidPush:(NSInteger)index serialIndex:(NSInteger)serialIndex {
    OptionsViewController *optionsViewController = [[OptionsViewController alloc] init];
    
    TrainingDay *trainingDay = [_trainingPlan.trainingDay objectAtIndex:index];
    NSInteger trainingDayContentIndex = serialIndex;
    for (NSInteger trainingDayIndex = 0; trainingDayIndex < index; trainingDayIndex++) {
        trainingDayContentIndex = trainingDayContentIndex - [_trainingPlan.trainingDay objectAtIndex:trainingDayIndex].trainingDayContent.count;
    }
    TrainingDayContent * trainingDayContent = [trainingDay.trainingDayContent objectAtIndex:trainingDayContentIndex];
    
    _selectTrainingDayIndex = index;
    _selectedTrainingIndex = serialIndex;
    
    optionsViewController.currentTrainingPlan = _trainingPlan;
    optionsViewController.trainingDay = trainingDay;
    optionsViewController.trainingDayContent = trainingDayContent;
    optionsViewController.optionType = OptionTypeTrainingDay;
    optionsViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsViewController.delegate = self;
    optionsViewController.fromDetailOfParent = YES;
    [self presentViewController:optionsViewController animated:YES completion:nil];
}

#pragma mark - UICollectionViewDelegate Methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]){
    
    }
    else {
        TrainingDayContent *selectTrainingDayContent = [_items objectAtIndex:indexPath.row];
        TrainingDay *selectTrainingDay;
        
        for(TrainingDay *trainingDay in _trainingPlan.trainingDay){
            for(TrainingDayContent *trainingDayContent in trainingDay.trainingDayContent){
                if([trainingDayContent.trainingId isEqualToString:selectTrainingDayContent.trainingId])
                    selectTrainingDay = trainingDay;
            }
        }
        
        Training *training;
        if (selectTrainingDayContent.training != nil) {
            training = selectTrainingDayContent.training;
        }
        else {
            training = [Training trainingWithServerId:selectTrainingDayContent.trainingId];
        }
        
        if (training &&
            training.trainingExercises.count > 0 &&
            training.trainingExercises) {
            
            WorkoutDetailViewController *workoutdetailViewController = [[WorkoutDetailViewController alloc] init];
            workoutdetailViewController.currentTrainingPlan = _trainingPlan;
            workoutdetailViewController.currentTrainingDay = selectTrainingDay;
            workoutdetailViewController.currentTraining = training;
            workoutdetailViewController.trainingDayContent = selectTrainingDayContent;
            [self.navigationController pushViewController:workoutdetailViewController animated:YES];
        }
        else {
            [MRProgressOverlayView showOverlayAddedTo:self.view animated:YES];
            _selectedTrainingDayContent = selectTrainingDayContent;
        }
    }
}

#pragma mark UICollectionViewFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]) {
        return CGSizeMake(WIDTH, [UtilManager height:50]);
    }
    else {
        return CGSizeMake(WIDTH , [UtilManager height:80]);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake([UtilManager height:0], [UtilManager width:0], [UtilManager height:0], [UtilManager width:0]);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return [UtilManager height:0];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return [UtilManager height:0];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(0.,0.);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeMake(0.,0.);
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    //return _currentTraining.trainingExercises.count;
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _items.count;
    //return _trainingPlan.trainingDay.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]) {
        TrainingDay *trainingDay = [_items objectAtIndex:indexPath.row];
        if ([trainingDay.restDay isEqual:@1]) {
            int aux = 0;
            for (int i = 0 ; i <= indexPath.row; i++) {
                if ([[_trainingDaysIndex objectAtIndex:i] isEqual:@NO]) {
                    aux++;
                }
            }
            
            RelaxCell *relaxCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"relaxCell" forIndexPath:indexPath];
            
            //[relaxCell.dayLabel setText:[NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"Día", nil),trainingDay.order.intValue]];
            
            relaxCell.dayLabel.text = [NSString stringWithFormat:@"%@ %ld",
                                       NSLocalizedString(@"Día", nil),
                                       indexPath.row + 1 - aux];
            
            UIView *bottomView = [[UIView alloc] init];
            bottomView.frame = (CGRect){
                .size = CGSizeMake(CGRectGetWidth(collectionView.bounds)/2, CGRectGetHeight(relaxCell.bounds))
            };
            
            //relaxCell.revealView = bottomView;
            //bottomView.backgroundColor =  [UIColor redColor];
            
            //relaxCell.allowedDirection = CADRACSwippableCellAllowedDirectionLeft;
            //relaxCell.revealView = bottomView;
            
            /*
             [relaxCell.workoutImageView setHidden:YES];
             [relaxCell.titleLabel setHidden:TRUE];
             [relaxCell.clockImageView setHidden:TRUE];
             [relaxCell.kCalImageView setHidden:TRUE];
             [relaxCell.energyLabel setHidden:TRUE];
             [relaxCell.separatorView setHidden:TRUE];*/
            
            return relaxCell;
        }
    }
    else {
        TrainingPlanCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        TrainingDayContent *trainingDayContent = [_items objectAtIndex:indexPath.row];
    
        if ([[_trainingDaysIndex objectAtIndex:indexPath.row]isEqual:@YES]) {
            cell.dayLabel.hidden = NO;
            cell.menuButton.hidden = NO;
            cell.circleImageView.hidden = NO;
            cell.isFirstDayContent = @YES;
            cell.verticalSeparatorView.frame = CGRectMake(CGRectGetMinX(cell.verticalSeparatorView.frame),
                                                          CGRectGetMaxY(cell.circleImageView.frame) + [UtilManager height:5],
                                                          1,
                                                          [UtilManager height:55]);
            
        }
        else {
            cell.dayLabel.hidden = YES;
            cell.menuButton.hidden = YES;
            cell.circleImageView.hidden = YES;
            cell.isFirstDayContent = @NO;
            cell.verticalSeparatorView.frame = CGRectMake(CGRectGetMinX(cell.verticalSeparatorView.frame),
                                                          0,
                                                          1,
                                                          [UtilManager height:90]);
            
        }
        
        [cell setNeedsLayout];
        
        Training *training = trainingDayContent.training;
        if (!training) {
            training = [Training trainingWithServerId:trainingDayContent.trainingId];
            if (training) {
                RLMRealm *realm = [AppContext currentRealm];
                
                [realm transactionWithBlock:^{
                    [trainingDayContent setTraining:training];
                }];
    
                [realm beginWriteTransaction];
                NSError *error;
                [realm commitWriteTransaction:&error];
                if (error) {
                    NSLog(@"%@",error.description);
                }
            }
        }
        
        cell.titleLabel.text = training.title;
        
        int order = 1;
        for (TrainingDay *trainingDay in _trainingPlan.trainingDay) {
            for (TrainingDayContent *dayContent in trainingDay.trainingDayContent) {
                if ([dayContent.trainingId isEqual:trainingDayContent.trainingId]) {
                    cell.dayLabel.text = [NSString stringWithFormat:@"%@ %d", NSLocalizedString(@"Día", nil),order];
                    cell.currentIndex = order - 1;
                    break;
                }
            }
            order ++;
        }
        
        NSInteger kCal = 0;
        for (TrainingExercise *trainingExercise in training.trainingExercises) {
            kCal = kCal + [TrainingExercise getKCals:trainingExercise];
        }
        cell.kCalLabel.text = [NSString stringWithFormat:@"%ld kCal", (long)kCal];
        
        if (training.trainingExercises.count == 1) {
            cell.exerciseLabel.text  = [NSString stringWithFormat:@"1 %@", NSLocalizedString(@"Ejercicio", nil)];
        }
        else {
            cell.exerciseLabel.text = [NSString stringWithFormat:@"%lu %@",
                                       MAX((unsigned long)training.exercises.count,
                                           training.trainingExercises.count),
                                       NSLocalizedString(@"Ejercicios", nil)];
            
        }
        
        int sec = [Training getMinutes:training];
        cell.timeLabel.text = [NSString stringWithFormat:@"%d min",sec / 60];
        
        NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
        for (Tag *tag in training.tags) {
            [tags addObject:tag];
        }
        
        for (Tag *tagsExercise in [Training getTrainingsExercisesTags:training]) {
            [tags addObject:tagsExercise];
        }
        
        cell.delegate = self;
        
        cell.tagView.tags = tags;
        [cell.tagView reloadTagSubviews];
        
        return cell;
    }
    
    return nil;
    
    /*
    UIView *bottomView = [[UIView alloc] init];
    bottomView.frame = (CGRect){
        .size = CGSizeMake(CGRectGetWidth(collectionView.bounds)/2, CGRectGetHeight(cell.bounds))
    };
    
    cell.allowedDirection = CADRACSwippableCellAllowedDirectionLeft;
    [cell setCurrentIndex:indexPath.row];
    [cell setDelegate:self];
    cell.revealView = bottomView;

     [[cell.revealViewSignal filter:^BOOL(NSNumber *isRevealed) {
     return [isRevealed boolValue];
     }] subscribeNext:^(id x) {
     [[self.collectionView visibleCells] enumerateObjectsUsingBlock:^(CustomCollectionCell *otherCell, NSUInteger idx, BOOL *stop) {
     if (otherCell != cell)
     {
     [otherCell hideRevealViewAnimated:YES];
     }
     }];
     }];
     
     
     bottomView.backgroundColor =  [UIColor redColor];
     */
    
    //TrainingDayContent *trainingDayContent = [trainingDay.trainingDayContent objectAtIndex:0];
    
    //[[cell energyLabel] setText:[NSString stringWithFormat:@"%d kCal",exercise.energy.intValue]];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        CSSectionHeader *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                   withReuseIdentifier:@"sectionHeader"
                                                                          forIndexPath:indexPath];
        
        
        return cell;
        
    }
    else if ([kind isEqualToString:CSStickyHeaderParallaxHeader]) {
        TrainingPlanTopCell *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                       withReuseIdentifier:@"CSStickyHeaderParallaxHeader"
                                                                              forIndexPath:indexPath];
        
        
//        [[cell daysLabel] setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_trainingPlan.trainingDay.count,NSLocalizedString(@"Días", nil)]];
//
//        [[cell exercisesLabel] setText:[NSString stringWithFormat:@"%d %@",[TrainingPlan numberOfTrainings:_trainingPlan],NSLocalizedString(@"Entrenamientos", nil)]];
//        [cell.titleHeaderLabel setText:_trainingPlan.title];
//        [cell setDelegate:self];
//
//        [[cell tagView] setTags:[TrainingPlan getTrainingsTags:_trainingPlan]];
//        [[cell tagView] reloadTagSubviews];
        
        return cell;
    }
    
    return nil;
}

#pragma mark MBTwitterScrollDelegate Methods

- (void)editButtonDidPress {
    if (_trainingTableView.tableView.editing) {
        [self reloadItems];
        [_trainingTableView.tableView setEditing:FALSE animated:TRUE];
        [_trainingTableView.tableView reloadData];
        [_executive updateTrainingPlan:_trainingPlan];
    }
    else {
        [self reloadDaysItems];
        [_trainingTableView.tableView reloadData];
        [_trainingTableView.tableView setEditing:TRUE animated:TRUE];
    }
}

- (void)didPressShowInfoButton {
    _infoPopupViewController.rlmObject = _trainingPlan;
    [self.view addSubview:_infoPopupViewController.view];
}

- (void)didPressSimplifyButton:(UIButton *)sender {
    _isSimplify = sender.selected;
    [_trainingTableView.tableView reloadData];
}

- (void)didPressUserProfileButton {
}

- (void)didPressUserRateButton {
    NSString * trainingPlanId = _trainingPlan.serverTrainingPlanId;
    [WebServiceManager getTrainingPlansWithCompletionBlock:^(BOOL isSuccess) {
        _trainingPlan = [TrainingPlan getTrainingPlanWithServerId:trainingPlanId];
        [self presentUserRateView:_trainingPlan];
    }];
}

- (void)backButtonDidPress {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)optionButtonDidPress {
    OptionsViewController *optionsViewController = [[OptionsViewController alloc] init];
    optionsViewController.currentTrainingPlan = _trainingPlan;
    optionsViewController.optionType = OptionTypeTrainings;
    optionsViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsViewController.delegate = self;
    optionsViewController.fromCalendar = _fromCalendar;
    [self presentViewController:optionsViewController animated:YES completion:nil];
}

#pragma mark WebServiceDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:nil];
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object {
    if (webServiceType == WebServiceTypeGetTraining) {
        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
            NSDictionary *returnObject = (NSDictionary *)object;
            
            NSLog(@"%@",returnObject);
            
            NSString *workoutId = [returnObject valueForKey:@"trainingId"];
            
            Training *currentTraining = [Training trainingWithId:workoutId];
            
            if (!currentTraining) {
                [ParserManager parseTraining:returnObject];
                currentTraining = [Training trainingWithId:workoutId];
            }
            
            WorkoutDetailViewController *workoutdetailViewController = [[WorkoutDetailViewController alloc] init];
            workoutdetailViewController.currentTrainingPlan = _trainingPlan;
            workoutdetailViewController.currentTraining = currentTraining;
            workoutdetailViewController.currentTrainingDay = _selectedTrainingDay;
            workoutdetailViewController.trainingDayContent = _selectedTrainingDayContent;
            [self.navigationController pushViewController:workoutdetailViewController animated:YES];
        }];
    }
    else {
        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
            NSDictionary *workoutDic = (NSDictionary *)object;
            NSString *workoutId = [workoutDic valueForKey:@"_id"];
            Training *currentTraining = [Training trainingWithId:workoutId];
            
            WorkoutDetailViewController *workoutdetailViewController = [[WorkoutDetailViewController alloc] init];
            workoutdetailViewController.currentTrainingPlan = _trainingPlan;
            workoutdetailViewController.currentTraining = currentTraining;
            [self.navigationController pushViewController:workoutdetailViewController animated:YES];
        }];
    }
    
    [self reloadItems];
    _trainingTableView.currentTrainingPlan = _trainingPlan;
    [_trainingTableView.tableView reloadData];
    [_trainingTableView reloadTrainingPlansValues];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object {
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:nil];
}

#pragma mark - TrainingPlanDisplay Delegate Methods

- (void)didUpdateTrainingPlan:(TrainingPlan *)trainingPlan {
    [self reloadItems];
    _trainingTableView.currentTrainingPlan = trainingPlan;
    [_trainingTableView.tableView reloadData];
    [_trainingTableView reloadTrainingPlansValues];
}

- (void)reloadAllFitness {
    [WebServiceManager getTrainingsWithCompletionBlock:^(BOOL isSuccess) {
        [WebServiceManager getTrainingPlansWithCompletionBlock:^(BOOL isSuccess) {
        }];
    }];
}

- (void)presentWorkOutViewController:(Training *)training {
    WorkoutDetailViewController *trainingDetailViewController = [[WorkoutDetailViewController alloc] init];
    trainingDetailViewController.currentTraining = training;
    [self.navigationController pushViewController:trainingDetailViewController animated:YES];
}

#pragma mark - UserSelectorViewControllerDelegate Methods

- (void)didPressOKWithSelectedResults:(NSArray*)selectedResults
                           andContent:(RLMObject*)content {
    
    UserMeta * userMeta = [selectedResults firstObject];
    [_executive sendContent:content
                    toToken:userMeta.token];
}

@end
