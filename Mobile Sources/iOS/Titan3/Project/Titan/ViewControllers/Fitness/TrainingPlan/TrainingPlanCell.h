//
//  TrainingPlanCell.h
//  Titan
//
//  Created by Manuel Manzanera on 26/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagView.h"

@protocol TrainingPlanCellDelegate <NSObject>

- (void)optionTrainingDayDidPush:(NSInteger)index serialIndex:(NSInteger)serialIndex;

@end

@interface TrainingPlanCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *separatorView;

@property (nonatomic, strong) UIImageView *circleImageView;
@property (nonatomic, strong) UIView *verticalSeparatorView;
@property (nonatomic, strong) UILabel *dayLabel;
@property (nonatomic, strong) UILabel *exerciseLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) UILabel *kCalLabel;
@property (nonatomic, strong) UIButton *menuButton;

@property (nonatomic, strong) NSNumber *isFirstDayContent;

@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, assign) NSInteger serialIndex;
@property (nonatomic, strong) UIView *panView;
@property (nonatomic, assign) id<TrainingPlanCellDelegate>delegate;

@property (nonatomic, assign) BOOL isSimplify;

@end
