//
//  TrainingPlanAddViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 14/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "FitnessViewController.h"
#import "TrainingPlan.h"

@interface TrainingPlanAddViewController : ParentViewController

@property (nonatomic, assign) FitnessViewController *fitnessViewController;
@property (nonatomic, strong) TrainingPlan *currentTrainingPlan;

@end
