//
//  SegmentedCollectionViewCell.m
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SegmentedCollectionViewCell.h"

#import "AppConstants.h"
#import "UtilManager.h"

@implementation SegmentedCollectionViewCell

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        
        [self setBackgroundColor:[UIColor clearColor]];
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., self.bounds.size.width, self.bounds.size.height)];
        [_iconImageView setCenter:self.contentView.center];
        [_iconImageView setContentMode:UIViewContentModeScaleAspectFit];
        
        [[self contentView] addSubview:_iconImageView];
        
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(self.bounds.size.width - 1.5, [UtilManager height:5], [UtilManager width:1],[UtilManager height:frame.size.height - 10])];
        [separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        [[self contentView] addSubview:separatorView];
        
        _selectedView = [[UIView alloc] initWithFrame:CGRectMake(0., self.bounds.size.height - 4, self.bounds.size.width, 4)];
        [_selectedView setBackgroundColor:[UIColor clearColor]];
        [_selectedView setHidden:YES];
        
        [[self contentView] addSubview:_selectedView];
        
    }
    return self;
}

- (void) configCellWithData:(id)data {
}

- (UIFont*) selectedFont {
    return [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0];
}

- (UIFont*) normalFont {
    return [UIFont fontWithName:@"HelveticaNeue" size:12.0];
}

- (void) setCellSelected:(BOOL)selected {
    if(selected)
        [_selectedView setHidden:false];
    else
        [_selectedView setHidden:true];
}

@end
