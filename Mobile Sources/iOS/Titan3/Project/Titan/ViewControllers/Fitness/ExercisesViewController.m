//
//  ExercisesViewController.m
//  Titan
//
//  Created by Marcus Lee on 16/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ExercisesViewController.h"
#import "ExerciseDetailViewController.h"
#import "TrainingPlanDetailViewController.h"
#import "TrainingAddViewController.h"
#import "TrainingPlanAddViewController.h"
#import "SeriesViewController.h"
#import "WorkoutDetailViewController.h"
#import "MenuContainerNavController.h"
#import "OptionsViewController.h"
#import "RNBlurModalView.h"
#import "FitnessCell.h"
#import "ExerciseCell.h"
#import "FitnessPlanCell.h"
#import "SegmentView.h"
#import "Tag.h"
#import "Exercise.h"
#import "Training.h"
#import "TrainingPlan.h"
#import "TrainingDay.h"
#import "TrainingExercise.h"
#import "TrainingDayContent.h"
#import "Muscle.h"
#import "UIImageView+AFNetworking.h"
#import "ExcercisesExecutive.h"

@interface ExercisesViewController () <UITableViewDelegate, UITableViewDataSource, ExcercisesDisplay>

@property (nonatomic, strong) UITableView *fitnessTableView;

@property (nonatomic, strong) NSArray *items;

@property (nonatomic, strong) UISegmentedControl *segmentControl;
@property (nonatomic) NSInteger segmentState;

@property (nonatomic, strong) UILabel *warningLabel;

@property (nonatomic, strong) UIView *segmentControlView;
@property (nonatomic, assign) BOOL isFromPush;

@property (nonatomic, strong) MenuContainerNavController *menuContainerNavController;

@property (nonatomic, strong) ExcercisesExecutive * executive;

@end

@implementation ExercisesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isFromPush = FALSE;
    
    _menuContainerNavController = (MenuContainerNavController *)self.navigationController;
    
    _segmentState = 0;
    
    [self configureView];
    
    _executive = [[ExcercisesExecutive alloc] initWithDisplay:self];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NAV_RIGHT1_ACTION object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_executive loadItemsForTraining:_training
                exerciseSegmentState:_segmentState];
}

- (void)configureView{
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _warningLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:200], [UtilManager height:60])];
    [_warningLabel setNumberOfLines:0];
    [_warningLabel setTextColor:GRAY_REGISTER_FONT];
    [_warningLabel setFont:[UIFont fontWithName:REGULAR_FONT size:18]];
    [_warningLabel setCenter:self.view.center];
    [_warningLabel setTextAlignment:NSTextAlignmentCenter];
    
    [[self view] addSubview:_warningLabel];
    
    _fitnessTableView  = [[UITableView alloc] initWithFrame:CGRectMake(0., NAV_HEIGHT + TAB_BAR_HEIGHT , WIDTH, HEIGHT - NAV_HEIGHT - TAB_BAR_HEIGHT - FOOTER_BAR_HEIGHT) style:UITableViewStylePlain];
    [_fitnessTableView setDelegate:self];
    [_fitnessTableView setDataSource:self];
    [_fitnessTableView setBounces:FALSE];
    [_fitnessTableView setBackgroundColor:[UIColor clearColor]];
    [_fitnessTableView registerClass:[FitnessCell class] forCellReuseIdentifier:@"fitnessCell"];
    [_fitnessTableView registerClass:[FitnessPlanCell class] forCellReuseIdentifier:@"fitnessPlanCell"];
    [_fitnessTableView registerClass:[ExerciseCell class] forCellReuseIdentifier:@"exerciseCell"];
    [_fitnessTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(rightSwipe)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    
    [_fitnessTableView addGestureRecognizer:recognizer];
    
    [self segmentControlTabsView];
    [self.view addSubview:[self headerView]];
    [[self view] addSubview:_fitnessTableView];
    
    self.navigationController.navigationBarHidden = YES;
}

- (void)rightSwipe{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTIF_MENU_SLIDE" object:nil];
}

- (UIView *)headerView{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:64])];
    
    [headerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:24], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    
    [headerView addSubview:backButton];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(backButton.frame),
                                                                    CGRectGetMinY(backButton.frame),
                                                                    WIDTH - 2 * CGRectGetMaxX(backButton.frame),
                                                                    CGRectGetHeight(backButton.frame))];
    
    [titleLabel setText:_training.title];
    [titleLabel setNumberOfLines:2];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    [headerView addSubview:titleLabel];
    
    return headerView;
}

- (void)didPressBackButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)segmentControlTabsView{
    _segmentControlView = [[UIView alloc] initWithFrame:CGRectMake(0., NAV_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    [_segmentControlView setBackgroundColor:[UIColor clearColor]];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    UIView * headerView = [self headerView];
    blurEffectView.frame = CGRectMake(CGRectGetMinX(headerView.frame),
                                      CGRectGetMaxY(headerView.frame),
                                      CGRectGetWidth(headerView.frame),
                                      CGRectGetHeight(headerView.frame));
    
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //[exerciseHeaderView addSubview:blurEffectView];
    
    _segmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Anaeróbicos", nil),NSLocalizedString(@"Aeróbicos", nil)]];
    [_segmentControl setSelectedSegmentIndex:_segmentState];
    [_segmentControl addTarget:self action:@selector(changeExercisesView:) forControlEvents:UIControlEventValueChanged];
    [_segmentControl setCenter:CGPointMake(_segmentControlView.center.x, [UtilManager height:25])];
    [_segmentControl setTintColor:YELLOW_APP_COLOR];
    
    [_segmentControlView addSubview:_segmentControl];
    
    [self.view addSubview:_segmentControlView];
}

- (void)changeExercisesView:(UISegmentedControl *)segmentControl{
    UISegmentedControl *currentSegmentControl = segmentControl;
    _segmentState = currentSegmentControl.selectedSegmentIndex;
    [_executive loadItemsForTraining:_training
                exerciseSegmentState:_segmentState];
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Exercise *exercise = [_items objectAtIndex:indexPath.row];
    
    SeriesViewController *seriesViewController = [[SeriesViewController alloc] init];
    [seriesViewController setExercise:exercise];
    
    [self presentViewController:seriesViewController animated:YES completion:nil];
}


#pragma mark UITableViewDataSource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *exerciseHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:60])];
    [exerciseHeaderView setBackgroundColor:[UIColor clearColor]];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    UIView * headerView = [self headerView];
    blurEffectView.frame = headerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [exerciseHeaderView addSubview:blurEffectView];
    
    _segmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Anaeróbicos", nil),NSLocalizedString(@"Aeróbicos", nil)]];
    [_segmentControl setSelectedSegmentIndex:_segmentState];
    [_segmentControl addTarget:self action:@selector(changeExercisesView:) forControlEvents:UIControlEventValueChanged];
    [_segmentControl setCenter:CGPointMake(exerciseHeaderView.center.x, [UtilManager height:25])];
    [_segmentControl setTintColor:YELLOW_APP_COLOR];
    
    [exerciseHeaderView addSubview:_segmentControl];
    
    return exerciseHeaderView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return exerciseCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return exerciseCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ExerciseCell *cell = [_fitnessTableView dequeueReusableCellWithIdentifier:@"exerciseCell"];
    Exercise *exercise = [_items objectAtIndex:indexPath.row];
    
    [[cell titleLabel] setText:exercise.title];
    NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
    
    if(exercise.tag1 && exercise.tag1.length > 0){
        Tag *tag1 = [Tag tagWithId:exercise.tag1];
        if(tag1)
            [tags addObject:tag1];
    }
    if(exercise.tag2 && exercise.tag2.length > 0){
        Tag *tag2 = [Tag tagWithId:exercise.tag2];
        if(tag2)
            [tags addObject:tag2];
    }
    if(exercise.tag3 && exercise.tag3.length > 0){
        Tag *tag3 = [Tag tagWithId:exercise.tag3];
        if(tag3)
            [tags addObject:tag3];
    }
    
    [[cell tagView] setTags:tags];
    [[cell tagView] reloadTagSubviews];
    
    UIImageView *imageView = cell.exerciseImageView;
    
    NSURLRequest *exerciseImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_EXERCISES,exercise.picture]]
                                                          cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                      timeoutInterval:60];
    
    [cell.exerciseImageView setImageWithURLRequest:exerciseImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-small"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        [imageView setImage:image];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        [[imageView layer] setMasksToBounds:YES];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        NSLog(@"%@",error);
        [imageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
    }];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
    
    return cell;
}

#pragma mark - Exercises Display

- (void)didLoadExercises:(NSArray *)exercises {
    if(exercises.count == 0) {
        [_warningLabel setText:NSLocalizedString(@"Todavía no hay ejercicios de este tipo", nil)];
    }
    else {
        [_warningLabel setText:NSLocalizedString(@"", nil)];
    }
    
    _items = exercises;
    [_fitnessTableView reloadData];
}

@end
