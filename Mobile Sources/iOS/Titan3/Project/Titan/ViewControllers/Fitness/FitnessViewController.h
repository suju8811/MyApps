//
//  FitnessViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "TrainingPlan.h"
#import "FitnessExecutive.h"

@interface FitnessViewController : ParentViewController

@property (nonatomic, assign) FitnessSelectType selectType;
@property (nonatomic, assign) WorkoutSelectType workoutSelectType;
@property (nonatomic, assign) TrainingPlanSelectType trainingPlanSelectType;

@property (nonatomic, strong) TrainingPlan *trainingPlan;
@property (nonatomic, strong) Training *training;

@end
