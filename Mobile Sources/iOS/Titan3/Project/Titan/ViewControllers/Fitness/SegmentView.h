//
//  SegmentView.h
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//


#import <UIKit/UIKit.h>

typedef enum {
    ScrollingItemTypeBackgroundView,
    ScrollingItemTypeCollectionView,
    ScrollingItemTypeNone,
} ScrollingItemType;

typedef enum {
    SegmentViewTypeFitness,
    SegmentViewTypeFood
}SegmentViewType;

@class SegmentView;
@protocol SegmentViewDelegate;

@interface SegmentView : UIView

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) UIImageView *imgSelectedTab;
@property (strong, nonatomic) NSArray *titles;
@property (nonatomic) NSInteger selectedIndex;
@property (assign, nonatomic) id<SegmentViewDelegate> delegate;
@property (nonatomic, assign) SegmentViewType segmentViewType;

+ (SegmentView*) viewWithListTitle:(NSArray*)listTitle;

- (CGFloat) endOfOriginY;

- (void) reloadWithSelectedIndex:(NSInteger)itemIndex;
- (void) reloadSubViewFrameToCurrentPosition;
- (void) scrollingFromRemoteWithValue:(float)scrollingValue;

@end

@protocol SegmentViewDelegate <NSObject>
@optional
- (void) segmentView:(SegmentView*)segmentView didSelectedIndex:(NSInteger)index;
@end
