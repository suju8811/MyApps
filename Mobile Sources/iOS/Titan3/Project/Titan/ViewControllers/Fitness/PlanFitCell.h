//
//  PlanFitCell.h
//  Titan
//
//  Created by Manuel Manzanera on 24/4/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagView.h"
#import "Tag.h"

@interface PlanFitCell : UITableViewCell

@property (nonatomic, strong) UIImageView *circleImageView;
@property (nonatomic, strong) UIView *verticalSeparatorView;
@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *exercisesLabel;
@property (nonatomic, strong) UILabel *daysLabel;

@end
