//
//  FitnessViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "PlanListViewController.h"
#import "ExerciseDetailViewController.h"
#import "TrainingPlanDetailViewController.h"
#import "TrainingAddViewController.h"
#import "TrainingPlanAddViewController.h"
#import "SeriesViewController.h"
#import "WorkoutDetailViewController.h"
#import "MenuContainerNavController.h"
#import "OptionsViewController.h"
#import "RNBlurModalView.h"
#import "WebServiceManager.h"
#import "FitnessCell.h"
#import "ExerciseCell.h"
#import "FitnessPlanCell.h"
#import "SegmentView.h"
#import "Tag.h"
#import "Exercise.h"
#import "Training.h"
#import "TrainingPlan.h"
#import "TrainingDay.h"
#import "TrainingExercise.h"
#import "TrainingDayContent.h"
#import "Muscle.h"
#import "UIImageView+AFNetworking.h"

@interface PlanListViewController () <UITableViewDelegate, UITableViewDataSource, WebServiceManagerDelegate, FitnessPlanCellDelegate, OptionsViewControllerDelegate, FitnessCellDelegate>

@property (nonatomic, strong) UITableView *fitnessTableView;
@property (nonatomic, strong) UIButton *profileButton;
@property (nonatomic, strong) UIButton *favButton;
@property (nonatomic, strong) UIButton *comunityButton;
@property (nonatomic, strong) UIButton *downloadButton;

@property (nonatomic, strong) RLMResults *items;

@property (nonatomic, strong) UISegmentedControl *segmentControl;
@property (nonatomic) NSInteger segmentState;

@property (nonatomic, strong) UILabel *warningLabel;

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIView *segmentControlView;
@property (nonatomic, assign) BOOL isFromPush;

@property (nonatomic, strong) MenuContainerNavController *menuContainerNavController;

@end

@implementation PlanListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isFromPush = FALSE;
    
    _menuContainerNavController = (MenuContainerNavController *)self.navigationController;
    
    _workoutSelectType = WorkoutSelectTypeUser;
    _trainingPlanSelectType = TrainingPlanSelectTypeUser;
    _selectType = FitnessSelectTypePlan;
    _segmentState = 0;
    
    _items = [TrainingPlan getTrainingPlans];
    
    [self configureView];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NAV_RIGHT1_ACTION object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addAction) name:NAV_RIGHT1_ACTION object:nil];
    
    switch (_selectType) {
        case FitnessSelectTypePlan:{
            
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"PLANES DE ENTRENAMIENTO", nil)];
            
            if(_trainingPlanSelectType == TrainingPlanSelectTypeUser)
                _items = [TrainingPlan getTrainingPlans];
            else if(_trainingPlanSelectType == TrainingPlanSelectTypeFavourite)
                _items = [TrainingPlan getFavouritesTrainnigPlans];
            else if(_trainingPlanSelectType == TrainingPlanSelectTypeAdmin)
                _items = [TrainingPlan getTrainingPlansWithOwner:ADMIN_OWNER_ID];
            else if(_trainingPlanSelectType == TrainingPlanSelectTypeShared)
                _items = [TrainingPlan getFavouritesTrainnigPlans];
            
            if(_items.count == 0)
                [_warningLabel setText:NSLocalizedString(@"Todavía no has creado ningún plan", nil)];
            else
                [_warningLabel setText:NSLocalizedString(@"", nil)];
        }
            break;
        case FitnessSelectTyeWorkout:{
            
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"ENTRENAMIENTOS", nil)];
            
            if(_workoutSelectType == WorkoutSelectTypeUser)
                _items = [Training getTrainings];
            else if(_workoutSelectType == WorkoutSelectTypeAdmin)
                _items = [Training getTrainingsWithOwner:ADMIN_OWNER_ID];
            else if(_workoutSelectType == WorkoutSelectTypeFavourite)
                _items = [Training getFavouritesTrainings];
            else
                _items = [Training getTrainings];
            
            if(_items.count == 0)
                [_warningLabel setText:NSLocalizedString(@"Todavía no hay ningún entrenamiento", nil)];
            else
                [_warningLabel setText:NSLocalizedString(@"", nil)];
        }
            break;
        case FitnessSelectTypeExercise:{
            
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"EJERCICIOS", nil)];
            
            if(_segmentState == 0)
                _items = [Exercise getAnaerobicExercises];
            else
                _items = [Exercise getAerobicsExercises];
            if(_items.count == 0)
                [_warningLabel setText:NSLocalizedString(@"Todavía no hay ejercicios de este tipo", nil)];
            else
                [_warningLabel setText:NSLocalizedString(@"", nil)];
            break;
        }
        default:
            break;
    }
    
    [_fitnessTableView reloadData];
    
    if(_trainingPlan)
        [self presentTrainigPlanDetailViewController:_trainingPlan];
    else if (_training)
        [self presentWorkOutViewController:_training];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
    
    [[self view] setBackgroundColor:BLACK_APP_COLOR];
    //[self.view addSubview:[self headerView]];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _warningLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:200], [UtilManager height:60])];
    [_warningLabel setNumberOfLines:0];
    [_warningLabel setTextColor:GRAY_REGISTER_FONT];
    [_warningLabel setFont:[UIFont fontWithName:REGULAR_FONT size:18]];
    [_warningLabel setCenter:self.view.center];
    [_warningLabel setTextAlignment:NSTextAlignmentCenter];
    
    [[self view] addSubview:_warningLabel];
    
    _fitnessTableView  = [[UITableView alloc] initWithFrame:CGRectMake(0., TAB_BAR_HEIGHT + STATUS_BAR_HEIGHT, WIDTH, HEIGHT - TAB_BAR_HEIGHT - FOOTER_BAR_HEIGHT - STATUS_BAR_HEIGHT) style:UITableViewStylePlain];
    [_fitnessTableView setDelegate:self];
    [_fitnessTableView setDataSource:self];
    [_fitnessTableView setBounces:FALSE];
    [_fitnessTableView setBackgroundColor:[UIColor clearColor]];
    [_fitnessTableView registerClass:[FitnessCell class] forCellReuseIdentifier:@"fitnessCell"];
    [_fitnessTableView registerClass:[FitnessPlanCell class] forCellReuseIdentifier:@"fitnessPlanCell"];
    [_fitnessTableView registerClass:[ExerciseCell class] forCellReuseIdentifier:@"exerciseCell"];
    [_fitnessTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(rightSwipe)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    
    [_fitnessTableView addGestureRecognizer:recognizer];
    
    [self segmentControlHeaderView];
    [self addHeaderView];
    [[self view] addSubview:_fitnessTableView];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    [closeButton setTitle:NSLocalizedString(@"Cerrar", nil) forState:UIControlStateNormal];
    [[closeButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:19]];
    [closeButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:closeButton];
}

- (void)dismissView {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)rightSwipe{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTIF_MENU_SLIDE" object:nil];
}

- (void)addHeaderView{
    _headerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20], STATUS_BAR_HEIGHT, WIDTH - [UtilManager width:40], TAB_BAR_HEIGHT)];
    
    _favButton = [[UIButton alloc] initWithFrame:CGRectMake(0., [UtilManager height:5], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [_favButton setImage:[UIImage imageNamed:@"icon-fav-off"] forState:UIControlStateNormal];
    [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateSelected];
    [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateHighlighted];
    [_favButton setCenter:CGPointMake(_headerView.frame.size.width * 3/8, TAB_BAR_HEIGHT/2)];
    [_favButton setTag:1];
    [_favButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:_favButton];
    
    _profileButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
    [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateSelected];
    [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateHighlighted];
    [_profileButton setImage:[UIImage imageNamed:@"icon-profile-off"] forState:UIControlStateNormal];
    [_profileButton setCenter:CGPointMake(_headerView.frame.size.width/8, _favButton.center.y)];
    [_profileButton setTag:0];
    [_profileButton setSelected:YES];
    [_profileButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:_profileButton];
    
    _comunityButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
    [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateSelected];
    [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateHighlighted];
    [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-off"] forState:UIControlStateNormal];
    [_comunityButton setCenter:CGPointMake(5 * _headerView.frame.size.width/8, _favButton.center.y)];
    [_comunityButton setTag:2];
    [_comunityButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:_comunityButton];
    
    _downloadButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
    [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateSelected];
    [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateHighlighted];
    [_downloadButton setImage:[UIImage imageNamed:@"icon-download-off"] forState:UIControlStateNormal];
    [_downloadButton setCenter:CGPointMake(7 * _headerView.frame.size.width/8, _favButton.center.y)];
    [_downloadButton setTag:3];
    [_downloadButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:_downloadButton];
    
    [self.view addSubview:_headerView];
}

- (void)segmentControlHeaderView{
    _segmentControlView = [[UIView alloc] initWithFrame:CGRectMake(0., NAV_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    [_segmentControlView setBackgroundColor:[UIColor clearColor]];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = _headerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //[exerciseHeaderView addSubview:blurEffectView];
    
    _segmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Anaeróbicos", nil),NSLocalizedString(@"Aeróbicos", nil)]];
    [_segmentControl setSelectedSegmentIndex:_segmentState];
    [_segmentControl addTarget:self action:@selector(changeExercisesView:) forControlEvents:UIControlEventValueChanged];
    [_segmentControl setCenter:CGPointMake(_segmentControlView.center.x, [UtilManager height:25])];
    [_segmentControl setTintColor:YELLOW_APP_COLOR];
    
    [_segmentControlView addSubview:_segmentControl];
    
    [_segmentControlView setHidden:YES];
    
    [self.view addSubview:_segmentControlView];
}

- (void)headerAction:(id)sender{
    UIButton *touchButton = (UIButton *)sender;
    
    switch (touchButton.tag) {
        case 0:
            _workoutSelectType = WorkoutSelectTypeUser;
            _trainingPlanSelectType = TrainingPlanSelectTypeUser;
            [_profileButton setSelected:YES];
            [_favButton setSelected:FALSE];
            [_downloadButton setSelected:FALSE];
            [_comunityButton setSelected:FALSE];
            break;
        case 1:
            _workoutSelectType = WorkoutSelectTypeFavourite;
            _trainingPlanSelectType = TrainingPlanSelectTypeFavourite;
            [_profileButton setSelected:FALSE];
            [_favButton setSelected:YES];
            [_downloadButton setSelected:FALSE];
            [_comunityButton setSelected:FALSE];
            break;
        case 2:
            _workoutSelectType = WorkoutSelectTypeAdmin;
            _trainingPlanSelectType = TrainingPlanSelectTypeAdmin;
            [_profileButton setSelected:FALSE];
            [_favButton setSelected:FALSE];
            [_downloadButton setSelected:FALSE];
            [_comunityButton setSelected:TRUE];
            break;
        case 3:
            _workoutSelectType = WorkoutSelectTypeShared;
            _trainingPlanSelectType = TrainingPlanSelectTypeShared;
            [_profileButton setSelected:FALSE];
            [_favButton setSelected:FALSE];
            [_downloadButton setSelected:TRUE];
            [_comunityButton setSelected:FALSE];
            break;
        default:
            break;
    }
    
    if(_selectType == FitnessSelectTyeWorkout){
        if(_workoutSelectType == WorkoutSelectTypeUser)
            _items = [Training getTrainings];
        else if(_workoutSelectType == WorkoutSelectTypeAdmin)
            _items = [Training getTrainingsWithOwner:ADMIN_OWNER_ID];
        else if(_workoutSelectType == WorkoutSelectTypeFavourite)
            _items = [Training getFavouritesTrainings];
        else
            _items = [Training getTrainings];
    }else if (_selectType == FitnessSelectTypePlan){
        if(_trainingPlanSelectType == TrainingPlanSelectTypeUser)
            _items = [TrainingPlan getTrainingPlans];
        else if(_trainingPlanSelectType == TrainingPlanSelectTypeFavourite)
            _items = [TrainingPlan getFavouritesTrainnigPlans];
        else if(_workoutSelectType == TrainingPlanSelectTypeAdmin)
            _items = [TrainingPlan getTrainingPlansWithOwner:ADMIN_OWNER_ID];
        else if(_workoutSelectType == TrainingPlanSelectTypeShared)
            _items = [TrainingPlan getFavouritesTrainnigPlans];
    } else if (_selectType == FitnessSelectTypeExercise){
        
    }
    
    if(_items.count == 0)
        [_warningLabel setText:NSLocalizedString(@"Todavía no hay ningún entrenamiento", nil)];
    else
        [_warningLabel setText:NSLocalizedString(@"", nil)];
    
    [_fitnessTableView reloadData];
}

- (void)addAction{
    switch (_selectType) {
        case FitnessSelectTypePlan:
            [self presentTrainingPlanAddViewController];
            break;
        case FitnessSelectTyeWorkout:
            [self presentTrainingAddViewController];
            break;
            
        case FitnessSelectTypeExercise:
            
            break;
        default:
            break;
    }
}

- (void)changeExercisesView:(UISegmentedControl *)segmentControl{
    UISegmentedControl *currentSegmentControl = segmentControl;
    
    switch (currentSegmentControl.selectedSegmentIndex) {
            case 1:
            _items = [Exercise getAerobicsExercises];
            break;
            case 0:
            _items = [Exercise getAnaerobicExercises];
            break;
        default:
            break;
    }
    
    if(_items.count == 0)
        [_warningLabel setText:NSLocalizedString(@"Todavía no hay ejercicios de este tipo", nil)];
    else
        [_warningLabel setText:NSLocalizedString(@"", nil)];
    _segmentState = currentSegmentControl.selectedSegmentIndex;
    [_fitnessTableView reloadData];
}

- (void)presentTrainingAddViewController{
    TrainingAddViewController *trainingAddViewcontroller = [[TrainingAddViewController alloc] init];
    
    [trainingAddViewcontroller setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [trainingAddViewcontroller setFitnessViewController:self];
    
    [self presentViewController:trainingAddViewcontroller animated:YES completion:^{
        
    }];
    
    //[self.navigationController pushViewController:trainingAddViewcontroller animated:YES];
}

- (void)presentTrainingPlanAddViewController{
    TrainingPlanAddViewController *trainingPlanAddViewController = [[TrainingPlanAddViewController alloc] init];
    [trainingPlanAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [trainingPlanAddViewController setFitnessViewController:self];
    
    [self presentViewController:trainingPlanAddViewController animated:YES completion:^{
    
    }];
    
    //[self.navigationController pushViewController:trainingPlanAddViewController animated:YES];
}

- (void)presentWorkOutViewController:(Training *)training{
    WorkoutDetailViewController *trainingDetailViewController = [[WorkoutDetailViewController alloc] init];
    [trainingDetailViewController setCurrentTraining:training];
    [self.navigationController pushViewController:trainingDetailViewController animated:YES];
    [self setTraining:nil];
}

- (void)presentTrainigPlanDetailViewController:(TrainingPlan *)trainingPlan{
    TrainingPlanDetailViewController *trainingDetailViewController = [[TrainingPlanDetailViewController alloc] init];
    [trainingDetailViewController setTrainingPlan:trainingPlan];
    [self.navigationController pushViewController:trainingDetailViewController animated:TRUE];
    [self setTrainingPlan:nil];
}

#pragma mark OptionViewControllerDelegate Methods

- (void)editTrainingPlan:(TrainingPlan *)trainingPlan{
    TrainingPlanAddViewController *trainingPlanAddViewController = [[TrainingPlanAddViewController alloc] init];
    [trainingPlanAddViewController setCurrentTrainingPlan:trainingPlan];
    [trainingPlanAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [trainingPlanAddViewController setFitnessViewController:self];
    
    [self presentViewController:trainingPlanAddViewController animated:YES completion:^{
        
    }];
}

- (void)editTraining:(Training *)training{
    
    TrainingAddViewController *trainingAddViewcontroller = [[TrainingAddViewController alloc] init];
    [trainingAddViewcontroller setEditTraining:training];
    [trainingAddViewcontroller setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [trainingAddViewcontroller setFitnessViewController:self];
    
    [self presentViewController:trainingAddViewcontroller animated:YES completion:^{
        
    }];
}

#pragma mark FitnessPlanCellDelegate Methods

- (void)optionsDidPushInIndex:(NSInteger)index{
    TrainingPlan *training = [_items objectAtIndex:index];
    
    OptionsViewController *optionsViewController = [[OptionsViewController alloc] init];
    [optionsViewController setCurrentTrainingPlan:training];
    [optionsViewController setOptionType:OptionTypeTrainings];
    [optionsViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [optionsViewController setDelegate:self];
    [self presentViewController:optionsViewController animated:YES completion:^{
    }];
}

#pragma mark FitnessCellDelegate Methods

- (void)optionsTrainingDidPushInIndex:(NSInteger)index{
    Training *training = [_items objectAtIndex:index];
    
    OptionsViewController *optionsViewController = [[OptionsViewController alloc] init];
    [optionsViewController setCurrentTraining:training];
    [optionsViewController setOptionType:OptionTypeWorkout];
    [optionsViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [optionsViewController setDelegate:self];
    [self presentViewController:optionsViewController animated:YES completion:^{
    
    }];
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(_selectType == FitnessSelectTyeWorkout){
        
        Training *training = [_items objectAtIndex:indexPath.row];
        
        //if(training.trainingExercises.count == 0)
        //{
        _isFromPush = YES;
        //}else
        //    [self presentWorkOutViewController:training];
    } else if(_selectType == FitnessSelectTypeExercise){
        Exercise *exercise = [_items objectAtIndex:indexPath.row];
        //ExerciseDetailViewController *exerciseDetailViewController = [[ExerciseDetailViewController alloc] init];
        //[exerciseDetailViewController setExercise:exercise];
        
        SeriesViewController *seriesViewController = [[SeriesViewController alloc] init];
        [seriesViewController setExercise:exercise];
        
        //RNBlurModalView *modalView = [[RNBlurModalView alloc] initWithViewController:self.navigationController view:exerciseDetailViewController.view];
        //[exerciseDetailViewController setModalView:modalView];
        //[modalView setUserInteractionEnabled:TRUE];
        
        //[modalView show];
        
        [self presentViewController:seriesViewController animated:YES completion:^{
        
        }];
        
        //[self.navigationController pushViewController:exerciseDetailViewController animated:YES];
    } else if(_selectType == FitnessSelectTypePlan){
        TrainingPlan *trainingPlan = [_items objectAtIndex:indexPath.row];
        [self presentTrainigPlanDetailViewController:trainingPlan];
    }
}


#pragma mark UITableViewDataSource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    /*if(_selectType == FitnessSelectTypeExercise)
        return [UtilManager height:60];
    else
        return TAB_BAR_HEIGHT;
     */
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    /*if(_selectType == FitnessSelectTypeExercise)
     return [UtilManager height:60];
     else
     return TAB_BAR_HEIGHT;
     */
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(_selectType == FitnessSelectTypeExercise){
        UIView *exerciseHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:60])];
        [exerciseHeaderView setBackgroundColor:[UIColor clearColor]];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = _headerView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [exerciseHeaderView addSubview:blurEffectView];
    
        _segmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Anaeróbicos", nil),NSLocalizedString(@"Aeróbicos", nil)]];
        [_segmentControl setSelectedSegmentIndex:_segmentState];
        [_segmentControl addTarget:self action:@selector(changeExercisesView:) forControlEvents:UIControlEventValueChanged];
        [_segmentControl setCenter:CGPointMake(exerciseHeaderView.center.x, [UtilManager height:25])];
        [_segmentControl setTintColor:YELLOW_APP_COLOR];
    
        [exerciseHeaderView addSubview:_segmentControl];
    
        return exerciseHeaderView;
    
    }else{
        if(!_headerView)
        {
            _headerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:0], 0, WIDTH, TAB_BAR_HEIGHT)];
        
            UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
            UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
            blurEffectView.frame = _headerView.bounds;
            blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            
            //[_headerView addSubview:blurEffectView];
            
            [_headerView setBackgroundColor:[UIColor clearColor]];
            
            _favButton = [[UIButton alloc] initWithFrame:CGRectMake(0., [UtilManager height:5], [UtilManager width:40] * 0.8, [UtilManager width:40]* 0.8)];
            [_favButton setImage:[UIImage imageNamed:@"icon-fav-off"] forState:UIControlStateNormal];
            [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateSelected];
            [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateHighlighted];
            [_favButton setCenter:CGPointMake(_headerView.frame.size.width * 3/8,TAB_BAR_HEIGHT/2)];
            [_favButton setTag:1];
            [_favButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [_headerView addSubview:_favButton];
            
            _profileButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
            [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateSelected];
            [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateHighlighted];
            [_profileButton setImage:[UIImage imageNamed:@"icon-profile-off"] forState:UIControlStateNormal];
            [_profileButton setCenter:CGPointMake(_headerView.frame.size.width/8, _favButton.center.y)];
            [_profileButton setTag:0];
            [_profileButton setSelected:YES];
            [_profileButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [_headerView addSubview:_profileButton];
            
            _comunityButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
            [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateSelected];
            [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateHighlighted];
            [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-off"] forState:UIControlStateNormal];
            [_comunityButton setCenter:CGPointMake(5 * _headerView.frame.size.width/8, _favButton.center.y)];
            [_comunityButton setTag:2];
            [_comunityButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [_headerView addSubview:_comunityButton];
            
            _downloadButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
            [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateSelected];
            [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateHighlighted];
            [_downloadButton setImage:[UIImage imageNamed:@"icon-download-off"] forState:UIControlStateNormal];
            [_downloadButton setCenter:CGPointMake(7 * _headerView.frame.size.width/8, _favButton.center.y)];
            [_downloadButton setTag:3];
            [_downloadButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [_headerView addSubview:_downloadButton];
        }
        
        return _headerView;
        
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_selectType == FitnessSelectTypeExercise)
        return exerciseCellHeight;
    if(_selectType == FitnessSelectTypePlan)
        return trainingPlanCellHeight;
    return planCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_selectType == FitnessSelectTypeExercise)
        return exerciseCellHeight;
    if(_selectType == FitnessSelectTypePlan)
        return trainingPlanCellHeight;
    return planCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(_selectType == FitnessSelectTypePlan){
        FitnessPlanCell *cell = [_fitnessTableView dequeueReusableCellWithIdentifier:@"fitnessPlanCell"];
        TrainingPlan *trainingPlan = [_items objectAtIndex:indexPath.row];
        
        if(trainingPlan.isFavourite){
            NSMutableAttributedString *myString=[[NSMutableAttributedString alloc] initWithString:trainingPlan.title
                                                                                       attributes:@{
                                                                                                    NSForegroundColorAttributeName:YELLOW_APP_COLOR,
                                                                                                    NSFontAttributeName : [UIFont fontWithName:BOLD_FONT size:12]
                                                                                                    }
                                                 ];
            
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image = [UIImage imageNamed:@"icon-fav-on"];
            attachment.bounds = CGRectMake(5., -2., cell.titleLabel.frame.size.height, cell.titleLabel.frame.size.height);
            NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
            [myString appendAttributedString:attachmentString];
            cell.titleLabel.attributedText = myString;
        }else
            [[cell titleLabel] setText:trainingPlan.title];
        [[cell contentLabel] setText:trainingPlan.content];
        if(trainingPlan.trainingDay.count > 0)
            [[cell daysLabel] setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)trainingPlan.trainingDay.count,NSLocalizedString(@"días", nil)]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [[cell exercisesLabel] setText:[NSString stringWithFormat:@"%d %@",[TrainingPlan numberOfTrainings:trainingPlan],NSLocalizedString(@"Entrenamientos", nil)]];
        
        NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:trainingPlan.tags.count];
        
        for(Tag *tag in trainingPlan.tags){
            [tags addObject:tag];
        }
        
        [[cell tagView] setTags:tags];
        [[cell tagView] reloadTagSubviews];
        
        [cell setDelegate:self];
        
        [cell setCurrentIndex:indexPath.row];
        
        return cell;
    }else if(_selectType == FitnessSelectTypeExercise){
    
        ExerciseCell *cell = [_fitnessTableView dequeueReusableCellWithIdentifier:@"exerciseCell"];
        Exercise *exercise = [_items objectAtIndex:indexPath.row];
        
        [[cell titleLabel] setText:exercise.title];
        NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
        
        if(exercise.tag1 && exercise.tag1.length > 0){
            Tag *tag1 = [Tag tagWithId:exercise.tag1];
            if(tag1)
                [tags addObject:tag1];
        }
        if(exercise.tag2 && exercise.tag2.length > 0){
            Tag *tag2 = [Tag tagWithId:exercise.tag2];
            if(tag2)
                [tags addObject:tag2];
        }
        if(exercise.tag3 && exercise.tag3.length > 0){
            Tag *tag3 = [Tag tagWithId:exercise.tag3];
            if(tag3)
                [tags addObject:tag3];
        }
        
        [[cell tagView] setTags:tags];
        [[cell tagView] reloadTagSubviews];
        
        UIImageView *imageView = cell.exerciseImageView;
        
        NSURLRequest *exerciseImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_EXERCISES,exercise.picture]]
                                                          cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                      timeoutInterval:60];
        
        [cell.exerciseImageView setImageWithURLRequest:exerciseImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder-small"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            [imageView setImage:image];
            [imageView setContentMode:UIViewContentModeScaleAspectFill];
            [[imageView layer] setMasksToBounds:YES];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            NSLog(@"%@",error);
            [imageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
        }];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
        
        return cell;
    } else if(_selectType == FitnessSelectTyeWorkout){
        FitnessCell *cell = [_fitnessTableView dequeueReusableCellWithIdentifier:@"fitnessCell"];
        
        [cell setCurrentIndex:indexPath.row];
        [cell setDelegate:self];
        
        Training *training = [_items objectAtIndex:indexPath.row];
        
        if(training.isFavourite.boolValue){
            NSMutableAttributedString *myString=[[NSMutableAttributedString alloc] initWithString:training.title
                                                                                       attributes:@{
                                                                                                    NSForegroundColorAttributeName:YELLOW_APP_COLOR,
                                                                                                    NSFontAttributeName : [UIFont fontWithName:SEMIBOLD_FONT size:15]
                                                                                                    }
                                                 ];
            
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image = [UIImage imageNamed:@"icon-fav-on"];
            attachment.bounds = CGRectMake(5., -2., cell.titleLabel.frame.size.height, cell.titleLabel.frame.size.height);
            NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
            [myString appendAttributedString:attachmentString];
            cell.titleLabel.attributedText = myString;
        }else
            [[cell titleLabel] setText:training.title];
        
        
        
        NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
        
        for(Tag *tag in training.tags){
            [tags addObject:tag];
        }
        
        int secs = 0;
        NSInteger kCal = 0;
        
        for(TrainingExercise *trainingExercise in training.trainingExercises){
            secs = [TrainingExercise getExerciseTime:trainingExercise] + secs;
            
            for(Serie *serie in trainingExercise.series){
                kCal = kCal + [Serie kCalInSerie:serie];
            }
        }
        
        [[cell kCalLabel] setText:[NSString stringWithFormat:@"%ld kCal",kCal]];
        
        int minutes = secs / 60;
        
        if(minutes<60)
            [cell.timeLabel setText:[NSString stringWithFormat:@"%d min",minutes]];
        else{
            int hour = minutes/60;
            minutes = minutes/60;
            
            [cell.timeLabel setText:[NSString stringWithFormat:@"%d h %d min",hour,minutes]];
        }
       
        [[cell tagView] setTags:tags];
        [[cell tagView] reloadTagSubviews]; 
        
        [[cell calendarImageView] setHidden:TRUE];
        [[cell daysLabel] setHidden:TRUE];
        [[cell contentLabel] setText:training.content];
        
        
        
        long count = 0;
        
        if(training.trainingExercises.count >= training.trainingExercisesCount.intValue)
            count = training.trainingExercises.count ;
        else
            count = training.trainingExercisesCount.intValue;
        
        [[cell exercisesLabel] setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)count,NSLocalizedString(@"Ejercicios", nil)]];
        
        return cell;
    }
    
    return nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_selectType == FitnessSelectTyeWorkout || _selectType == FitnessSelectTypePlan)
        return TRUE;
    else
        return FALSE;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;

}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
    
        switch (_selectType) {
            case FitnessSelectTyeWorkout:
            {
                if(_workoutSelectType == WorkoutSelectTypeUser){
                    Training *currentTraining = [_items objectAtIndex:indexPath.row];
                    if([Training canDeleteTraining:currentTraining]) {
                        [Training deleteTraining:currentTraining];
                        [_fitnessTableView reloadData];
                    }else
                        [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"No tienes permisos para eliminar este Entrenamiento", nil)] animated:TRUE completion:^{
                            
                        }];
                }
            }
                break;
            case FitnessSelectTypePlan:
            {
                if(_trainingPlanSelectType == TrainingPlanSelectTypeUser){
                    TrainingPlan *trainingPlan = [_items objectAtIndex:indexPath.row];
                    
                    if([TrainingPlan canDeleteTrainingPlan:trainingPlan]) {                        [TrainingPlan deleteTrainingPlan:trainingPlan];
                        [_fitnessTableView reloadData];
                    } else
                        [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"No tienes permisos para eliminar este Plan de Entrenamiento", nil)] animated:TRUE completion:^{
                        
                        }];
                    
                    
                }
            }
                break;
                
            default:
                break;
        }
        
        /*
        if(_selectType == FitnessSelectTyeWorkout){
            [tableView beginUpdates];
            NSUInteger row = [indexPath row];
            NSMutableArray *itemsAux = [_items mutableCopy];
            
            [itemsAux removeObjectAtIndex:row];
            
            //_items = []
            
            _items =
            
            [_items removeObjectAtIndex:row];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView endUpdates];
        }
         */
    }
}

#pragma mark WebServiceDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{

}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    if(webServiceType == WebServiceTypeGetTraining){
        
        if(_isFromPush){
            NSDictionary *updateTrainingDic = (NSDictionary *)object;
            Training *updateTraining = [Training trainingWithId:[updateTrainingDic valueForKey:@"trainingId"]];
            
            if(!updateTraining)
                updateTraining = [Training trainingWithId:[updateTrainingDic valueForKey:@"_id"]];
        
            [self presentWorkOutViewController:updateTraining];
        }
    }
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
}

@end
