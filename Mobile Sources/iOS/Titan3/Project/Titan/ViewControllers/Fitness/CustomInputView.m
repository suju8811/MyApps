//
//  CustomInputView.m
//  Titan
//
//  Created by Manuel Manzanera on 5/4/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CustomInputView.h"

#import "UtilManager.h"
#import "AppContext.h"
#import "AppConstants.h"

@interface CustomInputView ()<UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) UIButton *acceptButton;

@end

@implementation CustomInputView

- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        [self setBackgroundColor:[UIColor blackColor]];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:5], [UtilManager height:5], frame.size.width - [UtilManager width:10], [UtilManager height:40])];
        [_titleLabel setNumberOfLines:0];
        [_titleLabel setTextColor:[UIColor whiteColor]];
        [_titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_titleLabel setText:@"Esto es una prueba de meter datos"];
        
        [self addSubview:_titleLabel];
        
        _inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y + _titleLabel.frame.size.height, _titleLabel.frame.size.width, UITEXTFIELD_HEIGHT)];
        [_inputTextField setBorderStyle:UITextBorderStyleRoundedRect];
        [_inputTextField setTextColor:YELLOW_APP_COLOR];
        [_inputTextField setTextAlignment:NSTextAlignmentCenter];
        [_inputTextField setBackgroundColor:[UIColor clearColor]];
        [[_inputTextField layer] setBorderWidth:1];
        [[_inputTextField layer] setBorderColor:[UIColor whiteColor].CGColor];
        [_inputTextField setDelegate:self];
        
        [self addSubview:_inputTextField];
        
        UIButton *confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(0., _inputTextField.frame.origin.y + _inputTextField.frame.size.height + [UtilManager height:20],[UtilManager width:110],[UtilManager height:28])];
        [confirmButton setTitle:NSLocalizedString(@"Confirmar", nil) forState:UIControlStateNormal];
        [confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [confirmButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
        [confirmButton setCenter:CGPointMake(self.center.x, confirmButton.center.y)];
        
        [self addSubview:confirmButton];
        
    }
    
    return self;
}

- (void)didPressBackButton {
    
    /*
    [_inputTextField resignFirstResponder];
    if([_delegate respondsToSelector:@selector(confirmWithText:andCustomInputType:)])
        [_delegate confirmWithText:[_inputTextField.text substringToIndex:_inputTextField.text.length - 2] andCustomInputType:_customInputType];
     */
}

- (void)presentPickerView{
   
    //[self hideKeyBoards];
        
    if(_pickerView)
    {
        [_pickerView removeFromSuperview];
        _pickerView = nil;
    }
    
    _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0., self.frame.size.height - 216, self.frame.size.width, 216)];
    [_pickerView setBackgroundColor:[UIColor whiteColor]];
    [_pickerView setUserInteractionEnabled:YES];
    [_pickerView setDelegate:self];
    [_pickerView setDataSource:self];
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_pickerView.frame.size.width - 80., _pickerView.frame.origin.y, 80., 24.)];
    [_acceptButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptPickerAction) forControlEvents:UIControlEventTouchUpInside];
        
    [self addSubview:_pickerView];
    [self addSubview:_acceptButton];
}

- (void)acceptPickerAction{
    NSInteger rowSelected = [_pickerView selectedRowInComponent:0];
    
    switch (_customInputType) {
        case CustomInputTypeWeight:
            _inputTextField.text = [NSString stringWithFormat:@"%ld Kg",rowSelected + 30];
            break;
        case CustomInputTypeIGC:
            _inputTextField.text = [NSString stringWithFormat:@"%ld %%",rowSelected + 3];
            break;
        case CustomInputTypeSquat:
            _inputTextField.text = [NSString stringWithFormat:@"%ld Kg",(long)rowSelected];
            break;
        case CustomInputTypeBenchPress:
            _inputTextField.text = [NSString stringWithFormat:@"%ld Kg",(long)rowSelected];
            break;
        case CustomInputTypeMilitaryPress:
            _inputTextField.text = [NSString stringWithFormat:@"%ld Kg",(long)rowSelected];
            break;
        case CustomInputTypeDeadWeight:
            _inputTextField.text = [NSString stringWithFormat:@"%ld Kg",(long)rowSelected];
            break;
        case CustomInputTypeDominated:
            _inputTextField.text = [NSString stringWithFormat:@"%ld rep.",(long)rowSelected];
            break;
        default:
            _inputTextField.text = [NSString stringWithFormat:@"%ld cm",(long)rowSelected];
            break;
    }
    
    [_pickerView  removeFromSuperview];
    _pickerView = nil;
    
    [_acceptButton removeFromSuperview];
    _acceptButton = nil;
}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    
    [self presentPickerView];
    
    return FALSE;
}

#pragma mark UIPickerDataSource Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (_customInputType) {
        case CustomInputTypeWeight:
            return 470;
            break;
        case CustomInputTypeIGC:
            return 72;
            break;
        default:
            return 500;
            break;
    }
    return 470;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (_customInputType) {
        case CustomInputTypeWeight:
            return [NSString stringWithFormat:@"%ld Kg",row + 30];
            break;
        case CustomInputTypeIGC:
            return [NSString stringWithFormat:@"%ld %%",row + 3];
            break;
        case CustomInputTypeSquat:
            return [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        case CustomInputTypeBenchPress:
            return [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        case CustomInputTypeMilitaryPress:
            return [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        case CustomInputTypeDeadWeight:
            return [NSString stringWithFormat:@"%ld Kg",(long)row];
            break;
        case CustomInputTypeDominated:
            return [NSString stringWithFormat:@"%ld reps",(long)row];
            break;
        default:
            return [NSString stringWithFormat:@"%ld cm",(long)row];
            break;
    }
    return [NSString stringWithFormat:@"%ld Kg",row + 30];
}

@end
