//
//  TrainingAddViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "FitnessViewController.h"
#import "Training.h"
#import "WorkoutDetailViewController.h"

@interface TrainingAddViewController : ParentViewController

@property (nonatomic, strong) Training *selectTraining;
@property (nonatomic, assign) FitnessViewController *fitnessViewController;
@property (nonatomic, strong) Training *editTraining;
@property (nonatomic, strong) id<WorkoutDetailViewControllerDelegate> workoutDetailViewControllerDelegate;

@end
