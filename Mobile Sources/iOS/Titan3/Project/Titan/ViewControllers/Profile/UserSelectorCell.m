//
//  UserSelectorCell.m
//  Titan
//
//  Created by Marcus Lee on 8/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "UserSelectorCell.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation UserSelectorCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self) {
        self.backgroundColor = UIColor.clearColor;
        self.contentView.backgroundColor = UIColor.clearColor;
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                [UtilManager height:5],
                                                                WIDTH - [UtilManager width:10],
                                                                [UtilManager height:25])];
        _titleLabel.adjustsFontSizeToFitWidth = YES;
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.textColor = YELLOW_APP_COLOR;
        _titleLabel.font = [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18];
        [self.contentView addSubview:_titleLabel];
        
        _checkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:25],
                                                                        0,
                                                                        [UtilManager width:18],
                                                                        [UtilManager width:18])];
        [_checkImageView setImage:[UIImage imageNamed:@"img-check"]];
        [_checkImageView setCenter:CGPointMake(_checkImageView.center.x, _titleLabel.center.y)];
        [_checkImageView setHidden:true];
        
        [[self contentView] addSubview:_checkImageView];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:40] - 1, WIDTH - [UtilManager width:20], 1)];
        [_separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        [_separatorView setAlpha:0.2];
        
        [[self contentView] addSubview:_separatorView];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
