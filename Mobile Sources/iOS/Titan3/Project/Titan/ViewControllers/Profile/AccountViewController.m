//
//  AccountViewController.m
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "AccountViewController.h"
#import "AccountExecutive.h"
#import "UserDefaultLibrary.h"
#import "InitViewController.h"
#import "RegisterNavController.h"
#import "AppDelegate.h"

@interface AccountViewController () <UITextFieldDelegate, AccountDisplay>

@property (nonatomic, strong) UITextField * userTextField;
@property (nonatomic, strong) UITextField * emailTextField;
@property (nonatomic, strong) UITextField * passwordTextField;
@property (nonatomic, strong) UIButton *privacityButton;
@property (nonatomic, strong) UILabel *privacityLabel;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) User * user;
@property (nonatomic, strong) AccountExecutive * executive;

@end

@implementation AccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _user = [AppContext sharedInstance].currentUser;
    
    [self configureView];
    
    _executive = [[AccountExecutive alloc] initWithDisplay:self];
}

- (void)configureView {
    self.view.backgroundColor = BLACK_APP_COLOR;
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.,0.,WIDTH , HEIGHT)];
    [_scrollView setBackgroundColor:[UIColor clearColor]];
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50], [UtilManager height:20], [UtilManager width:40], [UtilManager width:40])];
    [okButton setTitle:@"OK" forState:UIControlStateNormal];
    [okButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
    
    [_scrollView addSubview:okButton];
    
    _userTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:20],
                                                                   [UtilManager height:60],
                                                                   WIDTH - [UtilManager width:40],
                                                                   UITEXTFIELD_HEIGHT)];
    [_userTextField setBorderStyle:UITextBorderStyleNone];
    [[_userTextField layer] setMasksToBounds:YES];
    [_userTextField setTextColor:[UIColor whiteColor]];
    [_userTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_userTextField setReturnKeyType:UIReturnKeyDone];
    _userTextField.delegate = self;
    [_userTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_userTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_userTextField setPlaceholder:NSLocalizedString(@"USUARIO", nil)];
    [_userTextField setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    _userTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"USUARIO", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
     ];

    if(_user.name) {
        [_userTextField setText:_user.name];
    }
    
    UIView *leftUserView = [[UIView alloc] initWithFrame:CGRectMake(0.,
                                                                    0.,
                                                                    [UtilManager width:40],
                                                                    _userTextField.frame.size.height)];
    
    [leftUserView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *userImageView = [[UIImageView alloc] initWithFrame:leftUserView.bounds];
    [userImageView setImage:[UIImage imageNamed:@"icon-user"]];
    [userImageView setContentMode:UIViewContentModeCenter];
    
    [leftUserView addSubview:userImageView];
    [_userTextField setLeftView:leftUserView];
    
    [_userTextField setText:_user.name];
    
    [_scrollView addSubview:_userTextField];
    
    UIView *separatorUserView = [[UIView alloc] initWithFrame:CGRectMake(_userTextField.frame.origin.x, _userTextField.frame.origin.y + _userTextField.frame.size.height, _userTextField.frame.size.width, 1)];
    [separatorUserView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [_scrollView addSubview:separatorUserView];
    
    UIButton *changeUserNameButton = [[UIButton alloc] initWithFrame:CGRectMake(0.,
                                                                        CGRectGetMaxY(separatorUserView.frame),
                                                                        WIDTH,
                                                                        UITEXTFIELD_HEIGHT)];
    
    [changeUserNameButton setTitle:NSLocalizedString(@"Nombre de Usuario", nil) forState:UIControlStateNormal];
    [[changeUserNameButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:18]];
    [changeUserNameButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [changeUserNameButton addTarget:self action:@selector(didPressChangeUserNameButton) forControlEvents:UIControlEventTouchUpInside];
    
    [_scrollView addSubview:changeUserNameButton];
    
    _emailTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:20],
                                                                   CGRectGetMaxY(changeUserNameButton.frame) + [UtilManager height:10],
                                                                   WIDTH - [UtilManager width:40],
                                                                   UITEXTFIELD_HEIGHT)];
    [_emailTextField setBorderStyle:UITextBorderStyleNone];
    [[_emailTextField layer] setMasksToBounds:YES];
    [_emailTextField setTextColor:[UIColor whiteColor]];
    [_emailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_emailTextField setReturnKeyType:UIReturnKeyDone];
    _emailTextField.delegate = self;
    [_emailTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_emailTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_emailTextField setPlaceholder:NSLocalizedString(@"EMAIL", nil)];
    [_emailTextField setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    _emailTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"EMAIL", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
     ];

    if(_user.mail) {
        [_emailTextField setText:_user.mail];
    }

    UIView *leftMailView = [[UIView alloc] initWithFrame:CGRectMake(0.,
                                                                    0.,
                                                                    [UtilManager width:40],
                                                                    _emailTextField.frame.size.height)];

    [leftMailView setBackgroundColor:[UIColor clearColor]];

    UIImageView * emailImageView = [[UIImageView alloc] initWithFrame:leftUserView.bounds];
    [emailImageView setImage:[UIImage imageNamed:@"icon-mail"]];
    [emailImageView setContentMode:UIViewContentModeCenter];

    [leftMailView addSubview:emailImageView];
    [_emailTextField setLeftView:leftMailView];

    [_scrollView addSubview:_emailTextField];

    UIView *separatorEmailView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(_emailTextField.frame),
                                                                          CGRectGetMaxY(_emailTextField.frame),
                                                                          CGRectGetWidth(_emailTextField.frame),
                                                                          1)];

    [separatorEmailView setBackgroundColor:GRAY_REGISTER_FONT];

    [_scrollView addSubview:separatorEmailView];

    UIButton *changeEmailButton = [[UIButton alloc] initWithFrame:CGRectMake(0.,
                                                                             CGRectGetMaxY(separatorEmailView.frame),
                                                                             WIDTH,
                                                                             UITEXTFIELD_HEIGHT)];

    [changeEmailButton setTitle:NSLocalizedString(@"Correo de Usuario", nil) forState:UIControlStateNormal];
    [[changeEmailButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:18]];
    [changeEmailButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [changeEmailButton addTarget:self action:@selector(didPressChangeEmailButton) forControlEvents:UIControlEventTouchUpInside];

    [_scrollView addSubview:changeEmailButton];

    _passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:20],
                                                                   CGRectGetMaxY(changeEmailButton.frame) + [UtilManager height:10],
                                                                   WIDTH - [UtilManager width:40],
                                                                   UITEXTFIELD_HEIGHT)];
    [_passwordTextField setBorderStyle:UITextBorderStyleNone];
    [[_passwordTextField layer] setMasksToBounds:YES];
    [_passwordTextField setTextColor:[UIColor whiteColor]];
    [_passwordTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [_passwordTextField setReturnKeyType:UIReturnKeyDone];
    [_passwordTextField setSecureTextEntry:YES];
    _passwordTextField.delegate = self;
    [_passwordTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_passwordTextField setLeftViewMode:UITextFieldViewModeAlways];
    [_passwordTextField setPlaceholder:NSLocalizedString(@"CONTRASEÑA", nil)];
    [_passwordTextField setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    _passwordTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"CONTRASEÑA", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
     ];

    if(_user.password) {
        [_passwordTextField setText:_user.password];
    }

    UIView *leftPasswordView = [[UIView alloc] initWithFrame:CGRectMake(0.,
                                                                    0.,
                                                                    [UtilManager width:40],
                                                                    _passwordTextField.frame.size.height)];

    [leftPasswordView setBackgroundColor:[UIColor clearColor]];

    UIImageView * passwordImageView = [[UIImageView alloc] initWithFrame:leftPasswordView.bounds];
    [passwordImageView setImage:[UIImage imageNamed:@"icon-password"]];
    [passwordImageView setContentMode:UIViewContentModeCenter];

    [leftPasswordView addSubview:passwordImageView];
    [_passwordTextField setLeftView:leftPasswordView];

    [_scrollView addSubview:_passwordTextField];

    UIView *separatorPasswordView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(_passwordTextField.frame),
                                                                             CGRectGetMaxY(_passwordTextField.frame),
                                                                             CGRectGetWidth(_passwordTextField.frame),
                                                                             1)];
    [separatorPasswordView setBackgroundColor:GRAY_REGISTER_FONT];

    [_scrollView addSubview:separatorPasswordView];

    UIButton *changePasswordButton = [[UIButton alloc] initWithFrame:CGRectMake(0.,
                                                                                CGRectGetMaxY(separatorPasswordView.frame),
                                                                                WIDTH,
                                                                                UITEXTFIELD_HEIGHT)];

    [changePasswordButton setTitle:NSLocalizedString(@"Contraseña", nil) forState:UIControlStateNormal];
    [[changePasswordButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:18]];
    [changePasswordButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [changePasswordButton addTarget:self action:@selector(didPressChangePassword) forControlEvents:UIControlEventTouchUpInside];

    [_scrollView addSubview:changePasswordButton];

    UIButton *deleteAccountButton = [[UIButton alloc] initWithFrame:CGRectMake(0.,
                                                                                CGRectGetMaxY(changePasswordButton.frame) + [UtilManager height:20],
                                                                                WIDTH,
                                                                                UITEXTFIELD_HEIGHT)];

    [deleteAccountButton setTitle:NSLocalizedString(@"Eliminar Cuenta", nil) forState:UIControlStateNormal];
    [[deleteAccountButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:18]];
    [deleteAccountButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [deleteAccountButton addTarget:self action:@selector(didPressDeleteAccount) forControlEvents:UIControlEventTouchUpInside];

    [_scrollView addSubview:deleteAccountButton];

    _privacityButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(_passwordTextField.frame),
                                                                  CGRectGetMaxY(deleteAccountButton.frame) + [UtilManager height:10] ,
                                                                  [UtilManager width:40],
                                                                  [UtilManager width:40])];

    [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
    [_privacityButton addTarget:self action:@selector(didPressPrivacyButton) forControlEvents:UIControlEventTouchUpInside];

    [_scrollView addSubview:_privacityButton];

    _privacityLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_privacityButton.frame) + [UtilManager width:10],
                                                                CGRectGetMinY(_privacityButton.frame),
                                                                [UtilManager width:240],
                                                                _privacityButton.frame.size.height)];

    [_privacityLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    [_privacityLabel setTextAlignment:NSTextAlignmentLeft];
    [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    [_privacityLabel setTextColor:[UIColor whiteColor]];

    [_scrollView addSubview:_privacityLabel];

    if (_user.privacy && _user.privacy.boolValue) {
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    }else{
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-closed"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Privado", nil)];
    }
    
    [self.view addSubview:_scrollView];
}

#pragma mark - Actions

- (void)didPressOKButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didPressChangeUserNameButton {
    [_executive changeUserName:_userTextField.text];
}

- (void)didPressChangeEmailButton {
    [_executive changeEmail:_emailTextField.text];
}

- (void)didPressChangePassword {
    [_executive changePassword:_passwordTextField.text];
}

- (void)didPressDeleteAccount {
    [_executive deleteAccount];
}

- (void)didPressPrivacyButton {
    if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]]){
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-closed"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Privado", nil)];
    }else{
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - AccountDisplay

- (void)didChangeUserName:(NSString*)userName {
    [AppContext sharedInstance].currentUser.name = userName;
    [UserDefaultLibrary setWithKey:USER_NAME WithObject:userName];
}

- (void)didChangeEmail:(NSString*)email {
    [AppContext sharedInstance].currentUser.mail = email;
    [UserDefaultLibrary setWithKey:USER_EMAIL WithObject:email];
}

- (void)didChangePassword:(NSString*)password {
    [AppContext sharedInstance].currentUser.password = password;
    [UserDefaultLibrary setWithKey:USER_PASSWORD WithObject:password];
}

- (void)didDeleteAccount {
    [UserDefaultLibrary removeFromKey:IS_USER_LOGIN];
    [UserDefaultLibrary removeFromKey:USER_NAME];
    [UserDefaultLibrary removeFromKey:USER_PASSWORD];
    
    [[AppContext sharedInstance] setCurrentUser:nil];
    
    InitViewController *loginViewController = [[InitViewController alloc] init];
    RegisterNavController *signNavController = [[RegisterNavController alloc] initWithRootViewController:loginViewController];
    
    [UIView transitionFromView:[AppDelegate sharedDelegate].window.rootViewController.view
                        toView:signNavController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished)
     {
         [[[AppDelegate sharedDelegate] window] setRootViewController:signNavController];
     }];
}

@end
