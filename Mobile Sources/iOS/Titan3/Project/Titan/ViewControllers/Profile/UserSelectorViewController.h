//
//  UserSelectorViewController.h
//  Titan
//
//  Created by Marcus Lee on 8/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import <Realm/Realm.h>

@protocol UserSelectorViewControllerDelegate <NSObject>

- (void)didPressOKWithSelectedResults:(NSArray*)selectedResults
                           andContent:(RLMObject*)content;

@end

@interface UserSelectorViewController : ParentViewController

@property (nonatomic, strong) id<UserSelectorViewControllerDelegate> delegate;
@property (nonatomic, strong) RLMObject * rlmObject;

@end
