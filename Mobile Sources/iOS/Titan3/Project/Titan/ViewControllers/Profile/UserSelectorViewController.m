//
//  UserSelectorViewController.m
//  Titan
//
//  Created by Marcus Lee on 8/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "UserSelectorViewController.h"
#import "UserSelectorCell.h"
#import "UserSelectorViewExecutive.h"
#import "UserMeta.h"

@interface UserSelectorViewController () <UITableViewDataSource, UITableViewDelegate, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate, UserSelectorViewDisplay>

@property (nonatomic, strong) UITableView *selectorTableView;
@property (nonatomic, strong) UITableView *selectedTableView;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) UILabel *selectExercisesLabel;
@property (nonatomic, strong) UIButton *actionButton;
@property (nonatomic, strong) UIButton *okButton;

@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;

@property (nonatomic, strong) NSArray * items;
@property (nonatomic, strong) NSMutableArray *selectResults;
@property (nonatomic, strong) UserSelectorViewExecutive * executive;
@property (nonatomic, strong) NSArray *totalItems;

@end

@implementation UserSelectorViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _totalItems = [[NSMutableArray alloc] initWithArray:_searchResults];
    _selectResults = [[NSMutableArray alloc] init];
    
    [self configureView];
    
    _executive = [[UserSelectorViewExecutive alloc] initWithDisplay:self];
    [_executive displayDidLoad];
}

- (void)configureView {
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    backgroundImageView.image = [UIImage imageNamed:@"img-blur-bkg"];
    [self.view addSubview:backgroundImageView];
    
    _selectorTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.,[UtilManager height:20], WIDTH, HEIGHT - [UtilManager height:40]) style:UITableViewStylePlain];
    [_selectorTableView registerClass:[UserSelectorCell class] forCellReuseIdentifier:@"userSelectorCell"];
    _selectorTableView.delegate = self;
    _selectorTableView.dataSource = self;
    _selectorTableView.multipleTouchEnabled = YES;
    _selectorTableView.bounces = NO;
    _selectorTableView.backgroundColor = [UIColor clearColor];
    _selectorTableView.allowsMultipleSelection = YES;
    _selectorTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _selectorTableView.tableHeaderView = _searchController.searchBar;
    
    [self.view addSubview:_selectorTableView];
    
    _selectedTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, HEIGHT/3) style:UITableViewStylePlain];
    [_selectedTableView setDelegate:self];
    [_selectedTableView setDataSource:self];
    [_selectedTableView setMultipleTouchEnabled:YES];
    [_selectedTableView setBounces:NO];
    [_selectedTableView setAllowsMultipleSelection:TRUE];
    [_selectedTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_selectedTableView registerClass:[UserSelectorCell class] forCellReuseIdentifier:@"userSelectorCell"];
    [_selectedTableView setBackgroundColor:BLACK_APP_COLOR];
    
    [self.view addSubview:_selectedTableView];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    
    self.searchController.searchResultsUpdater = self;
    [_searchController setDelegate:self];
    [self.searchController.searchBar sizeToFit];
    self.searchController.searchBar.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    [self.searchController.searchBar setSearchBarStyle:UISearchBarStyleMinimal];
    [self.searchController.searchBar setBarTintColor:BLACK_APP_COLOR];
    [self.searchController.searchBar setKeyboardAppearance:UIKeyboardAppearanceDark];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]
     setTintColor:[UIColor whiteColor]];
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]
     setTintColor:[UIColor whiteColor]];
    [self.searchController.searchBar setTintColor:[UIColor whiteColor]];
    
    for (UIView *subView in self.searchController.searchBar.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                searchBarTextField.textColor = [UIColor whiteColor];
                break;
            }
        }
    }
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:60])];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    UIView *searchContainerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:44], 10, WIDTH - [UtilManager width:88], 40)];
    [searchContainerView addSubview:self.searchController.searchBar];
    [searchContainerView setBackgroundColor:[UIColor clearColor]];
    self.searchController.searchBar.frame = searchContainerView.frame;
    [self.searchController.searchBar setCenter:CGPointMake(searchContainerView.frame.size.width/2,searchContainerView.frame.size.height/2)];
    
    [headerView addSubview:searchContainerView];
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:40], [UtilManager height:20], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [okButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
    [okButton setCenter:CGPointMake(okButton.center.x, searchContainerView.center.y)];
    
    [headerView addSubview:okButton];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:20.], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton setHidden:FALSE];
    [backButton setCenter:CGPointMake(backButton.center.x, searchContainerView.center.y)];
    [headerView addSubview:backButton];
    
    _selectorTableView.tableHeaderView = headerView;
    
    NSMutableArray *scopeButtonTitles = [[NSMutableArray alloc] init];
    [scopeButtonTitles addObject:NSLocalizedString(@"All", @"Search display controller All button.")];
    
    self.definesPresentationContext = YES;
}

- (void)openSelectedView {
    [_selectedTableView reloadData];
    
    [UIView animateWithDuration:0.5 animations:^{
            if(_selectResults.count > 0 && _selectedTableView.frame.origin.y == HEIGHT - TAB_BAR_HEIGHT){
                [_selectedTableView setFrame:CGRectMake(0., HEIGHT -HEIGHT/3, WIDTH, _selectedTableView.frame.size.height)];
                [_actionButton setImage:[UIImage imageNamed:@"btn-down"] forState:UIControlStateNormal];
            }else /*if (_selectedTableView.frame.origin.y == HEIGHT - HEIGHT/3)*/{
                [_selectedTableView setFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, _selectedTableView.frame.size.height)];
                [_actionButton setImage:[UIImage imageNamed:@"btn-up"] forState:UIControlStateNormal];
            }
    } completion:nil];
}

#pragma mark - Actions

- (void)didPressBackButton {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didPressOKButton {
    [_executive didPressOKButton];
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [_searchController.searchBar resignFirstResponder];
    
    if(tableView == _selectorTableView) {
        UserMeta * selectingUserMeta = [_searchResults objectAtIndex:indexPath.row];
        [_selectResults removeAllObjects];
        for (UserMeta * userMeta in _searchResults) {
            userMeta.isSelected = NO;
        }
        
        if(selectingUserMeta.isSelected) {
            selectingUserMeta.isSelected = NO;
            [_selectResults removeObject:selectingUserMeta];
        }
        else {
            selectingUserMeta.isSelected = YES;
            [_selectResults addObject:selectingUserMeta];
        }
        
        [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Users seleccionados", nil)]];
    }
    else {
        UserMeta * userMeta = [_selectResults objectAtIndex:indexPath.row];
        if (userMeta.isSelected) {
            userMeta.isSelected = NO;
            [_selectResults removeObject:userMeta];
        }
        else {
            userMeta.isSelected = YES;
            [_selectResults addObject:userMeta];
        }
        
        [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Users seleccionados", nil)]];
    }
    
    [_selectedTableView reloadData];
    [_selectorTableView reloadData];
}

#pragma mark UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == _selectorTableView) {
        return _searchResults.count;
    }
    else {
        return _selectResults.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return [UtilManager height:40];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [UtilManager height:40];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _selectorTableView) {
        UserSelectorCell * userSelectorCell = [_selectorTableView dequeueReusableCellWithIdentifier:@"userSelectorCell"];
        UserMeta * userMeta = [_searchResults objectAtIndex:indexPath.row];
        userSelectorCell.titleLabel.text = userMeta.userName;
        
        if(userMeta.isSelected) {
            userSelectorCell.checkImageView.hidden = NO;
        }
        else {
            userSelectorCell.checkImageView.hidden = YES;
        }
        
        return userSelectorCell;
    }
    else {
        UserSelectorCell * userSelectorCell = [_selectorTableView dequeueReusableCellWithIdentifier:@"userSelectorCell"];
        UserMeta * userMeta = [_selectResults objectAtIndex:indexPath.row];
        userSelectorCell.titleLabel.text = userMeta.userName;
        
        if (userMeta.isSelected) {
            userSelectorCell.hidden = NO;
        }
        else {
            userSelectorCell.hidden = YES;
        }
        
        return userSelectorCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:50])];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    if(tableView == _selectedTableView){
        [headerView setBackgroundColor:BLACK_APP_COLOR];
        _selectExercisesLabel = [[UILabel alloc] initWithFrame:headerView.bounds];
        [_selectExercisesLabel setTextAlignment:NSTextAlignmentCenter];
        [_selectExercisesLabel setTextColor:YELLOW_APP_COLOR];
        [_selectExercisesLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_selectResults.count,NSLocalizedString(@"Users seleccionados", nil)]];
        [_selectExercisesLabel setUserInteractionEnabled:FALSE];
        
        [headerView addSubview:_selectExercisesLabel];
        
        //UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openSelectedView)];
        //[_selectExercisesLabel addGestureRecognizer:tapGesture];
        
        if(!_actionButton){
            _actionButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:20.], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
            [_actionButton setImage:[UIImage imageNamed:@"btn-up"] forState:UIControlStateNormal];
        }
        
        [_actionButton addTarget:self action:@selector(openSelectedView) forControlEvents:UIControlEventTouchUpInside];
        [_actionButton setCenter:CGPointMake(_actionButton.center.x, headerView.frame.size.height/2)];
        
        _okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:40], [UtilManager height:20], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
        [_okButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
        [_okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
        [_okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
        [_okButton setCenter:CGPointMake(_okButton.center.x, headerView.frame.size.height/2)];
        
        [headerView addSubview:_actionButton];
        
        //[headerView addSubview:_okButton];
        
        UITapGestureRecognizer *openGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openSelectedView)];
        [headerView addGestureRecognizer:openGesture];
        
        UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(openSelectedView)];
        [swipeGesture setDirection:UISwipeGestureRecognizerDirectionUp];
        
        [headerView addGestureRecognizer:swipeGesture];
        
        UISwipeGestureRecognizer *swipeDownGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(openSelectedView)];
        [swipeDownGesture setDirection:UISwipeGestureRecognizerDirectionDown];
        
        [headerView addGestureRecognizer:swipeDownGesture];
        
    }
    else {
        [headerView setBackgroundColor:BLACK_APP_COLOR];
    }
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:50])];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    return footerView;
}

- (void)reloadResults {
    _searchResults = [[NSMutableArray alloc] initWithCapacity:_items.count];
    
    for (UserMeta * userMeta in _items){
        for(UserMeta * selectUserMeta in _selectResults)
        {
            if([selectUserMeta.token isEqualToString:userMeta.token]) {
                userMeta.isSelected = YES;
                break;
            }
        }
        [_searchResults addObject:userMeta];
    }
}
    
#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    // update the filtered array based on the search text
    NSString *searchText = searchController.searchBar.text;
    
    // strip out all the leading and trailing spaces
    NSString *strippedString = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // break up the search terms (separated by spaces)
    NSArray *searchItems = nil;
    if (strippedString.length > 0) {
        searchItems = [strippedString componentsSeparatedByString:@" "];
    }
    
    NSPredicate *finalPredicate;
    for (NSString *searchString in searchItems) {
        finalPredicate = [NSPredicate predicateWithFormat:@"userName CONTAINS[c] %@",searchString];
        
    }
    
    if(finalPredicate) {
        _searchResults = [NSMutableArray arrayWithArray:[_totalItems filteredArrayUsingPredicate:finalPredicate]];
    }
    if(searchText.length == 0) {
        [self reloadResults];
    }
    
    //searchResults = [[searchResults filteredArrayUsingPredicate:finalPredicate] mutableCopy];
    // hand over the filtered results to our search results table
    self.definesPresentationContext = TRUE;
    
    [_selectorTableView reloadData];
}


#pragma mark - UISearchBarDelegate

// Workaround for bug: -updateSearchResultsForSearchController: is not called when scope buttons change
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
    [self updateSearchResultsForSearchController:self.searchController];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}


#pragma mark - Content Filtering


#pragma mark

- (void)willPresentSearchController:(UISearchController *)searchController{
    dispatch_async(dispatch_get_main_queue(), ^{
        //searchController.searchResultsController.view.hidden = NO;
        //[[searchController searchBar] setFrame:CGRectMake(0., 0., searchController.searchBar.frame.size.width, searchController.searchBar.frame.size.height)];
    });
}

- (void)didPresentSearchController:(UISearchController *)searchController{
    searchController.searchResultsController.view.hidden = NO;
}

- (void)willDismissSearchController:(UISearchController *)searchController{
    dispatch_async(dispatch_get_main_queue(), ^{
        //searchController.searchResultsController.view.hidden = YES;
        //[self.searchController.searchBar setFrame:CGRectMake(0., NAV_HEIGHT -  self.searchController.searchBar.frame.size.height, WIDTH, self.searchController.searchBar.frame.size.height)];
    });
}

- (void)didDismissSearchController:(UISearchController *)searchController{
    
}

- (void)presentSearchController:(UISearchController *)searchController{
    
}

#pragma mark - UserSelectorViewDelegate Methods

- (NSArray*)selectedItems {
    return _selectResults;
}

- (void)dismissWithConfirm {
    [self dismissViewControllerAnimated:YES completion:^{
        [_delegate didPressOKWithSelectedResults:_selectResults
                                      andContent:_rlmObject];
    }];
}

- (void)didLoadItems:(NSArray*)items {
    _items = items;
    [self reloadResults];
    [_selectorTableView reloadData];
    [_selectedTableView reloadData];
}

@end
