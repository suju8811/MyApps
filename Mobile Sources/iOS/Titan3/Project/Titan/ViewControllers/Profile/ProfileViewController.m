//
//  ProfileViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ProfileViewController.h"
#import "UploadMediaView.h"
#import "UIImageView+AFNetworking.h"
#import "User.h"
#import "ParserManager.h"
#import "MRProgress.h"
#import "ProfileExecutive.h"

@interface ProfileViewController ()<UIImagePickerControllerDelegate, UploadMediaViewDelegate, UITextFieldDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, ProfileDisplay>

@property (nonatomic, strong) UploadMediaView *uploadMediaView;
@property (nonatomic, strong) UIImage *userImage;
@property (nonatomic, strong) UIImagePickerController *picker;

@property (nonatomic, strong) UIImageView *profileImageView;

@property (nonatomic, strong) UITextField *birthTextField;
@property (nonatomic, strong) UITextField *genderTextField;
@property (nonatomic, strong) UITextField *heigthTextField;
@property (nonatomic, strong) UITextField *weigthTextField;

@property (nonatomic, strong) UIButton *acceptButton;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) NSDate *selectedDate;

@property (nonatomic, strong) UIPickerView *heigthPicker;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) User *user;

@property (nonatomic, strong) UIButton *shareButton;
@property (nonatomic, strong) ProfileExecutive * executive;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _user = [[AppContext sharedInstance] currentUser];
    
    [self configureView];
    
    _executive = [[ProfileExecutive alloc] initWithDisplay:self];
    [_executive displayDidLoad];
}

- (void)configureView {
    self.view.backgroundColor = BLACK_APP_COLOR;
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.,0.,WIDTH , HEIGHT)];
    _scrollView.backgroundColor = UIColor.clearColor;
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50],
                                                                    [UtilManager height:10],
                                                                    [UtilManager width:40],
                                                                    [UtilManager width:40])];
    
    [okButton setTitle:@"OK" forState:UIControlStateNormal];
    [okButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
    
    [_scrollView addSubview:okButton];
    
    _profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:50], [UtilManager height:80], [UtilManager height:90], [UtilManager height:90])];
    _profileImageView.backgroundColor = UIColor.clearColor;
    _profileImageView.image = [UIImage imageNamed:@"img-placeholder-small"];
    _profileImageView.layer.cornerRadius = _profileImageView.frame.size.width / 2.f;
    _profileImageView.layer.masksToBounds = YES;
    _profileImageView.center = CGPointMake(self.view.center.x, _profileImageView.center.y);
    _profileImageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *profileTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentPicView)];
    [_profileImageView addGestureRecognizer:profileTapGesture];
    [_scrollView addSubview:_profileImageView];
    
    //
    // Birth
    //
    
    _birthTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:20],
                                                                    CGRectGetMaxY(_profileImageView.frame) + [UtilManager height:20],
                                                                    WIDTH - [UtilManager width:40],
                                                                    UITEXTFIELD_HEIGHT)];
    
    _birthTextField.borderStyle = UITextBorderStyleNone;
    _birthTextField.textColor = UIColor.whiteColor;
    _birthTextField.layer.masksToBounds = YES;
    _birthTextField.keyboardType = UIKeyboardTypeEmailAddress;
    _birthTextField.returnKeyType = UIReturnKeyDone;
    _birthTextField.secureTextEntry = NO;
    _birthTextField.leftViewMode = UITextFieldViewModeAlways;
    _birthTextField.rightViewMode = UITextFieldViewModeAlways;
    _birthTextField.placeholder = NSLocalizedString(@"FECHA DE NACIMIENTO", nil);
    _birthTextField.font = [UIFont fontWithName:REGULAR_FONT size:16];
    _birthTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"FECHA DE NACIMIENTO", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:18.0]
                                                 }
     ];
    
    UIView *leftBirthView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40], UITEXTFIELD_HEIGHT)];
    leftBirthView.backgroundColor = UIColor.clearColor;
    UIImageView *birthImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40], UITEXTFIELD_HEIGHT)];
    birthImageView.image = [UIImage imageNamed:@"icon-birth"];
    birthImageView.contentMode = UIViewContentModeCenter;
    [leftBirthView addSubview:birthImageView];
    
    _birthTextField.leftView = leftBirthView;
    _birthTextField.delegate = self;
    
    [_scrollView addSubview:_birthTextField];
    
    UIView *separatorBirthView = [[UIView alloc] initWithFrame:CGRectMake(_birthTextField.frame.origin.x, _birthTextField.frame.origin.y + _birthTextField.frame.size.height, _birthTextField.frame.size.width, 1)];
    separatorBirthView.backgroundColor = GRAY_REGISTER_FONT;
    [_scrollView addSubview:separatorBirthView];
    
    //
    // Gender
    //
    
    _genderTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMinX(_birthTextField.frame),
                                                                     CGRectGetMaxY(_birthTextField.frame) + [UtilManager height:10],
                                                                     CGRectGetWidth(_birthTextField.frame),
                                                                     UITEXTFIELD_HEIGHT)];
    
    _genderTextField.borderStyle = UITextBorderStyleNone;
    _genderTextField.textColor = UIColor.whiteColor;
    _genderTextField.layer.masksToBounds = YES;
    _genderTextField.keyboardType = UIKeyboardTypeDefault;
    _genderTextField.returnKeyType = UIReturnKeyDone;
    _genderTextField.secureTextEntry = NO;
    _genderTextField.leftViewMode = UITextFieldViewModeAlways;
    _genderTextField.rightViewMode = UITextFieldViewModeAlways;
    _genderTextField.placeholder = NSLocalizedString(@"GÉNERO", nil);
    _genderTextField.font = [UIFont fontWithName:REGULAR_FONT size:16];
    _genderTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"GÉNERO", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:21.0]
                                                 }
     ];
    
    UIView *leftGenderView = [[UIView alloc] initWithFrame:CGRectMake(0.,
                                                                      0.,
                                                                      [UtilManager width:40],
                                                                      UITEXTFIELD_HEIGHT)];
    
    leftGenderView.backgroundColor = UIColor.clearColor;
    UIImageView *genderImageView = [[UIImageView alloc] initWithFrame:leftBirthView.bounds];
    genderImageView.image = [UIImage imageNamed:@"icon-gender"];
    genderImageView.contentMode = UIViewContentModeCenter;
    [leftGenderView addSubview:genderImageView];
    
    _genderTextField.leftView = leftGenderView;
    _genderTextField.delegate = self;
    [_scrollView addSubview:_genderTextField];
    
    UIView *separatorGenderView = [[UIView alloc] initWithFrame:CGRectMake(_birthTextField.frame.origin.x, _genderTextField.frame.origin.y + _genderTextField.frame.size.height, _birthTextField.frame.size.width, 1)];
    [separatorGenderView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [_scrollView addSubview:separatorGenderView];
    
    //
    // Height
    //
    
    _heigthTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMinX(_birthTextField.frame),
                                                                     CGRectGetMaxY(_genderTextField.frame) + [UtilManager height:10],
                                                                     CGRectGetWidth(_birthTextField.frame),
                                                                     UITEXTFIELD_HEIGHT)];
    
    _heigthTextField.borderStyle = UITextBorderStyleNone;
    _heigthTextField.textColor = UIColor.whiteColor;
    _heigthTextField.layer.masksToBounds = YES;
    _heigthTextField.keyboardType = UIKeyboardTypeEmailAddress;
    _heigthTextField.returnKeyType = UIReturnKeyDone;
    _heigthTextField.secureTextEntry = NO;
    _heigthTextField.leftViewMode = UITextFieldViewModeAlways;
    _heigthTextField.rightViewMode = UITextFieldViewModeAlways;
    _heigthTextField.placeholder = NSLocalizedString(@"ALTURA", nil);
    _heigthTextField.font = [UIFont fontWithName:REGULAR_FONT size:16];
    _heigthTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"ALTURA", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:21.0]
                                                 }
     ];
    
    UIView *leftHeigthView = [[UIView alloc] initWithFrame:CGRectMake(0.,
                                                                      0.,
                                                                      [UtilManager width:40],
                                                                      UITEXTFIELD_HEIGHT)];
    
    leftHeigthView.backgroundColor = UIColor.clearColor;
    UIImageView *heigthImageView = [[UIImageView alloc] initWithFrame:leftBirthView.bounds];
    heigthImageView.image = [UIImage imageNamed:@"icon-height"];
    heigthImageView.contentMode = UIViewContentModeCenter;
    [leftHeigthView addSubview:heigthImageView];
    
    _heigthTextField.leftView = leftHeigthView;
    _heigthTextField.delegate = self;
    [_scrollView addSubview:_heigthTextField];
    
    UIView *separatorHeigthView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(_birthTextField.frame),
                                                                           CGRectGetMaxY(_heigthTextField.frame),
                                                                           CGRectGetWidth(_birthTextField.frame),
                                                                           1)];
    
    separatorHeigthView.backgroundColor = GRAY_REGISTER_FONT;
    [_scrollView addSubview:separatorHeigthView];
    
    //
    // Weight
    //
    
    _weigthTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMinX(_birthTextField.frame),
                                                                     CGRectGetMaxY(_heigthTextField.frame) + [UtilManager height:10],
                                                                     CGRectGetWidth(_birthTextField.frame),
                                                                     UITEXTFIELD_HEIGHT)];
                                                                     
    _weigthTextField.borderStyle = UITextBorderStyleNone;
    _weigthTextField.textColor = UIColor.whiteColor;
    _weigthTextField.layer.masksToBounds = YES;
    _weigthTextField.keyboardType = UIKeyboardTypeDecimalPad;
    _weigthTextField.returnKeyType =  UIReturnKeyDone;
    _weigthTextField.secureTextEntry = NO;
    _weigthTextField.leftViewMode = UITextFieldViewModeAlways;
    _weigthTextField.rightViewMode = UITextFieldViewModeAlways;
    _weigthTextField.placeholder = NSLocalizedString(@"ALTURA", nil);
    _weigthTextField.font = [UIFont fontWithName:REGULAR_FONT size:16];
    _weigthTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"PESO", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName: GRAY_REGISTER_FONT,
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT size:21.0]
                                                 }
     ];
    
    UIView *leftWeigthView = [[UIView alloc] initWithFrame:CGRectMake(0.,
                                                                      0.,
                                                                      [UtilManager width:40],
                                                                      CGRectGetWidth(_birthTextField.frame))];
                                                                      
    leftWeigthView.backgroundColor = UIColor.clearColor;
    UIImageView *weigthImageView = [[UIImageView alloc] initWithFrame:leftBirthView.bounds];
    weigthImageView.image = [UIImage imageNamed:@"icon-weight"];
    weigthImageView.contentMode = UIViewContentModeCenter;
    [leftWeigthView addSubview:weigthImageView];
    
    _weigthTextField.leftView = leftWeigthView;
    _weigthTextField.delegate = self;
    [_scrollView addSubview:_weigthTextField];
    
    UIView *separatorWeigthView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(_birthTextField.frame),
                                                                           CGRectGetMaxY(_weigthTextField.frame),
                                                                           CGRectGetWidth(_birthTextField.frame),
                                                                           1)];
    
    separatorWeigthView.backgroundColor = GRAY_REGISTER_FONT;
    [_scrollView addSubview:separatorWeigthView];
    
    //
    // Submit Button
    //
    
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(0.f,
                                                                        HEIGHT - [UtilManager height:80],
                                                                        WIDTH,
                                                                        [UtilManager height:80])];
    
    [submitButton setTitle:NSLocalizedString(@"GUARDAR", nil) forState:UIControlStateNormal];
    submitButton.titleLabel.font = [UIFont fontWithName:BOLD_FONT size:18];
    [submitButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(didPressSubmitButton) forControlEvents:UIControlEventTouchUpInside];
    submitButton.backgroundColor = BLACK_HEADER_APP_COLOR;
    [_scrollView addSubview:submitButton];
    
    //
    // Add scroll view
    //
    
    [self.view addSubview:_scrollView];
}

- (void)didPressOKButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)presentPicView{
    if(_uploadMediaView) {
        _uploadMediaView = nil;
    }
    
    _uploadMediaView = [[UploadMediaView alloc] initWithFrame:self.view.bounds];
    [_uploadMediaView setDelegate:self];
    [self.view addSubview:_uploadMediaView];
}

- (void)hideKeyBoards {
    [self.view endEditing:YES];
}

- (void)presentDatePickerView {
    [self hideKeyBoards];
    
    if(_datePicker) {
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - 216, self.view.frame.size.width, 216)];
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    [_datePicker setDate:[NSDate dateWithTimeIntervalSinceNow:-60*60*24*365 * 18]];
    [_datePicker setMaximumDate:[NSDate dateWithTimeIntervalSinceNow:-60*60*24*365 * 18]];
    [_datePicker setMinimumDate:[NSDate dateWithTimeIntervalSinceNow:-60*60*24*365 * 75]];
    [_datePicker setBackgroundColor:[UIColor whiteColor]];
    [_datePicker setUserInteractionEnabled:YES];
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_datePicker.frame.size.width - 80., _datePicker.frame.origin.y, 80., 24.)];
    [_acceptButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptDatePickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_datePicker];
    [self.view addSubview:_acceptButton];
}

- (void)presentGenderSelector{
    
    [self hideKeyBoards];
    
    UIAlertController *genderController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"Seleccione un género", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *maleAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Male", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [_genderTextField setText:NSLocalizedString(@"MALE", nil)];
        [_scrollView setContentOffset:CGPointMake(0., 0.) animated:YES];
        
    }];
    
    UIAlertAction *femaleAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Female", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [_genderTextField setText:NSLocalizedString(@"FEMALE", nil)];
        [_scrollView setContentOffset:CGPointMake(0., 0.) animated:YES];
       
    }];
    
    [genderController addAction:maleAction];
    [genderController addAction:femaleAction];
    
    [self presentViewController:genderController animated:YES completion:nil];
}

- (void)acceptDatePickerAction{
    
    [_scrollView setContentOffset:CGPointMake(0., 0.) animated:YES];
    
    if(_acceptButton && _datePicker){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSString *dateString = [dateFormatter stringFromDate:_datePicker.date];
        [_birthTextField setText:dateString];
        
        _selectedDate = _datePicker.date;
        
        [_acceptButton removeFromSuperview];
        _acceptButton = nil;
        
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
}

- (void)presentPickerView{
    
    //[self hideKeyBoards];
    
    if(_heigthPicker)
    {
        [_heigthPicker removeFromSuperview];
        _heigthPicker = nil;
    }
    
    _heigthPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - 216, self.view.frame.size.width, 216)];
    [_heigthPicker setBackgroundColor:[UIColor whiteColor]];
    [_heigthPicker setUserInteractionEnabled:YES];
    [_heigthPicker setDelegate:self];
    [_heigthPicker setDataSource:self];
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_heigthPicker.frame.size.width - 80., _heigthPicker.frame.origin.y, 80., 24.)];
    [_acceptButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptPickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_heigthPicker];
    [self.view addSubview:_acceptButton];
}

- (void)acceptPickerAction{
    
    [_scrollView setContentOffset:CGPointMake(0., 0.) animated:YES];
    
    NSInteger rowSelected = [_heigthPicker selectedRowInComponent:0];
    
    _heigthTextField.text = [NSString stringWithFormat:@"%ld cm",(long)rowSelected + 120];
    
    [_heigthPicker  removeFromSuperview];
    _heigthPicker = nil;
    
    [_acceptButton removeFromSuperview];
    _acceptButton = nil;
}

#pragma mark - Actions

- (void)didPressSubmitButton {
    [_executive didPressSubmitButton];
}

- (NSDictionary *)userDictionary{
    
    NSMutableDictionary *userDictionary = [[NSMutableDictionary alloc] init];
    
    if(_userImage || _profileImageView.image){
        NSData *imageData = UIImageJPEGRepresentation(_profileImageView.image, 1.);
        NSString *encodeString = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        [userDictionary setObject:encodeString forKey:@"picture"];
    }
    
    if(_heigthTextField.text.length > 0){
        float heigth = [[_heigthTextField.text substringToIndex:3] floatValue]/100.;
        [userDictionary setObject:[NSNumber numberWithFloat:heigth] forKey:@"height"];
    }
    
    if(_genderTextField.text.length > 0)
        [userDictionary setObject:_genderTextField.text forKey:@"gender"];
    
    if(_selectedDate){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        [userDictionary setObject:[formatter stringFromDate:_selectedDate] forKey:@"birthDate"];
    }
    
    return userDictionary;
}


#pragma mark UploadViewDelegateMethods

- (void)dismissViewSelected{
    [_uploadMediaView removeFromSuperview];
}

- (void)galleryDidselected{
    [_uploadMediaView removeFromSuperview];
    _picker = [[UIImagePickerController alloc] init];
    [_picker setDelegate:self];
    [_picker setAllowsEditing:YES];
    [_picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:_picker animated:YES completion:^{
        
    }];
}

- (void)cameraDidSelected{
    [_uploadMediaView removeFromSuperview];
    _picker = [[UIImagePickerController alloc] init];
    [_picker setAllowsEditing:YES];
    [_picker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [_picker setDelegate:self];
    
    [self presentViewController:_picker animated:YES completion:^{
        
    }];
}

#pragma mark UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    _userImage = [info objectForKey:UIImagePickerControllerEditedImage];
    [_profileImageView setImage:_userImage];
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark ScrollViewdelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self acceptDatePickerAction];
    
    if([textField isEqual:_birthTextField]){
        [_scrollView setContentOffset:CGPointMake(0., [UtilManager height:92]) animated:YES];
        [self presentDatePickerView];
        return NO;
    }
    
    if([textField isEqual:_genderTextField]){
        [_scrollView setContentOffset:CGPointMake(0., [UtilManager height:92]) animated:YES];
        [self presentGenderSelector];
        return NO;
    }
    
    if([textField isEqual:_heigthTextField]){
        [_scrollView setContentOffset:CGPointMake(0., [UtilManager height:92]) animated:YES];
        [self presentPickerView];
        
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
}

#pragma mark UIPickerDataSource Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 80;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%ld cm",row + 120];
}

#pragma mark - ProfileDisplay Delegate

- (UpdateProfileRequest*)requestFromDisplay {
    return [[UpdateProfileRequest alloc] initWithToken:[AppContext sharedInstance].authToken
                                                 photo:_userImage
                                           dateOfBirth:_selectedDate
                                                gender:[_genderTextField.text isEqualToString:@"MALE"] ? @"1":@"2"
                                                height:[_heigthTextField.text integerValue]
                                                weight:[_weigthTextField.text integerValue]];
}

- (void)didUpdateProfile {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)reloadDisplay {
    UIImageView *imageView = _profileImageView;
    [_profileImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", PATH_IMAGE,[[[AppContext sharedInstance] currentUser] picture]]]] placeholderImage:[UIImage imageNamed:@"img-placeholder-small"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        [imageView setImage:image];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [[imageView layer] setBorderWidth:1];
        [[imageView layer] setBorderColor:YELLOW_APP_COLOR.CGColor];
        [[imageView layer] setCornerRadius:imageView.frame.size.width/2];
        [[imageView layer] setMasksToBounds:YES];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        NSLog(@"%@",error);
        [imageView setImage:[UIImage imageNamed:@"img-placeholder-small"]];
    }];
    
    if (_user.birthDate) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        _birthTextField.text = [dateFormatter stringFromDate:_user.birthDate];
    }
    
    if (_user.gender) {
        _genderTextField.text = [_user.gender isEqualToString:@"1"] ? @"MALE" : @"FEMALE";
    }
    
    if (_user.height) {
        _heigthTextField.text = [NSString stringWithFormat:@"%.f cm",_user.height.floatValue * 100];
    }
    
    if (_user.weight) {
        [_weigthTextField setText:[NSString stringWithFormat:@"%.f Kg",_user.weight.floatValue]];
    }
}

@end
