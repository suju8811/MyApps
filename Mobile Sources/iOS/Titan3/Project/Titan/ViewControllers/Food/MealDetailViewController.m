//
//  MealDetailViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "MealDetailViewController.h"
#import "UIImageView+AFNetworking.h"
#import "SelectorFoodViewController.h"
#import "MBTwitterScroll.h"
#import "FoodDetailView.h"
#import "RNBlurModalView.h"
#import "MealAlimentCell.h"
#import "AlimentCell.h"
#import "Ingredient.h"
#import "Aliment.h"
#import "Tag.h"
#import "TagView.h"
#import "OptionsFoodViewController.h"
#import "MealAddViewController.h"
#import "MealDetailExecutive.h"
#import "UIImage+AFNetworking.h"
#import "InfoPopupViewController.h"
#import "AlimentAddViewController.h"
#import "MealDetailInfoView.h"
#import "UserSelectorViewController.h"
#import "UserMeta.h"
#import "WebServiceManager.h"

@interface MealDetailViewController ()<UITableViewDelegate, UITableViewDataSource, AlimentCellDelegate, MBTwitterScrollDelegate, FoodDetailViewDelegate, SelectorFoodViewDelegate, OptionsFoodViewControllerDelegate, MealOperationViewDelegate, MealDetailDisplay, UserSelectorViewControllerDelegate>

@property (nonatomic, strong) UITableView *mealTableView;
@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) RNBlurModalView *modalView;
@property (nonatomic, strong) MBTwitterScroll *myTableView;
@property (nonatomic, strong) Meal *selectedMeal;
@property (nonatomic, strong) SelectorFoodViewController *selectorFoodViewController;
@property (nonatomic, strong) MealDetailExecutive * executive;
@property (nonatomic, strong) InfoPopupViewController * infoPopupViewController;
@property (nonatomic, strong) UserSelectorViewController * userSelectorViewController;

@end

@implementation MealDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    
    _executive = [[MealDetailExecutive alloc] initWithDisplay:self];
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"CustomView" bundle:nil];
    _infoPopupViewController = [storyboard instantiateInitialViewController];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (_selectorFoodViewController.presentDetailForReplacingFood) {
        _selectorFoodViewController.presentDetailForReplacingFood = NO;
        Aliment * replacingAliment = [_selectorFoodViewController.selectResults firstObject];
        
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            [replacingAliment setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
            _currentMeal.ingredients[_selectedAlimentIndex].aliment = replacingAliment;
            _currentMeal.ingredients[_selectedAlimentIndex].alimentId = replacingAliment.alimentId;
        }];
        
        [realm beginWriteTransaction];
        NSError *error;
        [realm commitWriteTransaction:&error];
        if(error){
            NSLog(@"%@",error.description);
        }
        
        UIImage * mealImage;
        
        if (_currentMeal.image) {
            NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString: _currentMeal.image]];
            if (imageData) {
                mealImage = [UIImage safeImageWithData:imageData];
            }
        }
        
        [_executive updateMeal:_currentMeal withImage:mealImage];
        [_myTableView setCurrentMeal:_currentMeal];
        [_myTableView.tableView reloadData];
        [_myTableView reloadMealValues];
    }
    else if(_selectorFoodViewController.presentMealDetail) {
        
        _selectorFoodViewController.presentMealDetail = NO;
        
        for(Aliment *alimentBack in _selectorFoodViewController.selectResults){
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                [alimentBack setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            
            BOOL updateAliment = TRUE;
            
            for(Ingredient *ingredient in _currentMeal.ingredients){
                
                if([ingredient.aliment.alimentId isEqualToString:alimentBack.alimentId])
                {
                    updateAliment = FALSE;
                    break;
                }
            }
            
            if(updateAliment){
                Ingredient *ingredient = [[Ingredient alloc] init];
                
                [ingredient setIngredientId:[UtilManager uuid]];
                [ingredient setAliment:alimentBack];
                [ingredient setAlimentId:alimentBack.alimentId];
                [ingredient setQuantity:[NSNumber numberWithInt:100]];
                [ingredient setQuantityUnit:alimentBack.measureUnit];
                
                RLMRealm *realm = [AppContext currentRealm];
                
                [realm transactionWithBlock:^{
                    [_currentMeal.ingredients addObject:ingredient];
                    [alimentBack setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
                }];
                
                [realm beginWriteTransaction];
                NSError *error;
                [realm commitWriteTransaction:&error];
                if(error){
                    NSLog(@"%@",error.description);
                }
            }
        }
        
        UIImage * mealImage;
        
        if (_currentMeal.image) {
            NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString: _currentMeal.image]];
            if (imageData) {
                mealImage = [UIImage safeImageWithData:imageData];
            }
        }
        [_executive updateMeal:_currentMeal withImage:mealImage];
        [_myTableView setCurrentMeal:_currentMeal];
        [_myTableView.tableView reloadData];
        [_myTableView reloadMealValues];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView{
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _myTableView = [[MBTwitterScroll alloc] initWithType:MBTableMeal];
     [_myTableView setCurrentMeal:_currentMeal];
    [_myTableView setupView];
    [_myTableView reloadMealValues];
    _myTableView.delegate = self;
    _myTableView.tableView.delegate = self;
    _myTableView.tableView.dataSource = self;
    _myTableView.tableView.bounces = FALSE;
    [_myTableView.tableView registerClass:[AlimentCell class] forCellReuseIdentifier:@"cell"];
    
    UIImageView *imageView;
    
    NSURLRequest *foodImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_MEAL,_currentMeal.image]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
    
    [imageView setImageWithURLRequest:foodImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        [_myTableView updateHeaderImage:image];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        NSLog(@"%@",error);
    }];
    
    [self.view addSubview:_myTableView];
    [self.view addSubview:[self footerView]];
}

- (UIView *)footerView{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0. ,HEIGHT - TAB_BAR_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = footerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [footerView addSubview:blurEffectView];
    
    UIButton *moreButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [moreButton setImage:[UIImage imageNamed:@"btn-add"] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    [moreButton setCenter:CGPointMake(self.view.center.x, footerView.frame.size.height/2)];
    
    [footerView addSubview:moreButton];
    
    return footerView;
}

- (UIView *)headerView{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:250])];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = headerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [headerView addSubview:blurEffectView];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:20], [UtilManager width:40], [UtilManager width:40])];
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    
    [headerView addSubview:backButton];
    
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 50.,[UtilManager height:30.], [UtilManager width:40], [UtilManager width:40])];
    [menuButton setImage:[UIImage imageNamed:@"btn-menu-settings"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setHidden:FALSE];
    [menuButton setImageEdgeInsets:UIEdgeInsetsMake(5., 5., 5., 5.)];
    [menuButton setCenter:CGPointMake(menuButton.center.x, backButton.center.y)];
    [headerView addSubview:menuButton];
    
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(backButton.frame.origin.x + backButton.frame.size.width, backButton.frame.origin.y + backButton.frame.size.height, WIDTH - 2 * (backButton.frame.origin.x + backButton.frame.size.width), [UtilManager height:25])];
    [titleLabel setText:_currentMeal.title];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setFont:[UIFont fontWithName:BOLD_FONT size:20]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    [headerView addSubview:titleLabel];
    
    NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
    
    for(Ingredient *ingredient in _currentMeal.ingredients){
        
        Tag *aliment1Tag = ingredient.aliment.tag1;
        Tag *alimentTag2 = ingredient.aliment.tag2;
        
        if(aliment1Tag && ![tags containsObject:aliment1Tag])
            [tags addObject:aliment1Tag];
        if(alimentTag2 && ![tags containsObject:alimentTag2])
            [tags addObject:alimentTag2];
    }
    
    _tagView = [[TagView alloc] initWithFrame:CGRectMake(0., titleLabel.frame.size.height + titleLabel.frame.origin.y + [UtilManager height:5], WIDTH, TAGVIEW_SMALL_HEIGHT)];
    [_tagView setIsCenter:TRUE];
    [_tagView setMode:TagsControlModeList];
    [_tagView setTags:tags];
    [_tagView reloadTagSubviews];
    [headerView addSubview:_tagView];
    
    UIImageView *kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:20], [UtilManager width:20], [UtilManager width:20])];
    [kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-green"]];
    [kCalImageView setCenter:CGPointMake(self.view.center.x - [UtilManager width:40], kCalImageView.center.y)];
    
    [headerView addSubview:kCalImageView];
    
    UILabel *_kCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(kCalImageView.frame.origin.x + kCalImageView.frame.size.width + 5, _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:20], [UtilManager width:90], titleLabel.frame.size.height)];
    [_kCalLabel setTextColor:GREEN_APP_COLOR];
    [_kCalLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    [_kCalLabel setText:@"3780 kCal"];
    
    [headerView addSubview:_kCalLabel];
    
    UILabel *_proteinLabel = [[UILabel alloc] initWithFrame:CGRectMake(_kCalLabel.frame.origin.x + _kCalLabel.frame.size.width, _kCalLabel.frame.origin.y + _kCalLabel.frame.size.height + [UtilManager height:10], [UtilManager width:60], [UtilManager width:60])];
    [[_proteinLabel layer] setBorderColor:BLUE_PERCENT_CIRCLE.CGColor];
    [[_proteinLabel layer] setBorderWidth:1];
    [[_proteinLabel layer] setCornerRadius:_proteinLabel.frame.size.width/2];
    [_proteinLabel setText:@"p\n26 g"];
    [_proteinLabel setNumberOfLines:2];
    [_proteinLabel setCenter:CGPointMake(WIDTH * 3/10, _proteinLabel.center.y)];
    [_proteinLabel setTextColor:BLUE_PERCENT_CIRCLE];
    [_proteinLabel setTextAlignment:NSTextAlignmentCenter];
    
    [headerView addSubview:_proteinLabel];
    
    UILabel *_carbsLabel = [[UILabel alloc] initWithFrame:CGRectMake(_proteinLabel.frame.origin.x + _proteinLabel.frame.size.width + [UtilManager width:10], 0., _proteinLabel.frame.size.width, _proteinLabel.frame.size.height)];
    [[_carbsLabel layer] setBorderColor:YELLOW_PERCENT_CIRCLE.CGColor];
    [[_carbsLabel layer] setBorderWidth:1];
    [[_carbsLabel layer] setCornerRadius:_proteinLabel.frame.size.width/2];
    [_carbsLabel setText:@"p\n26 g"];
    [_carbsLabel setNumberOfLines:2];
    [_carbsLabel setCenter:CGPointMake(WIDTH/2, _proteinLabel.center.y)];
    [_carbsLabel setTextColor:YELLOW_PERCENT_CIRCLE];
    [_carbsLabel setTextAlignment:NSTextAlignmentCenter];
    
    [headerView addSubview:_carbsLabel];
    
    UILabel *_fatsLabel = [[UILabel alloc] initWithFrame:CGRectMake(_carbsLabel.frame.origin.x + _carbsLabel.frame.size.width + [UtilManager width:10], 0., _proteinLabel.frame.size.width, _proteinLabel.frame.size.height)];
    [[_fatsLabel layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
    [[_fatsLabel layer] setBorderWidth:1];
    [[_fatsLabel layer] setCornerRadius:_proteinLabel.frame.size.width/2];
    [_fatsLabel setText:@"p\n26 g"];
    [_fatsLabel setNumberOfLines:2];
    [_fatsLabel setCenter:CGPointMake(WIDTH * 7 / 10, _proteinLabel.center.y)];
    [_fatsLabel setTextColor:ORANGE_PERCENT_CIRCLE];
    [_fatsLabel setTextAlignment:NSTextAlignmentCenter];
    
    [headerView addSubview:_fatsLabel];
    
    return headerView;
}

- (void)moreAction{
    _selectedMeal = nil;
    [self presentSelectorTableViewController];
}

- (void)presentSelectorTableViewController{
    _selectorFoodViewController = [[SelectorFoodViewController alloc] init];
    [_selectorFoodViewController setDelegate:self];
    [_selectorFoodViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [_selectorFoodViewController setSelectorType:SelectorFoodTypeMeal];
    [_selectorFoodViewController setSelectMeal:_currentMeal];
    [_selectorFoodViewController setPresentMealDetail:TRUE];
    
    [self presentViewController:_selectorFoodViewController animated:YES completion:^{
        
    }];
}

- (void)didPressBackButton {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark SelectorFoodViewController

- (void)returnData:(NSMutableArray *)returnData withType:(SelectorFoodType)selectorType{
    NSLog(@"%@",returnData);
}

#pragma mark FoodDetailViewDelegate Methods

- (void)quantity:(NSNumber *)quantity inIndex:(NSInteger)index{
    
    Ingredient *ingredient = [_currentMeal.ingredients objectAtIndex:index];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm transactionWithBlock:^{
        ingredient.quantity = quantity;
        [_currentMeal.ingredients replaceObjectAtIndex:index withObject:ingredient];
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    
    UIImage * mealImage;
    
    if (_currentMeal.image) {
        NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString: _currentMeal.image]];
        if (imageData) {
            mealImage = [UIImage safeImageWithData:imageData];
        }
    }
    [_executive updateMeal:_currentMeal withImage:mealImage];
    
    [_myTableView.tableView reloadData];
    [_myTableView reloadMealValues];
}

- (void)foodDetailView:(FoodDetailView *)foodDetailView didPressOKButton:(NSNumber *)quantityValue index:(NSInteger)index {
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [_mealTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Ingredient *ingredient = [_currentMeal.ingredients objectAtIndex:indexPath.row];
    
    FoodDetailView *foodDetailView = [[FoodDetailView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:20], WIDTH - [UtilManager width:20], HEIGHT - [UtilManager height:40]) andIngredient:ingredient];
    [foodDetailView setDelegate:self];
    [foodDetailView setIndex:indexPath.row];
    
    _modalView = [[RNBlurModalView alloc] initWithView:foodDetailView];
    [_modalView setUserInteractionEnabled:YES];
    
    //RNBlurModalView *modalView = [[RNBlurModalView alloc] initWithTitle:food.title message:food.content];
    
    [_modalView show];
}

#pragma mark UITableViewDataSource Methods

- (NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _currentMeal.ingredients.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AlimentCell *cell = [_myTableView.tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.delegate = self;
    cell.currentIndex = indexPath.row;
    
    Ingredient *ingredient = [_currentMeal.ingredients objectAtIndex:indexPath.row];
    Aliment *aliment = ingredient.aliment;
    
    if(!aliment){
        aliment = [Aliment getAlimentWithId:ingredient.alimentId];
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            ingredient.aliment = aliment;
        }];
        
        [realm beginWriteTransaction];
        NSError *error;
        [realm commitWriteTransaction:&error];
        if(error){
            NSLog(@"%@",error.description);
        }
    }
    
    [[cell titleLabel] setText:aliment.title];
    
    UIImageView *imageView = cell.foodImageView;
    
    NSURLRequest *foodImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_FOOD,aliment.image]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
    
    [cell.imageView setImageWithURLRequest:foodImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        [imageView setImage:image];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [[imageView layer] setMasksToBounds:YES];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        NSLog(@"%@",error);
        [imageView setImage:[UIImage imageNamed:@"img-placeholder"]];
    }];
    
    NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
    
    Tag * alimentTag;
    if (aliment.tag1) {
        alimentTag = [Tag tagWithTitle:aliment.tag1];
        if (alimentTag) {
            [tags addObject:alimentTag];
        }
    }
    
    if (aliment.tag2) {
        alimentTag = [Tag tagWithTitle:aliment.tag2];
        if (alimentTag) {
            [tags addObject:alimentTag];
        }
    }
    
    if (aliment.group) {
        alimentTag = [Tag tagWithTitle:aliment.group];
        if (alimentTag) {
            [tags addObject:alimentTag];
        }
    }
    
    cell.tagView.tags = tags;
    [cell.tagView reloadTagSubviews];
    
    [[cell weightLabel] setText:[NSString stringWithFormat:@"%d %@",ingredient.quantity.intValue,ingredient.quantityUnit]];
    
    float multiplicator = ingredient.quantity.floatValue / 100;
    
    [[cell kCalLabel] setText:[NSString stringWithFormat:@"%.0f kCals",(long)aliment.calories.intValue * multiplicator]];
    [[[cell proteinCircleView] countLabel] setText:[NSString stringWithFormat:@"%.0f g",(long)aliment.proteins.intValue * multiplicator]];
    [[[cell carbsCircleView] countLabel] setText:[NSString stringWithFormat:@"%.0f g",(long)aliment.carbs.intValue * multiplicator]];
    [[[cell fatsCircleView] countLabel] setText:[NSString stringWithFormat:@"%.0f g",(long)aliment.fats.intValue * multiplicator]];
    
    [cell setDelegate:self];
    [cell setCurrentIndex:indexPath.row];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [self headerView];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, TAB_BAR_HEIGHT)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return alimentCellHeigtht;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return alimentCellHeigtht;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
    //return [UtilManager height:250];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return 0;
    //return [UtilManager height:250];
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return TAB_BAR_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return TAB_BAR_HEIGHT;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView beginUpdates];
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm transactionWithBlock:^{
        [_currentMeal.ingredients removeObjectAtIndex:indexPath.row];
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [tableView endUpdates];
    [tableView reloadData];
    [_myTableView reloadMealValues];
    
    UIImage * mealImage;
    
    if (_currentMeal.image) {
        NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString: _currentMeal.image]];
        if (imageData) {
            mealImage = [UIImage safeImageWithData:imageData];
        }
    }
    [_executive updateMeal:_currentMeal withImage:mealImage];
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm transactionWithBlock:^{
        [_currentMeal.ingredients exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
}

/*
 - (NSIndexPath *)tableView:(UITableView *)tableView
 targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath
 toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
 NSDictionary *section = [data objectAtIndex:sourceIndexPath.section];
 NSUInteger sectionCount = [[section valueForKey:@"content"] count];
 if (sourceIndexPath.section != proposedDestinationIndexPath.section) {
 NSUInteger rowInSourceSection =
 (sourceIndexPath.section > proposedDestinationIndexPath.section) ?
 0 : sectionCount - 1;
 return [NSIndexPath indexPathForRow:rowInSourceSection inSection:sourceIndexPath.section];
 } else if (proposedDestinationIndexPath.row >= sectionCount) {
 return [NSIndexPath indexPathForRow:sectionCount - 1 inSection:sourceIndexPath.section];
 }
 // Allow the proposed destination.
 return proposedDestinationIndexPath;
 }
 */

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}


#pragma mark AlimentCellDelegate

- (void)alimentCellActionInIndex:(NSInteger)index {
    
    _selectedAlimentIndex = index;
    
    Ingredient *ingredient = [_currentMeal.ingredients objectAtIndex:index];
    Aliment * aliment = ingredient.aliment;
    OptionsFoodViewController *optionsViewController = [[OptionsFoodViewController alloc] init];
    optionsViewController.currentAliment = aliment;
    optionsViewController.optionFoodType = OptionFoodTypeAliment;
    optionsViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsViewController.delegate = self;
    optionsViewController.fromDetailOfParent = YES;
    [self presentViewController:optionsViewController animated:YES completion:nil];
}

#pragma mark MBTwitterScrollDelegate Methods

- (void)editButtonDidPress{
    
    if(_myTableView.tableView.editing) {
        [_myTableView.tableView setEditing:NO animated:TRUE];
        UIImage * mealImage;
        
        if (_currentMeal.image) {
            NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString: _currentMeal.image]];
            if (imageData) {
                mealImage = [UIImage safeImageWithData:imageData];
            }
        }
        
        [_executive updateMeal:_currentMeal withImage:mealImage];
    }
    else {
        [_myTableView.tableView setEditing:TRUE animated:TRUE];
    }
}

- (void)didPressShowInfoButton {
    _infoPopupViewController.rlmObject = _currentMeal;
    [self.view addSubview:_infoPopupViewController.view];
}

- (void)didPressShowDetailInfoButton {
    MealDetailInfoView * mealDetailView = [[MealDetailInfoView alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                                               [UtilManager height:20],
                                                                                               WIDTH - [UtilManager width:20],
                                                                                               HEIGHT - [UtilManager height:40])
                                                                         andMeal:_currentMeal
                                                                         isEditable:NO];
    
    _modalView = [[RNBlurModalView alloc] initWithView:mealDetailView];
    [_modalView setUserInteractionEnabled:YES];
    [_modalView show];
}

- (void)didPressUserRateButton {
    NSString * mealId = _currentMeal.serverMealId;
    [WebServiceManager getMealsWithCompletionBlock:^(BOOL isSuccess) {
        _currentMeal = [Meal mealWithServerId:mealId];
        [self presentUserRateView:_currentMeal];
    }];
}

- (void)backButtonDidPress{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)optionButtonDidPress{
    OptionsFoodViewController *optionsViewController = [[OptionsFoodViewController alloc] init];
    [optionsViewController setCurrentMeal:_currentMeal];
    [optionsViewController setOptionFoodType:OptionFoodTypeMeal];
    [optionsViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [optionsViewController setDelegate:self];
    [self presentViewController:optionsViewController animated:YES completion:nil];
}

- (void)recievedMBTwitterScrollEvent {
}


#pragma mark - OptionFoodViewControllerDelegate Methods

- (void)didPressEditFood:(RLMObject *)rlmObject {
    if ([rlmObject isKindOfClass:[Aliment class]]) {
        Aliment * aliment = (Aliment*)rlmObject;
        
        UIStoryboard * alimentAddStoryboard = [UIStoryboard storyboardWithName:@"AlimentAdd" bundle:nil];
        AlimentAddViewController * alimentAddViewController = [alimentAddStoryboard instantiateInitialViewController];
        alimentAddViewController.editAliment = aliment;
        alimentAddViewController.isSupplement = (aliment.isSupplement && aliment.isSupplement.boolValue);
        alimentAddViewController.mealOperationDelegate = self;
        alimentAddViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        
        [self presentViewController:alimentAddViewController animated:YES completion:nil];
    }
    else if ([rlmObject isKindOfClass:[Meal class]]) {
        Meal * meal = (Meal*)rlmObject;
        
        MealAddViewController *mealAddViewController = [[MealAddViewController alloc] init];
        mealAddViewController.editMeal = meal;
        mealAddViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        mealAddViewController.mealOperationViewDelegate = self;
        [self presentViewController:mealAddViewController animated:YES completion:nil];
    }
}

- (void)didDeletedMeal:(Meal*)meal {
    for (UIViewController * viewController in self.navigationController.viewControllers) {
        if ([viewController isKindOfClass:[FoodViewController class]]) {
            [self.navigationController popToViewController:viewController animated:YES];
            return;
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didPressReplaceDiet:(RLMObject *)rlmObject {
    
    _selectorFoodViewController = [[SelectorFoodViewController alloc] init];
    _selectorFoodViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    _selectorFoodViewController.selectorType = SelectorFoodTypeMealSingle;
    _selectorFoodViewController.selectMeal = _currentMeal;
    _selectorFoodViewController.selectedFoodIndex = _selectedAlimentIndex;
    _selectorFoodViewController.delegate = self;
    
    [self presentViewController:_selectorFoodViewController animated:YES completion:nil];
}

- (void)didPressDeleteFood:(RLMObject *)rlmObject {
    if ([rlmObject isKindOfClass:[Aliment class]]) {
        
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            [_currentMeal.ingredients removeObjectAtIndex:_selectedAlimentIndex];
        }];
        
        [realm beginWriteTransaction];
        NSError *error;
        [realm commitWriteTransaction:&error];
        if(error){
            NSLog(@"%@",error.description);
        }
        
        UIImage * mealImage;
        
        if (_currentMeal.image) {
            NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString: _currentMeal.image]];
            if (imageData) {
                mealImage = [UIImage safeImageWithData:imageData];
            }
        }
        
        [_executive updateMeal:_currentMeal withImage:mealImage];
    }
}

- (void)editDiet:(Diet *)diet {
}


- (void)editNutritionPlan:(NutritionPlan *)nutritionPlan {
}

- (void)didPressSendFood:(RLMObject *)rlmObject {
    _userSelectorViewController = [[UserSelectorViewController alloc] init];
    _userSelectorViewController.rlmObject = rlmObject;
    _userSelectorViewController.delegate = self;
    _userSelectorViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:_userSelectorViewController animated:YES completion:nil];
}

- (void)didPressShareFood:(RLMObject *)rlmObject {
    [_executive shareContent:rlmObject];
}

#pragma mark - MealDetailViewControllerDelegate Methods

- (void)didUpdatedMeal:(Meal*)meal {
    [_myTableView setCurrentMeal:_currentMeal];
    [_myTableView.tableView reloadData];
    [_myTableView reloadMealValues];
}

- (void)didUpdateAliment:(Aliment *)aliment {
    [_myTableView setCurrentMeal:_currentMeal];
    [_myTableView.tableView reloadData];
    [_myTableView reloadMealValues];
}

- (void)reloadAllElements {
    [WebServiceManager getAlimentsWithCompletionBlock:^(BOOL isSuccess) {
        [WebServiceManager getMealsWithCompletionBlock:^(BOOL isSuccess) {
            [WebServiceManager getDietsWithWithCompletionBlock:^(BOOL isSuccess) {
                [WebServiceManager getNutrionalPlansWithCompletionBlock:^(BOOL isSuccess) {
                    
                    [_myTableView.tableView reloadData];
                    [_myTableView reloadMealValues];
                }];
            }];
        }];
    }];
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
}

- (void)traitCollectionDidChange:(nullable UITraitCollection *)previousTraitCollection {
}

- (void)preferredContentSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
}

- (CGSize)sizeForChildContentContainer:(nonnull id<UIContentContainer>)container withParentContainerSize:(CGSize)parentSize {
    return CGSizeZero;
}

- (void)systemLayoutFittingSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container {
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
}

- (void)willTransitionToTraitCollection:(nonnull UITraitCollection *)newCollection withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator {
}

- (void)didUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context withAnimationCoordinator:(nonnull UIFocusAnimationCoordinator *)coordinator {
}

- (void)setNeedsFocusUpdate {
}

- (BOOL)shouldUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context {
    return YES;
}

- (void)updateFocusIfNeeded {
}

#pragma mark - UserSelectorViewControllerDelegate Methods

- (void)didPressOKWithSelectedResults:(NSArray*)selectedResults
                           andContent:(RLMObject*)content {
    
    UserMeta * userMeta = [selectedResults firstObject];
    [_executive sendContent:content
                    toToken:userMeta.token];
}

@end
