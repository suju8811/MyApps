//
//  AlimentAddViewController.m
//  Titan
//
//  Created by Marcus Lee on 25/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "AlimentAddViewController.h"
#import "AppConstants.h"
#import "SelectTagView.h"
#import "Tag.h"
#import "AlimentAddExecutive.h"
#import "UploadMediaView.h"
#import "UIImageView+AFNetworking.h"
#import "WebServiceManager.h"
#import "AttributeAddViewController.h"
#import "SecondaryAttributesViewController.h"

#define ATTRIBUTE_ADD_STORY_BOARD_ID @"AttributeAddViewStoryboardID"
#define SECONDARY_ATTRIBUTE_EMBEDDED_SEGUE @"SecondaryAttributeEmbeddedSegue"

@interface AlimentAddViewController () <SelectTagViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, AlimentAddDisplay, UploadMediaViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AttributeAddViewControllerDelegate, SecondaryAttributesViewControllerDelegate>

//
// Outlets
//

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UIView *tagContainerView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tagViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic) IBOutlet UITextField *measureUnitLabel;
@property (weak, nonatomic) IBOutlet UITextField *conversionChainTextField;
@property (weak, nonatomic) IBOutlet UITextField *conversionFactorTextField;
@property (weak, nonatomic) IBOutlet UIView *attributesView;
@property (weak, nonatomic) IBOutlet UILabel *attributesLabel;
@property (weak, nonatomic) IBOutlet UITextField *caloriesTextField;
@property (weak, nonatomic) IBOutlet UITextField *proteinsTextField;
@property (weak, nonatomic) IBOutlet UITextField *carbohydratorsTextField;
@property (weak, nonatomic) IBOutlet UITextField *fiberTextField;
@property (weak, nonatomic) IBOutlet UITextField *fatTextField;
@property (weak, nonatomic) IBOutlet UITextField *saturatedFatTextField;
@property (weak, nonatomic) IBOutlet UITextField *monounsaturatedFatTextField;
@property (weak, nonatomic) IBOutlet UITextField *polyunsaturatedFatTextField;
@property (weak, nonatomic) IBOutlet UITextField *agpAgsTextField;
@property (weak, nonatomic) IBOutlet UITextField *agpAgsAgmTextField;
@property (weak, nonatomic) IBOutlet UITextField *cholesterolTextField;
@property (weak, nonatomic) IBOutlet UITextField *alcoholTextField;
@property (weak, nonatomic) IBOutlet UITextField *waterTextField;
@property (weak, nonatomic) IBOutlet UITextField *sodiumTextField;
@property (weak, nonatomic) IBOutlet UITextField *sugarTextField;
@property (weak, nonatomic) IBOutlet UITextField *calciumTextField;
@property (weak, nonatomic) IBOutlet UITextField *ironTextField;
@property (weak, nonatomic) IBOutlet UITextField *iodoTextField;
@property (weak, nonatomic) IBOutlet UITextField *magnesiumTextField;
@property (weak, nonatomic) IBOutlet UITextField *zincTextField;
@property (weak, nonatomic) IBOutlet UITextField *seleniumTextField;
@property (weak, nonatomic) IBOutlet UITextField *potassiumTextField;
@property (weak, nonatomic) IBOutlet UITextField *phosphorusTextField;
@property (weak, nonatomic) IBOutlet UITextField *vitaminB1TextField;
@property (weak, nonatomic) IBOutlet UITextField *vitaminB2TextField;
@property (weak, nonatomic) IBOutlet UILabel *niacinTextField;
@property (weak, nonatomic) IBOutlet UITextField *vitaminB6TextField;
@property (weak, nonatomic) IBOutlet UITextField *folicAcidTextField;
@property (weak, nonatomic) IBOutlet UITextField *vitaminB12TextField;
@property (weak, nonatomic) IBOutlet UITextField *vitaminCTextField;
@property (weak, nonatomic) IBOutlet UITextField *retinolTextField;
@property (weak, nonatomic) IBOutlet UITextField *betaCaroteneTextField;
@property (weak, nonatomic) IBOutlet UITextField *vitaminATextField;
@property (weak, nonatomic) IBOutlet UILabel *vitaminDTextField;
@property (weak, nonatomic) IBOutlet UITextField *indexGlycemicTextField;
@property (weak, nonatomic) IBOutlet UITextField *bccasTextField;
@property (weak, nonatomic) IBOutlet UITextField *glutamineTextField;
@property (weak, nonatomic) IBOutlet UITextField *omega3TextField;
@property (weak, nonatomic) IBOutlet UITextField *omega6TextField;
@property (weak, nonatomic) IBOutlet UITextField *labellLabel;
@property (weak, nonatomic) IBOutlet UITextField *label2Label;
@property (weak, nonatomic) IBOutlet UITextField *label3TextField;
@property (weak, nonatomic) IBOutlet UIImageView *uploadImageView;
@property (weak, nonatomic) IBOutlet UIButton *privacyButton;
@property (weak, nonatomic) IBOutlet UILabel *privacyLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottomConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *secondaryAtrributeHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomToAttributeFromContentView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomToAttributeFromSecondary;

//
// Controls
//

@property (nonatomic, strong) UIView *containerPickerView;
@property (nonatomic, strong) UIPickerView *measureUnitPickerView;
@property (nonatomic, strong) UIPickerView *label1PickerView;
@property (nonatomic, strong) UIPickerView *label2PickerView;
@property (nonatomic, strong) SelectTagView *selectTagView;
@property (nonatomic, strong) UploadMediaView *uploadMediaView;
@property (nonatomic, strong) UIImagePickerController *picker;

//
// Variables
//

@property (nonatomic, strong) AlimentAddExecutive *executive;
@property (nonatomic, strong) NSMutableArray *tags;
@property (nonatomic, strong) NSMutableArray *selectedTags;
@property (nonatomic, assign) NSInteger selectedMeasureUnitIndex;
@property (nonatomic, assign) NSInteger selectedLabel1Index;
@property (nonatomic, assign) NSInteger selectedLabel2Index;
@property (nonatomic, strong) UIImage *uploadImage;
@property (nonatomic, strong) SecondaryAttributesViewController * secondaryAttributesViewController;
@property (nonatomic, strong) AttributeAddViewController * attributeAddViewController;

@end

@implementation AlimentAddViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeAction) name:NAV_RIGHT1_ACTION object:nil];
    
    [self configureView];

    _selectedMeasureUnitIndex = 0;
    _selectedLabel1Index = 0;
    _selectedLabel2Index = 0;
    
    _executive = [[AlimentAddExecutive alloc] initWithDisplay:self];
    [_executive displayDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didResizeKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didResizeKeyboard:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    if (!_isSupplement) {
        _bottomToAttributeFromContentView.active = YES;
        _bottomToAttributeFromSecondary.active = NO;
    }
    else {
        _bottomToAttributeFromContentView.active = NO;
        _bottomToAttributeFromSecondary.active = YES;
    }
}

- (void)configureView {
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissView)];
    [swipeGesture setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:swipeGesture];
    
    [_nameTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    _nameTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Nombre", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName:[UIColor grayColor],
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]
                                                 }
     ];
    
    _tags = [Tag getTagsWithType:@"type_1" andCollection:@"Meals"];
    
    _selectTagView = [[SelectTagView alloc] initWithFrame:CGRectMake(0, 0, WIDTH - [UtilManager width:20], [UtilManager height:30]) andTags:_tags];
    
    if(_createAliment)
    {
        _selectedTags = [[NSMutableArray alloc] init];
        if (_createAliment.tag1) {
            [_selectedTags addObject:_createAliment.tag1];
        }
        
        if (_createAliment.tag2) {
            [_selectedTags addObject:_createAliment.tag2];
        }
        
        [_selectTagView setSelectedTags:_selectedTags];
    }
    [_selectTagView setDelegate:self];
    [_tagContainerView addSubview:_selectTagView];
    
    [_contentTextView setKeyboardAppearance:UIKeyboardAppearanceDark];
    [[_contentTextView layer] setBorderWidth:1];
    [[_contentTextView layer] setBorderColor:BLACK_HEADER_APP_COLOR.CGColor];
    [_contentTextView setText:NSLocalizedString(@"Descripción", nil)];

    UIToolbar *toolbarWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetView)];
    [barButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarWeigth setItems:[NSArray arrayWithObjects:flexibleWeigthSpace, barButtonItem, nil]];
    _contentTextView.inputAccessoryView = toolbarWeigth;
    
    _attributesView.layer.borderWidth = 1;
    _attributesView.layer.borderColor = BLACK_HEADER_APP_COLOR.CGColor;
    _attributesLabel.layer.borderWidth = 1;
    _attributesLabel.layer.borderColor = BLACK_HEADER_APP_COLOR.CGColor;
    
    _uploadImageView.layer.borderWidth = 1;
    _uploadImageView.layer.borderColor = YELLOW_APP_COLOR.CGColor;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentImagePickView)];
    [_uploadImageView addGestureRecognizer:tapGesture];
    
    _privacyLabel.hidden = YES;
    _privacyButton.hidden = YES;
}

#pragma mark - UIStoryboard Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:SECONDARY_ATTRIBUTE_EMBEDDED_SEGUE]) {
        
        _secondaryAttributesViewController = (SecondaryAttributesViewController*)segue.destinationViewController;
        _secondaryAttributesViewController.delegate = self;
        _secondaryAttributesViewController.items = [[NSMutableArray alloc] init];
    }
}

#pragma mark - Actions

- (IBAction)didPressOKButton:(id)sender {
    _uploadImage = _uploadImageView.image;
    if (_editAliment) {
        [_executive editAlimentWithImage:_uploadImage];
    }
    else {
        [_executive createAlimentWithImage:_uploadImage];
    }
}

- (IBAction)didPressBackButton:(id)sender {
    [self dismissView];
}

- (void)dismissView {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)hidePickerView {
    if (_containerPickerView) {
        [_containerPickerView removeFromSuperview];
        _containerPickerView = nil;
    }
    
    if (_measureUnitPickerView) {
        [_measureUnitPickerView  removeFromSuperview];
        _measureUnitPickerView = nil;
    }
}

- (IBAction)didPressMeasureUnit:(id)sender {
    [self presentPickerView:AlimentPickerTypeMeasureUnit];
}

- (IBAction)didPressLabel1:(id)sender {
    [self presentPickerView:AlimentPickerTypeLabel1];
}

- (IBAction)didPressLabel2:(id)sender {
    [self presentPickerView:AlimentPickerTypeLabel2];
}

- (IBAction)didPressAddAttribute:(id)sender {
    UIStoryboard * alimentStoryboard = [UIStoryboard storyboardWithName:@"AlimentAdd" bundle:nil];
    _attributeAddViewController = [alimentStoryboard instantiateViewControllerWithIdentifier:ATTRIBUTE_ADD_STORY_BOARD_ID];
    
    [_attributeAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    _attributeAddViewController.delegate = self;
    [self presentViewController:_attributeAddViewController animated:YES completion:nil];
    
}

- (void)presentPickerView:(AlimentPickerType)pickerType {

    [self.view endEditing:YES];
    
    if(_containerPickerView) {
        [self hidePickerView];
    }
    
    _containerPickerView = [[UIView alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - [UtilManager height:206], WIDTH, [UtilManager height:206])];
    [_containerPickerView setBackgroundColor:[UIColor clearColor]];
    
    UIPickerView * pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0., [UtilManager height:44], self.view.frame.size.width, [UtilManager height:162])];
    [pickerView setBackgroundColor:BLACK_APP_COLOR];
    pickerView.tag = pickerType;
    
    [pickerView setUserInteractionEnabled:YES];
    [pickerView setDelegate:self];
    [pickerView setDataSource:self];
    pickerView.showsSelectionIndicator = YES;
    
    UIView *toolBarDescView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:44])];
    [toolBarDescView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIButton *doneDescButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:100],0. , [UtilManager width:90], [UtilManager height:35])];
    [doneDescButton addTarget:self action:@selector(acceptPickerAction:) forControlEvents:UIControlEventTouchUpInside];
    [doneDescButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [doneDescButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    doneDescButton.tag = pickerType;
    [toolBarDescView addSubview:doneDescButton];
    
    switch (pickerType) {
        case AlimentPickerTypeMeasureUnit:
            _measureUnitPickerView = pickerView;
            [_measureUnitPickerView selectRow:_selectedMeasureUnitIndex inComponent:0 animated:NO];
            [_containerPickerView addSubview:_measureUnitPickerView];
            break;
        case AlimentPickerTypeLabel1:
            _label1PickerView = pickerView;
            [_label1PickerView selectRow:_selectedLabel1Index inComponent:0 animated:NO];
            [_containerPickerView addSubview:_label1PickerView];
            break;
        case AlimentPickerTypeLabel2:
            _label2PickerView = pickerView;
            [_label2PickerView selectRow:_selectedLabel2Index inComponent:0 animated:NO];
            [_containerPickerView addSubview:_label2PickerView];
            break;
            
        default:
            break;
    }
    
    [_containerPickerView addSubview:toolBarDescView];
    
    [self.view addSubview:_containerPickerView];
}

- (void)acceptPickerAction:(UIButton*)sender {
    switch (sender.tag) {
        case AlimentPickerTypeMeasureUnit:
            _measureUnitLabel.text = [_executive pickerListForTag:sender.tag][_selectedMeasureUnitIndex];
            break;
            
        case AlimentPickerTypeLabel1:
            _labellLabel.text = [_executive pickerListForTag:sender.tag][_selectedLabel1Index];
            break;
            
        case AlimentPickerTypeLabel2:
            _label2Label.text = [_executive pickerListForTag:sender.tag][_selectedLabel2Index];
            break;
            
        default:
            break;
    }
    
    [self hidePickerView];
}

- (void)closeAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didPressPrivacyButton:(id)sender {
    if([[_privacyButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]]){
        [_privacyButton setImage:[UIImage imageNamed:@"img-lock-closed"] forState:UIControlStateNormal];
        [_privacyLabel setText:NSLocalizedString(@"Perfil Privado", nil)];
    }else{
        [_privacyButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
        [_privacyLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    }
}

#pragma mark - UITextFieldDelegat Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self hidePickerView];
    return YES;
}

#pragma mark UITextViewDelegate Methods

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
    return true;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [self hidePickerView];
    
    if([[textView text] isEqualToString:NSLocalizedString(@"Descripción", nil)]) {
        [_contentTextView setText:@""];
    }
    
    return  true;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return TRUE;
}

#pragma mark - Delegate For Text Field and Text View

- (void)resetView {
    [_contentTextView resignFirstResponder];
}

#pragma mark - SelectTabViewDelegate

- (void)actionDidPushReturnTags:(NSMutableArray *)selectedTags {
}

#pragma mark - UIPickerView Delegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    switch (pickerView.tag) {
        case AlimentPickerTypeMeasureUnit:
            _selectedMeasureUnitIndex = row;
            break;
        case AlimentPickerTypeLabel1:
            _selectedLabel1Index = row;
            break;
        case AlimentPickerTypeLabel2:
            _selectedLabel2Index = row;
            break;
        default:
            break;
    }
}

#pragma mark - UIPickerDataSource Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_executive pickerListForTag:pickerView.tag].count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    title = [_executive pickerListForTag:pickerView.tag][row];
    
    NSAttributedString *attString =
    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor lightTextColor],NSParagraphStyleAttributeName:paragraphStyle}];
    
    return attString;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [_executive pickerListForTag:pickerView.tag][row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return [UtilManager width:80];
}

- (void)presentImagePickView{
    if(_uploadMediaView)
        _uploadMediaView = nil;
    _uploadMediaView = [[UploadMediaView alloc] initWithFrame:self.view.bounds];
    [_uploadMediaView setDelegate:self];
    //[_uploadMediaView setAccessibilityLabel:tapGesture.accessibilityLabel];
    [self.view addSubview:_uploadMediaView];
}

#pragma mark - UploadMediaViewDelegate

- (void)galleryDidselected {
    _picker = [[UIImagePickerController alloc] init];
    [_picker setAccessibilityLabel:_uploadMediaView.accessibilityLabel];
    [_uploadMediaView removeFromSuperview];
    [_picker setDelegate:self];
    [_picker setAllowsEditing:YES];
    [_picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:_picker animated:YES completion:nil];
}

- (void)cameraDidSelected {
    _picker = [[UIImagePickerController alloc] init];
    [_picker setAccessibilityLabel:_uploadMediaView.accessibilityLabel];
    [_uploadMediaView removeFromSuperview];
    [_picker setAllowsEditing:YES];
    [_picker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [_picker setDelegate:self];
    
    [self presentViewController:_picker animated:YES completion:nil];
}

- (void)dismissViewSelected {
    [_uploadMediaView removeFromSuperview];
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    _uploadImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    [_uploadImageView setImage:_uploadImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Keyboard Notification Observer

- (void)didResizeKeyboard:(NSNotification*)aNotification {
    BOOL up = aNotification.name == UIKeyboardWillShowNotification;
    
    NSDictionary * userInfo = [aNotification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationOptions animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    if (up) {
        _scrollViewBottomConstraint.constant = CGRectGetHeight(keyboardEndFrame);
        [UIView animateWithDuration:animationDuration animations:^{
        } completion:^(BOOL finished) {
        }];
    }
    else {
        _scrollViewBottomConstraint.constant = 0;
        [UIView animateWithDuration:animationDuration animations:^{
        } completion:^(BOOL finished) {
        }];
    }
}

#pragma mark - AlimentAddDisplay Delegate

- (void)loadDisplay {
    if (_editAliment) {
        if (_isSupplement) {
            [_titleLabel setText:NSLocalizedString(@"EDITAR SUPLEMENTOS", nil)];
        }
        else {
            [_titleLabel setText:NSLocalizedString(@"EDITAR ALIMENTOS", nil)];
        }
    }
    else {
        if (_isSupplement) {
            [_titleLabel setText:NSLocalizedString(@"NUEVA SUPLEMENTOS", nil)];
        }
        else {
            [_titleLabel setText:NSLocalizedString(@"EDITAR ALIMENTOS", nil)];
        }
    }
    
    if(_editAliment) {
        _nameTextField.text = _editAliment.title;
        _contentTextView.text = _editAliment.content;
        
        NSArray * measureUnitArray = [_executive pickerListForTag:AlimentPickerTypeMeasureUnit];
        _selectedMeasureUnitIndex = [measureUnitArray indexOfObject:_editAliment.measureUnit];
        if (_selectedMeasureUnitIndex == NSNotFound) {
            _selectedMeasureUnitIndex = 0;
        }
        else {
            _measureUnitLabel.text = measureUnitArray[_selectedMeasureUnitIndex];
        }
        
        _conversionChainTextField.text = _editAliment.conversionString;
        _conversionFactorTextField.text = [_editAliment.conversionFactor stringValue];
        _caloriesTextField.text = [_editAliment.calories stringValue];
        _proteinsTextField.text = [_editAliment.proteins stringValue];
        _carbohydratorsTextField.text = [_editAliment.carbs stringValue];
        _fiberTextField.text = [_editAliment.fiber stringValue];
        _fatTextField.text = [_editAliment.fats stringValue];
        _saturatedFatTextField.text = [_editAliment.saturedFats stringValue];
        _monounsaturatedFatTextField.text = [_editAliment.monoUnsaturatedFats stringValue];
        _polyunsaturatedFatTextField.text = [_editAliment.polyUnsaturatedFats stringValue];
        _agpAgsTextField.text = [_editAliment.agpAgs stringValue];
        _agpAgsAgmTextField.text = [_editAliment.agpAgsAgm stringValue];
        _cholesterolTextField.text = [_editAliment.cholesterol stringValue];
        _alcoholTextField.text = [_editAliment.alcohol stringValue];
        _waterTextField.text = [_editAliment.water stringValue];
        _sodiumTextField.text = [_editAliment.sodium stringValue];
        _sugarTextField.text = [_editAliment.sugar stringValue];
        _calciumTextField.text = [_editAliment.calcium stringValue];
        _ironTextField.text = [_editAliment.iron stringValue];
        _iodoTextField.text = [_editAliment.iodine stringValue];
        _magnesiumTextField.text = [_editAliment.magnesium stringValue];
        _zincTextField.text = [_editAliment.zinc stringValue];
        _seleniumTextField.text = [_editAliment.selenium stringValue];
        _potassiumTextField.text = [_editAliment.potasium stringValue];
        _phosphorusTextField.text = [_editAliment.phosphorus stringValue];
        _vitaminB1TextField.text = [_editAliment.vitB1 stringValue];
        _vitaminB2TextField.text = [_editAliment.vitB2 stringValue];
        _niacinTextField.text = [_editAliment.niacin stringValue];
        _vitaminB6TextField.text = [_editAliment.vitB6 stringValue];
        _folicAcidTextField.text = [_editAliment.folicAcid stringValue];
        _vitaminB12TextField.text = [_editAliment.vitB12 stringValue];
        _vitaminCTextField.text = [_editAliment.vitC stringValue];
        _retinolTextField.text = [_editAliment.retynol stringValue];
        _betaCaroteneTextField.text = [_editAliment.betaCaronete stringValue];
        _vitaminATextField.text = [_editAliment.vitA stringValue];
        _vitaminDTextField.text = [_editAliment.vitD stringValue];
        _indexGlycemicTextField.text = [_editAliment.glycemicIndex stringValue];
        _bccasTextField.text = [_editAliment.bccas stringValue];
        _glutamineTextField.text = [_editAliment.glutamine stringValue];
        _omega3TextField.text = [_editAliment.omega3 stringValue];
        _omega6TextField.text = [_editAliment.omega6 stringValue];
        
        NSArray * tag1Array = [_executive pickerListForTag:AlimentPickerTypeLabel1];
        _selectedLabel1Index = [measureUnitArray indexOfObject:_editAliment.tag1];
        if (_selectedLabel1Index == NSNotFound) {
            _selectedLabel1Index = 0;
        }
        else {
            _labellLabel.text = tag1Array[_selectedLabel1Index];
        }
        
        NSArray * tag2Array = [_executive pickerListForTag:AlimentPickerTypeLabel2];
        _selectedLabel2Index = [measureUnitArray indexOfObject:_editAliment.tag2];
        if (_selectedLabel2Index == NSNotFound) {
            _selectedLabel2Index = 0;
        }
        else {
            _label2Label.text = tag2Array[_selectedLabel2Index];
        }

        [_uploadImageView setImageWithURL:[NSURL URLWithString:_editAliment.image]];
    }
}

- (Aliment*)alimentFromDisplay {
    Aliment * aliment = [self alimentFromFormFields];
    if (_editAliment) {
        aliment.alimentId = _editAliment.alimentId;
        aliment.supplementId = _editAliment.supplementId;
    }
    
    return aliment;
}

- (Aliment*)alimentFromFormFields {
    Aliment * aliment = [[Aliment alloc] init];
    
    aliment.title = _nameTextField.text;
    aliment.content = _contentTextView.text;
    
    NSArray * measureUnitArray = [_executive pickerListForTag:AlimentPickerTypeMeasureUnit];
    aliment.measureUnit = measureUnitArray[_selectedMeasureUnitIndex];
    
    aliment.conversionString = _conversionChainTextField.text;
    aliment.conversionFactor = [NSNumber numberWithFloat:[_conversionFactorTextField.text floatValue]];
    aliment.calories = [NSNumber numberWithFloat:[_caloriesTextField.text floatValue]];
    aliment.proteins = [NSNumber numberWithFloat:[_proteinsTextField.text floatValue]];
    aliment.carbs = [NSNumber numberWithFloat:[_carbohydratorsTextField.text floatValue]];
    aliment.fiber = [NSNumber numberWithFloat:[_fiberTextField.text floatValue]];
    aliment.fats = [NSNumber numberWithFloat:[_fatTextField.text floatValue]];
    aliment.saturedFats = [NSNumber numberWithFloat:[_saturatedFatTextField.text floatValue]];
    aliment.monoUnsaturatedFats = [NSNumber numberWithFloat:[_monounsaturatedFatTextField.text floatValue]];
    aliment.polyUnsaturatedFats = [NSNumber numberWithFloat:[_polyunsaturatedFatTextField.text floatValue]];
    aliment.agpAgs = [NSNumber numberWithFloat:[_agpAgsTextField.text floatValue]];
    aliment.agpAgsAgm = [NSNumber numberWithFloat:[_agpAgsAgmTextField.text floatValue]];
    aliment.cholesterol = [NSNumber numberWithFloat:[_cholesterolTextField.text floatValue]];
    aliment.alcohol = [NSNumber numberWithFloat:[_alcoholTextField.text floatValue]];
    aliment.water = [NSNumber numberWithFloat:[_waterTextField.text floatValue]];
    aliment.sodium = [NSNumber numberWithFloat:[_sodiumTextField.text floatValue]];
    aliment.sugar = [NSNumber numberWithFloat:[_sugarTextField.text floatValue]];
    aliment.calcium = [NSNumber numberWithFloat:[_calciumTextField.text floatValue]];
    aliment.iron = [NSNumber numberWithFloat:[_ironTextField.text floatValue]];
    aliment.iodine = [NSNumber numberWithFloat:[_iodoTextField.text floatValue]];
    aliment.magnesium = [NSNumber numberWithFloat:[_magnesiumTextField.text floatValue]];
    aliment.zinc = [NSNumber numberWithFloat:[_zincTextField.text floatValue]];
    aliment.selenium = [NSNumber numberWithFloat:[_seleniumTextField.text floatValue]];
    aliment.potasium = [NSNumber numberWithFloat:[_potassiumTextField.text floatValue]];
    aliment.phosphorus = [NSNumber numberWithFloat:[_phosphorusTextField.text floatValue]];
    aliment.vitB1 = [NSNumber numberWithFloat:[_vitaminB1TextField.text floatValue]];
    aliment.vitB2 = [NSNumber numberWithFloat:[_vitaminB2TextField.text floatValue]];
    aliment.niacin = [NSNumber numberWithFloat:[_niacinTextField.text floatValue]];
    aliment.vitB6 = [NSNumber numberWithFloat:[_vitaminB6TextField.text floatValue]];
    aliment.folicAcid = [NSNumber numberWithFloat:[_folicAcidTextField.text floatValue]];
    aliment.vitB12 = [NSNumber numberWithFloat:[_vitaminB12TextField.text floatValue]];
    aliment.vitC = [NSNumber numberWithFloat:[_vitaminCTextField.text floatValue]];
    aliment.retynol = [NSNumber numberWithFloat:[_retinolTextField.text floatValue]];
    aliment.betaCaronete = [NSNumber numberWithFloat:[_betaCaroteneTextField.text floatValue]];
    aliment.vitA = [NSNumber numberWithFloat:[_vitaminATextField.text floatValue]];
    aliment.vitD = [NSNumber numberWithFloat:[_vitaminDTextField.text floatValue]];
    aliment.glycemicIndex = [NSNumber numberWithFloat:[_indexGlycemicTextField.text floatValue]];
    aliment.bccas = [NSNumber numberWithFloat:[_bccasTextField.text floatValue]];
    aliment.glutamine = [NSNumber numberWithFloat:[_glutamineTextField.text floatValue]];
    aliment.omega3 = [NSNumber numberWithFloat:[_omega3TextField.text floatValue]];
    aliment.omega6 = [NSNumber numberWithFloat:[_omega6TextField.text floatValue]];
    
    NSArray * tag1Array = [_executive pickerListForTag:AlimentPickerTypeLabel1];
    aliment.tag1 = tag1Array[_selectedLabel1Index];
    NSArray * tag2Array = [_executive pickerListForTag:AlimentPickerTypeLabel2];
    aliment.tag2 = tag2Array[_selectedLabel2Index];
    
    if ([_privacyLabel.text isEqualToString:@"Perfil Público"]) {
        aliment.privacity = @"public";
    }
    else {
        aliment.privacity = @"private";
    }
    
    [aliment.secondaryAttributes removeAllObjects];
    if (_isSupplement) {
        aliment.isSupplement = @YES;
        [aliment.secondaryAttributes addObjects:_secondaryAttributesViewController.items];
    }
    
    return aliment;
}

- (void)didPostedAliment:(Aliment*)aliment {
    [WebServiceManager getAlimentsWithCompletionBlock:^(BOOL isSuccess) {
        if (_mealOperationDelegate) {
            [_mealOperationDelegate didUpdateAliment:aliment];
        }
        [self dismissView];
    }];
}

#pragma mark - AttributeAddViewControllerDelegate Methods

- (void)didAddAtrribute:(FoodAttribute*)attribute {
    [_secondaryAttributesViewController.items addObject:attribute];
    [_secondaryAttributesViewController.tableView reloadData];
}

#pragma mark - SecondaryAttributesViewControllerDelegate Methods

- (void)heightChanged:(CGFloat)height {
    _secondaryAtrributeHeightConstraint.constant = height;
}

@end
