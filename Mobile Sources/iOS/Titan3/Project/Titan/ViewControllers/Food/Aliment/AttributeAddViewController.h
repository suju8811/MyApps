//
//  AttributeAddViewController.h
//  Titan
//
//  Created by Marcus Lee on 2/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodAttribute.h"
#import "ParentViewController.h"

@protocol AttributeAddViewControllerDelegate <NSObject>

- (void)didAddAtrribute:(FoodAttribute*)attribute;

@end

@interface AttributeAddViewController : ParentViewController

@property (nonatomic, strong) id<AttributeAddViewControllerDelegate> delegate;

@end
