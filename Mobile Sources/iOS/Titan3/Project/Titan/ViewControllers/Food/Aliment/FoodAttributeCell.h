//
//  FoodAttributeCell.h
//  Titan
//
//  Created by Marcus Lee on 2/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodAttribute.h"

@interface FoodAttributeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *valueTextField;

- (void)setContentByAttribute:(FoodAttribute*)attribute;

@end
