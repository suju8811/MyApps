//
//  SecondaryAttributesViewController.h
//  Titan
//
//  Created by Marcus Lee on 2/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SecondaryAttributesViewControllerDelegate <NSObject>

- (void)heightChanged:(CGFloat)height;

@end

@interface SecondaryAttributesViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray * items;
@property (nonatomic, strong) id<SecondaryAttributesViewControllerDelegate> delegate;

@end
