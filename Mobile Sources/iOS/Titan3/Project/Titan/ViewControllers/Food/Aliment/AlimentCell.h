//
//  AlimentCell.h
//  Titan
//
//  Created by Manuel Manzanera on 17/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagView.h"
#import "PropertyCircleView.h"

@protocol AlimentCellDelegate;

@interface AlimentCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *weightLabel;
@property (nonatomic, strong) UILabel *kCalLabel;

@property (nonatomic, strong) PropertyCircleView *proteinCircleView;
@property (nonatomic, strong) PropertyCircleView *carbsCircleView;
@property (nonatomic, strong) PropertyCircleView *fatsCircleView;

@property (nonatomic, strong) UILabel *proteinLabel;
@property (nonatomic, strong) UILabel *carbsLabel;
@property (nonatomic, strong) UILabel *fatsLabel;

@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, assign) id<AlimentCellDelegate>delegate;

@property (nonatomic, strong) TagView *tagView;

@property (nonatomic, strong) UIView *separatorView;

@property (nonatomic, strong) UIImageView *foodImageView;
@property (nonatomic, strong) UIImageView *checkImageView;

@property (nonatomic, strong) UIButton *infoButton;

@end

@protocol AlimentCellDelegate <NSObject>

- (void)alimentCellActionInIndex:(NSInteger)index;

@end
