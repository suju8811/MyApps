//
//  AlimentAddViewController.h
//  Titan
//
//  Created by Marcus Lee on 25/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "FoodViewController.h"
#import "Aliment.h"
#import "MealDetailViewController.h"

@interface AlimentAddViewController : ParentViewController

@property (nonatomic, strong) Aliment *editAliment;
@property (nonatomic, strong) FoodViewController *foodViewController;
@property (nonatomic, strong) Aliment *createAliment;
@property (nonatomic, assign) BOOL isSupplement;
@property (nonatomic, strong) id<MealOperationViewDelegate> mealOperationDelegate;

@end
