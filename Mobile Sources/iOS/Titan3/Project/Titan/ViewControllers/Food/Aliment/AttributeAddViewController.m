//
//  AttributeAddViewController.m
//  Titan
//
//  Created by Marcus Lee on 2/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "AttributeAddViewController.h"
#import "AttributeAddExecutive.h"

@interface AttributeAddViewController () <AttributeAddDisplay>

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *valueTextField;
@property (nonatomic, strong) AttributeAddExecutive * executive;

@end

@implementation AttributeAddViewController

#pragma mark - UIView Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _executive = [[AttributeAddExecutive alloc] initWithDisplay:self];
}

#pragma mark - Actions

- (IBAction)didPressBackButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didPressOKButton:(id)sender {
    [_executive didPressOKButton];
}

#pragma mark - AttributeAddDisplay Delegate

- (FoodAttribute*)attributeFromDisplay {
    FoodAttribute * attribute = [[FoodAttribute alloc] init];
    
    attribute.name = _nameTextField.text;
    attribute.value = [NSNumber numberWithFloat:_valueTextField.text.floatValue];
    
    return attribute;
}

- (void)didConfirmAdd:(FoodAttribute*)attribute {
    [_delegate didAddAtrribute:attribute];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
