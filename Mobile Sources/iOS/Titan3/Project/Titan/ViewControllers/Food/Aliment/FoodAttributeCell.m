//
//  FoodAttributeCell.m
//  Titan
//
//  Created by Marcus Lee on 2/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FoodAttributeCell.h"

@implementation FoodAttributeCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setContentByAttribute:(FoodAttribute*)attribute {
    _nameLabel.text = attribute.name;
    _valueTextField.text = [attribute.value stringValue];
}

@end
