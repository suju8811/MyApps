//
//  SecondaryAttributesViewController.m
//  Titan
//
//  Created by Marcus Lee on 2/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SecondaryAttributesViewController.h"
#import "FoodAttributeCell.h"

#define FOOD_ATTRIBUTE_CELL_IDENTIFIER  @"FoodAttributeCell"
#define ESTIMATED_ROW_HEIGHT            127

@interface SecondaryAttributesViewController ()

@end

@implementation SecondaryAttributesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.estimatedRowHeight = ESTIMATED_ROW_HEIGHT;
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated {    
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    CGFloat height = self.tableView.contentSize.height;
    [_delegate heightChanged:height];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingFooterView:(UIView *)view forSection:(NSInteger)section {
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     FoodAttributeCell *cell = [tableView dequeueReusableCellWithIdentifier:FOOD_ATTRIBUTE_CELL_IDENTIFIER forIndexPath:indexPath];
    
    FoodAttribute * attribute = _items[indexPath.row];
    [cell setContentByAttribute:attribute];
    
    return cell;
}

@end
