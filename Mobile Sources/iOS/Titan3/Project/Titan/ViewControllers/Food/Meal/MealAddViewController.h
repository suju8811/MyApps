//
//  MealAddViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 10/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "FoodViewController.h"
#import "SelectorFoodViewController.h"
#import "Meal.h"
#import "MealDetailViewController.h"

@interface MealAddViewController : ParentViewController

@property (nonatomic, strong) Meal *editMeal;
@property (nonatomic, strong) FoodViewController *foodViewController;
@property (nonatomic, strong) Meal *createMeal;
@property (nonatomic, strong) SelectorFoodViewController *selectorFoodViewController;
@property (nonatomic, strong) id<MealOperationViewDelegate> mealOperationViewDelegate;

@end
