//
//  FoodCell.m
//  Titan
//
//  Created by Manuel Manzanera on 21/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "MealCell.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "UtilManager.h"

@implementation MealCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        
        [self setBackgroundColor:[UIColor clearColor]];
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        
        _foodImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager width:10] + [UtilManager width:80] * 0.166667 , [UtilManager width:80] * 0.66, [UtilManager width:80] * 0.66)];
        [_foodImageView setCenter:CGPointMake(_foodImageView.center.x, [UtilManager height:45])];
        
        [[self contentView] addSubview:_foodImageView];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_foodImageView.frame.origin.x + _foodImageView.frame.size.width + [UtilManager width:10], [UtilManager height:5], WIDTH - (_foodImageView.frame.origin.x + _foodImageView.frame.size.width + [UtilManager width:10]), [UtilManager height:25])];
        [_titleLabel setAdjustsFontSizeToFitWidth:YES];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setTextColor:YELLOW_APP_COLOR];
        [_titleLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]];
        
        [[self contentView] addSubview:_titleLabel];
        
        _weightLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x , _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:10], [UtilManager width:40], _titleLabel.frame.size.height)];
        [_weightLabel setTextColor:GRAY_REGISTER_FONT];
        [_weightLabel setText:@"100 gr"];
        [_weightLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [_weightLabel setAdjustsFontSizeToFitWidth:YES];
        
        //[[self contentView] addSubview:_weightLabel];
        
        UIImageView *kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + [UtilManager height:10], [UtilManager width:20] * 0.75, [UtilManager width:20] * 0.75)];
        [kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-green"]];
        
        [[self contentView] addSubview:kCalImageView];
        
        _kCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(kCalImageView.frame.origin.x + kCalImageView.frame.size.width + 5, _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:20], [UtilManager width:60], _titleLabel.frame.size.height)];
        [_kCalLabel  setCenter:CGPointMake(_kCalLabel.center.x, kCalImageView.center.y)];
        [_kCalLabel setTextColor:GREEN_APP_COLOR];
        [_kCalLabel setFont:_weightLabel.font];
        [_kCalLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_kCalLabel];
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake(_kCalLabel.frame.origin.x + _kCalLabel.frame.size.width + [UtilManager width:10], _titleLabel.frame.size.height + _titleLabel.frame.origin.y + [UtilManager height:5], _titleLabel.frame.size.width, TAGVIEW_SMALL_HEIGHT)];
        [_tagView setIsReduced:TRUE];
        [_tagView setMode:TagsControlModeList];
        
        [[self contentView] addSubview:_tagView];
        
        _proteinLabel = [[UILabel alloc] initWithFrame:CGRectMake(kCalImageView.frame.origin.x, _kCalLabel.frame.origin.y + _kCalLabel.frame.size.height + [UtilManager height:5], [UtilManager width:40], [UtilManager width:40])];
        [[_proteinLabel layer] setBorderColor:BLUE_PERCENT_CIRCLE.CGColor];
        [[_proteinLabel layer] setBorderWidth:1];
        [[_proteinLabel layer] setCornerRadius:_proteinLabel.frame.size.width/2];
        [_proteinLabel setText:@"p\n26 g"];
        [_proteinLabel setNumberOfLines:2];
        //[_proteinLabel setCenter:CGPointMake(_proteinLabel.center.x, _kCalLabel.center.y)];
        [_proteinLabel setTextColor:BLUE_PERCENT_CIRCLE];
        [_proteinLabel setTextAlignment:NSTextAlignmentCenter];
        
        //[[self contentView] addSubview:_proteinLabel];
        
        _proteinCircleView = [[PropertyCircleView alloc] initWithFrame:_proteinLabel.frame];
        [[_proteinCircleView layer] setBorderColor:BLUE_PERCENT_CIRCLE.CGColor];
        [[_proteinCircleView layer] setBorderWidth:1];
        [[_proteinCircleView layer] setCornerRadius:_proteinLabel.frame.size.width/2];
        
        [[_proteinCircleView initialLabel] setText:@"P"];
        [[_proteinCircleView initialLabel] setTextColor:BLUE_PERCENT_CIRCLE];
        [[_proteinCircleView initialLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:10]];
        [[_proteinCircleView countLabel] setText:@"95"];
        [[_proteinCircleView countLabel] setTextColor:BLUE_PERCENT_CIRCLE];
        [[_proteinCircleView measureLabel] setTextColor:BLUE_PERCENT_CIRCLE];
        [[_proteinCircleView countLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:15]];
        [[_proteinCircleView measureLabel] setText:@"g"];
        [[_proteinCircleView measureLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:15]];
        
        [[self contentView] addSubview:_proteinCircleView];
        
        _carbsLabel = [[UILabel alloc] initWithFrame:CGRectMake(_proteinLabel.frame.origin.x + _proteinLabel.frame.size.width + [UtilManager width:10], 0., _proteinLabel.frame.size.width, _proteinLabel.frame.size.height)];
        [[_carbsLabel layer] setBorderColor:YELLOW_PERCENT_CIRCLE.CGColor];
        [[_carbsLabel layer] setBorderWidth:1];
        [[_carbsLabel layer] setCornerRadius:_proteinLabel.frame.size.width/2];
        [_carbsLabel setText:@"p\n26 g"];
        [_carbsLabel setNumberOfLines:2];
        [_carbsLabel setCenter:CGPointMake(_carbsLabel.center.x, _proteinCircleView.center.y)];
        [_carbsLabel setTextColor:YELLOW_PERCENT_CIRCLE];
        [_carbsLabel setTextAlignment:NSTextAlignmentCenter];
        
        //[[self contentView] addSubview:_carbsLabel];
        
        _carbsCircleView = [[PropertyCircleView alloc] initWithFrame:_carbsLabel.frame];
        [[_carbsCircleView layer] setBorderColor:YELLOW_PERCENT_CIRCLE.CGColor];
        [[_carbsCircleView layer] setBorderWidth:1];
        [[_carbsCircleView layer] setCornerRadius:_proteinLabel.frame.size.width/2];
        
        [[_carbsCircleView initialLabel] setText:@"C"];
        [[_carbsCircleView initialLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
        [[_carbsCircleView countLabel] setText:@"95"];
        [[_carbsCircleView countLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
        [[_carbsCircleView measureLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
        [[_carbsCircleView measureLabel] setText:@"g"];
        [[_carbsCircleView initialLabel] setFont:[_proteinCircleView initialLabel].font];
        [[_carbsCircleView countLabel] setFont:[_proteinCircleView countLabel].font];
        [[_carbsCircleView measureLabel] setFont:[_proteinCircleView measureLabel].font];
        
        [[self contentView] addSubview:_carbsCircleView];
        
        _fatsLabel = [[UILabel alloc] initWithFrame:CGRectMake(_carbsLabel.frame.origin.x + _carbsLabel.frame.size.width + [UtilManager width:10], 0., _proteinLabel.frame.size.width, _proteinLabel.frame.size.height)];
        [[_fatsLabel layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
        [[_fatsLabel layer] setBorderWidth:1];
        [[_fatsLabel layer] setCornerRadius:_proteinLabel.frame.size.width/2];
        [_fatsLabel setText:@"p\n26 g"];
        [_fatsLabel setNumberOfLines:2];
        [_fatsLabel setCenter:CGPointMake(_fatsLabel.center.x, _proteinCircleView.center.y)];
        [_fatsLabel setTextColor:ORANGE_PERCENT_CIRCLE];
        [_fatsLabel setTextAlignment:NSTextAlignmentCenter];
        
        //[[self contentView] addSubview:_fatsLabel];
        
        _fatsCircleView = [[PropertyCircleView alloc] initWithFrame:_fatsLabel.frame];
        [[_fatsCircleView layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
        [[_fatsCircleView layer] setBorderWidth:1];
        [[_fatsCircleView layer] setCornerRadius:_proteinLabel.frame.size.width/2];
        
        [[_fatsCircleView initialLabel] setText:@"F"];
        [[_fatsCircleView initialLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
        [[_fatsCircleView countLabel] setText:@"95"];
        [[_fatsCircleView countLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
        [[_fatsCircleView measureLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
        [[_fatsCircleView measureLabel] setText:@"g"];
        [[_fatsCircleView initialLabel] setFont:[_proteinCircleView initialLabel].font];
        [[_fatsCircleView countLabel] setFont:[_proteinCircleView countLabel].font];
        [[_fatsCircleView measureLabel] setFont:[_proteinCircleView measureLabel].font];
        
        [[self contentView] addSubview:_fatsCircleView];
        
        _infoButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50],
                                                                 0.,
                                                                 [UtilManager width:40],
                                                                 [UtilManager width:40])];
        
        [_infoButton setCenter:CGPointMake(_infoButton.center.x, foodCellHeight/2)];
        [_infoButton addTarget:self action:@selector(infoAction) forControlEvents:UIControlEventTouchUpInside];
        [_infoButton setImage:[UIImage imageNamed:@"btn-options"] forState:UIControlStateNormal];
        
        [[self contentView] addSubview:_infoButton];
        
        _detailInfoButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(_infoButton.frame) - [UtilManager width:45],
                                                                 0.,
                                                                 [UtilManager width:40],
                                                                 [UtilManager width:40])];
        
        [_detailInfoButton setCenter:CGPointMake(_detailInfoButton.center.x, foodCellHeight/2)];
        [_detailInfoButton addTarget:self action:@selector(detailInfoAction) forControlEvents:UIControlEventTouchUpInside];
        [_detailInfoButton setImage:[UIImage imageNamed:@"btn_show_detail_info_small"] forState:UIControlStateNormal];
        
        [[self contentView] addSubview:_detailInfoButton];
        
        _checkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:25], 0, [UtilManager width:18], [UtilManager width:18])];
        [_checkImageView setImage:[UIImage imageNamed:@"img-check"]];
        [_checkImageView setCenter:CGPointMake(_checkImageView.center.x, foodCellHeight/2)];
        [_checkImageView setHidden:true];
        
        [[self contentView] addSubview:_checkImageView];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:10], foodCellHeight - 1, WIDTH - [UtilManager width:20], 1)];
        [_separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        [_separatorView setAlpha:0.2];
        
        [[self contentView] addSubview:_separatorView];
    }
    
    return self;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)infoAction {
    [_delegate mealCellActionInIndex:_currentIndex];
}

- (void)detailInfoAction {
    [_delegate mealCellDetailInfoActionInIndex:_currentIndex];
}

@end
