//
//  MealAddViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 10/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "MealAddViewController.h"
#import "WebServiceManager.h"
#import "UIImageView+AFNetworking.h"
#import "MRProgress.h"
#import "UploadMediaView.h"
#import "SelectTagView.h"
#import "MealAddExecutive.h"

@interface MealAddViewController ()<UITextFieldDelegate, UITextViewDelegate, SelectorFoodViewDelegate, UploadMediaViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,SelectTagViewDelegate, MealAddDisplay>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITextField *nameTextField;

@property (nonatomic, strong) UITextView *contentTextView;
@property (nonatomic, strong) UITextView *noteTextView;

@property (nonatomic, strong) UIButton *privacityButton;
@property (nonatomic, strong) UILabel *privacityLabel;

@property (nonatomic, strong) UploadMediaView *uploadMediaView;
@property (nonatomic, strong) UIImageView *uploadImageView;
@property (nonatomic, strong) UIImage *uploadImage;
@property (nonatomic, strong) UIImagePickerController *picker;

@property (nonatomic, strong) SelectTagView *selectTagView;
@property (nonatomic, strong) NSMutableArray *tags;
@property (nonatomic, strong) NSMutableArray *selectedTags;

@property (nonatomic, strong) MealAddExecutive * executive;

@end

@implementation MealAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeAction) name:NAV_RIGHT1_ACTION object:nil];
    
    [self configureView];
    
    _executive = [[MealAddExecutive alloc] initWithDisplay:self];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(_selectorFoodViewController.presentMealDetail){
        
        _selectorFoodViewController.presentMealDetail = FALSE;
        
        for(Aliment *aliment in _selectorFoodViewController.selectResults){
            
            Ingredient *ingredient = [[Ingredient alloc] init];
            [ingredient setIngredientId:[UtilManager uuid]];
            
            [ingredient setAliment:aliment];
            [ingredient setQuantity:[NSNumber numberWithInteger:100]];
            [ingredient setQuantityUnit:aliment.measureUnit];
            
            [_createMeal.ingredients addObject:ingredient];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                [aliment setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
        }
        
        [self createNewMeal];
    }
}
    
- (void)createNewMeal {
    [_executive createMealWithImage:_uploadImage];
}

- (void)closeAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)configureView{
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _nameTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:10], NAV_HEIGHT, WIDTH - [UtilManager width:20], UITEXTFIELD_HEIGHT)];
    
    [_nameTextField setDelegate:self];
    [_nameTextField setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]];
    [_nameTextField setTextColor:GRAY_REGISTER_FONT];
    [_nameTextField setBackgroundColor:[UIColor clearColor]];
    [_nameTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    _nameTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Nombre", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName:[UIColor grayColor],
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]
                                                 }
     ];
    
    if(_editMeal) {
        _nameTextField.text = _editMeal.title;
    }
    
    [self.view addSubview:_nameTextField];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _nameTextField.frame.origin.y + _nameTextField.frame.size.height , WIDTH - [UtilManager width:40], 1)];
    [separatorView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self.view addSubview:separatorView];
    
    _tags = [Tag getTagsWithType:@"type_1" andCollection:@"Meals"];
    
    _selectTagView = [[SelectTagView alloc] initWithFrame:CGRectMake([UtilManager width:10], separatorView.frame.origin.y + separatorView.frame.size.height + [UtilManager height:10], WIDTH - [UtilManager width:20], [UtilManager height:30]) andTags:_tags];
    
    if(_createMeal) {
        _selectedTags = [[NSMutableArray alloc] initWithCapacity:_createMeal.tags.count];
        for(Tag *tag in _createMeal.tags) {
            [_selectedTags addObject:tag];
        }
        [_selectTagView setSelectedTags:_selectedTags];
    }
    
    [_selectTagView setDelegate:self];
    
    [self.view addSubview:_selectTagView];
    
    _contentTextView = [[UITextView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _selectTagView.frame.origin.y + _selectTagView.frame.size.height + [UtilManager height:10], _nameTextField.frame.size.width, [UtilManager height:120])];
    [_contentTextView setTextColor:_nameTextField.textColor];
    [_contentTextView setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_contentTextView setTextAlignment:NSTextAlignmentJustified];
    [_contentTextView setText:NSLocalizedString(@"Descripción", nil)];
    [_contentTextView setFont:[UIFont fontWithName:ITALIC size:18]];
    [_contentTextView setBackgroundColor:[UIColor clearColor]];
    [[_contentTextView layer] setBorderWidth:1];
    [[_contentTextView layer] setBorderColor:BLACK_HEADER_APP_COLOR.CGColor];
    
    if(_editMeal) {
        _contentTextView.text = _editMeal.content;
    }
    
    [_contentTextView setDelegate:self];
    
    UIToolbar *toolbarWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //UIBarButtonItem *barWeigthButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resetView)];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetView)];
    [barButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarWeigth setItems:[NSArray arrayWithObjects:flexibleWeigthSpace, barButtonItem, nil]];
    _contentTextView.inputAccessoryView = toolbarWeigth;
    
    [self.view addSubview:_contentTextView];
    
    _noteTextView = [[UITextView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _contentTextView.frame.origin.y + _contentTextView.frame.size.height + [UtilManager height:10], _nameTextField.frame.size.width, [UtilManager height:180])];
    [_noteTextView setTextColor:_nameTextField.textColor];
    [_noteTextView setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_noteTextView setTextAlignment:NSTextAlignmentJustified];
    [_noteTextView setText:NSLocalizedString(@"Notas", nil)];
    [_noteTextView setFont:[UIFont fontWithName:ITALIC size:18]];
    [_noteTextView setBackgroundColor:[UIColor clearColor]];
    [[_noteTextView layer] setBorderWidth:1];
    [[_noteTextView layer] setBorderColor:BLACK_HEADER_APP_COLOR.CGColor];
    
    if(_editMeal) {
        _noteTextView.text = _editMeal.notes;
    }
    
    [_noteTextView setDelegate:self];
    
    UIToolbar *toolbarNoteWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarNoteWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleNoteWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //UIBarButtonItem *barWeigthButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resetView)];
    UIBarButtonItem *barNoteButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetView)];
    [barNoteButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barNoteButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarNoteWeigth setItems:[NSArray arrayWithObjects:flexibleNoteWeigthSpace, barNoteButtonItem, nil]];
    _noteTextView.inputAccessoryView = toolbarNoteWeigth;
    
    [self.view addSubview:_noteTextView];
    
    _privacityButton = [[UIButton alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x,_noteTextView.frame.origin.y + _noteTextView.frame.size.height + [UtilManager height:10] , [UtilManager width:40], [UtilManager width:40])];
    [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
    [_privacityButton addTarget:self action:@selector(privacityAction) forControlEvents:UIControlEventTouchUpInside];
    
//    [self.view addSubview:_privacityButton];
    
    _privacityLabel = [[UILabel alloc] initWithFrame:CGRectMake(_privacityButton.frame.origin.x + _privacityButton.frame.size.width + [UtilManager width:10], _privacityButton.frame.origin.y, [UtilManager width:240], _privacityButton.frame.size.height)];
    [_privacityLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    [_privacityLabel setTextAlignment:NSTextAlignmentLeft];
    [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    [_privacityLabel setTextColor:[UIColor whiteColor]];
    
//    [self.view addSubview:_privacityLabel];
    
    if([_editMeal.privacity isEqualToString:@"public"]){
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    }else{
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-closed"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Privado", nil)];
    }
    
    _uploadImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_noteTextView.frame.origin.x,
                                                                     CGRectGetMaxY(_noteTextView.frame) + [UtilManager height:20],
                                                                     [UtilManager width:100],
                                                                     [UtilManager width:100])];
    
    [_uploadImageView setUserInteractionEnabled:TRUE];
    [[_uploadImageView layer] setBorderWidth:1];
    [[_uploadImageView layer] setBorderColor:YELLOW_APP_COLOR.CGColor];
    [_uploadImageView setUserInteractionEnabled:TRUE];
    
    if(_editMeal){
        UIImageView *imageView = _uploadImageView;
        
        NSURLRequest *foodImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_MEAL,_createMeal.image]]
                                                          cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                      timeoutInterval:60];
        
        [_uploadImageView setImageWithURLRequest:foodImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
            [imageView setImage:image];
            [imageView setContentMode:UIViewContentModeScaleAspectFit];
            [[imageView layer] setMasksToBounds:YES];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
            NSLog(@"%@",error);
            [imageView setImage:[UIImage imageNamed:@"img-placeholder"]];
        }];
    }
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentPickView)];
    [_uploadImageView addGestureRecognizer:tapGesture];
    
    [self.view addSubview:_uploadImageView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, NAV_HEIGHT - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    if(_editMeal)
        [titleLabel setText:NSLocalizedString(@"EDITAR COMIDA", nil)];
    else
        [titleLabel setText:NSLocalizedString(@"NUEVA COMIDA", nil)];
    [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:titleLabel];
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50], [UtilManager height:20], [UtilManager width:40] * 0.88, [UtilManager width:40] * 0.8)];
    [okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
    [okButton setCenter:CGPointMake(okButton.center.x, titleLabel.center.y)];
    
    [self.view addSubview:okButton];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:20.], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton setHidden:FALSE];
    [backButton setCenter:CGPointMake(backButton.center.x, okButton.center.y)];
    
    [self.view addSubview:backButton];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didPressBackButton)];
    [swipeGesture setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:swipeGesture];
}

-  (void)setTagsToOff{
    RLMRealm *realm = [AppContext currentRealm];
    if(_createMeal){
        for(Tag *tag in _createMeal.tags){
            [realm transactionWithBlock:^{
                if([tag.status isEqualToString:@"Off"])
                    [tag setStatus:@"Off"];
                else
                    [tag setStatus:@"Off"];
            }];
        }
    }
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
}

- (void)presentPickView{
    if(_uploadMediaView)
        _uploadMediaView = nil;
    _uploadMediaView = [[UploadMediaView alloc] initWithFrame:self.view.bounds];
    [_uploadMediaView setDelegate:self];
    //[_uploadMediaView setAccessibilityLabel:tapGesture.accessibilityLabel];
    [self.view addSubview:_uploadMediaView];
}

- (void)didPressBackButton {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)privacityAction{
    if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]]){
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-closed"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Privado", nil)];
    }else{
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    }
}

#pragma mark SelectTagViewDelegate Methods

- (void)actionDidPushReturnTags:(NSMutableArray *)selectedTags{
    _selectedTags = nil;
    _selectedTags = [[NSMutableArray alloc] init];
    _selectedTags = selectedTags;
}

#pragma mark UploadViewDelegateMethods

- (void)dismissViewSelected{
    [_uploadMediaView removeFromSuperview];
}

- (void)galleryDidselected{
    _picker = [[UIImagePickerController alloc] init];
    [_picker setAccessibilityLabel:_uploadMediaView.accessibilityLabel];
    [_uploadMediaView removeFromSuperview];
    [_picker setDelegate:self];
    [_picker setAllowsEditing:YES];
    [_picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:_picker animated:YES completion:^{
        
    }];
}

- (void)cameraDidSelected{
    _picker = [[UIImagePickerController alloc] init];
    [_picker setAccessibilityLabel:_uploadMediaView.accessibilityLabel];
    [_uploadMediaView removeFromSuperview];
    [_picker setAllowsEditing:YES];
    [_picker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [_picker setDelegate:self];
    
    [self presentViewController:_picker animated:YES completion:^{
        
    }];
}

#pragma mark UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    _uploadImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    [_uploadImageView setImage:_uploadImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)resetView {
    [_contentTextView resignFirstResponder];
    [_noteTextView resignFirstResponder];
}

- (void)didPressOKButton {
    if([self checkFields]) {
        if(_editMeal) {
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                
                [_editMeal setTitle:_nameTextField.text];
                [_editMeal setContent:_contentTextView.text];
                [_editMeal setNotes:_noteTextView.text];
                
                if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]])
                    [_editMeal setPrivacity:@"public"];
                else
                    [_editMeal setPrivacity:@"private"];
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            
            [_executive editMealWithImage:_uploadImage];
            
        } else {
            
            _createMeal = [[Meal alloc] init];
            
            [_createMeal setMealId:[UtilManager uuid]];
            [_createMeal setTitle:_nameTextField.text];
            [_createMeal setContent:_contentTextView.text];
            [_createMeal setNotes:_noteTextView.text];
            
            if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]])
                [_createMeal setPrivacity:@"public"];
            else
                [_createMeal setPrivacity:@"private"];
            
            /*
            RLMRealm *realm = [AppContext currentRealm];
            [realm beginWriteTransaction];
            [realm addOrUpdateObject:_createMeal];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            */
            
            [_createMeal setOwner:[[AppContext sharedInstance] userId]];
            
            _selectorFoodViewController = [[SelectorFoodViewController alloc] init];
            [_selectorFoodViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [_selectorFoodViewController setSelectorType:SelectorFoodTypeMeal];
            [_selectorFoodViewController setSelectMeal:_createMeal];
            [_selectorFoodViewController setDelegate:self];
            
            [self presentViewController:_selectorFoodViewController animated:YES completion:nil];
        }
        
        
    } else
        [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)checkFields{
    
    if(_nameTextField.text.length == 0){
        [self presentViewController:[UtilManager presentAlertWithMessage:@"" andTitle:NSLocalizedString(@"El campo nombre es oblicatorio", nil)] animated:YES completion:^{
            
        }];
        
        return FALSE;
    } else{
        if(_editMeal)
        {
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                [_editMeal setTitle:_nameTextField.text];
                [_editMeal setContent:_contentTextView.text];
                [_editMeal setNotes:_noteTextView.text];
                if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]]) {
                    [_editMeal setPrivacity:@"public"];
                }
                else {
                    [_editMeal setPrivacity:@"private"];
                }
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
        }
        else{
            
        }
    }
    
    if(_editMeal){
        if(_editMeal.ingredients.count == 0){
            [self presentViewController:[UtilManager presentAlertWithMessage:@"" andTitle:NSLocalizedString(@"Debe añadir algún alimento", nil)] animated:YES completion:^{
                
            }];
            return FALSE;
        }
    }
    
    return TRUE;
}

#pragma mark UITextFieldDelegat Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}

#pragma mark UITextViewDelegate Methods

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
    return true;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if([[textView text] isEqualToString:NSLocalizedString(@"Descripción", nil)])
        [_contentTextView setText:@""];
    if([[textView text] isEqualToString:NSLocalizedString(@"Notas", nil)])
        [_noteTextView setText:@""];
    
    return  true;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return TRUE;
}

#pragma mark - MealAddDisplay Delegate

- (Meal*)mealFromDisplay {
    if (_editMeal) {
        return _editMeal;
    }
  
    return _createMeal;
}

- (void)didCreateMeal:(Meal *)meal {
    [WebServiceManager getMealsWithCompletionBlock:^(BOOL isSuccess) {
        if(_foodViewController) {
            [_foodViewController setCreateMeal:meal];
        }
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            if (_mealOperationViewDelegate) {
                [_mealOperationViewDelegate didUpdatedMeal:meal];
            }
        }];
    }];
}

@end
