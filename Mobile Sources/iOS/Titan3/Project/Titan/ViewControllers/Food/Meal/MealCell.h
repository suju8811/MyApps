//
//  FoodCell.h
//  Titan
//
//  Created by Manuel Manzanera on 21/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagView.h"
#import "PropertyCircleView.h"

@protocol MealCellDelegate;

@interface MealCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *weightLabel;
@property (nonatomic, strong) UILabel *kCalLabel;

@property (nonatomic, strong) PropertyCircleView *proteinCircleView;
@property (nonatomic, strong) PropertyCircleView *carbsCircleView;
@property (nonatomic, strong) PropertyCircleView *fatsCircleView;

@property (nonatomic, strong) UILabel *proteinLabel;
@property (nonatomic, strong) UILabel *carbsLabel;
@property (nonatomic, strong) UILabel *fatsLabel;

@property (nonatomic, strong) UIButton *infoButton;
@property (nonatomic, strong) UIButton *detailInfoButton;

@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, assign) id<MealCellDelegate>delegate;

@property (nonatomic, strong) TagView *tagView;

@property (nonatomic, strong) UIView *separatorView;

@property (nonatomic, strong) UIImageView *foodImageView;
@property (nonatomic, strong) UIImageView *checkImageView;


@end

@protocol MealCellDelegate <NSObject>

- (void)mealCellActionInIndex:(NSInteger)index;

- (void)mealCellDetailInfoActionInIndex:(NSInteger)index;

@end
