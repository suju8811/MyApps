//
//  FoodViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FoodViewController.h"
#import "MealDetailViewController.h"
#import "DietDetailViewController.h"
#import "NutritionPlanDetailViewController.h"
#import "SegmentView.h"
#import "PropertyCircleView.h"
#import "Food.h"
#import "Diet.h"
#import "Aliment.h"
#import "Meal.h"
#import "MealCell.h"
#import "DietCell.h"
#import "AlimentCell.h"
#import "Tag.h"
#import "PlanFoodViewCell.h"
#import "RNBlurModalView.h"
#import "FoodDetailView.h"
#import "UIImageView+AFNetworking.h"
#import "MenuContainerNavController.h"
#import "OptionsFoodViewController.h"
#import "MealAddViewController.h"
#import "DietAddViewController.h"
#import "NutritionPlanAddViewController.h"
#import "AlimentAddViewController.h"
#import "WebServiceManager.h"
#import "SelectorFoodViewController.h"
#import "UserDefaultLibrary.h"
#import "MealDetailInfoView.h"
#import "DietDetailInfoView.h"
#import "NutritionPlanDetailinfoView.h"
#import "UserSelectorViewController.h"
#import "UserMeta.h"

@interface FoodViewController ()<UITableViewDelegate, UITableViewDataSource, AlimentCellDelegate, MealCellDelegate, OptionsFoodViewControllerDelegate, DietCellDelegate, PlanFoodCellDelegate, FoodDisplay, SelectorFoodViewDelegate, UserSelectorViewControllerDelegate>

@property (nonatomic, strong) UIButton *profileButton;
@property (nonatomic, strong) UIButton *favButton;
@property (nonatomic, strong) UIButton *comunityButton;
@property (nonatomic, strong) UIButton *downloadButton;

@property (nonatomic, strong) SegmentView *segmentedView;
@property (strong, nonatomic) NSArray *segmentTitles;

@property (nonatomic, strong) UITableView *foodTableView;

@property (nonatomic, strong) UIButton *planButton;
@property (nonatomic, strong) UIButton *dietButton;
@property (nonatomic, strong) UIButton *mealButton;
@property (nonatomic, strong) UIButton *alimentButton;

@property (nonatomic, strong) UILabel *planLabel;
@property (nonatomic, strong) UILabel *dietLabel;
@property (nonatomic, strong) UILabel *mealLabel;
@property (nonatomic, strong) UILabel *alimentLabel;

@property (nonatomic, strong) UISegmentedControl *segmentControl;
@property (nonatomic) NSInteger segmentState;

@property (nonatomic, strong) RLMResults *items;
@property (nonatomic, strong) RNBlurModalView *modalView;

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIView *segmentControlView;

@property (nonatomic, strong) NSMutableArray *foodIndex;

@property (nonatomic, strong) MenuContainerNavController *menuContainerNavController;

@property (nonatomic, strong) UILabel *warningLabel;
@property (nonatomic, strong) FoodExecutive * executive;

@property (nonatomic, assign) NSInteger selectedFoodIndex;
@property (nonatomic, strong) NSString * keyword;

@property (nonatomic, strong) SelectorFoodViewController * selectorFoodViewController;
@property (nonatomic, strong) UserSelectorViewController * userSelectorViewController;

@property (nonatomic, strong) UIButton *acceptButton;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) NSDate *endDate;

@end

@implementation FoodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _menuContainerNavController = (MenuContainerNavController *)self.navigationController;
    _selectType = FoodSelectTypePlan;
    
    _items = [NutritionPlan getNutritionPlansWithOwner:[[[AppContext sharedInstance] currentUser] userId]];
    
    [self configureView];
    
    _executive = [[FoodExecutive alloc] initWithDisplay:self];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NAV_RIGHT1_ACTION object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPressSearchButton) name:NAV_RIGHT1_ACTION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPressAddButton) name:NAV_RIGHT2_ACTION object:nil];
    
    [_executive loadItemsForFoodSelectType:_selectType
                            mealSelectType:_mealSelectType
                            dietSelectType:_dietSelectType
                   nutritionPlanSelectType:_nutritionPlanSelectType
                       alimentSegmentState:_segmentState
                                searchText:_keyword];
}


- (void)configureView{
    self.view.backgroundColor = BLACK_APP_COLOR;
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    backgroundImageView.image =[UIImage imageNamed:@"img-blur-bkg"];
    [self.view addSubview:backgroundImageView];
    
    //[self.view addSubview:[self headerView]];
    
    _warningLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:200], [UtilManager height:60])];
    [_warningLabel setNumberOfLines:0];
    [_warningLabel setTextColor:GRAY_REGISTER_FONT];
    [_warningLabel setFont:[UIFont fontWithName:REGULAR_FONT size:18]];
    [_warningLabel setCenter:self.view.center];
    [_warningLabel setTextAlignment:NSTextAlignmentCenter];
    
    [[self view] addSubview:_warningLabel];
    
    _foodTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., NAV_HEIGHT + TAB_BAR_HEIGHT, WIDTH, HEIGHT - NAV_HEIGHT - TAB_BAR_HEIGHT- FOOTER_BAR_HEIGHT) style:UITableViewStylePlain];
    [_foodTableView registerClass:[MealCell class] forCellReuseIdentifier:@"foodCell"];
    [_foodTableView registerClass:[PlanFoodViewCell class] forCellReuseIdentifier:@"planFoodCell"];
    [_foodTableView registerClass:[DietCell class] forCellReuseIdentifier:@"dietCell"];
    [_foodTableView registerClass:[AlimentCell class] forCellReuseIdentifier:@"alimentCell"];
    [_foodTableView setBounces:FALSE];
    [_foodTableView setDelegate:self];
    [_foodTableView setDataSource:self];
    [_foodTableView setBackgroundColor:[UIColor clearColor]];
    [_foodTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_foodTableView setSectionIndexColor:YELLOW_APP_COLOR];
    [_foodTableView setSectionIndexBackgroundColor:[UIColor clearColor]];
    [_foodTableView setBounces:FALSE];
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(rightSwipe)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    
    [_foodTableView addGestureRecognizer:recognizer];
    
    [self.view addSubview:_foodTableView];
    [self.view addSubview:[self footerView]];
    
    [self addHeaderView];
    [self addSegmentControlView];
    
}

- (void)rightSwipe {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTIF_MENU_SLIDE" object:nil];
}

- (void)addHeaderView {
    _headerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20],
                                                           NAV_HEIGHT,
                                                           WIDTH - [UtilManager width:40],
                                                           TAB_BAR_HEIGHT)];
    
    _favButton = [[UIButton alloc] initWithFrame:CGRectMake(0.f,
                                                            [UtilManager height:5],
                                                            [UtilManager width:40] * 0.8f,
                                                            [UtilManager width:40]* 0.8f)];
    
    [_favButton setImage:[UIImage imageNamed:@"icon-fav-off"] forState:UIControlStateNormal];
    [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateSelected];
    [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateHighlighted];
    _favButton.center = CGPointMake(_headerView.frame.size.width * 3/8, _favButton.center.y);
    _favButton.tag = 1;
    [_favButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    [_headerView addSubview:_favButton];
    
    _profileButton = [[UIButton alloc] initWithFrame:CGRectMake(0.f,
                                                                0.f,
                                                                CGRectGetWidth(_favButton.frame),
                                                                CGRectGetHeight(_favButton.frame))];
    
    [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateSelected];
    [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateHighlighted];
    [_profileButton setImage:[UIImage imageNamed:@"icon-profile-off"] forState:UIControlStateNormal];
    _profileButton.center = CGPointMake(_headerView.frame.size.width/8, _favButton.center.y);
    _profileButton.tag = 0;
    _profileButton.selected = YES;
    [_profileButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    [_headerView addSubview:_profileButton];
    
    _comunityButton = [[UIButton alloc] initWithFrame:CGRectMake(0.f,
                                                                 0.f,
                                                                 CGRectGetWidth(_favButton.frame),
                                                                 CGRectGetHeight(_favButton.frame))];
    
    [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateSelected];
    [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateHighlighted];
    [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-off"] forState:UIControlStateNormal];
    _comunityButton.center = CGPointMake(5 * _headerView.frame.size.width/8, _favButton.center.y);
    _comunityButton.tag = 2;
    [_comunityButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    [_headerView addSubview:_comunityButton];
    
    _downloadButton = [[UIButton alloc] initWithFrame:CGRectMake(0.f,
                                                                 0.f,
                                                                 CGRectGetWidth(_favButton.frame),
                                                                 CGRectGetHeight(_favButton.frame))];
    
    [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateSelected];
    [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateHighlighted];
    [_downloadButton setImage:[UIImage imageNamed:@"icon-download-off"] forState:UIControlStateNormal];
    _downloadButton.center = CGPointMake(7 * _headerView.frame.size.width/8, _favButton.center.y);
    _downloadButton.tag = 3;
    [_downloadButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    [_headerView addSubview:_downloadButton];
    
    [[self view] addSubview:_headerView];
}

- (void)addSegmentControlView {
    _segmentControlView = [[UIView alloc] initWithFrame:CGRectMake(0.f,
                                                                   NAV_HEIGHT,
                                                                   WIDTH,
                                                                   TAB_BAR_HEIGHT)];
    
    _segmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Alimentos", nil),
                                                                  NSLocalizedString(@"Suplementos", nil)]];
    _segmentControl.selectedSegmentIndex = _segmentState;
    [_segmentControl addTarget:self action:@selector(changeAlimentsView:) forControlEvents:UIControlEventValueChanged];
    _segmentControl.backgroundColor = BLACK_APP_COLOR;
    _segmentControl.center = CGPointMake(_segmentControlView.center.x,
                                         CGRectGetHeight(_segmentControlView.frame)/2);
    
    _segmentControl.tintColor = YELLOW_APP_COLOR;
    [_segmentControlView addSubview:_segmentControl];
    _segmentControlView.hidden = YES;
    
    [self.view addSubview:_segmentControlView];
}

- (UIView *)footerView {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.f,
                                                                  HEIGHT - FOOTER_BAR_HEIGHT,
                                                                  WIDTH,
                                                                  FOOTER_BAR_HEIGHT)];
    
    footerView.backgroundColor = BLACK_HEADER_APP_COLOR;
    
    _planButton = [[UIButton alloc] initWithFrame:CGRectMake(0.f,
                                                             0.f,
                                                             TAB_BAR_HEIGHT * 0.6f,
                                                             TAB_BAR_HEIGHT * 0.6f)];
    
    [_planButton setImage:[UIImage imageNamed:@"icon-footer-plan"] forState:UIControlStateNormal];
    [_planButton setImage:[_planButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    _planButton.tintColor = YELLOW_APP_COLOR;
    _planButton.selected = YES;
    _planButton.tag = 0;
    [_planButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [_planButton setCenter:CGPointMake(WIDTH/8, footerView.frame.size.height * 0.4)];
    [footerView addSubview:_planButton];
    
    _planLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f,
                                                           CGRectGetMaxY(_planButton.frame) + [UtilManager height:5],
                                                           WIDTH/4,
                                                           [UtilManager height:10])];
    
    _planLabel.textAlignment = NSTextAlignmentCenter;
    _planLabel.center = CGPointMake(_planButton.center.x, _planLabel.center.y);
    _planLabel.text = NSLocalizedString(@"Planes", nil);
    _planLabel.font = [UIFont fontWithName:BOLD_FONT size:9];
    _planLabel.textColor = YELLOW_APP_COLOR;
    [footerView addSubview:_planLabel];
    
    _dietButton = [[UIButton alloc] initWithFrame:CGRectMake(0.f,
                                                             0.f,
                                                             CGRectGetWidth(_planButton.frame),
                                                             CGRectGetHeight(_planButton.frame))];
    
    [_dietButton setImage:[UIImage imageNamed:@"icon-footer-diet"] forState:UIControlStateNormal];
    [_dietButton setImage:[_dietButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    _dietButton.tintColor = YELLOW_APP_COLOR;
    _dietButton.tag = 1;
    _dietButton.center = CGPointMake(WIDTH * 3 / 8, _planButton.center.y);
    [_dietButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_dietButton];
    
    _dietLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f,
                                                           CGRectGetMinY(_planLabel.frame),
                                                           WIDTH/4,
                                                           [UtilManager height:10])];
    
    _dietLabel.textAlignment = NSTextAlignmentCenter;
    _dietLabel.center = CGPointMake(_dietButton.center.x, _planLabel.center.y);
    _dietLabel.text = NSLocalizedString(@"Dietas", nil);
    _dietLabel.font = [UIFont fontWithName:BOLD_FONT size:9];
    _dietLabel.textColor = UIColor.whiteColor;
    [footerView addSubview:_dietLabel];
    
    _mealButton = [[UIButton alloc] initWithFrame:CGRectMake(0.f,
                                                             0.f,
                                                             CGRectGetWidth(_planButton.frame),
                                                             CGRectGetHeight(_planButton.frame))];
    
    [_mealButton setImage:[UIImage imageNamed:@"icon-footer-food"] forState:UIControlStateNormal];
    [_mealButton setImage:[_mealButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    _mealButton.tintColor = YELLOW_APP_COLOR;
    _mealButton.center = CGPointMake(WIDTH * 5 / 8, _planButton.center.y);
    _mealButton.tag = 2;
    _mealButton.titleLabel.font = _planButton.titleLabel.font;
    [_mealButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_mealButton];
    
    _mealLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f,
                                                           CGRectGetMinY(_planLabel.frame),
                                                           WIDTH/4,
                                                           [UtilManager height:10])];
    
    _mealLabel.textAlignment = NSTextAlignmentCenter;
    _mealLabel.center = CGPointMake(_mealButton.center.x, _planLabel.center.y);
    _mealLabel.text = NSLocalizedString(@"Comidas", nil);
    _mealLabel.font = [UIFont fontWithName:BOLD_FONT size:9];
    _mealLabel.textColor = UIColor.whiteColor;
    [footerView addSubview:_mealLabel];
    
    _alimentButton = [[UIButton alloc] initWithFrame:CGRectMake(0.f,
                                                                0.f,
                                                                CGRectGetWidth(_planButton.frame),
                                                                CGRectGetHeight(_planButton.frame))];
    
    [_alimentButton setImage:[UIImage imageNamed:@"icon-footer-aliment"] forState:UIControlStateNormal];
    [_alimentButton setImage:[_alimentButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    _alimentButton.tintColor = YELLOW_APP_COLOR;
    _alimentButton.center = CGPointMake(WIDTH * 7 / 8, _planButton.center.y);
    _alimentButton.tag = 3;
    _alimentButton.titleLabel.font = _planButton.titleLabel.font;
    [_alimentButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_alimentButton];
    
    _alimentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f,
                                                              CGRectGetMinY(_planLabel.frame),
                                                              WIDTH/4,
                                                              [UtilManager height:10])];
    
    _alimentLabel.textAlignment = NSTextAlignmentCenter;
    _alimentLabel.center = CGPointMake(_alimentButton.center.x, _planLabel.center.y);
    _alimentLabel.text = NSLocalizedString(@"Alimentos", nil);
    _alimentLabel.font = [UIFont fontWithName:BOLD_FONT size:9];
    _alimentLabel.textColor = UIColor.whiteColor;
    [footerView addSubview:_alimentLabel];
    
    return footerView;
}

- (void)headerAction:(id)sender {
    UIButton *touchButton = (UIButton *)sender;
    
    _profileButton.selected = NO;
    _favButton.selected = NO;
    _downloadButton.selected = NO;
    _comunityButton.selected = NO;
    
    switch (touchButton.tag) {
        case 0:
            _mealSelectType = MealSelectTypeUser;
            _dietSelectType = DietSelectTypeUser;
            _nutritionPlanSelectType = NutritionPlanSelectTypeUser;
            _profileButton.selected = YES;
            break;
        case 1:
            _mealSelectType = MealSelectTypeFavourite;
            _dietSelectType = DietSelectTypeFavourite;
            _nutritionPlanSelectType = NutritionPlanSelectTypeFavourite;
            _favButton.selected = YES;
            break;
        case 2:
            _mealSelectType = MealSelectTypeAdmin;
            _dietSelectType = DietSelectTypeAdmin;
            _nutritionPlanSelectType = NutritionPlanSelectTypeAdmin;
            _comunityButton.selected = YES;
            break;
        case 3:
            _mealSelectType = MealSelectTypeShared;
            _dietSelectType = DietSelectTypeShared;
            _nutritionPlanSelectType = NutritionPlanSelectTypeShared;
            _downloadButton.selected = YES;
            break;
        default:
            break;
    }
    
    [_executive loadItemsForFoodSelectType:_selectType
                            mealSelectType:_mealSelectType
                            dietSelectType:_dietSelectType
                   nutritionPlanSelectType:_nutritionPlanSelectType
                       alimentSegmentState:_segmentState
                                searchText:_keyword];
}

- (void)footerAction:(id)sender {
    
    UIButton *touchButton = (UIButton *)sender;

    if (touchButton.tag == _selectType) {
        return;
    }
    
    _keyword = @"";
    
    switch (touchButton.tag) {
        case 0:
            _selectType = FoodSelectTypePlan;
            break;
        case 1:
            _selectType = FoodSelectTyeDiet;
            break;
        case 2:
            _selectType = FoodSelectTypeMeal;
            break;
        case 3:
            _selectType = FoodSelectTypeAliment;
            break;
        default:
            break;
    }
    
    [_executive loadItemsForFoodSelectType:_selectType
                            mealSelectType:_mealSelectType
                            dietSelectType:_dietSelectType
                   nutritionPlanSelectType:_nutritionPlanSelectType
                       alimentSegmentState:_segmentState
                                searchText:_keyword];
}

- (void)changeAlimentsView:(UISegmentedControl *)segmentControl {
    UISegmentedControl *currentSegmentControl = segmentControl;
    
    _segmentState = currentSegmentControl.selectedSegmentIndex;
    
    [_executive loadItemsForFoodSelectType:_selectType
                            mealSelectType:_mealSelectType
                            dietSelectType:_dietSelectType
                   nutritionPlanSelectType:_nutritionPlanSelectType
                       alimentSegmentState:_segmentState
                                searchText:_keyword];
}

- (void)didPressAddButton {
    switch (_selectType) {
        case FoodSelectTypeMeal:
            [self presentMealAddViewController];
            break;
        case FoodSelectTyeDiet:
            [self presentDietAddViewController];
            break;
        case FoodSelectTypePlan:
            [self presentNutritionPlanAddViewController];
            break;
        case FoodSelectTypeAliment:
            [self presentAlimentAddViewController];
            break;
        default:
            break;
    }
}

- (void)didPressSearchButton {
    if (_menuContainerNavController.searchContainerView.isHidden) {
        [_menuContainerNavController showSearchBar:YES];
    }
    else {
        [_menuContainerNavController showSearchBar:NO];
    }
}

- (void)presentMealAddViewController {
    MealAddViewController *mealAddViewController = [[MealAddViewController alloc] init];
    [mealAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [mealAddViewController setFoodViewController:self];
    
    [self presentViewController:mealAddViewController animated:YES completion:nil];
}

- (void)presentDietAddViewController{
    DietAddViewController *dietAddViewController = [[DietAddViewController alloc] init];
    [dietAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [dietAddViewController setFoodViewController:self];
    
    [self presentViewController:dietAddViewController animated:YES completion:nil];
}

- (void)presentNutritionPlanAddViewController {
    NutritionPlanAddViewController *nutritionPlanAddViewController = [[NutritionPlanAddViewController alloc] init];
    [nutritionPlanAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [nutritionPlanAddViewController setFoodViewController:self];
    
    [self presentViewController:nutritionPlanAddViewController animated:YES completion:nil];
}

- (void)presentAlimentAddViewController {
    UIStoryboard * alimentAddStoryboard = [UIStoryboard storyboardWithName:@"AlimentAdd" bundle:nil];
    AlimentAddViewController * alimentAddViewController = [alimentAddStoryboard instantiateInitialViewController];

    if (_segmentState == 0) {
        alimentAddViewController.isSupplement = NO;
    }
    else {
        alimentAddViewController.isSupplement = YES;
    }
    
    [alimentAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [alimentAddViewController setFoodViewController:self];

    [self presentViewController:alimentAddViewController animated:YES completion:nil];
}

#pragma mark UITableviewDataSource Methods

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
        return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(_selectType == FoodSelectTypeAliment) {
        NSString *alphabet = [_foodIndex objectAtIndex:section];
        NSPredicate *predicate =
        [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", alphabet];
        
        NSMutableArray *raceNameList = [[NSMutableArray alloc] init];
        for (int index = 0; index < _items.count; index++) {
            Aliment *aliment = [_items objectAtIndex:index];
            [raceNameList addObject:aliment.title ? aliment.title : @""];
        }
        
//        NSArray *racesList = [raceNameList filteredArrayUsingPredicate:predicate];
        
        //NSNumber *numberOfRows = [NSNumber numberWithInteger:[racesList count]] ;
        
        //[sectionIndex addObject:numberOfRows];
        //return [racesList  count];
        
        return _items.count;
        
    }
    
    return _items.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
        return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (_selectType) {
        case FoodSelectTypePlan:
            return planFooodCellHeight;
            break;
        case FoodSelectTyeDiet:
            return dietCellHeight;
            break;
        case FoodSelectTypeMeal:
            return foodCellHeight;
            break;
        case FoodSelectTypeAliment:
            return alimentCellHeigtht;
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (_selectType) {
        case FoodSelectTypePlan:
            return planFooodCellHeight;
            break;
        case FoodSelectTyeDiet:
            return dietCellHeight;
            break;
        case FoodSelectTypeMeal:
            return foodCellHeight;
            break;
        case FoodSelectTypeAliment:
            return alimentCellHeigtht;
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (_selectType) {
        case FoodSelectTypePlan:{
            NutritionPlan *nutritionPlan  = [_items objectAtIndex:indexPath.row];
            
            PlanFoodViewCell *cell = [_foodTableView dequeueReusableCellWithIdentifier:@"planFoodCell"];

            if (nutritionPlan.isFavourite != nil &&
               nutritionPlan.isFavourite.boolValue) {
                
                NSMutableAttributedString *myString=[[NSMutableAttributedString alloc] initWithString:nutritionPlan.title
                                                                                           attributes:@{
                                                                                                        NSForegroundColorAttributeName:YELLOW_APP_COLOR,
                                                                                                        NSFontAttributeName : [UIFont fontWithName:BOLD_FONT size:12]
                                                                                                        }
                                                     ];
                
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                attachment.image = [UIImage imageNamed:@"icon-fav-on"];
                attachment.bounds = CGRectMake(5., -2., cell.titleLabel.frame.size.height, cell.titleLabel.frame.size.height);
                NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
                [myString appendAttributedString:attachmentString];
                cell.titleLabel.attributedText = myString;
            }
            else {
                cell.titleLabel.text = nutritionPlan.title;
            }
            
            cell.contentLabel.text = nutritionPlan.content;
            if (nutritionPlan.nutritionDays.count > 0) {
                cell.daysLabel.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)nutritionPlan.nutritionDays.count, NSLocalizedString(@"días", nil)];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.exercisesLabel.text =  [NSString stringWithFormat:@"%d %@",(int)nutritionPlan.nutritionDays.count, NSLocalizedString(@"dietas", nil)];
            
//            NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:trainingPlan.tags.count];
            
//            for(Tag *tag in trainingPlan.tags){
//                [tags addObject:tag];
//            }
            
//            [[cell tagView] setTags:tags];
//            [[cell tagView] reloadTagSubviews];
            
            [cell setDelegate:self];
            
            [cell setCurrentIndex:indexPath.row];
            
            return cell;
//            [[cell titleLabel] setText:nutritionPlan.title];
//            [[cell contentLabel] setText:nutritionPlan.content];
//            [[cell daysLabel] setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)nutritionPlan.nutritionDays.count,NSLocalizedString(@"días", nil)]];
//
//            [cell setDelegate:self];
//            [cell setCurrentIndex:indexPath.row];
//
//            return cell;
        }
        break;
        case FoodSelectTyeDiet:
        {
            DietCell *cell = [_foodTableView dequeueReusableCellWithIdentifier:@"dietCell"];
            
            Diet *diet = [_items objectAtIndex:indexPath.row];
            if(diet.isFavourite.boolValue){
                NSMutableAttributedString *myString=[[NSMutableAttributedString alloc] initWithString:diet.title
                                                                                           attributes:@{
                                                                                                        NSForegroundColorAttributeName:YELLOW_APP_COLOR,
                                                                                                        NSFontAttributeName : [UIFont fontWithName:SEMIBOLD_FONT size:15]
                                                                                                        }
                                                     ];
                
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                attachment.image = [UIImage imageNamed:@"icon-fav-on"];
                attachment.bounds = CGRectMake(5., -2., cell.titleLabel.frame.size.height, cell.titleLabel.frame.size.height);
                NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
                [myString appendAttributedString:attachmentString];
                cell.titleLabel.attributedText = myString;
            }else
                [[cell titleLabel] setText:diet.title];
            [cell setCurrentIndex:indexPath.row];
            [cell setDelegate:self];
            
            [[cell kCalLabel] setText:[NSString stringWithFormat:@"%d kCals", (int)[Diet getKCalInDiet:diet]]];
            [[[cell proteinCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",[Diet getDietProtein:diet]]];
            [[[cell carbsCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",[Diet getDietCarb:diet]]];
            [[[cell fatsCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",[Diet getDietFats:diet]]];
            
            return cell;
        }
            break;
        case FoodSelectTypeMeal: {
            MealCell *cell = [_foodTableView dequeueReusableCellWithIdentifier:@"foodCell"];
            
            [cell setDelegate:self];
            
            Meal *meal = [_items objectAtIndex:indexPath.row];
            
            if(meal.isFavourite.boolValue){
                NSMutableAttributedString *myString=[[NSMutableAttributedString alloc] initWithString:meal.title
                                                                                           attributes:@{
                                                                                                        NSForegroundColorAttributeName:YELLOW_APP_COLOR,
                                                                                                        NSFontAttributeName : [UIFont fontWithName:SEMIBOLD_FONT size:15]
                                                                                                        }
                                                     ];
                
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                attachment.image = [UIImage imageNamed:@"icon-fav-on"];
                attachment.bounds = CGRectMake(5., -2., cell.titleLabel.frame.size.height, cell.titleLabel.frame.size.height);
                NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
                [myString appendAttributedString:attachmentString];
                cell.titleLabel.attributedText = myString;
            }
            else {
                [[cell titleLabel] setText:meal.title];
            }
            
            
            UIImageView *imageView = cell.foodImageView;
            
            NSURLRequest *foodImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_MEAL,meal.image]]
                                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                          timeoutInterval:60];
            
            [cell.imageView setImageWithURLRequest:foodImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                [imageView setImage:image];
                [imageView setContentMode:UIViewContentModeScaleAspectFit];
                [[imageView layer] setMasksToBounds:YES];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                NSLog(@"%@",error);
                [imageView setImage:[UIImage imageNamed:@"img-placeholder"]];
            }];
            
            NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:meal.tags.count];
            
            for(Tag *tag in meal.tags){
                [tags addObject:tag];
            }
            
            [[cell tagView] setTags:tags];
            [[cell tagView] reloadTagSubviews];
            [[cell tagView] setHidden:FALSE];
            
            [[cell kCalLabel] setText:[NSString stringWithFormat:@"%d kCals", (int)[Meal getKCalInMeal:meal]]];
            [[[cell proteinCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",[Meal getMealProtein:meal]]];
            [[[cell carbsCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",[Meal getMealCarb:meal]]];
            [[[cell fatsCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",[Meal getMealFats:meal]]];
            
            [cell setCurrentIndex:indexPath.row];
            
            return cell;
        }
            break;
        case FoodSelectTypeAliment:
        {
            AlimentCell *cell = [_foodTableView dequeueReusableCellWithIdentifier:@"alimentCell"];
            cell.tagView.hidden = NO;
            cell.tagView.userInteractionEnabled = NO;
            
            //NSString *alphabet = [_foodIndex objectAtIndex:[indexPath  section]];
            
            //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", alphabet];
            
            //NSMutableArray *countryNameList = [[NSMutableArray alloc] init];
            //for(int i=0;i<[_items count];i++)
            //{
            Aliment *aliment = [_items objectAtIndex:indexPath.row];
            //    [countryNameList addObject:aliment.title];
            //}
            
            //NSArray *racesList =[countryNameList filteredArrayUsingPredicate:predicate];
            
            //if([racesList count]>0)
            //{
            //    NSString *currentAliment = [racesList objectAtIndex:indexPath.row];
            
            
        //    Aliment *aliment = [Aliment getAlimentWithName:currentAliment];
            [[cell titleLabel] setText:aliment.title];
            
            UIImageView *imageView = cell.foodImageView;
            
            NSURLRequest *foodImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_FOOD,aliment.image]]
                                                                   cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                               timeoutInterval:60];
            
            [cell.imageView setImageWithURLRequest:foodImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                [imageView setImage:image];
                [imageView setContentMode:UIViewContentModeScaleAspectFit];
                [[imageView layer] setMasksToBounds:YES];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                 NSLog(@"%@",error);
                [imageView setImage:[UIImage imageNamed:@"img-placeholder"]];
            }];
            
            NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
            
            Tag * alimentTag;
            if (aliment.tag1) {
                alimentTag = [Tag tagWithTitle:aliment.tag1];
                if (alimentTag) {
                    [tags addObject:alimentTag];
                }
            }
            
            if (aliment.tag2) {
                alimentTag = [Tag tagWithTitle:aliment.tag2];
                if (alimentTag) {
                    [tags addObject:alimentTag];
                }
            }
            
            if (aliment.group) {
                alimentTag = [Tag tagWithTitle:aliment.group];
                if (alimentTag) {
                    [tags addObject:alimentTag];
                }
            }
            
            cell.tagView.tags = tags;
            [cell.tagView reloadTagSubviews];
            
            cell.kCalLabel.text = [NSString stringWithFormat:@"%ld kCals", (long)aliment.calories.intValue];
            cell.proteinCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g", (long)aliment.proteins.intValue];
            cell.carbsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g", (long)aliment.carbs.intValue];
            cell.fatsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g", (long)aliment.fats.intValue];
            cell.weightLabel.text = [NSString stringWithFormat:@"100 %@", aliment.measureUnit];
            cell.delegate = self;
            cell.currentIndex = indexPath.row;
            
            return cell;
        }
            break;
        default:
            return nil;
            break;
    }
    
    return nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_selectType == FoodSelectTypePlan && _nutritionPlanSelectType == NutritionPlanSelectTypeUser) {
        return UITableViewCellEditingStyleDelete;
    }
    
    if (_selectType == FoodSelectTypeMeal && _mealSelectType == MealSelectTypeUser) {
        return UITableViewCellEditingStyleDelete;
    }
    
    if (_selectType == FoodSelectTyeDiet && _mealSelectType == DietSelectTypeUser) {
        return UITableViewCellEditingStyleDelete;
    }
    
    if (_selectType == FoodSelectTypeAliment) {
        return UITableViewCellEditingStyleDelete;
    }
    
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        switch (_selectType) {
            case FoodSelectTypeMeal: {
                Meal * meal = [_items objectAtIndex:indexPath.row];
                [_executive deleteMeal:meal];
            }
                break;
            case FoodSelectTyeDiet: {
                Diet * diet = [_items objectAtIndex:indexPath.row];
                [_executive deleteDiet:diet];
            }
                break;
            case FoodSelectTypePlan: {
                NutritionPlan * nutritionPlan = [_items objectAtIndex:indexPath.row];
                [_executive deleteNutritionPlan:nutritionPlan];
            }
                break;
            case FoodSelectTypeAliment: {
                Aliment * aliment = [_items objectAtIndex:indexPath.row];
                [_executive deleteAliment:aliment];
            }
                break;
                
            default:
                break;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    /*
     if(_selectType == FoodSelectTypeAliment)
     return [UtilManager height:60];
     else
     return TAB_BAR_HEIGHT;
     */
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    /*
    if(_selectType == FoodSelectTypeAliment)
        return [UtilManager height:60];
    else
        return TAB_BAR_HEIGHT;
     */
    return 0;
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    _selectedFoodIndex = indexPath.row;
    
    if(_selectType == FoodSelectTypePlan){
        NutritionPlan *nutritionPlan = [_items objectAtIndex:indexPath.row];
    }
    if(_selectType == FoodSelectTyeDiet){
        Diet *diet = [_items objectAtIndex:indexPath.row];
        
        DietDetailViewController *dietDetailViewcontroller = [[DietDetailViewController alloc] init];
        [dietDetailViewcontroller setCurrentDiet:diet];
        [dietDetailViewcontroller setOptionMenuDelegate:self];
        [dietDetailViewcontroller setFoodRootViewController:self];
        
        [self.navigationController pushViewController:dietDetailViewcontroller animated:YES];
        
    }
    if(_selectType == FoodSelectTypeMeal){
        Meal *meal = [_items objectAtIndex:indexPath.row];
        
        MealDetailViewController *mealDetailViewController = [[MealDetailViewController alloc] init];
        [mealDetailViewController setCurrentMeal:meal];
        [self.navigationController pushViewController:mealDetailViewController animated:YES];
    } else if (_selectType == FoodSelectTypeAliment){
        
        //NSString *alphabet = [_foodIndex objectAtIndex:[indexPath  section]];
        
        //NSPredicate *predicate =
        //[NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", alphabet];
        
        //NSMutableArray *countryNameList = [[NSMutableArray alloc] init];
        //for(int i=0;i<[_items count];i++)
        //{
            Aliment *aliment = [_items objectAtIndex:indexPath.row];
        //    [countryNameList addObject:aliment.title];
        //}
        
        //NSArray *racesList =[countryNameList filteredArrayUsingPredicate:predicate];
        //NSString *currentAliment;
        
        //if([racesList count]>0){
            //currentAliment = [racesList objectAtIndex:indexPath.row];
            
            //Aliment *aliment = [Aliment getAlimentWithName:currentAliment];
            
            FoodDetailView *foodDetailView = [[FoodDetailView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:20], WIDTH - [UtilManager width:20], HEIGHT - [UtilManager height:40]) andAliment:aliment];
            
            _modalView = [[RNBlurModalView alloc] initWithView:foodDetailView];
            [_modalView setUserInteractionEnabled:YES];
            
            //RNBlurModalView *modalView = [[RNBlurModalView alloc] initWithTitle:food.title message:food.content];
            
            [_modalView show];
        //}
        
        
    }
    else if (_selectType == FoodSelectTypePlan) {
        NutritionPlan *nutritionPlan = [_items objectAtIndex:indexPath.row];
        NutritionPlanDetailViewController * nutritionPlanDetailViewController = [[NutritionPlanDetailViewController alloc] init];
        nutritionPlanDetailViewController.currentNutritionPlan = nutritionPlan;
        [self.navigationController pushViewController:nutritionPlanDetailViewController animated:YES];
    }
    
    [_foodTableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark AlimentCellDelegate

- (void)alimentCellActionInIndex:(NSInteger)index {
    Aliment * aliment = [_items objectAtIndex:index];
    
    OptionsFoodViewController *optionsViewController = [[OptionsFoodViewController alloc] init];
    [optionsViewController setCurrentAliment:aliment];
    [optionsViewController setOptionFoodType:OptionFoodTypeAliment];
    [optionsViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [optionsViewController setDelegate:self];
    [self presentViewController:optionsViewController animated:YES completion:nil];
}

#pragma mark FoodCellDelegate Methods

- (void)mealCellActionInIndex:(NSInteger)index {
    Meal *meal = [_items objectAtIndex:index];
    
    OptionsFoodViewController *optionsViewController = [[OptionsFoodViewController alloc] init];
    optionsViewController.currentMeal = meal;
    optionsViewController.optionFoodType = OptionFoodTypeMeal;
    optionsViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsViewController.delegate = self;
    [self presentViewController:optionsViewController animated:YES completion:nil];
}

- (void)mealCellDetailInfoActionInIndex:(NSInteger)index {
    MealDetailInfoView * mealDetailView = [[MealDetailInfoView alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                                               [UtilManager height:20],
                                                                                               WIDTH - [UtilManager width:20],
                                                                                               HEIGHT - [UtilManager height:40])
                                                                            andMeal:_items[index]
                                                                         isEditable:NO];
    
    _modalView = [[RNBlurModalView alloc] initWithView:mealDetailView];
    [_modalView setUserInteractionEnabled:YES];
    [_modalView show];
}

#pragma mark DietCellDelegateMethods

- (void)dietCellActionInIndex:(NSInteger)index {
    
    Diet *diet = [_items objectAtIndex:index];
    
    OptionsFoodViewController *optionsViewController = [[OptionsFoodViewController alloc] init];
    [optionsViewController setCurrentDiet:diet];
    [optionsViewController setOptionFoodType:OptionFoodTypeDiet];
    [optionsViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [optionsViewController setDelegate:self];
    [self presentViewController:optionsViewController animated:YES completion:nil];
}

- (void)dietCellDetailInfoActionInIndex:(NSInteger)index {
    DietDetailInfoView * dietDetailView = [[DietDetailInfoView alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                                               [UtilManager height:20],
                                                                                               WIDTH - [UtilManager width:20],
                                                                                               HEIGHT - [UtilManager height:40])
                                                                            andDiet:_items[index]];
    
    _modalView = [[RNBlurModalView alloc] initWithView:dietDetailView];
    [_modalView setUserInteractionEnabled:YES];
    [_modalView show];
}

#pragma mark NutritionPlanCellDelegate Methods

- (void)nutritionPlanDidPushInIndex:(NSInteger)index{
    
    NutritionPlan *nutritionPlan = [_items objectAtIndex:index];
    
    OptionsFoodViewController *optionsViewController = [[OptionsFoodViewController alloc] init];
    optionsViewController.currentNutritionPlan = nutritionPlan;
    optionsViewController.optionFoodType = OptionFoodTypeNutritionPlan;
    optionsViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsViewController.delegate = self;
    [self presentViewController:optionsViewController animated:YES completion:nil];
    
    /*
    
    
    NutritionPlanAddViewController *nutritionPlanAddViewController = [[NutritionPlanAddViewController alloc] init];
    [nutritionPlanAddViewController setEditNutritionPlan:nutritionPlan];
    [nutritionPlanAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [nutritionPlanAddViewController setFoodViewController:self];
    
    [self presentViewController:nutritionPlanAddViewController animated:YES completion:^{
    
    }];
     */
}

- (void)nutritionPlanCellDetailInfoActionInIndex:(NSInteger)index {
    NutritionPlanDetailinfoView * mealDetailView = [[NutritionPlanDetailinfoView alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                                               [UtilManager height:20],
                                                                                               WIDTH - [UtilManager width:20],
                                                                                               HEIGHT - [UtilManager height:40])
                                                                                     andNutritionPlan:_items[index]];
    
    _modalView = [[RNBlurModalView alloc] initWithView:mealDetailView];
    [_modalView setUserInteractionEnabled:YES];
    [_modalView show];
}

#pragma mark OptionsFoodViewController Delegate

- (void)didPressAddToCalendar:(RLMObject *)rlmObject {
    if (![rlmObject isKindOfClass:[NutritionPlan class]]) {
        return;
    }
    
    [self.view endEditing:YES];
    
    if(_datePicker) {
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - 216, self.view.frame.size.width, 216)];
    _datePicker.datePickerMode = UIDatePickerModeDate;

    [_datePicker setDate:[NSDate date]];
    
    _datePicker.backgroundColor = [UIColor whiteColor];
    _datePicker.userInteractionEnabled = YES;
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_datePicker.frame.size.width - 80., _datePicker.frame.origin.y, 80., 24.)];
    [_acceptButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptDatePickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_datePicker];
    [self.view addSubview:_acceptButton];
}

- (void)acceptDatePickerAction {
    if(_acceptButton && _datePicker) { 
        NutritionPlan * nutritionPlan = _items[_selectedFoodIndex];
        [_executive addNutritionPlan:nutritionPlan
                              toDate:_datePicker.date];
        
        [_acceptButton removeFromSuperview];
        _acceptButton = nil;
        
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
}

- (void)didPressDeleteFood:(RLMObject*)rlmObject {
    if ([rlmObject isKindOfClass:[Diet class]]) {
        Diet * diet = (Diet*)rlmObject;
        [_executive deleteDiet:diet];
    }
    else if ([rlmObject isKindOfClass:[Meal class]]) {
        Meal * meal = (Meal*)rlmObject;
        [_executive deleteMeal:meal];
    }
    else if ([rlmObject isKindOfClass:[Aliment class]]) {
        Aliment * aliment = (Aliment*)rlmObject;
        [_executive deleteAliment:aliment];
    }
}

- (void)didPressEditFood:(RLMObject *)rlmObject {
    if ([rlmObject isKindOfClass:[NutritionPlan class]]) {
        NutritionPlan * nutritionPlan = (NutritionPlan*)rlmObject;
        
        NutritionPlanAddViewController *nutritionPlanAddViewController = [[NutritionPlanAddViewController alloc] init];
        
        nutritionPlanAddViewController.editNutritionPlan = nutritionPlan;
        nutritionPlanAddViewController.modalTransitionStyle =  UIModalTransitionStyleCoverVertical;
        nutritionPlanAddViewController.foodViewController = self;
        
        [self presentViewController:nutritionPlanAddViewController animated:YES completion:nil];
    }
    
    if ([rlmObject isKindOfClass:[Diet class]]) {
        Diet * diet = (Diet*)rlmObject;
        
        DietAddViewController *dietAddViewController = [[DietAddViewController alloc] init];
        dietAddViewController.editDiet = diet;
        dietAddViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        dietAddViewController.foodViewController = self;
        
        [self presentViewController:dietAddViewController animated:YES completion:nil];
    }
    
    if ([rlmObject isKindOfClass:[Meal class]]) {
        Meal * meal = (Meal*)rlmObject;
        
        MealAddViewController *mealAddViewController = [[MealAddViewController alloc] init];
        mealAddViewController.editMeal = meal;
        mealAddViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        mealAddViewController.foodViewController = self;
        
        [self presentViewController:mealAddViewController animated:YES completion:nil];
    }
    
    if ([rlmObject isKindOfClass:[Aliment class]]) {
        Aliment * aliment = (Aliment*)rlmObject;
        
        UIStoryboard * alimentAddStoryboard = [UIStoryboard storyboardWithName:@"AlimentAdd" bundle:nil];
        AlimentAddViewController * alimentAddViewController = [alimentAddStoryboard instantiateInitialViewController];
        alimentAddViewController.editAliment = aliment;
        
        if (_segmentState == 0) {
            alimentAddViewController.isSupplement = NO;
        }
        else {
            alimentAddViewController.isSupplement = YES;
        }
        
        [alimentAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        [alimentAddViewController setFoodViewController:self];
        
        [self presentViewController:alimentAddViewController animated:YES completion:nil];
    }
}

- (void)didPressFusionFood:(RLMObject*)rlmObject {
    Meal * selectedMeal = (Meal*)rlmObject;
    
    RLMRealm *realm = [AppContext currentRealm];
    if (selectedMeal.mergedMeals &&
        selectedMeal.mergedMeals.count > 0) {
        
        [realm transactionWithBlock:^{
            for (Meal * meal in selectedMeal.mergedMeals) {
                meal.childOfMergedMeal = @NO;
            }
        }];
        
        [Meal deleteMeal:selectedMeal];
        [Meal saveMergedMealIdsToPreference];
        
        [_executive loadItemsForFoodSelectType:_selectType
                                mealSelectType:_mealSelectType
                                dietSelectType:_dietSelectType
                       nutritionPlanSelectType:_nutritionPlanSelectType
                           alimentSegmentState:_segmentState
                                    searchText:_keyword];
        
        return;
    }

    _selectorFoodViewController = [[SelectorFoodViewController alloc] init];
    _selectorFoodViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    _selectorFoodViewController.selectorType = SelectorFoodTypeDietSingle;
    _selectorFoodViewController.selectMeal = selectedMeal;
    _selectorFoodViewController.selectedFoodIndex = _selectedFoodIndex;
    _selectorFoodViewController.delegate = self;
    
    [self presentViewController:_selectorFoodViewController animated:YES completion:nil];
}

- (void)didPressSendFood:(RLMObject *)rlmObject {
    _userSelectorViewController = [[UserSelectorViewController alloc] init];
    _userSelectorViewController.rlmObject = rlmObject;
    _userSelectorViewController.delegate = self;
    _userSelectorViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:_userSelectorViewController animated:YES completion:nil];
}

- (void)didPressShareFood:(RLMObject *)rlmObject {
    [_executive shareContent:rlmObject];
}

#pragma mark - Food Display Delegate

- (void)didLoadItems:(RLMResults*)items
         forFoodType:(FoodSelectType)foodType {
    
    _planButton.selected = NO;
    _dietButton.selected = NO;
    _mealButton.selected = NO;
    _alimentButton.selected = NO;
    
    _planLabel.textColor = UIColor.whiteColor;
    _dietLabel.textColor = UIColor.whiteColor;
    _mealLabel.textColor = UIColor.whiteColor;
    _alimentLabel.textColor = UIColor.whiteColor;
    
    switch (foodType) {
        case FoodSelectTypePlan:
            _menuContainerNavController.titleLabel.text = NSLocalizedString(@"PLANES DE NUTRICIÓN", nil);
            _planButton.selected = YES;
            _headerView.hidden = NO;
            _segmentControlView.hidden = YES;
            _planLabel.textColor = YELLOW_APP_COLOR;
            if(items.count == 0) {
                _warningLabel.text = NSLocalizedString(@"Todavía no has creado ningún plan nutricional", nil);
            }
            else {
                _warningLabel.text = NSLocalizedString(@"", nil);
            }
            break;
        case FoodSelectTyeDiet:
            _menuContainerNavController.titleLabel.text = NSLocalizedString(@"DIETAS", nil);
            _dietButton.selected = YES;
            _headerView.hidden = NO;
            _segmentControlView.hidden = YES;
            _dietLabel.textColor = YELLOW_APP_COLOR;
            if(items.count == 0) {
                _warningLabel.text = NSLocalizedString(@"Todavía no hay ninguna dieta", nil);
            }
            else {
                _warningLabel.text = NSLocalizedString(@"", nil);
            }
            break;
        case FoodSelectTypeMeal:
            _menuContainerNavController.titleLabel.text = NSLocalizedString(@"COMIDAS", nil);
            _mealButton.selected = YES;
            _headerView.hidden = NO;
            _segmentControlView.hidden = YES;
            _mealLabel.textColor = YELLOW_APP_COLOR;
            if(items.count == 0) {
                _warningLabel.text = NSLocalizedString(@"Todavía no hay ninguna commida", nil);
            }
            else {
                _warningLabel.text = NSLocalizedString(@"", nil);
            }
            break;
        case FoodSelectTypeAliment:
            _menuContainerNavController.titleLabel.text = NSLocalizedString(@"ALIMENTOS", nil);
            _alimentButton.selected = YES;
            _headerView.hidden = YES;
            _segmentControlView.hidden = NO;
            _alimentLabel.textColor = YELLOW_APP_COLOR;
            if(items.count == 0) {
                [_warningLabel setText:NSLocalizedString(@"Todavía no hay alimentos de este tipo", nil)];
            }
            else {
                [_warningLabel setText:NSLocalizedString(@"", nil)];
            }
            
            for (Aliment * aliment in _items) {
                //---get the first char of each state---
                char alphabet;
                if (aliment.title.length > 0) {
                    alphabet = [aliment.title characterAtIndex:0];
                }
                else {
                    alphabet = 70;
                }
                NSString *uniChar = [NSString stringWithFormat:@"%c", alphabet];
                
                if(!_foodIndex) {
                    _foodIndex = [[NSMutableArray alloc] init];
                }
                //---add each letter to the index array---
                if (![_foodIndex containsObject:uniChar]) {
                    [_foodIndex addObject:uniChar];
                }
            }
            break;
        default:
            break;
    }
    
    _items = items;
    [_foodTableView reloadData];
}

- (void)reloadItems {
    [WebServiceManager getAlimentsWithCompletionBlock:^(BOOL isSuccess) {
        [WebServiceManager getMealsWithCompletionBlock:^(BOOL isSuccess) {
            [WebServiceManager getDietsWithWithCompletionBlock:^(BOOL isSuccess) {
                [WebServiceManager getNutrionalPlansWithCompletionBlock:^(BOOL isSuccess) {
                    
                    [_executive loadItemsForFoodSelectType:_selectType
                                            mealSelectType:_mealSelectType
                                            dietSelectType:_dietSelectType
                                   nutritionPlanSelectType:_nutritionPlanSelectType
                                       alimentSegmentState:_segmentState
                                                searchText:_keyword];
                }];
            }];
        }];
    }];
}

- (void)didDeletedNutritionPlan:(NutritionPlan*)nutritionPlan {
    [WebServiceManager getNutrionalPlansWithCompletionBlock:^(BOOL isSuccess) {
        [_executive loadItemsForFoodSelectType:_selectType
                                mealSelectType:_mealSelectType
                                dietSelectType:_dietSelectType
                       nutritionPlanSelectType:_nutritionPlanSelectType
                           alimentSegmentState:_segmentState
                                    searchText:_keyword];
    }];
}

- (void)didDeletedMeal:(Meal*)meal {
    [WebServiceManager getMealsWithCompletionBlock:^(BOOL isSuccess) {
        [_executive loadItemsForFoodSelectType:_selectType
                                mealSelectType:_mealSelectType
                                dietSelectType:_dietSelectType
                       nutritionPlanSelectType:_nutritionPlanSelectType
                           alimentSegmentState:_segmentState
                                    searchText:_keyword];
    }];
}

- (void)didDeleteDiet:(Diet*)diet {
    [WebServiceManager getDietsWithWithCompletionBlock:^(BOOL isSuccess) {
        [_executive loadItemsForFoodSelectType:_selectType
                                mealSelectType:_mealSelectType
                                dietSelectType:_dietSelectType
                       nutritionPlanSelectType:_nutritionPlanSelectType
                           alimentSegmentState:_segmentState
                                    searchText:_keyword];
    }];
}

- (void)didDeleteAliment:(Aliment*)aliment {
    [WebServiceManager getAlimentsWithCompletionBlock:^(BOOL isSuccess) {
        [_executive loadItemsForFoodSelectType:_selectType
                                mealSelectType:_mealSelectType
                                dietSelectType:_dietSelectType
                       nutritionPlanSelectType:_nutritionPlanSelectType
                           alimentSegmentState:_segmentState
                                    searchText:_keyword];
    }];
}


#pragma mark - SearchBarDelegate Methods

- (void)didSearchButtonClicked:(UISearchBar*)searchBar {
}

- (void)didSearchBar:(UISearchBar*)searchBar TextChange:(NSString*)searchText {
    _keyword = searchText;
    [_executive loadItemsForFoodSelectType:_selectType
                            mealSelectType:_mealSelectType
                            dietSelectType:_dietSelectType
                   nutritionPlanSelectType:_nutritionPlanSelectType
                       alimentSegmentState:_segmentState
                                searchText:_keyword];
}

#pragma mark - SelectorFoodViewDelegate

- (void)didPressOKForMergeFood:(RLMObject*)rlmObject {
    RLMRealm *realm = [AppContext currentRealm];
    Meal * mergingMeal = (Meal*)rlmObject;
    Meal * selectedMeal = [_items objectAtIndex:_selectedFoodIndex];
    
    Meal * mergedMeal = [Meal mergeMeal:selectedMeal WithMeal:mergingMeal];
    [Meal saveMeal:mergedMeal];
    
    [realm transactionWithBlock:^{
        mergingMeal.childOfMergedMeal = @YES;
        selectedMeal.childOfMergedMeal = @YES;
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [Meal saveMergedMealIdsToPreference];
    
    [_executive loadItemsForFoodSelectType:_selectType
                            mealSelectType:_mealSelectType
                            dietSelectType:_dietSelectType
                   nutritionPlanSelectType:_nutritionPlanSelectType
                       alimentSegmentState:_segmentState
                                searchText:_keyword];
}

#pragma mark - UserSelectorViewController Delegate

- (void)didPressOKWithSelectedResults:(NSArray *)selectedResults
                           andContent:(RLMObject *)content {
    
    UserMeta * userMeta = [selectedResults firstObject];
    [_executive sendContent:content
                    toToken:userMeta.token];
}

@end
