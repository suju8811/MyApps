//
//  PropertyCircleView.h
//  Titan
//
//  Created by Manuel Manzanera on 10/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PropertyCircleView : UIView

@property (nonatomic, strong) UILabel *initialLabel;
@property (nonatomic, strong) UILabel *countLabel;
@property (nonatomic, strong) UILabel *measureLabel;

@end
