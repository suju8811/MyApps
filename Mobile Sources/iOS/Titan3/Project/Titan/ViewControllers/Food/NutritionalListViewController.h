//
//  FoodViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Meal.h"
#import "Diet.h"
#import "NutritionPlan.h"

#import "ParentViewController.h"
#import "FoodViewController.h"

@interface NutritionalListViewController : ParentViewController

@property (nonatomic, assign) FoodSelectType selectType;

@property (nonatomic, assign) MealSelectType mealSelectType;
@property (nonatomic, assign) DietSelectType dietSelectType;
@property (nonatomic, assign) NutritionPlanSelectType nutritionPlanSelectType;

@property (nonatomic, strong) Meal *createMeal;
@property (nonatomic, strong) Diet *createDiet;
@property (nonatomic, strong) NutritionPlan *createNutritionPlan;

@end
