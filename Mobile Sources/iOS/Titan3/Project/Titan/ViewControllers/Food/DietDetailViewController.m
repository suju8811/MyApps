//
//  DietDetailViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 2/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DietDetailViewController.h"
#import "MealDetailViewController.h"
#import "UIImageView+AFNetworking.h"
#import "MBTwitterScroll.h"
#import "MealCell.h"
#import "SelectorFoodViewController.h"
#import "DietDetailExecutive.h"
#import "OptionsFoodViewController.h"
#import "MealAddViewController.h"
#import "DietAddViewController.h"
#import "InfoPopupViewController.h"
#import "MealDetailInfoView.h"
#import "DietDetailInfoView.h"
#import "RNBlurModalView.h"
#import "WebServiceManager.h"
#import "UserSelectorViewController.h"
#import "UserMeta.h"
#import "DateTimeUtil.h"

@interface DietDetailViewController ()<UITableViewDelegate, UITableViewDataSource
, MBTwitterScrollDelegate, DietDetailDisplay, DietDetailViewControllerDelegate, OptionsFoodViewControllerDelegate, MealCellDelegate, MealOperationViewDelegate, SelectorFoodViewDelegate, UserSelectorViewControllerDelegate>

@property (nonatomic, strong) MBTwitterScroll *dietTableView;
@property (nonatomic, strong) SelectorFoodViewController *selectorFoodViewController;
@property (nonatomic, strong) DietDetailExecutive * executive;
@property (nonatomic, assign) NSInteger currentMealIndex;
@property (nonatomic, strong) InfoPopupViewController * infoPopupViewController;
@property (nonatomic, strong) RNBlurModalView *modalView;
@property (nonatomic, strong) UserSelectorViewController * userSelectorViewController;
@property (nonatomic, strong) UIDatePicker * datePicker;
@property (nonatomic, strong) UIButton * acceptButton;

@end

@implementation DietDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureView];
    
    _executive = [[DietDetailExecutive alloc] initWithDisplay:self];
    _currentMealIndex = 0;
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"CustomView" bundle:nil];
    _infoPopupViewController = [storyboard instantiateInitialViewController];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (_selectorFoodViewController.presentDetailForReplacingFood) {
        _selectorFoodViewController.presentDetailForReplacingFood = NO;
        _selectorFoodViewController.presentDietDetail = NO;
        
        Meal * replacingMeal = [_selectorFoodViewController.selectResults firstObject];
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm transactionWithBlock:^{
            [replacingMeal setValue:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
            [_currentDiet.dietMeals[_selectedMealIndex].meals replaceObjectAtIndex:0 withObject:replacingMeal];
        }];
        
        [realm beginWriteTransaction];
        NSError *error;
        [realm commitWriteTransaction:&error];
        if(error){
            NSLog(@"%@",error.description);
        }
        
        [_executive updateDiet:_currentDiet];
    }
    else if(_selectorFoodViewController.presentDietDetail) {
        
        _selectorFoodViewController.presentDietDetail = NO;
        _selectorFoodViewController.presentDetailForReplacingFood = NO;
        
        NSMutableArray * dietMealAux = [[NSMutableArray alloc] init];
        RLMRealm *realm = [AppContext currentRealm];
        NSError *error;
        
        NSInteger order = _currentDiet.dietMeals.count;
        for (Meal *mealBack in _selectorFoodViewController.selectResults) {
            
            [realm transactionWithBlock:^{
                [mealBack setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
            }];
            
            [realm beginWriteTransaction];
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            
            DietMeal * dietMeal = [[DietMeal alloc] init];
            dietMeal.order = @(order);
            dietMeal.dietMealId = [UtilManager uuid];
            [dietMeal.meals addObject:mealBack];
            [dietMealAux addObject:dietMeal];
            order = order + 1;
        }
        
        [realm transactionWithBlock:^{
            [_currentDiet.dietMeals removeAllObjects];
            [_currentDiet.dietMeals addObjects:dietMealAux];
        }];
        
        [realm beginWriteTransaction];
        [realm commitWriteTransaction:&error];
        if (error) {
            NSLog(@"%@", error.localizedDescription);
        }
        
        [_executive updateDiet:_currentDiet];
    }
}

- (void)configureView{
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _dietTableView = [[MBTwitterScroll alloc] initWithType:MBTableDiet];
    
    [_dietTableView setCurrentDiet:_currentDiet];
    
    [_dietTableView setupView];
    [_dietTableView reloadDietValues];
    
    _dietTableView.delegate = self;
    _dietTableView.tableView.delegate = self;
    _dietTableView.tableView.dataSource = self;
    _dietTableView.tableView.bounces = FALSE;
    [_dietTableView.tableView registerClass:[MealCell class] forCellReuseIdentifier:@"mealCell"];
    
    [self.view addSubview:_dietTableView];
    [self.view addSubview:[self footerView]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)footerView{
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.,HEIGHT - [UtilManager height:50], WIDTH, [UtilManager height:50])];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = footerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [footerView addSubview:blurEffectView];
    
    UIButton *moreButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:40] * 0.8, [UtilManager width:40]* 0.8)];
    [moreButton setImage:[UIImage imageNamed:@"btn-add"] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(didPressAddMoreButton) forControlEvents:UIControlEventTouchUpInside];
    [moreButton setCenter:CGPointMake(self.view.center.x, footerView.frame.size.height/2)];
    
    [footerView addSubview:moreButton];
    
    return footerView;
}

- (void)didPressAddMoreButton {
    _selectorFoodViewController = [[SelectorFoodViewController alloc] init];
    [_selectorFoodViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [_selectorFoodViewController setSelectorType:SelectorFoodTypeDiet];
    [_selectorFoodViewController setSelectDiet:_currentDiet];
    [_selectorFoodViewController setPresentMealDetail:TRUE];
    
    [self presentViewController:_selectorFoodViewController animated:YES completion:nil];
}

#pragma mark MBTwitterScrollDelegate Methods

- (void)editButtonDidPress {
    if(_dietTableView.tableView.editing) {
        [_executive updateDiet:_currentDiet];
        [_dietTableView.tableView setEditing:NO animated:YES];
    }
    else {
        [_dietTableView.tableView setEditing:YES animated:YES];
    }
}

- (void)didPressShowInfoButton {
    _infoPopupViewController.rlmObject = _currentDiet;
    [self.view addSubview:_infoPopupViewController.view];
}

- (void)didPressShowDetailInfoButton {
    DietDetailInfoView * dietDetailView = [[DietDetailInfoView alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                                               [UtilManager height:20],
                                                                                               WIDTH - [UtilManager width:20],
                                                                                               HEIGHT - [UtilManager height:40])
                                                                            andDiet:_currentDiet];
    
    _modalView = [[RNBlurModalView alloc] initWithView:dietDetailView];
    [_modalView setUserInteractionEnabled:YES];
    [_modalView show];
}

- (void)didPressUserRateButton {
    NSString * dietId = _currentDiet.serverDietId;
    [WebServiceManager getDietsWithWithCompletionBlock:^(BOOL isSuccess) {
        _currentDiet = [Diet dietWithServerId:dietId];
        [self presentUserRateView:_currentDiet];
    }];
}

- (void)backButtonDidPress{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)optionButtonDidPress {
    OptionsFoodViewController *optionsFoodViewController = [[OptionsFoodViewController alloc] init];
    optionsFoodViewController.currentDiet = _currentDiet;
    optionsFoodViewController.optionFoodType = OptionFoodTypeDiet;
    optionsFoodViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsFoodViewController.delegate = self;
    [self presentViewController:optionsFoodViewController animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_dietTableView.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    DietMeal * currentDietMeal = [_currentDiet.dietMeals objectAtIndex:indexPath.row];
    _currentMealIndex = indexPath.row;
    
    MealDetailViewController * mealDetailViewController = [[MealDetailViewController alloc] init];
    if (currentDietMeal.meals.count > 0) {
        mealDetailViewController.currentMeal = [currentDietMeal.meals firstObject];
    }
    
    [self.navigationController pushViewController:mealDetailViewController animated:YES];
}

#pragma mark UITableViewDataSource Methods

- (NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _currentDiet.dietMeals.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MealCell *cell = [_dietTableView.tableView dequeueReusableCellWithIdentifier:@"mealCell"];
    cell.delegate = self;
    cell.currentIndex = indexPath.row;
    
    DietMeal * dietMeal = [_currentDiet.dietMeals objectAtIndex:indexPath.row];
    Meal * meal;
    if (dietMeal.meals.count > 0) {
        meal = [dietMeal.meals firstObject];
    }
    
    if (!meal) {
        return cell;
    }
    cell.titleLabel.text = meal.title;
    
    UIImageView *imageView = cell.foodImageView;
    
    NSURLRequest *foodImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_MEAL,meal.image]]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
    
    [cell.imageView setImageWithURLRequest:foodImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
        [imageView setImage:image];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [[imageView layer] setMasksToBounds:YES];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
        NSLog(@"%@",error);
        [imageView setImage:[UIImage imageNamed:@"img-placeholder"]];
    }];
    
    cell.tagView.tags = [Meal getMealTags:meal];
    [cell.tagView reloadTagSubviews];
    cell.tagView.hidden = NO;
    cell.kCalLabel.text = [NSString stringWithFormat:@"%d kCals", (int)[Meal getKCalInMeal:meal]];
    cell.proteinCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g", [Meal getMealProtein:meal]];
    cell.carbsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g", [Meal getMealCarb:meal]];
    cell.fatsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g",[Meal getMealFats:meal]];
    
    return cell;
}

- (nullable NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(8_0) __TVOS_PROHIBITED {
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"Delete", nil)  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        [_dietTableView.tableView setEditing:NO];
        
        if (_currentDiet.dietMeals.count > 1) {
            [tableView beginUpdates];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            DietMeal * dietMeal = _currentDiet.dietMeals[indexPath.row];
            [realm deleteObject:dietMeal];
            [realm transactionWithBlock:^{
                for (NSInteger index = 0; index < _currentDiet.dietMeals.count; index ++) {
                    _currentDiet.dietMeals[index].order = @(index);
                }
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            
            [_executive updateDiet:_currentDiet];
            
            [tableView endUpdates];
            [tableView reloadData];
            [_dietTableView reloadDietValues];
        } else {
            [self presentViewController:[UtilManager presentAlertWithMessage:NSLocalizedString(@"Una dieta debe contener una comida", nil)] animated:YES completion:^{
                
            }];
        }
    }];
    deleteAction.backgroundColor = [UIColor redColor];
    
    UITableViewRowAction *moreAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:NSLocalizedString(@"+", nil)  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        [_dietTableView.tableView setEditing:NO];
        
        _selectorFoodViewController = [[SelectorFoodViewController alloc] init];
        [_selectorFoodViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [_selectorFoodViewController setSelectorType:SelectorFoodTypeDiet];
        [_selectorFoodViewController setSelectDiet:_currentDiet];
        [_selectorFoodViewController setPresentMealDetail:YES];
        
        [self presentViewController:_selectorFoodViewController animated:YES completion:nil];
    }];
    moreAction.backgroundColor = UIColor.lightGrayColor;
    
    return @[deleteAction, moreAction];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, TAB_BAR_HEIGHT)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return foodCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return foodCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
    //return [UtilManager height:250];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return 0;
    //return [UtilManager height:250];
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return TAB_BAR_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return TAB_BAR_HEIGHT;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    RLMRealm *realm = [AppContext currentRealm];
    NSError *error;
    
    [realm transactionWithBlock:^{
        NSInteger tempOrder = _currentDiet.dietMeals[sourceIndexPath.row].order.integerValue;
        _currentDiet.dietMeals[sourceIndexPath.row].order = @(_currentDiet.dietMeals[destinationIndexPath.row].order.integerValue);
        _currentDiet.dietMeals[destinationIndexPath.row].order = @(tempOrder);
        [_currentDiet.dietMeals exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
    }];
    
    [realm beginWriteTransaction];
    [realm commitWriteTransaction:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

#pragma mark - DietDetailDisplay delegates

- (void)didUpdatedDiet:(Diet*)diet {
    [_dietTableView reloadDietValues];
    [_dietTableView.tableView reloadData];
}

- (void)didUpdatedMeal:(Meal*)meal {
    [_dietTableView reloadDietValues];
    [_dietTableView.tableView reloadData];
}

- (void)reloadAllElements {
    [WebServiceManager getAlimentsWithCompletionBlock:^(BOOL isSuccess) {
        [WebServiceManager getMealsWithCompletionBlock:^(BOOL isSuccess) {
            [WebServiceManager getDietsWithWithCompletionBlock:^(BOOL isSuccess) {
                [WebServiceManager getNutrionalPlansWithCompletionBlock:^(BOOL isSuccess) {
                    
                    [_dietTableView reloadDietValues];
                    [_dietTableView.tableView reloadData];
                }];
            }];
        }];
    }];
}

#pragma mark - OptionsFoodViewController Delegate

- (void)editDiet:(Diet *)diet {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)editNutritionPlan:(NutritionPlan *)nutritionPlan {
}

- (void)didDeletedMeal:(Meal*)meal {
    [_dietTableView reloadDietValues];
    [_dietTableView.tableView reloadData];
}

- (void)didPressEditFood:(RLMObject*)rlmObject {
    if ([rlmObject isKindOfClass:[Meal class]]) {
        Meal * meal = (Meal*)rlmObject;
        MealAddViewController *mealAddViewController = [[MealAddViewController alloc] init];
        mealAddViewController.editMeal = meal;
        mealAddViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        mealAddViewController.mealOperationViewDelegate = self;
        [self presentViewController:mealAddViewController animated:YES completion:nil];
    }
    else if ([rlmObject isKindOfClass:[Diet class]]) {
        Diet * diet = (Diet*)rlmObject;
        DietAddViewController *dietAddViewController = [[DietAddViewController alloc] init];
        dietAddViewController.editDiet = diet;
        dietAddViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        dietAddViewController.dietDetailViewControllerDelegate = self;
        
        [self presentViewController:dietAddViewController animated:YES completion:nil];
    }
}

- (void)didPressFavoriteFood:(RLMObject*)rlmObject {
    [_dietTableView reloadDietValues];
    [_dietTableView.tableView reloadData];
}

- (void)didPressDeleteFood:(RLMObject*)rlmObject {
    if ([rlmObject isKindOfClass:[Meal class]]) {
        RLMRealm *realm = [AppContext currentRealm];
        NSError *error;
        
        DietMeal * dietMeal = _currentDiet.dietMeals[_currentMealIndex];
        [realm deleteObject:dietMeal];
        [realm transactionWithBlock:^{
            for (NSInteger index = 0; index < _currentDiet.dietMeals.count; index ++) {
                _currentDiet.dietMeals[index].order = @(index);
            }
        }];
        
        [realm beginWriteTransaction];
        [realm commitWriteTransaction:&error];
        if (error) {
            NSLog(@"%@", error.localizedDescription);
        }
        
        [_executive updateDiet:_currentDiet];
        
        return;
    }
    
    [self.navigationController popToViewController:_foodRootViewController animated:YES];
    [_optionMenuDelegate didPressDeleteFood:rlmObject];
}

- (void)didPressDuplicateFood:(RLMObject*)rlmObject {
    if ([rlmObject isKindOfClass:[Meal class]]) {
        Meal * meal = (Meal*)rlmObject;
        
        RLMRealm *realm = [AppContext currentRealm];
        NSError *error;
        
        DietMeal * dietMeal = [[DietMeal alloc] init];
        dietMeal.dietMealId = [UtilManager uuid];
        dietMeal.order = @(_currentDiet.dietMeals.count);
        [dietMeal.meals addObject:meal];
        
        [realm transactionWithBlock:^{
            [_currentDiet.dietMeals addObject:dietMeal];
        }];
        
        [realm beginWriteTransaction];
        [realm commitWriteTransaction:&error];
        if (error) {
            NSLog(@"%@", error.localizedDescription);
        }
        [_executive updateDiet:_currentDiet];
    }
}

- (void)didPressReplaceDiet:(RLMObject*)rlmObject {
    if ([rlmObject isKindOfClass:[Meal class]]) {
        _selectorFoodViewController = [[SelectorFoodViewController alloc] init];
        _selectorFoodViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        _selectorFoodViewController.selectorType = SelectorFoodTypeDietSingle;
        _selectorFoodViewController.selectDiet = _currentDiet;
        _selectorFoodViewController.selectedFoodIndex = _selectedMealIndex;
        _selectorFoodViewController.delegate = self;
        
        [self presentViewController:_selectorFoodViewController animated:YES completion:nil];
    }
}

- (void)didPressFusionFood:(RLMObject*)rlmObject {
    Meal * selectedMeal = (Meal*)rlmObject;
    
    RLMRealm *realm = [AppContext currentRealm];
    if (selectedMeal.mergedMeals &&
        selectedMeal.mergedMeals.count > 0) {
        
        DietMeal * dietMeal = _currentDiet.dietMeals[_selectedMealIndex];
        NSInteger order = dietMeal.order.integerValue;
        
        DietMeal * mergeDietMeal = [[DietMeal alloc] init];
        mergeDietMeal.dietMealId = [UtilManager uuid];
        mergeDietMeal.order = @(order);
        [mergeDietMeal.meals addObjects:selectedMeal.mergedMeals];
        
        [realm transactionWithBlock:^{
            [_currentDiet.dietMeals replaceObjectAtIndex:_selectedMealIndex withObject:mergeDietMeal];
        }];
        
//        [Meal deleteMeal:selectedMeal];
//        [Meal saveMergedMealIdsToPreference];
        
        [_dietTableView reloadDietValues];
        [_dietTableView.tableView reloadData];
        return;
    }
    
    _selectorFoodViewController = [[SelectorFoodViewController alloc] init];
    _selectorFoodViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    _selectorFoodViewController.selectorType = SelectorFoodTypeDietSingle;
    _selectorFoodViewController.selectMeal = selectedMeal;
    _selectorFoodViewController.selectedFoodIndex = _selectedMealIndex;
    _selectorFoodViewController.delegate = self;
    
    [self presentViewController:_selectorFoodViewController animated:YES completion:nil];
}

- (void)didPressSendFood:(RLMObject *)rlmObject {
    _userSelectorViewController = [[UserSelectorViewController alloc] init];
    _userSelectorViewController.rlmObject = rlmObject;
    _userSelectorViewController.delegate = self;
    _userSelectorViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:_userSelectorViewController animated:YES completion:nil];
}

- (void)didPressShareFood:(RLMObject *)rlmObject {
    [_executive shareContent:rlmObject];
}

- (void)optionsFoodViewController:(OptionsFoodViewController *)optionsFoodViewController didPressTimePicker:(RLMObject *)rlmObject {
    if (![rlmObject isKindOfClass:[DietMeal class]]) {
        return;
    }
    
    [self.view endEditing:YES];
    
    if (_datePicker) {
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
    
    DietMeal * dietMeal = (DietMeal*)rlmObject;
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,
                                                                 CGRectGetHeight(self.view.frame) - 216,
                                                                 CGRectGetWidth(self.view.frame),
                                                                 216)];
    
    _datePicker.datePickerMode = UIDatePickerModeTime;
    NSDate * startTime = [DateTimeUtil dateFromString:dietMeal.startTime
                                               format:@"HH:mm:ss"];
    
    _datePicker.date = startTime ? startTime : [NSDate date];
    
    _datePicker.backgroundColor = UIColor.whiteColor;
    _datePicker.userInteractionEnabled = YES;
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_datePicker.frame) - 80,
                                                               CGRectGetMinY(_datePicker.frame),
                                                               80,
                                                               24)];
    
    [_acceptButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptDatePickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_datePicker];
    [self.view addSubview:_acceptButton];
}

- (void)acceptDatePickerAction {
    if (_acceptButton &&
        _datePicker) {
        
        DietMeal * dietMeal = [_currentDiet.dietMeals objectAtIndex:_selectedMealIndex];
        [_executive updateStartTime:_datePicker.date
                         ofDietMeal:dietMeal
                             ofDiet:_currentDiet];
        
        [_acceptButton removeFromSuperview];
        _acceptButton = nil;
        
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
}

#pragma mark - MealCellDelegate Methods

- (void)mealCellActionInIndex:(NSInteger)index {
    DietMeal * dietMeal = [_currentDiet.dietMeals objectAtIndex:index];
    
    _selectedMealIndex = index;
    OptionsFoodViewController *optionsViewController = [[OptionsFoodViewController alloc] init];
    optionsViewController.currentMeal = [dietMeal.meals firstObject];
    optionsViewController.currentDietMeal = dietMeal;
    optionsViewController.optionFoodType = OptionFoodTypeMeal;
    optionsViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsViewController.delegate = self;
    optionsViewController.fromDetailOfParent = YES;
    [self presentViewController:optionsViewController animated:YES completion:nil];
}

- (void)mealCellDetailInfoActionInIndex:(NSInteger)index {
    Meal * meal = [_currentDiet.dietMeals[index].meals firstObject];
    MealDetailInfoView * mealDetailView = [[MealDetailInfoView alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                                               [UtilManager height:20],
                                                                                               WIDTH - [UtilManager width:20],
                                                                                               HEIGHT - [UtilManager height:40])
                                                                            andMeal:meal
                                                                         isEditable:NO];
    
    _modalView = [[RNBlurModalView alloc] initWithView:mealDetailView];
    [_modalView setUserInteractionEnabled:YES];
    [_modalView show];
}

#pragma mark - MealOperationViewDelegate Methods

#pragma mark - SelectorFoodViewController Delegate

- (void)didPressOKForMergeFood:(RLMObject*)rlmObject {
//    RLMRealm *realm = [AppContext currentRealm];
//    Meal * mergingMeal = (Meal*)rlmObject;
//    Meal * selectedMeal = [_currentDiet.meals objectAtIndex:_selectedMealIndex];
//    
//    Meal * mergedMeal = [Meal mergeMeal:selectedMeal WithMeal:mergingMeal];
//    [Meal saveMeal:mergedMeal];
//    
//    [realm transactionWithBlock:^{
//        mergingMeal.childOfMergedMeal = @YES;
//        selectedMeal.childOfMergedMeal = @YES;
//        [_currentDiet.meals ]
//    }];
//    
//    [realm beginWriteTransaction];
//    NSError *error;
//    [realm commitWriteTransaction:&error];
//    if(error){
//        NSLog(@"%@",error.description);
//    }
//    
//    [Meal saveMergedMealIdsToPreference];
}

#pragma mark - UserSelectorViewControllerDelegate Methods

- (void)didPressOKWithSelectedResults:(NSArray*)selectedResults
                           andContent:(RLMObject*)content {
    
    UserMeta * userMeta = [selectedResults firstObject];
    [_executive sendContent:content
                    toToken:userMeta.token];
}

@end
