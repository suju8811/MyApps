//
//  PlanFoodViewCell.h
//  Titan
//
//  Created by Manuel Manzanera on 21/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagView.h"

@protocol PlanFoodCellDelegate <NSObject>

- (void)nutritionPlanDidPushInIndex:(NSInteger)index;

- (void)nutritionPlanCellDetailInfoActionInIndex:(NSInteger)index;

@end

@interface PlanFoodViewCell : UITableViewCell

//@property (nonatomic, strong) UILabel *titleLabel;
//
//@property (nonatomic, strong) TagView *tagView;
//
//@property (nonatomic, strong) UILabel *contentLabel;
//@property (nonatomic, strong) UILabel *daysLabel;
//
//@property (nonatomic, assign) NSInteger currentIndex;
//@property (nonatomic, assign) id<PlanFoodCellDelegate>delegate;
//
//@property (nonatomic, strong) UIView *separatorView;

//

@property (nonatomic, strong) UIView *containerView;

@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *exercisesLabel;
@property (nonatomic, strong) UILabel *daysLabel;
@property (nonatomic, strong) UIView *separatorView;

@property (nonatomic, strong) UIView *panView;
@property (nonatomic, strong) UIButton *menuButton;
@property (nonatomic, strong) UIButton *detailInfoButton;

@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, assign) id<PlanFoodCellDelegate>delegate;

@property (nonatomic, strong) UIImageView *checkImageView;

@end
