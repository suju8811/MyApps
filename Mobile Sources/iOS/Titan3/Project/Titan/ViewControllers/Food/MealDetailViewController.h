//
//  MealDetailViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "Meal.h"
#import "FoodViewController.h"

@protocol MealOperationViewDelegate <NSObject>

- (void)didUpdatedMeal:(Meal*)meal;

@optional
- (void)didUpdateAliment:(Aliment*)aliment;

@end

@interface MealDetailViewController : ParentViewController

@property (nonatomic, strong) FoodViewController * foodViewController;
@property (nonatomic, strong) Meal *currentMeal;
@property (nonatomic, assign) NSInteger selectedAlimentIndex;

@end
