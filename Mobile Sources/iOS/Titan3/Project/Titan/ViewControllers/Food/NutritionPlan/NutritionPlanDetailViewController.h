//
//  NutritionPlanDetailViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 16/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "NutritionPlan.h"
#import "SchedulePlan.h"

@protocol NutritionPlanOperationViewDelegate <NSObject>

- (void)didUpdateNutritionPlan:(NutritionPlan*)nutritionPlan;

@end

@interface NutritionPlanDetailViewController : ParentViewController

@property (nonatomic, strong) NutritionPlan *currentNutritionPlan;
@property (nonatomic, assign) NSInteger fromCalendar;
@property (nonatomic, strong) SchedulePlan * schedulePlan;
@property (nonatomic, strong) NSDate * selectedDate;

@end
