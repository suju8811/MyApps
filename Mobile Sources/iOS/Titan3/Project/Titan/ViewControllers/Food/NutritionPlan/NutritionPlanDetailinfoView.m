//
//  NutritionPlanDetailinfoView.m
//  Titan
//
//  Created by Marcus Lee on 8/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "NutritionPlanDetailinfoView.h"
#import "UtilManager.h"
#import "AppConstants.h"
#import "AppContext.h"
#import "TagView.h"
#import "UIImageView+AFNetworking.h"

@interface NutritionPlanDetailinfoView () <UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIPageControl *pageControl;

@property (nonatomic, strong) UIButton *quantityButton;
@property (nonatomic, strong) UITextField *quantityValue;

@property (nonatomic, strong) UITextField *measureValue;
@property (nonatomic, strong) UIButton *measureButton;

@property (nonatomic, strong) UILabel *sugarsLabel;
@property (nonatomic, strong) UILabel *satFatLabel;
@property (nonatomic, strong) UILabel *mon_satFatLabel;
@property (nonatomic, strong) UILabel *pol_satFatLabel;
@property (nonatomic, strong) UILabel *fiberLabel;

@property (nonatomic, strong) UILabel *fiberValueLabel;
@property (nonatomic, strong) UILabel *bcaaValueLabel;
@property (nonatomic, strong) UILabel *glutValueLabel;
@property (nonatomic, strong) UILabel *ome3ValueLabel;
@property (nonatomic, strong) UILabel *ome6ValueLabel;
@property (nonatomic, strong) UILabel *waterValueLabel;
@property (nonatomic, strong) UILabel *alcoholValueLabel;

@property (nonatomic, strong) UILabel *calciumValue;
@property (nonatomic, strong) UILabel *ironValue;
@property (nonatomic, strong) UILabel *iodoValue;
@property (nonatomic, strong) UILabel *magnesiumValue;
@property (nonatomic, strong) UILabel *zincValue;
@property (nonatomic, strong) UILabel *seleniumValue;
@property (nonatomic, strong) UILabel *sodiumValue;
@property (nonatomic, strong) UILabel *potassiumValue;
@property (nonatomic, strong) UILabel *phosphorusValue;

@property (nonatomic, strong) UILabel *b1Value;
@property (nonatomic, strong) UILabel *b2Value;
@property (nonatomic, strong) UILabel *niacinValue;
@property (nonatomic, strong) UILabel *AValue;
@property (nonatomic, strong) UILabel *folicValue;
@property (nonatomic, strong) UILabel *b12Value;
@property (nonatomic, strong) UILabel *cValue;
@property (nonatomic, strong) UILabel *dValue;
@property (nonatomic, strong) UILabel *eValue;

@property (nonatomic, strong) NSNumber *quantity;

@end

@implementation NutritionPlanDetailinfoView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self configureViewWithFrame:frame];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
   andNutritionPlan:(NutritionPlan *)nutritionPlan {
    
    self = [super initWithFrame:frame];
    
    if(self) {
        _currentNutritionPlan = nutritionPlan;
        [self configureViewWithFrame:frame];
    }
    
    return self;
}

- (void)configureViewWithFrame:(CGRect)frame{
    NSUInteger numberPages = 2;
    
    [self setUserInteractionEnabled:YES];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:self.bounds];
    [alphaView setBackgroundColor:[UIColor blackColor]];
    [alphaView setAlpha:0.7];
    
    [self addSubview:alphaView];
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0., .0, frame.size.width, frame.size.height)];
    [_scrollView setContentSize:CGSizeMake(frame.size.width * 2, frame.size.height)];
    [_scrollView setShowsHorizontalScrollIndicator:TRUE];
    _scrollView.pagingEnabled = YES;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.scrollsToTop = NO;
    _scrollView.delegate = self;
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., frame.size.width, frame.size.height)];
    [backgroundView setBackgroundColor:[UIColor blackColor]];
    [backgroundView setAlpha:0.5];
    
    [self addSubview:backgroundView];
    
    [_scrollView addSubview:[self firstView]];
    [_scrollView addSubview:[self secondView]];
    [_scrollView addSubview:[self thirdView]];
    
    [_scrollView setDelegate:self];
    
    [self addSubview:_scrollView];
    
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:60], [UtilManager height:20])];
    [_pageControl setCenter:CGPointMake(self.center.x, frame.size.height - [UtilManager height:15])];
    [_pageControl setCurrentPage:1];
    [_pageControl setNumberOfPages:numberPages];
    [_pageControl setPageIndicatorTintColor:[UIColor grayColor]];
    [_pageControl setCurrentPageIndicatorTintColor:[UIColor whiteColor]];
    
    [self addSubview:_pageControl];
}

- (UIView *)firstView{
    UIView *firstView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., self.frame.size.width, self.frame.size.height)];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:10],[UtilManager width:200],[UtilManager height:35])];
    [_titleLabel setTextColor:YELLOW_APP_COLOR];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setText:_currentNutritionPlan.title];
    [_titleLabel setAdjustsFontSizeToFitWidth:YES];
    [_titleLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:21]];
    
    [_titleLabel setCenter:CGPointMake(self.center.x, _titleLabel.center.y)];
    
    [self addSubview:_titleLabel];
    
    TagView *tagView = [[TagView alloc] initWithFrame:CGRectMake(0, _titleLabel.frame.size.height + _titleLabel.frame.origin.y, WIDTH, TAGVIEW_SMALL_HEIGHT)];
    [tagView setCenter:CGPointMake(self.center.x, tagView.center.y)];
    [tagView setIsCenter:TRUE];
    [tagView setMode:TagsControlModeList];
    
    NSMutableArray *tags = [NutritionPlan getTagsInNutritionPlan:_currentNutritionPlan];
    
    [tagView setTags:tags];
    [tagView reloadTagSubviews];
    
    [self addSubview:tagView];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:5], tagView.frame.origin.y + tagView.frame.size.height + [UtilManager height:5], self.frame.size.width - [UtilManager width:10], 1)];
    [separatorView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self addSubview:separatorView];
    
    _quantityButton = [[UIButton alloc] initWithFrame:CGRectMake(0.,separatorView.frame.origin.y + 1, WIDTH/2, [UtilManager height:40])];
    [_quantityButton setBackgroundColor:[UIColor clearColor]];
    
    _quantityValue = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:0],
                                                                   separatorView.frame.origin.y + [UtilManager height:10],
                                                                   WIDTH/2,
                                                                   [UtilManager height:20])];
    
    NSInteger quantityValue = [NutritionPlan getNutritionPlanQuantity:_currentNutritionPlan];
    _quantityValue.text = [NSString stringWithFormat:@"%d ", (int)quantityValue];
    _quantityValue.textColor  = UIColor.redColor;
    _quantityValue.textAlignment = NSTextAlignmentRight;
    //    [_quantityValue setDelegate:self];
    _quantityValue.userInteractionEnabled = NO;
    _quantityValue.keyboardType = UIKeyboardTypeDecimalPad;
    _quantityValue.keyboardAppearance = UIKeyboardAppearanceDark;
    _quantityValue.font = [UIFont fontWithName:REGULAR_FONT_OPENSANS size:21];
    
    UIToolbar *toolbarWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *barWeigthButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resetView)];
    [barWeigthButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barWeigthButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarWeigth setItems:[NSArray arrayWithObjects:flexibleWeigthSpace, barWeigthButtonItem, nil]];
    _quantityValue.inputAccessoryView = toolbarWeigth;
    
    [self addSubview:_quantityValue];
    
    [_quantityButton addTarget:self action:@selector(inputQuantityValue) forControlEvents:UIControlEventTouchUpInside];
    
    [firstView addSubview:_quantityButton];
    
    _measureButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH/2,separatorView.frame.origin.y + 1, WIDTH/2, [UtilManager height:40])];
    [_measureButton setBackgroundColor:[UIColor clearColor]];
    
    _measureValue = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH/2, separatorView.frame.origin.y + [UtilManager height:10], WIDTH/2, [UtilManager height:20])];
    [_measureValue setText:[NSString stringWithFormat:@"  %@",
                            @"GR" ]];
    [_measureValue setTextColor:[UIColor redColor]];
    [_measureValue setTextAlignment:NSTextAlignmentLeft];
    //    [_measureValue setDelegate:self];
    _measureValue.userInteractionEnabled = NO;
    [_measureValue setKeyboardType:UIKeyboardTypeDecimalPad];
    [_measureValue setUserInteractionEnabled:TRUE];
    [_measureValue setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_measureValue setFont:[UIFont fontWithName:BOLD_FONT size:23]];
    
    [self addSubview:_measureValue];
    
    //    [_measureButton addTarget:self action:@selector(inputMeasureValue) forControlEvents:UIControlEventTouchUpInside];
    
    [firstView addSubview:_measureButton];
    
    UIView *kCalseparatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:5], _quantityButton.frame.origin.y + _quantityButton.frame.size.height + [UtilManager height:1], self.frame.size.width - [UtilManager width:10], 1)];
    [kCalseparatorView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self addSubview:kCalseparatorView];
    
    _kCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, kCalseparatorView.frame.origin.y + kCalseparatorView.frame.size.height + [UtilManager height:5], _titleLabel.frame.size.width, _titleLabel.frame.size.height)];
    [_kCalLabel setTextColor:GREEN_APP_COLOR];
    [_kCalLabel setText:[NSString stringWithFormat:@"%d kCal", (int)[NutritionPlan getKCalInNutritionPlan:_currentNutritionPlan]]];
    [_kCalLabel setTextAlignment:NSTextAlignmentCenter];
    [_kCalLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:22]];
    
    [firstView addSubview:_kCalLabel];
    
    /// Gramos de Proteinas
    
    _proteinLabel = [[UILabel alloc] initWithFrame:CGRectMake(_kCalLabel.frame.origin.x + _kCalLabel.frame.size.width, _kCalLabel.frame.origin.y + _kCalLabel.frame.size.height + [UtilManager height:10], [UtilManager width:80], [UtilManager width:80])];
    [[_proteinLabel layer] setBorderColor:BLUE_PERCENT_CIRCLE.CGColor];
    [[_proteinLabel layer] setBorderWidth:1];
    [[_proteinLabel layer] setCornerRadius:_proteinLabel.frame.size.width/2];
    [_proteinLabel setText:[NSString stringWithFormat:@"P\n%d g", (int)[NutritionPlan getNutritionPlanProtein:_currentNutritionPlan]]];
    [_proteinLabel setNumberOfLines:2];
    [_proteinLabel setCenter:CGPointMake(WIDTH/4, _proteinLabel.center.y)];
    [_proteinLabel setTextColor:BLUE_PERCENT_CIRCLE];
    [_proteinLabel setTextAlignment:NSTextAlignmentCenter];
    [_proteinLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:21]];
    
    //[firstView addSubview:_proteinLabel];
    
    _proteinCircleView = [[PropertyCircleView alloc] initWithFrame:_proteinLabel.frame];
    
    [[_proteinCircleView layer] setBorderColor:BLUE_PERCENT_CIRCLE.CGColor];
    [[_proteinCircleView layer] setBorderWidth:1];
    [[_proteinCircleView layer] setCornerRadius:_proteinLabel.frame.size.width/2];
    
    [[_proteinCircleView initialLabel] setText:NSLocalizedString(@"Proteinas", nil)];
    [[_proteinCircleView initialLabel] setTextColor:BLUE_PERCENT_CIRCLE];
    [[_proteinCircleView initialLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:11]];
    [[_proteinCircleView countLabel] setText:[NSString stringWithFormat:@"%d g", (int)[NutritionPlan getNutritionPlanProtein:_currentNutritionPlan]]];
    [[_proteinCircleView countLabel] setTextColor:BLUE_PERCENT_CIRCLE];
    [[_proteinCircleView measureLabel] setTextColor:BLUE_PERCENT_CIRCLE];
    [[_proteinCircleView countLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:26]];
    [[_proteinCircleView measureLabel] setText:@"g"];
    [[_proteinCircleView measureLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:26]];
    
    [firstView addSubview:_proteinCircleView];
    
    /// Gramos de Hidratos
    
    _carbsLabel = [[UILabel alloc] initWithFrame:CGRectMake(_proteinLabel.frame.origin.x + _proteinLabel.frame.size.width + [UtilManager width:10], 0., _proteinLabel.frame.size.width, _proteinLabel.frame.size.height)];
    [[_carbsLabel layer] setBorderColor:YELLOW_PERCENT_CIRCLE.CGColor];
    [[_carbsLabel layer] setBorderWidth:1];
    [[_carbsLabel layer] setCornerRadius:_proteinLabel.frame.size.width/2];
    [_carbsLabel setText:[NSString stringWithFormat:@"C\n%d g", (int)[NutritionPlan getNutritionPlanCarb:_currentNutritionPlan]]];
    [_carbsLabel setNumberOfLines:2];
    [_carbsLabel setCenter:CGPointMake(WIDTH/2, _proteinLabel.center.y)];
    [_carbsLabel setTextColor:YELLOW_PERCENT_CIRCLE];
    [_carbsLabel setTextAlignment:NSTextAlignmentCenter];
    [_carbsLabel setFont:_proteinLabel.font];
    
    //[firstView addSubview:_carbsLabel];
    
    _carbsCircleView = [[PropertyCircleView alloc] initWithFrame:_carbsLabel.frame];
    
    [[_carbsCircleView layer] setBorderColor:YELLOW_PERCENT_CIRCLE.CGColor];
    [[_carbsCircleView layer] setBorderWidth:1];
    [[_carbsCircleView layer] setCornerRadius:_proteinLabel.frame.size.width/2];
    
    [[_carbsCircleView initialLabel] setText:NSLocalizedString(@"Carbos", nil)];
    [[_carbsCircleView initialLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
    [[_carbsCircleView initialLabel] setFont:_proteinCircleView.initialLabel.font];
    [[_carbsCircleView countLabel] setText:[NSString stringWithFormat:@"%d g", (int)[NutritionPlan getNutritionPlanCarb:_currentNutritionPlan]]];
    [[_carbsCircleView countLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
    [[_carbsCircleView measureLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
    [[_carbsCircleView countLabel] setFont:_proteinCircleView.countLabel.font];
    [[_carbsCircleView measureLabel] setText:@"g"];
    [[_carbsCircleView measureLabel] setFont:_proteinCircleView.measureLabel.font];
    
    [firstView addSubview:_carbsCircleView];
    
    /// gramos de totales de grasas
    
    _fatsLabel = [[UILabel alloc] initWithFrame:CGRectMake(_carbsLabel.frame.origin.x + _carbsLabel.frame.size.width + [UtilManager width:10], 0., _proteinLabel.frame.size.width, _proteinLabel.frame.size.height)];
    [[_fatsLabel layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
    [[_fatsLabel layer] setBorderWidth:1];
    [[_fatsLabel layer] setCornerRadius:_proteinLabel.frame.size.width/2];
    [_fatsLabel setText:[NSString stringWithFormat:@"G\n%d g", (int)[NutritionPlan getNutritionPlanFats:_currentNutritionPlan]]];
    [_fatsLabel setNumberOfLines:2];
    [_fatsLabel setCenter:CGPointMake(WIDTH * 3 / 4, _proteinLabel.center.y)];
    [_fatsLabel setTextColor:ORANGE_PERCENT_CIRCLE];
    [_fatsLabel setTextAlignment:NSTextAlignmentCenter];
    [_fatsLabel setFont:_proteinLabel.font];
    
    //[firstView addSubview:_fatsLabel];
    
    _fatsCircleView = [[PropertyCircleView alloc] initWithFrame:_fatsLabel.frame];
    
    [[_fatsCircleView layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
    [[_fatsCircleView layer] setBorderWidth:1];
    [[_fatsCircleView layer] setCornerRadius:_fatsLabel.frame.size.width/2];
    [[_fatsCircleView initialLabel] setText:NSLocalizedString(@"Grasas", nil)];
    [[_fatsCircleView initialLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_fatsCircleView initialLabel] setFont:_proteinCircleView.initialLabel.font];
    [[_fatsCircleView countLabel] setText:[NSString stringWithFormat:@"%d g", (int)[NutritionPlan getNutritionPlanFats:_currentNutritionPlan]]];
    [[_fatsCircleView countLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_fatsCircleView measureLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_fatsCircleView countLabel] setFont:_proteinCircleView.countLabel.font];
    [[_fatsCircleView measureLabel] setText:@"g"];
    [[_fatsCircleView measureLabel] setFont:_proteinCircleView.measureLabel.font];
    
    
    [firstView addSubview:_fatsCircleView];
    
    
    //// Gramos de azúcares
    
    _sugarsLabel = [[UILabel alloc] initWithFrame:CGRectMake(_proteinLabel.frame.origin.x + _proteinLabel.frame.size.width + [UtilManager width:10], _carbsLabel.frame.origin.y + _carbsLabel.frame.size.height + [UtilManager height:10], _proteinLabel.frame.size.width * 0.8, _proteinLabel.frame.size.height * 0.8)];
    [[_sugarsLabel layer] setBorderColor:YELLOW_PERCENT_CIRCLE.CGColor];
    [[_sugarsLabel layer] setBorderWidth:1];
    [[_sugarsLabel layer] setCornerRadius:_sugarsLabel.frame.size.width/2];
    [_sugarsLabel setText:[NSString stringWithFormat:@"Azu\n%d g", (int)[NutritionPlan getNutritionPlanSugar:_currentNutritionPlan]]];
    [_sugarsLabel setNumberOfLines:2];
    [_sugarsLabel setCenter:CGPointMake(WIDTH/2, _sugarsLabel.center.y)];
    [_sugarsLabel setTextColor:YELLOW_PERCENT_CIRCLE];
    [_sugarsLabel setTextAlignment:NSTextAlignmentCenter];
    [_sugarsLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]];
    
    //[firstView addSubview:_sugarsLabel];
    _sugarCircleView = [[PropertyCircleView alloc] initWithFrame:_sugarsLabel.frame];
    
    [[_sugarCircleView layer] setBorderColor:YELLOW_PERCENT_CIRCLE.CGColor];
    [[_sugarCircleView layer] setBorderWidth:1];
    [[_sugarCircleView layer] setCornerRadius:_sugarsLabel.frame.size.width/2];
    [[_sugarCircleView initialLabel] setText:NSLocalizedString(@"Azúcar", nil)];
    [[_sugarCircleView initialLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
    [[_sugarCircleView countLabel] setText:[NSString stringWithFormat:@"%d g", (int)[NutritionPlan getNutritionPlanSugar:_currentNutritionPlan]]];
    [[_sugarCircleView countLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
    [[_sugarCircleView measureLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
    [[_sugarCircleView measureLabel] setText:@"g"];
    [[_sugarCircleView initialLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:9]];
    [[_sugarCircleView countLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:17]];
    [[_sugarCircleView measureLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:17]];
    
    [firstView addSubview:_sugarCircleView];
    
    //// Gramos de saturadas
    
    _satFatLabel= [[UILabel alloc] initWithFrame:CGRectMake(_proteinLabel.frame.origin.x + _proteinLabel.frame.size.width + [UtilManager width:10], _carbsLabel.frame.origin.y + _carbsLabel.frame.size.height + [UtilManager height:10], _sugarsLabel.frame.size.width, _sugarsLabel.frame.size.height)];
    [[_satFatLabel layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
    [[_satFatLabel layer] setBorderWidth:1];
    [[_satFatLabel layer] setCornerRadius:_satFatLabel.frame.size.width/2];
    [_satFatLabel setText:[NSString stringWithFormat:@"Sat\n%d g", (int)[NutritionPlan getNutritionPlanSaturedFats:_currentNutritionPlan]]];
    [_satFatLabel setNumberOfLines:2];
    [_satFatLabel setCenter:CGPointMake(_fatsLabel.center.x, _sugarsLabel.center.y)];
    [_satFatLabel setTextColor:ORANGE_PERCENT_CIRCLE];
    [_satFatLabel setTextAlignment:NSTextAlignmentCenter];
    [_satFatLabel setFont:_sugarsLabel.font];
    
    //[firstView addSubview:_satFatLabel];
    
    _satFatsCircleView = [[PropertyCircleView alloc] initWithFrame:_satFatLabel.frame];
    
    [[_satFatsCircleView layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
    [[_satFatsCircleView layer] setBorderWidth:1];
    [[_satFatsCircleView layer] setCornerRadius:_sugarsLabel.frame.size.width/2];
    [[_satFatsCircleView initialLabel] setText:NSLocalizedString(@"Sat", nil)];
    [[_satFatsCircleView initialLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_satFatsCircleView countLabel] setText:[NSString stringWithFormat:@"%d g", (int)[NutritionPlan getNutritionPlanSaturedFats:_currentNutritionPlan]]];
    [[_satFatsCircleView countLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_satFatsCircleView measureLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_satFatsCircleView measureLabel] setText:@"g"];
    [[_satFatsCircleView initialLabel] setFont:_sugarCircleView.initialLabel.font];
    [[_satFatsCircleView countLabel] setFont:_sugarCircleView.countLabel.font];
    [[_satFatsCircleView measureLabel] setFont:_sugarCircleView.measureLabel.font];
    
    [firstView addSubview:_satFatsCircleView];
    //// Gramos de Monosaturadas
    
    _mon_satFatLabel= [[UILabel alloc] initWithFrame:CGRectMake(_proteinLabel.frame.origin.x + _proteinLabel.frame.size.width + [UtilManager width:10], _satFatLabel.frame.origin.y + _satFatLabel.frame.size.height + [UtilManager height:10],_sugarsLabel.frame.size.width, _sugarsLabel.frame.size.height)];
    [[_mon_satFatLabel layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
    [[_mon_satFatLabel layer] setBorderWidth:1];
    [[_mon_satFatLabel layer] setCornerRadius:_sugarsLabel.frame.size.width/2];
    [_mon_satFatLabel setText:[NSString stringWithFormat:@"Mon\n%d g", (int)[NutritionPlan getNutritionPlanMonoUnsaturedFats:_currentNutritionPlan]]];
    [_mon_satFatLabel setNumberOfLines:2];
    [_mon_satFatLabel setCenter:CGPointMake(_fatsLabel.center.x, _mon_satFatLabel.center.y)];
    [_mon_satFatLabel setTextColor:ORANGE_PERCENT_CIRCLE];
    [_mon_satFatLabel setTextAlignment:NSTextAlignmentCenter];
    [_mon_satFatLabel setFont:_sugarsLabel.font];
    
    //[firstView addSubview:_mon_satFatLabel];
    
    _monFatsCircleView = [[PropertyCircleView alloc] initWithFrame:_mon_satFatLabel.frame];
    
    [[_monFatsCircleView layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
    [[_monFatsCircleView layer] setBorderWidth:1];
    [[_monFatsCircleView layer] setCornerRadius:_mon_satFatLabel.frame.size.width/2];
    [[_monFatsCircleView initialLabel] setText:NSLocalizedString(@"Mon", nil)];
    [[_monFatsCircleView initialLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_monFatsCircleView countLabel] setText:[NSString stringWithFormat:@"%d g", (int)[NutritionPlan getNutritionPlanMonoUnsaturedFats:_currentNutritionPlan]]];
    [[_monFatsCircleView countLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_monFatsCircleView measureLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_monFatsCircleView measureLabel] setText:@"g"];
    [[_monFatsCircleView initialLabel] setFont:_sugarCircleView.initialLabel.font];
    [[_monFatsCircleView countLabel] setFont:_sugarCircleView.countLabel.font];
    [[_monFatsCircleView measureLabel] setFont:_sugarCircleView.measureLabel.font];
    
    [firstView addSubview:_monFatsCircleView];
    
    //// Gramos de Polisaturadas
    
    _pol_satFatLabel= [[UILabel alloc] initWithFrame:CGRectMake(_proteinLabel.frame.origin.x + _proteinLabel.frame.size.width + [UtilManager width:10], _mon_satFatLabel.frame.origin.y + _mon_satFatLabel.frame.size.height + [UtilManager height:10], _sugarsLabel.frame.size.width, _sugarsLabel.frame.size.height)];
    [[_pol_satFatLabel layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
    [[_pol_satFatLabel layer] setBorderWidth:1];
    [[_pol_satFatLabel layer] setCornerRadius:_sugarsLabel.frame.size.width/2];
    [_pol_satFatLabel setText:[NSString stringWithFormat:@"Pol\n%d g", (int)[NutritionPlan getNutritionPlanPolyUnsaturedFats:_currentNutritionPlan]]];
    [_pol_satFatLabel setNumberOfLines:2];
    [_pol_satFatLabel setCenter:CGPointMake(_fatsLabel.center.x, _pol_satFatLabel.center.y)];
    [_pol_satFatLabel setTextColor:ORANGE_PERCENT_CIRCLE];
    [_pol_satFatLabel setTextAlignment:NSTextAlignmentCenter];
    [_pol_satFatLabel setFont:_sugarsLabel.font];
    
    //[firstView addSubview:_pol_satFatLabel];
    
    _polFatsCircleView = [[PropertyCircleView alloc] initWithFrame:_pol_satFatLabel.frame];
    
    [[_polFatsCircleView layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
    [[_polFatsCircleView layer] setBorderWidth:1];
    [[_polFatsCircleView layer] setCornerRadius:_mon_satFatLabel.frame.size.width/2];
    [[_polFatsCircleView initialLabel] setText:NSLocalizedString(@"Pol", nil)];
    [[_polFatsCircleView initialLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_polFatsCircleView countLabel] setText:[NSString stringWithFormat:@"%d g",(int)[NutritionPlan getNutritionPlanPolyUnsaturedFats:_currentNutritionPlan]]];
    [[_polFatsCircleView countLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_polFatsCircleView measureLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
    [[_polFatsCircleView measureLabel] setText:@"g"];
    [[_polFatsCircleView initialLabel] setFont:_sugarCircleView.initialLabel.font];
    [[_polFatsCircleView countLabel] setFont:_sugarCircleView.countLabel.font];
    [[_polFatsCircleView measureLabel] setFont:_sugarCircleView.measureLabel.font];
    
    [firstView addSubview:_polFatsCircleView];
    
    _fiberLabel= [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10],_sugarsLabel.frame.origin.y + _sugarsLabel.frame.size.height + [UtilManager height:40], [UtilManager width:70], [UtilManager height:20])];
    [_fiberLabel setText:NSLocalizedString(@"Fiber:", nil)];
    [_fiberLabel setTextColor:GRAY_REGISTER_FONT];
    [_fiberLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]];
    
    [firstView addSubview:_fiberLabel];
    
    _fiberValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(_fiberLabel.frame.origin.x + _fiberLabel.frame.size.width,_fiberLabel.frame.origin.y, [UtilManager width:70], _fiberLabel.frame.size.height)];
    [_fiberValueLabel setTextColor:[UIColor whiteColor]];
    [_fiberValueLabel setText:[NSString stringWithFormat:@"%.2f g", [NutritionPlan getNutritionPlanFiber:_currentNutritionPlan]]];
    [_fiberValueLabel setAdjustsFontSizeToFitWidth:YES];
    [_fiberValueLabel setTextAlignment:NSTextAlignmentRight];
    
    [firstView addSubview:_fiberValueLabel];
    
    UIView *fiberSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(_fiberLabel.frame.origin.x, _fiberLabel.frame.origin.y + _fiberLabel.frame.size.height + [UtilManager height:5], _fiberLabel.frame.size.width + _fiberValueLabel.frame.size.width + [UtilManager width:10], 1)];
    [fiberSeparatorView setBackgroundColor:GRAY_REGISTER_FONT];
    [firstView addSubview:fiberSeparatorView];
    
    UILabel *bcaaLabel = [[UILabel alloc] initWithFrame:CGRectMake(_fiberLabel.frame.origin.x,_fiberLabel.frame.origin.y + _fiberLabel.frame.size.height, [UtilManager width:60], [UtilManager height:20])];
    [bcaaLabel setText:NSLocalizedString(@"BCAAs:", nil)];
    [bcaaLabel setTextColor:[UIColor whiteColor]];
    [bcaaLabel setAdjustsFontSizeToFitWidth:YES];
    
    //[firstView addSubview:bcaaLabel];
    
    _bcaaValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(bcaaLabel.frame.origin.x + bcaaLabel.frame.size.width,bcaaLabel.frame.origin.y, [UtilManager width:50], bcaaLabel.frame.size.height)];
    [_bcaaValueLabel setTextColor:[UIColor whiteColor]];
    [_bcaaValueLabel setText:[NSString stringWithFormat:@"%.2f g", [NutritionPlan getNutritionPlanBccas:_currentNutritionPlan]]];
    
    //[firstView addSubview:_bcaaValueLabel];
    
    UILabel *glutLabel = [[UILabel alloc] initWithFrame:CGRectMake(_fiberLabel.frame.origin.x,_bcaaValueLabel.frame.origin.y + _bcaaValueLabel.frame.size.height, [UtilManager width:85], [UtilManager height:20])];
    [glutLabel setText:NSLocalizedString(@"Glutamina:", nil)];
    [glutLabel setTextColor:[UIColor whiteColor]];
    
    //[firstView addSubview:glutLabel];
    
    _glutValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(glutLabel.frame.origin.x + glutLabel.frame.size.width,glutLabel.frame.origin.y, [UtilManager width:80], bcaaLabel.frame.size.height)];
    [_glutValueLabel setTextColor:[UIColor whiteColor]];
    [_glutValueLabel setText:[NSString stringWithFormat:@"%.2f g", [NutritionPlan getNutritionPlanGlutamine:_currentNutritionPlan]]];
    
    //[firstView addSubview:_glutValueLabel];
    
    UILabel *ome3Label = [[UILabel alloc] initWithFrame:CGRectMake(_fiberLabel.frame.origin.x,_glutValueLabel.frame.origin.y + _glutValueLabel.frame.size.height, [UtilManager width:85], [UtilManager height:20])];
    [ome3Label setText:NSLocalizedString(@"Omega-3:", nil)];
    [ome3Label setTextColor:[UIColor whiteColor]];
    
    //[firstView addSubview:ome3Label];
    
    _ome3ValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(ome3Label.frame.origin.x + ome3Label.frame.size.width,ome3Label.frame.origin.y, [UtilManager width:70], bcaaLabel.frame.size.height)];
    [_ome3ValueLabel setTextColor:[UIColor whiteColor]];
    [_ome3ValueLabel setText:[NSString stringWithFormat:@"%.2f g", [NutritionPlan getNutritionPlanOmega3:_currentNutritionPlan]]];
    
    //[firstView addSubview:_ome3ValueLabel];
    
    UILabel *ome6Label = [[UILabel alloc] initWithFrame:CGRectMake(_fiberLabel.frame.origin.x,_ome3ValueLabel.frame.origin.y + _ome3ValueLabel.frame.size.height, ome3Label.frame.size.width, [UtilManager height:20])];
    [ome6Label setText:NSLocalizedString(@"Omega-6:", nil)];
    [ome6Label setTextColor:[UIColor whiteColor]];
    
    //[firstView addSubview:ome6Label];
    
    _ome6ValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(ome6Label.frame.origin.x + ome6Label.frame.size.width,ome6Label.frame.origin.y, _ome3ValueLabel.frame.size.width, bcaaLabel.frame.size.height)];
    [_ome6ValueLabel setTextColor:[UIColor whiteColor]];
    [_ome6ValueLabel setText:[NSString stringWithFormat:@"%.2f g", [NutritionPlan getNutritionPlanOmega6:_currentNutritionPlan]]];
    
    //[firstView addSubview:_ome6ValueLabel];
    
    UILabel *waterLabel = [[UILabel alloc] initWithFrame:CGRectMake(_fiberLabel.frame.origin.x,fiberSeparatorView.frame.origin.y + [UtilManager height:5], [UtilManager width:70], [UtilManager height:20])];
    [waterLabel setText:NSLocalizedString(@"Water:", nil)];
    [waterLabel setTextColor:[UIColor whiteColor]];
    [waterLabel setAdjustsFontSizeToFitWidth:YES];
    [waterLabel setTextColor:_fiberLabel.textColor];
    [waterLabel setFont:_fiberLabel.font];
    
    [firstView addSubview:waterLabel];
    
    _waterValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(waterLabel.frame.origin.x + waterLabel.frame.size.width,waterLabel.frame.origin.y, [UtilManager width:70], bcaaLabel.frame.size.height)];
    [_waterValueLabel setTextColor:[UIColor whiteColor]];
    [_waterValueLabel setText:[NSString stringWithFormat:@"%.2f g", [NutritionPlan getNutritionPlanWater:_currentNutritionPlan]]];
    [_waterValueLabel setAdjustsFontSizeToFitWidth:YES];
    [_waterValueLabel setTextAlignment:_fiberValueLabel.textAlignment];
    
    [firstView addSubview:_waterValueLabel];
    
    UIView *waterSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(_fiberLabel.frame.origin.x, _waterValueLabel.frame.origin.y + _waterValueLabel.frame.size.height + [UtilManager height:5], fiberSeparatorView.frame.size.width, 1)];
    [waterSeparatorView setBackgroundColor:GRAY_REGISTER_FONT];
    [firstView addSubview:waterSeparatorView];
    
    UILabel *alcoholLabel = [[UILabel alloc] initWithFrame:CGRectMake(_fiberLabel.frame.origin.x,waterSeparatorView.frame.origin.y  + [UtilManager height:5], [UtilManager width:70], [UtilManager height:20])];
    [alcoholLabel setText:NSLocalizedString(@"Alcohol:", nil)];
    [alcoholLabel setTextColor:_fiberLabel.textColor];
    [alcoholLabel setFont:_fiberLabel.font];
    
    [firstView addSubview:alcoholLabel];
    
    _alcoholValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(alcoholLabel.frame.origin.x + alcoholLabel.frame.size.width,alcoholLabel.frame.origin.y, [UtilManager width:70], bcaaLabel.frame.size.height)];
    [_alcoholValueLabel setTextColor:[UIColor whiteColor]];
    [_alcoholValueLabel setText:[NSString stringWithFormat:@"%.2f g", [NutritionPlan getNutritionPlanAlcohol:_currentNutritionPlan]]];
    [_alcoholValueLabel setTextAlignment:_fiberValueLabel.textAlignment];
    
    [firstView addSubview:_alcoholValueLabel];
    
    UIView *alcoholSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(_fiberLabel.frame.origin.x, _alcoholValueLabel.frame.origin.y + _alcoholValueLabel.frame.size.height + [UtilManager height:5], fiberSeparatorView.frame.size.width, 1)];
    [alcoholSeparatorView setBackgroundColor:GRAY_REGISTER_FONT];
    [firstView addSubview:alcoholSeparatorView];
    
    
    return firstView;
}

- (UIView *)secondView {
    UIView *secondView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width, 0., self.frame.size.width, self.frame.size.height)];
    
    ////
    
    UILabel *titleSecondLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], _kCalLabel.frame.origin.y, [UtilManager width:200], [UtilManager height:24])];
    [titleSecondLabel setText:NSLocalizedString(@"Minerales & Vitaminas", nil)];
    [titleSecondLabel setTextColor:YELLOW_APP_COLOR];
    [titleSecondLabel setFont:[UIFont fontWithName:REGULAR_FONT size:16]];
    
    [secondView addSubview:titleSecondLabel];
    
    ///// First Column
    
    UILabel *calciumLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:10], titleSecondLabel.frame.origin.y + titleSecondLabel.frame.size.height + [UtilManager height:20], [UtilManager width:70], [UtilManager height:20])];
    [calciumLabel setText:NSLocalizedString(@"Calcium:", nil)];
    [calciumLabel setTextColor:_fiberLabel.textColor];
    [calciumLabel setAdjustsFontSizeToFitWidth:YES];
    [calciumLabel setFont:_fiberLabel.font];
    
    
    [secondView addSubview:calciumLabel];
    
    _calciumValue = [[UILabel alloc] initWithFrame:CGRectMake(calciumLabel.frame.origin.x + calciumLabel.frame.size.width, calciumLabel.frame.origin.y, [UtilManager width:80], [UtilManager height:20])];
    [_calciumValue setText:[NSString stringWithFormat:@"%.2f mg", [NutritionPlan getKCalInNutritionPlan:_currentNutritionPlan]]];
    [_calciumValue setTextColor:[UIColor whiteColor]];
    [_calciumValue setAdjustsFontSizeToFitWidth:YES];
    
    [_calciumValue setFont:calciumLabel.font];
    
    [secondView addSubview:_calciumValue];
    
    UILabel *ironLabel = [[UILabel alloc] initWithFrame:CGRectMake(calciumLabel.frame.origin.x, calciumLabel.frame.size.height + calciumLabel.frame.origin.y + [UtilManager height:8], calciumLabel.frame.size.width, calciumLabel.frame.size.height)];
    [ironLabel setText:NSLocalizedString(@"Iron:", nil)];
    [ironLabel setAdjustsFontSizeToFitWidth:YES];
    [ironLabel setFont:calciumLabel.font];
    [ironLabel setTextColor:calciumLabel.textColor];
    
    [secondView addSubview:ironLabel];
    
    _ironValue = [[UILabel alloc] initWithFrame:CGRectMake(ironLabel.frame.origin.x + ironLabel.frame.size.width, ironLabel.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_ironValue setText:[NSString stringWithFormat:@"%.2f mg", [NutritionPlan getNutritionPlanIron:_currentNutritionPlan]]];
    [_ironValue setTextColor:[UIColor whiteColor]];
    [_ironValue setAdjustsFontSizeToFitWidth:YES];
    [_ironValue setFont:calciumLabel.font];
    
    [secondView addSubview:_ironValue];
    
    UILabel *iodoLabel = [[UILabel alloc] initWithFrame:CGRectMake(calciumLabel.frame.origin.x, ironLabel.frame.size.height + ironLabel.frame.origin.y+ [UtilManager height:8], calciumLabel.frame.size.width, calciumLabel.frame.size.height)];
    [iodoLabel setText:NSLocalizedString(@"Iodo:", nil)];
    [iodoLabel setTextColor:_fiberLabel.textColor];
    [iodoLabel setAdjustsFontSizeToFitWidth:YES];
    [iodoLabel setFont:calciumLabel.font];
    
    [secondView addSubview:iodoLabel];
    
    _iodoValue = [[UILabel alloc] initWithFrame:CGRectMake(iodoLabel.frame.origin.x + iodoLabel.frame.size.width, iodoLabel.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_iodoValue setText:[NSString stringWithFormat:@"%.2f ng", [NutritionPlan getNutritionPlanIodine:_currentNutritionPlan]]];
    [_iodoValue setTextColor:[UIColor whiteColor]];
    [_iodoValue setAdjustsFontSizeToFitWidth:YES];
    [_iodoValue setFont:calciumLabel.font];
    
    [secondView addSubview:_iodoValue];
    
    UILabel *magnesiumLabel = [[UILabel alloc] initWithFrame:CGRectMake(calciumLabel.frame.origin.x, iodoLabel.frame.size.height + iodoLabel.frame.origin.y+ [UtilManager height:8], [UtilManager width:98], calciumLabel.frame.size.height)];
    [magnesiumLabel setText:NSLocalizedString(@"Magnesium:", nil)];
    [magnesiumLabel setTextColor:_fiberLabel.textColor];
    [magnesiumLabel setAdjustsFontSizeToFitWidth:YES];
    [magnesiumLabel setFont:calciumLabel.font];
    
    [secondView addSubview:magnesiumLabel];
    
    _magnesiumValue = [[UILabel alloc] initWithFrame:CGRectMake(magnesiumLabel.frame.origin.x + magnesiumLabel.frame.size.width, magnesiumLabel.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_magnesiumValue setText:[NSString stringWithFormat:@"%.2f mg", [NutritionPlan getNutritionPlanMagnesium:_currentNutritionPlan]]];
    [_magnesiumValue setTextColor:[UIColor whiteColor]];
    [_magnesiumValue setAdjustsFontSizeToFitWidth:YES];
    [_magnesiumValue setFont:calciumLabel.font];
    
    [secondView addSubview:_magnesiumValue];
    
    UILabel *zincLabel = [[UILabel alloc] initWithFrame:CGRectMake(calciumLabel.frame.origin.x, magnesiumLabel.frame.size.height + magnesiumLabel.frame.origin.y+ [UtilManager height:8], calciumLabel.frame.size.width, calciumLabel.frame.size.height)];
    [zincLabel setText:NSLocalizedString(@"Zinc:", nil)];
    [zincLabel setTextColor:_fiberLabel.textColor];
    [zincLabel setAdjustsFontSizeToFitWidth:YES];
    [zincLabel setFont:calciumLabel.font];
    
    [secondView addSubview:zincLabel];
    
    _zincValue = [[UILabel alloc] initWithFrame:CGRectMake(zincLabel.frame.origin.x + zincLabel.frame.size.width, zincLabel.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_zincValue setText:[NSString stringWithFormat:@"%.2f mg", [NutritionPlan getNutritionPlanZinc:_currentNutritionPlan]]];
    [_zincValue setTextColor:[UIColor whiteColor]];
    [_zincValue setAdjustsFontSizeToFitWidth:YES];
    [_zincValue setFont:calciumLabel.font];
    
    [secondView addSubview:_zincValue];
    
    UILabel *seleniumLabel = [[UILabel alloc] initWithFrame:CGRectMake(calciumLabel.frame.origin.x, zincLabel.frame.size.height + zincLabel.frame.origin.y+ [UtilManager height:8], [UtilManager width:80], calciumLabel.frame.size.height)];
    [seleniumLabel setText:NSLocalizedString(@"Selenium:", nil)];
    [seleniumLabel setTextColor:_fiberLabel.textColor];
    [seleniumLabel setAdjustsFontSizeToFitWidth:YES];
    [seleniumLabel setFont:calciumLabel.font];
    
    [secondView addSubview:seleniumLabel];
    
    _seleniumValue = [[UILabel alloc] initWithFrame:CGRectMake(seleniumLabel.frame.origin.x + seleniumLabel.frame.size.width, seleniumLabel.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_seleniumValue setText:[NSString stringWithFormat:@"%.2f ng", [NutritionPlan getNutritionPlanSelenium:_currentNutritionPlan]]];
    [_seleniumValue setTextColor:[UIColor whiteColor]];
    [_seleniumValue setAdjustsFontSizeToFitWidth:YES];
    [_seleniumValue setFont:calciumLabel.font];
    
    [secondView addSubview:_seleniumValue];
    
    UILabel *sodiumLabel = [[UILabel alloc] initWithFrame:CGRectMake(calciumLabel.frame.origin.x, seleniumLabel.frame.size.height + seleniumLabel.frame.origin.y+ [UtilManager height:8], [UtilManager width:66], calciumLabel.frame.size.height)];
    [sodiumLabel setText:NSLocalizedString(@"Sodium:", nil)];
    [sodiumLabel setTextColor:_fiberLabel.textColor];
    [sodiumLabel setAdjustsFontSizeToFitWidth:YES];
    [sodiumLabel setFont:calciumLabel.font];
    
    [secondView addSubview:sodiumLabel];
    
    _sodiumValue = [[UILabel alloc] initWithFrame:CGRectMake(sodiumLabel.frame.origin.x + sodiumLabel.frame.size.width, sodiumLabel.frame.origin.y,_calciumValue.frame.size.width, [UtilManager height:20])];
    [_sodiumValue setText:[NSString stringWithFormat:@"%.2f mg", [NutritionPlan getNutritionPlanSodium:_currentNutritionPlan]]];
    [_sodiumValue setTextColor:[UIColor whiteColor]];
    [_sodiumValue setAdjustsFontSizeToFitWidth:YES];
    [_sodiumValue setFont:calciumLabel.font];
    
    [secondView addSubview:_sodiumValue];
    
    UILabel *potassiumLabel = [[UILabel alloc] initWithFrame:CGRectMake(calciumLabel.frame.origin.x, sodiumLabel.frame.size.height + sodiumLabel.frame.origin.y+ [UtilManager height:8], [UtilManager width:90], calciumLabel.frame.size.height)];
    [potassiumLabel setText:NSLocalizedString(@"Potassium:", nil)];
    [potassiumLabel setTextColor:_fiberLabel.textColor];
    [potassiumLabel setAdjustsFontSizeToFitWidth:YES];
    [potassiumLabel setFont:calciumLabel.font];
    
    [secondView addSubview:potassiumLabel];
    
    _potassiumValue = [[UILabel alloc] initWithFrame:CGRectMake(potassiumLabel.frame.origin.x + potassiumLabel.frame.size.width, potassiumLabel.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_potassiumValue setText:[NSString stringWithFormat:@"%.2f g", [NutritionPlan getNutritionPlanPotasium:_currentNutritionPlan]]];
    [_potassiumValue setTextColor:[UIColor whiteColor]];
    [_potassiumValue setAdjustsFontSizeToFitWidth:YES];
    [_potassiumValue setFont:calciumLabel.font];
    
    [secondView addSubview:_potassiumValue];
    
    UILabel *phosphorusLabel = [[UILabel alloc] initWithFrame:CGRectMake(calciumLabel.frame.origin.x, potassiumLabel.frame.size.height + potassiumLabel.frame.origin.y + [UtilManager height:8] , [UtilManager width:105], calciumLabel.frame.size.height)];
    [phosphorusLabel setText:NSLocalizedString(@"Phosphorus:", nil)];
    [phosphorusLabel setTextColor:_fiberLabel.textColor];
    [phosphorusLabel setAdjustsFontSizeToFitWidth:YES];
    [phosphorusLabel setFont:calciumLabel.font];
    
    [secondView addSubview:phosphorusLabel];
    
    _phosphorusValue = [[UILabel alloc] initWithFrame:CGRectMake(phosphorusLabel.frame.origin.x + phosphorusLabel.frame.size.width, phosphorusLabel.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_phosphorusValue setText:[NSString stringWithFormat:@"%.2f g", [NutritionPlan getNutritionPlanPhosphorus:_currentNutritionPlan]]];
    [_phosphorusValue setTextColor:[UIColor whiteColor]];
    [_phosphorusValue setAdjustsFontSizeToFitWidth:YES];
    [_phosphorusValue setFont:calciumLabel.font];
    
    [secondView addSubview:_phosphorusValue];
    
    ///// Second Column
    
    UILabel *b1Label = [[UILabel alloc] initWithFrame:CGRectMake(self.center.x, calciumLabel.frame.origin.y, [UtilManager width:50], [UtilManager height:20])];
    [b1Label setText:NSLocalizedString(@"B1:", nil)];
    [b1Label setTextColor:_fiberLabel.textColor];
    [b1Label setAdjustsFontSizeToFitWidth:YES];
    [b1Label setFont:calciumLabel.font];
    
    [secondView addSubview:b1Label];
    
    _b1Value = [[UILabel alloc] initWithFrame:CGRectMake(b1Label.frame.origin.x + b1Label.frame.size.width, b1Label.frame.origin.y,_calciumValue.frame.size.width, [UtilManager height:20])];
    [_b1Value setText:[NSString stringWithFormat:@"%.3f mg", [NutritionPlan getNutritionPlanVitB1:_currentNutritionPlan]]];
    [_b1Value setTextColor:[UIColor whiteColor]];
    [_b1Value setAdjustsFontSizeToFitWidth:YES];
    [_b1Value setFont:calciumLabel.font];
    
    [secondView addSubview:_b1Value];
    
    UILabel *b2Label = [[UILabel alloc] initWithFrame:CGRectMake(b1Label.frame.origin.x, ironLabel.frame.origin.y, calciumLabel.frame.size.width, calciumLabel.frame.size.height)];
    [b2Label setText:NSLocalizedString(@"B2:", nil)];
    [b2Label setTextColor:_fiberLabel.textColor];
    [b2Label setAdjustsFontSizeToFitWidth:YES];
    [b2Label setFont:calciumLabel.font];
    
    [secondView addSubview:b2Label];
    
    _b2Value = [[UILabel alloc] initWithFrame:CGRectMake(b2Label.frame.origin.x + b2Label.frame.size.width, b2Label.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_b2Value setText:[NSString stringWithFormat:@"%.3f mg", [NutritionPlan getNutritionPlanVitB2:_currentNutritionPlan]]];
    [_b2Value setTextColor:[UIColor whiteColor]];
    [_b2Value setAdjustsFontSizeToFitWidth:YES];
    [_b2Value setFont:calciumLabel.font];
    
    [secondView addSubview:_b2Value];
    
    UILabel *niacinLabel = [[UILabel alloc] initWithFrame:CGRectMake(b1Label.frame.origin.x, iodoLabel.frame.origin.y, calciumLabel.frame.size.width, calciumLabel.frame.size.height)];
    [niacinLabel setText:NSLocalizedString(@"Niacin:", nil)];
    [niacinLabel setTextColor:_fiberLabel.textColor];
    [niacinLabel setAdjustsFontSizeToFitWidth:YES];
    [niacinLabel setFont:calciumLabel.font];
    
    [secondView addSubview:niacinLabel];
    
    _niacinValue = [[UILabel alloc] initWithFrame:CGRectMake(niacinLabel.frame.origin.x + niacinLabel.frame.size.width, niacinLabel.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_niacinValue setText:[NSString stringWithFormat:@"%d g", (int)[NutritionPlan getNutritionPlanNiacin:_currentNutritionPlan]]];
    [_niacinValue setTextColor:[UIColor whiteColor]];
    [_niacinValue setAdjustsFontSizeToFitWidth:YES];
    [_niacinValue setFont:calciumLabel.font];
    
    [secondView addSubview:_niacinValue];
    
    UILabel *ALabel = [[UILabel alloc] initWithFrame:CGRectMake(b1Label.frame.origin.x, magnesiumLabel.frame.origin.y, calciumLabel.frame.size.width, calciumLabel.frame.size.height)];
    [ALabel setText:NSLocalizedString(@"A:", nil)];
    [ALabel setTextColor:_fiberLabel.textColor];
    [ALabel setAdjustsFontSizeToFitWidth:YES];
    [ALabel setFont:calciumLabel.font];
    
    [secondView addSubview:ALabel];
    
    _AValue = [[UILabel alloc] initWithFrame:CGRectMake(ALabel.frame.origin.x + ALabel.frame.size.width, ALabel.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_AValue setText:[NSString stringWithFormat:@"%d g", (int)[NutritionPlan getNutritionPlanVitA:_currentNutritionPlan]]];
    [_AValue setTextColor:[UIColor whiteColor]];
    [_AValue setAdjustsFontSizeToFitWidth:YES];
    [_AValue setFont:calciumLabel.font];
    
    [secondView addSubview:_AValue];
    
    UILabel *folicLabel = [[UILabel alloc] initWithFrame:CGRectMake(b1Label.frame.origin.x, zincLabel.frame.origin.y, [UtilManager width:100], calciumLabel.frame.size.height)];
    [folicLabel setText:NSLocalizedString(@"Folic ac.:", nil)];
    [folicLabel setTextColor:_fiberLabel.textColor];
    [folicLabel setAdjustsFontSizeToFitWidth:YES];
    [folicLabel setFont:calciumLabel.font];
    
    [secondView addSubview:folicLabel];
    
    _folicValue = [[UILabel alloc] initWithFrame:CGRectMake(folicLabel.frame.origin.x + folicLabel.frame.size.width, folicLabel.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_folicValue setText:[NSString stringWithFormat:@"%d g", (int)[NutritionPlan getNutritionPlanFolicAcid:_currentNutritionPlan]]];
    [_folicValue setTextColor:[UIColor whiteColor]];
    [_folicValue setAdjustsFontSizeToFitWidth:YES];
    [_folicValue setFont:calciumLabel.font];
    
    [secondView addSubview:_folicValue];
    
    UILabel *b12Label = [[UILabel alloc] initWithFrame:CGRectMake(b1Label.frame.origin.x, seleniumLabel.frame.origin.y, calciumLabel.frame.size.width, calciumLabel.frame.size.height)];
    [b12Label setText:NSLocalizedString(@"B12:", nil)];
    [b12Label setTextColor:_fiberLabel.textColor];
    [b12Label setAdjustsFontSizeToFitWidth:YES];
    [b12Label setFont:calciumLabel.font];
    
    [secondView addSubview:b12Label];
    
    _b12Value = [[UILabel alloc] initWithFrame:CGRectMake(b12Label.frame.origin.x + b12Label.frame.size.width, b12Label.frame.origin.y,_calciumValue.frame.size.width, [UtilManager height:20])];
    [_b12Value setText:[NSString stringWithFormat:@"%d g", (int)[NutritionPlan getNutritionPlanVitB12:_currentNutritionPlan]]];
    [_b12Value setTextColor:[UIColor whiteColor]];
    [_b12Value setAdjustsFontSizeToFitWidth:YES];
    [_b12Value setFont:calciumLabel.font];
    
    [secondView addSubview:_b12Value];
    
    UILabel *cLabel = [[UILabel alloc] initWithFrame:CGRectMake(b1Label.frame.origin.x, sodiumLabel.frame.origin.y, calciumLabel.frame.size.width, calciumLabel.frame.size.height)];
    [cLabel setText:NSLocalizedString(@"C:", nil)];
    [cLabel setTextColor:_fiberLabel.textColor];
    [cLabel setAdjustsFontSizeToFitWidth:YES];
    [cLabel setFont:calciumLabel.font];
    
    [secondView addSubview:cLabel];
    
    _cValue = [[UILabel alloc] initWithFrame:CGRectMake(cLabel.frame.origin.x + cLabel.frame.size.width, cLabel.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_cValue setText:[NSString stringWithFormat:@"%d g", (int)[NutritionPlan getNutritionPlanVitC:_currentNutritionPlan]]];
    [_cValue setTextColor:[UIColor whiteColor]];
    [_cValue setAdjustsFontSizeToFitWidth:YES];
    [_cValue setFont:calciumLabel.font];
    
    [secondView addSubview:_cValue];
    
    UILabel *dLabel = [[UILabel alloc] initWithFrame:CGRectMake(b1Label.frame.origin.x, potassiumLabel.frame.origin.y, calciumLabel.frame.size.width, calciumLabel.frame.size.height)];
    [dLabel setText:NSLocalizedString(@"D:", nil)];
    [dLabel setTextColor:_fiberLabel.textColor];
    [dLabel setAdjustsFontSizeToFitWidth:TRUE];
    [dLabel setFont:calciumLabel.font];
    
    [secondView addSubview:dLabel];
    
    _dValue = [[UILabel alloc] initWithFrame:CGRectMake(dLabel.frame.origin.x + dLabel.frame.size.width, dLabel.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_dValue setText:[NSString stringWithFormat:@"%d g", (int)[NutritionPlan getNutritionPlanVitD:_currentNutritionPlan]]];
    [_dValue setTextColor:[UIColor whiteColor]];
    [_dValue setAdjustsFontSizeToFitWidth:TRUE];
    [_dValue setFont:calciumLabel.font];
    
    [secondView addSubview:_dValue];
    
    UILabel *eLabel = [[UILabel alloc] initWithFrame:CGRectMake(b1Label.frame.origin.x, phosphorusLabel.frame.origin.y, calciumLabel.frame.size.width, calciumLabel.frame.size.height)];
    [eLabel setText:NSLocalizedString(@"E:", nil)];
    [eLabel setTextColor:_fiberLabel.textColor];
    [eLabel setAdjustsFontSizeToFitWidth:TRUE];
    [eLabel setFont:calciumLabel.font];
    
    [secondView addSubview:eLabel];
    
    _eValue = [[UILabel alloc] initWithFrame:CGRectMake(eLabel.frame.origin.x + eLabel.frame.size.width, eLabel.frame.origin.y, _calciumValue.frame.size.width, [UtilManager height:20])];
    [_eValue setText:[NSString stringWithFormat:@"%d g", (int)[NutritionPlan getNutritionPlanVitD:_currentNutritionPlan]]];
    [_eValue setTextColor:[UIColor whiteColor]];
    [_eValue setAdjustsFontSizeToFitWidth:TRUE];
    [_eValue setFont:calciumLabel.font];
    
    [secondView addSubview:_eValue];
    
    return secondView;
}

- (UIView *)thirdView{
    UIView *thirdView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width * 2, 0., self.frame.size.width, self.frame.size.height)];
    [thirdView setBackgroundColor:[UIColor clearColor]];
    
    return thirdView;
}

- (void)reloadViews{
    
}


// at the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = CGRectGetWidth(self.scrollView.frame);
    NSUInteger page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
    
    // a possible optimization would be to unload the views+controllers which are no longer visible
}

//- (void)inputMeasureValue{
//    [_quantityValue resignFirstResponder];
//
//    if([_measureValue.text isEqualToString:[NSString stringWithFormat:@"  %@",_currentAliment.measureUnit]]){
//        [_measureValue setText:[NSString stringWithFormat:@"  %@",_currentAliment.conversionString]];
//        if(_quantityValue.text.intValue%_currentAliment.conversionFactor.intValue != 0)
//            [_quantityValue setText:[NSString stringWithFormat:@"%d",_quantityValue.text.intValue/_currentAliment.conversionFactor.intValue + 1]];
//        else
//            [_quantityValue setText:[NSString stringWithFormat:@"%d",_quantityValue.text.intValue/_currentAliment.conversionFactor.intValue]];
//        _quantity = [NSNumber numberWithInteger:_quantityValue.text.intValue * _currentAliment.conversionFactor.intValue];
//    }else{
//        [_measureValue setText:[NSString stringWithFormat:@"  %@",_currentAliment.measureUnit]];
//        [_quantityValue setText:[NSString stringWithFormat:@"%d",_quantityValue.text.intValue * _currentAliment.conversionFactor.intValue]];
//        _quantity = [NSNumber numberWithInteger:_quantityValue.text.intValue];
//    }
//
//    if([_delegate respondsToSelector:@selector(quantity:inIndex:)])
//        [_delegate quantity:_quantity inIndex:_index];
//
//    float multiplicator = _quantity.floatValue / 100;
//    [self recalculateAllValues:multiplicator];
//}

- (void)inputQuantityValue{
    [_quantityValue becomeFirstResponder];
}

- (void)resetView{
    [_quantityValue resignFirstResponder];
}

//- (void)recalculateAllValues:(CGFloat)multiplicator{
//    [_kCalLabel setText:[NSString stringWithFormat:@"%.0f kCal",_currentAliment.calories.floatValue * multiplicator]];
//
//    [[_proteinCircleView countLabel] setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.proteins.intValue * multiplicator]];
//    [[_carbsCircleView countLabel] setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.carbs.intValue * multiplicator]];
//    [[_fatsCircleView countLabel] setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.fats.intValue * multiplicator]];
//
//    [[_sugarCircleView countLabel] setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.sugar.intValue * multiplicator]];
//    [[_satFatsCircleView countLabel] setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.saturedFats.intValue * multiplicator]];
//    [[_monFatsCircleView countLabel] setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.monoUnsaturatedFats.intValue * multiplicator]];
//    [[_polFatsCircleView countLabel] setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.polyUnsaturatedFats.intValue * multiplicator]];
//    [_fiberValueLabel setText:[NSString stringWithFormat:@"%.2f g",_currentAliment.fiber.doubleValue * multiplicator]];
//    [_bcaaValueLabel setText:[NSString stringWithFormat:@"%.2f g",_currentAliment.bccas.doubleValue * multiplicator]];
//    [_glutValueLabel setText:[NSString stringWithFormat:@"%.2f g",_currentAliment.glutamine.doubleValue *multiplicator]];
//    [_ome3ValueLabel setText:[NSString stringWithFormat:@"%.2f g",_currentAliment.omega3.doubleValue * multiplicator]];
//    [_ome6ValueLabel setText:[NSString stringWithFormat:@"%.2f g",_currentAliment.omega6.doubleValue * multiplicator]];
//    [_waterValueLabel setText:[NSString stringWithFormat:@"%.2f g",_currentAliment.water.doubleValue * multiplicator]];
//    [_alcoholValueLabel setText:[NSString stringWithFormat:@"%.2f g",_currentAliment.alcohol.doubleValue * multiplicator]];
//
//    [_calciumValue setText:[NSString stringWithFormat:@"%.2f mg",_currentAliment.calcium.floatValue * multiplicator]];
//    [_ironValue setText:[NSString stringWithFormat:@"%.2f mg",_currentAliment.iron.floatValue * multiplicator]];
//    [_iodoValue setText:[NSString stringWithFormat:@"%.2f ng",_currentAliment.iodine.floatValue * multiplicator]];
//    [_magnesiumValue setText:[NSString stringWithFormat:@"%.2f mg",_currentAliment.magnesium.floatValue * multiplicator]];
//    [_zincValue setText:[NSString stringWithFormat:@"%.2f mg",_currentAliment.zinc.floatValue *multiplicator]];
//    [_seleniumValue setText:[NSString stringWithFormat:@"%.2f ng",_currentAliment.selenium.floatValue *multiplicator]];
//    [_sodiumValue setText:[NSString stringWithFormat:@"%.2f mg",_currentAliment.sodium.floatValue *multiplicator]];
//    [_potassiumValue setText:[NSString stringWithFormat:@"%.2f g",_currentAliment.potasium.floatValue *multiplicator]];
//    [_phosphorusValue setText:[NSString stringWithFormat:@"%.2f g",_currentAliment.phosphorus.floatValue *multiplicator]];
//    [_b1Value setText:[NSString stringWithFormat:@"%.3f mg",_currentAliment.vitB1.floatValue *multiplicator]];
//    [_b2Value setText:[NSString stringWithFormat:@"%.3f mg",_currentAliment.vitB2.floatValue *multiplicator]];
//    [_niacinValue setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.niacin.intValue *multiplicator]];
//    [_AValue setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.vitA.intValue *multiplicator]];
//    [_folicValue setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.folicAcid.intValue *multiplicator]];
//    [_b12Value setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.vitB12.intValue *multiplicator]];
//    [_cValue setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.vitC.intValue *multiplicator]];
//    [_dValue setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.vitD.intValue *multiplicator]];
//    [_eValue setText:[NSString stringWithFormat:@"%.0f g",_currentAliment.vitD.intValue *multiplicator]];
//}
//
//#pragma mark UITextFieldDelegate Methods

//- (BOOL)textFieldShouldClear:(UITextField *)textField{
//    return TRUE;
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField{
//    return TRUE;
//}
//
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
//    [textField resignFirstResponder];
//
//    _quantity = [NSNumber numberWithInt:textField.text.intValue];
//    [_quantityValue setText:[NSString stringWithFormat:@"%@ ",textField.text]];
//
//    if(![_measureValue.text isEqualToString:[NSString stringWithFormat:@"  %@",_currentAliment.measureUnit]])
//        _quantity = [NSNumber numberWithInteger:_quantityValue.text.intValue * _currentAliment.conversionFactor.intValue];
//
//    if([_delegate respondsToSelector:@selector(quantity:inIndex:)])
//        [_delegate quantity:_quantity inIndex:_index];
//
//    float multiplicator = _quantity.floatValue / 100;
//    [self recalculateAllValues:multiplicator];
//
//    return TRUE;
//}
//
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
//
//    //NSRange range =[textField.text rangeOfString:[NSString stringWithFormat:@" %@",_currentAliment.measureUnit]];
//    //[textField setText:[textField.text substringToIndex:range.location]];
//
//    return TRUE;
//}
//
//- (void)textFieldDidBeginEditing:(UITextField *)textField{
//    [textField selectAll:self];
//    [UIMenuController sharedMenuController].menuVisible = NO;
//}

@end
