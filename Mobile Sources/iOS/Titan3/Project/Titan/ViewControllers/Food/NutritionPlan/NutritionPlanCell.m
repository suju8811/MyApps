//
//  NutritionPlanCell.m
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "NutritionPlanCell.h"
#import "AppConstants.h"
#import "UtilManager.h"

@implementation NutritionPlanCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.clipsToBounds = YES;
    self.contentView.clipsToBounds = YES;
    
    if(self){
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:100], [UtilManager height:10], WIDTH - [UtilManager width:100], [UtilManager height:20])];
        [_titleLabel setText:@"Sesión inicial nutrición"];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setTextColor:YELLOW_APP_COLOR];
        [_titleLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
        [_titleLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_titleLabel];
        
        
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        //[self.workoutImageView setHidden:TRUE];
        
        _dayLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:5], [UtilManager height:10], [UtilManager width:40], [UtilManager height:30])];
        [_dayLabel setText:@"Día 1"];
        [_dayLabel setTextColor:YELLOW_APP_COLOR];
        [_dayLabel setFont:[UIFont fontWithName:BOLD_FONT size:15]];
        [_dayLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_dayLabel];
        
        _circleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_dayLabel.frame.origin.x + _dayLabel.frame.size.width + [UtilManager width:5], _dayLabel.frame.origin.y, [UtilManager height:15], [UtilManager height:15])];
        [[_circleImageView layer] setBorderColor:YELLOW_APP_COLOR.CGColor];
        [[_circleImageView layer] setBorderWidth:3];
        [[_circleImageView layer] setCornerRadius:_circleImageView.frame.size.height/2];
        [_circleImageView setCenter:CGPointMake(_circleImageView.center.x, _dayLabel.center.y)];
        
        [[self contentView] addSubview:_circleImageView];
        
        _verticalSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(0, _circleImageView.frame.size.height + _circleImageView.frame.origin.y + [UtilManager height:5], 1, [UtilManager height:70])];
        [_verticalSeparatorView setBackgroundColor:[UIColor lightGrayColor]];
        [_verticalSeparatorView setCenter:CGPointMake(_circleImageView.center.x, _verticalSeparatorView.center.y)];
        
        [[self contentView] addSubview:_verticalSeparatorView];
        
        [self.titleLabel setFrame:CGRectMake(_circleImageView.frame.origin.x + _circleImageView.frame.size.width + [UtilManager width:15], _dayLabel.frame.origin.y, [UtilManager width:240], _dayLabel.frame.size.height)];
        [self.titleLabel setTextColor:[UIColor whiteColor]];
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake(self.titleLabel.frame.origin.x,
                                                             CGRectGetMaxY(self.titleLabel.frame),
                                                             WIDTH - self.titleLabel.frame.origin.x - [UtilManager width:50],
                                                             TAGVIEW_SMALL_HEIGHT)]; //andTags:tags withTagsControlMode:TagsControlModeList];
        
        [_tagView setMode:TagsControlModeList];
        _tagView.isCenter = NO;
        [_tagView setUserInteractionEnabled:FALSE];
        [_tagView reloadTagSubviews];
        
        [self.contentView addSubview:_tagView];
        
        _foodsLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x,
                                                                CGRectGetMaxY(_tagView.frame) + [UtilManager height:5],
                                                                [UtilManager width:90],
                                                                [UtilManager height:20])];
        
        [_foodsLabel setTextColor:GRAY_REGISTER_FONT];
        [_foodsLabel setText:@"1 Comida"];
        [_foodsLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [_foodsLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_foodsLabel];
        
        UIImageView *kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_foodsLabel.frame.origin.x + _foodsLabel.frame.size.width, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + [UtilManager height:10], [UtilManager width:20] * 0.75, [UtilManager width:20] * 0.75)];
        [kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-green"]];
        [kCalImageView setCenter:CGPointMake(kCalImageView.center.x, _foodsLabel.center.y)];
        
        [[self contentView] addSubview:kCalImageView];
        
        _kCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(kCalImageView.frame.origin.x + kCalImageView.frame.size.width + 5, _foodsLabel.frame.origin.y, [UtilManager width:60], _foodsLabel.frame.size.height)];
        [_kCalLabel  setCenter:CGPointMake(_kCalLabel.center.x, kCalImageView.center.y)];
        [_kCalLabel setTextColor:GREEN_APP_COLOR];
        [_kCalLabel setFont:_foodsLabel.font];
        [_kCalLabel setAdjustsFontSizeToFitWidth:YES];
        [_kCalLabel setText:@"1230 kCals"];
        
        [[self contentView] addSubview:_kCalLabel];
        
        _proteinLabel = [[UILabel alloc] initWithFrame:CGRectMake(_foodsLabel.frame.origin.x,
                                                                  _kCalLabel.frame.origin.y + _kCalLabel.frame.size.height + [UtilManager height:5],
                                                                  [UtilManager width:40],
                                                                  [UtilManager width:40])];
        
        [[_proteinLabel layer] setBorderColor:BLUE_PERCENT_CIRCLE.CGColor];
        [[_proteinLabel layer] setBorderWidth:1];
        [[_proteinLabel layer] setCornerRadius:_proteinLabel.frame.size.width/2];
        [_proteinLabel setText:@"p\n26 g"];
        [_proteinLabel setNumberOfLines:2];
        [_proteinLabel setTextColor:BLUE_PERCENT_CIRCLE];
        [_proteinLabel setTextAlignment:NSTextAlignmentCenter];
        
        _proteinCircleView = [[PropertyCircleView alloc] initWithFrame:_proteinLabel.frame];
        [[_proteinCircleView layer] setBorderColor:BLUE_PERCENT_CIRCLE.CGColor];
        [[_proteinCircleView layer] setBorderWidth:1];
        [[_proteinCircleView layer] setCornerRadius:_proteinLabel.frame.size.width/2];
        
        [[_proteinCircleView initialLabel] setText:@"p"];
        [[_proteinCircleView initialLabel] setTextColor:BLUE_PERCENT_CIRCLE];
        [[_proteinCircleView initialLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:10]];
        [[_proteinCircleView countLabel] setText:@"95"];
        [[_proteinCircleView countLabel] setTextColor:BLUE_PERCENT_CIRCLE];
        [[_proteinCircleView measureLabel] setTextColor:BLUE_PERCENT_CIRCLE];
        [[_proteinCircleView countLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:14]];
        [[_proteinCircleView measureLabel] setText:@"g"];
        [[_proteinCircleView measureLabel] setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:10]];
        
        [[self contentView] addSubview:_proteinCircleView];
        
        _carbsLabel = [[UILabel alloc] initWithFrame:CGRectMake(_proteinLabel.frame.origin.x + _proteinLabel.frame.size.width + [UtilManager width:10],
                                                                0.,
                                                                _proteinLabel.frame.size.width,
                                                                _proteinLabel.frame.size.height)];
        
        [[_carbsLabel layer] setBorderColor:YELLOW_PERCENT_CIRCLE.CGColor];
        [[_carbsLabel layer] setBorderWidth:1];
        [[_carbsLabel layer] setCornerRadius:_proteinLabel.frame.size.width/2];
        [_carbsLabel setText:@"p\n26 g"];
        [_carbsLabel setNumberOfLines:2];
        [_carbsLabel setCenter:CGPointMake(_carbsLabel.center.x, _proteinCircleView.center.y)];
        [_carbsLabel setTextColor:YELLOW_PERCENT_CIRCLE];
        [_carbsLabel setTextAlignment:NSTextAlignmentCenter];
        
        _carbsCircleView = [[PropertyCircleView alloc] initWithFrame:_carbsLabel.frame];
        [[_carbsCircleView layer] setBorderColor:YELLOW_PERCENT_CIRCLE.CGColor];
        [[_carbsCircleView layer] setBorderWidth:1];
        [[_carbsCircleView layer] setCornerRadius:_proteinLabel.frame.size.width/2];
        
        [[_carbsCircleView initialLabel] setText:@"c"];
        [[_carbsCircleView initialLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
        [[_carbsCircleView countLabel] setText:@"95"];
        [[_carbsCircleView countLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
        [[_carbsCircleView measureLabel] setTextColor:YELLOW_PERCENT_CIRCLE];
        [[_carbsCircleView measureLabel] setText:@"g"];
        [[_carbsCircleView initialLabel] setFont:[_proteinCircleView initialLabel].font];
        [[_carbsCircleView countLabel] setFont:[_proteinCircleView countLabel].font];
        [[_carbsCircleView measureLabel] setFont:[_proteinCircleView measureLabel].font];
        
        [[self contentView] addSubview:_carbsCircleView];
        
        _fatsLabel = [[UILabel alloc] initWithFrame:CGRectMake(_carbsLabel.frame.origin.x + _carbsLabel.frame.size.width + [UtilManager width:10],
                                                               0.,
                                                               _proteinLabel.frame.size.width,
                                                               _proteinLabel.frame.size.height)];
        
        [[_fatsLabel layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
        [[_fatsLabel layer] setBorderWidth:1];
        [[_fatsLabel layer] setCornerRadius:_proteinLabel.frame.size.width/2];
        [_fatsLabel setText:@"p\n26 g"];
        [_fatsLabel setNumberOfLines:2];
        [_fatsLabel setCenter:CGPointMake(_fatsLabel.center.x, _proteinCircleView.center.y)];
        [_fatsLabel setTextColor:ORANGE_PERCENT_CIRCLE];
        [_fatsLabel setTextAlignment:NSTextAlignmentCenter];
        
        _fatsCircleView = [[PropertyCircleView alloc] initWithFrame:_fatsLabel.frame];
        [[_fatsCircleView layer] setBorderColor:ORANGE_PERCENT_CIRCLE.CGColor];
        [[_fatsCircleView layer] setBorderWidth:1];
        [[_fatsCircleView layer] setCornerRadius:_proteinLabel.frame.size.width/2];
        
        [[_fatsCircleView initialLabel] setText:@"f"];
        [[_fatsCircleView initialLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
        [[_fatsCircleView countLabel] setText:@"95"];
        [[_fatsCircleView countLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
        [[_fatsCircleView measureLabel] setTextColor:ORANGE_PERCENT_CIRCLE];
        [[_fatsCircleView measureLabel] setText:@"g"];
        [[_fatsCircleView initialLabel] setFont:[_proteinCircleView initialLabel].font];
        [[_fatsCircleView countLabel] setFont:[_proteinCircleView countLabel].font];
        [[_fatsCircleView measureLabel] setFont:[_proteinCircleView measureLabel].font];
        
        [[self contentView] addSubview:_fatsCircleView];
        
        _menuButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50], [UtilManager height:10], [UtilManager width:35], [UtilManager width:35])];
        [_menuButton addTarget:self action:@selector(menuAction) forControlEvents:UIControlEventTouchUpInside];
        [_menuButton setImage:[UIImage imageNamed:@"btn-options"] forState:UIControlStateNormal];
        [_menuButton setCenter:CGPointMake(_menuButton.center.x, _tagView.center.y)];
        
        [[self contentView] addSubview:_menuButton];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        //[self.workoutImageView setHidden:TRUE];
        
        _dayLabel = [[UILabel alloc] initWithFrame:CGRectMake([UtilManager width:5], [UtilManager height:10], [UtilManager width:40], [UtilManager height:30])];
        [_dayLabel setText:@"Día 1"];
        [_dayLabel setTextColor:YELLOW_APP_COLOR];
        [_dayLabel setFont:[UIFont fontWithName:BOLD_FONT size:15]];
        [_dayLabel setAdjustsFontSizeToFitWidth:YES];
        
        [[self contentView] addSubview:_dayLabel];
        
        _circleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_dayLabel.frame.origin.x + _dayLabel.frame.size.width + [UtilManager width:5], _dayLabel.frame.origin.y, [UtilManager height:15], [UtilManager height:15])];
        [[_circleImageView layer] setBorderColor:YELLOW_APP_COLOR.CGColor];
        [[_circleImageView layer] setBorderWidth:3];
        [[_circleImageView layer] setCornerRadius:_circleImageView.frame.size.height/2];
        [_circleImageView setCenter:CGPointMake(_circleImageView.center.x, _dayLabel.center.y)];
        
        [[self contentView] addSubview:_circleImageView];
        
        _verticalSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(0, _circleImageView.frame.size.height + _circleImageView.frame.origin.y + [UtilManager height:5], 1, [UtilManager height:50])];
        [_verticalSeparatorView setBackgroundColor:[UIColor lightGrayColor]];
        [_verticalSeparatorView setCenter:CGPointMake(_circleImageView.center.x, _verticalSeparatorView.center.y)];
        
        [[self contentView] addSubview:_verticalSeparatorView];
        
        [self.titleLabel setFrame:CGRectMake(_circleImageView.frame.origin.x + _circleImageView.frame.size.width + [UtilManager width:15], _dayLabel.frame.origin.y, [UtilManager width:240], _dayLabel.frame.size.height)];
        [self.titleLabel setTextColor:[UIColor whiteColor]];
        
        _tagView = [[TagView alloc] initWithFrame:CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height, WIDTH - self.titleLabel.frame.origin.x - [UtilManager width:40], TAGVIEW_SMALL_HEIGHT)]; //andTags:tags withTagsControlMode:TagsControlModeList];
        [_tagView setMode:TagsControlModeList];
        [_tagView setIsCenter:FALSE];
        [_tagView setUserInteractionEnabled:FALSE];
        [_tagView reloadTagSubviews];
        
        [self.contentView addSubview:_tagView];
        
        _menuButton = [[UIButton alloc] initWithFrame:CGRectMake(self.contentView.frame.size.width - [UtilManager width:50], [UtilManager height:10], [UtilManager width:35], [UtilManager width:35])];
        [_menuButton addTarget:self action:@selector(menuAction) forControlEvents:UIControlEventTouchUpInside];
        [_menuButton setImage:[UIImage imageNamed:@"btn-options"] forState:UIControlStateNormal];
        [_menuButton setCenter:CGPointMake(_menuButton.center.x, _tagView.center.y)];
        
        [[self contentView] addSubview:_menuButton];
        
        _detailInfoButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(_menuButton.frame) - [UtilManager width:45],
                                                                       0.,
                                                                       [UtilManager width:40],
                                                                       [UtilManager width:40])];
        
        [_detailInfoButton setCenter:CGPointMake(_detailInfoButton.center.x, foodCellHeight/2)];
        [_detailInfoButton addTarget:self action:@selector(detailInfoAction) forControlEvents:UIControlEventTouchUpInside];
        [_detailInfoButton setImage:[UIImage imageNamed:@"btn_show_detail_info_small"] forState:UIControlStateNormal];
        
        [[self contentView] addSubview:_detailInfoButton];
        self.contentView.clipsToBounds = YES;
    }
    return self;
}

- (void)menuAction{
    [_delegate optionNutritionDayDidPush:_currentIndex];
}

- (void)detailInfoAction {
    [_delegate nutritionPlanCellDetailInfoActionInIndex:_currentIndex];
}

- (void)setIsSimplify:(BOOL)isSimplify {
    _isSimplify = isSimplify;
    if (_isSimplify) {
        _menuButton.center = CGPointMake(_menuButton.center.x, _titleLabel.center.y);
    }
    else {
        _menuButton.center = CGPointMake(_menuButton.center.x, _tagView.center.y);
    }
}

@end
