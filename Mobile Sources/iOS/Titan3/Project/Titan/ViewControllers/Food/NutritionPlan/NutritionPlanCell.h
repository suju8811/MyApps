//
//  NutritionPlanCell.h
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagView.h"
#import "PropertyCircleView.h"

@protocol NutritionPlanCellDelegate <NSObject>

- (void)optionNutritionDayDidPush:(NSInteger)index;

- (void)nutritionPlanCellDetailInfoActionInIndex:(NSInteger)index;

@end

@interface NutritionPlanCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *separatorView;

@property (nonatomic, strong) UILabel *foodsLabel;
@property (nonatomic, strong) UILabel *kCalLabel;

@property (nonatomic, strong) PropertyCircleView *proteinCircleView;
@property (nonatomic, strong) PropertyCircleView *carbsCircleView;
@property (nonatomic, strong) PropertyCircleView *fatsCircleView;

@property (nonatomic, strong) UILabel *proteinLabel;
@property (nonatomic, strong) UILabel *carbsLabel;
@property (nonatomic, strong) UILabel *fatsLabel;

@property (nonatomic, strong) UIImageView *circleImageView;
@property (nonatomic, strong) UIView *verticalSeparatorView;
@property (nonatomic, strong) UILabel *dayLabel;
@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) UIButton *menuButton;
@property (nonatomic, strong) UIButton *detailInfoButton;

@property (nonatomic, strong) NSNumber *isFirstDayContent;

@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) UIView *panView;
@property (nonatomic, assign) id<NutritionPlanCellDelegate>delegate;
@property (nonatomic, assign) BOOL isSimplify;

@end
