//
//  NutritionPlanDetailViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "NutritionPlanDetailViewController.h"
#import "NutritionPlanAddViewController.h"
#import "DietDetailViewController.h"
#import "DietAddViewController.h"

#import "WebServiceManager.h"
#import "MRProgress.h"
#import "Exercise.h"
#import "PlanFitCell.h"
#import "Tag.h"
#import "TagView.h"

#import "ParserManager.h"

#import "RelaxCell.h"
#import "DietCell.h"
#import "CSSectionHeader.h"
#import "CSCell.h"
#import "TrainingPlanTopCell.h"
#import "CSStickyParallaxHeaderViewController.h"
#import "CSStickyHeaderFlowLayout.h"
#import "CSStickyHeaderFlowLayoutAttributes.h"
#import "UIImageView+AFNetworking.h"
#import "OptionsFoodViewController.h"

#import "MBTwitterScroll.h"

#import "NutritionPlanDetailExecutive.h"
#import "NutritionPlanCell.h"
#import "SelectorFoodViewController.h"
#import "InfoPopupViewController.h"
#import "DietDetailInfoView.h"
#import "NutritionPlanDetailinfoView.h"
#import "RNBlurModalView.h"
#import "UserSelectorViewController.h"
#import "UserMeta.h"

@interface NutritionPlanDetailViewController ()<UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, TrainingPlanTopHeaderDelegate, SelectorFoodViewDelegate, NutritionPlanCellDelegate, OptionsFoodViewControllerDelegate, MBTwitterScrollDelegate, NutritionPlanDetailDisplay, NutritionPlanOperationViewDelegate, DietDetailViewControllerDelegate, UserSelectorViewControllerDelegate>

@property (nonatomic, strong) MBTwitterScroll *dietTableView;
@property (nonatomic, strong) TagView *tagView;
@property (nonatomic, strong) SelectorFoodViewController *selectorFoodViewController;

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, assign) BOOL isSuperSerieEditing;

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *trainingDaysIndex;
@property (nonatomic, strong) TrainingDay *selectedTrainingDay;
@property (nonatomic, strong) TrainingDayContent *selectedTrainingDayContent;

@property (nonatomic, assign) BOOL isRemoveTrainingPlan;

@property (nonatomic, strong) NutritionPlanDetailExecutive * executive;

@property (nonatomic, assign) NSInteger selectedNutritionDayIndex;

@property (nonatomic, strong) InfoPopupViewController * infoPopupViewController;

@property (nonatomic, strong) RNBlurModalView *modalView;

@property (nonatomic, assign) BOOL isSimplify;
@property (nonatomic, strong) UserSelectorViewController * userSelectorViewController;

@property (nonatomic, strong) UIButton *acceptButton;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) NSDate *endDate;

@end

@implementation NutritionPlanDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _items = [[NSMutableArray alloc] init];
    _trainingDaysIndex = [[NSMutableArray alloc] init];
    _isSimplify = NO;
    
    [self reloadItems];
    
    _isSuperSerieEditing = NO;
    
    [self configureView];
    
    _executive = [[NutritionPlanDetailExecutive alloc] initWithDisplay:self];
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"CustomView" bundle:nil];
    _infoPopupViewController = [storyboard instantiateInitialViewController];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    RLMRealm *realm = [AppContext currentRealm];
    if(_isRemoveTrainingPlan) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (_selectorFoodViewController != nil &&
             _selectorFoodViewController.presentDetailForReplacingFood) {
        
        _selectorFoodViewController.presentDetailForReplacingFood = NO;
        _selectorFoodViewController.presentNutritionPlanDetail = NO;
        Diet * replacingDiet = [_selectorFoodViewController.selectResults firstObject];
        
        [realm transactionWithBlock:^{
            _currentNutritionPlan.nutritionDays[_selectedNutritionDayIndex].diet = replacingDiet;
            _currentNutritionPlan.nutritionDays[_selectedNutritionDayIndex].dietId = replacingDiet.serverDietId;
            
            [replacingDiet setValue:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
        }];
        
        [realm beginWriteTransaction];
        NSError *error;
        [realm commitWriteTransaction:&error];
        if(error){
            NSLog(@"%@",error.description);
        }
        
        [_executive updateNutritionPlan:_currentNutritionPlan];
    }
    else {
        [self reloadItems];
        
        [_dietTableView setCurrentNutritionPlan:_currentNutritionPlan];
        [[_dietTableView tableView] reloadData];
        [_dietTableView reloadNutritionPlanValues];
        
        if (_selectorFoodViewController.presentNutritionPlanDetail) {
            
            _selectorFoodViewController.presentNutritionPlanDetail = NO;
            _selectorFoodViewController.presentDetailForReplacingFood = NO;
            
            for (Diet * diet in _selectorFoodViewController.selectResults) {
            
                NutritionDay * nutritionDay = [[NutritionDay alloc] init];
                
                nutritionDay.diet = diet;
                nutritionDay.dietId = diet.serverDietId;
                
                [realm transactionWithBlock:^{
                    [diet setValue:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
                    [_currentNutritionPlan.nutritionDays addObject:nutritionDay];
                }];
                
                [realm beginWriteTransaction];
                [realm addOrUpdateObject:_currentNutritionPlan];
                NSError *error;
                [realm commitWriteTransaction:&error];
                if(error){
                    NSLog(@"%@",error.description);
                }
            }
            
            [_executive updateNutritionPlan:_currentNutritionPlan];
        }
    }
}

- (void)reloadItems{
    
    if(_items.count > 0){
        [_items removeAllObjects];
        [_trainingDaysIndex removeAllObjects];
    }
    
    for(NutritionDay * nutritionDay in _currentNutritionPlan.nutritionDays) {
        int index = 0;
        
        [_items addObject:nutritionDay];
        if (index == 0) {
            [_trainingDaysIndex addObject:@YES];
        }
        else {
            [_trainingDaysIndex addObject:@NO];
        }
    }
}

- (void)reloadDaysItems{
    if(_items.count > 0){
        [_items removeAllObjects];
        [_trainingDaysIndex removeAllObjects];
    }
    
    for(NutritionDay *nutritionDay in _currentNutritionPlan.nutritionDays){
//        if(trainingDay.restDay.intValue == 1){
//            [_items addObject:trainingDay];
//            [_trainingDaysIndex addObject:[NSNumber numberWithBool:TRUE]];
//        }
        int index = 0;
//        for(TrainingDayContent *trainingDayContent in trainingDay.trainingDayContent){
            if(index == 0){
                [_items addObject:nutritionDay];
                [_trainingDaysIndex addObject:[NSNumber numberWithBool:TRUE]];
                break;
            }
//            index ++;
//        }
    }
}

- (void)configureView{
    
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    _dietTableView = [[MBTwitterScroll alloc] initWithType:MBTableNutritionPlan];
    
    _dietTableView.currentNutritionPlan = _currentNutritionPlan;
    
    [_dietTableView setupView];
    
    _dietTableView.delegate = self;
    _dietTableView.tableView.delegate = self;
    _dietTableView.tableView.dataSource = self;
    [_dietTableView.tableView registerClass:[NutritionPlanCell class] forCellReuseIdentifier:@"nutritionPlanCell"];
    [_dietTableView.tableView registerClass:[RelaxCell class] forCellReuseIdentifier:@"relaxCell"];
    [_dietTableView.tableView registerClass:[DietCell class] forCellReuseIdentifier:@"dietCell"];
    _dietTableView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _dietTableView.tableView.bounces = NO;
    
    if (_fromCalendar == 1) {
        NSDate * today = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:[NSDate date] options:0];
        
        NSComparisonResult comparisonResult = [_selectedDate compare:today];
        if (comparisonResult == NSOrderedDescending ||
            comparisonResult == NSOrderedSame) {
            _dietTableView.menuButton.hidden = YES;
            _dietTableView.showInfoButton.hidden = YES;
            _dietTableView.userRateButton.hidden = YES;
            _dietTableView.userProfileButton.hidden = YES;
            _dietTableView.showDetailInfoButton.hidden = YES;
            _dietTableView.optionButton.hidden = YES;
        }
    }
    
    [self.view addSubview:_dietTableView];
    
    [self.view addSubview:[self footerView]];
}

- (void)configureCollectionView{
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    CSStickyHeaderFlowLayout *flowLayout = [[CSStickyHeaderFlowLayout alloc] init];
    flowLayout.parallaxHeaderReferenceSize = CGSizeMake(WIDTH,NAV_HEIGHT);
    flowLayout.scrollDirection =  UICollectionViewScrollDirectionVertical;
    flowLayout.minimumLineSpacing = [UtilManager height:10];
    flowLayout.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, [UtilManager height:175]);
    flowLayout.parallaxHeaderMinimumReferenceSize = CGSizeMake(self.view.frame.size.width, NAV_HEIGHT);
    flowLayout.parallaxHeaderAlwaysOnTop = YES;
    
    // If we want to disable the sticky header effect
    flowLayout.disableStickyHeaders = YES;
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, HEIGHT - TAB_BAR_HEIGHT) collectionViewLayout:flowLayout];
    
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [_collectionView registerClass:[NutritionPlanCell class] forCellWithReuseIdentifier:@"cell"];
    [_collectionView registerClass:[RelaxCell class] forCellWithReuseIdentifier:@"relaxCell"];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    //[self.collectionView registerNib:self.headerNib
    //      forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader
    //             withReuseIdentifier:@"header"];
    [_collectionView registerClass:[TrainingPlanTopCell class] forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader withReuseIdentifier:@"CSStickyHeaderParallaxHeader"];
    
    
    [self.view addSubview:_collectionView];
    
    [self.view addSubview:[self footerView]];
}

- (void)reloadLayout {
    
    CSStickyHeaderFlowLayout *layout = (id)_collectionView.collectionViewLayout;
    
    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]]) {
        layout.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, [UtilManager height:175]);
        layout.parallaxHeaderMinimumReferenceSize = CGSizeMake(self.view.frame.size.width, NAV_HEIGHT);
        layout.itemSize = CGSizeMake(self.view.frame.size.width, layout.itemSize.height);
        layout.parallaxHeaderAlwaysOnTop = YES;
        
        // If we want to disable the sticky header effect
        layout.disableStickyHeaders = YES;
    }
    
}

- (UIView *)headerView{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:200])];
    
    [headerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:20], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    
    [headerView addSubview:backButton];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(backButton.frame.origin.x + backButton.frame.size.width, backButton.frame.origin.y + backButton.frame.size.height, WIDTH - 2 * (backButton.frame.origin.x + backButton.frame.size.width), [UtilManager height:45])];
    [titleLabel setText:_currentNutritionPlan.title];
    [titleLabel setNumberOfLines:2];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    [headerView addSubview:titleLabel];
    
    long exercisesCount = 0;
    NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
    
    for(NutritionDay *nutritionDay in _currentNutritionPlan.nutritionDays){
        exercisesCount = exercisesCount + 1;
    }
    
    _tagView = [[TagView alloc] initWithFrame:CGRectMake(0., titleLabel.frame.size.height + titleLabel.frame.origin.y + [UtilManager height:5], WIDTH, [UtilManager height:30])];
    
    [_tagView setTags:tags];
    [_tagView reloadTagSubviews];
    [_tagView setMode:TagsControlModeList];
    [_tagView setIsCenter:TRUE];
    
    [headerView addSubview:_tagView];
    
    UILabel *exercisesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., _tagView.frame.origin.y + _tagView.frame.size.height + [UtilManager height:10], [UtilManager width:120], [UtilManager height:25])];
    [exercisesLabel setTextColor:GRAY_REGISTER_FONT];
    [exercisesLabel setTextAlignment:NSTextAlignmentRight];
    
    [exercisesLabel setText:[NSString stringWithFormat:@"%ld %@",exercisesCount,NSLocalizedString(@"Ejercicios", nil)]];
    
    [headerView addSubview:exercisesLabel];
    
    UILabel *daysLabel = [[UILabel alloc] initWithFrame:CGRectMake(exercisesLabel.frame.origin.x + exercisesLabel.frame.size.width + [UtilManager width:15], exercisesLabel.frame.origin.y, exercisesLabel.frame.size.width, exercisesLabel.frame.size.height)];
    [daysLabel setFont:exercisesLabel.font];
    [daysLabel setTextColor:exercisesLabel.textColor];
    [daysLabel setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_currentNutritionPlan.nutritionDays.count,NSLocalizedString(@"días", nil)]];
    
    [headerView addSubview:daysLabel];
    
    return headerView;
}

- (UIView *)footerView{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0. ,HEIGHT - TAB_BAR_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    [footerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIButton *moreButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10], 0., [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [moreButton setImage:[UIImage imageNamed:@"btn-add"] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(didPressAddMoreButton) forControlEvents:UIControlEventTouchUpInside];
    [moreButton setCenter:CGPointMake(WIDTH/4, TAB_BAR_HEIGHT/2)];
    
    [footerView addSubview:moreButton];
    
    UILabel *relaxDay = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH/2 + [UtilManager width:5], 0., WIDTH/2 - [UtilManager width:10], [UtilManager height:25])];
    [relaxDay setTextAlignment:NSTextAlignmentRight];
    [relaxDay setTextColor:[UIColor whiteColor]];
    [relaxDay setFont:[UIFont fontWithName:REGULAR_FONT size:24]];
    [relaxDay setAdjustsFontSizeToFitWidth:YES];
    [relaxDay setTextColor:YELLOW_APP_COLOR];
    [relaxDay setText:NSLocalizedString(@"DÍA DE DESCANSO", nil)];
    [relaxDay setCenter:CGPointMake(relaxDay.center.x, moreButton.center.y)];
    [relaxDay setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *gestureRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addRelaxDay)];
    [relaxDay addGestureRecognizer:gestureRecog];
    
    [footerView addSubview:relaxDay];
    
    return footerView;
}

- (void)didPressBackButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didPressAddMoreButton {
    _selectedTrainingDay = nil;
    [self presentSelectorFoodViewController];
}

- (void)addRelaxDay{
    
    NutritionDay *nutritionDay = [[NutritionDay alloc] init];
//    [trainingDay setRestDay:[NSNumber numberWithInt:1]];
    [nutritionDay setOrder:[NSNumber numberWithLong:_currentNutritionPlan.nutritionDays.count + 1]];
    
    RLMRealm *realm = [AppContext currentRealm];
    [realm transactionWithBlock:^{
        [_currentNutritionPlan.nutritionDays addObject:nutritionDay];
    }];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:_currentNutritionPlan];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [_executive updateNutritionPlan:_currentNutritionPlan];
    [self reloadItems];
}

- (void)presentSelectorFoodViewController {
    _selectorFoodViewController = [[SelectorFoodViewController alloc] init];
    _selectorFoodViewController.delegate = self;
    _selectorFoodViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    _selectorFoodViewController.nutritionPlan = _currentNutritionPlan;
    [_selectorFoodViewController setSelectorType:SelectorFoodTypeNutritionPlan];
    [self presentViewController:_selectorFoodViewController animated:YES completion:nil];
}

#pragma mark OptionsViewController Delegate Methods

#pragma mark HeaderDeelgate

- (void)backTopHeaderAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)menuTopHeaderAction {
    OptionsFoodViewController * optionsFoodViewController = [[OptionsFoodViewController alloc] init];
    optionsFoodViewController.currentNutritionPlan = _currentNutritionPlan;
    optionsFoodViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsFoodViewController.delegate = self;
    [self presentViewController:optionsFoodViewController animated:YES completion:nil];
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:FALSE];
    
    Diet *currentDiet = _currentNutritionPlan.nutritionDays[indexPath.row].diet;
    _selectedNutritionDayIndex = indexPath.row;
    
    DietDetailViewController * dietDetailViewController = [[DietDetailViewController alloc] init];
    [dietDetailViewController setCurrentDiet:currentDiet];
    
    [self.navigationController pushViewController:dietDetailViewController animated:YES];
}

#pragma mark UITableViewDataSource Methods

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]])
        return [UtilManager height:50];
    else {
        if (_isSimplify) {
            return [UtilManager height:20];
        }
        else {
            return [UtilManager height:80];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]])
        return [UtilManager height:50];
    else {
        if (_isSimplify) {
            return [UtilManager height:43];
        }
        else {
            return [UtilManager height:132];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
    return TAB_BAR_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return TAB_BAR_HEIGHT;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, TAB_BAR_HEIGHT)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    return footerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[NutritionDay class]]) {
        NutritionPlanCell *cell = [_dietTableView.tableView dequeueReusableCellWithIdentifier:@"nutritionPlanCell"];
        cell.isSimplify = _isSimplify;
        NutritionDay * nutritionDay = [_items objectAtIndex:indexPath.row];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if([[_trainingDaysIndex objectAtIndex:indexPath.row]isEqual:[NSNumber numberWithBool:TRUE]]){
            [[cell dayLabel] setHidden:FALSE];
            [[cell menuButton] setHidden:FALSE];
            [[cell circleImageView] setHidden:FALSE];
            [cell setIsFirstDayContent:[NSNumber numberWithBool:TRUE]];
            cell.verticalSeparatorView.frame = CGRectMake(cell.verticalSeparatorView.frame.origin.x,
                                                          cell.circleImageView.frame.size.height + cell.circleImageView.frame.origin.y + [UtilManager height:5],
                                                          1,
                                                          [UtilManager height:75]);
            
        }
        else {
            cell.dayLabel.hidden = YES;
            cell.circleImageView.hidden = YES;
            cell.isFirstDayContent = @NO;
            cell.verticalSeparatorView.frame = CGRectMake(cell.verticalSeparatorView.frame.origin.x,0, 1, [UtilManager height:90]);
        }
        
        [cell setNeedsLayout];
        
        Diet * diet = nutritionDay.diet;
        
        if (!diet) {
            diet = [Diet dietWithServerId:nutritionDay.dietId];
            if (diet) {
                RLMRealm *realm = [AppContext currentRealm];
                
                [realm transactionWithBlock:^{
                    [nutritionDay setDiet:diet];
                }];
                
                [realm beginWriteTransaction];
                NSError *error;
                [realm commitWriteTransaction:&error];
                if(error){
                    NSLog(@"%@",error.description);
                }
            }
        }
        
        if(diet.isFavourite != nil && diet.isFavourite.boolValue){
            NSMutableAttributedString *myString=[[NSMutableAttributedString alloc] initWithString:diet.title
                                                                                       attributes:@{
                                                                                                    NSForegroundColorAttributeName:YELLOW_APP_COLOR,
                                                                                                    NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]
                                                                                                    }
                                                 ];
            
            NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
            attachment.image = [UIImage imageNamed:@"icon-fav-on"];
            attachment.bounds = CGRectMake(5., -2., cell.titleLabel.frame.size.height, cell.titleLabel.frame.size.height);
            NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
            [myString appendAttributedString:attachmentString];
            cell.titleLabel.attributedText = myString;
        }
        else {
            cell.titleLabel.text = diet.title;
        }
        
        int order = 0;
        for (NSInteger index = 0; index <= indexPath.row; index++) {
            if ([_trainingDaysIndex[index] boolValue]) {
                order ++;
            }
        }
        
        [cell.dayLabel setText:[NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"Día", nil),order]];
        [cell setCurrentIndex:order-1];
        
        NSInteger kCal = 0;
        
        for (DietMeal * dietMeal in diet.dietMeals) {
            for (Meal * meal in dietMeal.meals) {
                kCal = kCal + meal.calcium.intValue;
            }
        }
        [[cell kCalLabel] setText:[NSString stringWithFormat:@"%ld kCal",(long)kCal]];
        
        NSInteger mealsCount = 0;
        for (DietMeal * dietMeal in diet.dietMeals) {
            mealsCount = mealsCount + dietMeal.meals.count;
        }
        if (mealsCount == 1) {
            cell.foodsLabel.text = [NSString stringWithFormat:@"1 %@",NSLocalizedString(@"Comida", nil)];
        }
        else {
            cell.foodsLabel.text = [NSString stringWithFormat:@"%d %@",
                                    (int)mealsCount,
                                    NSLocalizedString(@"Comidas", nil)];
        }
        
        NSInteger kProtein = [Diet getDietProtein:diet];
        cell.proteinCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g",(long)kProtein];
        
        NSInteger kCarbs = [Diet getDietCarb:diet];
        cell.carbsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g",(long)kCarbs];
        
        NSInteger kFats = [Diet getDietFats:diet];
        cell.fatsCircleView.countLabel.text = [NSString stringWithFormat:@"%ld g",(long)kFats];
        
        cell.delegate = self;
        cell.currentIndex = indexPath.row;
        
        cell.tagView.tags = [Diet getTagsInDiet:diet];
        [cell.tagView reloadTagSubviews];
        
        if (cell.isSimplify) {
            cell.tagView.hidden = YES;
        }
        else {
            cell.tagView.hidden = NO;
        }
        
        return cell;
        
    }
    
    return nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]){
        return TRUE;
    } else {
        if([[_trainingDaysIndex objectAtIndex:indexPath.row]isEqual:[NSNumber numberWithBool:TRUE]])
            return TRUE;
        else
            return TRUE;
    }
    
    return TRUE;
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[NutritionDay class]]){
        return YES;
    }
    else {
        if([[_trainingDaysIndex objectAtIndex:indexPath.row]isEqual:[NSNumber numberWithBool:TRUE]])
            return YES;
        else
            return YES;
    }
    
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm transactionWithBlock:^{
        [_currentNutritionPlan.nutritionDays exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
    }];
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [self reloadItems];
    [_dietTableView reloadNutritionPlanValues];
    [_dietTableView.tableView reloadData];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]){
        return TRUE;
    } else {
        //if([[_trainingDaysIndex objectAtIndex:indexPath.row]isEqual:[NSNumber numberWithBool:TRUE]])
        return TRUE;
        //else
        //    return FALSE;
    }
    
    return TRUE;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]){
        return TRUE;
    } else {
        //if([[_trainingDaysIndex objectAtIndex:indexPath.row]isEqual:[NSNumber numberWithBool:TRUE]])
        return TRUE;
        //else
        //    return FALSE;
    }
    
    return TRUE;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if(_items.count > 1){
            if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]){
                RLMRealm *realm  = [AppContext currentRealm];
                
                [realm transactionWithBlock:^{
                    [_currentNutritionPlan.nutritionDays removeObjectAtIndex:indexPath.row];
                }];
                
                [realm beginWriteTransaction];
                NSError *error;
                [realm commitWriteTransaction:&error];
                if(error){
                    NSLog(@"%@",error.description);
                }
            }else{
                
                RLMRealm *realm  = [AppContext currentRealm];
                
                [realm transactionWithBlock:^{
                    [_currentNutritionPlan.nutritionDays removeObjectAtIndex:indexPath.row];
                }];
                
                [realm beginWriteTransaction];
                NSError *error;
                [realm commitWriteTransaction:&error];
                if(error){
                    NSLog(@"%@",error.description);
                }
                
            }
            [self reloadItems];
            [_dietTableView.tableView reloadData];
            
            [_executive updateNutritionPlan:_currentNutritionPlan];
            
            
        } else {
            [self presentViewController:[UtilManager presentAlertWithMessage:@"Un entrenamiento debe tener un ejercicio. Puedo borrar directamente el entrenamiento."] animated:YES completion:^{
            }];
        }
    } else {
        NSLog(@"Editing");
        
    }
}

#pragma mark UICollectionViewDelegate Methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]){
        
    }
    else {
    }
}

#pragma mark UICollectionViewFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]])
        return CGSizeMake(WIDTH, [UtilManager height:50]);
    else
        return CGSizeMake(WIDTH , [UtilManager height:80]);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake([UtilManager height:0], [UtilManager width:0], [UtilManager height:0], [UtilManager width:0]);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return [UtilManager height:0];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return [UtilManager height:0];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(0.,0.);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeMake(0.,0.);
}


#pragma mark UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    //return _currentTraining.trainingExercises.count;
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return _items.count;
    
    //return _trainingPlan.trainingDay.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if([[_items objectAtIndex:indexPath.row] isKindOfClass:[TrainingDay class]]){
        
        TrainingDay *trainingDay = [_items objectAtIndex:indexPath.row];
        
        if([trainingDay.restDay isEqual:[NSNumber numberWithInt:1]]){
            
            int aux = 0;
            
            for(int i = 0 ; i<=indexPath.row; i++){
                if([[_trainingDaysIndex objectAtIndex:i] isEqual:[NSNumber numberWithBool:FALSE]])
                    aux++;
            }
            
            RelaxCell *relaxCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"relaxCell" forIndexPath:indexPath];
            
            //[relaxCell.dayLabel setText:[NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"Día", nil),trainingDay.order.intValue]];
            
            [relaxCell.dayLabel setText:[NSString stringWithFormat:@"%@ %ld",NSLocalizedString(@"Día", nil),indexPath.row + 1 - aux]];
            
            UIView *bottomView = [[UIView alloc] init];
            bottomView.frame = (CGRect){
                .size = CGSizeMake(CGRectGetWidth(collectionView.bounds)/2, CGRectGetHeight(relaxCell.bounds))
            };
            
            //relaxCell.revealView = bottomView;
            //bottomView.backgroundColor =  [UIColor redColor];
            
            //relaxCell.allowedDirection = CADRACSwippableCellAllowedDirectionLeft;
            //relaxCell.revealView = bottomView;
            
            /*
             [relaxCell.workoutImageView setHidden:YES];
             [relaxCell.titleLabel setHidden:TRUE];
             [relaxCell.clockImageView setHidden:TRUE];
             [relaxCell.kCalImageView setHidden:TRUE];
             [relaxCell.energyLabel setHidden:TRUE];
             [relaxCell.separatorView setHidden:TRUE];*/
            
            return relaxCell;
        }
    } else {
        
        NutritionPlanCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell"
                                                                           forIndexPath:indexPath];
        TrainingDayContent *trainingDayContent = [_items objectAtIndex:indexPath.row];
        
        if([[_trainingDaysIndex objectAtIndex:indexPath.row]isEqual:[NSNumber numberWithBool:TRUE]]){
            [[cell dayLabel] setHidden:FALSE];
            [[cell menuButton] setHidden:FALSE];
            [[cell circleImageView] setHidden:FALSE];
            [cell setIsFirstDayContent:[NSNumber numberWithBool:TRUE]];
            [cell.verticalSeparatorView setFrame:CGRectMake(cell.verticalSeparatorView.frame.origin.x, cell.circleImageView.frame.size.height + cell.circleImageView.frame.origin.y + [UtilManager height:5], 1, [UtilManager height:55])];
        }else{
            [[cell dayLabel] setHidden:TRUE];
            [[cell menuButton] setHidden:TRUE];
            [[cell circleImageView] setHidden:TRUE];
            [cell setIsFirstDayContent:[NSNumber numberWithBool:FALSE]];
            [cell.verticalSeparatorView setFrame:CGRectMake(cell.verticalSeparatorView.frame.origin.x,0, 1, [UtilManager height:90])];
        }
        
        [cell setNeedsLayout];
        
        Training *training = trainingDayContent.training;
        
        if(!training){
            training = [Training trainingWithServerId:trainingDayContent.trainingId];
            if(training)
            {
                RLMRealm *realm = [AppContext currentRealm];
                
                [realm transactionWithBlock:^{
                    [trainingDayContent setTraining:training];
                }];
                
                [realm beginWriteTransaction];
                NSError *error;
                [realm commitWriteTransaction:&error];
                if(error){
                    NSLog(@"%@",error.description);
                }
            }
        }
        
        [[cell titleLabel] setText:training.title];
        
        int order = 1;
        
//        for(TrainingDay *trainingDay in _trainingPlan.trainingDay){
//            for(TrainingDayContent *dayContent in trainingDay.trainingDayContent){
//                if([dayContent.trainingId isEqual:trainingDayContent.trainingId]){
//                    //[cell.dayLabel setText:[NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"Día", nil),trainingDay.order.intValue]];
//                    [cell.dayLabel setText:[NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"Día", nil),order]];
//                    [cell setCurrentIndex:order-1];
//                    break;
//                }
//            }
//            order ++;
//        }
        
        NSInteger kCal = 0;
        
        for(TrainingExercise *trainingExercise in training.trainingExercises){
            kCal = kCal + [TrainingExercise getKCals:trainingExercise];
        }
        
        [[cell kCalLabel] setText:[NSString stringWithFormat:@"%ld kCal",(long)kCal]];
        
        NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
        
        for(Tag *tag in training.tags){
            [tags addObject:tag];
        }
        
        for(Tag *tagsExercise in [Training getTrainingsExercisesTags:training]){
            [tags addObject:tagsExercise];
        }
        
        [cell setDelegate:self];
        
        [[cell tagView] setTags:tags];
        [[cell tagView] reloadTagSubviews];
        
        return cell;
        
    }
    
    return nil;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        
        CSSectionHeader *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                   withReuseIdentifier:@"sectionHeader"
                                                                          forIndexPath:indexPath];
        
        
        return cell;
        
    } else if ([kind isEqualToString:CSStickyHeaderParallaxHeader]) {
        TrainingPlanTopCell *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                       withReuseIdentifier:@"CSStickyHeaderParallaxHeader"
                                                                              forIndexPath:indexPath];
        cell.exercisesLabel.text = @"aaa";
        return cell;
//
//
//        [[cell daysLabel] setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)_trainingPlan.trainingDay.count,NSLocalizedString(@"Días", nil)]];
//
//        [[cell exercisesLabel] setText:[NSString stringWithFormat:@"%d %@",[TrainingPlan numberOfTrainings:_trainingPlan],NSLocalizedString(@"Entrenamientos", nil)]];
//        [cell.titleHeaderLabel setText:_trainingPlan.title];
//        [cell setDelegate:self];
//
//        [[cell tagView] setTags:[TrainingPlan getTrainingsTags:_trainingPlan]];
//        [[cell tagView] reloadTagSubviews];
//
//        return cell;
    }
    return nil;
}

#pragma mark MBTwitterScrollDelegate Methods

- (void)editButtonDidPress{
    if (_dietTableView.tableView.editing) {
        [_dietTableView.tableView setEditing:NO animated:YES];
        [_executive updateNutritionPlan:_currentNutritionPlan];
    }
    else {
        [_dietTableView.tableView setEditing:YES animated:YES];
    }
}

- (void)didPressShowInfoButton {
    _infoPopupViewController.rlmObject = _currentNutritionPlan;
    [self.view addSubview:_infoPopupViewController.view];
}

- (void)didPressShowDetailInfoButton {
    NutritionPlanDetailinfoView * nutritionPlanDetailView = [[NutritionPlanDetailinfoView alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                                               [UtilManager height:20],
                                                                                               WIDTH - [UtilManager width:20],
                                                                                               HEIGHT - [UtilManager height:40])
                                                                                     andNutritionPlan:_currentNutritionPlan];
    
    _modalView = [[RNBlurModalView alloc] initWithView:nutritionPlanDetailView];
    [_modalView setUserInteractionEnabled:YES];
    [_modalView show];
}

- (void)didPressSimplifyButton:(UIButton *)sender {
    _isSimplify = sender.selected;
    [[_dietTableView tableView] reloadData];
}

- (void)didPressUserRateButton {
    NSString * nutritionPlanId = _currentNutritionPlan.serverNutritionPlanId;
    [WebServiceManager getNutrionalPlansWithCompletionBlock:^(BOOL isSuccess) {
        
        _currentNutritionPlan = [NutritionPlan getNutritionPlanWithId:nutritionPlanId];
        [self presentUserRateView:_currentNutritionPlan];
    }];
}

- (void)backButtonDidPress{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)optionButtonDidPress {
    OptionsFoodViewController *optionsFoodViewController = [[OptionsFoodViewController alloc] init];
    optionsFoodViewController.currentNutritionPlan = _currentNutritionPlan;
    optionsFoodViewController.optionFoodType = OptionFoodTypeNutritionPlan;
    optionsFoodViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsFoodViewController.delegate = self;
    optionsFoodViewController.fromCalendar = _fromCalendar;
    [self presentViewController:optionsFoodViewController animated:YES completion:nil];
}

#pragma mark - NutritionPlanDetailDisplay Delegate

- (void)didUpdateNutritionPlan:(NutritionPlan*)nutritionPlan {
    [self reloadItems];
    [_dietTableView setCurrentNutritionPlan:nutritionPlan];
    [[_dietTableView tableView] reloadData];
    [_dietTableView reloadNutritionPlanValues];
}

- (void)reloadAllElements {
    [WebServiceManager getAlimentsWithCompletionBlock:^(BOOL isSuccess) {
        [WebServiceManager getMealsWithCompletionBlock:^(BOOL isSuccess) {
            [WebServiceManager getDietsWithWithCompletionBlock:^(BOOL isSuccess) {
                [WebServiceManager getNutrionalPlansWithCompletionBlock:^(BOOL isSuccess) {
                    
                    [self reloadItems];
                    [[_dietTableView tableView] reloadData];
                    [_dietTableView reloadNutritionPlanValues];
                }];
            }];
        }];
    }];
}

#pragma mark - NutritionPlanCellDelegate

- (void)optionNutritionDayDidPush:(NSInteger)index {
    OptionsFoodViewController *optionsFoodViewController = [[OptionsFoodViewController alloc] init];
    
    NutritionDay * nutritionDay = [_currentNutritionPlan.nutritionDays objectAtIndex:index];

    _selectedNutritionDayIndex = index;

    optionsFoodViewController.currentDiet = nutritionDay.diet;
    optionsFoodViewController.optionFoodType = OptionFoodTypeDiet;
    optionsFoodViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    optionsFoodViewController.delegate = self;
    optionsFoodViewController.fromDetailOfParent = YES;
    [self presentViewController:optionsFoodViewController animated:YES completion:nil];
}

- (void)nutritionPlanCellDetailInfoActionInIndex:(NSInteger)index {
    NutritionDay * nutritionDay = [_items objectAtIndex:index];
    DietDetailInfoView * mealDetailView = [[DietDetailInfoView alloc] initWithFrame:CGRectMake([UtilManager width:10],
                                                                                               [UtilManager height:20],
                                                                                               WIDTH - [UtilManager width:20],
                                                                                               HEIGHT - [UtilManager height:40])
                                                                            andDiet:nutritionDay.diet];
    
    _modalView = [[RNBlurModalView alloc] initWithView:mealDetailView];
    [_modalView setUserInteractionEnabled:YES];
    [_modalView show];
}

#pragma mark - OptionsFoodViewControllerDelegate Methods

- (void)didPressAddToCalendar:(RLMObject *)rlmObject {
    if (![rlmObject isKindOfClass:[NutritionPlan class]]) {
        return;
    }
    
    [self.view endEditing:YES];
    
    if(_datePicker) {
        [_datePicker removeFromSuperview];
        _datePicker = nil;
    }
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0., self.view.frame.size.height - 216, self.view.frame.size.width, 216)];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    
    [_datePicker setDate:[NSDate date]];
    
    _datePicker.backgroundColor = [UIColor whiteColor];
    _datePicker.userInteractionEnabled = YES;
    
    _acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(_datePicker.frame.size.width - 80., _datePicker.frame.origin.y, 80., 24.)];
    [_acceptButton setTitleColor:BLACK_APP_COLOR forState:UIControlStateNormal];
    [_acceptButton setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    [_acceptButton addTarget:self action:@selector(acceptDatePickerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_datePicker];
    [self.view addSubview:_acceptButton];
}

- (void)acceptDatePickerAction {
    if(_acceptButton && _datePicker) {
        NutritionPlan * nutritionPlan = _currentNutritionPlan;
        [_executive addNutritionPlan:nutritionPlan
                              toDate:_datePicker.date];
        
        [_acceptButton removeFromSuperview];
        _acceptButton = nil;
        
        [_datePicker removeFromSuperview];
        _datePicker = nil;
        
    }
}

- (void)editNutritionPlan:(NutritionPlan *)nutritionPlan {
    NutritionPlanAddViewController *nutritionPlanAddViewController = [[NutritionPlanAddViewController alloc] init];
    nutritionPlanAddViewController.editNutritionPlan = nutritionPlan;
    nutritionPlanAddViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    nutritionPlanAddViewController.nutritionPlanOperationDelegate = self;
    [self presentViewController:nutritionPlanAddViewController animated:YES completion:nil];
}

- (void)didPressFavoriteFood:(RLMObject*)rlmObject {
    [self reloadItems];
    if ([rlmObject isKindOfClass:[NutritionPlan class]]) {
        NutritionPlan * nutritionPlan = (NutritionPlan*)rlmObject;
        _dietTableView.currentNutritionPlan = nutritionPlan;
    }
    
    [_dietTableView.tableView reloadData];
    [_dietTableView reloadNutritionPlanValues];
}

- (void)didPressEditFood:(RLMObject*)rlmObject {
    if ([rlmObject isKindOfClass:[Diet class]]) {
        Diet * diet = (Diet*)rlmObject;
        DietAddViewController *dietAddViewController = [[DietAddViewController alloc] init];
        dietAddViewController.editDiet = diet;
        dietAddViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        dietAddViewController.dietDetailViewControllerDelegate = self;
        
        [self presentViewController:dietAddViewController animated:YES completion:nil];
    }
    else if ([rlmObject isKindOfClass:[NutritionPlan class]]) {
        NutritionPlan * nutritionPlan = (NutritionPlan*)rlmObject;
        
        NutritionPlanAddViewController *nutritionPlanAddViewController = [[NutritionPlanAddViewController alloc] init];
        
        nutritionPlanAddViewController.editNutritionPlan = nutritionPlan;
        nutritionPlanAddViewController.modalTransitionStyle =  UIModalTransitionStyleCoverVertical;
        nutritionPlanAddViewController.nutritionPlanOperationDelegate = self;
        
        [self presentViewController:nutritionPlanAddViewController animated:YES completion:nil];
    }
}

- (void)didPressReplaceDiet:(RLMObject*)rlmObject {
    if ([rlmObject isKindOfClass:[Diet class]]) {
        _selectorFoodViewController = [[SelectorFoodViewController alloc] init];
        _selectorFoodViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        _selectorFoodViewController.selectorType = SelectorFoodTypeNutritionPlanSingle;
        _selectorFoodViewController.nutritionPlan = _currentNutritionPlan;
        _selectorFoodViewController.selectedFoodIndex = _selectedNutritionDayIndex;
        _selectorFoodViewController.delegate = self;
        
        [self presentViewController:_selectorFoodViewController animated:YES completion:nil];
    }
}

- (void)didPressDuplicateFood:(RLMObject*)rlmObject; {
    if ([rlmObject isKindOfClass:[NutritionPlan class]]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if ([rlmObject isKindOfClass:[Diet class]]) {
        Diet * diet = (Diet*)rlmObject;
        NutritionDay * nutritionDay = [[NutritionDay alloc] init];
        nutritionDay.dietId = diet.serverDietId;
        nutritionDay.diet = diet;
        
        RLMRealm *realm = [AppContext currentRealm];
        NSError *error;
        
        [realm transactionWithBlock:^{
            [_currentNutritionPlan.nutritionDays addObject:nutritionDay];
        }];
        
        [realm beginWriteTransaction];
        [realm commitWriteTransaction:&error];
        if (error) {
            NSLog(@"%@", error.localizedDescription);
        }
        [_executive updateNutritionPlan:_currentNutritionPlan];
    }
}

- (void)didPressDeleteFood:(RLMObject*)rlmObject {
    if ([rlmObject isKindOfClass:[NutritionPlan class]]) {
        _isRemoveTrainingPlan = YES;
    }
    else if ([rlmObject isKindOfClass:[Diet class]]) {
        Diet * diet = (Diet*)diet;
        
        RLMRealm *realm = [AppContext currentRealm];
        NSError *error;
        
        [realm transactionWithBlock:^{
            [_currentNutritionPlan.nutritionDays removeObjectAtIndex:_selectedNutritionDayIndex];
        }];
        
        [realm beginWriteTransaction];
        [realm commitWriteTransaction:&error];
        if (error) {
            NSLog(@"%@", error.localizedDescription);
        }
        
        [_executive updateNutritionPlan:_currentNutritionPlan];
        
        return;
    }
}

- (void)didPressReplicateFood:(RLMObject *)rlmObject count:(NSInteger)count {
    if ([rlmObject isKindOfClass:[Diet class]]) {
        Diet * diet = (Diet*)rlmObject;
        
        NSMutableArray<NutritionDay*>* nutritionDaysAux = [[NSMutableArray alloc] init];
        for (NSInteger index = 0; index < count; index++) {
            NutritionDay * nutritionDay = [[NutritionDay alloc] init];
            nutritionDay.dietId = diet.serverDietId;
            nutritionDay.diet = diet;
            
            [nutritionDaysAux addObject:nutritionDay];
        }
        
        RLMRealm *realm = [AppContext currentRealm];
        NSError *error;
        
        [realm transactionWithBlock:^{
            [_currentNutritionPlan.nutritionDays addObjects:nutritionDaysAux];
        }];
        
        [realm beginWriteTransaction];
        [realm commitWriteTransaction:&error];
        if (error) {
            NSLog(@"%@", error.localizedDescription);
        }
        [_executive updateNutritionPlan:_currentNutritionPlan];
    }
}

- (void)didPressReplicateTodo {
    NSMutableArray<NutritionDay*>* nutritionDaysAux = [[NSMutableArray alloc] init];
    for (NutritionDay * nutritionDay in _currentNutritionPlan.nutritionDays)  {
        NutritionDay * newNutritionDay = [[NutritionDay alloc] init];
        newNutritionDay.diet = nutritionDay.diet;
        newNutritionDay.dietId  = nutritionDay.dietId;
        [nutritionDaysAux addObject:newNutritionDay];
    }
    
    RLMRealm *realm = [AppContext currentRealm];
    NSError *error;
    
    [realm transactionWithBlock:^{
        [_currentNutritionPlan.nutritionDays addObjects:nutritionDaysAux];
    }];
    
    [realm beginWriteTransaction];
    [realm commitWriteTransaction:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
    }
    [_executive updateNutritionPlan:_currentNutritionPlan];
}

- (void)didPressSendFood:(RLMObject *)rlmObject {
    _userSelectorViewController = [[UserSelectorViewController alloc] init];
    _userSelectorViewController.rlmObject = rlmObject;
    _userSelectorViewController.delegate = self;
    _userSelectorViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:_userSelectorViewController animated:YES completion:nil];
}

- (void)didPressShareFood:(RLMObject *)rlmObject {
    [_executive shareContent:rlmObject];
}

#pragma mark - DietDetailViewControllerDelegate

- (void)didUpdatedDiet:(Diet*)diet {
    [self reloadItems];
    [_dietTableView.tableView reloadData];
    [_dietTableView reloadNutritionPlanValues];
}


#pragma mark - UserSelectorViewControllerDelegate Methods

- (void)didPressOKWithSelectedResults:(NSArray*)selectedResults
                           andContent:(RLMObject*)content {
    
    UserMeta * userMeta = [selectedResults firstObject];
    [_executive sendContent:content
                    toToken:userMeta.token];
}

@end

