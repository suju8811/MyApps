//
//  NutritionPlanAddViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 12/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "NutritionPlan.h"
#import "FoodViewController.h"
#import "SelectorFoodViewController.h"
#import "NutritionPlanDetailViewController.h"

@interface NutritionPlanAddViewController : ParentViewController

@property (nonatomic, strong) NutritionPlan *editNutritionPlan;
@property (nonatomic, strong) FoodViewController *foodViewController;
@property (nonatomic, strong) NutritionPlan *createNutritionPlan;
@property (nonatomic, strong) SelectorFoodViewController *selectorFoodViewController;
@property (nonatomic, strong) id<NutritionPlanOperationViewDelegate> nutritionPlanOperationDelegate;

@end
