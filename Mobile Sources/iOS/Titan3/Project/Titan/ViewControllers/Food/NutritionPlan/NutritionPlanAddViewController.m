//
//  NutritionPlanAddViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 12/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "NutritionPlanAddViewController.h"
#import "WebServiceManager.h"
#import "MRProgress.h"
#import "NutritionPlanAddExecutive.h"

@interface NutritionPlanAddViewController ()<UITextFieldDelegate, UITextViewDelegate, WebServiceManagerDelegate, SelectorFoodViewDelegate, NutritionPlanAddDisplay>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITextField *nameTextField;

@property (nonatomic, strong) UITextView *contentTextView;
@property (nonatomic, strong) UITextView *noteTextView;

@property (nonatomic, strong) UIButton *privacityButton;
@property (nonatomic, strong) UILabel *privacityLabel;

@property (nonatomic, strong) NutritionPlanAddExecutive * executive;

@end

@implementation NutritionPlanAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeAction) name:NAV_RIGHT1_ACTION object:nil];
    
    [self configureView];
    
    _executive = [[NutritionPlanAddExecutive alloc] initWithDisplay:self];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(_selectorFoodViewController.presentNutritionPlanDetail){
        
        _selectorFoodViewController.presentNutritionPlanDetail = FALSE;
        
        for(Diet *diet in _selectorFoodViewController.selectResults){
            
            Diet *duplicateDiet = [Diet duplicateDiet:diet];
            
            [duplicateDiet setIsCopy:[NSNumber numberWithBool:TRUE]];
            
            NutritionDay *nutritionDay = [[NutritionDay alloc] init];
            
            [nutritionDay setDiet:duplicateDiet];
            
            [nutritionDay setOrder:[NSNumber numberWithInt:1]];
            
            [_createNutritionPlan.nutritionDays addObject:nutritionDay];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                [diet setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
        }
        
        [_executive createNutritionPlan:_createNutritionPlan];
    }
}

- (void)closeAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)configureView{
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _nameTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:10], NAV_HEIGHT, WIDTH - [UtilManager width:20], UITEXTFIELD_HEIGHT)];
    
    [_nameTextField setDelegate:self];
    [_nameTextField setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]];
    [_nameTextField setTextColor:GRAY_REGISTER_FONT];
    [_nameTextField setBackgroundColor:[UIColor clearColor]];
    [_nameTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    _nameTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Nombre", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName:[UIColor grayColor],
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]
                                                 }
     ];
    
    if(_editNutritionPlan)
        _nameTextField.text = _editNutritionPlan.title;
    
    [self.view addSubview:_nameTextField];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _nameTextField.frame.origin.y + _nameTextField.frame.size.height , WIDTH - [UtilManager width:40], 1)];
    [separatorView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self.view addSubview:separatorView];
    
    _contentTextView = [[UITextView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _nameTextField.frame.origin.y + _nameTextField.frame.size.height + [UtilManager height:10], _nameTextField.frame.size.width, [UtilManager height:120])];
    [_contentTextView setTextColor:_nameTextField.textColor];
    [_contentTextView setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_contentTextView setTextAlignment:NSTextAlignmentJustified];
    [_contentTextView setText:NSLocalizedString(@"Descripción", nil)];
    [_contentTextView setFont:[UIFont fontWithName:ITALIC size:18]];
    [_contentTextView setBackgroundColor:[UIColor clearColor]];
    [[_contentTextView layer] setBorderWidth:1];
    [[_contentTextView layer] setBorderColor:BLACK_HEADER_APP_COLOR.CGColor];
    
    if(_editNutritionPlan) {
        _contentTextView.text = _editNutritionPlan.content;
    }
    
    [_contentTextView setDelegate:self];
    
    UIToolbar *toolbarWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //UIBarButtonItem *barWeigthButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resetView)];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetView)];
    [barButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarWeigth setItems:[NSArray arrayWithObjects:flexibleWeigthSpace, barButtonItem, nil]];
    _contentTextView.inputAccessoryView = toolbarWeigth;
    
    [self.view addSubview:_contentTextView];
    
    _noteTextView = [[UITextView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _contentTextView.frame.origin.y + _contentTextView.frame.size.height + [UtilManager height:10], _nameTextField.frame.size.width, [UtilManager height:180])];
    [_noteTextView setTextColor:_nameTextField.textColor];
    [_noteTextView setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_noteTextView setTextAlignment:NSTextAlignmentJustified];
    [_noteTextView setText:NSLocalizedString(@"Notas", nil)];
    [_noteTextView setFont:[UIFont fontWithName:ITALIC size:18]];
    [_noteTextView setBackgroundColor:[UIColor clearColor]];
    [[_noteTextView layer] setBorderWidth:1];
    [[_noteTextView layer] setBorderColor:BLACK_HEADER_APP_COLOR.CGColor];
    
    if (_editNutritionPlan) {
        _noteTextView.text = _editNutritionPlan.notes;
    }
    
    [_noteTextView setDelegate:self];
    
    UIToolbar *toolbarNoteWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarNoteWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleNoteWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //UIBarButtonItem *barWeigthButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resetView)];
    UIBarButtonItem *barNoteButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetView)];
    [barNoteButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barNoteButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarNoteWeigth setItems:[NSArray arrayWithObjects:flexibleNoteWeigthSpace, barNoteButtonItem, nil]];
    _noteTextView.inputAccessoryView = toolbarNoteWeigth;
    
    [self.view addSubview:_noteTextView];
    
    _privacityButton = [[UIButton alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x,_noteTextView.frame.origin.y + _noteTextView.frame.size.height + [UtilManager height:10] , [UtilManager width:40], [UtilManager width:40])];
    [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
    [_privacityButton addTarget:self action:@selector(privacityAction) forControlEvents:UIControlEventTouchUpInside];
    
//    [self.view addSubview:_privacityButton];
    
    _privacityLabel = [[UILabel alloc] initWithFrame:CGRectMake(_privacityButton.frame.origin.x + _privacityButton.frame.size.width + [UtilManager width:10], _privacityButton.frame.origin.y, [UtilManager width:240], _privacityButton.frame.size.height)];
    [_privacityLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    [_privacityLabel setTextAlignment:NSTextAlignmentLeft];
    [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    [_privacityLabel setTextColor:[UIColor whiteColor]];
    
//    [self.view addSubview:_privacityLabel];
    
    if([_editNutritionPlan.privacity isEqualToString:@"public"]){
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    }else{
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-closed"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Privado", nil)];
    }
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, NAV_HEIGHT - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    if(_editNutritionPlan)
        [titleLabel setText:NSLocalizedString(@"EDITAR PLAN", nil)];
    else
        [titleLabel setText:NSLocalizedString(@"NUEVO PLAN", nil)];
    [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:titleLabel];
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50], [UtilManager height:20], [UtilManager width:40] * 0.88, [UtilManager width:40] * 0.8)];
    [okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
    [okButton setCenter:CGPointMake(okButton.center.x, titleLabel.center.y)];
    
    [self.view addSubview:okButton];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:20.], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton setHidden:FALSE];
    [backButton setCenter:CGPointMake(backButton.center.x, okButton.center.y)];
    
    [self.view addSubview:backButton];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didPressBackButton)];
    [swipeGesture setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:swipeGesture];
}

- (void)didPressBackButton {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)privacityAction{
    if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]]){
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-closed"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Privado", nil)];
    }else{
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    }
}

- (void)resetView{
    [_contentTextView resignFirstResponder];
    [_noteTextView resignFirstResponder];
}

- (void)didPressOKButton{
    if([self checkFields]){
        if(_editNutritionPlan){
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                
                [_editNutritionPlan setTitle:_nameTextField.text];
                [_editNutritionPlan setContent:_contentTextView.text];
                [_editNutritionPlan setNotes:_noteTextView.text];
                
                if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]])
                    [_editNutritionPlan setPrivacity:@"public"];
                else
                    [_editNutritionPlan setPrivacity:@"private"];
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            
            [_executive updateNutritionPlan:_editNutritionPlan];
        }
        else {
            
            _createNutritionPlan = [[NutritionPlan alloc] init];
            
            [_createNutritionPlan setNutritionPlanId:[UtilManager uuid]];
            [_createNutritionPlan setTitle:_nameTextField.text];
            [_createNutritionPlan setContent:_contentTextView.text];
            [_createNutritionPlan setNotes:_noteTextView.text];
            
            if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]])
                [_createNutritionPlan setPrivacity:@"public"];
            else
                [_createNutritionPlan setPrivacity:@"private"];
            
            [_createNutritionPlan setOwner:[[AppContext sharedInstance] userId]];
            
            _selectorFoodViewController = [[SelectorFoodViewController alloc] init];
            [_selectorFoodViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [_selectorFoodViewController setSelectorType:SelectorFoodTypeNutritionPlan];
            [_selectorFoodViewController setNutritionPlan:_createNutritionPlan];
            [_selectorFoodViewController setDelegate:self];
            
            [self presentViewController:_selectorFoodViewController animated:YES completion:^{
                
            }];
        }
        
        
    } else
        [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)checkFields{
    
    if(_nameTextField.text.length == 0){
        [self presentViewController:[UtilManager presentAlertWithMessage:@"" andTitle:NSLocalizedString(@"El campo nombre es oblicatorio", nil)] animated:YES completion:^{
            
        }];
        
        return FALSE;
    } else{
        if(_editNutritionPlan)
        {
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                [_editNutritionPlan setTitle:_nameTextField.text];
                [_editNutritionPlan setContent:_contentTextView.text];
                [_editNutritionPlan setNotes:_noteTextView.text];
                if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]])
                    [_editNutritionPlan setPrivacity:@"public"];
                else
                    [_editNutritionPlan setPrivacity:@"private"];
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
        }
        else{
            
        }
    }
    
    if(_editNutritionPlan){
        if(_editNutritionPlan.nutritionDays.count == 0){
            [self presentViewController:[UtilManager presentAlertWithMessage:@"" andTitle:NSLocalizedString(@"Debe añadir alguna comida", nil)] animated:YES completion:^{
                
            }];
            return FALSE;
        }
    }
    
    return TRUE;
}

#pragma mark UITextFieldDelegat Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}

#pragma mark UITextViewDelegate Methods

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
    return true;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if([[textView text] isEqualToString:NSLocalizedString(@"Descripción", nil)])
        [_contentTextView setText:@""];
    if([[textView text] isEqualToString:NSLocalizedString(@"Notas", nil)])
        [_noteTextView setText:@""];
    
    return  true;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return TRUE;
}

#pragma mark WebServiceDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        
    }];
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        if (webServiceType == WebServiceTypeUpdateNutritionPlan){
            NSDictionary *returnDictionary = (NSDictionary *)object;
            
            NSLog(@"%@",returnDictionary);
            
            //[_createMeal setServerMealId:[returnDictionary valueForKey:@"data"]];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm beginWriteTransaction];
            [realm addOrUpdateObject:_editNutritionPlan];
            NSError *error;
            [realm commitWriteTransaction:&error];
            
            if(error){
                NSLog(@"%@",error.description);
            }
            
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
        }
    }];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:nil];
}

#pragma mark - NutritionPlanAddDisplayDelegate

- (NutritionPlan*)nutritionPlanFromDisplay {
    if (_editNutritionPlan) {
        return _editNutritionPlan;
    }
    return _createNutritionPlan;
}

- (void)didPostNutritionPlan:(NutritionPlan*)nutritionPlan {
    [WebServiceManager getNutrionalPlansWithCompletionBlock:^(BOOL isSuccess) {
        if(_foodViewController) {
            [_foodViewController setCreateNutritionPlan:_createNutritionPlan];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (void)didUpdateNutritionPlan:(NutritionPlan*)nutritionPlan {
    [WebServiceManager getNutrionalPlansWithCompletionBlock:^(BOOL isSuccess) {
        if(_foodViewController) {
            [_foodViewController setCreateNutritionPlan:_createNutritionPlan];
        }
        
        if (_nutritionPlanOperationDelegate) {
            [_nutritionPlanOperationDelegate didUpdateNutritionPlan:nutritionPlan];
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

@end
