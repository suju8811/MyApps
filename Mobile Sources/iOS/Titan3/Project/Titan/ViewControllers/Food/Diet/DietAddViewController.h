//
//  DietAddViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 12/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "FoodViewController.h"
#import "SelectorFoodViewController.h"
#import "Diet.h"
#import "DietDetailViewController.h"

@interface DietAddViewController : ParentViewController

@property (nonatomic, strong) Diet *editDiet;
@property (nonatomic, strong) FoodViewController *foodViewController;
@property (nonatomic, strong) Diet *createDiet;
@property (nonatomic, strong) SelectorFoodViewController *selectorFoodViewController;
@property (nonatomic, strong) id<DietDetailViewControllerDelegate> dietDetailViewControllerDelegate;

@end
