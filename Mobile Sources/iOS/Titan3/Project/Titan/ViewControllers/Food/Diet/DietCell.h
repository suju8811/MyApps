//
//  DietCell.h
//  Titan
//
//  Created by Manuel Manzanera on 22/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyCircleView.h"

@protocol DietCellDelegate <NSObject>

- (void)dietCellActionInIndex:(NSInteger)index;

- (void)dietCellDetailInfoActionInIndex:(NSInteger)index;

@end

@interface DietCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *foodsLabel;
@property (nonatomic, strong) UILabel *kCalLabel;

@property (nonatomic, strong) PropertyCircleView *proteinCircleView;
@property (nonatomic, strong) PropertyCircleView *carbsCircleView;
@property (nonatomic, strong) PropertyCircleView *fatsCircleView;

@property (nonatomic, strong) UILabel *proteinLabel;
@property (nonatomic, strong) UILabel *carbsLabel;
@property (nonatomic, strong) UILabel *fatsLabel;

@property (nonatomic, strong) UIView *separatorView;

@property (nonatomic, strong) UIButton *infoButton;
@property (nonatomic, strong) UIButton *detailInfoButton;
@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, assign) id<DietCellDelegate>delegate;
@property (nonatomic, strong) UIImageView *checkImageView;

@end
