//
//  DietAddViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 12/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DietAddViewController.h"
#import "WebServiceManager.h"
#import "MRProgress.h"
#import "Meal.h"
#import "DietAddExecutive.h"

@interface DietAddViewController ()<UITextFieldDelegate, UITextViewDelegate, WebServiceManagerDelegate, SelectorFoodViewDelegate, DietAddDisplay>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITextField *nameTextField;

@property (nonatomic, strong) UITextView *contentTextView;
@property (nonatomic, strong) UITextView *noteTextView;

@property (nonatomic, strong) UIButton *privacityButton;
@property (nonatomic, strong) UILabel *privacityLabel;
@property (nonatomic, strong) DietAddExecutive * executive;

@end

@implementation DietAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeAction) name:NAV_RIGHT1_ACTION object:nil];
    
    [self configureView];
    
    _executive = [[DietAddExecutive alloc] initWithDisplay:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(_selectorFoodViewController.presentDietDetail){
        
        _selectorFoodViewController.presentDietDetail = FALSE;
        
        for(Meal *meal in _selectorFoodViewController.selectResults){
            DietMeal * dietMeal = [[DietMeal alloc] init];
            dietMeal.dietMealId = [UtilManager uuid];
            [dietMeal.meals addObject:meal];
            dietMeal.order = @(dietMeal.meals.count);
            [_createDiet.dietMeals addObject:dietMeal];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                [meal setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
        }
        
        [self createNewDiet];
    }
}

- (void)createNewDiet {

    [_executive createDiet];
}

- (void)closeAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)configureView{
    [self.view setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    _nameTextField = [[UITextField alloc] initWithFrame:CGRectMake([UtilManager width:10], NAV_HEIGHT, WIDTH - [UtilManager width:20], UITEXTFIELD_HEIGHT)];
    
    [_nameTextField setDelegate:self];
    [_nameTextField setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]];
    [_nameTextField setTextColor:GRAY_REGISTER_FONT];
    [_nameTextField setBackgroundColor:[UIColor clearColor]];
    [_nameTextField setKeyboardAppearance:UIKeyboardAppearanceDark];
    _nameTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Nombre", nil)
                                    attributes:@{
                                                 NSForegroundColorAttributeName:[UIColor grayColor],
                                                 NSFontAttributeName : [UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]
                                                 }
     ];
    
    if(_editDiet)
        _nameTextField.text = _editDiet.title;
    
    [self.view addSubview:_nameTextField];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _nameTextField.frame.origin.y + _nameTextField.frame.size.height , WIDTH - [UtilManager width:40], 1)];
    [separatorView setBackgroundColor:GRAY_REGISTER_FONT];
    
    [self.view addSubview:separatorView];
    
    _contentTextView = [[UITextView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _nameTextField.frame.origin.y + _nameTextField.frame.size.height + [UtilManager height:10], _nameTextField.frame.size.width, [UtilManager height:120])];
    [_contentTextView setTextColor:_nameTextField.textColor];
    [_contentTextView setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_contentTextView setTextAlignment:NSTextAlignmentJustified];
    [_contentTextView setText:NSLocalizedString(@"Descripción", nil)];
    [_contentTextView setFont:[UIFont fontWithName:ITALIC size:18]];
    [_contentTextView setBackgroundColor:[UIColor clearColor]];
    [[_contentTextView layer] setBorderWidth:1];
    [[_contentTextView layer] setBorderColor:BLACK_HEADER_APP_COLOR.CGColor];
    
    if(_editDiet)
        _contentTextView.text = _editDiet.content;
    
    [_contentTextView setDelegate:self];
    
    UIToolbar *toolbarWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //UIBarButtonItem *barWeigthButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resetView)];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetView)];
    [barButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarWeigth setItems:[NSArray arrayWithObjects:flexibleWeigthSpace, barButtonItem, nil]];
    _contentTextView.inputAccessoryView = toolbarWeigth;
    
    [self.view addSubview:_contentTextView];
    
    _noteTextView = [[UITextView alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x, _contentTextView.frame.origin.y + _contentTextView.frame.size.height + [UtilManager height:10], _nameTextField.frame.size.width, [UtilManager height:180])];
    [_noteTextView setTextColor:_nameTextField.textColor];
    [_noteTextView setKeyboardAppearance:UIKeyboardAppearanceDark];
    [_noteTextView setTextAlignment:NSTextAlignmentJustified];
    [_noteTextView setText:NSLocalizedString(@"Notas", nil)];
    [_noteTextView setFont:[UIFont fontWithName:ITALIC size:18]];
    [_noteTextView setBackgroundColor:[UIColor clearColor]];
    [[_noteTextView layer] setBorderWidth:1];
    [[_noteTextView layer] setBorderColor:BLACK_HEADER_APP_COLOR.CGColor];
    
    if(_editDiet)
        _noteTextView.text = _editDiet.note;
    
    [_noteTextView setDelegate:self];
    
    UIToolbar *toolbarNoteWeigth = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIDTH, [UtilManager height:35])];
    toolbarNoteWeigth.barStyle=UIBarStyleBlackTranslucent;
    UIBarButtonItem *flexibleNoteWeigthSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //UIBarButtonItem *barWeigthButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resetView)];
    UIBarButtonItem *barNoteButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Aceptar", nil) style:UIBarButtonItemStyleDone target:self action:@selector(resetView)];
    [barNoteButtonItem setTitle:NSLocalizedString(@"Aceptar", nil)];
    [barNoteButtonItem setTintColor:GRAY_REGISTER_FONT];
    [toolbarNoteWeigth setItems:[NSArray arrayWithObjects:flexibleNoteWeigthSpace, barNoteButtonItem, nil]];
    _noteTextView.inputAccessoryView = toolbarNoteWeigth;
    
    [self.view addSubview:_noteTextView];
    
    _privacityButton = [[UIButton alloc] initWithFrame:CGRectMake(_nameTextField.frame.origin.x,_noteTextView.frame.origin.y + _noteTextView.frame.size.height + [UtilManager height:10] , [UtilManager width:40], [UtilManager width:40])];
    [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
    [_privacityButton addTarget:self action:@selector(privacityAction) forControlEvents:UIControlEventTouchUpInside];
    
//    [self.view addSubview:_privacityButton];
    
    _privacityLabel = [[UILabel alloc] initWithFrame:CGRectMake(_privacityButton.frame.origin.x + _privacityButton.frame.size.width + [UtilManager width:10], _privacityButton.frame.origin.y, [UtilManager width:240], _privacityButton.frame.size.height)];
    [_privacityLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:16]];
    [_privacityLabel setTextAlignment:NSTextAlignmentLeft];
    [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    [_privacityLabel setTextColor:[UIColor whiteColor]];
    
//    [self.view addSubview:_privacityLabel];
    
    if([_editDiet.privacity isEqualToString:@"public"]){
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    }else{
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-closed"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Privado", nil)];
    }
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height - [UtilManager height:5], WIDTH, NAV_HEIGHT - [[UIApplication sharedApplication] statusBarFrame].size.height + [UtilManager height:5])];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    if(_editDiet)
        [titleLabel setText:NSLocalizedString(@"EDITAR DIETA", nil)];
    else
        [titleLabel setText:NSLocalizedString(@"NUEVA DIETA", nil)];
    [titleLabel setFont:[UIFont fontWithName:LIGHT_FONT size:21]];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:titleLabel];
    
    UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - [UtilManager width:50], [UtilManager height:20], [UtilManager width:40] * 0.88, [UtilManager width:40] * 0.8)];
    [okButton setImage:[UIImage imageNamed:@"icon-ok-on"] forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(didPressOKButton) forControlEvents:UIControlEventTouchUpInside];
    [okButton setCenter:CGPointMake(okButton.center.x, titleLabel.center.y)];
    
    [self.view addSubview:okButton];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake([UtilManager width:10],[UtilManager height:20.], [UtilManager width:40] * 0.8, [UtilManager width:40] * 0.8)];
    [backButton setImage:[UIImage imageNamed:@"btn-back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didPressBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton setHidden:FALSE];
    [backButton setCenter:CGPointMake(backButton.center.x, okButton.center.y)];
    
    [self.view addSubview:backButton];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didPressBackButton)];
    [swipeGesture setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:swipeGesture];
}

- (void)didPressBackButton {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)privacityAction{
    if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]]){
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-closed"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Privado", nil)];
    }else{
        [_privacityButton setImage:[UIImage imageNamed:@"img-lock-open"] forState:UIControlStateNormal];
        [_privacityLabel setText:NSLocalizedString(@"Perfil Público", nil)];
    }
}

- (void)resetView{
    [_contentTextView resignFirstResponder];
    [_noteTextView resignFirstResponder];
}

- (void)didPressOKButton {
    if ([self checkFields]) {
        if(_editDiet) {
            [_executive editDiet];
        }
        else {
            _createDiet = [[Diet alloc] init];
            _createDiet.dietId = [UtilManager uuid];
            _createDiet.title = _nameTextField.text;
            _createDiet.content = _contentTextView.text;
            _createDiet.note = _noteTextView.text;
            if ([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]]) {
                [_createDiet setPrivacity:@"public"];
            }
            else {
                [_createDiet setPrivacity:@"private"];
            }
            
            _createDiet.owner = [AppContext sharedInstance].currentUser.name;
            
            _selectorFoodViewController = [[SelectorFoodViewController alloc] init];
            _selectorFoodViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            _selectorFoodViewController.selectorType = SelectorFoodTypeDiet;
            _selectorFoodViewController.selectDiet = _createDiet;
            _selectorFoodViewController.delegate = self;
            [self presentViewController:_selectorFoodViewController animated:YES completion:nil];
        }
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (BOOL)checkFields{
    
    if(_nameTextField.text.length == 0){
        [self presentViewController:[UtilManager presentAlertWithMessage:@"" andTitle:NSLocalizedString(@"El campo nombre es oblicatorio", nil)] animated:YES completion:^{
            
        }];
        
        return FALSE;
    } else{
        if(_editDiet)
        {
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm transactionWithBlock:^{
                [_editDiet setTitle:_nameTextField.text];
                [_editDiet setContent:_contentTextView.text];
                [_editDiet setNote:_noteTextView.text];
                if([[_privacityButton.imageView image] isEqual:[UIImage imageNamed:@"img-lock-open"]])
                    [_editDiet setPrivacity:@"public"];
                else
                    [_editDiet setPrivacity:@"private"];
            }];
            
            [realm beginWriteTransaction];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
        }
        else{
            
        }
    }
    
    if(_editDiet){
        if(_editDiet.dietMeals.count == 0){
            [self presentViewController:[UtilManager presentAlertWithMessage:@"" andTitle:NSLocalizedString(@"Debe añadir alguna comida", nil)] animated:YES completion:^{
                
            }];
            return FALSE;
        }
    }
    
    return TRUE;
}

#pragma mark UITextFieldDelegat Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}

#pragma mark UITextViewDelegate Methods

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
    return true;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if([[textView text] isEqualToString:NSLocalizedString(@"Descripción", nil)])
        [_contentTextView setText:@""];
    if([[textView text] isEqualToString:NSLocalizedString(@"Notas", nil)])
        [_noteTextView setText:@""];
    
    return  true;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return TRUE;
}

#pragma mark WebServiceDelegate Methods

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType{
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        
    }];
}

- (void)dataDidDownload:(WebServiceType)webServiceType andObject:(NSObject *)object{
    
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        if(webServiceType == WebServiceTypePostDiet){
            NSDictionary *returnDictionary = (NSDictionary *)object;
            
            [_createDiet setServerDietId:[returnDictionary valueForKey:@"data"]];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm beginWriteTransaction];
            [realm addOrUpdateObject:_createDiet];
            NSError *error;
            [realm commitWriteTransaction:&error];
            
            if(error){
                NSLog(@"%@",error.description);
            }
            
            if(_foodViewController)
                [_foodViewController setCreateDiet:_createDiet];
            
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
            
        } else if(webServiceType == WebServiceTypeUpdateDiet){
            NSDictionary *returnDictionary = (NSDictionary *)object;
            
            NSLog(@"%@",returnDictionary);
            
            //[_createMeal setServerMealId:[returnDictionary valueForKey:@"data"]];
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm beginWriteTransaction];
            [realm addOrUpdateObject:_editDiet];
            NSError *error;
            [realm commitWriteTransaction:&error];
            
            if(error){
                NSLog(@"%@",error.description);
            }
            
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
            
        }
    }];
}

- (void)dataDidDownloadWithFail:(WebServiceType)webServiceType andObject:(NSObject *)object{
    [MRProgressOverlayView dismissOverlayForView:self.view animated:YES completion:^{
        
    }];
}

#pragma mark - DietAddDisplay Delegate

- (Diet*)dietFromDisplay {
    if (_editDiet) {
        return _editDiet;
    }
    
    return _createDiet;
}

- (void)didCreatedDiet:(Diet*)diet {
    [WebServiceManager getDietsWithWithCompletionBlock:^(BOOL isSuccess) {
        if(_foodViewController) {
            [_foodViewController setCreateDiet:diet];
        }
        
        [self dismissViewControllerAnimated:YES completion:^{
            if (_dietDetailViewControllerDelegate) {
                [_dietDetailViewControllerDelegate didUpdatedDiet:diet];
            }
        }];
    }];
}

@end
