//
//  DietDetailInfoView.h
//  Titan
//
//  Created by Marcus Lee on 8/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Diet.h"
#import "PropertyCircleView.h"

@interface DietDetailInfoView : UIView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *kCalLabel;
@property (nonatomic, strong) UILabel *proteinLabel;
@property (nonatomic, strong) UILabel *carbsLabel;
@property (nonatomic, strong) UILabel *fatsLabel;

@property (nonatomic, strong) PropertyCircleView *proteinCircleView;
@property (nonatomic, strong) PropertyCircleView *carbsCircleView;
@property (nonatomic, strong) PropertyCircleView *fatsCircleView;

@property (nonatomic, strong) PropertyCircleView *sugarCircleView;
@property (nonatomic, strong) PropertyCircleView *satFatsCircleView;

@property (nonatomic, strong) PropertyCircleView *monFatsCircleView;

@property (nonatomic, strong) PropertyCircleView *polFatsCircleView;

@property (nonatomic, strong) Diet *currentDiet;

//@property (nonatomic, assign) id<FoodDetailViewDelegate>delegate;

@property (nonatomic, assign) NSInteger index;

- (id)initWithFrame:(CGRect)frame
            andDiet:(Diet *)diet;

@end
