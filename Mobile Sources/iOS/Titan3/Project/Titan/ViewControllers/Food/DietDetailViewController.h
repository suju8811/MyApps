//
//  DietDetailViewController.h
//  Titan
//
//  Created by Manuel Manzanera on 2/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "Diet.h"
#import "OptionsFoodViewController.h"

@protocol DietDetailViewControllerDelegate <NSObject>

- (void)didUpdatedDiet:(Diet*)diet;

@end

@interface DietDetailViewController : ParentViewController

@property (nonatomic, strong) Diet *currentDiet;
@property (nonatomic, strong) ParentViewController * foodRootViewController;
@property (nonatomic, strong) id<OptionsFoodViewControllerDelegate> optionMenuDelegate;
@property (nonatomic, assign) NSInteger selectedMealIndex;
@property (nonatomic, assign) BOOL fromDetailFitness;

@end
