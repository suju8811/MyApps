//
//  FoodDetailView.h
//  Titan
//
//  Created by Manuel Manzanera on 7/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Aliment.h"
#import "PropertyCircleView.h"
#import "RNBlurModalView.h"
#import "Ingredient.h"

@class FoodDetailView;

@protocol FoodDetailViewDelegate <NSObject>

- (void)quantity:(NSNumber *)quantity inIndex:(NSInteger)index;
- (void)foodDetailView:(FoodDetailView*)foodDetailView didPressOKButton:(NSNumber*)quantityValue index:(NSInteger)index;

@end

@interface FoodDetailView : UIView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *kCalLabel;
@property (nonatomic, strong) UILabel *proteinLabel;
@property (nonatomic, strong) UILabel *carbsLabel;
@property (nonatomic, strong) UILabel *fatsLabel;

@property (nonatomic, strong) PropertyCircleView *proteinCircleView;
@property (nonatomic, strong) PropertyCircleView *carbsCircleView;
@property (nonatomic, strong) PropertyCircleView *fatsCircleView;

@property (nonatomic, strong) PropertyCircleView *sugarCircleView;
@property (nonatomic, strong) PropertyCircleView *satFatsCircleView;

@property (nonatomic, strong) PropertyCircleView *monFatsCircleView;

@property (nonatomic, strong) PropertyCircleView *polFatsCircleView;

@property (nonatomic, strong) RNBlurModalView * superModalView;

@property (nonatomic, strong) Aliment *currentAliment;

@property (nonatomic, strong) Ingredient *currentIngredient;

@property (nonatomic, assign) id<FoodDetailViewDelegate>delegate;

@property (nonatomic, assign) NSInteger index;

@property (nonatomic, assign) BOOL isEditable;

- (id)initWithFrame:(CGRect)frame andAliment:(Aliment *)aliment;
- (id)initWithFrame:(CGRect)frame andIngredient:(Ingredient *)ingredient;

- (void)didPressOKButton;

@end
