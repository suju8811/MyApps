//
//  PropertyCircleView.m
//  Titan
//
//  Created by Manuel Manzanera on 10/5/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "PropertyCircleView.h"

@implementation PropertyCircleView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        _initialLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., frame.size.width, frame.size.height/2.55)];
        [_initialLabel setAdjustsFontSizeToFitWidth:YES];
        [_initialLabel setTextAlignment:NSTextAlignmentCenter];
        [_initialLabel setBackgroundColor:[UIColor clearColor]];
        
        _countLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., frame.size.width * 0.85, frame.size.height * 0.55)];
        [_countLabel setAdjustsFontSizeToFitWidth:YES];
        [_countLabel setTextAlignment:NSTextAlignmentCenter];
        [_countLabel setBackgroundColor:[UIColor clearColor]];
        //[_countLabel setCenter:CGPointMake(_countLabel.center.x, self.center.y)];
        [_countLabel setCenter:CGPointMake(frame.size.width/2, self.frame.size.height/2)];
        
        _measureLabel = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width * 0.7, frame.size.height * 0.55, frame.size.width * 0.1, _countLabel.frame.size.height)];
        [_measureLabel setAdjustsFontSizeToFitWidth:YES];
        [_measureLabel setTextAlignment:NSTextAlignmentLeft];
        [_measureLabel setBackgroundColor:[UIColor clearColor]];
        [_measureLabel setCenter:CGPointMake(_measureLabel.center.x, self.frame.size.height/2)];
        
        [self addSubview:_initialLabel];
        [self addSubview:_countLabel];
        //[self addSubview:_measureLabel];
    }
    
    return self;
}
@end
