//
//  MealAlimentCell.m
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "MealAlimentCell.h"
#import "AppContext.h"
#import "UtilManager.h"
#import "AppConstants.h"

@implementation MealAlimentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self){
        
        [self setBackgroundColor:[UIColor clearColor]];
        [[self contentView] setBackgroundColor:[UIColor clearColor]];
        
        _alimentImageView = [[UIImageView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager width:10], [UtilManager width:80], [UtilManager width:80])];
        
        [[self contentView] addSubview:_alimentImageView];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_alimentImageView.frame.origin.x + _alimentImageView.frame.size.width + [UtilManager width:10], [UtilManager height:5], [UtilManager width:200], [UtilManager height:25])];
        [_titleLabel setAdjustsFontSizeToFitWidth:YES];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setTextColor:YELLOW_APP_COLOR];
        [_titleLabel setFont:[UIFont fontWithName:REGULAR_FONT_OPENSANS size:18]];
        
        [[self contentView] addSubview:_titleLabel];
        
        _quantityLabel = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, _titleLabel.frame.origin.y + _titleLabel.frame.size.height, [UtilManager width:100], [UtilManager height:20])];
        [_quantityLabel setTextColor:GRAY_REGISTER_FONT];
        [_quantityLabel setFont:[UIFont fontWithName:LIGHT_FONT size:16]];
        
        [[self contentView] addSubview:_quantityLabel];
        
        UIImageView *kCalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_quantityLabel.frame.origin.x + _quantityLabel.frame.size.width + [UtilManager width:10], _quantityLabel.frame.origin.y, [UtilManager width:20], [UtilManager width:20])];
        [kCalImageView setImage:[UIImage imageNamed:@"icon-kcal-green"]];
        
        [[self contentView] addSubview:kCalImageView];
        
        _kCalLabel = [[UILabel alloc] initWithFrame:CGRectMake(kCalImageView.frame.origin.x + kCalImageView.frame.size.width + 5, kCalImageView.frame.origin.y, [UtilManager width:90], _titleLabel.frame.size.height)];
        [_kCalLabel setTextColor:GREEN_APP_COLOR];
        [_kCalLabel setText:@"3780 kCal"];
        
        [[self contentView] addSubview:_kCalLabel];
        
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:99], WIDTH - [UtilManager width:20], 1)];
        [_separatorView setBackgroundColor:GRAY_REGISTER_FONT];
        
        [[self contentView] addSubview:_separatorView];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
