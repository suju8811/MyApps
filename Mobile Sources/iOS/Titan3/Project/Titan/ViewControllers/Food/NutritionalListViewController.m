//
//  FoodViewController.m
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "NutritionalListViewController.h"
#import "MealDetailViewController.h"
#import "DietDetailViewController.h"
#import "NutritionPlanDetailViewController.h"
#import "SegmentView.h"
#import "PropertyCircleView.h"
#import "Food.h"
#import "Diet.h"
#import "Aliment.h"
#import "Meal.h"
#import "MealCell.h"
#import "DietCell.h"
#import "AlimentCell.h"
#import "Tag.h"
#import "PlanFoodViewCell.h"
#import "RNBlurModalView.h"
#import "FoodDetailView.h"
#import "UIImageView+AFNetworking.h"
#import "MenuContainerNavController.h"
#import "OptionsFoodViewController.h"
#import "MealAddViewController.h"
#import "DietAddViewController.h"
#import "NutritionPlanAddViewController.h"

@interface NutritionalListViewController ()<UITableViewDelegate, UITableViewDataSource, AlimentCellDelegate, MealCellDelegate, OptionsFoodViewControllerDelegate, DietCellDelegate, PlanFoodCellDelegate>

@property (nonatomic, strong) UIButton *profileButton;
@property (nonatomic, strong) UIButton *favButton;
@property (nonatomic, strong) UIButton *comunityButton;
@property (nonatomic, strong) UIButton *downloadButton;

@property (nonatomic, strong) SegmentView *segmentedView;
@property (strong, nonatomic) NSArray *segmentTitles;

@property (nonatomic, strong) UITableView *foodTableView;

@property (nonatomic, strong) UIButton *planButton;
@property (nonatomic, strong) UIButton *dietButton;
@property (nonatomic, strong) UIButton *eatButton;
@property (nonatomic, strong) UIButton *foodButton;

@property (nonatomic, strong) UILabel *planLabel;
@property (nonatomic, strong) UILabel *dietLabel;
@property (nonatomic, strong) UILabel *eatLabel;
@property (nonatomic, strong) UILabel *foodLabel;

@property (nonatomic, strong) UISegmentedControl *segmentControl;
@property (nonatomic) NSInteger segmentState;

@property (nonatomic, strong) RLMResults *items;
@property (nonatomic, strong) RNBlurModalView *modalView;

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIView *segmentControlView;

@property (nonatomic, strong) NSMutableArray *foodIndex;

@property (nonatomic, strong) MenuContainerNavController *menuContainerNavController;

@property (nonatomic, strong) UILabel *warningLabel;

@end

@implementation NutritionalListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _menuContainerNavController = (MenuContainerNavController *)self.navigationController;
    _selectType = FoodSelectTypePlan;
    
    _items = [NutritionPlan getNutritionPlansWithOwner:[[[AppContext sharedInstance] currentUser] userId]];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NAV_RIGHT1_ACTION object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addAction) name:NAV_RIGHT1_ACTION object:nil];
    
    switch (_selectType) {
        case FoodSelectTypePlan:{
            
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"PLANES DE NUTRICIÓN", nil)];
            
            if(_nutritionPlanSelectType == NutritionPlanSelectTypeUser)
                _items = [NutritionPlan getNutritionPlansWithOwner:[[AppContext sharedInstance] currentUser].userId];
            else if(_nutritionPlanSelectType == NutritionPlanSelectTypeFavourite)
                _items = [NutritionPlan getFavouritesNutritionPlans];
            else if(_nutritionPlanSelectType == NutritionPlanSelectTypeAdmin)
                _items = [NutritionPlan getNutritionPlansWithOwner:ADMIN_OWNER_ID];
            else if(_nutritionPlanSelectType == NutritionPlanSelectTypeShared)
                _items = nil;
            
            [_planButton setSelected:TRUE];
            [_dietButton setSelected:FALSE];
            [_foodButton setSelected:FALSE];
            [_eatButton setSelected:FALSE];
            if(_items.count == 0)
                [_warningLabel setText:NSLocalizedString(@"Todavía no has creado ningún plan nutricional", nil)];
            else
                [_warningLabel setText:NSLocalizedString(@"", nil)];
        }
            break;
        case FoodSelectTyeDiet:{
            
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"DIETAS", nil)];
            
            if(_dietSelectType == DietSelectTypeUser)
                _items = [Diet getDietsWithOwner:[[AppContext sharedInstance] currentUser].userId];
            else if(_dietSelectType == DietSelectTypeFavourite)
                _items = [Diet getFavouritesDiets];
            else if(_dietSelectType == DietSelectTypeAdmin)
                _items = [Diet getDietsWithOwner:ADMIN_OWNER_ID];
            else if(_dietSelectType == DietSelectTypeShared)
                _items = nil;
            
            [_planButton setSelected:FALSE];
            [_dietButton setSelected:TRUE];
            [_eatButton setSelected:FALSE];
            [_foodButton setSelected:FALSE];
            
            if(_items.count == 0)
                [_warningLabel setText:NSLocalizedString(@"Todavía no hay ninguna dieta", nil)];
            else
                [_warningLabel setText:NSLocalizedString(@"", nil)];
        }
            break;
        case FoodSelectTypeMeal:{
            
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"COMIDAS", nil)];
            
            if(_mealSelectType == MealSelectTypeUser)
                _items = [Meal getMealsWithOwner:[[AppContext sharedInstance] currentUser].userId];
            else if(_mealSelectType == MealSelectTypeFavourite)
                _items = [Meal getFavouritesMeals];
            else if(_mealSelectType == MealSelectTypeAdmin)
                _items = [Meal getMealsWithOwner:ADMIN_OWNER_ID];
            else if(_mealSelectType == MealSelectTypeShared)
                _items = nil;
            
            [_planButton setSelected:FALSE];
            [_dietButton setSelected:FALSE];
            [_eatButton setSelected:TRUE];
            [_foodButton setSelected:FALSE];
            
            if(_items.count == 0)
                [_warningLabel setText:NSLocalizedString(@"Todavía no hay ninguna commida", nil)];
            else
                [_warningLabel setText:NSLocalizedString(@"", nil)];
            break;
        }
        case FoodSelectTypeAliment:{
        
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"ALIMENTOS", nil)];
            
            break;
        }
        default:
            break;
    }
    
    [_foodTableView reloadData];
}


- (void)configureView{
    [[self view] setBackgroundColor:BLACK_APP_COLOR];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImageView setImage:[UIImage imageNamed:@"img-blur-bkg"]];
    [self.view addSubview:backgroundImageView];
    
    //[self.view addSubview:[self headerView]];
    
    _warningLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., [UtilManager width:200], [UtilManager height:60])];
    [_warningLabel setNumberOfLines:0];
    [_warningLabel setTextColor:GRAY_REGISTER_FONT];
    [_warningLabel setFont:[UIFont fontWithName:REGULAR_FONT size:18]];
    [_warningLabel setCenter:self.view.center];
    [_warningLabel setTextAlignment:NSTextAlignmentCenter];
    
    [[self view] addSubview:_warningLabel];
    
    _foodTableView = [[UITableView alloc] initWithFrame:CGRectMake(0., STATUS_BAR_HEIGHT + TAB_BAR_HEIGHT, WIDTH, HEIGHT - TAB_BAR_HEIGHT- FOOTER_BAR_HEIGHT- STATUS_BAR_HEIGHT) style:UITableViewStylePlain];
    [_foodTableView registerClass:[MealCell class] forCellReuseIdentifier:@"foodCell"];
    [_foodTableView registerClass:[PlanFoodViewCell class] forCellReuseIdentifier:@"planFoodCell"];
    [_foodTableView registerClass:[DietCell class] forCellReuseIdentifier:@"dietCell"];
    [_foodTableView registerClass:[AlimentCell class] forCellReuseIdentifier:@"alimentCell"];
    [_foodTableView setBounces:FALSE];
    [_foodTableView setDelegate:self];
    [_foodTableView setDataSource:self];
    [_foodTableView setBackgroundColor:[UIColor clearColor]];
    [_foodTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_foodTableView setSectionIndexColor:YELLOW_APP_COLOR];
    [_foodTableView setSectionIndexBackgroundColor:[UIColor clearColor]];
    [_foodTableView setBounces:FALSE];
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(rightSwipe)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    
    [_foodTableView addGestureRecognizer:recognizer];
    
    [self.view addSubview:_foodTableView];
    
//    [self.view addSubview:[self footerView]];
    
    [self addHeaderView];
    [self addSegmentControlView];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0., HEIGHT - TAB_BAR_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];
    [closeButton setTitle:NSLocalizedString(@"Cerrar", nil) forState:UIControlStateNormal];
    [[closeButton titleLabel] setFont:[UIFont fontWithName:BOLD_FONT size:19]];
    [closeButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:closeButton];
    
}

- (void)dismissView {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)rightSwipe{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NOTIF_MENU_SLIDE" object:nil];
}

- (void)addHeaderView{
    _headerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:20], STATUS_BAR_HEIGHT, WIDTH - [UtilManager width:40], TAB_BAR_HEIGHT)];
    
    _favButton = [[UIButton alloc] initWithFrame:CGRectMake(0., [UtilManager height:5], [UtilManager width:40] * 0.8, [UtilManager width:40]* 0.8)];
    [_favButton setImage:[UIImage imageNamed:@"icon-fav-off"] forState:UIControlStateNormal];
    [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateSelected];
    [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateHighlighted];
    [_favButton setCenter:CGPointMake(_headerView.frame.size.width * 3/8, _favButton.center.y)];
    [_favButton setTag:0];
    [_favButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:_favButton];
    
    _profileButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
    [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateSelected];
    [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateHighlighted];
    [_profileButton setImage:[UIImage imageNamed:@"icon-profile-off"] forState:UIControlStateNormal];
    [_profileButton setCenter:CGPointMake(_headerView.frame.size.width/8, _favButton.center.y)];
    [_profileButton setTag:1];
    [_profileButton setSelected:YES];
    [_profileButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:_profileButton];
    
    _comunityButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
    [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateSelected];
    [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateHighlighted];
    [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-off"] forState:UIControlStateNormal];
    [_comunityButton setCenter:CGPointMake(5 * _headerView.frame.size.width/8, _favButton.center.y)];
    [_comunityButton setTag:2];
    [_comunityButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:_comunityButton];
    
    _downloadButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
    [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateSelected];
    [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateHighlighted];
    [_downloadButton setImage:[UIImage imageNamed:@"icon-download-off"] forState:UIControlStateNormal];
    [_downloadButton setCenter:CGPointMake(7 * _headerView.frame.size.width/8, _favButton.center.y)];
    [_downloadButton setTag:3];
    [_downloadButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_headerView addSubview:_downloadButton];
    
    [[self view] addSubview:_headerView];
}

- (void)addSegmentControlView{
    _segmentControlView = [[UIView alloc] initWithFrame:CGRectMake(0, NAV_HEIGHT, WIDTH, TAB_BAR_HEIGHT)];

    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = _segmentControlView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //[headerView addSubview:blurEffectView];
    
    _segmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Alimentos", nil),NSLocalizedString(@"Suplementos", nil)]];
    [_segmentControl setSelectedSegmentIndex:_segmentState];
    [_segmentControl addTarget:self action:@selector(changeAlimentsView:) forControlEvents:UIControlEventValueChanged];
    [_segmentControl setBackgroundColor:BLACK_APP_COLOR];
    [_segmentControl setCenter:CGPointMake(_segmentControlView.center.x, _segmentControlView.frame.size.height/2)];
    [_segmentControl setTintColor:YELLOW_APP_COLOR];
    
    [_segmentControlView addSubview:_segmentControl];
    
    [_segmentControlView setHidden:TRUE];
    
    [[self view] addSubview:_segmentControlView];
}

- (UIView *)footerView{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0., HEIGHT - FOOTER_BAR_HEIGHT, WIDTH, FOOTER_BAR_HEIGHT)];
    [footerView setBackgroundColor:BLACK_HEADER_APP_COLOR];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = footerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //[footerView addSubview:blurEffectView];
    
    _planButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., TAB_BAR_HEIGHT * 0.6, TAB_BAR_HEIGHT * 0.6)];
    //[_planButton setTitle:NSLocalizedString(@"PLANES", nil) forState:UIControlStateNormal];
    //[_planButton setTitleColor:GRAY_REGISTER_FONT forState:UIControlStateNormal];
    //[_planButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateHighlighted];
    //[_planButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateSelected];
    //[[_planButton titleLabel] setFont:[UIFont fontWithName:REGULAR_FONT size:17]];
    [_planButton setImage:[UIImage imageNamed:@"icon-footer-plan"] forState:UIControlStateNormal];
    [_planButton setImage:[_planButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    [_planButton setTintColor:YELLOW_APP_COLOR];
    [_planButton setSelected:TRUE];
    [_planButton setTag:0];
    [_planButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [_planButton setCenter:CGPointMake(WIDTH/8, footerView.frame.size.height * 0.4)];
    [footerView addSubview:_planButton];
    
    _planLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., _planButton.frame.origin.y + _planButton.frame.size.height + [UtilManager height:5], WIDTH/4, [UtilManager height:10])];
    [_planLabel setTextAlignment:NSTextAlignmentCenter];
    [_planLabel setCenter:CGPointMake(_planButton.center.x, _planLabel.center.y)];
    [_planLabel setText:NSLocalizedString(@"Planes", nil)];
    [_planLabel setFont:[UIFont fontWithName:BOLD_FONT size:9]];
    [_planLabel setTextColor:YELLOW_APP_COLOR];
    
    [footerView addSubview:_planLabel];
    
    _dietButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _planButton.frame.size.width, _planButton.frame.size.height)];
    //[_dietButton setTitle:NSLocalizedString(@"DIETAS\nDIARIAS", nil) forState:UIControlStateNormal];
    //[_dietButton setTitleColor:GRAY_REGISTER_FONT forState:UIControlStateNormal];
    //[_dietButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateHighlighted];
    //[_dietButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateSelected];
    //[[_dietButton titleLabel] setFont:_planButton.titleLabel.font];
    [_dietButton setImage:[UIImage imageNamed:@"icon-footer-diet"] forState:UIControlStateNormal];
    [_dietButton setImage:[_dietButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    [_dietButton setTintColor:YELLOW_APP_COLOR];
    [_dietButton setTag:1];
    [_dietButton setCenter:CGPointMake(WIDTH * 3 / 8, _planButton.center.y)];
    [_dietButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_dietButton];
    
    _dietLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., _planLabel.frame.origin.y, WIDTH/4, [UtilManager height:10])];
    [_dietLabel setTextAlignment:NSTextAlignmentCenter];
    [_dietLabel setCenter:CGPointMake(_dietButton.center.x, _planLabel.center.y)];
    [_dietLabel setText:NSLocalizedString(@"Dietas", nil)];
    [_dietLabel setFont:[UIFont fontWithName:BOLD_FONT size:9]];
    [_dietLabel setTextColor:[UIColor whiteColor]];
    
    [footerView addSubview:_dietLabel];
    
    _eatButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _planButton.frame.size.width, _planButton.frame.size.height)];
    //[_eatButton setTitle:NSLocalizedString(@"COMIDAS", nil) forState:UIControlStateNormal];
    //[_eatButton setTitleColor:GRAY_REGISTER_FONT forState:UIControlStateNormal];
    //[_eatButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateHighlighted];
    //[_eatButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateSelected];
    [_eatButton setImage:[UIImage imageNamed:@"icon-footer-food"] forState:UIControlStateNormal];
    [_eatButton setImage:[_eatButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    [_eatButton setTintColor:YELLOW_APP_COLOR];
    [_eatButton setCenter:CGPointMake(WIDTH * 5 / 8, _planButton.center.y)];
    [_eatButton setTag:2];
    [[_eatButton titleLabel] setFont:_planButton.titleLabel.font];
    [_eatButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_eatButton];
    
    _eatLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., _planLabel.frame.origin.y , WIDTH/4, [UtilManager height:10])];
    [_eatLabel setTextAlignment:NSTextAlignmentCenter];
    [_eatLabel setCenter:CGPointMake(_eatButton.center.x, _planLabel.center.y)];
    [_eatLabel setText:NSLocalizedString(@"Comidas", nil)];
    [_eatLabel setFont:[UIFont fontWithName:BOLD_FONT size:9]];
    [_eatLabel setTextColor:[UIColor whiteColor]];
    
    [footerView addSubview:_eatLabel];
    
    _foodButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0.,_planButton.frame.size.width, _planButton.frame.size.height)];
    //[_foodButton setTitle:NSLocalizedString(@"ALIMENTOS", nil) forState:UIControlStateNormal];
    //[_foodButton setTitleColor:GRAY_REGISTER_FONT forState:UIControlStateNormal];
    //[_foodButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateHighlighted];
    //[_foodButton setTitleColor:YELLOW_APP_COLOR forState:UIControlStateSelected];
    [_foodButton setImage:[UIImage imageNamed:@"icon-footer-aliment"] forState:UIControlStateNormal];
    [_foodButton setImage:[_foodButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    [_foodButton setTintColor:YELLOW_APP_COLOR];
    [_foodButton setCenter:CGPointMake(WIDTH * 7 / 8, _planButton.center.y)];
    [_foodButton setTag:3];
    [[_foodButton titleLabel] setFont:_planButton.titleLabel.font];
    [_foodButton addTarget:self action:@selector(footerAction:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:_foodButton];
    
    _foodLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., _planLabel.frame.origin.y, WIDTH/4, [UtilManager height:10])];
    [_foodLabel setTextAlignment:NSTextAlignmentCenter];
    [_foodLabel setCenter:CGPointMake(_foodButton.center.x, _planLabel.center.y)];
    [_foodLabel setText:NSLocalizedString(@"Alimentos", nil)];
    [_foodLabel setFont:[UIFont fontWithName:BOLD_FONT size:9]];
    [_foodLabel setTextColor:[UIColor whiteColor]];
    
    [footerView addSubview:_foodLabel];
    
    return footerView;
}

- (void)headerAction:(id)sender{
    UIButton *touchButton = (UIButton *)sender;
    
    switch (touchButton.tag) {
        case 0:
            
            _nutritionPlanSelectType = NutritionPlanSelectTypeFavourite;
            _dietSelectType = DietSelectTypeFavourite;
            _mealSelectType = MealSelectTypeFavourite;
            
            [_profileButton setSelected:FALSE];
            [_favButton setSelected:YES];
            [_downloadButton setSelected:FALSE];
            [_comunityButton setSelected:FALSE];
        {
            if(_selectType == FoodSelectTypeMeal)
                _items = [Meal getFavouritesMeals];
            if(_selectType == FoodSelectTyeDiet)
                _items = [Diet getFavouritesDiets];
            if(_selectType == FoodSelectTypePlan)
                _items = [NutritionPlan getFavouritesNutritionPlans];
            
        }
            break;
        case 1:
            
            _nutritionPlanSelectType = NutritionPlanSelectTypeUser;
            _dietSelectType = DietSelectTypeUser;
            _mealSelectType = MealSelectTypeUser;
            
            [_profileButton setSelected:YES];
            [_favButton setSelected:FALSE];
            [_downloadButton setSelected:FALSE];
            [_comunityButton setSelected:FALSE];
        {
            if(_selectType == FoodSelectTypeMeal)
                _items = [Meal getMealsWithOwner:[[[AppContext sharedInstance] currentUser] userId]];
            if(_selectType == FoodSelectTyeDiet)
                _items = [Diet getDietsWithOwner:[[[AppContext sharedInstance] currentUser] userId]];
            if(_selectType == FoodSelectTypePlan)
                _items = [NutritionPlan getNutritionPlansWithOwner:[[[AppContext sharedInstance] currentUser] userId]];
        }
            break;
        case 2:
            
            _nutritionPlanSelectType = NutritionPlanSelectTypeAdmin;
            _dietSelectType = DietSelectTypeAdmin;
            _mealSelectType = MealSelectTypeAdmin;
            
            [_profileButton setSelected:FALSE];
            [_favButton setSelected:FALSE];
            [_downloadButton setSelected:FALSE];
            [_comunityButton setSelected:TRUE];
        {
            if(_selectType == FoodSelectTypeMeal)
                _items = [Meal getMealsWithOwner:ADMIN_OWNER_ID];
            if(_selectType == FoodSelectTyeDiet)
                _items = [Diet getDietsWithOwner:ADMIN_OWNER_ID];
            if(_selectType == FoodSelectTypePlan)
                _items = [NutritionPlan getNutritionPlansWithOwner:ADMIN_OWNER_ID];
        }
            break;
        case 3:
            
            _nutritionPlanSelectType = NutritionPlanSelectTypeShared;
            _dietSelectType = DietSelectTypeShared;
            _mealSelectType = MealSelectTypeShared;
            
            [_profileButton setSelected:FALSE];
            [_favButton setSelected:FALSE];
            [_downloadButton setSelected:TRUE];
            [_comunityButton setSelected:FALSE];
        {
            if(_selectType == FoodSelectTypeMeal)
                _items = nil;
            if(_selectType == FoodSelectTypePlan)
                _items = nil;
            if(_selectType == FoodSelectTyeDiet)
                _items = nil;
        }
            break;
        default:
            break;
    }
    
    [_foodTableView reloadData];
    
}

- (void)footerAction:(id)sender{
    
    UIButton *touchButton = (UIButton *)sender;
    
    /*
     */
    
    switch (touchButton.tag) {
        case 0:
        {
            //[UIView animateWithDuration:0.5 animations:^{
            //    [_headerView setHidden:FALSE];
            //    [_foodTableView setFrame:CGRectMake(0., NAV_HEIGHT + TAB_BAR_HEIGHT, WIDTH, HEIGHT - 2 * TAB_BAR_HEIGHT - NAV_HEIGHT)];
            //}];
            
            [_segmentControlView setHidden:TRUE];
            [_headerView setHidden:FALSE];
            
            _selectType = FoodSelectTypePlan;
            [_planButton setSelected:TRUE];
            [_dietButton setSelected:FALSE];
            [_foodButton setSelected:FALSE];
            [_eatButton setSelected:FALSE];
            
            [_planLabel setTextColor:YELLOW_APP_COLOR];
            [_dietLabel setTextColor:[UIColor whiteColor]];
            [_foodLabel setTextColor:[UIColor whiteColor]];
            [_eatLabel setTextColor:[UIColor whiteColor]];
            
            if(_profileButton.selected)
                _items = [NutritionPlan getNutritionPlansWithOwner:[[AppContext sharedInstance] currentUser].userId];
            else if(_favButton.selected)
                _items = [NutritionPlan getFavouritesNutritionPlans];
            else if(_comunityButton.selected)
                _items = [NutritionPlan getNutritionPlansWithOwner:ADMIN_OWNER_ID];
            else
                _items = nil;
            
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"PLANES NUTRICIONALES", nil)];
            
            break;
        }
        case 1:
        {
            //[UIView animateWithDuration:0.5 animations:^{
            //    [_headerView setHidden:FALSE];
            //    [_foodTableView setFrame:CGRectMake(0., NAV_HEIGHT + TAB_BAR_HEIGHT, WIDTH, HEIGHT - 2 * TAB_BAR_HEIGHT - NAV_HEIGHT)];
            //}];
            _selectType = FoodSelectTyeDiet;
            
            [_segmentControlView setHidden:TRUE];
            [_headerView setHidden:FALSE];
            
            [_planButton setSelected:FALSE];
            [_dietButton setSelected:TRUE];
            [_eatButton setSelected:FALSE];
            [_foodButton setSelected:FALSE];
            
            [_planLabel setTextColor:[UIColor whiteColor]];
            [_dietLabel setTextColor:YELLOW_APP_COLOR];
            [_foodLabel setTextColor:[UIColor whiteColor]];
            [_eatLabel setTextColor:[UIColor whiteColor]];
            
            if(_profileButton.selected)
                _items = [Diet getDietsWithOwner:[[AppContext sharedInstance] currentUser].userId];
            else if(_favButton.selected)
                _items = [Diet getFavouritesDiets];
            else if(_comunityButton.selected)
                _items = [Diet getDietsWithOwner:ADMIN_OWNER_ID];
            else
                _items = nil;
            
            
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"DIETAS", nil)];
            
            break;
        }
        case 2:
        {
            //[UIView animateWithDuration:0.5 animations:^{
            //    [_headerView setHidden:FALSE];
            //    [_foodTableView setFrame:CGRectMake(0., NAV_HEIGHT + TAB_BAR_HEIGHT, WIDTH, HEIGHT - 2 * TAB_BAR_HEIGHT - NAV_HEIGHT)];
            //}];
            _selectType = FoodSelectTypeMeal;
            
            [_segmentControlView setHidden:TRUE];
            [_headerView setHidden:FALSE];
            
            [_planButton setSelected:FALSE];
            [_dietButton setSelected:FALSE];
            [_eatButton setSelected:TRUE];
            [_foodButton setSelected:FALSE];
            
            [_planLabel setTextColor:[UIColor whiteColor]];
            [_dietLabel setTextColor:[UIColor whiteColor]];
            [_foodLabel setTextColor:[UIColor whiteColor]];
            [_eatLabel setTextColor:YELLOW_APP_COLOR];
            
    
            if(_profileButton.selected)
                _items = [Meal getMealsWithOwner:[[AppContext sharedInstance] currentUser].userId];
            else if(_favButton.selected)
                _items = [Meal getFavouritesMeals];
            else if(_comunityButton.selected)
                _items = [Meal getMealsWithOwner:ADMIN_OWNER_ID];
            else
                _items = nil;
            
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"COMIDAS", nil)];
            
            break;
        }
        case 3:
        {
            //[UIView animateWithDuration:0.5 animations:^{
            //    [_headerView setHidden:TRUE];
            //    [_foodTableView setFrame:CGRectMake(0., NAV_HEIGHT, WIDTH, HEIGHT - NAV_HEIGHT - TAB_BAR_HEIGHT)];
            //}];
            
            _selectType = FoodSelectTypeAliment;
            
            [_segmentControlView setHidden:FALSE];
            [_headerView setHidden:TRUE];
            
            _items = [Aliment getAliments];
            
            for (int i=0; i<[_items count]; i++)
            {
                Aliment *aliment = [_items objectAtIndex:i];
                
                //---get the first char of each state---
                char alphabet = [aliment.title characterAtIndex:0];
                NSString *uniChar = [NSString stringWithFormat:@"%c", alphabet];
                
                if(!_foodIndex)
                    _foodIndex = [[NSMutableArray alloc] init];
                //---add each letter to the index array---
                if (![_foodIndex containsObject:uniChar])
                    [_foodIndex addObject:uniChar];
            }
            
            [_planButton setSelected:FALSE];
            [_dietButton setSelected:FALSE];
            [_eatButton setSelected:FALSE];
            [_foodButton setSelected:TRUE];
            
            [_planLabel setTextColor:[UIColor whiteColor]];
            [_dietLabel setTextColor:[UIColor whiteColor]];
            [_foodLabel setTextColor:YELLOW_APP_COLOR];
            [_eatLabel setTextColor:[UIColor whiteColor]];
            
            [_menuContainerNavController.titleLabel setText:NSLocalizedString(@"ALIMENTOS/SUPLEMENTOS", nil)];
            
            break;
        }
        default:
            break;
    }
    
    if(_items > 0)
        [_warningLabel setText:NSLocalizedString(@"", nil)];
    
    [_foodTableView reloadData];
}

- (void)changeAlimentsView:(UISegmentedControl *)segmentControl{
    UISegmentedControl *currentSegmentControl = segmentControl;
    
    switch (currentSegmentControl.selectedSegmentIndex) {
        case 1:
            _items = nil;
            break;
        case 0:
            _items = [Aliment getAliments];
            break;
        default:
            break;
    }
    
    _segmentState = currentSegmentControl.selectedSegmentIndex;
    [_foodTableView reloadData];
}

- (void)addAction{
    switch (_selectType) {
        case FoodSelectTypeMeal:
            [self presentMealAddViewController];
            break;
        case FoodSelectTyeDiet:
            [self presentDietAddViewController];
            break;
        case FoodSelectTypePlan:
            [self presentNutritionPlanAddViewController];
            break;
        default:
            break;
    }
}

- (void)presentMealAddViewController{
    MealAddViewController *mealAddViewController = [[MealAddViewController alloc] init];
    [mealAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [mealAddViewController setFoodViewController:self];
    
    [self presentViewController:mealAddViewController animated:YES completion:^{
        
    }];
}

- (void)presentDietAddViewController{
    DietAddViewController *dietAddViewController = [[DietAddViewController alloc] init];
    [dietAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [dietAddViewController setFoodViewController:self];
    
    [self presentViewController:dietAddViewController animated:YES completion:^{
        
    }];
}

- (void)presentNutritionPlanAddViewController{
    NutritionPlanAddViewController *nutritionPlanAddViewController = [[NutritionPlanAddViewController alloc] init];
    [nutritionPlanAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [nutritionPlanAddViewController setFoodViewController:self];
    
    [self presentViewController:nutritionPlanAddViewController animated:YES completion:^{
        
    }];
}


#pragma mark UITableviewDataSource Methods

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    //if(_selectType == FoodSelectTypeAliment)
    //    return _foodIndex;
    //else
        return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(_selectType == FoodSelectTypeAliment)
    {
        NSString *alphabet = [_foodIndex objectAtIndex:section];
        NSPredicate *predicate =
        [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", alphabet];
        
        NSMutableArray *raceNameList = [[NSMutableArray alloc] init];
        for(int i=0;i<[_items count];i++)
        {
            Aliment *aliment = [_items objectAtIndex:i];
            [raceNameList addObject:aliment.title];
        }
        
        NSArray *racesList =[raceNameList filteredArrayUsingPredicate:predicate];
        
        //NSNumber *numberOfRows = [NSNumber numberWithInteger:[racesList count]] ;
        
        //[sectionIndex addObject:numberOfRows];
        //return [racesList  count];
        
        return _items.count;
        
    }
    
    return _items.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //if(_selectType == FoodSelectTypeAliment)
    //    return _foodIndex.count;
    //else
        return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (_selectType) {
        case FoodSelectTypePlan:
            return planFooodCellHeight;
            break;
        case FoodSelectTyeDiet:
            return dietCellHeight;
            break;
        case FoodSelectTypeMeal:
            return foodCellHeight;
            break;
        case FoodSelectTypeAliment:
            return alimentCellHeigtht;
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (_selectType) {
        case FoodSelectTypePlan:
            return planFooodCellHeight;
            break;
        case FoodSelectTyeDiet:
            return dietCellHeight;
            break;
        case FoodSelectTypeMeal:
            return foodCellHeight;
            break;
        case FoodSelectTypeAliment:
            return alimentCellHeigtht;
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (_selectType) {
        case FoodSelectTypePlan:{
            
            NutritionPlan *nutritionPlan  = [_items objectAtIndex:indexPath.row];
            
            PlanFoodViewCell *cell = [_foodTableView dequeueReusableCellWithIdentifier:@"planFoodCell"];
           
            [[cell titleLabel] setText:nutritionPlan.title];
            [[cell contentLabel] setText:nutritionPlan.content];
            [[cell daysLabel] setText:[NSString stringWithFormat:@"%lu %@",(unsigned long)nutritionPlan.nutritionDays.count,NSLocalizedString(@"días", nil)]];
           
            [cell setDelegate:self];
            [cell setCurrentIndex:indexPath.row];
            
            return cell;
        }
        break;
        case FoodSelectTyeDiet:
        {
            DietCell *cell = [_foodTableView dequeueReusableCellWithIdentifier:@"dietCell"];
            
            Diet *diet = [_items objectAtIndex:indexPath.row];
            if(diet.isFavourite.boolValue){
                NSMutableAttributedString *myString=[[NSMutableAttributedString alloc] initWithString:diet.title
                                                                                           attributes:@{
                                                                                                        NSForegroundColorAttributeName:YELLOW_APP_COLOR,
                                                                                                        NSFontAttributeName : [UIFont fontWithName:SEMIBOLD_FONT size:15]
                                                                                                        }
                                                     ];
                
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                attachment.image = [UIImage imageNamed:@"icon-fav-on"];
                attachment.bounds = CGRectMake(5., -2., cell.titleLabel.frame.size.height, cell.titleLabel.frame.size.height);
                NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
                [myString appendAttributedString:attachmentString];
                cell.titleLabel.attributedText = myString;
            }else
                [[cell titleLabel] setText:diet.title];
            [cell setCurrentIndex:indexPath.row];
            [cell setDelegate:self];
            
            [[cell kCalLabel] setText:[NSString stringWithFormat:@"%d kCals", (int)[Diet getKCalInDiet:diet]]];
            [[[cell proteinCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",[Diet getDietProtein:diet]]];
            [[[cell carbsCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",[Diet getDietCarb:diet]]];
            [[[cell fatsCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",[Diet getDietFats:diet]]];
            
            return cell;
        }
            break;
        case FoodSelectTypeMeal:
        {
            MealCell *cell = [_foodTableView dequeueReusableCellWithIdentifier:@"foodCell"];
            
            [cell setDelegate:self];
            
            Meal *meal = [_items objectAtIndex:indexPath.row];
            
            if(meal.isFavourite.boolValue){
                NSMutableAttributedString *myString=[[NSMutableAttributedString alloc] initWithString:meal.title
                                                                                           attributes:@{
                                                                                                        NSForegroundColorAttributeName:YELLOW_APP_COLOR,
                                                                                                        NSFontAttributeName : [UIFont fontWithName:SEMIBOLD_FONT size:15]
                                                                                                        }
                                                     ];
                
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                attachment.image = [UIImage imageNamed:@"icon-fav-on"];
                attachment.bounds = CGRectMake(5., -2., cell.titleLabel.frame.size.height, cell.titleLabel.frame.size.height);
                NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
                [myString appendAttributedString:attachmentString];
                cell.titleLabel.attributedText = myString;
            }else
                [[cell titleLabel] setText:meal.title];
            
            
            UIImageView *imageView = cell.foodImageView;
            
            NSURLRequest *foodImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_MEAL,meal.image]]
                                                              cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                          timeoutInterval:60];
            
            [cell.imageView setImageWithURLRequest:foodImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                [imageView setImage:image];
                [imageView setContentMode:UIViewContentModeScaleAspectFit];
                [[imageView layer] setMasksToBounds:YES];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                NSLog(@"%@",error);
                [imageView setImage:[UIImage imageNamed:@"img-placeholder"]];
            }];
            
            NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:meal.tags.count];
            
            for(Tag *tag in meal.tags){
                [tags addObject:tag];
            }
            
            [[cell tagView] setTags:tags];
            [[cell tagView] reloadTagSubviews];
            [[cell tagView] setHidden:FALSE];
            
            [[cell kCalLabel] setText:[NSString stringWithFormat:@"%d kCals", (int)[Meal getKCalInMeal:meal]]];
            [[[cell proteinCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",[Meal getMealProtein:meal]]];
            [[[cell carbsCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",[Meal getMealCarb:meal]]];
            [[[cell fatsCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",[Meal getMealFats:meal]]];
            
            [cell setCurrentIndex:indexPath.row];
            
            return cell;
        }
            break;
        case FoodSelectTypeAliment:
        {
            AlimentCell *cell = [_foodTableView dequeueReusableCellWithIdentifier:@"alimentCell"];
            
            [[cell tagView] setHidden:FALSE];
            [[cell tagView] setUserInteractionEnabled:FALSE];
            
            //NSString *alphabet = [_foodIndex objectAtIndex:[indexPath  section]];
            
            //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", alphabet];
            
            //NSMutableArray *countryNameList = [[NSMutableArray alloc] init];
            //for(int i=0;i<[_items count];i++)
            //{
            Aliment *aliment = [_items objectAtIndex:indexPath.row];
            //    [countryNameList addObject:aliment.title];
            //}
            
            //NSArray *racesList =[countryNameList filteredArrayUsingPredicate:predicate];
            
            //if([racesList count]>0)
            //{
            //    NSString *currentAliment = [racesList objectAtIndex:indexPath.row];
            
            
            //    Aliment *aliment = [Aliment getAlimentWithName:currentAliment];
                [[cell titleLabel] setText:aliment.title];
                
                UIImageView *imageView = cell.foodImageView;
                
                NSURLRequest *foodImageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PATH_IMAGE_FOOD,aliment.image]]
                                                                       cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                                   timeoutInterval:60];
                
                [cell.imageView setImageWithURLRequest:foodImageRequest placeholderImage:[UIImage imageNamed:@"img-placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, UIImage *image){
                    [imageView setImage:image];
                    [imageView setContentMode:UIViewContentModeScaleAspectFit];
                    [[imageView layer] setMasksToBounds:YES];
                } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                     NSLog(@"%@",error);
                    [imageView setImage:[UIImage imageNamed:@"img-placeholder"]];
                }];
                
                NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:0];
                
                Tag *aliment1Tag = aliment.tag1;
                Tag *alimentTag2 = aliment.tag2;
                
                if(aliment1Tag)
                    [tags addObject:aliment1Tag];
                if(alimentTag2)
                    [tags addObject:alimentTag2];
                
                [[cell tagView] setTags:tags];
                [[cell tagView] reloadTagSubviews];
                
                [[cell kCalLabel] setText:[NSString stringWithFormat:@"%ld kCals",(long)aliment.calories.intValue]];
                [[[cell proteinCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",(long)aliment.proteins.intValue]];
                [[[cell carbsCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",(long)aliment.carbs.intValue]];
                [[[cell fatsCircleView] countLabel] setText:[NSString stringWithFormat:@"%ld g",(long)aliment.fats.intValue]];
                [[cell weightLabel] setText:[NSString stringWithFormat:@"100 %@",aliment.measureUnit]];
            
                [cell setDelegate:self];
                [cell setCurrentIndex:indexPath.row];
            
          //  }
            
            return cell;
        }
            break;
        default:
            return nil;
            break;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    /*
     if(_selectType == FoodSelectTypeAliment)
     return [UtilManager height:60];
     else
     return TAB_BAR_HEIGHT;
     */
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    /*
    if(_selectType == FoodSelectTypeAliment)
        return [UtilManager height:60];
    else
        return TAB_BAR_HEIGHT;
     */
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(_selectType == FoodSelectTypeAliment){
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., WIDTH, [UtilManager height:60])];
        [headerView setBackgroundColor:[UIColor clearColor]];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = headerView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [headerView addSubview:blurEffectView];
        
        _segmentControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Alimentos", nil),NSLocalizedString(@"Suplementos", nil)]];
        [_segmentControl setSelectedSegmentIndex:_segmentState];
        [_segmentControl addTarget:self action:@selector(changeAlimentsView:) forControlEvents:UIControlEventValueChanged];
        [_segmentControl setBackgroundColor:BLACK_APP_COLOR];
        [_segmentControl setCenter:CGPointMake(headerView.center.x, headerView.frame.size.height/2)];
        [_segmentControl setTintColor:YELLOW_APP_COLOR];
        
        [headerView addSubview:_segmentControl];
        
        return headerView;
    
    }else{
        
        if(!_headerView)
        {
            _headerView = [[UIView alloc] initWithFrame:CGRectMake([UtilManager width:0], 0, WIDTH, TAB_BAR_HEIGHT)];
        
            UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
            UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
            blurEffectView.frame = _headerView.bounds;
            blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            
            //[_headerView addSubview:blurEffectView];
            
            [_headerView setBackgroundColor:[UIColor clearColor]];
            
            
            _favButton = [[UIButton alloc] initWithFrame:CGRectMake(0., [UtilManager height:5], [UtilManager width:40] * 0.8, [UtilManager width:40]* 0.8)];
            [_favButton setImage:[UIImage imageNamed:@"icon-fav-off"] forState:UIControlStateNormal];
            [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateSelected];
            [_favButton setImage:[UIImage imageNamed:@"icon-fav-on"] forState:UIControlStateHighlighted];
            [_favButton setCenter:CGPointMake(_headerView.frame.size.width * 3/8, TAB_BAR_HEIGHT/2)];
            [_favButton setTag:1];
            [_favButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [_headerView addSubview:_favButton];
            
            _profileButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
            [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateSelected];
            [_profileButton setImage:[UIImage imageNamed:@"icon-profile-on"] forState:UIControlStateHighlighted];
            [_profileButton setImage:[UIImage imageNamed:@"icon-profile-off"] forState:UIControlStateNormal];
            [_profileButton setCenter:CGPointMake(_headerView.frame.size.width/8, _favButton.center.y)];
            [_profileButton setTag:0];
            [_profileButton setSelected:YES];
            [_profileButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [_headerView addSubview:_profileButton];
            
            _comunityButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
            [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateSelected];
            [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-on"] forState:UIControlStateHighlighted];
            [_comunityButton setImage:[UIImage imageNamed:@"icon-comunity-off"] forState:UIControlStateNormal];
            [_comunityButton setCenter:CGPointMake(5 * _headerView.frame.size.width/8, _favButton.center.y)];
            [_comunityButton setTag:2];
            [_comunityButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [_headerView addSubview:_comunityButton];
            
            _downloadButton = [[UIButton alloc] initWithFrame:CGRectMake(0., 0., _favButton.frame.size.width, _favButton.frame.size.height)];
            [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateSelected];
            [_downloadButton setImage:[UIImage imageNamed:@"icon-download-on"] forState:UIControlStateHighlighted];
            [_downloadButton setImage:[UIImage imageNamed:@"icon-download-off"] forState:UIControlStateNormal];
            [_downloadButton setCenter:CGPointMake(7 * _headerView.frame.size.width/8, _favButton.center.y)];
            [_downloadButton setTag:3];
            [_downloadButton addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [_headerView addSubview:_downloadButton];
        }
        
        return _headerView;

    }
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(_selectType == FoodSelectTypePlan){
        NutritionPlan *nutritionPlan = [_items objectAtIndex:indexPath.row];
        
        
    
    }
    if(_selectType == FoodSelectTyeDiet){
        Diet *diet = [_items objectAtIndex:indexPath.row];
        
        DietDetailViewController *dietDetailViewcontroller = [[DietDetailViewController alloc] init];
        [dietDetailViewcontroller setCurrentDiet:diet];
        
        [self.navigationController pushViewController:dietDetailViewcontroller animated:YES];
        
    }
    if(_selectType == FoodSelectTypeMeal){
        Meal *meal = [_items objectAtIndex:indexPath.row];
        
        MealDetailViewController *mealDetailViewController = [[MealDetailViewController alloc] init];
        [mealDetailViewController setCurrentMeal:meal];
        [self.navigationController pushViewController:mealDetailViewController animated:YES];
    } else if (_selectType == FoodSelectTypeAliment){
        
        //NSString *alphabet = [_foodIndex objectAtIndex:[indexPath  section]];
        
        //NSPredicate *predicate =
        //[NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", alphabet];
        
        //NSMutableArray *countryNameList = [[NSMutableArray alloc] init];
        //for(int i=0;i<[_items count];i++)
        //{
            Aliment *aliment = [_items objectAtIndex:indexPath.row];
        //    [countryNameList addObject:aliment.title];
        //}
        
        //NSArray *racesList =[countryNameList filteredArrayUsingPredicate:predicate];
        //NSString *currentAliment;
        
        //if([racesList count]>0){
            //currentAliment = [racesList objectAtIndex:indexPath.row];
            
            //Aliment *aliment = [Aliment getAlimentWithName:currentAliment];
            
            FoodDetailView *foodDetailView = [[FoodDetailView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:20], WIDTH - [UtilManager width:20], HEIGHT - [UtilManager height:40]) andAliment:aliment];
            
            _modalView = [[RNBlurModalView alloc] initWithView:foodDetailView];
            [_modalView setUserInteractionEnabled:YES];
            
            //RNBlurModalView *modalView = [[RNBlurModalView alloc] initWithTitle:food.title message:food.content];
            
            [_modalView show];
        //}
        
        
    }
    
    [_foodTableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark AlimentCellDelegate

- (void)alimentCellActionInIndex:(NSInteger)index{
    
    Aliment *aliment = [_items objectAtIndex:index];
    
    FoodDetailView *foodDetailView = [[FoodDetailView alloc] initWithFrame:CGRectMake([UtilManager width:10], [UtilManager height:20], WIDTH - [UtilManager width:20], HEIGHT - [UtilManager height:40]) andAliment:aliment];

    _modalView = [[RNBlurModalView alloc] initWithView:foodDetailView];
    [_modalView setUserInteractionEnabled:YES];
    
    //RNBlurModalView *modalView = [[RNBlurModalView alloc] initWithTitle:food.title message:food.content];

    [_modalView show];
}

#pragma mark FoodCellDelegate Methods

- (void)mealCellActionInIndex:(NSInteger)index{
    Meal *meal = [_items objectAtIndex:index];
    
    OptionsFoodViewController *optionsViewController = [[OptionsFoodViewController alloc] init];
    [optionsViewController setCurrentMeal:meal];
    [optionsViewController setOptionFoodType:OptionFoodTypeMeal];
    [optionsViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [optionsViewController setDelegate:self];
    [self presentViewController:optionsViewController animated:YES completion:nil];
}

#pragma mark DietCellDelegateMethods

- (void)dietCellActionInIndex:(NSInteger)index{
    
    Diet *diet = [_items objectAtIndex:index];
    
    OptionsFoodViewController *optionsViewController = [[OptionsFoodViewController alloc] init];
    [optionsViewController setCurrentDiet:diet];
    [optionsViewController setOptionFoodType:OptionFoodTypeDiet];
    [optionsViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [optionsViewController setDelegate:self];
    [self presentViewController:optionsViewController animated:YES completion:^{
        
    }];
}

#pragma mark NutritionPlanCellDelegate Methods

- (void)nutritionPlanDidPushInIndex:(NSInteger)index{
    
    NutritionPlan *nutritionPlan = [_items objectAtIndex:index];
    
    OptionsFoodViewController *optionsViewController = [[OptionsFoodViewController alloc] init];
    [optionsViewController setCurrentNutritionPlan:nutritionPlan];
    [optionsViewController setOptionFoodType:OptionFoodTypeNutritionPlan];
    [optionsViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [optionsViewController setDelegate:self];
    [self presentViewController:optionsViewController animated:YES completion:^{
        
    }];
    
    /*
    
    
    NutritionPlanAddViewController *nutritionPlanAddViewController = [[NutritionPlanAddViewController alloc] init];
    [nutritionPlanAddViewController setEditNutritionPlan:nutritionPlan];
    [nutritionPlanAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [nutritionPlanAddViewController setFoodViewController:self];
    
    [self presentViewController:nutritionPlanAddViewController animated:YES completion:^{
    
    }];
     */
}

#pragma mark OptionsFoodViewController

- (void)editMeal:(Meal *)meal {
    MealAddViewController *mealAddViewController = [[MealAddViewController alloc] init];
    [mealAddViewController setEditMeal:meal];
    [mealAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [mealAddViewController setFoodViewController:self];
    
    [self presentViewController:mealAddViewController animated:YES completion:nil];
}

- (void)editDiet:(Diet *)diet {
    DietAddViewController *dietAddViewController = [[DietAddViewController alloc] init];
    [dietAddViewController setEditDiet:diet];
    [dietAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [dietAddViewController setFoodViewController:self];
    
    [self presentViewController:dietAddViewController animated:YES completion:^{
        
    }];
}

- (void)editNutritionPlan:(NutritionPlan *)nutritionPlan{
    
    NutritionPlanAddViewController *nutritionPlanAddViewController = [[NutritionPlanAddViewController alloc] init];
    
    [nutritionPlanAddViewController setEditNutritionPlan:nutritionPlan];
    [nutritionPlanAddViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [nutritionPlanAddViewController setFoodViewController:self];
    
    [self presentViewController:nutritionPlanAddViewController animated:YES completion:^{
        
    }];
}

@end
