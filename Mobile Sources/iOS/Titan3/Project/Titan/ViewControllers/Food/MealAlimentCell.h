//
//  MealAlimentCell.h
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MealAlimentCell : UITableViewCell

@property (nonatomic, strong) UIImageView *alimentImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *quantityLabel;
@property (nonatomic, strong) UILabel *kCalLabel;
@property (nonatomic, strong) UIView *separatorView;

@end
