//
//  AuthorizeChatRequest.h
//  Titan
//
//  Created by Marcus Lee on 12/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "BaseRequest.h"
#import "ChatConfiguration.h"

@interface AuthorizeChatRequest : BaseRequest

@property (nonatomic, strong) NSString * accountId;
@property (nonatomic, strong) NSString * accessToken;
@property (nonatomic, strong) NSString * fcmRegId;

- (id)initWithAccountId:(NSInteger)accountId
            accessToken:(NSString*)accessToken
               fcmRegId:(NSString*)fcmRegId;

- (void)postWithCompletionBlock:(void (^)(id, NSString *))completionBlock;

@end
