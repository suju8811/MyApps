//
//  LoginChatRequest.h
//  Titan
//
//  Created by Marcus Lee on 12/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface LoginChatRequest : BaseRequest

@property (nonatomic, strong) NSString * username;
@property (nonatomic, strong) NSString * password;

- (id)initWithUserName:(NSString*)username
              password:(NSString*)password;

- (void)postWithCompletionBlock:(void (^)(id, NSString *))completionBlock;

@end
