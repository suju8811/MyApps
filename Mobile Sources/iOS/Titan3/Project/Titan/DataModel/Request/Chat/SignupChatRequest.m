//
//  SignupChatRequest.m
//  Titan
//
//  Created by Marcus Lee on 12/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "SignupChatRequest.h"
#import "AppConstants.h"
#import "Titan-Swift.h"

@implementation SignupChatRequest

- (id)initWithUserName:(NSString*)username
              fullname:(NSString*)fullname
              password:(NSString*)password
                 email:(NSString*)email {
    
    self = [super init];
    
    _username = username;
    _fullname = fullname;
    _password = password;
    _email = email;
    
    return self;
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"clientId":@"1",
      @"username":_username,
      @"fullname":_fullname,
      @"password":_password,
      @"email":_email,
      @"ios_fcm_regId":[[iApp sharedInstance] getFcmRegId]
      };
}

- (void)postWithCompletionBlock:(void (^)(id, NSString *))completionBlock {
    
    KClient * client = [[KClient alloc] initWithBaseURL:[NSURL URLWithString:CHAT_API_BASE_PATH]];
    [client setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [client setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [client sendRequestWithType:REQUEST_POST
                           path:@"account.signUp.inc.php"
                         params:[self getSerializedDictionary]
                    requireAuth:NO
                   successBlock:^(id result) {
                       
                       JSONDecoder * decoder = [[JSONDecoder alloc] init];
                       NSDictionary* response = [decoder objectWithData:result];
                       BOOL responseError = [[response valueForKey:@"error"] boolValue];
                       if (!responseError) {
                           NSString * accessToken = [response valueForKey:@"accessToken"];
                           [[iApp sharedInstance] setAccessTokenWithAccess_token:accessToken];
                           
                           //
                           // Get account array
                           //
                           
                           NSArray * accountArray = [response valueForKey:@"account"];
                           
                           //
                           // Check account state
                           //
                           
                           [[iApp sharedInstance] authorizeWithResponse:accountArray[0]];
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               completionBlock(response, nil);
                           });
                           
                           return;
                       }
                       
                       NSInteger error_code = [[response valueForKey:@"error_code"] integerValue];
                       
                       switch (error_code) {
                           case 300: { // ERROR_LOGIN_TAKEN
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   completionBlock(@"login_taken_message", @"");
                               });
                           }
                               break;
                           
                           case 301: { //ERROR_EMAIL_TAKEN
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   completionBlock(@"email_taken_message", @"");
                               });
                           }
                               break;
                           
                           default: {
                               NSInteger error_type = [[response valueForKey:@"error_type"] integerValue];
                               switch (error_type) {
                                   case 0: {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           completionBlock(@"username_error", @"");
                                       });
                                   }
                                       break;
                                       
                                   case 1: {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           completionBlock(@"password_error", @"");
                                       });
                                   }
                                       break;
                                       
                                   case 2: {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           completionBlock(@"email_error", @"");
                                       });
                                   }
                                       break;
                                       
                                   default: {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           completionBlock(@"fullname_error", @"");
                                       });
                                   }
                                       break;
                           }
                           
                           break;
                       }
                   }
                       
                   } failedBlock:^(NSError *error) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           completionBlock(nil, error.localizedDescription);
                       });
                   }];
}

@end
