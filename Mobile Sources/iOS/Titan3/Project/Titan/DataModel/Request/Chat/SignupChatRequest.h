//
//  SignupChatRequest.h
//  Titan
//
//  Created by Marcus Lee on 12/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface SignupChatRequest : BaseRequest

@property (nonatomic, strong) NSString * username;
@property (nonatomic, strong) NSString * fullname;
@property (nonatomic, strong) NSString * password;
@property (nonatomic, strong) NSString * email;

- (id)initWithUserName:(NSString*)username
              fullname:(NSString*)fullname
              password:(NSString*)password
                 email:(NSString*)email;

- (void)postWithCompletionBlock:(void (^)(id, NSString *))completionBlock;

@end
