//
//  LoginChatRequest.m
//  Titan
//
//  Created by Marcus Lee on 12/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "LoginChatRequest.h"
#import "AppConstants.h"
#import "Titan-Swift.h"

@implementation LoginChatRequest

- (id)initWithUserName:(NSString*)username
              password:(NSString*)password {
    
    self = [super init];
    
    _username = username;
    _password = password;
    
    return self;
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"clientId":@"1",
      @"username":_username,
      @"password":_password,
      @"ios_fcm_regId":[[iApp sharedInstance] getFcmRegId]
      };
}

- (void)postWithCompletionBlock:(void (^)(id, NSString *))completionBlock {
    
    KClient * client = [[KClient alloc] initWithBaseURL:[NSURL URLWithString:CHAT_API_BASE_PATH]];
    [client setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [client setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [client sendRequestWithType:REQUEST_POST
                           path:@"account.signIn.inc.php"
                         params:[self getSerializedDictionary]
                    requireAuth:NO
                   successBlock:^(id result) {
                       
                       JSONDecoder * decoder = [[JSONDecoder alloc] init];
                       NSDictionary* response = [decoder objectWithData:result];
                       BOOL responseError = [[response valueForKey:@"error"] boolValue];
                       if (responseError) {
                           completionBlock(nil, @"Error al calificar la aplicación");
                           return;
                       }
                       
                       NSString * accessToken = [response valueForKey:@"accessToken"];
                       [[iApp sharedInstance] setAccessTokenWithAccess_token:accessToken];
                       
                       //
                       // Get account array
                       //
                       
                       NSArray * accountArray = [response valueForKey:@"account"];
                       
                       //
                       // Check account state
                       //
                       
                       [[iApp sharedInstance] authorizeWithResponse:accountArray[0]];
                       
                       dispatch_async(dispatch_get_main_queue(), ^{
                           completionBlock(response, nil);
                       });
                       
                   } failedBlock:^(NSError *error) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           completionBlock(nil, error.localizedDescription);
                       });
                   }];
}

@end
