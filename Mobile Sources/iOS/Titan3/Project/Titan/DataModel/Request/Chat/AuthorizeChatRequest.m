//
//  AuthorizeChatRequest.m
//  Titan
//
//  Created by Marcus Lee on 12/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "AuthorizeChatRequest.h"
#import "AppConstants.h"
#import "JSONKit.h"
#import "Titan-Swift.h"

@implementation AuthorizeChatRequest

- (id)initWithAccountId:(NSInteger)accountId
            accessToken:(NSString*)accessToken
               fcmRegId:(NSString*)fcmRegId {

    self = [super init];
    
    _accountId = [NSString stringWithFormat:@"%d", (int)accountId];
    _accessToken = accessToken;
    _fcmRegId = fcmRegId;
    
    return self;
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"clientId":@"1",
      @"accountId":_accountId,
      @"accessToken":_accessToken,
      @"ios_fcm_regId":_fcmRegId
      };
}

- (void)postWithCompletionBlock:(void (^)(id, NSString *))completionBlock {
    
    KClient * client = [[KClient alloc] initWithBaseURL:[NSURL URLWithString:CHAT_API_BASE_PATH]];
    [client setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [client setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [client sendRequestWithType:REQUEST_POST
                           path:@"account.authorize.inc.php"
                         params:[self getSerializedDictionary]
                    requireAuth:NO
                   successBlock:^(id result) {
                       
                       JSONDecoder * decoder = [[JSONDecoder alloc] init];
                       NSDictionary* response = [decoder objectWithData:result];
                       BOOL responseError = [[response valueForKey:@"error"] boolValue];
                       if (responseError) {
                           completionBlock(nil, @"Error al calificar la aplicación");
                           return;
                       }
                       
                       NSString * accessToken = [response valueForKey:@"accessToken"];
                       [[iApp sharedInstance] setAccessTokenWithAccess_token:accessToken];
                       
                       //
                       // Get account array
                       //
                       
                       NSArray * accountArray = [response valueForKey:@"account"];
                       
                       //
                       // Check account state
                       //
                       
                       NSInteger accountState = [[accountArray[0] valueForKey:@"state"] integerValue];
//                       if (1.0f > 1) {
//
//                           accountState = Int((accountArray[0]["account_state"] as? String)!)!
//
//                       } else {
                       
                           // old server engine
                           
//                        accountState = Int((accountArray[0]["state"] as? String)!)!
//                       }
                       
                       //
                       // if account state ENABLE
                       //
                       
                       if (accountState == 0) {
                           [[iApp sharedInstance] authorizeWithResponse:accountArray[0]];
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               completionBlock(response, nil);
                           });
                       }
                       
    } failedBlock:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock(nil, error.localizedDescription);
        });
    }];
    
}

@end
