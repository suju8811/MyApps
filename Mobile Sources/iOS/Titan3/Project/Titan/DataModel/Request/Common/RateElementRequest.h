//
//  RateElementRequest.h
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface RateElementRequest : BaseRequest

@property (nonatomic, strong) NSString * token;
@property (nonatomic, assign) float rate;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, strong) NSString * elementId;
@property (nonatomic, strong) NSString * note;

- (id) initWithToken:(NSString*)token
                rate:(float)rate
                note:(NSString*)note
                type:(NSInteger)type
           elementId:(NSString*)elementId;

@end
