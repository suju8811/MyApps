//
//  GetTagsRequest.m
//  Titan
//
//  Created by Marcus Lee on 30/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "GetTagsRequest.h"

@implementation GetTagsRequest

- (id)initWithToken:(NSString*)token {
    
    self = [super init];
    
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"TAGS2";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token
      };
}

@end
