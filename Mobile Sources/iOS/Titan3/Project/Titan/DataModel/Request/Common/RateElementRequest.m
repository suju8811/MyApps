//
//  RateElementRequest.m
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RateElementRequest.h"

@implementation RateElementRequest

- (id) initWithToken:(NSString*)token
                rate:(float)rate
                note:(NSString*)note
                type:(NSInteger)type
           elementId:(NSString*)elementId {
    
    self = [super init];
    
    _token = token;
    _rate = rate;
    _type = type;
    _elementId = elementId;
    _note = note;
    
    return self;
}

- (NSString*)getPath {
    return @"puntuacion";
}

- (NSDictionary*)getSerializedDictionary {
    return @{
             @"token" : _token,
             @"numero" : @((int)_rate),
             @"tipo" : @(_type),
             @"tipo_id" :_elementId,
             @"opinion" :_note
             };
}

@end
