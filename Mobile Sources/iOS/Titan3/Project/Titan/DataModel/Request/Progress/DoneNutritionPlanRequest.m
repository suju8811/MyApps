//
//  DoneNutritionPlanRequest.m
//  Titan
//
//  Created by Marcus Lee on 18/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "DoneNutritionPlanRequest.h"

@implementation DoneNutritionPlanRequest

- (id)initWithToken:(NSString*)token
    nutritionPlanId:(NSString*)nutritionPlanId {
    
    self = [super init];
    
    _token = token;
    _nutritionPlanId = nutritionPlanId;
    
    return self;
}

- (NSString*)getPath {
    return @"done_planes_nutricionales";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token,
      @"id":_nutritionPlanId
      };
}

@end
