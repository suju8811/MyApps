//
//  CreateLogRequest.h
//  Titan
//
//  Created by Marcus Lee on 29/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"
#import "Log.h"

@interface CreateLogRequest : BaseRequest

@property NSString * token;
@property Log * log;
@property UIImage * image;

- (id)initWithToken:(NSString*)token
                log:(Log*)log
              image:(UIImage*)image;

@end
