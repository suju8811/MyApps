//
//  GetProgress2Request.h
//  Titan
//
//  Created by Marcus Lee on 24/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface GetProgress2Request : BaseRequest

@property NSString * token;
@property NSDate * date1;
@property NSDate * date2;

- (id)initWithToken:(NSString*)token
              date1:(NSDate*)date1
              date2:(NSDate*)date2;

@end
