//
//  DeleteLogRequest.m
//  Titan
//
//  Created by Marcus Lee on 29/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DeleteLogRequest.h"

@implementation DeleteLogRequest

- (id)initWithLogId:(NSString*)logId
              token:(NSString*)token {
    
    self = [super init];
    
    _logId = logId;
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"eliminar_registro";
}

- (NSDictionary*)getSerializedDictionary {
    NSDictionary * resultDictionary =
    @{
      @"id":_logId,
      @"token":_token
      };
    
    return resultDictionary;
}

@end
