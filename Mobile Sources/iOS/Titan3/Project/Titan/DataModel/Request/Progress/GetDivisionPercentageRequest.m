//
//  GetDivisionPercentageRequest.m
//  Titan
//
//  Created by Marcus Lee on 10/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "GetDivisionPercentageRequest.h"

@implementation GetDivisionPercentageRequest

- (id)initWithToken:(NSString*)token {
    
    self = [super init];
    
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"division_porcentajes";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token
      };
}

@end
