//
//  DoneTrainingRequest.h
//  Titan
//
//  Created by Marcus Lee on 18/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface DoneTrainingRequest : BaseRequest

@property NSString * token;
@property NSString * trainingId;

- (id)initWithToken:(NSString*)token
         trainingId:(NSString*)trainingId;


@end
