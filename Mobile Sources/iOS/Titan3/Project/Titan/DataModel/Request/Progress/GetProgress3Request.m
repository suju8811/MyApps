//
//  GetProgress3Request.m
//  Titan
//
//  Created by Marcus Lee on 24/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "GetProgress3Request.h"

@implementation GetProgress3Request

- (id)initWithToken:(NSString*)token
           bodyPart:(NSString*)bodyPart {
    
    self = [super init];
    
    _token = token;
    _bodyPart = bodyPart;
    
    return self;
}

- (NSString*)getPath {
    return @"progreso3";
}

- (NSDictionary*)getSerializedDictionary {
    return @{
             @"token":_token,
             @"cuerpo":[_bodyPart uppercaseString]
             };
}

@end
