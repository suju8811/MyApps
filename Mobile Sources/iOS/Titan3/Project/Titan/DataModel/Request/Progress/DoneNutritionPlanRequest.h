//
//  DoneNutritionPlanRequest.h
//  Titan
//
//  Created by Marcus Lee on 18/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface DoneNutritionPlanRequest : BaseRequest

@property NSString * token;
@property NSString * nutritionPlanId;

- (id)initWithToken:(NSString*)token
    nutritionPlanId:(NSString*)nutritionPlanId;

@end
