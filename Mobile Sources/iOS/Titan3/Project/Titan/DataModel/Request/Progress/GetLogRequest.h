//
//  GetLogRequest.h
//  Titan
//
//  Created by Marcus Lee on 29/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface GetLogRequest : BaseRequest

@property NSString * token;

- (id)initWithToken:(NSString*)token;

@end
