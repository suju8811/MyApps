//
//  DoneTrainingRequest.m
//  Titan
//
//  Created by Marcus Lee on 18/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "DoneTrainingRequest.h"

@implementation DoneTrainingRequest

- (id)initWithToken:(NSString*)token
         trainingId:(NSString*)trainingId {
    
    self = [super init];
    
    _token = token;
    _trainingId = trainingId;
    
    return self;
}

- (NSString*)getPath {
    return @"done_entrenamientos";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token,
      @"id":_trainingId
      };
}

@end
