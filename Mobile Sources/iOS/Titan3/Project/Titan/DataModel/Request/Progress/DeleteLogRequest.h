//
//  DeleteLogRequest.h
//  Titan
//
//  Created by Marcus Lee on 29/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface DeleteLogRequest : BaseRequest

@property NSString * token;

@property NSString * logId;

- (id)initWithLogId:(NSString*)logId
              token:(NSString*)token;

@end
