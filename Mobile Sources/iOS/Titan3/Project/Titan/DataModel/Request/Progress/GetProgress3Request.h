//
//  GetProgress3Request.h
//  Titan
//
//  Created by Marcus Lee on 24/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface GetProgress3Request : BaseRequest

@property NSString * token;
@property NSString * bodyPart;

- (id)initWithToken:(NSString*)token
           bodyPart:(NSString*)bodyPart;

@end
