//
//  GetProgress2Request.m
//  Titan
//
//  Created by Marcus Lee on 24/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "GetProgress2Request.h"
#import "DateTimeUtil.h"

@implementation GetProgress2Request

- (id)initWithToken:(NSString*)token
              date1:(NSDate*)date1
              date2:(NSDate*)date2 {

    self = [super init];
    
    _token = token;
    _date1 = date1;
    _date2 = date2;
    
    return self;
}

- (NSString*)getPath {
    return @"progreso2";
}

- (NSDictionary*)getSerializedDictionary {
    return @{
             @"token":_token,
             @"fecha1":[DateTimeUtil stringFromDate:_date1 format:@"yyyy-MM-dd"],
             @"fecha2":[DateTimeUtil stringFromDate:_date2 format:@"yyyy-MM-dd"]
             };
}

@end
