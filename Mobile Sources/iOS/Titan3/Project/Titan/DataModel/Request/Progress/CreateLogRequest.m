//
//  CreateLogRequest.m
//  Titan
//
//  Created by Marcus Lee on 29/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CreateLogRequest.h"
#import "DateTimeUtil.h"

@implementation CreateLogRequest

- (id)initWithToken:(NSString*)token
                log:(Log*)log
              image:(UIImage*)image {
    
    self = [super init];
    
    _token = token;
    _log = log;
    _image = image;
    
    return self;
}

- (NSString*)getPath {
    return @"crear_registro";
}

- (NSDictionary*)getSerializedDictionary {
    NSData *imageData = UIImageJPEGRepresentation(_image, 1.);
    NSString *encodeString = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    NSMutableDictionary * resultDictionary
    = [NSMutableDictionary dictionaryWithDictionary:
       @{
         @"token":_token,
         @"fecha":_log.revisionDate ? [DateTimeUtil stringFromDate:_log.revisionDate format:@"yyyy-MM-dd"] : [DateTimeUtil stringFromDate:[NSDate date] format:@"yyyy-MM-dd"],
         @"photo":encodeString == nil ? @"":encodeString,
         @"peso":_log.weight,
         @"igc":_log.igc,
         @"tmb":_log.bmr,
         @"rm":_log.rm,
         @"altura":_log.height,
         @"cuello":_log.neck,
         @"espalda":_log.back,
         @"bicepizq":_log.leftBicep,
         @"bicepdrc":_log.rightBicep,
         @"antebrazoizq":_log.leftForearm,
         @"antebrazodrc":_log.rightForearm,
         @"cadera":_log.hip,
         @"cintura":_log.waist,
         @"cuadricepsizq":_log.leftThigh,
         @"cuadricepsdrc":_log.rightThigh,
         @"gemeloizq":_log.leftLeg,
         @"gemelodrc":_log.rightLeg,
         @"gemelodch":_log.rightLeg,
         }];
    return resultDictionary;
}

@end
