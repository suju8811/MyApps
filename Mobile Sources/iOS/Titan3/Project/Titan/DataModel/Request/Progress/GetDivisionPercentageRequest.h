//
//  GetDivisionPercentageRequest.h
//  Titan
//
//  Created by Marcus Lee on 10/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface GetDivisionPercentageRequest : BaseRequest

@property NSString * token;

- (id)initWithToken:(NSString*)token;

@end
