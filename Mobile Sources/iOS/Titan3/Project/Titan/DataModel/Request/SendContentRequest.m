//
//  ShareContentRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SendContentRequest.h"
#import "Training.h"
#import "TrainingPlan.h"
#import "Aliment.h"
#import "Meal.h"
#import "Diet.h"
#import "NutritionPlan.h"

@implementation SendContentRequest

- (id) initWithToken1:(NSString*)token1
               token2:(NSString*)token2
            rlmObject:(RLMObject*)rlmObject {
    
    self = [super init];
    
    _token1 = token1;
    _token2 = token2;
    _rlmObject = rlmObject;
    
    return self;
}

- (NSString*)getPath {
    return @"enviar";
}

- (NSDictionary*)getSerializedDictionary {
    NSString * contentId;
    NSString * contentType;
    
    if ([_rlmObject isKindOfClass:[Training class]]) {
        Training * training = (Training*)_rlmObject;
        contentType = @"1";
        contentId = training.serverTrainingId;
    }
    else if ([_rlmObject isKindOfClass:[TrainingPlan class]]) {
        TrainingPlan * trainingPlan = (TrainingPlan*)_rlmObject;
        contentType = @"2";
        contentId = trainingPlan.serverTrainingPlanId;
    }
    else if ([_rlmObject isKindOfClass:[Aliment class]]) {
        Aliment * aliment = (Aliment*)_rlmObject;
        if (aliment.isSupplement &&
            aliment.isSupplement.boolValue) {
            
            contentType = @"4";
            contentId = aliment.supplementId;
        }
        else {
            contentType = @"3";
            contentId = aliment.alimentId;
        }
    }
    else if ([_rlmObject isKindOfClass:[Meal class]]) {
        Meal * meal = (Meal*)_rlmObject;
        contentType = @"5";
        contentId = meal.serverMealId;
    }
    else if ([_rlmObject isKindOfClass:[Diet class]]) {
        Diet * diet = (Diet*)_rlmObject;
        contentType = @"6";
        contentId = diet.serverDietId;
    }
    else if ([_rlmObject isKindOfClass:[NutritionPlan class]]) {
        NutritionPlan * nutritionPlan = (NutritionPlan*)_rlmObject;
        contentType = @"7";
        contentId = nutritionPlan.serverNutritionPlanId;
    }
    
    return
    @{
      @"token1":_token1,
      @"token2":_token2,
      @"tipo_id":contentId,
      @"tipo":contentType
      };
}


@end
