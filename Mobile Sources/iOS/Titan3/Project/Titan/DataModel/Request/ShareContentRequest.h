//
//  ShareContentRequest.h
//  Titan
//
//  Created by Marcus Lee on 9/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"
#import <Realm/Realm.h>

@interface ShareContentRequest : BaseRequest

@property NSString * token1;
@property RLMObject * rlmObject;

- (id) initWithToken1:(NSString*)token1
            rlmObject:(RLMObject*)rlmObject;

@end
