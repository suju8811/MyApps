//
//  GetExercisesRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "GetExercisesRequest.h"

@implementation GetExercisesRequest

- (id)initWithToken:(NSString*)token {
    
    self = [super init];
    
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"ejercicios";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token
      };
}

@end
