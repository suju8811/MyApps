//
//  GetTrainingPlansRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "GetTrainingPlansRequest.h"

@implementation GetTrainingPlansRequest

- (id)initWithToken:(NSString*)token
          updatedAt:(NSString*)updatedAt {
    
    self = [super init];
    
    _token = token;
    _updatedAt = updatedAt;
    
    return self;
}

- (NSString*)getPath {
    return @"planes_entrenamientos";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token,
      @"updatedAt":_updatedAt
      };
}

@end
