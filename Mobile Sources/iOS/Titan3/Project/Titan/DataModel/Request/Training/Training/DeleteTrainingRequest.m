//
//  DeleteTrainingRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DeleteTrainingRequest.h"

@implementation DeleteTrainingRequest

- (id)initWithTrainingId:(NSString*)trainingId
                   token:(NSString*)token {
    
    self = [super init];
    
    _trainingId = trainingId;
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"delete_entrenamientos";
}

- (NSDictionary*)getSerializedDictionary {
    NSDictionary * resultDictionary =
        @{
          @"id":_trainingId,
          @"token":_token
          };
    
    return resultDictionary;
}

@end
