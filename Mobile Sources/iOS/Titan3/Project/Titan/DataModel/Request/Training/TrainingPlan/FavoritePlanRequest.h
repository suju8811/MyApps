//
//  FavoritePlanRequest.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface FavoritePlanRequest : BaseRequest

@property NSString * token;

@property NSString * planId;

@property (nonatomic, assign) BOOL isFavorite;

- (id)initWithPlanId:(NSString*)planId
               token:(NSString*)token
          isFavorite:(BOOL)isFavorite;

@end
