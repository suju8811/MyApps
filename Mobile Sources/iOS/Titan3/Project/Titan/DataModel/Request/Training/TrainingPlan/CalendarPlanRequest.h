//
//  CalendarPlanRequest.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface CalendarPlanRequest : BaseRequest

@property NSString * token;

@property NSString * planId;

@property NSString * fetcha;

- (id)initWithPlanId:(NSString*)planId
               token:(NSString*)token
              fetcha:(NSString*)fetcha;

@end
