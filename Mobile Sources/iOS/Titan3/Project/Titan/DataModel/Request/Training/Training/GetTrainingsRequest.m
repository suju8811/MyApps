//
//  GetTrainingsRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "GetTrainingsRequest.h"

@implementation GetTrainingsRequest

- (id)initWithToken:(NSString*)token {
    
    self = [super init];
    
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"entrenamientos";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token
      };
}

@end
