//
//  FavoriteTrainingRequest.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface FavoriteTrainingRequest : BaseRequest

@property NSString * token;

@property NSString * trainingId;

@property (nonatomic, assign) BOOL isFavorite;

- (id)initWithTrainingId:(NSString*)trainingId
                   token:(NSString*)token
              isFavorite:(BOOL)isFavorite;

@end
