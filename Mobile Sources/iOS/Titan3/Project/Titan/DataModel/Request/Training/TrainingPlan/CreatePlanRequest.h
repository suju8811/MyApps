//
//  CreatePlanRequest.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"
#import "TrainingPlan.h"

@interface CreateTrainingPlanRequest : BaseRequest

@property NSString * token;

@property TrainingPlan * trainingPlan;

- (id)initWithToken:(NSString*)token
       trainingPlan:(TrainingPlan*)trainingPlan;

@end
