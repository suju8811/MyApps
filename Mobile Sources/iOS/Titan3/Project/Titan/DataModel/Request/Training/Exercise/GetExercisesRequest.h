//
//  GetExercisesRequest.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface GetExercisesRequest : BaseRequest

@property NSString * token;

- (id)initWithToken:(NSString*)token;

@end
