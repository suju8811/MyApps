//
//  EditTrainingRequest.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"
#import "Training.h"

@interface EditTrainingRequest : BaseRequest

@property NSString * token;

@property Training * training;

- (id)initWithTraining:(NSString*)token
              training:(Training*)training;

@end
