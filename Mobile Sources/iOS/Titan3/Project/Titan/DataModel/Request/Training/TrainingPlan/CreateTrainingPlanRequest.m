//
//  CreatePlanRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CreateTrainingPlanRequest.h"

@implementation CreateTrainingPlanRequest

- (id)initWithToken:(NSString*)token
       trainingPlan:(TrainingPlan*)trainingPlan {
    
    self = [super init];
    
    _token = token;
    _trainingPlan = trainingPlan;
    
    return self;
}

- (NSString*)getPath {
    return @"crear_planes";
}

- (NSDictionary*)getSerializedDictionary {
    NSMutableArray * planDayArray = [[NSMutableArray alloc] init];
    for (TrainingDay * planDay in _trainingPlan.trainingDay) {
        
        NSMutableArray * planDayContentArray = [[NSMutableArray alloc] init];
        for (TrainingDayContent * trainingDayContent in planDay.trainingDayContent) {
            NSDictionary * trainingDayContentDictionary =
            @{
              @"training":trainingDayContent.trainingId,
              @"start_hour":trainingDayContent.start == nil ? @"" : trainingDayContent.start,
              @"stop_hour":trainingDayContent.end == nil ? @"" : trainingDayContent.end
              };
            
            [planDayContentArray addObject:trainingDayContentDictionary];
        }
        
        NSDictionary * planDayDictionary =
        @{
          @"rest":planDay.restDay == nil ? [NSNumber numberWithInt:0] : planDay.restDay,
          @"orden":planDay.order,
          @"plan_dia_training":planDayContentArray
          };
        
        [planDayArray addObject:planDayDictionary];
    }
    
    NSMutableArray * tagsArray = [[NSMutableArray alloc] init];
    for (Tag * tag in _trainingPlan.tags) {
        [tagsArray addObject:tag.title];
    }
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString * startDateString = [formatter stringFromDate:_trainingPlan.startDate];
    NSString * endDateString = [formatter stringFromDate:_trainingPlan.endDate];
    
    NSMutableDictionary * resultDictionary
    = [NSMutableDictionary dictionaryWithDictionary:
       @{
         @"token":_token,
         @"titulo":_trainingPlan.title,
         @"descripcion":_trainingPlan.desc,
         @"nota":_trainingPlan.notes,
         @"comienzo":startDateString == nil ? @"" : startDateString,
         @"finalizacion":endDateString == nil ? @"" : endDateString,
         @"plan_dia":[self jsonFromArray:planDayArray],
         @"plan_tags":[self jsonFromArray:tagsArray],
         @"calendario":_trainingPlan.isCalendarCopy ? _trainingPlan.isCalendarCopy : @0,
         }];
    return resultDictionary;
}

@end
