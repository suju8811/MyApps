//
//  EditPlanRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "EditTrainingPlanRequest.h"

@implementation EditTrainingPlanRequest

- (id)initWithTraining:(NSString*)token
          trainingPlan:(TrainingPlan*)trainingPlan {
    
    self = [super init];
    
    _token = token;
    _trainingPlan = trainingPlan;
    
    return self;
}

- (NSString*)getPath {
    return @"editar_planes";
}

- (NSDictionary*)getSerializedDictionary {
    NSMutableArray * planDayArray = [[NSMutableArray alloc] init];
    for (TrainingDay * planDay in _trainingPlan.trainingDay) {
        
        NSMutableArray * planDayContentArray = [[NSMutableArray alloc] init];
        for (TrainingDayContent * trainingDayContent in planDay.trainingDayContent) {
            NSString * trainingId = trainingDayContent.trainingId;
            if (trainingDayContent.training.isCalendarCopy &&
                trainingDayContent.training.isCalendarCopy.boolValue) {
                trainingId = [trainingId substringWithRange:NSMakeRange(0, trainingId.length - [@"-schedule" length])];
            }
            
            NSDictionary * trainingDayContentDictionary =
            @{
              @"training":trainingId,
              @"start_hour":trainingDayContent.start == nil ? @"" : trainingDayContent.start,
              @"stop_hour":trainingDayContent.end == nil ? @"" : trainingDayContent.end
              };
            
            [planDayContentArray addObject:trainingDayContentDictionary];
        }
        
        NSDictionary * planDayDictionary =
        @{
          @"rest":planDay.restDay == nil ? [NSNumber numberWithInt:0] : planDay.restDay,
          @"orden":planDay.order,
          @"plan_dia_training":planDayContentArray
          };
        
        [planDayArray addObject:planDayDictionary];
    }
    
    NSMutableArray * tagsArray = [[NSMutableArray alloc] init];
    for (Tag * tag in _trainingPlan.tags) {
        [tagsArray addObject:tag.title];
    }
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString * startDateString = [formatter stringFromDate:_trainingPlan.startDate];
    NSString * endDateString = [formatter stringFromDate:_trainingPlan.endDate];
    
    NSMutableDictionary * resultDictionary
    = [NSMutableDictionary dictionaryWithDictionary:
       @{
         @"id":_trainingPlan.serverTrainingPlanId,
         @"token":_token,
         @"titulo":_trainingPlan.title,
         @"descripcion":_trainingPlan.desc,
         @"nota":_trainingPlan.notes,
         @"comienzo":startDateString == nil ? @"2017-11-15" : startDateString,
         @"finalizacion":endDateString == nil ? @"2017-11-15" : endDateString,
         @"plan_dia":[self jsonFromArray:planDayArray],
         @"plan_tags":[self jsonFromArray:tagsArray],
         }];
    return resultDictionary;
}

@end
