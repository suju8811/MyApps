//
//  PostTrainingRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CreateTrainingRequest.h"

@implementation CreateTrainingRequest

- (id)initWithTraining:(NSString*)token
              training:(Training*)training {
    
    self = [super init];
    
    _token = token;
    _training = training;
    
    return self;
}

- (NSString*)getPath {
    return @"crear_entrenamientos";
}

- (NSDictionary*)getSerializedDictionary {
    NSMutableArray * tagsArray = [[NSMutableArray alloc] init];
    for (Tag * tag in _training.tags) {
        [tagsArray addObject:@{@"etiqueta":tag.title}];
    }
    
    if (tagsArray.count == 0) {
        [tagsArray addObject:@{@"etiqueta":_training.title}];
    }
    
    NSMutableArray * exercisesArray = [[NSMutableArray alloc] init];
    for (TrainingExercise * trainingExercise in _training.trainingExercises) {
        for (NSUInteger index = 0; index < trainingExercise.exercises.count; index ++) {
            Exercise * exercise = trainingExercise.exercises[index];
            
            Serie * serie = nil;
            if (trainingExercise.series.count > 0) {
                serie = [trainingExercise.series firstObject];
            }
            NSString * serieId = nil;
            if (serie != nil) {
                serieId = serie.serverSerieId;
            }
            
            Serie * superSerie = nil;
            if (trainingExercise.secondExerciseSeries.count > 0) {
                superSerie = [trainingExercise.secondExerciseSeries firstObject];
            }
            NSString * superSerieId, *superSerieTitle;
            if (superSerie != nil) {
                superSerieId = superSerie.serverSerieId;
                superSerieTitle = superSerie.title;
            }
            
            NSMutableArray * steps = [[NSMutableArray alloc] init];
            for (Serie * serie in trainingExercise.series) {
                NSDictionary * stepDictionary =
                @{
                  @"reps":serie.reps,
                  @"kg":serie.weight,
                  @"time":serie.execTime,
                  };
                [steps addObject:stepDictionary];
            }
            
            for (Serie * serie in trainingExercise.secondExerciseSeries) {
                NSDictionary * stepDictionary =
                @{
                  @"reps":serie.reps,
                  @"kg":serie.weight,
                  @"time":serie.execTime,
                  };
                [steps addObject:stepDictionary];
            }
            
            NSDictionary * trainingExerciseDictionary =
            @{
              @"tag1_id":(exercise.tag1 == nil ? @"1" : exercise.tag1),
              @"tag2_id":(exercise.tag2 == nil ? @"1" : exercise.tag2),
              @"tag3_id":(exercise.tag3 == nil ? @"1" : exercise.tag3),
              @"tipo":(exercise.type == nil ? @"" : exercise.type),
              @"titulo":(exercise.title == nil ? @"" : exercise.title),
              @"descripcion":(exercise.desc == nil ? @"" : exercise.desc),
              @"dificultad":(exercise.weightDifficulty == nil ? @"" : exercise.weightDifficulty),
              @"energia":(exercise.energy == nil ? @"" : exercise.energy),
              @"imagen":(exercise.picture == nil ? @"" : exercise.picture),
              @"documento":(exercise.document == nil ? @"" : exercise.document),
              @"energias":(trainingExercise.totalEnergy == nil ? @"" : trainingExercise.totalEnergy),
              @"nota":(exercise.desc == nil ? @"" : exercise.desc),
              @"orden":(trainingExercise.order == nil ? @"" : trainingExercise.order),
              @"steps":[self jsonFromArray:steps],
              @"serie":(serieId == nil ? @"" : serieId),
              @"super_serie":(trainingExercise.isSuperSerie && trainingExercise.isSuperSerie.boolValue) ? @"1":@"0",
              @"titulo_super_serie":(superSerieTitle == nil ? @"" : superSerieTitle),
              @"rest":(trainingExercise.rest == nil ? @"" : trainingExercise.rest),
              @"weight":(exercise.weightDifficulty == nil ? @"" : exercise.weightDifficulty),
              @"intensity":@"",
              @"reps":@"",
              @"orders":@"",
              @"rest_super":@"",
              @"weight_super":@"",
              @"intensity_super":@"",
              @"reps_super":@"",
              @"orders_super":@"",
              @"steps":@"",
              };
            [exercisesArray addObject:trainingExerciseDictionary];
        }
    }
    
    NSMutableDictionary * resultDictionary
        = [NSMutableDictionary dictionaryWithDictionary:
           @{
             @"token":_token,
             @"titulo":_training.title,
             @"descripcion":_training.desc == nil ? @"" : _training.desc,
             @"anotacion":_training.annotation == nil ? @"" : _training.annotation,
             @"privacidad":_training.privacity == nil ? @"public" : _training.privacity,
             @"puntuacion":_training.punctation == nil ? @"" : _training.punctation,
             @"energia":(_training.totalEnergy == nil ? [NSNumber numberWithInt:0] : _training.totalEnergy),
             @"duracion":(_training.duration == nil ? [NSNumber numberWithInt:0] : _training.duration),
             @"fecha":(_training.updatedAt == nil ? @"" : _training.updatedAt),
             @"tags":[self jsonFromArray:tagsArray],
             @"ejercicios":[self jsonFromArray:exercisesArray]
            }];
    return resultDictionary;
}

@end
