//
//  FavoritePlanRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FavoritePlanRequest.h"

@implementation FavoritePlanRequest

- (id)initWithPlanId:(NSString*)planId
               token:(NSString*)token
          isFavorite:(BOOL)isFavorite {
    
    self = [super init];
    
    _planId = planId;
    _token = token;
    _isFavorite = isFavorite;
    
    return self;
}

- (NSString*)getPath {
    return @"favorito_planes";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"id":_planId,
      @"token":_token,
      @"favorito":(_isFavorite ? @"1":@"0")
      };
}

@end
