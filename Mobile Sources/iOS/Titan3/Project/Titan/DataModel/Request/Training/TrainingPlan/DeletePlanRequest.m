//
//  DeletePlanRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DeletePlanRequest.h"

@implementation DeletePlanRequest

- (id)initWithPlanId:(NSString*)planId
               token:(NSString*)token {
    
    self = [super init];
    
    _planId = planId;
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"delete_planes";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"id":_planId,
      @"token":_token
      };
}

@end
