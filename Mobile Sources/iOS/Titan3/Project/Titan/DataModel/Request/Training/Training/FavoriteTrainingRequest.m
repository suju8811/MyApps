//
//  FavoriteTrainingRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FavoriteTrainingRequest.h"

@implementation FavoriteTrainingRequest

- (id)initWithTrainingId:(NSString*)trainingId
                   token:(NSString*)token
              isFavorite:(BOOL)isFavorite {
    
    self = [super init];
    
    _trainingId = trainingId;
    _token = token;
    _isFavorite = isFavorite;
    
    return self;
}

- (NSString*)getPath {
    return @"favorito_entrenamientos";
}

- (NSDictionary*)getSerializedDictionary {
    NSDictionary * resultDictionary =
        @{
          @"id":_trainingId,
          @"token":_token,
          @"favorito":(_isFavorite ? @"1":@"0")
          };
    
    return resultDictionary;
}

@end
