//
//  DeleteTrainingRequest.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface DeleteTrainingRequest : BaseRequest

@property NSString * token;

@property NSString * trainingId;

- (id)initWithTrainingId:(NSString*)trainingId
                   token:(NSString*)token;

@end
