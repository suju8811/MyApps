//
//  DeletePlanRequest.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface DeletePlanRequest : BaseRequest

@property NSString * token;

@property NSString * planId;

- (id)initWithPlanId:(NSString*)planId
               token:(NSString*)token;

@end
