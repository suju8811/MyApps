//
//  CalendarPlanRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CalendarPlanRequest.h"

@implementation CalendarPlanRequest

- (id)initWithPlanId:(NSString*)planId
               token:(NSString*)token
              fetcha:(NSString*)fetcha {
    self = [super init];
    
    _planId = planId;
    _token = token;
    _fetcha = fetcha;
    
    return self;
}

- (NSString*)getPath {
    return @"calendario_planes";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"id":_planId,
      @"token":_token,
      @"fecha":_fetcha
      };
}

@end
