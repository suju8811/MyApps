//
//  BaseRequest.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONKit.h"
#import "KClient.h"

@interface BaseRequest : NSObject

@property NSString * path;

@property NSDictionary * serializedDictionary;

- (NSDictionary*)getSerializedDictionary;
- (NSString*)getPath;

- (void)get:(BOOL) requireAuth
completionBlock:(void (^)(id response, NSError * error)) completionBlock;

- (void)post:(BOOL) requireAuth
completionBlock:(void (^)(id response, NSError * error)) completionBlock;

- (void)put:(BOOL) requireAuth
completionBlock:(void (^)(id response, NSError * error)) completionBlock;

- (void)head:(BOOL) requireAuth
completionBlock:(void (^)(id response, NSError * error)) completionBlock;

- (void)delete:(BOOL) requireAuth
completionBlock:(void (^)(id response, NSError * error)) completionBlock;

- (NSString*)jsonFromArray:(NSArray*)objectsArray;
@end
