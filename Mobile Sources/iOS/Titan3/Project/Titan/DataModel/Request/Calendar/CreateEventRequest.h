//
//  CreateEventRequest.h
//  Titan
//
//  Created by Marcus Lee on 15/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"
#import "Event.h"

@interface CreateEventRequest : BaseRequest

@property NSString * token;

@property Event * event;

- (id)initWithToken:(NSString*)token
              event:(Event*)event;

@end
