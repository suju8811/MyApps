//
//  ChangeCalendarRequest.h
//  Titan
//
//  Created by Marcus Lee on 22/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface ChangeCalendarRequest : BaseRequest

@property (nonatomic, strong) NSString * token;
@property (nonatomic, assign) BOOL isDelete;
@property (nonatomic, strong) NSDate * date1;
@property (nonatomic, strong) NSDate * date2;
@property (nonatomic, assign) NSInteger days;

- (id)initWithToken:(NSString*)token
             delete:(BOOL)isDelete
              date1:(NSDate*)date1
              date2:(NSDate*)date2
               days:(NSInteger)days;

@end
