//
//  GetSchedulesRequest.h
//  Titan
//
//  Created by Marcus Lee on 21/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface GetSchedulesRequest : BaseRequest

@property NSString * token;
@property NSString * year;
@property NSString * month;

- (id)initWithToken:(NSString*)token;

- (id)initWithToken:(NSString*)token
               year:(NSString*)year
              month:(NSString*)month;

@end
