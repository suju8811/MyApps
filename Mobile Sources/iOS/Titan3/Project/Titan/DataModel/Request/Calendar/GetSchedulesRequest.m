//
//  GetSchedulesRequest.m
//  Titan
//
//  Created by Marcus Lee on 21/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "GetSchedulesRequest.h"

@implementation GetSchedulesRequest

- (id)initWithToken:(NSString*)token {
    self = [super init];
    
    _token = token;
    _year = nil;
    _month = nil;
    
    return self;
}

- (id)initWithToken:(NSString*)token
               year:(NSString*)year
              month:(NSString*)month {
    
    self = [super init];
    
    _token = token;
    _year = year;
    _month = month;
    
    return self;
}

- (NSString*)getPath {
    return @"calendario";
}

- (NSDictionary*)getSerializedDictionary {
    NSMutableDictionary * serializedDictionary = [[NSMutableDictionary alloc] init];
    [serializedDictionary setObject:_token forKey:@"token"];
    if (_year) {
        [serializedDictionary setObject:_year forKey:@"year"];
    }
    if (_month) {
        [serializedDictionary setObject:_month forKey:@"month"];
    }
    return serializedDictionary;
}

@end
