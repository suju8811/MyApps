//
//  CreateEventRequest.m
//  Titan
//
//  Created by Marcus Lee on 15/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CreateEventRequest.h"

@implementation CreateEventRequest

- (id)initWithToken:(NSString*)token
              event:(Event*)event {
    
    self = [super init];
    
    _token = token;
    _event = event;
    
    return self;
}

- (NSString*)getPath {
    return @"crear_evento";
}

- (NSDictionary*)getSerializedDictionary {
    NSDateFormatter * formater = [[NSDateFormatter alloc] init];
    formater.dateFormat = @"yyyy-MM-dd";
    NSString * dateString = [formater stringFromDate:_event.startDate];
    formater.dateFormat = @"hh:mm";
    NSString * timeString = [formater stringFromDate:_event.startDate];
    
    NSMutableDictionary * resultDictionary
    = [NSMutableDictionary dictionaryWithDictionary:
       @{
         @"token":_token,
         @"titulo":_event.title,
         @"tipo":_event.type,
         @"notas":_event.note,
         @"fecha":dateString,
         @"hora":timeString,
         @"repetir":_event.repeat,
         @"dias":_event.days
         }];
    return resultDictionary;
}

@end
