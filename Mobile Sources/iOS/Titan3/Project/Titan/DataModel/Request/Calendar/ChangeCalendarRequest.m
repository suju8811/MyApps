//
//  ChangeCalendarRequest.m
//  Titan
//
//  Created by Marcus Lee on 22/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "ChangeCalendarRequest.h"
#import "DateTimeUtil.h"

@implementation ChangeCalendarRequest

- (id)initWithToken:(NSString*)token
             delete:(BOOL)isDelete
              date1:(NSDate*)date1
              date2:(NSDate*)date2
               days:(NSInteger)days {
    
    self = [super init];
    
    _token = token;
    _isDelete = isDelete;
    _date1 = date1;
    _date2 = date2;
    _days = days;
    
    return self;
}

- (NSString*)getPath {
    return @"cambiocalendario";
}

- (NSDictionary*)getSerializedDictionary {
    NSDictionary * serializedDictionary = @{
                                            @"token":_token,
                                            @"eliminar":_isDelete ? @"1" : @"0",
                                            @"fecha1":[DateTimeUtil stringFromDate:_date1 format:@"yyyy-MM-dd"],
                                            @"fecha2":[DateTimeUtil stringFromDate:_date2 format:@"yyyy-MM-dd"],
                                            @"dias":[NSString stringWithFormat:@"%d", (int)_days]
                                            };
    
    return serializedDictionary;
}

@end
