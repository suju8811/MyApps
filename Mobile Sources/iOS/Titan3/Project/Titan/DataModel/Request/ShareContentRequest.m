//
//  ShareContentRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ShareContentRequest.h"
#import "Training.h"
#import "TrainingPlan.h"
#import "Aliment.h"
#import "Meal.h"
#import "Diet.h"
#import "NutritionPlan.h"
#import <Parse/Parse.h>
#import "UIImage+AFNetworking.h"

@implementation ShareContentRequest

- (id) initWithToken1:(NSString*)token1
            rlmObject:(RLMObject*)rlmObject {
    
    self = [super init];
    
    _token1 = token1;
    _rlmObject = rlmObject;
    
    return self;
}

- (NSString*)getPath {
    return @"compartir";
}

- (NSDictionary*)getSerializedDictionary {
    NSString * contentId;
    NSString * contentType;
    
    if ([_rlmObject isKindOfClass:[Training class]]) {
        Training * training = (Training*)_rlmObject;
        contentType = @"1";
        contentId = training.serverTrainingId;
    }
    else if ([_rlmObject isKindOfClass:[TrainingPlan class]]) {
        TrainingPlan * trainingPlan = (TrainingPlan*)_rlmObject;
        contentType = @"2";
        contentId = trainingPlan.serverTrainingPlanId;
    }
    else if ([_rlmObject isKindOfClass:[Aliment class]]) {
        Aliment * aliment = (Aliment*)_rlmObject;
        if (aliment.isSupplement &&
            aliment.isSupplement.boolValue) {
            
            contentType = @"4";
            contentId = aliment.supplementId;
        }
        else {
            contentType = @"3";
            contentId = aliment.alimentId;
        }
    }
    else if ([_rlmObject isKindOfClass:[Meal class]]) {
        Meal * meal = (Meal*)_rlmObject;
        contentType = @"5";
        contentId = meal.serverMealId;
    }
    else if ([_rlmObject isKindOfClass:[Diet class]]) {
        Diet * diet = (Diet*)_rlmObject;
        contentType = @"6";
        contentId = diet.serverDietId;
    }
    else if ([_rlmObject isKindOfClass:[NutritionPlan class]]) {
        NutritionPlan * nutritionPlan = (NutritionPlan*)_rlmObject;
        contentType = @"7";
        contentId = nutritionPlan.serverNutritionPlanId;
    }
    
    return
    @{
      @"token1":_token1,
      @"tipo_id":contentId,
      @"tipo":contentType
      };
}

- (NSString*)messageForContent {
    NSString * message = @"";
    
    if ([_rlmObject isKindOfClass:[Training class]]) {
        Training * training = (Training*)_rlmObject;
        message = [NSString stringWithFormat:@"Entrenamiento: %@ \n Descripción: %@", training.title, training.desc];
    }
    else if ([_rlmObject isKindOfClass:[TrainingPlan class]]) {
        TrainingPlan * trainingPlan = (TrainingPlan*)_rlmObject;
        message = [NSString stringWithFormat:@"Plan de entrenamiento: %@ \n Descripción: %@", trainingPlan.title, trainingPlan.desc];
    }
    else if ([_rlmObject isKindOfClass:[Aliment class]]) {
        Aliment * aliment = (Aliment*)_rlmObject;
        if (aliment.isSupplement &&
            aliment.isSupplement.boolValue) {
            message = [NSString stringWithFormat:@"Alimento: %@", aliment.title];
        }
        else {
            message = [NSString stringWithFormat:@"Supplemento: %@", aliment.title];
        }
    }
    else if ([_rlmObject isKindOfClass:[Meal class]]) {
        Meal * meal = (Meal*)_rlmObject;
        message = [NSString stringWithFormat:@"Comida: %@ \n Descripción: %@", meal.title, meal.notes];
    }
    else if ([_rlmObject isKindOfClass:[Diet class]]) {
        Diet * diet = (Diet*)_rlmObject;
        message = [NSString stringWithFormat:@"Dieta: %@ \n Descripción: %@", diet.title, diet.note];
    }
    else if ([_rlmObject isKindOfClass:[NutritionPlan class]]) {
        NutritionPlan * nutritionPlan = (NutritionPlan*)_rlmObject;
        message = [NSString stringWithFormat:@"Plan de nutrición: %@ \n Descripción: %@", nutritionPlan.title, nutritionPlan.notes];
    }
    return message;
}

- (UIImage*)imageForContent {
    UIImage * image = [UIImage imageNamed:@"img-placeholder-small"];
    NSString * imageURLString;
    
    if ([_rlmObject isKindOfClass:[Aliment class]]) {
        Aliment * aliment = (Aliment*)_rlmObject;
        imageURLString = aliment.image;
    }
    else if ([_rlmObject isKindOfClass:[Meal class]]) {
        Meal * meal = (Meal*)_rlmObject;
        imageURLString = meal.image;
    }
    
    if (imageURLString) {
        NSData * imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageURLString]];
        if (imageData != nil) {
            image = [UIImage safeImageWithData:imageData];
        }
    }
    
    return image;
}

- (void)post:(BOOL)requireAuth
completionBlock:(void (^)(id response, NSError * error)) completionBlock {
    
    PFObject * postsClass = [PFObject objectWithClassName:@"Posts"];
    PFUser * currentUser = [PFUser currentUser];
    
    // Save PFUser as a Pointer
    postsClass[@"postUser"] = currentUser;
    
    // Save data
    NSString * postText = [self messageForContent];
    postsClass[@"postText"] = postText;
    
    NSMutableArray * keywords = [NSMutableArray arrayWithArray:[postText.lowercaseString componentsSeparatedByString:@" "]];
    if (currentUser[@"fullName"]) {
        [keywords addObjectsFromArray:[[currentUser[@"fullName"] lowercaseString] componentsSeparatedByString:@" "]];
    }
    
    postsClass[@"keywords"] = keywords;
    
    postsClass[@"city"] = @"n/d";
    postsClass[@"isReported"] = @NO;
    
    UIImage * postImage = [self imageForContent];
    
    // Save Image
    if (postImage != nil) {
        NSData * imageData = UIImageJPEGRepresentation(postImage, .8f);
        PFFile * imageFile = [PFFile fileWithName:@"image.jpg" data:imageData];
        postsClass[@"postImageFile"] = imageFile;
    }
    
    [postsClass saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (error == nil) {
            [super post:requireAuth completionBlock:completionBlock];
        }
        else {
            completionBlock(nil, error);
        }
    }];
}

@end
