//
//  UpdateProfileRequest.m
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "UpdateProfileRequest.h"

@implementation UpdateProfileRequest

- (id) initWithToken:(NSString*)token
               photo:(UIImage*)photo
         dateOfBirth:(NSDate*)dateOfBirth
              gender:(NSString*)gender
              height:(NSInteger)height
              weight:(NSInteger)weight {
    
    self = [super init];
    
    _token = token;
    _photo = photo;
    _dateOfBirth = dateOfBirth;
    _gender = gender;
    _weight = weight;
    
    return self;
}

- (NSString*)getPath {
    return @"perfil";
}

- (NSDictionary*)getSerializedDictionary {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString * dateString = [formatter stringFromDate:_dateOfBirth];
    
    NSData *imageData = UIImageJPEGRepresentation(_photo, 1.);
    NSString *encodeString = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    NSDictionary * json =
    @{
      @"token":_token,
      @"fecha":dateString ? dateString : @"",
      @"genero":_gender,
//      @"foto":encodeString ? encodeString : @"",
      };
    return json;
}

@end
