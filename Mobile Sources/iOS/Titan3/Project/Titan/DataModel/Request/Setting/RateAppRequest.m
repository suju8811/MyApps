//
//  RateAppRequest.m
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RateAppRequest.h"

@implementation RateAppRequest

- (id) initWithToken:(NSString*)token
              number:(CGFloat)number {
    
    self = [super init];
    
    _token = token;
    _number = number;
    
    return self;
}

- (NSString*)getPath {
    return @"valorar";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token,
      @"numero":@((int)_number)
      };
}

@end
