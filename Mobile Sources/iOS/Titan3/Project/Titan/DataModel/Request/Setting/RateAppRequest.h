//
//  RateAppRequest.h
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface RateAppRequest : BaseRequest

@property NSString * token;
@property CGFloat number;

- (id) initWithToken:(NSString*)token
              number:(CGFloat)number;

@end
