//
//  SubmitSuggestionRequest.m
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SubmitSuggestionRequest.h"

@implementation SubmitSuggestionRequest

- (id) initWithToken:(NSString*)token
          suggestion:(Suggestion*)suggestion {
    self = [super init];
    _token = token;
    _suggestion = suggestion;
    return self;
}

- (NSString*)getPath {
    return @"dudas";
}

- (NSDictionary*)getSerializedDictionary {
    NSString * encodeString;
    if (_suggestion.image) {
        NSData *imageData = UIImageJPEGRepresentation(_suggestion.image, 1.);
        encodeString = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    }
    return
    @{
      @"token":_token,
      @"titulo":_suggestion.title,
      @"descripcion":_suggestion.desc,
      @"foto":encodeString ? encodeString : @""
      };
}


@end
