//
//  SubmitSuggestionRequest.h
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"
#import "Suggestion.h"

@interface SubmitSuggestionRequest : BaseRequest

@property NSString * token;
@property Suggestion * suggestion;

- (id) initWithToken:(NSString*)token
          suggestion:(Suggestion*)suggestion;

@end
