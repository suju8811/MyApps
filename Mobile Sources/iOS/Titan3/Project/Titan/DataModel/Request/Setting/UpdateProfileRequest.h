//
//  UpdateProfileRequest.h
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface UpdateProfileRequest : BaseRequest

@property NSString * token;
@property UIImage * photo;
@property NSDate * dateOfBirth;
@property NSString * gender;
@property NSInteger height;
@property NSInteger weight;

- (id) initWithToken:(NSString*)token
               photo:(UIImage*)photo
         dateOfBirth:(NSDate*)dateOfBirth
              gender:(NSString*)gender
              height:(NSInteger)height
              weight:(NSInteger)weight;

@end
