//
//  UserMeta.h
//  Titan
//
//  Created by Marcus Lee on 8/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserMeta : NSObject

@property (nonatomic, strong) NSString * token;
@property (nonatomic, strong) NSString * userName;
@property (nonatomic, assign) BOOL isSelected;

- (void)parseFromDictionary:(NSDictionary*)userMetaDictionary;

@end
