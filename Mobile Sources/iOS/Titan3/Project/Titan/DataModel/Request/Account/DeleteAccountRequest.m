//
//  DeleteAccountRequest.m
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DeleteAccountRequest.h"

@implementation DeleteAccountRequest

- (id) initWithToken:(NSString*)token {
    self = [super init];
    
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"eliminar";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token
      };
}

@end
