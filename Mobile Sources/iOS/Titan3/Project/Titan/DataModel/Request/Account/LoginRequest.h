//
//  LoginRequest.h
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface LoginRequest : BaseRequest

@property NSString * username;
@property NSString * password;

- (id) initWithUserName:(NSString*)username
               password:(NSString*)password;

- (void)loginWithCompletionBlock:(void (^)(BOOL isSuccess, NSString * error)) completionBlock;

@end
