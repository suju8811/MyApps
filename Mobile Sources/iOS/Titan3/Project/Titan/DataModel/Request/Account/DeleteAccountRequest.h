//
//  DeleteAccountRequest.h
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface DeleteAccountRequest : BaseRequest

@property (nonatomic, strong) NSString * token;

- (id) initWithToken:(NSString*)token;

@end
