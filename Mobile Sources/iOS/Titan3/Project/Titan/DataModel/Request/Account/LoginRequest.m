//
//  LoginRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "LoginRequest.h"
#import "KClient.h"
#import "AppContext.h"
#import "AppConstants.h"
#import "WebServiceManager.h"
#import "UserDefaultLibrary.h"
#import "DateTimeUtil.h"

@implementation LoginRequest

- (id) initWithUserName:(NSString*)username
               password:(NSString*)password {
    self = [super init];
    
    _username = username;
    _password = password;
    
    return self;
}

- (NSString*)getPath {
    return @"login";
}

- (void)loginWithCompletionBlock:(void (^)(BOOL isSuccess, NSString * error)) completionBlock {
    
    [self post:NO completionBlock:^(id response, NSError *error) {
       
        if (error) {
            completionBlock(NO, error.localizedDescription);
            return;
        }
        
        JSONDecoder * decoder = [[JSONDecoder alloc] init];
        NSArray<NSDictionary*>* responseArray = [decoder objectWithData:response];
        NSDictionary * loginResponse = [responseArray firstObject];
        
        if ([loginResponse[@"active"] intValue] == 1) {
            [[AppContext sharedInstance] setAuthToken:loginResponse[@"token"]];
            [AppContext sharedInstance].weight = [loginResponse[@"peso"] intValue];
            if ([loginResponse[@"peso"] intValue] <= 0) {
                [AppContext sharedInstance].weight = 60;
            }
            
            [AppContext saveInitialData];
            
            [UserDefaultLibrary setWithKey:IS_USER_LOGIN WithObject:[NSNumber numberWithBool:TRUE]];
            [UserDefaultLibrary setWithKey:USER_PASSWORD WithObject:_password];
            [UserDefaultLibrary setWithKey:USER_NAME WithObject:_username];
            
            [AppContext sharedInstance].currentUser = [[User alloc] init];
            [AppContext sharedInstance].currentUser.userId = [AppContext sharedInstance].authToken;
            [AppContext sharedInstance].currentUser.name = _username;
            [AppContext sharedInstance].currentUser.mail = [loginResponse valueForKey:@"email"];
            [AppContext sharedInstance].currentUser.password = _password;
            [AppContext sharedInstance].currentUser.birthDate = [DateTimeUtil dateFromString:[loginResponse valueForKey:@"fecha_nacimiento"] format:@"yyyy-MM-dd"];
            [AppContext sharedInstance].currentUser.gender = [loginResponse valueForKey:@"genero"];
            [AppContext sharedInstance].currentUser.height = @([[loginResponse valueForKey:@"altura"] floatValue]);
            [AppContext sharedInstance].currentUser.weight = @([[loginResponse valueForKey:@"peso"] intValue]);
            [AppContext sharedInstance].currentUser.privacy = [NSNumber numberWithBool:YES];
            
            completionBlock(true, nil);
        }
        else {
            completionBlock(false, nil);
        }
    }];
}

- (NSDictionary*)getSerializedDictionary {
    return
        @{
             @"username":_username,
             @"password":_password
         };
}

@end
