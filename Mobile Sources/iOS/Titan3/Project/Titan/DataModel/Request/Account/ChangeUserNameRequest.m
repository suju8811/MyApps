//
//  ChangeUserNameRequest.m
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ChangeUserNameRequest.h"

@implementation ChangeUserNameRequest

- (id) initWithToken:(NSString*)token
            userName:(NSString*)userName {
    
    self = [super init];
    
    _token = token;
    _userName = userName;
    
    return self;
}

- (NSString*)getPath {
    return @"cambio_nombre";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token,
      @"nombre":_userName
      };
}

@end
