//
//  ChangePasswordRequest.h
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface ChangePasswordRequest : BaseRequest

@property (nonatomic, strong) NSString * token;
@property (nonatomic, strong) NSString * password;

- (id) initWithToken:(NSString*)token
            password:(NSString*)password;

@end
