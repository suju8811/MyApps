//
//  ChangeEmailRequest.m
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ChangeEmailRequest.h"

@implementation ChangeEmailRequest

- (id) initWithToken:(NSString*)token
               email:(NSString*)email {
    
    self = [super init];
    
    _token = token;
    _email = email;
    
    return self;
}

- (NSString*)getPath {
    return @"cambio_email";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token,
      @"email":_email
      };
}

@end
