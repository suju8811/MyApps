//
//  ChangePasswordRequest.m
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ChangePasswordRequest.h"

@implementation ChangePasswordRequest

- (id) initWithToken:(NSString*)token
            password:(NSString*)password {
    
    self = [super init];
    
    _token = token;
    _password = password;
    
    return self;
}

- (NSString*)getPath {
    return @"cambio_pass";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token,
      @"password":_password
      };
}

@end
