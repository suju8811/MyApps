//
//  ChangeUserNameRequest.h
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface ChangeUserNameRequest : BaseRequest

@property (nonatomic, strong) NSString * token;
@property (nonatomic, strong) NSString * userName;

- (id) initWithToken:(NSString*)token
            userName:(NSString*)userName;

@end
