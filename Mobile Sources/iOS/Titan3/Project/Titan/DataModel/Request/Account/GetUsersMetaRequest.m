//
//  GetUsersMetaRequest.m
//  Titan
//
//  Created by Marcus Lee on 8/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "GetUsersMetaRequest.h"

@implementation GetUsersMetaRequest

- (NSString*)getPath {
    return @"lista";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{};
}

@end
