//
//  UserMeta.m
//  Titan
//
//  Created by Marcus Lee on 8/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "UserMeta.h"

@implementation UserMeta

- (void)parseFromDictionary:(NSDictionary*)userMetaDictionary {
    _token = [userMetaDictionary valueForKey:@"token"];
    _userName = [userMetaDictionary valueForKey:@"nombre"];
}

@end
