//
//  ChangeEmailRequest.h
//  Titan
//
//  Created by Marcus Lee on 10/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface ChangeEmailRequest : BaseRequest

@property (nonatomic, strong) NSString * token;
@property (nonatomic, strong) NSString * email;

- (id) initWithToken:(NSString*)token
               email:(NSString*)email;

@end
