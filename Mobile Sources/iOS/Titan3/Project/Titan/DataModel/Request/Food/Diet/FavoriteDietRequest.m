//
//  FavoriteDietRequest.m
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FavoriteDietRequest.h"

@implementation FavoriteDietRequest

- (id)initWithDietId:(NSString*)dietId
               token:(NSString*)token
          isFavorite:(BOOL)isFavorite {
    
    self = [super init];
    
    _dietId = dietId;
    _token = token;
    _isFavorite = isFavorite;
    
    return self;
}

- (NSString*)getPath {
    return @"favorito_dietas";
}

- (NSDictionary*)getSerializedDictionary {
    NSDictionary * resultDictionary =
    @{
      @"id":_dietId,
      @"token":_token,
      @"favorito":(_isFavorite ? @"1":@"0")
      };
    
    return resultDictionary;
}

@end
