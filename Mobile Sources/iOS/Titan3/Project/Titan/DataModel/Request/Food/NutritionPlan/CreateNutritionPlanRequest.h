//
//  CreateNutritionPlanRequest.h
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"
#import "NutritionPlan.h"

@interface CreateNutritionPlanRequest : BaseRequest

@property NSString * token;

@property NutritionPlan * nutritionPlan;

- (id)initWithToken:(NSString*)token
      nutritionPlan:(NutritionPlan*)nutritionPlan;

@end
