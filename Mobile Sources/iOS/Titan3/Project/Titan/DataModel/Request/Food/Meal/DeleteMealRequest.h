//
//  DeleteMealRequest.h
//  Titan
//
//  Created by Marcus Lee on 28/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface DeleteMealRequest : BaseRequest

@property NSString * token;

@property NSString * mealId;

- (id)initWithMealId:(NSString*)mealId
               token:(NSString*)token;

@end
