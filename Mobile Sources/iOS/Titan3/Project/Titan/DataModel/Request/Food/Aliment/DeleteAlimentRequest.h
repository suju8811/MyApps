//
//  DeleteAlimentRequest.h
//  Titan
//
//  Created by Marcus Lee on 2/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"
#import "Aliment.h"

@interface DeleteAlimentRequest : BaseRequest

@property NSString * token;

@property NSString * alimentId;

- (id)initWithAlimentId:(NSString*)alimentId
                  token:(NSString*)token;

@end
