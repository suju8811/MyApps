//
//  EditMealRequest.h
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"
#import "Meal.h"

@interface EditMealRequest : BaseRequest

@property NSString * token;
@property Meal * meal;
@property UIImage * image;

- (id)initWithToken:(NSString*)token
               meal:(Meal*)meal
              image:(UIImage*)image;

@end
