//
//  DeleteMealRequest.m
//  Titan
//
//  Created by Marcus Lee on 28/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DeleteMealRequest.h"

@implementation DeleteMealRequest

- (id)initWithMealId:(NSString*)mealId
               token:(NSString*)token {
    
    self = [super init];
    
    _mealId = mealId;
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"delete_comidas";
}

- (NSDictionary*)getSerializedDictionary {
    NSDictionary * resultDictionary =
    @{
      @"id":_mealId,
      @"token":_token
      };
    
    return resultDictionary;
}

@end
