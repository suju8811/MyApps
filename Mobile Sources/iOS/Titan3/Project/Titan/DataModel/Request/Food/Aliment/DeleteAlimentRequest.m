//
//  DeleteAlimentRequest.m
//  Titan
//
//  Created by Marcus Lee on 2/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DeleteAlimentRequest.h"

@implementation DeleteAlimentRequest

- (id)initWithAlimentId:(NSString*)alimentId
                  token:(NSString*)token {
    
    self = [super init];
    
    _alimentId = alimentId;
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"eliminar_alimento";
}

- (NSDictionary*)getSerializedDictionary {
    NSDictionary * resultDictionary =
    @{
      @"id":_alimentId,
      @"token":_token
      };
    
    return resultDictionary;
}

@end
