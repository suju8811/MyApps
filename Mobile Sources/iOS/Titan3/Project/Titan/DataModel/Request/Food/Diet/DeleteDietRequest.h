//
//  DeleteDietRequest.h
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface DeleteDietRequest : BaseRequest

@property NSString * token;

@property NSString * dietId;

- (id)initWithDietId:(NSString*)dietId
               token:(NSString*)token;

@end
