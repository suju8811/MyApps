//
//  GetNutritionPlanRequest.h
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface GetNutritionPlansRequest : BaseRequest

@property NSString * token;
@property NSString * updatedAt;

- (id)initWithToken:(NSString*)token
          updatedAt:(NSString*)updatedAt;

@end
