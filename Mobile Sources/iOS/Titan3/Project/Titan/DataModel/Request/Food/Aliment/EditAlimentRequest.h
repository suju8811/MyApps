//
//  EditAlimentRequest.h
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseRequest.h"
#import "Aliment.h"

@interface EditAlimentRequest : BaseRequest

@property NSString * token;

@property Aliment * aliment;

@property UIImage * image;

- (id)initWithToken:(NSString*)token
            aliment:(Aliment*)aliment
              image:(UIImage*)image;
@end
