//
//  GetNutritionPlanRequest.m
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "GetNutritionPlansRequest.h"

@implementation GetNutritionPlansRequest

- (id)initWithToken:(NSString*)token
          updatedAt:(NSString*)updatedAt {
    
    self = [super init];
    
    _token = token;
    _updatedAt = updatedAt;
    
    return self;
}

- (NSString*)getPath {
    return @"planes_nutricionales";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token,
      @"updatedAt":_updatedAt
      };
}

@end
