//
//  FavoriteMealRequest.h
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface FavoriteMealRequest : BaseRequest

@property NSString * token;

@property NSString * mealId;

@property (nonatomic, assign) BOOL isFavorite;

- (id)initWithMealId:(NSString*)mealId
               token:(NSString*)token
          isFavorite:(BOOL)isFavorite;

@end
