//
//  FavoriteMealRequest.m
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FavoriteMealRequest.h"

@implementation FavoriteMealRequest

- (id)initWithMealId:(NSString*)mealId
               token:(NSString*)token
          isFavorite:(BOOL)isFavorite {
    
    self = [super init];
    
    _mealId = mealId;
    _token = token;
    _isFavorite = isFavorite;
    
    return self;
}

- (NSString*)getPath {
    return @"favorito_comidas";
}

- (NSDictionary*)getSerializedDictionary {
    NSDictionary * resultDictionary =
    @{
      @"id":_mealId,
      @"token":_token,
      @"favorito":(_isFavorite ? @"1":@"0")
      };
    
    return resultDictionary;
}

@end
