//
//  GetDietsRequest.m
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "GetDietsRequest.h"

@implementation GetDietsRequest

- (id)initWithToken:(NSString*)token {
    
    self = [super init];
    
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"dietas";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token
      };
}

@end
