//
//  GetAlimentsRequest.m
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "GetAlimentsRequest.h"

@implementation GetAlimentsRequest

- (id)initWithToken:(NSString*)token {
    
    self = [super init];
    
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"alimentos";
}

- (NSDictionary*)getSerializedDictionary {
    return
    @{
      @"token":_token
      };
}

@end
