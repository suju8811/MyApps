//
//  CreateNutritionPlanRequest.m
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CreateNutritionPlanRequest.h"
#import "TrainingDay.h"

@implementation CreateNutritionPlanRequest

- (id)initWithToken:(NSString*)token
      nutritionPlan:(NutritionPlan*)nutritionPlan {
    
    self = [super init];
    
    _token = token;
    _nutritionPlan = nutritionPlan;
    
    return self;
}

- (NSString*)getPath {
    return @"crear_planes_nutricionales";
}

- (NSDictionary*)getSerializedDictionary {
    NSMutableArray * planDayArray = [[NSMutableArray alloc] init];
    NSUInteger order = 1;
    for (NutritionDay * nutritionDay in _nutritionPlan.nutritionDays) {
        NSMutableArray * planDayContentArray = [[NSMutableArray alloc] init];
        for (NSInteger index = 0; index < 1; index++) {
            if (nutritionDay.diet == nil) {
                continue;
            }
            NSDictionary * trainingDayContentDictionary =
            @{
              @"training":nutritionDay.diet.serverDietId,
              @"start_hour":nutritionDay.start ? nutritionDay.start : @"",
              @"stop_hour":nutritionDay.end ? nutritionDay.end : @""
              };

            [planDayContentArray addObject:trainingDayContentDictionary];
        }
        
        NSDictionary * nutritionDayDictionary =
        @{
          @"rest":@0,
          @"orden":nutritionDay.order ? nutritionDay.order : [NSNumber numberWithInteger:order],
          @"plan_dia_training":planDayContentArray
          };
        [planDayArray addObject:nutritionDayDictionary];
        order = order + 1;
    }
    
    NSMutableArray * tagsArray = [[NSMutableArray alloc] init];
//    for (Tag * tag in _nutritionPlan.tags) {
//        [tagsArray addObject:tag.title];
//    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString * startDateString = [formatter stringFromDate:_nutritionPlan.startDate];
    NSString * endDateString = [formatter stringFromDate:_nutritionPlan.endDate];
    
    NSMutableDictionary * resultDictionary
    = [NSMutableDictionary dictionaryWithDictionary:
       @{
         @"token":_token,
         @"titulo":_nutritionPlan.title,
         @"descripcion":_nutritionPlan.content,
         @"nota":_nutritionPlan.notes,
         @"comienzo":startDateString ? startDateString : @"",
         @"finalizacion":endDateString ? endDateString : @"",
         @"plan_dia":[self jsonFromArray:planDayArray],
         @"plan_tags":[self jsonFromArray:tagsArray],
         @"calendario":_nutritionPlan.isCalendarCopy ? _nutritionPlan.isCalendarCopy : @0,
         }];
    return resultDictionary;
}

@end
