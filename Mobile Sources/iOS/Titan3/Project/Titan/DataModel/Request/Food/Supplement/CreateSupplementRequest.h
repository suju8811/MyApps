//
//  CreateSupplementRequest.h
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"
#import "Aliment.h"

@interface CreateSupplementRequest : BaseRequest

@property NSString * token;

@property Aliment * aliment;

- (id)initWithToken:(NSString*)token
            aliment:(Aliment*)aliment;

@end
