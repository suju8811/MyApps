//
//  DeleteNutritionPlanRequest.m
//  Titan
//
//  Created by Marcus Lee on 28/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DeleteNutritionPlanRequest.h"

@implementation DeleteNutritionPlanRequest

- (id)initWithNutritionPlanId:(NSString*)nutritionPlanId
                        token:(NSString*)token {
    
    self = [super init];
    
    _nutritionPlanId = nutritionPlanId;
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"delete_planes_nutricionales";
}

- (NSDictionary*)getSerializedDictionary {
    NSDictionary * resultDictionary =
    @{
      @"id":_nutritionPlanId,
      @"token":_token
      };
    
    return resultDictionary;
}
@end
