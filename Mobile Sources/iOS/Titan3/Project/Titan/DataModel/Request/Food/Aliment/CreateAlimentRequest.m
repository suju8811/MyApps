//
//  CreateAlimentRequest.m
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CreateAlimentRequest.h"
#import "FoodAttribute.h"

@implementation CreateAlimentRequest

- (id)initWithToken:(NSString*)token
            aliment:(Aliment*)aliment
              image:(UIImage*)image {
    
    self = [super init];
    
    _token = token;
    _aliment = aliment;
    _image = image;
    
    return self;
}

- (NSString*)getPath {
    if (_aliment.isSupplement) {
        return @"crear_suplementos";
    }
    else {
        return @"crear_alimentos";
    }
}

- (NSDictionary*)getSerializedDictionary {
    NSData *imageData = UIImageJPEGRepresentation(_image, 1);
    NSString *encodeString = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    NSMutableArray * secondaryAttributes = [[NSMutableArray alloc] init];
    if (_aliment.isSupplement) {
        for (FoodAttribute * foodAtrribute in _aliment.secondaryAttributes) {
            NSDictionary * secondaryAttributesDictionary =
            @{
              @"titulo":foodAtrribute.name,
              @"valor":foodAtrribute.value
              };
            
            [secondaryAttributes addObject:secondaryAttributesDictionary];
        }
    }
    
    NSMutableDictionary * resultDictionary
    = [NSMutableDictionary dictionaryWithDictionary:
       @{
         @"token":_token,
         (_aliment.isSupplement && _aliment.isSupplement.boolValue) ? @"titulo" : @"nombre" :_aliment.title,
         @"descripcion":_aliment.content,
         @"medida":_aliment.measureUnit,
         (_aliment.isSupplement && _aliment.isSupplement.boolValue) ? @"conversion" : @"cadena_conversion":_aliment.conversionString,
         @"factor_conversion":_aliment.conversionFactor,
         @"calorias":_aliment.calories,
         @"proteinas":_aliment.proteins,
         @"carbohidratos":_aliment.carbs,
         @"fibra":_aliment.fiber,
         @"grasas":_aliment.fats,
         @"grasas_saturadas":_aliment.saturedFats,
         @"grasas_monoinsaturadas":_aliment.monoUnsaturatedFats,
         @"grasas_poliinsaturadas":_aliment.polyUnsaturatedFats,
         @"agp_ags":_aliment.agpAgs,
         @"agp_ags_agm":_aliment.agpAgsAgm,
         @"colesterol":_aliment.cholesterol,
         @"alcohol":_aliment.alcohol,
         @"agua":_aliment.water,
         @"sodio":_aliment.sodium,
         @"azucar":_aliment.sugar,
         @"calcio":_aliment.calcium,
         @"hierro":_aliment.iron,
         @"yodo":_aliment.iodine,
         @"magnesio":_aliment.magnesium,
         @"zinc":_aliment.zinc,
         @"selenio":_aliment.selenium,
         @"potasio":_aliment.potasium,
         @"fosforo":_aliment.phosphorus,
         @"v_b1":_aliment.vitB1,
         @"v_b2":_aliment.vitB2,
         @"niacina":_aliment.niacin,
         @"v_b6":_aliment.vitB6,
         @"acido_folico":_aliment.folicAcid,
         @"v_b12":_aliment.vitB12,
         @"v_c":_aliment.vitC,
         @"retinol":_aliment.retynol,
         @"beta_caroteno":_aliment.betaCaronete,
         @"v_a":_aliment.vitA,
         @"v_d":_aliment.vitD,
         @"indice_glucemico":_aliment.glycemicIndex,
         @"bccas":_aliment.bccas,
         @"glutamina":_aliment.glutamine,
         @"omega3":_aliment.omega3,
         @"omega6":_aliment.omega6,
         @"etiqueta1":_aliment.tag1,
         @"etiqueta2":_aliment.tag2,
         (_aliment.isSupplement && _aliment.isSupplement.boolValue) ? @"enlace" : @"etiqueta3":_aliment.group ? _aliment.group : @"",
         @"imagen":encodeString ? encodeString : @"",
         (_aliment.isSupplement && _aliment.isSupplement.boolValue) ?
         @"energia":@"popularidad":(_aliment.isSupplement && _aliment.isSupplement.boolValue) ? @1 : _aliment.privacity,
         @"secundarios":[self jsonFromArray:secondaryAttributes]
         }];
    return resultDictionary;
}

@end
