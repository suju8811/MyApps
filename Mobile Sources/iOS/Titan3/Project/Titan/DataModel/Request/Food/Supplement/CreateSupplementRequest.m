//
//  CreateSupplementRequest.m
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "CreateSupplementRequest.h"

@implementation CreateSupplementRequest

- (id)initWithToken:(NSString*)token
            aliment:(Aliment*)aliment {
    
    self = [super init];
    
    _token = token;
    _aliment = aliment;
    
    return self;
}

- (NSString*)getPath {
    return @"crear_suplementos";
}

- (NSDictionary*)getSerializedDictionary {
    
    NSMutableDictionary * secondaryDictionary = [NSMutableDictionary dictionaryWithDictionary:
                                                 @{
                                                   @"titulo":@1,
                                                   @"valor":@1
                                                   }];
    
    NSMutableDictionary * resultDictionary
    = [NSMutableDictionary dictionaryWithDictionary:
       @{
         @"token":_token,
         @"titulo":@1,
         @"descripcion":@1,
         @"medida":@1,
         @"etiqueta1_id":@1,
         @"etiqueta2_id":@1,
         @"imagen":@1,
         @"enlace":@1,
         @"conversion":@1,
         @"energia":@1,
         @"factor_conversion":@1,
         @"calorias":@1,
         @"proteinas":@1,
         @"carbohidratos":@1,
         @"fibra":@1,
         @"grasas":@1,
         @"grasas_saturadas":@1,
         @"grasa_monoinsaturadas":@1,
         @"grasas_poliinsaturadas":@1,
         @"agp_ags":@1,
         @"agp_ags_agm":@1,
         @"colesterol":@1,
         @"alcohol":@1,
         @"agua":@1,
         @"sodio":@1,
         @"azucar":@1,
         @"calcio":@1,
         @"hierro":@1,
         @"yodo":@1,
         @"magnesio":@1,
         @"zinc":@1,
         @"selenio":@1,
         @"potasio":@1,
         @"fosforo":@1,
         @"v_b1":@1,
         @"v_b2":@1,
         @"niacina":@1,
         @"v_b6":@1,
         @"acido_folico":@1,
         @"v_b12":@1,
         @"v_c":@1,
         @"retinol":@1,
         @"beta_caroteno":@1,
         @"v_a":@1,
         @"v_d":@1,
         @"indice_glucemico":@1,
         @"bccas":@1,
         @"glutamina":@1,
         @"omega3":@1,
         @"omega6":@1,
         @"secundarios":secondaryDictionary
         }];
    return resultDictionary;
}

@end
