//
//  DeleteDietRequest.m
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DeleteDietRequest.h"

@implementation DeleteDietRequest

- (id)initWithDietId:(NSString*)dietId
               token:(NSString*)token {
    
    self = [super init];
    
    _dietId = dietId;
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"delete_dietas";
}

- (NSDictionary*)getSerializedDictionary {
    NSDictionary * resultDictionary =
    @{
      @"id":_dietId,
      @"token":_token
      };
    
    return resultDictionary;
}

@end
