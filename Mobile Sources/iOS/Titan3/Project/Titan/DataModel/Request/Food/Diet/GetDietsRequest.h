//
//  GetDietsRequest.h
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface GetDietsRequest : BaseRequest

@property NSString * token;

- (id)initWithToken:(NSString*)token;

@end
