//
//  FavoriteNutritionPlanRequest.h
//  Titan
//
//  Created by Marcus Lee on 28/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface FavoriteNutritionPlanRequest : BaseRequest

@property NSString * token;

@property NSString * nutritionPlanId;

@property (nonatomic, assign) BOOL isFavorite;

- (id)initWithNutritionPlanId:(NSString*)nutritionPlanId
                        token:(NSString*)token
                   isFavorite:(BOOL)isFavorite;

@end
