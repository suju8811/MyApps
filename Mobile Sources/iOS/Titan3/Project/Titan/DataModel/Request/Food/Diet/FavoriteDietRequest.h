//
//  FavoriteDietRequest.h
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface FavoriteDietRequest : BaseRequest

@property NSString * token;

@property NSString * dietId;

@property (nonatomic, assign) BOOL isFavorite;

- (id)initWithDietId:(NSString*)dietId
               token:(NSString*)token
          isFavorite:(BOOL)isFavorite;

@end
