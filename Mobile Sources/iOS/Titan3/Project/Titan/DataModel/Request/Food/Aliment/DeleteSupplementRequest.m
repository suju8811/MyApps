//
//  DeleteSupplementRequest.m
//  Titan
//
//  Created by Marcus Lee on 6/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "DeleteSupplementRequest.h"

@implementation DeleteSupplementRequest

- (id)initWithSupplementId:(NSString*)supplementId
                     token:(NSString*)token {
    
    self = [super init];
    
    _supplementId = supplementId;
    _token = token;
    
    return self;
}

- (NSString*)getPath {
    return @"eliminar_suplemento";
}

- (NSDictionary*)getSerializedDictionary {
    NSDictionary * resultDictionary =
    @{
      @"id":_supplementId,
      @"token":_token
      };
    
    return resultDictionary;
}

@end
