//
//  DeleteNutritionPlanRequest.h
//  Titan
//
//  Created by Marcus Lee on 28/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@interface DeleteNutritionPlanRequest : BaseRequest

@property NSString * token;

@property NSString * nutritionPlanId;

- (id)initWithNutritionPlanId:(NSString*)nutritionPlanId
                        token:(NSString*)token;

@end
