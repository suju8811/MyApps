//
//  DeleteSupplementRequest.h
//  Titan
//
//  Created by Marcus Lee on 6/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"
#import "Aliment.h"

@interface DeleteSupplementRequest : BaseRequest

@property NSString * token;

@property NSString * supplementId;

- (id)initWithSupplementId:(NSString*)supplementId
                     token:(NSString*)token;

@end
