//
//  EditDietRequest.h
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"
#import "Diet.h"

@interface EditDietRequest : BaseRequest

@property NSString * token;
@property Diet * diet;

- (id)initWithToken:(NSString*)token
               diet:(Diet*)diet;

@end
