//
//  EditDietRequest.m
//  Titan
//
//  Created by Marcus Lee on 29/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "EditDietRequest.h"

@implementation EditDietRequest

- (id)initWithToken:(NSString*)token
               diet:(Diet*)diet {
    
    self = [super init];
    
    _token = token;
    _diet = diet;
    
    return self;
}

- (NSString*)getPath {
    return @"editar_dietas";
}

- (NSDictionary*)getSerializedDictionary {
    
    NSMutableArray * meals = [[NSMutableArray alloc] init];
    NSInteger order = 0;
    for (DietMeal * dietMeal in _diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            int quantity = 0;
            for (Ingredient * ingredient in meal.ingredients) {
                if (ingredient.quantity) {
                    quantity += ingredient.quantity.intValue;
                }
            }
            
            NSDictionary * mealDictionary =
            @{
              @"orden":@(order),
              @"comida_id":meal.serverMealId,
              @"medida":meal.measureUnit ? meal.measureUnit : @"",
              @"cantidad":[NSNumber numberWithInt:quantity],
              @"start":dietMeal.startTime ? dietMeal.startTime : @"00:00:00"
              };
            
            [meals addObject:mealDictionary];
        }
        order ++;
    }
    
    NSMutableDictionary * resultDictionary
    = [NSMutableDictionary dictionaryWithDictionary:
       @{
         @"id":_diet.serverDietId,
         @"token":_token,
         @"nombre":_diet.title,
         @"nota":_diet.content,
         @"calorias":_diet.calories ? _diet.calories:@1,
         @"proteinas":_diet.proteins ? _diet.proteins:@1,
         @"carbohidratos":_diet.carbs ? _diet.carbs:@0,
         @"fibra":_diet.fiber ? _diet.fiber:@0,
         @"grasas":_diet.fats ? _diet.fats:@0,
         @"grasas_saturadas":_diet.saturedFats ? _diet.saturedFats:@0,
         @"grasas_monoinsaturadas":_diet.monoUnsaturatedFats ? _diet.monoUnsaturatedFats:@0 ,
         @"grasas_poliinsaturadas":_diet.polyUnsaturatedFats ? _diet.polyUnsaturatedFats:@0,
         @"agp_ags":_diet.agpAgs ? _diet.agpAgs:@0,
         @"agp_ags_agm":_diet.agpAgsAgm ? _diet.agpAgsAgm:@0,
         @"colesterol":_diet.cholesterol ? _diet.cholesterol:@0,
         @"alcohol":_diet.alcohol ? _diet.alcohol:@0,
         @"agua":_diet.water ? _diet.water:@0,
         @"sodio":_diet.sodium ? _diet.sodium:@0,
         @"azucar":_diet.sugar ? _diet.sugar:@0,
         @"calcio":_diet.calcium ? _diet.calcium:@0,
         @"hierro":_diet.iron ? _diet.iron:@0,
         @"yodo":_diet.iodine ? _diet.iodine:@0,
         @"magnesio":_diet.magnesium ? _diet.magnesium:@0,
         @"zinc":_diet.zinc ? _diet.zinc:@0,
         @"selenio":_diet.selenium ? _diet.selenium:@0,
         @"potasio":_diet.potasium ? _diet.potasium:@0,
         @"fosforo":_diet.phosphorus ? _diet.phosphorus:@0,
         @"v_b1":_diet.vitB1 ? _diet.vitB1:@0,
         @"v_b2":_diet.vitB2 ? _diet.vitB2:@0,
         @"niacina":_diet.niacin ? _diet.niacin:@0,
         @"v_b6":_diet.vitB6 ? _diet.vitB6:@0,
         @"acido_folico":_diet.folicAcid ? _diet.folicAcid:@0,
         @"v_b12":_diet.vitB12 ? _diet.vitB12:@0,
         @"v_c":_diet.vitC ? _diet.vitC:@0,
         @"retinol":_diet.retynol ? _diet.retynol:@0,
         @"beta_caroteno":_diet.betaCaronete ? _diet.betaCaronete:@0,
         @"v_a":_diet.vitA ? _diet.vitA:@0,
         @"v_d":_diet.vitD ? _diet.vitD:@0,
         @"indice_glucemico":_diet.glycemicIndex ? _diet.glycemicIndex:@0,
         @"bccas":_diet.bccas ? _diet.bccas:@0,
         @"glutamina":_diet.glutamine ? _diet.glutamine:@0,
         @"omega3":_diet.omega3 ? _diet.omega3:@0,
         @"omega6":_diet.omega6 ? _diet.omega6:@0,
         @"etiqueta1":@"1",
         @"etiqueta2":@"2",
         @"etiqueta3":@"3",
         @"comidas":[self jsonFromArray:meals]
         }];
    return resultDictionary;
}

@end
