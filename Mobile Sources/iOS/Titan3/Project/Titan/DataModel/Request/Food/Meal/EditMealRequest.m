//
//  EditMealRequest.m
//  Titan
//
//  Created by Marcus Lee on 23/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "EditMealRequest.h"

@implementation EditMealRequest

- (id)initWithToken:(NSString*)token
               meal:(Meal*)meal
              image:(UIImage*)image {
    
    self = [super init];
    
    _token = token;
    _meal = meal;
    _image = image;
    
    return self;
}

- (NSString*)getPath {
    return @"editar_comidas";
}

- (NSDictionary*)getSerializedDictionary {
    
    NSMutableArray * ingredients = [[NSMutableArray alloc] init];
    for(Ingredient *ingredient in _meal.ingredients) {
        NSString * alimentId = ingredient.aliment.alimentId;
        NSString * supplement = @"0";
        if (ingredient.aliment.isSupplement &&
            ingredient.aliment.isSupplement.boolValue) {
            
            alimentId = ingredient.aliment.supplementId;
            supplement = @"1";
        }
        
        NSDictionary * ingredientDictionary =
        @{
          @"orden":@1,
          @"alimento_id":alimentId,
          @"suplemento":supplement,
          @"medida":ingredient.quantityUnit,
          @"cantidad":ingredient.quantity
          };
        
        [ingredients addObject:ingredientDictionary];
    }
    
    NSData *imageData = UIImageJPEGRepresentation(_image, 1.);
    NSString *encodeString = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    NSMutableDictionary * resultDictionary
    = [NSMutableDictionary dictionaryWithDictionary:
       @{
         @"id":_meal.serverMealId,
         @"token":_token,
         @"nombre":_meal.title,
         @"nota":_meal.content,
         @"calorias":_meal.calories ? _meal.calories:@1,
         @"proteinas":_meal.proteins ? _meal.proteins:@1,
         @"carbohidratos":_meal.carbs ? _meal.carbs:@0,
         @"fibra":_meal.fiber ? _meal.fiber:@0,
         @"grasas":_meal.fats ? _meal.fats:@0,
         @"grasas_saturadas":_meal.saturedFats ? _meal.saturedFats:@0,
         @"grasas_monoinsaturadas":_meal.monoUnsaturatedFats ? _meal.monoUnsaturatedFats:@0 ,
         @"grasas_poliinsaturadas":_meal.polyUnsaturatedFats ? _meal.polyUnsaturatedFats:@0,
         @"agp_ags":_meal.agpAgs ? _meal.agpAgs:@0,
         @"agp_ags_agm":_meal.agpAgsAgm ? _meal.agpAgsAgm:@0,
         @"colesterol":_meal.cholesterol ? _meal.cholesterol:@0,
         @"alcohol":_meal.alcohol ? _meal.alcohol:@0,
         @"agua":_meal.water ? _meal.water:@0,
         @"sodio":_meal.sodium ? _meal.sodium:@0,
         @"azucar":_meal.sugar ? _meal.sugar:@0,
         @"calcio":_meal.calcium ? _meal.calcium:@0,
         @"hierro":_meal.iron ? _meal.iron:@0,
         @"yodo":_meal.iodine ? _meal.iodine:@0,
         @"magnesio":_meal.magnesium ? _meal.magnesium:@0,
         @"zinc":_meal.zinc ? _meal.zinc:@0,
         @"selenio":_meal.selenium ? _meal.selenium:@0,
         @"potasio":_meal.potasium ? _meal.potasium:@0,
         @"fosforo":_meal.phosphorus ? _meal.phosphorus:@0,
         @"v_b1":_meal.vitB1 ? _meal.vitB1:@0,
         @"v_b2":_meal.vitB2 ? _meal.vitB2:@0,
         @"niacina":_meal.niacin ? _meal.niacin:@0,
         @"v_b6":_meal.vitB6 ? _meal.vitB6:@0,
         @"acido_folico":_meal.folicAcid ? _meal.folicAcid:@0,
         @"v_b12":_meal.vitB12 ? _meal.vitB12:@0,
         @"v_c":_meal.vitC ? _meal.vitC:@0,
         @"retinol":_meal.retynol ? _meal.retynol:@0,
         @"beta_caroteno":_meal.betaCaronete ? _meal.betaCaronete:@0,
         @"v_a":_meal.vitA ? _meal.vitA:@0,
         @"v_d":_meal.vitD ? _meal.vitD:@0,
         @"indice_glucemico":_meal.glycemicIndex ? _meal.glycemicIndex:@0,
         @"bccas":_meal.bccas ? _meal.bccas:@0,
         @"glutamina":_meal.glutamine ? _meal.glutamine:@0,
         @"omega3":_meal.omega3 ? _meal.omega3:@0,
         @"omega6":_meal.omega6 ? _meal.omega6:@0,
         @"etiqueta1":@"1",
         @"etiqueta2":@"2",
         @"etiqueta3":@"3",
         @"imagen":encodeString == nil ? @"":encodeString,
         @"ingredientes":[self jsonFromArray:ingredients]
         }];
    return resultDictionary;
}

@end
