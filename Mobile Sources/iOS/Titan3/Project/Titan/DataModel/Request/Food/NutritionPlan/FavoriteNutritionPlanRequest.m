//
//  FavoriteNutritionPlanRequest.m
//  Titan
//
//  Created by Marcus Lee on 28/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FavoriteNutritionPlanRequest.h"

@implementation FavoriteNutritionPlanRequest

- (id)initWithNutritionPlanId:(NSString*)nutritionPlanId
                        token:(NSString*)token
                   isFavorite:(BOOL)isFavorite {
    
    self = [super init];
    
    _nutritionPlanId = nutritionPlanId;
    _token = token;
    _isFavorite = isFavorite;
    
    return self;
}

- (NSString*)getPath {
    return @"favorito_planes_nutricionales";
}

- (NSDictionary*)getSerializedDictionary {
    NSDictionary * resultDictionary =
    @{
      @"id":_nutritionPlanId,
      @"token":_token,
      @"favorito":(_isFavorite ? @"1":@"0")
      };
    
    return resultDictionary;
}

@end
