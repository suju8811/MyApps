//
//  BaseRequest.m
//  Titan
//
//  Created by Marcus Lee on 9/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseRequest.h"

@implementation BaseRequest

- (void)post:(BOOL)requireAuth
completionBlock:(void (^)(id response, NSError * error)) completionBlock {
    
    [self sendRequest:REQUEST_POST
          requireAuth:requireAuth
      completionBlock:^(id response, NSError *error) {
        completionBlock(response, error);
      }];
}

- (void)get:(BOOL)requireAuth
completionBlock:(void (^)(id response, NSError * error)) completionBlock {
    
    [self sendRequest:REQUEST_GET
          requireAuth:requireAuth
      completionBlock:^(id response, NSError *error) {
          completionBlock(response, error);
      }];
}

- (void)put:(BOOL) requireAuth
completionBlock:(void (^)(id response, NSError * error)) completionBlock {
    
    [self sendRequest:REQUEST_PUT
          requireAuth:requireAuth
      completionBlock:^(id response, NSError *error) {
          completionBlock(response, error);
      }];
}

- (void)head:(BOOL) requireAuth
completionBlock:(void (^)(id response, NSError * error)) completionBlock {
    
    [self sendRequest:REQUEST_HEAD
          requireAuth:requireAuth
      completionBlock:^(id response, NSError *error) {
          completionBlock(response, error);
      }];
}

- (void)delete:(BOOL) requireAuth
completionBlock:(void (^)(id response, NSError * error)) completionBlock {
    
    [self sendRequest:REQUEST_DELETE
          requireAuth:requireAuth
      completionBlock:^(id response, NSError *error) {
          completionBlock(response, error);
      }];
}

- (void)sendRequest:(REQUEST_TYPE)requestType
        requireAuth:(BOOL) requireAuth
    completionBlock:(void (^)(id response, NSError * error)) completionBlock {
    
    KClient * client = [KClient getInstance];
//    if ([[self getPath] isEqualToString:@"crear_planes"]) {
//        client = [KClient getTestInstance];
//    }
    
    [client sendRequestWithType:REQUEST_POST
                           path:[self getPath]
                         params:[self getSerializedDictionary]
                    requireAuth:requireAuth
                   successBlock:^(id result) {
                       completionBlock(result, nil);
                   }
                    failedBlock:^(NSError *error) {
                        completionBlock(nil, error);
                    }];
}

- (NSDictionary*)getSerializedDictionary {
    return [[NSDictionary alloc] init];
}

- (NSString*)getPath {
    return @"";
}

- (NSString*)jsonFromArray:(NSArray*)objectsArray {
    NSError * error;
    NSString * jsonString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:objectsArray
                                                                                           options:NSJSONWritingPrettyPrinted
                                                                                             error:&error]
                                                  encoding:NSUTF8StringEncoding];
    
    if (error != nil) {
        return nil;
    }
    
    return jsonString;
}

@end
