//
//  Tag.m
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import "Tag.h"
#import "AppContext.h"

@implementation Tag

+ (NSString *)primaryKey {
    return @"tagId";
}

+ (BOOL)deleteAllTags {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[Tag allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)saveTag:(Tag *)tag{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:tag];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return FALSE;
    }
    
    return YES;
}

+ (BOOL)saveTags:(NSMutableArray *)tags{
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:tags];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return FALSE;
    }
    
    return YES;
}

+ (Tag *)tagWithId:(NSString *)tagId {
    RLMResults<Tag *> *tags = [Tag objectsWhere:[NSString stringWithFormat:@"tagId = '%@'", tagId]];
    
    if(tags.count != 0)
        return [tags objectAtIndex:0];
    else
        return nil;
}

+ (Tag *)tagWithTitle:(NSString *)title {
    RLMResults<Tag *> *tags = [Tag objectsWhere:[NSString stringWithFormat:@"title = '%@'", [title uppercaseString]]];
    
    if(tags.count != 0)
        return [tags objectAtIndex:0];
    else
        return nil;
}

+ (NSMutableArray *)getTagsWithType:(NSString *)type andCollection:(NSString *)collection{
    RLMResults<Tag *> *tags = [Tag objectsWhere:[NSString stringWithFormat:@"type = '%@' AND collection = '%@'",type,collection]];
    
    NSMutableArray *tagsArray = [[NSMutableArray alloc] initWithCapacity:tags.count];
    
    for(Tag *tag in tags){
        [tagsArray addObject:tag];
    }
    
    return tagsArray;
}

+ (NSMutableArray *)getAllTags {
    RLMResults<Tag *> *tags = [Tag allObjects];
    
    NSMutableArray *tagsArray = [[NSMutableArray alloc] initWithCapacity:tags.count];
    
    for(Tag *tag in tags){
        [tagsArray addObject:tag];
    }
    
    return tagsArray;
}

+ (NSDate *)lastUpdateDate{
    RLMResults<Tag *> *tags = [[Tag allObjects] sortedResultsUsingKeyPath:@"updatedAt" ascending:FALSE];
    
    if(tags.count > 0)
        return [[tags objectAtIndex:0] updatedAt];
    else
        return [NSDate dateWithTimeIntervalSince1970:0];
}

- (void)parseFromDictionary:(NSDictionary*)tagDictionary {
    self.tagId = [tagDictionary valueForKey:@"code"];
    self.title = [tagDictionary valueForKey:@"nombre"];
    self.type = [tagDictionary valueForKey:@"zona"];
}

+(BOOL)deselectAllTags {
    
    RLMResults<Tag *> *tags = [Tag allObjects];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    for(Tag *tag in tags){
        [realm transactionWithBlock:^{
            [tag setStatus:@"Off"];
        }];
    }
    
    return TRUE;
}

@end
