//
//  RateInfo.h
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface RateInfo : RLMObject

@property NSString * rlmInfoId;
@property NSString * userName;
@property NSNumber<RLMFloat> * rate;
@property NSString * opinion;

+ (BOOL)deleteAllRateInfos;

@end

RLM_ARRAY_TYPE(RateInfo)
