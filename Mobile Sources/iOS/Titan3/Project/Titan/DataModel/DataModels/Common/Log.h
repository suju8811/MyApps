//
//  Logs.h
//  Titan
//
//  Created by Manuel Manzanera on 27/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface Log : RLMObject

@property NSString *logId;
@property NSDate *createAt;
@property NSDate *updatedAt;
@property NSDate *revisionDate;
@property NSString *owner;
@property NSNumber <RLMFloat> *weight;             //
@property NSNumber <RLMFloat> *percent;            //
@property NSString *picture1;                      //
@property NSString *picture2;                      //
@property NSString *picture3;                      //
@property NSString *picture4;                      //
@property NSString *picture5;                      //
@property NSString *picture6;                      //
@property NSNumber <RLMFloat> *bmr;
@property NSNumber <RLMFloat> *rm;
@property NSNumber  <RLMInt> *igc;                 //igc

@property NSNumber <RLMInt> *age;                   //age
@property NSNumber <RLMInt> *height;                //age
@property NSNumber <RLMBool> *isSelected;

/// Pesos levantados
@property NSNumber <RLMFloat> *squat;              //Sentadillas
@property NSNumber <RLMFloat> *benchPress;         //Press Banca
@property NSNumber <RLMFloat> *militaryPress;      //Press Militar
@property NSNumber <RLMFloat> *deadWeight;         //Peso Muerto
@property NSNumber <RLMFloat> *dominated;          //Dominadas

/// Medidas Corporales
@property NSNumber <RLMInt> *neck;                  //Neck
@property NSNumber <RLMInt> *back;                  //Back
@property NSNumber <RLMInt> *leftBicep;             //Left Bicep
@property NSNumber <RLMInt> *rightBicep;            //Right Bicep
@property NSNumber <RLMInt> *hip;                   //Hip
@property NSNumber <RLMInt> *waist;                 //cintura
@property NSNumber <RLMInt> *leftForearm;           //antebrazo izquierdo
@property NSNumber <RLMInt> *rightForearm;          //antebrazo derecho
@property NSNumber <RLMInt> *leftThigh;             //muslo izquierdo
@property NSNumber <RLMInt> *rightThigh;            //muslo derecho
@property NSNumber <RLMInt> *leftLeg;               //muslo izquierdo
@property NSNumber <RLMInt> *rightLeg;              //muslo derecho

+ (BOOL)deleteAllLogs;
+ (BOOL)saveLogs:(NSMutableArray *)logs;
+ (BOOL)deleteLog:(Log *)log;
+ (Log *)getLogWithId:(NSString *)logId;
+ (Log *)getMaxValueWithKey:(NSString *)key;
+ (Log *)getMinValueWithKey:(NSString *)key;
+ (RLMResults *)getLogsWithProperty:(NSString*)property value:(id)value;
+ (float)getAverageValueWithKey:(NSString *)key;
+ (RLMResults *)getLogs;
+ (Log *)getLastLog;
+ (RLMResults *)getLogsInDate:(NSDate *)date;
+ (RLMResults *)getLogsBetweenDates:(NSDate *)startDate andEndDate:(NSDate *)endDate;

+ (NSDate *)lastUpdateDate;
+ (Log*)duplicateLog:(Log*)log;

- (void)parseFromDictionary:(NSDictionary*)logDictionary;

@end
