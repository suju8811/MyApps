//
//  Tag.h
//  loqiva
//
//  Created by Manuel Manzanera on 2/5/16.
//  Copyright © 2016 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface Tag : RLMObject

@property NSString *tagId;
@property NSString *title;
@property NSString *collection;
@property NSNumber <RLMInt>* order;
@property NSString *owner;
@property NSDate *createdAt;
@property NSDate *updatedAt;
@property NSString *type;                   //// WORKOUT

//Only for selectors
@property NSString *status;

+ (BOOL)deleteAllTags;

+ (Tag *)tagWithId:(NSString *)tagId;
+ (Tag *)tagWithTitle:(NSString *)title;

+ (BOOL)saveTag:(Tag *)tag;
+ (BOOL)saveTags:(NSMutableArray *)tags;

+ (NSMutableArray *)getTagsWithType:(NSString *)type andCollection:(NSString *)collection;

+ (NSMutableArray *)getAllTags;

+ (NSDate *)lastUpdateDate;

- (void)parseFromDictionary:(NSDictionary*)tagDictionary;

+ (BOOL)deselectAllTags;

@end

RLM_ARRAY_TYPE(Tag)
