//
//  Logs.m
//  Titan
//
//  Created by Manuel Manzanera on 27/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Log.h"
#import "AppContext.h"
#import "DateManager.h"
#import "DateTimeUtil.h"

@implementation Log

+ (NSString *)primaryKey {
    return @"logId";
}

+ (BOOL)deleteAllLogs {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[Log allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)saveLogs:(NSMutableArray *)logs{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:logs];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return FALSE;
    }
    
    return YES;
}

+ (BOOL)deleteLog:(Log *)log{
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObject:log];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return FALSE;
    }
    
    return YES;
}

+ (Log *)getLogWithId:(NSString *)logId{
    RLMResults<Log *> *logs = [Log objectsWhere:[NSString stringWithFormat:@"logId = '%@'",logId]];
    
    if(logs.count > 0)
        return [logs objectAtIndex:0];
    else
        return nil;
}

+ (RLMResults *)getLogs {
    RLMResults<Log *> *logs = [Log allObjects];
    RLMResults<Log *> *sortedLogs = [logs sortedResultsUsingKeyPath:@"revisionDate" ascending:NO];
    return sortedLogs;
}

+ (Log *)getMaxValueWithKey:(NSString *)key {

    RLMResults<Log *> *logs = [[Log allObjects]
                               sortedResultsUsingKeyPath:key ascending:NO];
    
    if (logs.count > 0) {
        return [logs objectAtIndex:0];
    }
    else {
        return nil;
    }
}

+ (Log *)getMinValueWithKey:(NSString *)key {
    
    RLMResults<Log *> *logs = [[Log allObjects]
                                     sortedResultsUsingKeyPath:key ascending:YES];
    
    if(logs.count > 0) {
        return [logs objectAtIndex:0];
    }
    else {
        return nil;
    }
}

+ (float)getAverageValueWithKey:(NSString *)key{
    
    RLMResults<Log *> *logs = [[Log allObjects]
                               sortedResultsUsingKeyPath:key ascending:YES];
    
    if(logs.count > 0)
    {
        float account = 0.;
        for(Log *log in logs){
            if([key isEqualToString:@"weight"])
                account = account + log.weight.floatValue;
            else if([key isEqualToString:@"bmr"])
                account = account + log.bmr.floatValue;
            else if([key isEqualToString:@"igc"])
                account = account + log.igc.floatValue;
        }
        
        return account/logs.count;
    }
    else
        return 0.;
}

+ (Log *)getLastLog{
    
    RLMResults<Log *> *logs = [Log allObjects];
    if(logs.count > 0){
        RLMResults<Log *> *sortedLogs = [logs sortedResultsUsingKeyPath:@"createAt" ascending:TRUE];
        return [sortedLogs objectAtIndex:logs.count-1];
    }
    
    return nil;
}

+ (RLMResults *)getLogsInDate:(NSDate *)date{
    
//    NSCalendar *calendar  =[NSCalendar currentCalendar];
//    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:date];
//    NSDate *compareDate = [calendar dateFromComponents:dateComponents];
    
    NSLog(@"%@",[DateManager startOfDay:date]);
    NSLog(@"%@",[DateManager endOfDay:date]);
    
    RLMResults<Log *> *logs = [Log objectsWhere:@"revisionDate >= %@ AND revisionDate <= %@",[DateManager startOfDay:date],[DateManager endOfDay:date]];
    
    if(logs.count > 0)
        NSLog(@"Hay en fecha");
    
    return logs;
}

+ (RLMResults *)getLogsBetweenDates:(NSDate *)startDate andEndDate:(NSDate *)endDate{
    RLMResults<Log *> *logs = [Log objectsWhere:@"revisionDate >= %@ and revisionDate <= %@",startDate,endDate];
    RLMResults<Log *> *sortedLogs = [logs sortedResultsUsingKeyPath:@"revisionDate" ascending:NO];
    return sortedLogs;
}

+ (NSDate *)lastUpdateDate{
    RLMResults<Log *> *logs = [[Log allObjects] sortedResultsUsingKeyPath:@"updatedAt" ascending:FALSE];
    
    if(logs.count > 0)
        return [[logs objectAtIndex:0] updatedAt];
    else
        return [NSDate dateWithTimeIntervalSince1970:0];
}

+ (RLMResults *)getLogsWithProperty:(NSString*)property value:(id)value {
    NSString * predicateString = [NSString stringWithFormat:@"%@ == %@", property, value];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:predicateString];
    RLMResults<Log*>* logs = [Log objectsWithPredicate:predicate];
    return logs;
}

+ (Log*)duplicateLog:(Log*)log {
    Log * duplicatedLog = [[Log alloc] init];
    
    duplicatedLog.logId = log.logId;
    duplicatedLog.createAt = log.createAt;
    duplicatedLog.updatedAt = log.updatedAt;
    duplicatedLog.revisionDate = log.revisionDate;
    duplicatedLog.owner = log.owner;
    duplicatedLog.weight = log.weight;
    duplicatedLog.percent = log.percent;
    duplicatedLog.picture1 = log.picture1;
    duplicatedLog.picture2 = log.picture2;
    duplicatedLog.picture3 = log.picture3;
    duplicatedLog.picture4 = log.picture4;
    duplicatedLog.picture5 = log.picture5;
    duplicatedLog.picture6 = log.picture6;
    duplicatedLog.bmr = log.bmr;
    duplicatedLog.igc = log.igc;
    duplicatedLog.age = log.age;
    duplicatedLog.height = log.height;
    
    //
    //  Pesos levantados
    //
    
    duplicatedLog.squat = log.squat;
    duplicatedLog.benchPress = log.benchPress;
    duplicatedLog.militaryPress = log.militaryPress;
    duplicatedLog.deadWeight = log.deadWeight;
    duplicatedLog.dominated = log.dominated;
    
    //
    //  Medidas Corporales
    //
    
    duplicatedLog.neck = log.neck;
    duplicatedLog.back = log.back;
    duplicatedLog.leftBicep = log.leftBicep;
    duplicatedLog.rightBicep = log.rightBicep;
    duplicatedLog.hip = log.hip;
    duplicatedLog.waist = log.waist;
    duplicatedLog.leftForearm = log.leftForearm;
    duplicatedLog.rightForearm = log.rightForearm;
    duplicatedLog.leftThigh = log.leftThigh;
    duplicatedLog.rightThigh = log.rightThigh;
    duplicatedLog.leftLeg = log.leftLeg;
    duplicatedLog.rightLeg = log.rightLeg;
    
    return duplicatedLog;
}

- (void)parseFromDictionary:(NSDictionary*)logDictionary {
    _logId = [logDictionary valueForKey:@"code"];
    _height = @([[logDictionary valueForKey:@"altura"] intValue]);
    _rightForearm = @([[logDictionary valueForKey:@"antebrazodrc"] intValue]);
    _leftForearm = @([[logDictionary valueForKey:@"antebrazoizq"] intValue]);
    _rightBicep = @([[logDictionary valueForKey:@"bicepdrc"] intValue]);
    _leftBicep = @([[logDictionary valueForKey:@"bicepizq"] intValue]);
    _hip = @([[logDictionary valueForKey:@"cadera"] intValue]);
    _waist = @([[logDictionary valueForKey:@"cintura"] intValue]);
    _rightThigh = @([[logDictionary valueForKey:@"cuadricepsdrc"] intValue]);
    _leftThigh = @([[logDictionary valueForKey:@"cuadricepsizq"] intValue]);
    _neck = @([[logDictionary valueForKey:@"cuello"] intValue]);
    _back = @([[logDictionary valueForKey:@"espalda"] intValue]);
    _revisionDate = [DateTimeUtil dateFromString:[logDictionary valueForKey:@"fecha"]
                                       format:@"yyyy-MM-dd"];
    
    _rightLeg = @([[logDictionary valueForKey:@"gemelodch"] intValue]);
    _leftLeg = @([[logDictionary valueForKey:@"gemeloizq"] intValue]);
    _igc = @([[logDictionary valueForKey:@"igc"] intValue]);
    _weight = @([[logDictionary valueForKey:@"peso"] floatValue]);
    _picture1 = [logDictionary valueForKey:@"photo"];
    _rm = @([[logDictionary valueForKey:@"rm"] floatValue]);
    _bmr = @([[logDictionary valueForKey:@"tmb"] floatValue]);
}

@end
