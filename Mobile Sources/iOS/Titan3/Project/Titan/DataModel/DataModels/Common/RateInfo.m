//
//  RateInfo.m
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "RateInfo.h"
#import "AppContext.h"

@implementation RateInfo

+ (BOOL)deleteAllRateInfos {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[RateInfo allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

@end
