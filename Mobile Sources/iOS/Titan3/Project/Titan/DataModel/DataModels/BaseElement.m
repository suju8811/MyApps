//
//  BaseElement.m
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "BaseElement.h"

@implementation BaseElement

- (void)parseFromDictionary:(NSDictionary*)elementDictionary {
    NSArray<NSDictionary*>* rateInfoDictionaryArray = [elementDictionary valueForKey:@"estrella_lista"];
    
    for (NSDictionary * rateInfoDictionary in rateInfoDictionaryArray) {
        RateInfo * rateInfo = [[RateInfo alloc] init];
        rateInfo.userName = [rateInfoDictionary valueForKey:@"nombre"];
        rateInfo.rate = @([[rateInfoDictionary valueForKey:@"numero"] floatValue]);
        rateInfo.opinion = [rateInfoDictionary valueForKey:@"opinion"];
        [self.rateInfos addObject:rateInfo];
    }
}

@end
