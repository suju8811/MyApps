//
//  Serie.h
//  Titan
//
//  Created by Manuel Manzanera on 7/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "Exercise.h"

@interface Serie : RLMObject

@property NSString *serieId;
@property NSString *serverSerieId;
@property NSString * title;
@property NSDate *createdAt;
@property NSDate *updatedAt;
@property NSString *owner;

@property Exercise *exercise;
@property Exercise *secondExercise;

/*
 ANAERÓBICO
 -REPS
    Reps
    Kg
    Tiempo de Descanso
 -TIME
    Tiempo de ejecución
    Kg
    Tiempo de Descanso
 AERÓBICO
    Tiempo
    Intensidad
    Tiempo de Descanso
 */

@property NSNumber <RLMFloat> *intensityFactor;           //intensidad
@property NSNumber <RLMInt> *reps;                        //repeticiones
@property NSNumber <RLMInt> *rest;                        //tiempo de descanso de la serie
@property NSNumber <RLMFloat> *weight;                    //peso
@property NSNumber <RLMInt> *execTime;                    //Tiempo de Ejecución

@property NSNumber <RLMInt> *order;
@property NSNumber<RLMBool> *isSubserie;
@property NSNumber <RLMBool> *isSuperSerie;
@property NSNumber <RLMInt> * executingStatus;

+ (BOOL)deleteAllSeries;
+ (BOOL)addSerie:(Serie *)serie;
+ (BOOL)deleteSerie:(Serie *)serie;
+ (BOOL)deleteSerieWithId:(NSString *)serieId;
    
+ (NSInteger)kCalInSerie:(Serie *)serie;
+ (BOOL)isEmptySerie:(Serie *)serie;
+ (BOOL)isEmptyRepsSerie:(Serie *)serie;
    
@end

RLM_ARRAY_TYPE(Serie)
