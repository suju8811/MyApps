//
//  Muscle.h
//  Titan
//
//  Created by Manuel Manzanera on 14/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface Muscle : RLMObject

@property NSString *muscleId;
@property NSString *title;
@property NSString *content;
@property NSDate *createdAt;
@property NSString *owner;

+ (BOOL)saveMuscles:(NSMutableArray *)muscles;
+ (Muscle *)getMuscleWithId:(NSString *)muscleId;
+ (RLMResults *)getMuscles;

@end
