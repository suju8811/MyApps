//
//  ValueConstant.h
//  Titan
//
//  Created by Manuel Manzanera on 24/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface ValueConstant : RLMObject

@property NSNumber <RLMFloat> *value;
@property NSString *valueConstantId;
@property NSString *name;
@property NSString *owner;

@property NSDate *createdAt;
@property NSDate *updatedAt;

+ (ValueConstant *)getValueConstantsWithName:(NSString *)name;
+ (BOOL)saveConstants:(NSMutableArray *)constants;
+ (NSDate *)lastUpdateDate;

@end
