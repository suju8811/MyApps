//
//  Event.m
//  Titan
//
//  Created by Marcus Lee on 15/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Event.h"
#import "AppContext.h"
#import "UtilManager.h"
#import "DateTimeUtil.h"

@implementation Event

+ (NSString *)primaryKey{
    return @"eventId";
}

+ (BOOL)deleteAllEvents {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[Event allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (NSArray<Event*>*)getLatestEvents:(NSInteger)count {
    
    NSDate * today = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:[NSDate date] options:0];
    
    RLMResults<Event *> *events = [[Event objectsWhere:@"selectedDate != %@", today] sortedResultsUsingKeyPath:@"selectedDate" ascending:NO];
    
    NSMutableArray<Event*> * eventsArray = [[NSMutableArray alloc] init];
    NSInteger realCount = MIN(count, events.count);
    for (NSInteger index = 0; index < realCount; index ++) {
        
        if ([today compare:events[index].selectedDate] != NSOrderedSame) {
            [eventsArray addObject:events[index]];
        }
    }
    
    return eventsArray;
}

+ (Event*)getEventWithScheduleDate:(NSDate *)date {
    NSDate * dateWithoutTime = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateBySettingHour:0 minute:0 second:0 ofDate:date options:0];
    
    RLMResults<Event *> *events = [Event objectsWhere:@"selectedDate == %@", dateWithoutTime];

    if(events != nil && events.count > 0) {
        return events.firstObject;
    }

    return nil;
}

+ (BOOL)saveEvent:(Event *)event {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:event];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)saveEvents:(NSMutableArray *)events {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:events];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (Event*)duplicateEvent:(Event*)event {
    Event * duplicatedEvent = [[Event alloc] init];
    
    duplicatedEvent.eventId = [UtilManager uuid];
    duplicatedEvent.title = event.title;
    duplicatedEvent.type = event.type;
    duplicatedEvent.note = event.note;
    duplicatedEvent.startDate = event.startDate;
    duplicatedEvent.selectedDate = event.selectedDate;
    duplicatedEvent.repeat = event.repeat;
    duplicatedEvent.days = event.days;
    
    return duplicatedEvent;
}

- (void)parseFromDictionary:(NSDictionary*)eventDictionary {
    _eventId = [eventDictionary valueForKey:@"113"];
    _title = [eventDictionary valueForKey:@"titulo"];
    _note = [eventDictionary valueForKey:@"notas"];
    _type = @([[eventDictionary valueForKey:@"tipo"] intValue]);
    _selectedDate = [DateTimeUtil dateFromString:[eventDictionary valueForKey:@"fecha"] format:@"yyyy-MM-dd"];
    _startDate = [DateTimeUtil dateFromString:[eventDictionary valueForKey:@"hora"] format:@"HH:mm:ss"];
    _repeat = @([[eventDictionary valueForKey:@"repetir"] intValue]);
}

@end

