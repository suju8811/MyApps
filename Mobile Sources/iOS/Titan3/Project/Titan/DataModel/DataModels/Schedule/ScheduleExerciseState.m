//
//  ScheduleExerciseState.m
//  Titan
//
//  Created by Marcus Lee on 17/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "ScheduleExerciseState.h"

@implementation ScheduleExerciseState

+ (NSString *)primaryKey{
    return @"trainingExerciseId";
}

@end
