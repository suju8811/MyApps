//
//  Event.h
//  Titan
//
//  Created by Marcus Lee on 15/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Realm/Realm.h>

@interface Event : RLMObject

@property NSString * eventId;
@property NSString * title;
@property NSNumber <RLMInt> *type;
@property NSString * note;
@property NSDate * startDate;
@property NSDate * selectedDate;
@property NSNumber <RLMInt> *repeat;
@property NSNumber <RLMInt> *days;

+ (BOOL)deleteAllEvents;

+ (Event*)getEventWithScheduleDate:(NSDate *)date;

+ (NSArray<Event*>*)getLatestEvents:(NSInteger)count;

+ (BOOL)saveEvent:(Event *)event;

+ (BOOL)saveEvents:(NSMutableArray *)events;

+ (Event*)duplicateEvent:(Event*)event;

- (void)parseFromDictionary:(NSDictionary*)eventDictionary;

@end
