//
//  SchedulePlan.m
//  Titan
//
//  Created by Marcus Lee on 14/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "SchedulePlan.h"
#import "AppContext.h"

@implementation SchedulePlan

+ (NSString *)primaryKey{
    return @"schedulePlanId";
}

+ (BOOL)deleteAllSchedulePlans {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[SchedulePlan allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)saveSchedulePlans:(NSMutableArray *)plans {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:plans];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)deleteSchedulePlan:(SchedulePlan *)schedulePlan {
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObject:schedulePlan];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    return YES;
}

//+ (SchedulePlan *)getSchedulePlanWithScheduleDate:(NSDate *)date {
//    RLMResults<SchedulePlan *> *schedulePlans = [SchedulePlan objectsWhere:@"scheduleDate == %@", date];
//
//    if(schedulePlans != nil && schedulePlans.count > 0) {
//        return schedulePlans.firstObject;
//    }
//    
//    return nil;
//}

+ (NSArray *)getScheduleTrainingPlanBetweenDate:(NSDate *)fromDate
                                             andDate:(NSDate*)toDate {

    NSMutableArray * resultPlans = [[NSMutableArray alloc] init];
    RLMResults<SchedulePlan *> *unsortedSchedulePlans = [SchedulePlan allObjects];
    RLMResults<SchedulePlan *> *schedulePlans = [unsortedSchedulePlans sortedResultsUsingKeyPath:@"scheduleDate" ascending:NO];
    
    for (SchedulePlan * schedulePlan in schedulePlans) {
        TrainingPlan * trainingPlan = schedulePlan.plan;
        if (trainingPlan == nil) {
            continue;
        }
        
        NSDate * startDate = schedulePlan.scheduleDate;
//        NSDate * endDate = startDate;
//        if (trainingPlan.trainingDay.count > 1) {
//            endDate = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
//                                                               value:trainingPlan.trainingDay.count - 1
//                                                              toDate:startDate
//                                                             options:0];
//        }
        
        if (fromDate >= startDate &&
            toDate <= startDate) {
            
            [resultPlans addObject:schedulePlan];
        }
    }
    
    return resultPlans;
}

+ (SchedulePlan *)getScheduleTrainingPlanWithScheduleDate:(NSDate *)date {
    RLMResults<SchedulePlan *> *schedulePlans = [SchedulePlan allObjects];
    for (SchedulePlan * schedulePlan in schedulePlans) {
        TrainingPlan * trainingPlan = schedulePlan.plan;
        if (trainingPlan == nil) {
            continue;
        }
            
        NSDate * startDate = schedulePlan.scheduleDate;
        NSDate * endDate = startDate;
        if (trainingPlan.trainingDay.count > 1) {
            endDate = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                                        value:trainingPlan.trainingDay.count - 1
                                                                       toDate:startDate
                                                                      options:0];
        }
        
        if (date >= startDate &&
            date <= endDate) {
            
            return schedulePlan;
        }
    }
    
    return nil;
}

+ (TrainingPlan*)getReposeWithScheduleDate:(NSDate *)date {
    RLMResults<TrainingPlan *> *plans = [TrainingPlan objectsWhere:@"(startDate <= %@) AND (endDate >= %@) AND isRepose = TRUE", date, date];
//    NSPredicate *aerobicPredicate = [NSPredicate predicateWithFormat:@"startDate <= %@ AND endDate >= %@ AND isRepose = TRUE", date, date];
//    RLMResults<TrainingPlan *> *plans = [TrainingPlan objectsWithPredicate:aerobicPredicate];
    
    if (plans != nil && plans.count > 0) {
        return plans.firstObject;
    }
    
    return nil;
}

+ (SchedulePlan *)getScheduleNutritionPlanWithScheduleDate:(NSDate *)date {
    RLMResults<SchedulePlan *> *schedulePlans = [SchedulePlan allObjects];
    for (SchedulePlan * schedulePlan in schedulePlans) {
        NutritionPlan * nutritionPlan = schedulePlan.nutritionPlan;
        if (nutritionPlan == nil) {
            continue;
        }
        
        NSDate * startDate = schedulePlan.scheduleDate;
        NSDate * endDate = startDate;
        if (nutritionPlan.nutritionDays.count > 1) {
            endDate = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                               value:nutritionPlan.nutritionDays.count - 1
                                                              toDate:startDate
                                                             options:0];
        }
        
        if (date >= startDate &&
            date <= endDate) {
            
            return schedulePlan;
        }
    }
    
    return nil;
}

+ (NSArray *)getScheduleTrainingPlans {
    NSMutableArray * resultPlans = [[NSMutableArray alloc] init];
    RLMResults<SchedulePlan *> *unsortedSchedulePlans = [SchedulePlan allObjects];
    RLMResults<SchedulePlan *> *schedulePlans = [unsortedSchedulePlans sortedResultsUsingKeyPath:@"scheduleDate" ascending:NO];
    
    for (SchedulePlan * schedulePlan in schedulePlans) {
        TrainingPlan * trainingPlan = schedulePlan.plan;
        if (trainingPlan == nil) {
            continue;
        }
        
        [resultPlans addObject:schedulePlan];
    }
    
    return resultPlans;
}

+ (NSArray *)getScheduleNutritionPlans {
    NSMutableArray * resultPlans = [[NSMutableArray alloc] init];
    RLMResults<SchedulePlan *> *unsortedSchedulePlans = [SchedulePlan allObjects];
    RLMResults<SchedulePlan *> *schedulePlans = [unsortedSchedulePlans sortedResultsUsingKeyPath:@"scheduleDate" ascending:NO];
    
    for (SchedulePlan * schedulePlan in schedulePlans) {
        NutritionPlan * nutritionPlan = schedulePlan.nutritionPlan;
        if (nutritionPlan == nil) {
            continue;
        }
        
        [resultPlans addObject:schedulePlan];
    }
    
    return resultPlans;
}

+ (NSArray *)getScheduleNutritionPlanBetweenDate:(NSDate *)fromDate
                                         andDate:(NSDate*)toDate {
    
    NSMutableArray * resultPlans = [[NSMutableArray alloc] init];
    RLMResults<SchedulePlan *> *unsortedSchedulePlans = [SchedulePlan allObjects];
    RLMResults<SchedulePlan *> *schedulePlans = [unsortedSchedulePlans sortedResultsUsingKeyPath:@"scheduleDate" ascending:NO];
    for (SchedulePlan * schedulePlan in schedulePlans) {
        NutritionPlan * nutritionPlan = schedulePlan.nutritionPlan;
        if (nutritionPlan == nil) {
            continue;
        }
        
        NSDate * startDate = schedulePlan.scheduleDate;
        NSDate * endDate = startDate;
        if (nutritionPlan.nutritionDays.count > 1) {
            endDate = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                               value:nutritionPlan.nutritionDays.count - 1
                                                              toDate:startDate
                                                             options:0];
        }
        
        if (fromDate >= startDate &&
            toDate <= startDate) {
            
            [resultPlans addObject:schedulePlan];
        }
    }
    
    return resultPlans;
}

+ (SchedulePlan *)getMaxTrainingScheduleWithKey:(NSString *)key {
    RLMResults<SchedulePlan *> * sortedSchedulePlans = [[SchedulePlan allObjects] sortedResultsUsingKeyPath:key ascending:NO];
    NSMutableArray * resultPlans = [[NSMutableArray alloc] init];
    for (SchedulePlan * schedulePlan in sortedSchedulePlans) {
        TrainingPlan * trainingPlan = schedulePlan.plan;
        if (trainingPlan) {
            [resultPlans addObject:schedulePlan];
        }
    }
    
    if (resultPlans.count > 0) {
        return [resultPlans firstObject];
    }
    else {
        return nil;
    }
}

+ (SchedulePlan *)getMinTrainingScheduleWithKey:(NSString *)key {
    RLMResults<SchedulePlan *> * sortedSchedulePlans = [[SchedulePlan allObjects] sortedResultsUsingKeyPath:key ascending:YES];
    NSMutableArray * resultPlans = [[NSMutableArray alloc] init];
    for (SchedulePlan * schedulePlan in sortedSchedulePlans) {
        TrainingPlan * trainingPlan = schedulePlan.plan;
        if (trainingPlan) {
            [resultPlans addObject:schedulePlan];
        }
    }
    
    if (resultPlans.count > 0) {
        return [resultPlans firstObject];
    }
    else {
        return nil;
    }
}

@end
