//
//  SchedulePlan.h
//  Titan
//
//  Created by Marcus Lee on 14/11/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "TrainingPlan.h"
#import "NutritionPlan.h"

@interface SchedulePlan : RLMObject

@property NSString * schedulePlanId;

@property NSDate *scheduleDate;

@property TrainingPlan *plan;

@property NutritionPlan * nutritionPlan;

@property NSNumber <RLMBool> *isSelected;

+ (BOOL)deleteAllSchedulePlans;

+ (BOOL)saveSchedulePlans:(NSMutableArray *)plans;

+ (BOOL)deleteSchedulePlan:(SchedulePlan *)schedulePlan;

+ (NSArray *)getScheduleTrainingPlans;
+ (SchedulePlan *)getScheduleTrainingPlanWithScheduleDate:(NSDate *)date;
+ (NSArray *)getScheduleTrainingPlanBetweenDate:(NSDate *)fromDate
                                        andDate:(NSDate*)toDate;

+ (NSArray *)getScheduleNutritionPlans;
+ (SchedulePlan *)getScheduleNutritionPlanWithScheduleDate:(NSDate *)date;
+ (NSArray *)getScheduleNutritionPlanBetweenDate:(NSDate *)fromDate
                                         andDate:(NSDate*)toDate;

+ (TrainingPlan*)getReposeWithScheduleDate:(NSDate *)date;

+ (SchedulePlan *)getMaxTrainingScheduleWithKey:(NSString *)key;

+ (SchedulePlan *)getMinTrainingScheduleWithKey:(NSString *)key;

@end
