//
//  ScheduleExerciseState.h
//  Titan
//
//  Created by Marcus Lee on 17/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import <Realm/Realm.h>

@interface ScheduleExerciseState : RLMObject

@property NSString * scheduleId;

@property NSString * trainingExerciseId;

@property NSNumber <RLMBool> * done;

@end
