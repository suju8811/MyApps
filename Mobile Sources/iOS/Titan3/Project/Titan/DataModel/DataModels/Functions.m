//
//  Functions.m
//  Titan
//
//  Created by Manuel Manzanera on 25/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Functions.h"

@implementation Functions

+ (NSNumber *)getFuser:(NSInteger)weigth andGender:(NSString *)gender andBMR:(float)bmr{
    
    if(weigth == 0)
        return [NSNumber numberWithFloat:2.006];
    else if(!gender || gender.length == 0)
        return [NSNumber numberWithFloat:2.006];
    else{
        if([gender isEqualToString:@"male"]){
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"fUserMen" ofType:@"json"];
            NSData *data = [NSData dataWithContentsOfFile:filePath];
            NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            
            for(NSDictionary *valueDictionay in jsonArray){
                NSNumber *minW = [valueDictionay valueForKey:@"minW"];
                NSNumber *maxW = [valueDictionay valueForKey:@"maxW"];
                NSNumber *minIGC = [valueDictionay valueForKey:@"minIGC"];
                NSNumber *maxIGC = [valueDictionay valueForKey:@"maxIGC"];
                
                if(weigth <= maxW.integerValue && weigth >= minW.integerValue && bmr <= maxIGC.floatValue && bmr >= minIGC.floatValue){
                    return [valueDictionay valueForKey:@"value"];
                }
            }
            
        
        }else{
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"fUserWomen" ofType:@"json"];
            NSData *data = [NSData dataWithContentsOfFile:filePath];
            NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            
            for(NSDictionary *valueDictionay in jsonArray){
                NSNumber *minW = [valueDictionay valueForKey:@"minW"];
                NSNumber *maxW = [valueDictionay valueForKey:@"maxW"];
                NSNumber *minIGC = [valueDictionay valueForKey:@"minIGC"];
                NSNumber *maxIGC = [valueDictionay valueForKey:@"maxIGC"];
                
                if(weigth <= maxW.integerValue && weigth >= minW.integerValue && bmr <= maxIGC.floatValue && bmr >= minIGC.floatValue){
                    return [valueDictionay valueForKey:@"value"];
                }
            }
        }
    }
    
    return [NSNumber numberWithInt:0];
}

+ (NSNumber *)getFReps:(NSInteger)reps{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"fReps" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    for(NSDictionary *valueDictionay in jsonArray){
        NSNumber *valueRep = [valueDictionay valueForKey:@"minR"];
        
        if(valueRep.integerValue == reps){
            return [valueDictionay valueForKey:@"value"];
        }
    
    }
    
    return [NSNumber numberWithFloat:1];
}

+ (NSNumber *)getFWeightWithDifficulty:(NSInteger)difficulty andDec:(float)dec{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"fWeight" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    for(NSDictionary *valueDictionay in jsonArray){
        
        NSNumber *difficultyDict = [valueDictionay valueForKey:@"difficulty"];
        if(difficultyDict.integerValue == difficulty){
            NSNumber *min = [valueDictionay valueForKey:@"minW"];
            NSNumber *max = [valueDictionay valueForKey:@"maxW"];
            
            if(dec >= min.floatValue && dec<= max.floatValue){
                return [valueDictionay valueForKey:@"value"];
            }
        }
    }
    
    return [NSNumber numberWithFloat:1];
}

+ (NSNumber *)getFRestInSeconds:(NSInteger)seconds{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"fRest" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    for(NSDictionary *valueDictionay in jsonArray){
        NSNumber *min = [valueDictionay valueForKey:@"minR"];
        NSNumber *max = [valueDictionay valueForKey:@"maxR"];
            
        if(seconds >= min.integerValue && seconds<= max.integerValue){
            return [valueDictionay valueForKey:@"value"];
        }
        
    }
    
    return [NSNumber numberWithFloat:1];
}

@end
