//
//  Serie.m
//  Titan
//
//  Created by Manuel Manzanera on 7/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Serie.h"
#import "AppContext.h"
#import "User.h"
#import "Exercise.h"
#import "Functions.h"

@implementation Serie

+(NSString *)primaryKey{
    return @"serieId";
}

+ (BOOL)deleteAllSeries {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[Exercise allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (NSInteger)kCalInSerie:(Serie *)serie{
    
    Exercise *serieExercise = serie.exercise;
    
    float kCal = 0;
    float userWeight = 60;
    NSInteger userAge = 20;

    if([[[AppContext sharedInstance] currentUser] weight])
        userWeight = [[[AppContext sharedInstance] currentUser] weight].floatValue;
    if([[[AppContext sharedInstance] currentUser] age])
        userAge = [[[AppContext sharedInstance] currentUser] age].integerValue;
    
    if([serieExercise.energyType isEqualToString:@"reps"]){
        kCal = serie.reps.floatValue * serieExercise.energy.floatValue * [Functions getFReps:serie.reps.integerValue].floatValue * [Functions getFRestInSeconds:serie.rest.integerValue].floatValue * [Functions getFWeightWithDifficulty:serieExercise.weightDifficulty.integerValue andDec:(serie.weight.floatValue/userWeight)].floatValue;
    
    }else if ([serieExercise.type isEqualToString:@"aerobic"]){
        if([[[[AppContext sharedInstance] currentUser] gender] isEqualToString:@"male"])
            kCal = 0.2017 * userAge/4.184 + 0.1988*userWeight +(0.6309 * ((220 - userAge)/100) * serie.intensityFactor.floatValue)* serie.execTime.integerValue/60 - 55.0969;
        else
            kCal = 0.074 * userAge/4.184 + 0.1263*userWeight +(0.4472 * ((220 - userAge)/100) * serie.intensityFactor.floatValue) * serie.execTime.integerValue/60 - 20.4022;
        
    }else{
        kCal = serie.execTime.integerValue * serieExercise.energy.floatValue * [Functions getFRestInSeconds:serie.rest.integerValue].floatValue; // *FWeight
    }
    
    if(kCal < 0)
        kCal = 0;
    
    return kCal;
}
    
+ (BOOL)addSerie:(Serie *)serie{
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:serie];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error)
        NSLog(@"%@",error.description);
    
    return TRUE;
}
    
+ (BOOL)deleteSerie:(Serie *)serie {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObject:serie];
    
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}
    
+ (BOOL)deleteSerieWithId:(NSString *)serieId{
    RLMResults<Serie *> *series = [Serie objectsWhere:@"serieId = %@",serieId];
    
    if (series.count > 0){
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm beginWriteTransaction];
        [realm deleteObject:[series objectAtIndex:0]];
        
        NSError *error;
        [realm commitWriteTransaction:&error];
        
        if(error){
            NSLog(@"%@",error.description);
            return FALSE;
        }
    }
    
    return TRUE;
}

+ (BOOL)isEmptySerie:(Serie *)serie{
    
    if(serie.reps.intValue == 0 && serie.rest.intValue == 0 && serie.weight.intValue == 0)
        return true;
    
    return false;
}

+ (BOOL)isEmptyRepsSerie:(Serie *)serie{
    if(serie){
        if(serie.reps.intValue == 0 && serie.execTime.intValue == 0)
            return true;
    }
    
    return false;
}

@end
