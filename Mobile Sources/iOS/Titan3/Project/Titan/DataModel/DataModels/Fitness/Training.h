//
//  Training.h
//  Titan
//
//  Created by Manuel Manzanera on 20/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseElement.h"
#import "Exercise.h"
#import "TrainingExercise.h"
#import "Tag.h"

@interface Training : BaseElement

@property NSString *trainingId;
@property NSString *serverTrainingId;
@property NSString *content;
@property NSString *notes;
@property NSDate *createdAt;
@property NSDate *updatedAt;
@property NSString *owner;
@property NSString *title;
@property NSString *desc;
@property NSString *annotation;
@property NSString *punctation;
@property NSNumber<RLMInt> *totalEnergy;
@property NSNumber<RLMInt> *duration;
@property NSString *privacity;

@property RLMArray <Tag *><Tag> *tags;
@property RLMArray <Exercise *><Exercise> *exercises;
@property RLMArray <TrainingExercise *><TrainingExercise> *trainingExercises;
@property NSNumber <RLMInt> *trainingExercisesCount;

@property NSNumber <RLMBool> *isFavourite;                              // Property to asign if is favourite
@property NSNumber <RLMBool> *isShared;                               // Property to asign if it is shared

/////Property Only for Training Selector Table
@property NSNumber <RLMBool> *isSelected;
@property NSNumber <RLMBool> *isCopy;
@property NSNumber <RLMBool> *isCalendarCopy;

@property NSString *trainingPlanId;

+ (BOOL)deleteAllTraining;
+ (Training *)trainingWithId:(NSString *)training;
+ (Training *)trainingWithServerId:(NSString *)training;
+ (BOOL)saveTrainings:(NSMutableArray *)trainings;
+ (BOOL)updateTraining:(Training *)updateTraining;
+ (BOOL)deleteTraining:(Training *)deleteTraining;
+ (BOOL)deleteTrainingExercise:(TrainingExercise *)deleteTrainingExercise inTraining:(Training *)training;
+ (BOOL)deleteSuperSerie:(TrainingExercise *)superSerieTrainingExercise inTraining:(Training *)training;
+ (BOOL)canDeleteTraining:(Training *)training;

+ (RLMResults *)getTrainings;
+ (RLMResults *)getTrainingsWithKeyword:(NSString*)keyword;
+ (RLMResults *)getTrainingsWithOwner:(NSString *)owner;
+ (RLMResults *)getMyTrainings;
+ (RLMResults *)getMyTrainingsWithKeyword:(NSString*)keyword;
+ (RLMResults *)getOtherTrainings;
+ (RLMResults *)getOtherTrainingsWithKeyword:(NSString*)keyword;
+ (RLMResults *)getFavouritesTrainings;
+ (RLMResults *)getFavouritesTrainingsWithKeyword:(NSString*)keyword;
+ (RLMResults *)getSharedTrainings;
+ (RLMResults *)getSharedTrainingsWithKeyword:(NSString*)keyword;

+ (int)getMinutes:(Training *)training;
+ (NSDate *)lastUpdateDate;

+ (BOOL)deselectAllExercises;
+ (Training *)copyTraining:(Training *)training;

+ (NSMutableArray *)getTrainingsExercisesTags:(Training *)training;

- (void)parseFromDictionary:(NSDictionary*)trainingDictionary;

@end
