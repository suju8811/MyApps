//
//  TrainingPlan.h
//  Titan
//
//  Created by Manuel Manzanera on 2/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrainingDay.h"
#import "BaseElement.h"

@interface TrainingPlan : BaseElement

@property NSString *trainingPlanId;
@property NSString *title;
@property NSString *desc;
@property NSString *serverTrainingPlanId;
@property NSDate *createdAt;
@property NSDate *updatedAt;

@property NSDate *startDate;
@property NSDate *endDate;

@property NSString *owner;
@property NSString *content;
@property NSString *notes;

@property NSString *privacity;

@property RLMArray <Tag *><Tag> *tags;
    
@property RLMArray <TrainingDay *><TrainingDay> *trainingDay;
@property NSNumber <RLMBool> *isFavourite;                              // Property to asign if is favourite
@property NSNumber <RLMBool> *isShared;                                 // Property to asign if is shared

@property NSNumber <RLMBool> *isCopy;
@property NSNumber <RLMBool> *isCalendarCopy;

@property NSNumber <RLMBool> *isRepose;

@property BOOL isSelected;

+ (BOOL)deleteAllTrainingPlans;

+ (BOOL)saveTrainingPlans:(NSMutableArray *)plans;

+ (RLMResults *)getTrainingPlans;
+ (RLMResults *)getReposes;
+ (RLMResults *)getTrainingPlansWithKeyword:(NSString*)keyword;
+ (RLMResults *)getMyTrainingPlans;
+ (RLMResults *)getMyTrainingPlansWithKeyword:(NSString*)keyword;
+ (RLMResults *)getOtherTrainingPlans;
+ (RLMResults *)getOtherTrainingPlansWithKeyword:(NSString*)keyword;
+ (RLMResults *)getFavouritesTrainnigPlans;
+ (RLMResults *)getFavouritesTrainnigPlansWithKeyword: (NSString*)keyword;
+ (RLMResults *)getSharedTrainnigPlans;
+ (RLMResults *)getSharedTrainnigPlansWithKeyword:(NSString*)keyword;

+ (TrainingPlan *)getTrainingPlanWithId:(NSString *)trainingPlanId;
+ (TrainingPlan *)getTrainingPlanWithServerId:(NSString *)trainingPlanServerId;
+ (RLMResults *)getTrainingPlansWithOwner:(NSString *)owner;
+ (RLMResults *)getTrainingPlansInDate:(NSDate *)date;

+ (NSMutableArray *)getTrainingsTags:(TrainingPlan *)trainingPlan;

+ (BOOL)deleteInfoInPlan:(TrainingPlan *)trainingPlan;
+ (BOOL)deleteTrainingPlan:(TrainingPlan *)trainingPlan;
+ (BOOL)deleteTrainingDayContent:(TrainingDayContent*)trainingDayContent inTrainingDay:(TrainingDay *)trainingDay;
+ (BOOL)deleteTrainingDay:(TrainingDay *)trainingDay inTrainingPlan:(TrainingPlan *)trainingPlan;
+ (BOOL)canDeleteTrainingPlan:(TrainingPlan *)trainingPlan;

+ (NSDictionary *)trainingDictionaryFromTrainingPlan:(TrainingPlan *)trainingPlan;

+ (NSDate *)lastUpdateDate;
+ (TrainingPlan *)copyTrainingPlan:(TrainingPlan *)duplicateTrainingPlan;

+ (int)numberOfTrainings:(TrainingPlan *)trainingPlan;

+ (NSInteger)getTrainingPlankCal:(TrainingPlan *)trainingPlan;

- (void)parseFromDictionary: (NSDictionary*)trainingPlanDictionary;
- (void)parseFromDictionary: (NSDictionary*)trainingPlanDictionary isSchedule:(BOOL)isSchedule;

@end
