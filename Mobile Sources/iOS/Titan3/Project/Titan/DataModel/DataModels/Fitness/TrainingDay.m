//
//  TrainingDay.m
//  Titan
//
//  Created by Manuel Manzanera on 7/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingDay.h"
#import "TrainingDayContent.h"
#import "Training.h"
#import "AppContext.h"

@implementation TrainingDay

+ (BOOL)deleteAllTrainingDays {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[TrainingDay allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)deleteTrainingDay:(TrainingDay *)trainingDay {
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObject:trainingDay];
    
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if (error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (TrainingDay *)copyTrainingDay:(TrainingDay *)trainingDay{
    TrainingDay *copyTrainingDay = [[TrainingDay alloc] init];
    
    [copyTrainingDay setRestDay:trainingDay.restDay];
    
    if(copyTrainingDay.restDay.intValue != 1){
        
        for(TrainingDayContent *trainingDaycontent in trainingDay.trainingDayContent){
            
            TrainingDayContent *copyTrainingDayContent = [TrainingDayContent copyTrainingDayContent:trainingDaycontent];
            [copyTrainingDay.trainingDayContent addObject:copyTrainingDayContent];
        }
    }
    
    return copyTrainingDay;
}

+ (NSMutableArray *)copyTrainingDay:(TrainingDay *)trainingDay numberOfTime:(NSInteger)numberOfReplies{
    
    NSMutableArray *replies = [[NSMutableArray alloc] initWithCapacity:numberOfReplies];
    
    for(int i = 0; i<numberOfReplies ; i++){
        TrainingDay *copyTrainingDay = [[TrainingDay alloc] init];
        
        [copyTrainingDay setRestDay:trainingDay.restDay];
        
        if(copyTrainingDay.restDay.intValue != 1){
            
            for(TrainingDayContent *trainingDaycontent in trainingDay.trainingDayContent){
                
                TrainingDayContent *copyTrainingDayContent = [TrainingDayContent copyTrainingDayContent:trainingDaycontent];
                [copyTrainingDay.trainingDayContent addObject:copyTrainingDayContent];
            }
        }
        
        [replies addObject:copyTrainingDay];
    }
    
    return replies;
}

@end

