//
//  TrainingDayContent.h
//  Titan
//
//  Created by Manuel Manzanera on 7/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "Training.h"

@interface TrainingDayContent : RLMObject

@property NSString *code;
@property NSString *start;
@property NSString *end;
@property Training *training;
@property NSString *trainingId;
@property NSString *originId;
@property NSNumber <RLMInt> *order;
@property NSNumber <RLMBool> *isClone;

+ (BOOL)deleteAllTrainingDayContents;
+ (TrainingDayContent *)copyTrainingDayContent:(TrainingDayContent *)trainingDayContent;
+ (BOOL)deleteTrainingDayContent:(TrainingDayContent *)trainingDayContent;

@end

RLM_ARRAY_TYPE(TrainingDayContent)
