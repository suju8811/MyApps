//
//  Exercise.m
//  Titan
//
//  Created by Manuel Manzanera on 20/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Exercise.h"
#import "AppContext.h"
#import "UtilManager.h"

@implementation Exercise

+ (NSString *)primaryKey {
    return @"exerciseId";
}

+ (BOOL)deleteAllExercises {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[Exercise allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)saveExercises:(NSMutableArray *)exercises{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:exercises];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return FALSE;
    }
    
    return YES;
}

+ (Exercise *)exerciseWithId:(NSString *)exerciseId{
    RLMResults<Exercise *> *exercises = [Exercise objectsWhere:[NSString stringWithFormat:@"exerciseId = '%@'",exerciseId]];
    
    if(exercises.count != 0) {
        return [exercises objectAtIndex:0];
    }
    else {
        return nil;
    }
}

+ (RLMResults *)getExercises{
    RLMResults<Exercise *> *exercises = [Exercise allObjects];
    RLMResults<Exercise *> *sortedExercise = [exercises sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedExercise;
}

+ (RLMResults *)getAerobicsExercises {
    RLMResults<Exercise *> *exercises = [Exercise allObjects];
    
    NSPredicate *aerobicPredicate = [NSPredicate predicateWithFormat:@"isPropertyOfTraining != TRUE AND (type = 'AEROBICO' OR type == 'aerobico')"];
    exercises = [Exercise objectsWithPredicate:aerobicPredicate];
    RLMResults<Exercise *> *sortedExercise = [exercises sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedExercise;
}

+ (RLMResults *)getAerobicsExercisesWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Exercise getAerobicsExercises];
    }
    
    RLMResults<Exercise *> *exercises = [Exercise allObjects];
    
    NSString * predicateString = [NSString stringWithFormat:@"isPropertyOfTraining != TRUE AND (type = 'AEROBICO' OR type == 'aerobico') AND (title LIKE '*%@*' OR content LIKE '*%@*' OR note LIKE '*%@*')", keyword, keyword, keyword];
    NSPredicate * aerobicPredicate = [NSPredicate predicateWithFormat:predicateString];
    exercises = [Exercise objectsWithPredicate:aerobicPredicate];
    RLMResults<Exercise *> *sortedExercise = [exercises sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedExercise;
}

+ (RLMResults *)getAnaerobicExercises {
    RLMResults<Exercise *> *exercises = [Exercise allObjects];
    
    NSPredicate *aerobicPredicate = [NSPredicate predicateWithFormat:@"isPropertyOfTraining != TRUE AND (type = 'ANAEROBICO' OR type == 'anaerobico')"];
    exercises = [Exercise objectsWithPredicate:aerobicPredicate];
    RLMResults<Exercise *> *sortedExercise = [exercises sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedExercise;
}

+ (RLMResults *)getAnaerobicExercisesWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Exercise getAnaerobicExercises];
    }
    
    RLMResults<Exercise *> *exercises = [Exercise allObjects];
    
    NSString * predicateString = [NSString stringWithFormat:@"isPropertyOfTraining != TRUE AND (type = 'ANAEROBICO' OR type == 'anaerobico') AND (title LIKE '*%@*' OR content LIKE '*%@*' OR note LIKE '*%@*')", keyword, keyword, keyword];
    NSPredicate * aerobicPredicate = [NSPredicate predicateWithFormat:predicateString];
    exercises = [Exercise objectsWithPredicate:aerobicPredicate];
    RLMResults<Exercise *> *sortedExercise = [exercises sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedExercise;
}

+ (NSDate *)lastUpdateDate{
    RLMResults<Exercise *> *exercises = [[Exercise allObjects] sortedResultsUsingKeyPath:@"updatedAt" ascending:YES];
    
    if(exercises.count > 0)
        return [[exercises lastObject] updatedAt];
    else
        return [NSDate dateWithTimeIntervalSince1970:0];
}
    
+(BOOL)deselectAllExercises{
    
    RLMResults<Exercise *> *exercises = [Exercise allObjects];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    for(Exercise *exercise in exercises){
        [realm transactionWithBlock:^{
            [exercise setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
        }];
    }
    

    
    return TRUE;
}

- (void)parseFromDictionary:(NSDictionary*)exerciseDictionary {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    self.exerciseId = [exerciseDictionary valueForKey:@"code"];
    self.tag1 = [exerciseDictionary valueForKey:@"tag1_id"];
    self.tag2 = [exerciseDictionary valueForKey:@"tag2_id"];
    self.tag3 = [exerciseDictionary valueForKey:@"tag3_id"];
    self.borl = [exerciseDictionary valueForKey:@"borl"];
    NSString * type = [exerciseDictionary valueForKey:@"tipo"];
    if (type) {
        type = [type lowercaseString];
    }
    self.type = type;
    self.title = [exerciseDictionary valueForKey:@"titulo"];
    self.desc = [exerciseDictionary valueForKey:@"descripcion"];
    self.weightDifficulty = [UtilManager intFromString:[exerciseDictionary valueForKey:@"dificultad"]];
    self.energy = [UtilManager floatFromString:[exerciseDictionary valueForKey:@"energia"]];
    self.picture = [exerciseDictionary valueForKey:@"imagen"];
    self.document = [exerciseDictionary valueForKey:@"documento"];
    self.note = [exerciseDictionary valueForKey:@"nota"];
    self.isSelected = NO;
    self.isPropertyOfTraining = @NO;
    
    [self setIsSelected:FALSE];
}

@end
