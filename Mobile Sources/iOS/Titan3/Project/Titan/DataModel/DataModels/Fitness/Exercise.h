//
//  Exercise.h
//  Titan
//
//  Created by Manuel Manzanera on 20/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface Exercise : RLMObject

@property NSString *exerciseId;
@property NSDate *createdAt;
@property NSDate *updatedAt;
@property NSString *owner;
@property NSString *title;
@property NSString *type;
@property NSString *note;
@property NSString *energyType;
@property NSString *content;
@property NSNumber <RLMInt> *weightDifficulty;
@property NSNumber <RLMFloat> *energy;
@property NSString *picture;
@property NSString *document;
@property NSString * borl;

@property NSString *tag1;
@property NSString *tag2;
@property NSString *tag3;

@property NSString * desc;
@property NSNumber<RLMBool> * isPropertyOfTraining;

//@property NSString *muscle;

/////Property Only for Exercise Selector Table
@property BOOL isSelected;

+ (BOOL)deleteAllExercises;
+ (Exercise *)exerciseWithId:(NSString *)exerciseId;
+ (BOOL)saveExercises:(NSMutableArray *)exercises;
+ (RLMResults *)getExercises;
+ (RLMResults *)getAerobicsExercisesWithKeyword:(NSString*)keyword;
+ (RLMResults *)getAerobicsExercises;
+ (RLMResults *)getAnaerobicExercisesWithKeyword:(NSString*)keyword;
+ (RLMResults *)getAnaerobicExercises;

+ (NSDate *)lastUpdateDate;
    
+(BOOL)deselectAllExercises;

- (void)parseFromDictionary:(NSDictionary*)exerciseDictionary;

@end

RLM_ARRAY_TYPE(Exercise)
