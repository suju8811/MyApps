//
//  Training.m
//  Titan
//
//  Created by Manuel Manzanera on 20/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Training.h"
#import "AppContext.h"
#import "UtilManager.h"


@implementation Training

+ (NSString *)primaryKey {
    return @"trainingId";
}

+ (BOOL)deleteAllTraining {
    for (Training * training in [Training allObjects]) {
        if (![Training deleteTraining:training]) {
            return NO;
        }
    }
    return YES;
}

//Get All Trainings

+ (RLMResults *)getTrainings {
    RLMResults<Training *> *trainings = [Training objectsWhere:@"isCalendarCopy!=TRUE"];
    RLMResults<Training *> *sortedTrainings = [trainings sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedTrainings;
}

+ (RLMResults *)getTrainingsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Training getTrainings];
    }

    NSString * predicateString = [NSString stringWithFormat:@"title LIKE '*%@*' OR desc LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*'", keyword, keyword, keyword, keyword];
    RLMResults<Training *> *trainings = [Training objectsWhere:predicateString];
    RLMResults<Training *> *sortedTrainings = [trainings sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedTrainings;
}

+ (RLMResults *)getTrainingsWithOwner:(NSString *)owner{
    
    RLMResults<Training *> *trainings = [Training objectsWhere:@"owner == %@ AND isCalendarCopy!=TRUE", owner];
    RLMResults<Training *> *sortedTrainings = [trainings sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedTrainings;

}

+ (RLMResults *)getMyTrainingsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Training getMyTrainings];
    }

    NSString * predicateString = [NSString stringWithFormat:@"owner == '%@' AND isCalendarCopy!=TRUE AND (title LIKE '*%@*' OR desc LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", [AppContext sharedInstance].currentUser.name, keyword, keyword, keyword, keyword];
    RLMResults<Training *> *trainings = [Training objectsWhere:predicateString];
    RLMResults<Training *> *sortedTrainings = [trainings sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedTrainings;
    
}

+ (RLMResults *)getMyTrainings {
    
    NSString * userName = [AppContext sharedInstance].currentUser.name;
    RLMResults<Training *> *trainings = [Training objectsWhere:@"owner == %@ AND isCalendarCopy!=TRUE", userName];
    RLMResults<Training *> *sortedTrainings = [trainings sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedTrainings;
}

+ (RLMResults *)getOtherTrainingsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Training getOtherTrainings];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"owner != '%@' AND isCalendarCopy!=TRUE AND (title LIKE '*%@*' OR desc LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", [AppContext sharedInstance].currentUser.name, keyword, keyword, keyword, keyword];
    RLMResults<Training *> *trainings = [Training objectsWhere:predicateString];
    RLMResults<Training *> *sortedTrainings = [trainings sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedTrainings;
}

+ (RLMResults *)getOtherTrainings {
    
    RLMResults<Training *> *trainings = [Training objectsWhere:@"owner != %@ AND isCalendarCopy!=TRUE", [AppContext sharedInstance].currentUser.name];
    RLMResults<Training *> *sortedTrainings = [trainings sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedTrainings;
}

+ (RLMResults *)getFavouritesTrainingsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Training getFavouritesTrainings];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"isFavourite == TRUE AND isCalendarCopy!=TRUE AND (title LIKE '*%@*' OR desc LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", keyword, keyword, keyword, keyword];
    RLMResults<Training *> *trainings = [Training objectsWhere:predicateString];
    RLMResults<Training *> *sortedTrainings = [trainings sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedTrainings;
}

+ (RLMResults *)getFavouritesTrainings {
    RLMResults<Training *> *trainings = [Training objectsWhere:@"isFavourite == TRUE AND isCalendarCopy!=TRUE"];
    RLMResults<Training *> *sortedTrainings = [trainings sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedTrainings;
}

+ (RLMResults *)getSharedTrainingsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Training getSharedTrainings];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"isShared == TRUE AND isCalendarCopy!=TRUE AND (title LIKE '*%@*' OR desc LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", keyword, keyword, keyword, keyword];
    RLMResults<Training *> *trainings = [Training objectsWhere:predicateString];
    RLMResults<Training *> *sortedTrainings = [trainings sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedTrainings;
}

+ (RLMResults *)getSharedTrainings {
    RLMResults<Training *> *trainings = [Training objectsWhere:@"isShared==TRUE AND isCalendarCopy!=TRUE"];
    RLMResults<Training *> *sortedTrainings = [trainings sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedTrainings;
}

// Save Trainings

+ (BOOL)saveTrainings:(NSMutableArray *)trainings {
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:trainings];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if (error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

// Update Trainings

+ (BOOL)updateTraining:(Training *)updateTraining {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:updateTraining];
    
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if (error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)deleteTraining:(Training *)deleteTraining {
    
//    RLMRealm *realm = [AppContext currentRealm];
//
//    NSArray * tags = [Tag getAllTags];
//    for(Tag *tag in tags){
//        [realm transactionWithBlock:^{
//            [tag setStatus:stateValue];
//        }];
//    }
//
//    [realm beginWriteTransaction];
//    NSError *error;
//    [realm commitWriteTransaction:&error];
//    if(error){
//        NSLog(@"%@",error.description);
//    }
    
    RLMRealm *realm = [AppContext currentRealm];
    [realm beginWriteTransaction];
    for(TrainingExercise *trainingExercise in deleteTraining.trainingExercises) {
        
        for (Serie *serie in trainingExercise.series) {
            [realm deleteObject:serie];
        }
        
        for (Serie *serie in trainingExercise.secondExerciseSeries) {
            [realm deleteObject:serie];
        }
        
        for (Exercise * exercise in trainingExercise.exercises) {
            [realm deleteObject:exercise];
        }
        
        [realm deleteObject:trainingExercise];
    }
    
    if(deleteTraining) {
        [realm deleteObject:deleteTraining];
    }
    
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)deleteTrainingExercise:(TrainingExercise *)deleteTrainingExercise inTraining:(Training *)training {
    
    RLMRealm *realm = [AppContext currentRealm];
    
    int count = 0;
    
    for(TrainingExercise *trainingExercise in training.trainingExercises){
    
        for(Serie *serie in trainingExercise.series){
            [realm beginWriteTransaction];
            [realm deleteObject:serie];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
        }
        
        for(Serie *serie in trainingExercise.secondExerciseSeries){
            [realm beginWriteTransaction];
            [realm deleteObject:serie];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
        }
        
        if([trainingExercise isEqual:deleteTrainingExercise]){
            
            [realm transactionWithBlock:^{
                [training.trainingExercises removeObjectAtIndex:count];
                [training setTrainingExercisesCount:[NSNumber numberWithInt:training.trainingExercisesCount.intValue - 1]];
            }];
            
            [realm beginWriteTransaction];
            [realm deleteObject:trainingExercise];
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            
            return YES;
        }
        
        count ++;
    }
    
    return YES;
}

+ (BOOL)deleteSuperSerie:(TrainingExercise *)superSerieTrainingExercise inTraining:(Training *)training{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    int order = 0;
    
    for(Exercise *exercise in superSerieTrainingExercise.exercises){
    
        TrainingExercise *trainingExercise = [[TrainingExercise alloc] init];
        
        [trainingExercise setIsSelected:[NSNumber numberWithBool:FALSE]];
        
        [trainingExercise setOrder:[NSNumber numberWithInt:order]];
        [trainingExercise.exercises addObject:exercise];
        [trainingExercise setTitle:exercise.title];
        [trainingExercise setTrainingId:[UtilManager uuid]];
        [trainingExercise setOwner:[[AppContext sharedInstance] userId]];
        
        order ++;
        
        [realm transactionWithBlock:^{
            [training.trainingExercises addObject:trainingExercise];
        }];
    }
    
    [realm beginWriteTransaction];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    [Training deleteTrainingExercise:superSerieTrainingExercise inTraining:training];
    
    return TRUE;

}

+ (BOOL)canDeleteTraining:(Training *)training{
    if([training.owner isEqualToString:[[AppContext sharedInstance] userId]])
        return YES;
    else
        return FALSE;
}

// Get Training With Id

+ (Training *)trainingWithId:(NSString *)training{
    
    RLMResults <Training *>*trainings = [Training objectsWhere:@"trainingId = %@",training];
    
    if(trainings.count > 0)
        return [trainings objectAtIndex:0];
    else
        return nil;
}

+ (Training *)trainingWithServerId:(NSString *)training{
    RLMResults <Training *>*trainings = [Training objectsWhere:@"serverTrainingId = %@",training];
    
    if(trainings.count > 0)
        return [trainings objectAtIndex:0];
    else
        return nil;
}

+ (int)getMinutes:(Training *)training{
    
    int sec = 0;
    
    if(training.trainingExercises.count == 0)
        return 0;
    else{
        for(TrainingExercise *trainingExercise in training.trainingExercises){
            sec = [TrainingExercise getExerciseTime:trainingExercise] + sec;
        }
    }
    
    return sec;
}

+ (NSDate *)lastUpdateDate{
    RLMResults<Training *> *trainings = [[Training allObjects] sortedResultsUsingKeyPath:@"updatedAt" ascending:FALSE];
    
    if(trainings.count > 0)
        return [[trainings objectAtIndex:0] updatedAt];
    else
        return [NSDate dateWithTimeIntervalSince1970:0];
}

+ (Training *)copyTraining:(Training *)training{
    Training *copyTraining = [[Training alloc] init];
    
    [copyTraining setTrainingId:[UtilManager uuid]];
    [copyTraining setServerTrainingId:training.serverTrainingId];
    
    [copyTraining setContent:training.content];
    [copyTraining setNotes:training.notes];
    
    [copyTraining setCreatedAt:[NSDate date]];
    [copyTraining setUpdatedAt:[NSDate date]];
    [copyTraining setOwner:[[AppContext sharedInstance] userId]];
    [copyTraining setTitle:training.title];
    [copyTraining setDesc:training.desc];
    [copyTraining setAnnotation:training.annotation];
    [copyTraining setPrivacity:training.privacity];
    [copyTraining setPunctation:training.punctation];
    [copyTraining setTotalEnergy:training.totalEnergy];
    [copyTraining setDuration:training.duration];
    [copyTraining setTags:training.tags];
    [copyTraining setTrainingExercisesCount:training.trainingExercisesCount];
    [copyTraining setIsFavourite:[NSNumber numberWithBool:FALSE]];
    [copyTraining setIsCopy:[NSNumber numberWithBool:TRUE]];
    [copyTraining setIsSelected:[NSNumber numberWithBool:FALSE]];
    [copyTraining setExercises:training.exercises];
    [copyTraining setTrainingPlanId:training.trainingPlanId];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    for(TrainingExercise *trainingExercise in training.trainingExercises){
        TrainingExercise *newTrainingExercise = [[TrainingExercise alloc] init];
        
        [newTrainingExercise setTrainingId:[UtilManager uuid]];
        [newTrainingExercise setTitle:trainingExercise.title];
        [newTrainingExercise setContent:trainingExercise.content];
        [newTrainingExercise setOwner:[[AppContext sharedInstance] userId]];
        [newTrainingExercise setOrder:trainingExercise.order];
        [newTrainingExercise setRandomKey:trainingExercise.randomKey];
        [newTrainingExercise setTotalEnergy:trainingExercise.totalEnergy];
        
        for(Exercise *exercise in trainingExercise.exercises){
            [newTrainingExercise.exercises addObject:exercise];
        }
        
        for (Serie *serie in trainingExercise.series){
            
            Serie *newSerie = [[Serie alloc] init];
            
            [newSerie setSerieId:[UtilManager uuid]];
            [newSerie setOwner:serie.owner];
            [newSerie setExercise:serie.exercise];
            [newSerie setIntensityFactor:serie.intensityFactor];
            [newSerie setReps:serie.reps];
            [newSerie setRest:serie.rest];
            [newSerie setWeight:serie.weight];
            [newSerie setIsSubserie:serie.isSubserie];
            [newSerie setOrder:serie.order];
    
            [realm transactionWithBlock:^{
                [[newTrainingExercise series] addObject:newSerie];
            }];
            
            [realm beginWriteTransaction];
            [realm addOrUpdateObject:newSerie];
    
            NSError *error;
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            
            [realm beginWriteTransaction];
            [realm addOrUpdateObject:newTrainingExercise];
            
            [realm commitWriteTransaction:&error];
            if(error){
                NSLog(@"%@",error.description);
            }
            
        }
        
        [[copyTraining trainingExercises] addObject:trainingExercise];
    }
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:copyTraining];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    return copyTraining;
}

+(BOOL)deselectAllExercises{
    
    RLMResults<Training *> *trainings = [Training allObjects];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    for(Training *training in trainings){
        [realm transactionWithBlock:^{
            [training setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
        }];
    }
    
    return TRUE;
}

+ (NSMutableArray *)getTrainingsExercisesTags:(Training *)training {
    
    NSMutableArray *tags = [[NSMutableArray alloc] init];
    
    for (TrainingExercise *trainingExercise in training.trainingExercises){
        
        for(Exercise *exercise in trainingExercise.exercises){
            Tag *tag;
            
            if(exercise.tag1){
                tag = [Tag tagWithId:exercise.tag1];
                if (tag) {
                    if(![tags containsObject:tag]) {
                        [tags addObject:tag];
                    }
                }
            }
            
            if(exercise.tag2){
                tag = [Tag tagWithId:exercise.tag2];
                if(tag){
                    if(![tags containsObject:tag])
                        [tags addObject:tag];
                }
            }
            
            if(exercise.tag3){
                tag = [Tag tagWithId:exercise.tag3];
                if(tag){
                    if(![tags containsObject:tag])
                        [tags addObject:tag];
                }
            }
            
        }
    
    }
    
    return tags;
}

- (void)parseFromDictionary:(NSDictionary*)trainingDictionary {
    [super parseFromDictionary:trainingDictionary];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    if (self.trainingId == nil) {
        self.trainingId = [trainingDictionary valueForKey:@"id"];
    }
    
    self.serverTrainingId = [trainingDictionary valueForKey:@"id"];
    self.title = [trainingDictionary valueForKey:@"titulo"];
    self.desc = [trainingDictionary valueForKey:@"descripcion"];
    self.annotation = [trainingDictionary valueForKey:@"anotacion"];
    self.privacity = [trainingDictionary valueForKey:@"privacidad"];
    self.punctation = [trainingDictionary valueForKey:@"puntuacion"];
    self.totalEnergy = [UtilManager intFromString:[trainingDictionary valueForKey:@"energia"]];
    self.duration = [UtilManager intFromString:[trainingDictionary valueForKey:@"duracion"]];
    self.updatedAt = [formatter dateFromString:[trainingDictionary valueForKey:@"fecha"]];
    self.owner = [trainingDictionary valueForKey:@"user"];
    
    NSArray<NSDictionary*>* exercises = [trainingDictionary valueForKey:@"ejercicios"];
    self.trainingExercisesCount = @(exercises.count);
    
    if(exercises.count > 0) {
        NSMutableArray *exercisesAux = [[NSMutableArray alloc] initWithCapacity:exercises.count];
        
        //
        // Group by order
        //
        
        NSMutableDictionary * orderGroupedDictionary = [[NSMutableDictionary alloc] init];
        for (NSDictionary * exerciseDictionary in exercises) {
            NSNumber * order = [UtilManager intFromString:[exerciseDictionary valueForKey:@"orden"]];
            if (order == nil) {
                order = @0;
            }
            
            NSMutableArray * exerciseDictionaryArray;
            if (orderGroupedDictionary[order]) {
                exerciseDictionaryArray = orderGroupedDictionary[order];
            }
            else {
                exerciseDictionaryArray = [[NSMutableArray alloc] init];
            }
            [exerciseDictionaryArray addObject:exerciseDictionary];
            [orderGroupedDictionary setObject:exerciseDictionaryArray forKey:order];
        }
        
        for (NSNumber * keyOrder in [orderGroupedDictionary allKeys]) {
            NSArray * exerciseDictionaryArray = [orderGroupedDictionary objectForKey:keyOrder];
            NSMutableArray<Exercise*>* exerciseArray = [[NSMutableArray<Exercise*> alloc] init];
            for (NSDictionary * exerciseDictionary in exerciseDictionaryArray) {
                Exercise * exercise = [[Exercise alloc] init];
                [exercise parseFromDictionary:exerciseDictionary];
                exercise.isPropertyOfTraining = @YES;
                exercise.exerciseId = [NSString stringWithFormat:@"T-%@", exercise.exerciseId];
                [exerciseArray addObject:exercise];
            }
            
            TrainingExercise * trainingExercise = [[TrainingExercise alloc] init];
            [trainingExercise.exercises addObjects:exerciseArray];
            [trainingExercise parseFromDictionary:[exerciseDictionaryArray firstObject]];
            [exercisesAux addObject:trainingExercise];
        }
        
        if (exercisesAux.count > 0) {
            [self.trainingExercises addObjects:exercisesAux];
        }
    }
    
    NSArray<NSDictionary*> *tags = [trainingDictionary valueForKey:@"etiquetas"];
    if(tags.count > 0){
        NSMutableArray *tagsAux = [[NSMutableArray alloc] initWithCapacity:tags.count];
        
        for(NSDictionary *tagDictionary in tags){
            Tag * tag = [[Tag alloc] init];
            tag.tagId = [tagDictionary valueForKey:@"entrenamiento_id"];
            tag.title = [tagDictionary valueForKey:@"etiqueta"];
        }
        
        if(tagsAux.count > 0) {
            [self.tags addObjects:tagsAux];
        }
    }
}

@end
