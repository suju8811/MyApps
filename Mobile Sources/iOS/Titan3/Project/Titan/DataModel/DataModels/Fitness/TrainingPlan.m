//
//  TrainingPlan.m
//  Titan
//
//  Created by Manuel Manzanera on 2/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingPlan.h"
#import "AppContext.h"
#import "UtilManager.h"

@implementation TrainingPlan

- (id)init {
    self = [super init];
    return self;
}

+ (NSString *)primaryKey {
    return @"trainingPlanId";
}

+ (BOOL)deleteAllTrainingPlans {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[TrainingPlan allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)saveTrainingPlans:(NSMutableArray *)plans {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:plans];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)deleteTrainingPlan:(TrainingPlan *)trainingPlan {
    
    [TrainingPlan deleteInfoInPlan:trainingPlan];
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObject:trainingPlan];
    
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if (error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)deleteInfoInPlan:(TrainingPlan *)trainingPlan {

    for(TrainingDay *trainingDay in trainingPlan.trainingDay) {
        for(TrainingDayContent *trainingDayContent in trainingDay.trainingDayContent) {
            
            RLMRealm *realm = [AppContext currentRealm];
            
            [realm beginWriteTransaction];
            [realm deleteObject:trainingDayContent];
            
            NSError *error;
            [realm commitWriteTransaction:&error];
        }
        
        RLMRealm *realm = [AppContext currentRealm];
        
        [realm beginWriteTransaction];
        [realm deleteObject:trainingDay];
        
        NSError *error;
        [realm commitWriteTransaction:&error];
        if (error) {
            NSLog(@"%@",error.description);
            return NO;
        }
    }
    
    return YES;
}

+ (BOOL)deleteTrainingDayContent:(TrainingDayContent*)trainingDayContent inTrainingDay:(TrainingDay *)trainingDay {
    if (trainingDay.trainingDayContent.count <= 1) {
        return [TrainingDay deleteTrainingDay:trainingDay];
    }
    
    return [TrainingDayContent deleteTrainingDayContent:trainingDayContent];
}

+ (BOOL)deleteTrainingDay:(TrainingDay *)trainingDay inTrainingPlan:(TrainingPlan *)trainingPlan {

    RLMRealm *realm = [AppContext currentRealm];
        
    for(TrainingDayContent *trainingDayContent in trainingDay.trainingDayContent) {
        for(TrainingDayContent *trainingDayContent in trainingDay.trainingDayContent) {
            [realm beginWriteTransaction];
            [realm deleteObject:trainingDayContent];
            NSError *error;
            [realm commitWriteTransaction:&error];
        }
        
        [realm beginWriteTransaction];
        [realm deleteObject:trainingDayContent];
        NSError *error;
        [realm commitWriteTransaction:&error];
            
        if (error) {
            NSLog(@"%@",error.description);
            return NO;
        }
    }
    
    [realm beginWriteTransaction];
    [realm deleteObject:trainingDay];
    NSError *error;
    [realm commitWriteTransaction:&error];
        
    if (error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)canDeleteTrainingPlan:(TrainingPlan *)trainingPlan {
    
    if ([trainingPlan.owner isEqualToString:[AppContext sharedInstance].currentUser.name]) {
        return YES;
    }
    else {
        return NO;
    }
}

+ (RLMResults *)getTrainingPlansWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [TrainingPlan getTrainingPlans];
    }

    NSString * predicateString = [NSString stringWithFormat:@"isCalendarCopy == FALSE AND isRepose == FALSE AND (title LIKE '*%@*' OR desc LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", keyword, keyword, keyword, keyword];
    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:predicateString];
    return trainings;
}

+ (RLMResults *)getTrainingPlans {
    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:@"isRepose==FALSE AND isCalendarCopy==FALSE"];
    return trainings;
}

+ (RLMResults *)getMyTrainingPlansWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [TrainingPlan getMyTrainingPlans];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"owner='%@' AND isRepose == FALSE AND isCalendarCopy==FALSE AND (title LIKE '*%@*' OR desc LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", [AppContext sharedInstance].currentUser.name, keyword, keyword, keyword, keyword];
    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:predicateString];
    return trainings;
}

+ (RLMResults *)getMyTrainingPlans {
    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:@"owner=%@ AND isCalendarCopy== FALSE AND isRepose = FALSE", [AppContext sharedInstance].currentUser.name];
//    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:@"owner=%@", [AppContext sharedInstance].currentUser.name];
    return trainings;
}

+ (RLMResults *)getOtherTrainingPlansWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [TrainingPlan getOtherTrainingPlans];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"owner!='%@' AND isCalendarCopy==FALSE AND isRepose == FALSE AND (title LIKE '*%@*' OR desc LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", [AppContext sharedInstance].currentUser.name, keyword, keyword, keyword, keyword];
    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:predicateString];
    return trainings;
}

+ (RLMResults *)getOtherTrainingPlans {
    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:@"owner!=%@ AND isCalendarCopy==FALSE AND isRepose==FALSE", [AppContext sharedInstance].currentUser.name];
//    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:@"owner!=%@", [AppContext sharedInstance].currentUser.name];
    return trainings;
}

+ (RLMResults *)getReposes {
    RLMResults<TrainingPlan *> *reposes = [TrainingPlan objectsWhere:@"isRepose==TRUE"];
    return reposes;
}

+ (TrainingPlan *)getTrainingPlanWithId:(NSString *)trainingPlanId{
     RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:@"trainingPlanId==%@",trainingPlanId];
     if(trainings.count > 0)
         return trainings.firstObject;
    return nil;
}

+ (TrainingPlan *)getTrainingPlanWithServerId:(NSString *)trainingPlanServerId {
    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:@"serverTrainingPlanId==%@", trainingPlanServerId];
    if(trainings.count > 0)
        return trainings.firstObject;
    return nil;
}

+ (RLMResults *)getTrainingPlansWithOwner:(NSString *)owner{
    
    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:@"owner == %@ AND isCalendarCopy != TRUE AND isRepose = FALSE",owner];
    return trainings;
}

+ (RLMResults *)getFavouritesTrainnigPlansWithKeyword: (NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [TrainingPlan getFavouritesTrainnigPlans];
    }

    NSString * predicateString = [NSString stringWithFormat:@"isFavourite==TRUE AND isCalendarCopy==FALSE AND isRepose = FALSE AND (title LIKE '*%@*' OR desc LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", keyword, keyword, keyword, keyword];
    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:predicateString];
    return trainings;
}

+ (RLMResults *)getFavouritesTrainnigPlans {
    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:@"isFavourite==TRUE AND isCalendarCopy==FALSE AND isRepose==FALSE"];
    return trainings;
}

+ (RLMResults *)getSharedTrainnigPlansWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [TrainingPlan getSharedTrainnigPlans];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"isShared == TRUE AND isCalendarCopy==FALSE AND isRepose==FALSE AND (title LIKE '*%@*' OR desc LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", keyword, keyword, keyword, keyword];
    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:predicateString];
    return trainings;
}

+ (RLMResults *)getSharedTrainnigPlans {
    RLMResults<TrainingPlan *> *trainings = [TrainingPlan objectsWhere:@"isShared = TRUE AND isCalendarCopy==FALSE AND isRepose = FALSE"];
    return trainings;
}

+ (RLMResults *)getTrainingPlansInDate:(NSDate *)date{
    
    RLMResults<TrainingPlan *> *trainingPlans = [TrainingPlan objectsWhere:@"startDate <= %@ AND endDate >= %@ AND isCalendarCopy = TRUE AND isRepose = FALSE",date,date];
    
    return trainingPlans;
}

+ (NSDictionary *)trainingDictionaryFromTrainingPlan:(TrainingPlan *)trainingPlan{

    NSMutableDictionary *trainingPlanDictionary = [[NSMutableDictionary alloc] init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    [trainingPlanDictionary setObject:trainingPlan.title forKey:@"title"];
    if(trainingPlan.content && ![trainingPlan.notes isEqualToString:NSLocalizedString(@"Descripción", nil)])
        [trainingPlanDictionary setObject:trainingPlan.content forKey:@"content"];
    [trainingPlanDictionary setValue:trainingPlan.trainingPlanId forKey:@"trainingPlanId"];
    
    if(trainingPlan.notes && ![trainingPlan.notes isEqualToString:NSLocalizedString(@"Notas", nil)])
        [trainingPlanDictionary setObject:trainingPlan.notes forKey:@"note"];
    if(trainingPlan.privacity)
        [trainingPlanDictionary setObject:trainingPlan.privacity forKey:@"privacity"];
    else
        [trainingPlanDictionary setObject:@"public" forKey:@"privacity"];
    
    NSMutableArray *tags = [[NSMutableArray alloc] initWithCapacity:trainingPlan.tags.count];
    for(Tag *tag in trainingPlan.tags)
        [tags addObject:tag.tagId];
    
    [trainingPlanDictionary setObject:tags forKey:@"tags"];
    
    NSMutableArray *trainingDayArray = [[NSMutableArray alloc] init];
    
    for(TrainingDay *trainingDay in trainingPlan.trainingDay){
    
        NSMutableDictionary *trainingDayDictionary = [[NSMutableDictionary alloc] init];
        
        if(trainingDay.trainingDayContent.count == 0)
            [trainingDayDictionary setObject:[NSNumber numberWithBool:TRUE] forKey:@"rest_day"];
        else
            [trainingDayDictionary setObject:[NSNumber numberWithBool:FALSE] forKey:@"rest_day"];
        
        NSMutableArray *trainingDaysContentArray = [[NSMutableArray alloc] initWithCapacity:trainingDay.trainingDayContent.count];
        for(TrainingDayContent *trainingDayContent in trainingDay.trainingDayContent){
            NSMutableDictionary *trainingDayContentDictionary = [[NSMutableDictionary alloc] init];
            
            NSString *training;
            
            if(trainingDayContent.training.serverTrainingId)
                training = trainingDayContent.training.serverTrainingId;
            else
                training = trainingDayContent.trainingId;
            
            if(trainingDayContent.isClone.boolValue)
                [trainingDayContentDictionary setObject:[NSNumber numberWithBool:trainingDayContent.isClone.boolValue] forKey:@"clone"];
            
            [trainingDayContentDictionary setObject:training forKey:@"training"];
            if(trainingDayContent.training.trainingId){
                [trainingDayContentDictionary setObject:trainingDayContent.training.trainingId forKey:@"trainingId"];
                [trainingDayContentDictionary setObject:training forKey:@"training"];
            }else
                [trainingDayContentDictionary setObject:trainingDayContent.trainingId forKey:@"trainingId"];
            [trainingDaysContentArray addObject:trainingDayContentDictionary];
        }
        
        [trainingDayDictionary setObject:trainingDaysContentArray forKey:@"training_day_content"];
        [trainingDayArray addObject:trainingDayDictionary];
    }
    
    [trainingPlanDictionary setObject:trainingDayArray forKey:@"training_day"];
    
    return trainingPlanDictionary;
}


+ (NSMutableArray *)getTrainingsTags:(TrainingPlan *)trainingPlan{
    NSMutableArray *tags = [[NSMutableArray alloc] init];
    
    for (TrainingDay *trainingDay in trainingPlan.trainingDay) {
        if (trainingDay.restDay.intValue != 1) {
            for (TrainingDayContent *trainingDayContent in trainingDay.trainingDayContent) {
                Training *training = trainingDayContent.training;
                for (Tag *tag in [Training getTrainingsExercisesTags:training]) {
                    if (tag) {
                        if (![tags containsObject:tag]) {
                            [tags addObject:tag];
                        }
                    }
                }
            }
        }
    }
    
    return tags;
}

+ (TrainingPlan *)copyTrainingPlan:(TrainingPlan *)trainingPlan {
    TrainingPlan *newTrainingPlan = [[TrainingPlan alloc] init];
    newTrainingPlan.trainingPlanId = [UtilManager uuid];
    
    /*
     @property RLMArray <TrainingDay *><TrainingDay> *trainingDay;
     @property BOOL isFavourite;
     */

    newTrainingPlan.title = [NSString stringWithFormat:@"%@ %@",trainingPlan.title,NSLocalizedString(@"Copia", nil)];
    newTrainingPlan.desc = trainingPlan.desc;
    newTrainingPlan.createdAt = [NSDate date];
    newTrainingPlan.updatedAt = [NSDate date];
    newTrainingPlan.owner = [AppContext sharedInstance].currentUser.name;
    newTrainingPlan.content = trainingPlan.content;
    newTrainingPlan.notes = trainingPlan.notes;
    if(trainingPlan.privacity) {
        newTrainingPlan.privacity = trainingPlan.privacity;
    }
    else {
        newTrainingPlan.privacity = @"public";
    }
    
    newTrainingPlan.isCopy = trainingPlan.isCopy;
    newTrainingPlan.tags = trainingPlan.tags;
    
    newTrainingPlan.isFavourite = @NO;
    newTrainingPlan.trainingDay = trainingPlan.trainingDay;
    
    return newTrainingPlan;
}


+ (NSDate *)lastUpdateDate{
    RLMResults<TrainingPlan *> *trainingPlans = [[TrainingPlan allObjects] sortedResultsUsingKeyPath:@"updatedAt" ascending:FALSE];
    
    if(trainingPlans.count > 0)
        return [[trainingPlans objectAtIndex:0] updatedAt];
    else
        return [NSDate dateWithTimeIntervalSince1970:0];
}

+ (int)numberOfTrainings:(TrainingPlan *)trainingPlan{
    
    int exercises = 0;
    
    for(TrainingDay *trainingDay in trainingPlan.trainingDay){
//        if(trainingDay.restDay.intValue != 1)
//        {
            exercises = (int) trainingDay.trainingDayContent.count + exercises;
//        }
    }
    
    return exercises;
}


+ (NSInteger)getTrainingPlankCal:(TrainingPlan *)trainingPlan{
    
    NSInteger kCal = 0;
    
    for(TrainingDay *trainingDay in trainingPlan.trainingDay)
    {
        if(trainingDay.restDay.intValue != 1){
            
            for(TrainingDayContent *trainingDayContent in trainingDay.trainingDayContent){
                Training *training = trainingDayContent.training;
                for(TrainingExercise *trainingExercise in training.trainingExercises){
                    kCal = kCal + [TrainingExercise getKCals:trainingExercise];
                }
                
            }
        }
    }
    
    return kCal;
}

- (void)parseFromDictionary: (NSDictionary*)trainingPlanDictionary isSchedule:(BOOL)isSchedule {
    [super parseFromDictionary:trainingPlanDictionary];
    
    self.serverTrainingPlanId = [trainingPlanDictionary valueForKey:@"id"];
    self.title = [trainingPlanDictionary valueForKey:@"titulo"];
    self.desc = [trainingPlanDictionary valueForKey:@"descripcion"];
    self.notes = [trainingPlanDictionary valueForKey:@"nota"];
    
    if ([self.title hasPrefix:@"@@"]) {
        self.title = [self.title substringFromIndex:2];
        self.isRepose = @YES;
    }
    else {
        self.isRepose = @NO;
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    if([trainingPlanDictionary valueForKey:@"comienzo"]){
        self.startDate = [formatter dateFromString:[trainingPlanDictionary valueForKey:@"comienzo"]];
        self.endDate = [formatter dateFromString:[trainingPlanDictionary valueForKey:@"finalizacion"]];
        self.isCalendarCopy = @YES;
    }
    
    self.createdAt = [formatter dateFromString:[trainingPlanDictionary valueForKey:@"createdAt"]];
    self.updatedAt = [formatter dateFromString:[trainingPlanDictionary valueForKey:@"updatedAt"]];
    self.owner = [trainingPlanDictionary valueForKey:@"user"];
    self.content = [trainingPlanDictionary valueForKey:@"content"];
    
    
    NSArray *tags = [trainingPlanDictionary valueForKey:@"tags"];
    [self.tags removeAllObjects];
    
    if(tags.count > 0) {
        NSMutableArray *tagsAux = [[NSMutableArray alloc] initWithCapacity:tags.count];
        
        for(NSString *tagsString in tags){
            Tag *tag = [Tag tagWithId:tagsString];
            if(tag) {
                [tagsAux addObject:tag];
            }
        }
        
        if(tagsAux.count > 0) {
            [self.tags addObjects:tagsAux];
        }
    }
    
    NSArray * trainingDays = [trainingPlanDictionary valueForKey:@"plan_dia"];
    NSArray * trainingDayContents = [trainingPlanDictionary valueForKey:@"plan_dia_training"];
    
    [self.trainingDay removeAllObjects];
    
    NSMutableArray * trainingDayObjectsAux = [[NSMutableArray alloc] init];
    for (NSDictionary * trainingDayDictionary in trainingDays) {
        
        TrainingDay *trainingDay = [[TrainingDay alloc] init];
        
        trainingDay.code = [UtilManager intFromString:[trainingDayDictionary valueForKey:@"code"]];
        trainingDay.order = [UtilManager intFromString:[trainingDayDictionary valueForKey:@"orden"]];
        trainingDay.restDay = [UtilManager intFromString:[trainingDayDictionary valueForKey:@"rest"]];
        trainingDay.planId = [trainingDayDictionary valueForKey:@"plan_id"];
        
        NSMutableArray * trainingDayContentObjectsAux = [[NSMutableArray alloc] init];
        for (NSDictionary * trainingDayContentDictionary in trainingDayContents) {
            NSNumber * planDayContentplanDayId = [trainingDayContentDictionary valueForKey:@"plan_dia_id"];
            if (planDayContentplanDayId.intValue == trainingDay.code.intValue) {
                
                TrainingDayContent *trainingDayContent = [[TrainingDayContent alloc] init];
                
                trainingDayContent.code = [trainingDayContentDictionary valueForKey:@"code"];
                trainingDayContent.end = [trainingDayContentDictionary valueForKey:@"stop_hour"];
                trainingDayContent.start = [trainingDayContentDictionary valueForKey:@"start_hour"];
                NSString * trainingId = [trainingDayContentDictionary valueForKey:@"training"];
                if (isSchedule) {
                    trainingId = [NSString stringWithFormat:@"%@-schedule", trainingId];
                }
                Training * existTraining = [Training trainingWithId:trainingId];
                if (existTraining) {
                    trainingDayContent.training = existTraining;
                }
                
                trainingDayContent.trainingId = trainingId;
                
                [trainingDayContentObjectsAux addObject:trainingDayContent];
            }
        }
        [trainingDay.trainingDayContent addObjects:trainingDayContentObjectsAux];
        
        [trainingDayObjectsAux addObject:trainingDay];
    }
    
    [self.trainingDay addObjects:trainingDayObjectsAux];
}

- (void)parseFromDictionary: (NSDictionary*)trainingPlanDictionary {
    [self parseFromDictionary:trainingPlanDictionary isSchedule:NO];
}

@end
