//
//  TrainingExercise.h
//  Titan
//
//  Created by Manuel Manzanera on 7/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "Exercise.h"
#import "Serie.h"

@interface TrainingExercise : RLMObject

@property NSString *trainingId;

@property NSString *serverTrainingId;
@property NSString *content;
@property NSDate *createdAt;
@property NSDate *updatedAt;
@property NSString *owner;
@property NSString *title;
@property NSString *randomKey;
@property NSString * rest;
@property NSNumber<RLMInt> *order;
@property NSNumber<RLMFloat> *totalEnergy;
@property RLMArray <Exercise *><Exercise> *exercises;
@property RLMArray <Serie *><Serie> *series;
@property RLMArray <Serie *><Serie> *secondExerciseSeries;
//@property Training *training;
@property NSNumber <RLMBool> *isSelected;
@property NSNumber <RLMBool> *isSubSerie;
@property NSNumber <RLMBool> *isSuperSerie;
@property NSNumber <RLMBool> *done;
@property NSString * startDate;
@property NSString * endDate;

+ (BOOL)deleteAllTrainingExercises;
+ (BOOL)saveTrainings:(NSMutableArray *)trainings;
+ (TrainingExercise *)exerciseWithId:(NSString *)exerciseId;

+ (int)getExerciseTime:(TrainingExercise *)trainingExercise;
+ (NSInteger)getKCals:(TrainingExercise *)trainingExercise;

+ (TrainingExercise *)duplicateTrainingExercise:(TrainingExercise *)trainingExercise;

- (void)parseFromDictionary:(NSDictionary*)trainingExerciseDictionary;

@end

RLM_ARRAY_TYPE(TrainingExercise)
