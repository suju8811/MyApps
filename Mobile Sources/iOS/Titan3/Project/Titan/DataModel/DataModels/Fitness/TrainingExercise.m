//
//  TrainingExercise.m
//  Titan
//
//  Created by Manuel Manzanera on 7/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingExercise.h"
#import "AppContext.h"
#import "AppConstants.h"
#import "UtilManager.h"
#import "ValueConstant.h"
#import "JSONKit.h"

@implementation TrainingExercise

+ (NSString *)primaryKey{
    return @"trainingId";
}

+ (BOOL)deleteAllTrainingExercises {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[TrainingExercise allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)saveTrainings:(NSMutableArray *)trainings{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:trainings];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return FALSE;
    }
    
    return TRUE;
}

+ (TrainingExercise *)exerciseWithId:(NSString *)exerciseId{
    RLMResults<TrainingExercise *> *exercises = [TrainingExercise objectsWhere:[NSString stringWithFormat:@"serverTrainingId = '%@'",exerciseId]];
    
    if(exercises.count != 0)
        return [exercises objectAtIndex:0];
    else
        return nil;
}

+ (int)getExerciseTime:(TrainingExercise *)trainingExercise{
    
    int secs = 0;
    int tRep = 2;
    
    if(trainingExercise.series.count == 0)
        return 0;
    
    for(Serie *serie in trainingExercise.series){
        
        if([serie.exercise.type isEqualToString:@"aerobic"])
             secs = serie.execTime.intValue + serie.rest.intValue + secs;
        else
            secs = serie.reps.intValue * tRep + serie.rest.intValue + secs;
    }
    
    if(secs == 0)
        return 90;
    else
        return secs + 180;
}

+ (NSInteger)getKCals:(TrainingExercise *)trainingExercise{
    
    NSInteger kCals = 0;
    
    for(Serie *serie in trainingExercise.series){
        kCals = kCals + [Serie kCalInSerie:serie];
    }
    
    ValueConstant *valueConstant = [ValueConstant getValueConstantsWithName:@"FMetPike"];
    
    if(valueConstant)
        kCals = kCals * valueConstant.value.floatValue;
    else
        kCals = kCals * FMetPike;
    return kCals;
}

+ (TrainingExercise *)duplicateTrainingExercise:(TrainingExercise *)trainingExercise{
    TrainingExercise *duplicateTrainingExercise = [[TrainingExercise alloc] init];
    
    [duplicateTrainingExercise setTitle:trainingExercise.title];
    [duplicateTrainingExercise setOwner:[[AppContext sharedInstance] userId]];
    
    [duplicateTrainingExercise setTrainingId:[UtilManager uuid]];
    
    [duplicateTrainingExercise setContent:trainingExercise.content];
    [duplicateTrainingExercise setOrder:[NSNumber numberWithInt:trainingExercise.order.intValue+1]];
    [duplicateTrainingExercise setRandomKey:trainingExercise.randomKey];
    [duplicateTrainingExercise setTotalEnergy:trainingExercise.totalEnergy];
    
    [duplicateTrainingExercise setExercises:trainingExercise.exercises];
    
    for (Serie *serie in trainingExercise.series){
        
        Serie *newSerie = [[Serie alloc] init];
        
        [newSerie setSerieId:[UtilManager uuid]];
        [newSerie setOwner:serie.owner];
        [newSerie setExercise:serie.exercise];
        [newSerie setIntensityFactor:serie.intensityFactor];
        [newSerie setReps:serie.reps];
        [newSerie setWeight:serie.weight];
        [newSerie setRest:serie.rest];
        [newSerie setIsSubserie:serie.isSubserie];
        [newSerie setOrder:serie.order];
        [newSerie setIsSuperSerie:serie.isSuperSerie];
        
        [[duplicateTrainingExercise series] addObject:newSerie];
    }

    return duplicateTrainingExercise;
}

- (void)parseFromDictionary:(NSDictionary*)trainingExerciseDictionary {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    self.trainingId = [UtilManager uuid];
    self.serverTrainingId = [trainingExerciseDictionary valueForKey:@"entrenamiento_id"];
    self.totalEnergy = [UtilManager floatFromString:[trainingExerciseDictionary valueForKey:@"energias"]];
    self.order = [UtilManager intFromString:[trainingExerciseDictionary valueForKey:@"orden"]];
    self.done = [UtilManager intFromString:[trainingExerciseDictionary valueForKey:@"done"]];
    
    NSString * stepString = [trainingExerciseDictionary valueForKey:@"steps"];
    if (![stepString isKindOfClass:[NSNull class]] &&
        stepString.length > 0) {
        
        NSArray<NSDictionary*>* stepsDictionary = [stepString objectFromJSONString];
        for (NSDictionary * stepDictionary in stepsDictionary) {
            Serie * serie = [[Serie alloc] init];
            serie.serieId = [UtilManager uuid];
            serie.reps = @([[stepDictionary valueForKey:@"reps"] intValue]);
            serie.weight = @([[stepDictionary valueForKey:@"kg"] floatValue]);
            serie.execTime = @([[stepDictionary valueForKey:@"time"] intValue]);
            serie.rest = @10;
            if (self.exercises && self.exercises.count > 0) {
                serie.exercise = [self.exercises firstObject];
            }
            [self.series addObject:serie];
        }
    }
    self.isSuperSerie = @([[trainingExerciseDictionary valueForKey:@"super_serie"] intValue] == 1);
    self.isSubSerie = @([[trainingExerciseDictionary valueForKey:@"serie"] intValue] == 1);
//    [self.exercises addObject:exercise];
//    [self setTitleSuperSerie:[trainingExerciseDictionary valueForKey:@"titulo_super_serie"]];
//    [self setRest:[trainingExerciseDictionary valueForKey:@"rest"]];
//    [self setWeight:[trainingExerciseDictionary valueForKey:@"weight"]];
//    [self setIntensity:[trainingExerciseDictionary valueForKey:@"intensity"]];
//    [self setRep:[trainingExerciseDictionary valueForKey:@"rep"]];
//    [self setOrders:[trainingExerciseDictionary valueForKey:@"orders"]];
//    [self setRestSuper:[trainingExerciseDictionary valueForKey:@"rest_super"]];
//    [self setWeightSuper:[trainingExerciseDictionary valueForKey:@"weight_super"]];
//    [self setIntensitySuper:[trainingExerciseDictionary valueForKey:@"intensity_super"]];
//    [self setRepsSuper:[trainingExerciseDictionary valueForKey:@"reps_super"]];
//    [self setOrdersSuper:[trainingExerciseDictionary valueForKey:@"orders_super"]];
    
    self.isSelected = @NO;
}

@end
