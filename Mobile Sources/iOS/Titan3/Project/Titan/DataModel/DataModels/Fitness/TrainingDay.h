//
//  TrainingDay.h
//  Titan
//
//  Created by Manuel Manzanera on 7/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "TrainingDayContent.h"

@interface TrainingDay : RLMObject

@property NSString *planId;
@property NSNumber <RLMInt> *code;
@property NSNumber <RLMInt> *restDay;
@property RLMArray <TrainingDayContent *><TrainingDayContent> *trainingDayContent;
@property NSNumber <RLMInt> *order;

+ (BOOL)deleteAllTrainingDays;
+ (BOOL)deleteTrainingDay:(TrainingDay *)trainingDay;
+ (TrainingDay *)copyTrainingDay:(TrainingDay *)trainingDay;
+ (NSMutableArray *)copyTrainingDay:(TrainingDay *)trainingDay numberOfTime:(NSInteger)numberOfReplies;

@end

RLM_ARRAY_TYPE(TrainingDay)
