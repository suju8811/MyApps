//
//  TrainingDayContent.m
//  Titan
//
//  Created by Manuel Manzanera on 7/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "TrainingDayContent.h"
#import "Training.h"
#import "AppContext.h"

@implementation TrainingDayContent

//+ (NSString *)primaryKey {
//    return @"code";
//}
+ (BOOL)deleteAllTrainingDayContents {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[TrainingDayContent allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)deleteTrainingDayContent:(TrainingDayContent *)trainingDayContent {
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObject:trainingDayContent];
    
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if (error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (TrainingDayContent *)copyTrainingDayContent:(TrainingDayContent *)trainingDayContent{

    TrainingDayContent *copyTrainingDayContent = [[TrainingDayContent alloc] init];
    
    [copyTrainingDayContent setStart:trainingDayContent.start];
    [copyTrainingDayContent setEnd:trainingDayContent.end];
    [copyTrainingDayContent setOrder:trainingDayContent.order];
    
//    Training *copyTraining = [Training copyTraining:trainingDayContent.training];
    Training *copyTraining = trainingDayContent.training;
    
    [copyTrainingDayContent setIsClone:[NSNumber numberWithBool:TRUE]];
    [copyTrainingDayContent setTraining:copyTraining];
    [copyTrainingDayContent setTrainingId:copyTraining.serverTrainingId];
    
    return copyTrainingDayContent;

}

@end
