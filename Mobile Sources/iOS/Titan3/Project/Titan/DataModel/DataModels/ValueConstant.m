//
//  ValueConstant.m
//  Titan
//
//  Created by Manuel Manzanera on 24/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "ValueConstant.h"
#import "AppContext.h"

@implementation ValueConstant

+ (NSString *)primaryKey {
    return @"valueConstantId";
}

+ (BOOL)saveConstants:(NSMutableArray *)constants{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:constants];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return FALSE;
    }
    
    return YES;
}

+ (NSDate *)lastUpdateDate{
    RLMResults<ValueConstant *> *valueConstants = [[ValueConstant allObjects] sortedResultsUsingKeyPath:@"updatedAt" ascending:FALSE];
    
    if(valueConstants.count > 0)
        return [[valueConstants objectAtIndex:0] updatedAt];
    else
        return [NSDate dateWithTimeIntervalSince1970:0];
}

+ (ValueConstant *)getValueConstantsWithName:(NSString *)name{
    RLMResults<ValueConstant *> *valueConstants = [ValueConstant objectsWhere:[NSString stringWithFormat:@"name = '%@'",name]];
    
    if(valueConstants.count != 0)
        return [valueConstants objectAtIndex:0];
    else
        return nil;
}

@end
