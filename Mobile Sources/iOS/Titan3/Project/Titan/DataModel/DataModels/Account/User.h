//
//  User.h
//  Titan
//
//  Created by Manuel Manzanera on 13/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface User : RLMObject

@property NSString *userId;
@property NSString *name;
@property NSString *lastName;
@property NSString *mail;
@property NSString *password;
@property NSString *gender;
@property NSNumber <RLMBool> *privacy;
@property NSNumber <RLMInt> *weight;
@property NSNumber <RLMInt> *age;
@property NSNumber <RLMFloat> *height;
@property NSNumber <RLMFloat> *bmr;
@property NSString *picture;

@property NSDate *birthDate;
@property NSDate *createdAt;

+ (NSNumber *)calculateUserBMR;
+ (NSNumber *)calculateBMRWithWeight:(NSNumber *)weigth andAge:(NSNumber *)userAge andGender:(NSString *)userGender andHeight:(NSNumber *)height;
+ (NSNumber *)fUser;

@end
