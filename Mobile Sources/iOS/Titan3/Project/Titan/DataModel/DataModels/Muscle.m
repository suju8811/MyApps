//
//  Muscle.m
//  Titan
//
//  Created by Manuel Manzanera on 14/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Muscle.h"
#import "AppContext.h"

@implementation Muscle

+ (NSString *)primaryKey {
    return @"muscleId";
}

+ (BOOL)saveMuscles:(NSMutableArray *)muscles{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:muscles];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return FALSE;
    }
    
    return YES;
}

+ (Muscle *)getMuscleWithId:(NSString *)muscleId{
    RLMResults<Muscle *> *muscles = [Muscle objectsWhere:@"muscleId = %@",muscleId];
    
    if(muscles.count > 0)
        return [muscles objectAtIndex:0];
    else
        return nil;
    
}

+ (RLMResults *)getMuscles{
    RLMResults  <Muscle*> *muscles = [Muscle allObjects];
    
    return muscles;
}


@end
