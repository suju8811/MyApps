//
//  ChatConfiguration.h
//  Titan
//
//  Created by Marcus Lee on 12/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatConfiguration : NSObject

@property (nonatomic, strong) NSString * chatId;
@property (nonatomic, strong) NSString * chatToken;
@property (nonatomic, strong) NSString * chatUserName;
@property (nonatomic, strong) NSString * chatPassword;

- (id)initWithCoder:(NSCoder*)coder;
- (void)encodeWithCoder:(NSCoder*)coder;

@end
