//
//  NotificationSetting.m
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "NotificationSetting.h"

@implementation NotificationSetting

- (id)init {
    self = [super init];
    
    _general = YES;
    _sound = YES;
    
    _trainingInformative = YES;
    _trainingReminder = YES;
    _trainingAlerts = NO;
    
    _nutritionReminder = YES;
    _nutritionAlerts = NO;
    
    _community = YES;
    
    return self;
}

- (id)initWithCoder:(NSCoder*)coder {
    self = [super init];
    
    if (self) {
        _general = [[coder decodeObjectForKey:@"notification_setting_general"] boolValue];
        _sound = [[coder decodeObjectForKey:@"notification_setting_sound"] boolValue];
        
        _trainingInformative = [[coder decodeObjectForKey:@"notification_setting_training_informative"] boolValue];
        _trainingReminder = [[coder decodeObjectForKey:@"notification_setting_training_reminder"] boolValue];
        _trainingAlerts = [[coder decodeObjectForKey:@"notification_setting_training_alerts"] boolValue];
        
        _nutritionReminder = [[coder decodeObjectForKey:@"notification_setting_nutrition_reminder"] boolValue];
        _nutritionAlerts = [[coder decodeObjectForKey:@"notification_setting_nutrition_alerts"] boolValue];
        
        _community = [[coder decodeObjectForKey:@"notification_setting_community"] boolValue];
        }
        
        return self;
}

- (void)encodeWithCoder:(NSCoder*)coder {
    [coder encodeObject:[NSNumber numberWithBool:_general] forKey:@"notification_setting_general"];
    [coder encodeObject:[NSNumber numberWithBool:_sound] forKey:@"notification_setting_sound"];
    
    [coder encodeObject:[NSNumber numberWithBool:_trainingInformative] forKey:@"notification_setting_training_informative"];
    [coder encodeObject:[NSNumber numberWithBool:_trainingReminder] forKey:@"notification_setting_training_reminder"];
    [coder encodeObject:[NSNumber numberWithBool:_trainingAlerts] forKey:@"notification_setting_training_alerts"];
    
    [coder encodeObject:[NSNumber numberWithBool:_nutritionReminder] forKey:@"notification_setting_nutrition_reminder"];
    [coder encodeObject:[NSNumber numberWithBool:_nutritionAlerts] forKey:@"notification_setting_nutrition_alerts"];
    
    [coder encodeObject:[NSNumber numberWithBool:_community] forKey:@"notification_setting_community"];
    
}

@end

