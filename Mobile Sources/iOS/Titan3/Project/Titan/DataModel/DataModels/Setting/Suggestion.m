//
//  Suggestion.m
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Suggestion.h"

@implementation Suggestion

- (id)init {
    self = [super init];
    _title = @"";
    _desc = @"";
    _suggstionOrDoubt = 0;
    return self;
}
@end
