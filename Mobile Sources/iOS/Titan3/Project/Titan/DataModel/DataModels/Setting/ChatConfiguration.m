//
//  ChatConfiguration.m
//  Titan
//
//  Created by Marcus Lee on 12/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "ChatConfiguration.h"

@implementation ChatConfiguration

- (id)init {
    self = [super init];
    
    return self;
}

- (id)initWithCoder:(NSCoder*)coder {
    self = [super init];
    
    if (self) {
        _chatId = [coder decodeObjectForKey:@"chat_configuration_id"];
        _chatToken = [coder decodeObjectForKey:@"chat_configuration_token"];
        _chatUserName = [coder decodeObjectForKey:@"chat_configuration_user_name"];
        _chatPassword = [coder decodeObjectForKey:@"chat_configuration_password"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder*)coder {
    [coder encodeObject:_chatId forKey:@"chat_configuration_id"];
    [coder encodeObject:_chatToken forKey:@"chat_configuration_token"];
    [coder encodeObject:_chatUserName forKey:@"chat_configuration_user_name"];
    [coder encodeObject:_chatPassword forKey:@"chat_configuration_password"];
}

@end
