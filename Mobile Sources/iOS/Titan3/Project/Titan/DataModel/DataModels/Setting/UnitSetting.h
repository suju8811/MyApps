//
//  UnitSetting.h
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnitSetting : NSObject

@property (nonatomic, assign) NSInteger weightUnit;
@property (nonatomic, assign) NSInteger heightUnit;
@property (nonatomic, assign) NSInteger tmbUnit;

- (id)initWithCoder:(NSCoder*)coder;
- (void)encodeWithCoder:(NSCoder*)coder;

@end
