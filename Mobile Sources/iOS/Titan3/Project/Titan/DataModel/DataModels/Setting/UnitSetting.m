//
//  UnitSetting.m
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "UnitSetting.h"

@implementation UnitSetting

- (id)init {
    self = [super init];
    
    _weightUnit = 0;
    _heightUnit = 0;
    _tmbUnit = 0;
    
    return self;
}

- (id)initWithCoder:(NSCoder*)coder {
    self = [super init];
    
    if (self) {
        _weightUnit = [[coder decodeObjectForKey:@"unit_setting_weight"] boolValue];
        _heightUnit = [[coder decodeObjectForKey:@"unit_setting_height"] boolValue];
        _tmbUnit = [[coder decodeObjectForKey:@"unit_setting_tmb"] boolValue];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder*)coder {
    [coder encodeObject:[NSNumber numberWithInteger:_weightUnit] forKey:@"unit_setting_weight"];
    [coder encodeObject:[NSNumber numberWithInteger:_heightUnit] forKey:@"unit_setting_height"];
    [coder encodeObject:[NSNumber numberWithInteger:_tmbUnit] forKey:@"unit_setting_tmb"];
}

@end
