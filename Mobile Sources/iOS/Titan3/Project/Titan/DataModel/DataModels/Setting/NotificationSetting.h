//
//  NotificationSetting.h
//  Titan
//
//  Created by Marcus Lee on 12/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationSetting : NSObject

@property (nonatomic, assign) BOOL general;
@property (nonatomic, assign) BOOL sound;

@property (nonatomic, assign) BOOL trainingInformative;
@property (nonatomic, assign) BOOL trainingReminder;
@property (nonatomic, assign) BOOL trainingAlerts;

@property (nonatomic, assign) BOOL nutritionReminder;
@property (nonatomic, assign) BOOL nutritionAlerts;

@property (nonatomic, assign) BOOL community;

- (id)initWithCoder:(NSCoder*)coder;
- (void)encodeWithCoder:(NSCoder*)coder;

@end
