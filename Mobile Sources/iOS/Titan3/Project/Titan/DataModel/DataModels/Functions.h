//
//  Functions.h
//  Titan
//
//  Created by Manuel Manzanera on 25/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Functions : NSObject

+ (NSNumber *)getFuser:(NSInteger)weigth andGender:(NSString *)gender andBMR:(float)bmr;
+ (NSNumber *)getFReps:(NSInteger)reps;
+ (NSNumber *)getFWeightWithDifficulty:(NSInteger)difficulty andDec:(float)dec;
+ (NSNumber *)getFRestInSeconds:(NSInteger)seconds;

@end
