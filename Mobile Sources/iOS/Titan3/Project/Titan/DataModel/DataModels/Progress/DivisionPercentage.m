//
//  DivisionPercentage.m
//  Titan
//
//  Created by Marcus Lee on 10/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "DivisionPercentage.h"
#import "UtilManager.h"

@implementation DivisionPercentage

- (id)init {
    self = [super init];
    
    _back = 0;
    _lumbares = 45;
    _shoulders = 22;
    _trapecios = 0;
    _chest = 45;
    _triceps = 0;
    _biceps = 22;
    _forearm = 0;
    _abs = 0;
    _gluteo = 45;
    _quadriceps = 45;
    _femoral = 22;
    _twin = 22;
    
    return self;
}

- (void)parseFromDictionary:(NSDictionary*)divisionDictionary {
    self.back = [[divisionDictionary valueForKey:@"ESPALDA"] floatValue];
    self.lumbares = [[divisionDictionary valueForKey:@"LUMBARES"] floatValue];
    self.shoulders = [[divisionDictionary valueForKey:@"HOMBROS"] floatValue];
    self.trapecios = [[divisionDictionary valueForKey:@"TRAPECIOS"] floatValue];
    self.chest = [[divisionDictionary valueForKey:@"PECHO"] floatValue];
    self.triceps = [[divisionDictionary valueForKey:@"TRICEPS"] floatValue];
    self.biceps = [[divisionDictionary valueForKey:@"BICEPS"] floatValue];
    self.forearm = [[divisionDictionary valueForKey:@"ANTEBRAZO"] floatValue];
    self.abs = [[divisionDictionary valueForKey:@"ABDOMINALES"] floatValue];
    self.gluteo = [[divisionDictionary valueForKey:@"GLUTEO"] floatValue];
    self.quadriceps = [[divisionDictionary valueForKey:@"CUADRICEPS"] floatValue];
    self.femoral = [[divisionDictionary valueForKey:@"FEMORAL"] floatValue];
    self.twin = [[divisionDictionary valueForKey:@"GEMELO"] floatValue];
}

@end
