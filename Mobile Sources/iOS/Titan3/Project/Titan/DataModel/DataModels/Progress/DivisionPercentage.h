//
//  DivisionPercentage.h
//  Titan
//
//  Created by Marcus Lee on 10/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DivisionPercentage : NSObject

@property (nonatomic, assign) float back;
@property (nonatomic, assign) float lumbares;
@property (nonatomic, assign) float shoulders;
@property (nonatomic, assign) float trapecios;
@property (nonatomic, assign) float chest;
@property (nonatomic, assign) float triceps;
@property (nonatomic, assign) float biceps;
@property (nonatomic, assign) float forearm;
@property (nonatomic, assign) float abs;
@property (nonatomic, assign) float gluteo;
@property (nonatomic, assign) float quadriceps;
@property (nonatomic, assign) float femoral;
@property (nonatomic, assign) float twin;

- (void)parseFromDictionary:(NSDictionary*)divisionDictionary;

@end
