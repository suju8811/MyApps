//
//  Ingredient.m
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Ingredient.h"
#import "AppContext.h"

@implementation Ingredient

+ (NSString *)primaryKey{
    return @"ingredientId";
}

+ (BOOL)deleteAllIngredients {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[Ingredient allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

@end
