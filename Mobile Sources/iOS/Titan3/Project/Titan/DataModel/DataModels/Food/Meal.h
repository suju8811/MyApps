//
//  Meal.h
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseElement.h"
#import "Ingredient.h"
#import "Tag.h"


RLM_ARRAY_TYPE(Meal)

@interface Meal : BaseElement

@property NSString *mealId;                 //_id
@property NSString *serverMealId;
@property NSDate *createdAt;                //createdAt
@property NSDate *updatedAt;                //updatedAt
@property NSString *owner;                  //Owner
@property NSString *title;                  //title
@property NSString *content;                //content
@property NSString *notes;                  //notes
@property NSString *privacity;
@property NSString *image;                  //image

@property RLMArray <Tag *><Tag> *tags;

///////////////////////////////// Attributes

@property NSNumber<RLMInt> *calories;                       //calories
@property NSNumber<RLMInt> *carbs;                          //carbs
@property NSNumber<RLMInt> *conversionFactor;               //conversionFactor
@property NSString *conversionString;                       //conversionString
@property NSNumber<RLMInt> *fats;                           //fats
@property NSNumber<RLMInt> *fiber;                          //fiber
@property NSString *measureUnit;                            //measureUnit
@property NSNumber<RLMFloat> *monoUnsaturatedFats;          //monoUnsaturatedFats
@property NSNumber<RLMFloat> *polyUnsaturatedFats;          //polyUnsaturatedFats
@property NSNumber<RLMInt> *proteins;                       //proteins
@property NSNumber<RLMFloat> *sodium;                       //sodium
@property NSNumber<RLMInt> *sugar;                          //sugar
@property NSNumber<RLMFloat> *water;                        //water

@property NSNumber <RLMInt> *agpAgs;                        //agpAgs
@property NSNumber <RLMFloat> *agpAgsAgm;                   //agpAgsAgm
@property NSNumber <RLMInt> *alcohol;                       //alcohol
@property NSNumber <RLMInt> *bccas;                         //bccas
@property NSNumber <RLMInt> *betaCaronete;                  //betaCarotene
@property NSNumber <RLMInt> *calcium;                       //calcium
@property NSNumber <RLMFloat> *cholesterol;                 //cholesterol
@property NSNumber <RLMFloat> *folicAcid;                   //folicAcid
@property NSNumber <RLMFloat> *glutamine;                   //glutamine
@property NSNumber <RLMFloat> *glycemicIndex;               //glycemicIndex
@property NSNumber <RLMFloat> *iodine;                      //iodine
@property NSNumber <RLMFloat> *iron;                        //iron
@property NSNumber <RLMFloat> *magnesium;                   //magnesium
@property NSNumber <RLMFloat> *niacin;                      //niacin
@property NSNumber <RLMFloat> *omega3;
@property NSNumber <RLMFloat> *omega6;
@property NSNumber <RLMFloat> *phosphorus;
@property NSNumber <RLMFloat> *potasium;
@property NSNumber <RLMFloat> *retynol;
@property NSNumber <RLMFloat> *saturedFats;
@property NSNumber <RLMFloat> *selenium;
@property NSNumber <RLMFloat> *vitA;
@property NSNumber <RLMFloat> *vitB1;
@property NSNumber <RLMFloat> *vitB2;
@property NSNumber <RLMFloat> *vitB12;
@property NSNumber <RLMFloat> *vitB6;
@property NSNumber <RLMFloat> *vitC;
@property NSNumber <RLMFloat> *vitD;
@property NSNumber <RLMFloat> *zinc;

@property NSNumber <RLMBool> *isFavourite;                             // Property to asign if is favourite
@property NSNumber <RLMBool> *isShared;                                // Property to asign if it is shared
@property NSNumber <RLMBool> *isSelected;
@property NSNumber <RLMBool> *isCopy;
@property NSNumber <RLMInt> *order;

@property RLMArray<Meal*><Meal>* mergedMeals;
@property NSNumber <RLMBool> *childOfMergedMeal;

@property RLMArray <Ingredient *><Ingredient> *ingredients;

+ (BOOL)deleteAllMeals;

+ (void)saveMergedMealIdsToPreference;

+ (BOOL)saveMeals:(NSMutableArray *)meals;
+ (BOOL)saveMeal:(Meal *)meal;

+ (RLMResults *)getMeals;
+ (RLMResults *)getMyMeals;
+ (RLMResults *)getMyMealsWithKeyword:(NSString*)keyword;
+ (RLMResults *)getOtherMeals;
+ (RLMResults *)getOtherMealsWithKeyword:(NSString*)keyword;
+ (RLMResults *)getMealsWithOwner:(NSString *)owner;
+ (RLMResults *)getFavouritesMeals;
+ (RLMResults *)getFavouritesMealsWithKeyword:(NSString*)keyword;
+ (RLMResults *)getSharedMeals;
+ (RLMResults *)getSharedMealsWithKeyword:(NSString*)keyword;
+ (Meal *)mealWithId:(NSString *)mealId;
+ (Meal *)mealWithServerId:(NSString *)serverId;
+ (Meal*)mergeMeal:(Meal*)meal1 WithMeal:(Meal*)meal2;

+ (BOOL)deleteMeal:(Meal *)meal;
+ (BOOL)deleteMealsIngredients:(Meal *)meal;
+ (BOOL)canDeleteMeal:(Meal *)meal;

+ (Meal *)duplicateMeal:(Meal *)meal;

+ (NSDictionary *)mealDictionary:(Meal *)meal;

+ (NSMutableArray *)getMealTags:(Meal *)meal;
+ (NSInteger)getMealQuantity:(Meal*)meal;
+ (float)getKCalInMeal:(Meal *)meal;
+ (NSInteger)getMealProtein:(Meal *)meal;
+ (NSInteger)getMealCarb:(Meal *)meal;
+ (NSInteger)getMealFats:(Meal *)meal;
+ (NSInteger)getMealSugar:(Meal *)meal;
+ (float)getMealSaturedFats:(Meal *)meal;
+ (float)getMealMonoUnsaturedFats:(Meal *)meal;
+ (float)getMealPolyUnsaturedFats:(Meal *)meal;
+ (float)getMealFiber:(Meal *)meal;
+ (float)getMealBccas:(Meal *)meal;
+ (float)getMealGlutamine:(Meal *)meal;
+ (float)getMealOmega3:(Meal *)meal;
+ (float)getMealOmega6:(Meal *)meal;
+ (float)getMealWater:(Meal *)meal;
+ (float)getMealAlcohol:(Meal *)meal;
+ (float)getMealIron:(Meal *)meal;
+ (float)getMealIodine:(Meal *)meal;
+ (float)getMealMagnesium:(Meal *)meal;
+ (float)getMealZinc:(Meal *)meal;
+ (float)getMealSelenium:(Meal *)meal;
+ (float)getMealSodium:(Meal *)meal;
+ (float)getMealPotasium:(Meal *)meal;
+ (float)getMealPhosphorus:(Meal *)meal;
+ (float)getMealVitB1:(Meal *)meal;
+ (float)getMealVitB2:(Meal *)meal;
+ (float)getMealNiacin:(Meal *)meal;
+ (float)getMealVitA:(Meal *)meal;
+ (float)getMealFolicAcid:(Meal *)meal;
+ (float)getMealVitB12:(Meal *)meal;
+ (float)getMealVitC:(Meal *)meal;
+ (float)getMealVitD:(Meal *)meal;

+ (NSDate *)lastUpdateDate;

- (void)parseFromDictionary:(NSDictionary*)mealDictionary;

@end
