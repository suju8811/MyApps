//
//  Ingredient.h
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "Aliment.h"

@interface Ingredient : RLMObject

@property NSString *ingredientId;       //mealsId + AlimentId
@property Aliment *aliment;
@property NSNumber <RLMInt> *quantity;
@property NSString *quantityUnit;
@property NSString *alimentId;
@property NSNumber <RLMInt> *order;

+ (BOOL)deleteAllIngredients;

@end

RLM_ARRAY_TYPE(Ingredient)
