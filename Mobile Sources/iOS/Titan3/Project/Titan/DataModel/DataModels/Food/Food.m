//
//  Food.m
//  Titan
//
//  Created by Manuel Manzanera on 20/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Food.h"
#import "AppContext.h"

@implementation Food

+ (NSString *)primaryKey {
    return @"foodId";
}

+ (BOOL)deleteAllFoods {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[Food allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)saveFoods:(NSMutableArray *)foods{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:foods];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return FALSE;
    }
    
    return TRUE;
}

+ (RLMResults *)getFoods {
    
    RLMResults<Food *> *foods = [Food allObjects];
    RLMResults<Food *> *sortedFoods = [ foods sortedResultsUsingKeyPath:@"title" ascending:YES];

    return sortedFoods;
    
}

@end
