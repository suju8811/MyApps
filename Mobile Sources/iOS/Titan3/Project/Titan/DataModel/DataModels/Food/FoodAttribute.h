//
//  FoodAttribute.h
//  Titan
//
//  Created by Marcus Lee on 2/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface FoodAttribute : RLMObject

@property NSString * name;
@property NSNumber<RLMFloat> *value;

+ (BOOL)deleteAllFoodAttributes;

@end

RLM_ARRAY_TYPE(FoodAttribute)
