//
//  Food.h
//  Titan
//
//  Created by Manuel Manzanera on 20/2/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface Food : RLMObject

@property NSString *foodId;
@property NSNumber<RLMInt> *calories;
@property NSNumber<RLMInt> *carbs;
@property NSDate *createdAt;
@property NSDate *updatedAt;
@property NSNumber<RLMInt> *fats;
@property NSString *owner;
@property NSNumber<RLMInt> *proteins;
@property NSString *title;
@property NSNumber<RLMInt> *sugar;
@property NSNumber<RLMInt> *fiber;
@property NSString *measureUnit;
@property NSNumber<RLMFloat> *monoUnsaturatedFats;
@property NSNumber<RLMFloat> *polyUnsaturatedFats;
@property NSNumber<RLMFloat> *unsaturatedFats;
@property NSNumber<RLMFloat> *sodium;
@property NSNumber<RLMFloat> *water;
@property NSString *content;

+ (BOOL)deleteAllFoods;
+ (BOOL)saveFoods:(NSMutableArray *)foods;
+ (RLMResults *)getFoods;

@end
