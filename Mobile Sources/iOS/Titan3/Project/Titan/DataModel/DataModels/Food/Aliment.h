//
//  Aliment.h
//  Titan
//
//  Created by Manuel Manzanera on 2/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "Tag.h"
#import "FoodAttribute.h"

@interface Aliment : RLMObject

@property NSString *alimentId;
@property NSString *supplementId;
@property NSString *title;
@property NSNumber<RLMFloat> *calories;                       //calories
@property NSNumber<RLMFloat> *carbs;                          //carbs
@property NSNumber<RLMFloat> *conversionFactor;               //conversionFactor
@property NSString *conversionString;                       //conversionString
@property NSDate *createdAt;
@property NSDate *updatedAt;
@property NSString *owner;
@property NSNumber<RLMFloat> *fats;                           //fats
@property NSNumber<RLMFloat> *fiber;                          //fiber
@property NSString *measureUnit;                            //measureUnit
@property NSNumber<RLMFloat> *monoUnsaturatedFats;          //monoUnsaturatedFats
@property NSNumber<RLMFloat> *polyUnsaturatedFats;          //polyUnsaturatedFats
@property NSNumber<RLMFloat> *proteins;                       //proteins
@property NSNumber<RLMFloat> *sodium;                       //sodium
@property NSNumber<RLMInt> *sugar;                          //sugar
@property NSNumber<RLMFloat> *water;                        //water

@property NSNumber <RLMFloat> *agpAgs;                        //agpAgs
@property NSNumber <RLMFloat> *agpAgsAgm;                   //agpAgsAgm
@property NSNumber <RLMFloat> *alcohol;                       //alcohol
@property NSNumber <RLMFloat> *bccas;                         //bccas
@property NSNumber <RLMFloat> *betaCaronete;                  //betaCarotene
@property NSNumber <RLMFloat> *calcium;                       //calcium
@property NSNumber <RLMFloat> *cholesterol;                 //cholesterol
@property NSNumber <RLMFloat> *folicAcid;                   //folicAcid
@property NSNumber <RLMFloat> *glutamine;                   //glutamine
@property NSNumber <RLMFloat> *glycemicIndex;               //glycemicIndex
@property NSNumber <RLMFloat> *iodine;                      //iodine
@property NSNumber <RLMFloat> *iron;                        //iron
@property NSNumber <RLMFloat> *magnesium;                   //magnesium
@property NSNumber <RLMFloat> *niacin;                      //niacin
@property NSNumber <RLMFloat> *omega3;
@property NSNumber <RLMFloat> *omega6;
@property NSNumber <RLMFloat> *phosphorus;
@property NSNumber <RLMFloat> *potasium;
@property NSNumber <RLMFloat> *retynol;
@property NSNumber <RLMFloat> *saturedFats;
@property NSNumber <RLMFloat> *selenium;
@property NSNumber <RLMFloat> *vitA;
@property NSNumber <RLMFloat> *vitB1;
@property NSNumber <RLMFloat> *vitB2;
@property NSNumber <RLMFloat> *vitB12;
@property NSNumber <RLMFloat> *vitB6;
@property NSNumber <RLMFloat> *vitC;
@property NSNumber <RLMFloat> *vitD;
@property NSNumber <RLMFloat> *zinc;

@property RLMArray <FoodAttribute *><FoodAttribute> *secondaryAttributes;

@property NSString *image;

@property NSNumber<RLMFloat> *unsaturatedFats;
@property NSString *content;

@property NSString *tag1;
@property NSString *tag2;
@property NSString *group;

@property NSNumber <RLMBool> *isSelected;
@property NSNumber <RLMBool> *isSupplement;
@property NSString *privacity;

+ (BOOL)deleteAllAliments;
+ (BOOL)saveAliments:(NSMutableArray *)aliments;
+ (RLMResults *)getAliments;
+ (RLMResults *)getAlimentsWithKeyword:(NSString*)keyword;
+ (RLMResults *)getSupplements;
+ (RLMResults *)getSupplementsWithKeyword:(NSString*)keyword;
+ (Aliment *)getAlimentWithId:(NSString *)alimentId;
+ (Aliment *)getAlimentWithName:(NSString *)title;
+ (BOOL)deleteAliment:(Aliment *)aliment;

+ (NSDate *)lastUpdateDate;

+ (Aliment*)copyAliment:(Aliment*)aliment;

- (void)parseFromDictionary:(NSDictionary*)alimentDictionary;

@end

RLM_ARRAY_TYPE(Aliment)
