//
//  NutritionDay.m
//  Titan
//
//  Created by Manuel Manzanera on 12/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "NutritionDay.h"
#import "AppContext.h"

@implementation NutritionDay

+ (BOOL)deleteAllNutritionDays {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[NutritionDay allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}


+ (NutritionDay *)copyNutritionDay:(NutritionDay *)nutritionDay {
    NutritionDay *copyTrainingDay = [[NutritionDay alloc] init];
    
//    [copyTrainingDay setRestDay:nutritionDay.restDay];
//
//    if(copyTrainingDay.restDay.intValue != 1){
//
//        for(TrainingDayContent *trainingDaycontent in trainingDay.trainingDayContent){
//
//            TrainingDayContent *copyTrainingDayContent = [TrainingDayContent copyTrainingDayContent:trainingDaycontent];
//            [copyTrainingDay.trainingDayContent addObject:copyTrainingDayContent];
//        }
//    }
    
    return copyTrainingDay;
}

+ (NSArray<NutritionDay*>*)copyNutritionDay:(NutritionDay *)nutritionDay numberOfTime:(NSInteger)numberOfReplies {
    
    NSMutableArray *replies = [[NSMutableArray alloc] initWithCapacity:numberOfReplies];
    
//    for(int i = 0; i<numberOfReplies ; i++){
//        TrainingDay *copyTrainingDay = [[TrainingDay alloc] init];
//
//        [copyTrainingDay setRestDay:trainingDay.restDay];
//
//        if(copyTrainingDay.restDay.intValue != 1){
//
//            for(TrainingDayContent *trainingDaycontent in trainingDay.trainingDayContent){
//
//                TrainingDayContent *copyTrainingDayContent = [TrainingDayContent copyTrainingDayContent:trainingDaycontent];
//                [copyTrainingDay.trainingDayContent addObject:copyTrainingDayContent];
//            }
//        }
//
//        [replies addObject:copyTrainingDay];
//    }
    
    return replies;
}

@end
