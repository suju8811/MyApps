//
//  FoodAttribute.m
//  Titan
//
//  Created by Marcus Lee on 2/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "FoodAttribute.h"
#import "AppContext.h"

@implementation FoodAttribute

+ (BOOL)deleteAllFoodAttributes {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[FoodAttribute allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

@end
