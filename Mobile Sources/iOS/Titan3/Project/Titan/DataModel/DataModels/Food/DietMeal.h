//
//  DietMeal.h
//  Titan
//
//  Created by Marcus Lee on 25/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import <Realm/Realm.h>
#import "Meal.h"

@interface DietMeal : RLMObject

@property NSString * dietMealId;
@property NSNumber<RLMInt>* order;
@property RLMArray <Meal *><Meal> *meals;
@property NSString * startTime;

+ (BOOL)saveDietMeal:(DietMeal *)dietMeal;
+ (BOOL)deleteAllDietMeals;
+ (BOOL)deleteDietMeal:(DietMeal *)dietMeal;
+ (DietMeal *)duplicateMeal:(DietMeal *)dietMeal;
- (void)parseFromDictionary:(NSDictionary*)dietMealDictionary;

@end

RLM_ARRAY_TYPE(DietMeal)
