//
//  Aliment.m
//  Titan
//
//  Created by Manuel Manzanera on 2/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Aliment.h"
#import "AppContext.h"
#import "AlimentAddExecutive.h"
#import "UtilManager.h"

@implementation Aliment

+ (NSString *)primaryKey {
    return @"alimentId";
}

+ (BOOL)deleteAllAliments {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[Aliment allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)saveAliments:(NSMutableArray *)aliments {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:aliments];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return FALSE;
    }
    
    return TRUE;
}

+ (RLMResults *)getAliments {
    RLMResults<Aliment *> *aliments = [Aliment objectsWhere:@"isSupplement = FALSE"];
    RLMResults<Aliment *> *sortedAliments = [aliments sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedAliments;
}

+ (RLMResults *)getAlimentsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Aliment getAliments];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"isSupplement = FALSE AND (title LIKE '*%@*' OR content LIKE '*%@*')", keyword, keyword];
    RLMResults<Aliment *> *aliments = [Aliment objectsWhere:predicateString];
    RLMResults<Aliment *> *sortedAliments = [aliments sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedAliments;
}

+ (RLMResults *)getSupplements {
    RLMResults<Aliment *> *aliments = [Aliment objectsWhere:@"isSupplement = TRUE"];
    RLMResults<Aliment *> *sortedAliments = [aliments sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedAliments;
}

+ (RLMResults *)getSupplementsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Aliment getSupplements];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"isSupplement = TRUE AND (title LIKE '*%@*' OR content LIKE '*%@*')", keyword, keyword];
    RLMResults<Aliment *> *aliments = [Aliment objectsWhere:predicateString];
    RLMResults<Aliment *> *sortedAliments = [aliments sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedAliments;
}

+ (Aliment *)getAlimentWithId:(NSString *)alimentId{
    RLMResults<Aliment *> *aliments = [Aliment objectsWhere:[NSString stringWithFormat:@"alimentId = '%@'",alimentId]];
    
    if(aliments.count != 0)
        return [aliments objectAtIndex:0];
    else
        return nil;
}

+ (Aliment *)getAlimentWithName:(NSString *)title{
    RLMResults<Aliment *> *aliments = [Aliment objectsWhere:[NSString stringWithFormat:@"title = '%@'",title]];
    
    if(aliments.count != 0)
        return [aliments objectAtIndex:0];
    else
        return nil;
}

+ (BOOL)deleteAliment:(Aliment *)aliment {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObject:aliment];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    return YES;
}

+ (NSDate *)lastUpdateDate{
    RLMResults<Aliment *> *aliments = [[Aliment allObjects] sortedResultsUsingKeyPath:@"updatedAt" ascending:FALSE];
    
    if(aliments.count > 0)
        return [[aliments objectAtIndex:0] updatedAt];
    else
        return [NSDate dateWithTimeIntervalSince1970:0];
}

+ (Aliment*)copyAliment:(Aliment*)aliment {
    Aliment * newAliment = [[Aliment alloc] init];
    
    newAliment.supplementId = aliment.supplementId;
    newAliment.title = aliment.title;
    newAliment.content = aliment.content;
    newAliment.conversionString = aliment.conversionString;
    newAliment.conversionFactor = aliment.conversionFactor;
    newAliment.calories = aliment.calories;
    newAliment.proteins = aliment.proteins;
    newAliment.carbs = aliment.carbs;
    newAliment.fiber = aliment.fiber;
    newAliment.fats = aliment.fats;
    newAliment.saturedFats = aliment.saturedFats;
    newAliment.monoUnsaturatedFats = aliment.monoUnsaturatedFats;
    newAliment.polyUnsaturatedFats = aliment.polyUnsaturatedFats;
    newAliment.agpAgs = aliment.agpAgs;
    newAliment.agpAgsAgm = aliment.agpAgsAgm;
    newAliment.cholesterol = aliment.cholesterol;
    newAliment.alcohol = aliment.alcohol;
    newAliment.water = aliment.water;
    newAliment.sodium = aliment.sodium;
    newAliment.sugar = aliment.sugar;
    newAliment.calcium = aliment.calcium;
    newAliment.iron = aliment.iron;
    newAliment.iodine = aliment.iodine;
    newAliment.magnesium = aliment.magnesium;
    newAliment.zinc = aliment.zinc;
    newAliment.selenium = aliment.selenium;
    newAliment.potasium = aliment.potasium;
    newAliment.phosphorus = aliment.phosphorus;
    newAliment.vitB1 = aliment.vitB1;
    newAliment.vitB2 = aliment.vitB2;
    newAliment.niacin = aliment.niacin;
    newAliment.vitB6 = aliment.vitB6;
    newAliment.folicAcid = aliment.folicAcid;
    newAliment.vitB12 = aliment.vitB12;
    newAliment.vitC = aliment.vitC;
    newAliment.retynol = aliment.retynol;
    newAliment.betaCaronete = aliment.betaCaronete;
    newAliment.vitA = aliment.vitA;
    newAliment.vitD = aliment.vitD;
    newAliment.glycemicIndex = aliment.glycemicIndex;
    newAliment.bccas = aliment.bccas;
    newAliment.glutamine = aliment.glutamine;
    newAliment.omega3 = aliment.omega3;
    newAliment.omega6 = aliment.omega6;
    newAliment.tag1 = aliment.tag1;
    newAliment.tag2 = aliment.tag2;
    newAliment.group = aliment.group;
    newAliment.image = aliment.image;
    newAliment.privacity = aliment.privacity;
    newAliment.secondaryAttributes = aliment.secondaryAttributes;
    
    return newAliment;
}

- (void)parseFromDictionary:(NSDictionary*)alimentDictionary {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    if ([alimentDictionary valueForKey:@"code"]) {
        [self setAlimentId:[alimentDictionary valueForKey:@"code"]];
    }
    else {
        self.alimentId = [NSString stringWithFormat:@"s%@", [alimentDictionary valueForKey:@"id"]];
        [self setSupplementId:[alimentDictionary valueForKey:@"id"]];
    }
    
    NSString * dateString = [alimentDictionary valueForKey:@"timestamp"];
    [self setCreatedAt:[formatter dateFromString:dateString]];
    [self setUpdatedAt:[formatter dateFromString:dateString]];
    
    if ([alimentDictionary valueForKey:@"nombre"]) {
        [self setTitle:[alimentDictionary valueForKey:@"nombre"]];
    }
    else {
        [self setTitle:[alimentDictionary valueForKey:@"titulo"]];
    }
    [self setContent:[alimentDictionary valueForKey:@"descripcion"]];
    [self setMeasureUnit:[alimentDictionary valueForKey:@"medida"]];
    if ([alimentDictionary valueForKey:@"cadena_conversion"]) {
        [self setConversionString:[alimentDictionary valueForKey:@"cadena_conversion"]];
    }
    else {
        [self setConversionString:[alimentDictionary valueForKey:@"conversion"]];
    }
    self.conversionFactor = [UtilManager floatFromString:[alimentDictionary valueForKey:@"factor_conversion"]];
    self.calories = [UtilManager floatFromString:[alimentDictionary valueForKey:@"calorias"]];
    self.proteins = [UtilManager floatFromString:[alimentDictionary valueForKey:@"proteinas"]];
    self.carbs = [UtilManager floatFromString:[alimentDictionary valueForKey:@"carbohidratos"]];
    self.fiber = [UtilManager floatFromString:[alimentDictionary valueForKey:@"fibra"]];
    self.fats = [UtilManager floatFromString:[alimentDictionary valueForKey:@"grasas"]];
    self.saturedFats = [UtilManager floatFromString:[alimentDictionary valueForKey:@"grasas_saturadas"]];
    self.monoUnsaturatedFats = [UtilManager floatFromString:[alimentDictionary valueForKey:@"grasas_monoinsaturadas"]];
    self.polyUnsaturatedFats  = [UtilManager floatFromString:[alimentDictionary valueForKey:@"grasas_poliinsaturadas"]];
    self.agpAgs = [UtilManager floatFromString:[alimentDictionary valueForKey:@"agp_ags"]];
    self.agpAgsAgm = [UtilManager floatFromString:[alimentDictionary valueForKey:@"agp_ags_agm"]];
    self.cholesterol = [UtilManager floatFromString:[alimentDictionary valueForKey:@"colesterol"]];
    self.alcohol = [UtilManager floatFromString:[alimentDictionary valueForKey:@"alcohol"]];
    self.water = [UtilManager floatFromString:[alimentDictionary valueForKey:@"agua"]];
    self.sodium = [UtilManager floatFromString:[alimentDictionary valueForKey:@"sodio"]];
    self.sugar = [UtilManager intFromString:[alimentDictionary valueForKey:@"azucar"]];
    self.calcium = [UtilManager floatFromString:[alimentDictionary valueForKey:@"calcio"]];
    self.iron = [UtilManager floatFromString:[alimentDictionary valueForKey:@"hierro"]];
    self.iodine = [UtilManager floatFromString:[alimentDictionary valueForKey:@"yodo"]];
    self.magnesium = [UtilManager floatFromString:[alimentDictionary valueForKey:@"magnesio"]];
    self.zinc = [UtilManager floatFromString:[alimentDictionary valueForKey:@"zinc"]];
    self.selenium = [UtilManager floatFromString:[alimentDictionary valueForKey:@"selenio"]];
    self.potasium = [UtilManager floatFromString:[alimentDictionary valueForKey:@"potasio"]];
    self.phosphorus = [UtilManager floatFromString:[alimentDictionary valueForKey:@"fosforo"]];
    self.vitB1 = [UtilManager floatFromString:[alimentDictionary valueForKey:@"v_b1"]];
    self.vitB2 = [UtilManager floatFromString:[alimentDictionary valueForKey:@"v_b2"]];
    self.niacin = [UtilManager floatFromString:[alimentDictionary valueForKey:@"niacina"]];
    self.vitB6 = [UtilManager floatFromString:[alimentDictionary valueForKey:@"v_b6"]];
    self.folicAcid = [UtilManager floatFromString:[alimentDictionary valueForKey:@"acido_folico"]];
    self.vitB12 = [UtilManager floatFromString:[alimentDictionary valueForKey:@"v_b12"]];
    self.vitC = [UtilManager floatFromString:[alimentDictionary valueForKey:@"v_c"]];
    self.retynol = [UtilManager floatFromString:[alimentDictionary valueForKey:@"retinol"]];
    self.betaCaronete = [UtilManager floatFromString:[alimentDictionary valueForKey:@"beta_caroteno"]];
    self.vitA = [UtilManager floatFromString:[alimentDictionary valueForKey:@"v_a"]];
    self.vitD = [UtilManager floatFromString:[alimentDictionary valueForKey:@"v_d"]];
    self.glycemicIndex = [UtilManager floatFromString:[alimentDictionary valueForKey:@"indice_glucemico"]];
    self.bccas = [UtilManager floatFromString:[alimentDictionary valueForKey:@"bccas"]];
    self.glutamine = [UtilManager floatFromString:[alimentDictionary valueForKey:@"glutamina"]];
    self.omega3 = [UtilManager floatFromString:[alimentDictionary valueForKey:@"omega3"]];
    self.omega6 = [UtilManager floatFromString:[alimentDictionary valueForKey:@"omega6"]];
    self.tag1 = [alimentDictionary valueForKey:@"etiqueta1"];
    if (self.tag1.length > 0) {
        if (![Tag tagWithTitle:self.tag1]) {
            Tag * tag  = [[Tag alloc] init];
            tag.tagId = [UtilManager uuid];
            if (self.supplementId) {
                tag.type = @"5";
            }
            else {
                tag.type = @"4";
            }
            tag.title = [self.tag1 uppercaseString];
            [Tag saveTag:tag];
        }
    }
    self.tag2 = [alimentDictionary valueForKey:@"etiqueta2"];
    if (self.tag2.length > 0) {
        if (![Tag tagWithTitle:self.tag2]) {
            Tag * tag  = [[Tag alloc] init];
            tag.tagId = [UtilManager uuid];
            if (self.supplementId) {
                tag.type = @"5";
            }
            else {
                tag.type = @"4";
            }
            tag.title = [self.tag2 uppercaseString];
            [Tag saveTag:tag];
        }
    }
    self.group = [alimentDictionary valueForKey:@"etiqueta3"];
    if (self.group.length > 0) {
        if (![Tag tagWithTitle:self.group]) {
            Tag * tag  = [[Tag alloc] init];
            tag.tagId = [UtilManager uuid];
            if (self.supplementId) {
                tag.type = @"5";
            }
            else {
                tag.type = @"4";
            }
            tag.title = [self.group uppercaseString];
            [Tag saveTag:tag];
        }
    }
    self.image = [alimentDictionary valueForKey:@"imagen"];
    self.privacity = [alimentDictionary valueForKey:@"popularidad"];
    
    NSArray *secondaryAttributes = [alimentDictionary valueForKey:@"suplementos_secundarios"];
    if (secondaryAttributes == nil) {
        return;
    }
    
    for(NSDictionary *secondaryAttributeDictionary in secondaryAttributes) {
        FoodAttribute * foodAttribute = [[FoodAttribute alloc] init];
        foodAttribute.name = [secondaryAttributeDictionary valueForKey:@"titulo"];
        foodAttribute.value = @([[secondaryAttributeDictionary valueForKey:@"valor"] floatValue]);
        
        [self.secondaryAttributes addObject:foodAttribute];
    }
}

@end
