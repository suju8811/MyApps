//
//  DietMeal.m
//  Titan
//
//  Created by Marcus Lee on 25/1/18.
//  Copyright © 2018 inup. All rights reserved.
//

#import "DietMeal.h"
#import "AppContext.h"
#import "UtilManager.h"

@implementation DietMeal

+ (NSString *)primaryKey{
    return @"dietMealId";
}

+ (BOOL)deleteAllDietMeals {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[DietMeal allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)deleteDietMeal:(DietMeal *)dietMeal {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObject:dietMeal];
    
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if (error) {
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)saveDietMeal:(DietMeal *)dietMeal {
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:dietMeal];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (DietMeal *)duplicateMeal:(DietMeal *)dietMeal {
    DietMeal * duplicateDietMeal = [[DietMeal alloc] init];
    
    duplicateDietMeal.dietMealId = [UtilManager uuid];
    duplicateDietMeal.order = dietMeal.order;
    for (Meal * meal in dietMeal.meals) {
        [duplicateDietMeal.meals addObject:meal];
    }
    
    return duplicateDietMeal;
}

- (void)parseFromDictionary:(NSDictionary*)dietMealDictionary {
    self.dietMealId = [dietMealDictionary valueForKey:@"id"];
    self.order = [UtilManager intFromString:[dietMealDictionary valueForKey:@"orden"]];
    self.startTime = [dietMealDictionary valueForKey:@"start"];
    NSArray * mealsArray = [dietMealDictionary valueForKey:@"comida"];
    for (NSDictionary * mealDictionary in mealsArray) {
        NSString * mealId = [mealDictionary valueForKey:@"code"];
        Meal * meal = [Meal mealWithServerId:mealId];
        if (meal) {
            [self.meals addObject:meal];
            continue;
        }
        
        meal = [[Meal alloc] init];
        [meal parseFromDictionary:mealDictionary];
    }
}

@end
