//
//  Meal.m
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Meal.h"
#import "AppContext.h"
#import "UtilManager.h"
#import "UserDefaultLibrary.h"

@implementation Meal

+ (NSString *)primaryKey{
    return @"mealId";
}

+ (BOOL)deleteAllMeals {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[Meal allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (void)saveMergedMealIdsToPreference {
    NSMutableArray<Meal*> * childrenOfParents = [[NSMutableArray alloc] init];
    for (Meal * meal in [Meal allObjects]) {
        if (meal.childOfMergedMeal &&
            meal.childOfMergedMeal.boolValue) {
            [childrenOfParents addObject:meal];
        }
    }
    
    [UserDefaultLibrary setWithKey:@"child_meals_for_merge_count" WithObject:[NSNumber numberWithInteger:childrenOfParents.count]];
    for (NSInteger index = 0; index < childrenOfParents.count; index++) {
        NSString * mealServerIdKey = [NSString stringWithFormat:@"child_meals_for_merge_count_%d", (int)index];
        [UserDefaultLibrary setWithKey:mealServerIdKey WithObject:childrenOfParents[index].serverMealId];
    }
}

+ (BOOL)saveMeals:(NSMutableArray *)meals{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:meals];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return FALSE;
    }
    
    return YES;
}

+ (BOOL)saveMeal:(Meal *)meal{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:meal];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (Meal*)mergeMeal:(Meal*)meal1 WithMeal:(Meal*)meal2 {
    Meal * mergedMeal = [[Meal alloc] init];
    mergedMeal.mealId = [NSString stringWithFormat:@"%@+%@", meal1.mealId, meal2.mealId];
    mergedMeal.serverMealId = [NSString stringWithFormat:@"%@+%@", meal1.serverMealId, meal2.serverMealId];
    mergedMeal.title = [NSString stringWithFormat:@"%@+%@", meal1.title, meal2.title];
    mergedMeal.content = [NSString stringWithFormat:@"%@+%@", meal1.content, meal2.content];
    mergedMeal.notes = [NSString stringWithFormat:@"%@+%@", meal1.notes, meal2.notes];
    mergedMeal.privacity = [NSString stringWithFormat:@"%@+%@", meal1.privacity, meal2.privacity];
    [mergedMeal.tags addObjects:meal1.tags];
    [mergedMeal.tags addObjects:meal2.tags];
    
    ///////////////////////////////// Attributes
    mergedMeal.calories = @(meal1.calories.intValue + meal2.calories.intValue);
    mergedMeal.carbs = @(meal1.carbs.intValue + meal2.carbs.intValue);
    mergedMeal.conversionFactor = @(meal1.conversionFactor.intValue + meal2.conversionFactor.intValue);
    mergedMeal.conversionString = [NSString stringWithFormat:@"%@+%@", meal1.conversionString, meal2.conversionString];
    mergedMeal.fats = @(meal1.fats.intValue + meal2.fats.intValue);
    mergedMeal.fiber = @(meal1.fiber.intValue + meal2.fiber.intValue);
    mergedMeal.measureUnit = [NSString stringWithFormat:@"%@+%@", meal1.measureUnit, meal2.measureUnit];
    mergedMeal.monoUnsaturatedFats = @(meal1.monoUnsaturatedFats.floatValue + meal2.monoUnsaturatedFats.floatValue);
    mergedMeal.polyUnsaturatedFats = @(meal1.polyUnsaturatedFats.floatValue + meal2.polyUnsaturatedFats.floatValue);
    mergedMeal.proteins = @(meal1.proteins.intValue + meal2.proteins.intValue);
    mergedMeal.sodium = @(meal1.sodium.floatValue + meal2.sodium.floatValue);
    mergedMeal.sugar = @(meal1.sugar.intValue + meal2.sugar.intValue);
    mergedMeal.water = @(meal1.water.floatValue + meal2.water.floatValue);
    mergedMeal.agpAgs = @(meal1.agpAgs.intValue + meal2.agpAgs.intValue);
    mergedMeal.agpAgsAgm = @(meal1.agpAgsAgm.floatValue + meal2.agpAgsAgm.floatValue);
    mergedMeal.alcohol = @(meal1.alcohol.intValue + meal2.alcohol.intValue);
    mergedMeal.bccas = @(meal1.bccas.intValue + meal2.bccas.intValue);
    mergedMeal.betaCaronete = @(meal1.betaCaronete.intValue + meal2.betaCaronete.intValue);
    mergedMeal.calcium = @(meal1.calcium.intValue + meal2.calcium.intValue);
    mergedMeal.cholesterol = @(meal1.cholesterol.floatValue + meal2.cholesterol.floatValue);
    mergedMeal.folicAcid = @(meal1.folicAcid.floatValue + meal2.folicAcid.floatValue);
    mergedMeal.glutamine = @(meal1.glutamine.floatValue + meal2.glutamine.floatValue);
    mergedMeal.glycemicIndex = @(meal1.glycemicIndex.floatValue + meal2.glycemicIndex.floatValue);
    mergedMeal.iodine = @(meal1.iodine.floatValue + meal2.iodine.floatValue);
    mergedMeal.iron = @(meal1.iron.floatValue + meal2.iron.floatValue);
    mergedMeal.magnesium = @(meal1.magnesium.floatValue + meal2.magnesium.floatValue);
    mergedMeal.niacin = @(meal1.niacin.floatValue + meal2.niacin.floatValue);
    mergedMeal.omega3 = @(meal1.omega3.floatValue + meal2.omega3.floatValue);
    mergedMeal.omega6 = @(meal1.omega6.floatValue + meal2.omega6.floatValue);
    mergedMeal.phosphorus = @(meal1.phosphorus.floatValue + meal2.phosphorus.floatValue);
    mergedMeal.potasium = @(meal1.potasium.floatValue + meal2.potasium.floatValue);
    mergedMeal.retynol = @(meal1.retynol.floatValue + meal2.retynol.floatValue);
    mergedMeal.saturedFats = @(meal1.saturedFats.floatValue + meal2.saturedFats.floatValue);
    mergedMeal.selenium = @(meal1.selenium.floatValue + meal2.selenium.floatValue);
    mergedMeal.vitA = @(meal1.vitA.floatValue + meal2.vitA.floatValue);
    mergedMeal.vitB1 = @(meal1.vitB1.floatValue + meal2.vitB1.floatValue);
    mergedMeal.vitB2 = @(meal1.vitB2.floatValue + meal2.vitB2.floatValue);
    mergedMeal.vitB12 = @(meal1.vitB12.floatValue + meal2.vitB12.floatValue);
    mergedMeal.vitB6 = @(meal1.vitB6.floatValue + meal2.vitB6.floatValue);
    mergedMeal.vitC = @(meal1.vitC.floatValue + meal2.vitC.floatValue);
    mergedMeal.vitD = @(meal1.vitD.floatValue + meal2.vitD.floatValue);
    mergedMeal.zinc = @(meal1.zinc.floatValue + meal2.zinc.floatValue);
    mergedMeal.isFavourite = @NO;
    mergedMeal.owner = meal1.owner;
    mergedMeal.isSelected = @NO;
    mergedMeal.isCopy = @NO;
    mergedMeal.order = @0;
    [mergedMeal.mergedMeals addObject:meal1];
    [mergedMeal.mergedMeals addObject:meal2];
    mergedMeal.childOfMergedMeal = @NO;
    [mergedMeal.ingredients addObjects:meal1.ingredients];
    [mergedMeal.ingredients addObjects:meal2.ingredients];
    
    return mergedMeal;
}

+ (RLMResults *)getMeals{
    
    RLMResults<Meal *> *meals = [Meal objectsWhere:@"isCopy != true AND childOfMergedMeal != true"];
    RLMResults<Meal *> *sortedMeals = [meals sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
    
}

+ (RLMResults *)getMealsWithOwner:(NSString *)owner{
    
    RLMResults<Meal *> *meals = [Meal objectsWhere:@"owner == %@ AND isCopy != true AND childOfMergedMeal != true",owner];
    RLMResults<Meal *> *sortedMeals = [meals sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (RLMResults *)getMyMeals {
    
    RLMResults<Meal *> *meals = [Meal objectsWhere:@"owner == %@ AND isCopy != true AND childOfMergedMeal != true", [AppContext sharedInstance].currentUser.name];
    RLMResults<Meal *> *sortedMeals = [meals sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (RLMResults *)getMyMealsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        
        return [Meal getMyMeals];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"owner == '%@' AND isCopy != true AND childOfMergedMeal != true AND (title LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", [AppContext sharedInstance].currentUser.name, keyword, keyword, keyword];
    RLMResults<Meal *> *meals = [Meal objectsWhere:predicateString];
    RLMResults<Meal *> *sortedMeals = [meals sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (RLMResults *)getOtherMeals {
    
    RLMResults<Meal *> *meals = [Meal objectsWhere:@"owner != %@ AND isCopy != true AND childOfMergedMeal != true", [AppContext sharedInstance].currentUser.name];
    RLMResults<Meal *> *sortedMeals = [meals sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (RLMResults *)getOtherMealsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        
        return [Meal getOtherMeals];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"owner != '%@' AND isCopy != true AND childOfMergedMeal != true AND (title LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", [AppContext sharedInstance].currentUser.name, keyword, keyword, keyword];
    RLMResults<Meal *> *meals = [Meal objectsWhere:predicateString];
    RLMResults<Meal *> *sortedMeals = [meals sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (RLMResults *)getFavouritesMeals {
    RLMResults<Meal *> *meals = [Meal objectsWhere:@"isFavourite = TRUE AND isCopy != true AND childOfMergedMeal != true"];
    RLMResults<Meal *> *sortedMeals = [meals sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (RLMResults *)getFavouritesMealsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Meal getFavouritesMeals];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"isFavourite = TRUE AND isCopy != true AND childOfMergedMeal != true AND (title LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", keyword, keyword, keyword];
    RLMResults<Meal *> *meals = [Meal objectsWhere:predicateString];
    RLMResults<Meal *> *sortedMeals = [meals sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (RLMResults *)getSharedMeals {
    RLMResults<Meal *> *meals = [Meal objectsWhere:@"isShared = TRUE AND isCopy != true AND childOfMergedMeal != true"];
    RLMResults<Meal *> *sortedMeals = [meals sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (RLMResults *)getSharedMealsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Meal getSharedMeals];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"isShared = TRUE AND isCopy != true AND childOfMergedMeal != true AND (title LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", keyword, keyword, keyword];
    RLMResults<Meal *> *meals = [Meal objectsWhere:predicateString];
    RLMResults<Meal *> *sortedMeals = [meals sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (Meal *)mealWithId:(NSString *)mealId{
    
    if(![mealId isKindOfClass:[NSString class]])
        return nil;
    
    RLMResults <Meal *>*meals = [Meal objectsWhere:@"mealId = %@",mealId];
    
    if(meals.count > 0)
        return [meals firstObject];
    else
        return nil;
}

+ (Meal *)mealWithServerId:(NSString *)serverId {
    
    if(![serverId isKindOfClass:[NSString class]])
        return nil;
    
    RLMResults <Meal *>*meals = [Meal objectsWhere:@"serverMealId = %@",serverId];
    
    if(meals.count > 0)
        return [meals firstObject];
    else
        return nil;
}


+ (BOOL)deleteMeal:(Meal *)meal {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObject:meal];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    return YES;
}

+ (BOOL)deleteMealsIngredients:(Meal *)meal{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    for(Ingredient *ingredient in meal.ingredients){
        [realm beginWriteTransaction];
        [realm deleteObject:ingredient];
        NSError *error;
        [realm commitWriteTransaction:&error];
        if(error){
            NSLog(@"%@",error.description);
        }
    }
    
    return YES;
}

+ (BOOL)canDeleteMeal:(Meal *)meal{
    if([meal.owner isEqualToString:[[AppContext sharedInstance] userId]])
        return YES;
    else
        return NO;
}

+ (Meal *)duplicateMeal:(Meal *)meal{

    Meal *duplicateMeal = [[Meal alloc] init];
    
    [duplicateMeal setMealId:[UtilManager uuid]];
    [duplicateMeal setServerMealId:meal.serverMealId];
    [duplicateMeal setTitle:[NSString stringWithFormat:@"%@",meal.title]];
    [duplicateMeal setOwner:[[AppContext sharedInstance] userId]];
    [duplicateMeal setContent:meal.content];
    [duplicateMeal setNotes:meal.notes];
    
    [duplicateMeal setCreatedAt:[NSDate date]];
    [duplicateMeal setUpdatedAt:[NSDate date]];
    
    for(Ingredient *ingredient in meal.ingredients){
    
        Ingredient *newIngredient = [[Ingredient alloc] init];
        [newIngredient setIngredientId:[UtilManager uuid]];
        
        [newIngredient setAliment:ingredient.aliment];
        [newIngredient setAlimentId:ingredient.alimentId];
        [newIngredient setQuantity:ingredient.quantity];
        [newIngredient setQuantityUnit:ingredient.quantityUnit];
        
        [duplicateMeal.ingredients addObject:newIngredient];
    }
    
    return duplicateMeal;
}

+ (NSDictionary *)mealDictionary:(Meal *)meal {
    
    NSMutableDictionary *mealDictionary = [[NSMutableDictionary alloc] init];
    
    [mealDictionary setValue:meal.mealId forKey:@"mealId"];
    [mealDictionary setValue:meal.title forKey:@"title"];
    [mealDictionary setValue:meal.owner forKey:@"owner"];
    [mealDictionary setValue:meal.content forKey:@"content"];
    [mealDictionary setValue:meal.notes forKey:@"note"];
    
    NSMutableArray *ingredients = [[NSMutableArray alloc] initWithCapacity:meal.ingredients.count];
    
    for(Ingredient *ingredient in meal.ingredients){
    
        NSMutableDictionary *ingredientDictionary = [[NSMutableDictionary alloc] init];
        
        [ingredientDictionary setValue:ingredient.quantity forKey:@"quantity"];
        [ingredientDictionary setValue:ingredient.quantityUnit forKey:@"quantityUnit"];
        [ingredientDictionary setValue:ingredient.aliment.alimentId forKey:@"food"];
        
        [ingredients addObject:ingredientDictionary];
    }
    
    [mealDictionary setObject:ingredients forKey:@"ingredients"];
    
    return mealDictionary;
}

+ (NSMutableArray *)getMealTags:(Meal *)meal {
    NSMutableArray *tags = [[NSMutableArray alloc] init];
    
    for (Ingredient *ingredient in meal.ingredients) {
        Aliment * aliment = ingredient.aliment;
        Tag *tag;
        
        if (aliment.tag1) {
            tag = [Tag tagWithTitle:aliment.tag1];
            if (tag) {
                if (![tags containsObject:tag]) {
                    [tags addObject:tag];
                }
            }
        }
        
        if (aliment.tag2) {
            tag = [Tag tagWithTitle:aliment.tag2];
            if (tag) {
                if (![tags containsObject:tag]) {
                    [tags addObject:tag];
                }
            }
        }
        
        if (aliment.group) {
            tag = [Tag tagWithId:aliment.group];
            if (tag) {
                if (![tags containsObject:tag]) {
                    [tags addObject:tag];
                }
            }
        }
    }
    
    return tags;
}

+ (NSInteger)getMealQuantity:(Meal*)meal {
    NSInteger quantity = 0;
    
    for(Ingredient *ingredient in meal.ingredients) {
        quantity = quantity + ingredient.quantity.intValue;
    }
    
    return quantity;
}

+ (float)getKCalInMeal:(Meal *)meal {
    float kCals = 0;
    
    for(Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        kCals = kCals + aliment.calories.floatValue * multiplicator;
    }
    
    return kCals;
    
}

+ (NSInteger)getMealProtein:(Meal *)meal {
    
    NSInteger proteins = 0;
    
    for(Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        proteins = proteins + aliment.proteins.floatValue * multiplicator;
    }
    
    return proteins;
}

+ (NSInteger)getMealCarb:(Meal *)meal {
    NSInteger carbs = 0;
    
    for(Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        carbs = carbs + aliment.carbs.floatValue * multiplicator;
    }
    
    return carbs;
}

+ (NSInteger)getMealFats:(Meal *)meal {
    NSInteger fats = 0;
    
    for(Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        fats = fats + aliment.fats.floatValue * multiplicator;
    }
    
    return fats;
}

+ (NSInteger)getMealSugar:(Meal *)meal {
    NSInteger sugar = 0;
    
    for(Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        sugar = sugar + aliment.sugar.floatValue * multiplicator;
    }
    
    return sugar;
}

+ (float)getMealSaturedFats:(Meal *)meal {
    float saturedFats = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        saturedFats = saturedFats + aliment.saturedFats.floatValue * multiplicator;
    }
    
    return saturedFats;
}

+ (float)getMealMonoUnsaturedFats:(Meal *)meal {
    float monoUnsaturedFats = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        monoUnsaturedFats = monoUnsaturedFats + aliment.monoUnsaturatedFats.floatValue * multiplicator;
    }
    
    return monoUnsaturedFats;
}

+ (float)getMealPolyUnsaturedFats:(Meal *)meal {
    float polyUnsaturedFats = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        polyUnsaturedFats = polyUnsaturedFats + aliment.polyUnsaturatedFats.floatValue * multiplicator;
    }
    
    return polyUnsaturedFats;
}

+ (float)getMealFiber:(Meal *)meal {
    float fiber = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        fiber = fiber + aliment.fiber.floatValue * multiplicator;
    }
    
    return fiber;
}

+ (float)getMealBccas:(Meal *)meal {
    float bccas = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        bccas = bccas + aliment.bccas.floatValue * multiplicator;
    }
    
    return bccas;
}

+ (float)getMealGlutamine:(Meal *)meal {
    float glutamine = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        glutamine = glutamine + aliment.glutamine.floatValue * multiplicator;
    }
    
    return glutamine;
}

+ (float)getMealOmega3:(Meal *)meal {
    float omega3 = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        omega3 = omega3 + aliment.omega3.floatValue * multiplicator;
    }
    
    return omega3;
}

+ (float)getMealOmega6:(Meal *)meal {
    float omega6 = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        omega6 = omega6 + aliment.omega6.floatValue * multiplicator;
    }
    
    return omega6;
}

+ (float)getMealWater:(Meal *)meal {
    float water = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        water = water + aliment.water.floatValue * multiplicator;
    }
    
    return water;
}

+ (float)getMealAlcohol:(Meal *)meal {
    float alcohol = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        alcohol = alcohol + aliment.water.floatValue * multiplicator;
    }
    
    return alcohol;
}

+ (float)getMealIron:(Meal *)meal {
    float iron = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        iron = iron + aliment.iron.floatValue * multiplicator;
    }
    
    return iron;
}

+ (float)getMealIodine:(Meal *)meal {
    float iodine = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        iodine = iodine + aliment.iodine.floatValue * multiplicator;
    }
    
    return iodine;
}

+ (float)getMealMagnesium:(Meal *)meal {
    float magnesium = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        magnesium = magnesium + aliment.magnesium.floatValue * multiplicator;
    }
    
    return magnesium;
}

+ (float)getMealZinc:(Meal *)meal {
    float zinc = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        zinc = zinc + aliment.zinc.floatValue * multiplicator;
    }
    
    return zinc;
}

+ (float)getMealSelenium:(Meal *)meal {
    float selenium = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        selenium = selenium + aliment.selenium.floatValue * multiplicator;
    }
    
    return selenium;
}

+ (float)getMealSodium:(Meal *)meal {
    float sodium = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        sodium = sodium + aliment.sodium.floatValue * multiplicator;
    }
    
    return sodium;
}

+ (float)getMealPotasium:(Meal *)meal {
    float potasium = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        potasium = potasium + aliment.potasium.floatValue * multiplicator;
    }
    
    return potasium;
}

+ (float)getMealPhosphorus:(Meal *)meal {
    float phosphorus = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        phosphorus = phosphorus + aliment.phosphorus.floatValue * multiplicator;
    }
    
    return phosphorus;
}

+ (float)getMealVitB1:(Meal *)meal {
    float vitB1 = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        vitB1 = vitB1 + aliment.vitB1.floatValue * multiplicator;
    }
    
    return vitB1;
}

+ (float)getMealVitB2:(Meal *)meal {
    float vitB2 = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        vitB2 = vitB2 + aliment.vitB2.floatValue * multiplicator;
    }
    
    return vitB2;
}

+ (float)getMealNiacin:(Meal *)meal {
    float niacin = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        niacin = niacin + aliment.niacin.floatValue * multiplicator;
    }
    
    return niacin;
}

+ (float)getMealVitA:(Meal *)meal {
    float vitA = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        vitA = vitA + aliment.vitA.floatValue * multiplicator;
    }
    
    return vitA;
}

+ (float)getMealFolicAcid:(Meal *)meal {
    float folicAcid = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        folicAcid = folicAcid + aliment.folicAcid.floatValue * multiplicator;
    }
    
    return folicAcid;
}

+ (float)getMealVitB12:(Meal *)meal {
    float vitB12 = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        vitB12 = vitB12 + aliment.vitB12.floatValue * multiplicator;
    }
    
    return vitB12;
}

+ (float)getMealVitC:(Meal *)meal {
    float vitC = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        vitC = vitC + aliment.vitC.floatValue * multiplicator;
    }
    
    return vitC;
}

+ (float)getMealVitD:(Meal *)meal; {
    float vitD = 0;
    
    for (Ingredient *ingredient in meal.ingredients) {
        float multiplicator = ingredient.quantity.floatValue / 100;
        Aliment *aliment = ingredient.aliment;
        vitD = vitD + aliment.vitD.floatValue * multiplicator;
    }
    
    return vitD;
}

+ (NSDate *)lastUpdateDate{
    RLMResults<Meal *> *meals = [[Meal allObjects] sortedResultsUsingKeyPath:@"updatedAt" ascending:FALSE];
    
    if(meals.count > 0)
        return [[meals objectAtIndex:0] updatedAt];
    else
        return [NSDate dateWithTimeIntervalSince1970:0];
}

- (void)parseFromDictionary:(NSDictionary*)mealDictionary {
    [super parseFromDictionary:mealDictionary];
    
    self.owner = [mealDictionary valueForKey:@"user"];
    self.folicAcid = [UtilManager floatFromString:[mealDictionary valueForKey:@"acido_folico"]];
    self.agpAgs = [UtilManager intFromString:[mealDictionary valueForKey:@"agp_ags"]];
    self.agpAgsAgm = [UtilManager floatFromString:[mealDictionary valueForKey:@"agp_ags_adm"]];
    self.water = [UtilManager floatFromString:[mealDictionary valueForKey:@"agua"]];
    self.alcohol = [UtilManager intFromString:[mealDictionary valueForKey:@"alcohol"]];
    self.sugar = [UtilManager intFromString:[mealDictionary valueForKey:@"azucar"]];
    self.bccas = [UtilManager intFromString:[mealDictionary valueForKey:@"bccas"]];
    self.betaCaronete = [UtilManager intFromString:[mealDictionary valueForKey:@"beta_caroteno"]];
    self.calcium = [UtilManager intFromString:[mealDictionary valueForKey:@"calcio"]];
    self.calories = [UtilManager intFromString:[mealDictionary valueForKey:@"calorias"]];
    self.carbs = [UtilManager intFromString:[mealDictionary valueForKey:@"carbohidratos"]];
    self.cholesterol = [UtilManager floatFromString:[mealDictionary valueForKey:@"colesterol"]];
    self.content = [mealDictionary valueForKey:@"contenido"];
    self.fiber = [UtilManager intFromString:[mealDictionary valueForKey:@"fibra"]];
    self.phosphorus = [UtilManager floatFromString:[mealDictionary valueForKey:@"fosforo"]];
    self.glycemicIndex = [UtilManager floatFromString:[mealDictionary valueForKey:@"glucemino"]];
    self.glutamine = [UtilManager floatFromString:[mealDictionary valueForKey:@"glutamina"]];
    self.fats = [UtilManager intFromString:[mealDictionary valueForKey:@"grasas"]];
    self.monoUnsaturatedFats = [UtilManager floatFromString:[mealDictionary valueForKey:@"grasas_monoinsaturadas"]];
    self.polyUnsaturatedFats = [UtilManager floatFromString:[mealDictionary valueForKey:@"grasas_poliinsaturadas"]];
    self.saturedFats = [UtilManager floatFromString:[mealDictionary valueForKey:@"grasas_saturadas"]];
    self.iron = [UtilManager floatFromString:[mealDictionary valueForKey:@"hierro"]];
    self.mealId = [mealDictionary valueForKey:@"id"];
    self.serverMealId = [mealDictionary valueForKey:@"id"];
    self.image = [mealDictionary valueForKey:@"imagen"];

    NSArray *ingredients = [mealDictionary valueForKey:@"ingredientes"];

    for(NSDictionary *ingredientDictionary in ingredients){

        Ingredient *ingredient = [[Ingredient alloc] init];
        ingredient.ingredientId = [ingredientDictionary valueForKey:@"id"];
        ingredient.quantity = [UtilManager intFromString:[ingredientDictionary valueForKey:@"cantidad"]];
        ingredient.quantityUnit = [ingredientDictionary valueForKey:@"medida"];
        
        NSArray * alimentDictionary = [ingredientDictionary valueForKey:@"alimento"];
        NSArray * supplementDictionary = [ingredientDictionary valueForKey:@"sup_secundarios"];
        NSString * alimentId = [alimentDictionary valueForKey:@"code"];
        if (supplementDictionary) {
            alimentId = [NSString stringWithFormat:@"s%@", alimentId];
        }
        Aliment *aliment = [Aliment getAlimentWithId:alimentId];
        ingredient.aliment = aliment;
        
        [self.ingredients addObject:ingredient];
    }
    
    self.magnesium = [UtilManager floatFromString:[mealDictionary valueForKey:@"magnesio"]];
    self.niacin = [UtilManager floatFromString:[mealDictionary valueForKey:@"niacina"]];
    self.title = [mealDictionary valueForKey:@"nombre"];
    self.notes = [mealDictionary valueForKey:@"nota"];
    self.omega3 = [UtilManager floatFromString:[mealDictionary valueForKey:@"omega3"]];
    self.omega6 = [UtilManager floatFromString:[mealDictionary valueForKey:@"omega6"]];
    self.potasium = [UtilManager floatFromString:[mealDictionary valueForKey:@"potasio"]];
    self.proteins = [UtilManager intFromString:[mealDictionary valueForKey:@"proteinas"]];
    self.retynol = [UtilManager floatFromString:[mealDictionary valueForKey:@"retinol"]];
    self.selenium = [UtilManager floatFromString:[mealDictionary valueForKey:@"selenio"]];
    self.sodium = [UtilManager floatFromString:[mealDictionary valueForKey:@"sodio"]];
    self.vitA = [UtilManager floatFromString:[mealDictionary valueForKey:@"v_a"]];
    self.vitB1 = [UtilManager floatFromString:[mealDictionary valueForKey:@"v_b1"]];
    self.vitB12 = [UtilManager floatFromString:[mealDictionary valueForKey:@"v_b12"]];
    self.vitB2 = [UtilManager floatFromString:[mealDictionary valueForKey:@"v_b2"]];
    self.vitB6 = [UtilManager floatFromString:[mealDictionary valueForKey:@"v_b6"]];
    self.vitC = [UtilManager floatFromString:[mealDictionary valueForKey:@"v_c"]];
    self.vitD = [UtilManager floatFromString:[mealDictionary valueForKey:@"v_d"]];
    self.iodine = [UtilManager floatFromString:[mealDictionary valueForKey:@"iodine"]];
    self.zinc = [UtilManager floatFromString:[mealDictionary valueForKey:@"zinc"]];
}

@end
