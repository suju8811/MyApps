//
//  NutritionDay.h
//  Titan
//
//  Created by Manuel Manzanera on 12/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "Diet.h"

@interface NutritionDay : RLMObject

@property NSNumber <RLMInt> *order;
@property Diet *diet;
@property NSString *dietId;
@property NSString *code;
@property NSString * start;
@property NSString * end;

+ (BOOL)deleteAllNutritionDays;
+ (NutritionDay *)copyNutritionDay:(NutritionDay *)nutritionDay;
+ (NSArray<NutritionDay*>*)copyNutritionDay:(NutritionDay *)nutritionDay numberOfTime:(NSInteger)numberOfReplies;

@end

RLM_ARRAY_TYPE(NutritionDay)
