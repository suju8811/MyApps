//
//  Diet.m
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "Diet.h"
#import "AppContext.h"
#import "UtilManager.h"

@implementation Diet

+ (NSString *)primaryKey{
    return @"dietId";
}

+ (BOOL)deleteAllDiets {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[Diet allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)saveDiets:(NSMutableArray *)diets{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:diets];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return FALSE;
    }
    
    return YES;
}

+ (RLMResults *)getDiets{
    
    RLMResults<Diet *> *diets = [Diet objectsWhere:@"isCopy != true"];
    RLMResults<Diet *> *sortedDiets = [diets sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedDiets;
    
}

+ (RLMResults *)getDietsWithOwner:(NSString *)owner{
    
    RLMResults<Diet *> *diets = [Diet objectsWhere:@"owner == %@ AND isCopy != true",owner];
    RLMResults<Diet *> *sortedDiets = [diets sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedDiets;
    
}

+ (RLMResults *)getMyDiets {
    
    RLMResults<Diet *> *diets = [Diet objectsWhere:@"owner == %@", [AppContext sharedInstance].currentUser.name];
    RLMResults<Diet *> *sortedMeals = [diets sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (RLMResults*)getMyDietsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Diet getMyDiets];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"owner == '%@' AND (title LIKE '*%@*' OR content LIKE '*%@*' OR note LIKE '*%@*')", [AppContext sharedInstance].currentUser.name, keyword, keyword, keyword];
    RLMResults<Diet *> *diets = [Diet objectsWhere:predicateString];
    RLMResults<Diet *> *sortedMeals = [diets sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (RLMResults *)getOtherDiets {
    
    RLMResults<Diet *> *diets = [Diet objectsWhere:@"owner != %@", [AppContext sharedInstance].currentUser.name];
    RLMResults<Diet *> *sortedMeals = [diets sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (RLMResults *)getOtherDietsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Diet getOtherDiets];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"owner != '%@' AND (title LIKE '*%@*' OR content LIKE '*%@*' OR note LIKE '*%@*')", [AppContext sharedInstance].currentUser.name, keyword, keyword, keyword];
    RLMResults<Diet *> *diets = [Diet objectsWhere:predicateString];
    RLMResults<Diet *> *sortedMeals = [diets sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (RLMResults *)getFavouritesDiets {
    RLMResults<Diet *> *diets = [Diet objectsWhere:@"isFavourite = TRUE AND isCopy != true"];
    RLMResults<Diet *> *sortedDiets = [diets sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedDiets;
}

+ (RLMResults *)getFavouritesDietsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Diet getFavouritesDiets];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"isFavourite = TRUE AND isCopy != true AND (title LIKE '*%@*' OR content LIKE '*%@*' OR note LIKE '*%@*')", keyword, keyword, keyword];
    RLMResults<Diet *> *diets = [Diet objectsWhere:predicateString];
    RLMResults<Diet *> *sortedMeals = [diets sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (RLMResults *)getSharedDiets {
    RLMResults<Diet *> *diets = [Diet objectsWhere:@"isShared = TRUE AND isCopy != true"];
    RLMResults<Diet *> *sortedDiets = [diets sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedDiets;
}

+ (RLMResults *)getSharedDietsWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [Diet getSharedDiets];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"isShared = TRUE AND isCopy != true AND (title LIKE '*%@*' OR content LIKE '*%@*' OR note LIKE '*%@*')", keyword, keyword, keyword];
    RLMResults<Diet *> *diets = [Diet objectsWhere:predicateString];
    RLMResults<Diet *> *sortedMeals = [diets sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedMeals;
}

+ (Diet *)dietWithId:(NSString *)dietId{
    
    if(![dietId isKindOfClass:[NSString class]])
        return nil;
    
    RLMResults <Diet *>*diets = [Diet objectsWhere:@"dietId = %@",dietId];
    
    if(diets.count > 0)
        return [diets firstObject];
    else
        return nil;
}

+ (Diet *)dietWithServerId:(NSString *)serverDietId {
    
    if(![serverDietId isKindOfClass:[NSString class]])
        return nil;
    
    RLMResults <Diet *>*diets = [Diet objectsWhere:@"serverDietId = %@", serverDietId];
    
    if(diets.count > 0)
        return [diets firstObject];
    else
        return nil;
}

+ (BOOL)deleteDiet:(Diet *)diet {
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObject:diet];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    return YES;
}

+ (BOOL)deleteMealsInDiet:(Diet *)diet{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    int index = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        
        [realm transactionWithBlock:^{
           [diet.dietMeals removeObjectAtIndex:index];
        }];
        
        [DietMeal deleteDietMeal:dietMeal];
        index++;
    }
    
    return YES;
}

+ (BOOL)canDeleteDiet:(Diet *)diet{
    if([diet.owner isEqualToString:[[AppContext sharedInstance] userId]])
        return YES;
    else
        return FALSE;
}


+ (Diet *)duplicateDiet:(Diet *)diet{
    
    Diet *duplicateDiet = [[Diet alloc] init];
    
    [duplicateDiet setDietId:[UtilManager uuid]];
    [duplicateDiet setServerDietId:diet.serverDietId];
    [duplicateDiet setTitle:[NSString stringWithFormat:@"%@",diet.title]];
    [duplicateDiet setOwner:[[AppContext sharedInstance] userId]];
    [duplicateDiet setContent:diet.content];
    [duplicateDiet setNote:diet.note];
    
    [duplicateDiet setCreatedAt:[NSDate date]];
    [duplicateDiet setUpdatedAt:[NSDate date]];
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        DietMeal * duplicateDietMeal = [DietMeal duplicateMeal:dietMeal];
        [duplicateDiet.dietMeals addObject:duplicateDietMeal];
    }
    
    return duplicateDiet;
}

+ (NSDate *)lastUpdateDate{
    RLMResults<Diet *> *diets = [[Diet allObjects] sortedResultsUsingKeyPath:@"updatedAt" ascending:FALSE];
    
    if(diets.count > 0)
        return [[diets objectAtIndex:0] updatedAt];
    else
        return [NSDate dateWithTimeIntervalSince1970:0];
}

+ (NSInteger)getDietQuantity:(Diet*)diet {
    NSInteger quantity = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            quantity = quantity + [Meal getMealQuantity:meal];
        }
    }
    
    return quantity;
}

+ (float)getKCalInDiet:(Diet *)diet{
    float kCals = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal *meal in dietMeal.meals) {
            kCals = kCals + [Meal getKCalInMeal:meal];
        }
    }
    
    return kCals;
}

+ (NSInteger)getDietProtein:(Diet *)diet {
    NSInteger proteins = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            proteins = proteins + [Meal getMealProtein:meal];
        }
    }
    
    return proteins;
}

+ (NSInteger)getDietCarb:(Diet *)diet {
    NSInteger carbs = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            carbs = carbs + [Meal getMealCarb:meal];
        }
    }
    
    return carbs;
}

+ (NSInteger)getDietFats:(Diet *)diet {
    NSInteger fats = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            fats = fats + [Meal getMealFats:meal];
        }
    }
    
    return fats;
}

+ (NSInteger)getDietSugar:(Diet *)diet {
    NSInteger sugar = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            sugar = sugar + [Meal getMealSugar:meal];
        }
    }
    
    return sugar;
}

+ (float)getDietSaturedFats:(Diet *)diet {
    float saturedFats = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal *meal in dietMeal.meals) {
            saturedFats = saturedFats + [Meal getMealSaturedFats:meal];
        }
    }
    
    return saturedFats;
}

+ (float)getDietMonoUnsaturedFats:(Diet *)diet {
    float monoUnsaturedFats = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            monoUnsaturedFats = monoUnsaturedFats + [Meal getMealMonoUnsaturedFats:meal];
        }
    }
    
    return monoUnsaturedFats;
}

+ (float)getDietPolyUnsaturedFats:(Diet *)diet {
    float polyUnsaturedFats = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            polyUnsaturedFats = polyUnsaturedFats + [Meal getMealPolyUnsaturedFats:meal];
        }
    }
    
    return polyUnsaturedFats;
}

+ (float)getDietFiber:(Diet *)diet {
    float fiber = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            fiber = fiber + [Meal getMealFiber:meal];
        }
    }
    
    return fiber;
}

+ (float)getDietBccas:(Diet *)diet {
    float bccas = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal *meal in dietMeal.meals) {
            bccas = bccas + [Meal getMealBccas:meal];
        }
    }
    
    return bccas;
}

+ (float)getDietGlutamine:(Diet *)diet {
    float glutamine = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal *meal in dietMeal.meals) {
            glutamine = glutamine + [Meal getMealGlutamine:meal];
        }
    }
    
    return glutamine;
}

+ (float)getDietOmega3:(Diet *)diet {
    float omega3 = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal *meal in dietMeal.meals) {
            omega3 = omega3 + [Meal getMealOmega3:meal];
        }
    }
    
    return omega3;
}

+ (float)getDietOmega6:(Diet *)diet {
    float omega6 = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal *meal in dietMeal.meals) {
            omega6 = omega6 + [Meal getMealOmega6:meal];
        }
    }
    
    return omega6;
}

+ (float)getDietWater:(Diet *)diet {
    float water = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal *meal in dietMeal.meals) {
            water = water + [Meal getMealWater:meal];
        }
    }
    
    return water;
}

+ (float)getDietAlcohol:(Diet *)diet {
    float alcohol = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal *meal in dietMeal.meals) {
            alcohol = alcohol + [Meal getMealAlcohol:meal];
        }
    }
    
    return alcohol;
}

+ (float)getDietIron:(Diet *)diet {
    float iron = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal *meal in dietMeal.meals) {
            iron = iron + [Meal getMealIron:meal];
        }
    }
    
    return iron;
}

+ (float)getDietIodine:(Diet *)diet {
    float iodine = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            iodine = iodine + [Meal getMealIodine:meal];
        }
    }
    
    return iodine;
}

+ (float)getDietMagnesium:(Diet *)diet {
    float magnesium = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            magnesium = magnesium + [Meal getMealMagnesium:meal];
        }
    }
    
    return magnesium;
}

+ (float)getDietZinc:(Diet *)diet {
    float zinc = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal *meal in dietMeal.meals) {
            zinc = zinc + [Meal getMealZinc:meal];
        }
    }
    
    return zinc;
}

+ (float)getDietSelenium:(Diet *)diet {
    float selenium = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            selenium = selenium + [Meal getMealSelenium:meal];
        }
    }
    
    return selenium;
}

+ (float)getDietSodium:(Diet *)diet {
    float sodium = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            sodium = sodium + [Meal getMealSodium:meal];
        }
    }
    
    return sodium;
}

+ (float)getDietPotasium:(Diet *)diet {
    float potasium = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            potasium = potasium + [Meal getMealPotasium:meal];
        }
    }
    
    return potasium;
}

+ (float)getDietPhosphorus:(Diet *)diet {
    float phosphorus = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal *meal in dietMeal.meals) {
            phosphorus = phosphorus + [Meal getMealPhosphorus:meal];
        }
    }
    
    return phosphorus;
}

+ (float)getDietVitB1:(Diet *)diet {
    float vitB1 = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal *meal in dietMeal.meals) {
            vitB1 = vitB1 + [Meal getMealVitB1:meal];
        }
    }
    
    return vitB1;
}

+ (float)getDietVitB2:(Diet *)diet {
    float vitB2 = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            vitB2 = vitB2 + [Meal getMealVitB2:meal];
        }
    }
    
    return vitB2;
}

+ (float)getDietNiacin:(Diet *)diet {
    float niacin = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            niacin = niacin + [Meal getMealNiacin:meal];
        }
    }
    
    return niacin;
}

+ (float)getDietVitA:(Diet *)diet {
    float vitA = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            vitA = vitA + [Meal getMealVitA:meal];
        }
    }
    
    return vitA;
}

+ (float)getDietFolicAcid:(Diet *)diet {
    float folicAcid = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            folicAcid = folicAcid + [Meal getMealFolicAcid:meal];
        }
    }
    
    return folicAcid;
}

+ (float)getDietVitB12:(Diet *)diet {
    float vitB12 = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            vitB12 = vitB12 + [Meal getMealVitB12:meal];
        }
    }
    
    return vitB12;
}

+ (float)getDietVitC:(Diet *)diet {
    float vitC = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            vitC = vitC + [Meal getMealVitC:meal];
        }
    }
    
    return vitC;
}

+ (float)getDietVitD:(Diet *)diet; {
    float vitD = 0;
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            vitD = vitD + [Meal getMealVitD:meal];
        }
    }
    
    return vitD;
}

+ (NSMutableArray *)getTagsInDiet:(Diet *)diet {
    NSMutableArray *tags = [[NSMutableArray alloc] init];
    
    for (DietMeal * dietMeal in diet.dietMeals) {
        for (Meal * meal in dietMeal.meals) {
            for (Tag * tag in [Meal getMealTags:meal]) {
                if (tag) {
                    if (![tags containsObject:tag]) {
                        [tags addObject:tag];
                    }
                }
            }
        }
    }
    
    return tags;
}

- (void)parseFromDictionary:(NSDictionary*)dietDictionary {
    [super parseFromDictionary:dietDictionary];
    
    self.owner = [dietDictionary valueForKey:@"user"];
    self.folicAcid = [UtilManager floatFromString:[dietDictionary valueForKey:@"folidAcid"]];
    self.agpAgs = [UtilManager intFromString:[dietDictionary valueForKey:@"agp_ags"]];
    self.agpAgsAgm = [UtilManager floatFromString:[dietDictionary valueForKey:@"agp_ags_adm"]];
    self.water = [UtilManager floatFromString:[dietDictionary valueForKey:@"agua"]];
    self.alcohol = [UtilManager intFromString:[dietDictionary valueForKey:@"alcohol"]];
    self.sugar = [UtilManager intFromString:[dietDictionary valueForKey:@"azucar"]];
    self.bccas = [UtilManager intFromString:[dietDictionary valueForKey:@"bccas"]];
    self.betaCaronete = [UtilManager intFromString:[dietDictionary valueForKey:@"beta_caroteno"]];
    self.calcium = [UtilManager intFromString:[dietDictionary valueForKey:@"calcio"]];
    self.calories = [UtilManager intFromString:[dietDictionary valueForKey:@"calorias"]];
    self.carbs = [UtilManager intFromString:[dietDictionary valueForKey:@"carbohidratos"]];
    self.cholesterol = [UtilManager floatFromString:[dietDictionary valueForKey:@"colesterol"]];
    self.content = [dietDictionary valueForKey:@"contenido"];
    
    NSArray *dietMeals = [dietDictionary valueForKey:@"dietas_comidas"];
    
    for (NSDictionary *dietMealDictionary in dietMeals) {
        DietMeal * dietMeal = [[DietMeal alloc] init];
        [dietMeal parseFromDictionary:dietMealDictionary];
        [self.dietMeals addObject:dietMeal];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    self.createdAt = [formatter dateFromString:[dietDictionary valueForKey:@"fecha_comienzo"]];
    self.updatedAt = [formatter dateFromString:[dietDictionary valueForKey:@"fecha_comienzo"]];
    self.fiber = [UtilManager intFromString:[dietDictionary valueForKey:@"fibra"]];
    self.phosphorus = [UtilManager floatFromString:[dietDictionary valueForKey:@"fosforo"]];
    self.glycemicIndex = [UtilManager floatFromString:[dietDictionary valueForKey:@"glucemino"]];
    self.glutamine = [UtilManager floatFromString:[dietDictionary valueForKey:@"glutamina"]];
    self.fats = [UtilManager intFromString:[dietDictionary valueForKey:@"grasas"]];
    self.monoUnsaturatedFats = [UtilManager floatFromString:[dietDictionary valueForKey:@"grasas_monoinsaturadas"]];
    self.polyUnsaturatedFats = [UtilManager floatFromString:[dietDictionary valueForKey:@"grasas_poliinsaturadas"]];
    self.saturedFats = [UtilManager floatFromString:[dietDictionary valueForKey:@"grasas_saturadas"]];
    self.iron = [UtilManager floatFromString:[dietDictionary valueForKey:@"hierro"]];
    self.serverDietId = [dietDictionary valueForKey:@"id"];
    self.dietId = [dietDictionary valueForKey:@"id"];
    self.magnesium = [UtilManager floatFromString:[dietDictionary valueForKey:@"magnesio"]];
    self.niacin = [UtilManager floatFromString:[dietDictionary valueForKey:@"niacina"]];
    self.note = [dietDictionary valueForKey:@"notas"];
    self.omega3 = [UtilManager floatFromString:[dietDictionary valueForKey:@"omega3"]];
    self.omega6 = [UtilManager floatFromString:[dietDictionary valueForKey:@"omega6"]];
    self.potasium = [UtilManager floatFromString:[dietDictionary valueForKey:@"potasio"]];
    self.proteins = [UtilManager intFromString:[dietDictionary valueForKey:@"proteinas"]];
    self.retynol = [UtilManager floatFromString:[dietDictionary valueForKey:@"retinol"]];
    self.selenium = [UtilManager floatFromString:[dietDictionary valueForKey:@"selenio"]];
    self.sodium = [UtilManager floatFromString:[dietDictionary valueForKey:@"sodio"]];
    self.title = [dietDictionary valueForKey:@"titulo"];
    self.vitA = [UtilManager floatFromString:[dietDictionary valueForKey:@"v_a"]];
    self.vitB1 = [UtilManager floatFromString:[dietDictionary valueForKey:@"v_b1"]];
    self.vitB12 = [UtilManager floatFromString:[dietDictionary valueForKey:@"v_b12"]];
    self.vitB2 = [UtilManager floatFromString:[dietDictionary valueForKey:@"v_b2"]];
    self.vitB6 = [UtilManager floatFromString:[dietDictionary valueForKey:@"v_b6"]];
    self.vitC = [UtilManager floatFromString:[dietDictionary valueForKey:@"v_c"]];
    self.vitD = [UtilManager floatFromString:[dietDictionary valueForKey:@"v_d"]];
    self.iodine = [UtilManager floatFromString:[dietDictionary valueForKey:@"yodo"]];
    self.zinc = [UtilManager floatFromString:[dietDictionary valueForKey:@"zinc"]];
}

@end
