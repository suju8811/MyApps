//
//  NutritionPlan.m
//  Titan
//
//  Created by Manuel Manzanera on 12/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "NutritionPlan.h"
#import "AppContext.h"
#import "UtilManager.h"

@implementation NutritionPlan

+ (NSString *)primaryKey{
    return @"nutritionPlanId";
}

+ (BOOL)deleteAllNutritionPlans {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:[NutritionPlan allObjects]];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)saveNutritionPlan:(NutritionPlan *)nutritionPlan {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:nutritionPlan];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (BOOL)saveNutritionsPlans:(NSMutableArray *)nutritionPlans {
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:nutritionPlans];
    NSError *error;
    [realm commitWriteTransaction:&error];
    
    if(error){
        NSLog(@"%@",error.description);
        return NO;
    }
    
    return YES;
}

+ (RLMResults *)getNutritionPlans {
    
    RLMResults<NutritionPlan *> *nutritionPlan = [NutritionPlan objectsWhere:@"isCalendarCopy==FALSE"];
    RLMResults<NutritionPlan *> *sortedNutritionPlan = [nutritionPlan sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedNutritionPlan;
}

+ (RLMResults *)getNutritionPlansWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [NutritionPlan getNutritionPlans];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"isCalendarCopy==FALSE AND isRepose==FALSE AND (title LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", keyword, keyword, keyword];
    RLMResults<NutritionPlan *> *nutritionPlan = [NutritionPlan objectsWhere:predicateString];
    RLMResults<NutritionPlan *> *sortedNutritionPlan = [nutritionPlan sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedNutritionPlan;
}

+ (RLMResults *)getMyNutritionPlans {
    
    RLMResults<NutritionPlan *> *nutritionPlan = [NutritionPlan objectsWhere:@"owner==%@ AND isCalendarCopy==FALSE", [AppContext sharedInstance].currentUser.name];
    RLMResults<NutritionPlan *> *sortedNutritionPlan = [nutritionPlan sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedNutritionPlan;
}

+ (RLMResults *)getMyNutritionPlansWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [NutritionPlan getMyNutritionPlans];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"owner=='%@' AND isCalendarCopy==FALSE AND (title LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", [AppContext sharedInstance].currentUser.name, keyword, keyword, keyword];
    RLMResults<NutritionPlan *> *nutritionPlan = [NutritionPlan objectsWhere:predicateString];
    RLMResults<NutritionPlan *> *sortedNutritionPlan = [nutritionPlan sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedNutritionPlan;
}

+ (RLMResults *)getOtherNutritionPlans {
    
    RLMResults<NutritionPlan *> *nutritionPlan = [NutritionPlan objectsWhere:@"owner!=%@ AND isCalendarCopy==FALSE", [AppContext sharedInstance].currentUser.name];
    RLMResults<NutritionPlan *> *sortedNutritionPlan = [nutritionPlan sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedNutritionPlan;
}

+ (RLMResults *)getOtherNutritionPlansWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [NutritionPlan getOtherNutritionPlans];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"owner!='%@' AND isCalendarCopy==FALSE AND (title LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", [AppContext sharedInstance].currentUser.name, keyword, keyword, keyword];
    RLMResults<NutritionPlan *> *nutritionPlan = [NutritionPlan objectsWhere:predicateString];
    RLMResults<NutritionPlan *> *sortedNutritionPlan = [nutritionPlan sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedNutritionPlan;
}

+ (RLMResults *)getNutritionPlansWithOwner:(NSString *)owner {
    
    RLMResults<NutritionPlan *> *nutritionPlans = [NutritionPlan objectsWhere:@"owner == %@ AND isCalendarCopy==FALSE",owner];
    RLMResults<NutritionPlan *> *sortedNutritionPlans = [nutritionPlans sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedNutritionPlans;
    
}

+ (RLMResults *)getFavouritesNutritionPlans {
    RLMResults<NutritionPlan *> *nutritionPlans = [NutritionPlan objectsWhere:@"isFavourite = TRUE AND isCalendarCopy==FALSE"];
    RLMResults<NutritionPlan *> *sortedNutritionPlans = [nutritionPlans sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedNutritionPlans;
}

+ (RLMResults *)getFavouritesNutritionPlansWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [NutritionPlan getFavouritesNutritionPlans];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"isFavourite == TRUE AND isCalendarCopy==FALSE AND (title LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", keyword, keyword, keyword];
    RLMResults<NutritionPlan *> *nutritionPlans = [NutritionPlan objectsWhere:predicateString];
    RLMResults<NutritionPlan *> *sortedNutritionPlans = [nutritionPlans sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedNutritionPlans;
}

+ (RLMResults *)getSharedPlans {
    RLMResults<NutritionPlan *> *nutritionPlans = [NutritionPlan objectsWhere:@"isShared == TRUE AND isCalendarCopy==FALSE"];
    RLMResults<NutritionPlan *> *sortedNutritionPlans = [nutritionPlans sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedNutritionPlans;
}

+ (RLMResults *)getSharedPlansNutritionPlansWithKeyword:(NSString*)keyword {
    if (keyword == nil ||
        keyword.length == 0) {
        return [NutritionPlan getSharedPlans];
    }
    
    NSString * predicateString = [NSString stringWithFormat:@"isShared == TRUE AND isCalendarCopy==FALSE AND (title LIKE '*%@*' OR content LIKE '*%@*' OR notes LIKE '*%@*')", keyword, keyword, keyword];
    RLMResults<NutritionPlan *> *nutritionPlans = [NutritionPlan objectsWhere:predicateString];
    RLMResults<NutritionPlan *> *sortedNutritionPlans = [nutritionPlans sortedResultsUsingKeyPath:@"title" ascending:YES];
    
    return sortedNutritionPlans;
}

+ (NutritionPlan *)getNutritionPlanWithId:(NSString *)nutritionPlanId {
    RLMResults<NutritionPlan *> *nutritionPlans = [NutritionPlan objectsWhere:@"serverNutritionPlanId == %@",nutritionPlanId];
    if(nutritionPlans.count > 0)
        return nutritionPlans.firstObject;
    return nil;
}

+ (BOOL)deleteNutritionPlan:(NutritionPlan *)nutritionPlan{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObject:nutritionPlan];
    NSError *error;
    [realm commitWriteTransaction:&error];
    if(error){
        NSLog(@"%@",error.description);
    }
    
    return YES;
}

+ (BOOL)deleteDietInNutritionPlan:(NutritionPlan *)nutritionPlan{
    
    RLMRealm *realm = [AppContext currentRealm];
    
    int index = 0;
    
    for(NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        
        Diet *diet = nutritionDay.diet;
        
        if(!diet)
            diet = [Diet dietWithId:nutritionDay.dietId];
        
        if(diet)
            [Diet deleteDiet:diet];
        
        [realm transactionWithBlock:^{
            [nutritionPlan.nutritionDays removeObjectAtIndex:index];
        }];
        
        index++;
    }
    
    return YES;
}

+ (BOOL)canDeleteNutritionPlan:(NutritionPlan *)nutritionPlan{
    if([nutritionPlan.owner isEqualToString:[[AppContext sharedInstance] userId]])
        return YES;
    else
        return FALSE;
}

+ (NSInteger)getNutritionPlanQuantity:(NutritionPlan*)nutritionPlan {
    NSInteger quantity = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays) {
        quantity = quantity + [Diet getDietQuantity:nutritionDay.diet];
    }
    
    return quantity;
}

+ (float)getKCalInNutritionPlan:(NutritionPlan *)nutritionPlan {
    float kCals = 0;
    
    for (NutritionDay * nutritionDay in nutritionPlan.nutritionDays) {
        kCals = kCals + [Diet getKCalInDiet:nutritionDay.diet];
    }
    
    return kCals;
}

+ (NSInteger)getNutritionPlanProtein:(NutritionPlan *)nutritionPlan {
    NSInteger proteins = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays) {
        proteins = proteins + [Diet getDietProtein:nutritionDay.diet];
    }
    
    return proteins;
}

+ (NSInteger)getNutritionPlanCarb:(NutritionPlan *)nutritionPlan {
    NSInteger carbs = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays) {
        carbs = carbs + [Diet getDietCarb:nutritionDay.diet];
    }
    
    return carbs;
}

+ (NSInteger)getNutritionPlanFats:(NutritionPlan *)nutritionPlan {
    NSInteger fats = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays) {
        fats = fats + [Diet getDietFats:nutritionDay.diet];
    }
    
    return fats;
}

+ (NSInteger)getNutritionPlanSugar:(NutritionPlan *)nutritionPlan {
    NSInteger sugar = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        sugar = sugar + [Diet getDietSugar:nutritionDay.diet];
    }
    
    return sugar;
}

+ (float)getNutritionPlanSaturedFats:(NutritionPlan *)nutritionPlan {
    float saturedFats = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        saturedFats = saturedFats + [Diet getDietSaturedFats:nutritionDay.diet];
    }
    
    return saturedFats;
}

+ (float)getNutritionPlanMonoUnsaturedFats:(NutritionPlan *)nutritionPlan {
    float monoUnsaturedFats = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        monoUnsaturedFats = monoUnsaturedFats + [Diet getDietMonoUnsaturedFats:nutritionDay.diet];
    }
    
    return monoUnsaturedFats;
}

+ (float)getNutritionPlanPolyUnsaturedFats:(NutritionPlan *)nutritionPlan {
    float polyUnsaturedFats = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        polyUnsaturedFats = polyUnsaturedFats + [Diet getDietPolyUnsaturedFats:nutritionDay.diet];
    }
    
    return polyUnsaturedFats;
}

+ (float)getNutritionPlanFiber:(NutritionPlan *)nutritionPlan {
    float fiber = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        fiber = fiber + [Diet getDietFiber:nutritionDay.diet];
    }
    
    return fiber;
}

+ (float)getNutritionPlanBccas:(NutritionPlan *)nutritionPlan {
    float bccas = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        bccas = bccas + [Diet getDietBccas:nutritionDay.diet];
    }
    
    return bccas;
}

+ (float)getNutritionPlanGlutamine:(NutritionPlan *)nutritionPlan {
    float glutamine = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        glutamine = glutamine + [Diet getDietGlutamine:nutritionDay.diet];
    }
    
    return glutamine;
}

+ (float)getNutritionPlanOmega3:(NutritionPlan *)nutritionPlan {
    float omega3 = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        omega3 = omega3 + [Diet getDietOmega3:nutritionDay.diet];
    }
    
    return omega3;
}

+ (float)getNutritionPlanOmega6:(NutritionPlan *)nutritionPlan {
    float omega6 = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        omega6 = omega6 + [Diet getDietOmega6:nutritionDay.diet];
    }
    
    return omega6;
}

+ (float)getNutritionPlanWater:(NutritionPlan *)nutritionPlan {
    float water = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        water = water + [Diet getDietWater:nutritionDay.diet];
    }
    
    return water;
}

+ (float)getNutritionPlanAlcohol:(NutritionPlan *)nutritionPlan {
    float alcohol = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        alcohol = alcohol + [Diet getDietAlcohol:nutritionDay.diet];
    }
    
    return alcohol;
}

+ (float)getNutritionPlanIron:(NutritionPlan *)nutritionPlan {
    float iron = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        iron = iron + [Diet getDietIron:nutritionDay.diet];
    }
    
    return iron;
}

+ (float)getNutritionPlanIodine:(NutritionPlan *)nutritionPlan {
    float iodine = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        iodine = iodine + [Diet getDietIodine:nutritionDay.diet];
    }
    
    return iodine;
}

+ (float)getNutritionPlanMagnesium:(NutritionPlan *)nutritionPlan {
    float magnesium = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        magnesium = magnesium + [Diet getDietMagnesium:nutritionDay.diet];
    }
    
    return magnesium;
}

+ (float)getNutritionPlanZinc:(NutritionPlan *)nutritionPlan {
    float zinc = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        zinc = zinc + [Diet getDietZinc:nutritionDay.diet];
    }
    
    return zinc;
}

+ (float)getNutritionPlanSelenium:(NutritionPlan *)nutritionPlan {
    float selenium = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        selenium = selenium + [Diet getDietSelenium:nutritionDay.diet];
    }
    
    return selenium;
}

+ (float)getNutritionPlanSodium:(NutritionPlan *)nutritionPlan {
    float sodium = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        sodium = sodium + [Diet getDietSodium:nutritionDay.diet];
    }
    
    return sodium;
}

+ (float)getNutritionPlanPotasium:(NutritionPlan *)nutritionPlan {
    float potasium = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        potasium = potasium + [Diet getDietPotasium:nutritionDay.diet];
    }
    
    return potasium;
}

+ (float)getNutritionPlanPhosphorus:(NutritionPlan *)nutritionPlan {
    float phosphorus = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        phosphorus = phosphorus + [Diet getDietPhosphorus:nutritionDay.diet];
    }
    
    return phosphorus;
}

+ (float)getNutritionPlanVitB1:(NutritionPlan *)nutritionPlan {
    float vitB1 = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        vitB1 = vitB1 + [Diet getDietVitB1:nutritionDay.diet];
    }
    
    return vitB1;
}

+ (float)getNutritionPlanVitB2:(NutritionPlan *)nutritionPlan {
    float vitB2 = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        vitB2 = vitB2 + [Diet getDietVitB2:nutritionDay.diet];
    }
    
    return vitB2;
}

+ (float)getNutritionPlanNiacin:(NutritionPlan *)nutritionPlan {
    float niacin = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        niacin = niacin + [Diet getDietNiacin:nutritionDay.diet];
    }
    
    return niacin;
}

+ (float)getNutritionPlanVitA:(NutritionPlan *)nutritionPlan {
    float vitA = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        vitA = vitA + [Diet getDietVitA:nutritionDay.diet];
    }
    
    return vitA;
}

+ (float)getNutritionPlanFolicAcid:(NutritionPlan *)nutritionPlan {
    float folicAcid = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        folicAcid = folicAcid + [Diet getDietFolicAcid:nutritionDay.diet];
    }
    
    return folicAcid;
}

+ (float)getNutritionPlanVitB12:(NutritionPlan *)nutritionPlan {
    float vitB12 = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        vitB12 = vitB12 + [Diet getDietVitB12:nutritionDay.diet];
    }
    
    return vitB12;
}

+ (float)getNutritionPlanVitC:(NutritionPlan *)nutritionPlan {
    float vitC = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        vitC = vitC + [Diet getDietVitC:nutritionDay.diet];
    }
    
    return vitC;
}

+ (float)getNutritionPlanVitD:(NutritionPlan *)nutritionPlan; {
    float vitD = 0;
    
    for (NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        vitD = vitD + [Diet getDietVitD:nutritionDay.diet];
    }
    
    return vitD;
}

+ (NSMutableArray *)getTagsInNutritionPlan:(NutritionPlan *)nutritionPlan {
    
    NSMutableArray *tags = [[NSMutableArray alloc] init];
    
    for (NutritionDay * nutritionDay in nutritionPlan.nutritionDays) {
        for (Tag * tag in [Diet getTagsInDiet:nutritionDay.diet]) {
            if (tag) {
                if (![tags containsObject:tag]) {
                    [tags addObject:tag];
                }
            }
        }
    }
    
    return tags;
}

+ (NSDictionary *)nutritionPlanDictionary:(NutritionPlan *)nutritionPlan{
    
    NSMutableDictionary *nutritionPlanDictionary = [[NSMutableDictionary alloc] init];
    
    [nutritionPlanDictionary setValue:nutritionPlan.nutritionPlanId forKey:@"nutritionPlanId"];
    [nutritionPlanDictionary setValue:nutritionPlan.title forKey:@"title"];
    [nutritionPlanDictionary setValue:nutritionPlan.owner forKey:@"owner"];
    [nutritionPlanDictionary setValue:nutritionPlan.content forKey:@"content"];
    [nutritionPlanDictionary setValue:nutritionPlan.notes forKey:@"note"];
    
    NSMutableArray *nutritionDays = [[NSMutableArray alloc] initWithCapacity:nutritionPlan.nutritionDays.count];
    
    for(NutritionDay *nutritionDay in nutritionPlan.nutritionDays){
        
        NSMutableDictionary *nutritionDayDictionary = [[NSMutableDictionary alloc] init];
        if(nutritionDay.diet && nutritionDay.diet.serverDietId)
            [nutritionDayDictionary setObject:nutritionDay.diet.serverDietId forKey:@"diet"];
        else
            [nutritionDayDictionary setObject:nutritionDay.dietId forKey:@"diet"];
        [nutritionDays addObject:nutritionDayDictionary];
    }
    
    [nutritionPlanDictionary setObject:nutritionDays forKey:@"nutrition_day"];
    
    return nutritionPlanDictionary;
}

+ (NutritionPlan *)duplicateNutritionPlan:(NutritionPlan *)nutritionPlan{
    NutritionPlan *duplicatePlan = [[NutritionPlan alloc] init];
    
    duplicatePlan.nutritionPlanId = [UtilManager uuid];
    duplicatePlan.serverNutritionPlanId = nutritionPlan.serverNutritionPlanId;
    duplicatePlan.title = nutritionPlan.title;
    duplicatePlan.owner = [AppContext sharedInstance].currentUser.name;
    duplicatePlan.content = nutritionPlan.content;
    duplicatePlan.notes = nutritionPlan.notes;
    
    duplicatePlan.createdAt = [NSDate date];
    duplicatePlan.updatedAt = [NSDate date];
    
    for (NutritionDay * nutritionDay in nutritionPlan.nutritionDays) {
        [duplicatePlan.nutritionDays addObject:nutritionDay];
    }
    
    return duplicatePlan;
}

+ (NSDate *)lastUpdateDate{
    RLMResults<NutritionPlan *> *nutritionPlans = [[NutritionPlan allObjects] sortedResultsUsingKeyPath:@"updatedAt" ascending:FALSE];
    
    if(nutritionPlans.count > 0)
        return [[nutritionPlans objectAtIndex:0] updatedAt];
    else
        return [NSDate dateWithTimeIntervalSince1970:0];
}

- (void)parseFromDictionary:(NSDictionary*)nutritionPlanDictionary {
    [super parseFromDictionary:nutritionPlanDictionary];
    
    [self parseFromDictionary:nutritionPlanDictionary
                   planDayKey:@"Plan_nutricional_dia"
            planDayContentKey:@"Plan_nutricional_dia_training"];
}

- (void)parseFromDictionary:(NSDictionary *)nutritionPlanDictionary
                 planDayKey:(NSString*)planDayKey
          planDayContentKey:(NSString*)planDayContentKey {
    
    [super parseFromDictionary:nutritionPlanDictionary];
    
    self.serverNutritionPlanId = [nutritionPlanDictionary valueForKey:@"id"];
    self.title = [nutritionPlanDictionary valueForKey:@"titulo"];
    self.content = [nutritionPlanDictionary valueForKey:@"descripcion"];
    self.notes = [nutritionPlanDictionary valueForKey:@"nota"];
    self.owner = [nutritionPlanDictionary valueForKey:@"user"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    if([nutritionPlanDictionary valueForKey:@"comienzo"]) {
        self.startDate = [formatter dateFromString:[nutritionPlanDictionary valueForKey:@"comienzo"]];
        self.endDate = [formatter dateFromString:[nutritionPlanDictionary valueForKey:@"finalizacion"]];
    }
    
    NSArray * planDays = [nutritionPlanDictionary valueForKey:planDayKey];
    NSArray * planDayContents = [nutritionPlanDictionary valueForKey:planDayContentKey];
    
    [self.nutritionDays removeAllObjects];
    
    NSMutableArray * nutritionDayObjectsAux = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index < planDayContents.count; index ++ ) {
        NSDictionary * nutritionDayDictionary = planDays[index];
        NSDictionary * nutritionDayContentDictionary = planDayContents[index];
        
        NutritionDay * nutritionDay = [[NutritionDay alloc] init];
        nutritionDay.code = [nutritionDayDictionary valueForKey:@"code"];
        nutritionDay.order = [UtilManager intFromString:[nutritionDayDictionary valueForKey:@"orden"]];
        NSString * dietId = [nutritionDayContentDictionary valueForKey:@"training"];
        nutritionDay.dietId = dietId;
        Diet * diet = [Diet dietWithServerId:dietId];
        nutritionDay.diet = diet;
        
        [nutritionDayObjectsAux addObject:nutritionDay];
    }
    
    [self.nutritionDays addObjects:nutritionDayObjectsAux];
}

@end
