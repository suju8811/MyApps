//
//  NutritionPlan.h
//  Titan
//
//  Created by Manuel Manzanera on 12/7/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseElement.h"
#import "NutritionDay.h"
#import "Diet.h"

@interface NutritionPlan : BaseElement

@property NSString *nutritionPlanId;
@property NSString *serverNutritionPlanId;
@property NSDate *createdAt;
@property NSDate *updatedAt;

@property NSDate *startDate;
@property NSDate *endDate;

@property NSString *owner;
@property NSString *title;
@property NSString *content;
@property NSString *notes;

@property NSString *privacity;

@property NSNumber <RLMBool> *isFavourite;                             // Property to asign if is favourite
@property NSNumber <RLMBool> *isShared;                             // Property to asign if is favourite
@property NSNumber <RLMBool> *isSelected;
@property NSNumber <RLMBool> *isCopy;

@property NSNumber <RLMInt> *order;
@property NSNumber <RLMBool> *isCalendarCopy;

@property RLMArray <NutritionDay *><NutritionDay> *nutritionDays;

+ (BOOL)deleteAllNutritionPlans;
+ (BOOL)saveNutritionPlan:(NutritionPlan *)nutritionPlan;
+ (BOOL)saveNutritionsPlans:(NSMutableArray *)nutritionPlans;

+ (RLMResults *)getNutritionPlans;
+ (RLMResults *)getNutritionPlansWithKeyword:(NSString*)keyword;
+ (RLMResults *)getMyNutritionPlans;
+ (RLMResults *)getMyNutritionPlansWithKeyword:(NSString*)keyword;
+ (RLMResults *)getOtherNutritionPlans;
+ (RLMResults *)getOtherNutritionPlansWithKeyword:(NSString*)keyword;
+ (RLMResults *)getNutritionPlansWithOwner:(NSString *)owner;
+ (NutritionPlan *)getNutritionPlanWithId:(NSString *)nutritionPlanId;
+ (RLMResults *)getFavouritesNutritionPlans;
+ (RLMResults *)getFavouritesNutritionPlansWithKeyword:(NSString*)keyword;
+ (RLMResults *)getSharedPlans;
+ (RLMResults *)getSharedPlansNutritionPlansWithKeyword:(NSString*)keyword;

+ (BOOL)deleteNutritionPlan:(NutritionPlan *)nutritionPlan;
+ (BOOL)deleteDietInNutritionPlan:(NutritionPlan *)nutritionPlan;
+ (BOOL)canDeleteNutritionPlan:(NutritionPlan *)nutritionPlan;

+ (NSInteger)getNutritionPlanQuantity:(NutritionPlan*)nutritionPlan;
+ (float)getKCalInNutritionPlan:(NutritionPlan *)nutritionPlan;
+ (NSInteger)getNutritionPlanProtein:(NutritionPlan *)nutritionPlan;
+ (NSInteger)getNutritionPlanCarb:(NutritionPlan *)nutritionPlan;
+ (NSInteger)getNutritionPlanFats:(NutritionPlan *)nutritionPlan;
+ (NSInteger)getNutritionPlanSugar:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanSaturedFats:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanMonoUnsaturedFats:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanPolyUnsaturedFats:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanFiber:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanBccas:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanGlutamine:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanOmega3:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanOmega6:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanWater:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanAlcohol:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanIron:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanIodine:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanMagnesium:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanZinc:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanSelenium:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanSodium:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanPotasium:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanPhosphorus:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanVitB1:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanVitB2:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanNiacin:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanVitA:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanFolicAcid:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanVitB12:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanVitC:(NutritionPlan *)nutritionPlan;
+ (float)getNutritionPlanVitD:(NutritionPlan *)nutritionPlan;
+ (NSMutableArray *)getTagsInNutritionPlan:(NutritionPlan *)nutritionPlan;

+ (NutritionPlan *)duplicateNutritionPlan:(NutritionPlan *)nutritionPlan;
+ (NSDictionary *)nutritionPlanDictionary:(NutritionPlan *)nutritionPlan;

+ (NSDate *)lastUpdateDate;

- (void)parseFromDictionary:(NSDictionary*)nutritionPlanDictionary;

- (void)parseFromDictionary:(NSDictionary *)nutritionPlanDictionary
                 planDayKey:(NSString*)planDayKey
          planDayContentKey:(NSString*)planDayContentKey;

@end
