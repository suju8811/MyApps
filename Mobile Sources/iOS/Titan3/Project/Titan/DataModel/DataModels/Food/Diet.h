//
//  Diet.h
//  Titan
//
//  Created by Manuel Manzanera on 13/3/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DietMeal.h"
#import "BaseElement.h"

@interface Diet : BaseElement

@property NSString *dietId;
@property NSString *serverDietId;
@property NSDate *createdAt;
@property NSDate *updatedAt;
@property NSString *owner;
@property NSString *title;
@property NSString *content;
@property NSString *note;
@property NSNumber<RLMInt> *numberOfMeals;
@property NSString *privacity;
@property NSNumber <RLMInt> *order;

///////////////////////////////// Attributes

@property NSNumber<RLMInt> *calories;                       //calories
@property NSNumber<RLMInt> *carbs;                          //carbs
@property NSNumber<RLMInt> *conversionFactor;               //conversionFactor
@property NSString *conversionString;                       //conversionString
@property NSNumber<RLMInt> *fats;                           //fats
@property NSNumber<RLMInt> *fiber;                          //fiber
@property NSString *measureUnit;                            //measureUnit
@property NSNumber<RLMFloat> *monoUnsaturatedFats;          //monoUnsaturatedFats
@property NSNumber<RLMFloat> *polyUnsaturatedFats;          //polyUnsaturatedFats
@property NSNumber<RLMInt> *proteins;                       //proteins
@property NSNumber<RLMFloat> *sodium;                       //sodium
@property NSNumber<RLMInt> *sugar;                          //sugar
@property NSNumber<RLMFloat> *water;                        //water

@property NSNumber <RLMInt> *agpAgs;                        //agpAgs
@property NSNumber <RLMFloat> *agpAgsAgm;                   //agpAgsAgm
@property NSNumber <RLMInt> *alcohol;                       //alcohol
@property NSNumber <RLMInt> *bccas;                         //bccas
@property NSNumber <RLMInt> *betaCaronete;                  //betaCarotene
@property NSNumber <RLMInt> *calcium;                       //calcium
@property NSNumber <RLMFloat> *cholesterol;                 //cholesterol
@property NSNumber <RLMFloat> *folicAcid;                   //folicAcid
@property NSNumber <RLMFloat> *glutamine;                   //glutamine
@property NSNumber <RLMFloat> *glycemicIndex;               //glycemicIndex
@property NSNumber <RLMFloat> *iodine;                      //iodine
@property NSNumber <RLMFloat> *iron;                        //iron
@property NSNumber <RLMFloat> *magnesium;                   //magnesium
@property NSNumber <RLMFloat> *niacin;                      //niacin
@property NSNumber <RLMFloat> *omega3;
@property NSNumber <RLMFloat> *omega6;
@property NSNumber <RLMFloat> *phosphorus;
@property NSNumber <RLMFloat> *potasium;
@property NSNumber <RLMFloat> *retynol;
@property NSNumber <RLMFloat> *saturedFats;
@property NSNumber <RLMFloat> *selenium;
@property NSNumber <RLMFloat> *vitA;
@property NSNumber <RLMFloat> *vitB1;
@property NSNumber <RLMFloat> *vitB2;
@property NSNumber <RLMFloat> *vitB12;
@property NSNumber <RLMFloat> *vitB6;
@property NSNumber <RLMFloat> *vitC;
@property NSNumber <RLMFloat> *vitD;
@property NSNumber <RLMFloat> *zinc;

@property NSNumber <RLMBool> *isFavourite;                             // Property to asign if is favourite
@property NSNumber <RLMBool> *isShared;                                // Property to asign if it is shared
@property NSNumber <RLMBool> *isSelected;
@property NSNumber <RLMBool> *isCopy;

@property RLMArray <DietMeal *><DietMeal> *dietMeals;
@property RLMArray <Ingredient *><Ingredient> *ingredients;

+ (BOOL)deleteAllDiets;
+ (BOOL)saveDiets:(NSMutableArray *)diets;

+ (RLMResults *)getDiets;
+ (RLMResults *)getMyDiets;
+ (RLMResults*)getMyDietsWithKeyword:(NSString*)keyword;
+ (RLMResults *)getOtherDiets;
+ (RLMResults *)getOtherDietsWithKeyword:(NSString*)keyword;
+ (RLMResults *)getDietsWithOwner:(NSString *)owner;
+ (RLMResults *)getFavouritesDiets;
+ (RLMResults *)getFavouritesDietsWithKeyword:(NSString*)keyword;
+ (RLMResults *)getSharedDiets;
+ (RLMResults *)getSharedDietsWithKeyword:(NSString*)keyword;
+ (Diet *)dietWithId:(NSString *)dietId;
+ (Diet *)dietWithServerId:(NSString *)serverDietId;

+ (BOOL)deleteDiet:(Diet *)diet;
+ (BOOL)deleteMealsInDiet:(Diet *)diet;
+ (BOOL)canDeleteDiet:(Diet *)diet;

+ (Diet *)duplicateDiet:(Diet *)diet;

+ (NSMutableArray *)getTagsInDiet:(Diet *)diet;

+ (NSInteger)getDietQuantity:(Diet*)diet;
+ (float)getKCalInDiet:(Diet *)diet;
+ (NSInteger)getDietProtein:(Diet *)diet;
+ (NSInteger)getDietCarb:(Diet *)diet;
+ (NSInteger)getDietFats:(Diet *)diet;
+ (NSInteger)getDietSugar:(Diet *)diet;
+ (float)getDietSaturedFats:(Diet *)diet;
+ (float)getDietMonoUnsaturedFats:(Diet *)diet;
+ (float)getDietPolyUnsaturedFats:(Diet *)diet;
+ (float)getDietFiber:(Diet *)diet;
+ (float)getDietBccas:(Diet *)diet;
+ (float)getDietGlutamine:(Diet *)diet;
+ (float)getDietOmega3:(Diet *)diet;
+ (float)getDietOmega6:(Diet *)diet;
+ (float)getDietWater:(Diet *)diet;
+ (float)getDietAlcohol:(Diet *)diet;
+ (float)getDietIron:(Diet *)diet;
+ (float)getDietIodine:(Diet *)diet;
+ (float)getDietMagnesium:(Diet *)diet;
+ (float)getDietZinc:(Diet *)diet;
+ (float)getDietSelenium:(Diet *)diet;
+ (float)getDietSodium:(Diet *)diet;
+ (float)getDietPotasium:(Diet *)diet;
+ (float)getDietPhosphorus:(Diet *)diet;
+ (float)getDietVitB1:(Diet *)diet;
+ (float)getDietVitB2:(Diet *)diet;
+ (float)getDietNiacin:(Diet *)diet;
+ (float)getDietVitA:(Diet *)diet;
+ (float)getDietFolicAcid:(Diet *)diet;
+ (float)getDietVitB12:(Diet *)diet;
+ (float)getDietVitC:(Diet *)diet;
+ (float)getDietVitD:(Diet *)diet;

+ (NSDate *)lastUpdateDate;

- (void)parseFromDictionary:(NSDictionary*)mealDictionary;

@end

RLM_ARRAY_TYPE(Diet)
