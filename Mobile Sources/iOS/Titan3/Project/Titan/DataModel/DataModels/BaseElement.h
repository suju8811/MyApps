//
//  BaseElement.h
//  Titan
//
//  Created by Marcus Lee on 22/12/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import <Realm/Realm.h>
#import "RateInfo.h"

@interface BaseElement : RLMObject

@property RLMArray <RateInfo *><RateInfo> *rateInfos;

- (void)parseFromDictionary:(NSDictionary*)elementDictionary;

@end
