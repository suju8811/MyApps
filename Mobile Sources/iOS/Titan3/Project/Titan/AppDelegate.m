//
//  AppDelegate.m
//  Titan
//
//  Created by Manuel Manzanera on 22/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#import "AppDelegate.h"
#import "SplashViewController.h"
#import "InitViewController.h"
#import "RegisterNavController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Parse/Parse.h>
#import <ParseFacebookUtilsV4/ParseFacebookUtilsV4.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>
#import "Titan-Swift.h"

#define PARSE_APP_KEY       @"UKRh7XbuOXadmMQlow3cmOSUp0v8pYvftnULXc7C"
#define PARSE_CLIENT_KEY    @"eqKhTryI61zsaOh8kqMleTewuGIhqbBAK9Fn8FsZ"

@interface AppDelegate () <UNUserNotificationCenterDelegate, FIRMessagingDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    SplashViewController *splashViewController = [[SplashViewController alloc] init];
    
    [Fabric with:@[[Crashlytics class]]];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor blackColor];
    [self.window setRootViewController:splashViewController];
    [self.window makeKeyAndVisible];
    
    //
    // Configure Push Notification
    //
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if (!error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                });
            }
        }];
        
        [FIRMessaging messaging].remoteMessageDelegate = self;
    }
    else {
        UIUserNotificationSettings * settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeBadge categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    [FIRApp configure];
    if ([FIRInstanceID instanceID].token) {
        NSLog(@"Firebase Instance ID token: %@", [FIRInstanceID instanceID].token);
        
        [iApp.sharedInstance setFcmRegIdWithIos_fcm_regid:[FIRInstanceID instanceID].token];
    }

    [self connectToFcm];
    
    //
    // Parse Configuration
    //
    
    // Set App ID
    ParseClientConfiguration * configuration = [ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration>  _Nonnull configuration) {
        configuration.applicationId = PARSE_APP_KEY;
        configuration.clientKey = PARSE_CLIENT_KEY;
        configuration.server = @"https://parseapi.back4app.com";
    }];
    [Parse initializeWithConfiguration:configuration];
    
    // REGISTER FOR PUSH NOTIFICATIONS
    UIUserNotificationSettings * settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound
                                                                              categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    application.applicationIconBadgeNumber = 0;


    // Init Facebook Utils
    [PFFacebookUtils initializeFacebookWithApplicationLaunchOptions:launchOptions];

//    // ADD 3D-TOUCH SHORTCUT ACTIONS FOR HARD-PRESS ON THE APP ICON
//    if #available(iOS 9.0, *) {
//        let shortcut1 = UIMutableApplicationShortcutItem(type: "post",
//                                                         localizedTitle: "Post Picture",
//                                                         localizedSubtitle: "",
//                                                         icon: UIApplicationShortcutIcon(type: .add),
//                                                         userInfo: nil
//                                                         )
//
//        let shortcut2 = UIMutableApplicationShortcutItem(type: "search",
//                                                         localizedTitle: "Search on \(APP_NAME)",
//                                                         localizedSubtitle: "",
//                                                         icon: UIApplicationShortcutIcon(type: .search),
//                                                         userInfo: nil
//                                                         )
//
//        let shortcut3 = UIMutableApplicationShortcutItem(type: "share",
//                                                         localizedTitle: "Share \(APP_NAME)",
//                                                         localizedSubtitle: "",
//                                                         icon: UIApplicationShortcutIcon(type: .share),
//                                                         userInfo: nil
//                                                         )
//
//        application.shortcutItems = [shortcut1, shortcut2, shortcut3]
//
//    } else { /* Fallback on earlier versions */ }
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
}

//func tokenRefreshNotification(_ notification: Notification) {
//
//    if let refreshedToken = FIRInstanceID.instanceID().token() {
//
//        iApp.sharedInstance.setFcmRegId(ios_fcm_regid: refreshedToken);
//    }
//
//    // Connect to FCM since connection may have failed when attempted before having a token.
//    connectToFcm()
//}

- (void)connectToFcm {
    [FIRMessaging.messaging connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect with FCM. \(error)");
        }
        else {
            NSLog(@"Connected to FCM.");
            NSString * refreshedToken = FIRInstanceID.instanceID.token;
            if (refreshedToken) {
                NSLog(@"%@", refreshedToken);
                [[iApp sharedInstance] setFcmRegIdWithIos_fcm_regid:refreshedToken];
            }
        }
    }];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Titan"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

+ (AppDelegate *)sharedDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark - UNUserNotificationCenterDelegate

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    NSLog(@"Tapped in notification");
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSLog(@"Notification being triggered");
    
    if ([notification.request.identifier isEqualToString:@"follower"]) {
        completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionSound | UNNotificationPresentationOptionBadge);
    }
    else if ([notification.request.identifier isEqualToString:@"message"]) {
        completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionSound | UNNotificationPresentationOptionBadge);
    }
    else if ([notification.request.identifier isEqualToString:@"system"]) {
        completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionSound | UNNotificationPresentationOptionBadge);
    }
    else if ([notification.request.identifier isEqualToString:@"custom"]) {
        completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionSound | UNNotificationPresentationOptionBadge);
    }
    else if ([notification.request.identifier isEqualToString:@"personal"]) {
        completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionSound | UNNotificationPresentationOptionBadge);
    }
}

#pragma mark - FIRMessagingDelegate

- (void)createNotifyWithId:(NSString*)notifyId
                     title:(NSString*)title
                  subtitle:(NSString*)subtitle
                      body:(NSString*)body {
    
    NSLog(@"notification will be triggered in five seconds..Hold on tight");
    
    UNMutableNotificationContent * content = [[UNMutableNotificationContent alloc] init];
    content.title = title;
    
    if (subtitle.length > 0) {
        content.subtitle = subtitle;
    }
    
    if (body.length > 0) {
        content.body = body;
    }
    
    content.sound = [UNNotificationSound defaultSound];
    
    UNTimeIntervalNotificationTrigger * trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:0.1 repeats:NO];
    UNNotificationRequest * request = [UNNotificationRequest requestWithIdentifier:notifyId content:content trigger:trigger];
    
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"%@", error.localizedDescription);
        }
    }];
}

// Receive data message on iOS 10 devices while app is in the foreground.
- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    NSString * type = [remoteMessage.appData valueForKey:@"type"];
    if (type) {
        NSInteger mType = [type integerValue];
        NSString * accountId = [remoteMessage.appData valueForKey:@"accountId"];
        NSInteger mAccountId = [accountId integerValue];
        
        switch (mType) {
            case 9: // GCM_NOTIFY_MESSAGE
                if ([[iApp sharedInstance] getId] != 0 && [[iApp sharedInstance] getId] == mAccountId) {
                    NSInteger mChatId = [[remoteMessage.appData valueForKey:@"id"] integerValue];

                    if ([[iApp sharedInstance] getCurrentChatId] != mChatId) {
                        if ([[iApp sharedInstance] getMessagesCount] == 0) {
                            [[iApp sharedInstance] setMessagesCountWithMessagesCount:[[iApp sharedInstance] getMessagesCount] + 1];
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateBadges" object:nil];
                        }
                        
                        if ([[iApp sharedInstance] getAllowMessagesGCM] == 1) {
                            [self createNotifyWithId:@"message"
                                               title:NSLocalizedString(@"label_new_message_title", @"")
                                            subtitle:@""
                                                body:NSLocalizedString(@"label_new_message", @"")];
                            
                        }
                    }

                    if ([[iApp sharedInstance] getId] != 0 &&
                        [[iApp sharedInstance] getId] == mAccountId &&
                        [[iApp sharedInstance] getCurrentChatId] == mChatId) {
                        
                        [[iApp sharedInstance] setMsgInfo:[[remoteMessage.appData valueForKey:@"msgId"] integerValue]
                                               fromUserId:[[remoteMessage.appData valueForKey:@"msgFromUserId"] integerValue]
                                                     text:[remoteMessage.appData valueForKey:@"msgMessage"]
                                                 photoUrl:[remoteMessage.appData valueForKey:@"msgFromUserPhotoUrl"]
                                                 fullname:[remoteMessage.appData valueForKey:@"msgFromUserFullname"]
                                                 username:[remoteMessage.appData valueForKey:@"msgFromUserUsername"]
                                                 imageUrl:[remoteMessage.appData valueForKey:@"msgImgUrl"]
                                                  timeAgo:[remoteMessage.appData valueForKey:@"msgTimeAgo"]];

                        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateChat" object:nil];
                    }

                }

                break;
                
            case 7: // GCM_NOTIFY_FOLLOWER
                if ([[iApp sharedInstance] getId] != 0 &&
                    [[iApp sharedInstance] getId] == mAccountId) {
                    
                    [[iApp sharedInstance] setNotificationsCountWithNotificationsCount:[[iApp sharedInstance] getNotificationsCount] + 1];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateBadges" object:nil];

                    if ([[iApp sharedInstance] getAllowFollowersGCM] == 1) {
                        [self createNotifyWithId:@"follower" title:NSLocalizedString(@"label_new_follower_title", @"") subtitle:@"" body:NSLocalizedString(@"label_new_follower", @"")];
                    }
                }

                break;

            case 1: {// GCM_NOTIFY_SYSTEM
                NSString * appTitle = [[NSBundle mainBundle].infoDictionary valueForKey:(__bridge NSString*)kCFBundleNameKey];
                NSString * fcmMessage = [remoteMessage.appData valueForKey:@"msg"];
                [self createNotifyWithId:@"system" title:appTitle subtitle:@"" body:fcmMessage];
            }
                break;

            case 2: { // GCM_NOTIFY_CUSTOM
                if ([[iApp sharedInstance] getId] != 0) {
                    NSString * appTitle = [[NSBundle mainBundle].infoDictionary valueForKey:(__bridge NSString*)kCFBundleNameKey];
                    NSString * fcmMessage = [remoteMessage.appData valueForKey:@"msg"];

                    [self createNotifyWithId:@"custom" title:appTitle subtitle:@"" body:fcmMessage];
                }
            }

                break;

            case 8: { // GCM_NOTIFY_PERSONAL
                if ([[iApp sharedInstance] getId] != 0 &&
                    [[iApp sharedInstance] getId] == mAccountId) {

                    NSString * appTitle = [[NSBundle mainBundle].infoDictionary valueForKey:(__bridge NSString*)kCFBundleNameKey];
                    NSString * fcmMessage = [remoteMessage.appData valueForKey:@"msg"];
                    
                    [self createNotifyWithId:@"personal" title:appTitle subtitle:@"" body:fcmMessage];
                }
            }

                break;

            default:
                break;
        }
    }
}

@end
