//
//  KAppSetting.h
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppSettingBase.h"
#import "NotificationSetting.h"
#import "UnitSetting.h"
#import "ChatConfiguration.h"

#define PREFERENCE_NOTIFICATION_SETTING         @"AppData_Notification_Setting"
#define PREFERENCE_UNIT_SETTING                 @"AppData_Unit_Setting"
#define PREFERENCE_CHAT_CONFIGURATION           @"AppData_Chat_Configuration"

@interface AppSetting : AppSettingBase

+ (AppSetting *) sharedInstance;

/**
 *  Preferences
 */

@property (nonatomic, strong) NSString * deviceKeychainID;
@property (nonatomic, strong) NotificationSetting * notificationSetting;
@property (nonatomic, strong) UnitSetting * unitSetting;
@property (nonatomic, strong) ChatConfiguration * chatConfiguration;

@end

static inline AppSetting * appSetting() {
    
    return [AppSetting sharedInstance];
}
