//
//  AppSettingBase.m
//  AStreamServer
//
//  Created by Michael Lee on 9/17/15.
//  Copyright (c) 2015 Michael Lee. All rights reserved.
//

#import "AppSettingBase.h"

@implementation AppSettingBase

- (instancetype) init {
    
    self = [super init];
    if (self)
        [self setup];
    
    return self;
}

- (void) setup {
    
    [self loadSetting];
}

- (void) loadSetting {
    
}

- (void) saveSetting {
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSInteger) integerForKey:(NSString *)key defVal:(NSInteger)defVal {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:key])
        return [[NSUserDefaults standardUserDefaults] integerForKey:key];
    
    return defVal;
}

- (void) setInteger:(NSInteger)value forKey:(NSString *)key {
    
    [[NSUserDefaults standardUserDefaults] setInteger:value forKey:key];
}

- (BOOL) boolForKey:(NSString *)key defVal:(BOOL)defVal {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:key])
        return [[NSUserDefaults standardUserDefaults] boolForKey:key];
    
    return defVal;
}

- (void) setBool:(BOOL)value forKey:(NSString *)key {
    
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:key];
}

- (float) floatForKey:(NSString *)key defVal:(float)defVal {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:key])
        return [[NSUserDefaults standardUserDefaults] floatForKey:key];
    
    return defVal;
}

- (void) setFloat:(float)value forKey:(NSString *)key {
    
    [[NSUserDefaults standardUserDefaults] setFloat:value forKey:key];
}

- (double) doubleForKey:(NSString *)key defVal:(double)defVal {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:key])
        return [[NSUserDefaults standardUserDefaults] doubleForKey:key];
    
    return defVal;
}

- (void) setDouble:(double)value forKey:(NSString*)key {
    
    [[NSUserDefaults standardUserDefaults] setDouble:value forKey:key];
}

- (id) oldObjectForKey:(NSString*)key defVal:(id)defVal {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:key]) {

        NSData * data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        return [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }

    return defVal;
}

- (id) objectForKey:(NSString *)key defVal:(id)defVal {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:key]) {
        
        NSData * data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        return [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    
    return defVal;
    
//    NSString * filePath = [NSString stringWithFormat:@"%@%@", PACKAGEPATH, key];
//    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
//        
//        return [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
//    }
    
//    return defVal;
}

- (void) setObject:(id)value forKey:(NSString *)key {
    
    NSData * data = [NSKeyedArchiver archivedDataWithRootObject:value];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
//    [NSKeyedArchiver archiveRootObject:value toFile:[NSString stringWithFormat:@"%@%@", PACKAGEPATH, key]];
}

- (NSString *) stringForKey:(NSString *)key defVal:(NSString *)defVal {
    
    return [self objectForKey:key defVal:defVal];
}

- (void) setString:(NSString *)value forKey:(NSString *)key {
    
    [self setObject:value forKey:key];
}

@end
