//
//  AppSettingBase.h
//  AStreamServer
//
//  Created by Michael Lee on 9/17/15.
//  Copyright (c) 2015 Michael Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppSettingBase : NSObject

- (void) setup;
- (void) saveSetting;
- (void) loadSetting;

- (NSInteger) integerForKey:(NSString *)key defVal:(NSInteger)defVal;
- (void) setInteger:(NSInteger)value forKey:(NSString *)key;

- (BOOL) boolForKey:(NSString *)key defVal:(BOOL)defVal;
- (void) setBool:(BOOL)value forKey:(NSString *)key;

- (float) floatForKey:(NSString *)key defVal:(float)defVal;
- (void) setFloat:(float)value forKey:(NSString *)key;

- (double) doubleForKey:(NSString *)key defVal:(double)defVal;
- (void) setDouble:(double)value forKey:(NSString*)key;

- (id) oldObjectForKey:(NSString*)key defVal:(id)defVal;
- (id) objectForKey:(NSString *)key defVal:(id)defVal;
- (void) setObject:(id)value forKey:(NSString *)key;

- (NSString *) stringForKey:(NSString *)key defVal:(NSString *)defVal;
- (void) setString:(NSString *)value forKey:(NSString *)key;

@end
