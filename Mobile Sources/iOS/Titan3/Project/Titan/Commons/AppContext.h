//
//  AppContext.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "User.h"
#import <Realm/Realm.h>

@interface AppContext : NSObject

@property (nonatomic, strong) NSString *authToken;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, assign) BOOL isClient;
@property (nonatomic, assign) BOOL isAuthenticateClient;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, strong) NSString *pushToken;
@property (nonatomic, assign) NSInteger weight;

+ (AppContext *)sharedInstance;
+ (RLMRealm *)currentRealm;
+ (void)saveInitialData;

@end
