//
//  KAppSetting.m
//  Kalahari
//
//  Created by LMAN on 3/28/16.
//  Copyright © 2016 kalahari. All rights reserved.
//

#import "AppSetting.h"
#import "AppDelegate.h"
#import "STKeychain.h"

@implementation AppSetting

+ (AppSetting *) sharedInstance {
    
    static AppSetting * instance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        instance = [[AppSetting alloc] init];
    });
    
    return instance;
}

- (void)loadSetting {
    _notificationSetting = [self objectForKey:PREFERENCE_NOTIFICATION_SETTING defVal:[[NotificationSetting alloc] init]];
    _unitSetting = [self objectForKey:PREFERENCE_UNIT_SETTING defVal:[[UnitSetting alloc] init]];
    _chatConfiguration = [self objectForKey:PREFERENCE_CHAT_CONFIGURATION defVal:nil];
}

- (void)setNotificationSetting:(NotificationSetting *)notificationSetting {
    _notificationSetting = notificationSetting;
    [self setObject:_notificationSetting forKey:PREFERENCE_NOTIFICATION_SETTING];
}

- (void)setUnitSetting:(UnitSetting *)unitSetting {
    _unitSetting = unitSetting;
    [self setObject:_unitSetting forKey:PREFERENCE_UNIT_SETTING];
}

- (void)setChatConfiguration:(ChatConfiguration *)chatConfiguration {
    _chatConfiguration = chatConfiguration;
    [self setObject:_chatConfiguration forKey:PREFERENCE_CHAT_CONFIGURATION];
}

- (void)setDeviceKeychainID:(NSString *)deviceKeychainID {
    
    _deviceKeychainID = deviceKeychainID;
    
    NSString * objectString = deviceKeychainID;
    NSError * error = nil;
    [STKeychain storeUsername:@"ColorMatter_DeviceID" andPassword:objectString forServiceName:@"com.vitell.ColorMatter" updateExisting:YES error:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
}

#pragma mark -
#pragma mark - Init App Data

- (id)init {
    
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

@end
