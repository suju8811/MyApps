//
//  AppContext.m
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//


#import "AppContext.h"
#import "WebServiceManager.h"
#import "AppConstants.h"
#import "Schedule.h"
#import "SchedulePlan.h"
#import "RateInfo.h"
#import "Log.h"
#import "Food.h"
#import "Progress.h"
#import "TagUtil.h"

@implementation AppContext

+ (AppContext *)sharedInstance
{
    static dispatch_once_t once;
    static AppContext *appContext;
    dispatch_once(&once, ^{ appContext = [[self alloc] init]; });
    return appContext;
}

+ (RLMRealm *)currentRealm{
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    return realm;
}

+ (void)saveInitialData {
    if([[AppContext sharedInstance] pushToken].length > 5) {
        //            NSDictionary *tokenDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[[AppContext sharedInstance] pushToken],@"pushToken",@"Titan",@"appName",[UtilManager getMetadataDictionary],@"metadata", nil];
        //[WebServiceManager postToken:tokenDictionary andDelegate:nil];
    }
    
    [RateInfo deleteAllRateInfos];
    [Log deleteAllLogs];
    [Tag deleteAllTags];
    
    [Serie deleteAllSeries];
    [Exercise deleteAllExercises];
    [TrainingDay deleteAllTrainingDays];
    [TrainingDayContent deleteAllTrainingDayContents];
    [TrainingExercise deleteAllTrainingExercises];
    [Training deleteAllTraining];
    [TrainingPlan deleteAllTrainingPlans];
    
    [Aliment deleteAllAliments];
    [Food deleteAllFoods];
    [Ingredient deleteAllIngredients];
    [NutritionDay deleteAllNutritionDays];
    [FoodAttribute deleteAllFoodAttributes];
    [DietMeal deleteAllDietMeals];
    [Meal deleteAllMeals];
    [Diet deleteAllDiets];
    [NutritionPlan deleteAllNutritionPlans];
    
    [TagUtil getTagsWithCompletionBlock:^(BOOL isSuccess) {
        [WebServiceManager getExercises];
        [WebServiceManager getTrainingsWithCompletionBlock:^(BOOL isSuccess) {
            [WebServiceManager getTrainingPlansWithCompletionBlock:^(BOOL isSuccess) {
                [WebServiceManager getAlimentsWithCompletionBlock:^(BOOL isSuccess) {
                    [WebServiceManager getMealsWithCompletionBlock:^(BOOL isSuccess) {
                        [WebServiceManager getDietsWithWithCompletionBlock:^(BOOL isSuccess) {
                            [WebServiceManager getNutrionalPlansWithCompletionBlock:^(BOOL isSuccess) {
                                [SchedulePlan deleteAllSchedulePlans];
                                [Event deleteAllEvents];
                                [Schedule getSchedules];
                                
                                [WebServiceManager getUserRevision:[[AppContext sharedInstance] userId] andDelegate:self];
                                [WebServiceManager getConstants];
                            }];
                        }];
                    }];
                }];
            }];
        }];
    }];
    
    [Progress getLogs];
}

@end
