//
//  AppConstants.h
//  Titan
//
//  Created by Manuel Manzanera on 26/1/17.
//  Copyright © 2017 inup. All rights reserved.
//

#ifndef AppConstants_h
#define AppConstants_h

//
// Chat URL
//

#define CHAT_API_BASE_PATH      @"http://ineedhere.com/api/v1/method/"

////////////////////////// FRAMES /////////////////////////////

#define WINDOW_ROOT  [UIApplication sharedApplication].keyWindow.rootViewController.view
#define VIEW_FRAME CGRectMake(0., 0., [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)
#define WIDTH [[UIScreen mainScreen] bounds].size.width
#define HEIGHT [[UIScreen mainScreen] bounds].size.height
#define NAV_HEIGHT [UtilManager height:72]
#define UITEXTFIELD_HEIGHT 41
#define TAB_BAR_HEIGHT [UtilManager height:50]
#define FOOTER_BAR_HEIGHT [UtilManager height:60]
#define UISEARCHBAR_HEIGHT [UtilManager height:50]
#define TAGVIEW_SMALL_HEIGHT [UtilManager height:15]
#define STATUS_BAR_HEIGHT [UtilManager height:20]

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define FUNCTION_VIEWCONTROLLER_WIDTH WIDTH * 0.8

//LOGIN & REGISTER
#define LOGIN_BUTTON_HEIGHT [UtilManager height:55]


////////////////////////// FONTS /////////////////////////////
#define LIGHT_FONT @"Dolce Vita Light"
#define REGULAR_FONT @"Dolce Vita"
#define BOLD_DOLCE_FONT @"Dolce Vita Heavy"

#define LIGHT_FONT_OPENSANS @"OpenSans-Light"
#define REGULAR_FONT_OPENSANS @"OpenSans"
#define CONDLIGHT_FONT @"OpenSans-CondensedLight"
#define SEMIBOLD_FONT @"OpenSans-Semibold"
#define ITALIC @"OpenSans-Italic"
#define LIGHT_ITALIC @"OpenSansLight-Italic"
#define CONDBOLD_FONT @"OpenSans-CondBold"
#define BOLD_FONT @"OpenSans-Bold"
#define EXTRA_BOLD_FONT @"OpenSans-Extrabold"
#define SEMI_BOLD_ITALIC @"OpenSans-SemiboldItalic"
#define COND_BOLD_ITALIC @"OpenSans-CondensedLightItalic"
#define BOLD_ITALIC @"OpenSans-BoldItalic"
#define EXTRA_BOLD_ITALIC_FONT @"OpenSans-ExtraboldItalic"

////////////////////////// COLORS ///////////////////////////
#define BLACK_APP_COLOR [UtilManager colorwithHexString:@"20232d" alpha:1]
//#define BLACK_HEADER_APP_COLOR [UtilManager colorwithHexString:@"2b2e3b" alpha:1]
#define BLACK_HEADER_APP_COLOR [UIColor colorWithRed:42/255. green:46/255. blue:58/255. alpha:1]
#define YELLOW_APP_COLOR [UtilManager colorwithHexString:@"eae200" alpha:1]
#define GRAY_REGISTER_FONT [UtilManager colorwithHexString:@"65666a" alpha:1]
#define BLUE_FACEBOOK_FONT [UtilManager colorwithHexString:@"23399b" alpha:1]
#define RED_APP_COLOR [UtilManager colorwithHexString:@"ff4748" alpha:1]
#define GREEN_APP_COLOR [UtilManager colorwithHexString:@"00cd7b" alpha:1]

#define BLUE_PERCENT_CIRCLE [UtilManager colorwithHexString:@"3caae3" alpha:1]
#define YELLOW_PERCENT_CIRCLE [UtilManager colorwithHexString:@"e4b50b" alpha:1]
#define ORANGE_PERCENT_CIRCLE [UtilManager colorwithHexString:@"f05707" alpha:1]
#define BLUE_WEIGTH_PERCENT_CIRCLE [UtilManager colorwithHexString:@"7abeee" alpha:1]

#define exerciseCellHeight [UtilManager height:80]
#define planTitleCellHeight [UtilManager height:80]
#define planCellHeight [UtilManager height:80]
#define trainingPlanCellHeight [UtilManager height:150]

#define foodCellHeight [UtilManager height:120]
#define planFooodCellHeight [UtilManager height:150]
#define dietCellHeight [UtilManager height:140]
#define alimentCellHeigtht [UtilManager height:90]

#define FMetPike 1.05
#define ADMIN_OWNER_ID @"pY3QN58pqefLYYX3y"

//y2dqcM9QRQHRfsKX7?store=images

#define PATH_IMAGE @"http://titan.inup.es:3008/cfs/files/profilePictures/"
#define IMAGES_EXTENSION @"?store=images"
#define THUMBS_EXTENSION @"?store=thumbs"
#define PATH_IMAGE_EXERCISES @"http://titan.inup.es:3008/cfs/files/exercisesPhotos/"
#define PATH_IMAGE_FOOD @"http://titan.inup.es:3008/cfs/files/foodsPhotos/"
#define PATH_IMAGE_MEAL @"http://titan.inup.es:3008/cfs/files/mealsPictures/"
#define PATH_IMAGE_LOGS @"http://titan.inup.es:3008/cfs/files/usersRevisionPhotos/"


///////////////////////// ALERTS //////////////////////////
#define NO_INTERNET_CONNECTION_ALERT NSLocalizedString(@"Necesita conexión a Internet.", nil)

//////////////////////// USERDEFAULTLIBRARY KEYS //////////

#define IS_USER_LOGIN @"isUserLogin"
#define USER_EMAIL @"userEmail"
#define USER_PASSWORD @"userPassword"
#define USER_NAME @"userName"

///////////////////////// NOTIFICATIONS //////////////////////////

#define NAV_RIGHT1_ACTION @"NotificationNavigationBarRight1Action"
#define NAV_RIGHT2_ACTION @"NotificationNavigationBarRight2Action"

#endif /* AppConstants_h */
