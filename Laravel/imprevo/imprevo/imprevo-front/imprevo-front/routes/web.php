<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Level;
use App\Code;
/*Route::get('/', function () {
    return view('index');
});*/
$locale = Code::where("code_name", "lang")->first()->code_value;
app()->setLocale($locale);


Route::get('/tryit', function () {
    return view('tryit');
});

Route::get('/courses', function () {
    return view('courses');
});

Route::get('/freebies', function () {
    return view('freebies');
});

Route::get('/password/forgot_password_success', function(){

  return view('auth/passwords/forgot_password_success');
});
Auth::routes();

Route::get('/home', 'HomeController@index');
Route::post('/home', 'HomeController@AcceptCookie');
Route::get('/', 'PageController@ViewPage');

Route::get('user/activation/{token}', 'Auth\RegisterController@userActivation');


Route::get('/lessons/{id}', 'LessonController@index');
Route::get('/lessonHome/{id}', 'LessonHomeController@index');
Route::get('/exercise/{id}', 'ExerciseController@index');
Route::post('/exercise/{id}', 'ExerciseController@AddUserData');

Route::get('/sales/course/{id}', 'CoursesalesController@ViewPage');
Route::get('/shoppingcart', 'ShoppingcartController@index');

Route::get('/order', array('as' => 'order', 'uses' => 'OrderController@index'));
Route::post('/order/billing1', 'OrderController@login');
Route::post('/order/billing2', 'OrderController@register');
Route::post('/order/billinginfo', 'OrderController@billinginfo');
Route::post('/order/payments', 'OrderController@payments');
Route::post('/order/purchase_overview', 'OrderController@purchase_overview');
Route::get('/order/status/pending', 'OrderController@payment_pending');
Route::get('/order/status/successful', 'OrderController@payment_success');
Route::get('/order/status/failed', 'OrderController@payment_failed');

Route::post('/payment/paypal', array('as' => 'addmoney.paypal','uses' => 'PaypalController@postPaymentWithpaypal'));
Route::post('/paypal/ipn', array('as' => 'addmoney.paypal.ipn','uses' => 'PaypalController@paypalIpn'));
Route::get('paypal', array('as' => 'payment.paypal.status','uses' => 'PaypalController@getPaymentStatus',));

Route::post('/payment/barion', array('as' => 'addmoney.barion','uses' => 'BarionController@postPaymentWithbarion'));
//Route::get('/barion', array('as' => 'payment.barion.status','uses' => 'BarionController@getPaymentStatus',));
Route::post('/barion/callback', array('as' => 'payment.barion.status','uses' => 'BarionController@getPaymentStatus',));

Route::post('/payment/bank', array('as' => 'addmoney.barion','uses' => 'BankController@postPaymentWithBank'));



Route::get('/profile/getstates', 'ProfileController@getstates');
Route::get('/profile', array('as' => 'profile','uses' => 'ProfileController@ViewProfile',));
Route::post('/profile' ,'ProfileController@PostProfile');
Route::post('/contact', 'ContactController@PostMessage');

Route::get('/shop', 'ShopController@index');
Route::post('/search', 'CourseController@query');
Route::get('/blog/category/{name}', 'BlogController@index');
Route::get('/blog/{static_url}', 'BlogController@ViewBlogPageByIndex');
Route::post('/blog/{static_url}', 'BlogController@ViewBlogPageByIndex');

Route::get('/blog', 'BlogController@index');
Route::get('/course/{id}', 'CourseController@index');
Route::get('/test', 'TestController@index');
Route::post('/reporterror', 'ExerciseController@ReportError');
Route::get('/{static_url}', 'PageController@ViewPage');
