-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: imprevo
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activations`
--

DROP TABLE IF EXISTS `activations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activations`
--

LOCK TABLES `activations` WRITE;
/*!40000 ALTER TABLE `activations` DISABLE KEYS */;
INSERT INTO `activations` VALUES (3,4,'oNqL3z9a0dSBo07AkATqJPLf4hkzXgJ0',1,'2016-12-01 19:36:12','2016-12-01 19:36:12','2016-12-01 19:36:12');
/*!40000 ALTER TABLE `activations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(20) DEFAULT NULL,
  `contents` varchar(255) DEFAULT '',
  `word_id` varchar(255) DEFAULT '',
  `question_id` int(20) DEFAULT NULL,
  `type` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,'33','',1,0),(2,'34','',1,0),(3,'35','1',NULL,1);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cats`
--

DROP TABLE IF EXISTS `cats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cats`
--

LOCK TABLES `cats` WRITE;
/*!40000 ALTER TABLE `cats` DISABLE KEYS */;
INSERT INTO `cats` VALUES (15,'Fruits','2016-12-10 00:15:06','2016-12-10 00:32:53'),(16,'Animals','2016-12-10 00:15:11','2016-12-10 00:33:00'),(17,'Grammar','2017-01-20 19:25:49','2017-02-20 10:42:50');
/*!40000 ALTER TABLE `cats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codes`
--

DROP TABLE IF EXISTS `codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code_value` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codes`
--

LOCK TABLES `codes` WRITE;
/*!40000 ALTER TABLE `codes` DISABLE KEYS */;
INSERT INTO `codes` VALUES (1,'lang','en','0000-00-00 00:00:00','2016-12-20 17:02:35'),(2,'siteTitle','IMPREVO Learning','2016-02-14 11:30:07','2016-12-02 01:27:29'),(3,'siteDesc','This is test description','2016-12-02 00:43:09','2016-12-02 01:23:09'),(4,'keywords','imprevo, learning, english','2016-12-02 00:43:09','2016-12-02 01:23:09'),(5,'siteEmail','polarislee1984@outlook.com','2016-12-02 00:43:09','2016-12-02 01:23:09'),(6,'gaCode','Google analytics Code Sample test','2016-12-02 00:43:09','2017-01-23 23:16:06');
/*!40000 ALTER TABLE `codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Australia'),(2,'New Zealand'),(3,'UK'),(4,'USA'),(5,'Canada'),(7,'Ireland'),(13,'Afghanistan'),(14,'Albania'),(15,'Algeria'),(16,'American Samoa'),(17,'Andorra'),(18,'Angola'),(19,'Anguilla'),(20,'Antigua and Barbuda'),(21,'Argentina'),(22,'Armenia'),(23,'Aruba'),(25,'Azerbaijan'),(26,'Bahamas'),(27,'Bahrain'),(28,'Bangladesh'),(29,'Barbados'),(30,'Belarus'),(31,'Belgium'),(32,'Belize'),(33,'Benin'),(34,'Bermuda'),(35,'Bhutan'),(36,'Bolivia'),(37,'Bosnia-Herzegovina'),(38,'Botswana'),(39,'Bouvet Island'),(40,'Brazil'),(41,'Brunei'),(42,'Bulgaria'),(43,'Burkina Faso'),(44,'Burundi'),(45,'Cambodia'),(46,'Cameroon'),(47,'Cape Verde'),(48,'Cayman Islands'),(49,'Central African Republic'),(50,'Chad'),(51,'Chile'),(52,'China'),(53,'Christmas Island'),(54,'Cocos (Keeling) Islands'),(55,'Colombia'),(56,'Comoros'),(57,'Congo, Democratic Republic of the (Zaire)'),(58,'Congo, Republic of'),(59,'Cook Islands'),(60,'Costa Rica'),(61,'Croatia'),(62,'Cuba'),(63,'Cyprus'),(64,'Czech Republic'),(65,'Denmark'),(66,'Djibouti'),(67,'Dominica'),(68,'Dominican Republic'),(69,'Ecuador'),(70,'Egypt'),(71,'El Salvador'),(72,'Equatorial Guinea'),(73,'Eritrea'),(74,'Estonia'),(75,'Ethiopia'),(76,'Falkland Islands'),(77,'Faroe Islands'),(78,'Fiji'),(79,'Finland'),(80,'France'),(81,'French Guiana'),(82,'Gabon'),(83,'Gambia'),(84,'Georgia'),(85,'Germany'),(86,'Ghana'),(87,'Gibraltar'),(88,'Greece'),(89,'Greenland'),(90,'Grenada'),(91,'Guadeloupe (French)'),(92,'Guam (USA)'),(93,'Guatemala'),(94,'Guinea'),(95,'Guinea Bissau'),(96,'Guyana'),(97,'Haiti'),(98,'Holy See'),(99,'Honduras'),(100,'Hong Kong'),(101,'Hungary'),(102,'Iceland'),(103,'India'),(104,'Indonesia'),(105,'Iran'),(106,'Iraq'),(107,'Israel'),(108,'Italy'),(109,'Ivory Coast (Cote D`Ivoire)'),(110,'Jamaica'),(111,'Japan'),(112,'Jordan'),(113,'Kazakhstan'),(114,'Kenya'),(115,'Kiribati'),(116,'Kuwait'),(117,'Kyrgyzstan'),(118,'Laos'),(119,'Latvia'),(120,'Lebanon'),(121,'Lesotho'),(122,'Liberia'),(123,'Libya'),(124,'Liechtenstein'),(125,'Lithuania'),(126,'Luxembourg'),(127,'Macau'),(128,'Macedonia'),(129,'Madagascar'),(130,'Malawi'),(131,'Malaysia'),(132,'Maldives'),(133,'Mali'),(134,'Malta'),(135,'Marshall Islands'),(136,'Martinique (French)'),(137,'Mauritania'),(138,'Mauritius'),(139,'Mayotte'),(140,'Mexico'),(141,'Micronesia'),(142,'Moldova'),(143,'Monaco'),(144,'Mongolia'),(145,'Montenegro'),(146,'Montserrat'),(147,'Morocco'),(148,'Mozambique'),(149,'Myanmar'),(150,'Namibia'),(151,'Nauru'),(152,'Nepal'),(153,'Netherlands'),(154,'Netherlands Antilles'),(155,'New Caledonia (French)'),(156,'Nicaragua'),(157,'Niger'),(158,'Nigeria'),(159,'Niue'),(160,'Norfolk Island'),(161,'North Korea'),(162,'Northern Mariana Islands'),(163,'Norway'),(164,'Oman'),(165,'Pakistan'),(166,'Palau'),(167,'Panama'),(168,'Papua New Guinea'),(169,'Paraguay'),(170,'Peru'),(171,'Philippines'),(172,'Pitcairn Island'),(173,'Poland'),(174,'Polynesia (French)'),(175,'Portugal'),(176,'Puerto Rico'),(177,'Qatar'),(178,'Reunion'),(179,'Romania'),(180,'Russia'),(181,'Rwanda'),(182,'Saint Helena'),(183,'Saint Kitts and Nevis'),(184,'Saint Lucia'),(185,'Saint Pierre and Miquelon'),(186,'Saint Vincent and Grenadines'),(187,'Samoa'),(188,'San Marino'),(189,'Sao Tome and Principe'),(190,'Saudi Arabia'),(191,'Senegal'),(192,'Serbia'),(193,'Seychelles'),(194,'Sierra Leone'),(195,'Singapore'),(196,'Slovakia'),(197,'Slovenia'),(198,'Solomon Islands'),(199,'Somalia'),(200,'South Africa'),(201,'South Georgia and South Sandwich Islands'),(202,'South Korea'),(203,'Spain'),(204,'Sri Lanka'),(205,'Sudan'),(206,'Suriname'),(207,'Svalbard and Jan Mayen Islands'),(208,'Swaziland'),(209,'Sweden'),(210,'Switzerland'),(211,'Syria'),(212,'Taiwan'),(213,'Tajikistan'),(214,'Tanzania'),(215,'Thailand'),(216,'Timor-Leste (East Timor)'),(217,'Togo'),(218,'Tokelau'),(219,'Tonga'),(220,'Trinidad and Tobago'),(221,'Tunisia'),(222,'Turkey'),(223,'Turkmenistan'),(224,'Turks and Caicos Islands'),(225,'Tuvalu'),(226,'Uganda'),(227,'Ukraine'),(228,'United Arab Emirates'),(229,'Uruguay'),(230,'Uzbekistan'),(231,'Vanuatu'),(232,'Venezuela'),(233,'Vietnam'),(234,'Virgin Islands'),(235,'Wallis and Futuna Islands'),(236,'Yemen'),(237,'Zambia'),(238,'Zimbabwe'),(239,'Austria'),(240,'Palestinian Territory');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_level`
--

DROP TABLE IF EXISTS `course_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` varchar(45) DEFAULT NULL,
  `level_id` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_level`
--

LOCK TABLES `course_level` WRITE;
/*!40000 ALTER TABLE `course_level` DISABLE KEYS */;
INSERT INTO `course_level` VALUES (1,'2','5',NULL,NULL),(2,'2','4',NULL,NULL),(3,'2','6',NULL,NULL),(6,'5','5',NULL,NULL),(8,'1','5',NULL,NULL),(9,'1','6',NULL,NULL),(10,'1','14',NULL,NULL),(11,'1','16',NULL,NULL),(12,'3','4',NULL,NULL),(13,'3','6',NULL,NULL),(14,'3','13',NULL,NULL),(15,'1','1',NULL,NULL),(16,'5','1',NULL,NULL),(17,'5','6',NULL,NULL),(18,'6','1',NULL,NULL),(19,'4','1',NULL,NULL),(20,'4','5',NULL,NULL),(21,'4','6',NULL,NULL),(22,'7','6',NULL,NULL),(23,'7','13',NULL,NULL),(24,'7','14',NULL,NULL);
/*!40000 ALTER TABLE `course_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_module`
--

DROP TABLE IF EXISTS `course_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` varchar(45) DEFAULT NULL,
  `module_id` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_module`
--

LOCK TABLES `course_module` WRITE;
/*!40000 ALTER TABLE `course_module` DISABLE KEYS */;
INSERT INTO `course_module` VALUES (2,'2','7',NULL,NULL),(3,'4','8',NULL,NULL),(4,'4','9',NULL,NULL),(5,'5','8',NULL,NULL),(7,'2','3',NULL,NULL),(8,'2','11',NULL,NULL),(9,'1','3',NULL,NULL),(10,'1','8',NULL,NULL),(11,'1','11',NULL,NULL),(12,'3','10',NULL,NULL),(13,'3','11',NULL,NULL),(14,'5','7',NULL,NULL),(15,'6','7',NULL,NULL),(16,'6','8',NULL,NULL),(17,'6','12',NULL,NULL),(18,'7','3',NULL,NULL),(19,'7','7',NULL,NULL),(20,'7','8',NULL,NULL),(21,'7','11',NULL,NULL);
/*!40000 ALTER TABLE `course_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `photo` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `is_public` char(1) DEFAULT '0',
  `is_free` char(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (1,'course_test1','/upload/image/1485876957_371.jpg','course1  description\r\n','1','1',NULL,'2017-01-31 07:36:01',NULL,NULL),(2,'course2','/upload/image/1485876976_m1.jpg','course2_description','1','1',NULL,'2017-01-31 07:36:16',NULL,NULL),(3,'Pre-intermediate',NULL,'Pre-intermediate-description','1','1','2017-01-31 07:45:54','2017-01-31 07:45:54',NULL,NULL),(4,'Elementary',NULL,'Elementary-description',NULL,'1','2017-01-31 07:46:27','2017-01-31 07:46:27',NULL,NULL),(5,'course11','/upload/image/1487593150_bg_fingers_with_stars.png','Test course','1','1','2017-02-20 12:19:10','2017-02-20 12:19:10',NULL,NULL),(6,'English course',NULL,'',NULL,NULL,'2017-02-20 12:29:30','2017-02-20 12:29:30',NULL,NULL),(7,'course12',NULL,'course12-description','1','1','2017-02-20 14:47:09','2017-02-20 14:47:09',NULL,NULL);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exercises`
--

DROP TABLE IF EXISTS `exercises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exercises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `video_url` varchar(256) DEFAULT NULL,
  `content1` varchar(2000) DEFAULT NULL,
  `content2` varchar(2000) DEFAULT NULL,
  `lesson_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `audio` varchar(256) DEFAULT NULL,
  `quiz_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exercises`
--

LOCK TABLES `exercises` WRITE;
/*!40000 ALTER TABLE `exercises` DISABLE KEYS */;
INSERT INTO `exercises` VALUES (14,'quiz','Select the image that’s represented by the word below!  ','quiz',8,NULL,NULL,NULL,1,'2017-03-01 09:04:33','2017-03-01 09:04:33',80,NULL,NULL,1),(15,'Olvasd el a szöveget  ','Olvasd el a szöveget','text',7,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.dol-ore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat. facer possim assum. Typi non habent',NULL,1,'2017-03-01 09:04:33','2017-03-01 09:04:33',80,NULL,NULL,NULL),(17,'Videó','Hogyan és mire használd a present perfect-et?   ','video',8,'https://www.youtube.com/watch?v=XGSy3_Czz8k',NULL,NULL,1,'2017-03-01 09:04:33','2017-03-01 09:04:33',80,NULL,NULL,NULL),(27,'exercise_test_quiz1234','exercise_test_descr','text',11,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis','1234',4,'2017-01-21 08:31:55','2017-01-21 08:34:55',80,NULL,NULL,NULL),(33,'Átirat','Hallás utáni szövegértés modul','translation',7,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis','Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',4,'2017-01-21 09:05:35','2017-01-21 09:05:35',80,NULL,'/upload/audio/1482482113_5.wav',NULL),(34,'ex1','ex1_description','text',7,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis','Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',5,'2017-02-15 02:01:29','2017-02-15 02:01:31',80,NULL,NULL,NULL),(35,'ex2','ex2_description','text',8,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis','Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',5,'2017-02-19 02:53:36','2017-02-19 02:53:39',80,NULL,NULL,NULL),(36,'ex3','ex3_description','text',8,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',NULL,6,'2017-02-19 02:53:37','2017-02-19 02:53:39',80,NULL,NULL,NULL),(37,'ex4','ex4_description','text',11,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',NULL,6,'2017-02-19 02:59:22','2017-02-19 02:59:23',80,NULL,NULL,NULL),(38,'ex5','ex5_description','text',11,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',NULL,7,'2017-02-19 03:18:32','2017-02-19 03:18:33',80,NULL,NULL,NULL),(39,'ex6','ex6_description','text',7,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',NULL,7,'2017-02-19 03:21:10','2017-02-19 03:21:12',80,NULL,NULL,NULL),(40,'ex7','ex7_description','text',8,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',NULL,8,'2017-02-19 03:21:11','2017-02-19 03:21:12',80,NULL,NULL,NULL),(41,'ex8','ex8_description','text',11,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',NULL,8,'2017-02-19 03:22:04','2017-02-19 03:22:05',80,NULL,NULL,NULL),(42,'ex9','ex9_description','text',7,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',NULL,9,'2017-02-19 03:22:50','2017-02-19 03:22:51',80,NULL,NULL,NULL),(43,'ex10','ex10_description','text',8,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',NULL,9,'2017-02-19 03:23:35','2017-02-19 03:23:36',80,NULL,NULL,NULL),(44,'ex11','ex11_description','text',11,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',NULL,10,'2017-02-19 03:25:04','2017-02-19 03:25:05',80,NULL,NULL,NULL),(45,'ex12','ex12_description','text',7,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',NULL,11,'2017-02-19 03:27:08','2017-02-19 03:27:09',80,NULL,NULL,NULL),(46,'ex13','ex13_description','text',8,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',NULL,12,'2017-02-19 03:27:39','2017-02-19 03:27:40',80,NULL,NULL,NULL),(47,'ex14','ex14_description','text',11,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',NULL,13,'2017-02-19 03:28:12','2017-02-19 03:28:13',80,NULL,NULL,NULL),(48,'ex15','ex15_description','text',7,NULL,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit,sed diam nonummy nibh euis- mod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis',NULL,14,'2017-02-20 03:32:16','2017-02-20 03:32:17',80,NULL,NULL,NULL),(50,'lesson25-text-exercise','lesson25-text-exercise-description','text',8,NULL,'fawefawe fawef awef awefawe fwe awefa wef  afwea wefaw efawe fawef awe brawebrawerawebrawerabwerawerbawerbawerabw ',NULL,15,'2017-02-20 11:15:49','2017-02-20 11:15:49',80,NULL,NULL,NULL),(51,'lesson25-translate-exercise','lesson25-translate-exercise-description','translation',11,NULL,'afwe fawe fawe fawefwefFirst1/2 column First1/2 columnFirst1/2 columnFirst1/2 column First1/2 column First1/2 column First1/2 columnFirst1/2 columnFirst1/2 columnFirst1/2 column First1/2 columnFirst1/2 column','Second1/2 column Second1/2 column Second1/2 column Second1/2 columnSecond1/2 column Second1/2 column  Second1/2 columnSecond1/2 column Second1/2 column Second1/2 column',15,'2017-02-20 11:16:54','2017-03-02 06:27:40',80,NULL,'/upload/audio/1488464860_20 - 1BE - 19 - LISTENING - FINAL.mp3',NULL),(52,'Famous Americans – George Washington!','','text',12,NULL,'<h3>Famous Americans – George Washington!</h3> <p>George Washington was the first President of the United States. He is also called one of the Founding Fathers of the country. </p>  <p>Washington was born in 1732, in Virginia. When he was a child, he studied maths and surveying. He wanted to go to school in England, but he couldn’t because his father died when he was 15.  </p>  <p>When he was 16, he started working as a surveyor. Once he made enough money, he bought a land in the Shenandoah Valley. When he was 20, he inherited a plantation called Mount Vernon from his brother. Washington’s brother, Lawrence, died of Tuberculosis. </p>  <p>He started his military career during the French and Indian war. During the war, the French captured him, but they later released him. </p>  <p>George married Martha Custis in 1759, who already had 2 children from her previous marriage. Because of his marriage, Washington became an aristocrat, and met important people. The family lived at the Mt. Vernon estate until the beginning of the American Revolution.</p>  <p>On his farm, he grew crops, tobacco and wheat, bred horses and he also owned a flour mill.</p>  <p>The Revolutionary war started in 1775. The Americans needed a well-respected leader who had military experience, and they chose Washington. He became the Commander-in-chief of the new Continental Army. In the end, the British army surrendered and America became an independent country.</p>  <p>In 1789, Washington became the first President of the country. The ceremony was held in New York, which was the nation’s capital at that time. He served two terms as president, and he died in 1799. </p> ',NULL,16,'2017-02-20 12:32:23','2017-02-20 12:35:22',80,NULL,NULL,NULL),(53,'Select image','','quiz',7,NULL,'','',16,'2017-02-20 13:12:36','2017-02-20 13:12:36',80,NULL,NULL,2),(54,'Possessive Pronouns','Please watch the video!','video',8,'https://www.youtube.com/watch?v=JoJaOmL4Upo',NULL,NULL,16,'2017-02-20 15:43:58','2017-02-25 09:29:11',80,NULL,NULL,NULL),(55,'Quiz-exe','Quiz-exe-description','quiz',3,NULL,'','asdasdasdasd',15,'2017-02-23 02:01:12','2017-03-02 08:48:05',80,NULL,'/upload/audio/1488473285_20 - 1BE - 19 - LISTENING - FINAL.mp3',4),(56,'Transcript','Here you can see the reading exercise and its Hungarian translation.','translation',12,NULL,'<h3>Famous Americans – George Washington!</h3>  <p>George Washington was the first President of the United States. He is also called one of the Founding Fathers of the country. </p>  <p>Washington was born in 1732, in Virginia. When he was a child, he studied maths and surveying. He wanted to go to school in England, but he couldn’t because his father died when he was 15.  </p>  <p>When he was 16, he started working as a surveyor. Once he made enough money, he bought a land in the Shenandoah Valley. When he was 20, he inherited a plantation called Mount Vernon from his brother. Washington’s brother, Lawrence, died of Tuberculosis. </p>  <p>He started his military career during the French and Indian war. During the war, the French captured him, but they later released him.  George married Martha Custis in 1759, who already had 2 children from her previous marriage. Because of his marriage, Washington became an aristocrat, and met important people. The family lived at the Mt. Vernon estate until the beginning of the American Revolution.</p>  <p>On his farm, he grew crops, tobacco and wheat, bred horses and he also owned a flour mill.</p>  <p>The Revolutionary war started in 1775. The Americans needed a well-respected leader who had military experience, and they chose Washington. He became the Commander-in-chief of the new Continental Army. In the end, the British army surrendered and America became an independent country.</p>  <p>In 1789, Washington became the first President of the country. The ceremony was held in New York, which was the nation’s capital at that time. He served two terms as president, and he died in 1799. </p>','<h3>Híres Amerikaiak – George Washington!</h3>  <p>George Washington volt az Egyesült Államok első elnöke. Az alapítóatyák egyikeként is szokták emlegetni. </p>  <p>1732-ben született Virginiában. Gyerekkorában matematikát és földmérést tanult. Angliába akart menni iskolába, de nem tudott, mert az apja meghalt 15 éves korában. </p>  <p>16 évesen kezdett el dolgozni földmérőként. Mihelyst elég pénzt gyűjtött, földet vásárolt a Shenandoah völgyben. 20 évesen örökölt egy ültetvényt a bátyjától, melyet Mount Vernonnak neveztek. Washington testvére, Lawrence, tuberkulózisban halt meg. </p>  <p>A katonai karrierjét a Francia-Indián háború alatt kezdte. A háború alatt a franciák foglyul ejtették, de később szabadon engedték.  George feleségül vette Martha Curtist 1759-ben, akinek már volt 2 gyermeke egy korábbi házasságból. A házassága révén George arisztokrata lett és fontos emberekkel találkozott. A család a Mount Vernon birtokon élt az amerikai szabadságharc kitöréséig. </p>  <p>A farmon búzát, dohányt termelt, lovakat tenyésztett és egy malomnak is a tulajdonosa volt.</p>  <p>A szabadságharc 1775-ben kezdődött. Az amerikaiaknak szükségük volt egy tiszteletnek örvendő vezetőre, aki katonai tapasztalattal is rendelkezett. Washingtont választották. Ő lett az új hadsereg főparancsnoka. Végül a Brit hadsereg megadta magát, Amerika pedig szabad országgá vált. </p>  <p>1789-ben Washington lett az ország első elnöke. A beiktatás New Yorkban volt, mivel ez volt a főváros abban az időben. Két terminust teljesített elnökként és 1799-ben halt meg.</p>',16,'2017-02-28 10:38:13','2017-03-02 06:41:27',80,NULL,'/upload/audio/1488465687_20 - 1BE - 19 - LISTENING - FINAL.mp3',NULL),(57,'Select the correct answer!','','quiz',12,NULL,'','',16,'2017-02-28 11:41:22','2017-02-28 11:41:22',80,NULL,NULL,5);
/*!40000 ALTER TABLE `exercises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lessons`
--

DROP TABLE IF EXISTS `lessons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lessons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `is_trial` char(1) DEFAULT '0',
  `course_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `is_public` char(1) DEFAULT '0',
  `is_free` char(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lessons`
--

LOCK TABLES `lessons` WRITE;
/*!40000 ALTER TABLE `lessons` DISABLE KEYS */;
INSERT INTO `lessons` VALUES (1,'lessonTest','test','/upload/image/1487473905_8.png','1',1,1,'1','1','2016-12-22 02:44:52','2017-02-18 19:11:45'),(4,'lesson1','lesson1-description','/upload/image/1487473858_6.png','1',1,1,'1',NULL,'2017-01-16 08:47:20','2017-02-18 19:10:58'),(5,'lesson2','lesson2-description','/upload/image/1482403492_images (2).jpeg','1',1,1,NULL,'1','2017-01-20 19:19:36','2017-01-20 19:19:36'),(6,'lesson3','lesson3-description','/upload/image/1487473884_7.png','1',1,1,'1','1','2017-01-21 08:31:08','2017-02-18 19:11:24'),(7,'lesson4','lesson4-description','/upload/image/1487473543_1.png',NULL,1,1,'1','1','2017-02-18 19:05:43','2017-02-18 19:05:43'),(8,'lesson5','lesson5-description','/upload/image/1487473595_2.png','1',1,1,'1','1','2017-02-18 19:06:35','2017-02-18 19:06:35'),(9,'lesson6','lesson6-description','/upload/image/1487473620_3.png',NULL,1,5,'1','1','2017-02-18 19:07:00','2017-02-18 19:07:00'),(10,'lesson7','lesson7-description','/upload/image/1487473761_4.png','1',1,5,'1',NULL,'2017-02-18 19:09:21','2017-02-18 19:09:21'),(11,'lesson8','lesson8-description','/upload/image/1487473798_5.png',NULL,1,5,'1','1','2017-02-18 19:09:58','2017-02-18 19:09:58'),(12,'lesson9','lesson9-description','/upload/image/1487473937_9.png','1',1,6,'1','1','2017-02-18 19:12:17','2017-02-18 19:12:17'),(13,'lesson10','lesson10-description','/upload/image/1487473961_10.png','1',1,6,'1','1','2017-02-18 19:12:41','2017-02-18 19:12:41'),(14,'lesson11','lesson11-description','/upload/image/1481635525_images (5).jpeg','0',1,6,'0','0','2017-02-20 03:30:55','2017-02-20 03:30:57'),(15,'Lesson25','Lesson25-description','/upload/image/1488443223_untitled.jpg','1',1,1,'1','1','2017-02-20 10:16:55','2017-03-02 00:27:03'),(16,'Lesson 20','','/upload/image/1487602196_images (1).jpg',NULL,6,1,NULL,NULL,'2017-02-20 12:31:27','2017-02-20 14:49:56');
/*!40000 ALTER TABLE `lessons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `levels`
--

DROP TABLE IF EXISTS `levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `levels`
--

LOCK TABLES `levels` WRITE;
/*!40000 ALTER TABLE `levels` DISABLE KEYS */;
INSERT INTO `levels` VALUES (1,'beginner','2016-12-13 05:22:24','2016-12-13 05:22:24'),(5,'immediate','2016-12-13 05:22:24','2016-12-13 05:22:24'),(6,'advanced','2016-12-13 05:22:24','2016-12-13 05:22:24'),(13,'level-1','2016-12-21 18:52:24','2016-12-22 19:15:51'),(14,'level-2','2016-12-21 18:52:24','2016-12-22 19:13:58'),(16,'level-3','2017-01-20 19:11:25','2017-01-20 19:11:25'),(17,'Reading','2017-02-20 12:29:43','2017-02-20 12:29:43');
/*!40000 ALTER TABLE `levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (3,'module1','2016-12-13 05:22:24','2016-12-13 05:22:24'),(7,'Szókincs','2016-12-13 05:25:25','2017-02-07 02:15:45'),(8,'Nyelvtan','2016-12-21 18:52:24','2017-02-07 02:16:25'),(9,'m2','2016-12-21 18:52:24','2016-12-21 18:52:24'),(10,'m3','2016-12-21 18:52:24','2016-12-21 18:52:24'),(11,'module2','2016-12-22 23:08:26','2016-12-22 23:08:26'),(12,'Reading','2017-02-20 12:30:12','2017-02-20 12:30:12');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) DEFAULT NULL,
  `content` varchar(4096) DEFAULT NULL,
  `seo_title` varchar(256) DEFAULT NULL,
  `seo_description` varchar(256) DEFAULT NULL,
  `seo_keywords` varchar(256) DEFAULT NULL,
  `adwords_code` varchar(4096) DEFAULT NULL,
  `facebook_pixel_code` varchar(4096) DEFAULT NULL,
  `is_show` char(1) DEFAULT NULL,
  `static_url` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='make pages ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'Page 1','      <div class=\"mdl-cell mdl-cell--12-col\" style=\"height:100px;\">\r\n        <img src=\"/pages/logo.png\" border=\"0\" alt=\"\" width=\"200\" height=\"100\">\r\n      </div>\r\n      <div class=\"mdl-cell mdl-cell--6-col\" style=\"\">\r\n        <div class=\"mdl-cell mdl-cell--12-col\" style=\"\">\r\n          <span style=\"font-weight:bold;font-size:25px\">Iratkozz fel a 20 részes, ingyenes angol tanfolyamra!</span>\r\n        </div>\r\n        <div class=\"mdl-cell mdl-cell--12-col\" style=\"\">\r\n          <span style=\"font-size:20px\">Teljesen kezdőknek is ideális, e-mailes leckék!</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"mdl-cell  mdl-cell--6-col\" style=\"\">\r\n        <img src=\"/pages/room.png\" border=\"0\" alt=\"\" width=\"300\" height=\"300\">\r\n      </div>','SEO_TITLE','SEO_DESCRIPTION','SEO_KEYWORDS','<script>\r\n  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\r\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r\n  })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');\r\n\r\n  ga(\'create\', \'UA-68401008-1\', \'auto\');\r\n  ga(\'send\', \'pageview\');\r\n\r\n</script>','<script>\r\n!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?\r\nn.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;\r\nn.push=n;n.loaded=!0;n.version=\'2.0\';n.queue=[];t=b.createElement(e);t.async=!0;\r\nt.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,\r\ndocument,\'script\',\'https://connect.facebook.net/en_US/fbevents.js\');\r\n\r\nfbq(\'init\', \'1692560164346282\');\r\nfbq(\'track\', \"PageView\");</script>\r\n<noscript><img height=\"1\" width=\"1\" style=\"display:none\"\r\nsrc=\"https://www.facebook.com/tr?id=1692560164346282&ev=PageView&noscript=1\"\r\n/></noscript>','1','page-1','2017-03-09 23:08:33','2017-03-13 07:34:01',80,NULL),(2,'Page 2','Page1-Content','','','','','','1','page-2','2017-03-12 18:43:37','2017-03-12 18:51:26',80,NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('polarislee1984@outlook.com','78531b4a62bb1b1b6ab1ef1d0e125f07263e238c4615185d948313040defd7f3','2017-01-23 08:19:21'),('hahaha906@outlook.com','$2y$10$vEGmJQ1lSmKvK1pljoF7We9gXNN8tsUpquqNa1lFXky/CYBKuBnFm','2017-01-31 19:42:12'),('skyClean906@gmail.com','$2y$10$QMh9ulogDipPGbh.kMrW.OYD80u1UtWtqMUhEHymG.448Urotpqdy','2017-02-18 02:56:46');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persistences`
--

DROP TABLE IF EXISTS `persistences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persistences`
--

LOCK TABLES `persistences` WRITE;
/*!40000 ALTER TABLE `persistences` DISABLE KEYS */;
INSERT INTO `persistences` VALUES (88,4,'pFG16BB6qgyRC5JZ17Sle6TgQwn8ODTS','2016-12-01 19:44:26','2016-12-01 19:44:26'),(91,4,'DhEkKwQaA0hDxHQOlkDEW0Vnzljtg54E','2016-12-01 23:12:53','2016-12-01 23:12:53'),(96,4,'oHucIoCD0qycKDjdbjPa1hk3IFa4Q6O6','2016-12-02 01:45:22','2016-12-02 01:45:22'),(99,4,'1dZA8OxB21ZzfJSsublBthX2lUHe6Esz','2016-12-02 06:48:11','2016-12-02 06:48:11'),(100,4,'RMzKZXDKxHa8IRhaaElCziZ5tEOOLpBC','2016-12-08 19:17:06','2016-12-08 19:17:06'),(101,4,'abcmLC0rmtDbzElEFJhZ4kdUmN8xJ24x','2016-12-08 19:31:31','2016-12-08 19:31:31'),(102,4,'BffzN27umyOGVun3StQj4U1OiVbVnB1J','2016-12-08 22:40:14','2016-12-08 22:40:14'),(103,4,'JQNR9LPLICbOGNfFB4ZSCTpcq82q2Q7P','2016-12-09 04:41:23','2016-12-09 04:41:23'),(104,4,'NxqzEcljrH4Ma9WY7ootH8YGRT2ymbF4','2016-12-09 07:13:19','2016-12-09 07:13:19'),(105,4,'dFZEGju8xFlPzE11Zl1XCCV8aO6HiZu1','2016-12-09 18:22:08','2016-12-09 18:22:08'),(106,4,'6XfzyvYDmFflK1lBic1hIPSzGfEyPLMt','2016-12-09 23:49:30','2016-12-09 23:49:30'),(107,4,'p9Na0sB3WodCy8Zl8gKzqQNdNc0ErYdf','2016-12-13 01:37:17','2016-12-13 01:37:17'),(108,4,'fFTuOn6KOvaAZFTyb3TNS0UIjvzUFSwE','2016-12-13 01:45:38','2016-12-13 01:45:38'),(109,4,'HSqB2jHVsgi88V59No7VKLfa9XS4Lcl7','2016-12-13 05:13:15','2016-12-13 05:13:15'),(110,4,'FaiNxBFg6KwRVLXpXQZDKEVlxJMfZ65Q','2016-12-14 00:05:30','2016-12-14 00:05:30');
/*!40000 ALTER TABLE `persistences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instruction` varchar(2048) DEFAULT '',
  `note` varchar(2048) DEFAULT '',
  `type` int(20) NOT NULL DEFAULT '0',
  `answer_data` varchar(2048) DEFAULT '',
  `quiz_id` int(11) NOT NULL DEFAULT '0',
  `correct_answer_id` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (85,'Select Image below','zxcvz',2,'3,4,5,6,7',1,NULL,'2017-01-27 02:22:04','2017-02-21 14:33:35',NULL,NULL),(99,'<p>It is <font color=\"#e79439\">______ </font>you to decide</p>','<p>select words</p>',1,'for,to,of',1,'for','2017-01-29 04:23:51','2017-02-22 16:12:57',NULL,NULL),(137,'<p>single choice test <u><font color=\\\"#e79439\\\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</font></u>&nbsp;test?</p>','single choice no',1,'8,9,10',4,'8','2017-02-20 10:21:08','2017-02-20 10:24:03',NULL,NULL),(138,'<p>Select Image Quiz</p>','select image no',2,'3,4,5,6,7',4,NULL,'2017-02-20 10:57:42','2017-02-20 11:52:14',NULL,NULL),(139,'<p><u>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</u>example.</p>','singlenote',1,'19,20,21',4,'19','2017-02-20 11:04:33','2017-02-20 11:04:33',NULL,NULL),(140,'<p align=\"\\\\&quot;center\\\\&quot;\" style=\"text-align: center;\"><b>Please select correct Image.</b><br></p>',NULL,2,'3,4,5,6,7',2,NULL,'2017-02-20 13:11:37','2017-02-22 16:34:55',NULL,NULL),(143,'This is instruction','<p><br></p>',1,'this is choice1,this is choice2,this is choice3',7,'this is choice1','2017-02-28 11:00:56','2017-02-28 11:02:34',NULL,NULL),(144,'<div style=\"text-align: center; \"><b>Please select the correct answer!</b></div><div style=\"text-align: center; \">Washington did not go to school in England because...<br></div>','',1,'as asd sadsa,sad sad sad sa ',7,'as asd sadsa','2017-02-28 11:03:35','2017-02-28 11:23:42',NULL,NULL),(145,'<p align=\"center\"><b>Washington did not go to school in England because...</b><br></p>','',1,'A he studied maths,B he was born in Virginia,C his father died',5,'A he studied maths','2017-02-28 11:27:52','2017-02-28 11:27:52',NULL,NULL),(146,'<p align=\"center\"><b>The 16-year-old Washington ... a land in the Shenandoah Valley.</b><br></p>','',1,'A bought,B buyed,C buys',5,'A bought','2017-02-28 11:31:53','2017-02-28 11:31:53',NULL,NULL),(147,'<p align=\"center\"><b>Marta Custis was a...</b><br></p>','',1,'A rich woman,B a poor woman,C a soldier',5,'A rich woman','2017-02-28 11:33:42','2017-02-28 11:33:42',NULL,NULL),(148,'<p align=\"center\"><b>Washington ... crops, tobacco and wheat.</b><br></p>','',1,'A grow,B grew,C growed',5,'B grew','2017-02-28 11:35:06','2017-02-28 11:35:06',NULL,NULL),(149,'<p align=\"center\"><b>The Revolutionary war ...</b><br></p>','',1,'A start in 1775,B started in 1777,C started in 1775',5,'B started in 1777','2017-02-28 11:37:02','2017-02-28 11:37:02',NULL,NULL),(150,'<p align=\"center\"><b>In 1789...</b><br></p>','',1,'Washington died in the USA.,Washington became the first President of the USA.,Washington became the third President of the USA.',5,'Washington became the first President of the USA.','2017-02-28 11:39:56','2017-02-28 11:39:56',NULL,NULL);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quizzes`
--

DROP TABLE IF EXISTS `quizzes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quizzes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `random_type` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `lesson_id` int(11) DEFAULT '0',
  `module_id` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `shortcode` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quizzes`
--

LOCK TABLES `quizzes` WRITE;
/*!40000 ALTER TABLE `quizzes` DISABLE KEYS */;
INSERT INTO `quizzes` VALUES (1,'quiz12',3,1,1,1,3,'2016-12-23 04:14:59','2017-02-20 10:56:04',80,80,'[quiz-id=1]'),(2,'Lesson 20 - Select image - Vocab',1,6,1,16,7,'2017-02-20 13:02:21','2017-02-22 15:18:52',80,80,'[quiz-id=2]'),(3,'Lesson 20 - Single choice',2,6,1,16,12,'2017-02-20 13:29:32','2017-02-20 13:29:32',80,NULL,'[quiz-id=3]'),(4,'Quiz25',1,1,1,15,8,'2017-02-23 01:11:34','2017-02-23 01:11:34',80,NULL,'[quiz-id=4]'),(5,'Lesson 20 - Single choice - Reading',2,6,1,16,12,'2017-02-27 09:42:08','2017-03-02 02:03:49',80,80,'[quiz-id=5]'),(7,'q-test1',1,1,5,1,3,'2017-02-28 10:59:13','2017-02-28 10:59:13',80,NULL,'[quiz-id=7]');
/*!40000 ALTER TABLE `quizzes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS `reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reminders`
--

LOCK TABLES `reminders` WRITE;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_users`
--

DROP TABLE IF EXISTS `role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_users`
--

LOCK TABLES `role_users` WRITE;
/*!40000 ALTER TABLE `role_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `throttle`
--

DROP TABLE IF EXISTS `throttle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `throttle`
--

LOCK TABLES `throttle` WRITE;
/*!40000 ALTER TABLE `throttle` DISABLE KEYS */;
INSERT INTO `throttle` VALUES (77,NULL,'global',NULL,'2016-12-01 19:08:33','2016-12-01 19:08:33'),(78,NULL,'ip','::1','2016-12-01 19:08:33','2016-12-01 19:08:33'),(79,NULL,'global',NULL,'2016-12-01 20:29:50','2016-12-01 20:29:50'),(80,NULL,'ip','::1','2016-12-01 20:29:50','2016-12-01 20:29:50'),(81,NULL,'global',NULL,'2016-12-09 23:49:05','2016-12-09 23:49:05'),(82,NULL,'ip','::1','2016-12-09 23:49:05','2016-12-09 23:49:05'),(83,21,'user',NULL,'2016-12-09 23:49:05','2016-12-09 23:49:05'),(84,NULL,'global',NULL,'2016-12-20 06:34:03','2016-12-20 06:34:03'),(85,NULL,'ip','::1','2016-12-20 06:34:03','2016-12-20 06:34:03'),(86,4,'user',NULL,'2016-12-20 06:34:03','2016-12-20 06:34:03'),(87,NULL,'global',NULL,'2016-12-20 06:58:32','2016-12-20 06:58:32'),(88,NULL,'ip','::1','2016-12-20 06:58:32','2016-12-20 06:58:32'),(89,4,'user',NULL,'2016-12-20 06:58:32','2016-12-20 06:58:32'),(90,NULL,'global',NULL,'2016-12-20 06:58:36','2016-12-20 06:58:36'),(91,NULL,'ip','::1','2016-12-20 06:58:36','2016-12-20 06:58:36'),(92,4,'user',NULL,'2016-12-20 06:58:36','2016-12-20 06:58:36'),(93,NULL,'global',NULL,'2016-12-20 06:58:40','2016-12-20 06:58:40'),(94,NULL,'ip','::1','2016-12-20 06:58:40','2016-12-20 06:58:40'),(95,4,'user',NULL,'2016-12-20 06:58:40','2016-12-20 06:58:40');
/*!40000 ALTER TABLE `throttle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `token` varchar(50) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1=Activate, 2=Reste Pswd',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=live, 0=expired',
  `created_at` int(10) unsigned NOT NULL,
  `updated_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`token`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
INSERT INTO `tokens` VALUES ('38b8ecbeeca23fa87b7591a7205ae81e',1,1,0,1416419461,1416419643);
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_activations`
--

DROP TABLE IF EXISTS `user_activations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_activations` (
  `id_user` int(30) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_activations`
--

LOCK TABLES `user_activations` WRITE;
/*!40000 ALTER TABLE `user_activations` DISABLE KEYS */;
INSERT INTO `user_activations` VALUES (81,'DnPGlSnKG5sHivjhSUaIs0tCk0RHqk'),(82,'iXgB650ewQbRvXDIWD2Vman48cD0LQ');
/*!40000 ALTER TABLE `user_activations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_groups`
--

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` VALUES (1,'user'),(2,'student'),(3,'VIP'),(4,'Supervisor'),(5,'Administrator'),(6,'Super administrator');
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userdatas`
--

DROP TABLE IF EXISTS `userdatas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userdatas` (
  `id` int(10) DEFAULT NULL,
  `done_exercises` varchar(255) DEFAULT NULL,
  `current_exercise` int(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='user have some info.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userdatas`
--

LOCK TABLES `userdatas` WRITE;
/*!40000 ALTER TABLE `userdatas` DISABLE KEYS */;
INSERT INTO `userdatas` VALUES (80,'14,15,50,51,48,52,54,21,42,33,17,53,55,34,35,36,39,40,56,57',27,'2017-03-02 09:43:56','2017-02-20 09:48:26'),(82,'15,56',53,'2017-03-01 09:29:41','2017-03-01 09:29:28'),(84,',53,56',54,'2017-03-02 06:32:15','2017-03-02 02:38:05');
/*!40000 ALTER TABLE `userdatas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permission` int(11) DEFAULT NULL,
  `is_activated` int(5) DEFAULT '0',
  `course` int(11) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (80,'skyClean906@gmail.com','$2y$10$GZx3Z4qel34lwd0DScf2k.gBR5ad1yhJO/BhQMjYELNpAyaNFQ4Y.','skyClean906@gmail.com',6,1,1,NULL,'2017-02-03 06:51:14','2017-02-27 14:16:41','GRtmsqRE0hT7aEpN1bvqxIfHJf2pvkOlO1aP73ew2sZNL6Rk7reVxCJJC5dO'),(82,'scifiw@gmail.com','$2y$10$cZc2lfARvRDB1CR39xG6heRtscxXFTQEjjwb2z.zEianGIilHZt.K','scifiw@gmail.com',2,0,6,NULL,'2017-02-27 09:50:09','2017-02-27 09:57:18','NMD8dKmi23Tifw3aCV9BHT8szeZnRQsXdt9PLVAG4wJ0TOi5q5AWfliPoGQW'),(83,'dtommy1979@gmail.com','$2y$10$5JcXyFHdYsdtVgoD6P5D7OAe77QbAvWfC32xc9A4WIEimtxUHDHsa','dtommy1979@gmail.com',2,0,6,NULL,'2017-02-28 11:49:59','2017-02-28 12:05:37',NULL),(84,'fox0905@outlook.com','$2y$10$E2lQYeT32cvoUcJ2ijt7QO4A66ovCY9nYluJ1YjA94rJlBCz1FdQG','fox0905@outlook.com',1,0,NULL,NULL,'2017-03-02 02:06:32','2017-03-02 02:06:32',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `words`
--

DROP TABLE IF EXISTS `words`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_word` varchar(256) DEFAULT NULL,
  `translation` varchar(256) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `copyright_url` varchar(256) DEFAULT NULL,
  `audio` varchar(256) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `note` varchar(256) DEFAULT NULL,
  `example` varchar(256) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `words`
--

LOCK TABLES `words` WRITE;
/*!40000 ALTER TABLE `words` DISABLE KEYS */;
INSERT INTO `words` VALUES (3,'pipenapple','ananász','/upload/image/pinenapple.png','http://imprevo.hu','/upload/audio/1487669491_REMINDER.WAV',15,'','this is example','2017-02-25 08:57:24','2016-12-10 06:01:07'),(4,'Apple','alma','/upload/image/apple.png','','/upload/audio/1487595890_211.mp3',15,'This is Apple note','This is tiger','2017-02-20 13:04:50','2016-12-20 17:05:51'),(5,'Orange','narancs','/upload/image/orange.png','','/upload/audio/1487596006_3254.mp3',15,'This is Orange note','1234','2017-02-20 13:06:46','2017-01-20 19:24:30'),(6,'banana','banán','/upload/image/banana.png','','/upload/audio/1487596038_378.mp3',15,'This is banana note','','2017-02-20 13:07:18',NULL),(7,'peach','őszibarack','/upload/image/peach.png',NULL,'/upload/audio/1482482113_5.wav',15,'This is peach note',NULL,NULL,NULL);
/*!40000 ALTER TABLE `words` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-13 23:35:07
