{!! trans('reminder_mail.dear') !!}<br><br>
{!! trans('reminder_mail.span1') !!} {site_title} {!! trans('reminder_mail.span14') !!}<br><br>
{!! trans('reminder_mail.span2') !!} {{$order_number}}<br><br>
{!! trans('reminder_mail.span3') !!} {{$order_payment_method}}<br><br>

{!! trans('reminder_mail.span4') !!}<br><br>
{!! trans('reminder_mail.span5') !!} +36 20 541 3799<br><br>
{!! trans('reminder_mail.span6') !!} hello@imprevo.com<br><br>

{!! trans('reminder_mail.span7') !!} <br><br>
{!! trans('reminder_mail.span8') !!} Imprevo Kft.<br><br>
{!! trans('reminder_mail.span9') !!} K&H Bank Zrt.<br><br>
{!! trans('reminder_mail.span10') !!} 10402142-50526771-77801015<br><br>
{!! trans('reminder_mail.span11') !!} <br><br>
{!! trans('reminder_mail.span12') !!}<br><br>
{!! trans('reminder_mail.span13') !!}<br><br>

