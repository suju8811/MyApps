<!DOCTYPE html>
<html lang="en" style="position: relative;min-height: 100%;">
<head>
  <!--GaCode-->
@if (isset($settings))
  <?php echo html_entity_decode($settings['gaCode'])?>
@endif
<!--End GaCode-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	@if (isset($settings))
    <meta name="description" content="{{$settings['siteDesc']}}">
    <meta name="keywords" content="{{$settings['keywords']}}">
    <title>{{$settings['siteTitle']}}</title>
	@else
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Imprevo</title>
	@endif

    <link href='https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en' rel='stylesheet' type='text/css'>
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		<!-- Vendor CSS -->
	<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap-custom.css" />

	<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
	<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
	<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

	<!-- Theme CSS -->
	<link rel="stylesheet" href="assets/stylesheets/theme.css" />

	<!-- Skin CSS -->
	<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<!-- Head Libs -->
	<script src="assets/vendor/modernizr/modernizr.js"></script>


	<script src="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.min.js"></script>
	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.indigo-pink.min.css">
	<!-- Material Design icon font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />

	<link rel="stylesheet" href="{{ Config::get('RELATIVE_URL') }}/assets/vendor/pnotify/pnotify.custom.css" />

	<script src="/assets/vendor/pnotify/pnotify.custom.js"></script>

	<!--getmdl-select-->
	<link rel="stylesheet" href="https://cdn.rawgit.com/CreativeIT/getmdl-select/master/getmdl-select.min.css">
	<script defer src="https://cdn.rawgit.com/CreativeIT/getmdl-select/master/getmdl-select.min.js"></script>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <!--Facebook Pixel Code-->


    <!--End Facebook Pixel Code-->
</head>

<style>
html, body {
	font-family: 'Roboto', sans-serif;
}

.dropdown-menu-nav{
	margin-top:0px;
	padding:0px 20px 15px 20px;
	background:#399aed
}

.dropdown-menu-nav > a{
	padding:10px 0px 10px 0px ;
	color:#ffffff;
	border-bottom:1px solid #338ad5;
	text-decoration:none;
}

.dropdown-menu-nav > a:hover{
	background:#399aed;
	color:#117a97;
}

.dropdown-menu-nav:before {
	position: absolute;
	top: -9px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #000000;
	border-left: 9px solid transparent;
	content: '';
	z-index:999999;
}

nav > a:hover {
	text-decoration:none;
}
	.is-focused .mdl-textfield__input {
		border:none;
	}

	#send-button{
		background-color:#f29f00;
		border-color:#f29f00;

	}

	#send-button:hover{
		background-color:#f29f00;
		border-color:#f29f00;
	}

	#save-button{
		background-color:#f29f00;
		border-color:#f29f00;
		color:#ffffff;
	}

	#sub-button {
		margin-left:30px;
		background-color:#ffffff;
		color:#399aed;
		border:none;
		outline:none;
	}

	#sub-button:hover {
		margin-left:30px;
		background-color:#dfe8ef;
		color:#399aed;
	}
	.socialbuttons .facebook{
		width:40px;
		height:40px;
		background:url('/images/social-facebook-icon.png') center/cover;
	}
	.socialbuttons .facebook:hover{
		background:url('/images/social-facebook-icon-hover.png') center/cover;
	}
	.socialbuttons .twitter{
		width:40px;
		height:40px;
		background:url('/images/social-twitter-icon.png') center/cover;
	}
	.socialbuttons .twitter:hover{
		background:url('/images/social-twitter-icon-hover.png') center/cover;
	}
	.socialbuttons .goggle{
		width:40px;
		height:40px;
		background:url('/images/social-goggle-icon.png') center/cover;
	}
	.socialbuttons .goggle:hover{
		background:url('/images/social-goggle-icon-hover.png') center/cover;
	}
	.socialbuttons .insta{
		width:40px;
		height:40px;
		background:url('/images/social-youtube-icon.png') center/cover;
	}
	.socialbuttons .insta:hover{
		background:url('/images/social-youtube-icon-hover.png') center/cover;
	}
	.link1{
		color:#66abae;
	}
	.link1:hover{
		color:#ffffff;
		text-decoration:none;
	}
	.recent-links{
		color:#000000;
		text-decoration:none;
	}

	.recent-links:hover {
		text-decoration:none;
	}
	.category-links{
		color:#000000;
		text-decoration:none;
	}
	.category-links:hover{
		text-decoration:none;
	}
	#create_name:hover{
		color:#399aed;
	}

<style>

  .fb-page, .fb-page:before, .fb-page:after {
    border: 1px solid #ccc;
  }

  .fb-page:before, .fb-page:after {
    content: "";
    position: absolute;
    bottom: -3px;
    left: 2px;
    right: 2px;
    height: 1px;
    border-top: none
  }

  .fb-page:after {
    left: 4px;
    right: 4px;
    bottom: -5px;
    box-shadow: 0 0 2px #ccc
  }
</style>
<style>
html, body {
	font-family: 'Roboto', sans-serif;
}


#my-account-button{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #f29f00;
	width:150px;
	height:40px;
}

#my-account-button:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #d98f00;
}

#my-account-button-logged{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #399aed;
	width:150px;
	height:40px;
}

#my-account-button-logged:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #2081d5;
}

.footer-link{
	padding:10px;
	font-weight:700;
	color: #6c937b;
}

#footer-1div{
	text-align:right;
}

#footer-2div{
	text-align:center;
}

#footer-3div{
	text-align:left;
}

#header-div{
	background: #006e73;
	padding:20px 50px 20px 50px;
}

#div-footer {
	height:140px;
}
#try-it-button-second{
	color:#399aed;
	background-color:#ffffff;
	width:200px;
}

#try-it-button-second:hover{
	color:#399aed;
	background-color:#dfe8ef;
	font-weight:bold;
}
#footer-div-bottom {
	width: 60% !important;
}

@media (max-width: 1777px)
{
	#footer-div-bottom {
		width: 70% !important;
	}
}

@media (max-width: 1705px) {
	.content{
		width:80%;
	}
}

@media (max-width: 1530px) {
  #footer-links-div	{
		width: 70% !important;
  }
  #footer-div-bottom {
  	width: 80% !important;
  }
}
@media (max-width: 1350px)
{
	#footer-div-bottom {
		width: 87% !important;
	}
}
@media (max-width: 1250px) {
  #footer-links-div	{
		width: 90% !important;
  }
  .content{
  	width:100%;
	margin-left:35px !important;
	margin-right:35px !important;
  }
	#profile-nav-div{
		width:30%;
	}
	#main_page_div{
		width:70%;
	}
  #footer-div-bottom {
  	width: 100% !important;
  }
}
@media (max-width: 984px) {
	#profile-nav-div{
		width:40%;
	}
	#main_page_div{
		width:60%;
	}
}
@media (max-width: 950px) {
  #footer-links-div	{
		width: 100% !important;
  }
}
@media (max-width: 830px) {
	#main_page_div{
		width:100%;
	}
}
@media (max-width: 1500px) {

}
@media (max-width: 1405px) {
	.content{
		width:90%;
	}
}



@media (max-width: 769px) {

	#footer-1div{
		text-align:center;
	}

	#footer-2div{
		text-align:center;
	}

	#footer-3div{
		text-align:center;
	}

	#dropdown2:before {
		display: none;
	}

	#dropdown1:before {
		display: none;
	}
	#my-account-button{
		width:100px;
		height:40px;
		font-size:10px;
	}
	#my-account-button-logged{
		font-size: 10px;
		width:100px;
		height:40px;
	}
	.footer-link{
		font-size:12px;
	}

	#div-footer {
		height:230px;
	}
	.phone-footer-accordion {
	width: 380px;
	border-radius: 5px;
	overflow: hidden;
	margin: auto;
	}

	.phone-footer-accordion .item .heading {
		height: 50px;
		line-height: 50px;
		font-size: 15px;
		cursor: pointer;
		color: #ffffff;
		padding-left: 15px;
		background: #006469 url('/images/phone_footer_accordion.png') no-repeat;
		background-position: right 20px top -95px;
		border-bottom: 1px solid #4d9a9d;
		box-sizing: border-box;
		text-align:left;
		}

		.phone-footer-accordion .item.open .heading,
		.phone-footer-accordion .item:last-child .heading { border: 0; }

		.phone-footer-accordion .item.open .heading { background-position: right 20px top -5px; }

		.phone-footer-accordion .item .content {
		display: none;
		padding: 15px;
		background: #006469;
		font-size: 14px;
	}
	.content{
		margin-left:5px !important;
		margin-right:5px !important;
	}
	#main_page_div{
		padding:5px !important;
		width:100%;
	}
}

</style>
<body style="padding:0px; width:100%; height:100%;margin: 0;overflow-x: hidden">
  @if (isset($settings))
    <?php echo html_entity_decode($settings['gaBodyCode'])?>
  @endif
<!--<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" style="width:100%;min-height:100%">-->
<div id='Parent-Div-Background' style="position:relative; margin:0; width:100%;min-height:100%;">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	  @include('layouts.header')

	<main class="mdl-layout__content" style="background-color:#ebf3ef;width:100%;margin:0px;padding:0px;overflow:hidden">
	<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;margin:0px;width:100%;background: #9fc3ae;padding:0px 0px 0px 0px;font-size: 16px;height:250px">
		<div class="content mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0px;margin:0px">
			<span><a href="/" style="padding-left:25px;color:#d5e7dc;line-height: 40px;text-decoration: none">{!! trans('home.imprevo') !!}</a></span>
			<span style="padding-left:10px;color:#d5e7dc;line-height: 40px">/</span>
			<span style="padding-left:10px;color:#d5e7dc;line-height: 40px">{!! trans('profile.profile') !!}</span>
		</div>
	</div>
	<form id="formDetail" name="formDetail"  role="form" action="/profile" method="post" encType="multipart/form-data" style="">
	{{ csrf_field() }}
    <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;padding:0px;margin-top:-210px">
        <div class="content mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="justify-content:center;padding:0px;">
          <div id="main_page_div" class="mdl-cell mdl-cell mdl-cell--9-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="background-color:#ffffff;padding:20px;margin:0;">
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:10px">
					<span style="font-weight:bold; font-size:25px">{!! trans('profile.profilesettings') !!}</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-bottom:20px">
					<hr style="width:100%;height:1px; background-color:#cccccc">
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="background-color:#9fc3ae;padding:5px 15px 5px 15px">
					<span style="font-size:18px; color:#ffffff">{!! trans('profile.basicinfo') !!}</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
						<input class="mdl-textfield__input" type="name" id="name" name="name"  value="{{$user->name}}" autofocus>
						<label class="mdl-textfield__label" for="name" style="font-weight:bold">{!! trans('profile.yourname') !!}</label>
					</div>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
						<input class="mdl-textfield__input" type="email" id="email" name="email"  value="{{$user->email}}" disabled>
						<label class="mdl-textfield__label" for="email" style="font-weight:bold">{!! trans('profile.youremailaddress') !!}</label>
					</div>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="background-color:#9fc3ae;padding:5px 15px 5px 15px">
					<span style="font-size:18px; color:#ffffff">{!! trans('profile.passwordsettings') !!}</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
						<input class="mdl-textfield__input" type="password" id="current_password" name="current_password"  value="" autofocus>
						<label class="mdl-textfield__label" for="current_password" style="font-weight:bold">{!! trans('profile.yourcurrentpassword') !!}</label>
					</div>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
						<input class="mdl-textfield__input" type="password" id="password" name="password"  value="{{ old('password') }}" autofocus>
						<label class="mdl-textfield__label" for="password" style="font-weight:bold">{!! trans('profile.yournewpassword') !!}</label>
					</div>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
						<input class="mdl-textfield__input" type="password" id="password_confirm" name="password_confirm"  value="{{ old('password_confirm') }}" autofocus>
						<label class="mdl-textfield__label" for="password_confirm" style="font-weight:bold">{!! trans('profile.confirmnewpassword') !!}</label>
					</div>
				</div>
				@if ($message = Session::get('error'))
					<div class="" style="text-align:center;color:#ff0000">
						<p>{{ $message }}</p>
					</div>
				@endif
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="background-color:#9fc3ae;padding:5px 15px 5px 15px">
					<span style="font-size:18px; color:#ffffff">{!! trans('profile.billingaddress') !!}</span>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding-left:0px">
					<div class="mdl-cell mdl-cell mdl-cell--6-col" style="margin-left:0px;">

            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:10px;">
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth getmdl-select__fix-height" style="width:100%">
                <input class="mdl-textfield__input" type="text" id="country" readonly>
                <input type="hidden" name="country" value=" @if ($user) @if ($user->country) {{$user->country}} @endif @endif"/>
                <i class="mdl-icon-toggle__label material-icons" >keyboard_arrow_down</i>
                <label for="country" class="mdl-textfield__label">{!! trans('profile.country') !!}</label>
                <ul for="country" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                  @foreach ($countrylist as $country)
                    <li class="mdl-menu__item" data-val="{{$country}}" @if ($user) @if ($user->country) @if ($user->country == $country) data-selected="true" @endif @else @if ($country == "Hungary") data-selected="true" @endif @endif @endif
                     >{{$country}}</li>
                  @endforeach
                </ul>
              </div>
            </div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
								<input class="mdl-textfield__input" type="text" id="city" name="city"  value="{{$user->city}}" autofocus>
								<label class="mdl-textfield__label" for="city" style="font-weight:bold">{!! trans('profile.towncity') !!}</label>
							</div>
						</div>
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--6-col" style="margin-left:0px;">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
								<input class="mdl-textfield__input" type="text" id="postcode" name="postcode"  value="{{$user->zipcode}}" autofocus>
								<label class="mdl-textfield__label" for="postcode" style="font-weight:bold">{!! trans('profile.postcode') !!}</label>
							</div>
						</div>


            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:12px;">
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth getmdl-select__fix-height" style="width:100%">
                <input class="mdl-textfield__input" type="text" id="county" readonly>
                <input type="hidden" name="county" value="@if ($user) @if ($user->county) {{$user->county}} @endif @endif"/>
                <i class="mdl-icon-toggle__label material-icons" >keyboard_arrow_down</i>
                <label for="county" class="mdl-textfield__label">{!! trans('profile.county') !!}</label>
                <ul for="county" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                  @foreach ($states as $state)
                    <li class="mdl-menu__item" data-val="{{$state}}" @if ($user && $user->county == $state) data-selected="true"  @endif
                     >{{$state}}</li>
                  @endforeach
                </ul>
              </div>
            </div>

					</div>
					<div class="mdl-cell mdl-cell mdl-cell--12-col" style="margin-left:0px">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
							<input class="mdl-textfield__input" type="text" id="street_address" name="street_address"  value="{{$user->street_address}}" autofocus>
							<label class="mdl-textfield__label" for="street_address" style="font-weight:bold">{!! trans('profile.streetaddress') !!}</label>
						</div>
					</div>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="background-color:#9fc3ae;padding:5px 15px 5px 15px">
					<span style="font-size:18px; color:#ffffff">{!! trans('profile.companydetail') !!}</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
						<input class="mdl-textfield__input" type="text" id="company" name="company"  value="{{$user->company}}" autofocus>
						<label class="mdl-textfield__label" for="company" style="font-weight:bold">{!! trans('profile.companyname') !!}</label>
					</div>
				</div>
				<div id="tax-div" class="mdl-cell mdl-cell mdl-cell--12-col" style="">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
						<input class="mdl-textfield__input" type="text" id="tax_num" name="tax_num"  value="{{$user->tax_num}}" autofocus>
						<label class="mdl-textfield__label" for="tax_num" style="font-weight:bold">{!! trans('profile.taxid') !!}</label>
					</div>
				</div>

				<div id="vat-div" class="mdl-cell mdl-cell mdl-cell--12-col" style="display:none;">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
						<input class="mdl-textfield__input" type="text" id="eu_vat_num" name="eu_vat_num"  value="{{$user->eu_vat_num}}" autofocus>
						<label class="mdl-textfield__label" for="eu_vat_num" style="font-weight:bold">{!! trans('profile.euvat') !!}</label>
					</div>
				</div>

				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
					<button id="save-button" class="mdl-button mdl-js-button mdl-button--colored" type="submit" style="width:100px">
						{!! trans('profile.save') !!}
					</button>
				</div>
          </div>
          <div id="profile-nav-div" class="mdl-cell mdl-cell mdl-cell--3-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="background-color:#ebf3ef;padding:0;margin:0">
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0px;background:url('/images/newsletter-background.png') no-repeat center/cover;padding:0px 0px 0px 0px;height:202px;width:100%">
				<div class="mdl-cell mdl-cell--12-col" style="padding-top:15px;padding-left:35px;">
					<span style="font-size:30px; color:#ffffff">{!! trans('profile.sidetoptitle') !!}</span>
				</div>
				<div class="mdl-cell mdl-cell--12-col" style="padding-left:35px;">
					<span style="font-size:16px; color:#ffffff">{!! trans('profile.sidespan1') !!}</span>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
					<div class="mdl-cell mdl-cell--10-col" style="margin:0; padding:0;">
						<form action="https://sendy.imprevo.info/subscribe" method="POST" accept-charset="utf-8" style="width:100%;margin:0;padding:0">
							<div class="input-group" style="margin:0; padding:0;">
								<input type="text" id="email" name="email" class="form-control" placeholder="Email cím">
								<input type="hidden" name="list" value="dWVr892XDx8pprHN8763yTtK6w"/>
								<span class="input-group-btn">
									<button id="send-button" class="btn btn-primary" type="submit" style="">
										<i aria-hidden="true" class="fa fa-arrow-right"></i>
									</button>
								</span>
							</div>
						</form>
					</div>
				</div>
			</div>

            @if ($recent_blogs)
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0px 10px 10px">
              <span style="color:#3b4a51;font-weight:bold; font-size:14px">{!! trans('profile.sidespan2') !!}</span>
            </div>
                @foreach($recent_blogs as $rblog)
                <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0px; border-bottom:1px solid #eee;padding-left:30px; padding-bottom:10px">
                  <a class="recent-links" href="/blog/{{$rblog}}" style=""><span style="padding-right:10px">></span>{{$rblog->title}}</a>
                </div>
                @endforeach
            @endif
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0 10px 10px">
              <span style="color:#3b4a51;font-weight:bold; font-size:14px">{!! trans('profile.sidespan3') !!}</span>
            </div>
            @foreach(\App\Blogcat::all() as $blogcat)
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="border-bottom:1px solid #eee;padding-left:30px; padding-bottom:10px">
              <a class="category-links" href="/blog/category/{{$blogcat->title}}" style=""><span style="padding-right:10px">></span>{{$blogcat->title}} ({{count($blogcat->blogs)}})</a>
            </div>
            @endforeach
			<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0 0px 10px">
				<span style="color:#3b4a51;font-weight:bold; font-size:14px">{!! trans('profile.sidespan4') !!}</span>
			</div>
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="background:url('/images/banner.png') no-repeat center/cover; height:230px; ">
            </div>
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0 20px 10px">
         <!--     <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="standard" data-action="like" data-show-faces="true" data-share="false"></div>-->
				<div class="fb-page" data-href="https://www.facebook.com/imprevo" data-small-header="false" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
				</div>
            </div>

          </div>
        </div>
    </div>
	</form>
</main>
		<div id="section4" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="margin:0;width:100%;background-color:#399aed;padding:40px 0px 40px 0px;justify-content:center">
			<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--4-col-phone" style="text-align:center; ">
				<span style="color:#ffffff; font-size:30px;">Próbáld ki az IMPREVO-t még ma ingyen
				</span>
			</div>
			<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--4-col-phone" style="text-align:center; ">
				<button id="try-it-button-second" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
					PRÓBÁLD KI INGYEN!
				</button>
			</div>
		</div>
    @include('layouts.cookie')
		@include('layouts.footer')
</div>

<script src="/assets/vendor/jquery/jquery.js"></script>
<script src="/assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/vendor/jquery-validation/jquery.validate.min.js"></script>

<script src="/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="/assets/vendor/bootstrap/js/bootstrap.js"></script>
<script src="/assets/vendor/nanoscroller/nanoscroller.js"></script>
<script src="/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="/assets/vendor/jquery-placeholder/jquery-placeholder.js"></script>

<!-- Specific Page Vendor -->

<script src="/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="/assets/javascripts/theme.js"></script>

<!-- Theme Custom -->
<script src="/assets/javascripts/theme.custom.js"></script>

<!-- Theme Initialization Files -->
<script src="/assets/javascripts/theme.init.js"></script>

<script>

    function goLogin() {
        location.href = '/login'
    }

    function goProfile(e) {
		location.href = '/profile';
    }

	function isMobileDevice() {
		return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
	};

	console.log(isMobileDevice());
	if (!isMobileDevice()){
		$('ul.nav li.dropdown').hover(function() {
			$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
		}, function() {
			$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
		});
	}
	$('.phone-footer-accordion .item .heading').click(function() {

		var a = $(this).closest('.item');
		var b = $(a).hasClass('open');
		var c = $(a).closest('.accordion').find('.open');

		if(b != true) {
			$(c).find('.content').slideUp(200);
			$(c).removeClass('open');
		}

		$(a).toggleClass('open');
		$(a).find('.content').slideToggle(200);

	});


function OnChangeCountry()
{
	country = document.getElementById('country').value;

	var route_url = '/profile/getstates?countryname=' + country;
	console.log(route_url);
	$.ajax({
		url: route_url,
		type: 'get',
		data: '',
		async: true,
		success: function (ret) {
			console.log(ret[0]);
			console.log(ret[1]);
			//console.log(states);
			if (ret[0])
			{
				html = '<option value=""></option>';
				county_tag = document.getElementById('county');
				for (index in ret)
				{
					html = html + '<option value="'+ ret[index] + '">' + ret[index] + '</option>';
				}
				county_tag.innerHTML = html;
			}

			if (ret[1] == "Europe" && country != "Hungary")
			{
				console.log('vat');
				document.getElementById('tax-div').style.display='none';
				document.getElementById('vat-div').style.display='inline';
			}
			else
			{
				document.getElementById('tax-div').style.display='inline';
				document.getElementById('vat-div').style.display='none';
			}
		},
		cache: false,
		contentType: false,
		processData: false
	});
}

function save(){

}



</script>

</body>
</html>
