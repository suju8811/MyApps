<!DOCTYPE html>
<html lang="en" style="position: relative;min-height: 100%;">
<head>
  @if (isset($settings))
    <?php echo html_entity_decode($settings['gaCode'])?>
  @endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	@if (isset($settings))
    <meta name="description" content="{{$settings['siteDesc']}}">
    <meta name="keywords" content="{{$settings['keywords']}}">
    <title>{{$settings['siteTitle']}}</title>
	@else
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title>{{$course->title}}</title>

	@endif


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="/css/material.min.css" />
    <link rel="stylesheet" href="/css/styles.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="{{ Config::get('RELATIVE_URL') }}/assets/vendor/pnotify/pnotify.custom.css" />
    <link rel="stylesheet" type="text/css" href="https://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.css" />
	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- Specific Page Vendor CSS -->
	<link rel="stylesheet" href="/assets/vendor/owl.carousel/assets/owl.carousel.css" />
	<link rel="stylesheet" href="/assets/vendor/owl.carousel/assets/owl.theme.default.css" />
	<!-- Theme CSS -->
	<link rel="stylesheet" href="/assets/stylesheets/theme-fix.css" />

	<!-- Skin CSS -->
	<link rel="stylesheet" href="/assets/stylesheets/skins/default.css" />

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="/assets/stylesheets/theme-custom.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <script src="/assets/vendor/modernizr/modernizr.js"></script>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.js"></script>
    <script src="/assets/vendor/pnotify/pnotify.custom.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="/assets/vendor/nanoscroller/nanoscroller.js"></script>
	<script src="/assets/vendor/owl.carousel/owl.carousel.js"></script>

	<script src="/assets/javascripts/theme.js"></script>

	<!-- Theme Custom -->
	<script src="/assets/javascripts/theme.custom.js"></script>

	<!-- Theme Initialization Files -->
	<script src="/assets/javascripts/theme.init.js"></script>


    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>
<body style="padding:0px; height:100%;margin: 0;overflow-x: hidden">
  @if (isset($settings))
    <?php echo html_entity_decode($settings['gaBodyCode'])?>
  @endif
<div id='Parent-Div-Background'style="padding:0; margin:0px;position:relative;width:100%;min-height:100%;">
<style>
html, body {
	font-family: 'Roboto', sans-serif;
}

.navbar-default {
	width:100%;
	background:rgba(255,255,255,0);
	margin:0;
	border:none;

}

.navbar-default .navbar-nav{
	padding-right:15px;
}

.navbar-default .navbar-nav > li{

	padding-right:25px;
}

.navbar-default .navbar-nav .dropdown{

}

.navbar-default .navbar-nav > li > a{
	color:#ffffff;
	font-size: 14px;
	text-transform: uppercase;
}

.navbar-default .navbar-nav > li > a:hover{
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > li > a:focus{
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a{
	background:rgba(255,255,255,0);
	color:#ffffff;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a:hover{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a:focus{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu{
	margin-top:10px;
	padding:15px 20px 15px 20px;
	background:#399aed
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li{
	padding:0;
	border-bottom:1px solid #338ad5;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a{
	padding:10px 0px 10px 0px ;
	color:#ffffff;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a:hover{
	background:#399aed;
	color:#117a97;
}

#dropdown3 {
	background:#399aed;
	color:#ffffff;
}

#dropdown3:before {
	position: absolute;
	top: -9px;
	left:50px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

#dropdown2:before {
	position: absolute;
	top: -9px;
	left:120px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

#dropdown1:before {
	position: absolute;
	top: -9px;
	left:60px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

.navbar-default .navbar-toggle {
		border:none;
		padding-right:15px;
}

.navbar-default .first-nav {

}

@media (max-width: 767px) {
    .navbar-default .navbar-collapse {
        background:#ffffff;
		float:right;
		width:50%;
    }

	.navbar-default .navbar-nav > li > a{
		color:#6c937b;
	}
	.navbar-default .navbar-nav > li {
		border-bottom: 1px solid #eeeeee;
	}
}

#header-div
{
	width:100%;
	/*justify-content:center;*/
	padding:10px 0 10px 0;
	margin:0;
	/*height:100px;*/


}

/*#header-div:hover
{
	width:100%;
	padding:5px 0 5px 0;
	margin:0;

	background-color:#006e73;
	-webkit-box-shadow: 0px 2px 5px 2px #004a4d;
   -moz-box-shadow: 0px 2px 5px 2px #004a4d;
        box-shadow: 0px 2px 5px 2px #004a4d;
}*/

.fixed {
	-webkit-transform: translate3d(0,0,0);
	position:fixed;
	top:0px;
	background-color:#006e73;
	-webkit-box-shadow: 0px 2px 5px 1px #004a4d;
   -moz-box-shadow: 0px 2px 5px 1px #004a4d;
        box-shadow: 0px 2px 5px 1px #004a4d;
	z-index:999;
	padding:0px 0 0px 0;
}

#my-account-button{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #f29f00;
	width:150px;
	height:40px;
}

#my-account-button:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #d98f00;
}

#my-account-button-logged{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #399aed;
	width:150px;
	height:40px;
}

#my-account-button-logged:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #2081d5;
}

#purchase-button{
	 background-color:#399aed;
	 font-size: 18px;
}

#purchase-button:hover{
	 background-color:#2081d5;
	 font-size: 18px;
}



.section3-link2
{
	color:#000000;
	span {
		color:#39929e;
	}
}

.section3-link2 a
{
	color:#399aed;
}

.section3-link2 a:hover
{
	color:#399aed;
	text-decoration:underline;
}

#try-it-button-second{
	color:#399aed;
	background-color:#ffffff;
	margin-left:30px;
}

#try-it-button-second:hover{
	color:#399aed;
	background-color:#dfe8ef;
	font-weight:bold;
	margin-left:30px;
}



.a-next-prev {
	padding-bottom:150px;
	float:right;
	color:black;
	font-size:40px;
	cursor:pointer;
}

.a-next-prev:hover {
	padding-bottom:150px;
	float:right;
	color:#399aed;
	font-size:40px;
	cursor:pointer;
}

.socialbuttons .facebook{
	width:40px;
	height:40px;
	background:url('/images/social-facebook-icon.png') center/cover;
}
.socialbuttons .facebook:hover{
	background:url('/images/social-facebook-icon-hover.png') center/cover;
}
.socialbuttons .twitter{
	width:40px;
	height:40px;
	background:url('/images/social-twitter-icon.png') center/cover;
}
.socialbuttons .twitter:hover{
	background:url('/images/social-twitter-icon-hover.png') center/cover;
}
.socialbuttons .goggle{
	width:40px;
	height:40px;
	background:url('/images/social-goggle-icon.png') center/cover;
}
.socialbuttons .goggle:hover{
	background:url('/images/social-goggle-icon-hover.png') center/cover;
}
.socialbuttons .insta{
	width:40px;
	height:40px;
	background:url('/images/social-youtube-icon.png') center/cover;
}
.socialbuttons .insta:hover{
	background:url('/images/social-youtube-icon-hover.png') center/cover;
}

.link1{
	color:#66abae;
}
.link1:hover{
	color:#ffffff;
	text-decoration:none;
}

#section2-scrolldown{
	z-index:99;
	position:relative;
	background:url('/images/what_is_imprevo_section1-scrolldown.png') no-repeat;
	height:50px;
	width:50px
    left:-webkit-calc(50% - 25px);
    left:-moz-calc(50% - 25px);
	left:calc(50% - 25px);
}

#section2-scrolldown:hover{
	z-index:99;
	position:relative;
	background:url('/images/what_is_imprevo_section1-scrolldown-hover.png') no-repeat;
	height:50px;
	width:50px
    left:-webkit-calc(50% - 25px);
    left:-moz-calc(50% - 25px);
	left:calc(50% - 25px);
}

.section2-line {
	z-index:0;
	position:relative;
	left:40%;
}

.footer-link{
	padding:10px;
	font-weight:700;
	color: #6c937b;
}

#footer-1div{
	text-align:right;
}

#footer-2div{
	text-align:center;
}

#footer-3div{
	text-align:left;
}
#div-footer {
	height:140px;
}
#footer-div-bottom {
	width: 60% !important;
}
#section1{
	width:1104px;
}
@media (max-width: 1850px) {
	.footer {
		width:80%;
	}
}
@media (max-width: 1345px) {
	.footer {
		width:90%;
	}
}

@media (max-width: 1185px) {
	.footer {
		width:100%;
	}
}

@media (max-width: 1530px) {


  #section4-div0 {
	  width:85%;
  }
}

@media (max-width: 1470px) {
	#nav-bar{
		width: 70% !important;
	}

	#section4-div0 {
		width:95%;
	}
}






@media (max-width: 1380px){
	#nav-bar{
		width: 80% !important;
	}
}



@media (max-width: 1209px){
	#nav-bar{
		width: 90% !important;
	}

	.navbar-default .navbar-nav {
		padding-right:0px;
	}

	.navbar-default .navbar-nav > li {
		padding-right:0px !important;
	}

}
@media (max-width: 1100px){
	#section1{
	  width:100% !important;
	}
	#section1-main-card{
		margin-top:-430px !important;
	}

	#section1{
	  width:95% !important;
	}
}

@media (max-width: 840px)
{
	#header-div{
		padding:0 !important;
		margin:0;
	}

	#nav-bar{
		padding:0;
		margin:0;
	}
	.navbar-header {
        float: none;
    }
    .navbar-left {
        float: none !important;
    }
    .navbar-toggle {
        display: block;
    }
    .navbar-collapse {
        border-top: 1px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
    }
    .navbar-fixed-top {
        top: 0;
        border-width: 0 0 1px;
    }

    .navbar-collapse.collapse {
        display: none!important;
    }

    .navbar-nav {
        float: none!important;
        margin-top: 5px;
		margin-right:0px !important;
    }

	#dropdown1, #dropdown2, #dropdown3{
		padding:0px 10px 0px 10px;

	}

    .navbar-nav>li {
        float: none;
    }
    .navbar-nav>li>a {
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .collapse.in{
        display:block !important;
    }
}

@media (max-width: 769px) {
    .navbar-default .navbar-collapse {
        background:#ffffff;
		float:right;
		width:50%;
		z-index:99999;
    }
	.navbar-default{
		padding-right:0px;
	}
	.navbar-default .navbar-nav > li > a{
		color:#6c937b !important;
	}
	.navbar-default .navbar-nav > li {
		border-bottom: 1px solid #eeeeee;
	}

	.navbar-default .navbar-toggle{
		padding-top:5px;
	}

	.navbar-default .navbar-toggle i{
		font-size:18px;
	}

	.navbar-brand img{
		height:14px;
		width: auto;
	}

	#footer-1div{
		text-align:center;
	}

	#footer-2div{
		text-align:center;
	}

	#footer-3div{
		text-align:center;
	}

	#dropdown3:before {
		display: none !important;
	}

	#dropdown2:before {
		display: none !important;
	}

	#dropdown1:before {
		display: none !important;
	}

	#my-account-button{
		width:100px;
		height:40px;
		font-size:10px;
	}
	#my-account-button-logged{
		font-size: 10px;
		width:100px;
		height:40px;
	}
	.footer-link{
		font-size:12px;
	}

	#div-footer {
		height:230px;
	}

	#footer-1div{
		text-align:center;
	}

	#footer-2div{
		text-align:center;
	}

	#footer-3div{
		text-align:center;
	}

	#footer-span1{
		font-size:20px !important;
	}

	#footer-div1{
		padding-top: 50px !important;
	}
	.phone-footer-accordion {
	width: 380px;
	border-radius: 5px;
	overflow: hidden;
	margin: auto;
	}

	.phone-footer-accordion .item .heading {
		height: 50px;
		line-height: 50px;
		font-size: 15px;
		cursor: pointer;
		color: #ffffff;
		padding-left: 15px;
		background: #006469 url('/images/phone_footer_accordion.png') no-repeat;
		background-position: right 20px top -95px;
		border-bottom: 1px solid #4d9a9d;
		box-sizing: border-box;
		text-align:left;
		}

		.phone-footer-accordion .item.open .heading,
		.phone-footer-accordion .item:last-child .heading { border: 0; }

		.phone-footer-accordion .item.open .heading { background-position: right 20px top -5px; }

		.phone-footer-accordion .item .content {
		display: none;
		padding: 15px;
		background: #006469;
		font-size: 14px;
	}
}

@media (max-width: 769px) {
    .navbar-default .navbar-collapse {
        background:#ffffff;
		float:right;
		width:50%;
    }

	.navbar-default .navbar-nav > li > a{
		color:#6c937b;
	}
	.navbar-default .navbar-nav > li {
		border-bottom: 1px solid #eeeeee;
	}

	#hero-div-title{
		font-size:25px !important;
		line-height: 40px !important;
	}
	#hero-div-span{
		font-size:16px !important;
	}
	#hero-div{
		padding-bottom: 50px !important;
	}
	#section1 {
		margin-top:-46px !important;
	}
	#section1-main-card{
		margin-top:0px !important;
	}
	#section1-div1, #section1-div2{
		padding-left: 0px !important;
		padding:0px 0px 0px 30px !important;
	}
	.section1-div-text{
		padding:0 !important;
	}

	#section2-title{
		padding-top: 20px !important;
	}
	#owl-carousel-div{
		padding-top: 0px !important;
	}

	#try-it-button-second{
		margin-left: 0px;
	}
	.section4-div1{
		padding-left: 0px !important;
	}
	.section4-div1-div1{
		padding-top: 10px !important;
	}
	#section4{
		padding:10px 0px 0px 0px !important;
	}
	span.section3-link2{
		font-size:14px !important;

	}
	#section4-div-latest{
		padding-bottom:30px !important;
	}

	#section4-span1 {
		font-size:25px !important;
		line-height:30px !important;
	}

	#footer-div1{
		padding-top:40px !important;
	}

	#footer-div1-span{
		font-size:20px !important;
	}
}

@media (max-width: 450px) {
	#owl-carousel{
		width:100% !important;
	}

}
</style>


<main class="mdl-layout__content" style="display:inline;width:100%;margin:0px;padding:0px;">
    <div id="full_content_div" style="width:100%;padding:0px;margin:0px">
		<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px;width:100%; background:url('/images/what_is_imprevo_hero_background.png') no-repeat; ">
			<div id="header-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
				<div id="nav-bar" class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--4-col-phone mdl-cell--8-col-tablet">
					<nav class="navbar navbar-default">
						<div class="container-fluid">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
									<i aria-hidden="true" class="fa fa-navicon" style="color:#ffffff"></i>
								</button>
								<a class="navbar-brand" href="/"><img src='/images/logo.png'></img></a>
							</div>
							<div class="collapse navbar-collapse  navbar-right" id="myNavbar">
								<ul class="nav navbar-nav">
									<li><a href="https://imprevo.hu/what-is-imprevo">{!! trans('headerfooter.header-span1') !!}</a></li>
									<li class="dropdown">
										<a href="https://imprevo.hu/shop">{!! trans('headerfooter.header-span2') !!}</a>
										<ul id="dropdown1" class="dropdown-menu">
											<li><a class="a-submenu" href="https://imprevo.hu/sales/course/10">{!! trans('headerfooter.header-span3') !!} </a></li>
											<li><a class="a-submenu" href="https://imprevo.hu/sales/course/11">{!! trans('headerfooter.header-span4') !!} </a></li>
										</ul>
									</li>
									<li><a href="https://imprevo.hu/blog">{!! trans('headerfooter.header-span5') !!}</a></li>
									<li class="dropdown">
										<a id="header-span6" class="dropdown-toggle" data-toggle="dropdown2" href="https://imprevo.hu/ingyenes-tartalmak">Ingyenesek<!--<span class="caret"></span>--></a>
										<ul id="dropdown2" class="dropdown-menu">
											<li><a id="header-span7" class="a-submenu" href="https://imprevo.hu/20-részes-ingyenes-angol-tanfolyam">20 részes email tanfolyam</a></li>
											<li><a id="header-span8" class="a-submenu" href="https://imprevo.hu/tanulj-meg-angolul-6-hónap-alatt-az-imprevo-val">6 hónap alatt angolul</a></li>
											<li><a id="header-span9" class="a-submenu" href="https://imprevo.hu/angol-igeidők-ebook">Angol igeidők</a></li>
											<li><a id="header-span10" class="a-submenu" href="https://imprevo.hu/angol-levelek-írása">Angol levelek írása</a></li>
										</ul>
									</li>
								</ul>
								<ul class="nav navbar-nav navbar-right">
									<li class="dropdown" style="">
									@if (Auth::guest())
										<button id="my-account-button" onclick="goProfile(event)" class="my-nav-button dropdown-toggle mdl-button mdl-js-button mdl-button--raised mdl-button--colored" data-toggle="dropdown" style="border-radius:2px;outline:none;border:none;">
											{!! trans('headerfooter.header-button1') !!}
										</button>
									@else
										<button class="dropdown-toggle" id="my-account-button-logged" onclick="location.href = '/home'" class="my-nav-button dropdown-toggle" data-toggle="dropdown" style="border-radius:2px;outline:none;border:none;">
											{!! trans('headerfooter.header-button2') !!}
										</button>
										<ul id="dropdown3" class="dropdown-menu">
											<li><a class="a-submenu" href="/profile">{!! trans('headerfooter.header-dropdown1') !!}</a></li>
											<li><a class="a-submenu" href="#" onclick="Logout(event)">{!! trans('headerfooter.header-dropdown2') !!}</a></li>
										</ul>
									@endif
									</li>
								<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
								</ul>
							</div>
						</div>
					</nav>
				</div>
			</div>
			<div id="hero-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="padding-bottom:200px;">
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
					<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
						<!--Learn English at your own pace with the full IMPREVO Online English course-->
						<span id="hero-div-title" style="font-weight:300; font-size:50px;color:#ffffff;line-height: 60px;">{!! trans('coursesales12.span1') !!}</span>
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center; padding:0; margin:0;">
					<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
						<!--Interactive lessons, exciting themes, from start to finish with lots of tasks-->
						<span id="hero-div-span" style="font-weight:300; font-size:22px;color:#ffffff">{!! trans('coursesales12.span2') !!}</span>
					</div>
				</div>
				<!--<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
					<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:center">
						<button id="try-it-button" class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
							MEGNÉZEM A TANFOLYAMOT
						</button>
					</div>
				</div>	-->
			</div>

		<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="background-color:#ffffff;margin:0;padding:0px;width: 100%;justify-content:center">
			<div id="section1" class="mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--7-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="overflow:visible; margin-top:-80px;background-color:#f8f8f8;padding:0px;">
				<div id="section1-div1" class="mdl-grid mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--order-2-tablet mdl-cell--order-2-phone" style="background-color:#f8f8f8;padding:10px; padding-left:40px">
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin-left:0; padding-left:0">
						<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="margin-left:0; padding-left:0">
							<img src="/images/what_is_imprevo_section2-ticket.png">
						</div>
						<div class="section1-div-text mdl-cell mdl-cell mdl-cell--11-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-left:10px;">
							<span style="font-size:14px;color:#000000">{!! trans('coursesales12.span3') !!}</span>
						</div>
					</div>
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin-left:0; padding-left:0">
						<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="margin-left:0; padding-left:0">
							<img src="/images/what_is_imprevo_section2-ticket.png">
						</div>
						<div class="section1-div-text mdl-cell mdl-cell mdl-cell--11-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-left:10px;">
							<span style="font-size:14px;color:#000000">{!! trans('coursesales12.span4') !!}</span>
						</div>
					</div>
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--6-col-tablet mdl-cell--4-col-phone" style="margin-left:0; padding-left:0">
						<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="margin-left:0; padding-left:0">
							<img src="/images/what_is_imprevo_section2-ticket.png">
						</div>
						<div class="section1-div-text mdl-cell mdl-cell mdl-cell--11-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-left:10px;">
							<span style="font-size:14px;color:#000000">{!! trans('coursesales12.span5') !!}</span>
						</div>
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--6-col mdl-cell--hide-tablet mdl-cell--hide-phone " style="padding:10px;">

				</div>
				<div id="section1-div2" class="mdl-grid mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--order-3-tablet mdl-cell--order-3-phone" style="background-color:#f8f8f8;padding:10px; padding-left:0px">
					<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin-left:0; padding-left:0">
						<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="margin-left:0; padding-left:0">
							<img src="/images/what_is_imprevo_section2-ticket.png">
						</div>
						<div class="section1-div-text mdl-cell mdl-cell mdl-cell--11-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-left:10px;">
							<span style="font-size:14px;color:#000000">{!! trans('coursesales12.span6') !!} <span style="font-weight:bold">{!! trans('coursesales12.span7') !!}</span>, {!! trans('coursesales12.span8') !!}</span>
						</div>
					</div>
					<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin-left:0; padding-left:0">
						<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="margin-left:0; padding-left:0">
							<img src="/images/what_is_imprevo_section2-ticket.png">
						</div>
						<div class="section1-div-text mdl-cell mdl-cell mdl-cell--11-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-left:10px;">
							<span style="font-size:14px;color:#000000"><span style="font-weight:bold">{!! trans('coursesales12.span9') !!}</span> {!! trans('coursesales12.span10') !!}</span>
						</div>
					</div>
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet" style="margin-left:0; padding-left:0">
						<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="margin-left:0; padding-left:0">
							<img src="/images/what_is_imprevo_section2-ticket.png">
						</div>
						<div class="section1-div-text mdl-cell mdl-cell mdl-cell--11-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-left:10px;">
							<span style="font-size:14px;color:#000000">{!! trans('coursesales12.span11') !!}</span>
						</div>
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing mdl-cell--order-1-tablet mdl-cell--order-1-phone" style="background-color:#ffffff;padding:0;margin:0;justify-content:center;">
					<div id="section1-main-card" class="mdl-grid mdl-cell mdl-cell--5-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing mdl-card mdl-shadow--4dp" style="overflow:visible;margin:0;padding:0px; background-color:#ffffff; margin-top:-342px;">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:12px;margin:0;text-align:center;background-color:#f29f00">
							<span style="color:#ffffff"><img src="/images/course_salespage_smallogo.png" style="padding-right:10px">IMPREVO {{$course->title}}</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:10px; padding-top:50px;margin:0;text-align:center;">
							<span id="price-span" style="color:#000000;font-size:20px;">{!! trans('coursesales12.span13') !!}</span>
						</div>
						<div id="price-div" class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:10px; margin:0;text-align:center;">

						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:10px; padding-top:20px;margin:0;text-align:center;">
							<img src="/images/course_salespage_alak.png" style="width:95%">
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:10px; padding-top:20px;margin:0;text-align:center;">
							<span style="color:#b0b0b0; font-size:14px;">{!! trans('coursesales12.span14') !!}</span>
						</div>

						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:10px; padding-top:30px;margin:0;text-align:center;">
							<span style="font-size:14px;color:#000000;">{!! trans('coursesales12.span15') !!}</span>
						</div>

						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:10px; padding-top:30px;margin:0;text-align:center;">
							@if ($product)
							<button id="purchase-button" onclick="location.href = '/shoppingcart?products={{$product->id}}'" class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
								{!! trans('coursesales12.but1') !!}
							</button>
							@endif
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:10px; padding-top:20px; padding-bottom:50px;margin:0;text-align:center;">
							<span style="font-size:14px;"> {!! trans('coursesales12.span16') !!}</span>
						</div>
					</div>
				</div>
			</div>


			<div id="section2" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;width:100%;background:#ffffff">
				<div id="section2-title" class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:70px;text-align:center">
					<span style="font-size:14px;color:#399aed">{!! trans('coursesales12.span17') !!}</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
					<span style="line-height:40px; font-size:30px;">{!! trans('coursesales12.span18') !!}</span>
				</div>
				<div id="owl-carousel-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:50px;justify-content:center;">
					<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--middle mdl-cell--hide-phone" style="">
						<a id="carousel_prev" class="a-next-prev"><i class="fa fa-chevron-left"></i></a>
					</div>
					<div id="owl-carousel" class="owl-carousel owl-theme" data-plugin-carousel data-plugin-options='{ "dots": true, "autoplay": false, "autoplayTimeout": 3000, "loop": false, "margin": 10, "nav": false, "responsive": {"0":{"items":1 }, "600":{"items":3 }, "1000":{"items":3 } }  }'
						style="width:70%;">
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-size:15px;">“ Rendszerezett, jól felépített tananyag, könnyű használni. Engem mindig magával ragad, nehéz abbahagyni. ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">Gabi</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-size:15px;">“ Egy igazán modern segítség a nyelvtanuláshoz. Az ingyenes leckékkel érdemes kipróbálni, nem lehet veszíteni rajta. ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">István</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-size:15px;">“ Ez a tananyag alkalmas az önálló tanulásra. Még egészen az elején tartok, de már egyre jobban megértem a hallott szöveget is. ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">ANNA</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-size:15px;">“ Kreatív, jó humorú, logikus, könnyen emészthető. ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">Zsuzsi</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-size:15px;">“ Korrekt, nagyon részletes magyarázatokkal, és az ára is megfelelő. És az, hogy korlátlan a hozzáférés, király! ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">Ottó</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-size:15px;">“ Célszerű. Érthető, nem kell hozzá előzetesen nyelvtani zseninek lenni. Tetszetős a szavak memoriter módszere, … ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">Zsolt</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-size:15px;">“ Ha teljes körű angol nyelvórákat szeretnél venni, és nem szeretnél csoportos foglalkozáson részt venni,... ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">Bea</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-size:15px;">“ Olcsón, és profi rendszerrel, könnyedén a saját tempódban tudsz megtanulni, a kezdő szinttől kezdve a haladó középfokig.... Ajánlom mindenkinek!”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">Péter</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-size:15px;">“ Összességében nagyon jó lehetőség a nyelvtanulásra. A tanfolyamok töredékéért kapom minimum ugyanazt, de szerintem ... ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">Kázmér</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-size:15px;">“ Nyelvtanfolyamra is járok! E mellé nagyon nagy segítség az IMPREVO! A tanfolyamon általában sietnek,...! ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">György</span>
								</div>

							</div>
						</div>
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--middle mdl-cell--hide-phone" style="">
						<a id="carousel_next" class="a-next-prev" style="float:left"><i class="fa fa-chevron-right"></i></a>
					</div>
				</div>
			</div>
		 </div>

			<div id="section3" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet" style="margin:0;padding:40px 0 40px 0;width:100%;background-color:#399aed;text-align:center; justify-content:center">
				<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
					<span style="color:#ffffff; font-size:25px; margin-bottom:20px;">{!! trans('coursesales12.span19') !!}</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
					<button id="try-it-button-second" onclick="location.href='/register'" class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
						{!! trans('coursesales12.span20') !!}
					</button>
				</div>
			</div>
			<div id="section4" class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:40px 0 40px 0;width:100%;background-color:#ffffff; justify-content:center">
				<div id="section4-div0" class="mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
					<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
						<span id="section4-span1" style="line-height:50px;font-size:40px">{!! trans('coursesales12.span21') !!}</span>
					</div>
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="">
						<div class="section4-div1 mdl-grid mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-left:100px;">
							<div class="section4-div1-div1 mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="padding-top:30px">
								<span style="color:#399aed">{!! trans('coursesales12.span22') !!}</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--10-col mdl-cell--4-col-phone" style="border-bottom:1px solid #eee; padding-top:20px; padding-bottom:15px">
								<span style="">{!! trans('coursesales12.span23') !!} </span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="padding-top:15px">
								<span style="color:#399aed">{!! trans('coursesales12.span24') !!}</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--10-col mdl-cell--4-col-phone" style="border-bottom:1px solid #eee; padding-top:20px; padding-bottom:15px">
								<span style="">{!! trans('coursesales12.span25') !!}</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="padding-top:15px">
								<span style="color:#399aed">{!! trans('coursesales12.span26') !!}</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--10-col mdl-cell--4-col-phone" style="border-bottom:1px solid #eee; padding-top:20px; padding-bottom:15px">
								<span style="">{!! trans('coursesales12.span27') !!}</span>
							</div>

						</div>
						<div class="section4-div1 mdl-grid mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-left:100px">
							<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="padding-top:30px">
								<span style="color:#399aed">{!! trans('coursesales12.span28') !!}</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--10-col mdl-cell--4-col-phone" style="padding-top:20px;border-bottom:1px solid #eee;padding-bottom:15px">
								<span style="">{!! trans('coursesales12.span29') !!} </span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="padding-top:15px">
								<span style="color:#399aed">{!! trans('coursesales12.span30') !!}</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--10-col mdl-cell--4-col-phone" style="padding-top:20px;border-bottom:1px solid #eee;padding-bottom:15px">
								<span style="">{!! trans('coursesales12.span31') !!}</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="padding-top:15px">
								<span style="color:#399aed">{!! trans('coursesales12.span32') !!}</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--10-col mdl-cell--4-col-phone" style="padding-top:20px;border-bottom:1px solid #eee;padding-bottom:15px">
								<span style="">{!! trans('coursesales12.span33') !!}</span>
							</div>
						</div>
					</div>
					<div id="section4-div-latest" class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
						<span class="section3-link2" style="font-size:16px;">{!! trans('coursesales12.span34') !!} <a href='https://imprevo.hu/what-is-imprevo'>{!! trans('coursesales12.span35') !!}</a> {!! trans('coursesales12.span36') !!} <a href='https://imprevo.hu/kik-vagyunk'>{!! trans('coursesales12.span37') !!}</a></span>
					</div>
				</div>

			</div>
		 </div>
		@include('layouts.footer')
	</div>
</main>
</div>
<script>
function goLogin() {
      location.href = '/login'
}

function goProfile(e) {
  e.preventDefault();
  document.getElementById('logout-form').submit();
}

function Logout(e) {
    e.preventDefault();
    document.getElementById('logout-form').submit();
}

$(document).ready(function() {

    var owl = $("#owl-carousel");


    // Custom Navigation Events
    $("#carousel_next").click(function() {
		console.log('next');
		console.log(owl);
         owl.trigger('next.owl.carousel');
    });

    $("#carousel_prev").click(function() {
		console.log('prev');
         owl.trigger('prev.owl.carousel');
    });

});

</script>
<script>

window.onscroll = function ()  { //detect page scroll
	var sticky = $('#header-div'),
	scroll = document.documentElement.scrollTop || document.body.scrollTop;

	if (scroll >= 100) {
		sticky.addClass('fixed');
		document.getElementById("header-div").style.padding = "5px 0px 5px 0px";
	}
	else{
		sticky.removeClass('fixed');
		document.getElementById("header-div").style.padding = "12px 0px 12px 0px";
	}

};
function isMobileDevice() {
	return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

console.log(isMobileDevice());
if (!isMobileDevice()){
	$('ul.nav li.dropdown').hover(function() {
	$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
	}, function() {
	$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
	});
}

	$('#my-account-button-logged').hover(function() {
		$('#dropdown3').stop(true, true).delay(100).fadeIn(500);
	}, function() {
		$('#dropdown3').stop(true, true).delay(100).fadeOut(500);
	});

		$('.phone-footer-accordion .item .heading').click(function() {

		var a = $(this).closest('.item');
		var b = $(a).hasClass('open');
		var c = $(a).closest('.accordion').find('.open');

		if(b != true) {
			$(c).find('.content').slideUp(200);
			$(c).removeClass('open');
		}

		$(a).toggleClass('open');
		$(a).find('.content').slideToggle(200);

	});
</script>
<script>
var html = '';
var left_days = null;
var left_hours = null;
var left_mins = null;
var remain_orders = null;
var distance = 0;
//var timeLocal = new Date();

//var millDiff = timeLocal.getTime() - timeServer.getTime();


@if ($product)
  @if ($product->sale_price)
     @if ($product->sale_price_end_date)
       var timeServer = new Date(<?='"'.date('Y-m-d H:i:s').'"' ?>);
        end_time = new Date("{{$product->sale_price_end_date}}").getTime();
        distance = (end_time - timeServer.getTime()) / 1000 ;

        if (distance > 0)
        {
      		   left_mins = Math.floor((distance / 60) % 60);
      		   left_hours = Math.floor((distance / (60 * 60)) % 24);
      		   left_days = Math.floor(distance / (60 * 60 * 24));
        }
     @endif

     @if ($product->sale_price_orders_num)
        var total = parseInt("{{$product->sale_price_orders_num}}");
        var current = parseInt("{{$product->sale_price_orders_current}}");
        remain_orders = total - current;
     @endif

     if (distance > 0 && remain_orders > 0)
     {
	   var str = '';
	   str = left_days + ' NAP ' + left_hours +  ' ÓRA ' + left_mins + ' PERC';
	   if (left_days == 0)
	   {
		   str = left_hours +  ' ÓRA ' + left_mins + ' PERC';
	   }
	   else if (left_days == 0 && left_hours == 0)
	   {
		   str = left_mins + ' PERC';
	   }
       @if ($shoppingcartsetting['currency'] == "USD")
          html = '<div class="mdl-cell mdl-cell mdl-cell--12-col">' +
                    '<span style="color:#0c484c;font-size:40px; text-decoration: line-through; padding:0;">' +
                        '$<?php echo number_format($product->regular_price) ?>' +
                    '</span>' +
                  '</div>';
          html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col">' +
                    '<span style="color:#0c484c;font-size:20px; padding:0;">' +
                        'helyett' +
                    '</span>' +
                  '</div>';

          html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px">' +
                    '<span style="color:#000000;font-size:70px;  padding:0;">' +
                        '$<?php echo number_format($product->sale_price) ?>' +
                    '</span>' +
                '</div>';
          html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px">' +
                          '<span style="color:#000000;font-size:18px; padding:0; font-weight:bold;">' +
                              '<span style="color:#f29f00">' + str + ' </span>MÚLVA LEJÁR' +
                          '</span>'
                  '</div>';

          html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col">' +
                      '<span style="color:#000000;font-size:18px; padding:0; font-weight:bold;">' +
                          'MÉG 79 AKCIÓS CSOMAG ELÉRHETŐ!' +
                      '</span>' +
                '</div>';
       @else
       html = '<div class="mdl-cell mdl-cell mdl-cell--12-col">' +
                 '<span style="color:#0c484c;font-size:40px; text-decoration: line-through; padding:0;">' +
                     '<?php echo number_format($product->regular_price) ?> <span style="font-size:20px;vertical-align: super;">Ft</span>' +
                 '</span>' +
               '</div>';
       html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col">' +
                 '<span style="color:#0c484c;font-size:20px; padding:0;">' +
                     'helyett' +
                 '</span>' +
               '</div>';

       html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px">' +
                 '<span style="color:#000000;font-size:70px;">' +
                     '<?php echo number_format($product->sale_price) ?> <span style="font-size:20px;vertical-align: super;">Ft</span>' +
                 '</span>' +
             '</div>';
       html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px">' +
                       '<span style="color:#000000;font-size:18px; padding:0; font-weight:bold;">' +
                           '<span style="color:#f29f00">' + str + ' </span>MÚLVA LEJÁR' +
                       '</span>'
               '</div>';

       html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col">' +
                   '<span style="color:#000000;font-size:18px; padding:0; font-weight:bold;">' +
                       'MÉG' + '<span style="color:#f29f00">' + ' ' + remain_orders + ' AKCIÓS CSOMAG </span> ELÉRHETŐ!' +
                   '</span>' +
             '</div>';
       @endif
     }
     else {
	   document.getElementById('price-span').innerHTML = "{!! trans('coursesales12.span46') !!}";
       @if ($shoppingcartsetting['currency'] == "USD")
          html = '<span style="color:#000000;font-size:70px; padding:0px;">' +
                    '$<?php echo number_format($product->regular_price) ?>' +
              '</span>';
       @else
          html = '<span style="color:#000000;font-size:70px; padding:0px">' +
                 '<?php echo number_format($product->regular_price) ?> <span style="font-size:20px;vertical-align: super;">Ft</span>' +
           '</span>';
       @endif
     }
  @else
	 document.getElementById('price-span').innerHTML = "{!! trans('coursesales12.span46') !!}";
     @if ($shoppingcartsetting['currency'] == "USD")
        html = '<span style="color:#000000;font-size:70px; padding:0;">' +
                  '$<?php echo number_format($product->regular_price) ?>' +
            '</span>';
     @else
        html = '<span style="color:#000000;font-size:70px; padding:0;">' +
               '<?php echo number_format($product->regular_price) ?> <span style="font-size:20px;vertical-align: super;">Ft</span>' +
         '</span>';
     @endif
  @endif
@endif
document.getElementById('price-div').innerHTML = html;
</script>
</body>
</html>
