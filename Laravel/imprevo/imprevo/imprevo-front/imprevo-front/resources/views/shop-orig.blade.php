@extends('layouts.app')
<style>
#brecumb_text{
	padding:5px 40px 5px 50px;
}

.div-part1{
	width: 33.33333% !important;
}
.div-part2{
	width: 40% !important;
}
.div-part3 {
	width: 26.66666% !important;
}

tr{
	height: 40px;
}
td > span {
	font-size: 16px;
	color:#354f56;
}

#addcart {
	background-color:#f29f00;
	width:160px;
	height:50px;
}

@media (max-width: 450px) {
	#brecumb_text{
		padding:5px 0px 5px 10px;
	}
}
</style>


@section('content')
<div id="main-content" class="mdl-layout__content" style="width:100%;background:#ebf3ef;">
    <div id="brecumb_text" style="background: #9fc3ae;font-size: 16px;">
        <span><a href="/" style="color:#d5e7dc;line-height: 40px;text-decoration: none">{!! trans('order.imprevo') !!}</a></span>
        <span style="color:#d5e7dc;line-height: 40px">/</span>
        <span style="color:#fff;line-height: 40px">{!! trans('order.shop') !!}</span>
    </div>
	<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="justify-content:center;min-height:550px;">
		<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:50px;">
			<section class="panel form-wizard" id="w1" style="width:100%">
				<div class="panel-body panel-body-nopadding" style="background:#ebf3ef">
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="width:100%;background:#ebf3ef;padding:0; margin:0">
						<h3 style="color:#3b4a51">IMPREVO Shop</h3>
					</div>
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="background:#ffffff;width:100%;height:350px; padding:0; margin:0">
							<div class="mdl-cell div-part1" style="padding:0; margin:0">
									<img src="/images/shop_pic1.png" style="width:100%; height:100%" />
							</div>
							<div class="mdl-cell div-part2" style="padding-left:50px">
									<div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0; margin-top:30px">
										<h3 style="color:#354f56; padding:0; margin:0">IMPREVO Online Angol ALL STAR</h3>
									</div>
									<div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
										<table>
											<tbody>
												<tr>
													<td><img src="/images/shop_tick.png" style="width:25px; height:25px"/></td>
													<td><span style="padding:10px">200+ lecke teljesen az alapoktól a felsőfokig</span></td>
												</tr>
												<tr>
													<td><img src="/images/shop_tick.png" style="width:25px; height:25px"/></td>
													<td><span style="padding:10px">200+ lecke teljesen az alapoktól a felsőfokig</span></td>
												</tr>
												<tr>
													<td><img src="/images/shop_tick.png" style="width:25px; height:25px"/></td>
													<td><span style="padding:10px">200+ lecke teljesen az alapoktól a felsőfokig</span></td>
												</tr>
												<tr>
													<td><img src="/images/shop_tick.png" style="width:25px; height:25px"/></td>
													<td><span style="padding:10px">200+ lecke teljesen az alapoktól a felsőfokig</span></td>
												</tr>
												<tr>
													<td><img src="/images/shop_tick.png" style="width:25px; height:25px"/></td>
													<td><span style="padding:10px">200+ lecke teljesen az alapoktól a felsőfokig</span></td>
												</tr>
											</tbody>
										</table>
									</div>
							</div>
							<div class="mdl-cell div-part3" style="vertical-align:middle; padding:30px">
								<div class="mdl-cell--12-col" style="text-align:center; padding:10; padding-top:">
									<span style="font-size:20px;text-decoration:line-through">23.990Ft</span>
								</div>
								<div class="mdl-cell--12-col" style= "padding:10;text-align:center">
									<span style="font-size:40px">19.990Ft</span>
								</div>
								<div class="mdl-cell--12-col" style="padding:10;text-align:center">
									<span><span style="font-size:16px;color:#f29f00">10 nap 8 ora 30 pec</span>mulva lejar<br/><span style="color:#f29f00">79 akcios csomag</span>elerheto</span>
								</div>
								<div class="mdl-cell--12-col" style="padding:10;text-align:center">
									<button id="addcart" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">KOSÁRBA</button>
								</div>
								<div class="mdl-cell--12-col" style="padding:10;text-align:center">
									<span style="color:#bfbfbf"> * A feltüntetett ár bruttó ár,<br/> a 27% ÁFA-t tartalmazza!</span>
								</div>										
							</div>
					</div>
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="width:100%; background:#63a77f; height:350px; padding:0; margin:0; margin-top:20px">
							<div class="mdl-cell div-part1" style="padding:0">
									<img src="/images/shop_pic1.png" style="width:100%; height:100%;  margin:0" />
							</div>
							<div class="mdl-cell div-part2" style="padding-left:50px">
									<div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0; margin-top:30px">
										<h3 style="color:#354f56; padding:0; margin:0">IMPREVO Online Angol ALL STAR</h3>
									</div>
									<div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
										<table>
											<tbody>
												<tr>
													<td><img src="/images/shop_tick.png" style="width:25px; height:25px"/></td>
													<td><span style="padding:10px">200+ lecke teljesen az alapoktól a felsőfokig</span></td>
												</tr>
												<tr>
													<td><img src="/images/shop_tick.png" style="width:25px; height:25px"/></td>
													<td><span style="padding:10px">200+ lecke teljesen az alapoktól a felsőfokig</span></td>
												</tr>
												<tr>
													<td><img src="/images/shop_tick.png" style="width:25px; height:25px"/></td>
													<td><span style="padding:10px">200+ lecke teljesen az alapoktól a felsőfokig</span></td>
												</tr>
												<tr>
													<td><img src="/images/shop_tick.png" style="width:25px; height:25px"/></td>
													<td><span style="padding:10px">200+ lecke teljesen az alapoktól a felsőfokig</span></td>
												</tr>
												<tr>
													<td><img src="/images/shop_tick.png" style="width:25px; height:25px"/></td>
													<td><span style="padding:10px">200+ lecke teljesen az alapoktól a felsőfokig</span></td>
												</tr>
											</tbody>
										</table>
									</div>
							</div>
							<div class="mdl-cell div-part3" style="vertical-align:middle; padding:30px">
								<div class="mdl-cell--12-col" style="text-align:center; padding:10; padding-top:">
									<span style="font-size:20px;text-decoration:line-through">23.990Ft</span>
								</div>
								<div class="mdl-cell--12-col" style= "padding:10;text-align:center">
									<span style="font-size:40px">19.990Ft</span>
								</div>
								<div class="mdl-cell--12-col" style="padding:10;text-align:center">
									<span><span style="font-size:16px;color:#f29f00">10 nap 8 ora 30 pec</span>mulva lejar<br/><span style="color:#f29f00">79 akcios csomag</span>elerheto</span>
								</div>
								<div class="mdl-cell--12-col" style="padding:10;text-align:center">
									<button id="addcart" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">KOSÁRBA</button>
								</div>
								<div class="mdl-cell--12-col" style="padding:10;text-align:center">
									<span style="color:#bfbfbf"> * A feltüntetett ár bruttó ár,<br/> a 27% ÁFA-t tartalmazza!</span>
								</div>										
							</div>
					</div>					
				</div>
			</section>
		</div>
	</div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script type="text/javascript">


</script>
@endsection
