@extends('layouts.app3')
<style>

.wizard-steps > li >a{
	padding:20px;
	height:70px;
}
.wizard-steps > li >a >span{
	background-color:#006e73;
	border:1px solid #ffffff;
	width:24px;
	height:24px;
	padding: 5px 7px 5px 7px;
}
.wizard-steps .active >a >span{
	background-color:#ffffff;
	border:1px solid #006e73;
	color:#006e73;
}
.is-focused .mdl-textfield__input {
	border:none;
}

div.mdl-textfield.is-dirty {
   padding-right:25px !important;
   background: url('/images/tick1.png') no-repeat !important;
   background-position: right 0px top 25px !important;
}

div.mdl-textfield.is-dirty.is-invalid{
   background: none !important;
}

div.mdl-textfield_password{
   background: none !important;
}

.accordion {
  width: 380px;
  border-radius: 5px;
  overflow: hidden;
  margin: auto;
}

.accordion .item .heading {
  height: 50px;
  line-height: 50px;
  font-size: 17px;
  cursor: pointer;
  color: #000000;
  padding-left: 15px;
  background: #f9f9f9 url('/images/order_accordion_arrow.png') no-repeat;
  background-position: right 20px top -95px;
  border: 1px solid #eee;
  box-sizing: border-box;
}

.accordion .item.open .heading,
.accordion .item:last-child .heading { border: 0; }

.accordion .item.open .heading { background-position: right 20px top -5px; }

.accordion .item .content {
  display: none;
  padding: 15px;
  background: #f9f9f9;
  font-size: 14px;
}



#GoBill ,#GoPAYMENT ,#GoConfirm ,#GoPay ,#GoBill-register, #GoHome{
	background-color:#399aed;
}

#GoBill:hover, ##GoBill-register:hover, GoHome:hover{
	background-color:#2081d5;
}

#GoBill-register{
	background-color:#2081d5;
}

#GoBill-register.disabled{
	background-color:#eee;
}

#GoPAYMENT:hover{
	background-color:#2081d5;
}

#GoConfirm:hover{
	background-color:#2081d5;
}

#GoPay:hover{
	background-color:#2081d5;
}

#tooltip-barion-img{
	position:relative;
}

.mdl-tooltip
{
	padding:10px;
	max-width:500px !important;
	width:400px;
	background-color:#9fc3ae;
}
.mdl-tooltip.is-active {
	will-change: unset;
	background-color:#9fc3ae;
	text-align:center;
	color:#ffffff;
}

.mdl-tooltip.is-active:before{
	content: ' ';
	position: absolute;
	left: 200px;
	top: -7px;
	width: 14px;
	height: 14px;
	background-color: #9fc3ae;
	border-top: 1px solid #9fc3ae;
	border-left: 1px solid #9fc3ae;
	border-bottom: none;
	border-right: none;
	transform: rotate(45deg);
	-webkit-transform: rotate(45deg);
	-moz-transform: rotate(45deg);
}

#brecumb_text{
	padding:5px 40px 5px 80px;
}
#main-content{
	padding-bottom:150px;
}

.tab-title {
	font-size:25px;
}

@media (max-width:1500px)
{
	.confirm-content{
		width: 80% !important;
	}
}

@media (max-width: 1380px) {
	.wizard-steps > li >a{
		padding:20px;
		padding-top:26px;
		height:85px;
		font-size:14px !important;
	}
	.wizard-tabs ul > li > a:before, .wizard-tabs ul > li > a:after {
		border-bottom: 42px solid transparent !important;
		border-top: 42px solid transparent !important;
	}

	.divtitle {
		padding:20px 0px 20px 0px !important;
	}
}
@media (max-width:1240px)
{
	.confirm-content{
		width: 90% !important;
	}
}
@media (max-width:1080px)
{
	.confirm-content{
		width: 100% !important;
	}
}
@media (max-width: 975px) {
	.wizard-steps > li >a{
		padding:20px;
		height:85px;
		font-size:13px !important;
	}
}
@media (max-width: 769px) {
	.wizard-steps > li >a{
		padding:20px;
		height:85px;
		font-size:13px !important;
	}
	.wizard-steps > li >a >span{
		background-color:#006e73;
		border:1px solid #ffffff;
		width:24px;
		height:24px;
		display:inline-block !important;
		padding: 5px 7px 5px 7px;
	}

	.tab-title {
		font-size:20px;
	}
	.wizard-tabs ul > li > a:before, .wizard-tabs ul > li > a:after {
		border:none !important;
	}
	#main-content{
		padding-bottom:250px;
	}
}

@media (max-width: 450px) {
	.wizard-steps > li >a{
		text-align:center !important;
		padding:5px;
		font-size:10px !important;
	}

	.wizard-steps .active >a >span{
		background-color:#ffffff;
		border:1px solid #006e73;
		color:#006e73;
	}

	#brecumb_text{
		padding:5px 0px 5px 10px;
	}

}

</style>


@section('content')
<div id="main-content" class="mdl-layout__content" style="width:100%;background:#ebf3ef;">
    <div id="brecumb_text" style="background: #9fc3ae;font-size: 16px;">
        <span><a href="/" style="color:#d5e7dc;line-height: 40px;text-decoration: none">{!! trans('order.imprevo') !!}</a></span>
        <span style="color:#d5e7dc;line-height: 40px">/</span>
        <span style="color:#fff;line-height: 40px">{!! trans('order.shop') !!}</span>
    </div>
	<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="justify-content:center;min-height:550px;">
		<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:50px;">
			<section class="panel form-wizard" id="w1" style="width:100%">
				<div class="panel-body panel-body-nopadding">
					<div class="wizard-tabs">
						<ul id="tab-desktop" class="wizard-steps mdl-cell--hide-phone mdl-cell--hide-tablet">
							<li id="w1-tab1">
								<a href="#w1-step1" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">1</span>
									{!! trans('order.register') !!}
								</a>
							</li>
							<li id="w1-tab2">
								<a href="#w1-step2" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">2</span>
									{!! trans('order.span1') !!}
								</a>
							</li>
							<li  id="w1-tab3" class="active">
								<a href="#w1-step3" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">3</span>
									{!! trans('order.span3') !!}
								</a>
							</li>
							<li id="w1-tab4">
								<a href="#w1-step4" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">4</span>
									{!! trans('order.span4') !!}
								</a>
							</li>
						</ul>
						<ul class="wizard-steps mdl-cell--hide-desktop">
							<li id="w1-tab1">
								<a href="#w1-step1" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">1</span><br>
									{!! trans('order.register') !!}
								</a>
							</li>
							<li id="w1-tab2">
								<a href="#w1-step2" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">2</span><br>
									{!! trans('order.span1') !!}
								</a>
							</li>
							<li  id="w1-tab3"  class="active">
								<a href="#w1-step3" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">3</span><br>
									{!! trans('order.span3') !!}
								</a>
							</li>
							<li id="w1-tab4">
								<a href="#w1-step4" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">4</span><br>
									{!! trans('order.span4') !!}
								</a>
							</li>
						</ul>
					</div>
						<div class="tab-content" style="padding:0;">
							<div id="w1-step3" class="tab-pane active">
								<form id="form-payment" name="form-payment" role="form" class="form-horizontal" action="/order/purchase_overview" method="post" encType="multipart/form-data">
									{{ csrf_field() }}
									<input type="hidden" name="tax_num" value="{{$tax_num}}"/>
									<input type="hidden" name="is_valid_vat" value="{{$is_valid_vat}}"/>
									<input type="hidden" name="eu_vat_num" value="{{$eu_vat_num}}"/>
									<input type="hidden" id="payment_input" name="payment_input" value=""/>
									<input type="hidden" id="account" name="account" value=""/>
									<input type="hidden" id="selectProducts" name="selectProducts" value="{{$productstr}}"/>

									<div id="payments-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;">
									<div class="divtitle mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding:50px 0px 50px 0px">
										<span class="tab-title" style="font-size:25px; color:#000000">{!! trans('order.span32') !!}</span>
									</div>

									<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="">
										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="border:1px solid #eee;margin:0; background-color:#f9f9f9">
											<div class="mdl-cell mdl-cell mdl-cell--hide-desktop mdl-cell--hide-tablet mdl-cell--4-col-phone" style="text-align:center">
												<img src="/images/order_barion_logo.png" style="width:80px; height:30px">
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-phone" style="text-align:center; padding:20px 0px 20px 0px;">
												<label class="mdl-radio mdl-js-radio" for="option1">
													<input type="radio" id="option1" name="payment_radio" class="mdl-radio__button" value="barion" checked>
													<span class="mdl-radio__label"></span>
												</label>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--7-col mdl-cell--3-col-phone" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
													<span style="font-size:16px; font-weight:bold">{{$shoppingcartsetting['barion_title']}}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
													<span style="">
														({!! trans('order.span33') !!}) <img id="tooltip-barion-img" src="/images/shoppingcart-tooltip.png" style="padding-left:20px">
													</span>

												</div>
												<div class="mdl-tooltip barion" for="tooltip-barion-img" style="">
													<!--<span style="font-size:14px" data-tooltip data-width="350" class="has-tip" title="Lorem ipsum dolor">{{$shoppingcartsetting['barion_description']}}</span>-->
													<span style="font-size:14px">{{$shoppingcartsetting['barion_description']}}</span>
												</div>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--hide-phone" style="text-align:center">
												<img src="/images/order_barion_logo.png">
											</div>
										</div>

										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="border:1px solid #eee;margin:0; background-color:#f9f9f9">
											<div class="mdl-cell mdl-cell mdl-cell--hide-desktop mdl-cell--hide-tablet mdl-cell--4-col-phone" style="text-align:center">
												<img src="/images/order-paypal-logo.png" style="width:80px; height:30px">
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-phone" style="text-align:center; padding:20px 0px 20px 0px;">
												<label class="mdl-radio mdl-js-radio" for="option2">
													<input type="radio" id="option2" name="payment_radio" class="mdl-radio__button" value="paypal">
													<span class="mdl-radio__label"></span>
												</label>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--7-col mdl-cell--3-col-phone" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
													<span style="font-size:16px; font-weight:bold">{{$shoppingcartsetting['paypal_title']}}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
													<span style="">({!! trans('order.span33') !!})<img id="tooltip-paypal-img" src="/images/shoppingcart-tooltip.png" style="padding-left:20px"></span>
												</div>
												<div class="mdl-tooltip paypal" for="tooltip-paypal-img" style="">
													<span style="font-size:14px">{{$shoppingcartsetting['paypal_description']}}</span>
												</div>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--hide-phone" style="text-align:center">
												<img src="/images/order-paypal-logo.png">
											</div>
										</div>

										<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee;margin:0; background-color:#f9f9f9">
											<div class="mdl-cell mdl-cell mdl-cell--hide-desktop mdl-cell--hide-tablet mdl-cell--4-col-phone" style="text-align:center">
												<img src="/images/order-bank-transfer.png" style="width:80px; height:30px">
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-phone" style="text-align:center; padding:20px 0px 20px 0px;">
												<label class="mdl-radio mdl-js-radio" for="option3">
													<input type="radio" id="option3" name="payment_radio" class="mdl-radio__button" value="bank">
													<span class="mdl-radio__label"></span>
												</label>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--7-col mdl-cell--3-col-phone" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
													<span style="font-size:16px; font-weight:bold">{{$shoppingcartsetting['bank_title']}}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
													<span style="">({!! trans('order.span34') !!})<img id="tooltip-bank-img" src="/images/shoppingcart-tooltip.png" style="padding-left:20px"></span>
												</div>
												<div class="mdl-tooltip bank" for="tooltip-bank-img" style="">
													<span style="font-size:14px">{{$shoppingcartsetting['bank_description']}}</span>
												</div>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--hide-phone" style="text-align:center">
												<img src="/images/order-bank-transfer.png">
											</div>
										</div>
									</div>
									<div class="mdl-grid mdl-cell mdl-cell--7-col" style="margin:0;padding:0;">
										<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
											<span style="font-size:16px;color:#979797">
												<i aria-hidden="true" class="fa fa-lock" style="padding-right:10px;font-size:16px;">
												</i>
												{!! trans('order.span35') !!}
											</span>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
											<span style="font-size:16px;color:#979797"></span>
										</div>
									</div>
									<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0;justify-content:center">
										<div class="mdl-grid mdl-cell mdl-cell--6-col">
											<div class="mdl-cell mdl-cell mdl-cell--7-col" style="text-align:center">
												<span id="GoConfirm" class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
													{!! trans('order.span36') !!}
												</span>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--5-col" style="text-align:center">
												<a href="/order?back=2" style="cursor:pointer;color:#979797">{!! trans('order.span37') !!}</a>
											</div>
										</div>
									</div>
								</div>
								</form>
							</div>
						</div>
				</div>
			</section>
		</div>
	</div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script type="text/javascript">

function isMobileDevice() {
	return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

if (!isMobileDevice) {
	$('#tab-desktop').remove();
}

var countries = <?php echo json_encode($countrylist) ?>;
var currentTab = 1;
var EuropeUnionlist = ['Austria', 'Italy',
'Belgium',	'Latvia',
'Bulgaria',	'Lithuania',
'Croatia',	'Luxembourg',
'Cyprus',	'Malta',
'Czechia',	'Netherlands',
'Denmark',	'Poland',
'Estonia',	'Portugal',
'Finland',	'Romania',
'France',	'Slovakia',
'Germany'	,'Slovenia',
'Greece',	'Spain',
'Hungary'	,'Sweden',
'Ireland',	'United Kingdom'];

var notEuropeUnionlist = ['Albania', 'Andorra', 'Bosnia and Herzegovina', 'Liechtenstein', 'Norway'];
var total = 0;
@foreach ($products as $product)
	@if (!is_null($product->sale_price))
		each_price = parseFloat("{{$product->sale_price}}");
	@else
		each_price =  parseFloat("{{$product->regular_price}}");
	@endif
	total = total + each_price;
@endforeach


@if ($user)
	document.getElementById('account').value = "{{$user->email}}";
@endif


$(document).ready(function () {
	$('#GoConfirm').click(function(){
		value = $('input[name="payment_radio"]:checked').val();
		document.getElementById('payment_input').value = value;
		document.getElementById('form-payment').submit();
	})

	$('.accordion .item .heading').click(function() {
		var a = $(this).closest('.item');
		var b = $(a).hasClass('open');
		var c = $(a).closest('.accordion').find('.open');

		if(b != true) {
			$(c).find('.content').slideUp(200);
			$(c).removeClass('open');
		}

		$(a).toggleClass('open');
		$(a).find('.content').slideToggle(200);
	});
});
</script>
@endsection
