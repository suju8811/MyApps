@extends('layouts.app3')
<style>

.wizard-steps > li >a{
	padding:20px;
	height:70px;
}
.wizard-steps > li >a >span{
	background-color:#006e73;
	border:1px solid #ffffff;
	width:24px;
	height:24px;
	padding: 5px 7px 5px 7px;
}
.wizard-steps .active >a >span{
	background-color:#ffffff;
	border:1px solid #006e73;
	color:#006e73;
}
.is-focused .mdl-textfield__input {
	border:none;
}

div.mdl-textfield.is-dirty {
   padding-right:25px !important;
   background: url('/images/tick1.png') no-repeat !important;
   background-position: right 0px top 25px !important;
}

div.mdl-textfield.is-dirty.is-invalid{
   background: none !important;
}

div.mdl-textfield_password{
   background: none !important;
}

.accordion {
  width: 380px;
  border-radius: 5px;
  overflow: hidden;
  margin: auto;
}

.accordion .item .heading {
  height: 50px;
  line-height: 50px;
  font-size: 17px;
  cursor: pointer;
  color: #000000;
  padding-left: 15px;
  background: #f9f9f9 url('/images/order_accordion_arrow.png') no-repeat;
  background-position: right 20px top -95px;
  border: 1px solid #eee;
  box-sizing: border-box;
}

.accordion .item.open .heading,
.accordion .item:last-child .heading { border: 0; }

.accordion .item.open .heading { background-position: right 20px top -5px; }

.accordion .item .content {
  display: none;
  padding: 15px;
  background: #f9f9f9;
  font-size: 14px;
}



#GoBill ,#GoPAYMENT ,#GoConfirm ,#GoPay ,#GoBill-register, #GoHome{
	background-color:#399aed;
}

#GoBill:hover, ##GoBill-register:hover, GoHome:hover{
	background-color:#2081d5;
}

#GoBill-register{
	background-color:#2081d5;
}

#GoBill-register.disabled{
	background-color:#eee;
}

#GoPAYMENT:hover{
	background-color:#2081d5;
}

#GoConfirm:hover{
	background-color:#2081d5;
}

#GoPay:hover{
	background-color:#2081d5;
}

#tooltip-barion-img{
	position:relative;
}

.mdl-tooltip
{
	padding:10px;
	max-width:500px !important;
	width:400px;
	background-color:#9fc3ae;
}
.mdl-tooltip.is-active {
	will-change: unset;
	background-color:#9fc3ae;
	text-align:center;
	color:#ffffff;
}

.mdl-tooltip.is-active:before{
	content: ' ';
	position: absolute;
	left: 200px;
	top: -7px;
	width: 14px;
	height: 14px;
	background-color: #9fc3ae;
	border-top: 1px solid #9fc3ae;
	border-left: 1px solid #9fc3ae;
	border-bottom: none;
	border-right: none;
	transform: rotate(45deg);
	-webkit-transform: rotate(45deg);
	-moz-transform: rotate(45deg);
}

#brecumb_text{
	padding:5px 40px 5px 80px;
}
#main-content{
	padding-bottom:150px;
}

.tab-title {
	font-size:25px;
}

@media (max-width:1500px)
{
	.confirm-content{
		width: 80% !important;
	}
}

@media (max-width: 1380px) {
	.wizard-steps > li >a{
		padding:20px;
		padding-top:26px;
		height:85px;
		font-size:14px !important;
	}
	.wizard-tabs ul > li > a:before, .wizard-tabs ul > li > a:after {
		border-bottom: 42px solid transparent !important;
		border-top: 42px solid transparent !important;
	}

	.divtitle {
		padding:20px 0px 20px 0px !important;
	}
}
@media (max-width:1240px)
{
	.confirm-content{
		width: 90% !important;
	}
}
@media (max-width:1080px)
{
	.confirm-content{
		width: 100% !important;
	}
}
@media (max-width: 975px) {
	.wizard-steps > li >a{
		padding:20px;
		height:85px;
		font-size:13px !important;
	}
}
@media (max-width: 769px) {
	.wizard-steps > li >a{
		padding:20px;
		height:85px;
		font-size:13px !important;
	}
	.wizard-steps > li >a >span{
		background-color:#006e73;
		border:1px solid #ffffff;
		width:24px;
		height:24px;
		display:inline-block !important;
		padding: 5px 7px 5px 7px;
	}

	.tab-title {
		font-size:20px;
	}
	.wizard-tabs ul > li > a:before, .wizard-tabs ul > li > a:after {
		border:none !important;
	}
	#main-content{
		padding-bottom:250px;
	}
}

@media (max-width: 450px) {
	.wizard-steps > li >a{
		text-align:center !important;
		padding:5px;
		font-size:10px !important;
	}

	.wizard-steps .active >a >span{
		background-color:#ffffff;
		border:1px solid #006e73;
		color:#006e73;
	}

	#brecumb_text{
		padding:5px 0px 5px 10px;
	}

}

</style>


@section('content')
<div id="main-content" class="mdl-layout__content" style="width:100%;background:#ebf3ef;">
    <div id="brecumb_text" style="background: #9fc3ae;font-size: 16px;">
        <span><a href="/" style="color:#d5e7dc;line-height: 40px;text-decoration: none">{!! trans('order.imprevo') !!}</a></span>
        <span style="color:#d5e7dc;line-height: 40px">/</span>
        <span style="color:#fff;line-height: 40px">{!! trans('order.shop') !!}</span>
    </div>
	<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="justify-content:center;min-height:550px;">
		<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:50px;">
			<section class="panel form-wizard" id="w1" style="width:100%">
				<div class="panel-body panel-body-nopadding">
					<div class="wizard-tabs">
						<ul id="tab-desktop" class="wizard-steps mdl-cell--hide-phone mdl-cell--hide-tablet">
							<li id="w1-tab1">
								<a href="#w1-step1" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">1</span>
									{!! trans('order.register') !!}
								</a>
							</li>
							<li id="w1-tab2"  class="active">
								<a href="#w1-step2" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">2</span>
									{!! trans('order.span1') !!}
								</a>
							</li>
							<li  id="w1-tab3">
								<a href="#w1-step3" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">3</span>
									{!! trans('order.span3') !!}
								</a>
							</li>
							<li id="w1-tab4">
								<a href="#w1-step4" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">4</span>
									{!! trans('order.span4') !!}
								</a>
							</li>
						</ul>
						<ul class="wizard-steps mdl-cell--hide-desktop">
							<li id="w1-tab1" class="active">
								<a href="#w1-step1" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">1</span><br>
									{!! trans('order.register') !!}
								</a>
							</li>
							<li id="w1-tab2">
								<a href="#w1-step2" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">2</span><br>
									{!! trans('order.span1') !!}
								</a>
							</li>
							<li  id="w1-tab3">
								<a href="#w1-step3" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">3</span><br>
									{!! trans('order.span3') !!}
								</a>
							</li>
							<li id="w1-tab4">
								<a href="#w1-step4" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">4</span><br>
									{!! trans('order.span4') !!}
								</a>
							</li>
						</ul>
					</div>
						<div class="tab-content" style="padding:0;">
							<div id="w1-step2" class="tab-pane active">
								<form id="billing-personal-form" name="billing-personal-form" role="form" class="form-horizontal" action="/order/billinginfo" method="post" encType="multipart/form-data">
									{{ csrf_field() }}
									<input type="hidden" name="productstr" value="{{$productstr}}"/>
									<div id="billing-personal-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="justify-content:center;">
										<div class="divtitle mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="justify-content:center;text-align:center; padding:50px 0px 50px 0px">
											<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop " style="">
												<span class="tab-title" style="color:#000000;line-height:150%;">{!! trans('order.span18') !!}</span>
											</div>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--12-col" style="margin:0px; height:1px;background-color:#eeeeee"></div>
										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="justify-content:center">
											<div class="mdl-cell mdl-cell mdl-cell--6-col" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="text" id="name" name="name"  @if ($user) @if ($user->name != $user->email) value="{{$user->name}}" @endif @endif style="" autofocus>
														<label class="mdl-textfield__label" for="name" style="font-weight:bold">{!! trans('order.span19') !!}</label>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:0px">
													<a onclick="goCompanyInfo()" style="color:#399aed; cursor:pointer">{!! trans('order.span20') !!}</a>
												</div>

												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth getmdl-select__fix-height" style="width:100%">
														<input class="mdl-textfield__input" type="text" id="country" readonly>
														<input type="hidden" name="country" value=" @if ($user) @if ($user->country) {{$user->country}} @endif @endif"/>
														<i class="mdl-icon-toggle__label material-icons" >keyboard_arrow_down</i>
														<label for="country" class="mdl-textfield__label">{!! trans('order.span21') !!}</label>
														<ul for="country" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
															@foreach ($countrylist as $country => $region)
																<li class="mdl-menu__item" data-val="{{$country}}" @if ($user) @if ($user->country) @if ($user->country == $country) data-selected="true" @endif @else @if ($country == "Hungary") data-selected="true" @endif @endif @endif
																 >{{$country}}</li>
															@endforeach
														</ul>
													</div>
												</div>

												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px;padding-top:20px">
													<div class="mdl-cell mdl-cell mdl-cell--8-col" style="margin:0;padding-right:20px;">
														<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
															<input class="mdl-textfield__input" type="text" id="postcode" name="postcode"  @if ($user) value="{{$user->zipcode}}" @endif style="" autofocus>
															<label class="mdl-textfield__label" for="postcode" style="font-weight:bold">{!! trans('order.span22') !!}</label>
														</div>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--4-col" style="margin:0">
														<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
															<input class="mdl-textfield__input" type="text" id="city" name="city" @if ($user) value="{{$user->city}}" @endif style="">
															<label class="mdl-textfield__label" for="city" style="font-weight:bold">{!! trans('order.span23') !!}</label>
														</div>
													</div>
												</div>

												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0px;padding-top:20px">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="text" id="street_address" name="street_address"  @if ($user) value="{{$user->street_address}}" @endif>
														<label class="mdl-textfield__label" for="street_address" style="font-weight:bold">{!! trans('order.span24') !!}</label>
													</div>
												</div>

												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0; padding-top:20px;padding-bottom:50px">
													<div class="mdl-cell mdl-cell mdl-cell--7-col" style="">
														<span id="GoPAYMENT" class="GoPAYMENT next mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
															{!! trans('order.span25') !!}
														</span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--5-col" style="padding:10px">
														<a onclick="goBack()" style="color:#979797;cursor:pointer">{!! trans('order.span26') !!}</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
								<form id="billing-company-form" name="billing-company-form" role="form" class="form-horizontal" action="/order/billinginfo" method="post" encType="multipart/form-data">
									{{ csrf_field() }}
									<input type="hidden" name="productstr" value="{{$productstr}}"/>
									<div id="billing-company-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="display:none;justify-content:center;">
										<div class="divtitle mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;text-align:center; padding:50px 0px 50px 0px">
											<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="">
												<span class="tab-title" style="font-size:25px; color:#000000; line-height:150%;">{!! trans('order.span27') !!}</span>
											</div>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--12-col" style="margin:0px; height:1px;background-color:#eeeeee"></div>
										<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
											<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="text" id="company-name" name="company-name"  @if ($user) value="{{$user->company}}" @endif style="" autofocus>
														<label class="mdl-textfield__label" for="company-name" style="font-weight:bold">{!! trans('order.span28') !!}</label>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:0px">
													<a onclick="goPersonalInfo()" style="color:#399aed;cursor:pointer">{!! trans('order.span29') !!}</a>
												</div>

												<div id="tax-div" class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="text" id="tax_num" name="tax_num"  @if($user) @if ($user->tax_num) value="{{$user->tax_num}}" @else value="" @endif @endif style="">
														<label class="mdl-textfield__label" for="tax_num" style="font-weight:bold">{!! trans('order.span30') !!}</label>
													</div>
												</div>

												<div id="vat-div" class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="text" id="eu_vat_num" name="eu_vat_num"  @if($user) @if ($user->eu_vat_num) value="{{$user->eu_vat_num}}" @else value="" @endif @endif style="">
														<label class="mdl-textfield__label" for="eu_vat_num" style="font-weight:bold">{!! trans('order.span31') !!}</label>
													</div>
												</div>



													<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
														<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth getmdl-select__fix-height" style="width:100%">
															<input class="mdl-textfield__input" type="text" id="company-country" readonly onchange="OnCountry()">
															<input type="hidden" name="company-country" value=""/>
															<label for="company-country" style="float:right">
																<i class="mdl-icon-toggle__label material-icons" >keyboard_arrow_down</i>
															</label>
															<label for="company-country" class="mdl-textfield__label">{!! trans('order.span21') !!}</label>
															<ul for="country" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
																@foreach ($countrylist as $country => $region)
																<li class="mdl-menu__item" data-val="{{$country}}" @if ($user) @if ($user->country) @if ($user->country == $country) data-selected="true" @endif @else @if ($country == "Hungary") data-selected="true" @endif @endif @endif
																 >{{$country}}</li>
																@endforeach
															</ul>
														</div>
													</div>


												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px;padding-top:20px">
													<div class="mdl-cell mdl-cell mdl-cell--8-col" style="margin:0">
														<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px;padding-right:20px;">
															<input class="mdl-textfield__input" type="text" id="company-postcode" name="company-postcode"  @if($user) value="{{$user->zipcode}}" @endif style="" autofocus>
															<label class="mdl-textfield__label" for="company-postcode" style="font-weight:bold">{!! trans('order.span22') !!}</label>
														</div>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--4-col" style="margin:0">
														<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
															<input class="mdl-textfield__input" type="text" id="company-city" name="company-city"  @if ($user) value="{{$user->city}}" @endif style="">
															<label class="mdl-textfield__label" for="city" style="font-weight:bold">{!! trans('order.span23') !!}</label>
														</div>
													</div>
												</div>

												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0px;padding-top:20px">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="text" id="company-street_address" name="company-street_address"  @if($user) value="{{$user->street_address}}" @endif>
														<label class="mdl-textfield__label" for="company-street_address" style="font-weight:bold">{!! trans('order.span24') !!}</label>
													</div>
												</div>

												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0; padding-top:20px;padding-bottom:50px">
													<div class="mdl-cell mdl-cell mdl-cell--7-col" style="">
														<span id="GoPAYMENT" class="GoPAYMENT mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
															{!! trans('order.span25') !!}
														</span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--5-col" style="padding:10px">
														<a onclick="selectTab(1)" style="color:#979797;cursor:pointer">{!! trans('order.span26') !!}</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
								<form id="form-continue" name="form-continue" action="/order/payments" role="form" class="form-horizontal" method="post" encType="multipart/form-data">
									<input type="hidden" id="tax_num" name="tax_num" value=""/>
									<input type="hidden" id="is_valid_vat" name="is_valid_vat" value=""/>
									<input type="hidden" id="eu_vat_num" name="eu_vat_num" value=""/>
									<input type="hidden" name="productstr" value="{{$productstr}}"/>
								</form>
							</div>
						</div>
				</div>
			</section>
		</div>
	</div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script type="text/javascript">

function isMobileDevice() {
	return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

if (!isMobileDevice) {
	$('#tab-desktop').remove();
}


var countries = <?php echo json_encode($countrylist) ?>;
var currentTab = 1;
var EuropeUnionlist = ['Austria', 'Italy',
'Belgium',	'Latvia',
'Bulgaria',	'Lithuania',
'Croatia',	'Luxembourg',
'Cyprus',	'Malta',
'Czechia',	'Netherlands',
'Denmark',	'Poland',
'Estonia',	'Portugal',
'Finland',	'Romania',
'France',	'Slovakia',
'Germany'	,'Slovenia',
'Greece',	'Spain',
'Hungary'	,'Sweden',
'Ireland',	'United Kingdom'];

var notEuropeUnionlist = ['Albania', 'Andorra', 'Bosnia and Herzegovina', 'Liechtenstein', 'Norway'];
var total = 0;
@foreach ($products as $product)
	@if (!is_null($product->sale_price))
		each_price = parseFloat("{{$product->sale_price}}");
	@else
		each_price =  parseFloat("{{$product->regular_price}}");
	@endif
	total = total + each_price;
@endforeach




$(document).ready(function () {
  //your code here
	$('.GoPAYMENT').click(function(){
		if (document.getElementById('billing-company-div').style.display == 'none')
		{
			var c = document.getElementById('billing-personal-div').getElementsByTagName("*");
			for (i = 0; i < c.length; i++) {
				if (c[i].tagName == "INPUT" || c[i].tagName == "SELECT")
				{
					if (!c[i].value)
					{
						if (c[i].id == 'tax_num' || c[i].id == 'eu_vat_num')
							continue;
						new PNotify({
							title: 'Empty fields!',
							type: 'warning',
							delay: 3500,
							hide: true,
							sticker:false,
							icon: false,
							addclass: 'ui-pnotify-no-icon',
						});
						c[i].focus();
						return false;
					}
				}
			}

			$.ajax({ // create an AJAX call...
				data: $('#billing-personal-form').serialize(), // get the form data
				type: $('#billing-personal-form').attr('method'), // GET or POST
				url: $('#billing-personal-form').attr('action'), // the file to call
				success: function(response) { // on success..
					country_name = document.getElementById('country').value;
					var region = countries[country_name];

					//if (region == "Europe" && notEuropeUnionlist.indexOf(country_name) == -1)
					if (EuropeUnionlist.indexOf(country_name) != -1)
					{
						document.getElementById('is_valid_vat').value = 0;
					}
					else
					{
						document.getElementById('is_valid_vat').value = -1;
					}

					document.getElementById('company-name').value = '';
					document.getElementById('form-continue').submit();
				}
			});
		} else {

			var c = document.getElementById('billing-company-div').getElementsByTagName("*");

			for (i = 0; i < c.length; i++) {
				if (c[i].tagName == "INPUT" || c[i].tagName == "SELECT")
				{
					if (!c[i].value)
					{
						if (c[i].id == 'tax_num' || c[i].id == 'eu_vat_num')
								continue;
						new PNotify({
							title: 'Empty fields!',
							type: 'warning',
							delay: 3500,
							hide: true,
							sticker:false,
							icon: false,
							addclass: 'ui-pnotify-no-icon',
						});
						c[i].focus();
						return false;
					}
				}
			}
			console.log('clicked');
			$.ajax({ // create an AJAX call...
				data: $('#billing-company-form').serialize(), // get the form data
				type: $('#billing-company-form').attr('method'), // GET or POST
				url: $('#billing-company-form').attr('action'), // the file to call
				success: function(response) { // on success..
					console.log('success');
					country_name = document.getElementById('company-country').value;
					var region = countries[country_name];

					if (EuropeUnionlist.indexOf(country_name) != -1 && country_name != "Hungary")
					{
						if (response.is_valid_vat == 1)
						{
							PNotify.removeAll();
							new PNotify({
								title: 'Your Eu Vat number is valid.',
								type: 'success',
								delay: 3500,
								hide: true,
								sticker:false,
								icon: false,
								addclass: 'ui-pnotify-no-icon',
							});
						}
						else
						{
							PNotify.removeAll();
							new PNotify({
								text: 'Your Eu Vat number is not valid.',
								type: 'error',
								delay: 3500,
								icon: false,
								hide: true,
								sticker:false,
								addclass: 'ui-pnotify-no-icon',
							});
						}
						document.getElementById('tax_num').value = '';
						document.getElementById('is_valid_vat').value = response.is_valid_vat;
					}
					else if (country_name == "Hungary")
					{
						document.getElementById('is_valid_vat').value = 0;
						document.getElementById('eu_vat_num').value = '';
					}
					else
					{
						document.getElementById('is_valid_vat').value = -1;
						document.getElementById('eu_vat_num').value = '';
					}
					document.getElementById('form-continue').submit();
				}
			});
		}
	})

	$('.accordion .item .heading').click(function() {

		var a = $(this).closest('.item');
		var b = $(a).hasClass('open');
		var c = $(a).closest('.accordion').find('.open');

		if(b != true) {
			$(c).find('.content').slideUp(200);
			$(c).removeClass('open');
		}

		$(a).toggleClass('open');
		$(a).find('.content').slideToggle(200);

	});

	OnCountry();

});



function goCompanyInfo()
{
	document.getElementById('billing-personal-div').style.display = 'none';
	document.getElementById('billing-company-div').style.display = 'inline';
}

function goPersonalInfo()
{
	document.getElementById('billing-personal-div').style.display = 'inline';
	document.getElementById('billing-company-div').style.display = 'none';
}

function OnCountry()
{
	country_name = document.getElementById('company-country').value;
	var region = countries[country_name];

	//if (region == "Europe" && country_name != "Hungary" && notEuropeUnionlist.indexOf(country_name) == -1)
	if (EuropeUnionlist.indexOf(country_name) != -1 && country_name != "Hungary")
	{
		document.getElementById('tax-div').style.display='none';
		document.getElementById('vat-div').style.display='inline';
	}
	else
	{
		document.getElementById('tax-div').style.display='inline';
		document.getElementById('vat-div').style.display='none';
	}
}

function goBack(){
	location.href = "/order?back=1";
}

</script>
@endsection
