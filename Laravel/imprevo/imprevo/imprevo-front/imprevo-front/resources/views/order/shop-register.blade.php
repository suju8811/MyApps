@extends('layouts.app3')
<style>

.wizard-steps > li >a{
	padding:20px;
	height:70px;
}
.wizard-steps > li >a >span{
	background-color:#006e73;
	border:1px solid #ffffff;
	width:24px;
	height:24px;
	padding: 5px 7px 5px 7px;
}
.wizard-steps .active >a >span{
	background-color:#ffffff;
	border:1px solid #006e73;
	color:#006e73;
}
.is-focused .mdl-textfield__input {
	border:none;
}

div.mdl-textfield.is-dirty {
   padding-right:25px !important;
   background: url('/images/tick1.png') no-repeat !important;
   background-position: right 0px top 25px !important;
}

div.mdl-textfield.is-dirty.is-invalid{
   background: none !important;
}

div.mdl-textfield_password{
   background: none !important;
}

.accordion {
  width: 380px;
  border-radius: 5px;
  overflow: hidden;
  margin: auto;
}

.accordion .item .heading {
  height: 50px;
  line-height: 50px;
  font-size: 17px;
  cursor: pointer;
  color: #000000;
  padding-left: 15px;
  background: #f9f9f9 url('/images/order_accordion_arrow.png') no-repeat;
  background-position: right 20px top -95px;
  border: 1px solid #eee;
  box-sizing: border-box;
}

.accordion .item.open .heading,
.accordion .item:last-child .heading { border: 0; }

.accordion .item.open .heading { background-position: right 20px top -5px; }

.accordion .item .content {
  display: none;
  padding: 15px;
  background: #f9f9f9;
  font-size: 14px;
}



#GoBill ,#GoPAYMENT ,#GoConfirm ,#GoPay ,#GoBill-register, #GoHome{
	background-color:#399aed;
}

#GoBill:hover, ##GoBill-register:hover, GoHome:hover{
	background-color:#2081d5;
}

#GoBill-register{
	background-color:#2081d5;
}

#GoBill-register.disabled{
	background-color:#eee;
}

#GoPAYMENT:hover{
	background-color:#2081d5;
}

#GoConfirm:hover{
	background-color:#2081d5;
}

#GoPay:hover{
	background-color:#2081d5;
}

#tooltip-barion-img{
	position:relative;
}

.mdl-tooltip
{
	padding:10px;
	max-width:500px !important;
	width:400px;
	background-color:#9fc3ae;
}
.mdl-tooltip.is-active {
	will-change: unset;
	background-color:#9fc3ae;
	text-align:center;
	color:#ffffff;
}

.mdl-tooltip.is-active:before{
	content: ' ';
	position: absolute;
	left: 200px;
	top: -7px;
	width: 14px;
	height: 14px;
	background-color: #9fc3ae;
	border-top: 1px solid #9fc3ae;
	border-left: 1px solid #9fc3ae;
	border-bottom: none;
	border-right: none;
	transform: rotate(45deg);
	-webkit-transform: rotate(45deg);
	-moz-transform: rotate(45deg);
}

#brecumb_text{
	padding:5px 40px 5px 80px;
}
#main-content{
	padding-bottom:150px;
}

.tab-title {
	font-size:25px;
}

@media (max-width:1500px)
{
	.confirm-content{
		width: 80% !important;
	}
}

@media (max-width: 1380px) {
	.wizard-steps > li >a{
		padding:20px;
		padding-top:26px;
		height:85px;
		font-size:14px !important;
	}
	.wizard-tabs ul > li > a:before, .wizard-tabs ul > li > a:after {
		border-bottom: 42px solid transparent !important;
		border-top: 42px solid transparent !important;
	}

	.divtitle {
		padding:20px 0px 20px 0px !important;
	}
}
@media (max-width:1240px)
{
	.confirm-content{
		width: 90% !important;
	}
}
@media (max-width:1080px)
{
	.confirm-content{
		width: 100% !important;
	}
}
@media (max-width: 975px) {
	.wizard-steps > li >a{
		padding:20px;
		height:85px;
		font-size:13px !important;
	}
}
@media (max-width: 769px) {
	.wizard-steps > li >a{
		padding:20px;
		height:85px;
		font-size:13px !important;
	}
	.wizard-steps > li >a >span{
		background-color:#006e73;
		border:1px solid #ffffff;
		width:24px;
		height:24px;
		display:inline-block !important;
		padding: 5px 7px 5px 7px;
	}

	.tab-title {
		font-size:20px;
	}
	.wizard-tabs ul > li > a:before, .wizard-tabs ul > li > a:after {
		border:none !important;
	}
	#main-content{
		padding-bottom:250px;
	}
}

@media (max-width: 450px) {
	.wizard-steps > li >a{
		text-align:center !important;
		padding:5px;
		font-size:10px !important;
	}

	.wizard-steps .active >a >span{
		background-color:#ffffff;
		border:1px solid #006e73;
		color:#006e73;
	}

	#brecumb_text{
		padding:5px 0px 5px 10px;
	}

}

</style>


@section('content')
<div id="main-content" class="mdl-layout__content" style="width:100%;background:#ebf3ef;">
    <div id="brecumb_text" style="background: #9fc3ae;font-size: 16px;">
        <span><a href="/" style="color:#d5e7dc;line-height: 40px;text-decoration: none">{!! trans('order.imprevo') !!}</a></span>
        <span style="color:#d5e7dc;line-height: 40px">/</span>
        <span style="color:#fff;line-height: 40px">{!! trans('order.shop') !!}</span>
    </div>
	<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="justify-content:center;min-height:550px;">
		<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:50px;">
			<section class="panel form-wizard" id="w1" style="width:100%">
				<div class="panel-body panel-body-nopadding">
					<div class="wizard-tabs">
						<ul id="tab-desktop" class="wizard-steps mdl-cell--hide-phone mdl-cell--hide-tablet">
							<li id="w1-tab1" class="active">
								<a href="#w1-step1" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">1</span>
									{!! trans('order.register') !!}
								</a>
							</li>
							<li id="w1-tab2">
								<a href="#w1-step2" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">2</span>
									{!! trans('order.span1') !!}
								</a>
							</li>
							<li  id="w1-tab3">
								<a href="#w1-step3" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">3</span>
									{!! trans('order.span3') !!}
								</a>
							</li>
							<li id="w1-tab4">
								<a href="#w1-step4" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">4</span>
									{!! trans('order.span4') !!}
								</a>
							</li>
						</ul>
					</div>
						<div class="tab-content" style="padding:0;">
							<div id="w1-step1" class="tab-pane active" >
								<form id="order-login-form" name="order-login-form" role="form" class="form-horizontal" action="/order/billing1" method="post" encType="multipart/form-data">
									{{ csrf_field() }}
									<input type="hidden" name="productstr" value="{{$productstr}}"/>
									<div id="login-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="display:none;width:100%;margin:0;padding:0;justify-content:center;">
										<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center; padding:50px 0px 50px 0px">
											<span class="tab-title" style="color:#000000">{!! trans('order.span5') !!}</span>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="width:100%;margin:0;text-align:center; background-color:#f9f9f9; border:1px solid #eee; margin:0;padding:10px">
											<a onclick="goRegisterDiv()" style="cursor:pointer;font-size:14px; color:#399aed">{!! trans('order.span6') !!}</a>
										</div>
										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="justify-content:center">
											<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="email" id="login-email" name="login-email"  @if ($user) value="{{$user->email}}" @endif style="" autofocus>
														<label class="mdl-textfield__label" for="login-email" style="font-weight:bold">{!! trans('order.span7') !!}</label>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="password" id="login-password" name="login-password" pattern=".{5,}">
														<label class="mdl-textfield__label" for="login-password" style="font-weight:bold">{!! trans('order.span8') !!}</label>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<a href="{{ url('/password/reset') }}" style="color:#399aed;cursor:pointer">{!! trans('order.span9') !!}</a>
												</div>
												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0; padding-top:20px;padding-bottom:50px">
													<div class="mdl-cell mdl-cell mdl-cell--7-col" style="">
														<a id="GoBill" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
															{!! trans('order.span10') !!}
														</a>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--5-col" style="padding:7px">
														<a href="/shoppingcart?products={{$productstr}}" style="color:#979797">{!! trans('order.span11') !!}</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
								<form id="order-register-form" name="order-login-form" role="form" class="form-horizontal" action="/order/billing2" method="post" encType="multipart/form-data">
									<input type="hidden" name="productstr" value="{{$productstr}}"/>
									{{ csrf_field() }}
									<div id="register-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-grid--no-spacing" style="display:inline;justify-content:center;padding:0;">
										<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center; padding:50px 0px 50px 0px">
											<span class="tab-title" style="color:#000000">{!! trans('order.span12') !!}</span>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center; background-color:#f9f9f9; border:1px solid #eee; margin:0;padding:10px">
											<a onclick="goLoginDiv()" style="cursor:pointer;font-size:14px; color:#399aed">{!! trans('order.span13') !!}</a>
										</div>
										@if ($errors->has('email'))
										<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="margin:0;margin-top:10px;width:100%;padding:10px;border-top:1px solid #eee; border-bottom:1px solid #eee;background-color:#ffffff;text-align:center;color:#ff0000">
											<span>{{ $errors->first('email') }}</span>
										</div>
										@endif
										@if ($errors->has('password'))
										<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="margin:0;margin-top:10px;width:100%;padding:10px;background-color:#ffffff;text-align:center;color:#ff0000">
											<span>{{ $errors->first('password') }}</span>
										</div>
										@endif
										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="justify-content:center">
											<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="email" id="register-email" name="register-email"  @if ($user) value="{{$user->email}}" @endif style="" autofocus>
														<label class="mdl-textfield__label" for="register-email" style="font-weight:bold">{!! trans('order.span14') !!}</label>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="password" id="register-password" name="register-password"  style="" pattern=".{5,}">
														<label class="mdl-textfield__label" for="register-password" style="font-weight:bold">{!! trans('order.span15') !!}</label>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:0px">
													<span style="">Minimum 5 karakter</span>
												</div>
												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding-top:20px">
													<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="agreeForService">
														<input type="checkbox" id="agreeForService" class="mdl-checkbox__input" onclick="toggleAgree()">
														<span>{!! trans('order.span65') !!} <a href="/aszf" target="_blank">{!! trans('order.span16') !!}</a>et!</span>
													</label>
												</div>
												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0; padding-top:20px;padding-bottom:50px">
													<div class="mdl-cell mdl-cell mdl-cell--7-col" style="">
														<span id="GoBill-register" class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
															{!! trans('order.span10') !!}
														</span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--5-col" style="padding:7px;">
														<a href="/shoppingcart?products={{$productstr}}" style="color:#979797">{!! trans('order.span17') !!}</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
				</div>
			</section>
		</div>
	</div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script type="text/javascript">
function isMobileDevice() {
	return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

if (!isMobileDevice) {
	$('#tab-desktop').remove();
}


var countries = <?php echo json_encode($countrylist) ?>;
var currentTab = 1;
var EuropeUnionlist = ['Austria', 'Italy',
'Belgium',	'Latvia',
'Bulgaria',	'Lithuania',
'Croatia',	'Luxembourg',
'Cyprus',	'Malta',
'Czechia',	'Netherlands',
'Denmark',	'Poland',
'Estonia',	'Portugal',
'Finland',	'Romania',
'France',	'Slovakia',
'Germany'	,'Slovenia',
'Greece',	'Spain',
'Hungary'	,'Sweden',
'Ireland',	'United Kingdom'];

var notEuropeUnionlist = ['Albania', 'Andorra', 'Bosnia and Herzegovina', 'Liechtenstein', 'Norway'];
var total = 0;
@foreach ($products as $product)
	@if (!is_null($product->sale_price))
		each_price = parseFloat("{{$product->sale_price}}");
	@else
		each_price =  parseFloat("{{$product->regular_price}}");
	@endif
	total = total + each_price;
@endforeach



var bnext = false;
$(document).ready(function () {
  //your code here

	$('#GoBill').click(function(){
		email = document.getElementById('login-email');
		password = document.getElementById('login-password');

		if (!email.value)
		{
			new PNotify({
				title: 'Email empty!.',
				type: 'warning',
				delay: 3500,
				hide: true,
				sticker:false,
				icon: false,
				addclass: 'ui-pnotify-no-icon',
			});
			email.focus();
			return false;
		}

		if (!password.value || password.value.length < 5)
		{
			new PNotify({
				title: 'Please input password!<br>(at least 5 characters)',
				type: 'warning',
				delay: 3500,
				hide: true,
				sticker:false,
				icon: false,
				addclass: 'ui-pnotify-no-icon',
			});
			password.focus();
			return false;
		}
		document.getElementById('order-login-form').submit();
	})

	$('#GoBill-register').click(function(){
		email = document.getElementById('register-email');
		password = document.getElementById('register-password');
		agreeForService = document.getElementById('agreeForService');

		if (!email.value)
		{
			email.focus();
		}

		if (!password.value)
		{
			password.focus();
		}

		if (!password.value || !email.value)
		{
			PNotify.removeAll();
			new PNotify({
				title: 'Please fill input fields.',
				type: 'warning',
				delay: 3500,
				hide: true,
				sticker:false,
				icon: false,
				addclass: 'ui-pnotify-no-icon',
			});
			return false;
		}

		if (!agreeForService.checked)
		{
			new PNotify({
				title: 'Please check that you accept the terms and conditions!',
				type: 'warning',
				delay: 3500,
				hide: true,
				sticker:false,
				icon: false,
				addclass: 'ui-pnotify-no-icon',
			});
			agreeForService.focus();
			return false;
		}
		document.getElementById('order-register-form').submit();
	})


	$('.accordion .item .heading').click(function() {

		var a = $(this).closest('.item');
		var b = $(a).hasClass('open');
		var c = $(a).closest('.accordion').find('.open');

		if(b != true) {
			$(c).find('.content').slideUp(200);
			$(c).removeClass('open');
		}

		$(a).toggleClass('open');
		$(a).find('.content').slideToggle(200);

	});

});


function goRegisterDiv(){
	document.getElementById('register-div').style.display = 'inline';
	document.getElementById('login-div').style.display = 'none';
}

function goLoginDiv(){
	document.getElementById('register-div').style.display = 'none';
	document.getElementById('login-div').style.display = 'inline';
}

function toggleAgree() {
  var isAgree = document.getElementById("agreeForService").checked
}

</script>
@endsection
