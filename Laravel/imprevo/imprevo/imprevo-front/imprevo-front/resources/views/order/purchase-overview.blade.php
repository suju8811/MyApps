@extends('layouts.app3')
<style>

.wizard-steps > li >a{
	padding:20px;
	height:70px;
}
.wizard-steps > li >a >span{
	background-color:#006e73;
	border:1px solid #ffffff;
	width:24px;
	height:24px;
	padding: 5px 7px 5px 7px;
}
.wizard-steps .active >a >span{
	background-color:#ffffff;
	border:1px solid #006e73;
	color:#006e73;
}
.is-focused .mdl-textfield__input {
	border:none;
}

div.mdl-textfield.is-dirty {
   padding-right:25px !important;
   background: url('/images/tick1.png') no-repeat !important;
   background-position: right 0px top 25px !important;
}

div.mdl-textfield.is-dirty.is-invalid{
   background: none !important;
}

div.mdl-textfield_password{
   background: none !important;
}

.accordion {
  width: 380px;
  border-radius: 5px;
  overflow: hidden;
  margin: auto;
}

.accordion .item .heading {
  height: 50px;
  line-height: 50px;
  font-size: 17px;
  cursor: pointer;
  color: #000000;
  padding-left: 15px;
  background: #f9f9f9 url('/images/order_accordion_arrow.png') no-repeat;
  background-position: right 20px top -95px;
  border: 1px solid #eee;
  box-sizing: border-box;
}

.accordion .item.open .heading,
.accordion .item:last-child .heading { border: 0; }

.accordion .item.open .heading { background-position: right 20px top -5px; }

.accordion .item .content {
  display: none;
  padding: 15px;
  background: #f9f9f9;
  font-size: 14px;
}



#GoBill ,#GoPAYMENT ,#GoConfirm ,#GoPay ,#GoBill-register, #GoHome{
	background-color:#399aed;
}

#GoBill:hover, ##GoBill-register:hover, GoHome:hover{
	background-color:#2081d5;
}

#GoBill-register{
	background-color:#2081d5;
}

#GoBill-register.disabled{
	background-color:#eee;
}

#GoPAYMENT:hover{
	background-color:#2081d5;
}

#GoConfirm:hover{
	background-color:#2081d5;
}

#GoPay:hover{
	background-color:#2081d5;
}

#tooltip-barion-img{
	position:relative;
}

.mdl-tooltip
{
	padding:10px;
	max-width:500px !important;
	width:400px;
	background-color:#9fc3ae;
}
.mdl-tooltip.is-active {
	will-change: unset;
	background-color:#9fc3ae;
	text-align:center;
	color:#ffffff;
}

.mdl-tooltip.is-active:before{
	content: ' ';
	position: absolute;
	left: 200px;
	top: -7px;
	width: 14px;
	height: 14px;
	background-color: #9fc3ae;
	border-top: 1px solid #9fc3ae;
	border-left: 1px solid #9fc3ae;
	border-bottom: none;
	border-right: none;
	transform: rotate(45deg);
	-webkit-transform: rotate(45deg);
	-moz-transform: rotate(45deg);
}

#brecumb_text{
	padding:5px 40px 5px 80px;
}
#main-content{
	padding-bottom:150px;
}

.tab-title {
	font-size:25px;
}

@media (max-width:1500px)
{
	.confirm-content{
		width: 80% !important;
	}
}

@media (max-width: 1380px) {
	.wizard-steps > li >a{
		padding:20px;
		padding-top:26px;
		height:85px;
		font-size:14px !important;
	}
	.wizard-tabs ul > li > a:before, .wizard-tabs ul > li > a:after {
		border-bottom: 42px solid transparent !important;
		border-top: 42px solid transparent !important;
	}

	.divtitle {
		padding:20px 0px 20px 0px !important;
	}
}
@media (max-width:1240px)
{
	.confirm-content{
		width: 90% !important;
	}
}
@media (max-width:1080px)
{
	.confirm-content{
		width: 100% !important;
	}
}
@media (max-width: 975px) {
	.wizard-steps > li >a{
		padding:20px;
		height:85px;
		font-size:13px !important;
	}
}
@media (max-width: 769px) {
	.wizard-steps > li >a{
		padding:20px;
		height:85px;
		font-size:13px !important;
	}
	.wizard-steps > li >a >span{
		background-color:#006e73;
		border:1px solid #ffffff;
		width:24px;
		height:24px;
		display:inline-block !important;
		padding: 5px 7px 5px 7px;
	}

	.tab-title {
		font-size:20px;
	}
	.wizard-tabs ul > li > a:before, .wizard-tabs ul > li > a:after {
		border:none !important;
	}
	#main-content{
		padding-bottom:250px;
	}
}

@media (max-width: 450px) {
	.wizard-steps > li >a{
		text-align:center !important;
		padding:5px;
		font-size:10px !important;
	}

	.wizard-steps .active >a >span{
		background-color:#ffffff;
		border:1px solid #006e73;
		color:#006e73;
	}

	#brecumb_text{
		padding:5px 0px 5px 10px;
	}

}

</style>


@section('content')
<div id="main-content" class="mdl-layout__content" style="width:100%;background:#ebf3ef;">
    <div id="brecumb_text" style="background: #9fc3ae;font-size: 16px;">
        <span><a href="/" style="color:#d5e7dc;line-height: 40px;text-decoration: none">{!! trans('order.imprevo') !!}</a></span>
        <span style="color:#d5e7dc;line-height: 40px">/</span>
        <span style="color:#fff;line-height: 40px">{!! trans('order.shop') !!}</span>
    </div>
	<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="justify-content:center;min-height:550px;">
		<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:50px;">
			<section class="panel form-wizard" id="w1" style="width:100%">
				<div class="panel-body panel-body-nopadding">
					<div class="wizard-tabs">
						<ul id="tab-desktop" class="wizard-steps mdl-cell--hide-phone mdl-cell--hide-tablet">
							<li id="w1-tab1">
								<a href="#w1-step1" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">1</span>
									{!! trans('order.register') !!}
								</a>
							</li>
							<li id="w1-tab2">
								<a href="#w1-step2" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">2</span>
									{!! trans('order.span1') !!}
								</a>
							</li>
							<li  id="w1-tab3">
								<a href="#w1-step3" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">3</span>
									{!! trans('order.span3') !!}
								</a>
							</li>
							<li id="w1-tab4"  class="active">
								<a href="#w1-step4" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">4</span>
									{!! trans('order.span4') !!}
								</a>
							</li>
						</ul>
						<ul class="wizard-steps mdl-cell--hide-desktop">
							<li id="w1-tab1">
								<a href="#w1-step1" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">1</span><br>
									{!! trans('order.register') !!}
								</a>
							</li>
							<li id="w1-tab2">
								<a href="#w1-step2" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">2</span><br>
									{!! trans('order.span1') !!}
								</a>
							</li>
							<li  id="w1-tab3">
								<a href="#w1-step3" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">3</span><br>
									{!! trans('order.span3') !!}
								</a>
							</li>
							<li id="w1-tab4"  class="active">
								<a href="#w1-step4" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">4</span><br>
									{!! trans('order.span4') !!}
								</a>
							</li>
						</ul>
					</div>
						<div class="tab-content" style="padding:0;">
							<div id="w1-step4" class="tab-pane active">

								<div id="confirmation-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;">
									<div class="divtitle mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding:50px 0px 50px 0px; border-bottom:1px solid #eee">
										<span class="tab-title" style="font-size:25px; color:#000000">{!! trans('order.span38') !!}</span>
									</div>

									<div class="confirm-content mdl-cell mdl-cell mdl-cell--8-col" style="margin:0;border:1px solid #eee;padding:15px;">
										@foreach ($products as $product)
											<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="border-bottom:1px solid #eee;margin:0; background-color:#ffffff">
												<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone" style="padding:10px">
													<img src="{{$product->product_image}}" style="width:50px">
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--9-col mdl-cell--5-col-tablet mdl-cell--2-col-phone" style="padding:20px">
													<span style="font-size:16px; font-weight:bold">{{$product->title}}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="padding:20px;text-align:right">
													<span style="font-size:16px;">
													@if (!is_null($product->sale_price))
														@if ($shoppingcartsetting['currency'] == 'USD')
															$<?php echo number_format($product->sale_price) ?>
														@else
															<?php echo number_format($product->sale_price) ?> Ft
														@endif
													@else
														@if ($shoppingcartsetting['currency'] == 'USD')
															$<?php echo number_format($product->regular_price) ?>
														@else
															<?php echo number_format($product->regular_price) ?> Ft
														@endif
													@endif
													</span>
												</div>
											</div>
										@endforeach

										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing" style="margin:0;padding:15px;background-color:#ffffff">
											<div class="mdl-cell mdl-cell mdl-cell--7-col" style="">
											</div>
											<div class="mdl-grid mdl-cell mdl-cell--5-col mdl-grid--no-spacing" style="padding:0;">
												<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:right;padding:5">
													<span id="" style="font-size:16px; font-weight:bold">{!! trans('order.span39') !!}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:right;padding:5">
													<span id="total-subtotal" style="font-size:16px; font-weight:bold">141,515 Ft</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:right;padding:5">
													<span id="" style="font-size:16px; font-weight:bold">{!! trans('order.span40') !!}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:right;padding:5">
													<span id="total-tax" style="font-size:16px; font-weight:bold">141,515 Ft</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0;margin:0;padding:5">
													<hr style="margin:0; padding:0; height:1px; background-color:#000000; width:100%">
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:right;padding:5">
													<span id="" style="font-size:16px; font-weight:bold">{!! trans('order.span41') !!}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:right;padding:5">
													<span id="total-span" style="font-size:16px; font-weight:bold">141,515 Ft</span>
												</div>
											</div>
										</div>
										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing" style="margin:0; padding:15px;background-color:#ffffff">
											<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:right; ">

											</div>
										</div>
									</div>
									<div class="confirm-content mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;margin:0;border:1px solid #eee;border-top:none;padding:15px;">
										<div class="mdl-cell mdl-cell mdl-cell--7-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" style="text-align:right;">
											<span id="GoPay" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
												{!! trans('order.span42') !!}
											</span>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--5-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" style="text-align:left; padding: 10px;">
											<a href="/order?back=3" style="color:#979797;cursor:pointer">{!! trans('order.span43') !!}</a>
										</div>
									</div>
									<div class="confirm-content accordion mdl-cell mdl-cell mdl-cell--8-col" style="margin:0;">
										<div class="item">
											<div class="heading" style="border:1px solid #eee; ">{!! trans('order.span44') !!}</div>
											<div class="content" style="width:100%; border:1px solid #eee; border-top:none;border-bottom:none;margin:0;padding:0;">
												<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing" style="width:100%;margin:0;padding:0;">
												<div class="mdl-cell mdl-cell mdl-cell--4-col" style="margin:0; border:1px solid #eee; padding-left:20px">
													<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:10px">
														<span style="font-weight:bold; font-size:16px">{!! trans('order.span45') !!}</span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px">
														<span id="account-span" style="font-size:14px">dtommy79@freemail.hu</span>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--4-col" style="margin:0; border:1px solid #eee; padding-left:20px">
													<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:10px">
														<span style="font-weight:bold; font-size:16px">{!! trans('order.span46') !!}</span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px">
														<span id="billing-info-name" style="font-size:14px"></span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--12-col">
														<span id="billing-info-address" style="font-size:14px"></span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--12-col">
														<span id="billing-info-postcode" style="font-size:14px"></span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--12-col">
														<span id="billing-info-tax" style="font-size:14px"></span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--12-col">
														<span id="billing-info-vat" style="font-size:14px"></span>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--4-col" style="margin:0; border:1px solid #eee; padding-left:20px">
													<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:10px">
														<span style="font-weight:bold; font-size:16px">{!! trans('order.span47') !!}</span>
													</div>
													<div id="payment-img-div" class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px">
														<img src="/images/order-bank-transfer.png">
													</div>
												</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div id="result-div-success" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="display:none;justify-content:center;">
									<div class="divtitle mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding:50px 0px 50px 0px; border-bottom:1px solid #eee">
										<span class="tab-title" style="font-size:25px; color:#000000">{!! trans('order.span48') !!}</span>
									</div>
									<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:15px; text-align:center;">
											<img src="/images/order-result-successful-big-icon.png">
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:50px;text-align:center">
											<span style="color:#000000;font-size:16px; font-weight:bold">{!! trans('order.span49') !!}</span>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:50px;text-align:center;">
											<a id="GoHome" href="/home" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="width:200px;text-decoration:none">
												{!! trans('order.span50') !!}
											</a>
										</div>
									</div>
								</div>
								<div id="result-div-failed" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="display:none;justify-content:center;">
									<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding:50px 0px 50px 0px; border-bottom:1px solid #eee">
										<span class="tab-title" style="font-size:25px; color:#000000">{!! trans('order.span51') !!}</span>
									</div>
									<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:15px; text-align:center;">
											<img src="/images/payment-failed-big-icon.png">
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:50px;text-align:center;">
											<span style="color:#000000;font-size:16px; font-weight:bold">{!! trans('order.span52') !!}</span>
										</div>

										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:50px;text-align:center;">
											<a id="GoHome" href="#" onclick="selectTab(3)" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="text-decoration:none">
												{!! trans('order.span53') !!}
											</a>
										</div>
									</div>
								</div>
								<div id="result-div-pending" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="display:none;justify-content:center;">
									<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding:50px 0px 50px 0px; border-bottom:1px solid #eee">
										<span class="tab-title" style="font-size:25px; color:#000000">{!! trans('order.span48') !!}</span>
									</div>
									<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:15px; text-align:center;">
											<img src="/images/order-result-successful-big-icon.png">
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:15px; ">
											<ul>
												<li>{!! trans('order.span54') !!} {{Session::get('orderid')}} </li>
												<li>{!! trans('order.span55') !!}</li>
												<li>{!! trans('order.span56') !!}</li>
											</ul>
										</div>

										<div class="mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="background-color:#ebf3ef; margin:0;padding:15px; text-align:center;">
											<div class="mdl-cell mdl-cell mdl-cell--12-col">
												<span style="float:left">{!! trans('order.span57') !!}</span>
												<span style="float:right">Imprevo Kft.</span>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--12-col">
												<span style="float:left">{!! trans('order.span58') !!} </span>
												<span style="float:right">10402142-50526771-77801015</span>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--12-col">
												<span style="float:left">{!! trans('order.span59') !!}</span>
												<span style="float:right">{{Session::get('orderid')}}</span>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--12-col">
												<span style="float:left">{!! trans('order.span60') !!}</span>
												<span id="total-result-span" style="float:right">
													@if ($shoppingcartsetting['currency'] == 'USD')
														${{Session::get('total_price')}}
													@else
														{{Session::get('total_price')}} Ft
													@endif
												</span>
											</div>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:15px;">
											<ul>
												<li>{!! trans('order.span61') !!}</li>
											</ul>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:15px;text-align:center;">
											<a id="GoHome" href="/order?back=3" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="text-decoration:none">
												{!! trans('order.span62') !!}
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
			</section>
			<form id="payment-confirm-form" name="payment-confirm-form" role="form" class="form-horizontal" action="/payment" method="post" encType="multipart/form-data">
				{{ csrf_field() }}
				<input type="hidden" id="payment_input" value="{{$payment_input}}"/>
				<input type="hidden" id="is_valid_vat" name="is_valid_vat" value="{{$is_valid_vat}}"/>
				<input type="hidden" id="account" name="account" value="{{$account}}"/>
				<input type="hidden" id="selectProducts" name="selectProducts" value="{{$productstr}}"/>
			</form>
		</div>
	</div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script type="text/javascript">

function isMobileDevice() {
	return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

if (isMobileDevice) {
	$('#tab-desktop').remove();
}


function CommaFormatted(amount)
{
	var delimiter = ","; // replace comma if desired
	amount = amount + '';
	var a = amount.split('.',2)
	var d='';
	if (a[1])
		d = a[1];
	var i = parseInt(a[0]);
	if(isNaN(i)) { return ''; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while(n.length > 3)
	{
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
	}
	if(n.length > 0) { a.unshift(n); }
	n = a.join(delimiter);
	if(d.length < 1) { amount = n; }
	else { amount = n + '.' + d; }
	amount = minus + amount;
	return amount;
}

var total = 0;
@foreach ($products as $product)
	@if (!is_null($product->sale_price))
		each_price = parseFloat("{{$product->sale_price}}");
	@else
		each_price =  parseFloat("{{$product->regular_price}}");
	@endif
	total = total + each_price;
@endforeach

$(document).ready(function () {
	var  payment = document.getElementById('payment_input').value;
	var is_valid_vat = document.getElementById('is_valid_vat').value;
	var countries = <?php echo json_encode($countrylist) ?>;
	company = "{{$user->company}}";
	country_name = null;

	country_name = "{{$user->country}}";


	var region = countries[country_name];
	//console.log('is_valid_vat',is_valid_vat);
	if (is_valid_vat == -1)
	{
		@if ($shoppingcartsetting['currency'] == 'USD')
			document.getElementById('total-span').innerHTML = '$' + CommaFormatted(total.toFixed(2));
			document.getElementById('total-subtotal').innerHTML = '$' +  CommaFormatted(total.toFixed(2));
			document.getElementById('total-tax').innerHTML = '$0';
		@else
			document.getElementById('total-span').innerHTML =  CommaFormatted(total.toFixed(2)) + ' Ft';
			document.getElementById('total-subtotal').innerHTML =  CommaFormatted(total.toFixed(2)) + ' Ft';
			document.getElementById('total-tax').innerHTML = '0 Ft'
		@endif
	}
	else if (is_valid_vat == 0)
	{
			subtotal = total * 100 / 127;
			tax = total - subtotal;
			@if ($shoppingcartsetting['currency'] == 'USD')
				document.getElementById('total-subtotal').innerHTML = "$" +  CommaFormatted(subtotal.toFixed(2));
				document.getElementById('total-tax').innerHTML = "$" +  CommaFormatted(tax.toFixed(2));
				document.getElementById('total-span').innerHTML = "$" +  CommaFormatted(total.toFixed(2));
			@else
				document.getElementById('total-subtotal').innerHTML = CommaFormatted(subtotal.toFixed(2)) + " Ft";
				document.getElementById('total-tax').innerHTML = CommaFormatted(tax.toFixed(2)) + " Ft";
				document.getElementById('total-span').innerHTML = CommaFormatted(total.toFixed(2)) + " Ft";
			@endif
	}
	else if (is_valid_vat == 1)
	{
			subtotal = total * 100 / 127;
			tax = total - subtotal;
			@if ($shoppingcartsetting['currency'] == 'USD')
				document.getElementById('total-subtotal').innerHTML = "$" +  CommaFormatted(subtotal.toFixed(2));
				document.getElementById('total-tax').innerHTML = "$" +  0;
				document.getElementById('total-span').innerHTML = "$" +  CommaFormatted(subtotal.toFixed(2));
			@else
				document.getElementById('total-subtotal').innerHTML = CommaFormatted(subtotal.toFixed(2)) + " Ft";
				document.getElementById('total-tax').innerHTML = 0 + " Ft";
				document.getElementById('total-span').innerHTML = CommaFormatted(subtotal.toFixed(2)) + " Ft";
			@endif
	}


	document.getElementById('account-span').innerHTML = document.getElementById('account').value;

	var name = "{{$user->name}}";
	var city = "{{$user->city}}";
	var country = "{{$user->country}}";
	var street = "{{$user->street_address}}";
	var postcode = "{{$user->zipcode}}";

	if (company)
	{
		document.getElementById('billing-info-tax').innerHTML = "{{$user->tax_num}}";
		document.getElementById('billing-info-vat').innerHTML = "{{$user->eu_vat_num}}";
		document.getElementById('billing-info-name').innerHTML = company;
	}
	else
	{
		document.getElementById('billing-info-name').innerHTML = name;
	}
	document.getElementById('billing-info-address').innerHTML = street + ' ' + city + ' ' + country;
	document.getElementById('billing-info-postcode').innerHTML = postcode;

	if (payment == "barion")
	{
		document.getElementById('GoPay').innerHTML = '{!! trans("order.span64") !!}';
		document.getElementById('payment-img-div').innerHTML = '<img src="/images/order_barion_logo.png">';
	}
	else if (payment == "paypal")
	{
		document.getElementById('GoPay').innerHTML = '{!! trans("order.span63") !!}';
		document.getElementById('payment-img-div').innerHTML = '<img src="/images/order-paypal-logo.png">';
	}
	else
	{
		document.getElementById('GoPay').innerHTML = '{!! trans("order.span42") !!}';
		document.getElementById('payment-img-div').innerHTML = '<img src="/images/order-bank-transfer.png">';
	}

	$('#GoPay').click(function(){
		var route = '';
		if (payment == "paypal") route = '/payment/paypal';
		else if (payment == "barion") route = '/payment/barion';
		else route = '/payment/bank';
		console.log('gopay',payment,route);
		document.getElementById("payment-confirm-form").action = route;
		$('#payment-confirm-form').submit();
	})

	$('.accordion .item .heading').click(function() {
		var a = $(this).closest('.item');
		var b = $(a).hasClass('open');
		var c = $(a).closest('.accordion').find('.open');

		if(b != true) {
			$(c).find('.content').slideUp(200);
			$(c).removeClass('open');
		}

		$(a).toggleClass('open');
		$(a).find('.content').slideToggle(200);
	});
});
</script>
@endsection
