<style>
.navbar-default {
	width:100%;
	background:rgba(255,255,255,0);
	margin:0;
	border:none;

}

.navbar-default .navbar-nav{
	padding-right:15px;
}

.navbar-default .navbar-nav > li{

	padding-right:25px;
}

.navbar-default .navbar-nav .dropdown{

}

.navbar-default .navbar-nav > li > a{
	color:#ffffff;
	font-size: 14px;
	text-transform: uppercase;
}

.navbar-default .navbar-nav > li > a:hover{
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > li > a:focus{
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a{
	background:rgba(255,255,255,0);
	color:#ffffff;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a:hover{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a:focus{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu{
	margin-top:10px;
	padding:15px 20px 15px 20px;
	background:#399aed
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li{
	padding:0;
	border-bottom:1px solid #338ad5;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a{
	padding:10px 0px 10px 0px ;
	color:#ffffff;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a:hover{
	background:#399aed;
	color:#117a97;
}

#dropdown2:before {
	position: absolute;
	top: -9px;
	left:120px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

#dropdown1:before {
	position: absolute;
	top: -9px;
	left:60px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

.navbar-default .navbar-toggle {
		border:none;
		padding-right:15px;
}

.navbar-default .first-nav {

}


#header-div-homepage
{
	width:100%;
	padding:10px 0 10px 0;
	margin:0;
}

.fixed {
	-webkit-transform: translate3d(0,0,0);
	position:fixed;
	top:0px;
	background-color:#006e73;
	-webkit-box-shadow: 0px 2px 5px 1px #004a4d;
   -moz-box-shadow: 0px 2px 5px 1px #004a4d;
        box-shadow: 0px 2px 5px 1px #004a4d;
	z-index:999;
	padding:0px 0 0px 0;
}
#my-account-button{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #f29f00;
	width:150px;
	height:40px;
}

#my-account-button:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #d98f00;
}

#my-account-button-logged{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #399aed;
	width:150px;
	height:40px;
}

#my-account-button-logged:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #2081d5;
}

#try-it-button-second{
	color:#399aed;
	background-color:#ffffff;
	width:200px;
}

#try-it-button-second:hover{
	color:#399aed;
	background-color:#dfe8ef;
	font-weight:bold;
}

#scroll-div{
	color:#c0d9da;
}

#scroll-div:hover{
	color:#cde1e1;
}

.a-next-prev {
	padding-bottom:150px;
	float:right;
	color:black;
	font-size:40px;
}

.a-next-prev:hover {
	padding-bottom:150px;
	float:right;
	color:#399aed;
	font-size:40px;
}

.socialbuttons .facebook{
	width:40px;
	height:40px;
	background:url('/images/social-facebook-icon.png') center/cover;
}
.socialbuttons .facebook:hover{
	background:url('/images/social-facebook-icon-hover.png') center/cover;
}
.socialbuttons .twitter{
	width:40px;
	height:40px;
	background:url('/images/social-twitter-icon.png') center/cover;
}
.socialbuttons .twitter:hover{
	background:url('/images/social-twitter-icon-hover.png') center/cover;
}
.socialbuttons .goggle{
	width:40px;
	height:40px;
	background:url('/images/social-goggle-icon.png') center/cover;
}
.socialbuttons .goggle:hover{
	background:url('/images/social-goggle-icon-hover.png') center/cover;
}
.socialbuttons .insta{
	width:40px;
	height:40px;
	background:url('/images/social-youtube-icon.png') center/cover;
}
.socialbuttons .insta:hover{
	background:url('/images/social-youtube-icon-hover.png') center/cover;
}

.link1{
	color:#66abae;
}
.link1:hover{
	color:#ffffff;
	text-decoration:none;
}

#homepage-title {
	font-size:60px;
	font-weight:300;
	line-height:65px;
}

#homepage-title-div{
	text-align:left !important;
}

#try-it-button{
	font-size:16px;
	height:50px;
	width:280px;
	margin:10px;
}

#try-it-button-div{
	text-align:left !important;
}

.section3-text-div{
	text-align:left !important;
}

#owl-carousel{
	width:70%;
}

#hero-div{
	margin-top:100px;
}

#header-background-div
{
	background:url('/images/homepage-hero-background.png');
	background-repeat: no-repeat;

}
span{
	color:#000000;
}

#footer-div-bottom {
	width: 60% !important;
}
@media (max-width: 1777px)
{
	#footer-div-bottom {
		width: 70% !important;
	}

	#homepage-title {
		font-size:45px;
		font-weight:300;
		line-height:50px;
	}

	#try-it-button{
		width:220px;
	}
}
@media (max-width: 1530px) {
	#hero-div{
		margin-top:50px;
	}

	#footer-links-div{
		width: 70% !important;
	}
	#footer-div-bottom {
		width: 80% !important;
	}

}

@media (max-width: 1386px)
{
	#section1-content-div
	{
		width: 80% !important;
	}
	#hero-div{
		margin-top:50px;
	}
	#homepage-title {
		font-size:40px;
		font-weight:300;
		line-height:50px;
	}
	#hero-div-content{
		width:85%;
	}
	#footer-div-bottom {
		width: 87% !important;
	}
}
@media (max-width: 1245px) {
  #footer-links-div	{
		width: 90% !important;
  }
  #section1-content-div
  {
  	 width: 90% !important;
  }
  #footer-div-bottom {
  	width: 100% !important;
  }
}

@media (max-width:1110px){
	#footer-div-bottom{
		padding:0 !important;
	}
	#footer-div-bottom a{
		margin-left: 10px !important;
	}
	#section1-content-div
	{
		width: 100% !important;
	}
	#homepage-title {
		font-size:30px;
		font-weight:300;
		line-height:40px;
	}
	#try-it-button{
		width:180px;
	}
}

@media (max-width: 950px) {
  #footer-links-div	{
		width: 100% !important;
  }
  #homepage-title-div, #try-it-button-div {
	  text-align:center !important;
  }
  .section3-text-div{
	  text-align:center !important;
  }
  .section2-div-text {
	   text-align:center !important;
  }
}
@media (max-width: 400px) {
	#homepage-title {
		font-size:25px;
		font-weight:300;
		line-height:35px;
	}

	#try-it-button{
		font-size:15px;
		width:180px;
	}

	#owl-carousel{
		width:100%;
	}
	#hero-div{
		margin:0px;

	}


	#header-div-homepage
	{
		padding:5px 0 0px 0 !important;
		margin:0;
	}

	#nav-bar{
		margin:0;

	}

	.section1-block{
		padding-top:10px !important;
	}

	#section2{
		padding-top:0px !important;
	}

	#section2-div2
	{
		text-align:center !important;
	}

	#section5{
		padding-top:40px !important;
	}

	#carousel_div{
		padding-top:0px !important;
	}
}

@media (max-width: 1850px) {
	.footer {
		width:80%;
	}
}
@media (max-width: 1345px) {
	.footer {
		width:90%;
	}
}
@media (max-width: 1185px) {
	.footer {
		width:100%;
	}
}

@media (max-width: 769px) {
    .navbar-default .navbar-collapse {
        background:#ffffff;
		float:right;
		width:50%;
    }

	.navbar-default .navbar-nav > li > a{
		color:#6c937b;
	}

	.navbar-default .navbar-nav > li {
		border-bottom: 1px solid #eeeeee;
	}


}

</style>


		<div id="header-background-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px;width:100%;">
			<div id="header-div-homepage" class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing" style="justify-content:center">					
				<div id="nav-bar" class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;padding:0px 0px 0px 0px;">
					<nav class="navbar navbar-default"  style="">
						<div class="container-fluid">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
									<i aria-hidden="true" class="fa fa-navicon" style="color:#ffffff"></i>
								</button>
								<a class="navbar-brand" href="/"><img src='/images/logo.png'></img></a>
							</div>
							<div class="collapse navbar-collapse  navbar-right" id="myNavbar">
								<ul class="nav navbar-nav">
									<li><a id="header-span1" href="https://imprevo.hu/what-is-imprevo">A Imprevz?</a></li>
									<li class="dropdown">
										<a id="header-span2" class="dropdown-toggle" data-toggle="dropdown" href="#">KURZUSOK</a>
										<ul id="dropdown1" class="dropdown-menu">
											<li><a id="header-span3" class="a-submenu" href="https://imprevo.hu/sales/course/10">Angol tanfolyam </a></li>
											<li><a id="header-span4" class="a-submenu" href="https://imprevo.hu/sales/course/11">Hétköznapi angol </a></li>
										</ul>
									</li>
									<li><a id="header-span5" href="https://imprevo.hu/blog">Blog</a></li>
									<li class="dropdown">
										<a id="header-span6" class="dropdown-toggle" data-toggle="dropdown2" href="https://imprevo.hu/ingyenes-tartalmak">Ingyenesek<!--<span class="caret"></span>--></a>
										<ul id="dropdown2" class="dropdown-menu">
											<li><a id="header-span7" class="a-submenu" href="https://imprevo.hu/20-részes-ingyenes-angol-tanfolyam">20 részes email tanfolyam</a></li>
											<li><a id="header-span8" class="a-submenu" href="https://imprevo.hu/tanulj-meg-angolul-6-hónap-alatt-az-imprevo-val">6 hónap alatt angolul</a></li>
											<li><a id="header-span9" class="a-submenu" href="https://imprevo.hu/angol-igeidők-ebook">Angol igeidők</a></li>
											<li><a id="header-span10" class="a-submenu" href="https://imprevo.hu/angol-levelek-írása">Angol levelek írása</a></li>
										</ul>
									</li>
								</ul>
								<ul  id="nav-right-ul" class="nav navbar-nav navbar-right">


								</ul>
								<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
							</div>
						</div>
					</nav>
				</div>
			</div>
			<div id="hero-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="justify-content:center;width:100%;padding:0;">
				<div id="hero-div-content" class="mdl-grid mdl-cell mdl-cell--9-col mdl-cell--4-col-phone" style="padding:0;padding-top:10px;">
					<div class="mdl-cell mdl-cell mdl-cell--9-col mdl-cell--order-2 mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--order-1-phone" style="text-align:center;padding:0;">
						<img src="/images/homepage-graphics.png" style="width:80%;">
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--3-col mdl-cell--order-1 mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--order-2-phone" style="">
						<div id="homepage-title-div" class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="text-align:center">
							<span id="homepage-title" style="color:#ffffff;">Tanulj meg angolul egy hatékony nyelvtanuló rendszerrel!</span>
						</div>
						<div id="try-it-button-div" class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="text-align:center;margin:0px;padding:0;padding-top:10px;width:100%;">
							<button id="try-it-button" onclick="location.href = '/register?free=1'" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" onmouseover="try_it_button_mouseover(this)" onmouseout="try_it_button_mouseout(this)">
									PRÓBÁLD KI INGYEN!
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
				<div id="scroll-div" class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--8-col-phone" style="">
					<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
						<span style="color:#c0d9da; font-size:16px;">Görgess és tudj meg többet</span>
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
						<i aria-hidden="true" class="fa fa-chevron-down"></i>
					</div>
				</div>
			</div>

		<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px;width: 100%;">
			<div id="section1" class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;width:100%;background-color:#ffffff;margin:0;padding:0px;padding-top:50px; padding-bottom:50px;">
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
					<span style="color:#399aed">TELJESKÖRŰ</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
					<span style="color:#000000;font-size:30px;">Minden egyes lecke fejleszti angoltudásod.</span>
				</div>

				<div id="section1-content-div" class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" >
					<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;margin:0;padding:0px;">
						<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--order-2-phone">
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="margin:0;padding:0px;">
								<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/homepage-section1-icon1.png" style="width:100%;">
								</div>
								<div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--7-col-tablet mdl-cell--3-col-phone" style="margin:0;padding:0px;">
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:15px; font-weight:bold">Hallás utáni szövegértés</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span>Kezdetben könnyű, később folyamatosan nehezedő érdekes párbeszédek, élő szövegek, angolul és magyarul is leírva a végén.</span>
									</div>
								</div>
							</div>
							<div class="section1-block mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;padding-top:100px;">
								<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/homepage-section1-icon2.png" style="width:100%;">
								</div>
								<div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--7-col-tablet mdl-cell--3-col-phone" style="margin:0;padding:0px;">
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:15px; font-weight:bold">Anyanyelvi kiejtés</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet">
										<span>Minden szóhoz anyanyelvi kiejtés tartozik, amit akárhányszor meghallgathatsz és gyakorolhatsz.</span>
									</div>
								</div>
							</div>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-cell--order-1-phone mdl-cell--middle" style="text-align:center">
							<img src="/images/homepage_section1 screen_line.png" style="width:100%">
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--order-3-phone">
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="margin:0;padding:0px;">
								<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/homepage-section1-icon3.png" style="width:100%;">
								</div>
								<div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--7-col-tablet mdl-cell--3-col-phone" style="margin:0;padding:0px;">
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:15px; font-weight:bold">Érthető nyelvtani videók</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span>Az igeidőket és a nyelvtant elemeire bontottuk, videós példákon keresztül mutatjuk be, feladatokon keresztül gyakorolhatsz.</span>
									</div>
								</div>
							</div>
							<div class="section1-block mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;padding-top:100px;">
								<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/homepage-section1-icon4.png" style="width:100%;">
								</div>
								<div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--7-col-tablet mdl-cell--3-col-phone" style="margin:0;padding:0px;">
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:15px; font-weight:bold">Játékos szótanulás</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span>Minden leckében új szavakkal ismerkedhetsz meg különböző játékokon keresztül.</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="section2" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="width:100%;background-color:#ebf3ef;margin:0;padding:0px;justify-content:center;text-align:center;padding-top:80px">
				<div class="mdl-grid mdl-cell mdl-cell--8-col mdl-cell--4-col-phone" style="padding:0;margin:0;">
					<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--4-col-phone mdl-cell--order-2-tablet mdl-cell--order-2-phone">
						<img src="/images/homepage-section2-picture.png" style="width:100%">
					</div>
					<div id="section2-div2" class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--order-1-tablet mdl-cell--order-1-phone" style="padding:0;margin:0;text-align:left">
						<div class="section2-div-text mdl-cell mdl-cell mdl-cell--12-col" style="text-align:left; padding-top:40px;">
							<span style="color:#3897e8; font-size:12px">FELHASZNÁLÓBARÁT</span>
						</div>
						<div class="section2-div-text mdl-cell mdl-cell mdl-cell--12-col" style="text-align:left; padding:0;padding-top:15px;">
							<span style="color:#000000;font-size:30px;line-height:35px;">Végezd a saját<br> tempódban, a kezed<br> nem engedjük el.</span>
						</div>
						<div class="section2-div-text mdl-cell mdl-cell mdl-cell--12-col" style="text-align:left; padding-top:15px;">
							<span style="">Úgy haladsz végig a leckéken ahogy neked jól esik. Csaponghatsz, kiválaszthatod a csak számodra érdekes feladatokat és témákat, ha azonban segítséget vársz, visszatérhetsz a gondosan felépített és vezetett kurzushoz.</span>
						</div>
					</div>
				</div>
			</div>
			<div id="section3" class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%;background-color:#ffffff;margin:0;padding:0px;justify-content:center;">
				<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--4-col-phone mdl-cell--order-2-phone mdl-cell--middle">
					<div class="section3-text-div mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center;">
						<span style="color:#3897e8">KOMPAKT</span>
					</div>
					<div class="section3-text-div mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:15px;text-align:center;">
						<span style="font-size:30px;">Angolozz bárhol, bármikor</span>
					</div>
					<div class="section3-text-div mdl-cell mdl-cell mdl-cell--8-col" style="padding-top:30px;text-align:center;">
						<span style="line-height:25px">Az IMPREVO-t ott és akkor használod amikor akarod. Az leckéket mobilodról bárhonnan és bármikor elérheted. Buszozás, várakozás, vagy egy kávészünet? Minden időben alkalmat teremthetsz a tanulásra.
</span>
					</div>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--4-col-phone mdl-cell--order-1-phone" style="padding:0;margin:0;margin-left:15px;">
					<img src="/images/homepage-section3-picture.png" style="width:100%">
				</div>
			</div>

			<div id="section4-phone" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="margin:0;width:100%;background-color:#399aed;padding:40px 0px 40px 0px;justify-content:center">
				<span style="color:#ffffff; font-size:30px; padding-top:5px;padding-right:20px">Próbáld ki az IMPREVO-t ingyen!
				</span>
				<button id="try-it-button-second" onclick="location.href='https://imprevo.hu/register?free=1'" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
					KIPRÓBÁLOM!
				</button>
			</div>
			<div id="section4" class="mdl-grid mdl-cell mdl-cell--hide-desktop mdl-cell--4-col-phone" style="margin:0;width:100%;background-color:#399aed;padding:40px 0px 40px 0px;justify-content:center">

					<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center; ">
						<span style="color:#ffffff; font-size:30px;">Próbáld ki az IMPREVO-t még ma ingyen
						</span>
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center; ">
						<button id="try-it-button-second" onclick="location.href='https://imprevo.hu/register?free=1'" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
							PRÓBÁLD KI INGYEN!
						</button>
					</div>

			</div>
			<div id="section5" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="padding-top:100px;margin:0;width:100%;background:url('/images/homepage-section5-background.png') no-repeat center/cover">
				<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="text-align:center">
					<span style="font-size:14px;color:#399aed">RÓLUNK MONDTÁK</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="text-align:center">
					<span style="font-size:30px;line-height:30px;">Akik már tanultak az IMPREVO-val</span>
				</div>
				<div id="carousel_div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="padding:0;padding-top:50px;justify-content:center;">
					<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--middle mdl-cell--hide-phone" style="">
						<a id="carousel_prev" class="a-next-prev" style="cursor:pointer"><i class="fa fa-chevron-left"></i></a>
					</div>
					<div id="owl-carousel" class="owl-carousel owl-theme" data-plugin-carousel data-plugin-options='{ "dots": true, "autoplay": false, "autoplayTimeout": 3000, "loop": false, "margin": 10, "nav": false, "responsive": {"0":{"items":1 }, "600":{"items":3 }, "1000":{"items":3 } }  }'
						style="">
						<div class="mdl-grid mdl-cell mdl-cell--12-col">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
								<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
								<span style="font-style: italic; font-size:15px;">“ Nyelvtanfolyamra is járok! E mellé nagyon nagy segítség az IMPREVO! A tanfolyamon általában sietnek, itt csak akkor megyek tovább, ha mindent értek. A tanfolyamon is ajánlottam már többeknek. Sokat segített megérteni dolgokat, így nem maradtam le a tanfolyamon sem! ”</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
								<span style="font-size:15px;font-weight:bold">György</span>
							</div>

						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-style: italic; font-size:15px;">“ Rendszerezett, jól felépített tananyag, könnyű használni. Engem mindig magával ragad, nehéz abbahagyni. ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">Gabi</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-style: italic; font-size:15px;">“ Egy igazán modern segítség a nyelvtanuláshoz. Az ingyenes leckékkel érdemes kipróbálni, nem lehet veszíteni rajta. ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">István</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-style: italic; font-size:15px;">“ Ez a tananyag alkalmas az önálló tanulásra. Még egészen az elején tartok, de már egyre jobban megértem a hallott szöveget is. ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">ANNA</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-style: italic;font-size:20px;">“ Kreatív, jó humorú, logikus, könnyen emészthető. ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:20px;font-weight:bold">Zsuzsi</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-style: italic;font-size:15px;">“ Korrekt, nagyon részletes magyarázatokkal, és az ára is megfelelő. És az, hogy korlátlan a hozzáférés, király! ”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">Ottó</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-style: italic;font-size:15px;">“ Célszerű. Érthető, nem kell hozzá előzetesen nyelvtani zseninek lenni. Tetszetős a szavak memoriter módszere, és ...”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">Zsolt</span>
								</div>

							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
									<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
									<span style="font-style: italic;font-size:15px;">“ Ha teljes körű angol nyelvórákat szeretnél venni, és nem szeretnél csoportos foglalkozáson részt venni,...”</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
									<span style="font-size:15px;font-weight:bold">Bea</span>
								</div>

							</div>
						</div>
						<div class="mdl-grid mdl-cell mdl-cell--12-col">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
								<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
								<span style="font-style: italic;font-size:15px;">“ Olcsón, és profi rendszerrel, könnyedén a saját tempódban tudsz megtanulni, a kezdő szinttől kezdve a haladó középfokig. Klassz anyagok, videók és hanganyagok segítségével. Nekem nagyon tetszik. Ajánlom mindenkinek!”</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
								<span style="font-size:15px;font-weight:bold">Péter</span>
							</div>

						</div>
						<div class="mdl-grid mdl-cell mdl-cell--12-col">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
								<img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
								<span style="font-style: italic;font-size:15px;">“ Összességében nagyon jó lehetőség a nyelvtanulásra. A tanfolyamok töredékéért kapom minimum ugyanazt, de szerintem még többet is. A magyarázó videók is nagyon könnyen megérhetőek. Szerintem professzionálisan van felépítve az egész. ”</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
								<span style="font-size:15px;font-weight:bold">Kázmér</span>
							</div>

						</div>

					</div>
					<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--hide-phone mdl-cell--middle" style="">
						<a id="carousel_next" class="a-next-prev" style="float:left; cursor:pointer"><i class="fa fa-chevron-right"></i></a>
					</div>
				</div>
			</div>
		 </div>
		<div class="footer mdl-grid mdl-cell mdl-cell--12-col" style=" margin:0;padding:0px;background:#006469;width: 100%;justify-content:center">
		   <form action="http://sendy.imprevo.info/subscribe" method="POST" accept-charset="utf-8" style="text-align:center;width:100%;margin:0;padding:0">
				<input type="hidden" name="list" value="dWVr892XDx8pprHN8763yTtK6w"/>
				<div id="footer-div1" class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="padding-top:80px; text-align:center">
						<span id="footer-span1" style="font-size:30px; color:#ffffff">Értesülj legfrisebb leckéinkről,  ingyenes anyagainkról</span>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0; padding:0; width: 100%;justify-content:center">
					<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="padding-top:20px;text-align:center">
						<div class="input-group mb-md">
							<input type="text" class="form-control" placeholder="Email cím" style="height:45px;outline:none;border:none;">
							<span class="input-group-btn">
								<button class="btn btn-warning" type="button" style="height:45px;border-radius:0px">FELIRATKOZOM</button>
							</span>
						</div>
					</div>
				</div>
		   </form>
		   <div class="mdl-cell mdl-cell mdl-cell--4-col-phone mdl-cell--hide-desktop mdl-cell--hide-tablet" style="padding-top:20px;text-align:center">
				<div class="mdl-cell mdl-cell mdl-cell--4-col-phone">
					<input type="text" class="form-control" placeholder="Email cím" style="height:45px;outline:none;border:none;">
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--4-col-phone">
				    <button class="btn btn-warning" type="button" style="height:45px;border-radius:0px;width:100%;">FELIRATKOZOM</button>
				</div>
		   </div>

			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%; margin:0px;padding-top:15px; text-align:center; justify-content:center">
				<div class="footer mdl-grid mdl-cell mdl-cell--7-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="color:#ffffff; margin:0px;padding-top:15px; text-align:center; justify-content:center">
					<div class="mdl-grid mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
							<span style="color:#ffffff">Rólunk</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/kik-vagyunk" style="">Kik vagyunk?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/what-is-imprevo" style="">Mi az IMPREVO?</a>
						</div>

						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/contact-page" style="">Kapcsolat</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/sales/course/10" style="">Online Angol tanfolyam</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/sales/course/11" style="">Hallás utáni szövegértés leckék</a>
						</div>

						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/meg-lehet-tanulni-az-imprevo-val-angolul" style="">Meg lehet tanulni az IMPREVO-val angolul?</a>
						</div>
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span style="color:#ffffff">Ingyenes anyagok</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/angol-igeidők-összefoglaló-táblázat" style="">Igeidők összefoglaló táblázat</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/20-részes-ingyenes-angol-tanfolyam" style="">Ingyenes Kezdő Online Angol</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/blog/category/Zenés%20leckék" style="">Zenés leckék</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/ingyenes-tartalmak" style="">Nyelvtani anyagok</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/blog" style="">Blog</a>
						</div>
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span style="color:#ffffff">Segítség</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/gyik" style="">GYIK</a>
						</div>

						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/blog/tanulási-stílusok" style="">Milyen a tanulási stílusod?</a>
						</div>

					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span style="color:#ffffff">Ebookok</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/angol-igeidők-ebook" style="">Angol igeidők összefoglaló és példatár</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/angol-levelek-írása" style="">Angol levelek és e-mailek írása</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/tanulj-meg-angolul-6-hónap-alatt-az-imprevo-val" style="">Készülj fel a nyelvvizsgára 6 hónap alatt</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col" style="padding-top:50px;">
							<span style="color:#ffffff">KÖVESS MINKET</span>
						</div>
						<div class="socialbuttons mdl-grid mdl-cell mdl-cell--12-col" style="">
							<div class="mdl-cell mdl-cell mdl-cell--3-col">
								<a href="https://www.facebook.com/imprevo" target="_blank">
									<div class="facebook"></div>
								</a>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--3-col">
								<a href="https://twitter.com/ImprevoLearning" target="_blank">
									<div class="twitter"></div>
								</a>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--3-col">
								<a href="https://plus.google.com/103750741136736429559" target="_blank">
									<div class="goggle"></div>
								</a>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--3-col">
								<a href="https://www.youtube.com/channel/UC9zzm-SiiLWFX-a_M0E1QHQ" target="_blank">
									<div class="insta"></div>
								</a>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center;padding-top:10px">
								<img src="/images/footer-barion-logo.png">
							</div>
						</div>
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--hide-desktop " style="justify-content:center">
				  <div class="phone-footer-accordion" style="width:100%; padding:0; margin:0; text-align:left;">
					<div class="item" style="">
						<div class="heading" style="">Rólunk</div>
						<div class="content" style="padding-left:10px;margin:0">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0">
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/kik-vagyunk" style="">Kik vagyunk?</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/what-is-imprevo" style="">Mi az IMPREVO?</a>
								</div>

								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/contact-page" style="">Kapcsolat</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/sales/course/10" style="">Online Angol tanfolyam</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/sales/course/11" style="">Hallás utáni szövegértés leckék</a>
								</div>

								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/meg-lehet-tanulni-az-imprevo-val-angolul" style="">Meg lehet tanulni az IMPREVO-val angolul?</a>
								</div>
							</div>
						</div>
					</div>
					<div class="item" style="">
						<div class="heading" style="">Ingyenes anyagok</div>
						<div class="content" style="padding-left:10px;margin:0">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0">
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/angol-igeidők-összefoglaló-táblázat" style="">Igeidők összefoglaló táblázat</a>
								</div>

								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/20-részes-ingyenes-angol-tanfolyam" style="">Ingyenes Kezdő Online Angol</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/blog/category/Zenés%20leckék" style="">Zenés leckék</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/ingyenes-tartalmak" style="">Nyelvtani anyagok</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/blog" style="">Blog</a>
								</div>
							</div>
						</div>
					</div>
					<div class="item" style="">
						<div class="heading" style="">Segítség</div>
						<div class="content" style="padding-left:10px;margin:0">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0">
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/gyik" style="">GYIK</a>
								</div>

								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/blog/tanulási-stílusok" style="">Milyen a tanulási stílusod?</a>
								</div>

							</div>
						</div>
					</div>
					<div class="item" style="">
						<div class="heading" style="">Ebookok</div>
						<div class="content" style="padding-left:10px;margin:0">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0">
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/angol-igeidők-ebook" style="">Angol igeidők összefoglaló és példatár</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/angol-levelek-írása" style="">Angol levelek és e-mailek írása</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/tanulj-meg-angolul-6-hónap-alatt-az-imprevo-val" style="">Készülj fel a nyelvvizsgára 6 hónap alatt</a>
								</div>
							</div>
						</div>
					</div>
				  </div>
				  <div class="socialbuttons mdl-grid mdl-cell mdl-cell--4-col-tablet mdl-cell--3-col-phone mdl-grid-no--spacing" style="padding:0;margin:0;">
				  	<div class="mdl-cell mdl-cell mdl-cell--1-col-phone mdl-cell--2-col-tablet" style="">
				  		<a href="https://www.facebook.com/imprevo" target="_blank">
				  			<div class="facebook"></div>
				  		</a>
				  	</div>
				  	<div class="mdl-cell mdl-cell mdl-cell--1-col-phone mdl-cell--2-col-tablet">
				  		<a href="https://twitter.com/ImprevoLearning" target="_blank">
				  			<div class="twitter"></div>
				  		</a>
				  	</div>
				  	<div class="mdl-cell mdl-cell mdl-cell--1-col-phone mdl-cell--2-col-tablet">
				  		<a href="https://plus.google.com/103750741136736429559" target="_blank">
				  			<div class="goggle"></div>
				  		</a>
				  	</div>
				  	<div class="mdl-cell mdl-cell mdl-cell--1-col-phone mdl-cell--2-col-tablet">
				  		<a href="https://www.youtube.com/channel/UC9zzm-SiiLWFX-a_M0E1QHQ" target="_blank">
				  			<div class="insta"></div>
				  		</a>
				  	</div>
					<div class="mdl-cell mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="padding-top:10px; text-align:center">
						<img src="/images/footer-barion-logo.png">
					</div>
				  </div>
				</div>

				<div class="footer mdl-cell mdl-cell mdl-cell--7-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="text-align:center;margin:0; padding:0; padding:0px 20px 10px 20px">
					<hr style="width:100%; border:none;height:1px; background-color:#66abae;"></hr>
					<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:left;width:100%">
					<span style="color:#7fbabd">
						<a href="/" style="margin-right:20px">
							<img src="/images/logo.png">
						</a>
						Copyright 2017. Imprevo. Minden jog fenntartva.
					</span>
					<a class="link1" href="https://imprevo.hu/céginformáció" style="float:right;">Impresszum</a>
					<a class="link1" href="https://imprevo.hu/aszf" style="float:right;margin-right:15px;">Felhasználás feltételei</a>
					<a class="link1" href="https://imprevo.hu/adatvedelem" style="float:right;margin-right:15px;">Adatvédelem</a>

					<!--<a href="/" style="margin-left:20px">
						<img src="/images/Barion_logo.png">
					</a> -->
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--4-col-phone mdl-cell--hide-desktop mdl-cell--8-col-tablet" style="text-align:center;margin:0; padding:0; padding:0px 20px 10px 20px">
					<!--<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<a href="/" style="">
							<img src="/images/Barion_logo.png">
						</a>
					</div>-->
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<a class="link1" href="https://imprevo.hu/adatvedelem" style="">Adatvédelem</a>
					</div>
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<a class="link1" href="https://imprevo.hu/aszf" style="">Felhasználás feltételei</a>
					</div>
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<a class="link1" href="https://imprevo.hu/céginformáció" style="">Impresszum</a>
					</div>
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<a href="/" style="">
							<img src="/images/logo.png">
						</a>
					</div>
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<span style="color:#7fbabd">
								Copyright 2017. Imprevo. Minden jog fenntartva.
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
