<!DOCTYPE html>
<html lang="en" style="position: relative; min-height: 100%;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="{{$page->seo_description}}">
    <meta name="keywords" content="{{$page->seo_keywords}}">
    <title>{{$page->seo_title}}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    {{--<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue-light_blue.min.css" />--}}
    <link rel="stylesheet" href="/css/material.min.css" />
    <link rel="stylesheet" href="/css/styles.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="{{ Config::get('RELATIVE_URL') }}/assets/vendor/pnotify/pnotify.custom.css" />
    <link rel="stylesheet" type="text/css" href="http://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.css" />
	<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- Specific Page Vendor CSS -->
	<link rel="stylesheet" href="assets/vendor/owl.carousel/assets/owl.carousel.css" />
	<link rel="stylesheet" href="assets/vendor/owl.carousel/assets/owl.theme.default.css" />	
	<!-- Theme CSS -->
	<link rel="stylesheet" href="assets/stylesheets/theme.css" />

	<!-- Skin CSS -->
	<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">	

    <script src="/assets/vendor/modernizr/modernizr.js"></script>

	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.js"></script>
    <script src="/assets/vendor/pnotify/pnotify.custom.js"></script>	

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
	<script src="assets/vendor/owl.carousel/owl.carousel.js"></script>
	
	<script src="assets/javascripts/theme.js"></script>
	
	<!-- Theme Custom -->
	<script src="assets/javascripts/theme.custom.js"></script>
	
	<!-- Theme Initialization Files -->
	<script src="assets/javascripts/theme.init.js"></script>


    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <!--Facebook Pixel Code-->
    <?php echo html_entity_decode($page->facebook_pixel_code)?>
    <!--End Facebook Pixel Code-->
</head>
<body style="padding:0px; height:100%; margin: 0;overflow-x: hidden">
<!--<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" style="width:100%;min-height:100%">-->
<div id='Parent-Div-Background'style="padding:0; margin:0px;position:relative;width:100%;min-height:100%;">
    @if($page['is_show'])
    <header class="mdl-layout__header mdl-layout__header--transparent" style="margin:0px; padding:0px;background: #006e73;height:100px">
        <div class="mdl-layout__header-row" style="margin-top:15px">
            <!-- Title -->
                <span class="mdl-layout-title">
                    <a href="/"><img src="/images/logo.png"></a>
                </span>
            <!-- Add spacer, to align navigation to the right -->
            <div class="mdl-layout-spacer"></div>
            <!-- Navigation -->
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="/tryit" style="color:#fff;font-size: 18px">Try it</a>
                <a class="mdl-navigation__link" href="/courses" style="color:#fff;font-size: 18px">Course</a>
                <a id="submenu" class="mdl-navigation__link" href="#" style="color:#fff;font-size: 18px">Lessons</a>
                <a class="mdl-navigation__link" href="/freebies" style="color:#fff;font-size: 18px">Freebies</a>
                @if (Auth::guest())
                <button onclick="goLogin()" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="color:#fff;font-size: 18px;background: #f29f00;margin-left:150px">
                    LOGIN
                </button>
                @else
                    <button onclick="goProfile(event)" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="color:#fff;font-size: 18px;background: #f29f00;margin-left:150px">
                        MY ACCOUNT
                    </button>
                @endif
            </nav>
            <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu"  for="submenu">
                @foreach(\App\Level::all() as $level)
                    <a class="mdl-menu__item" href="/lessons/{{$level->id}}">{{$level->title}}</a>
                @endforeach
            </ul>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </header>
    @endif

<style>
.navbar-default {
	width:100%;
	background:rgba(255,255,255,0);
	margin:0;
	border:none;

}

.navbar-default .navbar-nav > li{
	padding-right:15px;
}

.navbar-default .navbar-nav .dropdown{
	
}


	
.navbar-default .navbar-nav > li > a{
	color:#ffffff;
	font-size: 18px;
}

.navbar-default .navbar-nav > li > a:hover{
    color:#399aed;
	font-size: 18px;
}

.navbar-default .navbar-nav > li > a:focus{
    color:#399aed;
	font-size: 18px;
}

.navbar-default .navbar-nav > .open > a{
	background:rgba(255,255,255,0);
	color:#ffffff;
	font-size: 18px;
}

.navbar-default .navbar-nav > .open > a:hover{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 18px;
}

.navbar-default .navbar-nav > .open > a:focus{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 18px;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu{
	background:#399aed	
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li{
	border-bottom:1px solid #338ad5;	
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a{
	color:#ffffff;	
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a:hover{
	background:#399aed;
	color:#117a97;	
}

.navbar-default .navbar-toggle {      
		border:none;
		padding-right:15px;
}

.navbar-default .first-nav {

}

@media (max-width: 767px) {
    .navbar-default .navbar-collapse {		
        background:#ffffff;
		float:right;
		width:50%;
    }
	
	.navbar-default .navbar-nav > li > a{		
		color:#6c937b;
	}
	.navbar-default .navbar-nav > li {		
		border-bottom: 1px solid #eeeeee;
	}
}			

#header-div
{
	width:100%;
	/*justify-content:center;*/
	padding:15px 0 15px 0;
	margin:0;
	/*height:100px;*/


}

#header-div:hover
{
	width:100%;
	/*justify-content:center;*/
	padding:15px 0 15px 0;
	margin:0;
	/*height:100px;*/
	
	background-color:#006e73;
	-webkit-box-shadow: 0px 2px 5px 2px #004a4d;
   -moz-box-shadow: 0px 2px 5px 2px #004a4d;
        box-shadow: 0px 2px 5px 2px #004a4d;
}

.fixed {
	-webkit-transform: translate3d(0,0,0);
	position:fixed;
	top:0px;
	background-color:#006e73;
	-webkit-box-shadow: 0px 2px 5px 2px #004a4d;
   -moz-box-shadow: 0px 2px 5px 2px #004a4d;
        box-shadow: 0px 2px 5px 2px #004a4d;		
	z-index:999;
	padding:0px 0 0px 0;
}

#try-it-button {
	 background-color:#399aed;
	 margin:10px;
	 color:#fff;
	 font-size: 18px;
}

#try-it-button:hover {
	 background-color:#2081d5;
	 margin:10px;
	 color:#fff;
	 font-size: 18px;
}

#try-it-button-second{
	color:#399aed;
	background-color:#ffffff;
	margin-left:50px;
}

#try-it-button-second:hover{
	color:#399aed;
	background-color:#dfe8ef;
	font-weight:bold;
	margin-left:50px;
}

#scroll-div{
	color:#338487;
}

#scroll-div:hover{
	color:#cde1e1;
}

.a-next-prev {
	padding-bottom:150px;
	float:right;
	color:black;
	font-size:40px;
}

.a-next-prev:hover {
	padding-bottom:150px;
	float:right;
	color:#399aed;
	font-size:40px;
}

.social-link{
	color: #6c937b;
	margin-right:30px;
	
}
.social-link:hover{
	color: #e2ef1a;		
}
.link1{
	color:#66abae;
}
.link1:hover{
	color:#ffffff;
	text-decoration:none;
}
</style>


<main class="mdl-layout__content" style="width:100%;margin:0px;padding:0px; @if ($page['is_show']) padding-bottom:140px; @endif" >
    <div id="full_content_div" style="width:100%;padding:0px;margin:0px">
		<?php echo html_entity_decode($page->full_width_content)?>

		<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px;width:100%;background:url('/images/homepage-hero-background.png') no-repeat center/cover; ">				
			<div id="header-div" class="mdl-grid mdl-cell mdl-cell--12-col">			
				<div class="mdl-cell mdl-cell mdl-cell--2-col"></div>
				<div class="mdl-cell mdl-cell mdl-cell--8-col" style="padding:0px 0px 0px 0px;">
					<nav class="navbar navbar-default"  style="">
						<div class="container-fluid">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
									<i aria-hidden="true" class="fa fa-navicon" style="color:#ffffff"></i>                       
								</button>								
								<a class="navbar-brand" href="/"><img src='/images/logo.png'></img></a>
							</div>
							<div class="collapse navbar-collapse  navbar-right" id="myNavbar">					
								<ul class="nav navbar-nav">
									<li><a href="">Mi Az Imprevo?</a></li>
									<li><a href="/tryit">Kipróbálom</a></li>
									<li><a href="/courses">Tanfolyam</a></li>
									<li><a href="/lessons">Leckék</a></li>
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle="dropdown" href="#">Ingyenesek<span class="caret"></span></a>
										<ul class="dropdown-menu">
											<li><a class="a-submenu" href="#">Blog</a></li>
											<li><a class="a-submenu" href="#">GYIK</a></li>
											<li><a class="a-submenu" href="#">Igeidők</a></li>
											<li><a class="a-submenu" href="#">20 részes ingyenes tanfolyam</a></li>
											<li><a class="a-submenu" href="#">6 hónap angol</a></li>
											<li><a class="a-submenu" href="#">Hogy tanulj angolul?</a></li>
											<li><a class="a-submenu" href="#">Ingyenes anyagok</a></li>
											<li><a class="a-submenu" href="#">Syllabus</a></li>
										</ul>
									</li>
								</ul>
								<ul class="nav navbar-nav navbar-right">
									<button onclick="goProfile(event)" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="margin:10px;color:#fff;font-size: 18px;background: #f29f00">
										MY ACCOUNT
									</button>
								</ul>									
							</div>
						</div>
					</nav>								
				</div>
			</div>	
			<div id="hero-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="margin-top:100px;">
				<div class="mdl-cell mdl-cell mdl-cell--2-col"></div>
				<div class="mdl-grid mdl-cell mdl-cell--9-col" style="padding-top:10px;">						
					<div class="mdl-cell mdl-cell mdl-cell--9-col mdl-cell--order-2 mdl-cell--8-col-phone mdl-cell--order-1-phone" style="text-align:center">
						<img src="/images/homepage-graphics.png" style="width:80%;">
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--3-col mdl-cell--order-1 mdl-cell--8-col-phone mdl-cell--order-7-phone" style="">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
							<span style="font-size:45px;color:#ffffff">Tanulj meg angolul egy hatékony nyelvtanuló rendszerrel!</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
							<button id="try-it-button" class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
									PRÓBÁLD KI INGYEN!
							</button>
						</div>
					</div>					
				</div>				
			</div>
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
				<div id="scroll-div" class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--8-col-phone" style="">
					<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
						<span style="font-size:18px;">Görgess és tudj meg többet</span>					
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
						<i aria-hidden="true" class="fa fa-chevron-down"></i>
					</div>	
				</div>
			</div>
			
		<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px;width: 100%;">
			<div id="section1" class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%;background-color:#ffffff;margin:0;padding:0px;">
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
					<span style="color:#399aed">TELJESKÖRŰ</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
					<span style="font-size:30px;">Minden egyes lecke fejleszti angoltudásod.</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--2-col"></div>
				<div class="mdl-cell mdl-cell mdl-cell--8-col" >	
					<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px;">
						<div class="mdl-cell mdl-cell mdl-cell--3-col">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px;">
								<div class="mdl-cell mdl-cell mdl-cell--2-col">
									<img src="/images/homepage-section1-icon1.png" style="width:100%;">
								</div>
								<div class="mdl-grid mdl-cell mdl-cell--10-col" style="margin:0;padding:0px;">
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:15px; font-weight:bold">Hallás utáni szövegértés gyakorlás</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span>Kezdetben könnyű, később folyamatosan nehezedő érdekes párbeszédek, élő szövegek, angolul és magyarul is leírva a végén.</span>
									</div>
								</div>
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px;padding-top:100px;">
								<div class="mdl-cell mdl-cell mdl-cell--2-col">
									<img src="/images/homepage-section1-icon2.png" style="width:100%;">
								</div>
								<div class="mdl-grid mdl-cell mdl-cell--10-col" style="margin:0;padding:0px;">
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:15px; font-weight:bold">Anyanyelvi kiejtés a szavakhoz</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span>Minden szóhoz anyanyelvi kiejtés tartozik, amit akárhány-szor meghallgathatsz és gyakorolhatsz.</span>
									</div>
								</div>
							</div>							
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--middle" style="padding-top:30px;text-align:center">
							<img src="/images/homepage_section1 screen_line.png" style="width:70%">
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--3-col">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px;">
								<div class="mdl-cell mdl-cell mdl-cell--2-col">
									<img src="/images/homepage-section1-icon1.png" style="width:100%;">
								</div>
								<div class="mdl-grid mdl-cell mdl-cell--10-col" style="margin:0;padding:0px;">
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:15px; font-weight:bold">Érthető nyelvtani magyarázatok, videóval</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span>Az igeidőket és a nyelvtani szabályokat elemeire bontottuk, videós példákon keresztül mutatjuk be, feladatokon keresztül jobban megértheted.</span>
									</div>
								</div>
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px;padding-top:100px;">
								<div class="mdl-cell mdl-cell mdl-cell--2-col">
									<img src="/images/homepage-section1-icon2.png" style="width:100%;">
								</div>
								<div class="mdl-grid mdl-cell mdl-cell--10-col" style="margin:0;padding:0px;">
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:15px; font-weight:bold">Játékos szótanulás minden leckében</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span>Minden leckében új szavakkal ismerkedhetsz meg különböző játékokon keresztül.</span>
									</div>
								</div>
							</div>									
						</div>
					</div>
				</div>					
			</div>	
			<div id="section2" class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%;background-color:#ebf3ef;margin:0;padding:0px;justify-content:center;padding-top:150px">
				<div class="mdl-grid mdl-cell mdl-cell--8-col">
					<div class="mdl-cell mdl-cell mdl-cell--8-col">
						<img src="/images/homepage-section2-picture.png" style="width:100%">
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--4-col">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:50px;">
							<span style="color:#3897e8; font-size:12px">FELHASZNÁLÓBARÁT</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:15px;">
							<span style="font-size:30px; font-weight:bold">Végezd a saját tempódban, a kezed nem engedjük el.</span>
						</div>		
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:15px">
							<span style="">Kezdetben könnyű, később folyamatosan nehezedő érdekes párbeszédek, élő szövegek, angolul és magyarul is leírva a végén.</span>
						</div>							
					</div>
				</div>
			</div>			
			<div id="section3" class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%;background-color:#ffffff;margin:0;padding:0px;justify-content:center;">
				<div class="mdl-cell mdl-cell mdl-cell--2-col"></div>
				<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--middle">
					<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
						<span style="color:#3897e8">KOMPAKT</span>
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:15px;">
						<span style="font-size:30px; font-weight:bold">Angolozz bárhol</span>
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--8-col" style="padding-top:30px;">
						<span style="">Az IMPREVO leckéit mobilodról bárhonnan és bármikor elérheted. Buszozás, várakozás, vagy egy kávészünet? Minden időben alkalmat teremthetsz a tanulásra. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla in viverra mauris. Curabitur dapibus suscipit nisi, sed.</span>
					</div>					
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--6-col" style="padding:0;margin:0;margin-left:15px;">
					<img src="/images/homepage-section3-picture.png" style="width:100%">
				</div>
			</div>
			<div id="section4" class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:40px 0 40px 0;width:100%;background-color:#399aed;text-align:center; justify-content:center">
				<span style="color:#ffffff; font-size:25px;">Próbáld ki az IMPREVO-t még ma ingyen
					<button id="try-it-button-second" class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
						PRÓBÁLD KI INGYEN!
					</button>
				</span>
			</div>
			<div id="section5" class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;width:100%;background:url('/images/homepage-section5-background.png') no-repeat center/cover">
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:150px;text-align:center">
					<span style="font-size:14px;color:#399aed">RÓLUNK MONDTÁK</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
					<span style="font-size:30px;">Akik már tanultak az IMPREVO-val</span>
				</div>		
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding-top:50px;justify-content:center;">
					<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--middle" style="">
						<a id="carousel_prev" class="a-next-prev" href="#"><i class="fa fa-chevron-left"></i></a> 
					</div>
					<div id="owl-carousel" class="owl-carousel owl-theme" data-plugin-carousel data-plugin-options='{ "dots": true, "autoplay": false, "autoplayTimeout": 3000, "loop": false, "margin": 10, "nav": false, "responsive": {"0":{"items":1 }, "600":{"items":3 }, "1000":{"items":3 } }  }'
						style="width:70%;">
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">		
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">		
									<img  src="/images/homepage-section5-anna.png" alt="" style="width:30%">
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">		
									<span style="font-size:15px;">“ Rendszerezett, jól felépített tananyag, könnyű használni. Engem mindig magával ragad, nehéz abbahagyni. ”</span>
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
									<span style="font-size:15px;font-weight:bold">Gabi</span>
								</div>			
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0px; margin:0px;text-align:center">		
									<span style="font-size:15px;font-weight:bold">Tanuló, Győr</span>
								</div>									
							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">		
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">		
									<img  src="/images/homepage-section5-david.png" alt="" style="width:30%">
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">		
									<span style="font-size:15px;">“ Egy igazán modern segítség a nyelvtanuláshoz. Az ingyenes leckékkel érdemes kipróbálni, nem lehet veszíteni rajta. ”</span>
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
									<span style="font-size:15px;font-weight:bold">István</span>
								</div>			
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
									<span style="font-size:15px;font-weight:bold">Tanuló, Győr</span>
								</div>									
							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">		
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">		
									<img  src="/images/homepage-section5-Kirsztina.png" alt="" style="width:30%">
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">		
									<span style="font-size:15px;">“ Ez a tananyag alkalmas az önálló tanulásra. Még egészen az elején tartok, de már egyre jobban megértem a hallott szöveget is. ”</span>
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
									<span style="font-size:15px;font-weight:bold">ANNA</span>
								</div>			
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
									<span style="font-size:15px;font-weight:bold">Tanuló, Győr</span>
								</div>									
							</div>
						</div>	
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">		
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">		
									<img  src="/images/homepage-section5-david.png" alt="" style="width:30%">
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">		
									<span style="font-size:20px;">“ Kreatív, jó humorú, logikus, könnyen emészthető. ”</span>
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
									<span style="font-size:20px;font-weight:bold">Zsuzsi</span>
								</div>			
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
									<span style="font-size:20px;font-weight:bold">Tanuló, Győr</span>
								</div>									
							</div>
						</div>	
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">		
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">		
									<img  src="/images/homepage-section5-david.png" alt="" style="width:30%">
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">		
									<span style="font-size:15px;">“ Korrekt, nagyon részletes magyarázatokkal, és az ára is megfelelő. És az, hogy korlátlan a hozzáférés, király! ”</span>
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
									<span style="font-size:15px;font-weight:bold">Ottó</span>
								</div>			
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
									<span style="font-size:15px;font-weight:bold">Tanuló, Győr</span>
								</div>									
							</div>
						</div>
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">		
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">		
									<img  src="/images/homepage-section5-david.png" alt="" style="width:30%">
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">		
									<span style="font-size:15px;">“ Célszerű. Érthető, nem kell hozzá előzetesen nyelvtani zseninek lenni. Tetszetős a szavak memoriter módszere, és az olvasmányok is érdekesek. Rá kell jönni mindenkinek magától milyen sorrendben építi fel az adott leckét, hogy minél hatékonyabb legyen, de ez ízlés kérdése… ”</span>
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
									<span style="font-size:15px;font-weight:bold">Zsolt</span>
								</div>			
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
									<span style="font-size:15px;font-weight:bold">Tanuló, Győr</span>
								</div>									
							</div>
						</div>	
						<div class="item" style="">
							<div class="mdl-grid mdl-cell mdl-cell--12-col">		
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">		
									<img  src="/images/homepage-section5-david.png" alt="" style="width:30%">
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">		
									<span style="font-size:15px;">“ Ha teljes körű angol nyelvórákat szeretnél venni, és nem szeretnél csoportos foglalkozáson részt venni, időt tölteni az odautazással, odaérni az órakezdésre, bátran fordulj az IMPREVO felé. Helytől, időtől függetlenül bármikor, bármeddig elérheted, és ami nagyon fontos, hogy szuper áron, és ami még fontosabb, hogy nagyon jól egymásra felépített tananyaggal. Próbáld ki! Hidd el, élvezni fogod minden egyes percét! ”</span>
								</div>	
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
									<span style="font-size:15px;font-weight:bold">Bea</span>
								</div>			
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
									<span style="font-size:15px;font-weight:bold">Tanuló, Győr</span>
								</div>									
							</div>
						</div>	
						<div class="mdl-grid mdl-cell mdl-cell--12-col">		
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">		
								<img  src="/images/homepage-section5-david.png" alt="" style="width:30%">
							</div>	
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">		
								<span style="font-size:15px;">“ Olcsón, és profi rendszerrel, könnyedén a saját tempódban tudsz megtanulni, a kezdő szinttől kezdve a haladó középfokig. 🙂 Klassz anyagok, videók és hanganyagok segítségével. 🙂 Nekem nagyon tetszik. 🙂 Ajánlom mindenkinek! 🙂 ”</span>
							</div>	
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
								<span style="font-size:15px;font-weight:bold">Péter</span>
							</div>			
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
								<span style="font-size:15px;font-weight:bold">Tanuló, Győr</span>
							</div>									
						</div>
						<div class="mdl-grid mdl-cell mdl-cell--12-col">		
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">		
								<img  src="/images/homepage-section5-david.png" alt="" style="width:30%">
							</div>	
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">		
								<span style="font-size:15px;">“ Összességében nagyon jó lehetőség a nyelvtanulásra. A tanfolyamok töredékéért kapom minimum ugyanazt, de szerintem még többet is. A magyarázó videók is nagyon könnyen megérhetőek. Szerintem professzionálisan van felépítve az egész. ”</span>
							</div>	
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
								<span style="font-size:15px;font-weight:bold">Kázmér</span>
							</div>			
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
								<span style="font-size:15px;font-weight:bold">Tanuló, Győr</span>
							</div>									
						</div>	
						<div class="mdl-grid mdl-cell mdl-cell--12-col">		
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">		
								<img  src="/images/homepage-section5-david.png" alt="" style="width:30%">
							</div>	
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">		
								<span style="font-size:15px;">“ Nyelvtanfolyamra is járok! E mellé nagyon nagy segítség az IMPREVO! A tanfolyamon általában sietnek, itt csak akkor megyek tovább, ha mindent értek. A tanfolyamon is ajánlottam már többeknek. Sokat segített megérteni dolgokat, így nem maradtam le a tanfolyamon sem! ”</span>
							</div>	
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
								<span style="font-size:15px;font-weight:bold">György</span>
							</div>			
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">		
								<span style="font-size:15px;font-weight:bold">Tanuló, Győr</span>
							</div>									
						</div>							
					</div>	
					<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--middle" style="">
						<a id="carousel_next" class="a-next-prev" href="#" style="float:left"><i class="fa fa-chevron-right"></i></a> 
					</div>					
				</div>
			</div>	
		 </div>

		<div class="footer mdl-grid mdl-cell mdl-cell--12-col" style=" margin:0;padding:0px;background:#006469;width: 100%;justify-content:center">
		   <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:80px; text-align:center">
				<span style="font-size:30px; color:#ffffff">Értesülj legfrisebb leckéinkről,  ingyenes anyagainkról</span>
		   </div>
		   <div class="mdl-cell mdl-cell mdl-cell--4-col" style="padding-top:20px;text-align:center">
				<div class="input-group mb-md">
					<input type="text" class="form-control" placeholder="Email cím">
					<span class="input-group-btn">
						<button class="btn btn-warning" type="button">FELIRATKOZOM</button>
					</span>
				</div>					
		   </div>
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%; margin:0px;padding-top:15px; text-align:center; justify-content:center">
				<div class="mdl-grid mdl-cell mdl-cell--7-col" style="color:#ffffff; margin:0px;padding-top:15px; text-align:center; justify-content:center">
					<div class="mdl-grid mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
							<span>Rólunk</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Kik vagyunk?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Mi az IMPREVO?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Shop</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Kapcsolat</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Online Angol tanfolyam</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Hallás utáni szövegértés leckék</a>
						</div>	
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Angol oktatás cégeknek</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Meg lehet tanulni az IMPREVO-val angolul?</a>
						</div>					
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span>Ingyenes anyagok</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Igeidők összefoglaló táblázat</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">20 részes ingyenes e-mailes tanfolyam</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Ingyenes Kezdő Online Angol</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Zenés leckék</a>
						</div>	
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Nyelvtani anyagok</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Blog</a>
						</div>					
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span>Segítség</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">GYIK</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Syllabus</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Milyen a tanulási stílusod?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">8+1 Tipp angoltanuláshoz</a>
						</div>						
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span>Ebookok</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Angol igeidők összefoglaló és példatár</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Angol levelek és e-mailek írása</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Tanulj meg 6 hónap alatt angolul</a>
						</div>	
						<div class="mdl-cell mdl-cell--12-col" style="padding-top:50px;">
							<span>KÖVESS MINKET</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col" style="padding-top:20px; text-align: center;font-size:25px">
							<a class="social-link" href=""><i class="fa fa-facebook" style=""></i></a>
							<a class="social-link" href=""><i class="fa fa-twitter" style=""></i></a>
							<a class="social-link" href=""><i class="fa fa-google-plus" style=""></i></a>
							<a class="social-link" href="" style="margin-right:0px"><i class="fa fa-youtube-play" ></i></a>
						</div>
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--7-col" style="text-align:left;margin:0; padding:0; padding:0px 20px 10px 20px">
					<hr style="width:100%; height:1px; background-color:#66abae;"></hr>
					<span style="color:#7fbabd">
						<a href="/" style="margin-right:20px">
							<img src="/images/logo.png">					
						</a> 
						Copyright 2016. Imprevo. Minden jog fenntartva.					
					</span>					
					<a class="link1" href="#" style="margin-left:70px;">Adatvédelem</a>
					<a class="link1" href="#" style="margin-left:15px;">Felhasználás feltételei</a>
					<a class="link1" href="#" style="margin-left:15px;">Impresszum</a>		
					<a href="/" style="float:right;margin-left:20px">
						<img src="/images/Barion_logo.png">					
					</a> 		
				</div>				
			</div>		   
		</div>			
		</div>
		
	</div>
    <div id="main_page_div" class="mdl-grid portfolio-max-width portfolio-contact" style="padding:0;">
		@if ($page['is_sidebar'])
			<div class="mdl-cell mdl-cell--9-col mdl-cell--6-col-tablet mdl-cell--3-col-phone">
				<?php echo html_entity_decode($page->content)?>
			</div>
			<div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--3-col-phone" style="margin-top: 2rem;">
				<!-- Block 1 -->
				<div class="demo-card-square mdl-card mdl-shadow--2dp">
		
					<div class="mdl-card__supporting-text">
						<img src="http://imprevo.hu/wp-content/uploads/2016/08/01_300x250_2-4.png" />
					</div>
				</div>
	
				<!-- Block 2 -->
	
				<div class="demo-card-square mdl-card mdl-shadow--2dp" style="margin-top: 1.5rem;">
			
					<h2 class="mdl-card__title-text" style="padding-left: 1rem; margin-top: 1rem; align-self: flex-start;">Hírlevél</h2>
					<div class="mdl-card__supporting-text">
						<form style="background-color: #fdfdfd; padding: 20px; border: 1px solid #ccc; border-radius: 3px;" action="http://sendy.imprevo.info/subscribe" method="POST" accept-charset="utf-8">
							<p style="text-align: center;">Iratkozz fel ingyenes anyagokért és újdonságokért!</p>
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="email" id="hirlevel">
								<label class="mdl-textfield__label" for="hirlevel">Email ...</label>
							</div>
							<input type="hidden" name="list" value="dWVr892XDx8pprHN8763yTtK6w"/>
							<input class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" style="margin-top: 5px; background: #006e73;" type="submit" name="submit" id="submit" value="Feliratkozom!" />
						</form>
					</div>
				</div>

				<!-- Block 3 -->
				<div class="demo-card-square mdl-card mdl-shadow--2dp" style="margin-top: 1.5rem;">
					<h2 class="mdl-card__title-text" style="padding-left: 1rem; margin-top: 1rem; align-self: flex-start;">Hasznos anyagaink</h2>
					<div class="mdl-card__supporting-text">

						<ul class="demo-list-icon mdl-list">
						<li class="mdl-list__item">
							<span class="mdl-list__item-primary-content">
								<i class="material-icons mdl-list__item-icon">done</i>
								<a href="#" style="text-decoration: none; color: #006e73;">20 részes ingyenes angol tanfolyam</a>
							</span>
						</li>
		
						<li class="mdl-list__item">
							<span class="mdl-list__item-primary-content">
								<i class="material-icons mdl-list__item-icon">done</i>
								<a href="#" style="text-decoration: none; color: #006e73;">Angol Igeidők Összefoglaló Táblázat</a>
							</span>
						</li>
		
						<li class="mdl-list__item">
							<span class="mdl-list__item-primary-content">
								<i class="material-icons mdl-list__item-icon">done</i>
								<a href="#" style="text-decoration: none; color: #006e73;">Tanulási stílusok – Te melyik vagy?</a>
							</span>
						</li>
		
						<li class="mdl-list__item">
							<span class="mdl-list__item-primary-content">
								<i class="material-icons mdl-list__item-icon">done</i>
								<a href="#" style="text-decoration: none; color: #006e73;">Meg lehet tanulni az IMPREVO-val angolul?</a>
							</span>
						</li>
		
						<li class="mdl-list__item">
							<span class="mdl-list__item-primary-content">
								<i class="material-icons mdl-list__item-icon">done</i>
								<a href="#" style="text-decoration: none; color: #006e73;">IMPREVO tartalomjegyzék</a>
							</span>
						</li>
		
						<li class="mdl-list__item">
							<span class="mdl-list__item-primary-content">
								<i class="material-icons mdl-list__item-icon">done</i>
								<a href="#" style="text-decoration: none; color: #006e73;">Ingyenes Anyagok</a>
							</span>
						</li>
		
						</ul>

					</div>
				</div>
			</div>	
		@else
			<?php echo html_entity_decode($page->content)?>
		@endif	
	</div>
</main>
  @if($page['is_show'])
  <div id="div-footer" style="z-index:2;background:#ffffff; border:1px solid #eee;position: absolute;left: 0;bottom: 0px;height:120px;width: 100%;padding-top:20px;">
          <ul class="mdl-mini-footer__link-list" style="justify-content:center;margin:0 auto;color: #6c937b">
              <li><a href="#">ÁSZF</a></li>
              <li><a href="#">Adatvédelem</a></li>
              <li><a href="#">Céginfó</a></li>
              <li><a href="#">Sütik</a></li>
              <li><a href="#">Online Angol Tanfolyam</a></li>
              <li><a href="#">Mi az Imprevo?</a></li>
              <li><a href="#">Kik vagyunk?</a></li>
              <li><a href="#">Árak</a></li>
              <li><a href="#">Kapcsolat</a></li>
          </ul>
          <div style="text-align: center;width: 100%; margin-top:10px; color: #6c937b;font-size:18px">
              <a href=""><i class="fa fa-facebook" style="color: #6c937b;margin-right:30px"></i></a>
              <a href=""><i class="fa fa-twitter" style="color: #6c937b;margin-right:30px"></i></a>
              <a href=""><i class="fa fa-google-plus" style="color: #6c937b;margin-right:30px"></i></a>
              <a href=""><i class="fa fa-youtube-play" style="color: #6c937b;"></i></a>
          </div>
          <div style="width: 100%;text-align: center;margin-top:10px">
              <span style="color: #6c937b">Copyright 2016. Imprevo. Minden jog fenntartva.</span>
          </div>
  </div>
  @endif
</div>


<script>
//var page_content = <?php echo json_encode($page->content)?>;
//document.getElementById('main_page_div').innerHTML = page_content;

//var full_width_content = <?php echo json_encode($page->full_width_content)?>;
//document.getElementById('full_content_div').innerHTML = full_width_content;

/*var facebook_pixel_code = <?php echo json_encode($page->facebook_pixel_code)?>;
document.getElementsByTagName("head")[0].innerHTML += '<!--Facebook Pixel Code-->';
document.getElementsByTagName("head")[0].innerHTML += facebook_pixel_code;
document.getElementsByTagName("head")[0].innerHTML += '<!--End Facebook Pixel Code-->';*/
function goLogin() {
      location.href = '/login'
}

function goProfile(e) {
  e.preventDefault();
  document.getElementById('logout-form').submit();
}




$(document).ready(function() {

    var owl = $("#owl-carousel");


    // Custom Navigation Events
    $("#carousel_next").click(function() {
		console.log('next');
		console.log(owl);
         owl.trigger('next.owl.carousel');
    });
	
    $("#carousel_prev").click(function() {
		console.log('prev');
         owl.trigger('prev.owl.carousel');
    });
	
	$('body').scroll(function(){
		var sticky = $('#header-div'),
		scroll = $('body').scrollTop();

		var top = (document.documentElement && document.documentElement.scrollTop) || 
              document.body.scrollTop || window.pageYOffset;
		console.log(scroll);
		console.log('top', top);
		if (scroll >= 100) sticky.addClass('fixed');
		else sticky.removeClass('fixed');
	});
	

});

</script>

<script>

window.onscroll = function ()  { //detect page scroll	
	var sticky = $('#header-div'),
	scroll = document.documentElement.scrollTop || document.body.scrollTop;
	console.log('window',scroll);
	if (scroll >= 100) {
		sticky.addClass('fixed');
		document.getElementById("header-div").style.padding = "5px 0px 5px 0px";
	}	
	else{ 		
		sticky.removeClass('fixed');	
		document.getElementById("header-div").style.padding = "12px 0px 12px 0px";
	}	
	
}; 

</script>
<!--Google adwords Code-->
 <?php echo html_entity_decode($page->adwords_code)?>
<!--End Google adwords Code-->
</body>
</html>
