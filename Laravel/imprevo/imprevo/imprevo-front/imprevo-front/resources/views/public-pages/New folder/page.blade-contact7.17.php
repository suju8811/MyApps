<style>
.navbar-default {
	width:100%;
	background:rgba(255,255,255,0);
	margin:0;
	border:none;

}

.navbar-default .navbar-nav{
	padding-right:15px;
}

.navbar-default .navbar-nav > li{
	
	padding-right:25px;
}

.navbar-default .navbar-nav .dropdown{
	
}

.navbar-default .navbar-nav > li > a{
	color:#ffffff;
	font-size: 14px;
	text-transform: uppercase;
}

.navbar-default .navbar-nav > li > a:hover{
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > li > a:focus{
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a{
	background:rgba(255,255,255,0);
	color:#ffffff;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a:hover{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a:focus{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu{
	margin-top:10px;
	padding:15px 20px 15px 20px;
	background:#399aed	
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li{
	padding:0;
	border-bottom:1px solid #338ad5;	
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a{
	padding:10px 0px 10px 0px ;
	color:#ffffff;	
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a:hover{
	background:#399aed;
	color:#117a97;	
}

#dropdown2:before {
	position: absolute;
	top: -9px;
	left:120px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

#dropdown1:before {
	position: absolute;
	top: -9px;
	left:60px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

.navbar-default .navbar-toggle {      
		border:none;
		padding-right:15px;
}

.navbar-default .first-nav {

}

@media (max-width: 767px) {
    .navbar-default .navbar-collapse {		
        background:#ffffff;
		float:right;
		width:50%;
    }
	
	.navbar-default .navbar-nav > li > a{		
		color:#6c937b;
	}
	.navbar-default .navbar-nav > li {		
		border-bottom: 1px solid #eeeeee;
	}
}			

#header-div-homepage
{
	width:100%;
	/*justify-content:center;*/
	padding:10px 0 10px 0;
	margin:0;
	/*height:100px;*/


}

.fixed {
	-webkit-transform: translate3d(0,0,0);
	position:fixed;
	top:0px;
	background-color:#006e73;
	-webkit-box-shadow: 0px 2px 5px 1px #004a4d;
   -moz-box-shadow: 0px 2px 5px 1px #004a4d;
        box-shadow: 0px 2px 5px 1px #004a4d;		
	z-index:999;
	padding:0px 0 0px 0;
}
#my-account-button{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #f29f00;
	width:150px;
	height:40px;
}

#my-account-button:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #d98f00;	
}

#my-account-button-logged{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #399aed;
	width:150px;
	height:40px;
}

#my-account-button-logged:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #2081d5;	
}

#try-it-button-second{
	color:#399aed;
	background-color:#ffffff;
	width:200px;
}

#try-it-button-second:hover{
	color:#399aed;
	background-color:#dfe8ef;
	font-weight:bold;
}

#scroll-div{
	color:#c0d9da;
}

#scroll-div:hover{
	color:#cde1e1;
}

.a-next-prev {
	padding-bottom:150px;
	float:right;
	color:black;
	font-size:40px;
}

.a-next-prev:hover {
	padding-bottom:150px;
	float:right;
	color:#399aed;
	font-size:40px;
}

.socialbuttons .facebook{
	width:40px;
	height:40px;
	background:url('/images/social-facebook-icon.png') center/cover;	
}
.socialbuttons .facebook:hover{
	background:url('/images/social-facebook-icon-hover.png') center/cover;	
}
.socialbuttons .twitter{
	width:40px;
	height:40px;
	background:url('/images/social-twitter-icon.png') center/cover;	
}
.socialbuttons .twitter:hover{
	background:url('/images/social-twitter-icon-hover.png') center/cover;	
}
.socialbuttons .goggle{
	width:40px;
	height:40px;
	background:url('/images/social-goggle-icon.png') center/cover;	
}
.socialbuttons .goggle:hover{
	background:url('/images/social-goggle-icon-hover.png') center/cover;	
}
.socialbuttons .insta{
	width:40px;
	height:40px;
	background:url('/images/social-youtube-icon.png') center/cover;	
}
.socialbuttons .insta:hover{
	background:url('/images/social-youtube-icon-hover.png') center/cover;	
}

.link1{
	color:#66abae;
}
.link1:hover{
	color:#ffffff;
	text-decoration:none;
}

#section2-scrolldown{
	z-index:99; 
	position:relative; 
	background:url('/images/what_is_imprevo_section1-scrolldown.png') no-repeat;
	height:50px; 
	width:50px	
    left:-webkit-calc(50% - 25px);
    left:-moz-calc(50% - 25px);	
	left:calc(50% - 25px);
}

#section2-scrolldown:hover{
	z-index:99; 
	position:relative; 
	background:url('/images/what_is_imprevo_section1-scrolldown-hover.png') no-repeat;
	height:50px; 
	width:50px	
    left:-webkit-calc(50% - 25px);
    left:-moz-calc(50% - 25px);	
	left:calc(50% - 25px);
}

.section2-line {	
	z-index:0;
	position:relative;
	left:40%;	
}

.is-focused > input {
	border:none;
}
</style>
		<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px;width:100%; background:url('/images/contact_hero_background.png') no-repeat; ">				
			<div id="header-div-homepage" class="mdl-grid mdl-cell mdl-cell--12-col">			
				<div class="mdl-cell mdl-cell mdl-cell--2-col"></div>
				<div class="mdl-cell mdl-cell mdl-cell--8-col" style="padding:0px 0px 0px 0px;">
					<nav class="navbar navbar-default"  style="">
						<div class="container-fluid">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
									<i aria-hidden="true" class="fa fa-navicon" style="color:#ffffff"></i>                       
								</button>								
								<a class="navbar-brand" href="/"><img src='/images/logo.png'></img></a>
							</div>
							<div class="collapse navbar-collapse  navbar-right" id="myNavbar">					
								<ul class="nav navbar-nav">
									<li><a id="header-span1" href="#">A Imprevz?</a></li>
									<li class="dropdown">
										<a id="header-span2" class="dropdown-toggle" data-toggle="dropdown" href="#">KURZUSOK</a>
										<ul id="dropdown1" class="dropdown-menu">
											<li><a id="header-span3" class="a-submenu" href="http://146.185.128.182/sales/course/10">Angol tanfolyam </a></li>
											<li><a id="header-span4" class="a-submenu" href="http://146.185.128.182/sales/course/11">Hétköznapi angol </a></li>
										</ul>										
									</li>
									<li><a id="header-span5" href="http://146.185.128.182/blog">Blog</a></li>
									<li class="dropdown">
										<a id="header-span6" class="dropdown-toggle" data-toggle="dropdown" href="#">Ingyenesek<!--<span class="caret"></span>--></a>
										<ul id="dropdown2" class="dropdown-menu">
											<li><a id="header-span7" class="a-submenu" href="#">20 részes email tanfolyam</a></li>
											<li><a id="header-span8" class="a-submenu" href="#">6 hónap alatt angolul</a></li>
											<li><a id="header-span9" class="a-submenu" href="#">Angol igeidők</a></li>
											<li><a id="header-span10" class="a-submenu" href="#">Angol levelek írása</a></li>
										</ul>
									</li>
								</ul>
								<ul  id="nav-right-ul" class="nav navbar-nav navbar-right">

									
								</ul>	
								<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>									
							</div>
						</div>
					</nav>								
				</div>
			</div>	
			<div id="hero-div" class="mdl-grid mdl-cell mdl-cell--12-col">
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
					<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:center">
						<span style="font-size:40px;color:#ffffff">Kapcsolat</span>
					</div>
				</div>			
			</div>	
			
		<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="margin:0;padding:0px;width: 100%;">
			<div id="section1" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="width:100%;background-color:#ebf3ef;margin:0;padding:0px;justify-content:center">
				<div class="mdl-cell mdl-cell mdl-cell--4-col" style="padding-top:30px;text-align:center">
					<span style="font-size:14px;font-weight:bold">Elakadtál, nem egyértelmű valami, kérdésed van? A leggyakoribb kérdésekrea választ összefoglaltuk Kérdések - Válaszok oldalunkon.</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;padding-bottom:20px;text-align:center">
					<button id="try-it-button" class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
						KÉRDÉSEK - VÁLASZOK
					</button>	
				</div>	
			</div>	
			<div id="section2" class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%;background-color:#ffffff;margin:0;padding:0px;padding-bottom:50px;justify-content:center;">
				<div class="mdl-grid mdl-cell mdl-cell--8-col" style="padding:0; margin:0; padding-top:100px;justify-content:center;">
					<div class="mdl-cell mdl-cell mdl-cell--8-col" style="">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
							<span style="font-size:25px;font-weight:bold">Küldj nekünk üzenetet</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--10-col" style="padding-top:20px;">
							<span style="font-weight:bold">Etiam non orci id mi luctus interdum non non urna. Aenean quis nisl tincidunt, congue metus sit amet, congue velit. Integer libero est, felis nec, volutpat it. Aenean consectetur augue a dui consectetur, non feugiat augue interdum. </span>
						</div>		
						<div class="mdl-grid mdl-cell mdl-cell--10-col" style="padding-left:0px;margin:0px;">
							<div class="mdl-cell mdl-cell mdl-cell--6-col">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
									<input class="mdl-textfield__input" type="email" id="email" name="email"  value="" autofocus>
									<label class="mdl-textfield__label" for="email" style="font-weight:bold">Név (kötelező)</label>
								</div>							
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--6-col">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
									<input class="mdl-textfield__input" type="email" id="email" name="email"  value="" autofocus>
									<label class="mdl-textfield__label" for="email" style="font-weight:bold">Email-cím (kötelező)</label>
								</div>			
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col">							
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
									<input class="mdl-textfield__input" type="email" id="email" name="email"  value="" autofocus>
									<label class="mdl-textfield__label" for="email" style="font-weight:bold">Üzenet tárgya</label>
								</div>
							</div>	
							<div class="mdl-cell mdl-cell mdl-cell--12-col">							
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
									<input class="mdl-textfield__input" type="email" id="email" name="email"  value="" autofocus>
									<label class="mdl-textfield__label" for="email" style="font-weight:bold">Üzenet</label>
								</div>
							</div>	
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:10px 0 10px 0;margin:0px;justify-content:center;"> <!--section2 button-->										
								<button id="try-it-button" class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored" style="position:relative; width:100%">
									MEGNÉZEM A TANFOLYAMOT
								</button>						
							</div>							
						</div>						
					</div>
					
					<div class="mdl-cell mdl-cell mdl-cell--4-col" style="text-align:center; background-color:#9fc3ae">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding-top:20px;">
							<img src="/images/contact-logo.png">
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding-top:15px;">
							<span style="color:#ffffff;font-size:30px">Elérhetőségeink</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding-top:10px;">
							<span style="color:#ffffff;">IMPREVO KFT</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding-top:10px;">
							<span style="color:#ffffff;">3516 Miskolc, Kaffka Margit u. 28</span>
						</div>						
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding-top:10px;">
							<span style="color:#ffffff;">+36 20 541 3799</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding-top:15px;">
							<span style="color:#ffffff;font-size:30px">Cégadatok</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding-top:10px;">
							<span style="color:#ffffff;">Cégjegyzékszám: 05-09-027716</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding-top:10px;">
							<span style="color:#ffffff;">Adószám: 25150535-2-05</span>
						</div>	
						
						<div class="socialbuttons mdl-grid mdl-cell mdl-cell--12-col" style="">
							<div class="mdl-cell mdl-cell mdl-cell--3-col mdl-cell--1-col-phone">
								<a href="https://www.facebook.com/imprevo" target="_blank">
									<div class="facebook"></div>
								</a>				
							</div>	
							<div class="mdl-cell mdl-cell mdl-cell--3-col mdl-cell--1-col-phone">
								<a href="https://twitter.com/ImprevoLearning" target="_blank">
									<div class="twitter"></div>
								</a>
							</div>	
							<div class="mdl-cell mdl-cell mdl-cell--3-col mdl-cell--1-col-phone">
								<a href="https://plus.google.com/103750741136736429559" target="_blank">
									<div class="goggle"></div>
								</a>              
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--3-col mdl-cell--1-col-phone">							
								<a href="https://www.youtube.com/channel/UC9zzm-SiiLWFX-a_M0E1QHQ" target="_blank">
									<div class="insta"></div>
								</a>
							</div>	
						</div>
					</div>
				
				</div>
			</div>			
					
			<div id="section3" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="margin:0;width:100%;background-color:#399aed;padding:40px 0px 40px 0px;justify-content:center">
				<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--4-col-phone" style="text-align:center; ">
					<span style="color:#ffffff; font-size:30px;">Próbáld ki az IMPREVO-t még ma ingyen
					</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--4-col-phone" style="text-align:center; ">
					<button id="try-it-button-second" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
						PRÓBÁLD KI INGYEN!
					</button>
				</div>	
			</div>		
		 </div>

		<div class="footer mdl-grid mdl-cell mdl-cell--12-col" style=" margin:0;padding:0px;background:#006469;width: 100%;justify-content:center">
		   <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="padding-top:80px; text-align:center">
				<span style="font-size:30px; color:#ffffff">Értesülj legfrisebb leckéinkről,  ingyenes anyagainkról</span>
		   </div>
		   <div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--hide-phone" style="padding-top:20px;text-align:center">
				<div class="input-group mb-md">
					<input type="text" class="form-control" placeholder="Email cím" style="height:45px;outline:none;border:none;">
					<span class="input-group-btn">
						<button class="btn btn-warning" type="button" style="height:45px;border-radius:0px">FELIRATKOZOM</button>
					</span>
				</div>					
		   </div>
		   <div class="mdl-cell mdl-cell mdl-cell--4-col-phone mdl-cell--hide-desktop mdl-cell--hide-tablet" style="padding-top:20px;text-align:center">
				<div class="mdl-cell mdl-cell mdl-cell--4-col-phone">
					<input type="text" class="form-control" placeholder="Email cím" style="height:45px;outline:none;border:none;">
				</div>					
				<div class="mdl-cell mdl-cell mdl-cell--4-col-phone">							
				    <button class="btn btn-warning" type="button" style="height:45px;border-radius:0px;width:100%;">FELIRATKOZOM</button>
				</div>					
		   </div>		   
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%; margin:0px;padding-top:15px; text-align:center; justify-content:center">
				<div class="mdl-grid mdl-cell mdl-cell--7-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="color:#ffffff; margin:0px;padding-top:15px; text-align:center; justify-content:center">
					<div class="mdl-grid mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
							<span>Rólunk</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Kik vagyunk?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Mi az IMPREVO?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Shop</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Kapcsolat</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Online Angol tanfolyam</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Hallás utáni szövegértés leckék</a>
						</div>	
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Angol oktatás cégeknek</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Meg lehet tanulni az IMPREVO-val angolul?</a>
						</div>					
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span>Ingyenes anyagok</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Igeidők összefoglaló táblázat</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">20 részes ingyenes e-mailes tanfolyam</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Ingyenes Kezdő Online Angol</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Zenés leckék</a>
						</div>	
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Nyelvtani anyagok</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Blog</a>
						</div>					
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span>Segítség</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">GYIK</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Syllabus</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Milyen a tanulási stílusod?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">8+1 Tipp angoltanuláshoz</a>
						</div>						
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span>Ebookok</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Angol igeidők összefoglaló és példatár</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Angol levelek és e-mailek írása</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Tanulj meg 6 hónap alatt angolul</a>
						</div>	
						<div class="mdl-cell mdl-cell--12-col" style="padding-top:50px;">
							<span>KÖVESS MINKET</span>
						</div>
						<div class="socialbuttons mdl-grid mdl-cell mdl-cell--12-col" style="">
							<div class="mdl-cell mdl-cell mdl-cell--3-col">
								<a href="https://www.facebook.com/imprevo" target="_blank">
									<div class="facebook"></div>
								</a>				
							</div>	
							<div class="mdl-cell mdl-cell mdl-cell--3-col">
								<a href="https://twitter.com/ImprevoLearning" target="_blank">
									<div class="twitter"></div>
								</a>
							</div>	
							<div class="mdl-cell mdl-cell mdl-cell--3-col">
								<a href="https://plus.google.com/103750741136736429559" target="_blank">
									<div class="goggle"></div>
								</a>              
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--3-col">							
								<a href="https://www.youtube.com/channel/UC9zzm-SiiLWFX-a_M0E1QHQ" target="_blank">
									<div class="insta"></div>
								</a>
							</div>	
						</div>
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--hide-desktop " style="justify-content:center">
				  <div class="phone-footer-accordion" style="width:100%; padding:0; margin:0; text-align:left;">
					<div class="item" style="">
						<div class="heading" style="">Rólunk</div>
						<div class="content" style="padding-left:10px;margin:0">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0">
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Kik vagyunk?</a>
								</div>	
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Mi az IMPREVO?</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Shop</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Kapcsolat</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Online Angol tanfolyam</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Hallás utáni szövegértés leckék</a>
								</div>	
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Angol oktatás cégeknek</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Meg lehet tanulni az IMPREVO-val angolul?</a>
								</div>								
							</div>
						</div>
					</div>
					<div class="item" style="">
						<div class="heading" style="">Ingyenes anyagok</div>
						<div class="content" style="padding-left:10px;margin:0">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0">
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Igeidők összefoglaló táblázat</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">20 részes ingyenes e-mailes tanfolyam</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Ingyenes Kezdő Online Angol</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Zenés leckék</a>
								</div>	
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Nyelvtani anyagok</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Blog</a>
								</div>
							</div>
						</div>
					</div>
					<div class="item" style="">
						<div class="heading" style="">Segítség</div>
						<div class="content" style="padding-left:10px;margin:0">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0">
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">GYIK</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Syllabus</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Milyen a tanulási stílusod?</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">8+1 Tipp angoltanuláshoz</a>
								</div>			
							</div>
						</div>
					</div>
					<div class="item" style="">
						<div class="heading" style="">Ebookok</div>
						<div class="content" style="padding-left:10px;margin:0">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0">
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Angol igeidők összefoglaló és példatár</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Angol levelek és e-mailek írása</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="#" style="">Tanulj meg 6 hónap alatt angolul</a>
								</div>	
							</div>
						</div>
					</div>				
				  </div>	
				  <div class="socialbuttons mdl-grid mdl-cell mdl-cell--4-col-tablet mdl-cell--3-col-phone mdl-grid-no--spacing" style="padding:0;margin:0;">	
				  	<div class="mdl-cell mdl-cell mdl-cell--1-col-phone mdl-cell--2-col-tablet" style="">
				  		<a href="https://www.facebook.com/imprevo" target="_blank">
				  			<div class="facebook"></div>
				  		</a>				
				  	</div>	
				  	<div class="mdl-cell mdl-cell mdl-cell--1-col-phone mdl-cell--2-col-tablet">
				  		<a href="https://twitter.com/ImprevoLearning" target="_blank">
				  			<div class="twitter"></div>
				  		</a>
				  	</div>	
				  	<div class="mdl-cell mdl-cell mdl-cell--1-col-phone mdl-cell--2-col-tablet">
				  		<a href="https://plus.google.com/103750741136736429559" target="_blank">
				  			<div class="goggle"></div>
				  		</a>              
				  	</div>
				  	<div class="mdl-cell mdl-cell mdl-cell--1-col-phone mdl-cell--2-col-tablet">							
				  		<a href="https://www.youtube.com/channel/UC9zzm-SiiLWFX-a_M0E1QHQ" target="_blank">
				  			<div class="insta"></div>
				  		</a>
				  	</div>
				  </div>				  
				</div>
				
				<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="text-align:center;margin:0; padding:0; padding:0px 20px 10px 20px">
					<hr style="width:100%; height:1px; background-color:#66abae;"></hr>
					<span style="color:#7fbabd">
						<a href="/" style="margin-right:20px">
							<img src="/images/logo.png">					
						</a> 
						Copyright 2016. Imprevo. Minden jog fenntartva.					
					</span>					
					<a class="link1" href="#" style="margin-left:70px;">Adatvédelem</a>
					<a class="link1" href="#" style="margin-left:15px;">Felhasználás feltételei</a>
					<a class="link1" href="#" style="margin-left:15px;">Impresszum</a>		
					<a href="/" style="margin-left:20px">
						<img src="/images/Barion_logo.png">					
					</a> 		
				</div>				
				<div class="mdl-grid mdl-cell mdl-cell--4-col-phone mdl-cell--hide-desktop mdl-cell--8-col-tablet" style="text-align:center;margin:0; padding:0; padding:0px 20px 10px 20px">										
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<a href="/" style="">
							<img src="/images/Barion_logo.png">					
						</a> 					
					</div>
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<a class="link1" href="#" style="">Adatvédelem</a>
					</div>
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">	
						<a class="link1" href="#" style="">Felhasználás feltételei</a>
					</div>	
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">	
						<a class="link1" href="#" style="">Impresszum</a>	
					</div>		
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">				
						<a href="/" style="">
							<img src="/images/logo.png">					
						</a> 										
					</div>
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">	
						<span style="color:#7fbabd">
								Copyright 2016. Imprevo. Minden jog fenntartva.
						</span>
					</div>					
				</div>				
			</div>		   
		</div>			
	</div>		
	</div>