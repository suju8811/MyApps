<style>
.navbar-default {
	width:100%;
	background:rgba(255,255,255,0);
	margin:0;
	border:none;

}

.navbar-default .navbar-nav{
	padding-right:15px;
}

.navbar-default .navbar-nav > li{

	padding-right:25px;
}

.navbar-default .navbar-nav .dropdown{

}

.navbar-default .navbar-nav > li > a{
	color:#ffffff;
	font-size: 14px;
	text-transform: uppercase;
}

.navbar-default .navbar-nav > li > a:hover{
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > li > a:focus{
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a{
	background:rgba(255,255,255,0);
	color:#ffffff;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a:hover{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a:focus{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu{
	margin-top:10px;
	padding:15px 20px 15px 20px;
	background:#399aed
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li{
	padding:0;
	border-bottom:1px solid #338ad5;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a{
	padding:10px 0px 10px 0px ;
	color:#ffffff;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a:hover{
	background:#399aed;
	color:#117a97;
}

#dropdown2:before {
	position: absolute;
	top: -9px;
	left:120px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

#dropdown1:before {
	position: absolute;
	top: -9px;
	left:60px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

.navbar-default .navbar-toggle {
		border:none;
		padding-right:15px;
}

.navbar-default .first-nav {

}

@media (max-width: 767px) {
    .navbar-default .navbar-collapse {
        background:#ffffff;
		float:right;
		width:50%;
    }

	.navbar-default .navbar-nav > li > a{
		color:#6c937b;
	}
	.navbar-default .navbar-nav > li {
		border-bottom: 1px solid #eeeeee;
	}
}

#header-div-homepage
{
	width:100%;
	padding:10px 0 10px 0;
	margin:0;
}

.fixed {
	-webkit-transform: translate3d(0,0,0);
	position:fixed;
	top:0px;
	background-color:#006e73;
	-webkit-box-shadow: 0px 2px 5px 1px #004a4d;
   -moz-box-shadow: 0px 2px 5px 1px #004a4d;
        box-shadow: 0px 2px 5px 1px #004a4d;
	z-index:999;
	padding:0px 0 0px 0;
}
#my-account-button{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #f29f00;
	width:150px;
	height:40px;
}

#my-account-button:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #d98f00;
}

#my-account-button-logged{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #399aed;
	width:150px;
	height:40px;
}

#my-account-button-logged:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #2081d5;
}

#try-it-button{
	background-color:#399aed;
}
#try-it-button:hover{
	background-color:#2081d5;
}
#try-it-button-second{
	color:#399aed;
	background-color:#ffffff;
	width:200px;
}

#try-it-button-second:hover{
	color:#399aed;
	background-color:#dfe8ef;
	font-weight:bold;
}

#scroll-div{
	color:#c0d9da;
}

#scroll-div:hover{
	color:#cde1e1;
}

.a-next-prev {
	padding-bottom:150px;
	float:right;
	color:black;
	font-size:40px;
}

.a-next-prev:hover {
	padding-bottom:150px;
	float:right;
	color:#399aed;
	font-size:40px;
}

.socialbuttons .facebook{
	width:40px;
	height:40px;
	background:url('/images/social-facebook-icon.png') center/cover;
}
.socialbuttons .facebook:hover{
	background:url('/images/social-facebook-icon-hover.png') center/cover;
}
.socialbuttons .twitter{
	width:40px;
	height:40px;
	background:url('/images/social-twitter-icon.png') center/cover;
}
.socialbuttons .twitter:hover{
	background:url('/images/social-twitter-icon-hover.png') center/cover;
}
.socialbuttons .goggle{
	width:40px;
	height:40px;
	background:url('/images/social-goggle-icon.png') center/cover;
}
.socialbuttons .goggle:hover{
	background:url('/images/social-goggle-icon-hover.png') center/cover;
}
.socialbuttons .insta{
	width:40px;
	height:40px;
	background:url('/images/social-youtube-icon.png') center/cover;
}
.socialbuttons .insta:hover{
	background:url('/images/social-youtube-icon-hover.png') center/cover;
}

.link1{
	color:#66abae;
}
.link1:hover{
	color:#ffffff;
	text-decoration:none;
}

#section2-scrolldown{
	z-index:99;
	position:relative;
	background:url('/images/what_is_imprevo_section1-scrolldown.png') no-repeat;
	height:50px;
	width:50px
    left:-webkit-calc(50% - 25px);
    left:-moz-calc(50% - 25px);
	left:calc(50% - 25px);
}

#section2-scrolldown:hover{
	z-index:99;
	position:relative;
	background:url('/images/what_is_imprevo_section1-scrolldown-hover.png') no-repeat;
	height:50px;
	width:50px
    left:-webkit-calc(50% - 25px);
    left:-moz-calc(50% - 25px);
	left:calc(50% - 25px);
}

.section2-line {
	position:relative;
	left:40%;
	width:250px;
	height:375px;
	top:-100px;
	z-index:1;
	overflow:visible
}

span {
	color:#3b4a51;
}

span.section2-span2{
	font-size:28px;
	font-weight:310;
	line-height:40px;
}
iframe {
	width:100%;
}

@media (max-width: 1850px) {
	.footer {
		width:80%;
	}
}
@media (max-width: 1345px) {
	.footer {
		width:90%;
	}
}
@media (max-width: 1185px) {
	.footer {
		width:100%;
	}
}
@media (max-width: 769px) {
    .navbar-default .navbar-collapse {
        background:#ffffff;
		float:right;
		width:50%;
    }

	#hero-div{
		margin:0px;
	}


	#header-div-homepage
	{
		padding:5px 0 0px 0 !important;
		margin:0;
	}

	#nav-bar{
		margin:0;
	}

	.section1-mark{
		width:60% !important;
	}

	.navbar-default .navbar-nav > li > a{
		color:#6c937b;
	}
	.navbar-default .navbar-nav > li {
		border-bottom: 1px solid #eeeeee;
	}

	#hero-div-span1{
		font-size:25px !important;

	}
	#section5 div{
		text-align:center !important;
		padding:0 !important;
	}

	#try-it-span{
		font-size:25px !important;
	}

	#footer-span1{
		font-size:20px !important;
	}

	#footer-div1{
		padding-top: 50px !important;
	}

	#section1-div1{
		padding-top:30px !important;
	}

	#section2-div1{
		padding-top:50px !important;
	}

	span.section2-span2{
		font-size:20px !important;
		font-weight:400;
		line-height:30px;
	}
}
</style>
		<div id="header-background-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px;width:100%; background:url('/images/what_is_imprevo_hero_background.png') no-repeat; ">
			<div id="header-div-homepage" class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing">
				<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
				<div id="nav-bar" class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0px 0px 0px 0px;">
					<nav class="navbar navbar-default"  style="">
						<div class="container-fluid">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
									<i aria-hidden="true" class="fa fa-navicon" style="color:#ffffff"></i>
								</button>
								<a class="navbar-brand" href="/"><img src='/images/logo.png'></img></a>
							</div>
							<div class="collapse navbar-collapse  navbar-right" id="myNavbar">
								<ul class="nav navbar-nav">
									<li><a id="header-span1" href="https://imprevo.hu/what-is-imprevo">A Imprevz?</a></li>
									<li class="dropdown">
										<a id="header-span2" class="dropdown-toggle" data-toggle="dropdown" href="#">KURZUSOK</a>
										<ul id="dropdown1" class="dropdown-menu">
											<li><a id="header-span3" class="a-submenu" href="https://imprevo.hu/sales/course/10">Angol tanfolyam </a></li>
											<li><a id="header-span4" class="a-submenu" href="https://imprevo.hu/sales/course/11">Hétköznapi angol </a></li>
										</ul>
									</li>
									<li><a id="header-span5" href="https://imprevo.hu/blog">Blog</a></li>
									<li class="dropdown">
										<a id="header-span6" class="dropdown-toggle" data-toggle="dropdown2" href="https://imprevo.hu/ingyenes-tartalmak">Ingyenesek<!--<span class="caret"></span>--></a>
										<ul id="dropdown2" class="dropdown-menu">
											<li><a id="header-span7" class="a-submenu" href="https://imprevo.hu/20-részes-ingyenes-angol-tanfolyam">20 részes email tanfolyam</a></li>
											<li><a id="header-span8" class="a-submenu" href="https://imprevo.hu/tanulj-meg-angolul-6-hónap-alatt-az-imprevo-val">6 hónap alatt angolul</a></li>
											<li><a id="header-span9" class="a-submenu" href="https://imprevo.hu/angol-igeidők-ebook">Angol igeidők</a></li>
											<li><a id="header-span10" class="a-submenu" href="https://imprevo.hu/angol-levelek-írása">Angol levelek írása</a></li>
										</ul>
									</li>
								</ul>
								<ul  id="nav-right-ul" class="nav navbar-nav navbar-right">


								</ul>
								<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
							</div>
						</div>
					</nav>
				</div>
			</div>
			<div id="hero-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing" style="width:100%">
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center; margin:0px;padding:0px;padding-top:80px;">
					<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
						<span id="hero-div-span1" style="font-weight:300; line-height:45px;font-size:40px;color:#ffffff">Az IMPREVO egy könnyen használható,egyenletes haladást biztosító interaktív online angol tanfolyam.</span>
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center; margin:0px;padding:0px">
					<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
						<span style="color:#ffffff">Velünk megtanulhatsz angolul, mert teljeskörűen lefedjük a különböző nyelvi készségeket.</span>
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center; margin:0px; padding:0px; padding-bottom:100px">
					<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
						<button id="try-it-button" onclick="location.href='/sales/course/10'" class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
							MEGNÉZEM A TANFOLYAMOT
						</button>
					</div>
				</div>
			</div>

		<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;width: 100%;">
			<div id="section1" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="justify-content:center;width:100%;background-color:#ffffff;margin:0;padding:0px;">
				<div id="section1-div1" class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:100px;text-align:center">
					<span style="font-size:30px;">Minden, amire szükséged van az angolozáshoz</span>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--8-col" style="padding:20px 0px 40px 0px;text-align:center">
					<span style="font-size:20px">Lorem ipsum dolor sit amet, consectetur adipiscing elit vivamus hendrerit augue tincidunt purus facilisis dictum nec ut elit enean a feugiat sem hendrerit erat, id pretium sem ulla cursus suscipit ornare</span>
				</div>

				<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" >
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;width:100%;">
						<div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;">
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;">
								<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="text-align:center;">
									<img class="section1-mark" src="/images/what_is_imprevo_section1-icon1.png" style="width:100%;">
								</div>
								<div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-top:0;margin-top:0">
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:18px; font-weight:bold">HALLOTT SZÖVEGÉRTÉS</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:18px">Autentikus amerikai és angol hanganyag nagy mennyiségben, kezdőknek tagoltan érthetően, haladóknak normál sebességgel.</span>
									</div>
								</div>
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;">
									<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone"></div>
									<div class="mdl-cell mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone">
										<img src="/images/what_is_imprevo_section1-tablet1.png" style="width:100%;">
									</div>
							</div>
						</div>

						<div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;">
						  <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;">
							<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="text-align:center;">
								<img class="section1-mark" src="/images/what_is_imprevo_section1-icon2.png" style="width:100%;">
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-top:0;margin-top:0">
								<div class="mdl-cell mdl-cell mdl-cell--12-col">
									<span style="font-size:18px; font-weight:bold">OLVASOTT SZÖVEGÉRTÉS</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--12-col">
									<span style="font-size:18px">Összefüggő szövegek érdekes témákkal, folyamatosan nehezedő felépítéssel, mindig egy lecke témájához igazítva feladatokkal.</span>
								</div>
							</div>
						  </div>
						  <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;">
						  	<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;padding-top:30px;">
						  	<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone"></div>
						  	<div class="mdl-cell mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone">
						  			<img src="/images/what_is_imprevo_section1-tablet2.png" style="width:100%;">
						  		</div>
						  	</div>
						  </div>
						</div>
						<div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;">
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;">
								<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="text-align:center;">
									<img class="section1-mark" src="/images/what_is_imprevo_section1-icon3.png" style="width:100%;">
								</div>
								<div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-top:0;margin-top:0">
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:18px; font-weight:bold">NYELVTAN</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:18px">Több mint 600 videóban foglaltunk össze közérthetően a szabályos beszéd és írás alapját képező nyelvtant, szituációs videókkal kiegészítve.</span>
									</div>
								</div>
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--order-2-phone" style="margin:0;padding:0px;">
									<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;">
									<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone"></div>
									<div class="mdl-cell mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone">
											<img src="/images/what_is_imprevo_section1-tablet3.png" style="width:100%;">
										</div>
									</div>
							</div>
						</div>
					</div>

					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;width:100%;">
						<div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;">
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;padding-top:30px;">
								<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone">
									<img class="section1-mark" src="/images/what_is_imprevo_section1-icon4.png" style="width:100%;">
								</div>
								<div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-top:0;margin-top:0">
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:18px; font-weight:bold">ÍRÁSKÉSZSÉG</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:18px">Videós elemzéseken keresztül mutatjuk be a szabályos és szép angol írás minden fortéját, így lesz mire építened.</span>
									</div>
								</div>
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--order-2-phone" style="margin:0;padding:0px;">
								<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;padding-top:30px;">
									<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone"></div>
									<div class="mdl-cell mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone">
										<img src="/images/what_is_imprevo_section1-tablet5.png" style="width:100%;">
									</div>
								</div>
							</div>
						</div>

						<div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;">
						    <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;padding-top:30px;">
								<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone">
									<img class="section1-mark" src="/images/what_is_imprevo_section1-icon5.png" style="width:100%;">
								</div>
								<div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-top:0;margin-top:0">
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:18px; font-weight:bold">SZÓKINCS</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:18px">Rengeteg játék segít elsajátítani több mint 5000 szót, ami már meghaladja a középfokú angol-tudás követelményeit is.</span>
									</div>
								</div>
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;">
									<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;padding-top:30px;">
									<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone"></div>
									<div class="mdl-cell mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone">
											<img src="/images/what_is_imprevo_section1-tablet6.png" style="width:100%;">
										</div>
									</div>
							</div>
						</div>
						<div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;">
						    <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;padding-top:30px;">
								<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone">
									<img class="section1-mark"  src="/images/what_is_imprevo_section1-icon6.png" style="width:100%;">
								</div>
								<div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-top:0;margin-top:0">
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:18px; font-weight:bold">FUNKCIONÁLIS NYELV</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-size:18px">Mindennapi szituációkkal mutatunk be olyan ökölszabályokat, amiket már az első alkalommal jól tudsz használni.</span>
									</div>
								</div>
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;">
								<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;padding-top:30px;">
									<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone"></div>
									<div class="mdl-cell mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone">
										<img src="/images/what_is_imprevo_section1-tablet6.png" style="width:100%;">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0px; margin:0px;height:25px; text-align:center;justify-content:center">
					<div id="section2-scrolldown">
					</div>
				</div>
			</div>
			<div id="section2" class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%;background-color:#ebf3ef;margin:0;padding:0px;justify-content:center;">
				<div id="section2-div1" class="mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0; margin:0; padding-top:100px;justify-content:center;">
					<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
						<span style="font-size:40px;">Pellentesque congue elementum sure</span>
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--10-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
						<span style="font-size:20px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit vivamus hendrerit augue tincidunt purus facilisisdictum nec ut elit enean a feugiat sem hendrerit erat, id pretium sem ulla cursus</span>
					</div>
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid-no--spacing" style="padding:0px; padding-top:50px;margin:0px;width:100%"> <!-- * -->
						<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-desktop mdl-cell--hide-tablet">
							<span style="font-size:14px; color:#399aed">SZÓKINCS</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-desktop mdl-cell--hide-tablet">
							<span class="section2-span2" style="font-size:28px;">Hallott szövegértési feladataink minden tudásszinten megfelelő terhelést biztosítanak!</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;z-index:2">
							<iframe src="https://player.vimeo.com/video/141313935" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>

						<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="">
							<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone">
								<span style="font-size:14px; color:#399aed">SZÓKINCS</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone">
								<span class="section2-span2" style="font-size:28px;">Hallott szövegértési feladataink minden tudásszinten megfelelő terhelést biztosítanak!</span>
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Több ezer szavas szókincs</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Minden szóhoz anyanyelvi kiejtés</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Minden szóhoz vizuális segédlet</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Interaktív játékok (például Memóriakártyák)</span>
								</div>
							</div>
						</div>
					</div> <!-- * -->

					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone mdl-cell--middle" style="z-index:0;padding:0px;margin:0px;height:200px">
						<img class="section2-line" src="/images/what_is_imprevo_section2-line1.png" style="">
					</div>

					<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px;margin:0px;"> <!-- * -->

						<div class="mdl-cell mdl-cell mdl-cell--6-col" style="">
							<div class="mdl-cell mdl-cell mdl-cell--12-col">
								<span style="font-size:14px; color:#399aed">NYELVTAN</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col">
								<span class="section2-span2" style="font-size:28px;">Az IMPREVO sok száz rövid nyelvtani magyarázó videót tartalmaz és persze rengeteg feladatot, hogy a monotóniát folyamatosan megtörjék.</span>
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone">
								<div class="mdl-cell mdl-cell mdl-cell--1-col">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col">
									<span style="font-size:15px;">Több ezer szavas szókincs</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col">
									<span style="font-size:15px;">Minden szóhoz anyanyelvi kiejtés</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col">
									<span style="font-size:15px;">Minden szóhoz vizuális segédlet</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col">
									<span style="font-size:15px;">Interaktív játékok (például Memóriakártyák)</span>
								</div>
							</div>
						</div>

						<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet" style="text-align:center;z-index:2">
							<iframe src="https://player.vimeo.com/video/142531344" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
						<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-desktop mdl-cell--hide-tablet">
							<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
								<img src="/images/what_is_imprevo_section2-ticket.png">
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
								<span style="font-size:15px;">Több ezer szavas szókincs</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
								<img src="/images/what_is_imprevo_section2-ticket.png">
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
								<span style="font-size:15px;">Minden szóhoz anyanyelvi kiejtés</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
								<img src="/images/what_is_imprevo_section2-ticket.png">
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
								<span style="font-size:15px;">Minden szóhoz vizuális segédlet</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
								<img src="/images/what_is_imprevo_section2-ticket.png">
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
								<span style="font-size:15px;">Interaktív játékok (például Memóriakártyák)</span>
							</div>
						</div>
					</div> <!-- * -->

					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="padding:0px;margin:0px;height:200px">
						<img class="section2-line" src="/images/what_is_imprevo_section2-line2.png">
					</div>

					<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px;margin:0px;"> <!-- * 3-->
						<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-desktop">
							<span style="font-size:14px; color:#399aed">OLVASOTT SZÖVEGÉRTÉS</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-desktop">
							<span class="section2-span2" style="font-size:28px;">Minden angol szöveg magyar fordítását is megtalálod az egyes leckék végén, sőt, még angol hang-anyagot is találsz, hogy az érthető kiejtés elsajátításához is használhasd.</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet" style="text-align:center;z-index:2">
							<iframe src="https://player.vimeo.com/video/142531343" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>

						<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet" style="">
							<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone">
								<span style="font-size:14px; color:#399aed">OLVASOTT SZÖVEGÉRTÉS</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone">
								<span class="section2-span2" style="font-size:28px;">Minden angol szöveg magyar fordítását is megtalálod az egyes leckék végén, sőt, még angol hang-anyagot is találsz, hogy az érthető kiejtés elsajátításához is használhasd.</span>
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col ">
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Több ezer szavas szókincs</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Minden szóhoz anyanyelvi kiejtés</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Minden szóhoz vizuális segédlet</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Interaktív játékok (például Memóriakártyák)</span>
								</div>
							</div>
						</div>
					</div> <!-- * 3-->

					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="padding:0px;margin:0px;height:200px">
						<img class="section2-line" src="/images/what_is_imprevo_section2-line1.png">
					</div>

					<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px;margin:0px;"> <!-- * 4-->
						<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet" style="">
							<div class="mdl-cell mdl-cell mdl-cell--12-col">
								<span style="font-size:14px; color:#399aed">NYELVTAN</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col">
								<span class="section2-span2" style="font-size:28px;">Az IMPREVO sok száz rövid nyelvtani magyarázó videót tartalmaz és persze rengeteg feladatot, hogy a monotóniát folyamatosan megtörjék.</span>
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone">
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet">
									<span style="font-size:15px;">Több ezer szavas szókincs</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet">
									<span style="font-size:15px;">Minden szóhoz anyanyelvi kiejtés</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col">
									<span style="font-size:15px;">Minden szóhoz vizuális segédlet</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col">
									<span style="font-size:15px;">Interaktív játékok (például Memóriakártyák)</span>
								</div>
							</div>
						</div>

						<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet" style="text-align:center;z-index:2">
							<iframe src="https://player.vimeo.com/video/142531344" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-desktop">
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Több ezer szavas szókincs</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Minden szóhoz anyanyelvi kiejtés</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Minden szóhoz vizuális segédlet</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Interaktív játékok (például Memóriakártyák)</span>
								</div>
							</div>
					</div> <!-- *4 -->

					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="padding:0px;margin:0px;height:200px">
						<img class="section2-line" src="/images/what_is_imprevo_section2-line2.png">
					</div>

					<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px;margin:0px;"> <!-- * 5-->
							<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-desktop">
								<span style="font-size:14px; color:#399aed">OLVASOTT SZÖVEGÉRTÉS</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-desktop">
								<span class="section2-span2" style="font-size:28px;">Minden angol szöveg magyar fordítását is megtalálod az egyes leckék végén, sőt, még angol hang-anyagot is találsz, hogy az érthető kiejtés elsajátításához is használhasd.</span>
							</div>
						<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet" style="text-align:center;z-index:2">
							<iframe src="https://player.vimeo.com/video/142531343" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>

						<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet" style="">
							<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone">
								<span style="font-size:14px; color:#399aed">OLVASOTT SZÖVEGÉRTÉS</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone">
								<span class="section2-span2" style="font-size:28px;">Minden angol szöveg magyar fordítását is megtalálod az egyes leckék végén, sőt, még angol hang-anyagot is találsz, hogy az érthető kiejtés elsajátításához is használhasd.</span>
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col">
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Több ezer szavas szókincs</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Minden szóhoz anyanyelvi kiejtés</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Minden szóhoz vizuális segédlet</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
									<span style="font-size:15px;">Interaktív játékok (például Memóriakártyák)</span>
								</div>
							</div>
						</div>
					</div> <!-- * 5-->

					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="padding:0px;margin:0px;height:200px">
						<img class="section2-line" src="/images/what_is_imprevo_section2-line1.png">
					</div>

					<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px;margin:0px;"> <!-- * 6-->
						<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet" style="">
							<div class="mdl-cell mdl-cell mdl-cell--12-col">
								<span style="font-size:14px; color:#399aed">NYELVTAN</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col">
								<span class="section2-span2" style="font-size:28px;">Az IMPREVO sok száz rövid nyelvtani magyarázó videót tartalmaz és persze rengeteg feladatot, hogy a monotóniát folyamatosan megtörjék.</span>
							</div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone">
								<div class="mdl-cell mdl-cell mdl-cell--1-col">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col">
									<span style="font-size:15px;">Több ezer szavas szókincs</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col">
									<span style="font-size:15px;">Minden szóhoz anyanyelvi kiejtés</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col">
									<span style="font-size:15px;">Minden szóhoz vizuális segédlet</span>
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--1-col">
									<img src="/images/what_is_imprevo_section2-ticket.png">
								</div>
								<div class="mdl-cell mdl-cell mdl-cell--11-col">
									<span style="font-size:15px;">Interaktív játékok (például Memóriakártyák)</span>
								</div>
							</div>
						</div>

						<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet" style="text-align:center; z-index:2">
							<iframe src="https://player.vimeo.com/video/142531344" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>

						<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-desktop">
							<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
								<img src="/images/what_is_imprevo_section2-ticket.png">
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
								<span style="font-size:15px;">Több ezer szavas szókincs</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
								<img src="/images/what_is_imprevo_section2-ticket.png">
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
								<span style="font-size:15px;">Minden szóhoz anyanyelvi kiejtés</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
								<img src="/images/what_is_imprevo_section2-ticket.png">
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
								<span style="font-size:15px;">Minden szóhoz vizuális segédlet</span>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone">
								<img src="/images/what_is_imprevo_section2-ticket.png">
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--11-col mdl-cell--7-col-tablet mdl-cell--3-col-phone">
								<span style="font-size:15px;">Interaktív játékok (például Memóriakártyák)</span>
							</div>
						</div>
					</div> <!-- *6 -->
					<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:50px 0 50px 0;margin:0px;justify-content:center;"> <!--section2 button-->
						<button id="try-it-button" onclick="location.href='/sales/course/10'"class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
							MEGNÉZEM A TANFOLYAMOT
						</button>
					</div>
				</div>
			</div>

			<div id="section5" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;width:100%;background-color:#399aed;padding:40px 0px 40px 0px;justify-content:center">
				<div class="mdl-grid mdl-cell mdl-cell--8-col" style="justify-content:center">
					<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--middle mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center; ">
						<span id="try-it-span" style="color:#ffffff; font-size:40px;">Próbáld ki az IMPREVO-t még ma ingyen
						</span>
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;">
						<button id="try-it-button-second" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
							PRÓBÁLD KI INGYEN!
						</button>
					</div>
				</div>
			</div>
		 </div>
	<!---footer---------------->
		  		<div class="footer mdl-grid mdl-cell mdl-cell--12-col" style=" margin:0;padding:0px;background:#006469;width: 100%;justify-content:center">
		   <form action="http://sendy.imprevo.info/subscribe" method="POST" accept-charset="utf-8" style="text-align:center;width:100%;margin:0;padding:0">
				<input type="hidden" name="list" value="dWVr892XDx8pprHN8763yTtK6w"/>
				<div id="footer-div1" class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="padding-top:80px; text-align:center">
						<span id="footer-span1" style="font-size:30px; color:#ffffff">Értesülj legfrisebb leckéinkről,  ingyenes anyagainkról</span>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0; padding:0; width: 100%;justify-content:center">
					<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="padding-top:20px;text-align:center">
						<div class="input-group mb-md">
							<input type="text" class="form-control" placeholder="Email cím" style="height:45px;outline:none;border:none;">
							<span class="input-group-btn">
								<button class="btn btn-warning" type="button" style="height:45px;border-radius:0px">FELIRATKOZOM</button>
							</span>
						</div>
					</div>
				</div>
		   </form>
		   <div class="mdl-cell mdl-cell mdl-cell--4-col-phone mdl-cell--hide-desktop mdl-cell--hide-tablet" style="padding-top:20px;text-align:center">
				<div class="mdl-cell mdl-cell mdl-cell--4-col-phone">
					<input type="text" class="form-control" placeholder="Email cím" style="height:45px;outline:none;border:none;">
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--4-col-phone">
				    <button class="btn btn-warning" type="button" style="height:45px;border-radius:0px;width:100%;">FELIRATKOZOM</button>
				</div>
		   </div>

			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%; margin:0px;padding-top:15px; text-align:center; justify-content:center">
				<div class="footer mdl-grid mdl-cell mdl-cell--7-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="color:#ffffff; margin:0px;padding-top:15px; text-align:center; justify-content:center">
					<div class="mdl-grid mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
							<span style="color:#ffffff">Rólunk</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/kik-vagyunk" style="">Kik vagyunk?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/what-is-imprevo" style="">Mi az IMPREVO?</a>
						</div>

						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/contact-page" style="">Kapcsolat</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/sales/course/10" style="">Online Angol tanfolyam</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/sales/course/11" style="">Hallás utáni szövegértés leckék</a>
						</div>

						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/meg-lehet-tanulni-az-imprevo-val-angolul" style="">Meg lehet tanulni az IMPREVO-val angolul?</a>
						</div>
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span style="color:#ffffff">Ingyenes anyagok</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/angol-igeidők-összefoglaló-táblázat" style="">Igeidők összefoglaló táblázat</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/20-részes-ingyenes-angol-tanfolyam" style="">Ingyenes Kezdő Online Angol</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/blog/category/Zenés%20leckék" style="">Zenés leckék</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/ingyenes-tartalmak" style="">Nyelvtani anyagok</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/blog" style="">Blog</a>
						</div>
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span style="color:#ffffff">Segítség</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/gyik" style="">GYIK</a>
						</div>

						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/blog/tanulási-stílusok" style="">Milyen a tanulási stílusod?</a>
						</div>

					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span style="color:#ffffff">Ebookok</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/angol-igeidők-ebook" style="">Angol igeidők összefoglaló és példatár</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/angol-levelek-írása" style="">Angol levelek és e-mailek írása</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="https://imprevo.hu/tanulj-meg-angolul-6-hónap-alatt-az-imprevo-val" style="">Készülj fel a nyelvvizsgára 6 hónap alatt</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col" style="padding-top:50px;">
							<span style="color:#ffffff">KÖVESS MINKET</span>
						</div>
						<div class="socialbuttons mdl-grid mdl-cell mdl-cell--12-col" style="">
							<div class="mdl-cell mdl-cell mdl-cell--3-col">
								<a href="https://www.facebook.com/imprevo" target="_blank">
									<div class="facebook"></div>
								</a>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--3-col">
								<a href="https://twitter.com/ImprevoLearning" target="_blank">
									<div class="twitter"></div>
								</a>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--3-col">
								<a href="https://plus.google.com/103750741136736429559" target="_blank">
									<div class="goggle"></div>
								</a>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--3-col">
								<a href="https://www.youtube.com/channel/UC9zzm-SiiLWFX-a_M0E1QHQ" target="_blank">
									<div class="insta"></div>
								</a>
							</div>
							<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center;padding-top:10px">
								<img src="/images/footer-barion-logo.png">
							</div>
						</div>
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--hide-desktop " style="justify-content:center">
				  <div class="phone-footer-accordion" style="width:100%; padding:0; margin:0; text-align:left;">
					<div class="item" style="">
						<div class="heading" style="">Rólunk</div>
						<div class="content" style="padding-left:10px;margin:0">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0">
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/kik-vagyunk" style="">Kik vagyunk?</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/what-is-imprevo" style="">Mi az IMPREVO?</a>
								</div>

								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/contact-page" style="">Kapcsolat</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/sales/course/10" style="">Online Angol tanfolyam</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/sales/course/11" style="">Hallás utáni szövegértés leckék</a>
								</div>

								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/meg-lehet-tanulni-az-imprevo-val-angolul" style="">Meg lehet tanulni az IMPREVO-val angolul?</a>
								</div>
							</div>
						</div>
					</div>
					<div class="item" style="">
						<div class="heading" style="">Ingyenes anyagok</div>
						<div class="content" style="padding-left:10px;margin:0">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0">
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/angol-igeidők-összefoglaló-táblázat" style="">Igeidők összefoglaló táblázat</a>
								</div>

								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/20-részes-ingyenes-angol-tanfolyam" style="">Ingyenes Kezdő Online Angol</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/blog/category/Zenés%20leckék" style="">Zenés leckék</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/ingyenes-tartalmak" style="">Nyelvtani anyagok</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/blog" style="">Blog</a>
								</div>
							</div>
						</div>
					</div>
					<div class="item" style="">
						<div class="heading" style="">Segítség</div>
						<div class="content" style="padding-left:10px;margin:0">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0">
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/gyik" style="">GYIK</a>
								</div>

								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/blog/tanulási-stílusok" style="">Milyen a tanulási stílusod?</a>
								</div>

							</div>
						</div>
					</div>
					<div class="item" style="">
						<div class="heading" style="">Ebookok</div>
						<div class="content" style="padding-left:10px;margin:0">
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0">
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/angol-igeidők-ebook" style="">Angol igeidők összefoglaló és példatár</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/angol-levelek-írása" style="">Angol levelek és e-mailek írása</a>
								</div>
								<div class="mdl-cell mdl-cell--12-col">
									<a class="link1" href="https://imprevo.hu/tanulj-meg-angolul-6-hónap-alatt-az-imprevo-val" style="">Készülj fel a nyelvvizsgára 6 hónap alatt</a>
								</div>
							</div>
						</div>
					</div>
				  </div>
				  <div class="socialbuttons mdl-grid mdl-cell mdl-cell--4-col-tablet mdl-cell--3-col-phone mdl-grid-no--spacing" style="padding:0;margin:0;">
				  	<div class="mdl-cell mdl-cell mdl-cell--1-col-phone mdl-cell--2-col-tablet" style="">
				  		<a href="https://www.facebook.com/imprevo" target="_blank">
				  			<div class="facebook"></div>
				  		</a>
				  	</div>
				  	<div class="mdl-cell mdl-cell mdl-cell--1-col-phone mdl-cell--2-col-tablet">
				  		<a href="https://twitter.com/ImprevoLearning" target="_blank">
				  			<div class="twitter"></div>
				  		</a>
				  	</div>
				  	<div class="mdl-cell mdl-cell mdl-cell--1-col-phone mdl-cell--2-col-tablet">
				  		<a href="https://plus.google.com/103750741136736429559" target="_blank">
				  			<div class="goggle"></div>
				  		</a>
				  	</div>
				  	<div class="mdl-cell mdl-cell mdl-cell--1-col-phone mdl-cell--2-col-tablet">
				  		<a href="https://www.youtube.com/channel/UC9zzm-SiiLWFX-a_M0E1QHQ" target="_blank">
				  			<div class="insta"></div>
				  		</a>
				  	</div>
					<div class="mdl-cell mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="padding-top:10px; text-align:center">
						<img src="/images/footer-barion-logo.png">
					</div>
				  </div>
				</div>

				<div class="footer mdl-cell mdl-cell mdl-cell--7-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="text-align:center;margin:0; padding:0; padding:0px 20px 10px 20px">
					<hr style="width:100%; border:none;height:1px; background-color:#66abae;"></hr>
					<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:left;width:100%">
					<span style="color:#7fbabd">
						<a href="/" style="margin-right:20px">
							<img src="/images/logo.png">
						</a>
						Copyright 2017. Imprevo. Minden jog fenntartva.
					</span>
					<a class="link1" href="https://imprevo.hu/céginformáció" style="float:right;">Impresszum</a>
					<a class="link1" href="https://imprevo.hu/aszf" style="float:right;margin-right:15px;">Felhasználás feltételei</a>
					<a class="link1" href="https://imprevo.hu/adatvedelem" style="float:right;margin-right:15px;">Adatvédelem</a>

					<!--<a href="/" style="margin-left:20px">
						<img src="/images/Barion_logo.png">
					</a> -->
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--4-col-phone mdl-cell--hide-desktop mdl-cell--8-col-tablet" style="text-align:center;margin:0; padding:0; padding:0px 20px 10px 20px">
					<!--<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<a href="/" style="">
							<img src="/images/Barion_logo.png">
						</a>
					</div>-->
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<a class="link1" href="https://imprevo.hu/adatvedelem" style="">Adatvédelem</a>
					</div>
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<a class="link1" href="https://imprevo.hu/aszf" style="">Felhasználás feltételei</a>
					</div>
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<a class="link1" href="https://imprevo.hu/céginformáció" style="">Impresszum</a>
					</div>
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<a href="/" style="">
							<img src="/images/logo.png">
						</a>
					</div>
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center">
						<span style="color:#7fbabd">
								Copyright 2017. Imprevo. Minden jog fenntartva.
						</span>
					</div>
				</div>
			</div>
		</div>
<!---footer---------------->

	</div>
	</div>
