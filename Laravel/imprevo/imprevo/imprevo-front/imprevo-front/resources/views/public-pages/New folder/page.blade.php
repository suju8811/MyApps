<!DOCTYPE html>
<html lang="en" style="position: relative;min-height: 100%;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="{{$page->seo_description}}">
    <meta name="keywords" content="{{$page->seo_keywords}}">
    <title>{{$page->seo_title}}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
   
    <link rel="stylesheet" href="/css/material.min.css" />

    <link rel="stylesheet" href="/css/styles.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="{{ Config::get('RELATIVE_URL') }}/assets/vendor/pnotify/pnotify.custom.css" />
    <link rel="stylesheet" type="text/css" href="http://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.css" />
	<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	
    <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- Specific Page Vendor CSS -->
	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue-light_blue.min.css" />
	
	<link rel="stylesheet" href="assets/vendor/owl.carousel/assets/owl.carousel.css" />
	<link rel="stylesheet" href="assets/vendor/owl.carousel/assets/owl.theme.default.css" />	
	<!-- Theme CSS -->
	<link rel="stylesheet" href="assets/stylesheets/theme.css" />

	<!-- Skin CSS -->
	<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">	
	
	<script src="/assets/vendor/modernizr/modernizr.js"></script>

	<script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.js"></script>
    <script src="/assets/vendor/pnotify/pnotify.custom.js"></script>	

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
	<script src="assets/vendor/owl.carousel/owl.carousel.js"></script>
	

	
	<script src="assets/javascripts/theme.js"></script>
	
	<!-- Theme Custom -->
	<script src="assets/javascripts/theme.custom.js"></script>
	
	<!-- Theme Initialization Files -->
	<script src="assets/javascripts/theme.init.js"></script>
	

 
	
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <!--Facebook Pixel Code-->
    <?php echo html_entity_decode($page->facebook_pixel_code)?>
    <!--End Facebook Pixel Code-->
</head>
<body style="padding:0px; height:100%;margin: 0;overflow-x: hidden">
<!--<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" style="width:100%;min-height:100%">-->
<div id='Parent-Div-Background'style="padding:0; margin:0px;position:relative;width:100%;min-height:100%;">
@if($page['is_show'])
    <header class="mdl-layout__header mdl-layout__header--transparent" style="margin:0px; padding:0px;background: #006e73;height:100px">
        <div class="mdl-layout__header-row" style="margin-top:15px">
            <!-- Title -->
                <span class="mdl-layout-title">
                    <a href="/"><img src="/images/logo.png"></a>
                </span>
            <!-- Add spacer, to align navigation to the right -->
            <div class="mdl-layout-spacer"></div>
            <!-- Navigation -->
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="/tryit" style="color:#fff;font-size: 18px">Try it</a>
                <a class="mdl-navigation__link" href="/courses" style="color:#fff;font-size: 18px">Course</a>
                <a id="submenu" class="mdl-navigation__link" href="#" style="color:#fff;font-size: 18px">Lessons</a>
                <a class="mdl-navigation__link" href="/freebies" style="color:#fff;font-size: 18px">Freebies</a>
                @if (Auth::guest())
                <button onclick="goLogin()" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="color:#fff;font-size: 18px;background: #f29f00;margin-left:150px">
                    LOGIN
                </button>
                @else
                    <button onclick="goProfile(event)" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="color:#fff;font-size: 18px;background: #f29f00;margin-left:150px">
                        MY ACCOUNT
                    </button>
                @endif
            </nav>
            <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu"  for="submenu">
                @foreach(\App\Level::all() as $level)
                    <a class="mdl-menu__item" href="/lessons/{{$level->id}}">{{$level->title}}</a>
                @endforeach
            </ul>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </header>
	@endif




<main class="mdl-layout__content" style="width:100%;margin:0px;padding:0px; @if ($page['is_show']) padding-bottom:140px; @endif" >
    <div id="full_content_div" style="width:100%;padding:0px;margin:0px">
		<?php echo html_entity_decode($page->full_width_content)?>
<style>

@media (max-width: 767px) {

}			


.link1{
	color:#66abae;
}
.link1:hover{
	color:#3b4a51;
	text-decoration:none;
}


#subscribe{
	color:#fff;
	font-size: 18px;
	background: #f29f00;
}

#subscribe:hover{
	color:#fff;
	font-size: 18px;
	background: #d98f00;
}

</style>		
		<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0px;padding:0px; width:100%; min-height:100vh;">
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;margin:0;padding:0px; padding-bottom:100px;width:100%;  background:url('/images/homepage-hero-background.png') no-repeat center/cover; ">							
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
					<div class="mdl-grid mdl-cell mdl-cell--12-col" style="height:75px; padding-top:50px;text-align:center;justify-content:center">
						<img src="/images/ebook-logo.png">
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:center">
						<span style="font-size:50px;color:#ffffff">Írj profi leveleket és emaileket angolul!</span>
					</div>						
				</div>		
				<div class="mdl-grid mdl-cell mdl-cell--8-col" style="padding-left:30px;padding-top:20px;">
					<div class="mdl-cell mdl-cell mdl-cell--6-col" style="">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
							<span style="font-size:15px;color:#ffffff;">Akár nyelvvizsgán, akár munkahelyeden!</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">							
							<span style="color:#ffffff;"><img src="/images/ebook_ticket.png" style="padding-right:10px">Magánjellegű és hivatalos levelek írása</span>					
						</div>					
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">							
							<span style="color:#ffffff;"><img src="/images/ebook_ticket.png" style="padding-right:10px">Részletes magyarázatok</span>					
						</div>					
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">							
							<span style="color:#ffffff;"><img src="/images/ebook_ticket.png" style="padding-right:10px">Tagolva, elemekre bontva</span>					
						</div>	
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">							
							<span style="color:#ffffff;"><img src="/images/ebook_ticket.png" style="padding-right:10px">Alap- és középfokú nyelvvizsga feladat megoldással, magyarázattal. </span>					
						</div>						
					</div>
				</div>					
			</div>	
			
			<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="background-color:#ffffff;justify-content:center; margin:0;padding:0px;width: 100%;">
				<div class="mdl-grid mdl-cell mdl-cell--8-col mdl-cell--4col-phone" style="padding-left:50px; height:120px">
					<div class="mdl-cell mdl-cell mdl-cell--5-col" style="padding:0px; margin:0px; padding-left:20px;overflow: visible; margin-top:-125px;background-color:#efefef;border:3px solid #eee; border-radius:5px;">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">							
							<span style="font-weight:bold; font-size:15px;">A könyvedet e-mail címedre küldjük</span>
						</div>						
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:5px;">							
							<span style=""> Ahhoz hogy megkapd kérlek iratkozz fel listánkra! Köszönjük.</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0;margin:0">							
							<form action="http://sendy.imprevo.info/subscribe" method="POST" accept-charset="utf-8" style="padding:0;margin:0;position:relative;width:100%">
								<div class="mdl-grid mdl-cell mdl-cell--10-col" style="padding-left:10px;margin:0;">
									<div class="mdl-cell mdl-cell mdl-cell--8-col" style="padding:0;margin:0;">
										<div class="input-group input-group-icon">
											<span class="input-group-addon">
												<span class="icon"><i class="fa fa-envelope"></i></span>
											</span>
											<input id="email" name="email" class="form-control" type="text" placeholder="Email" style="border-radius:0px">
										</div>	
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--4-col-phone" style="padding:0px;margin:0;">
										<button id="subscribe" type="submit" class="mdl-button" style="border-radius:0;width:100%;height:34px;margin-left:-0px;">
											KÉREM A KÖNYVET
										</button>
									</div>
								</div>
							</form>							
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:10px;">							
							<span>Ellenőrizd kérlek a „SPAM” vagy „Promóciók” mappát is a leveleződben!</span>
						</div>
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--2-col"></div>
					<div class="mdl-cell mdl-cell mdl-cell--5-col mdl-cell--hide-phone" style="overflow: visible; margin-top:-420px;">
						<img src="/images/ebook_2_leveliras-angolul-min.png">
					</div>
				</div>
			</div>	
			<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="background-color:#ffffff;justify-content:center; margin:0;padding:0px;width: 100%;">
				<div class="mdl-grid mdl-cell mdl-cell--8-col mdl-cell--4col-phone" style="padding:0;margin:0;padding-left:50px;">
					<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--order-2-phone" style="padding:0;margin:0;">
						<hr style="width:100%; height:1px; background-color:#dae9ea;"></hr>
					</div>
					<div class="mdl-grid mdl-cell mdl-cell--8-col mdl-cell--4-col-phone mdl-cell--order-3-phone"  style="padding:0; margin:0">	
						<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--4-col-phone" style="padding:0; margin:0;">
							<span style="color:#7fbabd">Copyright 2016. Imprevo. Minden jog fenntartva.</span>					
						</div>				
					</div>
					<div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--4-col-phone mdl-cell--order-1-phone" style="padding:0; margin:0;">						
						<a class="link1" href="#" style="margin-left:0px;">Adatvédelem</a>
						<a class="link1" href="#" style="margin-left:15px;">Felhasználás feltételei</a>
						<a class="link1" href="#" style="margin-left:15px;">Impresszum</a>	
					</div>	
				</div>
			</div>			
		</div>

	</div>
    <div id="main_page_div" class="mdl-grid portfolio-max-width portfolio-contact" style="padding:0;">
		@if ($page['is_sidebar'])
			<div class="mdl-cell mdl-cell--9-col mdl-cell--6-col-tablet mdl-cell--3-col-phone">
				<?php echo html_entity_decode($page->content)?>
			</div>
			<div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--3-col-phone" style="margin-top: 2rem;">
				<!-- Block 1 -->
				<div class="demo-card-square mdl-card mdl-shadow--2dp">
		
					<div class="mdl-card__supporting-text">
						<img src="http://imprevo.hu/wp-content/uploads/2016/08/01_300x250_2-4.png" />
					</div>
				</div>
	
				<!-- Block 2 -->
	
				<div class="demo-card-square mdl-card mdl-shadow--2dp" style="margin-top: 1.5rem;">
			
					<h2 class="mdl-card__title-text" style="padding-left: 1rem; margin-top: 1rem; align-self: flex-start;">Hírlevél</h2>
					<div class="mdl-card__supporting-text">
						<form style="background-color: #fdfdfd; padding: 20px; border: 1px solid #ccc; border-radius: 3px;" action="http://sendy.imprevo.info/subscribe" method="POST" accept-charset="utf-8">
							<p style="text-align: center;">Iratkozz fel ingyenes anyagokért és újdonságokért!</p>
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="email" id="hirlevel">
								<label class="mdl-textfield__label" for="hirlevel">Email ...</label>
							</div>
							<input type="hidden" name="list" value="dWVr892XDx8pprHN8763yTtK6w"/>
							<input class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" style="margin-top: 5px; background: #006e73;" type="submit" name="submit" id="submit" value="Feliratkozom!" />
						</form>
					</div>
				</div>

				<!-- Block 3 -->
				<div class="demo-card-square mdl-card mdl-shadow--2dp" style="margin-top: 1.5rem;">
					<h2 class="mdl-card__title-text" style="padding-left: 1rem; margin-top: 1rem; align-self: flex-start;">Hasznos anyagaink</h2>
					<div class="mdl-card__supporting-text">

						<ul class="demo-list-icon mdl-list">
						<li class="mdl-list__item">
							<span class="mdl-list__item-primary-content">
								<i class="material-icons mdl-list__item-icon">done</i>
								<a href="#" style="text-decoration: none; color: #006e73;">20 részes ingyenes angol tanfolyam</a>
							</span>
						</li>
		
						<li class="mdl-list__item">
							<span class="mdl-list__item-primary-content">
								<i class="material-icons mdl-list__item-icon">done</i>
								<a href="#" style="text-decoration: none; color: #006e73;">Angol Igeidők Összefoglaló Táblázat</a>
							</span>
						</li>
		
						<li class="mdl-list__item">
							<span class="mdl-list__item-primary-content">
								<i class="material-icons mdl-list__item-icon">done</i>
								<a href="#" style="text-decoration: none; color: #006e73;">Tanulási stílusok – Te melyik vagy?</a>
							</span>
						</li>
		
						<li class="mdl-list__item">
							<span class="mdl-list__item-primary-content">
								<i class="material-icons mdl-list__item-icon">done</i>
								<a href="#" style="text-decoration: none; color: #006e73;">Meg lehet tanulni az IMPREVO-val angolul?</a>
							</span>
						</li>
		
						<li class="mdl-list__item">
							<span class="mdl-list__item-primary-content">
								<i class="material-icons mdl-list__item-icon">done</i>
								<a href="#" style="text-decoration: none; color: #006e73;">IMPREVO tartalomjegyzék</a>
							</span>
						</li>
		
						<li class="mdl-list__item">
							<span class="mdl-list__item-primary-content">
								<i class="material-icons mdl-list__item-icon">done</i>
								<a href="#" style="text-decoration: none; color: #006e73;">Ingyenes Anyagok</a>
							</span>
						</li>
		
						</ul>

					</div>
				</div>
			</div>	
		@else
			<?php echo html_entity_decode($page->content)?>
		@endif	
	</div>
</main>
  @if($page['is_show'])
  <div id="div-footer" style="z-index:2;background:#ffffff; border:1px solid #eee;position: absolute;left: 0;bottom: 0px;height:120px;width: 100%;padding-top:20px;">
          <ul class="mdl-mini-footer__link-list" style="justify-content:center;margin:0 auto;color: #6c937b">
              <li><a href="#">ÁSZF</a></li>
              <li><a href="#">Adatvédelem</a></li>
              <li><a href="#">Céginfó</a></li>
              <li><a href="#">Sütik</a></li>
              <li><a href="#">Online Angol Tanfolyam</a></li>
              <li><a href="#">Mi az Imprevo?</a></li>
              <li><a href="#">Kik vagyunk?</a></li>
              <li><a href="#">Árak</a></li>
              <li><a href="#">Kapcsolat</a></li>
          </ul>
          <div style="text-align: center;width: 100%; margin-top:10px; color: #6c937b;font-size:18px">
              <a href=""><i class="fa fa-facebook" style="color: #6c937b;margin-right:30px"></i></a>
              <a href=""><i class="fa fa-twitter" style="color: #6c937b;margin-right:30px"></i></a>
              <a href=""><i class="fa fa-google-plus" style="color: #6c937b;margin-right:30px"></i></a>
              <a href=""><i class="fa fa-youtube-play" style="color: #6c937b;"></i></a>
          </div>
          <div style="width: 100%;text-align: center;margin-top:10px">
              <span style="color: #6c937b">Copyright 2016. Imprevo. Minden jog fenntartva.</span>
          </div>
  </div>
@endif
</div>


<script>
//var page_content = <?php echo json_encode($page->content)?>;
//document.getElementById('main_page_div').innerHTML = page_content;

//var full_width_content = <?php echo json_encode($page->full_width_content)?>;
//document.getElementById('full_content_div').innerHTML = full_width_content;

/*var facebook_pixel_code = <?php echo json_encode($page->facebook_pixel_code)?>;
document.getElementsByTagName("head")[0].innerHTML += '<!--Facebook Pixel Code-->';
document.getElementsByTagName("head")[0].innerHTML += facebook_pixel_code;
document.getElementsByTagName("head")[0].innerHTML += '<!--End Facebook Pixel Code-->';*/
function goLogin() {
      location.href = '/login'
}

function goProfile(e) {
  e.preventDefault();
  document.getElementById('logout-form').submit();
}

$(document).ready(function() {

    var owl = $("#owl-carousel");


    // Custom Navigation Events
    $("#carousel_next").click(function() {
		console.log('next');
		console.log(owl);
         owl.trigger('next.owl.carousel');
    });
	
    $("#carousel_prev").click(function() {
		console.log('prev');
         owl.trigger('prev.owl.carousel');
    });

});

</script>
<!--Google adwords Code-->
 <?php echo html_entity_decode($page->adwords_code)?>
<!--End Google adwords Code-->
</body>
</html>
