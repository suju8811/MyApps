<style>
.link1{
	color:#66abae;
}
.link1:hover{
	color:#3b4a51;
	text-decoration:none;
}


#subscribe{
	color:#fff;
	font-size: 18px;
	background: #f29f00;
}

#subscribe:hover{
	color:#fff;
	font-size: 18px;
	background: #d98f00;
}


@media (max-width: 1700px)
{
	#subscribe{
		font-size: 12px !important;
	}
}
@media (max-width: 1600px){
	
	#ebook-div3, #ebook-div2, #ebook-div1{
		width: 80% !important;
	}
}
@media (max-width: 1335px)
{
	#ebook-div3,#ebook-div2, #ebook-div1{
		width: 90% !important;
	}	
}

@media (max-width: 1200px)
{
	#ebook-div3,#ebook-div2, #ebook-div1{
		width: 100% !important;
	}	
}

@media (max-width:1060px)
{
	#ebook-div2{
		height:150px !important;
	}
}
@media (max-width: 769px) {
	#ebook-div1{
		padding-left:0px !important;
		text-align:center;
	}
	#ebook-div1 img{
		width: 20px;
		height:20px;
	}
	
	.ebook-list-div{
		justify-content:center !important;
	}
	

	
	#ebook-title{
		font-size:30px !important;
		line-height:40px !important;
	}
	#ebook-div1-title{
		font-size:16px !important;
	}
	.ebook1-div1-span{
		padding-right:0px;
		font-size:14px !important;
	}
	#ebook-div2{
		padding-left:0px !important;
		justify-content:center;
		margin:10px;
		height:200px !important;
		text-align:center;
	}
	#ebook-div2-div1{
		padding-left:0px !important;
		max-height: 330px !important;
	}
	#ebook-div2-div2{
		justify-content:center;
	}
	#ebook-div3{
		padding-left:0px !important;
		text-align:center;
	}
	.link1 {
		margin:0px !important;
	}
	#ebook-image-div{
		margin-top:-200px !important;
	}
	#ebook-image-div img{
		width:60% !important;
	}
	#ebook-div2-div1{
		margin-top: 0px !important;
	}
	#ebook-1-div{
		padding-bottom:200px !important;
	}
	#ebook-div2{
		min-height:500px;
	}
}		

</style>		
		<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0px;padding:0px; width:100%; min-height:100vh;">
			<div id="ebook-1-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;margin:0;padding:0px; padding-bottom:150px;width:100%;  background:url('/images/homepage-hero-background.png') no-repeat center/cover; ">							
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
					<div class="mdl-grid mdl-cell mdl-cell--12-col" style="height:75px; padding-top:50px;text-align:center;justify-content:center">
						<img src="/images/ebook-logo.png">
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
						<span id="ebook-title" style="font-weight:300;font-size:62px;line-height:65px;color:#ffffff">Írj profi leveleket és emaileket <br>angolul!</span>
					</div>						
				</div>		
				<div id="ebook-div1" class="mdl-grid mdl-cell mdl-cell--8-col mdl-grid-no--spacing mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0; margin:0;padding-left:30px;padding-top:20px;">
					<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">						
							<span id="ebook-div1-title" style="font-size:24px;color:#ffffff;">Akár nyelvvizsgán, akár munkahelyeden!</span>
						</div>
						<div class="ebook-list-div mdl-grid mdl-cell mdl-cell--12-col" style="padding-top:10px;text-align:left">							
							<table>
							<tbody>
								<tr>
									<td style="padding-top:10px; padding-right:10px; vertical-align:top">							
										<img src="/images/ebook_ticket.png" style="margin-top:0px;">
									</td>   
									<td style="padding-top:10px;">	
										<span class="ebook1-div1-span" style="font-size:22px;color:#ffffff;">Magánjellegű és hivatalos levelek írása</span>	
									</td>
								</tr>
								<tr>
									<td style="padding-top:10px; padding-right:10px; vertical-align:top">																	
										<img src="/images/ebook_ticket.png" style="margin-top:0px;">
									</td>   
									<td style="padding-top:10px;">											
										<span class="ebook1-div1-span" style="font-size:22px;color:#ffffff;">Részletes magyarázatok</span>						
									</td>
								</tr>	
								<tr>
									<td style="padding-top:10px; padding-right:10px; vertical-align:top">																	
										<img src="/images/ebook_ticket.png" style="margin-top:0px">
									</td>   
									<td style="padding-top:10px;">											
										<span class="ebook1-div1-span" style="font-size:22px;color:#ffffff;">Tagolva, elemekre bontva</span>
									</td>
								</tr>	
								<tr>
									<td style="padding-top:10px; padding-right:10px; vertical-align:top">							
										<img src="/images/ebook_ticket.png" style="margin-top:0px;">				
									</td>   
									<td style="padding-top:10px;">	
										<span class="ebook1-div1-span" style="font-size:22px; color:#ffffff;">
											Alap- és középfokú nyelvvizsga feladat megoldással, magyarázattal. 
										</span>	
									</td>
								</tr>								
							</tbody>
							</table>													
						</div>					

					</div>
				</div>					
			</div>	
			
			<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-grid--no-spacing" style="width:100%;background-color:#ffffff;justify-content:center; margin:0;padding:0px;width: 100%;">
				<div id="ebook-div2" class="mdl-grid mdl-cell mdl-cell--8-col mdl-grid--no-spacing mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-left:50px;">
					<div id="ebook-div2-div1" class="mdl-cell mdl-cell mdl-cell--5-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--order-2-tablet mdl-cell--order-2-phone" style="max-height:250px;padding:0px; margin:0px; padding-left:20px;overflow: visible; margin-top:-100px;background-color:#efefef;border:3px solid #eee; border-radius:5px;">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">							
							<span style="font-weight:bold; font-size:22px;">A könyvedet e-mail címedre küldjük</span>
						</div>						
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:5px;">							
							<span style="font-size:14px;"> Ahhoz hogy megkapd kérlek iratkozz fel listánkra! Köszönjük.</span>
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="width:100%; padding:0;margin:0">							
							<form action="http://sendy.imprevo.info/subscribe" method="POST" accept-charset="utf-8" style="padding:0;margin:0;position:relative;width:100%">
								<div id="ebook-div2-div2" class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing mdl-cell--4-col-phone" style="padding-left:10px;margin:0;">
									<div class="mdl-cell mdl-cell mdl-cell--7-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0;margin:0;">
										<div class="input-group input-group-icon">
											<span class="input-group-addon">
												<span class="icon" style="padding-top:15px;"><i class="fa fa-envelope"></i></span>
											</span>
											<input id="email" name="email" class="form-control" type="text" placeholder="Email" style="height:50px;border-radius:0px">
											<input type="hidden" name="list" value="X8p3Tm7632VLxWMfVoGaSuhQ"/>
										</div>	
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--5-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0px;margin:0;">
										<button id="subscribe" type="submit" class="mdl-button" style="font-size:15px;border-radius:0;width:100%;height:50px;margin-left:-0px;">
											KÉREM A KÖNYVET
										</button>
									</div>
								</div>
							</form>							
						</div>
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:10px;">							
							<span style="font-size:14px;">Ellenőrizd kérlek a „SPAM” vagy „Promóciók” mappát is a leveleződben!</span>
						</div>
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
					<div id="ebook-image-div" class="mdl-cell mdl-cell mdl-cell--5-col mdl-cell--8-tablet mdl-cell--4-col-phone mdl-cell--order-1-tablet mdl-cell--order-1-phone" style="text-align:center;overflow: visible; margin-top:-420px;">
						<img src="/images/ebook_2_leveliras-angolul-min.png" style="width:80%">
					</div>
			   </div>	
			</div>	
			<div  class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-grid--no-spacing" style="background-color:#ffffff;justify-content:center; margin:0;padding:0px;width: 100%;">
				<div id="ebook-div3" class="mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="padding-bottom:40px;padding-top:60px;margin:0;padding-left:50px;">
					<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--order-2-phone" style="padding:0;padding-bottom:30px;margin:0;">
						<hr style="width:100%; height:1px; background-color:#dae9ea;"></hr>
					</div>
					<div class="mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--order-3-phone"  style="padding:0; margin:0">	
						<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0; margin:0;">
							<span style="font-size:14px;color:#7fbabd">Copyright 2016. Imprevo. Minden jog fenntartva.</span>					
						</div>				
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--hide-tablet mdl-cell--hide-phone" style="padding:0; margin:0;">												
						<a class="link1" href="#" style="font-size:14px;margin-left:0px;">Adatvédelem</a>
						<a class="link1" href="https://imprevo.hu/aszf" style="font-size:14px;margin-left:15px;">Felhasználás feltételei</a>
						<a class="link1" href="#" style="font-size:14px;margin-left:15px;">Impresszum</a>	
					</div>	
					<div class="mdl-cell mdl-cell mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--hide-desktop" style="padding:0; margin:0;">												
					   <div class="mdl-cell mdl-cell mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--hide-desktop" style="padding-top:10px; margin:0;">												
						<a class="link1" href="#" style="font-size:14px;">Adatvédelem</a>
					   </div>	
					   <div class="mdl-cell mdl-cell mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--hide-desktop" style="padding-top:10px; margin:0;">												
						<a class="link1" href="https://imprevo.hu/aszf" style="font-size:14px;">Felhasználás feltételei</a>
					   </div>							
					   <div class="mdl-cell mdl-cell mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--hide-desktop" style="padding-top:10px; margin:0;">												
						<a class="link1" href="#" style="font-size:14px;">Impresszum</a>	
					   </div>	
					</div>	
				</div>
			</div>			
		</div>	