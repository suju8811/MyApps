<!DOCTYPE html>
<html lang="en" style="position: relative; min-height: 100%;">
<head>
  <!--GaCode-->
@if (isset($settings))
  <?php echo html_entity_decode($settings['gaCode'])?>
@endif
<!--End GaCode-->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

	@if ($page->seo_description == '' && $page->seo_keywords == '' && $page->seo_title == '')
		@if (isset($settings))
			<meta name="description" content="{{$settings['siteDesc']}}">
			<meta name="keywords" content="{{$settings['keywords']}}">
			<title>{{$settings['siteTitle']}}</title>
		@endif
	@else
		<meta name="description" content="{{$page->seo_description}}">
		<meta name="keywords" content="{{$page->seo_keywords}}">
		<title>{{$page->seo_title}}</title>
	@endif
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">

    <link rel="stylesheet" href="/css/material.min.css" />
    <link rel="stylesheet" href="/css/styles.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="{{ Config::get('RELATIVE_URL') }}/assets/vendor/pnotify/pnotify.custom.css" />
    <link rel="stylesheet" type="text/css" href="https://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.css" />
	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- Specific Page Vendor CSS -->
	<link rel="stylesheet" href="assets/vendor/owl.carousel/assets/owl.carousel.css" />
	<link rel="stylesheet" href="assets/vendor/owl.carousel/assets/owl.theme.default.css" />
	<!-- Theme CSS -->
	<link rel="stylesheet" href="assets/stylesheets/theme-fix.css"/>

	<!-- Skin CSS -->
	<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <script src="/assets/vendor/modernizr/modernizr.js"></script>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.js"></script>
    <script src="/assets/vendor/pnotify/pnotify.custom.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
	<script src="assets/vendor/owl.carousel/owl.carousel.js"></script>

	<script src="assets/javascripts/theme.js"></script>

	<!-- Theme Custom -->
	<script src="assets/javascripts/theme.custom.js"></script>

	<!-- Theme Initialization Files -->
	<script src="assets/javascripts/theme.init.js"></script>
	<script src="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.min.js"></script>
	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.indigo-pink.min.css">

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <!--Facebook Pixel Code-->
<?php echo html_entity_decode($page->facebook_pixel_code)?>
    <!--End Facebook Pixel Code-->

</head>
<style>

  .fb-page, .fb-page:before, .fb-page:after {
    border: 1px solid #ccc;
  }

  .fb-page:before, .fb-page:after {
    content: "";
    position: absolute;
    bottom: -3px;
    left: 2px;
    right: 2px;
    height: 1px;
    border-top: none
  }

  .fb-page:after {
    left: 4px;
    right: 4px;
    bottom: -5px;
    box-shadow: 0 0 2px #ccc
  }
</style>
<style>
html, body {
	font-family: 'Roboto', sans-serif;

}

.footer-link{
	padding:10px;
	font-weight:700;
	color: #6c937b;
}

#footer-1div{
	text-align:right;
}

#footer-2div{
	text-align:center;
}

#footer-3div{
	text-align:left;
}

#div-footer {
	height:140px;
}

.sidenav_links{
	color:#909090;
	text-decoration: none !important;
}

@media (max-width: 1380px){
	#footer-links-div{
		width:90%;
	}

}
@media (max-width:1055px){
	#footer-links-div{
		width:100%;
	}
}


@media (max-width:785px){

	#footer-1div{
		text-align:center;
	}

	#footer-2div{
		text-align:center;
	}

	#footer-3div{
		text-align:center;
	}

	.footer-link{
		font-size:12px;
	}

	#div-footer {
		height:230px;
	}
}




.phone-footer-accordion {
  width: 380px;
  border-radius: 5px;
  overflow: hidden;
  margin: auto;
}

.phone-footer-accordion .item .heading {
  height: 50px;
  line-height: 50px;
  font-size: 15px;
  cursor: pointer;
  color: #ffffff;
  padding-left: 15px;
  background: #006469 url('/images/phone_footer_accordion.png') no-repeat;
  background-position: right 20px top -95px;
  border-bottom: 1px solid #4d9a9d;
  box-sizing: border-box;
  text-align:left;
}

.phone-footer-accordion .item.open .heading,
.phone-footer-accordion .item:last-child .heading { border: 0; }

.phone-footer-accordion .item.open .heading { background-position: right 20px top -5px; }

.phone-footer-accordion .item .content {
  display: none;
  padding: 15px;
  background: #006469;
  font-size: 14px;
}



@media (max-width: 769px) {

	#footer-1div{
		text-align:center;
	}

	#footer-2div{
		text-align:center;
	}

	#footer-3div{
		text-align:center;
	}

	#dropdown3:before {
		display: none !important;
	}

	#dropdown2:before {
		display: none !important;
	}

	#dropdown1:before {
		display: none !important;
	}


	.footer-link{
		font-size:12px;
	}

	#div-footer {
		height:270px;
	}
}



</style>
<body style="padding:0px; height:100%; margin: 0;overflow-x: hidden">
  @if (isset($settings))
    <?php echo html_entity_decode($settings['gaBodyCode'])?>
  @endif
<!--<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" style="width:100%;min-height:100%">-->
<div id='Parent-Div-Background'style="padding:0; margin:0px;position:relative;width:100%;min-height:100%;background-color:#ffffff">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<form id="logout-form1" action="{{ url('/logout') }}" method="POST" style="display: none;">
	{{ csrf_field() }}
</form>
@if($page['is_show'])
  @include('layouts.header')
@endif

<main class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid-no--spacing" style="background-color:#ffffff;width:100%;min-height:100%;margin:0px;padding:0px;" >
    <div id="full_content_div" style="width:100%;padding:0px;margin:0px">
		<?php echo html_entity_decode($page->full_width_content)?>

		<!------------>
	</div>
    <div id="main_page_div" class="mdl-grid portfolio-max-width portfolio-contact" style="padding:0;">
		@if ($page['is_sidebar'])
			<div class="mdl-cell mdl-cell--9-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
				<?php echo html_entity_decode($page->content)?>
			</div>
			<div class="mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin-top: 2rem;">
				<!-- Block 1 -->
				<div class="demo-card-square mdl-card mdl-shadow--2dp">

					<div class="mdl-card__supporting-text">
						<img src="/images/banner.png" onclick="location.href='https://imprevo.hu/20-r%C3%A9szes-ingyenes-angol-tanfolyam'" style="cursor:pointer"/>
					</div>
				</div>

				<!-- Block 2 -->

				<div class="demo-card-square mdl-card mdl-shadow--2dp" style="margin-top: 1.5rem;">
					<h2 class="mdl-card__title-text" style="padding-left: 1rem; margin-top: 1rem; align-self: flex-start;">{!! trans('page.span1') !!}</h2>
					<div class="mdl-card__supporting-text">
						<form action="http://newsletter.imprevo.info/sendy/subscribe" method="POST" accept-charset="utf-8" style="background-color: #fdfdfd; padding: 20px; border: 1px solid #ccc; border-radius: 3px;">
							<input type="hidden" name="list" value="dWVr892XDx8pprHN8763yTtK6w"/>
							<p style="text-align: center;">{!! trans('page.span2') !!}</p>
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
								<input class="mdl-textfield__input" type="text" id="email" name="email">
								<label class="mdl-textfield__label" for="email">{!! trans('page.span3') !!}</label>
							</div>
							<input class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" style="margin-top: 5px; background: #006e73;" type="submit" name="submit" id="submit" value="Feliratkozom!" />
						</form>
					</div>
				</div>
				<div class="demo-card-square mdl-card mdl-shadow--2dp" style="margin-top: 1.5rem;">
					<h2 class="mdl-card__title-text" style="padding-left: 1rem; margin-top: 1rem; align-self: flex-start;">Hasznos anyagaink</h2>
					<div class="mdl-card__supporting-text">
						<a class="sidenav_links" href="https://imprevo.hu/20-részes-ingyenes-angol-tanfolyam">20 részes ingyenes angol tanfolyam</a>
						<hr>
						<a class="sidenav_links" href="https://imprevo.hu/angol-igeidőkÖsszefoglaló-táblázat">Angol igeidők összefoglaló táblázat</a>
						<hr>
						<a class="sidenav_links" href="https://imprevo.hu/blog/tanulási-stílusok">Tanulási stílusok</a>
						<hr>
						<a class="sidenav_links" href="https://imprevo.hu/meg-lehet-tanulni-az-imprevo-val-angolul">Meg lehet tanulni az Imprevo-val angolul</a>
						<hr>
						<a class="sidenav_links" href="https://imprevo.hu/ingyenes-tartalmak">Ingyenes anyagok</a>
						<hr>
					</div>
				</div>
				<div class="demo-card-square mdl-card mdl-shadow--2dp" style="margin-top: 1.5rem;">
					<h2 class="mdl-card__title-text" style="padding-left: 1rem; margin-top: 1rem; align-self: flex-start;">Kövess minket!</h2>
					<div class="mdl-card__supporting-text" style="width:100%">
						<div class="fb-page" data-href="https://www.facebook.com/imprevo" data-small-header="false" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
						</div>
					</div>
				</div>
			</div>
		@else
			<?php echo html_entity_decode($page->content)?>
		@endif
	</div>
</main>
  @if($page['is_show'])
    @if ($page['static_url'] != "home-page" && $page['static_url'] != "contact-page" && $page['static_url'] != "what-is-imprevo" && $page['static_url'] != "kik-vagyunk")
    <div id="div-footer" class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0; padding:0;z-index:2;background:#ffffff; position: absolute;left: 0;bottom: 0px;width: 100%;padding-top:20px;">
       <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-grid--no-spacing" style="justify-content:center; margin:0 auto; padding:0;">
			<div id="footer-links-div" class="mdl-cell mdl-cell mdl-cell--7-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="text-align:center; margin:0;paddig:0;">
				<a class="footer-link" href="https://imprevo.hu/aszf" style="padding:10px">ÁSZF</a>
				<a class="footer-link" href="https://imprevo.hu/adatvedelem" style="padding:10px">Adatvédelem</a>
				<a class="footer-link" href="https://imprevo.hu/céginformáció" style="padding:10px">Céginfó</a>
				<a class="footer-link" href="https://imprevo.hu/cookie-k-kezelése" style="padding:10px">Sütik</a>
				<a class="footer-link" href="https://imprevo.hu/sales/course/10" style="padding:10px">Online Angol Tanfolyam</a>
				<a class="footer-link" href="https://imprevo.hu/what-is-imprevo" style="padding:10px">Mi az Imprevo?</a>
				<a class="footer-link" href="https://imprevo.hu/kik-vagyunk" style="padding:10px">Kik vagyunk?</a>
				<a class="footer-link" href="https://imprevo.hu/contact-page" style="padding:10px">Kapcsolat</a>
			</div>
			<div id="footer-links-div-phone" class="mdl-grid mdl-cell mdl-cell--hide-desktop mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="justify-content:center; text-align:center;margin:0;paddig:0;">
				<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:10px">
					<a class="footer-link" href="https://imprevo.hu/aszf" style="padding:10px">ÁSZF</a>
					<a class="footer-link" href="https://imprevo.hu/adatvedelem" style="padding:10px">Adatvédelem</a>
					<a class="footer-link" href="https://imprevo.hu/céginformáció" style="padding:10px">Céginfó</a>
					<a class="footer-link" href="https://imprevo.hu/cookie-k-kezelése" style="padding:10px">Sütik</a>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:10px">
					<a class="footer-link" href="https://imprevo.hu/sales/course/10" style="">Online Angol Tanfolyam</a>
					<a class="footer-link" href="https://imprevo.hu/what-is-imprevo" style="">Mi az Imprevo?</a>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:10px">
					<a class="footer-link" href="https://imprevo.hu/sales/course/11" style="padding:10px">Kik vagyunk?</a>
					<a class="footer-link" href="https://imprevo.hu/contact-page" style="padding:10px">Kapcsolat</a>
				</div>
			</div>

        </div>
        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align: center;width: 100%; margin-top:10px; color: #6c937b;font-size:18px">
            <a href="https://www.facebook.com/imprevo"><i class="fa fa-facebook" style="color: #6c937b;margin-right:30px"></i></a>
            <a href="https://twitter.com/ImprevoLearning"><i class="fa fa-twitter" style="color: #6c937b;margin-right:30px"></i></a>
            <a href="https://plus.google.com/103750741136736429559"><i class="fa fa-google-plus" style="color: #6c937b;margin-right:30px"></i></a>
            <a href="https://www.youtube.com/channel/UC9zzm-SiiLWFX-a_M0E1QHQ"><i class="fa fa-youtube-play" style="color: #6c937b;"></i></a>
        </div>
        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="width: 100%;text-align: center;margin-top:10px;padding-bottom:15px;">
            <span style="padding-left:210px;color: #6c937b; font-size:12px;font-weight:600;">{!! trans('headerfooter.footer-span29') !!}</span>
			<img src="/images/footer-barion-logo.png" style="padding-left:10px">
        </div>
         <div class="mdl-cell mdl-cell mdl-cell--hide-desktop mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="width: 100%;text-align: center;margin-top:10px;padding-bottom:15px;">
			<div class="mdl-cell mdl-cell mdl-cell--8-col-tablet mdl-cell--4-col-phone">
				<span style="color: #6c937b; font-size:12px;font-weight:600;">{!! trans('headerfooter.footer-span29') !!}</span>
			</div>
			<div class="mdl-cell mdl-cell mdl-cell--8-col-tablet mdl-cell--4-col-phone">
				<img src="/images/footer-barion-logo.png" style="">
			</div>
        </div>
    </div>
    @endif
  @endif
</div>
@include('layouts.cookie')

<script>

function goLogin() {
      location.href = '/login'
}

function goProfile(e) {
  e.preventDefault();
  location.href = '/profile'
}

function goDashboard(e) {
  location.href = '/home'
}

function Logout(e) {
    e.preventDefault();
    document.getElementById('logout-form1').submit();
}

if (document.getElementById('contact-form'))
{
	console.log('asdfasdf');

	document.getElementById('contact-content-div').innerHTML = '@if ($message = Session::get("success")) \
								<div class="alert alert-success mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">\
									<p>{{ $message }}</p>\
								</div>\
							@endif' + document.getElementById('contact-content-div').innerHTML;
	document.getElementById('contact-form').innerHTML = '{{ csrf_field() }}' + document.getElementById('contact-form').innerHTML;
}

$(document).ready(function() {

    var owl = $("#owl-carousel");


    // Custom Navigation Events
    $("#carousel_next").click(function() {
         owl.trigger('next.owl.carousel');
    });

    $("#carousel_prev").click(function() {
         owl.trigger('prev.owl.carousel');
    });


});

$(function() {
    var div = $('iframe');
	if (div){
		var width = div.width();
		div.css('height', width*0.6);
	}
});

</script>

<script>
var home_header = document.getElementById("header-div-homepage");

//-------home page support --------
if (home_header){
	document.getElementById('header-span1').innerHTML = '{!! trans("headerfooter.header-span1") !!}';
	document.getElementById('header-span2').innerHTML = '{!! trans("headerfooter.header-span2") !!}';
	//document.getElementById('header-span3').innerHTML = '{!! trans("headerfooter.header-span3") !!}';
	//document.getElementById('header-span4').innerHTML = '{!! trans("headerfooter.header-span4") !!}';
	document.getElementById('header-span5').innerHTML = '{!! trans("headerfooter.header-span5") !!}';
	document.getElementById('header-span6').innerHTML = '{!! trans("headerfooter.header-span6") !!}';
	document.getElementById('header-span7').innerHTML = '{!! trans("headerfooter.header-span7") !!}';
	document.getElementById('header-span8').innerHTML = '{!! trans("headerfooter.header-span8") !!}';
	document.getElementById('header-span9').innerHTML = '{!! trans("headerfooter.header-span9") !!}';
	document.getElementById('header-span10').innerHTML = '{!! trans("headerfooter.header-span10") !!}';
	nav_right_ul = document.getElementById('nav-right-ul');
	$('.phone-footer-accordion .item .heading').click(function() {
		var a = $(this).closest('.item');
		var b = $(a).hasClass('open');
		var c = $(a).closest('.accordion').find('.open');

		if(b != true) {
			$(c).find('.content').slideUp(200);
			$(c).removeClass('open');
		}

		$(a).toggleClass('open');
		$(a).find('.content').slideToggle(200);

	});

	if (nav_right_ul)
	{
		var html = '';
		@if (Auth::guest())
			html = html + '<li  style="">';
			html = html + '<button id="my-account-button" onclick="goLogin(event)" class="dropdown-toggle mdl-button mdl-js-button mdl-button--raised mdl-button--colored" data-toggle="dropdown" style="border-radius:2px;outline:none;border:none;">' +
						'{!! trans("headerfooter.header-button1") !!}'	+
					'</button></li>';
		@else
			html = html + '<li class="dropdown" style="">';
			html = 	html + '<button class="dropdown-toggle" id="my-account-button-logged" onclick="goDashboard(event)" class="my-nav-button dropdown-toggle" data-toggle="dropdown" style="border-radius:2px;outline:none;border:none;">' +
						'{!! trans("headerfooter.header-button2") !!}' +
					'</button>';
			html = html + '<ul id="dropdown3" class="dropdown-menu">' +
						'<li><a class="a-submenu" href="/profile">{!! trans("headerfooter.header-dropdown1") !!}</a></li>' +
						'<li><a class="a-submenu" href="" onclick="Logout(event)">{!! trans("headerfooter.header-dropdown2") !!}</a></li>' +
				'</ul></li>';
		@endif


		nav_right_ul.innerHTML = html;
	}

	try_it_button = document.getElementById('try-it-button');

	if (try_it_button)
	{
		@if (Auth::guest())
			try_it_button.style.backgroundColor = '#399aed';
		@else
			try_it_button.style.backgroundColor = '#f29f00';
		@endif
	}
}
//------------------------


window.onscroll = function ()  { //detect page scroll
	home_header = document.getElementById("header-div-homepage");

	if (home_header){

		sticky = $('#header-div-homepage');
		scroll = document.documentElement.scrollTop || document.body.scrollTop;

		if (scroll >= 100) {
			sticky.addClass('fixed');
			document.getElementById("header-div-homepage").style.padding = "5px 0px 5px 0px";
		}
		else{
			sticky.removeClass('fixed');
			document.getElementById("header-div-homepage").style.padding = "12px 0px 12px 0px";
		}
	}
};

function isMobileDevice() {
	return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

if (!isMobileDevice()){
	$('ul.nav li.dropdown').hover(function() {
	$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
	}, function() {
	$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
	});
}


</script>

<script>
//home page script
	function try_it_button_mouseover(obj)
	{
		console.log('mouseover');
		@if (Auth::guest())
			obj.style.backgroundColor = '#2081d5';
		@else
			try_it_button.style.backgroundColor = '#d98f00';
		@endif
	}

	function try_it_button_mouseout(obj)
	{
		@if (Auth::guest())
			obj.style.backgroundColor = '#399aed';
		@else
			obj.style.backgroundColor = '#f29f00';
		@endif
	}
//
</script>
<!--Google adwords Code-->
 <?php echo html_entity_decode($page->adwords_code)?>
<!--End Google adwords Code-->
</body>
</html>
