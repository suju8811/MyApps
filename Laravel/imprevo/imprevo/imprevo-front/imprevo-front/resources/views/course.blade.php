@extends('layouts.app')
<link rel="stylesheet" href="/assets/autocomplete-search/style.css" />

<style>
#know_more_button {
  background:#399aed;
  color:white;
  font-size: 14px;
  width:100%;
  padding:0px 5px 0px 5px;
}

#know_more_button:hover {
  background:#2081d5;
}

.custom-row{
  background:#ffffff;

}
.custom-row:hover{
  background:#f0f0f0;
}

#banner-image {
  padding:10px;
  margin:0px 0px 0px 15px;
  width:110px;
  height:110px;
}
#brecumb_div{
  background: #9fc3ae;
  padding:5px 40px 5px 0px;
  padding-left:80px;
  font-size: 16px;
}

#main-content{
  min-height:750px;
}
@media (max-width: 400px) {
  #banner-image {
    padding:0px;
    margin:15px 0px 0px 10px;
    width:60px;
    height:60px;
  }

  #brecumb_div{
    padding-left:25px;
  }


}
</style>
@section('content')
<div id="main-content" class="mdl-layout__content" style="width:100%;background-color:#ebf3ef">
  <div id="brecumb_div" style="">
    <span><a href="/" style="color:#d5e7dc;line-height: 40px;text-decoration: none">Imprevo</a></span>
    <span style="padding:0px 5px 0px 5px;color:#d5e7dc;line-height: 40px">/</span>
    <span style="color:#fff;line-height: 40px">{{$course->title}}</span>
  </div>
  <div class="container-fluid" style="padding:20px 0;background:#ebf3ef; margin-bottom:50px;">
    <?php
      $assigned_courses = Auth::user()->courses;
      $b_assigned = false;
      if ($assigned_courses){
        foreach ($assigned_courses as $item){
          if ($item->id == $course->id)
            $b_assigned = true;
        }
      }
     ?>
    <div class="container desktop_tab mdl-cell--hide-phone">
      <h2 class="blue1">{{$course->title}}</h2>
      <div class="row">
        <div class="tab col-md-3 marT2" style="margin-top:30px;padding: 0 0 10px 20px;">
          <button class="tab_div @if ($tab != 2) active @endif" data-tab="tab_content1"><i class="fa fa-lightbulb-o"></i> {!! trans('course.tab1') !!}</button>
          <button class="tab_div @if ($tab == 2) active @endif" data-tab="tab_content2"><i class="fa fa-th"></i> {!! trans('course.tab2') !!}</button>
          <button class="tab_div" data-tab="tab_content3"><i class="fa fa-file-text"></i> {!! trans('course.tab3') !!}</button>
          <button class="tab_div" data-tab="tab_content4" style="margin-bottom:10px"><i class="fa fa-star-o"></i> {!! trans('course.tab4') !!}</button>
          <a href="/home"><i class="fa fa-angle-left"></i> {!! trans('course.tab5') !!}</a>
        </div>

        <div class="col-md-9" style="margin-top:30px;">
          <div id="tab_content1" class="tabcontent @if ($tab != 2) active @endif">
            <?php echo html_entity_decode($course->description)?>
          </div>

          <div id="tab_content2" class="tabcontent @if ($tab == 2) active @endif">
            @if ($course->is_purchased || $course->is_free || Auth::user()->permission >= 3 || $b_assigned)
            <div class="row progress_div">
              <div class="col-md-2"><img src="/images/icon.png" class="img-responsive" style="width:100%;"></div>
              <div class="col-md-10">
                <h4>{{$course->last_level}} {{$course->percent_done}}%
                  <button class="pull-right btn btn-primary" onclick="location.href = '/exercise/{{$course->last_exercise}}'">FOLYTATOM</button>
                </h4>
                <div class="progress" style="width:100%; margin-top:40px;background:#eeeeee;height:15px">
                  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:{{$course->percent_done}}%;background:#399aed;">
                  </div>
                </div>
              </div>
            </div>
            @if ($levels)
              @foreach($levels as $level)
              <div class="green_bg marT3"><strong>{{$level->title}}</strong></div>
              <div class="row">
                @foreach($level->course_lessons as $lesson)
                <div class="col-md-4">
                  <div class="tab_image" onclick="location.href = '/lessonHome/{{$lesson->id}}'">
                    <div class="overlay"></div>
                    <img src="{{$lesson->image}}" class="img img-responsive image_tab">
                    <div class="tab_image_text">
                      <span class="text-left">{{$lesson->title}}</span><span class="pull-right">
                      @if ($lesson->status == 'C')
                        <img src="/images/ic_level1.png">
                      @elseif ($lesson->status == 'B')
                        <img src="/images/ic_level2.png">
                      @else
                        <img src="/images/ic_level3.png">
                      @endif
                      </span>
                    </div>
                  </div>
                </div>
                @endforeach
              </div>
              @endforeach
          @endif

          @else
          <div class="row add_div" onclick="location.href = '/shoppingcart?products={{$course->product}}'" style="cursor:pointer;position:relative;height:120px;padding:10px;color:#fff;margin:0px !important;background:url('{{$course->banner}}') no-repeat center/cover">

          </div>
          <div class="green_bg marT3"><strong>{!! trans('course.strong1') !!}</strong></div>
          <div class="row">
            @foreach($lessons as $lesson)
            @if ($lesson->is_free || $lesson->is_trial)
            <div class="col-md-4">
              <div class="tab_image" onclick="location.href = '/lessonHome/{{$lesson->id}}'">
                <div class="overlay"></div>
                <img src="{{$lesson->image}}" class="img img-responsive image_tab">
                <div class="tab_image_text">
                  <span class="text-left">{{$lesson->title}}</span><span class="pull-right">
                    @if ($lesson->status == 'C')
                    <img src="/images/ic_level1.png">
                    @elseif ($lesson->status == 'B')
                    <img src="/images/ic_level2.png">
                    @else
                    <img src="/images/ic_level3.png">
                    @endif
                  </span>
                </div>
              </div>
            </div>
            @endif
            @endforeach

            @foreach($lessons as $lesson)
            @if (!$lesson->is_free && !$lesson->is_trial)
            <div class="col-md-4 ">
              <div class="tab_image grey_effect">
                <img src="{{$lesson->image}}" class="img img-responsive image_tab">
                <div class="tab_image_text">
                  <span class="">{{$lesson->title}}</span><span class="pull-right">
                    @if ($lesson->status == 'C')
                    <img src="/images/ic_level1.png">
                    @elseif ($lesson->status == 'B')
                    <img src="/images/ic_level2.png">
                    @else
                    <img src="/images/ic_level3.png">
                    @endif
                  </span>
                </div>
              </div>
            </div>
            @endif
            @endforeach
          </div>
          @endif
          </div>
          <div id="tab_content3" class="tabcontent">
            <div class="search_box">
              <span class="fa fa-search"></span><input id="autocomplete" type="text" placeholder="{!! trans('course.input_placeholder1') !!}" style="width:70%;padding:10px;border:0px;" >
              <button class="btn btn-warning pull-right btn-lg" onclick="syllabus_details(document.getElementById('autocomplete').value)" style="right: 55px;top: 20px;">{!! trans('course.button4') !!}</button>
            </div>
            <div id="syllabus-div" class="row" style="padding:0px 15px">
              @foreach($lessons as $lesson)
              <div class="green_bg marT3" style="padding:25px;">
                <strong>{{$lesson->title}}</strong>
                <button class="btn btn-lesson" onclick="location.href='/lessonHome/{{$lesson->id}}'">{!! trans('course.button1') !!}</button>
              </div>

              <div class="table-responsive">
                <table class="" style="background:#fff;border:0; width:100%;">
                  <tbody class="tab_table1">
                    @foreach($lesson->syllabus as $item)
                    <tr>
                      <td class="width40"><img src="{{$item->module_icon}}"/><b> {{$item->module_name}}</b></td>
                      <td class="width60">{{$item->description}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              @endforeach
            </div>
          </div>

          <div id="tab_content4" class="tabcontent">
            <div class="green_bg"><strong>{!! trans('course.strong2') !!}</strong></div>
            <div class="table-responsive marT3 desktop_table">
              <table class="table" style="background:#fff;">
                <thead>
                  <tr class="active">
                    <th>{!! trans('course.th1') !!}</th>
                    <th>{!! trans('course.th2') !!}</th>
                    <th>{!! trans('course.th3') !!}</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody class="tab_table">
                  @foreach ($latest_exercises as $lastest_exe)
                  <tr>
                    <td>{{$lastest_exe->title}}</td><td>{{$lastest_exe->lesson->title}}</td><td>{{$lastest_exe->created_at}}</td><td><a href="/exercise/{{$lastest_exe->id}}"><i class="fa fa-angle-right"></i></a></td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container mobile_tab">
      <h2 class="blue1">{{$course->title}}</h2>
      <ul class="nav nav-pills mobile_nav">
        <li @if ($tab != 2) class="active" @endif><a class="m_tab_link" data-toggle="pill" href="#m_tab_content1"><center><i class="fa fa-lightbulb-o"></i></center> {!! trans('course.mobile_tab1') !!}</a></li>
        <li @if ($tab == 2) class="active" @endif><a class="m_tab_link" data-toggle="pill" href="#m_tab_content2"><center><i class="fa fa-th"></i></center> {!! trans('course.mobile_tab2') !!}</a></li>
        <li><a class="m_tab_link" id="mobile-syllabus-tab" data-toggle="pill" href="#m_tab_content3"><center><i class="fa fa-file-text"></i></center>{!! trans('course.mobile_tab3') !!}.</a></li>
        <li><a class="m_tab_link" data-toggle="pill" href="#m_tab_content4"><center><i class="fa fa-star-o"></i></center> {!! trans('course.mobile_tab4') !!}</a></li>
      </ul>

      <div class="tab-content marT3"  style="background:#ebf3ef;">
        <div id="m_tab_content1" class="tab-pane fade @if ($tab != 2) in active @endif">
          <?php echo html_entity_decode($course->description)?>
        </div>

        <div id="m_tab_content2" class="tab-pane fade  @if ($tab == 2) in active @endif">
          @if (!$course->is_purchased && !$course->is_free && Auth::user()->permission < 3 && !$b_assigned)
          <div class="add_div row" onclick="location.href = '/shoppingcart?products={{$course->product}}'" style="cursor:pointer;position:relative;background:url('{{$course->banner_mobile}}') no-repeat center/cover; height:270px">
          </div>
          <div class="row">
          <div class="green_bg marT3"><strong style="font-size:15px">{!! trans('course.strong1') !!}</strong></div>
          <div class="row">
            @foreach($lessons as $lesson)
            <?php $i = 0;  $i++;?>
            @if ($lesson->is_free || $lesson->is_trial)
            <div class="col-md-4">
              <?php if ($i % 2 == 1) { ?>
              <div class="tab_image tab_imag_left" onclick="location.href = '/lessonHome/{{$lesson->id}}'">
              <?php }else { ?>
              <div class="tab_image tab_imag_right" onclick="location.href = '/lessonHome/{{$lesson->id}}'">
              <?php } ?>
                <div class="overlay"></div>
                <img src="{{$lesson->image}}" class="img img-responsive image_tab">
                <div class="tab_image_text">
                  <span class="text-left">{{$lesson->title}}</span><span class="pull-right">
                    @if ($lesson->status == 'C')
                    <img src="/images/ic_level1.png">
                    @elseif ($lesson->status == 'B')
                    <img src="/images/ic_level2.png">
                    @else
                    <img src="/images/ic_level3.png">
                    @endif
                  </span>
                </div>
              </div>
            </div>
            @endif
            @endforeach

            @foreach($lessons as $lesson)
            <?php $i = 0;  $i++;?>
            @if (!$lesson->is_free && !$lesson->is_trial)
            <div class="col-md-6">
            <?php if ($i % 2 == 1) { ?>
              <div class="tab_image grey_effect tab_imag_left">
              <?php }else { ?>
                    <div class="tab_image grey_effect tab_imag_right">
              <?php } ?>
              <img src="{{$lesson->image}}" class="img img-responsive image_tab">
              <div class="tab_image_text">
                <span class="">{{$lesson->title}}</span><span class="pull-right">
                  @if ($lesson->status == 'C')
                  <img src="/images/ic_level1.png">
                  @elseif ($lesson->status == 'B')
                  <img src="/images/ic_level2.png">
                  @else
                  <img src="/images/ic_level3.png">
                  @endif
                </span>
              </div>
              </div>
            </div>
            @endif
            @endforeach
          </div>
          </div>
          @else
          <div class="progress_div row">
            <img src="/images/icon.png" style="float:left;"/>
            <h4>{{$course->last_level}} {{$course->percent_done}}% </h4>
            <div class="progress" style="height:13px;background:#eeeeee;">
              <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:{{$course->percent_done}}%;background:#399aed;">
              </div>
            </div>
            <button class="btn btn-primary btn-sm" style="float:right;background:#399aed;">{!! trans('course.button2') !!}</button>
          </div>
        @if ($levels)
        @foreach($levels as $level)
        <div class="row">
        <div class="green_bg marT3"><strong>{{$level->title}}</strong></div>
        <div class="row">
          <?php $i = 0;  ?>
          @foreach($level->course_lessons as $lesson)
          <?php $i = $i + 1; ?>
          <div class="col-md-4">
            <?php if ($i % 2 == 1) { ?>
            <div class="tab_image tab_imag_left" onclick="location.href = '/lessonHome/{{$lesson->id}}'">
            <?php }else { ?>
            <div class="tab_image tab_imag_right" onclick="location.href = '/lessonHome/{{$lesson->id}}'">
            <?php } ?>
              <div class="overlay"></div>
              <img src="{{$lesson->image}}" class="img img-responsive image_tab">
              <div class="tab_image_text">
                <span class="text-left">{{$lesson->title}}</span><span class="pull-right">
                  @if ($lesson->status == 'C')
                  <img src="/images/ic_level1.png">
                  @elseif ($lesson->status == 'B')
                  <img src="/images/ic_level2.png">
                  @else
                  <img src="/images/ic_level3.png">
                  @endif
                </span>
              </div>
            </div>
          </div>
          @endforeach
        </div>
        </div>
        @endforeach
        @endif
        @endif
        </div>


        <div id="m_tab_content3" class="tab-pane fade">
          <div class="m_search_box row">
              <div class="input-group">
                <input id="mobile-search-autocomplete" type="text" class="form-control" placeholder="{!! trans('course.input_placeholder2') !!}" style="height:60px;">
                <div class="input-group-btn">
                  <button class="btn btn-default btn-lg btn-warning" onclick="syllabus_details(document.getElementById('mobile-search-autocomplete').value)">
                    <i class="glyphicon glyphicon-search" style="padding:10px;"></i>
                  </button>
                </div>
              </div>
          </div>
          <div id="mobile-syllabus-div" class="row">
            @foreach($lessons as $lesson)
            <div class="green_bg" style="padding:20px;">
              <strong style="font-size:12px">{{$lesson->title}}</strong>
              <button class="btn btn-sm" style="border:3px solid #fff;background:none;padding:5px;float:right;margin-top:-5px;" onclick="location.href='/lessonHome/{{$lesson->id}}'">{!! trans('course.button3') !!}</button>
            </div>

            <div class="table-responsive">
              <table class="" style="background:#fff;border:0; width:100%;">
                <tbody class="tab_table1">
                  @foreach($lesson->syllabus as $item)
                  <tr>
                    <td><img src="{{$item->module_icon}}" style="width:18px; height:18px;"/> <b> {{$item->module_name}}</b></td>
                    <td>{{$item->description}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            @endforeach
          </div>
        </div>

        <div id="m_tab_content4" class="tab-pane fade">
          <div class="green_bg row"><strong>{!! trans('course.strong2') !!}</strong></div>
          <div class="row">
            <div class="table-responsive  mobile_table marT3">
              <table class="table" style="background:#fff;">
                <thead>
                  <tr class="active">
                    <th>{!! trans('course.th1') !!}</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody class="tab_table">
                  @foreach ($latest_exercises as $lastest_exe)
                  <tr>
                    <td>
                      <b>{{$lastest_exe->title}}</b><br>
                      <span>{{$lastest_exe->lesson->title}} {{$lastest_exe->updated_at}}</span>
                    </td>
                    <td><a href="/exercise/{{$lastest_exe->id}}" style="margin-right:5px"><i class="fa fa-angle-right"></i></a></td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>

<script type="text/javascript" src="/assets/autocomplete-search/js/jquery.autocomplete.js"></script>


<script type="text/javascript">
$('.tab_div').click(function(event){
  $('.tabcontent').removeClass('active');
  $('.tab_div').removeClass('active');
  tab_content_id = $(this).data("tab");
  if (tab_content_id == 'tab_content3'){
      $('#autocomplete').val('');
      $('#mobile-search-autocomplete').val('');
      syllabus_details('');
  }

  $(this).addClass('active');


  $('#'+tab_content_id).addClass('active');
})

$('#mobile-syllabus-tab').click(function(){
  $('#autocomplete').val('');
  $('#mobile-search-autocomplete').val('');
  syllabus_details('');
})

function syllabus_details(query){
  if (query == null){
    query = $('#autocomplete').val();
  }
  url = '/search';
  course_id = "{{$course->id}}";
  console.log(query, course_id);
  var formData = new FormData();
  formData.append("query", query);
  formData.append("course_id", course_id);
  $.ajax({
    url: url,
    type: 'POST',
    data: formData,
    async: true,
    success: function (ret) {
      var data = JSON.parse(ret);

      var html = '';
      if (data != null){
        html = '<p style="background-color: #3bb116; color: #fff; padding: 20px;margin-top:20px">{!! trans("course.p1") !!}</p>';
      }
      @foreach ($lessons as $lesson)
      id = parseInt("{{$lesson->id}}");

      if (data == null || (data != null && (data.indexOf(id) != -1))){
        console.log(id);
        html = html = html + '<div class="green_bg marT3" style="padding:25px;">' +
        '<strong>{{$lesson->title}}</strong>' +
        '<button class="btn btn-lesson" onclick="location.href=\'/lessonHome/{{$lesson->id}}\'">{!! trans("course.button1") !!}</button>' +
        '</div>' +
        '<div class="table-responsive">' +
        '<table class="" style="background:#fff;border:0; width:100%;">' +
        '<tbody class="tab_table1">';
        @foreach($lesson->syllabus as $item)
        html = html +  '<tr>' +
        '<td class="width40"><img src="{{$item->module_icon}}"/><b> {{$item->module_name}}</b></td>' +
        '<td class="width60">{{$item->description}}</td>' +
        '</tr>';
        @endforeach
        html = html + '</tbody></table></div>';
      }
      @endforeach
      $('#syllabus-div').html(html);
      html = '';
      if (data != null){
        html = '<p style="background-color: #3bb116; color: #fff; padding: 20px;margin-top:20px">{!! trans("course.p1") !!}</p>';
      }
      @foreach($lessons as $lesson)
      id = parseInt("{{$lesson->id}}");
      if (data == null || (data != null && (data.indexOf(id) != -1))){
      html = html + '<div class="green_bg" style="padding:20px;">' +
        '<strong style="font-size:12px">{{$lesson->title}}</strong>' +
        '<button class="btn btn-sm" style="border:3px solid #fff;background:none;padding:5px;float:right;margin-top:-5px;" onclick="location.href=\'/lessonHome/{{$lesson->id}}\'">{!! trans("course.button3") !!}</button>' +
      '</div>';

      html = html + '<div class="table-responsive">' +
        '<table class="" style="background:#fff;border:0; width:100%;">' +
          '<tbody class="tab_table1">';
            @foreach($lesson->syllabus as $item)
            html = html + '<tr>' +
              '<td><img src="{{$item->module_icon}}" style="width:18px; height:18px;"/> <b> {{$item->module_name}}</b></td>' +
              '<td>{{$item->description}}</td>' +
            '</tr>';
            @endforeach
        html = html + '</tbody>' +
        '</table>' +
      '</div>';
      }
      @endforeach

      $('#mobile-syllabus-div').html(html);
      console.log(data);
    },
    error:function(request,status,error){
      console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
    },
    cache: false,
    contentType: false,
    processData: false
  });
}

$("#autocomplete").on('keyup', function (e) {
  if (e.keyCode == 13) {
    console.log('ajax calling');
    var url = "/search";
    query = $('#autocomplete').val();
    syllabus_details(query);
  }
});

$("#mobile-search-autocomplete").on('keyup', function (e) {
  if (e.keyCode == 13) {
    console.log('ajax calling');
    var url = "/search";
    query = $('#mobile-search-autocomplete').val();
    syllabus_details(query);
  }
});

$(document).ready(function(){
  console.log('done exericse', "{{$course->total_exe_count}}");
  $(function(){
    var select_data = [];
    @foreach($lessons as $lesson)
    select_data.push({value:"{{$lesson->title}}", data:"{{$lesson->id}}"});
    @if ($lesson->syllabus)
    @foreach ($lesson->syllabus as $item)
    select_data.push({value:"{{$item->description}}", data:"{{$lesson->id}}"});
    @endforeach
    @endif
    @endforeach
    // setup autocomplete function pulling from currencies[] array
    $('#autocomplete').autocomplete({
      lookup: select_data,
      onSelect: function (suggestion) {

      }
    });

    $('#mobile-search-autocomplete').autocomplete({
      lookup: select_data,
      onSelect: function (suggestion) {

      }
    })
  });
});

</script>

@endsection
