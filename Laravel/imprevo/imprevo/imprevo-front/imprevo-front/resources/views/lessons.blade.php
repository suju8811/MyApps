@extends('layouts.app')

@section('content')

<style>

#lesson_card:hover
{
  -webkit-box-shadow: 0px 2px 10px #919493;
  filter: brightness(0.85);
}

#main-content{
	padding-bottom:150px;
}
#button-div{
	text-align: right;
}
#breadcumb_text{
	padding:5px 20px 5px 70px;
	height:126px;
}
@media (max-width: 400px) {
	#breadcumb_text{
		padding:5px 20px 5px 25px;
		height:170px;
	}
	#main-content{
		padding-bottom:250px;
	}
	#button-div{
		text-align: left;
	}
}

</style>

<div id="main-content" class="mdl-layout__content"  style="width:100%;">
    <div id="breadcumb_text" style="background: #9fc3ae;font-size: 16px;">
        <span><a href="/" style="color:#d5e7dc;line-height: 40px;text-decoration: none">Imprevo</a></span>
        <span style="padding:0px 5px 0px 5px;color:#d5e7dc;line-height: 40px">/</span>
        <span style="color:#fff;line-height: 40px">{!! trans('lessons.span1') !!}</span>
        <div class="mdl-grid portfolio-max-width" style="">
            <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0; background: white">
                <div class="mdl-cell mdl-cell--8-col mdl-cell--6-col-tablet mdl-cell--4-col-phone">
                    <h2 id="h_level_title" class="mdl-card__title-text">{{$level->title}}</h2>
                    <div class="mdl-card__supporting-text padding-top">
                        <span id="span_current_lesson">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </div>
                </div>
                <div id="button-div" class="mdl-cell mdl-cell--4-col mdl-cell--2-col-tablet mdl-cell--4-col-phone" style="">
                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect imprevo-default" onclick="Continue()">
                        {!! trans('lessons.span2') !!}
                    </button>
                </div>
            </div>
        </div>
    </div>

	<div class="mdl-grid portfolio-max-width" style="min-height: 200px">
		<div id="lessons_div" class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;justify-content: center;">
			<div class="mdl-cell mdl-cell mdl-cell--8-col" style="margin-top:100px;text-align:center">
				<span style = "font-size:25px; color:#979797"> {!! trans('lessons.span3') !!}</span>
			</div>
		</div>
	</div>

</div>
    <script type="text/javascript">
        //var current_exercise = JSON.parse('<?php echo json_encode($current_exercise)?>');


        //console.log(current_exercise.length);
        //if (current_exercise.length > 0){
        var current_lesson = JSON.parse('<?php echo json_encode($current_exercise->lesson)?>');

        var level_exerciseList = {};
        var done_exercises = '{{$user_data->done_exercises}}';
        var done_exercises_array = done_exercises.split(',');
		var bExistLesson = false;
        @foreach($level->lessons as $lesson)

			if (current_lesson && "{{$lesson->id}}" == current_lesson.id)
			{
				bExistLesson = true;
			}
            level_exerciseList['{{$lesson->id}}'] = [];
            @foreach ($lesson->exercises as $l_exe)
              //level_exerciseList['{{$lesson->id}}'] = JSON.parse('<?php echo json_encode($lesson->exercises)?>');
              level_exerciseList['{{$lesson->id}}'].push({"id":"{{$l_exe->id}}"});
            @endforeach
        @endforeach
        if (bExistLesson)
		{
           document.getElementById('span_current_lesson').innerHTML = '{!! trans("lessons.span4") !!}' + current_lesson.title;
		}

        var level_exe_count = 0;
        var level_exe_done_count = 0;
		var latest_lesson = null;
        var html = '';
        @foreach($level->lessons as $lesson)
           var lesson_exe_count = 0;
           var lesson_exe_done_count = 0;
           for (exe in level_exerciseList['{{$lesson->id}}'])
           {
              var lesson_exe = level_exerciseList['{{$lesson->id}}'][exe];

              for (done_exe in done_exercises_array)
              {
                if (done_exercises_array[done_exe] == lesson_exe.id)
                {
                  lesson_exe_done_count++;
				          latest_lesson = "{{$lesson->title}}";
                  break;
                }
              }
              lesson_exe_count++;
              level_exe_count++;
           }
           level_exe_done_count = level_exe_done_count + lesson_exe_done_count;
           if (lesson_exe_count == lesson_exe_done_count)
           {
              img_src = "/images/ic_level1.png";
           }
           else if (lesson_exe_done_count == 0)
           {
              img_src = "/images/ic_level3.png";
           }
           else if (lesson_exe_count > lesson_exe_done_count)
           {
              img_src = "/images/ic_level2.png";
           }

           html = html + '<div id="lesson_card" class="mdl-card mdl-cell mdl-cell--4-col mdl-cell--3-col-tablet mdl-shadow--2dp pointer" onclick="GotoLesson(event, {{$lesson['id']}})">\
                               <figure class="mdl-card__media small-image">\
                                 <img class="article-image" src=\"' + '{{$lesson['image']}}' + '\" border="0" alt="" />\
                               </figure>\
                                   <div class="mdl-grid mdl-cell--12-col" style="padding:0">\
                                       <div class="mdl-cell mdl-cell--10-col pt5">' + '{{$lesson->title}}' + '</div>\
                                       <div class="mdl-cell mdl-cell--2-col">\
                                           <img src=' + img_src +' border="0" alt="">\
                                       </div>\
                                   </div>\
                               </div>'
        @endforeach
        if (!bExistLesson)
		{
		   if (!latest_lesson)
		   {
			  document.getElementById('span_current_lesson').innerHTML = '{!! trans("lessons.span4") !!}' + '-';
		   }
		   else
		   {
			  document.getElementById('span_current_lesson').innerHTML = '{!! trans("lessons.span4") !!}' + latest_lesson;
		   }
		}
        var pro = 0;
        if (level_exe_count != 0){
          document.getElementById('lessons_div').innerHTML = html;
          pro = level_exe_done_count * 100 / level_exe_count;
        }
        else {

        }

        document.getElementById('h_level_title').innerHTML = '{{$level->title}}' + " " + parseInt(pro) + "%";


        //}
        function GotoLesson(e, id) {
            e.preventDefault();
            e.stopPropagation();
            url = location.href;
            location.href = '\\lessonHome\\' + id;
        }
        function Continue() {
            @if ($current_exercise)
              var current_exercise = '{{$user_data->current_exercise}}';
			  if (current_exercise)
				location.href = '\\exercise\\' + current_exercise;
            @endif
        }
    </script>
@endsection
