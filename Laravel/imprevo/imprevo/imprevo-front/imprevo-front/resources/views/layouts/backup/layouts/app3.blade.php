<!DOCTYPE html>
<html lang="en" style="position: relative;min-height: 100%;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	@if (isset($settings))
    <meta name="description" content="{{$settings['siteDesc']}}">
    <meta name="keywords" content="{{$settings['keywords']}}">
    <title>{{$settings['siteTitle']}}</title>
	@else
    <meta name="description" content="">
    <meta name="keywords" content="">
	
    <title>Imprevo</title>		
	
	@endif

     <link href='https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en' rel='stylesheet' type='text/css'>
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		<!-- Vendor CSS -->
	<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap-custom.css" />

	<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
	<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
	<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

	<!-- Theme CSS -->
	<link rel="stylesheet" href="assets/stylesheets/theme.css" />

	<!-- Skin CSS -->
	<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<!-- Head Libs -->
	<script src="assets/vendor/modernizr/modernizr.js"></script>

	
	<script src="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.min.js"></script>
	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.indigo-pink.min.css">
	<!-- Material Design icon font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	
	<link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
	
	<link rel="stylesheet" href="{{ Config::get('RELATIVE_URL') }}/assets/vendor/pnotify/pnotify.custom.css" />
	
	<script src="/assets/vendor/pnotify/pnotify.custom.js"></script>	

	<!--getmdl-select-->   
	<link rel="stylesheet" href="https://cdn.rawgit.com/CreativeIT/getmdl-select/master/getmdl-select.min.css">
	<script defer src="https://cdn.rawgit.com/CreativeIT/getmdl-select/master/getmdl-select.min.js"></script>		
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
	  <!--GaCode-->
	@if (isset($settings))
		<?php echo html_entity_decode($settings['gaCode'])?>
	@endif	
	<!--End GaCode-->
    <!--Facebook Pixel Code-->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1692560164346282');
fbq('track', "PageView");

</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1692560164346282&ev=PageView&noscript=1"
/></noscript>	

    <!--End Facebook Pixel Code-->		
</head>
<style>
html, body {
	font-family: 'Roboto', sans-serif;
}

.navbar-default {
	width:100%;
	background:rgba(255,255,255,0);
	margin:0;
	border:none;

}

.navbar-default .navbar-nav{
	padding-right:15px;
}

.navbar-default .navbar-nav > li{
	
	padding-right:50px;
}

.navbar-default .navbar-nav .dropdown{
	
}

.navbar-default .navbar-nav > li > a{
	color:#ffffff;
	font-size: 14px;
	text-transform: uppercase;
}

.navbar-default .navbar-nav > li > a:hover{
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > li > a:focus{
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a{
	background:rgba(255,255,255,0);
	color:#ffffff;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a:hover{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a:focus{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu{
	margin-top:10px;
	padding:15px 20px 15px 20px;
	background:#399aed	
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li{
	padding:0;
	border-bottom:1px solid #338ad5;	
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a{
	padding:10px 0px 10px 0px ;
	color:#ffffff;	
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a:hover{
	background:#399aed;
	color:#117a97;	
}

#dropdown3 {
	background:#399aed;
	color:#ffffff;
}

#dropdown3:before {
	position: absolute;
	top: -9px;
	left:50px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

#dropdown2:before {
	position: absolute;
	top: -9px;
	left:120px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

#dropdown1:before {
	position: absolute;
	top: -9px;
	left:60px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

.navbar-default .navbar-toggle {      
		border:none;
		padding-right:15px;
}

.navbar-default .first-nav {

}

@media (max-width: 767px) {
    .navbar-default .navbar-collapse {		
        background:#ffffff;
		float:right;
		width:50%;
    }
	
	.navbar-default .navbar-nav > li > a{		
		color:#6c937b;
	}
	.navbar-default .navbar-nav > li {		
		border-bottom: 1px solid #eeeeee;
	}
}			

#header-div
{
	width:100%;
	/*justify-content:center;*/
	padding:10px 0 10px 0;
	margin:0;
	/*height:100px;*/


}

/*#header-div:hover
{
	width:100%;
	padding:5px 0 5px 0;
	margin:0;
	
	background-color:#006e73;
	-webkit-box-shadow: 0px 2px 5px 2px #004a4d;
   -moz-box-shadow: 0px 2px 5px 2px #004a4d;
        box-shadow: 0px 2px 5px 2px #004a4d;
}*/

.fixed {
	-webkit-transform: translate3d(0,0,0);
	position:fixed;
	top:0px;
	background-color:#006e73;
	-webkit-box-shadow: 0px 2px 5px 1px #004a4d;
   -moz-box-shadow: 0px 2px 5px 1px #004a4d;
        box-shadow: 0px 2px 5px 1px #004a4d;		
	z-index:999;
	padding:0px 0 0px 0;
}

#my-account-button{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #f29f00;
	width:150px;
	height:40px;
}

#my-account-button:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #d98f00;	
}

#my-account-button-logged{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #f29f00;
	width:150px;
	height:40px;
}

#my-account-button-logged:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #d98f00;	
}
</style>
<body style="padding:0; height:100%;margin: 0;overflow-x: hidden;">
<!--<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" style="width:100%;min-height:100%">-->
<div class="mdl-layout__header mdl-layout__header--transparent" style="background: #006e73;height:100px">
    <div class="mdl-layout__header-row" style="margin-top:15px;padding:0;">
			<div id="header-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid-no--spacing" style="padding-left:60px;padding-right:50px;">			
					<nav class="navbar navbar-default">
						<div class="container-fluid">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
									<i aria-hidden="true" class="fa fa-navicon" style="color:#ffffff"></i>                       
								</button>								
								<a class="navbar-brand" href="/"><img src='/images/logo.png'></img></a>
							</div>
							<div class="collapse navbar-collapse  navbar-right" id="myNavbar">					
								<ul class="nav navbar-nav" style="margin-right:80px">
									<li><a href="#">Az Imprevo</a></li>
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle="dropdown" href="#">KURZUSOK</a>
										<ul id="dropdown1" class="dropdown-menu">
											<li><a class="a-submenu" href="http://146.185.128.182/sales/course/10">Angol tanfolyam </a></li>
											<li><a class="a-submenu" href="http://146.185.128.182/sales/course/11">Hétköznapi angol </a></li>
										</ul>										
									</li>
									<li><a href="http://146.185.128.182/blog">Blog</a></li>
									<li class="dropdown" style="">
										<a class="dropdown-toggle" data-toggle="dropdown" href="#">Ingyenesek<!--<span class="caret"></span>--></a>
										<ul id="dropdown2" class="dropdown-menu">
											<li><a class="a-submenu" href="#">20 részes email tanfolyam</a></li>
											<li><a class="a-submenu" href="#">6 hónap alatt angolul</a></li>
											<li><a class="a-submenu" href="#">Angol igeidők</a></li>
											<li><a class="a-submenu" href="#">Angol levelek írása</a></li>
										</ul>
									</li>
								</ul>
								<ul class="nav navbar-nav navbar-right">
								   @if (Auth::guest())	
									<li  style="">															
										<button id="my-account-button" onclick="goLogin()" class="my-nav-button dropdown-toggle mdl-button mdl-js-button mdl-button--raised mdl-button--colored" data-toggle="dropdown" style="border-radius:2px;outline:none;border:none;">
											Log in
										</button>									
									</li>	
									@else 
									<li class="dropdown" style="">	
										<button class="dropdown-toggle" id="my-account-button-logged" onclick="location.href = '/'" class="my-nav-button dropdown-toggle" data-toggle="dropdown" style="border-radius:2px;outline:none;border:none;">
											Dashboard
										</button>
										<ul id="dropdown3" class="dropdown-menu">
											<li><a class="a-submenu" href="/profile">User profile</a></li>
											<li><a class="a-submenu" href="#" onclick="Logout(event)">Log out</a></li>
										</ul>										
									</li>	
									@endif
								</ul>	
								<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>								
							</div>
						</div>
					</nav>								
			</div>	
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
</div>
<div id='Parent-Div-Background'style="position:relative;width:100%;min-height:100%;">	
    <div class="mdl-layout__drawer mdl-layout--small-screen-only">
        <nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
            <a class="mdl-navigation__link is-active" href="tryit.html">Try it</a>
            <a class="mdl-navigation__link" href="courses.html">Course</a>
            <a class="mdl-navigation__link" href="lessons.html">Lessons</a>
            <a class="mdl-navigation__link" href="freebies.html">Freebies</a>
        </nav>
    </div>

    @yield('content')
    <div id="div-footer" style="z-index:2;background:#ffffff; border:1px solid #eee;position: absolute;left: 0;bottom: 0px;height:120px;width: 100%;padding-top:20px;">
            <ul class="mdl-mini-footer__link-list" style="justify-content:center;margin:0 auto;color: #6c937b">
                <li><a href="#">ÁSZF</a></li>
                <li><a href="#">Adatvédelem</a></li>
                <li><a href="#">Céginfó</a></li>
                <li><a href="#">Sütik</a></li>
                <li><a href="#">Online Angol Tanfolyam</a></li>
                <li><a href="#">Mi az Imprevo?</a></li>
                <li><a href="#">Kik vagyunk?</a></li>
                <li><a href="#">Kapcsolat</a></li>
            </ul>
            <div style="text-align: center;width: 100%; margin-top:10px; color: #6c937b;font-size:18px">
                <a href="https://www.facebook.com/imprevo"><i class="fa fa-facebook" style="color: #6c937b;margin-right:30px"></i></a>
                <a href="https://twitter.com/ImprevoLearning"><i class="fa fa-twitter" style="color: #6c937b;margin-right:30px"></i></a>
                <a href="https://plus.google.com/103750741136736429559"><i class="fa fa-google-plus" style="color: #6c937b;margin-right:30px"></i></a>
                <a href="https://www.youtube.com/channel/UC9zzm-SiiLWFX-a_M0E1QHQ"><i class="fa fa-youtube-play" style="color: #6c937b;"></i></a>
            </div>
            <div style="width: 100%;text-align: center;margin-top:10px">
                <span style="color: #6c937b">Copyright 2016. Imprevo. Minden jog fenntartva.</span>
            </div>
    </div>
</div>


<script src="/assets/vendor/jquery/jquery.js"></script>
<script src="/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="/assets/vendor/bootstrap/js/bootstrap.js"></script>
<script src="/assets/vendor/nanoscroller/nanoscroller.js"></script>
<script src="/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="/assets/vendor/jquery-placeholder/jquery-placeholder.js"></script>

<!-- Specific Page Vendor -->
<script src="/assets/vendor/jquery-validation/jquery.validate.js"></script>
<script src="/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="/assets/javascripts/theme.js"></script>

<!-- Theme Custom -->
<script src="/assets/javascripts/theme.custom.js"></script>

<!-- Theme Initialization Files -->
<script src="/assets/javascripts/theme.init.js"></script>



<script>
    function goLogin() {
        location.href = '/login'
    }
	
    function goProfile(e) {
		location.href = '/profile';
    }
	
	function Logout(e) {
        e.preventDefault();
        document.getElementById('logout-form').submit();
    }
	
	$('ul.nav li.dropdown').hover(function() {
		$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
	}, function() {
		$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
	});	
</script>
</body>
</html>
