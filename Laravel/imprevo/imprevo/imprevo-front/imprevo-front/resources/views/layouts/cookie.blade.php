<div  id="cookie-div" style="z-index:2; overflow:visible;border-radius:8px; background-color:#9fc3ae;position:fixed;bottom:0px;left:15%;width:70%">
  <form id="form" class="form-horizontal" role="form">
    {{ csrf_field() }}
  </form>
  <div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:5px;">
    <div class="mdl-cell mdl-cell mdl-cell--9-col" style="">
      <span style="font-weight:bold; font-size:16px; color:#024649;">{!! trans('home.cookiespan1') !!}
        <a target="_blank" href="{{isset($settings['cookieUrl'])? $settings['cookieUrl']:''}}" class="on-default pointer" style="color:#0f63ab">{!! trans('home.cookiemore') !!}</a></span>
      </div>
      <div class="mdl-cell mdl-cell--3-col " style="text-align: right">
        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect imprevo-default" style="background-color:#006e73; width:150px;" onclick="setCookie('imprevo', '1', 60);">
          {!! trans('home.cookieaccept') !!}
        </button>
      </div>
    </div>
</div>
<script>
  console.log('cookie');
  function setCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
     document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
     document.getElementById('cookie-div').style.display = 'none';
     console.log('setcookie');
  }

  function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
     var ca = decodedCookie.split(";");
      for(var i = 0; i < ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == " ") {
              c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
          }
      }
      return "";
  }

  function checkCookie() {
    var user=getCookie("imprevo");
    console.log('cookie', user);
    if (user != "") {
      return false;
    } else {
      return true
    }
  }

    if (checkCookie()){
      document.getElementById('cookie-div').style.display = 'inline';
    } else {
      document.getElementById('cookie-div').style.display = 'none';
    }

</script>
