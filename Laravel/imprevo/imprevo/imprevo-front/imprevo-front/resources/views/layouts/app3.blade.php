<!DOCTYPE html>
<html lang="en" style="position: relative;min-height: 100%;">
<?php
use App\Code;
$settings = [];
$codeList = Code::all();
for($i = 0; $i < count($codeList); $i++) {
  $settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
}
?>
<head>
  <!--GaCode-->
@if (isset($settings))
  <?php echo html_entity_decode($settings['gaCode'])?>
@endif
<!--End GaCode-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	@if (isset($settings))
    <meta name="description" content="{{$settings['siteDesc']}}">
    <meta name="keywords" content="{{$settings['keywords']}}">
    <title>{{$settings['siteTitle']}}</title>
	@else
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title>Imprevo</title>

	@endif

     <link href='https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en' rel='stylesheet' type='text/css'>
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		<!-- Vendor CSS -->
	<link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap-custom.css" />

	<link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
	<link rel="stylesheet" href="/assets/vendor/magnific-popup/magnific-popup.css" />
	<link rel="stylesheet" href="/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

	<!-- Theme CSS -->
	<link rel="stylesheet" href="/assets/stylesheets/theme.css" />

	<!-- Skin CSS -->
	<link rel="stylesheet" href="/assets/stylesheets/skins/default.css" />

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="/assets/stylesheets/theme-custom.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<!-- Head Libs -->
	<script src="/assets/vendor/modernizr/modernizr.js"></script>


	<script src="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.min.js"></script>
	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.1/material.indigo-pink.min.css">
	<!-- Material Design icon font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />

	<link rel="stylesheet" href="{{ Config::get('RELATIVE_URL') }}/assets/vendor/pnotify/pnotify.custom.css" />

	<script src="/assets/vendor/pnotify/pnotify.custom.js"></script>

	<!--getmdl-select-->
	<link rel="stylesheet" href="/assets/getmdl-select/getmdl-select.min.css">
	<script src="/assets/getmdl-select/getmdl-select.js"></script>

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
<style>
html, body {
	font-family: 'Roboto', sans-serif;
}


#my-account-button{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #f29f00;
	width:150px;
	height:40px;
}

#my-account-button:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #d98f00;
}

#my-account-button-logged{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #399aed;
	width:150px;
	height:40px;
}

#my-account-button-logged:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #2081d5;
}

.footer-link{
	padding:10px;
	font-weight:700;
	color: #6c937b;
}

#footer-1div{
	text-align:right;
}

#footer-2div{
	text-align:center;
}

#footer-3div{
	text-align:left;
}


#div-footer {
	height:140px;
}
@media (max-width: 1380px){
	#footer-links-div{
		width:90%;
	}

}

@media (max-width:1055px){
	#footer-links-div{
		width:100%;
	}
}



@media (max-width: 769px) {


	#footer-1div{
		text-align:center;
	}

	#footer-2div{
		text-align:center;
	}

	#footer-3div{
		text-align:center;
	}

	#dropdown2:before {
		display: none;
	}

	#dropdown1:before {
		display: none;
	}
	#my-account-button{
		width:100px;
		height:40px;
		font-size:10px;
	}
	#my-account-button-logged{
		font-size: 10px;
		width:100px;
		height:40px;
	}
	.footer-link{
		font-size:12px;
	}

	#div-footer {
		height:230px;
	}
}

</style>
<body style="padding:0px; margin: 0px; height:100%;width:100%; overflow-x: hidden;background:#ffffff;">
  @if (isset($settings))
    <?php echo html_entity_decode($settings['gaBodyCode'])?>
  @endif
<div id="Parent-Div-Background" style="position:relative; margin:0; width:100%;min-height:100%;overflow:hidden">
    @include('layouts.header')
    @yield('content')
    @include('layouts.cookie')
    <div id="div-footer" style="z-index:2;background:#ffffff; position: absolute;left: 0;bottom: 0px;width: 100%;margin:0; padding:0; padding-top:20px;">
        <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-grid--no-spacing" style="justify-content:center; margin:0 auto; padding:0;">
			<div id="footer-links-div" class="mdl-cell mdl-cell mdl-cell--7-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="text-align:center; margin:0;paddig:0;">
				<a class="footer-link" href="https://imprevo.hu/aszf" style="padding:10px">ÁSZF</a>
				<a class="footer-link" href="https://imprevo.hu/adatvedelem" style="padding:10px">Adatvédelem</a>
				<a class="footer-link" href="https://imprevo.hu/céginformáció" style="padding:10px">Céginfó</a>
				<a class="footer-link" href="#" style="padding:10px">Sütik</a>
				<a class="footer-link" href="https://imprevo.hu/sales/course/10" style="padding:10px">Online Angol Tanfolyam</a>
				<a class="footer-link" href="https://imprevo.hu/what-is-imprevo" style="padding:10px">Mi az Imprevo?</a>
				<a class="footer-link" href="https://imprevo.hu/kik-vagyunk" style="padding:10px">Kik vagyunk?</a>
				<a class="footer-link" href="https://imprevo.hu/contact-page" style="padding:10px">Kapcsolat</a>
			</div>
			<div id="footer-links-div-phone" class="mdl-grid mdl-cell mdl-cell--hide-desktop mdl-grid--no-spacing mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="justify-content:center; text-align:center; margin:0;paddig:0;">
				<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:10px">
					<a class="footer-link" href="https://imprevo.hu/aszf" style="padding:10px">ÁSZF</a>
					<a class="footer-link" href="https://imprevo.hu/adatvedelem" style="padding:10px">Adatvédelem</a>
					<a class="footer-link" href="https://imprevo.hu/céginformáció" style="padding:10px">Céginfó</a>
					<a class="footer-link" href="#" style="padding:10px">Sütik</a>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:10px">
					<a class="footer-link" href="https://imprevo.hu/sales/course/10" style="">Online Angol Tanfolyam</a>
					<a class="footer-link" href="https://imprevo.hu/what-is-imprevo" style="">Mi az Imprevo?</a>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:10px">
					<a class="footer-link" href="https://imprevo.hu/kik-vagyunk" style="padding:10px">Kik vagyunk?</a>
					<a class="footer-link" href="https://imprevo.hu/contact-page" style="padding:10px">Kapcsolat</a>
				</div>
			</div>

        </div>
        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align: center;width: 100%; margin-top:10px; color: #6c937b;font-size:18px">
            <a href="https://www.facebook.com/imprevo"><i class="fa fa-facebook" style="color: #6c937b;margin-right:30px"></i></a>
            <a href="https://twitter.com/ImprevoLearning"><i class="fa fa-twitter" style="color: #6c937b;margin-right:30px"></i></a>
            <a href="https://plus.google.com/103750741136736429559"><i class="fa fa-google-plus" style="color: #6c937b;margin-right:30px"></i></a>
            <a href="https://www.youtube.com/channel/UC9zzm-SiiLWFX-a_M0E1QHQ"><i class="fa fa-youtube-play" style="color: #6c937b;"></i></a>
        </div>
        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="width: 100%;text-align: center;margin-top:10px;padding-bottom:15px;">
            <span style="padding-left:210px;color: #6c937b; font-size:12px;font-weight:600;">{!! trans('headerfooter.footer-span29') !!}</span>
			<img src="/images/footer-barion-logo.png" style="padding-left:10px">
        </div>
         <div class="mdl-cell mdl-cell mdl-cell--hide-desktop mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="width: 100%;text-align: center;margin-top:10px;padding-bottom:15px;">
			<div class="mdl-cell mdl-cell mdl-cell--8-col-tablet mdl-cell--4-col-phone">
				<span style="color: #6c937b; font-size:12px;font-weight:600;">{!! trans('headerfooter.footer-span29') !!}</span>
			</div>
			<div class="mdl-cell mdl-cell mdl-cell--8-col-tablet mdl-cell--4-col-phone">
				<img src="/images/footer-barion-logo.png" style="">
			</div>
        </div>
    </div>
</div>


<script src="/assets/vendor/jquery/jquery.js"></script>
<script src="/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="/assets/vendor/bootstrap/js/bootstrap.js"></script>
<script src="/assets/vendor/nanoscroller/nanoscroller.js"></script>
<script src="/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="/assets/vendor/jquery-placeholder/jquery-placeholder.js"></script>

<!-- Specific Page Vendor -->
<script src="/assets/vendor/jquery-validation/jquery.validate.js"></script>
<script src="/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="/assets/javascripts/theme.js"></script>

<!-- Theme Custom -->
<script src="/assets/javascripts/theme.custom.js"></script>

<!-- Theme Initialization Files -->
<script src="/assets/javascripts/theme.init.js"></script>



<script>
    function goLogin() {
        location.href = '/login'
    }

    function goProfile(e) {
		location.href = '/profile';
    }

	function Logout(e) {
        e.preventDefault();
        document.getElementById('logout-form').submit();
    }

	function isMobileDevice() {
		return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
	};

	console.log(isMobileDevice());
	if (!isMobileDevice()){
		$('ul.nav li.dropdown').hover(function() {
			$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
		}, function() {
			$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
		});
	}
</script>
</body>
</html>
