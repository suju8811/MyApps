<style>
.top-header-link{
  color:#ffffff;
  text-decoration: none;
}

.top-header-link:hover{
  color:#f29f00;
  text-decoration: none;
}

.top-header-div-right{
  text-align:right;
  padding:0;
}

.top-header-div-left img{
  width:15px;
  height:15px;
  margin-right:10px;
}


.mdl-profile-button{
  background-color: #d98f00;
  color:#ffffff;
}
.navbar-default {
  width:100%;
  background:rgba(255,255,255,0);
  margin:0;
  border:none;
}

.navbar-default .navbar-nav > li{
  padding:0px;
}
.navbar-default .navbar-nav > li > a{
  color:#000000;
  font-size: 14px;
  font-weight: 550;
  height:80px;
  line-height: 80px;
  padding:0px;
  text-transform: uppercase;
}

.navbar-default .navbar-nav > li > a:hover{
  color:#f29f00;
}

.navbar-default .navbar-nav > li > a:focus{
  color:#f29f00;
}

.navbar-default .navbar-nav > .open > a{
  background:rgba(255,255,255,0);
  color:#ffffff;
  font-size: 14px;
}

.navbar-default .navbar-nav > .open > a:hover{
  background:rgba(255,255,255,0);
  color:#f29f00;
}

.navbar-default .navbar-nav > .open > a:focus{
  background:rgba(255,255,255,0);
  color:#f29f00;
}



.navbar-default .navbar-nav .dropdown .dropdown-menu{
  margin-top:10px;
  padding:15px 20px 15px 20px;
  background:#eaf2ee;
  border: 1px solid #202020;
  box-shadow:0px -2px 2px rgba(0, 0, 0, 0.4);
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li{
  padding:0;
  border-bottom:1px solid #d2d9d6;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a{
  padding:10px 0px 10px 0px ;
  color:#000;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a:hover{
  background:#eaf2ee;
  color:#f29f00;
}

#dropdown2:before {
  position: absolute;
  top: 0px;
  left:160px;
  display: inline-block;
  content: '';
  box-sizing: border-box;
  transform-origin: 0 0;
  transform: rotate(135deg);
  box-shadow: -3px 3px 3px 0 rgba(0, 0, 0, 0.4);
  border: 10px solid black;
  border-color: transparent transparent #eaf2ee #eaf2ee;
}

#dropdown3:before {
  position: absolute;
  top: 0px;
  left:160px;
  display: inline-block;
  content: '';
  box-sizing: border-box;
  transform-origin: 0 0;
  transform: rotate(135deg);
  box-shadow: -3px 3px 3px 0 rgba(0, 0, 0, 0.4);
  border: 10px solid black;
  border-color: transparent transparent #eaf2ee #eaf2ee;
}


.navbar-default .navbar-toggle {
  border:none;
  padding-top:21px;
  padding-right:15px;
}

.navbar-default .navbar-toggle > i{
  font-size:20px;
}

.navbar-default .navbar-toggle.collapsed > .fa-navicon{
  display:block;
}

.navbar-default .navbar-toggle.collapsed > .fa-close{
  display:none;
}

.navbar-default .navbar-toggle > .fa-navicon{
  display:none;
}

.navbar-default .navbar-toggle > .fa-close{
  display:block;
}

.navbar-toggle1.navbar-toggle.collapsed > .header-profile{
  display:block;
}

.navbar-toggle1.navbar-toggle.collapsed > .header-profile1{
  display:none;
}
.navbar-toggle1{
  padding-right:0px !important;
}
.navbar-toggle1 > .header-profile{
  display:none;
}

.navbar-toggle1 > .header-profile1{
  display:block;
}


.navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover{
  background-color: transparent;
}

.navbar-brand{
  padding-top:30px;
}

.header-profile-div{

  height:80px;
  text-align:right;
}

.header-profile-div.active{
  height:80px;
  text-align:right;
  color:#f29f00;
}

.header-profile-img {
  color:#000;
}
.navbar-profile.collapse {
  display:none !important;
}
.navbar-profile.collapse.in {
  display:block !important;
}

.mobile-menu{
  display:none;
}

.fixed {
  -webkit-transform: translate3d(0,0,0);
  position:fixed;
  top:0px;
  -webkit-box-shadow: 0px 2px 5px 1px #004a4d;
  -moz-box-shadow: 0px 2px 5px 1px #004a4d;
  box-shadow: 0px 2px 5px 1px #004a4d;
  z-index:999;
  padding:0px 0px 0px 0px;
}

@media (max-width: 1150px)
{
  .middle-div{
    width:80%;
  }
}

@media (max-width: 950px)
{
  .middle-div{
    width:90%;
  }
}

@media (max-width: 770px)
{
  .mobile-menu{
    display:block;
  }
  .navbar-nav{
    margin-left:0px;
  }
  .mdl-cell--12-col{
    width:100% !important;
  }
  #myNavbar{
    background-color: #eaf2ee;
    position:absolute;
    top:80px;
    width:100%;
  }
  #myNavbar1{
    background-color: #eaf2ee;
    position:absolute;
    top:80px;
    width:100%;
  }
  .mobile-nav-link{
    color:#ddab5f;
    text-decoration: none;
  }
  .middle-div{
    width:100% !important;
  }
  .navbar-header{
    margin-left:16px;
  }
  .navbar-brand img{
    width: 140px;
  }
}


@media (max-width: 500px)
{
  .top-header-div-left, .top-header-div-right{
    font-size:10px;
  }
  .top-header-div-left img{
    width:12px;
    height:12px;
    margin:0px 5px;
  }
  .top-header-div-right{
    text-align: right;
    padding-right:5px;
  }
}

div.top-alert-div{
  display:none;
  position:relative;
  background-color:#e84b05;
  width:100%;
  text-align:center;
  padding:10px 30px;
  margin:0px
}
.top-alert-close{
  font-size:26px;
  position:absolute;
  right:10px;
  top:50%;
  margin-top:-13px;
  color:#fb8c5c;
  cursor:pointer;
}

@media (max-width: 500px)
{
  .top-alert-close{
    font-size:26px;
    position:absolute;
    right:10px;
    top:10px;
    margin-top:0px;
  }
}
</style>
<?php
  $topalert = App\Topalert::all()->first();
?>
@if (Auth::guest())
<div id="header-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%;margin:0;padding:0;">
@else
<div id="header-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%;margin:0;padding:0;">
@endif
  @if (isset($topalert) && $topalert->enable_top_alert)
  <div id="top-alert-div" class="top-alert-div mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="">
      <span class="top-alert-close" onclick="oncloseTopAlert(event)" style="">&times;</span>
      <div class="mdl-cell mdl-cell mdl-cell--12-col" style="margin:0px; padding:0px">
    	   <?php echo html_entity_decode($topalert->alert_text)?>
      </div>
      @if ($topalert->is_show_count)
      <div class="mdl-cell mdl-cell mdl-cell--12-col" style="margin:0px; padding:0px">
      <p id="topalert_counter" style="color:#fff;font-size:16px;margin:0px;line-height: 16px;">{!! trans('headerfooter.top_alert_text') !!}
        <span id="top_alert_days" class="top_alert_days" style="font-weight:600;color:#fff"></span><span class="top_alert_days" style="font-size:16px;font-weight:600;font-style:italic;color:#fff"> {!! trans('headerfooter.day') !!} </span>
        <span id="top_alert_hours" class="top_alert_hours" style="font-weight:600;color:#fff"></span><span class="top_alert_hours" style="font-size:16px;font-weight:600;font-style:italic;color:#fff"> {!! trans('headerfooter.hour') !!} </span>
        <span id="top_alert_mins" class="top_alert_mins" style="font-weight:600;color:#fff"></span><span class="top_alert_mins" style="font-size:16px;font-weight:600;font-style:italic;color:#fff"> {!! trans('headerfooter.min') !!} </span>
        <span id="top_alert_secs" class="top_alert_secs" style="font-weight:600;color:#fff"></span><span class="top_alert_secs" style="font-size:16px;font-weight:600;font-style:italic;color:#fff"> {!! trans('headerfooter.sec') !!} </span>
      </p>
      </div>
      @endif
  </div>
  @endif
  @if (Auth::guest())
  <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="width:100%;background-color:#3b4a51; height:30px;margin:0;padding:0;justify-content:center;">
    <div class="middle-div mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0; margin:0">
      <div class="top-header-div-left mdl-cell mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" style="color:#ffffff;padding:0; margin:0;line-height:30px;width:50%;">
        <img src="/images/header_phone_handle.png" style=""/>Hívj minket: +36-70-286-7401
      </div>
      <div class="top-header-div-right mdl-cell mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" style="color:#ffffff; margin:0;line-height:30px;width:50%;">
        <a class="top-header-link" href="https://imprevo.hu/login">Bejelentkezés</a> | <a class="top-header-link" href="https://imprevo.hu/register">Regisztráció</a>
      </div>
    </div>
  </div>
  @endif
  <div class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%;background-color:#eaf2ee; height:80px;margin:0;padding:0;justify-content:center;">
    <div class="middle-div mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0; margin:0">
      <nav class="navbar navbar-default">
        <div class="navbar-header" style="">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
            <i aria-hidden="true" class="fa fa-navicon" style="color:#000"></i>
            <i aria-hidden="true" class="fa fa-close" style="color:#000"></i>
          </button>
          @if (!Auth::guest())
          <button type="button" class="navbar-toggle1 navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar1" style="padding-top:10px;">
            <img class="header-profile" src="/images/header_profile_deactive.png" style="margin-bottom:15px"/>
            <img class="header-profile1" src="/images/header_profile_active.png" style="margin-bottom:15px"/>
          </button>
          @endif
          <a class="navbar-brand" href="/" style="text-align: left;padding-left: 0px;"><img src='/images/header_imprevo_log.png'></img></a>
        </div>
        @if (!Auth::guest())
        <div class="collapse navbar-collapse navbar-right navbar-profile" id="myNavbar1" style="z-index:9999;height:80px;line-height:80px;padding:0px">
          <div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px 0px; margin:0; width:100%;">
            <div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px 20px; margin:0;border-bottom:1px solid #d2d9d6">
              <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px; border-bottom:1px solid #d2d9d6">
                <img src="/images/header_profile_icon_mail.png" style="margin-right:5px">
                <span>{{ Auth::user()->name}}<br><span style="padding-left:25px">{{ Auth::user()->email}}</span></span>
              </div>
              <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px; border-bottom:1px solid #d2d9d6">
                <img src="/images/header_profile_icon_book.png" style="margin-right:5px">
                <a href="https://imprevo.hu/home">Kurzusok</a>
              </div>
              <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px;border-bottom:1px solid #d2d9d6">
                <img src="/images/header_profile_icon_profill.png" style="margin-right:5px">
                <a href="https://imprevo.hu/profile">Profil</a>
              </div>
              <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px;">
                <img src="/images/header_profile_icon_logout.png" style="margin-right:5px">
                <span onclick="Logout(event)">Kijelentkezés</span>
              </div>
            </div>
          </div>
        </div>
        @endif
        <div class="collapse navbar-collapse  navbar-right" id="myNavbar" style="z-index:9999;height:80px;line-height:80px;padding:0px">
          <ul class="nav navbar-nav mdl-cell--hide-phone mdl-cell--hide-tablet" style="">
            <li><a href="https://imprevo.hu">Főoldal<span style="padding:15px;color:#e1e1e1">|</span></a></li>
            <li>
              <a href="https://imprevo.hu/shop">Webáruház<span style="padding:15px;color:#e1e1e1">|</span></a></a>
            </li>
            <li><a href="https://imprevo.hu/blog">Blog<span style="padding:15px;color:#e1e1e1">|</span></a></a></li>

            <li class="dropdown" style="">
              <a class="dropdown-toggle" data-toggle="dropdown2" href="#">Ingyenesek</a>
              <ul id="dropdown2" class="dropdown-menu">
                <li><a class="a-submenu" href="https://imprevo.hu/20-részes-ingyenes-angol-tanfolyam">20 részes email tanfolyam</a></li>
                <li><a class="a-submenu" href="https://imprevo.hu/20-részes-szókincsbővítő-tanfolyam">20 részes szókincsfejlesztő </a></li>
                <li><a class="a-submenu" href="https://imprevo.hu/tanulj-meg-angolul-6-hónap-alatt-az-imprevo-val">6 hónap alatt angolul </a></li>
                <li><a class="a-submenu" href="https://imprevo.hu/angol-igeidők-ebook">Angol igeidők </a></li>
                <li><a class="a-submenu" href="https://imprevo.hu/angol-levelek-írása">Angol levelek írása  </a></li>
                <li><a class="a-submenu" href="https://imprevo.hu/ingyenes-tartalmak">Ingyenes tananyagok  </a></li>
              </ul>
            </li>
            @if (!Auth::guest())
            <li class="dropdown" style="margin-left:15px;padding-left:15px;height:80px;border-left:1px solid #e1e1e1">
              <a class="dropdown-toggle" data-toggle="dropdown3">
                <div class="header-profile header-profile-div">
                  <img class="header-profile header-profile-user-img" src="/images/header_profile_deactive.png"/>
                  <span style="margin-left:5px;">{{ Auth::user()->name}}</span>
                  <img class="header-profile header-profile-img" src="/images/header_profile_arrow_down.png" style="margin-left:5px"/>
                </div>
              </a>
              <ul id="dropdown3" class="dropdown-menu">
                <li><a class="a-submenu">
                  <img src="/images/header_profile_icon_mail.png" style="margin-right:5px">
                  <span>{{ Auth::user()->name}}<br><span style="padding-left:25px">{{ Auth::user()->email}}</span></span>
                </a></li>
                <li><a class="a-submenu" href="https://imprevo.hu/home">
                  <img src="/images/header_profile_icon_book.png" style="margin-right:5px">
                  <span>Kurzusok</span>
                </a></li>
                <li><a class="a-submenu" href="https://imprevo.hu/profile">
                  <img src="/images/header_profile_icon_profill.png" style="margin-right:5px">
                  <span>Profil</span>
                </a></li>
                <li><a class="a-submenu" href="#" onclick="Logout(event)">
                  <img src="/images/header_profile_icon_logout.png" style="margin-right:5px">
                  <span>Kijelentkezés</span>
                </a></li>
              </ul>
            </li>
            @endif
          </ul>
          <div class="mobile-menu mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px 0px; margin:0;overflow-y:scroll;height:420px">
            <div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px 20px; margin:0;border-bottom:1px solid #d2d9d6">
              <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px; border-bottom:1px solid #d2d9d6">
                <a class="mobile-nav-link" href="https://imprevo.hu">Főoldal </a>
              </div>
              <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px; border-bottom:1px solid #d2d9d6">
                <a class="mobile-nav-link" href="https://imprevo.hu/shop">Webáruház</a>
              </div>
              <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px;">
                <a class="mobile-nav-link" href="https://imprevo.hu/blog">BLOG</a>
              </div>
            </div>
            <div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px 20px; margin:0;background-color:#ffffff;border-bottom:1px solid #d2d9d6;">
              <div class="mdl-grid mdl-cell mdl-cell--12-col" style="height:60px; padding:0px;margin:0;line-height:60px; margin:0;font-size:15px;border-bottom:1px solid #d2d9d6;">
                <div class="mdl-cell mdl-cell mdl-cell--6-col" style="padding:0px; margin:0px;">
                  <a class="mobile-nav-link">INGYENESEK</a>
                </div>
                <div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:right;padding-bottom:20px;">
                  <img src="/images/header_profile_arrow_down_active.png" />
                </div>
              </div>
              <div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px 0px; margin:0;background-color:#ffffff">
                <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px;border-bottom:1px solid #d2d9d6;">
                  <img src="/images/header_bullet.png" style="margin-right:5px"/> <a class="mobile-nav-link" href="https://imprevo.hu/20-részes-ingyenes-angol-tanfolyam">20 részes email tanfolyam</a>
                </div>
                <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px;border-bottom:1px solid #d2d9d6;">
                  <img src="/images/header_bullet.png" style="margin-right:5px"/> <a class="mobile-nav-link" href="https://imprevo.hu/20-részes-szókincsbővítő-tanfolyam">20 részes szókincsfejlesztő</a>
                </div>
                <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px;border-bottom:1px solid #d2d9d6;">
                  <img src="/images/header_bullet.png" style="margin-right:5px"/> <a class="mobile-nav-link" href="https://imprevo.hu/tanulj-meg-angolul-6-hónap-alatt-az-imprevo-val">6 hónap alatt angolul</a>
                </div>
                <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px;border-bottom:1px solid #d2d9d6;">
                  <img src="/images/header_bullet.png" style="margin-right:5px"/> <a class="mobile-nav-link" href="https://imprevo.hu/angol-igeidők-ebook">Angol igeidők</a>
                </div>
                <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px;border-bottom:1px solid #d2d9d6;">
                  <img src="/images/header_bullet.png" style="margin-right:5px"/> <a class="mobile-nav-link" href="https://imprevo.hu/angol-levelek-írása">Angol levelek írása</a>
                </div>
                <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px">
                  <img src="/images/header_bullet.png" style="margin-right:5px"/> <a class="mobile-nav-link" href="https://imprevo.hu/ingyenes-tartalmak">Ingyenes tananyagok</a>
                </div>
              </div>
            </div>
            @if (Auth::guest())
            <div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px 20px; margin:0;border-bottom:1px solid #d2d9d6">
              <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px; border-bottom:1px solid #d2d9d6">
                <button id="my-login-button" onclick="goLogin(event)" class="mdl-profile-button mdl-button mdl-js-button mdl-button--raised" style="border-radius:2px;width:100%;outline:none;border:none;">
                  Login
                </button>
              </div>
              <div class="mdl-cell mdl-cell mdl-cell--12-col" style="line-height:60px; margin:0;font-size:15px; border-bottom:1px solid #d2d9d6">
                <button id="my-register-button" onclick="location.href='/register'" class="mdl-profile-button mdl-button mdl-js-button mdl-button--raised" style="border-radius:2px;width:100%;outline:none;border:none;">
                  Register
                </button>
              </div>
            </div>
            @endif
          </div>
        </nav>
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </div>
    </div>
  </div>
  <script>
  var alert_left_days = null;
  var alert_left_hours = null;
  var alert_left_mins = null;
  var alert_left_secs = null;
  var alert_distance = 0;
  //var timeLocal = new Date();

  //var millDiff = timeLocal.getTime() - timeServer.getTime();
  if (sessionStorage.getItem("topalert") == "closed"){
    document.getElementById('top-alert-div').style.display='none';
  } else {
    document.getElementById('top-alert-div').style.display='inline';
  }
  $(document).ready(function(){

  @if ($topalert)
  @if ($topalert->is_show_count)
  @if ($topalert->countdown)

  var t = "{{$topalert->countdown}}".split(/[- :]/);
  var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
  var end_time = new Date(d).getTime();

  var strtime = <?php echo '"'.date('Y-m-d H:i:s').'"' ?>;
  t = strtime.split(/[- :]/);
  var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
  var alert_timeServer = new Date(d).getTime();

  alert_distance = (end_time - alert_timeServer) / 1000 ;

  if (alert_distance > 0)
  {
    alert_left_secs = Math.floor(alert_distance % 60);
    alert_left_mins = Math.floor((alert_distance / 60) % 60);
    alert_left_hours = Math.floor((alert_distance / (60 * 60)) % 24);
    alert_left_days = Math.floor(alert_distance / (60 * 60 * 24));
    $('#top_alert_days').html(alert_left_days);
    $('#top_alert_mins').html(alert_left_mins);
    $('#top_alert_hours').html(alert_left_hours);
    $('#top_alert_secs').html(alert_left_secs);

  }


  setInterval(function(){
    alert_distance = alert_distance - 1;
    if (alert_distance > 0)
    {
      alert_left_secs = Math.floor(alert_distance % 60);
      alert_left_mins = Math.floor((alert_distance / 60) % 60);
      alert_left_hours = Math.floor((alert_distance / (60 * 60)) % 24);
      alert_left_days = Math.floor(alert_distance / (60 * 60 * 24));
      $('#top_alert_days').html(alert_left_days);
      $('#top_alert_mins').html(alert_left_mins);
      $('#top_alert_hours').html(alert_left_hours);
      $('#top_alert_secs').html(alert_left_secs);
      if (alert_left_days == 0){
        $('.top_alert_days').hide();
      }
      if (alert_left_mins == 0){
        $('.top_alert_mins').hide();
      }
      if (top_alert_hours == 0){
        $('.top_alert_hours').hide();
      }
      if (top_alert_secs == 0){
        $('.top_alert_secs').hide();
      }
    }
  }, 1000);
  @endif
  @endif
  @endif

  window.onscroll = function ()  { //detect page scroll
    home_header = document.getElementById("header-div");

    if (home_header){
      sticky = $('#header-div');
      scroll = document.documentElement.scrollTop || document.body.scrollTop;

      if (scroll >= 80) {
        sticky.addClass('fixed');
      }
      else{
        sticky.removeClass('fixed');
      }
    }
  };

  if (sessionStorage.getItem("topalert") == "closed"){
    document.getElementById('top-alert-div').style.display='none';
  } else {
    document.getElementById('top-alert-div').style.display='inline';
  }

  });
  </script>

  <script>
  function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
  };

  if (!isMobileDevice()){
    $('ul.nav li.dropdown').hover(function() {
      $('.header-profile').addClass('active');
      $('.header-profile-user-img').attr('src', "/images/header_profile_active.png");
      $(".header-profile-img").attr("src", "/images/header_profile_arrow_down_active.png");
      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
    }, function() {
      $('.header-profile').removeClass('active');
      $('.header-profile-user-img').attr('src', "/images/header_profile_deactive.png");
      $(".header-profile-img").attr("src", "/images/header_profile_arrow_down.png");
      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
    });
  }


  function Logout(e) {
      e.preventDefault();
      document.getElementById('logout-form').submit();
  }

  function oncloseTopAlert(e){
    sessionStorage.setItem("topalert", "closed");
    document.getElementById('top-alert-div').style.display='none'
  }
</script>
