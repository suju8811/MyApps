<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Audio Player</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- required links -->
    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <link href="/assets/audio-player/css/audioPlayer.css" rel="stylesheet"/>
    <script src="/assets/audio-player/js/audioPlayer.js"></script>
</head>
<body>
<div class="dsap" data-audio="/assets/audio-player/audio/music5.mp3" data-thumbnail="/images/audioplayer.png" data-autoplay="false" style=""></div>															
</body>
</html>