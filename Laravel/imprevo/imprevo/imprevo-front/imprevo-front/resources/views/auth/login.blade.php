@extends('layouts.app2')
<style>
.is-focused .mdl-textfield__input {
	border:none;
	background-color:#ffffff;
}

.mdl-dialog{	
	padding:10px;
	background-color:#ffffff; 
	position:relative; 
	top: 50px; 
	left: 50%; 
	margin-left:-280px !important; 
	width: 560px !important;	
	margin-bottom:20px;	
}

#dialog-title{
	font-size: 30px;
}

#dialog-span1{
	font-size: 16px; 
}
#dialog-header{
	padding:30px 30px 0 30px;
}	

#login-footer-div{
	margin:20px 0 0 0;
	padding: 10px 0 0px 0;
}

div.mdl-textfield{
	margin-top:10px
}

#login-button-div{
	border-right:1px solid #dddddd;
	padding:10px 0px 0px 0px;
}	

#login-button-div button{
	width:120px;
}

#login-footer-span-div{
	padding-top:10px;
	padding-left:10px;
}
@media (max-width: 840px) {	
	.mdl-dialog{	
		padding:5px;
		background-color:#ffffff; 
		position:relative; 
		top: 10px; 
		left: 50%; 
		margin-left:-170px !important; 
		margin-bottom:20px;
		width: 340px !important;		
	}
	#dialog-title{
		font-size: 25px;
	}
	#dialog-span1{
		font-size: 14px;
	}
	
	div.mdl-textfield{
		margin-top:0px
	}
	#login-button-div{
		text-align:center;
		border:none;
		margin:10px 0px 0 0px;
	}
	
	#login-button-div button{
		width:100%;
	}
	
	#login-footer-span-div{
		
	}	
	
	#login-footer-span-div a{
		font-size:12px;
	}
	
	#login-footer-span-div p{
		font-size:12px;
	}
	
	#login-footer-div{
		margin:0px 0 0 0;
		padding: 0px 0 0 0;
	}	

}

@media (max-width: 768px) {	
	.mdl-dialog{	
		margin-bottom:150px;		
	}
}
</style>
@section('content')
    <div class="mdl-dialog">
        <div id="dialog-header" style="border-bottom: 1px solid #eee">
            <p id="dialog-title" style=" color:#3b4a51">{!! trans('auth.logintitle') !!}</p>
            <p id="dialog-span1" style="color:#b1b1b1">{!! trans('auth.loginspan-1') !!}</p>
        </div>
        @if ($errors->has('email'))
        <div style="color:#cf0000;background-color:#f8f8f8;padding:10px 30px 10px 30px;border-bottom: 1px solid #eee">
              <span style="font-weight:bold;">{!! trans('auth.error-span-1') !!} {{ $errors->first('email') }}</span>
        </div>
        @elseif ($message = Session::get('error'))
          <div style="color:#cf0000;background-color:#f8f8f8;padding:10px 30px 10px 30px;border-bottom: 1px solid #eee">
              <span style="font-weight:bold;">{{ $message }}</span>
          </div>
        @endif
        <div class="mdl-dialog__content" style="padding:10px;margin:0;">
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:10px;">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" style="width:100%">
                {{ csrf_field() }}
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;">
                    <input class="mdl-textfield__input" type="email" id="email" name="email" style="" value="{{ old('email') }}" autofocus>
                    <label class="mdl-textfield__label" for="email" style="font-weight:bold">{!! trans('auth.email') !!}</label>
                    @if ($errors->has('email'))
                        <span class="mdl-textfield__error">{{ $errors->first('email') }}</span>
                    @endif
                </div>					
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;">
                    <input class="mdl-textfield__input" type="password" id="password" name="password">
                    <label class="mdl-textfield__label" for="Password" style="font-weight:bold">{!! trans('auth.password') !!}</label>
                    @if ($errors->has('password'))
                        <span class="mdl-textfield__error">{{ $errors->first('password') }}</span>
                    @endif
                </div>
                <p>
                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="remember">
                        <input type="checkbox" id="remember" name="remember" class="mdl-checkbox__input" {{ old('remember') ? 'checked' : '' }}>
                        <span class="mdl-checkbox__label" style="color:#979797">{!! trans('auth.remember') !!}</span>
                    </label>
                </p>
                <div id="login-footer-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing" style="width: 100%;">
                    <div id="login-button-div" class="mdl-cell mdl-cell--4-col mdl-cell--4-col-phone" style="height:50px">
                        <button class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored" type="submit" style="background-color:#399aed;">
                            {!! trans('auth.login') !!}
                        </button>
                    </div>

                    <div id="login-footer-span-div" class="mdl-cell mdl-cell--8-col mdl-cell--4-col-phone">
                        <a href="{{ url('/password/reset') }}" style="color:#399aed">{!! trans('auth.forgot') !!}</a>
                        <p style="color:#000000;">{!! trans('auth.login-span-2') !!} <a href="{{ url('/register') }}" style="color:#399aed">{!! trans('auth.register') !!}</a></p>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <p>{{ $message }}</p>
                    </div>
                @endif
            </form>
			</div>
        </div>
    </div>
<script>
document.title = "IMPREVO – Bejelentkezés";
function MaterialSelect(element) {
  'use strict';

  this.element_ = element;
  this.maxRows = this.Constant_.NO_MAX_ROWS;
  // Initialize instance.
  this.init();
}

MaterialSelect.prototype.Constant_ = {
  NO_MAX_ROWS: -1,
  MAX_ROWS_ATTRIBUTE: 'maxrows'
};

MaterialSelect.prototype.CssClasses_ = {
  LABEL: 'mdl-textfield__label',
  INPUT: 'mdl-select__input',
  IS_DIRTY: 'is-dirty',
  IS_FOCUSED: 'is-focused',
  IS_DISABLED: 'is-disabled',
  IS_INVALID: 'is-invalid',
  IS_UPGRADED: 'is-upgraded'
};

MaterialSelect.prototype.onKeyDown_ = function(event) {
  'use strict';

  var currentRowCount = event.target.value.split('\n').length;
  if (event.keyCode === 13) {
    if (currentRowCount >= this.maxRows) {
      event.preventDefault();
    }
  }
};

MaterialSelect.prototype.onFocus_ = function(event) {
  'use strict';

  this.element_.classList.add(this.CssClasses_.IS_FOCUSED);
};

MaterialSelect.prototype.onBlur_ = function(event) {
  'use strict';

  this.element_.classList.remove(this.CssClasses_.IS_FOCUSED);
};

MaterialSelect.prototype.updateClasses_ = function() {
  'use strict';
  this.checkDisabled();
  this.checkValidity();
  this.checkDirty();
};

MaterialSelect.prototype.checkDisabled = function() {
  'use strict';
  if (this.input_.disabled) {
    this.element_.classList.add(this.CssClasses_.IS_DISABLED);
  } else {
    this.element_.classList.remove(this.CssClasses_.IS_DISABLED);
  }
};

MaterialSelect.prototype.checkValidity = function() {
  'use strict';
  if (this.input_.validity.valid) {
    this.element_.classList.remove(this.CssClasses_.IS_INVALID);
  } else {
    this.element_.classList.add(this.CssClasses_.IS_INVALID);
  }
};

MaterialSelect.prototype.checkDirty = function() {
  'use strict';
  if (this.input_.value && this.input_.value.length > 0) {
    this.element_.classList.add(this.CssClasses_.IS_DIRTY);
  } else {
    this.element_.classList.remove(this.CssClasses_.IS_DIRTY);
  }
};

MaterialSelect.prototype.disable = function() {
  'use strict';

  this.input_.disabled = true;
  this.updateClasses_();
};

MaterialSelect.prototype.enable = function() {
  'use strict';

  this.input_.disabled = false;
  this.updateClasses_();
};

MaterialSelect.prototype.change = function(value) {
  'use strict';

  if (value) {
    this.input_.value = value;
  }
  this.updateClasses_();
};

MaterialSelect.prototype.init = function() {
  'use strict';

  if (this.element_) {
    this.label_ = this.element_.querySelector('.' + this.CssClasses_.LABEL);
    this.input_ = this.element_.querySelector('.' + this.CssClasses_.INPUT);

    if (this.input_) {
      if (this.input_.hasAttribute(this.Constant_.MAX_ROWS_ATTRIBUTE)) {
        this.maxRows = parseInt(this.input_.getAttribute(
            this.Constant_.MAX_ROWS_ATTRIBUTE), 10);
        if (isNaN(this.maxRows)) {
          this.maxRows = this.Constant_.NO_MAX_ROWS;
        }
      }

      this.boundUpdateClassesHandler = this.updateClasses_.bind(this);
      this.boundFocusHandler = this.onFocus_.bind(this);
      this.boundBlurHandler = this.onBlur_.bind(this);
      this.input_.addEventListener('input', this.boundUpdateClassesHandler);
      this.input_.addEventListener('focus', this.boundFocusHandler);
      this.input_.addEventListener('blur', this.boundBlurHandler);

      if (this.maxRows !== this.Constant_.NO_MAX_ROWS) {
        // TODO: This should handle pasting multi line text.
        // Currently doesn't.
        this.boundKeyDownHandler = this.onKeyDown_.bind(this);
        this.input_.addEventListener('keydown', this.boundKeyDownHandler);
      }

      this.updateClasses_();
      this.element_.classList.add(this.CssClasses_.IS_UPGRADED);
    }
  }
};

MaterialSelect.prototype.mdlDowngrade_ = function() {
  'use strict';
  this.input_.removeEventListener('input', this.boundUpdateClassesHandler);
  this.input_.removeEventListener('focus', this.boundFocusHandler);
  this.input_.removeEventListener('blur', this.boundBlurHandler);
  if (this.boundKeyDownHandler) {
    this.input_.removeEventListener('keydown', this.boundKeyDownHandler);
  }
};

// The component registers itself. It can assume componentHandler is available
// in the global scope.
componentHandler.register({
  constructor: MaterialSelect,
  classAsString: 'MaterialSelect',
  cssClass: 'mdl-js-select',
  widget: true
});
    </script>
@endsection
