@extends('layouts.app2')
<style>
.is-focused .mdl-textfield__input {
	border:none;
	background-color:#ffffff;
}

#submitBtn:disabled{
	color:#ffffff;
	background-color:grey;
}

#submitBtn{
	background-color:#399aed;
	width:120px;
}

.mdl-dialog{
	padding:10px;
	background-color:#ffffff;
	position:relative;
	top: 50px;
	left: 50%;
	margin-left:-280px !important;
	width: 560px !important;
}

@media (max-width: 767px) {
	.mdl-dialog{
		padding:5px;
		background-color:#ffffff;
		position:relative;
		top: 50px;
		left: 50%;
		margin-left:-170px !important;
		margin-bottom:200px;
		width: 340px !important;
	}
}
</style>
<script src="https://www.google.com/recaptcha/api.js?hl=hu" async defer></script>
@section('content')
    <div class="mdl-dialog" style="">
		@if ($is_free)
		<div style="padding:30px 30px 0 30px;border-bottom: 1px solid #eee">
            <p style="font-size: 30px">{!! trans('auth.registertitle1') !!}</p>
            <p style="font-size: 16px;">{!! trans('auth.registertitlespan1') !!}</p>
        </div>
		@else
        <div style="padding:30px 30px 0 30px;border-bottom: 1px solid #eee">
            <p style="font-size: 30px">{!! trans('auth.registertitle2') !!}</p>
            <p style="font-size: 16px;">{!! trans('auth.regsitertitlespan2') !!}</p>
        </div>
		@endif

			@if ($message = Session::get('spam_email'))
		<div class="form-group">
				<div class="alert alert-error" style="color:#ff0000;text-align:center;">
					<p>{{ $message }}</p>
				</div>
		</div>
		@endif
        @if ($errors->has('email'))
        <div style="color:#cf0000;background-color:#f8f8f8;padding:10px 30px 10px 30px;border-bottom: 1px solid #eee">
              <span style="">{!! trans('auth.resgistererrorspan1') !!} {{ $errors->first('email') }}</span>
        </div>
        @elseif ($errors->has('password'))
        <div style="color:#cf0000;background-color:#f8f8f8;padding:10px 30px 10px 30px;border-bottom: 1px solid #eee">
              <span style="">{!! trans('auth.resgistererrorspan2') !!}</span>
        </div>
        @endif

        <div class="mdl-dialog__content" style="padding:10;">
            <form id="register-form" class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}
                <input id="name" type="hidden" class="form-control" name="name" value="nameisn'tneed">
                <div class="mdl-grid mdl-cell--12-col">
                    <div class="mdl-cell mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
                        <input class="mdl-textfield__input" type="email" id="email" name="email" value="{{ old('email') }}" autofocus>
                        <label class="mdl-textfield__label" for="email">{!! trans('auth.registeremail') !!}</label>
                        @if ($errors->has('email'))
                          <span class="mdl-textfield__error">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div>

                <div class="mdl-grid mdl-cell--12-col">
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
                            <input class="mdl-textfield__input" type="password" id="password" name="password">
                            <label class="mdl-textfield__label" for="Password">{!! trans('auth.registerpassword') !!}</label>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
                            <input class="mdl-textfield__input" type="password" id="password-confirm" name="password_confirmation">
                            <label class="mdl-textfield__label" for="Password">{!! trans('auth.registerpassword') !!}</label>
                            @if ($errors->has('password'))
                                <span class="mdl-textfield__error">{!! trans('auth.registererror1') !!}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding-left:15px">
                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="agreeForService">
                        <input type="checkbox" id="agreeForService" class="mdl-checkbox__input" onclick="toggleAgree()">
                        <span>{!! trans('auth.registerspan3') !!}<a href="https://imprevo.hu/aszf" target="_blank">{!! trans('auth.registerspan4') !!}</a></span>
                    </label>
                </div>

                <div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding-left:15px">
                    <button id="submitBtn" class="g-recaptcha mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" data-sitekey="6LezFUcUAAAAAMKdfExm9RZ-kkVLwroQk7ko8ulp"  data-type="image" data-callback='onSubmit' style=""  disabled="disabled">
                        {!! trans('auth.registerbtn') !!}
                    </button>
                </div>
            </form>
        </div>
    </div>
<script>

		document.title = "IMPREVO – Ingyenes regisztráció";

		$(document).ready(function() {
			$("#submitBtn").attr('disabled', 'disabled');
		})

		function onSubmit(token) {
			console.log(token);
		  document.getElementById("register-form").submit();
		}

    function toggleAgree() {
        var isAgree = document.getElementById("agreeForService").checked
        if(!isAgree) {
            $("#submitBtn").attr('disabled', 'disabled');
        } else {
            $("#submitBtn").removeAttr('disabled');
        }
    }
</script>
@endsection
