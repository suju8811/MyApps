@extends('layouts.app2')

@section('content')
    <dialog class="mdl-dialog" style="width:560px;padding:0;margin-top:100px">
        <div style="padding:50px 30px 30px 30px;border-bottom: 1px solid #eee">
            <p style="font-size: 30px">{!! trans('auth.success_span1') !!}</p>
        </div>

        <div class="mdl-dialog__content">
          <form class="form-horizontal" role="form" method="GET"  action="{{ url('/login') }}">
              {{ csrf_field() }}
            <div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0px 0 0 0;padding: 0px 0 0 0;width: 100%;">
              <div class="mdl-cell mdl-cell--12-col" style="padding:10px 0 0 0px">
                  <span>{!! trans('auth.success_span2') !!}</span>
              </div>
              <div class="mdl-cell mdl-cell--12-col" style="padding:10px 0 0 0px">
                  <span>{!! trans('auth.success_span3') !!} <span style="color:#399aed">{!! trans('auth.success_span4') !!}<span></span>
              </div>
              <div class="mdl-cell mdl-cell--12-col" style="margin:100px 0 0 0">
                  <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" type="submit" style="margin:0 0 0 10px;width:100%">
                          {!! trans('auth.forgot_span5') !!}
                  </button>
              </div>
          </div>
        </form>
    </dialog>
    <script>
        var dialog = document.querySelector('dialog');
        var showDialogButton = document.querySelector('#show-dialog');
        if (! dialog.showModal) {
            dialogPolyfill.registerDialog(dialog);
        }
        dialog.showModal();
    </script>
@endsection
