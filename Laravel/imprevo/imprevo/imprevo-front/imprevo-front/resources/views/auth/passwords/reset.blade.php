@extends('layouts.app2')
<style>
.mdl-dialog{	
	padding:10px;
	background-color:#ffffff; 
	position:relative; 
	top: 50px; 
	left: 50%; 
	margin-left:-280px !important; 
	width: 560px !important;		
}

@media (max-width: 767px) {	
	.mdl-dialog{	
		padding:5px;
		background-color:#ffffff; 
		position:relative; 
		top: 50px; 
		left: 50%; 
		margin-left:-170px !important; 
		margin-bottom:200px;
		width: 340px !important;		
	}
}
</style>
@section('content')
    <div class="mdl-dialog" style="">
        <div style="padding:30px 30px 0 30px;border-bottom: 1px solid #eee">
            <p style="font-size: 30px">{!! trans('auth.resettitle') !!}</p>
            <p style="font-size: 16px;">{!! trans('auth.resetspan') !!}</p>
        </div>
        <div class="mdl-dialog__content">
            <form id="resetPasswordForm" class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="mdl-grid mdl-cell--12-col">
                    <div class="mdl-cell mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
                        <input class="mdl-textfield__input" type="email" id="email" name="email">
                        <label class="mdl-textfield__label" for="email">{!! trans('auth.resetemail') !!}</label>
                        @if ($errors->has('email'))
                            <span class="mdl-textfield__error">{{ $errors->first('email') }}</span>
                        @endif 
                    </div>
                </div>
                <div class="mdl-grid mdl-cell--12-col">
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
                            <input class="mdl-textfield__input" type="password" id="password" name="password">
                            <label class="mdl-textfield__label" for="Password">{!! trans('auth.password') !!}</label>
                            @if ($errors->has('password'))
                                <span class="mdl-textfield__error">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
                            <input class="mdl-textfield__input" type="password" id="password-confirm" name="password_confirmation">
                            <label class="mdl-textfield__label" for="Password">{!! trans('auth.confirmpassword') !!}</label>
                        </div>
                    </div>
                </div>
                <div class="mdl-grid mdl-cell mdl-cell--12-col">
                    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" type="submit" style="width:120px">
                        {!! trans('auth.resetpassword') !!}
                    </button>
                </div>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </form>
        </div>
    </div>
    <script>

    </script>
@endsection
