@extends('layouts.app2')
<style>
.mdl-dialog{	
	padding:10px;
	background-color:#ffffff; 
	position:relative; 
	top: 50px; 
	left: 50%; 
	margin-left:-280px !important; 
	width: 560px !important;		
}

@media (max-width: 767px) {	
	.mdl-dialog{	
		padding:5px;
		background-color:#ffffff; 
		position:relative; 
		top: 50px; 
		left: 50%; 
		margin-left:-170px !important; 
		margin-bottom:200px;
		width: 340px !important;		
	}
}
</style>
@section('content')
    <div class="mdl-dialog" style="">
        <div style="padding:30px 30px 0 30px;border-bottom: 1px solid #eee">
            <p style="font-size: 30px">{!! trans('auth.forgot') !!}?</p>
            @if ($errors->has('email'))
            <p style="font-size: 16px; color:#979797">{!! trans('auth.forgot_span1') !!}</p>
            @else
            <p style="font-size: 16px; color:#979797">{!! trans('auth.forgot_span2') !!}</p>
            @endif
        </div>
        @if ($errors->has('email'))
        <div style="color:#cf0000;background-color:#f8f8f8;padding:10px 30px 10px 30px;border-bottom: 1px solid #eee">
              <span style="font-weight:bold;">{!! trans('auth.forgot_span3') !!} <a href="{{ url('/register') }}" style="color:#399aed">Register!</a></span>
        </div>
        @endif
        <div class="mdl-dialog__content" style="padding:0px 30px 0px 30px">
            <form class="form-horizontal" role="form" method="POST"  action="{{ url('/password/email') }}">
                {{ csrf_field() }}
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
                    <input class="mdl-textfield__input" type="email" id="email" name="email"  value="{{ old('email') }}" autofocus>
                    <label class="mdl-textfield__label" for="email">{!! trans('auth.email') !!}</label>
                    @if ($errors->has('email'))
                        <span class="mdl-textfield__error" style="color:#cf0000;font-weight:bold;">{!! trans('auth.forgot_span4') !!}</span>
                    @endif
                </div>
                <div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:20px 0 0 0;padding: 10px 0 0 0;width: 100%;">
                    <div class="mdl-cell mdl-cell--4-col" style="margin:10px 0 0 0">
                        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" type="submit" style="background-color:#399aed;width:120px">
                            {!! trans('auth.forgot_but1') !!}
                        </button>
                    </div>
                    <div class="mdl-cell mdl-cell--8-col" style="padding-top:7px">
                        <p><a href="{{ url('/login') }}" style="color:#979797; text-decoration: none;">{!! trans('auth.forgot_span5') !!}</a></p>
                    </div>
                </div>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </form>
        </div>
    </div>
    <script>

        if ('{{session('status')}}' == 'We have e-mailed your password reset link!')
        {
            location.href = 'forgot_password_success';
        }
    </script>
@endsection
