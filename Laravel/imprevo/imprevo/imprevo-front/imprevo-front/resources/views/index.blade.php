@extends('layouts.app')
<style>
#know_more_button {
	background:#399aed;
	color:white;
	font-size: 14px;
	width:100%;
	padding:0px 5px 0px 5px;
}
#know_more_button:hover {
	background:#2081d5;
}
.custom-row{
	background:#ffffff;

}
.custom-row:hover{
	background:#f0f0f0;
}

#banner-image {
	padding:10px;
	margin:0px 0px 0px 15px;
	width:110px;
	height:110px;
}
#brecumb_div{
	background: #9fc3ae;
	padding:5px 40px 5px 0px;
	padding-left:80px;
	font-size: 16px;
}


.panel-body p{
	color: #b1b1b1;
}
@media (max-width: 400px) {
	#banner-image {
		padding:0px;
		margin:15px 0px 0px 10px;
		width:60px;
		height:60px;
	}

	#brecumb_div{
		padding-left:25px;
	}

}
</style>
@section('content')
<div id="main-content" class="mdl-layout__content" style="width:100%;background-color:#ebf3ef; margin-bottom:30px;min-height:750px;">
	<div id="brecumb_div" style="">
		<span><a href="/" style="color:#d5e7dc;line-height: 40px;text-decoration: none">{!! trans('home.imprevo') !!}</a></span>
		<span style="padding:0px 5px 0px 5px;color:#d5e7dc;line-height: 40px">/</span>
		<span style="color:#fff;line-height: 40px">{!! trans('home.dashboard') !!}</span>
	</div>
	<div class="container-fluid" style="padding:20px 0;background:#ebf3ef; margin-bottom:50px;">
		<div class="container">
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0">
				<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
					<h1 id="course_title" class="matT2 marB3">{!! trans('dashboard.h1') !!}</h1>
				</div>
				@foreach ($courses as $course)
				@if (!$course->is_hide)
				<?php
				$assigned_courses = Auth::user()->courses;
				$b_assigned = false;
				if ($assigned_courses){
					foreach ($assigned_courses as $item){
						if ($item->id == $course->id)
						$b_assigned = true;
					}
				}
				?>
				@if ($course->is_free || $course->is_purchased || Auth::user()->permission >= 3 || $b_assigned)
				<div class="mdl-cell mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
					<div class="panel_div mdl-cell--hide-phone" onclick="goCourse({{$course->id}})">
						<div class="panel panel-default" style="margin-bottom:0px">
							<div class="panel-body">
								@if ($course->is_free)
								<button class="btn btn-danger" style="margin-bottom:10px">{!! trans('dashboard.button') !!}</button>
								@else
								<br><br>
								@endif
								<img src="{{$course->product_image}}" alt="Avatar" class="img img-responsive marT2 product-image" style="margin-top:0px"><hr>
								<h3 class="panel-title">{{$course->title}}</h3>
							</div>
							<div class="panel-footer">
								<div class="row">
									<div class="col-sm-6" style="text-align:left">
										<span>{{$course->lesson_count}} {!! trans('dashboard.lesson') !!}</span>
									</div>
									<div class="col-sm-6" style="text-align:right">
										<?php $first_lesson = $course->lessons[0];  $first_exe = $first_lesson->exercises[0]; ?>
										<a class="span_pad1 blue" @if ($course->last_exercise) href="/exercise/{{$course->last_exercise}}" @else href="/exercise/{{$first_exe->id}}" @endif>
											<i class="fa fa-angle-right fa-lg"></i> {!! trans('dashboard.link1') !!}
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="middle">
							<div class="text">
								<button class="btn panel_btn btn-lg marT2" onclick="location.href='/course/{{$course->id}}?tab=1'">{!! trans('dashboard.button1') !!}</button><br>
								<?php
								$go_exe = $first_exe->id;
								if ($course->last_exercise) {
									$go_exe = $course->last_exercise;
								}
								?>
							</div>
						</div>
					</div>
					<div class="panel_div1" data-toggle="modal" data-target="#panel_modal" onclick="triggerModal({{$course->id}}, '{{$course->title}}',  {{$course->product}}, '{{$course->product_image}}', 1)">
						<div class="panel panel-default" style="margin-bottom:0px">
							<div class="panel-body">
								<div style="width:50%; float:left;">
									<h3 class="panel-title">{{$course->title}}</h3>
									@if ($course->is_free)
									<button class="btn btn-danger marT4">{!! trans('dashboard.button') !!}</button>
									@endif
								</div>
								<div style="float:left;width:50%;"><img src="{{$course->product_image}}" alt="Avatar" class="img img-responsive marT2 panel_image" style="width:150px;"></div>
							</div>
							<div class="panel-footer">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0; margin:0">
									<div class="mdl-cell--6-col" style="width:50%;text-align:left">
										<span>{{$course->lesson_count}} {!! trans('dashboard.lesson') !!}</span>
									</div>
									<div class="mdl-cell--6-col" style="width:50%;text-align:right">
										<?php $first_lesson = $course->lessons[0];  $first_exe = $first_lesson->exercises[0]; ?>
										<a class="span_pad1 blue" @if ($course->last_exercise) href="/exercise/{{$course->last_exercise}}" @else href="/exercise/{{$first_exe->id}}" @endif>
											<i class="fa fa-angle-right fa-lg"></i> {!! trans('dashboard.link1') !!}
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@else
				<div class="mdl-cell mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
					<div class="panel_div mdl-cell--hide-phone" onclick="goCourse({{$course->id}})">
						<div class="panel panel-default" style="margin-bottom:0px">
							<div class="panel-body grey_effect"><br><br>
								<img src="{{$course->product_image}}" alt="Avatar" class="img img-responsive product-image"><hr>
								<h3 class="panel-title" >{{$course->title}}</h3>
							</div>
							<div class="panel-footer">
								<div class="row">
									<div class="col-sm-6" style="text-align:left">
										<a href="/course/{{$course->id}}?tab=2"><span class="orange">{!! trans('dashboard.span1') !!}</span></a>
									</div>
									<div class="col-sm-6" style="text-align:right">
										<a class="span_pad2 blue" href="/shoppingcart?products={{$course->product}}"><i class="fa fa-shopping-cart"></i> {!! trans('dashboard.link2') !!}</a>
									</div>
								</div>
							</div>
						</div>
						<div class="middle">
							<div class="text">
								<button class="btn panel_btn btn-lg" onclick="goshop(event, {{$course->product}})">{!! trans('dashboard.button3') !!}</button><br>
								<button class="btn panel_btn btn-lg marT2" onclick="location.href='/course/{{$course->id}}?tab=1'">{!! trans('dashboard.button4') !!}</button><br>
								<button class="btn panel_btn btn-lg marT2" onclick="location.href='/course/{{$course->id}}?tab=2'">{!! trans('dashboard.button5') !!}</button>
							</div>
						</div>
					</div>
					<div class="panel_div1 orange_effect"  data-toggle="modal" data-target="#panel_modal" onclick="triggerModal({{$course->id}}, '{{$course->title}}',  {{$course->product}}, '{{$course->product_image}}', 0)">
						<div class="panel panel-default" style="margin-bottom:0px">
							<div class="panel-body grey_effect">
								<div style="width:50%; float:left;">
									<h3 class="panel-title ">{{$course->title}}</h3>
								</div>
								<div style="float:left;width:50%;"><img src="{{$course->product_image}}" alt="Avatar" class="img img-responsive marT2 panel_image" style="width:150px;"></div>
							</div>
							<div class="panel-footer">
								<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0; margin:0">
									<div class="mdl-cell--6-col" style="width:50%;text-align:left">
										<a href="/course/{{$course->id}}?tab=2"><span class="orange">{!! trans('dashboard.span1') !!}</span></a>
									</div>
									<div class="mdl-cell--6-col" style="width:50%;text-align:right">
										<a href="/shoppingcart?products={{$course->product}}"><span class="span_pad2 blue"><i class="fa fa-shopping-cart"></i> {!! trans('dashboard.link2') !!}</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endif
				@endif
				@endforeach
			</div>
		</div>
	</div>
</div>


	<!--------------- Modal Forms-------------->
	<div id="panel_modal" class="panel_popup modal fade" role="dialog" style="margin-top: 100px;display:none">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="border: none;">
					<button class="close" data-dismiss="modal">X</button><br>
					<center><h2 id="modal-course-title" style="font-size:22px"></h2>
					</div>
					<div class="modal-body">
						<center><img id="modal-course-image" src="images/IMPREVO-Learn-English-in-6-months-copy.png" alt="img" class="img img-responsive marT2" style="height:180px;">
							<button id="modal-go-shop" class="btn panel_btn btn-lg marT3" style="width:240px;border-radius:0px;">{!! trans('dashboard.button3') !!}</button><br>
							<button id="modal-go-guide" class="btn panel_btn marT2 btn-lg" style="width:240px;border-radius:0px;">{!! trans('dashboard.button4') !!}</button><br>
							<button id="modal-go-lesson" class="btn panel_btn marT2 btn-lg" style="width:240px;border-radius:0px;">{!! trans('dashboard.button5') !!}</button></center>
						</div>
					</div>
				</div>
			</div>

			<script type="text/javascript">

			function triggerModal(courseId, courseTitle,  courseProduct, courseImage, is_purchased){

				$('#modal-course-title').html(courseTitle);
				$('#modal-course-image').attr('src', courseImage);

				console.log(is_purchased);
				if (is_purchased){
					$('#modal-go-shop').hide();
					$('#modal-go-lesson').hide();
				}
				else {
					$('#modal-go-shop').show();
					$('#modal-go-lesson').show();
				}
				$('#modal-go-shop').click(function(event){
					location.href = '/shoppingcart?products='+courseProduct;
				});
				$('#modal-go-guide').click(function(event){
					location.href = '/course/'+courseId+'?tab=1';
				});
				$('#modal-go-lesson').click(function(event){
					location.href = '/course/'+courseId+'?tab=2';
				});
			}

			function goshop(e, product_id){
				e.preventDefault();
				e.stopPropagation();
				console.log('aaa');
				location.href='/shoppingcart?products='+product_id;
			}

			function goCourse(course_id){
				location.href='/course/'+course_id;
			}

			function Accept()
			{
				var formData = $("#form").serialize();
				$.ajax({
					url:'/home',
					type: 'POST',
					data: formData,
					async: true,
					success: function (ret) {
						console.log(ret);
						document.getElementById('cookie-div').style.display = 'none';
					},
					error:  function(ret)
					{
						console.log(ret);

					}
				});
			}
		</script>

		@endsection
