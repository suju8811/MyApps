@extends('layouts.app')

@section('content')
<main class="mdl-layout__content" style="width:100%;padding-bottom:140px;">
    <div style="background: #9fc3ae;padding:10px 40px;font-size: 16px;height:40px;">
        <span><a href="/" style="color:#d5e7dc;line-height: 40px;text-decoration: none">Imprevo</a></span>
        <span style="color:#d5e7dc;line-height: 40px">/</span>
        <span style="color:#fff;line-height: 40px">Freebies</span>
    </div>
    <div class="mdl-grid portfolio-max-width">

        <div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--4dp">
            <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">About</h2>
            </div>
            <div class="mdl-card__media">
                <img class="article-image" src=" images/about-header.jpg" border="0" alt="">
            </div>

            <div class="mdl-grid portfolio-copy">
                <h3 class="mdl-cell mdl-cell--12-col mdl-typography--headline">Introduction</h3>
                <div class="mdl-cell mdl-cell--8-col mdl-card__supporting-text no-padding ">
                    <p>
                        Excepteur reprehenderit sint exercitation ipsum consequat qui sit id velit elit. Velit anim eiusmod labore sit amet. Voluptate voluptate irure occaecat deserunt incididunt esse in. Sunt velit aliquip sunt elit ex nulla reprehenderit qui ut eiusmod ipsum do. Duis veniam reprehenderit laborum occaecat id proident nulla veniam. Duis enim deserunt voluptate aute veniam sint pariatur exercitation. Irure mollit est sit labore est deserunt pariatur duis aute laboris cupidatat. Consectetur consequat esse est sit veniam adipisicing ipsum enim irure.
                    </p>
                </div>




                <h3 class="mdl-cell mdl-cell--12-col mdl-typography--headline">Irure mollit est sit labore</h3>

                <div class="mdl-cell mdl-cell--8-col mdl-card__supporting-text no-padding ">
                    <p>
                        Excepteur reprehenderit sint exercitation ipsum consequat qui sit id velit elit. Velit anim eiusmod labore sit amet. Voluptate voluptate irure occaecat deserunt incididunt esse in. Sunt velit aliquip sunt elit ex nulla reprehenderit qui ut eiusmod ipsum do. Duis veniam reprehenderit laborum occaecat id proident nulla veniam. Duis enim deserunt voluptate aute veniam sint pariatur exercitation. Irure mollit est sit labore est deserunt pariatur duis aute laboris cupidatat. Consectetur consequat esse est sit veniam adipisicing ipsum enim irure.
                    </p>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
