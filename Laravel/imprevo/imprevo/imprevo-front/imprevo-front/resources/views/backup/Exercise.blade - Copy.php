@extends('layouts.app')

@section('content')

<style>
#Reulst-div-content::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	border-radius: 10px;
	background-color: #F5F5F5;
}

#Reulst-div-content::-webkit-scrollbar
{
	width: 12px;
	background-color: #F5F5F5;
}

#Reulst-div-content::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	background-color: #399aed;
}

.answer_word {	
	outline:none;
	border: none;
	border-bottom: 2px solid #f29f00;
	text-align:center;
    min-width: 150px;
    max-width: 700px;	
}

.accordion {
  width: 380px;
  border-radius: 5px;
  overflow: hidden;
  margin: auto;
}

.accordion .item .heading {
  height: 50px;
  line-height: 50px;
  font-size: 17px;
  cursor: pointer;
  color: #fff;
  padding-left: 15px;
  background: #006e73 url('/images/accordion_arrow.png') no-repeat;
  background-position: right 20px top -95px;
  border-bottom: 1px solid #ec8484;
  box-sizing: border-box;
}

.accordion .item.open .heading,
.accordion .item:last-child .heading { border: 0; }

.accordion .item.open .heading { background-position: right 20px top -5px; }

.accordion .item .content {
  display: none;
  padding: 15px;
  background: #fff;
  font-size: 14px;
}

</style>

<main class="mdl-layout__content" style="width:100%; padding-bottom:140px;background: #ebf3ef">
    <div  class="mdl-layout" style="z-index: 0;background: #9fc3ae;padding:10px 0px;font-size: 14px; overflow:hidden;height:135px">
        <div class="mdl-layout-col" style="padding:5px 0px 10px 80px;">
            <span><a href="/" style="color:#d5e7dc;line-height: 20px;text-decoration: none">Imprevo&nbsp</a></span>
            <span style="color:#d5e7dc;line-height: 20px">/&nbsp</span>
            <span style="color:#d5e7dc;line-height: 20px">{{$lesson->course->title}}&nbsp</span>
            <span style="color:#d5e7dc;line-height: 20px">/&nbsp</span>
            <span style="color:#ffffff;line-height: 20px">{{$exercise->title}}</span>
        </div>
        <div class="mdl-layout-row" style="padding:0px 0px;border:1px solid #00ff00;">
			<div class="mdl-grid mdl-grid--no-spacing-cell mdl-cell--12-col" style="padding:0px; margin:0px;">			
				<div class="mdl-cell mdl-grid--no-spacing-cell mdl-cell--1-col" style="">
				</div>
				<div class="mdl-cell mdl-grid--no-spacing-cell mdl-cell--8-col" style="margin:-100px 0px 0px 4.3%">
				</div>
				<div class="mdl-cell mdl-grid--no-spacing-cell mdl-cell--2-col" style="border:1px solid #ff0000;margin:0px 0px 0px 0px;">
					
				</div>
            </div>
            
        </div>
        <div class="mdl-layout-row" style="padding:0px 0px;">
            <hr style="color:#d5e7dc; height:1px; float:right; background:#d5e7dc;width:23%">
        </div>
        <div class="mdl-layout-row" style="padding:0px 0px; ">
            <span style="float:right; font-size: 12px; width:20%;  padding:0px 0px 0px 0px">
                <a href="#" onclick="OnPreviousLesson(event)" style="float:left;color:#fff;text-decoration: none;">&lt  PREVIOUS</a>
                <a href="#" onclick="OnNextLesson(event)" style="float:right;color:#fff;text-decoration: none;margin: 0px 10% ">NEXT  &gt</a>
            </span>
        </div>
    </div>

    <div  style="padding:0px;background: #ebf3ef; border:1px;">
        <form id="exercise_form" class="form-horizontal" role="form">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$lesson->id}}">
        </form>
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
        <div class="mdl-grid mdl-grid--no-spacing-cell mdl-cell--12-col" style="padding:0px; margin:0px;background-color: #ebf3ef">
            <div class="mdl-cell mdl-grid--no-spacing-cell mdl-cell--1-col" style="">
            </div>
            <div class="mdl-cell mdl-grid--no-spacing-cell mdl-cell--8-col" style="margin:-100px 0px 0px 4.3%">
				<div class="mdl-card mdl-cell mdl-grid--no-spacing-cell mdl-cell--12-col mdl-shadow--4dp" style="overflow: visible; background-color: #ffffff; ">
                @if ($exercise->type == 'video')
                    <div id="video-div" class="mdl-cell mdl-cell--12-col" style="float:right;padding:0px 0px 10px 0px">
                        <div id="exercise-dialog" style="text-align: center;">
                            <div style="padding:20px 0px 20px 0px">
                                <p id="dialogtitle" name="dialogtitle" style="font-weight:bold;font-size: 20px">{{$exercise->description}}</p>
                            </div>

                            <div  id="video-stream" align="center" style="min-height:30%;padding:0px 40px 0px 40px"; >
                                <iframe id="video-url" height=400 src="" style="width:800;display:block;margin: 0px 0px 0px 0px">
                                </iframe>
                            </div>
                        </div>
                    </div>
                @endif
                @if ($exercise->type == 'text')
                    <div  id="text-div" style="margin: 30px 0px 0px 0px; padding:0px">
                        <div style="padding:10px 0px 0px 0px; text-align: center">
                            <p id="dialogtitle" name="dialogtitle" style="font-weight:bold;font-size: 20px">{{$exercise->description}}</p>
                        </div>
						@if ($exercise->audio)
						<!--	<div id="textaudio-div" class="mdl-cell mdl-cell--12-col mdl-cell--3-col-tablet" style="display:inline; text-align:center; padding:20px 100px 0px 100px">
								<audio controls preload="none" autoplay style="width:85%;padding:0px 0px 10px 0px; height: 35px">
									<source src="{{$exercise->audio}}" type="audio/mp3">
									<source src="{{$exercise->audio}}" type="audio/wav">
								</audio>
							</div>-->
							<div id="textaudio-div" class="mdl-cell mdl-cell--12-col mdl-cell--3-col-tablet" style="display:inline; justify-content:center; text-align:center; padding:20px 0px 0px 0px">																
								<div class="dsap" data-audio="{{$exercise->audio}}" data-thumbnail="/images/audioplayer.png" data-autoplay="false" style="position:relative; left:43%;"></div>								
							</div>
						@endif
                        <div style="overflow-y: hidden; margin: 0px 100px 0px 100px; min-height: 30%; margin-top:30px;line-height: 200%">
                        <span id="text-div-content">
							<?php echo html_entity_decode($exercise->content1)?>
                        </span>
                        </div>
                    </div>
                @endif
                @if ($exercise->type == 'translation')
                    <div  id="audio-div" style="margin: 30px 0px 0px 0px; padding:0px;">
                        <div style="padding:10px 0px 0px 0px; text-align: center">
                            <p id="dialogtitle" name="dialogtitle" style="font-weight:bold;font-size: 20px">{{$exercise->description}}</p>
                        </div>
                        <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="justify-content:center;margin:0px 0px 0px 0px;padding: 0px 0px 0px 0px">
                             <div class="mdl-cell mdl-cell--10-col mdl-cell--3-col-tablet">
                                <!-- <audio controls preload="none" autoplay style="width:100%;padding:0px 0px 10px 0px; height: 35px">
									<source src="{{$exercise->audio}}" type="audio/mp3">
                                    <source src="{{$exercise->audio}}" type="audio/wav">
                                 </audio>-->
								@if ($exercise->audio)
									<div class="dsap" data-audio="{{$exercise->audio}}" data-thumbnail="/images/audioplayer.png" data-autoplay="false" style="position:relative; left:43%;"></div>
								@endif
							</div>
                             
                             <div class="mdl-cell mdl-cell--5-col mdl-cell--3-col-tablet " style="min-height:25%;overflow-y: hidden;margin-left:0px; ">
                                    <span id="translation-div-column1">
										<?php echo html_entity_decode($exercise->content1)?>
                                    </span>
                              </div>
                              <div class="mdl-cell mdl-cell--5-col mdl-cell--3-col-tablet " style="min-height:25%;border-left:1px dotted #e5e5e5;overflow-y: hidden;margin-left:0px;padding:0px 0px 0px 0px;">
                                 <span id="translation-div-column2">
										<?php echo html_entity_decode($exercise->content2)?>
                                 </span>
                              </div>
                          </div>
                     </div>
                @endif
                @if ($exercise->type == 'quiz')
                    <div  id="quiz-div" style="padding: 10px 0px 0px 0px; min-height:400px" >
                        <div id="error_title" style="padding:20px 0px 0px 0px; text-align: center;display:none">
                            <p name="question-error" style="font-weight:bold;font-size: 20px">Quiz is Empty.</p>
                        </div>
						<div style="padding:30px 0px 0px 0px; text-align: center">
							<p id="exercise-description" name="dialogtitle" style="font-weight:bold;font-size: 25px">{{$exercise->description}}</p>
						</div>
						<div style="padding:3px 0px 0px 0px; text-align:center;">
							<p id="quiz_title_example" name="quiz_title_example" style="font-size: 16px"></p>
						</div>
						@if ($exercise->audio)
						<div id="quizaudio-div" class="mdl-cell mdl-cell--12-col mdl-cell--3-col-tablet" style="display:inline; text-align:center; padding:20px 100px 0px 100px">
						<!--		<audio controls preload="none" autoplay style="width:85%;padding:0px 0px 10px 0px; height: 35px">
									 <source src="{{$exercise->audio}}" type="audio/mp3">
									 <source src="{{$exercise->audio}}" type="audio/wav">
								</audio>-->					
							<div class="dsap" data-audio="{{$exercise->audio}}" data-thumbnail="/images/audioplayer.png" data-autoplay="false" style="position:relative; left:44%;"></div>															
						</div>
						@endif
                        <div  id="quiz-div-seletImage"  style="display:none;width:100%;margin: 30px 0px 0px 0px; padding:0px">
                            <div style="padding:3px 0px 0px 0px; text-align:center;">
                                <p id="quiz_selectImage_displayword" name="WordDisplay" style="color: #006e73; font-size: 20px; font-weight:700;"></p>
                            </div>
                            <div style="margin:3px 0px 0px 0px; text-align:center;">
                                <p id="quiz_selectImage_displaycorrectword" name="WordDisplay" style="color: #006e73; font-size: 20px; font-weight:700;">&nbsp;</p>
                            </div>
                            <div class="mdl-grid mdl-cell--12-col" style="min-height:280px;justify-content:center;padding:0px 40px 0px 0px">
                                <div class="demo-card-image mdl-card mdl-cell mdl-cell--3-col mdl-cell--3-col-tablet mdl-shadow--4dp" style="position:relative;top: 0;margin-left:50px;border-radius:8px;width: 200px;height: 180px;padding:5px">
                                    <div id="quiz-selectImage-word1-img" onclick="QuizNextClick(this)" style="cursor:pointer;width:190px;height: 170px;">
                                        <div class="mdl-card__title mdl-card--expand"></div>
                                        <div class="mdl-card__actions" style="margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0);">
                                            <input type="hidden" id="quiz-selectImage-word1-text" name="span" class="demo-card-image__filename" style="color: #fff;font-size: 14px;
                                             font-weight: bold "></input>
                                        </div>
                                    </div>
                                    <img id="copyright-img-1" src="/images/copyright.png" width="20" height="20" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="ClickCopyright(this,event)" style="z-index:2;position: relative; left:162px; top:-162px">
                                </div>
                                <div class="demo-card-image mdl-card mdl-cell mdl-cell--3-col mdl-cell--3-col-tablet mdl-shadow--4dp" style="position:relative;top: 0;margin-left:20px;border-radius:8px;width: 200px;height: 180px;padding:5px">
                                    <div id="quiz-selectImage-word2-img" onclick="QuizNextClick(this)" style="cursor:pointer; width: 190px;height: 170px;">										
                                        <div class="mdl-card__title mdl-card--expand"></div>
                                        <div class="mdl-card__actions" style="text-align: center;margin-top:95px;height: 52px; padding: 16px; background: rgba(50, 83, 58, 0);">
                                            <input type="hidden" id="quiz-selectImage-word2-text" name="span" class="demo-card-image__filename" style="color: #fff;font-size: 14px;
                                            font-weight: bold"></input>
                                        </div>
                                    </div>
                                    <img id="copyright-img-2" src="/images/copyright.png" width="20" height="20" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="ClickCopyright(this,event)" style="z-index:2;position: relative; left:162px; top:-162px">
                                </div>								
                            </div>
                            <div style="text-align:center;margin:0px 0px 0px 0px">
                                <button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButton()" style="background-image:url('/upload/image/speaker-icon.png');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">
                                </button>
                            </div>
                        </div>
                        <div  id="quiz-div-singchoice"  style="display:none;width:100%;margin: 30px 0px 0px 0px; padding:0px">
                            <div style="padding:0px 0px 0px 0px; text-align:center;">
                                <p id="quiz-singchoice-SentenceDisplay" name="SentenceDisplay" style="font-size:20px;color:#9fc3ae"></p>
                            </div>
                            <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="min-height:350px;padding:20px 0px 0px 0px; justify-content: center;">								
                                <div id="quiz-singchoice-buttons" class="mdl-cell mdl-cell--8-col" style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px">
                                </div>
								@if ($exercise->content2)							
								<div id="quiz-show-full-text" class="mdl-cell mdl-cell--8-col" style="margin:0px 0px 0px 15px; padding:0px 0px 0px 0px">	
								<div class="accordion" style="width:100%; padding:0; margin:0">
									<div class="item">
										<div class="heading">Show full text</div>
										<div class="content"><?php echo html_entity_decode($exercise->content2)?></div>
									</div>
								</div>	
								</div>
								@endif
                            </div>
                        </div>
						<div  id="quiz-div-typeaudio"  style="display:none;width:100%;margin: 30px 0px 0px 0px; padding:0px">
                            <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="min-height:280px;padding:20px 0px 0px 0px; justify-content: center;">								
								<div class="mdl-cell mdl-cell--12-col" style="text-align:center;padding:0;margin:0px 0px 0px 0px">
									<button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButton()" style="background:url('/images/speaker-icon-type-audio-quiz.png') no-repeat center/cover; background-color:#63a77f; width:120px; height:120px; margin:0px 0px 0px 0px">
									</button>
								</div>
								<div class="mdl-cell mdl-cell--6-col" style="padding:0;text-align:center;margin:0px 0px 0px 0px">	
									<input class="answer_word" type="text" id="input_typeaudio_word" name="input_typeaudio_word" style="width:100%">																
								</div>							
                            </div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
								<div id="quiz-audio-confirmation-correct" class="mdl-cell mdl-cell--5-col" style="display:none;background:url('/images/correct.png') no-repeat center;height:30px">
								</div>
								<div id="quiz-audio-confirmation-incorrect" class="mdl-cell mdl-cell--5-col" style="display:none;background:url('/images/incorrect.png') no-repeat center;height:30px">
								</div>
							</div>							
                        </div>
						<div  id="quiz-div-typeimage"  style="display:none;width:100%;margin: 30px 0px 0px 0px; padding:0px">                          
                            <div class="mdl-grid mdl-cell--12-col" style="min-height:280px;justify-content:center;padding:0px 0px 0px 0px">
                                <div class="demo-card-image mdl-card mdl-cell mdl-cell--3-col mdl-cell--3-col-tablet mdl-shadow--4dp" id="quiz-typeimage-card" style="position:relative;top: 0;margin-left:10px;border-radius:8px;width: 200px;height: 180px;padding:5px">
                                    <div id="quiz-typeImage-word-img" style=" width:190px;height: 170px;">
                                        <div class="mdl-card__title mdl-card--expand"></div>
                                        <div class="mdl-card__actions" style="margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0);">
                                            <input type="hidden" id="quiz-typeImage-word-text" name="span" class="demo-card-image__filename" style="color: #fff;font-size: 14px;
                                             font-weight: bold "></input>
                                        </div>
                                    </div>
                                    <img id="copyright-img" src="/images/copyright.png" width="20" height="20" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="ClickCopyright(this,event)" style="z-index:2;position: relative; left:162px; top:-162px">
                                </div>
								<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="padding:20px 0px 0px 0px; justify-content: center;">								
									<div class="mdl-cell mdl-cell--12-col" style="text-align:center;padding:0;margin:0px 0px 0px 0px">
										<button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButton()" style="background-image:url('/upload/image/speaker-icon.png');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">
										</button>
									</div>
									<div class="mdl-cell mdl-cell--6-col" style="padding:30px 0px 30px 0px;text-align:center;margin:0px 0px 0px 0px">	
										<input class="answer_word" type="text" id="input_typeimage_word" name="input_typeimage_word" style="width:100%">																
									</div>
								</div>
                            </div>
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
								<div id="quiz-image-confirmation-correct" class="mdl-cell mdl-cell--5-col" style="display:none;background:url('/images/correct.png') no-repeat center;height:30px">
								</div>
								<div id="quiz-image-confirmation-incorrect" class="mdl-cell mdl-cell--5-col" style="display:none;background:url('/images/incorrect.png') no-repeat center;height:30px">
								</div>
							</div>							
                        </div>
						<div  id="quiz-div-gapfill"  style="display:none; width:100%;margin: 30px 0px 0px 0px; padding:0px">
                            <div id="gapfill-space-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="padding:20px 0px 0px 0px; justify-content: center;min-height:280px;">								
								<div id="gapfill-div" class="mdl-cell mdl-cell--9-col" style="text-align:center;">								
									
								</div>	
								@if ($exercise->content2)							
								<div id="quiz-show-full-text" class="mdl-cell mdl-cell--8-col" style="margin:0px 0px 0px 15px; padding:0px 0px 0px 0px">	
								<div class="accordion" style="width:100%; padding:0; margin:0">
									<div class="item">
										<div class="heading">Show full text</div>
										<div class="content"><?php echo html_entity_decode($exercise->content2)?></div>
									</div>
								</div>	
								</div>
								@endif									
                            </div>
																											
							<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
								<div id="quiz-gapfill-confirmation-correct" class="mdl-cell mdl-cell--5-col" style="display:none;background:url('/images/correct.png') no-repeat center;height:30px">
								</div>
								<div id="quiz-gapfill-confirmation-incorrect" class="mdl-cell mdl-cell--5-col" style="display:none;background:url('/images/incorrect.png') no-repeat center;height:30px">
								</div>							
							</div>							
                        </div>						
                        <div  id="Result-div" style="display:none;margin: 30px 0px 0px 0px; padding:0px">
                            <div style="padding:10px 0px 0px 0px; text-align: center">
                                <p style="font-weight:bold;font-size: 20px">Congratulations,you've finished the quiz!</p>
                                <p style="font-weight:bold;font-size: 20px">Here are the correct answers</p>
                            </div>
                            <div style="padding:0px 0px 0px 0px; text-align:center;">
                                <p id="result-score" name="result-score" style="font-size:20px;color:#9fc3ae">0</p>
                            </div>
                            <div id="Reulst-div-content" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="min-height:350px;margin:0px 0px 0px 0px;padding: 0px 0px 0px 0px">
                            </div>
                        </div>
                    </div>
                @endif
                    <div  id="lock-div" style="margin: 30px 0px 0px 0px; padding:0px;display:none">
                        <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="justify-content:center;margin:0px 0px 0px 0px;padding: 30px 0px 50px 0px">
                             <div class="mdl-cell mdl-cell--12-col mdl-cell--3-col-tablet" style="text-align:center;">
								<img src = "/images/exericse-lock.png">
							 </div>
                            <div class="mdl-cell mdl-cell--9-col mdl-cell--3-col-tablet" style="text-align:center;padding-top:50px">
								<span style="font-size:25px; font-weight:bold">Sajnáljuk, de ez a lecke csak az IMPREVO tanulói számára érhető el.</span>
							 </div>
                            <div class="mdl-cell mdl-cell--12-col mdl-cell--3-col-tablet" style="text-align:center;padding-top:20px">
								<span style="font-size:25px;">Csatlakozz te is tanulóinkhoz!</span>
							 </div>		
							 <div class="mdl-cell mdl-cell--12-col mdl-cell--3-col-tablet" style="text-align:center;padding-top:20px">
								<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="color:#ffffff;background-color:#006e73" onclick="goToSaleCourse()">
									Tudj meg többet!
								</button>							 
							</div>	
                        </div>
                     </div>			
                <div id="hr-bottom-line" class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="float:bottom;padding:10px 0px 15px 0px">
                    <hr style="color:#eee; height:1px; margin:5px 0px 0px 0px">
                </div>
                <div id="hr-bottom-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="min-height:70px;float:bottom;margin:0px 0px 10px 20px;padding: 10px 0px 0px 0px">
                     <div id="progress" class="mdl-cell mdl-cell--7-col" style="padding:0px 0px 0px 0px;margin:0px 0px 0px 0px">
                         @if ($exercise->type == 'quiz')
							<div id="quiz-result-playagain-btn"  style="min-height:50px;display:none;margin:0px 0px 0px 0px;padding:0px 0px 0px 0px">
								 <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect imprevo-default" style="margin:0px 0px 20px 0px" onclick="playagain()">
										 Play again
								 </button>
							</div>
                            <div id='quiz-progress-div' style="background: #eeeeee;margin-top:10px;width:100%;height: 7px">
                                <div id="quiz-progress" style="background: #399aed;width:0%;height: 7px"></div>
                                <img id="quiz-progress-thumb" src="/images/slider-thumb.png" width="40" height="40" style="position: relative;
                                left: 0%;top: -24px;z-index: 1;margin:0px 0px 0px 0px">
                                <span id="quiz-progress-text", style="width:15px;height:16px;color:#399aed;font-size:10px;position: relative;
                                left: -6%;top:10px;z-index: 2;margin:0px 0px 0px 0px">0%</span>
                            </div>
                         @endif
                     </div>
                     <div class="mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--5-col" style="margin:0px 0px 10px 0px">
                         <div class="mdl-cell mdl-cell--6-col" style="float:right;padding:5px 0px 0px 0px">
                            <a href="#"  data-rel="popup" onclick="popup(event)" style="float:right;pading: 0px 0px 0px 0px;font-weight:bold;font-size: 12px; color:#979797; text-decoration: none">REPORT ERRORS</a>
                         </div>
                         <div class="mdl-cell mdl-cell--6-col" style="margin:0px 0px 0px 0px;padding:0px 0px 0px 0px">
                             <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect imprevo-default" onclick="GotoNext(event,{{$exercise->id}})" style="float:right">
                                NEXT EXERCISE
                             </button>
                         </div>
                     </div>
                </div>
				</div>
            </div>
            <div class="mdl-cell mdl-grid--no-spacing-cell mdl-cell--2-col" style="border:1px solid #ff0000;margin:0px 0px 0px 0px;">
                <nav class="mdl-navigation" style="color: #000000">
                    <ul id="nav-list" name="nav-list" style="float:left;width:100%;list-style-type: none;">
                    </ul>
                </nav>
           </div>
        </div>
    </div>
</main>
<script src="/assets/vendor/jquery/jquery.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
	document.getElementById('Parent-Div-Background').style.backgroundColor = '#ebf3ef';
    var id = '{{$lesson->id}}';

    var exe_id = '{{$exercise->id}}';
    var module_exerciseList = [];
    var curLesson_exerciseList = [];
    var result_status = false;
		var IsProcessing = false;
    @foreach($modules as $module)
				module_exerciseList['{{$module->id}}'] = [];
				@foreach ($module->exercises as $mexe)
					module_exerciseList['{{$module->id}}'].push({"id":"{{$mexe->id}}", "lesson_id":"{{$mexe->lesson_id}}", "title":"{{$mexe->title}}"});
				@endforeach
    @endforeach
    @foreach($modules as $module)
        var m_ex = module_exerciseList['{{$module->id}}'];

        if (m_ex.length > 0)
        {
            var count = 0;
            for (var i = 0; i < m_ex.length; i++)
             {

                 if (m_ex[i].lesson_id == id)
                 {
                   if (count==0)
                   {
                        curLesson_exerciseList['{{$module->id}}']=[];

                        count++;
                    }
                    curLesson_exerciseList['{{$module->id}}'].push(m_ex[i]);
                  }
             }
        }
    @endforeach

    var html='';
    @foreach($modules as $module)
        if (curLesson_exerciseList['{{$module->id}}'])
        {
            html = html + '<li class="nav-parent" style="margin-left: 15px">\
                    <a><span style="font-weight:bold;color: #000000">' + '{{$module->title}}' + '</span></a>\
                    <ul class="nav nav-children" style="margin:0; padding:6px 10px;list-style-type: none; text-decoration: none">';

            for (var i=0; i<curLesson_exerciseList['{{$module->id}}'].length; i++)
            {
                var exes = curLesson_exerciseList['{{$module->id}}'];

                var exe_i = exes[i];

                if (exe_i.id == exe_id)
                {
                    html = html + '<li style="margin:0; padding:8px 0px 0px 0px">\
                 <a href="/exercise/' + exe_i.id + '" style="color: #66b0ee;text-decoration: none">' + exe_i.title + '</a></li>';
                }
                else
                {
                    html = html + '<li style="margin:0; padding:8px 0px 0px 0px">\
                    <a href="/exercise/' + exe_i.id + '" style="color: #000000;text-decoration: none">' + exe_i.title + '</a></li>';
                }
            }
            html = html + '</ul></li>';

        }
    @endforeach

    document.getElementById('nav-list').innerHTML=html;
	console.log("{{$exercise->lesson->is_free}}");
	@if (!$exercise->lesson->is_trial)		
		audio_div = document.getElementById('audio-div');
		if (audio_div) audio_div.style.display = 'none';
		
		text_div = document.getElementById('text-div');
		if (text_div) text_div.style.display = 'none';	
		
		quiz_div = document.getElementById('quiz-div');
		if (quiz_div) quiz_div.style.display = 'none';
		
		video_div = document.getElementById('video-div');
		if (video_div) video_div.style.display = 'none';
		document.getElementById('hr-bottom-div').style.display = 'none';
		document.getElementById('hr-bottom-line').style.display = 'none';		
		document.getElementById('lock-div').style.display = 'inline';	
		
	@else
    var questions = null;
    var questions_random_array = [];
    var questions_result_array=[];
    var question_select_image_step = 0;
    var quiz_question_length = 0;
    var quiz_step = 0;
    var words = [];
	var audio_button_url=null;
    @if ($exercise->type == 'quiz')
		@foreach ($words as $word)
			word = {};
			word.id = "{{$word->id}}".trim();
			word.wordset_index = "{{$word->wordset_index}}".trim();
			word.source_word = "{{$word->source_word}}";
			word.translation = "{{$word->translation}}";
			word.copyright_url = "{{$word->copyright_url}}";
			word.image = "{{$word->image}}";
			word.audio = "{{$word->audio}}";
			word.note = "{{$word->note}}";
			words.push(word);
		@endforeach
        QuizInit();
    @endif
    @if ($exercise->type == 'video')
      var str_vimeo = "https://vimeo.com/";
      var str_update_vimeo = "https://player.vimeo.com/video/";
      var str_youtube = "https://www.youtube.com/watch?v=";
      var str_update_youtube = "https://www.youtube.com/embed/";

      var database_video_url = "{{$exercise->video_url}}";
      var update_url = null;
      if (database_video_url)
      {
        if (database_video_url.substr(0, 8) == "https://")
        {
          if (database_video_url.includes(str_vimeo))
          {
              url_id = database_video_url.slice(str_vimeo.length, database_video_url.length);
              update_url = str_update_vimeo + url_id;
          }
          else if (database_video_url.includes(str_youtube))
          {
              url_id = database_video_url.slice(str_youtube.length, database_video_url.length);
              update_url = str_update_youtube + url_id;
          }
        }
        else
        {
          var isVimeo = /^\d+$/.test(database_video_url);
          url_id = database_video_url;
          if (isVimeo)
          {
              update_url = str_update_vimeo + url_id;
          }
          else {
            update_url = str_update_youtube + url_id;
          }
        }
        document.getElementById("video-url").src = update_url;
      }
    @endif
	@endif
	$('#input_typeaudio_word').on('keyup', function (e) {
		if (e.keyCode == 13) {
        // Do something			
			var str = document.getElementById('input_typeaudio_word');
			if (str == '')
				return;

			QuizNextClick(null);
		}
	});
	
	$('#input_typeimage_word').on('keyup', function (e) {
		if (e.keyCode == 13) {
        // Do something			
			var str = document.getElementById('input_typeimage_word');
			if (str == '')
				return;
			QuizNextClick(null);
		}
	});
	

	
	$('.accordion .item .heading').click(function() {
		
		var a = $(this).closest('.item');
		var b = $(a).hasClass('open');
		var c = $(a).closest('.accordion').find('.open');
			
		if(b != true) {
			$(c).find('.content').slideUp(200);
			$(c).removeClass('open');
		}
		
		$(a).toggleClass('open');
		$(a).find('.content').slideToggle(200);

	});
	
	function goToSaleCourse(){
		course_id = "{{$exercise->lesson->course->id}}"
		location.href = "/sales/course/" + course_id;
	}			
	
	function onKeyUp_gapfill(event, tag_el)
	{			
		
		inputWidth = $('#'+tag_el.id).val().length * 8;
		$('#'+tag_el.id).css({
			width: inputWidth
		})
		
		if (event.keyCode == 13)
		{
			var input_elements = document.getElementsByName("input_gapfill");
			for (i in input_elements)
			{
				if (input_elements[i].value == ''){
					input_elements[i].focus();
					return;
				}				
			}
			QuizNextClick(null);
		}		
	}
	
	
    function popup(event) {
        event.preventDefault();
        event.stopPropagation();
        w2popup.open({
            titlebar: 'no',
            body: '<div class="w2ui-centered" style="padding:0px; margin:0px; height: 600; background-color:#ffffff">\
                      <div class="mdl-grid mdl-grid--no-spacing-cell--12-col" style="width:100%; border-bottom: 2px solid #dddddd">\
                          <div class="mdl-cell mdl-cell--12-col" style="text-align:left;">\
                             <h5>Report Errors</h5>\
                          </div>\
                          <div class="mdl-cell mdl-cell--12-col" style="text-align:left;">\
                             <span style="color:#979797">if you notived anything out of the ordinary, please send us a feedback.</span>\
                          </div>\
                      </div>\
                      <form id="form" role="form" class="form-horizontal form-bordered" action="{{ Config::get("RELATIVE_URL") }}/reporterror" method="post" encType="multipart/form-data">\
                      <div class="mdl-grid mdl-grid--no-spacing-cell--12-col" style="width:100%">\
                          <div class="mdl-cell mdl-cell--12-col" style="text-align:left;">\
                            <span style="color:#979797">lesson, exercise</span>\
                          </div>\
                          <div class="mdl-cell mdl-cell--12-col" style="height:30px;text-align:left;padding:0px, 0px, 10px, 0px; border-bottom:2px dotted #979797">\
                            <span style="font-size:15px;color:#979797">{{$lesson->title}}, {{$exercise->title}}</span>\
                          </div>\
                          <div class="mdl-cell mdl-cell--12-col" style="text-align:left;">\
                            <span style="color:#979797">Your message</span>\
                          </div>\
                          <div class="mdl-cell mdl-cell--12-col" style="">\
                              <textarea type="text" class="form-control" id="secondColumn" style="width:100%;height:200px;border: none;border-bottom:1px solid #dddddd"></textarea>\
                          </div>\
                      </div>\
                      <div class="mdl-grid mdl-grid--no-spacing-cell--12-col" style="margin:20px;text-align:left;">\
                         <div class="mdl-cell mdl-cell--4-col" style="text-align:left;">\
                         <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" type="submit" onclick="Send(event)" style="backgroundColor:#399aed;width:120px">\
                            Send\
                         </button>\
                         </div>\
                         <div class="mdl-cell mdl-cell--8-col" style="padding:9px;text-align:left;">\
                            <a href="#" data-rel="popup" onclick="Close(event)" style="text-decoration:none;font-size:13px;color:#b4b4b4">CANCEL</a>\
                         </div>\
                      </div>\
                      </form>\
                  </div>',
            width:  550,
            height: 600,
            color: '#000', // background-color for the screen lock div
            modal: true,
            style : ''  // additional styles
        });

    }

    function Close(event)
    {
      event.preventDefault();
      event.stopPropagation();
      w2popup.close();
    }

    function Send(event)
    {
      event.preventDefault();
      event.stopPropagation();
      w2popup.close();
      w2popup.open({
          titlebar: 'no',
          body:  '<div class="w2ui-centered" style="padding:0px; margin:0px; height: 600; background-color:#ffffff">\
                    <div class="mdl-grid mdl-grid--no-spacing-cell--12-col" style="width:100%; border-bottom: 2px solid #dddddd">\
                      <div class="mdl-cell mdl-cell--12-col" style="text-align:left;">\
                        <h5>Thank you for your feedback!</h5>\
                      </div>\
                    </div>\
                    <div class="mdl-grid mdl-grid--no-spacing-cell--12-col" style="margin:20px;text-align:left;">\
                       <div class="mdl-cell mdl-cell--4-col" style="text-align:left;">\
                          <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" onclick="w2popup.close()" style="backgroundColor:#399aed;width:120px">\
                              Close\
                          </button>\
                       </div>\
                    </div>\
                  </div>',
          width:  550,
          height: 250,
          color: '#000', // background-color for the screen lock div
          modal: true,
      });
    }
    function QuizInit()
    {
        quiz_step = 0;
        quiz_question_length = 0;
        questions_random_array = [];
        quiz_question_length = 0;
        questions_result_array=[]
        questions = null;
		question_settings = null;
        result_status = false;
        document.getElementById('Result-div').style.display = 'none';
        document.getElementById('quiz-div-seletImage').style.display = 'none';
        document.getElementById('quiz-div-singchoice').style.display = 'none';
		document.getElementById('quiz-div-typeaudio').style.display = 'none';
		document.getElementById('quiz-div-gapfill').style.display = 'none';
		document.getElementById('quiz-div-typeimage').style.display = 'none';
        document.getElementById('error_title').style.display = 'none';

        @if ($quiz)
			
			questions = [];
			@foreach ($quiz->questions as $question)
			{
				question = {};
				question.id = "{{$question->id}}".trim();
				question.instruction = <?php echo json_encode($question->instruction)?>;
				question.note = <?php echo json_encode($question->note)?>;;
				question.type = "{{$question->type}}";
				question.answer_data = <?php echo json_encode($question->answer_data)?>;
				question.quiz_id = "{{$question->quiz_id}}";
				question.correct_answer_id = <?php echo json_encode($question->correct_answer_id)?>;//"{{$question->correct_answer_id}}";
				questions.push(question);
			}			
			@endforeach
			
			quizsettings = [];
			@foreach ($quizsettings as $quizsetting)
			{
				quizsetting = {};				
				quizsetting.term = <?php echo json_encode($quizsetting->term)?>.trim().split('|');										
				quizsettings.push(quizsetting);
			}			
			@endforeach			
            if (questions.length == 0)
            {
                document.getElementById('error_title').style.display = 'inline';
                document.getElementById('quiz-progress').style.width = 0;
                return;
            }

        @endif
		
        for (var i = 0; i< questions.length; i++)
        {
            if (questions[i].type == 1)
            {
                questions_random_array.push(i);
            }
            else if (questions[i].type == 2)
            {

                Answer_data = questions[i].answer_data;
                ArrayAnswer = Answer_data.split(',');
                RandomArrayAnswer = Answer_data.split(',');
                var b = false;
                while(!b){
                    shuffle(RandomArrayAnswer);
                    b = true;
                    for (j in ArrayAnswer)
                    {
                        if (ArrayAnswer[j] == RandomArrayAnswer[j])
                        {
                            b = false;
                        }
                    }
                }

                for (answer in ArrayAnswer)
                {
                    questions_random_array.push([i, ArrayAnswer[answer].trim(), RandomArrayAnswer[answer].trim()]);
                }
            }
			else if (questions[i].type == 3)
            {

                Answer_data = questions[i].answer_data;
                ArrayAnswer = Answer_data.split(',');

                for (answer in ArrayAnswer)
                {
                    questions_random_array.push([i, ArrayAnswer[answer].trim()]);
                }
            }
			else if (questions[i].type == 4)
            {

                Answer_data = questions[i].answer_data;
                ArrayAnswer = Answer_data.split(',');

                for (answer in ArrayAnswer)
                {
                    questions_random_array.push([i, ArrayAnswer[answer].trim(), 0, 0]);
                }
            }
			else if (questions[i].type == 5)
            {
                var instruction = questions[i].instruction;
				var ArrayAnswer = [];
				var instruction_array = instruction.split("\{\{\{");
				var instruction_elements = [];
				
				for (var l in instruction_array)
				{					
					var element = instruction_array[l];
					element_split = element.split("\}\}\}");
					if (element_split.length == 2)
					{						
						ArrayAnswer.push(element_split[0]);
						instruction_elements.push('input');
						instruction_elements.push(element_split[1]);
					}
					else
					{
						instruction_elements.push(element_split[0]);
					}										
				}								
				questions_random_array.push([i, instruction_elements, ArrayAnswer, quizsettings, 0]);
            }		
			
        }
		
        quiz_question_length = questions_random_array.length;
		@if ($quiz)
			@if ($exercise->audio)
				document.getElementById('quizaudio-div').style.display = 'inline';
			@endif
			@if($quiz->random_type > 1)
			 shuffle(questions_random_array);
			@endif
		@endif

        QuizStep(null);
    }
	
	function removeSpecialChars(str) {
		return str.replace(/(?!\w|\s)./g, '')
				  .replace(/\s+/g, ' ')
				  .replace(/^(\s*)([\W\w]*)(\b\s*$)/g, '$2');
	}
	
	function RemovePunctuations(str)
	{
		//console.log('------------------');
		//console.log(str);
		str = str.replace(/[!&\/\\#,+()$~%.":*?<>{}]/g, '');
		//console.log(str);
		return str;
	}
    var status = 0;
    var quiz_parent_top = 0;
    var quiz_obj_top = 0;
	var bselect_image = true;
    function QuizNextClick(obj)
    {
	  if (IsProcessing) return;
	  IsProcessing = true;
      var question_info;
      var step = 0;

      if (quiz_step == 0)
      {
          step = quiz_question_length-1;
      }
      else
      {
          step = quiz_step-1;
      }

      if (quiz_step > 0 || (quiz_step==0 && result_status))
      {
          if (quiz_step == 0)
          {
              question_info = questions_random_array[step];
          }
          else
          {
              question_info = questions_random_array[step];
          }
          var youranswer = '';
		  
          if (question_info.length == 3) {
			  
              var word = null;
              for (i in words) {
                  if (words[i].wordset_index == question_info[1]) {
                      word = words[i];
                  }
              }
              childs = obj.children;

              textSpan = childs[1].children;
			 // console.log(textSpan);
              youranswer = textSpan[0].value;
			  
			//  console.log(youranswer);
              if (word.translation == youranswer)
              {
                if (status == 0 || bselect_image==false)
				{
                  questions_result_array.push([1,youranswer]);				  
				}
                status = 0;
              }
              else
              {
                if (status == 0 || bselect_image==false)
				{
                  questions_result_array.push([0,youranswer]);				  
				}
                status = 1;
              }
			  bselect_image = true;
          }
		  else if (question_info.length == 4) {
			  bselect_image = false;
              var word = null;
              for (i in words) {
                  if (words[i].wordset_index == question_info[1]) {
                      word = words[i];
                  }
              }
			  youranswer = document.getElementById('input_typeimage_word').value;			  
              if (word.source_word.toLowerCase() == youranswer.toLowerCase())
              {
                //if (status == 0)
                  questions_result_array.push([1,youranswer]);				  
				  status = 0;
              }
              else
              {
                //if (status == 0)
                  questions_result_array.push([0,youranswer]);				  
				  status = 1;
              }				  
		  }
		  else if (question_info.length == 5) {
			    bselect_image = false;
			    var input_elements = document.getElementsByName("input_gapfill");
				var answer_data_array = question_info[2];
				var youranswers = [];
				status = 1;
				var terms = question_info[3];
				for (i in input_elements)
				{		
					if (!input_elements[i].value) continue;				
					youranswers.push(input_elements[i].value);
					var element = RemovePunctuations(answer_data_array[i]).trim().toLowerCase();						
					var elements = element.split('|');
					var status_element = false;
					var terms = question_info[3];
					for (j in elements)
					{					
						if (status_element == true) break;
						for (p in terms)
						{												
							term = terms[p].term;											
							var term_current = null;
							if (status_element == true) break;
							for (h in term)
							{														
								if (elements[j].includes(term[h].toLowerCase()))
								{
									include_str = term[h].toLowerCase();
									term_current = term;
									if (status_element == true) break;
									for (p in term_current)
									{
										if (status_element == true) break;
										var replace_answer = elements[j].replace(include_str, term_current[p].toLowerCase());
										if (replace_answer == RemovePunctuations(input_elements[i].value.toLowerCase()).trim())
										{
											status_element = true;											
											break;
										}
									}								
									break;
								}
							}
						}
						
						if (elements[j] == RemovePunctuations(input_elements[i].value.toLowerCase()).trim())
						{
							status_element = true;
							break;
						}	
							
					}
					if (status_element == false){												
						status = 0;						
					}					
				}
				
				if (status == 1)
				{
					questions_result_array.push([1,youranswers]);						
				}	
				else
				{	
					questions_result_array.push([0,youranswers]);										
				}	
							 
		  }
		  else if (question_info.length == 2) {
			  bselect_image = false;
              var word = null;
              for (i in words) {
                  if (words[i].wordset_index == question_info[1]) {
                      word = words[i];
                  }
              }
			  youranswer = document.getElementById('input_typeaudio_word').value;
			  if (word.source_word.toLowerCase() == youranswer.toLowerCase())
              {
                //if (status == 0)
				  
                  questions_result_array.push([1,youranswer]);				  
                status = 0;
              }
              else
              {
                //if (status == 0)				  
                  questions_result_array.push([0,youranswer]);				  
                status = 1;
              }			  
		  }
          else
          {
              var question = questions[question_info];
              youranswer = obj.innerText;
			  
              if (youranswer.toLowerCase() == question.correct_answer_id.toLowerCase())
              {
                  if (status == 0)
                    questions_result_array.push([1,youranswer]);				
                  status = 0;
              }
              else
              {
                  if (status == 0)
                    questions_result_array.push([0,youranswer]);
                  status = 1;
              }
          }
      }



        if (question_info.length == 3) {
		  parent = obj.parentNode; 		
          if (status == 0)
          {
              parent.style.backgroundColor = '#5c9f77';
              document.getElementById('quiz_selectImage_displaycorrectword').innerHTML = youranswer;
          }
          else
          {
              parent.style.backgroundColor = '#ff0000'

              setTimeout(function(){BouncingEffect(parent, '-20px');}, 100);
              setTimeout(function(){BouncingEffect(parent, '20px');}, 200);
              setTimeout(function(){BouncingEffect(parent, '-15px');}, 300);
              setTimeout(function(){BouncingEffect(parent, '15px');}, 400);
              setTimeout(function(){BouncingEffect(parent, '-10px');}, 500);
              setTimeout(function(){BouncingEffect(parent, '10px');}, 600);
              setTimeout(function(){BouncingEffect(parent, '-5px');}, 700);
              setTimeout(function(){BouncingEffect(parent, '5px');}, 800);
              setTimeout(function(){BouncingEffect(parent, '0px');}, 900);
              quiz_step--;

          }
        }
		else if (question_info.length == 2)
		{
			if (status == 0)
			{							
				$('#input_typeaudio_word').css('border-bottom', '2px solid #008000');
				document.getElementById('quiz-audio-confirmation-correct').style.display = 'inline';				
			}
			else
			{				
				$("#input_typeaudio_word").css('border-bottom', '2px solid #ff0000');			
				document.getElementById('quiz-audio-confirmation-incorrect').style.display = 'inline';				
			}	
			//status = 0
		}
		else if (question_info.length == 4)
		{
			if (status == 0)
			{							
				$("#input_typeimage_word").css("border-bottom", "2px solid #008000");
				document.getElementById('quiz-image-confirmation-correct').style.display = 'inline';				
			}
			else
			{				
				$("#input_typeimage_word").css("border-bottom", "2px solid #ff0000");
				document.getElementById('quiz-image-confirmation-incorrect').style.display = 'inline';
				//quiz_step--;
			}				
		}
		else if (question_info.length == 5)
		{
			var input_elements = document.getElementsByName("input_gapfill");			 		    
			var answer_data_array = question_info[2];
			var terms = question_info[3];
			for (i in input_elements)
			{
				if (!input_elements[i].value) continue;				
				var element = RemovePunctuations(answer_data_array[i]).trim().toLowerCase();				
				var elements = element.split('|');				
				var status_element = false;
				
				for (j in elements)
				{	
					if (status_element) break;
					for (p in terms)
					{												
						term = terms[p].term;											
						var term_current = null;
						if (status_element == true) break;
						for (h in term)
						{						
							if (term_current) break;							
							if (elements[j].includes(term[h].toLowerCase()))
							{
								include_str = term[h].toLowerCase();
								term_current = term;
								if (status_element == true) break;
								for (p in term_current)
								{
									if (status_element == true) break;
									var replace_answer = elements[j].replace(include_str, term_current[p].toLowerCase());
									if (replace_answer == RemovePunctuations(input_elements[i].value.toLowerCase()).trim())
									{
										status_element = true;
										break;
									}
								}								
								break;
							}
						}
					}
					
					if (elements[j] == RemovePunctuations(input_elements[i].value.toLowerCase()).trim())
					{
						status_element = true;
						break;
					}								
				}
				
				if (status_element == false){						
					$('#input_gapfill'+i).css("border-bottom", "2px solid #ff0000");	
					document.getElementById('quiz-gapfill-confirmation-incorrect').style.display = 'inline';						
				}	
				else
				{
					$('#input_gapfill'+i).css("border-bottom", "2px solid #008000");		
					document.getElementById('quiz-gapfill-confirmation-correct').style.display = 'inline';
				}
			}												
		}
        else
        {
          if (status == 0)
          {
              obj.style.backgroundColor = '#5c9f77';
          }
          else
          {
              obj.style.backgroundColor = '#ff0000'
              obj.style.top = obj.style.top - 10;
              quiz_step--;
          }
        }

        setTimeout(function(){QuizStep(obj);}, 1000);
    }

    function BouncingEffect(obj, l)
    {
      if (obj)
      {
            obj.style.top = l;
      }
    }

    function QuizStep(obj)
    {
		IsProcessing = false;

        document.getElementById('Result-div').style.display = 'none';
		
		$("#input_typeaudio_word").css("border-bottom", "2px solid #f29f00");
		$('#input_typeaudio_word').val('');
		
		$("#input_typeimage_word").css("border-bottom", "2px solid #f29f00");
		$('#input_typeimage_word').val('');
		
		document.getElementById('quiz-gapfill-confirmation-incorrect').style.display = 'none';
		document.getElementById('quiz-gapfill-confirmation-correct').style.display = 'none';
		document.getElementById('quiz-audio-confirmation-incorrect').style.display = 'none';
		document.getElementById('quiz-audio-confirmation-correct').style.display = 'none';
		document.getElementById('quiz-image-confirmation-incorrect').style.display = 'none';
		document.getElementById('quiz-image-confirmation-correct').style.display = 'none';		
        if (obj)
        {
          parent = obj.parentNode;
          obj.style.backgroundColor = '#f4f4f4';
          obj.style.top = quiz_obj_top;

          parent.style.backgroundColor = '#ffffff'
          parent.style.top = quiz_parent_top;
          document.getElementById("quiz_selectImage_displaycorrectword").innerHTML = '&nbsp;'
        }
		
		if (quiz_step == -1 && status != 0)
		{
			quiz_step = quiz_question_length-1;
			result_status = false;
		}

        if (!result_status) {

          document.getElementById('quiz-div-seletImage').style.display = 'none';
          document.getElementById('quiz-div-singchoice').style.display = 'none';
		  document.getElementById('quiz-div-typeaudio').style.display = 'none';
		  document.getElementById('quiz-div-typeimage').style.display = 'none';
		  document.getElementById('quiz-div-gapfill').style.display = 'none';
          document.getElementById('quiz-result-playagain-btn').style.display = 'none';
          document.getElementById('quiz-progress-div').style.visibility = 'visible';

            var question_info = questions_random_array[quiz_step];

            var index_num = question_info[0];
			var question = questions[question_info];


            document.getElementById('quiz_title_example').innerHTML = <?php echo json_encode($exercise->content1)?>;
            if (question_info.length == 3) {
                var word = null;
                var random_word = null;
				
                for (i in words) {
                    if (words[i].wordset_index == question_info[1]) {
                        word = words[i];
                    }
                    if (words[i].wordset_index == question_info[2]) {
                        random_word = words[i];
                    }
                }
				if (word.audio)
					playSound(word.audio);
				audio_button_url = word.audio;	
				
                document.getElementById('quiz-div-seletImage').style.display = 'inline';
                document.getElementById('quiz_selectImage_displayword').innerHTML = word.source_word;
				var display_order = 0;
                if (status==0) display_order = Math.random();
                if (display_order < 0.5) {
                    document.getElementById('quiz-selectImage-word1-img').style.background = 'url("' + word.image + '") no-repeat center/190px 170px';
                    document.getElementById('quiz-selectImage-word1-text').value = word.translation;
                    document.getElementById('quiz-selectImage-word2-img').style.background = 'url("' + random_word.image + '") no-repeat center/190px 170px';
                    document.getElementById('quiz-selectImage-word2-text').value = random_word.translation;
                }
                else {
                    document.getElementById('quiz-selectImage-word2-img').style.background = 'url("' + word.image + '") no-repeat center/190px 170px';
                   document.getElementById('quiz-selectImage-word2-text').value = word.translation;
                    document.getElementById('quiz-selectImage-word1-img').style.background = 'url("' + random_word.image + '") no-repeat center/190px 170px';
                   document.getElementById('quiz-selectImage-word1-text').value = random_word.translation;
                }
            }
			else if (question_info.length == 2){
				var word = null;
                var random_word = null;
				
                for (i in words) {
                    if (words[i].wordset_index == question_info[1]) {
                        word = words[i];
                    }
                }
				if (word.audio)
					playSound(word.audio);		

				audio_button_url = word.audio;		
				
				
				document.getElementById('quiz-div-typeaudio').style.display = 'inline';
			}
			else if (question_info.length == 4){
				var word = null;
                var random_word = null;
				
                for (i in words) {
                    if (words[i].wordset_index == question_info[1]) {
                        word = words[i];
                    }
                }
				//if (word.audio)
				//	playSound(word.audio);
				audio_button_url = word.audio;				
				document.getElementById('quiz-div-typeimage').style.display = 'inline';
				document.getElementById('quiz-typeImage-word-img').style.background = 'url("' + word.image + '") no-repeat center/190px 170px';
				document.getElementById('quiz-typeImage-word-text').value = word.translation;     
			}
			else if (question_info.length == 5){				
				var html = '';
				var instruction_elements = question_info[1];
				var k = 0;

				for (i in instruction_elements)
				{
					if (instruction_elements[i] == 'input')
					{						
						html = html +  '<input class="answer_word" type="text" id="input_gapfill' + k + '" name="input_gapfill" onkeyup="onKeyUp_gapfill(event, this)" style="text-align:center"></input>';									
						k++;					
					}
					else
					{
						
						html = html + instruction_elements[i];
					}					
				}
				
				document.getElementById('gapfill-div').innerHTML = html;								
				document.getElementById('quiz-div-gapfill').style.display = 'inline';
				
				var gap0 = document.getElementById('input_gapfill0');
				if (gap0) gap0.focus();
			}			
            else {
                document.getElementById('quiz-div-singchoice').style.display = 'inline';				
                document.getElementById('quiz-singchoice-SentenceDisplay').innerHTML = question.instruction;
                Answer_data = question.answer_data;
                Answerarray = Answer_data.split(',');
				@if($quiz)
				@if ($quiz->random_type > 2)
					shuffle(Answerarray);
				@endif
				@endif	
                html = '';
                for (j in Answerarray) {
                    html = html + '<button class="mdl-button mdl-js-button mdl-button--raised" onclick="QuizNextClick(this)"\
                                style="font-size:18px;text-transform: none;text-align:left;margin:9px;width:100%">' + Answerarray[j] + '</button>';

                }
                document.getElementById('quiz-singchoice-buttons').innerHTML = html;
            }
			
            var percent = parseInt(quiz_step * 100 / (quiz_question_length));
            var text_percent = percent - 4;

            percent = percent + '%';
            document.getElementById('quiz-progress').style.width = percent;
            document.getElementById('quiz-progress-thumb').style.left = percent;

            text_percent = text_percent + '%';
            document.getElementById('quiz-progress-text').style.left = text_percent;
            document.getElementById('quiz-progress-text').innerHTML = percent;
			quiz_step++;			
			
            if (quiz_step >= quiz_question_length)
            {
				quiz_step = 0;
                result_status = true;
            }
            else
                result_status = false;
        }
        else
        {
            document.getElementById('quiz-div-seletImage').style.display = 'none';
            document.getElementById('quiz-div-singchoice').style.display = 'none';
			document.getElementById('quiz-div-typeaudio').style.display = 'none';
			document.getElementById('quiz-div-typeimage').style.display = 'none';
			document.getElementById('quiz-div-gapfill').style.display = 'none';
            document.getElementById('quiz-progress-div').style.visibility = 'hidden';
            document.getElementById('quiz-result-playagain-btn').style.display = 'inline';
			document.getElementById('Result-div').style.display = 'inline';
			@if ($exercise->audio)
				document.getElementById('quizaudio-div').style.display = 'none';
			@endif
            html='';
            var score = 0;
		
            for (var k=0; k<quiz_question_length; k++)
            {									
                if (questions_result_array[k][0] == 1) score++;
				
                var question_info = questions_random_array[k];
                if (question_info.length == 3) {
                    var word = null;
                    for (p in words) {
                        if (words[p].wordset_index == question_info[1]) {
                            word = words[p];
                        }
                    }

                    if (word.note == null)
                        note = '';
                    else
                        note = word.note;
					word_audio_url = word.audio;

                    html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                        <div class="demo-card-image mdl-card mdl-cell mdl-cell--2-col mdl-shadow--4dp" style="margin-left:20px;border-radius:8px;width: 200px;height: 180px;padding:5px">\
                        <div style="width: 190px;height: 170px; background: url(' + word.image + ') no-repeat center / 190px 170px;">\
                        <div class="mdl-card__title mdl-card--expand"></div>\
                        <div class="mdl-card__actions" style="visibility:hidden;margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0);">\
                        <span id="quiz-result-word1-text" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                    		font-weight: bold ">' + word.translation + '</span>\
                    		</div>\
                    		</div>\
                    		<img src="/images/copyright.png" width="20" height="20" style="z-index:1;position: relative; left:162px; top:-162px">\
                        </div>\
                        <div class="mdl-grid--no-spacing mdl-grid--no-spacing-cell mdl-cell--8-col mdl-cell--4-col-tablet" style="margin-left:30px;">\
                        <div class="mdl-grid mdl-grid--no-spacing-cell mdl-cell--12-col" style="">\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--8-col" style="">\
                        <div style="padding:10px 0px 0px 0px;">\
                        <p id="source_word" style="color:#9fc3ae; font-size: 16px">'+word.source_word+'</p>\
                        </div>\
                        </div>\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--4-col" style="">\
                        <div style="float:right;margin:0px 0px 0px 0px">\
                        <button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButtionResultDiv(' + word.id + ')" style="background-image:url(' + "/upload/image/speaker-icon.png" + ');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
                        </button>\
                        </div>\
                        </div>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <hr>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <p style="font-weight: bold; margin-bottom:10px">Note!</p>\
                    		<span>'+note+'</span>\
                    		</div>\
                    		</div>\
                    		</div>'
                }
				else if (question_info.length == 2) {
					var word = null;
                    for (p in words) {
                        if (words[p].wordset_index == question_info[1]) {
                            word = words[p];
                        }
                    }
					if (word.note == null)
                        note = '';
                    else
                        note = word.note;
										
                    html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                        <div class="demo-card-image mdl-card mdl-cell mdl-cell--2-col mdl-shadow--4dp" style="margin-left:20px;border-radius:8px;width: 200px;height: 180px;padding:5px">\
                        <div style="width: 190px;height: 170px; background: url(' + word.image + ') no-repeat center / 190px 170px;">\
                        <div class="mdl-card__title mdl-card--expand"></div>\
                        <div class="mdl-card__actions" style="visibility:hidden;margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0);">\
                        <span id="quiz-result-word1-text" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                    		font-weight: bold ">' + word.translation + '</span>\
                    		</div>\
                    		</div>\
                    		<img src="/images/copyright.png" width="20" height="20" style="z-index:1;position: relative; left:162px; top:-162px">\
                        </div>\
                        <div class="mdl-grid--no-spacing mdl-grid--no-spacing-cell mdl-cell--8-col mdl-cell--4-col-tablet" style="margin-left:30px;">\
                        <div class="mdl-grid mdl-grid--no-spacing-cell mdl-cell--12-col" style="">\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--8-col" style="">\
                        <div style="padding:10px 0px 0px 0px;">';

					if 	(questions_result_array[k][0] == 1)
					{					
                       html = html + '<p id="answer_result_word" style="color:#9fc3ae; font-size: 16px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">'+ questions_result_array[k][1] + '</span></p>';
					}   
				    else
					{						
						html = html + '<p id="answer_result_word" style="color:#9fc3ae; font-size: 16px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#ff0000">'+ questions_result_array[k][1] + '</span></p>';
					}
					html = html + '<p id="source_word" style="color:#9fc3ae; font-size: 16px">Correct answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + word.source_word + '</span></p>\
                        </div>\
                        </div>\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--4-col" style="">\
                        <div style="float:right;margin:0px 0px 0px 0px">\
                        <button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButtionResultDiv(' + word.id + ')" style="background-image:url(' + "/upload/image/speaker-icon.png" + ');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
                        </button>\
                        </div>\
                        </div>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <hr>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <p style="font-weight: bold; margin-bottom:10px">Note!</p>\
                    		<span>'+note+'</span>\
                    		</div>\
                    		</div>\
                    		</div>'								
				}
				else if (question_info.length == 4) {
					var word = null;
                    for (p in words) {
                        if (words[p].wordset_index == question_info[1]) {
                            word = words[p];
                        }
                    }
					if (word.note == null)
                        note = '';
                    else
                        note = word.note;					
					html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                        <div class="demo-card-image mdl-card mdl-cell mdl-cell--2-col mdl-shadow--4dp" style="margin-left:20px;border-radius:8px;width: 200px;height: 180px;padding:5px">\
                        <div style="width: 190px;height: 170px; background: url(' + word.image + ') no-repeat center / 190px 170px;">\
                        <div class="mdl-card__title mdl-card--expand"></div>\
                        <div class="mdl-card__actions" style="visibility:hidden;margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0);">\
                        <span id="quiz-result-word1-text" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                    		font-weight: bold ">' + word.translation + '</span>\
                    		</div>\
                    		</div>\
                    		<img src="/images/copyright.png" width="20" height="20" style="z-index:1;position: relative; left:162px; top:-162px">\
                        </div>\
                        <div class="mdl-grid--no-spacing mdl-grid--no-spacing-cell mdl-cell--8-col mdl-cell--4-col-tablet" style="margin-left:30px;">\
                        <div class="mdl-grid mdl-grid--no-spacing-cell mdl-cell--12-col" style="">\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--8-col" style="">\
                        <div style="padding:10px 0px 0px 0px;">';

					if 	(questions_result_array[k][0] == 1)
					{					
                       html = html + '<p id="answer_result_word" style="color:#9fc3ae; font-size: 16px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">'+ questions_result_array[k][1] + '</span></p>';
					}   
				    else
					{						
						html = html + '<p id="answer_result_word" style="color:#9fc3ae; font-size: 16px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#ff0000">'+ questions_result_array[k][1] + '</span></p>';
					}
					html = html + '<p id="source_word" style="color:#9fc3ae; font-size: 16px">Correct answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + word.source_word + '</span></p>\
                        </div>\
                        </div>\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--4-col" style="">\
                        <div style="float:right;margin:0px 0px 0px 0px">\
                        <button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButtionResultDiv(' + word.id + ')" style="background-image:url(' + "/upload/image/speaker-icon.png" + ');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
                        </button>\
                        </div>\
                        </div>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <hr>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <p style="font-weight: bold; margin-bottom:10px">Note!</p>\
                    		<span>'+note+'</span>\
                    		</div>\
                    		</div>\
                    		</div>'						
				}	
				else if (question_info.length == 5) {
					
					var question_str = '';
					var answer_str = '';
					var youranswers = questions_result_array[k][1];					
					var answer_data_array = question_info[2];
					var instruction_array = question_info[1];			
					var terms = question_info[3];
					var m = 0;
					for (var i in instruction_array)
					{
						if (instruction_array[i] == 'input')
						{		
							var element = RemovePunctuations(answer_data_array[m]).trim().toLowerCase();
							var elements = element.split('|');	
							var status_element = false;

							for (j in elements)
							{
								if (status_element) break;
								for (p in terms)
								{												
									term = terms[p].term;											
									var term_current = null;
									if (status_element == true) break;
									for (h in term)
									{						
										if (term_current) break;							
										if (elements[j].includes(term[h].toLowerCase()))
										{
											include_str = term[h].toLowerCase();
											term_current = term;
											if (status_element == true) break;
											for (p in term_current)
											{
												if (status_element == true) break;
												var replace_answer = elements[j].replace(include_str, term_current[p].toLowerCase());
												if (replace_answer == RemovePunctuations(youranswers[m].toLowerCase()).trim())
												{
													status_element = true;
													break;
												}
											}								
											break;
										}
									}
								}
								
								if (elements[j] == RemovePunctuations(youranswers[m].toLowerCase()).trim())
								{
									status_element = true;
									break;
								}																	
							}
								
							if (status_element == true)
							{
								answer_str = answer_str + '<span style="padding:0px 10px 0px 10px; border-bottom: 2px solid #008000">' + youranswers[m] + '</span>';
							}
							else
							{
								answer_str = answer_str + '<span style="padding:0px 10px 0px 10px; border-bottom: 2px solid #ff0000">' + youranswers[m] + '</span>';
							}
							question_str = question_str + '<span style="padding:0px 10px 0px 10px; border-bottom: 2px solid #f29f00">' + answer_data_array[m] + '</span>';
							
							m++;
						}
						else
						{
							question_str = question_str + instruction_array[i];
							answer_str = answer_str + instruction_array[i];
						}
					}		
					var question = questions[question_info[0]];					
					var note = '';
					if (question.note) note = question.note;
					html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
								<div class="mdl-cell mdl-cell--12-col" style="">\
								    <p style="font-weight: bold; font-size:20px; margin-bottom:10px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">'+ answer_str  + '</span></p>\
									<p style="font-weight: bold; font-size:20px; margin-bottom:10px">Correct answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question_str + '</span></p>\
									<hr>\
									<p style="font-weight: bold; font-size:20px; margin-bottom:10px">Note:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + note + '</span></p>\
                                </div>\
								</div>'				
				}				
                else
                {					
                    var question = questions[question_info];
					if (questions_result_array[k][0] == 1)		
						html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                            <div class="mdl-cell mdl-cell--12-col" style="">\
                                <span style="font-weight: bold; font-size:20px; margin-bottom:10px">Question:&nbsp<span style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.instruction + '</span></span>\
                                <p style="font-weight: bold; font-size:20px; margin-bottom:10px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">'+ questions_result_array[k][1] + '</span></p>\
                                <p style="font-weight: bold; font-size:20px; margin-bottom:10px">Correct answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.correct_answer_id + '</span></p>\
								<hr>\
								<p style="font-weight: bold; font-size:20px; margin-bottom:10px">Note:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.note + '</span></p>\
                                </div>\
							</div>';
					else
						html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                            <div class="mdl-cell mdl-cell--12-col" style="">\
                                <span style="font-weight: bold; font-size:20px; margin-bottom:10px">Question:&nbsp<span style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.instruction + '</span></span>\
                                <p style="font-weight: bold; font-size:20px; margin-bottom:10px">Your answer:&nbsp<span  style=" font-weight: normal; font-size:20px;color:#ff0000;">'+ questions_result_array[k][1] + '</span></p>\
                                <p style="font-weight: bold; font-size:20px; margin-bottom:10px">Correct answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.correct_answer_id + '</span></p>\
								<hr>\
								<p style="font-weight: bold; font-size:20px; margin-bottom:10px">Note:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.note + '</span></p>\
                                </div>\
							</div>';
						
                }

            }
            document.getElementById('result-score').innerHTML = 'Your Score: ' + score + '/' + quiz_question_length;
            document.getElementById('Reulst-div-content').innerHTML = html;
            var formData = $("form#exercise_form").serialize();

            $.ajax({
                url:'/exercise/' + exe_id,
                type: 'POST',
                data: formData,
                async: true,
                success: function (ret) {
                    console.log(ret);
                },
                error:  function(ret)
                {
                  console.log(ret);
                }
            });
        }
    }

    function shuffle (array) {
        var i = 0
            , j = 0
            , temp = null

        for (i = array.length - 1; i > 0; i -= 1) {
            j = Math.floor(Math.random() * (i + 1))
            temp = array[i]
            array[i] = array[j]
            array[j] = temp
        }
    }

    function GetWord(id)
    {
        for (word in words)
        {
            if (words[word].id == id)
            {
                return words[word];
            }
        }
    }

    function AudioButton()
    {
       //word_source = document.getElementById('quiz_selectImage_displayword').innerHTML;

       /*for (p in words) {
          if (words[p].source_word == word_source) {
              word = words[p];
          }
       }*/
	   
	   if (audio_button_url)
			playSound(audio_button_url);
    }

	function AudioButtionResultDiv(result_word_id)
	{
		for (p in words) {
			 if (words[p].wordset_index == result_word_id) {
					 word = words[p];
			 }
		}
		playSound(word.audio);
	}

    function playSound(url){
        var audio = document.createElement('audio');
        audio.style.display = "none";
        audio.src = url;
        audio.autoplay = true;
        audio.onended = function(){
          audio.remove()
        };
        document.body.appendChild(audio);
     }
    function mouseover(x) {
        x.src = "/images/copyright-hover.png";
    }

    function mouseout(x) {
        x.src = "/images/copyright.png";
    }

    function ClickCopyright(obj, event)
    {
      event.preventDefault();
      event.stopPropagation();
      parent = obj.parentNode;

	  console.log(parent);
      var childNodeArray = parent.childNodes;
      var child1Array = childNodeArray[1].childNodes;
	  console.log(child1Array);
      var child2Array = child1Array[3].childNodes;
	  console.log(child2Array);
      tran_word = child2Array[1].value;
	  console.log(child2Array[1]);
	  console.log(tran_word);
      var word = null;
      for (i in words) {
          if (words[i].translation == tran_word) {              
              word = words[i];
          }
      }
	  console.log(word);
      if (word)
      {
         if (word.copyright_url){

           w2popup.open({
			   title: 'CopyRight Url',
               body: '<div class="w2ui-centered" style="overflow:hidden;padding:0px; margin:0px; height: 400; width:400;backgroundColor:#0000ff">\
							 				  <div style="margin-bottom:30px">\
                        	<span style="font-size:16px; font-weight:bold; color:#399aed;"> Credit </span>\
												</div>\
												<div style="width:300;min-height:100px;font-size:14px;color:#399aed;">\
													<p>' + word.copyright_url + '</p>\
												</div>\
                     </div>',
               width:  400,
               height: 400,
               color: '#000',
							 backgroundColor:'#ffffff',
               modal: false,
               style : '',
               showClose       : true
           });
         }
         else {
            alert("Empty Copyright URL");
         }
      }
    }

    function playagain()
    {
        QuizInit();
    }

    function GotoNext(e,exercise_id) {
        var cur = 0;

        @foreach($modules as $module)
        if (cur != 2)
        {
            if (curLesson_exerciseList['{{$module->id}}']) {
                for (var i = 0; i < curLesson_exerciseList['{{$module->id}}'].length; i++) {
                    var exes = curLesson_exerciseList['{{$module->id}}'];

                var exe_i = exes[i];

                if (cur == 1) {
                    url = location.href;
                    location.href = '/exercise/' + exe_i['id'];
                    cur = 2;
                    break;
                }

                if (exe_i['id'] == exercise_id) {
                    cur = 1;
                }
                }
           }
        }
        @endforeach
    }

    function OnPreviousLesson(event)
    {
      event.preventDefault();
      event.stopPropagation();
      var prev_lesson_id = null;
      var isflag = false;
      @foreach ($lessons as $item)
      if (!isflag)
      {
        if ('{{$item->id}}' == '{{$lesson->id}}')
        {
          isflag = true;
        }
        else
        {
            prev_lesson_id = '{{$item->id}}';
        }
      }
      @endforeach
      if (prev_lesson_id)
      {
        location.href = '/lessonHome/' + prev_lesson_id;
      }

    }

    function OnNextLesson(event)
    {
      event.preventDefault();
      event.stopPropagation();
      var next_lesson_id = null;

      var isflag = 0;
      @foreach ($lessons as $item)
      if (isflag == 1)
      {
        next_lesson_id = '{{$item->id}}';
        isflag = 2;
      }
      if (isflag==0){
        if ('{{$item->id}}' == '{{$lesson->id}}')
        {
          isflag = 1;
        }
      }
      @endforeach
      if (next_lesson_id)
      {
        location.href = '/lessonHome/' + next_lesson_id;
      }
    }
 </script>
@endsection
