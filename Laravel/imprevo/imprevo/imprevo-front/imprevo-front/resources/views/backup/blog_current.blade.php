<!DOCTYPE html>
<html lang="en" style="position: relative;min-height: 100%;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="profile">
    <meta name="keywords" content="profile">
    <title>{{$blog->seo_title}}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    {{--<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue-light_blue.min.css" />--}}
    <link rel="stylesheet" href="/css/material.min.css" />
    <link rel="stylesheet" href="/css/styles.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="{{ Config::get('RELATIVE_URL') }}/assets/vendor/pnotify/pnotify.custom.css" />
    <link rel="stylesheet" type="text/css" href="https://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.css" />
	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.css" />



    <script src="/assets/vendor/modernizr/modernizr.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.js"></script>
    <script src="/assets/vendor/pnotify/pnotify.custom.js"></script>
	
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <link href="/assets/audio-player/css/audioPlayer.css" rel="stylesheet"/>
    <script src="/assets/audio-player/js/audioPlayer.js"></script>


    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <!--Facebook Pixel Code-->
    <?php echo html_entity_decode($blog->facebook_pixel_code)?>
    <!--End Facebook Pixel Code-->
</head>
<style>
#Reulst-div-content::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	border-radius: 10px;
	background-color: #F5F5F5;
}

#Reulst-div-content::-webkit-scrollbar
{
	width: 12px;
	background-color: #F5F5F5;
}

#Reulst-div-content::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	background-color: #399aed;
}

.answer_word {	
	outline:none;
	border: none;
	border-bottom: 2px solid #f29f00;
	text-align:center;
    min-width: 150px;
    max-width: 700px;	
}

.accordion {
  width: 380px;
  border-radius: 5px;
  overflow: hidden;
  margin: auto;
}

.accordion .item .heading {
  height: 50px;
  line-height: 50px;
  font-size: 17px;
  cursor: pointer;
  color: #fff;
  padding-left: 15px;
  background: #006e73 url('/images/accordion_arrow.png') no-repeat;
  background-position: right 20px top -95px;
  border-bottom: 1px solid #ec8484;
  box-sizing: border-box;
}

.accordion .item.open .heading,
.accordion .item:last-child .heading { border: 0; }

.accordion .item.open .heading { background-position: right 20px top -5px; }

.accordion .item .content {
  display: none;
  padding: 15px;
  background: #fff;
  font-size: 14px;
}
</style>
<style>
	#send-button{
		background-color:#f29f00;
		border-color:#f29f00;
		
	}
	
	#send-button:hover{
		background-color:#f29f00;
		border-color:#f29f00;
	}
	
	#sub-button {
		margin-left:30px;
		background-color:#ffffff;
		color:#399aed;
		border:none;
		outline:none;
	}
	
	#sub-button:hover {
		margin-left:30px;
		background-color:#dfe8ef;
		color:#399aed;
	}
	.social-link{
		color: #6c937b;
		margin-right:30px;
		
	}
	.social-link:hover{
		color: #e2ef1a;		
	}
	.link1{
		color:#66abae;
	}
	.link1:hover{
		color:#ffffff;
		text-decoration:none;
	}
	.recent-links{
		color:#000000;
		text-decoration:none;
	}
	
	.recent-links:hover {		
		text-decoration:none;
	}
	.category-links{
		color:#000000;
		text-decoration:none;
	}
	.category-links:hover{
		text-decoration:none;
	}
	#create_name:hover{
		color:#399aed;
	}
</style>
<style>

  .fb-page, .fb-page:before, .fb-page:after {
    border: 1px solid #ccc;
  }

  .fb-page:before, .fb-page:after {
    content: "";
    position: absolute;
    bottom: -3px;
    left: 2px;
    right: 2px;
    height: 1px;
    border-top: none
  }
  
  .fb-page:after {
    left: 4px;
    right: 4px;
    bottom: -5px;
    box-shadow: 0 0 2px #ccc
  }
</style>
<body style="padding:0px; height:100%;margin: 0;overflow-x: hidden">
<!--<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" style="width:100%;min-height:100%">-->
<div id='Parent-Div-Background' style="position:relative;width:100%;min-height:100%;">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <header class="mdl-layout__header mdl-layout__header--transparent" style="margin:0px; padding:0px;background: #006e73;height:100px">
        <div class="mdl-layout__header-row" style="margin-top:15px">
            <!-- Title -->
                <span class="mdl-layout-title">
                    <a href="/"><img src="/images/logo.png"></a>
                </span>
            <!-- Add spacer, to align navigation to the right -->
            <div class="mdl-layout-spacer"></div>
            <!-- Navigation -->
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="/tryit" style="color:#fff;font-size: 18px">Try it</a>
                <a class="mdl-navigation__link" href="/courses" style="color:#fff;font-size: 18px">Course</a>
                <a id="submenu" class="mdl-navigation__link" href="#" style="color:#fff;font-size: 18px">Lessons</a>
                <a class="mdl-navigation__link" href="/freebies" style="color:#fff;font-size: 18px">Freebies</a>
                @if (Auth::guest())
                <button onclick="goLogin()" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="color:#fff;font-size: 18px;background: #f29f00;margin-left:150px">
                    LOGIN
                </button>
                @else
                    <button onclick="goProfile(event)" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="color:#fff;font-size: 18px;background: #f29f00;margin-left:150px">
                        MY ACCOUNT
                    </button>
                @endif
            </nav>
            <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu"  for="submenu">
                @foreach(\App\Level::all() as $level)
                    <a class="mdl-menu__item" href="/lessons/{{$level->id}}">{{$level->title}}</a>
                @endforeach
            </ul>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </header>

    <div class="mdl-layout__drawer mdl-layout--small-screen-only">
        <nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
            <a class="mdl-navigation__link is-active" href="tryit.html">Try it</a>
            <a class="mdl-navigation__link" href="courses.html">Course</a>
            <a class="mdl-navigation__link" href="lessons.html">Lessons</a>
            <a class="mdl-navigation__link" href="freebies.html">Freebies</a>
        </nav>
    </div>

	<main class="mdl-layout__content" style="background-color:#ebf3ef;width:100%;margin:0px;padding:0px;padding-bottom:200px;">
	<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;margin:0px;width:100%;background: #9fc3ae;padding:0px 0px 0px 0px;font-size: 16px;height:250px">
		<div class="mdl-grid mdl-cell mdl-cell--8-col" style="padding:0px;margin:0px">
			<span><a href="/" style="padding-left:25px;color:#d5e7dc;line-height: 40px;text-decoration: none">Imprevo</a></span>
			<span style="padding-left:10px;color:#d5e7dc;line-height: 40px">/</span>
			<span style="padding-left:10px;color:#d5e7dc;line-height: 40px">Blog</span>		
			<span style="padding-left:10px;color:#d5e7dc;line-height: 40px">/</span>
			<span style="padding-left:10px;color:#fff;line-height: 40px"></span>		
		</div>
	</div>
    <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;padding:0px;margin-top:-210px"> 
        <div class="mdl-grid mdl-cell mdl-cell--8-col" style="padding:0px;">
          <div id="main_page_div" class="mdl-cell mdl-cell mdl-cell--9-col" style="background-color:#ffffff;padding:0px;margin:0;margin-left:20px;">

          </div>		  
          <div class="mdl-cell mdl-cell mdl-cell--3-col" style="background-color:#ebf3ef;padding:0;margin:0">		  
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="text-align:center;margin:0px;background:url('/images/newsletter-background.png') no-repeat center/cover;padding:0px 0px 0px 0px;height:202px;width:100%">
				<div class="mdl-cell mdl-cell--12-col" style="padding-top:20px;">
					<span style="font-size:16px; color:#ffffff">Imprevo hírlevél</span>
				</div>							
				<div class="mdl-cell mdl-cell--12-col">
					<span style="font-size:12px; text-align:left; color:#ffffff">Iratkozz fel ingyenes anyagokért és újdonságokért!</span>
				</div>							
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
					<div class="mdl-cell mdl-cell--10-col" style="margin:0; padding:0;">
						<form action="https://sendy.imprevo.info/subscribe" method="POST" accept-charset="utf-8" style="width:100%;margin:0;padding:0">						
							<div class="input-group" style="margin:0; padding:0;">
								<input type="text" id="email" name="email" class="form-control" placeholder="Email cím">
								<input type="hidden" name="list" value="dWVr892XDx8pprHN8763yTtK6w"/>
								<span class="input-group-btn">
									<button id="send-button" class="btn btn-primary" type="submit" style="">
										<i aria-hidden="true" class="fa fa-arrow-right"></i>
									</button>
								</span>
							</div>						
						</form>
					</div>			
				</div>							
			</div>
<!--            @if ($blog['is_image'])
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0 20px 0; justify-content:center">
               <div id="featured_image" style="width: 100%; min-height: 100px; background: url('{{$blog->featured_image}}') center / cover;">
               </div>
            </div>
            @endif-->
            @if ($recent_blogs)
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0px 10px 10px">
              <span style="color:#3b4a51;font-weight:bold; font-size:14px">Hasznos anyagaink</span>
            </div>
                @foreach($recent_blogs as $rblog)
                <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0px; border-bottom:1px solid #eee;padding-left:30px; padding-bottom:10px">
                  <a class="recent-links" href="/blog/{{$rblog}}" style=""><span style="padding-right:10px">></span>{{$rblog}}</a>
                </div>
                @endforeach
            @endif
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0 10px 10px">
              <span style="color:#3b4a51;font-weight:bold; font-size:14px">blog kategóriák</span>
            </div>
            @foreach(\App\Blogcat::all() as $blogcat)
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="border-bottom:1px solid #eee;padding-left:30px; padding-bottom:10px">
              <a class="category-links" href="/blog/category/{{$blogcat->title}}" style=""><span style="padding-right:10px">></span>{{$blogcat->title}} ({{count($blogcat->blogs)}})</a>
            </div>
            @endforeach
			<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0 0px 10px">
				<span style="color:#3b4a51;font-weight:bold; font-size:14px">INGYENES TANFOLYAM</span>
			</div>
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="background:url('/images/banner.png') no-repeat center/cover; height:230px; ">
            </div>
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0 20px 10px">
         <!--     <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="standard" data-action="like" data-show-faces="true" data-share="false"></div>-->
				<div class="fb-page" data-href="https://www.facebook.com/digital.inspiration" data-small-header="false" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
				</div>
            </div>
			
          </div>
        </div>
    </div>
</main>

	<div id="div-footer" style="padding:0px; z-index:2;border:1px solid #eee;position: absolute;left: 0;bottom: 0px;height:200px;width: 100%;background:#006a6f;">
		  <div class="mdl-cell mdl-cell--12-col" style="padding:0px; padding-top:15px;width:100%; margin:0px;text-align:center; background-color:#399aed; height:80px;">
			<span style="font-size:25px; color:#ffffff"> Próbáld ki az IMPREVO-t még ma ingyen 
				<button id="sub-button" class="btn btn-lg btn-primary" style="">Próbáld ki ingyen!</button>
			</span>
		  </div>
		  <div class="mdl-cell mdl-cell--12-col" style="padding:0px;width:100%; margin:0px;text-align:center; background-color:#006a6f; height:80px;">
			<div class="mdl-cell mdl-cell--12-col" style="margin:0px;padding-top:50px; text-align:center;">
				<span style="color:#ffffff; font-size:23px">Értesülj legfrisebb leckéinkről,  ingyenes anyagainkról</span>
			</div>
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%;background-color:#006a6f; margin:0px;padding-top:15px; text-align:center; justify-content:center">
				<div class="mdl-cell mdl-cell--6-col" style="margin:0; padding:0;background-color:#006a6f;">
					<form action="pages-search-results.html" style="width:100%;margin:0;padding:0">						
						<div class="input-group" style="margin:0; padding:0;">
							<input type="text" id="email" name="email" class="form-control" placeholder="Email cím">
							<span class="input-group-btn">
								<button id="send-button" class="btn btn-primary" type="button" style="">FELIRATKOZOM</button>
							</span>
						</div>						
					</form>
				</div>
			</div>
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%;background-color:#006a6f; margin:0px;padding-top:15px; text-align:center; justify-content:center">
				<div class="mdl-grid mdl-cell mdl-cell--7-col" style="color:#ffffff;background-color:#006a6f; margin:0px;padding-top:15px; text-align:center; justify-content:center">
					<div class="mdl-grid mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
							<span>Rólunk</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Kik vagyunk?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Mi az IMPREVO?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Shop</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Kapcsolat</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Online Angol tanfolyam</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Hallás utáni szövegértés leckék</a>
						</div>	
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Angol oktatás cégeknek</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Meg lehet tanulni az IMPREVO-val angolul?</a>
						</div>					
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span>Ingyenes anyagok</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Igeidők összefoglaló táblázat</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">20 részes ingyenes e-mailes tanfolyam</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Ingyenes Kezdő Online Angol</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Zenés leckék</a>
						</div>	
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Nyelvtani anyagok</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Blog</a>
						</div>					
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span>Segítség</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">GYIK</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Syllabus</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Milyen a tanulási stílusod?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">8+1 Tipp angoltanuláshoz</a>
						</div>						
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span>Ebookok</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Angol igeidők összefoglaló és példatár</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Angol levelek és e-mailek írása</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Tanulj meg 6 hónap alatt angolul</a>
						</div>	
						<div class="mdl-cell mdl-cell--12-col" style="padding-top:50px;">
							<span>KÖVESS MINKET</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col" style="padding-top:20px; text-align: center;font-size:25px">
							<a class="social-link" href=""><i class="fa fa-facebook" style=""></i></a>
							<a class="social-link" href=""><i class="fa fa-twitter" style=""></i></a>
							<a class="social-link" href=""><i class="fa fa-google-plus" style=""></i></a>
							<a class="social-link" href="" style="margin-right:0px"><i class="fa fa-youtube-play" ></i></a>
						</div>
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--7-col" style="text-align:left;margin:0; padding:0; padding:0px 20px 10px 20px">
					<hr style="width:100%; height:1px; background-color:#66abae;"></hr>
					<span style="color:#7fbabd">
						<a href="/" style="margin-right:20px">
							<img src="/images/logo.png">					
						</a> 
						Copyright 2016. Imprevo. Minden jog fenntartva.					
					</span>					
					<a class="link1" href="#" style="margin-left:150px;">Adatvédelem</a>
					<a class="link1" href="#" style="margin-left:15px;">Felhasználás feltételei</a>
					<a class="link1" href="#" style="margin-left:15px;">Impresszum</a>					
				</div>				
			</div>
		 </div>
	</div>
</div>
<script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<script src="/assets/vendor/jquery/jquery.js"></script>
<script src="/assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/vendor/jquery-validation/jquery.validate.min.js"></script>

<script src="/assets/vendor/bootstrap/js/bootstrap.js"></script>
<script src="/assets/vendor/nanoscroller/nanoscroller.js"></script>
<!-- Theme Base, Components and Settings -->
<script src="/assets/javascripts/theme.js"></script>

<!-- Theme Custom -->
<script src="/assets/javascripts/theme.custom.js"></script>

<!-- Theme Initialization Files -->
<script src="/assets/javascripts/theme.init.js"></script>
<script>
pagination_html = '<hr class="solid mb-none" />';
pagination_html = pagination_html + '<ul class="pagination" style="float:bottom">';
var page_count = JSON.parse('<?php echo json_encode($page_count)?>');
var page_index = JSON.parse('<?php echo json_encode($page_index)?>');

if (page_count > 1){
for (i=0; i<page_count; i++)
{
  if (i== (page_index-1))
  {
      if (i==0){
        pagination_html = pagination_html + '<li class="active">\
          <a href="/blog/{{$blog->static_url}}" onclick="pageIndex(' + (i+1) + ')">'+ (i+1) + '</a>\
        </li>';
      }
      else {
        pagination_html = pagination_html + '<li class="active">\
          <a href="/blog/{{$blog->static_url}}?page='+(i+1)+'" onclick="pageIndex(' + (i+1) + ')">'+ (i+1) + '</a>\
        </li>';
      }
  }
  else
  {
    if (i==0){
      pagination_html = pagination_html + '<li>\
        <a href="/blog/{{$blog->static_url}}" onclick="pageIndex(' + (i+1) + ')">'+ (i+1) + '</a>\
      </li>';
    }
    else {
      pagination_html = pagination_html + '<li>\
        <a href="/blog/{{$blog->static_url}}?page='+(i+1)+'" onclick="pageIndex(' + (i+1) + ')">'+ (i+1) + '</a>\
      </li>';
    }
  }
}
}
pagination_html = pagination_html +  '</ul>\</div>';
document.getElementById('pagination-div').innerHTML = pagination_html;
var quiz_list = JSON.parse('<?php echo json_encode($quiz_list)?>');
@if ($quiz_list)
  @foreach ($quiz_list as $quiz)
  var html = '<div  id="quiz-div-seletImage-{{$quiz->id}}"  style="display:none;width:100%;margin: 30px 0px 0px 0px; padding:0px">\
                <div style="padding:3px 0px 0px 0px; text-align:center;">\
                    <p id="quiz_selectImage_displayword-{{$quiz->id}}" name="WordDisplay" style="color:#9fc3ae; font-size: 16px"></p>\
                            </div>\
                            <div style="margin:3px 0px 0px 0px; text-align:center;">\
                                <p id="quiz_selectImage_displaycorrectword-{{$quiz->id}}" name="WordDisplay" style="color:#9fc3ae; font-size: 16px">&nbsp;</p>\
                            </div>\
                            <div class="mdl-grid mdl-cell--12-col" style="min-height:280px;justify-content:center;padding:0px 40px 0px 0px">\
                                <div class="demo-card-image mdl-card mdl-cell mdl-cell--3-col mdl-cell--3-col-tablet mdl-shadow--4dp" style="position:relative;top: 0;margin-left:50px;border-radius:8px;width: 200px;height: 180px;padding:5px">\
                                    <div id="quiz-selectImage-word1-img-{{$quiz->id}}" onclick="QuizNextClick(this,{{$quiz->id}})" style=" width:190px;height:170px;">\
                                        <div class="mdl-card__title mdl-card--expand"></div>\
                                        <div class="mdl-card__actions" style="margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0);">\
                                            <input type="hidden" id="quiz-selectImage-word1-text-{{$quiz->id}}" name="span" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                                             font-weight: bold "></input>\
                                        </div>\
										<img id="copyright-img-1" src="/images/copyright.png" width="20" height="20" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="ClickCopyright(this,event)" style="display:inline;z-index:99;position: relative; left:162px; top:-170px">\
                                    </div>\
								</div>\
                                <div class="demo-card-image mdl-card mdl-cell mdl-cell--3-col mdl-cell--3-col-tablet mdl-shadow--4dp" style="position:relative;top: 0;margin-left:20px;border-radius:8px;width: 200px;height: 180px;padding:5px">\
                                    <div id="quiz-selectImage-word2-img-{{$quiz->id}}" onclick="QuizNextClick(this, {{$quiz->id}})" style="width: 190px;height: 170px;">										\
                                        <div class="mdl-card__title mdl-card--expand"></div>\
                                        <div class="mdl-card__actions" style="text-align: center;margin-top:95px;height: 52px; padding: 16px; background: rgba(50, 83, 58, 0);">\
                                            <input type="hidden" id="quiz-selectImage-word2-text-{{$quiz->id}}" name="span" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                                            font-weight: bold"></input>\
                                        </div>\
										<img id="copyright-img-2" src="/images/copyright.png" width="20" height="20" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="ClickCopyright(this,event)" style="z-index:2;position: relative; left:162px; top:-170px">\
									</div>\
								</div>								\
                            </div>\
                            <div style="text-align:center;margin:0px 0px 0px 0px">\
                                <button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButton({{$quiz->id}})" style="background-image:url('+'/upload/image/speaker-icon.png'+');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
                                </button>\
                            </div>\
            </div>\
            <div  id="quiz-div-singchoice-{{$quiz->id}}"  style="display:none;width:100%;margin: 30px 0px 0px 0px; padding:0px">\
                <div style="padding:0px 0px 0px 0px; text-align:center;">\
                    <p id="quiz-singchoice-SentenceDisplay-{{$quiz->id}}" name="SentenceDisplay" style="font-size:20px;color:#9fc3ae"></p>\
                </div>\
                <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="min-height:350px;padding:20px 0px 0px 0px; justify-content: center;">								\
                    <div id="quiz-singchoice-buttons-{{$quiz->id}}" class="mdl-cell mdl-cell--8-col" style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px">\
                    </div>\
                </div>\
            </div>\
			<div  id="quiz-div-typeaudio-{{$quiz->id}}"  style="display:none;width:100%;margin: 30px 0px 0px 0px; padding:0px">\
                <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="min-height:280px;padding:20px 0px 0px 0px; justify-content: center;">								\
						<div class="mdl-cell mdl-cell--12-col" style="text-align:center;padding:0;margin:0px 0px 0px 0px">\
							<button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButton({{$quiz->id}})" style="background-image:url('+'/upload/image/speaker-icon.png'+');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
							</button>\
						</div>\
						<div class="mdl-cell mdl-cell--6-col" style="padding:0;text-align:center;margin:0px 0px 0px 0px">	\
							<input class="answer_word" type="text" id="input_typeaudio_word-{{$quiz->id}}" name="input_typeaudio_word-{{$quiz->id}}" style="width:100%">																\
						</div>							\
                </div>\
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">\
					<div id="quiz-audio-confirmation-correct-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="display:none;background:url('+'/images/correct.png'+') no-repeat center;height:30px">\
					</div>\
					<div id="quiz-audio-confirmation-incorrect-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="display:none;background:url('+'/images/incorrect.png'+') no-repeat center;height:30px">\
					</div>\
				</div>\	</div> \
			<div  id="quiz-div-typeimage-{{$quiz->id}}"  style="display:none;width:100%;margin: 30px 0px 0px 0px; padding:0px">                        \
                <div class="mdl-grid mdl-cell--12-col" style="min-height:280px;justify-content:center;padding:0px 0px 0px 0px">\
                    <div class="demo-card-image mdl-card mdl-cell mdl-cell--3-col mdl-cell--3-col-tablet mdl-shadow--4dp" id="quiz-typeimage-card" style="position:relative;top: 0;margin-left:10px;border-radius:8px;width: 200px;height: 180px;padding:5px">\
                        <div id="quiz-typeImage-word-img-{{$quiz->id}}" style=" width:190px;height: 170px;">\
                            <div class="mdl-card__title mdl-card--expand"></div>\
                            <div class="mdl-card__actions" style="margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0);">\
                                <input type="hidden" id="quiz-typeImage-word-text-{{$quiz->id}}" name="span" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                                 font-weight: bold "></input>\
                            </div>\
							<img id="copyright-img" src="/images/copyright.png" width="20" height="20" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="ClickCopyright(this,event)" style="z-index:2;position: relative; left:162px; top:-170px">\
                        </div>\
					</div>\
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="padding:20px 0px 0px 0px; justify-content: center;">								\
						<div class="mdl-cell mdl-cell--12-col" style="text-align:center;padding:0;margin:0px 0px 0px 0px">\
							<button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButton({{$quiz->id}})" style="background-image:url('+'/upload/image/speaker-icon.png'+');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
							</button>\
						</div>\
						<div class="mdl-cell mdl-cell--6-col" style="padding:30px 0px 30px 0px;text-align:center;margin:0px 0px 0px 0px">	\
							<input class="answer_word" type="text" id="input_typeimage_word-{{$quiz->id}}" name="input_typeimage_word-{{$quiz->id}}" style="width:100%">\
						</div>\
					</div>\
                </div>\
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">\
					<div id="quiz-image-confirmation-correct-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="display:none;background:url('+'/images/correct.png'+') no-repeat center;height:30px">\
					</div>\
					<div id="quiz-image-confirmation-incorrect-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="display:none;background:url('+'/images/incorrect.png'+') no-repeat center;height:30px">\
					</div>\
				</div>\
			</div>\
			<div  id="quiz-div-gapfill-{{$quiz->id}}"  style="display:none; width:100%;margin: 30px 0px 0px 0px; padding:0px">\
                <div id="gapfill-space-div-{{$quiz->id}}" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="padding:20px 0px 0px 0px; justify-content: center;min-height:280px;">								\
					<div id="gapfill-div-{{$quiz->id}}" class="mdl-cell mdl-cell--9-col" style="text-align:center;">								\
						\
					</div>	\
				</div>\
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">\
					<div id="quiz-gapfill-confirmation-correct-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="display:none;background:url('+'/images/correct.png'+') no-repeat center;height:30px">\
				</div>\
					<div id="quiz-gapfill-confirmation-incorrect-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="display:none;background:url('+'/images/incorrect.png'+') no-repeat center;height:30px">\
					</div>							\
				</div>							\
            </div>\
			<div  id="Result-div-{{$quiz->id}}" style="display:none;margin: 30px 0px 0px 0px; padding:0px">\
						<div style="padding:10px 0px 0px 0px; text-align: center">\
                              <p style="font-weight:bold;font-size: 20px">Congratulations,you have finished the quiz!</p>\
                              <p style="font-weight:bold;font-size: 20px">Here are the correct answers</p>\
                          </div>\
                          <div style="padding:0px 0px 0px 0px; text-align:center;">\
                              <p id="result-score-{{$quiz->id}}" name="result-score" style="font-size:20px;color:#9fc3ae">0</p>\
                          </div>\
                          <div id="Reulst-div-content-{{$quiz->id}}" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="min-height:350px;margin:0px 0px 0px 0px;padding: 0px 0px 0px 0px">\
                          </div>\
						</div>\
              </div>\
              <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="float:bottom;padding:10px 0px 15px 0px">\
                  <hr style="color:#eee; height:1px; margin:5px 0px 0px 0px">\
              </div>\
              <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="min-height:70px;margin:0px 0px 10px 20px;padding: 10px 0px 0px 0px">\
                 <div id="progress-{{$quiz->id}}" class="mdl-cell mdl-cell--7-col" style="width:100%;padding:0px 0px 0px 0px;margin:0px 0px 0px 0px">\
                   <div id="quiz-result-playagain-btn-{{$quiz->id}}"  style="min-height:50px;display:none;margin:0px 0px 0px 0px;padding:0px 0px 0px 0px">\
                       <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect imprevo-default" style="margin:0px 0px 20px 0px" onclick="playagain({{$quiz->id}})">\
                           Play again\
                       </button>\
                   </div>\
                   <div id="quiz-progress-div-{{$quiz->id}}" style="margin-top:10px; width:100%; height: 7px;background-color: #eeeeee;">\
                          <div id="quiz-progress-{{$quiz->id}}" style="background: #399aed;width:0%;height: 7px"></div>\
                          <img id="quiz-progress-thumb-{{$quiz->id}}" src="/images/slider-thumb.png" width="40" height="40" style="position: relative;\
                          left: 0%;top: -24px;z-index: 1;margin:0px 0px 0px 0px">\
                          <span id="quiz-progress-text-{{$quiz->id}}", style="width:15px;height:16px;color:#399aed;font-size:10px;position: relative;\
                          left: -6%;top:10px;z-index: 2;margin:0px 0px 0px 0px">0%</span>\
                  </div>\
                </div>\
            </div>';
            document.getElementById('quiz-part-{{$quiz->id}}').innerHTML = html;
		$('#input_typeaudio_word-{{$quiz->id}}').on('keyup', function (e) {
			if (e.keyCode == 13) {
			// Do something			
				var str = document.getElementById('input_typeaudio_word-{{$quiz->id}}');
				if (str == '')
					return;
	
				QuizNextClick(null, "{{$quiz->id}}");
			}
		});
	
		$('#input_typeimage_word-{{$quiz->id}}').on('keyup', function (e) {
			if (e.keyCode == 13) {
			// Do something			
				var str = document.getElementById('input_typeimage_word-{{$quiz->id}}');
				if (str == '')
					return;
				QuizNextClick(null,"{{$quiz->id}}");
			}
		});				
	
  @endforeach
  

  function onKeyUp_gapfill(event, tag_el, quiz_id)
  {			
	console.log('keyup');
	inputWidth = $('#'+tag_el.id).val().length * 8;
	$('#'+tag_el.id).css({
		width: inputWidth
	})
	
	if (event.keyCode == 13)
	{
		var input_elements = document.getElementsByName('input_gapfill-' + quiz_id);
		for (i in input_elements)
		{
			if (input_elements[i].value == ''){
				input_elements[i].focus();
				return;
			}				
		}
		QuizNextClick(null, quiz_id);
	}		
  }  
  var questions = {};
  var questions_random_array = {};
  var questions_result_array={};
  var question_select_image_step = {};
  var quiz_question_length = {};
  var quiz_step = {};
  var result_status = {};
  var status_question = {};
  var words = [];
  var quiz_parent_top = {};
  var quiz_obj_top = {};
  var IsProcessing = {};
  var audio_button_url={};    
  
  @foreach (\App\Word::all() as $word)
	word = {};
	word.id = "{{$word->id}}".trim();
	word.wordset_index = "{{$word->wordset_index}}".trim();
	word.wordset_id = "{{$word->wordset_id}}".trim();
	word.source_word = "{{$word->source_word}}";
	word.translation = "{{$word->translation}}";
	word.copyright_url = "{{$word->copyright_url}}";
	word.image = "{{$word->image}}";
	word.audio = "{{$word->audio}}";
	word.note = "{{$word->note}}";
	words.push(word);
  @endforeach	

  @foreach ($quiz_list as $quiz)
    questions["{{$quiz->id}}"] = null;
    questions_random_array["{{$quiz->id}}"] = [];
    questions_result_array["{{$quiz->id}}"]=[];
    question_select_image_step["{{$quiz->id}}"] = 0;
    quiz_question_length["{{$quiz->id}}"] = 0;
    quiz_step["{{$quiz->id}}"] = 0;

    result_status["{{$quiz->id}}"] = false;
    status_question["{{$quiz->id}}"] = 0;

    quiz_parent_top["{{$quiz->id}}"] = 0;
    quiz_obj_top["{{$quiz->id}}"] = 0;
    IsProcessing["{{$quiz->id}}"] = 0;
	audio_button_url["{{$quiz->id}}"] = 0;
  @endforeach


  @foreach ($quiz_list as $quiz)
      QuizInit("{{$quiz->id}}");
  @endforeach
@endif
function pageIndex(page)
{
	  event.preventDefault();
      event.stopPropagation();
	  document.getElementById('page').value = page;
	  $('#pageForm').submit();
}
  
function QuizInit(quiz_id)
{
    @foreach ($quiz_list as $quiz)
      if (quiz_id == "{{$quiz->id}}"){
        questions_random_array["{{$quiz->id}}"] = [];
        questions["{{$quiz->id}}"] = null;
        questions_result_array["{{$quiz->id}}"]=[];
        question_select_image_step["{{$quiz->id}}"] = 0;
        quiz_question_length["{{$quiz->id}}"] = 0;
        quiz_step["{{$quiz->id}}"] = 0;
        result_status["{{$quiz->id}}"] = false;
        status_question["{{$quiz->id}}"] = 0;

        document.getElementById('Result-div-{{$quiz->id}}').style.display = 'none';
        document.getElementById('quiz-div-seletImage-{{$quiz->id}}').style.display = 'none';
        document.getElementById('quiz-div-singchoice-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-div-typeaudio-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-div-gapfill-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-div-typeimage-{{$quiz->id}}').style.display = 'none';
	 
		questions["{{$quiz->id}}"]=[];
		@foreach ($quiz->questions as $question)		
			question = {};
			question.id = "{{$question->id}}".trim();
			question.instruction = <?php echo json_encode($question->instruction)?>;
			question.note = <?php echo json_encode($question->note)?>;;
			question.type = "{{$question->type}}";
			question.answer_data = <?php echo json_encode($question->answer_data)?>;
			question.quiz_id = "{{$question->quiz_id}}";
			question.correct_answer_id = <?php echo json_encode($question->correct_answer_id)?>;//"{{$question->correct_answer_id}}";
			questions["{{$quiz->id}}"].push(question);			
		@endforeach
		
		quizsettings = [];
		@foreach (\App\QuizSetting::all() as $quizsetting)
			quizsetting = {};				
			quizsetting.term = <?php echo json_encode($quizsetting->term)?>.trim().split('|');										
			quizsettings.push(quizsetting);
		@endforeach	
		
        if (questions.length == 0)
        {
            document.getElementById('quiz-progress-{{$quiz->id}}').style.width = 0;
            return;
        }


          for (var i = 0; i< questions["{{$quiz->id}}"].length; i++)
          {
              if (questions["{{$quiz->id}}"][i].type == 1)
              {
                  questions_random_array["{{$quiz->id}}"].push(i);
              }
              else if (questions["{{$quiz->id}}"][i].type == 2)
              {

                  Answer_data = questions["{{$quiz->id}}"][i].answer_data;
                  ArrayAnswer = Answer_data.split(',');
                  RandomArrayAnswer = Answer_data.split(',');
                  var b = false;
                  while(!b){
                      shuffle(RandomArrayAnswer);
                      b = true;
                      for (j in ArrayAnswer)
                      {
                          if (ArrayAnswer[j] == RandomArrayAnswer[j])
                          {
                              b = false
                          }
                      }
                  }

                  for (answer in ArrayAnswer)
                  {
                      questions_random_array["{{$quiz->id}}"].push([i, ArrayAnswer[answer].trim(), RandomArrayAnswer[answer].trim()]);
                  }
              }
			  else if (questions["{{$quiz->id}}"][i].type == 3)
			  {
              
			  	Answer_data = questions["{{$quiz->id}}"][i].answer_data;
			  	ArrayAnswer = Answer_data.split(',');
              
			  	for (answer in ArrayAnswer)
			  	{
			  		questions_random_array["{{$quiz->id}}"].push([i, ArrayAnswer[answer].trim()]);
			  	}
			  }
			  else if (questions["{{$quiz->id}}"][i].type == 4)
			  {
              
			  	Answer_data = questions["{{$quiz->id}}"][i].answer_data;
			  	ArrayAnswer = Answer_data.split(',');
              
			  	for (answer in ArrayAnswer)
			  	{
			  		questions_random_array["{{$quiz->id}}"].push([i, ArrayAnswer[answer].trim(), 0, 0]);
			  	}
			  }
			  else if (questions["{{$quiz->id}}"][i].type == 5)
			  {
			  	var instruction = questions["{{$quiz->id}}"][i].instruction;
			  	var ArrayAnswer = [];
			  	var instruction_array = instruction.split("\{\{\{");
			  	var instruction_elements = [];
			  	
			  	for (var l in instruction_array)
			  	{					
			  		var element = instruction_array[l];
			  		element_split = element.split("\}\}\}");
			  		if (element_split.length == 2)
			  		{						
			  			ArrayAnswer.push(element_split[0]);
			  			instruction_elements.push('input');
			  			instruction_elements.push(element_split[1]);
			  		}
			  		else
			  		{
			  			instruction_elements.push(element_split[0]);
			  		}										
			  	}								
			  	questions_random_array["{{$quiz->id}}"].push([i, instruction_elements, ArrayAnswer, quizsettings, 0]);
			  }					  
          }

          quiz_question_length["{{$quiz->id}}"] = questions_random_array["{{$quiz->id}}"].length;
  		  @if ($quiz)
  		  	@if($quiz->random_type > 1)
  		  	 shuffle(questions_random_array["{{$quiz->id}}"]);
  		  	@endif
  		  @endif
      }
    @endforeach
    QuizStep(null, quiz_id);
}
function removeSpecialChars(str) {
	return str.replace(/(?!\w|\s)./g, '')
			  .replace(/\s+/g, ' ')
			  .replace(/^(\s*)([\W\w]*)(\b\s*$)/g, '$2');
}

function RemovePunctuations(str)
{
	//console.log('------------------');
	//console.log(str);
	str = str.replace(/[!&\/\\#,+()$~%.":*?<>{}]/g, '');
	//console.log(str);
	return str;
}


function QuizNextClick(obj, quiz_id)
{
  @foreach ($quiz_list as $quiz)
  if ("{{$quiz->id}}" == quiz_id)
  {
    if (IsProcessing["{{$quiz->id}}"]) return;
    IsProcessing["{{$quiz->id}}"] = true;
    var question_info;
    var step = 0;

    if (quiz_step["{{$quiz->id}}"] == 0)
    {
        step = quiz_question_length["{{$quiz->id}}"]-1;
    }
    else
    {
        step = quiz_step["{{$quiz->id}}"]-1;
    }

    if (quiz_step["{{$quiz->id}}"] > 0 || (quiz_step["{{$quiz->id}}"]==0 && result_status["{{$quiz->id}}"]))
    {
        if (quiz_step["{{$quiz->id}}"] == 0)
        {
            question_info = questions_random_array["{{$quiz->id}}"][step];
        }
        else
        {
            question_info = questions_random_array["{{$quiz->id}}"][step];
        }
        var youranswer = '';
        if (question_info.length == 3) {
            var word = null;
            for (i in words) {
                if (words[i].wordset_id == "{{$quiz->wordset_id}}" && words[i].wordset_index == question_info[1]) {
                    word = words[i];
                }
            }
            childs = obj.children;

            textSpan = childs[1].children;

            youranswer = textSpan[0].innerText;
            if (word.translation == youranswer)
            {

              if (status_question["{{$quiz->id}}"] == 0)
                questions_result_array["{{$quiz->id}}"].push([1,youranswer]);
              status_question["{{$quiz->id}}"] = 0;
            }
            else
            {
              if (status_question["{{$quiz->id}}"] == 0)
                questions_result_array["{{$quiz->id}}"].push([0,youranswer]);
              status_question["{{$quiz->id}}"] = 1;
            }
        }
		else if (question_info.length == 4) {
              var word = null;
              for (i in words) {
                  if (words[i].wordset_id == "{{$quiz->wordset_id}}" && words[i].wordset_index == question_info[1]) {
                      word = words[i];
                  }
              }
			  youranswer = document.getElementById('input_typeimage_word-{{$quiz->id}}').value;			  
              if (word.source_word.toLowerCase() == youranswer.toLowerCase())
              {
                //if (status == 0)
                  questions_result_array["{{$quiz->id}}"].push([1,youranswer]);
				  status_question["{{$quiz->id}}"] = 0;
              }
              else
              {
                //if (status == 0)
                  questions_result_array["{{$quiz->id}}"].push([0,youranswer]);
				 status_question["{{$quiz->id}}"] = 1;
              }				  
		  }
		  else if (question_info.length == 5) {
			  
			    var input_elements = document.getElementsByName("input_gapfill-{{$quiz->id}}");
				var answer_data_array = question_info[2];
				var youranswers = [];
				status_question["{{$quiz->id}}"] = 1;
				var terms = question_info[3];
				for (i in input_elements)
				{		
					if (!input_elements[i].value) continue;				
					youranswers.push(input_elements[i].value);
					var element = RemovePunctuations(answer_data_array[i]).trim().toLowerCase();						
					var elements = element.split('|');
					var status_element = false;
					var terms = question_info[3];
					for (j in elements)
					{					
						if (status_element == true) break;
						for (p in terms)
						{												
							term = terms[p].term;											
							var term_current = null;
							if (status_element == true) break;
							for (h in term)
							{														
								if (elements[j].includes(term[h].toLowerCase()))
								{
									include_str = term[h].toLowerCase();
									term_current = term;
									if (status_element == true) break;
									for (p in term_current)
									{
										if (status_element == true) break;
										var replace_answer = elements[j].replace(include_str, term_current[p].toLowerCase());
										if (replace_answer == RemovePunctuations(input_elements[i].value.toLowerCase()).trim())
										{
											status_element = true;											
											break;
										}
									}								
									break;
								}
							}
						}
						
						if (elements[j] == RemovePunctuations(input_elements[i].value.toLowerCase()).trim())
						{
							status_element = true;
							break;
						}	
							
					}
					if (status_element == false){												
						 status_question["{{$quiz->id}}"] = 0;						
					}					
				}
				
				if ( status_question["{{$quiz->id}}"] == 1)
				{
					questions_result_array["{{$quiz->id}}"].push([1,youranswers]);										
				}	
				else
				{	
					questions_result_array["{{$quiz->id}}"].push([0,youranswers]);										
				}	
							 
		  }
		  else if (question_info.length == 2) {
              var word = null;
              for (i in words) {
                  if (words[i].wordset_id == "{{$quiz->wordset_id}}" && words[i].wordset_index == question_info[1]) {
                      word = words[i];
                  }
              }
			  youranswer = document.getElementById('input_typeaudio_word-{{$quiz->id}}').value;
			  if (word.source_word.toLowerCase() == youranswer.toLowerCase())
              {
                //if (status == 0)
                  questions_result_array["{{$quiz->id}}"].push([1,youranswer]);
                 status_question["{{$quiz->id}}"] = 0;
              }
              else
              {
                //if (status == 0)
                  questions_result_array["{{$quiz->id}}"].push([0,youranswer]);
                 status_question["{{$quiz->id}}"] = 1;
              }			  
		  }		
		  else
		  {
				var question = questions["{{$quiz->id}}"][question_info];
				youranswer = obj.innerText;
				if (youranswer == question.correct_answer_id.toLowerCase())
				{
					if (status_question["{{$quiz->id}}"] == 0)
						questions_result_array["{{$quiz->id}}"].push([1,youranswer]);
					status_question["{{$quiz->id}}"] = 0;
				}
				else
				{
					if (status_question["{{$quiz->id}}"] == 0)
						questions_result_array["{{$quiz->id}}"].push([0,youranswer]);
					status_question["{{$quiz->id}}"] = 1;
				}
		  }
    }

      

      if (question_info.length == 3) {
		 parent = obj.parentNode;
        if (status_question["{{$quiz->id}}"] == 0)
        {
            parent.style.backgroundColor = '#5c9f77';
            document.getElementById("quiz_selectImage_displaycorrectword-{{$quiz->id}}").innerHTML = youranswer;
        }
        else
        {
            parent.style.backgroundColor = '#ff0000'

            setTimeout(function(){BouncingEffect(parent, '-20px');}, 100);
            setTimeout(function(){BouncingEffect(parent, '20px');}, 200);
            setTimeout(function(){BouncingEffect(parent, '-15px');}, 300);
            setTimeout(function(){BouncingEffect(parent, '15px');}, 400);
            setTimeout(function(){BouncingEffect(parent, '-10px');}, 500);
            setTimeout(function(){BouncingEffect(parent, '10px');}, 600);
            setTimeout(function(){BouncingEffect(parent, '-5px');}, 700);
            setTimeout(function(){BouncingEffect(parent, '5px');}, 800);
            setTimeout(function(){BouncingEffect(parent, '0px');}, 900);
            quiz_step["{{$quiz->id}}"]--;

        }
      }
		else if (question_info.length == 2)
		{
			if (status_question["{{$quiz->id}}"] == 0)
			{							
				$('#input_typeaudio_word-{{$quiz->id}}').css('border-bottom', '2px solid #008000');
				document.getElementById('quiz-audio-confirmation-correct-{{$quiz->id}}').style.display = 'inline';				
			}
			else
			{				
				$("#input_typeaudio_word-{{$quiz->id}}").css('border-bottom', '2px solid #ff0000');			
				document.getElementById('quiz-audio-confirmation-incorrect-{{$quiz->id}}').style.display = 'inline';				
			}				
		}
		else if (question_info.length == 4)
		{
			if (status_question["{{$quiz->id}}"] == 0)
			{							
				$("#input_typeimage_word-{{$quiz->id}}").css("border-bottom", "2px solid #008000");
				document.getElementById('quiz-image-confirmation-correct-{{$quiz->id}}').style.display = 'inline';				
			}
			else
			{				
				$("#input_typeimage_word-{{$quiz->id}}").css("border-bottom", "2px solid #ff0000");
				document.getElementById('quiz-image-confirmation-incorrect-{{$quiz->id}}').style.display = 'inline';
				//quiz_step--;
			}				
		}
		else if (question_info.length == 5)
		{
			var input_elements = document.getElementsByName("input_gapfill-{{$quiz->id}}");			 		    
			var answer_data_array = question_info[2];
			var terms = question_info[3];
			for (i in input_elements)
			{
				if (!input_elements[i].value) continue;				
				var element = RemovePunctuations(answer_data_array[i]).trim().toLowerCase();				
				var elements = element.split('|');				
				var status_element = false;
				
				for (j in elements)
				{	
					if (status_element) break;
					for (p in terms)
					{												
						term = terms[p].term;											
						var term_current = null;
						if (status_element == true) break;
						for (h in term)
						{						
							if (term_current) break;							
							if (elements[j].includes(term[h].toLowerCase()))
							{
								include_str = term[h].toLowerCase();
								term_current = term;
								if (status_element == true) break;
								for (p in term_current)
								{
									if (status_element == true) break;
									var replace_answer = elements[j].replace(include_str, term_current[p].toLowerCase());
									if (replace_answer == RemovePunctuations(input_elements[i].value.toLowerCase()).trim())
									{
										status_element = true;
										break;
									}
								}								
								break;
							}
						}
					}
					
					if (elements[j] == RemovePunctuations(input_elements[i].value.toLowerCase()).trim())
					{
						status_element = true;
						break;
					}								
				}
				
				if (status_element == false){						
					$('#input_gapfill-{{$quiz->id}}'+i).css("border-bottom", "2px solid #ff0000");	
					document.getElementById('quiz-gapfill-confirmation-incorrect-{{$quiz->id}}').style.display = 'inline';						
				}	
				else
				{
					$('#input_gapfill-{{$quiz->id}}'+i).css("border-bottom", "2px solid #008000");		
					document.getElementById('quiz-gapfill-confirmation-correct-{{$quiz->id}}').style.display = 'inline';
				}
			}												
		}	  
      else
      {
        if (status_question["{{$quiz->id}}"] == 0)
        {
            obj.style.backgroundColor = '#5c9f77';
        }
        else
        {
            obj.style.backgroundColor = '#ff0000'
            obj.style.top = obj.style.top - 10;
            quiz_step["{{$quiz->id}}"]--;
        }
      }
  }


  @endforeach

   setTimeout(function(){QuizStep(obj, quiz_id);}, 1000);
}

function BouncingEffect(obj, l)
{
  if (obj)
  {
        obj.style.top = l;
  }
}

function QuizStep(obj, quiz_id)
{
  @foreach ($quiz_list as $quiz)
    if ("{{$quiz->id}}" == quiz_id)
    {
      IsProcessing["{{$quiz->id}}"] = false;

      document.getElementById('Result-div-{{$quiz->id}}').style.display = 'none';
	  $("#input_typeaudio_word-{{$quiz->id}}").css("border-bottom", "2px solid #f29f00");
	  $('#input_typeaudio_word-{{$quiz->id}}').val('');
		
	  $("#input_typeimage_word-{{$quiz->id}}").css("border-bottom", "2px solid #f29f00");
	  $('#input_typeimage_word-{{$quiz->id}}').val('');
	  
	  	document.getElementById('quiz-gapfill-confirmation-incorrect-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-gapfill-confirmation-correct-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-audio-confirmation-incorrect-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-audio-confirmation-correct-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-image-confirmation-incorrect-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-image-confirmation-correct-{{$quiz->id}}').style.display = 'none';	
      if (obj)
      {
        parent = obj.parentNode;
        obj.style.backgroundColor = '#f4f4f4';
        obj.style.top = quiz_obj_top["{{$quiz->id}}"];

        parent.style.backgroundColor = '#ffffff'
        parent.style.top = quiz_parent_top["{{$quiz->id}}"];
        document.getElementById("quiz_selectImage_displaycorrectword-{{$quiz->id}}").innerHTML = '&nbsp;'
      }
      if (quiz_step["{{$quiz->id}}"] == -1 && status_question["{{$quiz->id}}"] != 0)
      {

          quiz_step["{{$quiz->id}}"] = quiz_question_length["{{$quiz->id}}"]-1;
          result_status["{{$quiz->id}}"] = false;
      }

      if (!result_status["{{$quiz->id}}"]) {

        document.getElementById('quiz-div-seletImage-{{$quiz->id}}').style.display = 'none';
        document.getElementById('quiz-div-singchoice-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-div-typeaudio-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-div-typeimage-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-div-gapfill-{{$quiz->id}}').style.display = 'none';		
        document.getElementById('quiz-result-playagain-btn-{{$quiz->id}}').style.display = 'none';
        document.getElementById('quiz-progress-div-{{$quiz->id}}').style.visibility = 'visible';

          var question_info = questions_random_array["{{$quiz->id}}"][quiz_step["{{$quiz->id}}"]];

          var index_num = question_info[0];
          var question = questions["{{$quiz->id}}"][question_info];			  
          if (question_info.length == 3) {

              var word = null;
              var random_word = null;			  						 
              for (i in words) {	
				if (words[i].wordset_id == "{{$quiz->wordset_id}}"){				  
                  if (words[i].wordset_index == question_info[1]) {	
                      word = words[i];
                  }
                  if (words[i].wordset_index == question_info[2]) {
                      random_word = words[i];
                  }
				  if (word && random_word) break;
				}  
              }
			  if (word)
				playSound(word.audio);
			  audio_button_url["{{$quiz->id}}"] = word.audio;
              document.getElementById('quiz-div-seletImage-{{$quiz->id}}').style.display = 'inline';					  			  
              document.getElementById('quiz_selectImage_displayword-{{$quiz->id}}').innerHTML = word.source_word;

              var display_order = 0;
              if (status_question["{{$quiz->id}}"]==0) display_order = Math.random();
              if (display_order < 0.5) {
                  document.getElementById('quiz-selectImage-word1-img-{{$quiz->id}}').style.background = 'url("' + word.image + '") no-repeat center/190px 170px';
                  document.getElementById('quiz-selectImage-word1-text-{{$quiz->id}}').innerHTML = word.translation;
                  document.getElementById('quiz-selectImage-word2-img-{{$quiz->id}}').style.background = 'url("' + random_word.image + '") no-repeat center/190px 170px';
                  document.getElementById('quiz-selectImage-word2-text-{{$quiz->id}}').innerHTML = random_word.translation;
              }
              else {
                  document.getElementById('quiz-selectImage-word2-img-{{$quiz->id}}').style.background = 'url("' + word.image + '") no-repeat center/190px 170px';
                  document.getElementById('quiz-selectImage-word2-text-{{$quiz->id}}').innerHTML = word.translation;
                  document.getElementById('quiz-selectImage-word1-img-{{$quiz->id}}').style.background = 'url("' + random_word.image + '") no-repeat center/190px 170px';
                  document.getElementById('quiz-selectImage-word1-text-{{$quiz->id}}').innerHTML = random_word.translation;
              }
          }
		  else if (question_info.length == 2){
				var word = null;
                var random_word = null;
				
                for (i in words) {
                    if (words[i].wordset_id == "{{$quiz->wordset_id}}" && words[i].wordset_index == question_info[1]) {
                        word = words[i];
                    }
                }
				if (word.audio)
					playSound(word.audio);		

				audio_button_url["{{$quiz->id}}"] = word.audio;
				
				
				document.getElementById('quiz-div-typeaudio-{{$quiz->id}}').style.display = 'inline';
			}
			else if (question_info.length == 4){
				var word = null;
                var random_word = null;
				
                for (i in words) {
                    if (words[i].wordset_id == "{{$quiz->wordset_id}}" && words[i].wordset_index == question_info[1]) {
                        word = words[i];
                    }
                }
				if (word.audio)
					playSound(word.audio);
				audio_button_url["{{$quiz->id}}"] = word.audio;
				document.getElementById('quiz-div-typeimage-{{$quiz->id}}').style.display = 'inline';
				document.getElementById('quiz-typeImage-word-img-{{$quiz->id}}').style.background = 'url("' + word.image + '") no-repeat center/190px 170px';
				document.getElementById('quiz-typeImage-word-text-{{$quiz->id}}').innerHTML = word.translation;     
			}
			else if (question_info.length == 5){				
				var html = '';
				var instruction_elements = question_info[1];
				var k = 0;

				for (i in instruction_elements)
				{
					if (instruction_elements[i] == 'input')
					{						
						html = html +  '<input class="answer_word" type="text" id="input_gapfill-{{$quiz->id}}' + k + '" name="input_gapfill-{{$quiz->id}}" onkeyup="onKeyUp_gapfill(event, this, {{$quiz->id}})" style="text-align:center"></input>';									
						k++;					
					}
					else
					{
						
						html = html + instruction_elements[i];
					}					
				}
				
				document.getElementById('gapfill-div-{{$quiz->id}}').innerHTML = html;								
				document.getElementById('quiz-div-gapfill-{{$quiz->id}}').style.display = 'inline';
				
				var gap0 = document.getElementById('input_gapfill-{{$quiz->id}}0');
				if (gap0) gap0.focus();
			}					  
          else {
              document.getElementById('quiz-div-singchoice-{{$quiz->id}}').style.display = 'inline';

              document.getElementById('quiz-singchoice-SentenceDisplay-{{$quiz->id}}').innerHTML = question.instruction;
              Answer_data = question.answer_data;
              Answerarray = Answer_data.split(',');

              html = '';
              for (j in Answerarray) {
                  html = html + '<button class="mdl-button mdl-js-button mdl-button--raised" onclick="QuizNextClick(this, {{$quiz->id}})"\
                              style="font-size:18px;text-transform: lowercase;text-align:left;margin:9px;width:100%">' + Answerarray[j] + '</button>';

              }
              document.getElementById('quiz-singchoice-buttons-{{$quiz->id}}').innerHTML = html;
          }
          var percent = parseInt(quiz_step["{{$quiz->id}}"] * 100 / (quiz_question_length["{{$quiz->id}}"]));
          var text_percent = percent - 7;
          var text_thum = percent - 3;
          percent = percent + '%';
          text_thum = text_thum + '%';
          document.getElementById('quiz-progress-{{$quiz->id}}').style.width = percent;
          document.getElementById('quiz-progress-thumb-{{$quiz->id}}').style.left = text_thum;

          text_percent = text_percent + '%';
          document.getElementById('quiz-progress-text-{{$quiz->id}}').style.left = text_percent;
          document.getElementById('quiz-progress-text-{{$quiz->id}}').innerHTML = percent;
          quiz_step["{{$quiz->id}}"]++;

          if (quiz_step["{{$quiz->id}}"] >= quiz_question_length["{{$quiz->id}}"])
          {
              quiz_step["{{$quiz->id}}"] = 0;
              result_status["{{$quiz->id}}"] = true;
          }
          else
              result_status["{{$quiz->id}}"] = false;
      }
      else
      {
          document.getElementById('quiz-div-seletImage-{{$quiz->id}}').style.display = 'none';
          document.getElementById('quiz-div-singchoice-{{$quiz->id}}').style.display = 'none';
		  document.getElementById('quiz-div-typeaudio-{{$quiz->id}}').style.display = 'none';
		  document.getElementById('quiz-div-typeimage-{{$quiz->id}}').style.display = 'none';
		  document.getElementById('quiz-div-gapfill-{{$quiz->id}}').style.display = 'none';		  
          document.getElementById('quiz-progress-div-{{$quiz->id}}').style.visibility = 'hidden';
          document.getElementById('quiz-result-playagain-btn-{{$quiz->id}}').style.display = 'inline';
          document.getElementById('Result-div-{{$quiz->id}}').style.display = 'inline';

          html='';
          var score = 0;
          @if($quiz)
          @if ($quiz->random_type > 2)
              shuffle(questions_random_array["{{$quiz->id}}"]);
          @endif
          @endif
          for (var k=0; k<quiz_question_length["{{$quiz->id}}"]; k++)
          {
              if (questions_result_array["{{$quiz->id}}"][k][0] == 1) score++;

              var question_info = questions_random_array["{{$quiz->id}}"][k];
              if (question_info.length == 3) {
                  var word = null;
				  for (i in words) {	
				  	if (words[i].wordset_id == "{{$quiz->wordset_id}}"){				  
				  	if (words[i].wordset_index == question_info[1]) {	
				  		word = words[i];
				  	}
				  	if (words[i].wordset_index == question_info[2]) {
				  		random_word = words[i];
				  	}
				  	if (word && random_word) break;
				  	}  
				  }
                  if (word.note == null)
                      note = '';
                  else
                      note = word.note;
                  word_audio_url = word.audio;
                  html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                      <div class="demo-card-image mdl-card mdl-cell mdl-cell--2-col mdl-shadow--4dp" style="margin-left:20px;border-radius:10px;width: 200px;height: 200px;padding:10px">\
                      <div style="width: 180px;height: 180px; background: url(' + word.image + ') center / cover;">\
                      <div class="mdl-card__title mdl-card--expand"></div>\
                      <div class="mdl-card__actions" style="margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0.7);">\
                      <span id="quiz-result-word1-text" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                      font-weight: bold ">' + word.translation + '</span>\
                      </div>\
                      </div>\
                      <img src="/images/copyright.png" width="20" height="20" style="z-index:1;position: relative; left:155px; top:-175px">\
                      </div>\
                      <div class="mdl-grid--no-spacing mdl-grid--no-spacing-cell mdl-cell--8-col mdl-cell--4-col-tablet" style="margin-left:30px;">\
                      <div class="mdl-grid mdl-grid--no-spacing-cell mdl-cell--12-col" style="">\
                      <div class=" mdl-grid--no-spacing-cell mdl-cell--8-col" style="">\
                      <div style="padding:10px 0px 0px 0px;">\
                      <p id="source_word" style="color:#9fc3ae; font-size: 16px">'+word.source_word+'</p>\
                      </div>\
                      </div>\
                      <div class=" mdl-grid--no-spacing-cell mdl-cell--4-col" style="">\
                      <div style="float:right;margin:0px 0px 0px 0px">\
                      <button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButtionResultDiv(' + word.id + ')" style="background-image:url(' + '/upload/image/speaker-icon.png' + ');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
                      </button>\
                      </div>\
                      </div>\
                      </div>\
                      <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                      <hr>\
                      </div>\
                      <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                      <p style="font-weight: bold; margin-bottom:10px">Note!</p>\
                      <span>'+note+'</span>\
                      </div>\
                      </div>\
                      </div>'
              }
				else if (question_info.length == 2) {
					var word = null;
					for (i in words) {	
						if (words[i].wordset_id == "{{$quiz->wordset_id}}"){				  
						if (words[i].wordset_index == question_info[1]) {	
							word = words[i];
						}
						if (word) break;
						}  
					}
					if (word.note == null)
                        note = '';
                    else
                        note = word.note;
										
                    html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                        <div class="demo-card-image mdl-card mdl-cell mdl-cell--2-col mdl-shadow--4dp" style="margin-left:20px;border-radius:8px;width: 200px;height: 180px;padding:5px">\
                        <div style="width: 190px;height: 170px; background: url(' + word.image + ') no-repeat center / 190px 170px;">\
                        <div class="mdl-card__title mdl-card--expand"></div>\
                        <div class="mdl-card__actions" style="visibility:hidden;margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0);">\
                        <span id="quiz-result-word1-text" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                    		font-weight: bold ">' + word.translation + '</span>\
                    		</div>\
                    		</div>\
                    		<img src="/images/copyright.png" width="20" height="20" style="z-index:1;position: relative; left:162px; top:-162px">\
                        </div>\
                        <div class="mdl-grid--no-spacing mdl-grid--no-spacing-cell mdl-cell--8-col mdl-cell--4-col-tablet" style="margin-left:30px;">\
                        <div class="mdl-grid mdl-grid--no-spacing-cell mdl-cell--12-col" style="">\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--8-col" style="">\
                        <div style="padding:10px 0px 0px 0px;">';
					if (questions_result_array["{{$quiz->id}}"][k][0] == 1)
					{					
                       html = html + '<p id="answer_result_word" style="color:#9fc3ae; font-size: 16px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">'+ questions_result_array["{{$quiz->id}}"][k][1] + '</span></p>';
					}   
				    else
					{						
						html = html + '<p id="answer_result_word" style="color:#9fc3ae; font-size: 16px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#ff0000">'+ questions_result_array["{{$quiz->id}}"][k][1] + '</span></p>';
					}
					html = html + '<p id="source_word" style="color:#9fc3ae; font-size: 16px">Correct answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + word.source_word + '</span></p>\
                        </div>\
                        </div>\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--4-col" style="">\
                        <div style="float:right;margin:0px 0px 0px 0px">\
                        <button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButtionResultDiv(' + word.id + ')" style="background-image:url(' + "/upload/image/speaker-icon.png" + ');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
                        </button>\
                        </div>\
                        </div>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <hr>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <p style="font-weight: bold; margin-bottom:10px">Note!</p>\
                    		<span>'+note+'</span>\
                    		</div>\
                    		</div>\
                    		</div>'								
				}
				else if (question_info.length == 4) {
					var word = null;
					for (i in words) {	
						if (words[i].wordset_id == "{{$quiz->wordset_id}}"){				  
						if (words[i].wordset_index == question_info[1]) {	
							word = words[i];
						}
						if (word) break;
						}  
					}
					if (word.note == null)
                        note = '';
                    else
                        note = word.note;					
					html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                        <div class="demo-card-image mdl-card mdl-cell mdl-cell--2-col mdl-shadow--4dp" style="margin-left:20px;border-radius:8px;width: 200px;height: 180px;padding:5px">\
                        <div style="width: 190px;height: 170px; background: url(' + word.image + ') no-repeat center / 190px 170px;">\
                        <div class="mdl-card__title mdl-card--expand"></div>\
                        <div class="mdl-card__actions" style="visibility:hidden;margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0);">\
                        <span id="quiz-result-word1-text" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                    		font-weight: bold ">' + word.translation + '</span>\
                    		</div>\
                    		</div>\
                    		<img src="/images/copyright.png" width="20" height="20" style="z-index:1;position: relative; left:162px; top:-162px">\
                        </div>\
                        <div class="mdl-grid--no-spacing mdl-grid--no-spacing-cell mdl-cell--8-col mdl-cell--4-col-tablet" style="margin-left:30px;">\
                        <div class="mdl-grid mdl-grid--no-spacing-cell mdl-cell--12-col" style="">\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--8-col" style="">\
                        <div style="padding:10px 0px 0px 0px;">';

					if (questions_result_array["{{$quiz->id}}"][k][0] == 1)
					{					
                       html = html + '<p id="answer_result_word" style="color:#9fc3ae; font-size: 16px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">'+ questions_result_array["{{$quiz->id}}"][k][1] + '</span></p>';
					}   
				    else
					{						
						html = html + '<p id="answer_result_word" style="color:#9fc3ae; font-size: 16px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#ff0000">'+ questions_result_array["{{$quiz->id}}"][k][1] + '</span></p>';
					}
					html = html + '<p id="source_word" style="color:#9fc3ae; font-size: 16px">Correct answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + word.source_word + '</span></p>\
                        </div>\
                        </div>\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--4-col" style="">\
                        <div style="float:right;margin:0px 0px 0px 0px">\
                        <button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButtionResultDiv(' + word.id + ')" style="background-image:url(' + "/upload/image/speaker-icon.png" + ');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
                        </button>\
                        </div>\
                        </div>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <hr>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <p style="font-weight: bold; margin-bottom:10px">Note!</p>\
                    		<span>'+note+'</span>\
                    		</div>\
                    		</div>\
                    		</div>'								
				}	
				else if (question_info.length == 5) {
					
					var question_str = '';
					var answer_str = '';
					var youranswers = questions_result_array["{{$quiz->id}}"][k][1];					
					var answer_data_array = question_info[2];
					var instruction_array = question_info[1];			
					var terms = question_info[3];
					var m = 0;
					for (var i in instruction_array)
					{
						if (instruction_array[i] == 'input')
						{		
							var element = RemovePunctuations(answer_data_array[m]).trim().toLowerCase();
							var elements = element.split('|');	
							var status_element = false;

							for (j in elements)
							{
								if (status_element) break;
								for (p in terms)
								{												
									term = terms[p].term;											
									var term_current = null;
									if (status_element == true) break;
									for (h in term)
									{						
										if (term_current) break;							
										if (elements[j].includes(term[h].toLowerCase()))
										{
											include_str = term[h].toLowerCase();
											term_current = term;
											if (status_element == true) break;
											for (p in term_current)
											{
												if (status_element == true) break;
												var replace_answer = elements[j].replace(include_str, term_current[p].toLowerCase());
												if (replace_answer == RemovePunctuations(youranswers[m].toLowerCase()).trim())
												{
													status_element = true;
													break;
												}
											}								
											break;
										}
									}
								}
								
								if (elements[j] == RemovePunctuations(youranswers[m].toLowerCase()).trim())
								{
									status_element = true;
									break;
								}																	
							}
								
							if (status_element == true)
							{
								answer_str = answer_str + '<span style="padding:0px 10px 0px 10px; border-bottom: 2px solid #008000">' + youranswers[m] + '</span>';
							}
							else
							{
								answer_str = answer_str + '<span style="padding:0px 10px 0px 10px; border-bottom: 2px solid #ff0000">' + youranswers[m] + '</span>';
							}
							question_str = question_str + '<span style="padding:0px 10px 0px 10px; border-bottom: 2px solid #f29f00">' + answer_data_array[m] + '</span>';
							
							m++;
						}
						else
						{
							question_str = question_str + instruction_array[i];
							answer_str = answer_str + instruction_array[i];
						}
					}		
					var question = questions["{{$quiz->id}}"][question_info[0]];					
					var note = '';
					if (question.note) note = question.note;
					html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
								<div class="mdl-cell mdl-cell--12-col" style="">\
								    <p style="font-weight: bold; font-size:20px; margin-bottom:10px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">'+ answer_str  + '</span></p>\
									<p style="font-weight: bold; font-size:20px; margin-bottom:10px">Correct answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question_str + '</span></p>\
									<hr>\
									<p style="font-weight: bold; font-size:20px; margin-bottom:10px">Note:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + note + '</span></p>\
                                </div>\
								</div>'				
				}							  
              else
              {
                  var question = questions["{{$quiz->id}}"][question_info];
                  html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                          <div class="mdl-cell mdl-cell--12-col" style="">\
                              <span style="font-weight: bold; font-size:20px; margin-bottom:10px">Question:&nbsp<span style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.instruction + '</span></span>\
                              <p style="font-weight: bold; font-size:20px; margin-bottom:10px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">'+ questions_result_array["{{$quiz->id}}"][k][1] + '</span></p>\
                              <p style="font-weight: bold; font-size:20px; margin-bottom:10px">Correct answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.correct_answer_id + '</span></p>\
                              <hr>\
                              <p style="font-weight: bold; font-size:20px; margin-bottom:10px">Note:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.note + '</span></p>\
                          </div>\
                      </div>'
              }

          }
          document.getElementById('result-score-{{$quiz->id}}').innerHTML = 'Your Score: ' + score + '/' + quiz_question_length["{{$quiz->id}}"];
          document.getElementById('Reulst-div-content-{{$quiz->id}}').innerHTML = html;
      }

    }
  @endforeach
}

function shuffle (array) {
    var i = 0
        , j = 0
        , temp = null

    for (i = array.length - 1; i > 0; i -= 1) {
        j = Math.floor(Math.random() * (i + 1))
        temp = array[i]
        array[i] = array[j]
        array[j] = temp
    }
}

function AudioButton(quiz_id)
{ 
  if (audio_button_url[quiz_id])
	playSound(audio_button_url[quiz_id]);
}


function AudioButtionResultDiv(result_word_id)
{
  for (p in words) {
     if (words[p].id == result_word_id) {
         word = words[p];
     }
  }
  playSound(word.audio);
}

function playSound(url){
    var audio = document.createElement('audio');
    audio.style.display = "none";
    audio.src = url;
    audio.autoplay = true;
    audio.onended = function(){
      audio.remove()
    };
    document.body.appendChild(audio);
 }
function mouseover(x) {
    x.src = "/images/copyright-hover.png";
}

function mouseout(x) {
    x.src = "/images/copyright.png";
}

function ClickCopyright(obj, event)
{
	  
      event.preventDefault();
      event.stopPropagation();
      parent = obj.parentNode;


      var childNodeArray = parent.childNodes;
	  console.log(childNodeArray);
      var child1Array = childNodeArray[3].childNodes;

      tran_word = child1Array[1].innerText;

      var word = null;
      for (i in words) {
          if (words[i].translation == tran_word) {              
              word = words[i];
          }
      }

      if (word)
      {
         if (word.copyright_url){

           w2popup.open({
			   title: 'CopyRight Url',
               body: '<div class="w2ui-centered" style="overflow:hidden;padding:0px; margin:0px; height: 400; width:400;backgroundColor:#0000ff">\
							 				  <div style="margin-bottom:30px">\
                        	<span style="font-size:16px; font-weight:bold; color:#399aed;"> Credit </span>\
												</div>\
												<div style="width:300;min-height:100px;font-size:14px;color:#399aed;">\
													<p>' + word.copyright_url + '</p>\
												</div>\
                     </div>',
               width:  400,
               height: 400,
               color: '#000',
							 backgroundColor:'#ffffff',
               modal: false,
               style : '',
               showClose       : true
           });
         }
         else {
            alert("Empty Copyright URL");
         }
      }
}

function playagain(quiz_id)
{
    QuizInit(quiz_id);
}

function goLogin() {
      location.href = '/login'
}

function goProfile(e) {
  e.preventDefault();
  document.getElementById('logout-form').submit();
}  
</script>
<!--Google adwords Code-->
<?php echo html_entity_decode($blog->adwords_code)?>
<!--End Google adwords Code-->
<script id="dsq-count-scr" src="//imprevo.disqus.com/count.js" async></script>
</body>
</html>

