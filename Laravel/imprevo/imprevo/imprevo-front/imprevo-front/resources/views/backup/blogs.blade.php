<!DOCTYPE html>
<html lang="en" style="position: relative;min-height: 100%;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    {{--<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue-light_blue.min.css" />--}}
    <link rel="stylesheet" href="/css/material.min.css" />

    <link rel="stylesheet" href="/css/styles.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="{{ Config::get('RELATIVE_URL') }}/assets/vendor/pnotify/pnotify.custom.css" />
  <!--  <link rel="stylesheet" type="text/css" href="http://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.css" />-->
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.css" />
    <!-- Theme CSS -->
	<link rel="stylesheet" href="/assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
	<link rel="stylesheet" href="/assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="/assets/stylesheets/theme-custom.css">
	
	<style>

	.mdl-navigation__link{
		color:#fff;
		font-size: 18px;
	}
	.mdl-navigation__link:hover{
		color:#f29f00;
		text-decoration:none;
	}
	#login-btn {
		color:#fff;
		font-size: 18px;
		background: #f29f00;
		margin-left:150px;
		border:none;
		outline:none;
	}
	#login-btn:hover {
		background:#d98f00;
		
	}	
	.more-button{
		background-color:#399aed;
		padding-top:12px;
		color:#fff;
		height:45px;
		border:none;
		outline:none;
	}
	.more-button:hover{
		background-color:#2081d5;
		color:#fff;
	}
	.category-name-href{
		color:#909090;
		
	}
	.category-name-href:hover{
		color:#48555b;
		text-decoration:none;
	}
	.title-href{
		color:#399aed; 
		font-size:18px;
	}
	.title-href:hover{
		color:#3a6072;
		text-decoration:none;
	}
	
	#sub-button {
		margin-left:30px;
		background-color:#ffffff;
		color:#399aed;
	}
	
	#sub-button:hover {
		margin-left:30px;
		background-color:#dfe8ef;
		color:#399aed;
	}
	.social-link{
		color: #6c937b;
		margin-right:30px;
		
	}
	.social-link:hover{
		color: #e2ef1a;		
	}
	.link1{
		color:#66abae;
	}
	.link1:hover{
		color:#ffffff;
		text-decoration:none;
	}
	</style>
	
    <script src="/assets/vendor/modernizr/modernizr.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script src="/assets/vendor/pnotify/pnotify.custom.js"></script>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
	


<style>
 html {width:100%; overflow:visible; }
</style>	
</head>

<body style="height:100%;padding:0; margin: 0; overflow-x: hidden;  background-color:#ffffff">
<div id='Parent-Div-Background' style="overflow:visible;position:relative;width:100%; min-height:100%;">
    <header class="mdl-layout__header mdl-layout__header--transparent" style="width:100%; padding:0px;background: url('/images/blog_header.png') center/cover;height:250px">
        <div class="mdl-layout__header-row" style="margin-top:15px">
            <!-- Title -->
                <span class="mdl-layout-title">
                    <a href="/"><img src="/images/logo.png"></a>
                </span>
            <!-- Add spacer, to align navigation to the right -->
            <div class="mdl-layout-spacer"></div>
            <!-- Navigation -->
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="/tryit" style="">Try it</a>
                <a class="mdl-navigation__link" href="/courses" style="">Course</a>
                <a id="submenu" class="mdl-navigation__link" href="#" style="">Lessons</a>
                <a class="mdl-navigation__link" href="/freebies" style="">Freebies</a>
                @if (Auth::guest())
                <button id="login-btn" onclick="goLogin()" class="mb-xs mt-xs mr-xs btn btn-default" style="">
                    LOGIN
                </button>
                @else
                    <button id="login-btn" onclick="goProfile(event)" class="mb-xs mt-xs mr-xs btn btn-default" style="">
                        MY ACCOUNT
                    </button>
                @endif
            </nav>		
            <ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu"  for="submenu">
                @foreach(\App\Level::all() as $level)
                    <a class="mdl-menu__item" href="/lessons/{{$level->id}}">{{$level->title}}</a>
                @endforeach
            </ul>			
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
		<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center; padding-top:30px;"><span style="font-size:35px; color:#ffffff; text-align:center">IMPREVO blog</span></div>
    </header>

	<main class="mdl-layout__content" style="background-color:#ffffff;width:100%;margin:0px;padding:0px;padding-bottom:200px;">	
		<div class="portfolio-max-width" style="">
			<div id="blog-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="">							
			</div>
			<div class="mdl-cell mdl-cell mdl-cell--12-col" style="margin-top:10px;text-align:center">
				<div class="ajax-loading"  style="text-align: center;"><img src="/images/loading.gif" /></div>
			</div>
		</div>
	</main>

  <div id="div-footer" style="padding:0px; z-index:2;border:1px solid #eee;position: absolute;left: 0;bottom: 0px;height:200px;width: 100%;background:#006a6f;">
		  <div class="mdl-cell mdl-cell--12-col" style="padding:0px; padding-top:15px;width:100%; margin:0px;text-align:center; background-color:#399aed; height:80px;">
			<span style="font-size:25px; color:#ffffff"> Próbáld ki az IMPREVO-t még ma ingyen 
				<button id="sub-button" class="btn btn-lg btn-primary" style="">Próbáld ki ingyen!</button>
			</span>
		  </div>
		  <div class="mdl-cell mdl-cell--12-col" style="padding:0px;width:100%; margin:0px;text-align:center; background-color:#006a6f; height:80px;">
			<div class="mdl-cell mdl-cell--12-col" style="margin:0px;padding-top:50px; text-align:center;">
				<span style="color:#ffffff; font-size:23px">Értesülj legfrisebb leckéinkről,  ingyenes anyagainkról</span>
			</div>
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%;background-color:#006a6f; margin:0px;padding-top:15px; text-align:center; justify-content:center">
				<div class="mdl-cell mdl-cell--6-col" style="margin:0; padding:0;background-color:#006a6f;">
					<form action="pages-search-results.html" style="width:100%;margin:0;padding:0">						
						<div class="input-group" style="margin:0; padding:0;">
							<input type="text" id="email" name="email" class="form-control" placeholder="Email cím">
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button" style="background-color:#f29f00">FELIRATKOZOM</button>
							</span>
						</div>						
					</form>
				</div>
			</div>
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="width:100%;background-color:#006a6f; margin:0px;padding-top:15px; text-align:center; justify-content:center">
				<div class="mdl-grid mdl-cell mdl-cell--7-col" style="color:#ffffff;background-color:#006a6f; margin:0px;padding-top:15px; text-align:center; justify-content:center">
					<div class="mdl-grid mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
							<span>Rólunk</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Kik vagyunk?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Mi az IMPREVO?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Shop</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Kapcsolat</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Online Angol tanfolyam</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Hallás utáni szövegértés leckék</a>
						</div>	
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Angol oktatás cégeknek</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Meg lehet tanulni az IMPREVO-val angolul?</a>
						</div>					
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span>Ingyenes anyagok</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Igeidők összefoglaló táblázat</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">20 részes ingyenes e-mailes tanfolyam</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Ingyenes Kezdő Online Angol</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Zenés leckék</a>
						</div>	
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Nyelvtani anyagok</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Blog</a>
						</div>					
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span>Segítség</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">GYIK</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Syllabus</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Milyen a tanulási stílusod?</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">8+1 Tipp angoltanuláshoz</a>
						</div>						
					</div>
					<div class="mdl-cell mdl-cell--3-col" style="text-align:left">
						<div class="mdl-cell mdl-cell--12-col">
							<span>Ebookok</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Angol igeidők összefoglaló és példatár</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Angol levelek és e-mailek írása</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a class="link1" href="#" style="">Tanulj meg 6 hónap alatt angolul</a>
						</div>	
						<div class="mdl-cell mdl-cell--12-col" style="padding-top:50px;">
							<span>KÖVESS MINKET</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col" style="padding-top:20px; text-align: center;font-size:25px">
							<a class="social-link" href=""><i class="fa fa-facebook" style=""></i></a>
							<a class="social-link" href=""><i class="fa fa-twitter" style=""></i></a>
							<a class="social-link" href=""><i class="fa fa-google-plus" style=""></i></a>
							<a class="social-link" href="" style="margin-right:0px"><i class="fa fa-youtube-play" ></i></a>
						</div>
					</div>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--7-col" style="text-align:left;margin:0; padding:0; padding:0px 20px 10px 20px">
					<hr style="width:100%; height:1px; background-color:#66abae;"></hr>
					<span style="color:#7fbabd">
						<a href="/" style="margin-right:20px">
							<img src="/images/logo.png">					
						</a> 
						Copyright 2016. Imprevo. Minden jog fenntartva.					
					</span>					
					<a class="link1" href="#" style="margin-left:150px;">Adatvédelem</a>
					<a class="link1" href="#" style="margin-left:15px;">Felhasználás feltételei</a>
					<a class="link1" href="#" style="margin-left:15px;">Impresszum</a>					
				</div>				
			</div>
		 </div>
	</div>
</div>
<script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<script src="/assets/vendor/jquery/jquery.js"></script>
<script src="/assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/vendor/jquery-validation/jquery.validate.min.js"></script>



<script>
function goLogin() {
      location.href = '/login'
}

function goProfile(e) {
  e.preventDefault();
  document.getElementById('logout-form').submit();
}
</script>

<script>
var page = 1; //track user scroll as page number, right now page number is 1
load_more(page); //initial content load
var q_scroll = 0;
$('body').scroll(function() { //detect page scroll	
	console.log('scroll');
	scroll_div = document.getElementById('Parent-Div-Background');
	console.log($('#Parent-Div-Background').height());
	if (q_scroll == 0 && $('body').scrollTop()== 0) 
	{
		page++; //page number increment
        load_more(page); //load content 
	}
	q_scroll = (q_scroll + 1) % 20;
/*    if($('body').scrollTop()  >= $(document).height()) { //if user scrolled from top to bottom of the page
        page++; //page number increment
        load_more(page); //load content   
    }*/
});     

function load_more(page){
	@if ($blogcat)
		route_url = '/blog/category/' + "{{$blogcat->title}}?page=" + page;
	@else
		route_url = '/blog?page=' + page;
	@endif
	$.ajax(
        {
            url: route_url,
            type: "get",
            datatype: "html",
            beforeSend: function()
            {
                $('.ajax-loading').show();
            }
        })
        .done(function(data)
        {
            if(data.length == 0){
            console.log(data.length);
               
                //notify user if nothing to load
                $('.ajax-loading').html("No more blogs!");
                return;
            }
            $('.ajax-loading').hide(); //hide loading animation once data is received
            $("#blog-div").append(data); //append data into #results element          
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
              alert('No response from server');
        });
 }
</script>
</script>
<!--End Google adwords Code-->
</body>
</html>
