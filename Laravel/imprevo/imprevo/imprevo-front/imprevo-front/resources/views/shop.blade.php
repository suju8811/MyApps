@extends('layouts.app')
<style>
#banner-image {
	padding:10px;
	margin:0px 0px 0px 15px;
	width:110px;
	height:110px;
}
#brecumb_div{
	background: #9fc3ae;
	padding:5px 40px 5px 0px;
	padding-left:80px;
	font-size: 16px;
}

#addcart {
	background-color:#f29f00;
	width:160px;
	height:50px;
}

#addcart:hover {
	background-color:#d98f00;
	width:160px;
	height:50px;
}

#main-content{
	padding-bottom:170px;
}

#part1, #part2 {

}

tr{
	height: 40px;
}
tr > td {

}
tr > td > span {
	font-size: 14px;
	color:#354f56;
}

tr > td > img {
	width: 20px;
	height: 20px;
}


.part3-title {
	font-size: 18px;
	font-weight: bold;
	color: #000000;
}

.part-box {
	max-height:350px;
	min-height:346px;
}

.part-box > div:nth-child(2) {
	padding:20px;
	padding-left:50px;
}

.img-box-product {
	width:80%;
}

#main-widget {
	width:70%;
}
#part2-div-part3-price-div {
  margin-top:100px;
}

#part2-div-part3-addcart-div {
	margin-top:40px;
}

#part3 > div:nth-child(3n+1) {
	padding:0px;
	padding-right:20px;
}

#part3 > div:nth-child(3n+2) {
	padding:0px;
	padding:0px 10px 0px 10px;
}

#part3 > div:nth-child(3n+3) {
	padding:0px;
	padding-left:20px;
}

@media (max-width: 1650px) {
	#main-widget {
		width:90%;
	}
}
@media (max-width: 1270px) {
	#main-widget {
		width:100%;
	}
}
@media (max-width: 840px) {
	.img-box-product {
		width:40%;
	}
	#main-content{
		padding-bottom:250px;
	}
	.part-box > div:nth-child(2) {
		padding:10px;
	}
	#part2-div-part3-price-div {
		margin-top:20px;
	}
	#part2-div-part3-addcart-div {
		margin-top:20px;

	}
	.part-box {
		max-height:1200px;
	}
}


@media (max-width: 400px) {
	#banner-image {
		padding:0px;
		margin:15px 0px 0px 10px;
		width:60px;
		height:60px;
	}

	#brecumb_div{
		padding-left:25px;
	}

	#main-content{
		padding-bottom:250px;
	}

	#part1 > div:nth-child(2) {
		padding-left:0px;
	}

	.table-div {
		text-align:center;
		justify-content:center;
	}

	#part3 > div {
		padding:0 !important;
		margin-top:10px !important;
	}

	.part-box {
		max-height:900px;
	}

	.product-title {
		text-align:center;
	}
	.product-title a {
		font-size: 25px !important;
	}
}
</style>
@php
	function getFeatures($product) {
		$features_array = [];
		if ($product->features)
			$features_array = explode ('||', $product->features);
		return $features_array;
	}
@endphp
@section('content')
<div id="main-content" class="mdl-layout__content" style="width:100%;background-color:#ebf3ef;">
    <div id="brecumb_div" style="">
        <span><a href="/" style="color:#d5e7dc;line-height: 40px;text-decoration: none">{!! trans('home.imprevo') !!}</a></span>
        <span style="padding:0px 5px 0px 5px;color:#d5e7dc;line-height: 40px">/</span>
        <span style="color:#fff;line-height: 40px">shop</span>
    </div>
    <div id="main-widget" class="mdl-grid portfolio-max-width" style="max-width:100%;background-color:#ebf3ef">
		<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="color:#3b4a51; font-size:40px;width:100%;background:#ebf3ef;padding:0; margin:0; margin-top:40px; margin-bottom:25px; height:22px">
			IMPREVO Shop
		</div>
		@if ($product_sale)
		@foreach ($product_sale as $product)
		<div id="part1" class="part-box mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="background:#ffffff;width:100%; padding:0; margin:0;  margin-bottom:20px">
				<div class="mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="position:relative;text-align:center;background: url('/images/shop_pic1_back.png') center/cover; padding:30px; margin:0">
					@if (count($product_sale) == 1)
					<img src="/images/shop_part1_redstripe.png" style="position:absolute; top:0; left:0; height:30%"/>
					@endif
					@if ($product->product_image)
					<img class="img-box-product" src="{{$product->product_image}}" style=""/>
					@else
						<span> Nothing Image </span>
					@endif
				</div>
				<div class="mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="">
						<div class="product-title mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0; margin-top:10px">
							<a href="/sales/course/{{$product->course_id}}" style="text-decoration:none;color:#354f56; padding:0; margin:0; font-size:40px; line-height:40px">{{$product->title}}</a>
						</div>
						<div class="table-div mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
							<table>
								<tbody>
								    @foreach (getFeatures($product) as $feacture)
									<tr>
										<td><img src="/images/shop_tick.png" style=""/></td>
										<td style="padding-left:10px;"><span style="">{{$feacture}}</span></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
				</div>
				<div class="mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone " style="vertical-align:middle; padding:10px">
					<div id="price-div-{{$product->id}}" class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:10px; margin:0;text-align:center;">
						<div class="mdl-cell--12-col" style="text-align:center; padding:10; ">
							<span style="font-size:20px;text-decoration:line-through">23.990Ft</span>
						</div>
						<div class="mdl-cell--12-col" style= "padding:10;text-align:center">
							<span style="font-size:40px">19.990Ft</span>
						</div>
						<div class="mdl-cell--12-col" style="padding:10;text-align:center">
							<span><span style="font-size:16px;color:#f29f00;">10 nap 8 óra 30 pec </span>múlva lejár<br/><span style="color:#f29f00"> 79 akciós csomag </span>elérhető</span>
						</div>
					</div>
					<div class="mdl-cell--12-col" style="padding:10;text-align:center">
						<button id="addcart" onclick="location.href = '/shoppingcart?products={{$product->id}}'" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored"><i aria-hidden="true" class="fa fa-shopping-cart" style="padding-right:10px"></i>KOSÁRBA</button>
					</div>
					<div class="mdl-cell--12-col" style="padding:10;text-align:center">
						<span style="color:#bfbfbf"> * A feltüntetett ár bruttó ár,<br/> a 27% ÁFA-t tartalmazza!</span>
					</div>
				</div>
		</div>
		@endforeach
		@endif
		@if ($product_featured)
		@foreach ($product_featured as $product)
		<div id="part2" class="part-box mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="background:url('/images/shop_part2_back.png') center/cover; width:100%; padding:0; margin:0;">
			<div class="mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="vertical-align:middle;text-align:center;margin:0;padding:30px;">
				@if ($product->product_image)
				<img class="img-box-product" src="{{$product->product_image}}" style=""/>
				@else
					<span> Nothing Image </span>
				@endif
			</div>

			<div class="mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="">
					<div class="product-title mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0; margin-top:10px">
						<a href="/sales/course/{{$product->course_id}}" style="text-decoration:none; color:#354f56; padding:0; margin:0; font-size:40px; line-height:40px">{{$product->title}}</a>
					</div>
					<div class="table-div mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
						<table>
							<tbody>
								@foreach (getFeatures($product) as $feature)
									<tr>
										<td><img src="/images/shop_part2_tick.png" style=""/></td>
										<td style="	padding-left:10px;"><span style="">{{$feature}}</span></td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
			</div>
			@endforeach
			<div class="mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:10px">
				<div id="part2-div-part3-price-div" class="mdl-cell--12-col" style= "text-align:center">
					<span style="font-size:40px; color:#ffffff">
						@if ($shoppingcartsetting['currency'] == 'USD')
							$<?php echo number_format($product->regular_price);?>
						@else
							<?php echo number_format($product->regular_price);?> Ft
						@endif
					</span>
				</div>
				<div id="part2-div-part3-addcart-div" class="mdl-cell--12-col" style="text-align:center;">
					<button id="addcart" onclick="location.href = '/shoppingcart?products={{$product->id}}'" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored"><i aria-hidden="true" class="fa fa-shopping-cart" style="padding-right:10px"></i>KOSÁRBA</button>
				</div>
			</div>
		</div>
		@endif
		@if ($products)
		<div id="part3" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="width:100%; padding:0; margin:0; margin-top:20px">
			@foreach ($products as $product)
			<div class="mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:left; margin:0;">
				<div class="mdl-card mdl-grid mdl-cell mdl-cell--12-col mdl-shadow--4dp" style="width:100%; margin:0px;  background-color:#ffffff">
					<div class="mdl-cell mdl-cell--8-col mdl-cell--6-col-tablet mdl-cell--4-col-phone">
						<div class="mdl-cell mdl-cell--12-col">
							<a href="/sales/course/{{$product->course_id}}" class="part3-title" style="text-decoration:none">{{$product->title}}</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<span style="font-size:18px; color:#f29f00">
								@if ($shoppingcartsetting['currency'] == 'USD')
									$<?php echo number_format($product->price);?>
								@else
									<?php echo number_format($product->price);?> Ft
								@endif
							</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col">
							<a href="/shoppingcart?products={{$product->id}}" class="btn" style="height:50px; width:150px; background-color:#eeeeee; text-align:center; padding:15px;color:#399aed">
								<img src="/images/shop_part3_cart_icon.png" style="width:20px;height:20px" /> KOSÁRBA</a>
						</div>
					</div>
					<div class="mdl-cell mdl-cell--4-col mdl-cell--2-col-tablet mdl-cell--hide-phone" style="padding:10px">
						<img src="{{$product->product_image}}"  style="height:100%;max-height:98.2px" />
					</div>
				</div>
			</div>
			@endforeach
		</div>
		@endif
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
@if ($product_sale)
@foreach ($product_sale as $product)
var html = '';
var left_days = null;
var left_hours = null;
var left_mins = null;
var remain_orders = null;
var distance = 0;

     @if ($product->sale_price_end_date)
				var t = "{{$product->sale_price_end_date}}".split(/[- :]/);
				var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
				var end_time = new Date(d).getTime();

				var strtime = <?php echo '"'.date('Y-m-d H:i:s').'"' ?>;
				t = strtime.split(/[- :]/);
				var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
				var timeServer = new Date(d).getTime();

        distance = (end_time - timeServer) / 1000 ;

        if (distance > 0)
        {
      		   left_mins = Math.floor((distance / 60) % 60);
      		   left_hours = Math.floor((distance / (60 * 60)) % 24);
      		   left_days = Math.floor(distance / (60 * 60 * 24));
        }
     @endif

     @if ($product->sale_price_orders_num)
        var total = parseInt("{{$product->sale_price_orders_num}}");
        var current = parseInt("{{$product->sale_price_orders_current}}");
        remain_orders = total - current;
     @endif
     console.log(distance, remain_orders);
     if (distance > 0 && remain_orders > 0)
     {
	   var str = '';
	   str = left_days + ' NAP ' + left_hours +  ' ÓRA ' + left_mins + ' PERC';
	   if (left_days == 0)
	   {
		   str = left_hours +  ' ÓRA ' + left_mins + ' PERC';
	   }
	   else if (left_days == 0 && left_hours == 0)
	   {
		   str = left_mins + ' PERC';
	   }

       @if ($shoppingcartsetting['currency'] == "USD")
          html = '<div class="mdl-cell mdl-cell mdl-cell--12-col">' +
                    '<span style="color:#0c484c;font-size:20px; text-decoration: line-through; padding:0;">' +
                        '$<?php echo number_format($product->regular_price) ?>' +
                    '</span>' +
                  '</div>';


          html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:10px">' +
                    '<span style="color:#000000;font-size:40px;  padding:0;">' +
                        '$<?php echo number_format($product->sale_price) ?>' +
                    '</span>' +
                '</div>';
          html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px">' +
                          '<span style="color:#000000;font-size:16px; padding:0;">' +
                              '<span style="color:#f29f00;font-weight:bold;">' + str + ' </span>MÚLVA LEJÁR' +
                          '</span>'
                  '</div>';

          html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col">' +
                      '<span style="color:#000000;font-size:16px; font-weight:bold; padding:0;">' +
                          'MÉG' + '<span style="color:#f29f00">' + ' ' + remain_orders + ' AKCIÓS CSOMAG </span> ELÉRHETŐ!' +
                      '</span>' +
                '</div>';
       @else
       html = '<div class="mdl-cell mdl-cell mdl-cell--12-col">' +
                 '<span style="color:#0c484c;font-size:20px; text-decoration: line-through; padding:0;">' +
                     '<?php echo number_format($product->regular_price) ?> <span style="font-size:12px;vertical-align: super;">Ft</span>' +
                 '</span>' +
               '</div>';


       html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:10px">' +
                 '<span style="color:#000000;font-size:40px;">' +
                     '<?php echo number_format($product->sale_price) ?> <span style="font-size:20px;vertical-align: super;">Ft</span>' +
                 '</span>' +
             '</div>';

       html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px">' +
                       '<span style="color:#000000;font-size:16px; padding:0; font-weight:bold;">' +
                           '<span style="color:#f29f00">' + str + ' </span>MÚLVA LEJÁR' +
                       '</span>'
               '</div>';

       html = html + '<div class="mdl-cell mdl-cell mdl-cell--12-col">' +
                   '<span style="color:#000000;font-size:16px; padding:0; font-weight:bold;">' +
                       'MÉG' + '<span style="color:#f29f00">' + ' ' + remain_orders + ' AKCIÓS CSOMAG </span> ELÉRHETŐ!' +
                   '</span>' +
             '</div>';
       @endif
     } 
	 document.getElementById('price-div-'+"{{$product->id}}").innerHTML = html;
	 @endforeach
	 @endif
});
</script>

@endsection
