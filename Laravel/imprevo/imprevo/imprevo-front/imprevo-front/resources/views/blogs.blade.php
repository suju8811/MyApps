<!DOCTYPE html>
<html lang="en" style="position: relative;min-height: 100%;">
<head>
  <!--GaCode-->
@if (isset($settings))
  <?php echo html_entity_decode($settings['gaCode'])?>
@endif
<!--End GaCode-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

	@if (isset($settings))
    <meta name="description" content="{{$settings['siteDesc']}}">
    <meta name="keywords" content="{{$settings['keywords']}}">
    <title>{{$settings['siteTitle']}}</title>
	@else
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title>Imprevo</title>
	@endif

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="/css/material.min.css" />
    <link rel="stylesheet" href="/css/styles.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="{{ Config::get('RELATIVE_URL') }}/assets/vendor/pnotify/pnotify.custom.css" />
    <link rel="stylesheet" type="text/css" href="https://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.css" />
	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- Specific Page Vendor CSS -->
	<link rel="stylesheet" href="/assets/vendor/owl.carousel/assets/owl.carousel.css" />
	<link rel="stylesheet" href="/assets/vendor/owl.carousel/assets/owl.theme.default.css" />
	<!-- Theme CSS -->
	<link rel="stylesheet" href="/assets/stylesheets/theme-fix.css" />

	<!-- Skin CSS -->
	<link rel="stylesheet" href="/assets/stylesheets/skins/default.css" />

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="/assets/stylesheets/theme-custom.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <script src="/assets/vendor/modernizr/modernizr.js"></script>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.js"></script>
    <script src="/assets/vendor/pnotify/pnotify.custom.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="/assets/vendor/nanoscroller/nanoscroller.js"></script>
	<script src="/assets/vendor/owl.carousel/owl.carousel.js"></script>

	<script src="/assets/javascripts/theme.js"></script>

	<!-- Theme Custom -->
	<script src="/assets/javascripts/theme.custom.js"></script>

	<!-- Theme Initialization Files -->
	<script src="/assets/javascripts/theme.init.js"></script>

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <!--Facebook Pixel Code-->



    <!--End Facebook Pixel Code-->
</head>

<style>
	html, body {
		font-family: 'Roboto', sans-serif;
	}
	.more-button{
		background-color:#399aed;
		text-decoration:none;
		padding:10px 15px 10px 15px;
		color:#fff;
		height:40px;
		border:none;
		border-radius:0px;
		font-size:14px;
	}

	.more-button:hover{
		background-color:#2081d5;
		color:#fff;
	}

	.category-name-href{
		color:#909090;

	}
	.category-name-href:hover{
		color:#48555b;
		text-decoration:none;
	}
	.title-href{
		color:#399aed;
		font-size:18px;
	}
	.title-href:hover{
		color:#3a6072;
		text-decoration:none;
	}

	#sub-button {
		margin-left:30px;
		background-color:#ffffff;
		color:#399aed;
	}

	#sub-button:hover {
		margin-left:30px;
		background-color:#dfe8ef;
		color:#399aed;
	}
	.socialbuttons .facebook{
		width:40px;
		height:40px;
		background:url('/images/social-facebook-icon.png') center/cover;
	}
	.socialbuttons .facebook:hover{
		background:url('/images/social-facebook-icon-hover.png') center/cover;
	}
	.socialbuttons .twitter{
		width:40px;
		height:40px;
		background:url('/images/social-twitter-icon.png') center/cover;
	}
	.socialbuttons .twitter:hover{
		background:url('/images/social-twitter-icon-hover.png') center/cover;
	}
	.socialbuttons .goggle{
		width:40px;
		height:40px;
		background:url('/images/social-goggle-icon.png') center/cover;
	}
	.socialbuttons .goggle:hover{
		background:url('/images/social-goggle-icon-hover.png') center/cover;
	}
	.socialbuttons .insta{
		width:40px;
		height:40px;
		background:url('/images/social-youtube-icon.png') center/cover;
	}
	.socialbuttons .insta:hover{
		background:url('/images/social-youtube-icon-hover.png') center/cover;
	}
	.link1{
		color:#66abae;
	}
	.link1:hover{
		color:#ffffff;
		text-decoration:none;
	}
	#try-it-button-second{
		color:#399aed;
		background-color:#ffffff;
		width:200px;
	}

	#try-it-button-second:hover{
		color:#399aed;
		background-color:#dfe8ef;
		font-weight:bold;
	}
	#footer-div-bottom {
		width: 60% !important;
	}
	.phone-footer-accordion {
		width: 380px;
		border-radius: 5px;
		overflow: hidden;
		margin: auto;
	}

	.phone-footer-accordion .item .heading {
		height: 50px;
		line-height: 50px;
		font-size: 15px;
		cursor: pointer;
		color: #ffffff;
		padding-left: 15px;
		background: #006469 url('/images/phone_footer_accordion.png') no-repeat;
		background-position: right 20px top -95px;
		border-bottom: 1px solid #4d9a9d;
		box-sizing: border-box;
		text-align:left;
		}

		.phone-footer-accordion .item.open .heading,
		.phone-footer-accordion .item:last-child .heading { border: 0; }

		.phone-footer-accordion .item.open .heading { background-position: right 20px top -5px; }

		.phone-footer-accordion .item .content {
		display: none;
		padding: 15px;
		background: #006469;
		font-size: 14px;
	}

@media (max-width: 1777px)
{
	#footer-div-bottom {
		width: 70% !important;
	}
}

@media (max-width: 1500px) {

	#footer-div-bottom {
		width: 80% !important;
	}
}
@media (max-width: 1386px)
{
	#footer-div-bottom {
		width: 87% !important;
	}
}
@media (max-width: 769px) {

}
</style>
<body style="padding:0px; height:100%;margin: 0;overflow-x: hidden">
  @if (isset($settings))
    <?php echo html_entity_decode($settings['gaBodyCode'])?>
  @endif
<div id='Parent-Div-Background'style="background-color:#ffffff;padding:0; margin:0px;position:relative;width:100%;min-height:100%;">
<style>
html, body {
	font-family: 'Roboto', sans-serif;
}


</style>
<main class="mdl-layout__content" style="background-color:#ffffff;width:100%;margin:0px;padding:0px;">
    @include('layouts.header')
    @include('layouts.cookie')
  <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;margin:0;padding:0px;width:100%; min-height:350px; background:url('/images/blog-cover-image.png') center/cover no-repeat; ">

  <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing" style="justify-content:center;padding-top:50px">
    <span style="color:#ffffff; font-size:40px">IMPREVO BLOG</span>
  </div>
</div>
		<div class="portfolio-max-width" style="">
			<div id="blog-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="">
			</div>
			<div class="mdl-cell mdl-cell mdl-cell--12-col" style="margin-top:10px;text-align:center">
				<div class="ajax-loading"  style="text-align: center;"><img src="/images/loading.gif" /></div>
			</div>
		</div>
	</main>
			<div id="section4-phone" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="margin:0;width:100%;background-color:#399aed;padding:40px 0px 40px 0px;justify-content:center">
				<span style="color:#ffffff; font-size:30px; padding-top:5px;padding-right:20px">Próbáld ki az IMPREVO-t még ma ingyen
				</span>
				<button id="try-it-button-second" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
					PRÓBÁLD KI INGYEN!
				</button>
			</div>
			<div id="section4" class="mdl-grid mdl-cell mdl-cell--hide-desktop mdl-cell--4-col-phone" style="margin:0;width:100%;background-color:#399aed;padding:40px 0px 40px 0px;justify-content:center">

					<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center; ">
						<span style="color:#ffffff; font-size:30px;">Próbáld ki az IMPREVO-t még ma ingyen
						</span>
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center; ">
						<button id="try-it-button-second" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
							PRÓBÁLD KI INGYEN!
						</button>
					</div>

			</div>
			@include('layouts.footer')
	</div>
</div>
<script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<script src="/assets/vendor/jquery/jquery.js"></script>
<script src="/assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/vendor/jquery-validation/jquery.validate.min.js"></script>



<script>


	$('.phone-footer-accordion .item .heading').click(function() {

		var a = $(this).closest('.item');
		var b = $(a).hasClass('open');
		var c = $(a).closest('.accordion').find('.open');

		if(b != true) {
			$(c).find('.content').slideUp(200);
			$(c).removeClass('open');
		}

		$(a).toggleClass('open');
		$(a).find('.content').slideToggle(200);

	});
</script>

<script>
var page = 2; //track user scroll as page number, right now page number is 1
var page_done = 0;

function Logout(e) {
    e.preventDefault();
    document.getElementById('logout-form').submit();
}
$(document).ready(function(){
  $('body').scrollTop(0);
  window.scrollTo(0, 0);
  document.documentElement.scrollTo(0,0);
  document.body.scrollTo(0,0);
  document.documentElement.scrollTop = 0;
  document.body.scrollTop = 0;
  load_more(1); //initial content load
  var oldvalue = 0;
  window.onscroll = function ()  { //detect page scroll

  	height = $('#Parent-Div-Background').height();

  	scroll = document.documentElement.scrollTop || document.body.scrollTop;
    position = $(window).height() + scroll;

    console.log('scroll');

  	if ((height - 300 <= ($(window).height() + scroll)) && (oldvalue < position) && !page_done)
  	{
  		    page++; //page number increment
          load_more(page); //load content
  	}

    oldvalue = position;

    home_header = document.getElementById("header-div");

    if (home_header){
      console.log('agweg');
      sticky = $('#header-div');
      scroll = document.documentElement.scrollTop || document.body.scrollTop;

      if (scroll >= 80) {
        sticky.addClass('fixed');
      }
      else{
        sticky.removeClass('fixed');
      }
    }

  };
})
function load_more(page){
  // /console.log('page', page);
	@if ($blogcat)
		route_url = '/blog/category/' + "{{$blogcat->title}}?page=" + page;
	@else
		route_url = '/blog?page=' + page;
	@endif
	$.ajax(
        {
            url: route_url,
            type: "get",
            datatype: "html",
            beforeSend: function()
            {
                $('.ajax-loading').show();
            }
        })
        .done(function(data)
        {
            if(data.length == 0){
            //console.log(data.length);
                //notify user if nothing to load
                $('.ajax-loading').html("{!! trans('blog.span7') !!}");
                return;
            }
            $('.ajax-loading').hide(); //hide loading animation once data is received
            page_done = data[1];
            $("#blog-div").html(data[0]); //append data into #results element
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
              alert('No response from server');
        });
 }
	function isMobileDevice() {
		return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
	};

	console.log(isMobileDevice());
	if (!isMobileDevice()){
		$('ul.nav li.dropdown').hover(function() {
			$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
		}, function() {
			$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
		});
	}

	$('#my-account-button-logged').hover(function() {
		$('#dropdown3').stop(true, true).delay(100).fadeIn(500);
	}, function() {
		$('#dropdown3').stop(true, true).delay(100).fadeOut(500);
	});
</script>
<script>
var home_header = document.getElementById("header-div-homepage");

//-------home page support --------
if (home_header){
	document.getElementById('header-span1').innerHTML = '{!! trans("headerfooter.header-span1") !!}';
	document.getElementById('header-span2').innerHTML = '{!! trans("headerfooter.header-span2") !!}';
	//document.getElementById('header-span3').innerHTML = '{!! trans("headerfooter.header-span3") !!}';
	//document.getElementById('header-span4').innerHTML = '{!! trans("headerfooter.header-span4") !!}';
	document.getElementById('header-span5').innerHTML = '{!! trans("headerfooter.header-span5") !!}';
	document.getElementById('header-span6').innerHTML = '{!! trans("headerfooter.header-span6") !!}';
	document.getElementById('header-span7').innerHTML = '{!! trans("headerfooter.header-span7") !!}';
	document.getElementById('header-span8').innerHTML = '{!! trans("headerfooter.header-span8") !!}';
	document.getElementById('header-span9').innerHTML = '{!! trans("headerfooter.header-span9") !!}';
	document.getElementById('header-span10').innerHTML = '{!! trans("headerfooter.header-span10") !!}';
	nav_right_ul = document.getElementById('nav-right-ul');
	$('.phone-footer-accordion .item .heading').click(function() {
		var a = $(this).closest('.item');
		var b = $(a).hasClass('open');
		var c = $(a).closest('.accordion').find('.open');

		if(b != true) {
			$(c).find('.content').slideUp(200);
			$(c).removeClass('open');
		}

		$(a).toggleClass('open');
		$(a).find('.content').slideToggle(200);

	});

	if (nav_right_ul)
	{
		var html = '';
		@if (Auth::guest())
			html = html + '<li  style="">';
			html = html + '<button id="my-account-button" onclick="goLogin(event)" class="dropdown-toggle mdl-button mdl-js-button mdl-button--raised mdl-button--colored" data-toggle="dropdown" style="border-radius:2px;outline:none;border:none;">' +
						'{!! trans("headerfooter.header-button1") !!}'	+
					'</button></li>';
		@else
			html = html + '<li class="dropdown" style="">';
			html = 	html + '<button class="dropdown-toggle" id="my-account-button-logged" onclick="goDashboard(event)" class="my-nav-button dropdown-toggle" data-toggle="dropdown" style="border-radius:2px;outline:none;border:none;">' +
						'{!! trans("headerfooter.header-button2") !!}' +
					'</button>';
			html = html + '<ul id="dropdown3" class="dropdown-menu">' +
						'<li><a class="a-submenu" href="/profile">{!! trans("headerfooter.header-dropdown1") !!}</a></li>' +
						'<li><a class="a-submenu" href="" onclick="Logout(event)">{!! trans("headerfooter.header-dropdown2") !!}</a></li>' +
				'</ul></li>';
		@endif


		nav_right_ul.innerHTML = html;
	}

	try_it_button = document.getElementById('try-it-button');

	if (try_it_button)
	{
		@if (Auth::guest())
			try_it_button.style.backgroundColor = '#399aed';
		@else
			try_it_button.style.backgroundColor = '#f29f00';
		@endif
	}
}

function goLogin() {
      location.href = '/login'
}

function goProfile(e) {
  e.preventDefault();
  location.href = '/profile'
}

function goDashboard(e) {
  location.href = '/home'
}

function Logout(e) {
    e.preventDefault();
    document.getElementById('logout-form').submit();
}



</script>
<!--End Google adwords Code-->
</body>
</html>
