@extends('layouts.app')
<style>
#know_more_button {
	background:#399aed;
	color:white;
	font-size: 14px;
	width:100%;
}
#know_more_button:hover {
	background:#2081d5;

}
.custom-row{
	background:#ffffff;

}
.custom-row:hover{
	background:#f0f0f0;
}

/*#know_more_button:hover span{
	display:none;
}

#know_more_button:hover:after{
	content: "KIPRÓBÁLOM";
}*/

#continue-button{
	width:100%;
	color:#ffffff;
	background-color:#399aed;
}

#continue-button:hover{
	color:#ffffff;
	background-color:#2081d5;
	font-weight:bold;
}


.addcart
{
	margin-right:10px !important;
	height:40px !important;
	padding:0px 20px 0px 20px !important;
	background-color:#eeeeee !important;
	color:#399aed !important;
}

.addcart:hover
{
	background-color:#399aed !important;
	color:#ffffff !important;
}

#register-products-span-div{

}
#register-shopping-img-div{
	padding:30px;
}

#register-remove-div {
	border-left:1px solid #eee;
	margin:0;
	padding:0;
	justify-content:center;
}

#brecumb-div{
	padding:5px 40px 5px 50px;
}

#top-card-part-2{
	padding:40px 0px 70px 0px;
}

#register-div1{
	padding:40px 0px 70px 0px;
}

#main-content{
	padding-bottom:150px;
}

@media (max-width: 845px) {
	#register-products-span-div{
		text-align:center;
	}

	#register-shopping-img-div{
		padding:5px;
	}

	#register-remove-div {
		border:none;
	}

}

@media (max-width: 480px) {
	.addcart
	{
		margin:0px !important;
		padding:0px !important;
		height:40px !important;
		width:45px !important;
		background-color:#eeeeee !important;
		color:#399aed !important;
	}

	#brecumb-div{
		padding:5px 40px 5px 0px;
	}

	#top-card-part-2,#register-div1{
		padding:20px 0px 10px 0px;
	}

	#main-content{
		padding-bottom:250px;
	}

	.mdl-data-table {
		table-layout:fixed;
	}

	#table td{
		width: 100%;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		-o-text-overflow: ellipsis;
	}
}
</style>
@section('content')
<div id="main-content" class="mdl-layout__content" style="width:100%;background:#ebf3ef">
    <div id="brecumb-div" style="background: #9fc3ae;font-size: 16px;">
        <span><a href="/" style="margin-left:20px;color:#d5e7dc;line-height: 40px;text-decoration: none">{!! trans('home.imprevo') !!}</a></span>
        <span style="color:#d5e7dc;line-height: 40px">/</span>
        <span style="color:#fff;line-height: 40px;">{!! trans('shoppingcart.shop') !!}</span>
    </div>
    <div class="mdl-grid portfolio-max-width" style="min-height:700px">
        <div id="top-banner" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-grid--no-spacing" style="min-height:220px">
			<div id="register-products-span-div" class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:20px 0px 20px 0">
				<span style="font-size:30px;">{!! trans('shoppingcart.shoppingcart-span1') !!}</span>
			</div>
			<div id="productlists" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing">

			</div>
			<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="padding:20px 0px 0px 0">
				<div class="mdl-cell mdl-cell mdl-cell--7-col mdl-cell--4-col-tablet mdl-cell--hide-phone">
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--2-col-phone">
					<div class="mdl-cell mdl-cell mdl-cell--12-col">
						<span style="font-size:20px; font-weight:bold">{!! trans('shoppingcart.total') !!}</span>
					</div>
					<div class="mdl-cell mdl-cell mdl-cell--12-col">
						<span id="total-tag" style="font-size:20px; font-weight:bold"></span>
					</div>
				</div>
				<div class="mdl-cell mdl-cell mdl-cell--3-col mdl-cell--2-col-tablet mdl-cell--2-col-phone" style="">
					<button id="continue-button" onclick="oncontinue()"class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" onmouseover="" onmouseout="">
						{!! trans('shoppingcart.continue') !!}
					</button>
				</div>
			</div>
        </div>
		<div id="products-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="padding-top:10px;">    <!---->
			<div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="padding:20px 0px 20px 0">
				<span style="color:#acadad;font-size:16px">{!! trans('shoppingcart.shoppingcart-span2') !!}</span>
			</div>
			<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing mdl-cell--4-col-tablet"
			style="padding:0px;width:100%;">
			<table id="table" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" style="width:100%;background-color:#ffffff">
						<tbody id="related_products">
							@foreach($products as $item)
							<tr id="tr-{{$item->id}}" class="mdl-cell--hide-phone" style="border-bottom: 1px solid #eee;">
								<td style="padding:20px"><img src="{{$item->product_image}}" style="width:80px"></td>
								<td>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="font-weight:bold">{{$item->title}}</span>
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--12-col">
										<span style="">{{$item->description}}</span>
									</div>
								</td>
								<td>
									<div class="mdl-cell mdl-cell--12-col" style="padding-top:20px">
										<span style="">{!! trans('shoppingcart.shoppingcart-span3') !!}</span>
									</div>
									<div class="mdl-cell mdl-cell--12-col">
										<span style="font-size:20px;@if (!is_null($item->sale_price)) text-decoration:line-through;@endif">
											@if ($shoppingcartsetting['currency'] == "USD")
												$<?php echo number_format($item->regular_price) ?>
											@else
												<?php echo number_format($item->regular_price) ?> Ft
											@endif
										</span>
									</div>
								</td>
								<td>
									@if (!is_null($item->sale_price))
									<div class="mdl-cell mdl-cell--12-col" style="text-align:left;padding-top:20px">
										<span style="">{!! trans('shoppingcart.shoppingcart-span4') !!} <img src="/images/shoppingcart-tooltip.png" style="padding-left:20px"></span>
									</div>
									<div class="mdl-cell mdl-cell--12-col" style="text-align:left">
										<span style="font-size:20px;">
										@if ($shoppingcartsetting['currency'] == "USD")
											$<?php echo number_format($item->sale_price) ?>
										@else
											<?php echo number_format($item->sale_price) ?> Ft
										@endif
										</span>
									</div>
									@endif
								</td>
								<td>
									<button id="addcart-{{$item->id}}" onclick="AddCart({{$item->id}})" class="addcart mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
										<span><i aria-hidden="true" class="fa fa-shopping-cart" style="padding-right:10px"></i>{!! trans('shoppingcart.shoppingcart-span5') !!}</span>
									</button>
								</td>
							</tr>
							<tr id="tr-{{$item->id}}-phone" class="mdl-cell--hide-desktop mdl-cell--hide-tablet" style="padding:5px;width:100%;border-bottom: 1px solid #eee;">
								<td style="padding-left:5px; text-align:left; padding-right:0px;">
									<div>
										<span style="font-weight:bold">{{$item->title}}</span>
									</div>
									<div>
										<span style="">{{$item->description}}</span>
									</div>

									<div class="mdl-grid mdl-cell mdl-cell--4-col-phone mdl-grid--no-spacing" style="width:100%;padding:0;margin:0;">
										<div class="mdl-cell mdl-cell mdl-cell--2-col-phone">
											<div style="padding:20px 0px 0px 5px;text-align:left;">
												<span style="">{!! trans('shoppingcart.shoppingcart-span3') !!}</span>
											</div>
											<div style="padding:10px 0px 0px 5px;text-align:left;">
												<span style="font-size:12px;@if (!is_null($item->sale_price)) text-decoration:line-through;@endif">
													@if ($shoppingcartsetting['currency'] == "USD")
														$<?php echo number_format($item->regular_price) ?>
													@else
														<?php echo number_format($item->regular_price) ?> Ft
													@endif
												</span>
											</div>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--2-col-phone">
											@if (!is_null($item->sale_price))
												<div style="padding:20px 0px 0px 5px;text-align:left;">
													<span style="">{!! trans('shoppingcart.shoppingcart-span4') !!} <img src="/images/shoppingcart-tooltip.png" style="padding-left:20px"></span>
												</div>
												<div style="padding:10px 0px 0px 5px;text-align:left">
													<span style="font-size:12px;">
													@if ($shoppingcartsetting['currency'] == "USD")
														$<?php echo number_format($item->sale_price) ?>
													@else
														<?php echo number_format($item->sale_price) ?> Ft
													@endif
													</span>
												</div>
											@endif
										</div>
									</div>
								</td>
								<td style="width:95px">
									<div class="mdl-cell mdl-cell mdl-cell--4-col-phone" style="padding:10px;">
										<img src="{{$item->product_image}}" style="width:50px">
									</div>
									<div class="mdl-cell mdl-cell mdl-cell--4-col-phone" style="padding:10px;">
										<button id="addcart-phone-{{$item->id}}" onclick="AddCart({{$item->id}})" class="addcart mdl-button--raised" style="border:none;outline:none">
											<span><i aria-hidden="true" class="fa fa-shopping-cart" style="font-size:20px"></i></span>
										</button>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
			</div>
		</div>

    </div>
</div>

<script type="text/javascript">
 var saleproducts = [];


 @if ($productsales)
	 @foreach ($productsales as $product)
		saleproducts.push(parseInt("{{$product->id}}"));
	 @endforeach
 @endif



 RefreshPage();

 function RefreshPage()
 {
	//productlists
	var html = '';
	var total = 0;
	for (index in saleproducts)
	{
		var product = {};
		@foreach ($productsales as $item)
			if ("{{$item->id}}" == saleproducts[index])
			{
				product.id = "{{$item->id}}".trim();
				product.title = "{{$item->title}}".trim();
				product.sale_price = "{{$item->sale_price}}";
				product.regular_price = "{{$item->regular_price}}";
				product.description = "{{$item->description}}";
				product.product_image = "{{$item->product_image}}";
			}
		@endforeach


		@foreach ($products as $item)
			document.getElementById('tr-' + "{{$item->id}}").style.display = 'table-row';
			document.getElementById('tr-' + "{{$item->id}}" + '-phone').style.display = 'table-row';
			if ("{{$item->id}}" == saleproducts[index])
			{
				document.getElementById('tr-' + "{{$item->id}}").style.display = 'none';
				document.getElementById('tr-' + "{{$item->id}}" + '-phone').style.display = 'none';
				product.id = "{{$item->id}}".trim();
				product.title = "{{$item->title}}".trim();
				product.sale_price = "{{$item->sale_price}}";
				product.regular_price = "{{$item->regular_price}}";
				product.description = "{{$item->description}}";
				product.product_image = "{{$item->product_image}}";
			}
		@endforeach

		var purchased_products = "{{$purchased_products}}";
		purchased_products = purchased_products.split(',');
		for (index in purchased_products)
		{
			console.log('purchased', purchased_products[index]);
			if (document.getElementById('tr-' + purchased_products[index]))
			{
				document.getElementById('tr-' + purchased_products[index]).style.display = 'none';
				document.getElementById('tr-' + purchased_products[index] + '-phone').style.display = 'none';
			}
		}
		html = html +  '<div id="top-product-card-' + product.id + '" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing mdl-card mdl-shadow--4dp" style="margin:0; margin-top:5px;padding:0px;width:100%;background-color:#ffffff">\
							<div id="register-shopping-img-div" class="mdl-cell mdl-cell--2-col mdl-cell--1-col-tablet mdl-cell--hide-phone mdl-cell--middle" style="margin:0;justify-content:center;">\
								<img src="' + product.product_image + '" style="width:100%">\
							</div>\
							<div class="mdl-cell mdl-cell mdl-cell--5-col mdl-cell--2-col-tablet mdl-cell--hide-phone" style="margin:0">\
								<div class="mdl-cell mdl-cell--12-col" style="padding-top:35px">\
									<span style="font-size:25px;">' + product.title + '</span>\
								</div>\
								<div class="mdl-cell mdl-cell--12-col">\
									<span style="color:#b1b1b1">' + product.description + '</span>\
								</div>\
							</div>\
							<div class="mdl-cell mdl-cell mdl-cell--2-col-phone mdl-cell--hide-desktop mdl-cell--hide-tablet" style="margin:0">\
								<div class="mdl-cell mdl-cell--12-col" style="padding-top:35px">\
									<span style="font-size:25px;">' + product.title + '</span>\
								</div>\
								<div class="mdl-cell mdl-cell--12-col">\
									<span style="color:#b1b1b1">' + product.description + '</span>\
								</div>\
							</div>\
							<div id="register-shopping-img-div" class="mdl-cell mdl-cell mdl-cell--2-col-phone mdl-cell--hide-desktop mdl-cell--hide-tablet mdl-cell--middle" style="margin:0;text-align:center;justify-content:center;">\
								<img src="' + product.product_image + '" style="width:50%">\
							</div>\
							<div id="top-card-part-2" class="mdl-grid mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="">\
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">\
									<span style="">{!! trans("shoppingcart.shoppingcart-span3") !!}</span>\
								</div>\
								<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">\
									<span style="font-size:20px;';

					if (product.sale_price)
						html = html + 'text-decoration:line-through';
					html = html + '">';
					@if ($shoppingcartsetting['currency'] == "USD")
					   html = html + '$' + CommaFormatted(product.regular_price);
					@else
					   html = html + CommaFormatted(product.regular_price) + ' Ft';
					@endif

					html = html + '</span>\
							</div>\
							</div>\
						<div id="register-div1" class="mdl-grid mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--2-col-phone" style="">';
					if (product.sale_price)
					{
						html = html + '<div class="mdl-cell mdl-cell--12-col" style="">\
							<span style="">{!! trans("shoppingcart.shoppingcart-span4") !!} <img src="/images/shoppingcart-tooltip.png" style="padding-left:20px"></span>\
						</div>\
						<div class="mdl-cell mdl-cell--12-col">\
							<span style="font-size:20px;">';
						@if ($shoppingcartsetting['currency'] == "USD")
							html = html + '$' + CommaFormatted(product.sale_price);
						@else
							html = html + CommaFormatted(product.sale_price) + ' Ft';
						@endif
						html = html	+ '</span>\
						</div>';
					}
					html = html + '</div>\
					<div id="register-remove-div" class="mdl-grid mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone" style="">\
						<div class="mdl-cell mdl-cell mdl-cell--9-col " style="text-align:center; ">\
							<img src="/images/shoppingcart_remove_icon.png" style="padding-top:70px; cursor:pointer" onclick="Remove(' + product.id + ')">\
						</div>\
					</div>\
					</div>';
	   if (product.sale_price)
	   {
		   total = total + parseFloat(product.sale_price);
	   }
	   else
	   {
		   total = total + parseFloat(product.regular_price);
	   }
	}
	@if ($shoppingcartsetting['currency'] == "USD")
		document.getElementById('total-tag').innerHTML = '$' +  CommaFormatted(total);
	@else
		document.getElementById('total-tag').innerHTML = CommaFormatted(total) + ' Ft';
	@endif
	document.getElementById('productlists').innerHTML = html;
 }

function CommaFormatted(amount)
{
	var delimiter = ","; // replace comma if desired
	console.log(amount);
	amount = amount + '';
	var a = amount.split('.',2)
	var d='';
	if (a[1])
		d = a[1];
	var i = parseInt(a[0]);
	if(isNaN(i)) { return ''; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while(n.length > 3)
	{
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
	}
	if(n.length > 0) { a.unshift(n); }
	n = a.join(delimiter);
	if(d.length < 1) { amount = n; }
	else { amount = n + '.' + d; }
	amount = minus + amount;
	return amount;
}

 function AddCart(id)
 {
	saleproducts.push(id);
	document.getElementById('addcart-'+id).disabled = true;
	document.getElementById('addcart-phone-'+id).disabled = true;
	RefreshPage();
 }

 function Remove(id)
 {
	 console.log(id);
	 var index = saleproducts.indexOf(id);
	 if (index > -1) {
		saleproducts.splice(index, 1);
	 }
	 console.log(saleproducts);
	 if (document.getElementById('addcart-'+id))
	 {
		 document.getElementById('addcart-'+id).disabled = false;
		 document.getElementById('addcart-phone-'+id).disabled = false;
	 }
	 RefreshPage();
 }

 function oncontinue(){
	if (saleproducts.length == 0) {
		new PNotify({
			title: 'Empty Cart. Please select products!',
			type: 'error',
			delay: 3500,
			hide: true,
			sticker:false,
			icon: false,
			addclass: 'ui-pnotify-no-icon',
		});
		return false;
	}
	 var purchased_products = "{{$purchased_products}}";
	 purchased_products = purchased_products.split(',');
	 for (index in purchased_products)
	 {
		for (index1 in saleproducts)
		{
			if (purchased_products[index] == saleproducts[index1])
			{
				new PNotify({
					title: 'You have selected some products that you ordered before.',
					type: 'error',
					delay: 3500,
					hide: true,
					sticker:false,
					icon: false,
					addclass: 'ui-pnotify-no-icon',
				});
				return false;
			}
		}
	 }
	 var products = saleproducts.join();
	 location.href = '/order?products='+products;
 }
</script>
@endsection
