@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# Whoops!
@else
# {!! trans('auth.reset_hello') !!}!
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}
@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        default:
            $color = 'blue';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
{!! trans('auth.reset_regards') !!},<br>Imprevo
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
{!! trans('auth.reset_foot') !!} "{{ $actionText }}" {!! trans('auth.reset_foot1') !!} [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset
@endcomponent
