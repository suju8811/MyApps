@extends('layouts.app')

@section('content')
    <div class="mdl-layout" style="z-index: 0;background: #9fc3ae;padding:10px 10px;font-size: 16px; height:115px">

    </div>

    <main class="mdl-layout__content" style="z-index: 0;background: #ebf3ef; height:450px">
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
        <form id="nav-form" role="form">

        </form>
    </main>
    <form id="exercise_form" class="form-horizontal" role="form">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{$lesson->id}}">
    </form>
    <dialog id="exercise-dialog" class="mdl-dialog" style="z-index:1;width:60%;padding:0px 0px 0px 0px;margin-top:100px; height:70%">

    </dialog>


    <script type="text/javascript">
    var id = '{{$lesson->id}}';
    var module_exerciseList = [];
    var curLesson_exerciseList = [];
    @foreach($modules as $module)
				module_exerciseList['{{$module->id}}'] = [];
				@foreach ($module->exercises as $mexe)
					module_exerciseList['{{$module->id}}'].push({"id":"{{$mexe->id}}", "lesson_id":"{{$mexe->lesson_id}}", "title":"{{$mexe->title}}"});
				@endforeach
    @endforeach
    @foreach($modules as $module)
        var m_ex = module_exerciseList['{{$module->id}}'];

        if (m_ex.length > 0)
        {
            var count = 0;
            for (var i = 0; i < m_ex.length; i++)
             {

                 if (m_ex[i].lesson_id == id)
                 {

                    if (count==0)
                    {
                        curLesson_exerciseList['{{$module->id}}']=[];
                        count++;
                    }
                    curLesson_exerciseList['{{$module->id}}'].push(m_ex[i]);
                  }
             }
        }
    @endforeach

    var html='';
    var firstExe = null;
    @foreach($modules as $module)
        if (curLesson_exerciseList['{{$module->id}}'])
        {
            html = html + '<li class="nav-parent" style="margin-left: 0">\
                    <a><span style="font-weight:bold;color: #000000">' + '{{$module->title}}' + '</span></a>\
                    <ul class="nav nav-children" style="margin:0; padding:6px 10px;list-style-type: none; text-decoration: none">';
            var exes = curLesson_exerciseList['{{$module->id}}'];

            if (!firstExe)
            {
              firstExe =  exes[0];
            }
            for (var i=0; i<curLesson_exerciseList['{{$module->id}}'].length; i++)
            {
                var exe_i = exes[i];
                /*html = html + '<li style="margin:0; padding:8px">\
                 <a href="#" style="color: #000000;text-decoration: none"  onclick="playExercise(event,' + exe_i['id'] + ')">'+exe_i.title + '</a></li>';*/
                html = html + '<li style="margin:0; padding:8px">\
                 <a href="/exercise/' + exe_i.id + '" style="color: #000000;text-decoration: none">' + exe_i.title + '</a></li>';

            }
            html = html + '</ul></li>';

        }
    @endforeach
    //console.log(firstExe['id']);
    //document.getElementById('nav-list').innerHTML=html;
    location.href = '/exercise/' + firstExe['id'];
/*    function playExercise(id) {

        var dialog = document.getElementById("exercise-dialog");

        var exercise;
        @foreach ($lesson->exercises as $exe)
            if (id == '{{$exe['id']}}')
            {
                exercise = JSON.parse('<?php echo json_encode($exe)?>');
            }
        @endforeach

        $("#video-div").hide();
        $("#text-div").hide();
        $("#quiz-div-seletImage").hide();
        $("#quiz-div-singchoice").hide();
        $("#audio-div").hide();
        var dialogtitle = document.getElementById('dialogtitle');
        document.getElementById('progress').style.visibility = "hidden";
        if (exercise['type'] == "video")
        {
            dialogtitle.innerHTML = "Hogyan és mire használd a present perfect-et?";
            if (exercise['video_url'])
                document.getElementById('video-url').src = exercise['video_url'];
            $("#video-div").show();
        }
        else if (exercise['type'] == "quiz")
        {
            $("#quiz-div-seletImage").show();
            document.getElementById('progress').style.visibility = "visible";
            dialogtitle.innerHTML = "Select the Image that's represented by the word below!";
        }
        else if (exercise['type'] == "text")
        {
            dialogtitle.innerHTML = "Olvasd el a szöveget";
            document.getElementById('texttitle').innerHTML = exercise['description'];
            document.getElementById('text-div-content').innerHTML=exercise['content1'];
            $("#text-div").show();
        }
        else if (exercise['type'] == "translation")
        {
            dialogtitle.innerHTML = "Hallás utáni szövegértés modul";
            console.log(exercise['audio']);
            if (exercise['audio'])
            {
                document.getElementById('audioplay').innerHTML = '<source src="' +  exercise['audio'] + '" type="audio/wav"> + Your browser does not support the audio element.';

            }
            document.getElementById('translation-div-column1').innerHTML = exercise['content1'];
            document.getElementById('translation-div-column2').innerHTML = exercise['content2'];
            $("#audio-div").show();
        }
        dialog.show();
    }*/
 </script>
@endsection
