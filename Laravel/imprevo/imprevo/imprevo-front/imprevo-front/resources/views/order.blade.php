@extends('layouts.app3')
<style>

.wizard-steps > li >a{
	padding:20px;
	height:70px;
}
.wizard-steps > li >a >span{
	background-color:#006e73;
	border:1px solid #ffffff;
	width:24px;
	height:24px;
	padding: 5px 7px 5px 7px;
}
.wizard-steps .active >a >span{
	background-color:#ffffff;
	border:1px solid #006e73;
	color:#006e73;
}
.is-focused .mdl-textfield__input {
	border:none;
}

div.mdl-textfield.is-dirty {
   padding-right:25px !important;
   background: url('/images/tick1.png') no-repeat !important;
   background-position: right 0px top 25px !important;
}

div.mdl-textfield.is-dirty.is-invalid{
   background: none !important;
}

div.mdl-textfield_password{
   background: none !important;
}

.accordion {
  width: 380px;
  border-radius: 5px;
  overflow: hidden;
  margin: auto;
}

.accordion .item .heading {
  height: 50px;
  line-height: 50px;
  font-size: 17px;
  cursor: pointer;
  color: #000000;
  padding-left: 15px;
  background: #f9f9f9 url('/images/order_accordion_arrow.png') no-repeat;
  background-position: right 20px top -95px;
  border: 1px solid #eee;
  box-sizing: border-box;
}

.accordion .item.open .heading,
.accordion .item:last-child .heading { border: 0; }

.accordion .item.open .heading { background-position: right 20px top -5px; }

.accordion .item .content {
  display: none;
  padding: 15px;
  background: #f9f9f9;
  font-size: 14px;
}



#GoBill ,#GoPAYMENT ,#GoConfirm ,#GoPay ,#GoBill-register, #GoHome{
	background-color:#399aed;
}

#GoBill:hover, ##GoBill-register:hover, GoHome:hover{
	background-color:#2081d5;
}

#GoBill-register{
	background-color:#2081d5;
}

#GoBill-register.disabled{
	background-color:#eee;
}

#GoPAYMENT:hover{
	background-color:#2081d5;
}

#GoConfirm:hover{
	background-color:#2081d5;
}

#GoPay:hover{
	background-color:#2081d5;
}

#tooltip-barion-img{
	position:relative;
}

.mdl-tooltip
{
	padding:10px;
	max-width:500px !important;
	width:400px;
	background-color:#9fc3ae;
}
.mdl-tooltip.is-active {
	will-change: unset;
	background-color:#9fc3ae;
	text-align:center;
	color:#ffffff;
}

.mdl-tooltip.is-active:before{
	content: ' ';
	position: absolute;
	left: 200px;
	top: -7px;
	width: 14px;
	height: 14px;
	background-color: #9fc3ae;
	border-top: 1px solid #9fc3ae;
	border-left: 1px solid #9fc3ae;
	border-bottom: none;
	border-right: none;
	transform: rotate(45deg);
	-webkit-transform: rotate(45deg);
	-moz-transform: rotate(45deg);
}

#brecumb_text{
	padding:5px 40px 5px 80px;
}
#main-content{
	padding-bottom:150px;
}

.tab-title {
	font-size:25px;
}

@media (max-width:1500px)
{
	.confirm-content{
		width: 80% !important;
	}
}

@media (max-width: 1380px) {
	.wizard-steps > li >a{
		padding:20px;
		padding-top:26px;
		height:85px;
		font-size:14px !important;
	}
	.wizard-tabs ul > li > a:before, .wizard-tabs ul > li > a:after {
		border-bottom: 42px solid transparent !important;
		border-top: 42px solid transparent !important;
	}

	.divtitle {
		padding:20px 0px 20px 0px !important;
	}
}
@media (max-width:1240px)
{
	.confirm-content{
		width: 90% !important;
	}
}
@media (max-width:1080px)
{
	.confirm-content{
		width: 100% !important;
	}
}
@media (max-width: 975px) {
	.wizard-steps > li >a{
		padding:20px;
		height:85px;
		font-size:13px !important;
	}
}
@media (max-width: 769px) {
	.wizard-steps > li >a{
		padding:20px;
		height:85px;
		font-size:13px !important;
	}
	.wizard-steps > li >a >span{
		background-color:#006e73;
		border:1px solid #ffffff;
		width:24px;
		height:24px;
		display:inline-block !important;
		padding: 5px 7px 5px 7px;
	}

	.tab-title {
		font-size:20px;
	}
	.wizard-tabs ul > li > a:before, .wizard-tabs ul > li > a:after {
		border:none !important;
	}
	#main-content{
		padding-bottom:250px;
	}
}

@media (max-width: 450px) {
	.wizard-steps > li >a{
		text-align:center !important;
		padding:5px;
		font-size:10px !important;
	}

	.wizard-steps .active >a >span{
		background-color:#ffffff;
		border:1px solid #006e73;
		color:#006e73;
	}

	#brecumb_text{
		padding:5px 0px 5px 10px;
	}

}

</style>


@section('content')
<div id="main-content" class="mdl-layout__content" style="width:100%;background:#ebf3ef;">
    <div id="brecumb_text" style="background: #9fc3ae;font-size: 16px;">
        <span><a href="/" style="color:#d5e7dc;line-height: 40px;text-decoration: none">{!! trans('order.imprevo') !!}</a></span>
        <span style="color:#d5e7dc;line-height: 40px">/</span>
        <span style="color:#fff;line-height: 40px">{!! trans('order.shop') !!}</span>
    </div>
	<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="justify-content:center;min-height:550px;">
		<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:50px;">
			<section class="panel form-wizard" id="w1" style="width:100%">
				<div class="panel-body panel-body-nopadding">
					<div class="wizard-tabs">
						<ul id="tab-desktop" class="wizard-steps mdl-cell--hide-phone mdl-cell--hide-tablet">
							<li id="w1-tab1" class="active">
								<a href="#w1-step1" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">1</span>
									{!! trans('order.register') !!}
								</a>
							</li>
							<li id="w1-tab2">
								<a href="#w1-step2" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">2</span>
									{!! trans('order.span1') !!}
								</a>
							</li>
							<li  id="w1-tab3">
								<a href="#w1-step3" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">3</span>
									{!! trans('order.span3') !!}
								</a>
							</li>
							<li id="w1-tab4">
								<a href="#w1-step4" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">4</span>
									{!! trans('order.span4') !!}
								</a>
							</li>
						</ul>
						<ul class="wizard-steps mdl-cell--hide-desktop">
							<li id="w1-tab1" class="active">
								<a href="#w1-step1" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">1</span><br>
									{!! trans('order.register') !!}
								</a>
							</li>
							<li id="w1-tab2">
								<a href="#w1-step2" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">2</span><br>
									{!! trans('order.span1') !!}
								</a>
							</li>
							<li  id="w1-tab3">
								<a href="#w1-step3" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">3</span><br>
									{!! trans('order.span3') !!}
								</a>
							</li>
							<li id="w1-tab4">
								<a href="#w1-step4" data-toggle="tab" class="text-center">
									<span class="badge hidden-xs">4</span><br>
									{!! trans('order.span4') !!}
								</a>
							</li>
						</ul>
					</div>
						<div class="tab-content" style="padding:0;">
							<div id="w1-step1" class="tab-pane active" >
								<form id="order-login-form" name="order-login-form" role="form" class="form-horizontal" action="/order/login" method="post" encType="multipart/form-data">
									{{ csrf_field() }}
									<div id="login-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="display:none;width:100%;margin:0;padding:0;justify-content:center;">
										<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center; padding:50px 0px 50px 0px">
											<span class="tab-title" style="color:#000000">{!! trans('order.span5') !!}</span>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="width:100%;margin:0;text-align:center; background-color:#f9f9f9; border:1px solid #eee; margin:0;padding:10px">
											<a onclick="goRegisterDiv()" style="cursor:pointer;font-size:14px; color:#399aed">{!! trans('order.span6') !!}</a>
										</div>
										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="justify-content:center">
											<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="email" id="login-email" name="login-email"  @if ($user) value="{{$user->email}}" @endif style="" autofocus>
														<label class="mdl-textfield__label" for="login-email" style="font-weight:bold">{!! trans('order.span7') !!}</label>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="password" id="login-password" name="login-password" pattern=".{5,}">
														<label class="mdl-textfield__label" for="login-password" style="font-weight:bold">{!! trans('order.span8') !!}</label>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<a href="{{ url('/password/reset') }}" style="color:#399aed;cursor:pointer">{!! trans('order.span9') !!}</a>
												</div>
												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0; padding-top:20px;padding-bottom:50px">
													<div class="mdl-cell mdl-cell mdl-cell--7-col" style="">
														<a id="GoBill" class="next mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
															{!! trans('order.span10') !!}
														</a>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--5-col" style="padding:7px">
														<a href="/shoppingcart?products={{$productstr}}" style="color:#979797">{!! trans('order.span11') !!}</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
								<form id="order-register-form" name="order-login-form" role="form" class="form-horizontal" action="/order/register" method="post" encType="multipart/form-data">
									{{ csrf_field() }}
									<div id="register-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-grid--no-spacing" style="display:inline;justify-content:center;padding:0;">
										<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center; padding:50px 0px 50px 0px">
											<span class="tab-title" style="color:#000000">{!! trans('order.span12') !!}</span>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="text-align:center; background-color:#f9f9f9; border:1px solid #eee; margin:0;padding:10px">
											<a onclick="goLoginDiv()" style="cursor:pointer;font-size:14px; color:#399aed">{!! trans('order.span13') !!}</a>
										</div>
										@if ($errors->has('email'))
										<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="margin:0;margin-top:10px;width:100%;padding:10px;border-top:1px solid #eee; border-bottom:1px solid #eee;background-color:#ffffff;text-align:center;color:#ff0000">
											<span>{{ $errors->first('email') }}</span>
										</div>
										@endif
										@if ($errors->has('password'))
										<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="margin:0;margin-top:10px;width:100%;padding:10px;background-color:#ffffff;text-align:center;color:#ff0000">
											<span>{{ $errors->first('password') }}</span>
										</div>
										@endif
										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="justify-content:center">
											<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="email" id="register-email" name="register-email"  @if ($user) value="{{$user->email}}" @endif style="" autofocus>
														<label class="mdl-textfield__label" for="register-email" style="font-weight:bold">{!! trans('order.span14') !!}</label>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="password" id="register-password" name="register-password"  style="" pattern=".{5,}">
														<label class="mdl-textfield__label" for="register-password" style="font-weight:bold">{!! trans('order.span15') !!}</label>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:0px">
													<span style="">Minimum 5 karakter</span>
												</div>
												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding-top:20px">
													<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="agreeForService">
														<input type="checkbox" id="agreeForService" class="mdl-checkbox__input" onclick="toggleAgree()">
														<span>{!! trans('order.span65') !!} <a href="/aszf" target="_blank">{!! trans('order.span16') !!}</a>et!</span>
													</label>
												</div>
												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0; padding-top:20px;padding-bottom:50px">
													<div class="mdl-cell mdl-cell mdl-cell--7-col" style="">
														<span id="GoBill-register" class="next mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
															{!! trans('order.span10') !!}
														</span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--5-col" style="padding:7px;">
														<a href="/shoppingcart?products={{$productstr}}" style="color:#979797">{!! trans('order.span17') !!}</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div id="w1-step2" class="tab-pane">
								<form id="billing-personal-form" name="billing-personal-form" role="form" class="form-horizontal" action="/order/billinginfo" method="post" encType="multipart/form-data">
									{{ csrf_field() }}
									<div id="billing-personal-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="justify-content:center;">
										<div class="divtitle mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="justify-content:center;text-align:center; padding:50px 0px 50px 0px">
											<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop " style="">
												<span class="tab-title" style="color:#000000;line-height:150%;">{!! trans('order.span18') !!}</span>
											</div>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--12-col" style="margin:0px; height:1px;background-color:#eeeeee"></div>
										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="justify-content:center">
											<div class="mdl-cell mdl-cell mdl-cell--6-col" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="text" id="name" name="name"  @if ($user) @if ($user->name != $user->email) value="{{$user->name}}" @endif @endif style="" autofocus>
														<label class="mdl-textfield__label" for="name" style="font-weight:bold">{!! trans('order.span19') !!}</label>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:0px">
													<a onclick="goCompanyInfo()" style="color:#399aed; cursor:pointer">{!! trans('order.span20') !!}</a>
												</div>

												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth getmdl-select__fix-height" style="width:100%">
														<input class="mdl-textfield__input" type="text" id="country" readonly>
														<input type="hidden" name="country" value=" @if ($user) @if ($user->country) {{$user->country}} @endif @endif"/>
														<i class="mdl-icon-toggle__label material-icons" >keyboard_arrow_down</i>
														<label for="country" class="mdl-textfield__label">{!! trans('order.span21') !!}</label>
														<ul for="country" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
															@foreach ($countrylist as $country => $region)
																<li class="mdl-menu__item" data-val="{{$country}}" @if ($user) @if ($user->country) @if ($user->country == $country) data-selected="true" @endif @else @if ($country == "Hungary") data-selected="true" @endif @endif @endif
																 >{{$country}}</li>
															@endforeach
														</ul>
													</div>
												</div>

												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px;padding-top:20px">
													<div class="mdl-cell mdl-cell mdl-cell--8-col" style="margin:0;padding-right:20px;">
														<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
															<input class="mdl-textfield__input" type="text" id="postcode" name="postcode"  @if ($user) value="{{$user->zipcode}}" @endif style="" autofocus>
															<label class="mdl-textfield__label" for="postcode" style="font-weight:bold">{!! trans('order.span22') !!}</label>
														</div>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--4-col" style="margin:0">
														<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
															<input class="mdl-textfield__input" type="text" id="city" name="city" @if ($user) value="{{$user->city}}" @endif style="">
															<label class="mdl-textfield__label" for="city" style="font-weight:bold">{!! trans('order.span23') !!}</label>
														</div>
													</div>
												</div>

												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0px;padding-top:20px">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="text" id="street_address" name="street_address"  @if ($user) value="{{$user->street_address}}" @endif>
														<label class="mdl-textfield__label" for="street_address" style="font-weight:bold">{!! trans('order.span24') !!}</label>
													</div>
												</div>

												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0; padding-top:20px;padding-bottom:50px">
													<div class="mdl-cell mdl-cell mdl-cell--7-col" style="">
														<span id="GoPAYMENT" class="next mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
															{!! trans('order.span25') !!}
														</span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--5-col" style="padding:10px">
														<a onclick="selectTab(1)" style="color:#979797;cursor:pointer">{!! trans('order.span26') !!}</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
								<form id="billing-company-form" name="billing-company-form" role="form" class="form-horizontal" action="/order/billinginfo" method="post" encType="multipart/form-data">
									{{ csrf_field() }}
									<div id="billing-company-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="display:none;justify-content:center;">
										<div class="divtitle mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;text-align:center; padding:50px 0px 50px 0px">
											<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="">
												<span class="tab-title" style="font-size:25px; color:#000000; line-height:150%;">{!! trans('order.span27') !!}</span>
											</div>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--12-col" style="margin:0px; height:1px;background-color:#eeeeee"></div>
										<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
											<div class="mdl-cell mdl-cell mdl-cell--6-col mdl-cell--4-col-phone mdl-cell--8-col-tablet" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="text" id="company-name" name="company-name"  @if ($user) value="{{$user->company}}" @endif style="" autofocus>
														<label class="mdl-textfield__label" for="company-name" style="font-weight:bold">{!! trans('order.span28') !!}</label>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:0px">
													<a onclick="goPersonalInfo()" style="color:#399aed;cursor:pointer">{!! trans('order.span29') !!}</a>
												</div>

												<div id="tax-div" class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="text" id="tax_num" name="tax_num"  @if($user) @if ($user->tax_num) value="{{$user->tax_num}}" @else value="" @endif @endif style="">
														<label class="mdl-textfield__label" for="tax_num" style="font-weight:bold">{!! trans('order.span30') !!}</label>
													</div>
												</div>

												<div id="vat-div" class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="text" id="eu_vat_num" name="eu_vat_num"  @if($user) @if ($user->eu_vat_num) value="{{$user->eu_vat_num}}" @else value="" @endif @endif style="">
														<label class="mdl-textfield__label" for="eu_vat_num" style="font-weight:bold">{!! trans('order.span31') !!}</label>
													</div>
												</div>



													<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px;">
														<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fullwidth getmdl-select__fix-height" style="width:100%">
															<input class="mdl-textfield__input" type="text" id="company-country" readonly onchange="OnCountry()">
															<input type="hidden" name="company-country" value=""/>
															<label for="company-country" style="float:right">
																<i class="mdl-icon-toggle__label material-icons" >keyboard_arrow_down</i>
															</label>
															<label for="company-country" class="mdl-textfield__label">{!! trans('order.span21') !!}</label>
															<ul for="country" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
																@foreach ($countrylist as $country => $region)
																<li class="mdl-menu__item" data-val="{{$country}}" @if ($user) @if ($user->country) @if ($user->country == $country) data-selected="true" @endif @else @if ($country == "Hungary") data-selected="true" @endif @endif @endif
																 >{{$country}}</li>
																@endforeach
															</ul>
														</div>
													</div>


												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0px;padding-top:20px">
													<div class="mdl-cell mdl-cell mdl-cell--8-col" style="margin:0">
														<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px;padding-right:20px;">
															<input class="mdl-textfield__input" type="text" id="company-postcode" name="company-postcode"  @if($user) value="{{$user->zipcode}}" @endif style="" autofocus>
															<label class="mdl-textfield__label" for="company-postcode" style="font-weight:bold">{!! trans('order.span22') !!}</label>
														</div>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--4-col" style="margin:0">
														<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
															<input class="mdl-textfield__input" type="text" id="company-city" name="company-city"  @if ($user) value="{{$user->city}}" @endif style="">
															<label class="mdl-textfield__label" for="city" style="font-weight:bold">{!! trans('order.span23') !!}</label>
														</div>
													</div>
												</div>

												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0px;padding-top:20px">
													<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:100%;margin-top:10px">
														<input class="mdl-textfield__input" type="text" id="company-street_address" name="company-street_address"  @if($user) value="{{$user->street_address}}" @endif>
														<label class="mdl-textfield__label" for="company-street_address" style="font-weight:bold">{!! trans('order.span24') !!}</label>
													</div>
												</div>

												<div class="mdl-grid mdl-cell mdl-cell--12-col" style="padding:0;margin:0; padding-top:20px;padding-bottom:50px">
													<div class="mdl-cell mdl-cell mdl-cell--7-col" style="">
														<span id="GoPAYMENT" class="next mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
															{!! trans('order.span25') !!}
														</span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--5-col" style="padding:10px">
														<a onclick="selectTab(1)" style="color:#979797;cursor:pointer">{!! trans('order.span26') !!}</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>

							<div id="w1-step3" class="tab-pane">
								<div id="payments-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;">
									<div class="divtitle mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding:50px 0px 50px 0px">
										<span class="tab-title" style="font-size:25px; color:#000000">{!! trans('order.span32') !!}</span>
									</div>

									<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="">
										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="border:1px solid #eee;margin:0; background-color:#f9f9f9">
											<div class="mdl-cell mdl-cell mdl-cell--hide-desktop mdl-cell--hide-tablet mdl-cell--4-col-phone" style="text-align:center">
												<img src="/images/order_barion_logo.png" style="width:80px; height:30px">
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-phone" style="text-align:center; padding:20px 0px 20px 0px;">
												<label class="mdl-radio mdl-js-radio" for="option1">
													<input type="radio" id="option1" name="payment_radio" class="mdl-radio__button" value="barion" checked>
													<span class="mdl-radio__label"></span>
												</label>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--7-col mdl-cell--3-col-phone" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
													<span style="font-size:16px; font-weight:bold">{{$shoppingcartsetting['barion_title']}}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
													<span style="">
														({!! trans('order.span33') !!}) <img id="tooltip-barion-img" src="/images/shoppingcart-tooltip.png" style="padding-left:20px">
													</span>

												</div>
												<div class="mdl-tooltip barion" for="tooltip-barion-img" style="">
													<!--<span style="font-size:14px" data-tooltip data-width="350" class="has-tip" title="Lorem ipsum dolor">{{$shoppingcartsetting['barion_description']}}</span>-->
													<span style="font-size:14px">{{$shoppingcartsetting['barion_description']}}</span>
												</div>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--hide-phone" style="text-align:center">
												<img src="/images/order_barion_logo.png">
											</div>
										</div>

										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="border:1px solid #eee;margin:0; background-color:#f9f9f9">
											<div class="mdl-cell mdl-cell mdl-cell--hide-desktop mdl-cell--hide-tablet mdl-cell--4-col-phone" style="text-align:center">
												<img src="/images/order-paypal-logo.png" style="width:80px; height:30px">
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-phone" style="text-align:center; padding:20px 0px 20px 0px;">
												<label class="mdl-radio mdl-js-radio" for="option2">
													<input type="radio" id="option2" name="payment_radio" class="mdl-radio__button" value="paypal">
													<span class="mdl-radio__label"></span>
												</label>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--7-col mdl-cell--3-col-phone" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
													<span style="font-size:16px; font-weight:bold">{{$shoppingcartsetting['paypal_title']}}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
													<span style="">({!! trans('order.span33') !!})<img id="tooltip-paypal-img" src="/images/shoppingcart-tooltip.png" style="padding-left:20px"></span>
												</div>
												<div class="mdl-tooltip paypal" for="tooltip-paypal-img" style="">
													<span style="font-size:14px">{{$shoppingcartsetting['paypal_description']}}</span>
												</div>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--hide-phone" style="text-align:center">
												<img src="/images/order-paypal-logo.png">
											</div>
										</div>

										<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee;margin:0; background-color:#f9f9f9">
											<div class="mdl-cell mdl-cell mdl-cell--hide-desktop mdl-cell--hide-tablet mdl-cell--4-col-phone" style="text-align:center">
												<img src="/images/order-bank-transfer.png" style="width:80px; height:30px">
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-phone" style="text-align:center; padding:20px 0px 20px 0px;">
												<label class="mdl-radio mdl-js-radio" for="option3">
													<input type="radio" id="option3" name="payment_radio" class="mdl-radio__button" value="bank">
													<span class="mdl-radio__label"></span>
												</label>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--7-col mdl-cell--3-col-phone" style="">
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
													<span style="font-size:16px; font-weight:bold">{{$shoppingcartsetting['bank_title']}}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="">
													<span style="">({!! trans('order.span34') !!})<img id="tooltip-bank-img" src="/images/shoppingcart-tooltip.png" style="padding-left:20px"></span>
												</div>
												<div class="mdl-tooltip bank" for="tooltip-bank-img" style="">
													<span style="font-size:14px">{{$shoppingcartsetting['bank_description']}}</span>
												</div>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--hide-phone" style="text-align:center">
												<img src="/images/order-bank-transfer.png">
											</div>
										</div>
									</div>
									<div class="mdl-grid mdl-cell mdl-cell--7-col" style="margin:0;padding:0;">
										<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
											<span style="font-size:16px;color:#979797">
												<i aria-hidden="true" class="fa fa-lock" style="padding-right:10px;font-size:16px;">
												</i>
												{!! trans('order.span35') !!}
											</span>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
											<span style="font-size:16px;color:#979797"></span>
										</div>
									</div>
									<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0;justify-content:center">
										<div class="mdl-grid mdl-cell mdl-cell--6-col">
											<div class="mdl-cell mdl-cell mdl-cell--7-col" style="text-align:center">
												<span id="GoConfirm" class="next mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
													{!! trans('order.span36') !!}
												</span>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--5-col" style="text-align:center">
												<a onclick="selectTab(2)" style="cursor:pointer;color:#979797">{!! trans('order.span37') !!}</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="w1-step4" class="tab-pane">

								<div id="confirmation-div" class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center;">
									<div class="divtitle mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding:50px 0px 50px 0px; border-bottom:1px solid #eee">
										<span class="tab-title" style="font-size:25px; color:#000000">{!! trans('order.span38') !!}</span>
									</div>

									<div class="confirm-content mdl-cell mdl-cell mdl-cell--8-col" style="margin:0;border:1px solid #eee;padding:15px;">
										@foreach ($products as $product)
											<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="border-bottom:1px solid #eee;margin:0; background-color:#ffffff">
												<div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--1-col-phone" style="padding:10px">
													<img src="{{$product->product_image}}" style="width:50px">
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--9-col mdl-cell--5-col-tablet mdl-cell--2-col-phone" style="padding:20px">
													<span style="font-size:16px; font-weight:bold">{{$product->title}}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="padding:20px;text-align:right">
													<span style="font-size:16px;">
													@if (!is_null($product->sale_price))
														@if ($shoppingcartsetting['currency'] == 'USD')
															$<?php echo number_format($product->sale_price) ?>
														@else
															<?php echo number_format($product->sale_price) ?> Ft
														@endif
													@else
														@if ($shoppingcartsetting['currency'] == 'USD')
															$<?php echo number_format($product->regular_price) ?>
														@else
															<?php echo number_format($product->regular_price) ?> Ft
														@endif
													@endif
													</span>
												</div>
											</div>
										@endforeach

										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing" style="margin:0;padding:15px;background-color:#ffffff">
											<div class="mdl-cell mdl-cell mdl-cell--7-col" style="">
											</div>
											<div class="mdl-grid mdl-cell mdl-cell--5-col mdl-grid--no-spacing" style="padding:0;">
												<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:right;padding:5">
													<span id="" style="font-size:16px; font-weight:bold">{!! trans('order.span39') !!}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:right;padding:5">
													<span id="total-subtotal" style="font-size:16px; font-weight:bold">141,515 Ft</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:right;padding:5">
													<span id="" style="font-size:16px; font-weight:bold">{!! trans('order.span40') !!}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:right;padding:5">
													<span id="total-tax" style="font-size:16px; font-weight:bold">141,515 Ft</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0;margin:0;padding:5">
													<hr style="margin:0; padding:0; height:1px; background-color:#000000; width:100%">
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:right;padding:5">
													<span id="" style="font-size:16px; font-weight:bold">{!! trans('order.span41') !!}</span>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--6-col" style="text-align:right;padding:5">
													<span id="total-span" style="font-size:16px; font-weight:bold">141,515 Ft</span>
												</div>
											</div>
										</div>
										<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing" style="margin:0; padding:15px;background-color:#ffffff">
											<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:right; ">

											</div>
										</div>
									</div>
									<div class="confirm-content mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;margin:0;border:1px solid #eee;border-top:none;padding:15px;">
										<div class="mdl-cell mdl-cell mdl-cell--7-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" style="text-align:right;">
											<span id="GoPay" class="next mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
												{!! trans('order.span42') !!}
											</span>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--5-col mdl-cell--4-col-tablet mdl-cell--2-col-phone" style="text-align:left; padding: 10px;">
											<a onclick="selectTab(3)" style="color:#979797;cursor:pointer">{!! trans('order.span43') !!}</a>
										</div>
									</div>
									<div class="confirm-content accordion mdl-cell mdl-cell mdl-cell--8-col" style="margin:0;">
										<div class="item">
											<div class="heading" style="border:1px solid #eee; ">{!! trans('order.span44') !!}</div>
											<div class="content" style="width:100%; border:1px solid #eee; border-top:none;border-bottom:none;margin:0;padding:0;">
												<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing" style="width:100%;margin:0;padding:0;">
												<div class="mdl-cell mdl-cell mdl-cell--4-col" style="margin:0; border:1px solid #eee; padding-left:20px">
													<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:10px">
														<span style="font-weight:bold; font-size:16px">{!! trans('order.span45') !!}</span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px">
														<span id="account-span" style="font-size:14px">dtommy79@freemail.hu</span>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--4-col" style="margin:0; border:1px solid #eee; padding-left:20px">
													<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:10px">
														<span style="font-weight:bold; font-size:16px">{!! trans('order.span46') !!}</span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px">
														<span id="billing-info-name" style="font-size:14px"></span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--12-col">
														<span id="billing-info-address" style="font-size:14px"></span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--12-col">
														<span id="billing-info-postcode" style="font-size:14px"></span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--12-col">
														<span id="billing-info-tax" style="font-size:14px"></span>
													</div>
													<div class="mdl-cell mdl-cell mdl-cell--12-col">
														<span id="billing-info-vat" style="font-size:14px"></span>
													</div>
												</div>
												<div class="mdl-cell mdl-cell mdl-cell--4-col" style="margin:0; border:1px solid #eee; padding-left:20px">
													<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:10px">
														<span style="font-weight:bold; font-size:16px">{!! trans('order.span47') !!}</span>
													</div>
													<div id="payment-img-div" class="mdl-cell mdl-cell mdl-cell--12-col" style="padding-top:20px">
														<img src="/images/order-bank-transfer.png">
													</div>
												</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div id="result-div-success" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="display:none;justify-content:center;">
									<div class="divtitle mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding:50px 0px 50px 0px; border-bottom:1px solid #eee">
										<span class="tab-title" style="font-size:25px; color:#000000">{!! trans('order.span48') !!}</span>
									</div>
									<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:15px; text-align:center;">
											<img src="/images/order-result-successful-big-icon.png">
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:50px;text-align:center">
											<span style="color:#000000;font-size:16px; font-weight:bold">{!! trans('order.span49') !!}</span>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:50px;text-align:center;">
											<a id="GoHome" href="/home" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="width:200px;text-decoration:none">
												{!! trans('order.span50') !!}
											</a>
										</div>
									</div>
								</div>
								<div id="result-div-failed" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="display:none;justify-content:center;">
									<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding:50px 0px 50px 0px; border-bottom:1px solid #eee">
										<span class="tab-title" style="font-size:25px; color:#000000">{!! trans('order.span51') !!}</span>
									</div>
									<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:15px; text-align:center;">
											<img src="/images/payment-failed-big-icon.png">
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:50px;text-align:center;">
											<span style="color:#000000;font-size:16px; font-weight:bold">{!! trans('order.span52') !!}</span>
										</div>

										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:50px;text-align:center;">
											<a id="GoHome" href="#" onclick="selectTab(3)" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="text-decoration:none">
												{!! trans('order.span53') !!}
											</a>
										</div>
									</div>
								</div>
								<div id="result-div-pending" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="display:none;justify-content:center;">
									<div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center; padding:50px 0px 50px 0px; border-bottom:1px solid #eee">
										<span class="tab-title" style="font-size:25px; color:#000000">{!! trans('order.span48') !!}</span>
									</div>
									<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:15px; text-align:center;">
											<img src="/images/order-result-successful-big-icon.png">
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:15px; ">
											<ul>
												<li>{!! trans('order.span54') !!} {{Session::get('orderid')}} </li>
												<li>{!! trans('order.span55') !!}</li>
												<li>{!! trans('order.span56') !!}</li>
											</ul>
										</div>

										<div class="mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="background-color:#ebf3ef; margin:0;padding:15px; text-align:center;">
											<div class="mdl-cell mdl-cell mdl-cell--12-col">
												<span style="float:left">{!! trans('order.span57') !!}</span>
												<span style="float:right">Imprevo Kft.</span>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--12-col">
												<span style="float:left">{!! trans('order.span58') !!} </span>
												<span style="float:right">10402142-50526771-77801015</span>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--12-col">
												<span style="float:left">{!! trans('order.span59') !!}</span>
												<span style="float:right">{{Session::get('orderid')}}</span>
											</div>
											<div class="mdl-cell mdl-cell mdl-cell--12-col">
												<span style="float:left">{!! trans('order.span60') !!}</span>
												<span id="total-result-span" style="float:right">
													@if ($shoppingcartsetting['currency'] == 'USD')
														${{Session::get('total_price')}}
													@else
														{{Session::get('total_price')}} Ft
													@endif
												</span>
											</div>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:15px;">
											<ul>
												<li>{!! trans('order.span61') !!}</li>
											</ul>
										</div>
										<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:15px;text-align:center;">
											<a id="GoHome" href="/home" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="text-decoration:none">
												{!! trans('order.span62') !!}
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
			</section>
			<form id="payment-confirm-form" name="payment-confirm-form" role="form" class="form-horizontal" action="/payment" method="post" encType="multipart/form-data">
				{{ csrf_field() }}
				<input type="hidden" id="payment_input"/>
				<input type="hidden" id="is_valid_vat" name="is_valid_vat" value="-1"/>
				<input type="hidden" id="account" name="account" value=""/>
				<input type="hidden" id="selectProducts" name="selectProducts" value="{{$productstr}}"/>
			</form>
		</div>
	</div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script type="text/javascript">
function isMobileDevice() {
	return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};
if (isMobileDevice) {
	$('#tab-desktop').remove();
}


var countries = <?php echo json_encode($countrylist) ?>;
var currentTab = 1;
var EuropeUnionlist = ['Austria', 'Italy',
'Belgium',	'Latvia',
'Bulgaria',	'Lithuania',
'Croatia',	'Luxembourg',
'Cyprus',	'Malta',
'Czechia',	'Netherlands',
'Denmark',	'Poland',
'Estonia',	'Portugal',
'Finland',	'Romania',
'France',	'Slovakia',
'Germany'	,'Slovenia',
'Greece',	'Spain',
'Hungary'	,'Sweden',
'Ireland',	'United Kingdom'];

var notEuropeUnionlist = ['Albania', 'Andorra', 'Bosnia and Herzegovina', 'Liechtenstein', 'Norway'];
var total = 0;
@foreach ($products as $product)
	@if (!is_null($product->sale_price))
		each_price = parseFloat("{{$product->sale_price}}");
	@else
		each_price =  parseFloat("{{$product->regular_price}}");
	@endif
	total = total + each_price;
@endforeach


@if ($user)
	document.getElementById('account').value = "{{$user->email}}";
@endif





function CommaFormatted(amount)
{
	var delimiter = ","; // replace comma if desired
	amount = amount + '';
	var a = amount.split('.',2)
	var d='';
	if (a[1])
		d = a[1];
	var i = parseInt(a[0]);
	if(isNaN(i)) { return ''; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while(n.length > 3)
	{
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
	}
	if(n.length > 0) { a.unshift(n); }
	n = a.join(delimiter);
	if(d.length < 1) { amount = n; }
	else { amount = n + '.' + d; }
	amount = minus + amount;
	return amount;
}
var bnext = false;
$(document).ready(function () {
  //your code here
	$('#w1').bootstrapWizard({
		tabClass: 'wizard-steps',
		nextSelector: '.next',//'ul.pager li.next',
		previousSelector: '.prev',
		firstSelector: null,
		lastSelector: null,
		onNext: function( tab, navigation, index, newindex ) {
			index = currentTab;
			if (index == 1)
			{
				if (document.getElementById('register-div').style.display == 'inline')
				{
					email = document.getElementById('register-email');
					password = document.getElementById('register-password');
					agreeForService = document.getElementById('agreeForService');

					if (!email.value)
					{
						email.focus();
					}

					if (!password.value)
					{
						password.focus();

					}

					if (!password.value || !email.value)
					{
						PNotify.removeAll();
						new PNotify({
							title: 'Please fill input fields.',
							type: 'warning',
							delay: 3500,
							hide: true,
							sticker:false,
							icon: false,
							addclass: 'ui-pnotify-no-icon',
						});
						return false;
					}

					if (!agreeForService.checked)
					{
						new PNotify({
							title: 'Please check that you accept the terms and conditions!',
							type: 'warning',
							delay: 3500,
							hide: true,
							sticker:false,
							icon: false,
							addclass: 'ui-pnotify-no-icon',
						});
						agreeForService.focus();
						return false;
					}
					document.getElementById('account').value = email;
					document.getElementById('order-register-form').submit();
				}
				else
				{
					email = document.getElementById('login-email');
					password = document.getElementById('login-password');

					if (!email.value)
					{
						new PNotify({
							title: 'Email empty!.',
							type: 'warning',
							delay: 3500,
							hide: true,
							sticker:false,
							icon: false,
							addclass: 'ui-pnotify-no-icon',
						});
						email.focus();
						return false;
					}

					if (!password.value || password.value.length < 5)
					{
						new PNotify({
							title: 'Please input password!<br>(at least 5 characters)',
							type: 'warning',
							delay: 3500,
							hide: true,
							sticker:false,
							icon: false,
							addclass: 'ui-pnotify-no-icon',
						});
						password.focus();
						return false;
					}
					document.getElementById('account').value = email;
					document.getElementById('order-login-form').submit();
				}

			}
			else if (index == 2)
			{
				if (document.getElementById('billing-company-div').style.display == 'none')
				{
					var c = document.getElementById('billing-personal-div').getElementsByTagName("*");
					for (i = 0; i < c.length; i++) {
						if (c[i].tagName == "INPUT" || c[i].tagName == "SELECT")
						{
							if (!c[i].value)
							{
								if (c[i].id == 'tax_num' || c[i].id == 'eu_vat_num')
									continue;
								new PNotify({
									title: 'Empty fields!',
									type: 'warning',
									delay: 3500,
									hide: true,
									sticker:false,
									icon: false,
									addclass: 'ui-pnotify-no-icon',
								});
								c[i].focus();
								return false;
							}
						}
					}

					$.ajax({ // create an AJAX call...
						data: $('#billing-personal-form').serialize(), // get the form data
						type: $('#billing-personal-form').attr('method'), // GET or POST
						url: $('#billing-personal-form').attr('action'), // the file to call
						success: function(response) { // on success..
							country_name = document.getElementById('country').value;
							var region = countries[country_name];

							//if (region == "Europe" && notEuropeUnionlist.indexOf(country_name) == -1)
							if (EuropeUnionlist.indexOf(country_name) != -1)
							{
								document.getElementById('is_valid_vat').value = 0;
							}
							else
							{
								document.getElementById('is_valid_vat').value = -1;
							}

							document.getElementById('company-name').value = '';
							selectTab(3);
							//$('#w1').bootstrapWizard('show',2);
						}
					});
				}
				else
				{

					var c = document.getElementById('billing-company-div').getElementsByTagName("*");

					for (i = 0; i < c.length; i++) {
						if (c[i].tagName == "INPUT" || c[i].tagName == "SELECT")
						{
							if (!c[i].value)
							{
								if (c[i].id == 'tax_num' || c[i].id == 'eu_vat_num')
										continue;
								new PNotify({
									title: 'Empty fields!',
									type: 'warning',
									delay: 3500,
									hide: true,
									sticker:false,
									icon: false,
									addclass: 'ui-pnotify-no-icon',
								});
								c[i].focus();
								return false;
							}
						}
					}

					$.ajax({ // create an AJAX call...
						data: $('#billing-company-form').serialize(), // get the form data
						type: $('#billing-company-form').attr('method'), // GET or POST
						url: $('#billing-company-form').attr('action'), // the file to call
						success: function(response) { // on success..
							country_name = document.getElementById('company-country').value;
							var region = countries[country_name];
							//console.log(country_name);
							//console.log('response', response.is_valid_vat);

							if (EuropeUnionlist.indexOf(country_name) != -1 && country_name != "Hungary")
							{
								if (response.is_valid_vat == 1)
								{
									PNotify.removeAll();
									new PNotify({
										title: 'Your Eu Vat number is valid.',
										type: 'success',
										delay: 3500,
										hide: true,
										sticker:false,
										icon: false,
										addclass: 'ui-pnotify-no-icon',
									});
								}
								else
								{
									PNotify.removeAll();
									new PNotify({
										text: 'Your Eu Vat number is not valid.',
										type: 'error',
										delay: 3500,
										icon: false,
										hide: true,
										sticker:false,
										addclass: 'ui-pnotify-no-icon',
									});
								}
								document.getElementById('tax_num').value = '';
								document.getElementById('is_valid_vat').value = response.is_valid_vat;
							}
							else if (country_name == "Hungary")
							{
								document.getElementById('is_valid_vat').value = 0;
								document.getElementById('eu_vat_num').value = '';
							}
							else
							{
								document.getElementById('is_valid_vat').value = -1;
								document.getElementById('eu_vat_num').value = '';
							}

							var value = document.getElementById('is_valid_vat').value;
							//console.log('company');
							//console.log(value);
							//$('#w1').bootstrapWizard('show',2);
							selectTab(3);
							//return true;
						}
					});
				}
			}
			else if (index==3)
			{
				value = $('input[name="payment_radio"]:checked').val();
				document.getElementById('payment_input').value = value;
				var is_valid_vat = document.getElementById('is_valid_vat').value;

				company = document.getElementById('company-name').value;
				country_name = null;

				country_name = document.getElementById('company-country').value;

				if (!country_name)
					country_name = document.getElementById('country').value;

				var region = countries[country_name];
				//console.log('is_valid_vat',is_valid_vat);
				if (is_valid_vat == -1)
				{
					@if ($shoppingcartsetting['currency'] == 'USD')
						document.getElementById('total-span').innerHTML = '$' + CommaFormatted(total.toFixed(2));
						document.getElementById('total-subtotal').innerHTML = '$' +  CommaFormatted(total.toFixed(2));
						document.getElementById('total-tax').innerHTML = '$0';
					@else
						document.getElementById('total-span').innerHTML =  CommaFormatted(total.toFixed(2)) + ' Ft';
						document.getElementById('total-subtotal').innerHTML =  CommaFormatted(total.toFixed(2)) + ' Ft';
						document.getElementById('total-tax').innerHTML = '0 Ft'
					@endif
				}
				else if (is_valid_vat == 0)
				{
						subtotal = total * 100 / 127;
						tax = total - subtotal;
						@if ($shoppingcartsetting['currency'] == 'USD')
							document.getElementById('total-subtotal').innerHTML = "$" +  CommaFormatted(subtotal.toFixed(2));
							document.getElementById('total-tax').innerHTML = "$" +  CommaFormatted(tax.toFixed(2));
							document.getElementById('total-span').innerHTML = "$" +  CommaFormatted(total.toFixed(2));
						@else
							document.getElementById('total-subtotal').innerHTML = CommaFormatted(subtotal.toFixed(2)) + " Ft";
							document.getElementById('total-tax').innerHTML = CommaFormatted(tax.toFixed(2)) + " Ft";
							document.getElementById('total-span').innerHTML = CommaFormatted(total.toFixed(2)) + " Ft";
						@endif
				}
				else if (is_valid_vat == 1)
				{
						subtotal = total * 100 / 127;
						tax = total - subtotal;
						@if ($shoppingcartsetting['currency'] == 'USD')
							document.getElementById('total-subtotal').innerHTML = "$" +  CommaFormatted(subtotal.toFixed(2));
							document.getElementById('total-tax').innerHTML = "$" +  0;
							document.getElementById('total-span').innerHTML = "$" +  CommaFormatted(subtotal.toFixed(2));
						@else
							document.getElementById('total-subtotal').innerHTML = CommaFormatted(subtotal.toFixed(2)) + " Ft";
							document.getElementById('total-tax').innerHTML = 0 + " Ft";
							document.getElementById('total-span').innerHTML = CommaFormatted(subtotal.toFixed(2)) + " Ft";
						@endif
				}


				document.getElementById('account-span').innerHTML = document.getElementById('account').value;

				var name = document.getElementById('name').value;
				var company = document.getElementById('company-name').value;
				var city = document.getElementById('city').value;
				var country = document.getElementById('country').value;
				var street = document.getElementById('street_address').value;
				var postcode = document.getElementById('postcode').value;
				if (company)
				{
					city = document.getElementById('company-city').value;
					country = document.getElementById('company-country').value;
					street = document.getElementById('company-street_address').value;
					postcode = document.getElementById('company-postcode').value;
					document.getElementById('billing-info-tax').innerHTML = document.getElementById('tax_num').value;
					document.getElementById('billing-info-vat').innerHTML = document.getElementById('eu_vat_num').value;
					document.getElementById('billing-info-name').innerHTML = company;
				}
				else
				{
					document.getElementById('billing-info-name').innerHTML = name;
				}
				document.getElementById('billing-info-address').innerHTML = street + ' ' + city + ' ' + country;
				document.getElementById('billing-info-postcode').innerHTML = postcode;

				if (value == "barion")
				{
					document.getElementById('GoPay').innerHTML = '{!! trans("order.span64") !!}';
					document.getElementById('payment-img-div').innerHTML = '<img src="/images/order_barion_logo.png">';
				}
				else if (value == "paypal")
				{
					document.getElementById('GoPay').innerHTML = '{!! trans("order.span63") !!}';
					document.getElementById('payment-img-div').innerHTML = '<img src="/images/order-paypal-logo.png">';
				}
				else
				{
					document.getElementById('GoPay').innerHTML = '{!! trans("order.span42") !!}';
					document.getElementById('payment-img-div').innerHTML = '<img src="/images/order-bank-transfer.png">';
				}

				selectTab(4);
			}
			else if (index == 4)
			{
				var payment = document.getElementById('payment_input').value;
				var route = '';
				if (payment == "paypal") route = '/payment/paypal';
				else if (payment == "barion") route = '/payment/barion';
				else route = '/payment/bank';
				document.getElementById("payment-confirm-form").action = route;

				 $('#payment-confirm-form').submit();
				/*$.ajax({ // create an AJAX call...
					data: $('#payment-confirm-form').serialize(), // get the form data
					type: $('#payment-confirm-form').attr('method'), // GET or POST
					url: route, // the file to call
					success: function(response) { // on success..
						console.log(response);
						return true;
					}
				});					*/
				//$('#w1').bootstrapWizard('show',4);
			}
			return false;
		},
		onTabClick: function( tab, navigation, index, newindex ) {
			/*if ( newindex == index + 1 ) {
				return this.onNext( tab, navigation, index, newindex);
			} else if ( newindex > index + 1 ) {
				return false;
			} else {
				return true;
			}*/
			return false;
		},
		onTabChange: function( tab, navigation, index, newindex ) {
			/*var totalTabs = navigation.find('li').length - 1;
			$w1finish[ newindex != totalTabs ? 'addClass' : 'removeClass' ]( 'hidden' );
			$('#w1').find(this.nextSelector)[ newindex == totalTabs ? 'addClass' : 'removeClass' ]( 'hidden' );*/
			return false;
		}
	});

	@if ($user)
		//console.log('success-login');
		selectTab(2);
	@endif

	$('.accordion .item .heading').click(function() {

		var a = $(this).closest('.item');
		var b = $(a).hasClass('open');
		var c = $(a).closest('.accordion').find('.open');

		if(b != true) {
			$(c).find('.content').slideUp(200);
			$(c).removeClass('open');
		}

		$(a).toggleClass('open');
		$(a).find('.content').slideToggle(200);

	});

	OnCountry();
	@if (Session::get('results-Payment'))//$result_message = Session::get('result'))
		if ("{{Session::get('results-Payment')}}" == "Payment success")
		{
			document.getElementById('confirmation-div').style.display = 'none';
			document.getElementById('result-div-success').style.display = 'inline';
		}
		else if ("{{Session::get('results-Payment')}}" == "Payment failed")
		{
			document.getElementById('confirmation-div').style.display = 'none';
			document.getElementById('result-div-failed').style.display = 'inline';
		}
		else
		{
			document.getElementById('confirmation-div').style.display = 'none';
			document.getElementById('result-div-pending').style.display = 'inline';
		}
		selectTab(4);

	@endif
});


function selectTab (index)
{
	//w1-tab1 w1-step1
	//console.log('selecttab');
	//console.log(index);
	currentTab = index;
	//console.log('onNext', currentTab);
	$('#w1').bootstrapWizard('show',parseInt(index-1));
	// for (var i = 1; i < 5; i++)
	// {
  //
	// 	tabid = 'w1-tab' + i;
	// 	stepid = 'w1-step' + i;
  //
  //
	// 	if (i == index)
	// 	{
	// 		document.getElementById(tabid).classList.add('active');
	// 		document.getElementById(stepid).classList.add('active');
	// 	}
	// 	else
	// 	{
	// 		document.getElementById(tabid).classList.remove('active');
	// 		document.getElementById(stepid).classList.remove('active');
	// 	}
	// }
	//console.log(index-1);
	//$('#w1').bootstrapWizard('show',(index-1));
}

function goRegisterDiv(){
	document.getElementById('register-div').style.display = 'inline';
	document.getElementById('login-div').style.display = 'none';
}

function goLoginDiv(){
	document.getElementById('register-div').style.display = 'none';
	document.getElementById('login-div').style.display = 'inline';
}

function toggleAgree() {
    var isAgree = document.getElementById("agreeForService").checked

}

function goCompanyInfo()
{
	document.getElementById('billing-personal-div').style.display = 'none';
	document.getElementById('billing-company-div').style.display = 'inline';
	document.getElementById('company-country-div').style.width = '100%';

}

function goPersonalInfo()
{
	document.getElementById('billing-personal-div').style.display = 'inline';
	document.getElementById('billing-company-div').style.display = 'none';
}

function OnCountry()
{
	country_name = document.getElementById('company-country').value;
	var region = countries[country_name];

	//if (region == "Europe" && country_name != "Hungary" && notEuropeUnionlist.indexOf(country_name) == -1)
	if (EuropeUnionlist.indexOf(country_name) != -1 && country_name != "Hungary")
	{
		document.getElementById('tax-div').style.display='none';
		document.getElementById('vat-div').style.display='inline';
	}
	else
	{
		document.getElementById('tax-div').style.display='inline';
		document.getElementById('vat-div').style.display='none';
	}
}


</script>
@endsection
