<!DOCTYPE html>
<html lang="en" style="position: relative;min-height: 100%;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	@if ($blog->seo_description == '' && $blog->seo_keywords == '' && $blog->seo_title == '')
		@if (isset($settings))
			<meta name="description" content="{{$settings['siteDesc']}}">
			<meta name="keywords" content="{{$settings['keywords']}}">
			<title>{{$settings['siteTitle']}}</title>
		@endif
	@else
		<meta name="description" content="{{$blog->seo_description}}">
		<meta name="keywords" content="{{$blog->seo_keywords}}">

		<title>{{$blog->seo_title}}</title>
	@endif
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    {{--<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue-light_blue.min.css" />--}}
    <link rel="stylesheet" href="/css/material.min.css" />
    <link rel="stylesheet" href="/css/styles.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="{{ Config::get('RELATIVE_URL') }}/assets/vendor/pnotify/pnotify.custom.css" />
    <link rel="stylesheet" type="text/css" href="https://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.css" />
	  <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.css" />



    <script src="/assets/vendor/modernizr/modernizr.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.js"></script>
    <script src="/assets/vendor/pnotify/pnotify.custom.js"></script>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

    <link href="/assets/audio-player/css/audioPlayer.css" rel="stylesheet"/>
    <script src="/assets/audio-player/js/audioPlayer.js"></script>


    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
	  <!--GaCode-->
	@if (isset($settings))
		<?php echo html_entity_decode($settings['gaCode'])?>
	@endif
	<!--End GaCode-->
    <!--Facebook Pixel Code-->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1692560164346282');
fbq('track', "PageView");
<?php echo html_entity_decode($blog->facebook_pixel_code)?>

</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1692560164346282&ev=PageView&noscript=1"
/></noscript>

    <!--End Facebook Pixel Code-->
</head>
<style>
body{
	line-height: 28px;
}
p {
	font-size:16px	;
}
h1{
	font-size:26px	;
}
h2{
	font-size:25px	;
}
h3{
	font-size:24px	;
}
h4{
	font-size:22px	;
}
h5{
	font-size:20px	;
}
h6{
	font-size:17px	;
}

#Reulst-div-content::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	border-radius: 10px;
	background-color: #F5F5F5;
}

#Reulst-div-content::-webkit-scrollbar
{
	width: 12px;
	background-color: #F5F5F5;
}

#Reulst-div-content::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	background-color: #399aed;
}

.answer_word {
	outline:none;
	border: none;
	border-bottom: 2px solid #f29f00;
	text-align:center;
    min-width: 150px;
    max-width: 700px;
}

.accordion {
  width: 380px;
  border-radius: 5px;
  overflow: hidden;
  margin: auto;
}

.accordion .item .heading {
  height: 50px;
  line-height: 50px;
  font-size: 17px;
  cursor: pointer;
  color: #fff;
  padding-left: 15px;
  background: #006e73 url('/images/accordion_arrow.png') no-repeat;
  background-position: right 20px top -95px;
  border-bottom: 1px solid #ec8484;
  box-sizing: border-box;
}

.accordion .item.open .heading,
.accordion .item:last-child .heading { border: 0; }

.accordion .item.open .heading { background-position: right 20px top -5px; }

.accordion .item .content {
  display: none;
  padding: 15px;
  background: #fff;
  font-size: 14px;
}

.phone-footer-accordion {
  width: 380px;
  border-radius: 5px;
  overflow: hidden;
  margin: auto;
}

.phone-footer-accordion .item .heading {
  height: 50px;
  line-height: 50px;
  font-size: 15px;
  cursor: pointer;
  color: #ffffff;
  padding-left: 15px;
  background: #006469 url('/images/phone_footer_accordion.png') no-repeat;
  background-position: right 20px top -95px;
  border-bottom: 1px solid #4d9a9d;
  box-sizing: border-box;
  text-align:left;
}

.phone-footer-accordion .item.open .heading,
.phone-footer-accordion .item:last-child .heading { border: 0; }

.phone-footer-accordion .item.open .heading { background-position: right 20px top -5px; }

.phone-footer-accordion .item .content {
  display: none;
  padding: 15px;
  background: #006469;
  font-size: 14px;
}

</style>
<style>
	#try-it-button-second{
		color:#399aed;
		background-color:#ffffff;
		width:200px;
	}

	#try-it-button-second:hover{
		color:#399aed;
		background-color:#dfe8ef;
		font-weight:bold;
	}
	#send-button{
		background-color:#f29f00;
		border-color:#f29f00;

	}

	#send-button:hover{
		background-color:#f29f00;
		border-color:#f29f00;
	}

	#sub-button {
		margin-left:30px;
		background-color:#ffffff;
		color:#399aed;
		border:none;
		outline:none;
	}

	#sub-button:hover {
		margin-left:30px;
		background-color:#dfe8ef;
		color:#399aed;
	}

	.socialbuttons .facebook{
		width:40px;
		height:40px;
		background:url('/images/social-facebook-icon.png') center/cover;
	}
	.socialbuttons .facebook:hover{
		background:url('/images/social-facebook-icon-hover.png') center/cover;
	}
	.socialbuttons .twitter{
		width:40px;
		height:40px;
		background:url('/images/social-twitter-icon.png') center/cover;
	}
	.socialbuttons .twitter:hover{
		background:url('/images/social-twitter-icon-hover.png') center/cover;
	}
	.socialbuttons .goggle{
		width:40px;
		height:40px;
		background:url('/images/social-goggle-icon.png') center/cover;
	}
	.socialbuttons .goggle:hover{
		background:url('/images/social-goggle-icon-hover.png') center/cover;
	}
	.socialbuttons .insta{
		width:40px;
		height:40px;
		background:url('/images/social-youtube-icon.png') center/cover;
	}
	.socialbuttons .insta:hover{
		background:url('/images/social-youtube-icon-hover.png') center/cover;
	}

	.link1{
		color:#66abae;
	}
	.link1:hover{
		color:#ffffff;
		text-decoration:none;
	}
	.recent-links{
		color:#000000;
		text-decoration:none;
	}

	.recent-links:hover {
		text-decoration:none;
	}
	.category-links{
		color:#000000;
		text-decoration:none;
	}
  .sidenav_links{
  	color:#000000;
  	text-decoration: none !important;
  }
	.category-links:hover{
		text-decoration:none;
	}
	#create_name:hover{
		color:#399aed;
	}
</style>
<style>

  .fb-page, .fb-page:before, .fb-page:after {
    border: 1px solid #ccc;
  }

  .fb-page:before, .fb-page:after {
    content: "";
    position: absolute;
    bottom: -3px;
    left: 2px;
    right: 2px;
    height: 1px;
    border-top: none
  }

  .fb-page:after {
    left: 4px;
    right: 4px;
    bottom: -5px;
    box-shadow: 0 0 2px #ccc
  }
</style>
<style>
html, body {
	font-family: 'Roboto', sans-serif;
}
.navbar-default {
	width:100%;
	background:rgba(255,255,255,0);
	margin:0;
	border:none;

}

.navbar-default .navbar-nav{
	padding-right:15px;
}

.navbar-default .navbar-nav > li{

	padding-right:25px;
}

.navbar-default .navbar-nav .dropdown{

}

.navbar-default .navbar-nav > li > a{
	color:#ffffff;
	font-size: 14px;
  font-weight: 550;
	text-transform: uppercase;
}

.navbar-default .navbar-nav > li > a:hover{
    color:#399aed;
	font-size: 14px;
  font-weight: 550;
}

.navbar-default .navbar-nav > li > a:focus{
    color:#399aed;
	font-size: 14px;
  font-weight: 550;
}

.navbar-default .navbar-nav > .open > a{
	background:rgba(255,255,255,0);
	color:#ffffff;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a:hover{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav > .open > a:focus{
	background:rgba(255,255,255,0);
    color:#399aed;
	font-size: 14px;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu{
	margin-top:10px;
	padding:15px 20px 15px 20px;
	background:#399aed
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li{
	padding:0;
	border-bottom:1px solid #338ad5;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a{
	padding:10px 0px 10px 0px ;
	color:#ffffff;
}

.navbar-default .navbar-nav .dropdown .dropdown-menu > li > a:hover{
	background:#399aed;
	color:#117a97;
}

#dropdown2:before {
	position: absolute;
	top: -9px;
	left:120px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

#dropdown1:before {
	position: absolute;
	top: -9px;
	left:60px;
	display: inline-block;
	border-right: 9px solid transparent;
	border-bottom: 9px solid #399aed;
	border-left: 9px solid transparent;
	content: '';
}

.navbar-default .navbar-toggle {
		border:none;
		padding-right:15px;
}

.navbar-default .first-nav {

}


#header-div-homepage
{
	width:100%;
	padding:10px 0 10px 0;
	margin:0;
  background: #006e73;
}

.fixed {
	-webkit-transform: translate3d(0,0,0);
	position:fixed;
	top:0px;
	background-color:#006e73;
	-webkit-box-shadow: 0px 2px 5px 1px #004a4d;
   -moz-box-shadow: 0px 2px 5px 1px #004a4d;
        box-shadow: 0px 2px 5px 1px #004a4d;
	z-index:999;
	padding:0px 0 0px 0;
}
#my-account-button{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #f29f00;
	width:150px;
	height:40px;
}

#my-account-button:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #d98f00;
}

#my-account-button-logged{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #399aed;
	width:150px;
	height:40px;
}

#my-account-button-logged:hover{
	margin:6px;
	color:#fff;
	font-size: 14px;
	background: #2081d5;
}

#try-it-button-second{
	color:#399aed;
	background-color:#ffffff;
	width:200px;
}

#try-it-button-second:hover{
	color:#399aed;
	background-color:#dfe8ef;
	font-weight:bold;
}

#scroll-div{
	color:#c0d9da;
}

#scroll-div:hover{
	color:#cde1e1;
}

.a-next-prev {
	padding-bottom:150px;
	float:right;
	color:black;
	font-size:40px;
}

.a-next-prev:hover {
	padding-bottom:150px;
	float:right;
	color:#399aed;
	font-size:40px;
}

.socialbuttons .facebook{
	width:40px;
	height:40px;
	background:url('/images/social-facebook-icon.png') center/cover;
}
.socialbuttons .facebook:hover{
	background:url('/images/social-facebook-icon-hover.png') center/cover;
}
.socialbuttons .twitter{
	width:40px;
	height:40px;
	background:url('/images/social-twitter-icon.png') center/cover;
}
.socialbuttons .twitter:hover{
	background:url('/images/social-twitter-icon-hover.png') center/cover;
}
.socialbuttons .goggle{
	width:40px;
	height:40px;
	background:url('/images/social-goggle-icon.png') center/cover;
}
.socialbuttons .goggle:hover{
	background:url('/images/social-goggle-icon-hover.png') center/cover;
}
.socialbuttons .insta{
	width:40px;
	height:40px;
	background:url('/images/social-youtube-icon.png') center/cover;
}
.socialbuttons .insta:hover{
	background:url('/images/social-youtube-icon-hover.png') center/cover;
}

.link1{
	color:#66abae;
}
.link1:hover{
	color:#ffffff;
	text-decoration:none;
}

#homepage-title {
	font-size:60px;
	font-weight:300;
	line-height:65px;
}

#homepage-title-div{
	text-align:left !important;
}

#try-it-button{
	font-size:16px;
	height:50px;
	width:280px;
	margin:10px;
}

#try-it-button-div{
	text-align:left !important;
}

.section3-text-div{
	text-align:left !important;
}

#owl-carousel{
	width:70%;
}

#hero-div{
	margin-top:100px;
}

#header-background-div
{
	background:url('/images/homepage-hero-background.png');
	background-repeat: no-repeat;

}
span{
	color:#000000;
}

#footer-div-bottom {
	width: 60% !important;
}
@media (max-width: 1777px)
{
	#footer-div-bottom {
		width: 70% !important;
	}

	#homepage-title {
		font-size:45px;
		font-weight:300;
		line-height:50px;
	}

	#try-it-button{
		width:220px;
	}
}

@media (max-width: 1530px) {
	#hero-div{
		margin-top:50px;
	}

	#footer-links-div{
		width: 70% !important;
	}
	#footer-div-bottom {
		width: 80% !important;
	}

}

@media (max-width: 1386px)
{

	#hero-div{
		margin-top:50px;
	}
	#homepage-title {
		font-size:40px;
		font-weight:300;
		line-height:50px;
	}
	#hero-div-content{
		width:85%;
	}
	#footer-div-bottom {
		width: 87% !important;
	}

}
@media (max-width: 1245px) {
  #footer-links-div	{
		width: 90% !important;
  }

  #footer-div-bottom {
  	width: 100% !important;
  }
}

@media (max-width:1110px){
	#footer-div-bottom{
		padding:0 !important;
	}
	#footer-div-bottom a{
		margin-left: 10px !important;
	}

	#homepage-title {
		font-size:30px;
		font-weight:300;
		line-height:40px;
	}
	#try-it-button{
		width:180px;
	}
}

@media (max-width: 950px) {
  #footer-links-div	{
		width: 100% !important;
  }
  #homepage-title-div, #try-it-button-div {
	  text-align:center !important;
  }

}
@media (max-width: 400px) {
	#homepage-title {
		font-size:25px;
		font-weight:300;
		line-height:35px;
	}

	#try-it-button{
		font-size:15px;
		width:180px;
	}

	#header-div-homepage
	{
		padding:5px 0 0px 0 !important;
		margin:0;
	}

	#nav-bar{
		margin:0;

	}

}

@media (max-width: 1850px) {
	.footer {
		width:80%;
	}

  .main-content {
    width:80% !important;
  }
  #nav-bar{
    width:80% !important;
  }
}

@media (max-width: 1400px) {
	.footer {
		width:90%;
	}
  .main-content {
    width:90% !important;
  }
  #nav-bar{
    width:90% !important;
  }
}

@media (max-width: 1250px) {
	.footer {
		width:100%;
	}
  .main-content {
    width:100% !important;
  }
  #nav-bar{
    width:100% !important;
  }
}

@media (max-width: 769px) {
    .navbar-default .navbar-collapse {
        background:#ffffff;
		    float:right;
		    width:50%;
    }

	.navbar-default .navbar-nav > li > a{
		color:#6c937b;
	}

	.navbar-default .navbar-nav > li {
		border-bottom: 1px solid #eeeeee;
	}

  #main_page_div{
    margin:0px 10px 0px 10px !important;
  }
}

</style>
<style>

#counter, #counter1, #counter2{
  padding:0px 5px 0px 5px;
}
.btn-fb{background:linear-gradient(#849fcd,#45649c,#6d87ba); padding: 0px 15px 0px 10px;color: #fff;border: 1px solid #4b6aa0;}
.btn-fb1{background:#fff; border: 1px solid #e4e4e4; color: #e4e4e4;}
.fa-facebook1{background:linear-gradient(#849fcd,#45649c,#6d87ba); border: 1px solid #4b6aa0; border-radius: 10px 0 0 10px;}
.fa-facebook1:hover{background:linear-gradient(#6d87ba,#45649c,#849fcd); cursor:pointer;  }

.btn-tw{background:linear-gradient(#7fcbf0,#2989ca,#68b3e0); padding: 0px 15px 0px 10px;color: #fff;border: 1px solid #15b2d9;}
.btn-tw1{background:#fff; border: 1px solid #ddd;color: #ddd;}
.fa-twitter1{background: linear-gradient(#7fcbf0,#68b3e0,#2989ca);  border: 1px solid #5bcae6; border-radius: 10px 0 0 10px;}
.fa-twitter1:hover{background: linear-gradient(#7fcbf0,#2989ca,#68b3e0);cursor: pointer; }

.btn-gplus{background: linear-gradient(#ee9d8d,#e75a43,#f68573); padding: 0px 15px 0px 10px; color:#fff;border: 1px solid #e35e25;}
.btn-gplus1{background:#fff; border: 1px solid #ddd;color: #ddd;}
.fa-google1{background: linear-gradient(#ee9d8d,#f68573,#e75a43);  border: 1px solid #e75a43; border-radius: 10px 0 0 10px;}
.fa-google1:hover{background: linear-gradient(#ee9d8d,#e75a43,#f68573);cursor: pointer; }
.btn-group .btn {
  height:25px;
}
#counter, #counter1, #counter2 {
  margin-top:1px;
  height:23px;
}
.mt1{margin-top:38px;}
#update1 > iframe {
  margin:0;
  display: inline !important;
  opacity: 0;
}
#update2  iframe {
  opacity: 0 !important;
}



@media (max-width: 1205px){


	.navbar-default .navbar-nav {
		padding-right:0px;
	}

	.navbar-default .navbar-nav > li {
		padding-right:0px !important;
	}

}

@media (max-width: 840px)
{
	.navbar-header {
        float: none;
    }
    .navbar-left {
        float: none !important;
    }
    .navbar-toggle {
        display: block;
    }
    .navbar-collapse {
        border-top: 1px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
    }
    .navbar-fixed-top {
        top: 0;
        border-width: 0 0 1px;
    }

    .navbar-collapse.collapse {
        display: none!important;
    }

    .navbar-nav {
        float: none!important;
        margin-top: 7.5px;
		margin-right:0px !important;
    }

	#dropdown1, #dropdown2, #dropdown3{
		padding:0px 10px 0px 10px;

	}

    .navbar-nav>li {
        float: none;
    }
    .navbar-nav>li>a {
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .collapse.in{
        display:block !important;
    }
}

@media (max-width: 769px) {
	#header-div{
		background: #006e73;
		padding:20px 10px 20px 10px;
	}
    .navbar-default .navbar-collapse {
        background:#ffffff;
		float:right;
		width:50%;
		z-index:99999;
    }
	.navbar-default{
		padding-right:0px;
	}
	.navbar-default .navbar-nav > li > a{
		color:#6c937b !important;
	}
	.navbar-default .navbar-nav > li {
		border-bottom: 1px solid #eeeeee;
	}

	.navbar-default .navbar-toggle{
		padding-top:5px;
	}

	.navbar-default .navbar-toggle i{
		font-size:18px;
	}

	.navbar-brand img{
		height:14px;
		width: auto;
	}

	#footer-1div{
		text-align:center;
	}

	#footer-2div{
		text-align:center;
	}

	#footer-3div{
		text-align:center;
	}

	#dropdown3:before {
		display: none !important;
	}

	#dropdown2:before {
		display: none !important;
	}

	#dropdown1:before {
		display: none !important;
	}

	#my-account-button{
		width:100px;
		height:40px;
		font-size:10px;
	}
	#my-account-button-logged{
		font-size: 10px;
		width:100px;
		height:40px;
	}
	.footer-link{
		font-size:12px;
	}

	#div-footer {
		height:270px;
	}
}

@media (max-width: 767px) {
    .navbar-default .navbar-collapse {
        background:#ffffff;
		float:right;
		width:50%;
    }

	.navbar-default .navbar-nav > li > a{
		color:#6c937b;
	}
	.navbar-default .navbar-nav > li {
		border-bottom: 1px solid #eeeeee;
	}
}
</style>
<body style="padding:0px; min-height:100%;margin: 0;overflow-x: hidden">
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
<!--<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header" style="width:100%;min-height:100%">-->
<div id='Parent-Div-Background' style="display:inline;position:relative;width:100%;min-height:100%;background-color:#ebf3ef;">
  <div id="header-div-homepage" class="mdl-grid mdl-cell mdl-cell--12-col mdl-grid--no-spacing" style="justify-content:center">
    <div id="nav-bar" class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;padding:0px 0px 0px 0px;">
      <nav class="navbar navbar-default"  style="">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <i aria-hidden="true" class="fa fa-navicon" style="color:#ffffff"></i>
            </button>
            <a class="navbar-brand" href="/"><img src='/images/logo.png'></img></a>
          </div>
          <div class="collapse navbar-collapse  navbar-right" id="myNavbar">
            <ul class="nav navbar-nav">
              <li><a id="header-span1" href="https://imprevo.hu/what-is-imprevo">A Imprevz?</a></li>
              <li class="dropdown">
                <a id="header-span2" class="dropdown-toggle" data-toggle="dropdown" href="#">KURZUSOK</a>
                <ul id="dropdown1" class="dropdown-menu">
                  <li><a id="header-span3" class="a-submenu" href="https://imprevo.hu/sales/course/10">Angol tanfolyam </a></li>
                  <li><a id="header-span4" class="a-submenu" href="https://imprevo.hu/sales/course/11">Hétköznapi angol </a></li>
                </ul>
              </li>
              <li><a id="header-span5" href="https://imprevo.hu/blog">Blog</a></li>
              <li class="dropdown">
                <a id="header-span6" class="dropdown-toggle" data-toggle="dropdown2" href="https://imprevo.hu/ingyenes-tartalmak">Ingyenesek<!--<span class="caret"></span>--></a>
                <ul id="dropdown2" class="dropdown-menu">
                  <li><a id="header-span7" class="a-submenu" href="https://imprevo.hu/20-részes-ingyenes-angol-tanfolyam">20 részes email tanfolyam</a></li>
                  <li><a id="header-span8" class="a-submenu" href="https://imprevo.hu/tanulj-meg-angolul-6-hónap-alatt-az-imprevo-val">6 hónap alatt angolul</a></li>
                  <li><a id="header-span9" class="a-submenu" href="https://imprevo.hu/angol-igeidők-ebook">Angol igeidők</a></li>
                  <li><a id="header-span10" class="a-submenu" href="https://imprevo.hu/angol-levelek-írása">Angol levelek írása</a></li>
                </ul>
              </li>
            </ul>
            <ul  id="nav-right-ul" class="nav navbar-nav navbar-right">


            </ul>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
          </div>
        </div>
      </nav>
    </div>
  </div>
	<main class="mdl-layout__content" style="display:inline;background-color:#ebf3ef;width:100%;margin:0px;padding:0px;padding-bottom:200px;">
	<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="width:100%;justify-content:center;margin:0px;width:100%;background: #9fc3ae;padding:0px 0px 0px 0px;font-size: 16px;height:250px">
		<div class="main-content mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0px;margin:0px">
			<span><a href="/" style="padding-left:15px;color:#d5e7dc;line-height: 40px;text-decoration: none">{!! trans('home.imprevo') !!}</a></span>
			<span style="padding-left:10px;color:#d5e7dc;line-height: 40px">/</span>
			<span style="padding-left:10px;color:#d5e7dc;line-height: 40px">{!! trans('blog.blog') !!}</span>
			<span style="padding-left:10px;color:#d5e7dc;line-height: 40px">/</span>
			<span style="padding-left:10px;color:#fff;line-height: 40px">{{$blog->title}}</span>
		</div>
	</div>
    <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="width:100%;margin:0;justify-content:center;padding:0px;margin-top:-210px">
        <div class="main-content mdl-grid mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="justify-content:center;padding:0px;margin:0px">
          <div id="main_page_div" class="mdl-grid mdl-cell mdl-cell--9-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid--no-spacing" style="background-color:#ffffff;padding:0px;margin:0;">
			<div id="post-title" class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0px 0px 20px;">
                <span style="font-size:25px; font-weight:bold">{{$blog->title}}</span>
				<hr style="height:1px; background-color;#eee; margin:15px 0px 15px 0px">
                <span style="width:100%; padding:0px 0px 0px 0px;font-size:14px; color:#909090">{!! trans('blog.by') !!} <span id="create_name"> {{$blog->createdBy->name}} </span>| <?php $phpdate = strtotime($blog->created_at); echo date('M d Y', $phpdate); ?> | 0 {!! trans('blog.comment') !!}</span>
				<hr style="height:1px; background-color;#eee; margin:15px 0px 15px 0px">
        <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="margin:0; padding:0">
      		<div class="btn-group mt1">
      			<button type="button" class="btn btn-fb fa fa-facebook" style="font-size: 12px;border-radius: 7px 0 0 7px;"></button>
      			<button type="button" id="update" class="btn btn-fb fa-facebook1" style="outline:none;width:68px; text-align:center;z-index:10; border-radius: 7px; margin-left: -8px; //box-shadow: 0px 1px 1px #3e5a8c;">
              Share
            </button>
      		</div>

      		<div class="btn-group mt1" style="margin-left: 20px;">
      			<button type="button" class="btn btn-tw fa fa-twitter" style="font-size: 12px;border-radius: 7px 0 0 7px;"></button>
      			<button type="button" id="update1" class="twitter popup btn btn-tw fa-twitter1" data-size="large" style="outline:none;position:relative;z-index:10; padding:0; width:72px; border-radius: 7px; margin-left: -8px; box-shadow: 0px 1px 1px #67a5cf;">
              Tweet
            </button>
      		</div>

      		<div class="btn-group mt1" style="margin-left: 20px;">
      			<button type="button" class="btn btn-gplus fa fa-google-plus-square" style="font-size: 12px;border-radius: 7px 0 0 7px;"></button>
      			<button type="button" id="update2" class="btn btn-gplus fa-google1 "style="outline:none;z-index:10; width:72px; padding:0;border-radius: 7px; margin-left: -8px; box-shadow: 0px 1px 1px #e75a43;">
              Google+
            </button>
      		</div>
        </div>
        </div>
			<div id="main-content" class="mdl-cell mdl-cell mdl-cell--12-col" style="margin:0px;padding:25px 20px 0px 20px;">
				<?php echo html_entity_decode($blog->content)?>
			</div>
            <div id="pagination-div" class="mdl-cell mdl-cell mdl-cell--12-col" style="float:bottom;padding:0px 20px 0px 20px;text-align:right;"></div>
			<div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--hide-phone">
                @include('disqus')
            </div>
          </div>
          <div class="mdl-cell mdl-cell mdl-cell--3-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="background-color:#ebf3ef;padding:0;margin:0">
			<div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0px;background:url('/images/newsletter-background.png') no-repeat center/cover;padding:0px 0px 0px 0px;height:210px;width:100%">
				<div class="mdl-cell mdl-cell--12-col" style="padding-top:15px;padding-left:35px;">
					<span style="font-size:30px; color:#ffffff">{!! trans('blog.span1') !!}</span>
				</div>
				<div class="mdl-cell mdl-cell--12-col" style="padding-left:35px;">
					<span style="font-size:16px; color:#ffffff">{!! trans('blog.span2') !!}</span>
				</div>
				<div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
					<div class="mdl-cell mdl-cell--10-col" style="margin:0; padding:0;">
						<form action="http://sendy.imprevo.info/subscribe" method="POST" accept-charset="utf-8" style="width:100%;margin:0;padding:0">
							<div class="input-group" style="margin:0; padding:0;">
								<input type="text" id="email" name="email" class="form-control" placeholder="Email cím">
								<input type="hidden" name="list" value="dWVr892XDx8pprHN8763yTtK6w"/>
								<span class="input-group-btn">
									<button id="send-button" class="btn btn-primary" type="submit" style="">
										<i aria-hidden="true" class="fa fa-arrow-right"></i>
									</button>
								</span>
							</div>
						</form>
					</div>
				</div>
			</div>
<!--            @if ($blog['is_image'])
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0 20px 0; justify-content:center">
               <div id="featured_image" style="width: 100%; min-height: 100px; background: url('{{$blog->featured_image}}') center / cover;">
               </div>
            </div>
            @endif-->
            @if ($recent_blogs)
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0px 10px 10px">
                <span style="color:#3b4a51;font-weight:bold; font-size:14px">{!! trans('blog.span3') !!}</span>
            </div>
                @foreach($recent_blogs as $rblog)
                  @if ($rblog != '')
                    <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0px; border-bottom:1px solid #eee;padding-left:30px; padding-bottom:10px">
                      <a class="recent-links" href="/blog/{{$rblog}}" style=""><span style="padding-right:10px">></span>{{$rblog}}</a>
                    </div>
                  @endif
                @endforeach
            @endif
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0 10px 10px">
              <span style="color:#3b4a51;font-weight:bold; font-size:14px">{!! trans('blog.span4') !!}</span>
            </div>
            @foreach(\App\Blogcat::all() as $blogcat)
            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="border-bottom:1px solid #eee;padding-left:30px; padding-bottom:10px">
              <a class="category-links" href="/blog/category/{{$blogcat->title}}" style=""><span style="padding-right:10px">></span>{{$blogcat->title}} ({{count($blogcat->blogs)}})</a>
            </div>
            @endforeach
			<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:10px 0 10px 10px">
			<a href="https://www.azevhonlapja.hu/nevezes/benyujtott-palyamu/524/imprevo-online-angol">
				<img alt="Év honlapja verseny! Szavazz ránk!" src="https://www.azevhonlapja.hu/userfiles/Szavazoi_bannerek/2017/eh_2017_szavazas_160300_v2.gif" style="display: block; margin: 0 auto;" />
			</a>
			</div>

			<div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0 0px 10px">
				<span style="color:#3b4a51;font-weight:bold; font-size:14px">{!! trans('blog.span5') !!}</span>
			</div>
            <div class="mdl-cell mdl-cell mdl-cell--12-col" onclick="location.href='https://imprevo.hu/20-r%C3%A9szes-ingyenes-angol-tanfolyam'" style="cursor:pointer;background:url('/images/banner.png') no-repeat center/cover; height:230px; ">
            </div>

            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0px 10px 10px">
              <span style="color:#3b4a51;font-weight:bold; font-size:14px">Hasznos anyagaink</span>
            </div>

              <div class="mdl-cell mdl-cell mdl-cell--12-col" style="border-bottom:1px solid #eee;padding-left:30px; padding-bottom:10px">
              	<a class="sidenav_links" href="https://imprevo.hu/20-részes-ingyenes-angol-tanfolyam"><span style="padding-right:10px">></span>20 részes ingyenes angol tanfolyam</a>
              </div>
				      <div class="mdl-cell mdl-cell mdl-cell--12-col" style="border-bottom:1px solid #eee;padding-left:30px; padding-bottom:10px">
				               <a class="sidenav_links" href="https://imprevo.hu/angol-igeidőkÖsszefoglaló-táblázat"><span style="padding-right:10px">></span>Angol igeidők összefoglaló táblázat</a>
				      </div>
        <div class="mdl-cell mdl-cell mdl-cell--12-col" style="border-bottom:1px solid #eee;padding-left:30px; padding-bottom:10px">
				      <a class="sidenav_links" href="https://imprevo.hu/blog/tanulási-stílusok"><span style="padding-right:10px">></span>Tanulási stílusok</a>
        </div>
        <div class="mdl-cell mdl-cell mdl-cell--12-col" style="border-bottom:1px solid #eee;padding-left:30px; padding-bottom:10px">
				      <a class="sidenav_links" href="https://imprevo.hu/meg-lehet-tanulni-az-imprevo-val-angolul"><span style="padding-right:10px">></span>Meg lehet tanulni az Imprevo-val angolul</a>
        </div>
        <div class="mdl-cell mdl-cell mdl-cell--12-col" style="border-bottom:1px solid #eee;padding-left:30px; padding-bottom:10px">
				      <a class="sidenav_links" href="https://imprevo.hu/ingyenes-tartalmak"><span style="padding-right:10px">></span>Ingyenes anyagok</a>
        </div>


            <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:20px 0 20px 0px">
         <!--     <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="standard" data-action="like" data-show-faces="true" data-share="false"></div>-->
				<div class="demo-card-square mdl-card mdl-shadow--2dp" style="margin-top: 1.5rem;width:100%;">
					<h2 class="mdl-card__title-text" style="padding-left: 1rem; margin-top: 1rem; align-self: flex-start;">Kövess minket!</h2>
					<div class="mdl-card__supporting-text" style="width:100%">
						<div class="fb-page" data-href="https://www.facebook.com/imprevo" data-small-header="false" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
						</div>
					</div>
				</div>
            </div>

          </div>
        </div>
    </div>
    <form id="pageForm" role="form" action="/blog/{{$blog->static_url}}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id="page" name="page" />
    </form>
</main>
	<div id="section4-phone" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="margin:0;width:100%;background-color:#399aed;padding:40px 0px 40px 0px;justify-content:center">
		<span style="color:#ffffff; font-size:30px; padding-top:5px;padding-right:20px">Próbáld ki az IMPREVO-t még ma ingyen
		</span>
		<button id="try-it-button-second" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
			PRÓBÁLD KI INGYEN!
		</button>
	</div>
	<div id="section4" class="mdl-grid mdl-cell mdl-cell--hide-desktop mdl-cell--4-col-phone" style="margin:0;width:100%;background-color:#399aed;padding:40px 0px 40px 0px;justify-content:center">

			<div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center; ">
				<span style="color:#ffffff; font-size:30px;">Próbáld ki az IMPREVO-t még ma ingyen
				</span>
			</div>
			<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center; ">
				<button id="try-it-button-second" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
					PRÓBÁLD KI INGYEN!
				</button>
			</div>

	</div>
	@include('layouts.footer')
</div>
<script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<script src="/assets/vendor/jquery/jquery.js"></script>
<script src="/assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/vendor/jquery-validation/jquery.validate.min.js"></script>

<script src="/assets/vendor/bootstrap/js/bootstrap.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="/assets/javascripts/theme.js"></script>

<!-- Theme Custom -->
<script src="/assets/javascripts/theme.custom.js"></script>

<!-- Theme Initialization Files -->
<script src="/assets/javascripts/theme.init.js"></script>
<script>
pagination_html = '<hr class="solid mb-none" />';
pagination_html = pagination_html + '<ul class="pagination" style="float:bottom">';
var page_count = JSON.parse('<?php echo json_encode($page_count)?>');
var page_index = JSON.parse('<?php echo json_encode($page_index)?>');

if (page_count > 1){
for (i=0; i<page_count; i++)
{
  if (i== (page_index-1))
  {
      if (i==0){
        pagination_html = pagination_html + '<li class="active">\
          <a href="/blog/{{$blog->static_url}}" onclick="pageIndex(' + (i+1) + ')">'+ (i+1) + '</a>\
        </li>';
      }
      else {
        pagination_html = pagination_html + '<li class="active">\
          <a href="/blog/{{$blog->static_url}}?page='+(i+1)+'" onclick="pageIndex(' + (i+1) + ')">'+ (i+1) + '</a>\
        </li>';
      }
  }
  else
  {
    if (i==0){
      pagination_html = pagination_html + '<li>\
        <a href="/blog/{{$blog->static_url}}" onclick="pageIndex(' + (i+1) + ')">'+ (i+1) + '</a>\
      </li>';
    }
    else {
      pagination_html = pagination_html + '<li>\
        <a href="/blog/{{$blog->static_url}}?page='+(i+1)+'" onclick="pageIndex(' + (i+1) + ')">'+ (i+1) + '</a>\
      </li>';
    }
  }
}
}
pagination_html = pagination_html +  '</ul>\</div>';
document.getElementById('pagination-div').innerHTML = pagination_html;
var quiz_list = JSON.parse('<?php echo json_encode($quiz_list)?>');
@if ($quiz_list)
  @foreach ($quiz_list as $quiz)
  var html = '<div  id="quiz-div-seletImage-{{$quiz->id}}"  style="display:none;width:100%;margin: 30px 0px 0px 0px; padding:0px">\
                <div style="padding:3px 0px 0px 0px; text-align:center;">\
                    <p id="quiz_selectImage_displayword-{{$quiz->id}}" name="WordDisplay" style="color:#9fc3ae; font-size: 16px"></p>\
                            </div>\
                            <div style="margin:3px 0px 0px 0px; text-align:center;">\
                                <p id="quiz_selectImage_displaycorrectword-{{$quiz->id}}" name="WordDisplay" style="color:#9fc3ae; font-size: 16px">&nbsp;</p>\
                            </div>\
                            <div class="mdl-grid mdl-cell--12-col" style="min-height:280px;justify-content:center;padding:0px 40px 0px 0px">\
                                <div class="demo-card-image mdl-card mdl-cell mdl-cell--3-col mdl-cell--3-col-tablet mdl-shadow--4dp" style="position:relative;top: 0;margin-left:50px;border-radius:8px;width: 200px;height: 180px;padding:5px">\
                                    <div id="quiz-selectImage-word1-img-{{$quiz->id}}" onclick="QuizNextClick(this,{{$quiz->id}})" style=" width:190px;height:170px;">\
                                        <div class="mdl-card__title mdl-card--expand"></div>\
                                        <div class="mdl-card__actions" style="margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0);">\
                                            <input type="hidden" id="quiz-selectImage-word1-text-{{$quiz->id}}" name="span" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                                             font-weight: bold "></input>\
                                        </div>\
										<img id="copyright-img-1" src="/images/copyright.png" width="20" height="20" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="ClickCopyright(this,event)" style="display:inline;z-index:99;position: relative; left:162px; top:-170px">\
                                    </div>\
								</div>\
                                <div class="demo-card-image mdl-card mdl-cell mdl-cell--3-col mdl-cell--3-col-tablet mdl-shadow--4dp" style="position:relative;top: 0;margin-left:20px;border-radius:8px;width: 200px;height: 180px;padding:5px">\
                                    <div id="quiz-selectImage-word2-img-{{$quiz->id}}" onclick="QuizNextClick(this, {{$quiz->id}})" style="width: 190px;height: 170px;">										\
                                        <div class="mdl-card__title mdl-card--expand"></div>\
                                        <div class="mdl-card__actions" style="text-align: center;margin-top:95px;height: 52px; padding: 16px; background: rgba(50, 83, 58, 0);">\
                                            <input type="hidden" id="quiz-selectImage-word2-text-{{$quiz->id}}" name="span" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                                            font-weight: bold"></input>\
                                        </div>\
										<img id="copyright-img-2" src="/images/copyright.png" width="20" height="20" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="ClickCopyright(this,event)" style="z-index:2;position: relative; left:162px; top:-170px">\
									</div>\
								</div>								\
                            </div>\
                            <div style="text-align:center;margin:0px 0px 0px 0px">\
                                <button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButton({{$quiz->id}})" style="background-image:url('+'/upload/image/speaker-icon.png'+');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
                                </button>\
                            </div>\
            </div>\
            <div  id="quiz-div-singchoice-{{$quiz->id}}"  style="display:none;width:100%;margin: 30px 0px 0px 0px; padding:0px">\
                <div style="padding:0px 0px 0px 0px; text-align:center;">\
                    <p id="quiz-singchoice-SentenceDisplay-{{$quiz->id}}" name="SentenceDisplay" style="font-size:20px;color:#9fc3ae"></p>\
                </div>\
                <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="min-height:350px;padding:20px 0px 0px 0px; justify-content: center;">								\
                    <div id="quiz-singchoice-buttons-{{$quiz->id}}" class="mdl-cell mdl-cell--8-col" style="margin:0px 0px 0px 0px; padding:0px 0px 0px 0px">\
                    </div>\
                </div>\
            </div>\
			<div  id="quiz-div-typeaudio-{{$quiz->id}}"  style="display:none;width:100%;margin: 30px 0px 0px 0px; padding:0px">\
                <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="min-height:280px;padding:20px 0px 0px 0px; justify-content: center;">								\
						<div class="mdl-cell mdl-cell--12-col" style="text-align:center;padding:0;margin:0px 0px 0px 0px">\
							<button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButton({{$quiz->id}})" style="background-image:url('+'/upload/image/speaker-icon.png'+');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
							</button>\
						</div>\
						<div class="mdl-cell mdl-cell--6-col" style="padding:0;text-align:center;margin:0px 0px 0px 0px">	\
							<input class="answer_word" type="text" id="input_typeaudio_word-{{$quiz->id}}" name="input_typeaudio_word-{{$quiz->id}}" style="width:100%">																\
						</div>							\
                </div>\
                <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">\
                  <div id="quiz-audio-confirmation-correct-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="display:inline;height:30px;text-align:center;">\
                    <span style="color:#008000"><img src="/images/correct.png" /> {!! trans('exercise.exercise-span15') !!}</span>\
                  </div>\
                  <div id="quiz-audio-confirmation-incorrect-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="display:none;height:30px;text-align:center;">\
                    <span style="color:#ff0000"><img src="/images/incorrect.png" /> {!! trans('exercise.exercise-span16') !!}</span>\
                  </div>\
                  <div id="quiz-audio-confirmation-empty-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="display:none;color:#ff0000; height:30px; text-align:center">\
                    {!! trans('exercise.exercise-span12') !!}\
                  </div>									\
                </div>	\	</div> \
			<div  id="quiz-div-typeimage-{{$quiz->id}}"  style="display:none;width:100%;margin: 30px 0px 0px 0px; padding:0px">                        \
                <div class="mdl-grid mdl-cell--12-col" style="min-height:280px;justify-content:center;padding:0px 0px 0px 0px">\
                    <div class="demo-card-image mdl-card mdl-cell mdl-cell--3-col mdl-cell--3-col-tablet mdl-shadow--4dp" id="quiz-typeimage-card" style="position:relative;top: 0;margin-left:10px;border-radius:8px;width: 200px;height: 180px;padding:5px">\
                        <div id="quiz-typeImage-word-img-{{$quiz->id}}" style=" width:190px;height: 170px;">\
                            <div class="mdl-card__title mdl-card--expand"></div>\
                            <div class="mdl-card__actions" style="margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0);">\
                                <input type="hidden" id="quiz-typeImage-word-text-{{$quiz->id}}" name="span" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                                 font-weight: bold "></input>\
                            </div>\
							<img id="copyright-img" src="/images/copyright.png" width="20" height="20" onmouseover="mouseover(this)" onmouseout="mouseout(this)" onclick="ClickCopyright(this,event)" style="z-index:2;position: relative; left:162px; top:-170px">\
                        </div>\
					</div>\
					<div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="padding:20px 0px 0px 0px; justify-content: center;">								\
						<div class="mdl-cell mdl-cell--12-col" style="text-align:center;padding:0;margin:0px 0px 0px 0px">\
							<button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButton({{$quiz->id}})" style="background-image:url('+'/upload/image/speaker-icon.png'+');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
							</button>\
						</div>\
						<div class="mdl-cell mdl-cell--6-col" style="padding:30px 0px 30px 0px;text-align:center;margin:0px 0px 0px 0px">	\
							<input class="answer_word" type="text" id="input_typeimage_word-{{$quiz->id}}" name="input_typeimage_word-{{$quiz->id}}" style="width:100%">\
						</div>\
					</div>\
                </div>\
                <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">\
  								<div id="quiz-image-confirmation-correct-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="text-align:center;display:none;height:30px">\
  									<span style="color:#008000"><img src="/images/correct.png" /> {!! trans('exercise.exercise-span15') !!}</span>\
  								</div>\
  								<div id="quiz-image-confirmation-incorrect-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="text-align:center;display:none;height:30px">\
  									<span style="color:#ff0000"><img src="/images/incorrect.png" /> {!! trans('exercise.exercise-span16') !!}</span>\
  								</div>\
  								<div id="quiz-image-confirmation-empty-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="display:none;color:#ff0000; height:30px; text-align:center">\
  									{!! trans('exercise.exercise-span12') !!}\
  								</div>\
  							</div>	\
			</div>\
			<div id="quiz-div-gapfill-{{$quiz->id}}"  style="display:none; width:100%;margin: 30px 0px 0px 0px; padding:0px">\
        <div id="gapfill-space-div-{{$quiz->id}}" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="padding:20px 0px 0px 0px; justify-content: center;min-height:280px;">								\
					<div id="gapfill-div-{{$quiz->id}}" class="mdl-cell mdl-cell--9-col" style="text-align:center;">\
					</div>	\
				</div>\
        <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">\
          <div id="quiz-gapfill-confirmation-correct-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="display:none;height:30px;text-align:center;">\
            <span style="color:#008000"><img src="/images/correct.png" /> {!! trans('exercise.exercise-span15') !!}</span>\
          </div>\
          <div id="quiz-gapfill-confirmation-incorrect-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="display:none;height:30px;text-align:center;">\
            <span style="color:#ff0000"><img src="/images/incorrect.png" /> {!! trans('exercise.exercise-span16') !!}</span>\
          </div>\
          <div id="quiz-gapfill-confirmation-empty-{{$quiz->id}}" class="mdl-cell mdl-cell--5-col" style="display:none;color:#ff0000; height:30px; text-align:center">\
            {!! trans('exercise.exercise-span13') !!}\
          </div>\
        </div>		\
      </div>\
			<div  id="Result-div-{{$quiz->id}}" style="display:none;margin: 30px 0px 0px 0px; padding:0px">\
						<div style="padding:10px 0px 0px 0px; text-align: center">\
                                <p style="font-weight:bold;font-size: 20px">{!! trans("exercise.exercise-span1") !!}</p>\
                                <p style="font-weight:bold;font-size: 20px">{!! trans("exercise.exercise-span2") !!}</p>\
                          </div>\
                          <div style="padding:0px 0px 0px 0px; text-align:center;">\
                              <p id="result-score-{{$quiz->id}}" name="result-score" style="font-size:20px;color:#9fc3ae">0</p>\
                          </div>\
                          <div id="Reulst-div-content-{{$quiz->id}}" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="min-height:350px;margin:0px 0px 0px 0px;padding: 0px 0px 0px 0px">\
                          </div>\
						</div>\
              </div>\
              <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="float:bottom;padding:10px 0px 15px 0px">\
                  <hr style="color:#eee; height:1px; margin:5px 0px 0px 0px">\
              </div>\
              <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="min-height:70px;margin:0px 0px 10px 20px;padding: 10px 0px 0px 0px">\
                 <div id="progress-{{$quiz->id}}" class="mdl-cell mdl-cell--7-col" style="width:100%;padding:0px 0px 0px 0px;margin:0px 0px 0px 0px">\
                   <div id="quiz-result-playagain-btn-{{$quiz->id}}"  style="min-height:50px;display:none;margin:0px 0px 0px 0px;padding:0px 0px 0px 0px">\
                       <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect imprevo-default" style="margin:0px 0px 20px 0px" onclick="playagain({{$quiz->id}})">\
                           {!! trans("exercise.exercise-but2") !!}\
                       </button>\
                   </div>\
                   <div id="quiz-progress-div-{{$quiz->id}}" style="margin-top:10px; width:100%; height: 7px;background-color: #eeeeee;">\
                          <div id="quiz-progress-{{$quiz->id}}" style="background: #399aed;width:0%;height: 7px"></div>\
                          <img id="quiz-progress-thumb-{{$quiz->id}}" src="/images/slider-thumb.png" width="40" height="40" style="position: relative;\
                          left: 0%;top: -24px;z-index: 1;margin:0px 0px 0px 0px">\
                          <span id="quiz-progress-text-{{$quiz->id}}", style="width:15px;height:16px;color:#399aed;font-size:10px;position: relative;\
                          left: -6%;top:10px;z-index: 2;margin:0px 0px 0px 0px">0%</span>\
                  </div>\
                </div>\
            </div>';
            document.getElementById('quiz-part-{{$quiz->id}}').innerHTML = html;
		$('#input_typeaudio_word-{{$quiz->id}}').on('keyup', function (e) {
			if (e.keyCode == 13) {
			// Do something
				var str = document.getElementById('input_typeaudio_word-{{$quiz->id}}');
				if (str == '')
					return;

				QuizNextClick(null, "{{$quiz->id}}");
			}
		});

		$('#input_typeimage_word-{{$quiz->id}}').on('keyup', function (e) {
			if (e.keyCode == 13) {
			// Do something
				var str = document.getElementById('input_typeimage_word-{{$quiz->id}}');
				if (str == '')
					return;
				QuizNextClick(null,"{{$quiz->id}}");
			}
		});

  @endforeach


  function onKeyUp_gapfill(event, tag_el, quiz_id)
  {
	console.log('keyup');
	inputWidth = $('#'+tag_el.id).val().length * 8;
	$('#'+tag_el.id).css({
		width: inputWidth
	})

	if (event.keyCode == 13)
	{
		var input_elements = document.getElementsByName('input_gapfill-' + quiz_id);
		for (i in input_elements)
		{
			if (input_elements[i].value == ''){
				input_elements[i].focus();
				return;
			}
		}
		QuizNextClick(null, quiz_id);
	}
  }
  var questions = {};
  var questions_random_array = {};
  var questions_result_array={};
  var question_select_image_step = {};
  var quiz_question_length = {};
  var quiz_step = {};
  var result_status = {};
  var status_question = {};
  var words = [];
  var quiz_parent_top = {};
  var quiz_obj_top = {};
  var IsProcessing = {};
  var audio_button_url={};

  @foreach (\App\Word::all() as $word)
	word = {};
	word.id = "{{$word->id}}".trim();
	word.wordset_index = "{{$word->wordset_index}}".trim();
	word.wordset_id = "{{$word->wordset_id}}".trim();
	word.source_word = "{{$word->source_word}}";
	word.translation = "{{$word->translation}}";
	word.copyright_url = "{{$word->copyright_url}}";
	word.image = "{{$word->image}}";
	word.audio = "{{$word->audio}}";
	word.note = "{{$word->note}}";
	words.push(word);
  @endforeach

  @foreach ($quiz_list as $quiz)
    questions["{{$quiz->id}}"] = null;
    questions_random_array["{{$quiz->id}}"] = [];
    questions_result_array["{{$quiz->id}}"]=[];
    question_select_image_step["{{$quiz->id}}"] = 0;
    quiz_question_length["{{$quiz->id}}"] = 0;
    quiz_step["{{$quiz->id}}"] = 0;

    result_status["{{$quiz->id}}"] = false;
    status_question["{{$quiz->id}}"] = 0;

    quiz_parent_top["{{$quiz->id}}"] = 0;
    quiz_obj_top["{{$quiz->id}}"] = 0;
    IsProcessing["{{$quiz->id}}"] = 0;
	audio_button_url["{{$quiz->id}}"] = 0;
  @endforeach


  @foreach ($quiz_list as $quiz)
      QuizInit("{{$quiz->id}}");
  @endforeach
@endif

function pageIndex(page)
{
	  event.preventDefault();
    event.stopPropagation();
	  document.getElementById('page').value = page;
	  $('#pageForm').submit();
}

function QuizInit(quiz_id)
{
    @foreach ($quiz_list as $quiz)
      if (quiz_id == "{{$quiz->id}}"){
        questions_random_array["{{$quiz->id}}"] = [];
        questions["{{$quiz->id}}"] = null;
        questions_result_array["{{$quiz->id}}"]=[];
        question_select_image_step["{{$quiz->id}}"] = 0;
        quiz_question_length["{{$quiz->id}}"] = 0;
        quiz_step["{{$quiz->id}}"] = 0;
        result_status["{{$quiz->id}}"] = false;
        status_question["{{$quiz->id}}"] = 0;

        document.getElementById('Result-div-{{$quiz->id}}').style.display = 'none';
        document.getElementById('quiz-div-seletImage-{{$quiz->id}}').style.display = 'none';
        document.getElementById('quiz-div-singchoice-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-div-typeaudio-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-div-gapfill-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-div-typeimage-{{$quiz->id}}').style.display = 'none';

		questions["{{$quiz->id}}"]=[];
		@foreach ($quiz->questions as $question)
			question = {};
			question.id = "{{$question->id}}".trim();
			question.instruction = <?php echo json_encode($question->instruction)?>;
			question.note = <?php echo json_encode($question->note)?>;;
			question.type = "{{$question->type}}";
			question.answer_data = <?php echo json_encode($question->answer_data)?>;
			question.quiz_id = "{{$question->quiz_id}}";
			question.correct_answer_id = <?php echo json_encode($question->correct_answer_id)?>;//"{{$question->correct_answer_id}}";
			questions["{{$quiz->id}}"].push(question);
		@endforeach

		quizsettings = [];
		@foreach (\App\QuizSetting::all() as $quizsetting)
			quizsetting = {};
			quizsetting.term = <?php echo json_encode($quizsetting->term)?>.trim().split('|');
			quizsettings.push(quizsetting);
		@endforeach

        if (questions.length == 0)
        {
            document.getElementById('quiz-progress-{{$quiz->id}}').style.width = 0;
            return;
        }


          for (var i = 0; i< questions["{{$quiz->id}}"].length; i++)
          {
              if (questions["{{$quiz->id}}"][i].type == 1)
              {
                  questions_random_array["{{$quiz->id}}"].push(i);
              }
              else if (questions["{{$quiz->id}}"][i].type == 2)
              {
                  Answer_data = questions["{{$quiz->id}}"][i].answer_data;
                  ArrayAnswer = Answer_data.split(',');
                  RandomArrayAnswer = Answer_data.split(',');
                  var b = false;
                  while(!b){
                      shuffle(RandomArrayAnswer);
                      b = true;
                      for (j in ArrayAnswer)
                      {
                          if (ArrayAnswer[j] == RandomArrayAnswer[j])
                          {
                              b = false
                          }
                      }
                  }

                  for (answer in ArrayAnswer)
                  {
                      questions_random_array["{{$quiz->id}}"].push([i, ArrayAnswer[answer].trim(), RandomArrayAnswer[answer].trim()]);
                  }
              }
			  else if (questions["{{$quiz->id}}"][i].type == 3)
			  {

			  	Answer_data = questions["{{$quiz->id}}"][i].answer_data;
			  	ArrayAnswer = Answer_data.split(',');

			  	for (answer in ArrayAnswer)
			  	{
			  		questions_random_array["{{$quiz->id}}"].push([i, ArrayAnswer[answer].trim()]);
			  	}
			  }
			  else if (questions["{{$quiz->id}}"][i].type == 4)
			  {

			  	Answer_data = questions["{{$quiz->id}}"][i].answer_data;
			  	ArrayAnswer = Answer_data.split(',');

			  	for (answer in ArrayAnswer)
			  	{
			  		questions_random_array["{{$quiz->id}}"].push([i, ArrayAnswer[answer].trim(), 0, 0]);
			  	}
			  }
			  else if (questions["{{$quiz->id}}"][i].type == 5)
			  {
			  	var instruction = questions["{{$quiz->id}}"][i].instruction;
			  	var ArrayAnswer = [];
			  	var instruction_array = instruction.split("\{\{\{");
			  	var instruction_elements = [];

			  	for (var l in instruction_array)
			  	{
			  		var element = instruction_array[l];
			  		element_split = element.split("\}\}\}");
			  		if (element_split.length == 2)
			  		{
			  			ArrayAnswer.push(element_split[0]);
			  			instruction_elements.push('input');
			  			instruction_elements.push(element_split[1]);
			  		}
			  		else
			  		{
			  			instruction_elements.push(element_split[0]);
			  		}
			  	}
			  	questions_random_array["{{$quiz->id}}"].push([i, instruction_elements, ArrayAnswer, quizsettings, 0]);
			  }
          }

          quiz_question_length["{{$quiz->id}}"] = questions_random_array["{{$quiz->id}}"].length;
  		  @if ($quiz)
  		  	@if($quiz->random_type > 1)
  		  	 shuffle(questions_random_array["{{$quiz->id}}"]);
  		  	@endif
  		  @endif
      }
    @endforeach
    QuizStep(null, quiz_id);
}
function removeSpecialChars(str) {
	return str.replace(/(?!\w|\s)./g, '')
			  .replace(/\s+/g, ' ')
			  .replace(/^(\s*)([\W\w]*)(\b\s*$)/g, '$2');
}

function RemovePunctuations(str)
{
	//console.log('------------------');
	//console.log(str);
	str = str.replace(/[!&\/\\#,+()$~%.":*?<>{}]/g, '');
	//console.log(str);
	return str;
}


function QuizNextClick(obj, quiz_id)
{
  @foreach ($quiz_list as $quiz)
  if ("{{$quiz->id}}" == quiz_id)
  {
    if (IsProcessing["{{$quiz->id}}"]) return;
    IsProcessing["{{$quiz->id}}"] = true;
    var question_info;
    var step = 0;

    if (quiz_step["{{$quiz->id}}"] == 0)
    {
        step = quiz_question_length["{{$quiz->id}}"]-1;
    }
    else
    {
        step = quiz_step["{{$quiz->id}}"]-1;
    }

    if (quiz_step["{{$quiz->id}}"] > 0 || (quiz_step["{{$quiz->id}}"]==0 && result_status["{{$quiz->id}}"]))
    {
        if (quiz_step["{{$quiz->id}}"] == 0)
        {
            question_info = questions_random_array["{{$quiz->id}}"][step];
        }
        else
        {
            question_info = questions_random_array["{{$quiz->id}}"][step];
        }
        var youranswer = '';
        if (question_info.length == 3) {
            var word = null;
            for (i in words) {
                if (words[i].wordset_id == "{{$quiz->wordset_id}}" && words[i].wordset_index == question_info[1]) {
                    word = words[i];
                }
            }
            childs = obj.children;

            textSpan = childs[1].children;

            youranswer = textSpan[0].innerText;
            if (word.translation == youranswer)
            {

              if (status_question["{{$quiz->id}}"] == 0)
                questions_result_array["{{$quiz->id}}"].push([1,youranswer]);
              status_question["{{$quiz->id}}"] = 0;
            }
            else
            {
              if (status_question["{{$quiz->id}}"] == 0)
                questions_result_array["{{$quiz->id}}"].push([0,youranswer]);
              status_question["{{$quiz->id}}"] = 1;
            }
        }
		else if (question_info.length == 4) {
              var word = null;
              for (i in words) {
                  if (words[i].wordset_id == "{{$quiz->wordset_id}}" && words[i].wordset_index == question_info[1]) {
                      word = words[i];
                  }
              }
			  youranswer = document.getElementById('input_typeimage_word-{{$quiz->id}}').value;

				var textElement = document.createElement('textarea');
				textElement.innerHTML = word.source_word.toLowerCase();
				correctanswer = textElement.innerText;
				textElement.remove();
              console.log(correctanswer);
              if (correctanswer.trim() == youranswer.toLowerCase().trim())
              {
                //if (status == 0)
                  questions_result_array["{{$quiz->id}}"].push([1,youranswer]);
				  status_question["{{$quiz->id}}"] = 0;
              }
              else
              {
                //if (status == 0)
                  questions_result_array["{{$quiz->id}}"].push([0,youranswer]);
				 status_question["{{$quiz->id}}"] = 1;
              }
		  }
		  else if (question_info.length == 5) {

			    var input_elements = document.getElementsByName("input_gapfill-{{$quiz->id}}");
				var answer_data_array = question_info[2];
				var youranswers = [];
				status_question["{{$quiz->id}}"] = 1;
				var terms = question_info[3];
				for (i in input_elements)
				{
					if (!input_elements[i].value) continue;
					youranswers.push(input_elements[i].value);
					var element = RemovePunctuations(answer_data_array[i]).trim().toLowerCase();
					var elements = element.split('|');
					var status_element = false;
					var terms = question_info[3];
					for (j in elements)
					{
						if (status_element == true) break;
						for (p in terms)
						{
							term = terms[p].term;
							var term_current = null;
							if (status_element == true) break;
							for (h in term)
							{
								if (elements[j].includes(term[h].toLowerCase()))
								{
									include_str = term[h].toLowerCase();
									term_current = term;
									if (status_element == true) break;
									for (p in term_current)
									{
										if (status_element == true) break;
										var replace_answer = elements[j].replace(include_str, term_current[p].toLowerCase());
										if (replace_answer == RemovePunctuations(input_elements[i].value.toLowerCase()).trim())
										{
											status_element = true;
											break;
										}
									}
									break;
								}
							}
						}

						if (elements[j] == RemovePunctuations(input_elements[i].value.toLowerCase()).trim())
						{
							status_element = true;
							break;
						}

					}
					if (status_element == false){
						 status_question["{{$quiz->id}}"] = 0;
					}
				}

				if ( status_question["{{$quiz->id}}"] == 1)
				{
					questions_result_array["{{$quiz->id}}"].push([1,youranswers]);
				}
				else
				{
					questions_result_array["{{$quiz->id}}"].push([0,youranswers]);
				}

		  }
		  else if (question_info.length == 2) {
              var word = null;
              for (i in words) {
                  if (words[i].wordset_id == "{{$quiz->wordset_id}}" && words[i].wordset_index == question_info[1]) {
                      word = words[i];
                  }
              }
			  youranswer = document.getElementById('input_typeaudio_word-{{$quiz->id}}').value;
				var textElement = document.createElement('textarea');
				textElement.innerHTML = word.source_word.toLowerCase();
				correctanswer = textElement.innerText;
				textElement.remove();
			  if (correctanswer.trim() == youranswer.toLowerCase().trim())
              {
                //if (status == 0)
                  questions_result_array["{{$quiz->id}}"].push([1,youranswer]);
                 status_question["{{$quiz->id}}"] = 0;
              }
              else
              {
                //if (status == 0)
                  questions_result_array["{{$quiz->id}}"].push([0,youranswer]);
                 status_question["{{$quiz->id}}"] = 1;
              }
		  }
		  else
		  {
				var question = questions["{{$quiz->id}}"][question_info];
				childs = obj.children;
				textSpan = childs[0];
				youranswer = textSpan.innerHTML;
				if (youranswer.toLowerCase().trim() == question.correct_answer_id.toLowerCase().trim())
				{
					if (status_question["{{$quiz->id}}"] == 0)
						questions_result_array["{{$quiz->id}}"].push([1,youranswer]);
					status_question["{{$quiz->id}}"] = 0;
				}
				else
				{
					if (status_question["{{$quiz->id}}"] == 0)
						questions_result_array["{{$quiz->id}}"].push([0,youranswer]);
					status_question["{{$quiz->id}}"] = 1;
				}
		  }
    }



      if (question_info.length == 3) {
		 parent = obj.parentNode;
        if (status_question["{{$quiz->id}}"] == 0)
        {
            parent.style.backgroundColor = '#5c9f77';
            document.getElementById("quiz_selectImage_displaycorrectword-{{$quiz->id}}").innerHTML = youranswer;
        }
        else
        {
            parent.style.backgroundColor = '#ff0000'

            setTimeout(function(){BouncingEffect(parent, '-20px');}, 100);
            setTimeout(function(){BouncingEffect(parent, '20px');}, 200);
            setTimeout(function(){BouncingEffect(parent, '-15px');}, 300);
            setTimeout(function(){BouncingEffect(parent, '15px');}, 400);
            setTimeout(function(){BouncingEffect(parent, '-10px');}, 500);
            setTimeout(function(){BouncingEffect(parent, '10px');}, 600);
            setTimeout(function(){BouncingEffect(parent, '-5px');}, 700);
            setTimeout(function(){BouncingEffect(parent, '5px');}, 800);
            setTimeout(function(){BouncingEffect(parent, '0px');}, 900);
            quiz_step["{{$quiz->id}}"]--;

        }
      }
		else if (question_info.length == 2)
		{
			if (status_question["{{$quiz->id}}"] == 0)
			{
				$('#input_typeaudio_word-{{$quiz->id}}').css('border-bottom', '2px solid #008000');
				document.getElementById('quiz-audio-confirmation-correct-{{$quiz->id}}').style.display = 'inline';
			}
			else
			{
				$("#input_typeaudio_word-{{$quiz->id}}").css('border-bottom', '2px solid #ff0000');
				document.getElementById('quiz-audio-confirmation-incorrect-{{$quiz->id}}').style.display = 'inline';
			}
		}
		else if (question_info.length == 4)
		{
			if (status_question["{{$quiz->id}}"] == 0)
			{
				$("#input_typeimage_word-{{$quiz->id}}").css("border-bottom", "2px solid #008000");
				document.getElementById('quiz-image-confirmation-correct-{{$quiz->id}}').style.display = 'inline';
			}
			else
			{
				$("#input_typeimage_word-{{$quiz->id}}").css("border-bottom", "2px solid #ff0000");
				document.getElementById('quiz-image-confirmation-incorrect-{{$quiz->id}}').style.display = 'inline';
				//quiz_step--;
			}
		}
		else if (question_info.length == 5)
		{
			var input_elements = document.getElementsByName("input_gapfill-{{$quiz->id}}");
			var answer_data_array = question_info[2];
			var terms = question_info[3];
      console.log('anser_data_array', answer_data_array);
			for (i in input_elements)
			{
				if (!input_elements[i].value) continue;
				var element = RemovePunctuations(answer_data_array[i]).trim().toLowerCase();
				var elements = element.split('|');
				var status_element = false;

				for (j in elements)
				{
					if (status_element) break;
					for (p in terms)
					{
						term = terms[p].term;
						var term_current = null;
						if (status_element == true) break;
						for (h in term)
						{
							if (term_current) break;
							if (elements[j].includes(term[h].toLowerCase()))
							{
								include_str = term[h].toLowerCase();
								term_current = term;
								if (status_element == true) break;
								for (p in term_current)
								{
									if (status_element == true) break;
									var replace_answer = elements[j].replace(include_str, term_current[p].toLowerCase());
									if (replace_answer == RemovePunctuations(input_elements[i].value.toLowerCase()).trim())
									{
										status_element = true;
										break;
									}
								}
								break;
							}
						}
					}

					if (elements[j] == RemovePunctuations(input_elements[i].value.toLowerCase()).trim())
					{
						status_element = true;
						break;
					}
				}

				if (status_element == false){
					$('#input_gapfill-{{$quiz->id}}'+i).css("border-bottom", "2px solid #ff0000");
					document.getElementById('quiz-gapfill-confirmation-incorrect-{{$quiz->id}}').style.display = 'inline';
				}
				else
				{
					$('#input_gapfill-{{$quiz->id}}'+i).css("border-bottom", "2px solid #008000");
					document.getElementById('quiz-gapfill-confirmation-correct-{{$quiz->id}}').style.display = 'inline';
				}
			}
		}
      else
      {
        if (status_question["{{$quiz->id}}"] == 0)
        {
            obj.style.backgroundColor = '#5c9f77';
        }
        else
        {
            obj.style.backgroundColor = '#ff0000'
            obj.style.top = obj.style.top - 10;
            quiz_step["{{$quiz->id}}"]--;
        }
      }
  }


  @endforeach

   setTimeout(function(){QuizStep(obj, quiz_id);}, 1000);
}

function BouncingEffect(obj, l)
{
  if (obj)
  {
        obj.style.top = l;
  }
}

function QuizStep(obj, quiz_id)
{
  @foreach ($quiz_list as $quiz)
    if ("{{$quiz->id}}" == quiz_id)
    {
      IsProcessing["{{$quiz->id}}"] = false;

      document.getElementById('Result-div-{{$quiz->id}}').style.display = 'none';
	  $("#input_typeaudio_word-{{$quiz->id}}").css("border-bottom", "2px solid #f29f00");
	  $('#input_typeaudio_word-{{$quiz->id}}').val('');

	  $("#input_typeimage_word-{{$quiz->id}}").css("border-bottom", "2px solid #f29f00");
	  $('#input_typeimage_word-{{$quiz->id}}').val('');

	  	document.getElementById('quiz-gapfill-confirmation-incorrect-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-gapfill-confirmation-correct-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-audio-confirmation-incorrect-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-audio-confirmation-correct-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-image-confirmation-incorrect-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-image-confirmation-correct-{{$quiz->id}}').style.display = 'none';
      if (obj)
      {
        parent = obj.parentNode;
        obj.style.backgroundColor = '#f4f4f4';
        obj.style.top = quiz_obj_top["{{$quiz->id}}"];

        parent.style.backgroundColor = '#ffffff'
        parent.style.top = quiz_parent_top["{{$quiz->id}}"];
        document.getElementById("quiz_selectImage_displaycorrectword-{{$quiz->id}}").innerHTML = '&nbsp;'
      }
      if (quiz_step["{{$quiz->id}}"] == -1 && status_question["{{$quiz->id}}"] != 0)
      {

          quiz_step["{{$quiz->id}}"] = quiz_question_length["{{$quiz->id}}"]-1;
          result_status["{{$quiz->id}}"] = false;
      }

      if (!result_status["{{$quiz->id}}"]) {

        document.getElementById('quiz-div-seletImage-{{$quiz->id}}').style.display = 'none';
        document.getElementById('quiz-div-singchoice-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-div-typeaudio-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-div-typeimage-{{$quiz->id}}').style.display = 'none';
		document.getElementById('quiz-div-gapfill-{{$quiz->id}}').style.display = 'none';
        document.getElementById('quiz-result-playagain-btn-{{$quiz->id}}').style.display = 'none';
        document.getElementById('quiz-progress-div-{{$quiz->id}}').style.visibility = 'visible';

          var question_info = questions_random_array["{{$quiz->id}}"][quiz_step["{{$quiz->id}}"]];

          var index_num = question_info[0];
          var question = questions["{{$quiz->id}}"][question_info];
          if (question_info.length == 3) {

              var word = null;
              var random_word = null;
              for (i in words) {
				if (words[i].wordset_id == "{{$quiz->wordset_id}}"){
                  if (words[i].wordset_index == question_info[1]) {
                      word = words[i];
                  }
                  if (words[i].wordset_index == question_info[2]) {
                      random_word = words[i];
                  }
				  if (word && random_word) break;
				}
              }
			  if (word)
				playSound(word.audio);
			  audio_button_url["{{$quiz->id}}"] = word.audio;
              document.getElementById('quiz-div-seletImage-{{$quiz->id}}').style.display = 'inline';
              document.getElementById('quiz_selectImage_displayword-{{$quiz->id}}').innerHTML = word.source_word;

              var display_order = 0;
              if (status_question["{{$quiz->id}}"]==0) display_order = Math.random();
              if (display_order < 0.5) {
                  document.getElementById('quiz-selectImage-word1-img-{{$quiz->id}}').style.background = 'url("' + word.image + '") no-repeat center/190px 170px';
                  document.getElementById('quiz-selectImage-word1-text-{{$quiz->id}}').innerHTML = word.translation;
                  document.getElementById('quiz-selectImage-word2-img-{{$quiz->id}}').style.background = 'url("' + random_word.image + '") no-repeat center/190px 170px';
                  document.getElementById('quiz-selectImage-word2-text-{{$quiz->id}}').innerHTML = random_word.translation;
              }
              else {
                  document.getElementById('quiz-selectImage-word2-img-{{$quiz->id}}').style.background = 'url("' + word.image + '") no-repeat center/190px 170px';
                  document.getElementById('quiz-selectImage-word2-text-{{$quiz->id}}').innerHTML = word.translation;
                  document.getElementById('quiz-selectImage-word1-img-{{$quiz->id}}').style.background = 'url("' + random_word.image + '") no-repeat center/190px 170px';
                  document.getElementById('quiz-selectImage-word1-text-{{$quiz->id}}').innerHTML = random_word.translation;
              }
          }
		  else if (question_info.length == 2){
				var word = null;
                var random_word = null;

                for (i in words) {
                    if (words[i].wordset_id == "{{$quiz->wordset_id}}" && words[i].wordset_index == question_info[1]) {
                        word = words[i];
                    }
                }
				if (word.audio)
					playSound(word.audio);

				audio_button_url["{{$quiz->id}}"] = word.audio;


				document.getElementById('quiz-div-typeaudio-{{$quiz->id}}').style.display = 'inline';
			}
			else if (question_info.length == 4){
				var word = null;
                var random_word = null;

                for (i in words) {
                    if (words[i].wordset_id == "{{$quiz->wordset_id}}" && words[i].wordset_index == question_info[1]) {
                        word = words[i];
                    }
                }
				if (word.audio)
					playSound(word.audio);
				audio_button_url["{{$quiz->id}}"] = word.audio;
				document.getElementById('quiz-div-typeimage-{{$quiz->id}}').style.display = 'inline';
				document.getElementById('quiz-typeImage-word-img-{{$quiz->id}}').style.background = 'url("' + word.image + '") no-repeat center/190px 170px';
				document.getElementById('quiz-typeImage-word-text-{{$quiz->id}}').innerHTML = word.translation;
			}
			else if (question_info.length == 5){
				var html = '';
				var instruction_elements = question_info[1];
				var k = 0;
        console.log('instruction_elements', instruction_elements);
				for (i in instruction_elements)
				{
					if (instruction_elements[i] == 'input')
					{
						html = html +  '<input class="answer_word" type="text" id="input_gapfill-{{$quiz->id}}' + k + '" name="input_gapfill-{{$quiz->id}}" onkeyup="onKeyUp_gapfill(event, this, {{$quiz->id}})" style="text-align:center"></input>';
						k++;
					}
					else
					{
						html = html + instruction_elements[i];
					}
				}

				document.getElementById('gapfill-div-{{$quiz->id}}').innerHTML = html;
				document.getElementById('quiz-div-gapfill-{{$quiz->id}}').style.display = 'inline';

				var gap0 = document.getElementById('input_gapfill-{{$quiz->id}}0');
				if (gap0) gap0.focus();
			}
          else {
              document.getElementById('quiz-div-singchoice-{{$quiz->id}}').style.display = 'inline';

              document.getElementById('quiz-singchoice-SentenceDisplay-{{$quiz->id}}').innerHTML = question.instruction;
			  Answer_data = question.answer_data;
              var Answerarray = Answer_data.split(',');
			  if (Answer_data.includes("||"))
					Answerarray = Answer_data.split("||");
              html = '';
			  @if($quiz)
			  @if ($quiz->random_type > 2)
				 shuffle(Answerarray);
			  @endif
			  @endif
              for (j in Answerarray) {
                  html = html + '<div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--4dp" onclick="QuizNextClick(this, {{$quiz->id}})"\
                              style="background-color:#f4f4f4;font-size:16px;text-transform: none;text-align:left;margin:15px;padding:7px;"><span style="cursor:pointer">' + Answerarray[j] + '</span></div>';

              }

              document.getElementById('quiz-singchoice-buttons-{{$quiz->id}}').innerHTML = html;
          }
          var percent = parseInt(quiz_step["{{$quiz->id}}"] * 100 / (quiz_question_length["{{$quiz->id}}"]));
          var text_percent = percent - 7;
          var text_thum = percent - 3;
          percent = percent + '%';
          text_thum = text_thum + '%';
          document.getElementById('quiz-progress-{{$quiz->id}}').style.width = percent;
          document.getElementById('quiz-progress-thumb-{{$quiz->id}}').style.left = text_thum;

          text_percent = text_percent + '%';
          document.getElementById('quiz-progress-text-{{$quiz->id}}').style.left = text_percent;
          document.getElementById('quiz-progress-text-{{$quiz->id}}').innerHTML = percent;
          quiz_step["{{$quiz->id}}"]++;

          if (quiz_step["{{$quiz->id}}"] >= quiz_question_length["{{$quiz->id}}"])
          {
              quiz_step["{{$quiz->id}}"] = 0;
              result_status["{{$quiz->id}}"] = true;
          }
          else
              result_status["{{$quiz->id}}"] = false;
      }
      else
      {
          document.getElementById('quiz-div-seletImage-{{$quiz->id}}').style.display = 'none';
          document.getElementById('quiz-div-singchoice-{{$quiz->id}}').style.display = 'none';
		  document.getElementById('quiz-div-typeaudio-{{$quiz->id}}').style.display = 'none';
		  document.getElementById('quiz-div-typeimage-{{$quiz->id}}').style.display = 'none';
		  document.getElementById('quiz-div-gapfill-{{$quiz->id}}').style.display = 'none';
          document.getElementById('quiz-progress-div-{{$quiz->id}}').style.visibility = 'hidden';
          document.getElementById('quiz-result-playagain-btn-{{$quiz->id}}').style.display = 'inline';
          document.getElementById('Result-div-{{$quiz->id}}').style.display = 'inline';

          html='';
          var score = 0;

          for (var k=0; k<quiz_question_length["{{$quiz->id}}"]; k++)
          {
              if (questions_result_array["{{$quiz->id}}"][k][0] == 1) score++;

              var question_info = questions_random_array["{{$quiz->id}}"][k];
              if (question_info.length == 3) {
                  var word = null;
				  for (i in words) {
				  	if (words[i].wordset_id == "{{$quiz->wordset_id}}"){
				  	if (words[i].wordset_index == question_info[1]) {
				  		word = words[i];
				  	}
				  	if (words[i].wordset_index == question_info[2]) {
				  		random_word = words[i];
				  	}
				  	if (word && random_word) break;
				  	}
				  }
                  if (word.note == null)
                      note = '';
                  else
                      note = word.note;
                  word_audio_url = word.audio;
                  html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                      <div class="demo-card-image mdl-card mdl-cell mdl-cell--2-col mdl-shadow--4dp" style="margin-left:20px;border-radius:10px;width: 200px;height: 200px;padding:10px">\
                      <div style="width: 180px;height: 180px; background: url(' + word.image + ') center / cover;">\
                      <div class="mdl-card__title mdl-card--expand"></div>\
                      <div class="mdl-card__actions" style="margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0.7);">\
                      <span id="quiz-result-word1-text" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                      font-weight: bold ">' + word.translation + '</span>\
                      </div>\
                      </div>\
                      <img src="/images/copyright.png" width="20" height="20" style="z-index:1;position: relative; left:155px; top:-175px">\
                      </div>\
                      <div class="mdl-grid--no-spacing mdl-grid--no-spacing-cell mdl-cell--8-col mdl-cell--4-col-tablet" style="margin-left:30px;">\
                      <div class="mdl-grid mdl-grid--no-spacing-cell mdl-cell--12-col" style="">\
                      <div class=" mdl-grid--no-spacing-cell mdl-cell--8-col" style="">\
                      <div style="padding:10px 0px 0px 0px;">\
                      <p id="source_word" style="color:#9fc3ae; font-size: 16px">'+word.source_word+'</p>\
                      </div>\
                      </div>\
                      <div class=" mdl-grid--no-spacing-cell mdl-cell--4-col" style="">\
                      <div style="float:right;margin:0px 0px 0px 0px">\
                      <button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButtionResultDiv(' + word.id + ')" style="background-image:url(' + '/upload/image/speaker-icon.png' + ');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
                      </button>\
                      </div>\
                      </div>\
                      </div>\
                      <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                      <hr>\
                      </div>\
                      <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                      <p style="font-weight: bold; margin-bottom:10px"><?php echo trans("exercise.exercise-span14") ?></p>\
                      <span>'+note+'</span>\
                      </div>\
                      </div>\
                      </div>'
              }
				else if (question_info.length == 2) {
					var word = null;
					for (i in words) {
						if (words[i].wordset_id == "{{$quiz->wordset_id}}"){
						if (words[i].wordset_index == question_info[1]) {
							word = words[i];
						}
						if (word) break;
						}
					}
					if (word.note == null)
                        note = '';
                    else
                        note = word.note;

                    html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                        <div class="demo-card-image mdl-card mdl-cell mdl-cell--2-col mdl-shadow--4dp" style="margin-left:20px;border-radius:8px;width: 200px;height: 180px;padding:5px">\
                        <div style="width: 190px;height: 170px; background: url(' + word.image + ') no-repeat center / 190px 170px;">\
                        <div class="mdl-card__title mdl-card--expand"></div>\
                        <div class="mdl-card__actions" style="visibility:hidden;margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0);">\
                        <span id="quiz-result-word1-text" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                    		font-weight: bold ">' + word.translation + '</span>\
                    		</div>\
                    		</div>\
                    		<img src="/images/copyright.png" width="20" height="20" style="z-index:1;position: relative; left:162px; top:-162px">\
                        </div>\
                        <div class="mdl-grid--no-spacing mdl-grid--no-spacing-cell mdl-cell--8-col mdl-cell--4-col-tablet" style="margin-left:30px;">\
                        <div class="mdl-grid mdl-grid--no-spacing-cell mdl-cell--12-col" style="">\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--8-col" style="">\
                        <div style="padding:10px 0px 0px 0px;">';
					if (questions_result_array["{{$quiz->id}}"][k][0] == 1)
					{
                       html = html + '<p id="answer_result_word" style="color:#9fc3ae; font-size: 16px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">'+ questions_result_array["{{$quiz->id}}"][k][1] + '</span></p>';
					}
				    else
					{
						html = html + '<p id="answer_result_word" style="color:#9fc3ae; font-size: 16px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#ff0000">'+ questions_result_array["{{$quiz->id}}"][k][1] + '</span></p>';
					}
					html = html + '<p id="source_word" style="color:#9fc3ae; font-size: 16px">Correct answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + word.source_word + '</span></p>\
                        </div>\
                        </div>\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--4-col" style="">\
                        <div style="float:right;margin:0px 0px 0px 0px">\
                        <button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButtionResultDiv(' + word.id + ')" style="background-image:url(' + "/upload/image/speaker-icon.png" + ');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
                        </button>\
                        </div>\
                        </div>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <hr>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <p style="font-weight: bold; margin-bottom:10px"><?php echo trans("exercise.exercise-span14") ?></p>\
                    		<span>'+note+'</span>\
                    		</div>\
                    		</div>\
                    		</div>'
				}
				else if (question_info.length == 4) {
					var word = null;
					for (i in words) {
						if (words[i].wordset_id == "{{$quiz->wordset_id}}"){
						if (words[i].wordset_index == question_info[1]) {
							word = words[i];
						}
						if (word) break;
						}
					}
					if (word.note == null)
                        note = '';
                    else
                        note = word.note;
					html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                        <div class="demo-card-image mdl-card mdl-cell mdl-cell--2-col mdl-shadow--4dp" style="margin-left:20px;border-radius:8px;width: 200px;height: 180px;padding:5px">\
                        <div style="width: 190px;height: 170px; background: url(' + word.image + ') no-repeat center / 190px 170px;">\
                        <div class="mdl-card__title mdl-card--expand"></div>\
                        <div class="mdl-card__actions" style="visibility:hidden;margin-top:95px;height: 52px; padding: 16px;text-align: center; background: rgba(50, 83, 58, 0);">\
                        <span id="quiz-result-word1-text" class="demo-card-image__filename" style="color: #fff;font-size: 14px;\
                    		font-weight: bold ">' + word.translation + '</span>\
                    		</div>\
                    		</div>\
                    		<img src="/images/copyright.png" width="20" height="20" style="z-index:1;position: relative; left:162px; top:-162px">\
                        </div>\
                        <div class="mdl-grid--no-spacing mdl-grid--no-spacing-cell mdl-cell--8-col mdl-cell--4-col-tablet" style="margin-left:30px;">\
                        <div class="mdl-grid mdl-grid--no-spacing-cell mdl-cell--12-col" style="">\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--8-col" style="">\
                        <div style="padding:10px 0px 0px 0px;">';

					if (questions_result_array["{{$quiz->id}}"][k][0] == 1)
					{
                       html = html + '<p id="answer_result_word" style="color:#9fc3ae; font-size: 16px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">'+ questions_result_array["{{$quiz->id}}"][k][1] + '</span></p>';
					}
				    else
					{
						html = html + '<p id="answer_result_word" style="color:#9fc3ae; font-size: 16px">Your answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#ff0000">'+ questions_result_array["{{$quiz->id}}"][k][1] + '</span></p>';
					}
					html = html + '<p id="source_word" style="color:#9fc3ae; font-size: 16px">Correct answer:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + word.source_word + '</span></p>\
                        </div>\
                        </div>\
                        <div class=" mdl-grid--no-spacing-cell mdl-cell--4-col" style="">\
                        <div style="float:right;margin:0px 0px 0px 0px">\
                        <button class="mdl-button mdl-js-button mdl-button--fab" onclick="AudioButtionResultDiv(' + word.id + ')" style="background-image:url(' + "/upload/image/speaker-icon.png" + ');background-repeat:no-repeat; background-size:auto; background-color:#63a77f; width:50px; height:50px; margin:0px 0px 0px 0px">\
                        </button>\
                        </div>\
                        </div>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <hr>\
                        </div>\
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--4-col-tablet" style="">\
                        <p style="font-weight: bold; margin-bottom:10px"><?php echo trans("exercise.exercise-span14") ?></p>\
                    		<span>'+note+'</span>\
                    		</div>\
                    		</div>\
                    		</div>'
				}
				else if (question_info.length == 5) {

					var question_str = '';
					var answer_str = '';
					var youranswers = questions_result_array["{{$quiz->id}}"][k][1];
					var answer_data_array = question_info[2];
					var instruction_array = question_info[1];
					var terms = question_info[3];
					var m = 0;
					for (var i in instruction_array)
					{
						if (instruction_array[i] == 'input')
						{
							var element = RemovePunctuations(answer_data_array[m]).trim().toLowerCase();
							var elements = element.split('|');
							var status_element = false;

							for (j in elements)
							{
								if (status_element) break;
								for (p in terms)
								{
									term = terms[p].term;
									var term_current = null;
									if (status_element == true) break;
									for (h in term)
									{
										if (term_current) break;
										if (elements[j].includes(term[h].toLowerCase()))
										{
											include_str = term[h].toLowerCase();
											term_current = term;
											if (status_element == true) break;
											for (p in term_current)
											{
												if (status_element == true) break;
												var replace_answer = elements[j].replace(include_str, term_current[p].toLowerCase());
												if (replace_answer == RemovePunctuations(youranswers[m].toLowerCase()).trim())
												{
													status_element = true;
													break;
												}
											}
											break;
										}
									}
								}

								if (elements[j] == RemovePunctuations(youranswers[m].toLowerCase()).trim())
								{
									status_element = true;
									break;
								}
							}

							if (status_element == true)
							{
								answer_str = answer_str + '<span style="padding:0px 10px 0px 10px; border-bottom: 2px solid #008000">' + youranswers[m] + '</span>';
							}
							else
							{
								answer_str = answer_str + '<span style="padding:0px 10px 0px 10px; border-bottom: 2px solid #ff0000">' + youranswers[m] + '</span>';
							}
							question_str = question_str + '<span style="padding:0px 10px 0px 10px; border-bottom: 2px solid #f29f00">' + answer_data_array[m] + '</span>';

							m++;
						}
						else
						{
							question_str = question_str + instruction_array[i];
							answer_str = answer_str + instruction_array[i];
						}
					}
					var question = questions["{{$quiz->id}}"][question_info[0]];
					var note = '';
					if (question.note) note = question.note;
					html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
								<div class="mdl-cell mdl-cell--12-col" style="">\
								    <p style="font-weight: bold; font-size:20px; margin-bottom:10px"><?php echo trans("exercise.exercise-span18") ?>:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">'+ answer_str  + '</span></p>\
									<p style="font-weight: bold; font-size:20px; margin-bottom:10px"><?php echo trans("exercise.exercise-span19") ?>:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question_str + '</span></p>\
									<hr>\
									<p style="font-weight: bold; font-size:20px; margin-bottom:10px"><?php echo trans("exercise.exercise-span14") ?>:&nbsp<span  style="font-weight: normal; font-size:20px;color:#9fc3ae">' + note + '</span></p>\
                                </div>\
								</div>'
				}
              else
              {
                  var question = questions["{{$quiz->id}}"][question_info];
				  if (questions_result_array["{{$quiz->id}}"][k][0] == 1)
                  html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                          <div class="mdl-cell mdl-cell--12-col" style="">\
                              <span style="font-weight: bold; font-size:20px; margin-bottom:10px"><?php echo trans("exercise.exercise-span17") ?>:&nbsp<span style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.instruction + '</span></span>\
                              <p style="font-weight: bold; font-size:20px; margin-bottom:10px"><?php echo trans("exercise.exercise-span18") ?>:&nbsp<span style="font-weight: normal; font-size:20px;color:#9fc3ae">'+ questions_result_array["{{$quiz->id}}"][k][1] + '</span></p>\
                              <p style="font-weight: bold; font-size:20px; margin-bottom:10px"><?php echo trans("exercise.exercise-span19") ?>:&nbsp<span style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.correct_answer_id + '</span></p>\
                              <hr>\
                              <p style="font-weight: bold; font-size:20px; margin-bottom:10px"><?php echo trans("exercise.exercise-span14") ?>:&nbsp<span style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.note + '</span></p>\
                          </div>\
                      </div>'
				  else
                  html = html + '<div class="mdl-grid mdl-cell mdl-cell--12-col" style="border:1px solid #eee">\
                          <div class="mdl-cell mdl-cell--12-col" style="">\
                              <span style="font-weight: bold; font-size:20px; margin-bottom:10px"><?php echo trans("exercise.exercise-span17") ?>:&nbsp<span style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.instruction + '</span></span>\
                              <p style="font-weight: bold; font-size:20px; margin-bottom:10px"><?php echo trans("exercise.exercise-span18") ?>:&nbsp<span style="font-weight: normal; font-size:20px;color:#ff0000">'+ questions_result_array["{{$quiz->id}}"][k][1] + '</span></p>\
                              <p style="font-weight: bold; font-size:20px; margin-bottom:10px"><?php echo trans("exercise.exercise-span19") ?>:&nbsp<span style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.correct_answer_id + '</span></p>\
                              <hr>\
                              <p style="font-weight: bold; font-size:20px; margin-bottom:10px"><?php echo trans("exercise.exercise-span14") ?>:&nbsp<span style="font-weight: normal; font-size:20px;color:#9fc3ae">' + question.note + '</span></p>\
                          </div>\
                      </div>'
              }

          }
          document.getElementById('result-score-{{$quiz->id}}').innerHTML = '{!! trans("exercise.yourscore") !!} ' + score + '/' + quiz_question_length["{{$quiz->id}}"];
          document.getElementById('Reulst-div-content-{{$quiz->id}}').innerHTML = html;
      }

    }
  @endforeach
}

function shuffle (array) {
    var i = 0
        , j = 0
        , temp = null

    for (i = array.length - 1; i > 0; i -= 1) {
        j = Math.floor(Math.random() * (i + 1))
        temp = array[i]
        array[i] = array[j]
        array[j] = temp
    }
}

function AudioButton(quiz_id)
{
  if (audio_button_url[quiz_id])
	playSound(audio_button_url[quiz_id]);
}


function AudioButtionResultDiv(result_word_id)
{
  for (p in words) {
     if (words[p].id == result_word_id) {
         word = words[p];
     }
  }
  playSound(word.audio);
}

function playSound(url){
    var audio = document.createElement('audio');
    audio.style.display = "none";
    audio.src = url;
    audio.autoplay = true;
    audio.onended = function(){
      audio.remove()
    };
    document.body.appendChild(audio);
 }
function mouseover(x) {
    x.src = "/images/copyright-hover.png";
}

function mouseout(x) {
    x.src = "/images/copyright.png";
}

function ClickCopyright(obj, event)
{

      event.preventDefault();
      event.stopPropagation();
      parent = obj.parentNode;


      var childNodeArray = parent.childNodes;
	  console.log(childNodeArray);
      var child1Array = childNodeArray[3].childNodes;

      tran_word = child1Array[1].innerText;

      var word = null;
      for (i in words) {
          if (words[i].translation == tran_word) {
              word = words[i];
          }
      }

      if (word)
      {
         if (word.copyright_url){

           w2popup.open({
			   title: 'CopyRight Url',
               body: '<div class="w2ui-centered" style="overflow:hidden;padding:0px; margin:0px; height: 400; width:400;backgroundColor:#0000ff">\
							 				  <div style="margin-bottom:30px">\
                        	<span style="font-size:16px; font-weight:bold; color:#399aed;"> Credit </span>\
												</div>\
												<div style="width:300;min-height:100px;font-size:14px;color:#399aed;">\
													<p>' + word.copyright_url + '</p>\
												</div>\
                     </div>',
               width:  400,
               height: 400,
               color: '#000',
							 backgroundColor:'#ffffff',
               modal: false,
               style : '',
               showClose       : true
           });
         }
         else {
            alert("Empty Copyright URL");
         }
      }
}

function playagain(quiz_id)
{
    QuizInit(quiz_id);
}

function goLogin() {
      location.href = '/login'
}

function goProfile(e) {
  e.preventDefault();
  location.href = '/profile'
}

function goDashboard(e) {
  location.href = '/home'
}

function Logout(e) {
    e.preventDefault();
    document.getElementById('logout-form').submit();
}

function isMobileDevice() {
	return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

console.log(isMobileDevice());
if (!isMobileDevice()){
	$('ul.nav li.dropdown').hover(function() {
	$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
	}, function() {
	$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
	});
}
</script>
<!--Google adwords Code-->
<?php echo html_entity_decode($blog->adwords_code)?>
<!--End Google adwords Code-->
<script id="dsq-count-scr" src="//imprevo.disqus.com/count.js" async></script>
<script>
jQuery.sharedCount = function(url, fn) {
    url = encodeURIComponent(url || location.href);
    var domain = "//api.sharedcount.com/v1.0/"; /* SET DOMAIN */
    var apikey = "a5b137529d23aee902d87b497fb65888e89e54a7" /*API KEY HERE*/
    var arg = {
      data: {
        url : url,
        apikey : apikey
      },
        url: domain,
        cache: true,
        dataType: "json"
    };
    if ('withCredentials' in new XMLHttpRequest) {
        arg.success = fn;
    }
    else {
        var cb = "sc_" + url.replace(/\W/g, '');
        window[cb] = fn;
        arg.jsonpCallback = cb;
        arg.dataType += "p";
    }
    return jQuery.ajax(arg);
};

  $('#update1').click(function(event) {
    var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,

        url    = "https://twitter.com/intent/tweet?url=" + location.href;
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;

    window.open(url, 'twitter', opts);

    return false;
  });

  $('#update2').click(function(event) {
    var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,

        url    = "https://plus.google.com/share?url=" + location.href;
        opts   = 'menubar=no' +
                 ',toolbar=no'  +
                 ',resizable=yes' +
                 ',height='    + height    +
                 ',width='   + width;

    window.open(url, 'Google+', opts);

    return false;
  });

  $('#update').click(function(event) {
    var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = "http://www.facebook.com/sharer/sharer.php?kid_directed_site=0&title=Share&sdk=joey&display=popup&ref=plugin&src=share_button&u=" + location.href;
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;
        window.open(url, 'facebook', opts);
    return false;
  });

  $(document).ready(function(){
    var url = window.location.href;
    /*$.sharedCount(url, function(data){
        console.log('sharecount plugin', data);
        console.log(data.Facebook.total_count);
        console.log(data.GooglePlusOne);
    });

    $.getJSON("https://graph.facebook.com/"+url+"?callback=?", function(data) {
      console.log('count', data.share['share_count']);
      $('#counter').html(data.share['share_count']);
    });

    $.ajax({
      type: 'POST',
      url: 'https://clients6.google.com/rpc',
      processData: true,
      contentType: 'application/json',
      data: JSON.stringify({
        'method': 'pos.plusones.get',
        'id': url,
        'params': {
          'nolog': true,
          'id': url,
          'source': 'widget',
          'userId': '@viewer',
          'groupId': '@self'
        },
        'jsonrpc': '2.0',
        'apiVersion': 'v1'
        //'key': 'AIzaSyApHr-mcWQ2eauvdW76hP8iaIPV-tyFeiU',

      }),
      success: function(response) {
        console.log(response.result.metadata.globalCounts.count);
        $('#counter2').html(response.result.metadata.globalCounts.count);
      }
    });

    $.ajax({
          url: "https://opensharecount.com/count.json?url=" + url,
          type: 'GET',
          dataType: 'json',
          success: function (data, textStatus, xhr) {
                   console.log('openshare', data);
                   if (data['count'])
                    document.getElementById('counter1').innerHTML = data['count'];
          },
           error: function (xhr, textStatus, errorThrown) {
                console.log('Error in Database');
           }
    });

    $.ajax({
          url: "https://public.newsharecounts.com/count.json?url=" + url,
          type: 'GET',
          dataType: 'json',
          success: function (data, textStatus, xhr) {
                   console.log('newshare', data);
                   if (data['count'])
                     document.getElementById('counter1').innerHTML = data['count'];
          },
           error: function (xhr, textStatus, errorThrown) {
                console.log('Error in Database');
           }
    });

    $.ajax({
          url: 'https://plusone.google.com/_/+1/fastbutton?url=' + url,
          type: 'GET',
          dataType: 'HTML',
          success: function (data, textStatus, xhr) {
            console.log(data);
            var aggregate = $('#aggregateCount', data).html();
            exactMatch = $('script', data).html().match('\\s*c\\s*:\\s*(\\d+)');

            console.log(exactMatch ? exactMatch[1] + ' (' + aggregate + ')' : aggregate);
          },
           error: function (xhr, textStatus, errorThrown) {
                console.log('Error in Database');
           }
    });*/
});
</script>
<script>
var home_header = document.getElementById("header-div-homepage");

//-------home page support --------
if (home_header){
	document.getElementById('header-span1').innerHTML = '{!! trans("headerfooter.header-span1") !!}';
	document.getElementById('header-span2').innerHTML = '{!! trans("headerfooter.header-span2") !!}';
	//document.getElementById('header-span3').innerHTML = '{!! trans("headerfooter.header-span3") !!}';
	//document.getElementById('header-span4').innerHTML = '{!! trans("headerfooter.header-span4") !!}';
	document.getElementById('header-span5').innerHTML = '{!! trans("headerfooter.header-span5") !!}';
	document.getElementById('header-span6').innerHTML = '{!! trans("headerfooter.header-span6") !!}';
	document.getElementById('header-span7').innerHTML = '{!! trans("headerfooter.header-span7") !!}';
	document.getElementById('header-span8').innerHTML = '{!! trans("headerfooter.header-span8") !!}';
	document.getElementById('header-span9').innerHTML = '{!! trans("headerfooter.header-span9") !!}';
	document.getElementById('header-span10').innerHTML = '{!! trans("headerfooter.header-span10") !!}';
	nav_right_ul = document.getElementById('nav-right-ul');
	$('.phone-footer-accordion .item .heading').click(function() {
		var a = $(this).closest('.item');
		var b = $(a).hasClass('open');
		var c = $(a).closest('.accordion').find('.open');

		if(b != true) {
			$(c).find('.content').slideUp(200);
			$(c).removeClass('open');
		}

		$(a).toggleClass('open');
		$(a).find('.content').slideToggle(200);

	});

	if (nav_right_ul)
	{
		var html = '';
		@if (Auth::guest())
			html = html + '<li  style="">';
			html = html + '<button id="my-account-button" onclick="goLogin(event)" class="dropdown-toggle mdl-button mdl-js-button mdl-button--raised mdl-button--colored" data-toggle="dropdown" style="border-radius:2px;outline:none;border:none;">' +
						'{!! trans("headerfooter.header-button1") !!}'	+
					'</button></li>';
		@else
			html = html + '<li class="dropdown" style="">';
			html = 	html + '<button class="dropdown-toggle" id="my-account-button-logged" onclick="goDashboard(event)" class="my-nav-button dropdown-toggle" data-toggle="dropdown" style="border-radius:2px;outline:none;border:none;">' +
						'{!! trans("headerfooter.header-button2") !!}' +
					'</button>';
			html = html + '<ul id="dropdown3" class="dropdown-menu">' +
						'<li><a class="a-submenu" href="/profile">{!! trans("headerfooter.header-dropdown1") !!}</a></li>' +
						'<li><a class="a-submenu" href="" onclick="Logout(event)">{!! trans("headerfooter.header-dropdown2") !!}</a></li>' +
				'</ul></li>';
		@endif


		nav_right_ul.innerHTML = html;
	}

	try_it_button = document.getElementById('try-it-button');

	if (try_it_button)
	{
		@if (Auth::guest())
			try_it_button.style.backgroundColor = '#399aed';
		@else
			try_it_button.style.backgroundColor = '#f29f00';
		@endif
	}
}
//------------------------


$(document).ready(function(){
  var old_scroll_value = 0;
  window.onscroll = function ()  { //detect page scroll
  	home_header = document.getElementById("header-div-homepage");

  	if (home_header){

  		sticky = $('#header-div-homepage');
  		scroll = document.documentElement.scrollTop || document.body.scrollTop;

  		if (scroll >= 100) {
        sticky.addClass('fixed');
  			document.getElementById("header-div-homepage").style.padding = "5px 0px 5px 0px";
      /*  if (scroll > old_scroll_value)
        {
          document.getElementById("header-div-homepage").style.top = '40px';
        }
        else{
          document.getElementById("header-div-homepage").style.top = '0px';
        }*/
  		}
  		else{
  			sticky.removeClass('fixed');
  			document.getElementById("header-div-homepage").style.padding = "12px 0px 12px 0px";
  		}
      old_scroll_value = scroll;
  	}
  };
});


function isMobileDevice() {
	return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

console.log(isMobileDevice());
if (!isMobileDevice()){
	$('ul.nav li.dropdown').hover(function() {
	$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
	}, function() {
	$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
	});
}
function goLogin() {
      location.href = '/login'
}

function goProfile(e) {
  e.preventDefault();
  location.href = '/profile'
}

function goDashboard(e) {
  location.href = '/home'
}

function Logout(e) {
    e.preventDefault();
    document.getElementById('logout-form').submit();
}



</script>
</body>
</html>
