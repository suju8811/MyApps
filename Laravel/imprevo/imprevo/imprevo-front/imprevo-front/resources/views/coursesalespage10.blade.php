<!DOCTYPE html>
<html lang="en" style="position: relative;min-height: 100%;">
<head>
  @if (isset($settings))
  <?php echo html_entity_decode($settings['gaCode'])?>
  @endif
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @if (isset($settings))
  <meta name="description" content="Online angol nyelvtanfolyam több mint 200 leckével, nyelvtani magyarázó videókkal, anyanyelvi beszélőkkel, interaktív feladatokkal és érdekes témákkal.">
  <meta name="keywords" content="angol tanfolyam, online angol, angol tanulás gyorsan, angol tanulás online">
  <title>IMPREVO - Online Angol Nyelvtanfolyam</title>
  @else
  <meta name="description" content="Online angol nyelvtanfolyam több mint 200 leckével, nyelvtani magyarázó videókkal, anyanyelvi beszélőkkel, interaktív feladatokkal és érdekes témákkal.">
  <meta name="keywords" content="angol tanfolyam, online angol, angol tanulás gyorsan, angol tanulás online">

  <title>IMPREVO - Online Angol Nyelvtanfolyam</title>

  @endif


  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
  <link rel="stylesheet" href="/css/material.min.css" />
  <link rel="stylesheet" href="/css/styles.css" />
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
  <link rel="stylesheet" href="{{ Config::get('RELATIVE_URL') }}/assets/vendor/pnotify/pnotify.custom.css" />
  <link rel="stylesheet" type="text/css" href="https://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.css" />
  <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <!-- Specific Page Vendor CSS -->
  <link rel="stylesheet" href="/assets/vendor/owl.carousel/assets/owl.carousel.css" />
  <link rel="stylesheet" href="/assets/vendor/owl.carousel/assets/owl.theme.default.css" />
  <!-- Theme CSS -->
  <link rel="stylesheet" href="/assets/stylesheets/theme-fix.css" />

  <!-- Skin CSS -->
  <link rel="stylesheet" href="/assets/stylesheets/skins/default.css" />

  <!-- Theme Custom CSS -->
  <link rel="stylesheet" href="/assets/stylesheets/theme-custom.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

  <script src="/assets/vendor/modernizr/modernizr.js"></script>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://rawgit.com/vitmalina/w2ui/master/dist/w2ui.min.js"></script>
  <script src="/assets/vendor/pnotify/pnotify.custom.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="/assets/vendor/nanoscroller/nanoscroller.js"></script>
  <script src="/assets/vendor/owl.carousel/owl.carousel.js"></script>

  <script src="/assets/javascripts/theme.js"></script>

  <!-- Theme Custom -->
  <script src="/assets/javascripts/theme.custom.js"></script>

  <!-- Theme Initialization Files -->
  <script src="/assets/javascripts/theme.init.js"></script>


  <script>
  window.Laravel = {!! json_encode([
    'csrfToken' => csrf_token(),
  ]) !!};
  </script>

</head>
<body style="padding:0px; height:100%;margin: 0;overflow-x: hidden">
  @if (isset($settings))
    <?php echo html_entity_decode($settings['gaBodyCode'])?>
  @endif
  <div id='Parent-Div-Background'style="padding:0; margin:0px;position:relative;width:100%;min-height:100%;">

    <style>
    html, body {
      font-family: 'Roboto', sans-serif;
    }


    #purchase-button{
      background-color:#399aed;
      font-size: 18px;
    }

    #purchase-button:hover{
      background-color:#2081d5;
      font-size: 18px;
    }



    .section3-link2
    {
      color:#000000;
      span {
        color:#39929e;
      }
    }

    .section3-link2 a
    {
      color:#399aed;
    }

    .section3-link2 a:hover
    {
      color:#399aed;
      text-decoration:underline;
    }

    #try-it-button-second,#section3_button{
      color:#399aed;
      background-color:#ffffff;
      width:200px;
      height:40px;
    }

    #try-it-button-second:hover, #section3_button:hover{
      color:#399aed;
      background-color:#dfe8ef;
      font-weight:bold;
    }



    .a-next-prev {
      padding-bottom:150px;
      float:right;
      color:black;
      font-size:40px;
      cursor:pointer;
    }

    .a-next-prev:hover {
      padding-bottom:150px;
      float:right;
      color:#399aed;
      font-size:40px;
      cursor:pointer;
    }

    .socialbuttons .facebook{
      width:40px;
      height:40px;
      background:url('/images/social-facebook-icon.png') center/cover;
    }
    .socialbuttons .facebook:hover{
      background:url('/images/social-facebook-icon-hover.png') center/cover;
    }
    .socialbuttons .twitter{
      width:40px;
      height:40px;
      background:url('/images/social-twitter-icon.png') center/cover;
    }
    .socialbuttons .twitter:hover{
      background:url('/images/social-twitter-icon-hover.png') center/cover;
    }
    .socialbuttons .goggle{
      width:40px;
      height:40px;
      background:url('/images/social-goggle-icon.png') center/cover;
    }
    .socialbuttons .goggle:hover{
      background:url('/images/social-goggle-icon-hover.png') center/cover;
    }
    .socialbuttons .insta{
      width:40px;
      height:40px;
      background:url('/images/social-youtube-icon.png') center/cover;
    }
    .socialbuttons .insta:hover{
      background:url('/images/social-youtube-icon-hover.png') center/cover;
    }

    .link1{
      color:#66abae;
    }
    .link1:hover{
      color:#ffffff;
      text-decoration:none;
    }

    #section2-scrolldown{
      z-index:99;
      position:relative;
      background:url('/images/what_is_imprevo_section1-scrolldown.png') no-repeat;
      height:50px;
      width:50px
      left:-webkit-calc(50% - 25px);
      left:-moz-calc(50% - 25px);
      left:calc(50% - 25px);
    }

    #section2-scrolldown:hover{
      z-index:99;
      position:relative;
      background:url('/images/what_is_imprevo_section1-scrolldown-hover.png') no-repeat;
      height:50px;
      width:50px
      left:-webkit-calc(50% - 25px);
      left:-moz-calc(50% - 25px);
      left:calc(50% - 25px);
    }

    .section2-line {
      z-index:0;
      position:relative;
      left:40%;
    }

    .footer-link{
      padding:10px;
      font-weight:700;
      color: #6c937b;
    }

    #footer-1div{
      text-align:right;
    }

    #footer-2div{
      text-align:center;
    }

    #footer-3div{
      text-align:left;
    }
    #div-footer {
      height:140px;
    }
    #footer-div-bottom {
      width: 60% !important;
    }
    #section1{
      width:1104px;
    }

    .tailer{

    }

    .arrow {
      width:20px;
      height:10px;
      border-bottom-right-radius: 15px;
      border-bottom-left-radius: 15px;
      box-shadow: #101010 0px 2px 15px;
      position: relative;
      left: 100px;
      z-index: -100;
    }
    .arrowShadow {
      width:0px;
      height:0px;
      border-left:15px solid transparent;
      border-right:15px solid transparent;
      border-top:15px solid #f29f00;
      font-size:0px;
      line-height:0px;
      position: relative;
      top: 0px;
      left: 50%;
      margin-left:-15px;
      z-index: 200;
    }

    @media (max-width: 1850px) {
      .footer {
        width:80%;
      }
    }
    @media (max-width: 1445px) {
      #hero-div-content{
        width:80% !important;
      }
    }
    @media (max-width: 1345px) {
      #sectionnew_2{
        width:90% !important;
      }
      .footer {
        width:90%;
      }


    }

    @media (max-width: 1185px) {
      .footer {
        width:100%;
      }
      #hero-div-content{
        width:90% !important;
      }
    }

    @media (max-width: 1530px) {

    }

    @media (max-width: 1470px) {

    }

    @media (max-width: 1100px){
      #section1{
        width:100% !important;
      }
      #section1-main-card{
        margin-top:-430px !important;
      }

      #section1{
        width:95% !important;
      }

      #hero-div-content{
        width:100% !important;
      }
    }

    @media (max-width: 769px) {


      #footer-1div{
        text-align:center;
      }

      #footer-2div{
        text-align:center;
      }

      #footer-3div{
        text-align:center;
      }

      .footer-link{
        font-size:12px;
      }

      #div-footer {
        height:230px;
      }

      #footer-1div{
        text-align:center;
      }

      #footer-2div{
        text-align:center;
      }

      #footer-3div{
        text-align:center;
      }

      #footer-span1{
        font-size:20px !important;
      }

      #footer-div1{
        padding-top: 50px !important;
      }
      .phone-footer-accordion {
        width: 380px;
        border-radius: 5px;
        overflow: hidden;
        margin: auto;
      }

      .phone-footer-accordion .item .heading {
        height: 50px;
        line-height: 50px;
        font-size: 15px;
        cursor: pointer;
        color: #ffffff;
        padding-left: 15px;
        background: #006469 url('/images/phone_footer_accordion.png') no-repeat;
        background-position: right 20px top -95px;
        border-bottom: 1px solid #4d9a9d;
        box-sizing: border-box;
        text-align:left;
      }

      .phone-footer-accordion .item.open .heading,
      .phone-footer-accordion .item:last-child .heading { border: 0; }

      .phone-footer-accordion .item.open .heading { background-position: right 20px top -5px; }

      .phone-footer-accordion .item .content {
        display: none;
        padding: 15px;
        background: #006469;
        font-size: 14px;
      }
    }

    @media (max-width: 850px) {
      #hero-div-title{
        font-size:25px !important;
        line-height: 40px !important;
      }
      #hero-div-span{
        font-size:16px !important;
      }

      #section1 {
        margin-top:-46px !important;
      }
      #section1-main-card{
        margin-top:0px !important;
      }
      #section1-div1, #section1-div2{
        padding-left: 0px !important;
        padding:0px 0px 0px 30px !important;
      }
      .section1-div-text{
        padding:0 !important;
      }

      #section2-title{
        padding-top: 20px !important;
      }
      #owl-carousel-div{
        padding-top: 0px !important;
      }

      #try-it-button-second{
        margin-left: 0px;
      }
      .section4-div1{
        padding-left: 0px !important;
      }
      .section4-div1-div1{
        padding-top: 10px !important;
      }
      #section4{
        padding:10px 0px 0px 0px !important;
      }
      span.section3-link2{
        font-size:14px !important;

      }
      #section4-div-latest{
        padding-bottom:30px !important;
      }

      #section4-span1 {
        font-size:25px !important;
        line-height:30px !important;
      }

      #footer-div1{
        padding-top:40px !important;
      }

      #footer-div1-span{
        font-size:20px !important;
      }
    }

    @media (max-width: 850px) {
      #hero-text-divs{
        text-align:center;
      }
      .product_box_sales{
        height:200px !important;
        left:50% !important;
        bottom:-220px !important;
        margin-left:-130px !important;
      }
      .buy-text-div{
        justify-content: center;
      }
      #buy-main-div{
        width:100% !important;
      }
      #section_new_4_img{
        height:350px;
      }
      .section_new_4_divs{
        width:100% !important;
        text-align:center;
      }
      #section_new6_selected_card{
        margin-top:0px !important;
      }
      .section_new6_card{
        margin-top:10px !important;
        z-index: 10 !important;
      }

      #section2{
        padding-top:20px !important;
        margin-top:-35px !important;
      }

      #section_new_5{
        padding-bottom:50px !important;
      }
      #owl-carousel{
        width:100% !important;
      }
    }

    @media (max-width: 450px) {
      #owl-carousel{
        width:100% !important;
      }
      #hero-text-divs{
        text-align:center;
      }
      .product_box_sales{
        height:200px !important;
        left:50% !important;
        bottom:-220px !important;
        margin-left:-130px !important;
      }
      .buy-text-div{
        justify-content: center;
      }
      #buy-main-div{
        width:100% !important;
      }
      #section_new_4_img{
        height:280px;
      }
      .section_new_4_divs{
        width:100% !important;
        text-align:center;
      }
      #section_new6_selected_card{
        margin-top:0px !important;
      }
      .section_new6_card{
        margin-top:10px !important;
        z-index: 10 !important;
      }

      #section2{
        padding-top:20px !important;
        margin-top:-35px !important;
      }

      #section_new_5{
        padding-bottom:50px !important;
      }
    }
    </style>

    @if (!$product->sales)
      <style>
      @media (max-width: 850px){
        #hero-div{
          padding-top:40px !important;
          padding-bottom: 10px !important;
        }
      }
      @media (max-width: 450px){
        #hero-div{
          padding-top:20px !important;
        }
        .product_box{
          margin-left: -80px !important;
          height: 120px !important;
        }
        #product_box_div{
          height: 100px !important;
        }
        #span-price{
          font-size:25px !important;
        }
      }
      </style>
    @else
    <style>
    @media (max-width: 850px){
      #hero-div{
        padding-bottom: 180px !important;
      }

    }
    </style>
    @endif


    <main class="mdl-layout__content" style="display:inline;width:100%;margin:0px;padding:0px;">
      <div id="full_content_div" style="width:100%;padding:0px;margin:0px">
        @include('layouts.header')
        <div class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px;width:100%; background:url('/images/course_salespage_10_hero_background.png') no-repeat;background-color:#fff">
          <div id="hero-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:40px 0px 40px 0px; @if ($product->sale_price) padding-top:100px; padding-bottom:70px; @endif margin:0px;justify-content:center">
            <div id="hero-div-content" class="mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0px; margin:0px;">
              <div id="hero-text-divs" class="mdl-grid mdl-cell mdl-cell--9-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0px; margin:0px;">
                <!--Learn English at your own pace with the full IMPREVO Online English course-->
                <div class="mdl-cell mdl-cell mdl-cell--12-col">
                  <span id="hero-div-title" style="font-weight:300; font-size:50px;color:#ffffff;line-height: 60px;">Tanulj meg angolul saját tempódban
                    a teljeskörű <span style="font-weight:500">IMPREVO Online Angol</span>
                    tanfolyammal</span>
                  </div>
                  <div class="mdl-cell mdl-cell mdl-cell--12-col">
                    <span id="hero-div-span" style="font-weight:300; font-size:20px;color:#ffffff;line-height: 20px;">{!! trans('coursesales10.span2') !!}</span>
                  </div>
                </div>
                <div class="mdl-cell mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="position:relative">
                  @if ($product->sales)
                  <img class="product_box_sales" src="/images/course_salespage_10_box.png" style="position:absolute;left:-50px;bottom:-160px"/>
                  @else
                  <div id="product_box_div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="position:relative;height:250px">
                    <img class="product_box" src="/images/course_salespage_10_box.png" style="position:absolute;left: 50%; margin-left: -160px;height:250px"/>
                  </div>
                  <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;padding-top:10px">
                    <span id="span-price" style="color:#ffffff;font-size:50px;font-weight:300;">{{number_format($product->regular_price)}} <span style="font-size:20px;vertical-align: super;">Ft</span></span>
                  </div>
                  <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0px; padding:0px;justify-content:center">
                    <button onclick="location.href = '/shoppingcart?products={{$product->id}}'" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="background-color:#f29f00;width:90%;height:50px">
                      KOSÁRBA
                    </button>
                  </div>
                  @endif
                </div>
              </div>
            </div>
            @if ($product->sales)
            <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="background:url('/images/course_salespage_10_section1_background.png') no-repeat;justify-content:center;margin:0;padding:0px;width:100%; background-size:100% 100%;">
              <div id="buy-main-div" class="mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:50px 0px 40px 0px; margin:0px;">
                <div class="mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="buy-text-div mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <span style="color:#ffffff;font-size:50px;font-weight:300;">{{number_format($product->sale_price)}} <span style="font-size:20px;vertical-align: super;">Ft</span></span>
                  </div>
                  <div class="buy-text-div mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <span style="color:#ffffff;font-size:20px;font-weight:300;">Most akciós áron <span style="color:#f29f00;text-decoration: line-through;">{{number_format($product->regular_price)}} Ft</span> helyett</span>
                  </div>
                  <div class="buy-text-div mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                    <span style="color:#4c999d;font-size:15px;font-weight:300;">* A feltüntetett ár bruttó ár, a 27% ÁFA-t tartalmazza!</span>
                  </div>
                </div>
                <div class="mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" >
                  <div class="buy-text-div mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="justify-content:center">
                    <span style="color:#ffffff;font-size:20px;font-weight:300;">Az akcióból hátralévő idő:</span>
                  </div>
                  <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="justify-content:center;text-align:center">
                    <span style="border:2px solid #338b8f; border-radius:35px; width:70px;height:70px;color:#ffffff;font-size:20px;line-height:20px;padding-top:10px"><span class="days">3</span><br><span style="font-size:14px">nap</span></span>
                    <span style="border:2px solid #338b8f; border-radius:35px; width:70px;height:70px;color:#ffffff;font-size:20px;line-height:20px;padding-top:10px;margin-left:10px;"><span class="hours">5</span><br><span style="font-size:14px">óra</span></span>
                    <span style="border:2px solid #338b8f; border-radius:35px; width:70px;height:70px;color:#ffffff;font-size:20px;line-height:20px;padding-top:10px;margin-left:10px;"><span class="mins">21</span><br><span style="font-size:14px">perc</span></span>
                  </div>
                </div>
                <div class="mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--1-col mdl-cell--hide-tablet mdl-cell--hide-phone" >
                  <img src="/images/course_salespage_10_section1_seperator.png"/>
                </div>
                <div class="mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="justify-content:center;padding:5px">
                  <div class="mdl-cell mdl-cell--12-col" style="text-align:center;">
                    <button onclick="location.href = '/shoppingcart?products={{$product->id}}'" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="background-color:#f29f00;width:90%;height:50px">
                      KOSÁRBA
                    </button>
                  </div>
                  <div class="mdl-cell mdl-cell--12-col" style="text-align:center;margin-top:10px;">
                    <span style="color:#ffffff;font-size:20px">Még <span style="color:#f29f00"><span class="sales_count">43</span> akciós csomag</span> elérhető!</span>
                  </div>
                </div>
              </div>
            </div>
            @endif
            <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="background:#ffffff;justify-content:center;margin:0;padding:0px;width:100%">
              <div class="mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--hide-phone" style="padding:50px 0px 30px 0px;  margin:0px; ">
                <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="text-align:center">
                  <span style="font-size:40px;font-weight:500px;color:black">Próbáld ki ingyen!</span>
                </div>
                <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="text-align:center;margin-top:30px;">
                  <span style="font-size:20px;font-weight:500px;color:black">Az IMPREVO összes tanfolyama kipróbálható egy ingyenes regisztráció után, így megbizonyosodhatsz róla,
                    hogy ez a tanfolyam megfelelő-e a számodra, mielőtt megvásárolnád.</span>
                  </div>
                </div>
                <div id="sectionnew_2" class="mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0px 0px 0px 0px;  margin:0px;background:url('/images/course_salespage_10_section2_img.png') no-repeat; background-size:100% 100%">
                  <div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--4-col-phone">
                    <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone">
                      <div class="mdl-cell mdl-cell mdl-cell--2-col" style="text-align:right;">
                        <img src="/images/what_is_imprevo_section2-ticket.png" style="width:20px;height:20px">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--10-col">
                        <span style="font-size:15px;">200+ lecke teljesen az<br> alapoktól a felsőfokig</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--2-col" style="text-align:right;">
                        <img src="/images/what_is_imprevo_section2-ticket.png" style="width:20px;height:20px">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--10-col">
                        <span style="font-size:15px;">Irányított, összeszedett<br> haladás, saját ütemedben</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--2-col" style="text-align:right;">
                        <img src="/images/what_is_imprevo_section2-ticket.png" style="width:20px;height:20px">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--10-col">
                        <span style="font-size:15px;">Folyamatos hallás utáni<br> szövegértés gyakorlás</span>
                      </div>
                    </div>
                  </div>
                  <div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--8-col-phone" style="height:350px">
                  </div>
                  <div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--4-col-phone">
                    <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--hide-tablet mdl-cell--hide-phone">
                      <div class="mdl-cell mdl-cell mdl-cell--3-col"></div>
                      <div class="mdl-cell mdl-cell mdl-cell--2-col" style="text-align:right">
                        <img src="/images/what_is_imprevo_section2-ticket.png">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--7-col">
                        <span style="font-size:15px;">Szótanulás 5000+ szóval, <br>anyanyelvi kiejtéssel</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--3-col">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--2-col" style="text-align:right">
                        <img src="/images/what_is_imprevo_section2-ticket.png">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--7-col">
                        <span style="font-size:15px;">600+ magyarázó videó és <br>több ezer feladat</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--3-col">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--2-col" style="text-align:right">
                        <img src="/images/what_is_imprevo_section2-ticket.png">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--7-col">
                        <span style="font-size:15px;">Érthető, elemeire bontott<br>igeidők és nyelvtan</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--hide-desktop" style="background:#ffffff;justify-content:center;margin:0;padding:0px;width:100%">
                  <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="text-align:center;padding-top:20px">
                    <span style="font-size:40px;font-weight:500px;color:black">Próbáld ki ingyen!</span>
                  </div>
                  <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--4-col-phone" style="text-align:center;margin-top:30px;">
                    <span style="font-size:20px;font-weight:500px;color:black">Az IMPREVO összes tanfolyama kipróbálható egy ingyenes regisztráció után, így megbizonyosodhatsz róla,
                      hogy ez a tanfolyam megfelelő-e a számodra, mielőtt megvásárolnád.</span>
                  </div>
                  <div class="mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:20px 0px 0px 0px; margin:0px;">
                      <div class="mdl-cell mdl-cell mdl-cell--1-col" style="text-align:center;">
                        <img src="/images/what_is_imprevo_section2-ticket.png" style="width:20px;height:20px">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--3-col">
                        <span style="font-size:15px;">200+ lecke teljesen az alapoktól a felsőfokig</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--1-col" style="text-align:center;">
                        <img src="/images/what_is_imprevo_section2-ticket.png" style="width:20px;height:20px">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--3-col">
                        <span style="font-size:15px;">Irányított, összeszedett haladás, saját ütemedben</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--1-col" style="text-align:center;">
                        <img src="/images/what_is_imprevo_section2-ticket.png" style="width:20px;height:20px">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--3-col">
                        <span style="font-size:15px;">Folyamatos hallás utáni szövegértés gyakorlás</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--1-col" style="text-align:center;">
                        <img src="/images/what_is_imprevo_section2-ticket.png" style="width:20px;height:20px">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--3-col">
                        <span style="font-size:15px;">Szótanulás 5000+ szóval, anyanyelvi kiejtéssel</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--1-col" style="text-align:center;">
                        <img src="/images/what_is_imprevo_section2-ticket.png" style="width:20px;height:20px">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--3-col">
                        <span style="font-size:15px;">600+ magyarázó videó és több ezer feladat</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--1-col" style="text-align:center;">
                        <img src="/images/what_is_imprevo_section2-ticket.png" style="width:20px;height:20px">
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--3-col">
                        <span style="font-size:15px;">Érthető, elemeire bontott igeidők és nyelvtan</span>
                      </div>
                  </div>
                  <div class="mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:20px 0px 20px 0px; margin:0px;justify-content:center">
                      <img src="/images/course_salespage_10_section2_mobile_img.png" style="width:80%; height:250px"/>
                  </div>
                </div>
              <div id="section_new_2" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="justify-content:center;width:100%;background-color:#ebf3ef;margin:0;padding:0px;">
                <div id="section_new_2-div1" class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet" style="padding-top:100px;text-align:center">
                  <span style="font-size:30px;">Minden, amire szükséged van az angolozáshoz</span>
                </div>
                <div class="mdl-cell mdl-cell mdl-cell--8-col" style="padding:20px 0px 40px 0px;text-align:center">
                  <span style="font-size:20px"></span>
                </div>

                <div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone">
                  <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;width:100%;">
                    <div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;">
                      <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;">
                        <div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="text-align:center;">
                          <img class="section1-mark" src="/images/what_is_imprevo_section1-icon1.png" style="width:50px">
                        </div>
                        <div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-top:0;margin-top:0">
                          <div class="mdl-cell mdl-cell mdl-cell--12-col">
                            <span style="font-size:18px; font-weight:bold">HALLOTT SZÖVEGÉRTÉS</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col">
                            <span style="font-size:18px">Autentikus amerikai és angol hanganyag nagy mennyiségben, kezdőknek tagoltan érthetően, haladóknak normál sebességgel.</span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;">
                      <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;">
                        <div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="text-align:center;">
                          <img class="section1-mark" src="/images/what_is_imprevo_section1-icon2.png" style="width:50px">
                        </div>
                        <div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-top:0;margin-top:0">
                          <div class="mdl-cell mdl-cell mdl-cell--12-col">
                            <span style="font-size:18px; font-weight:bold">OLVASOTT SZÖVEGÉRTÉS</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col">
                            <span style="font-size:18px">Összefüggő szövegek érdekes témákkal, folyamatosan nehezedő felépítéssel, mindig egy lecke témájához igazítva feladatokkal.</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;">
                      <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;">
                        <div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="text-align:center;">
                          <img class="section1-mark" src="/images/what_is_imprevo_section1-icon3.png" style="width:50px">
                        </div>
                        <div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-top:0;margin-top:0">
                          <div class="mdl-cell mdl-cell mdl-cell--12-col">
                            <span style="font-size:18px; font-weight:bold">NYELVTAN</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col">
                            <span style="font-size:18px">Több mint 600 videóban foglaltunk össze a beszéd és írás alapját képező nyelvtant, szituációs videókkal kiegészítve.</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;width:100%;">
                    <div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;">
                      <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;padding-top:30px;">
                        <div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="text-align:center;">
                          <img class="section1-mark" src="/images/what_is_imprevo_section1-icon4.png" style="width:50px">
                        </div>
                        <div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-top:0;margin-top:0">
                          <div class="mdl-cell mdl-cell mdl-cell--12-col">
                            <span style="font-size:18px; font-weight:bold">ÍRÁSKÉSZSÉG</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col">
                            <span style="font-size:18px">Videós elemzéseken keresztül mutatjuk be a szabályos és szép angol írás minden fortéját, így lesz mire építened.</span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;">
                      <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;padding-top:30px;">
                        <div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="text-align:center;">
                          <img class="section1-mark" src="/images/what_is_imprevo_section1-icon5.png" style="width:50px">
                        </div>
                        <div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-top:0;margin-top:0">
                          <div class="mdl-cell mdl-cell mdl-cell--12-col">
                            <span style="font-size:18px; font-weight:bold">SZÓKINCS</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col">
                            <span style="font-size:18px">Rengeteg játék segít elsajátítani több mint 5000 szót, ami már meghaladja a középfokú angol-tudás követelményeit is.</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="mdl-grid mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-grid-no--spacing" style="margin:0;padding:0px;">
                      <div class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;padding-top:30px;">
                        <div class="mdl-cell mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--1-col-phone" style="text-align:center;">
                          <img class="section1-mark" src="/images/what_is_imprevo_section1-icon6.png" style="width:50px">
                        </div>
                        <div class="mdl-grid mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--3-col-phone" style="padding-top:0;margin-top:0">
                          <div class="mdl-cell mdl-cell mdl-cell--12-col">
                            <span style="font-size:18px; font-weight:bold">FUNKCIONÁLIS NYELV</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col">
                            <span style="font-size:18px">Mindennapi szituációkkal mutatunk be olyan ökölszabályokat, amiket már az első alkalommal jól tudsz használni.</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0px; margin:0px;height:25px; text-align:center;justify-content:center">
                  <div id="section2-scrolldown">
                  </div>
                </div>
              </div>
              <div id="section3" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet" style="margin:0;padding:40px 0 40px 0;width:100%;background-color:#399aed;text-align:center; justify-content:center">
                <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
                  <span style="color:#ffffff; font-size:40px; margin-bottom:20px;line-height:42px">Korlátlan, lejárat nélküli hozzáférés</span>
                </div>
                <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
                  <span style="color:#ffffff; font-size:25px; margin-bottom:20px;line-height:27px">Velünk megtanulhatsz angolul, mert teljeskörűen lefedjük a nyelvi készségeket!</span>
                </div>
                <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
                  <button onclick="location.href='/shoppingcart?products={{$product->id}}'" class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored" style="background:#ffffff; color:#399aed; width:150px; height:40px;">
                    KOSÁRBA
                  </button>
                </div>
              </div>
              <div id="section_new_4" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0; padding:0px 0px 0px 0;width:100%;background-color:#ffffff;justify-content:center;width:100%">
                <div class="section_new_4_divs mdl-grid mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--order-2-tablet mdl-cell--order-2-phone" style="width:50%;background-color:#006e73;margin:0px;padding:40px 0px 100px 0px">
                  <div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--4-col-phone"></div>
                  <div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--4-col-phone" style="margin:0px;padding:0px;margin-top:50px">
                    <span style="font-size:28px;color:#f29f00;line-height:30px">Nincsenek rejtett költségek és<br> egyéb kiadások! </span>
                  </div>
                  <div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--4-col-phone"></div>
                  <div class="mdl-cell mdl-cell mdl-cell--8-col mdl-cell--4-col-phone" style="margin:0px;padding:0px;margin-top:30px;">
                    <span style="font-size:40px;color:#ffffff;line-height:40px">Akkor angolozhatsz<br> amikor csak szeretnél!<br> Az új leckéket is ingyen<br> megkapod!</span>
                  </div>
                </div>
                <div id="section_new_4_img" class="section_new_4_divs mdl-grid mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-cell--order-1-tablet mdl-cell--order-1-phone" style="width:50%;margin:0px;padding:0px;background:url('/images/course_salespage_10_section_new4_img.png') no-repeat; background-size:100% 100%;">
                </div>
              </div>
              <div id="section_new_5" class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:70px 0 240px 0;width:100%;background-color:#ebf3ef; justify-content:center">
                <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
                  <span style="color:#000; font-size:40px; margin-bottom:20px;">Az IMPREVO számokban</span>
                </div>
                <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
                  <span style="color:#000; font-size:25px; margin-bottom:20px;">Az árak 2016 februári tartományokat vagy átlagot mutatnak, kategóriánként legalább<br>
                    3 piaci szereplő árainak figyelembevételével.</span>
                  </div>
                </div>
                <div id="section_new_6" class="mdl-grid mdl-cell mdl-cell--12-col" style="margin:0;padding:0px 0 0px 0;width:100%;background:url('/images/course_salespage_10_section_carsoule_back.png') no-repeat; justify-content:center;z-index:10">
                  <div class="section_new6_card mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--7-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding:0px;margin:0px; margin-top:-161px;">
                    <div class="mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="border:1px solid #d7d7d7;padding:0px;  margin:0px; background:#ffffff; position:relative;height:197px">
                      <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;padding:30px 0px 20px 0">
                        <span style="font-size:16px;color:black;font-weight:bold;">Átlagos szuperintenzív<br>angol tanfolyam</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;padding-bottom:15px;padding-bottom:50px;">
                        <span style="font-size:28px;color:black">~45,000 Ft</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="position:absolute;bottom:0px;background-color:#ebf3ef;text-align:center;border-top:1px solid #d7d7d7;height:35px;padding:5px 0px 0px 0px">
                        <span style="margin-top:10px;font-size:18px;font-weight:bold;color:black">100 óra</span>
                      </div>
                    </div>
                    <div class="section_new6_card mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="border:1px solid #d7d7d7;padding:0px;  margin:0px; background:#ffffff;position:relative;height:197px">
                      <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;padding:30px 0px 20px 0">
                        <span style="font-size:16px;color:black;font-weight:bold;">Magántanár</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;padding-bottom:50px;">
                        <span style="font-size:25px;color:black">2,500-5,000 Ft</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="position:absolute;bottom:0px;background-color:#ebf3ef;text-align:center;border-top:1px solid #d7d7d7;height:35px;padding:5px 0px 0px 0px">
                        <span style="margin-top:10px;font-size:18px;font-weight:bold;color:black">1 óra</span>
                      </div>
                    </div>
                    <div class="section_new6_card mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="border:1px solid #d7d7d7;padding:0px;  margin:0px; background:#ffffff; position:relative;height:197px">
                      <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;padding:30px 0px 20px 0">
                        <p style="font-size:16px;color:black;font-weight:bold;">Nyelviskola</p>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;padding-bottom:50px;">
                        <span style="font-size:25px;color:black">1,500-2,500 Ft</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="position:absolute;bottom:0px;background-color:#ebf3ef;text-align:center;border-top:1px solid #d7d7d7;height:35px;padding:5px 0px 0px 0px">
                        <span style="margin-top:10px;font-size:18px;font-weight:bold;color:black">1 óra</span>
                      </div>
                    </div>
                    <div class="section_new6_card mdl-grid mdl-grid--no-spacing mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="box-shadow: #101010 0px 0px 15px;border:1px solid #d7d7d7;padding:0px;  margin:0px; background:#ffffff;">
                      <div id="section_new6_selected_card" class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin-top:-35px;background-color:#f29f00;text-align:center;height:35px;padding:5px 0px 0px 0px">
                        <span style="font-size:16px;color:white;">A LEGJOBB VÁLASZTÁS</span>
                        <div class="arrowShadow"></div>
                      </div>

                      <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;padding:20px 0px 0px 0">
                        <img src="/images/course_salespage_10_section_new5_img.png" style="font-size:16px;color:black;font-weight:bold;"></img>
                      </div>

                      <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;padding:10px 0px 20px 0">
                        <span style="font-size:16px;color:black;font-weight:bold;">IMPREVO nyelvtanfolyam</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center;padding-bottom:15px;">
                        <span style="font-size:25px;color:black">144 Ft/lecke</span>
                      </div>
                      <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="background-color:#ebf3ef;text-align:center;border-top:1px solid #d7d7d7;height:35px;padding:5px 0px 0px 0px">
                        <span style="margin-top:10px;font-size:18px;font-weight:bold;color:black">és örökre a tiéd</span>
                      </div>
                    </div>
                  </div>
                  <div id="section2" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;width:100%;background:url('/images/course_salespage_10_section_carsoule_back.png') no-repeat;">
                    <div id="section2-title" class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:70px;text-align:center">
                      <span style="font-size:14px;color:#399aed">{!! trans('coursesales10.span17') !!}</span>
                    </div>
                    <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="text-align:center">
                      <span style="line-height:40px; font-size:30px;">{!! trans('coursesales10.span18') !!}</span>
                    </div>
                    <div id="owl-carousel-div" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="padding-top:50px;justify-content:center;">
                      <div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--middle mdl-cell--hide-tablet mdl-cell--hide-phone" style="">
                        <a id="carousel_prev" class="a-next-prev"><i class="fa fa-chevron-left"></i></a>
                      </div>
                      <div id="owl-carousel" class="owl-carousel owl-theme" data-plugin-carousel data-plugin-options='{ "dots": true, "autoplay": false, "autoplayTimeout": 3000, "loop": false, "margin": 10, "nav": false, "responsive": {"0":{"items":1 }, "1000":{"items":1 }, "1500":{"items":3 } }  }'
                      style="width:70%;">
                      <div class="item" style="">
                        <div class="mdl-grid mdl-cell mdl-cell--12-col">
                          <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
                            <img src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
                            <span style="font-size:15px;">“ Rendszerezett, jól felépített tananyag, könnyű használni. Engem mindig magával ragad, nehéz abbahagyni. ”</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
                            <span style="font-size:15px;font-weight:bold">Gabi</span>
                          </div>
                        </div>
                      </div>
                      <div class="item" style="">
                        <div class="mdl-grid mdl-cell mdl-cell--12-col">
                          <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
                            <img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
                            <span style="font-size:15px;">“ Egy igazán modern segítség a nyelvtanuláshoz. Az ingyenes leckékkel érdemes kipróbálni, nem lehet veszíteni rajta. ”</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
                            <span style="font-size:15px;font-weight:bold">István</span>
                          </div>
                        </div>
                      </div>
                      <div class="item" style="">
                        <div class="mdl-grid mdl-cell mdl-cell--12-col">
                          <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
                            <img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
                            <span style="font-size:15px;">“ Ez a tananyag alkalmas az önálló tanulásra. Még egészen az elején tartok, de már egyre jobban megértem a hallott szöveget is. ”</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
                            <span style="font-size:15px;font-weight:bold">ANNA</span>
                          </div>

                        </div>
                      </div>
                      <div class="item" style="">
                        <div class="mdl-grid mdl-cell mdl-cell--12-col">
                          <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
                            <img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
                            <span style="font-size:15px;">“ Kreatív, jó humorú, logikus, könnyen emészthető. ”</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
                            <span style="font-size:15px;font-weight:bold">Zsuzsi</span>
                          </div>

                        </div>
                      </div>
                      <div class="item" style="">
                        <div class="mdl-grid mdl-cell mdl-cell--12-col">
                          <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
                            <img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
                            <span style="font-size:15px;">“ Korrekt, nagyon részletes magyarázatokkal, és az ára is megfelelő. És az, hogy korlátlan a hozzáférés, király! ”</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
                            <span style="font-size:15px;font-weight:bold">Ottó</span>
                          </div>

                        </div>
                      </div>
                      <div class="item" style="">
                        <div class="mdl-grid mdl-cell mdl-cell--12-col">
                          <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
                            <img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
                            <span style="font-size:15px;">“ Célszerű. Érthető, nem kell hozzá előzetesen nyelvtani zseninek lenni. Tetszetős a szavak memoriter módszere, … ”</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
                            <span style="font-size:15px;font-weight:bold">Zsolt</span>
                          </div>

                        </div>
                      </div>
                      <div class="item" style="">
                        <div class="mdl-grid mdl-cell mdl-cell--12-col">
                          <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
                            <img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
                            <span style="font-size:15px;">“ Ha teljes körű angol nyelvórákat szeretnél venni, és nem szeretnél csoportos foglalkozáson részt venni,... ”</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
                            <span style="font-size:15px;font-weight:bold">Bea</span>
                          </div>

                        </div>
                      </div>
                      <div class="item" style="">
                        <div class="mdl-grid mdl-cell mdl-cell--12-col">
                          <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
                            <img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
                            <span style="font-size:15px;">“ Olcsón, és profi rendszerrel, könnyedén a saját tempódban tudsz megtanulni, a kezdő szinttől kezdve a haladó középfokig.... Ajánlom mindenkinek!”</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
                            <span style="font-size:15px;font-weight:bold">Péter</span>
                          </div>

                        </div>
                      </div>
                      <div class="item" style="">
                        <div class="mdl-grid mdl-cell mdl-cell--12-col">
                          <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
                            <img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
                            <span style="font-size:15px;">“ Összességében nagyon jó lehetőség a nyelvtanulásra. A tanfolyamok töredékéért kapom minimum ugyanazt, de szerintem ... ”</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
                            <span style="font-size:15px;font-weight:bold">Kázmér</span>
                          </div>
                        </div>
                      </div>
                      <div class="item" style="">
                        <div class="mdl-grid mdl-cell mdl-cell--12-col">
                          <div class="mdl-grid mdl-cell mdl-cell--12-col" style="justify-content:center">
                            <img  src="/images/empty-avator.png" alt="" style="width:110px; height:110px">
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="text-align:center">
                            <span style="font-size:15px;">“ Nyelvtanfolyamra is járok! E mellé nagyon nagy segítség az IMPREVO! A tanfolyamon általában sietnek,...! ”</span>
                          </div>
                          <div class="mdl-cell mdl-cell mdl-cell--12-col" style="padding:0; margin:0px;text-align:center">
                            <span style="font-size:15px;font-weight:bold">György</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="mdl-cell mdl-cell mdl-cell--1-col mdl-cell--1-col-tablet mdl-cell--middle mdl-cell--hide-tablet mdl-cell--hide-phone" style="">
                      <a id="carousel_next" class="a-next-prev" style="float:left"><i class="fa fa-chevron-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
              <div id="section3" class="mdl-grid mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet" style="margin:0;padding:40px 0 40px 0;width:100%;background-color:#399aed;text-align:center; justify-content:center">
                <div class="mdl-grid mdl-cell mdl-cell--8-col mdl-cell--hide-tablet mdl-cell--hide-phone" style="margin:0;padding:0px;justify-content:center">
                  <span id="section3_span" style="color:#ffffff; font-size:40px; margin-right:20px;margin-top:10px;padding-top:5px;">További információk a tanfolyamról</span>
                  <button id="section3_button" onclick="location.href='/shoppingcart?products=1'" class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored" style="margin-top:10px;">
                    RÉSZLETEK
                  </button>
                </div>
                <div class="mdl-grid mdl-cell mdl-cell--hide-desktop mdl-cell--8-col-tablet mdl-cell--4-col-phone" style="margin:0;padding:0px;justify-content:center">
                  <div class="mdl-cell mdl-cell mdl-cell--8-col-tablet mdl-cell--4-col" style="text-align:center">
                    <span style="color:#ffffff; font-size:30px; margin-right:20px;padding-top:5px;">További információk a tanfolyamról</span>
                  </div>
                  <div class="mdl-cell mdl-cell mdl-cell--8-col-tablet mdl-cell--4-col" style="text-align:center">
                  <button id="try-it-button-second" onclick="location.href='/shoppingcart?products=1'" class="mdl-button  mdl-js-button mdl-button--raised mdl-button--colored">
                    RÉSZLETEK
                  </button>
                  </div>
                </div>
              </div>
            </div>
            @include('layouts.footer')
          </div>
        </main>
      </div>
      <script>
      function goLogin() {
        location.href = '/login'
      }

      function goProfile(e) {
        e.preventDefault();
        document.getElementById('logout-form').submit();
      }

      function Logout(e) {
        e.preventDefault();
        document.getElementById('logout-form').submit();
      }

      $(document).ready(function() {

        var owl = $("#owl-carousel");


        // Custom Navigation Events
        $("#carousel_next").click(function() {
          console.log('next');
          console.log(owl);
          owl.trigger('next.owl.carousel');
        });

        $("#carousel_prev").click(function() {
          console.log('prev');
          owl.trigger('prev.owl.carousel');
        });

      });

      </script>
      <script>


      function isMobileDevice() {
        return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
      };

      console.log(isMobileDevice());
      if (!isMobileDevice()){
        $('ul.nav li.dropdown').hover(function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
        }, function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
        });
      }

      $('#my-account-button-logged').hover(function() {
        $('#dropdown3').stop(true, true).delay(100).fadeIn(500);
      }, function() {
        $('#dropdown3').stop(true, true).delay(100).fadeOut(500);
      });

      $('.phone-footer-accordion .item .heading').click(function() {

        var a = $(this).closest('.item');
        var b = $(a).hasClass('open');
        var c = $(a).closest('.accordion').find('.open');

        if(b != true) {
          $(c).find('.content').slideUp(200);
          $(c).removeClass('open');
        }

        $(a).toggleClass('open');
        $(a).find('.content').slideToggle(200);

      });
      </script>
      <script>
      var html = '';
      var left_days = null;
      var left_hours = null;
      var left_mins = null;
      var remain_orders = null;
      var distance = 0;
      //var timeLocal = new Date();

      //var millDiff = timeLocal.getTime() - timeServer.getTime();


      @if ($product)
      @if ($product->sale_price)
      @if ($product->sale_price_end_date)
      var timeServer = new Date(<?='"'.date('Y-m-d H:i:s').'"' ?>);
      end_time = new Date("{{$product->sale_price_end_date}}").getTime();
      distance = (end_time - timeServer.getTime()) / 1000 ;
      console.log(distance);
      if (distance > 0)
      {
        left_mins = Math.floor((distance / 60) % 60);
        left_hours = Math.floor((distance / (60 * 60)) % 24);
        left_days = Math.floor(distance / (60 * 60 * 24));
      }
      @endif

      @if ($product->sale_price_orders_num)
      var total = parseInt("{{$product->sale_price_orders_num}}");
      var current = parseInt("{{$product->sale_price_orders_current}}");
      remain_orders = total - current;
      @endif
      console.log(distance, remain_orders);
      if (distance > 0 && remain_orders > 0)
      {
        $('.days').html(left_days);
        $('.mins').html(left_mins);
        $('.hours').html(left_hours);
        $('.hours').html(left_hours);
        $('.sales_count').html(remain_orders);
      }
      @endif
      @endif

      </script>
    </body>
    </html>
