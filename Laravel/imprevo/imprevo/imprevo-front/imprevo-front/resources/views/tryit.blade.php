@extends('layouts.app')

@section('content')
<main class="mdl-layout__content" style="width:100%;padding-bottom:140px;">
    <div style="background: #9fc3ae;padding:10px 40px;font-size: 16px;height:40px;">
        <span><a href="/" style="color:#d5e7dc;line-height: 40px;text-decoration: none">Imprevo</a></span>
        <span style="color:#d5e7dc;line-height: 40px">/</span>
        <span style="color:#fff;line-height: 40px">Try it</span>
    </div>
    <div class="mdl-grid portfolio-max-width portfolio-contact">
        <div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--4dp">
            <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Contact</h2>
            </div>
            <div class="mdl-card__media">
                <img class="article-image" src=" images/contact-image.jpg" border="0" alt="">
            </div>
            <div class="mdl-card__supporting-text">
                <p>
                    Enim labore aliqua consequat ut quis ad occaecat aliquip incididunt. Sunt nulla eu enim irure enim nostrud aliqua consectetur ad consectetur sunt ullamco officia. Ex officia laborum et consequat duis.
                </p>
                <p>
                    Excepteur reprehenderit sint exercitation ipsum consequat qui sit id velit elit. Velit anim eiusmod labore sit amet.
                </p>
                <form action="#" class="">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" pattern="[A-Z,a-z, ]*" type="text" id="Name">
                        <label class="mdl-textfield__label" for="Name">Name...</label>
                        <span class="mdl-textfield__error">Letters and spaces only</span>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" id="Email">
                        <label class="mdl-textfield__label" for="Email">Email...</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <textarea class="mdl-textfield__input" type="text" rows="5" id="note"></textarea>
                        <label class="mdl-textfield__label" for="note">Enter note</label>
                    </div>
                    <p>
                        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit">
                            Submit
                        </button>
                    </p>
                </form>
            </div>
        </div>
    </div>
  </main>
  @endsection
