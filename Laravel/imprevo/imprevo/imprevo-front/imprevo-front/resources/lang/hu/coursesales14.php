<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'span1' => 'Fejleszd az angol szókincsedet<br> az IMPREVO tanfolyamával!',
	'span2' => 'Folyamatosan bővülő szókincsfejlesztő leckék, gondosan összeválogattott szavakkal és témákkal.',
	'span3' => 'Közel 100 lecke, 2000 szóval és kifejezéssel',
	'span4' => 'Folyamatosan bővülő leckék',
	'span5' => 'Témák szerint rendszerezett szavak',
	'span6' => 'Szótanulás 2000+ szóval és',
	'span7' => 'anyanyelvi kiejtéssel',
	'span8' => '',
	'span9' => 'Gondosan válogatott szavak',
	'span10' => 'és témák',
	'span11' => 'Jól megszokott IMPREVO minőség',
	'span12' => 'Érthető, elemeire bontott igeidők és nyelvtan',
	'span13' => 'Most akciós áron',
	'span14' => 'Korlátlan, lejárat nélküli hozzáférés',
	'span15' => ' Interaktív szótanulás, témákra bontva, <br>teljesen kezdő szinttől sok-sok feladattal',
	'but1' => 'VÁSÁRLÁS MOST',
	'span16' => '* A feltüntetett ár bruttó ár, a 27% ÁFA-t tartalmazza!',
	'span17' => 'RÓLUNK MONDTÁK',
	'span18' => 'Akik már tanultak az IMPREVO-val',
	'span19' => 'Csatlakozz te is több ezer emberhez, akik már kipróbálták az IMREVO-t',
	'span20' => 'PRÓBÁLD KI INGYEN!',
	'span21' => 'Gyakran Ismételt kérdések',
	'span22' => 'Lejár az előfizetésem?',
	'span23' => 'Nem jár le. Ha egyszer megveszed a csomagot, akkor örökké használhatod, a tiéd.',
	'span24' => 'Tényleg meg lehet tanulni angolul az IMPREVO-val?',
	'span25' => 'Határozottan állítjuk, hogy meg! Rengeteg anyagot készítettünk és gondosan felépítettük a rendszert, ha beleteszed a szükséges energiát, véleményünk szerint a legjobb magyar nyelven elérhető tanfolyamot kapod meg. <a target="_blank" href="https://imprevo.hu/sales/course/11">Egy külön cikkben is írtunk róla</a>, ha érdekelnek a részletek.',
	'span26' => 'Frissítéseket és új leckéket megkapom?',
	'span27' => 'Adott tanfolyamhoz érkeznek tananyagfrissítések és új leckék, ezeket természetesen ingyen megkapod. A különálló tanfolyamokat igény szerint vásárolhatod meg.',
	'span28' => 'Van az IMPREVO más nyelvre is angolon kívül?',
	'span29' => 'Az IMPREVO-val egyelőre csak angolul lehet tanulni.',
	'span30' => 'Lehet egyedül is tanulni vele?',
	'span31' => 'Igen. Kifejezetten az egyedüli nyelvtanuláshoz terveztük és készítettük, önmagában vagy kiegészítőnek is használható.',
	'span32' => 'Hogyan tudok fizetni, lehet részletre?',
	'span33' => 'A fizetés biztonságos, erre szakosodott partnereinken keresztül lehetséges. Van bankkártyás fizetés, normál banki átutalás és PayPal is, részletfizetés nem lehetséges.',
	'span34' => 'Ha van még kérdésed, nézd meg',
	'span35' => 'MI az IMPREVO',
	'span36' => 'vagy, hogy',
	'span37' => 'Kik is vagyunk',
	'span38' => 'Lesz használható angoltudásom rövid idő alatt?',
	'span39' => 'Ha a rövid idő 1-2 nap vagy hét, akkor a válasz az, hogy nem. Megfelelő idő és energia befektetésével viszont pár hónap alatt már látványosat fejlődhetsz.',	
	'span40' => 'Sok az átverés ezen a téren, nem bízom bennetek!',
	'span41' => 'És mi ezt teljesen megértjük. Egy rövid, <a target="_blank" href="https://imprevo.hu/register?free=1">ingyenes regisztráció</a> után a kurzusainkból több leckét is kipróbálhatsz fizetés nélkül. Az Általános Angol tanfolyamunk teljes első szintje ingyenesen elérhető! A puding próbája…',	
	'span42' => 'Ha csak napi 20 percet tudok foglalkozni vele, akkor is megéri?',
	'span43' => 'Minden gyakorlás számít a tanulás során, ha csak napi 10 percet foglalkozol vele, akkor is megéri! Elvárásaidat a befektetett energiához igazítsd, a rendszer hatékonyságát ez nem csökkenti!',	
	'span44' => 'Sok az ingyenes angolos anyag a neten, miért vagytok ti jobbak?',
	'span45' => 'A nyelvtani videók minden sallangtól mentesek csak a lényeget tartalmazzák ahhoz azonban, hogy végignézd őket, 1,5 napig kellene gép előtt ülnöd folyamatosan. Ehhez több száz oldalnyi szövegértés, anyanyelvi hanganyagok, több ezer feladat tartozik, szintekre bontva alaptól a felsőfokig. A tanfolyam akár 200 órányi gyakorlást is lehetővé tesz, ilyet ilyen áron máshol nem találsz szerintünk. ',	
	'span46' => 'Játékos, interaktív tanulás!',	
];