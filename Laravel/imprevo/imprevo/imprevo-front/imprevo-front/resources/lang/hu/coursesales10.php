<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'span1' => 'Tanulj meg angolul saját tempódban<br> a teljeskörű IMPREVO Online Angol tanfolyammal',
	'span2' => 'Interaktív leckék, izgalmas témák, teljesen kezdő szinttől sok-sok feladattal',
	'span3' => '200+ lecke teljesen az alapoktól a felsőfokig',
	'span4' => 'Irányított, összeszedett haladás, saját ütemedben',
	'span5' => 'Folyamatos hallás utáni szövegértés gyakorlás',
	'span6' => 'Szótanulás',
	'span7' => '5000+ szóval',
	'span8' => 'anyanyelvi kiejtéssel',
	'span9' => '600+ magyarázó videó',
	'span10' => 'és több ezer feladat',
	'span11' => 'Érthető, elemeire bontott igeidők és nyelvtan',
	'span12' => 'Érthető, elemeire bontott igeidők és nyelvtan',
	'span13' => 'Most akciós áron',
	'span14' => 'Korlátlan, lejárat nélküli hozzáférés',
	'span15' => 'Interaktív leckék, izgalmas témák, teljesen kezdő szinttől sok-sok feladattal',
	'but1' => 'VÁSÁRLÁS MOST',
	'span16' => '* A feltüntetett ár bruttó ár, a 27% ÁFA-t tartalmazza!',
	'span17' => 'RÓLUNK MONDTÁK',
	'span18' => 'Akik már tanultak az IMPREVO-val',
	'span19' => 'Csatlakozz te is több ezer emberhez, akik már kipróbálták az IMREVO-t',
	'span20' => 'PRÓBÁLD KI INGYEN!',
	'span21' => 'Gyakran Ismételt kérdések',
	'span22' => 'Lejár az előfizetésem?',
	'span23' => 'Nem jár le. Ha egyszer megveszed a csomagot, akkor örökké használhatod, a tiéd. ',
	'span24' => 'Tényleg meg lehet tanulni angolul az IMPREVO-val?',
	'span25' => 'Határozottan állítjuk, hogy meg! Rengeteg anyagot készítettünk és gondosan felépítettük a rendszert, ha beleteszed a szükséges energiát, véleményünk szerint a legjobb magyar nyelven elérhető tanfolyamot kapod meg. <a target="_blank" href="https://imprevo.hu/sales/course/10">Egy külön cikkben</a> is írtunk róla, ha érdekelnek a részletek.',
	'span26' => 'Frissítéseket és új leckéket megkapom?',
	'span27' => 'Adott tanfolyamhoz érkeznek tananyagfrissítések és új leckék, ezeket természetesen ingyen megkapod. A különálló tanfolyamokat igény szerint vásárolhatod meg.',
	'span28' => 'Lesz használható angoltudásom rövid idő alatt?',
	'span29' => 'Ha a rövid idő 1-2 nap vagy hét, akkor a válasz az, hogy nem. Megfelelő idő és energia befektetésével viszont pár hónap alatt már látványosat fejlődhetsz.',
	'span30' => 'Sok az átverés ezen a téren, nem bízom bennetek!',
	'span31' => 'És mi ezt teljesen megértjük. Egy rövid, <a target="_blank" href="https://imprevo.hu/register?free=1">ingyenes regisztráció</a> után a kurzusainkból több leckét is kipróbálhatsz fizetés nélkül. Az Általános Angol tanfolyamunk teljes első szintje ingyenesen elérhető! A puding próbája…',
	'span32' => 'Sok az ingyenes angolos anyag a neten, miért vagytok ti jobbak?',
	'span33' => 'A nyelvtani videók minden sallangtól mentesek csak a lényeget tartalmazzák ahhoz azonban, hogy végignézd őket, 1,5 napig kellene gép előtt ülnöd folyamatosan. Ehhez több száz oldalnyi szövegértés, anyanyelvi hanganyagok, több ezer feladat tartozik, szintekre bontva alaptól a felsőfokig. A tanfolyam akár 200 órányi gyakorlást is lehetővé tesz, ilyet ilyen áron máshol nem találsz szerintünk. ',
	'span34' => 'Ha van még kérdésed, nézd meg',
	'span35' => 'MI az IMPREVO',
	'span36' => 'vagy, hogy',
	'span37' => 'Kik is vagyunk',
	'span38' => 'Van az IMPREVO más nyelvre is angolon kívül?',
	'span39' => 'Az IMPREVO-val egyelőre csak angolul lehet tanulni. ',	
	'span40' => 'Lehet egyedül is tanulni vele?',
	'span41' => 'Igen. Kifejezetten az egyedüli nyelvtanuláshoz terveztük és készítettük, önmagában vagy kiegészítőnek is használható.',	
	'span42' => 'Hogyan tudok fizetni, lehet részletre?',
	'span43' => 'A fizetés biztonságos, erre szakosodott partnereinken keresztül lehetséges. Van bankkártyás fizetés, normál banki átutalás és PayPal is, részletfizetés nem lehetséges.',	
	'span44' => 'Ha csak napi 20 percet tudok foglalkozni vele, akkor is megéri?',
	'span45' => 'Minden gyakorlás számít a tanulás során, ha csak napi 10 percet foglalkozol vele, akkor is megéri! Elvárásaidat a befektetett energiához igazítsd, a rendszer hatékonyságát ez nem csökkenti!',		
	'span46' => 'Játékos, interaktív tanulás!',	
];