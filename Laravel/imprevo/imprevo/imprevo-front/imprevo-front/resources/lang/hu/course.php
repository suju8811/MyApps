<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'tab1' => 'Ismertető',
	'tab2' => 'Leckék',
	'tab3' => 'Tartalomjegyzék',
	'tab4' => 'Újdonságok',
	'tab5' => 'Vissza a tanfolyamokhoz',
	'input_placeholder1' => 'Keresés itt ...',
	'strong1' => 'AZ ÖSSZES LECKE CSAK IMPREVO ELŐFIZETÉSSEL ÉRHETŐ EL!',
	'button1' => 'UGRÁS A LECKÉRE',
	'strong2' => 'ÚJDONSÁGOK',
	'th1' => 'Feladat neve',
	'th2' => 'Leckék',
	'th3' => 'Hozzáadva',
	'mobile_tab1'=> 'Ismertető.',
	'mobile_tab2'=> 'Leckék.',
	'mobile_tab3'=> 'Tartalom',
	'mobile_tab4'=> 'Újdonságok.',
	'button2' => 'FOLYTATOM',
	'input_placeholder2' => 'Keresés itt …',
	'p1' => 'Keresési eredmények:',
	'button3' => 'UGRÁS',
	'button4' => 'KERESÉS',
];
