<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'profile' => 'Profil',
	'profilesettings' => 'Profilbeállítások',
	'basicinfo' => 'ÁLTALÁNOS INFO',
	'yourname' => 'Neved',
	'youremailaddress' => 'E-mail címed',
	'passwordsettings' => 'JELSZÓBEÁLLÍTÁSOK',
	'yourcurrentpassword' => 'Jelenlegi jelszó',
	'yournewpassword' => 'Új jelszavad',
	'confirmnewpassword' => 'Új jelszó megerősítése',
	'billingaddress' => 'SZÁMLÁZÁSI CÍM',
	'country' => 'Ország',
	'towncity' => 'Város',
	'postcode' => 'Irányítószám',
	'county' => 'Megye',
	'streetaddress' => 'Cím',
	'companydetail' => 'CÉGES ADATOK',
	'companyname' => 'Cégnév',
	'taxid' => 'Adószám',
	'euvat' => 'EU adószám',
	'save' => 'MENTÉS',
	'sidetoptitle' => 'Imprevo hírlevél',
	'sidespan1' => 'Iratkozz fel ingyenes anyagokért és újdonságokért!',
	'sidespan2' => 'Hasznos anyagaink',
	'sidespan3' => 'Blog kategóriák',
	'sidespan4' => 'INGYENES TANFOLYAM',
];
