<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dear' => 'Kedves felhasználó!!',
    'span1' => 'Szeretnénk értesíteni, hogy az',
	'span2' => 'Rendelés azonosító:',
	'span3' => 'Választott fizetési mód:',
	'span4' => 'Arra az esetre, ha problémába ütköztél a rendelés során, szívesen állunk rendelkezésedre. Az alábbi módokon léphetsz velünk kapcsolatba:',
	'span5' => 'Telefon:',
	'span6' => 'Email:',	
	'span7' => 'A rendelésedet banki átutalással is fizetheted. Banki adataink:',	
	'span8' => 'Cégnév:',	
	'span9' => 'Bank:',	
	'span10' => 'Bankszámlaszám:',	
	'span11' => 'Kérlek vedd figyelembe, hogy a függõben lévõ rendeléseket a rendszerünk 2 hét után automatikusan törli.',	
	'span12' => 'Ha kérdésed merülne fel, szívesen állunk rendelkezésedre..',	
	'span13' => 'Az Imprevo csapata!',	
	'span14' => 'oldalon leadott rendelésed még nem került kifizetésre.',
];
