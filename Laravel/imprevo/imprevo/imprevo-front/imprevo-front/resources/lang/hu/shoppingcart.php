<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'shop' => 'Shop',
	'shoppingcart-span1' => 'Kosár - 1 termék',
	'total' => 'ÖSSZESEN:',
	'continue' => 'TOVÁBB',
	'shoppingcart-span2' => 'HASONLÓ TERMÉKEK',
	'shoppingcart-span3' => 'Bruttó ár',
	'shoppingcart-span4' => 'Diszkont ár',
	'shoppingcart-span5' => 'HOZZÁADOM',
];
