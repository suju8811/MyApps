<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'imprevo' => 'Imprevo',
	'IMP' => 'IMPREVO',
	'dashboard' => 'Kezelőpult',
	'bannertext-green-title' => 'IMPREVO - Interaktív online angol leckék az IMPREVO-val!',
	'bannertext-green-description' => 'Videós magyarázatokkal és szótanuló játékokkal.',
	'bannerbutton1' => 'TUDJ&nbsp;MEG&nbsp;TÖBBET!',
	'bannerbutton2' => 'KIPRÓBÁLOM',
	'courses' => 'Kurzusok',
	'trythese' => 'Próbáld ki ezeket!',
	'trythesespan1' => 'Teljesen kezdő szinttől a felsőfokig',
	'trythesebutton' => 'ÉRDEKEL',
	'trythesespan2' => '50 anyanyelvi mini párbeszéd',
	'cookiespan1' => 'A böngésződben cookie-kat (adatfájlokat) tárolunk. A cookie-k személyek azonosítására nem alkalmasak, szolgáltatásaink biztosításához szükségesek.',
	'cookiemore' => 'További info',
	'cookieaccept' => 'Elfogadom',
	'continue' => 'TOVÁBB',
	'freelessons' => 'Ingyenes leckék ebben a kurzusban',
	'currentlevel' => 'JELENLEGI SZINTED',
	'choosefrom' => 'Válassz a kurzus tartalmából!',
	'currentlesson' => 'Jelenlegi lecke',
	'lessons' => 'lecke',
];
