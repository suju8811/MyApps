<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'span1' => 'Fejleszd hatékonyan a szókincsedet az<br/>IMPREVO Online Angol rendszerével!',
	'span2' => 'Interaktív szótanulás, témákra bontva, teljesen kezdő szinttől sok-sok feladattal',
	'span3' => 'Több mint 100 lecke,~2000 szóval',
	'span4' => 'Folyamatosan bővülő leckék.',
	'span5' => 'Témák szerint rendszerezett szavak.',
	'span6' => 'Szótanulás 2000+ szóval, anyanyelvi kiejtéssel',
	'span7' => 'feladatok',
	'span8' => '',
	'span9' => 'Gondosan válogatott szavak és témák.',
	'span10' => 'és élethelyzetek',
	'span11' => 'Jól megszokott IMPREVO minőség.',
	'span12' => 'Érthető, elemeire bontott igeidők és nyelvtan',
	'span13' => 'Most akciós áron',
	'span14' => 'Korlátlan, lejárat nélküli hozzáférés',
	'span15' => 'Interaktív szövegértés leckék, izgalmas témák, sok-sok szótanulással',
	'but1' => 'VÁSÁRLÁS MOST',
	'span16' => '* A feltüntetett ár bruttó ár, a 27% ÁFA-t tartalmazza!',
	'span17' => 'RÓLUNK MONDTÁK',
	'span18' => 'Akik már tanultak az IMPREVO-val',
	'span19' => 'Csatlakozz te is több ezer emberhez, akik már kipróbálták az IMREVO-t',
	'span20' => 'PRÓBÁLD KI INGYEN!',
	'span21' => 'Gyakran Ismételt kérdések',
	'span22' => 'Proin gravida fermentum venenatis?',
	'span23' => 'Phasellus mattis ultrices orci, vitae commodo libero dignissim id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris maximus consequat turpis vitae placerat. Praesent quis sagittis risus, mollis dignissim diam. Aliquam et elit tristique nunc varius egestas eu vitae. ',
	'span24' => 'Proin gravida fermentum venenatis?',
	'span25' => 'Aenean cursus augue nec justo vestibulum, at sollicitudin nisl ullamcorper. Pellentesque sed massa nec arcu ornare rhoncus.',
	'span26' => 'Proin gravida fermentum venenatis?',
	'span27' => 'Morbi egestas mi rutrum metus placerat auctor. Nulla nunc sem, facilisis nec arcu porttitor, sodales varius eros. Integer mi sapien, tincidunt non tristique vitae, molestie at nibh. Aliquam ut dui nisl.',
	'span28' => 'Proin gravida fermentum venenatis?',
	'span29' => 'Phasellus mattis ultrices orci, vitae commodo libero dignissim id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
	'span30' => 'Proin gravida fermentum venenatis?',
	'span31' => 'Morbi egestas mi rutrum metus placerat auctor. Nulla nunc sem, facilisis nec arcu porttitor, sodales varius eros. Integer mi sapien, tincidunt non tristique vitae, molestie at nibh. Aliquam ut dui nisl.',
	'span32' => 'Proin gravida fermentum venenatis?',
	'span33' => 'Mauris maximus consequat turpis vitae placerat. Praesent quis sagittis risus, mollis dignissim diam. Aliquam et elit tristique nunc varius egestas eu vitae. Aenean cursus augue nec justo vestibulum, at sollicitudin nisl ullamcorper. Pellentesque sed massa nec arcu ornare rhoncus nec eget nulla.',
	'span34' => 'Ha van még kérdésed, nézd meg',
	'span35' => 'MI az IMPREVO',
	'span36' => 'vagy, hogy',
	'span37' => 'Kik is vagyunk',
	'span38' => 'Lesz használható angoltudásom rövid idő alatt?',
	'span39' => 'Ha a rövid idő 1-2 nap vagy hét, akkor a válasz az, hogy nem. Megfelelő idő és energia befektetésével viszont pár hónap alatt már látványosat fejlődhetsz.',	
	'span40' => 'Sok az átverés ezen a téren, nem bízom bennetek!',
	'span41' => 'És mi ezt teljesen megértjük. Egy rövid, <a target="_blank" href="https://imprevo.hu/register?free=1">ingyenes regisztráció</a> után a kurzusainkból több leckét is kipróbálhatsz fizetés nélkül. Az Általános Angol tanfolyamunk teljes első szintje ingyenesen elérhető! A puding próbája…',	
	'span42' => 'Ha csak napi 20 percet tudok foglalkozni vele, akkor is megéri?',
	'span43' => 'Minden gyakorlás számít a tanulás során, ha csak napi 10 percet foglalkozol vele, akkor is megéri! Elvárásaidat a befektetett energiához igazítsd, a rendszer hatékonyságát ez nem csökkenti!',	
	'span44' => 'Sok az ingyenes angolos anyag a neten, miért vagytok ti jobbak?',
	'span45' => 'A nyelvtani videók minden sallangtól mentesek csak a lényeget tartalmazzák ahhoz azonban, hogy végignézd őket, 1,5 napig kellene gép előtt ülnöd folyamatosan. Ehhez több száz oldalnyi szövegértés, anyanyelvi hanganyagok, több ezer feladat tartozik, szintekre bontva alaptól a felsőfokig. A tanfolyam akár 200 órányi gyakorlást is lehetővé tesz, ilyet ilyen áron máshol nem találsz szerintünk. ',	
	'span46' => 'Most akciós áron:',	
];