<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'span1' => 'Leckék',
	'span2' => 'TOVÁBB',
	'span3' => 'Ide még nem került be egy lecke sem!',	
	'span4' => 'Jelenlegi lecke : ',
];