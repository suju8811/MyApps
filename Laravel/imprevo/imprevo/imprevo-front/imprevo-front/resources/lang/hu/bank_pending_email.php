<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dear' => 'Kedves',
    'span1' => 'Köszönjük, hogy az IMPREVO-t választottad.',
	'span2' => 'Mivel fizetéshez a banki átutalást választottad, a rendelésed állapota függőben van.',
	'span3' => 'Miután az átutalásod megérkezik a számlánkra teljes hozzáférésed lesz a vásárolt kurzushoz.',
	'span4' => 'Kérlek az átutalást az alábbi címre küld:',
	'span5' => 'Cég neve: Imprevo Kft.',
	'span6' => 'Bankszámlaszám: 10402142-50526771-77801015',
	'span7' => 'Rendelés azonosítód (közlemény rovatban feltüntetni):',
	'span8' => 'Siker - Rendelés!',
];
