<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dear' => 'Kedves felhasználó,',
    'span1' => 'Üdvözlünk az Imprevo-nál!',
	'span2' => 'Reméljük, hogy jól fogod magad érezni és sokat fogsz velünk tanulni.',
	'span3' => 'Az Imprevo-val nem kockáztatsz semmit, hiszen minden kurzusunkat ingyen kipróbálhatod. Az ingyenes leckékhez kattints az az alábbi linkre, majd válassz egy kurzust amibe szeretnél bepillantani.',
	'span4' => 'https://imprevo.hu/home',
	'span5' => 'Ha bármi kérdésed merülne fel, szívesen állunk rendelkezésedre.',
	'span6' => 'Az Imprevo csapata!',	
];
