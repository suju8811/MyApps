<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'span1' => 'Hírlevél',	
	'span2' => 'Iratkozz fel ingyenes anyagokért és újdonságokért!',
	'span3' => 'Email ...',
	'span4' => 'Hasznos anyagaink',
	'span5' => '20 részes ingyenes angol tanfolyam',
	'span6' => 'Angol Igeidők Összefoglaló Táblázat',
	'span7' => 'Tanulási stílusok – Te melyik vagy?',
	'span8' => 'Meg lehet tanulni az IMPREVO-val angolul?',
	'span9' => 'IMPREVO tartalomjegyzék',
	'span10' => 'Ingyenes Anyagok',
];