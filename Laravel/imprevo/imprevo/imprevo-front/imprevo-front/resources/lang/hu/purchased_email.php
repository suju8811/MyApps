<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dear' => 'Kedves felhasználó!',
    'span1' => 'Köszönjük, hogy minket választottál!',
	'span2' => 'A rendelési adataidat itt láthatod:',
	'span3' => 'Kurzus:',
	'span4' => 'Ár:',
	'span5' => 'Amennyiben a rendeléskor bankkártyával vagy PayPal-el fizettél, úgy a tanfolyamot azonnal elkezdheted használni. Lépj be az oldalunkon a vásárláskor használt email címmel és jelszóval. Egy másik email-ben elektronikus számlát küldünk. <br> Az alábbi linken hasznos útmutatókat találsz az oldal használatával kapcsolatban: <br> https://imprevo.hu/gyik',
	'span6' => 'Ha bármi kérdésed merülne fel, szívesen állunk rendelkezésedre.',
	'span7' => 'Az Imprevo csapata!',
];
