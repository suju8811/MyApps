<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'previous' => 'ELŐZŐ',
	'next' => 'KÖVETKEZŐ',
	'quizempty' => 'Ez a kvíz üres!.',
	'showfull' => 'Segítség',
	'exercise-span1' => 'Gratulálunk, befejezted a feladatot!',	
	'exercise-span2' => 'Íme a helyes válaszok',
	'exercise-span3' => 'Sajnáljuk, de ez a lecke csak az IMPREVO tanulói számára érhető el.',		
	'exercise-span4' => 'Csatlakozz te is tanulóinkhoz!',
	'exercise-but1' => 'Tudj meg többet!',
	'exercise-but2' => 'Újra',					
	'exercise-reporterror' => 'HIBAJELENTÉS',
	'exercise-but3' => 'KÖVETKEZŐ FELADAT',	
	'exercise-span5' => 'Ha hibát vagy problémát észleltés a feladattal kapcsolatban, kérlek jelezd nekünk.',
	'exercise-span6' => 'lecke, feladat',
	'exercise-span7' => 'Üzeneted',
	'exercise-span8' => 'Elküld',
	'exercise-span9' => 'MÉGSE',
	'exercise-span10' => 'Köszönjük a visszajelzésedet!',
	'exercise-span11' => 'Bezár',
	'yourscore'=>'Pontszámod: ',
	'exercise-span12' => 'Kérlek gépeld be a válaszod és nyomj egy entert!',
	'exercise-span13' => 'Kérlek gépeld be a megoldásod a sárga vonalra!',
	'exercise-span14' => 'Megjegyzés',
	'exercise-span15' => 'A válaszod helyes!',
	'exercise-span16' => 'A válaszod helytelen!',	
	'exercise-span17' => 'Kérdés',
	'exercise-span18' => 'Válaszod',
	'exercise-span19' => 'Helyes válasz',
	'exercise-span20' => 'A te válaszod',
];
