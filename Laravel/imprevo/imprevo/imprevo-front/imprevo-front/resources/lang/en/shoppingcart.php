<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'shop' => 'Shop',
	'shoppingcart-span1' => 'Shopping cart - 1 product',
	'total' => 'TOTAL:',
	'continue' => 'CONTINUE',
	'shoppingcart-span2' => 'SIMILAR PRODUCTS',
	'shoppingcart-span3' => 'Gross price',
	'shoppingcart-span4' => 'Sale price',
	'shoppingcart-span5' => 'ADD TO CART',
];
