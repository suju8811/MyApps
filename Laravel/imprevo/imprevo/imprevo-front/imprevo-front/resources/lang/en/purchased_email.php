<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dear' => 'Dear User!',
    'span1' => 'Thank you for your purchase at',
	'span2' => 'Your purchase details can be seen below:',
	'span3' => 'Course:',
	'span4' => 'Price:',
	'span5' => 'You will receive an electronic invoice in a separate email.',
	'span6' => 'If you have any question, please do not hesitate to contact us.',
	'span7' => 'Imprevo Team!',
];
