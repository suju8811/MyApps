<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'span1' => 'Tanulj meg angolul saját tempódban a teljeskörű IMPREVO Online Angol tanfolyammal',
	'span2' => 'Interaktív leckék, izgalmas témák, teljesen kezdő szinttől sok-sok feladattal',
	'span3' => '200+ lecke teljesen az alapoktól a felsőfokig',
	'span4' => 'Irányított, összeszedett haladás, saját ütemedben',
	'span5' => 'Folyamatos hallás utáni szövegértés gyakorlás',
	'span6' => 'Szótanulás',
	'span7' => '5000+ szóval',
	'span8' => 'anyanyelvi kiejtéssel',
	'span9' => '600+ magyarázó videó',
	'span10' => 'és több ezer feladat',
	'span11' => 'Érthető, elemeire bontott igeidők és nyelvtan',
	'span12' => 'Érthető, elemeire bontott igeidők és nyelvtan',
	'span13' => 'Most akciós áron',
	'span14' => 'Korlátlan, lejárat nélküli hozzáférés',
	'span15' => 'Interaktív leckék, izgalmas témák, teljesen kezdő szinttől sok-sok feladattal',
	'but1' => 'VÁSÁRLÁS MOST',
	'span16' => '* A feltüntetett ár bruttó ár, a 27% ÁFA-t tartalmazza!',
	'span17' => 'RÓLUNK MONDTÁK',
	'span18' => 'Akik már tanultak az IMPREVO-val',
	'span19' => 'Csatlakozz te is több ezer emberhez, akik már kipróbálták az IMREVO-t',
	'span20' => 'PRÓBÁLD KI INGYEN!',
	'span21' => 'Gyakran Ismételt kérdések',
	'span22' => 'Proin gravida fermentum venenatis?',
	'span23' => 'Phasellus mattis ultrices orci, vitae commodo libero dignissim id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris maximus consequat turpis vitae placerat. Praesent quis sagittis risus, mollis dignissim diam. Aliquam et elit tristique nunc varius egestas eu vitae.',
	'span24' => 'Proin gravida fermentum venenatis?',
	'span25' => 'Aenean cursus augue nec justo vestibulum, at sollicitudin nisl ullamcorper. Pellentesque sed massa nec arcu ornare rhoncus.',
	'span26' => 'Proin gravida fermentum venenatis?',
	'span27' => 'Morbi egestas mi rutrum metus placerat auctor. Nulla nunc sem, facilisis nec arcu porttitor, sodales varius eros. Integer mi sapien, tincidunt non tristique vitae, molestie at nibh. Aliquam ut dui nisl.',
	'span28' => 'Proin gravida fermentum venenatis?',
	'span29' => 'Phasellus mattis ultrices orci, vitae commodo libero dignissim id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
	'span30' => 'Proin gravida fermentum venenatis?',
	'span31' => 'Morbi egestas mi rutrum metus placerat auctor. Nulla nunc sem, facilisis nec arcu porttitor, sodales varius eros. Integer mi sapien, tincidunt non tristique vitae, molestie at nibh. Aliquam ut dui nisl.',
	'span32' => 'Proin gravida fermentum venenatis?',
	'span33' => 'Mauris maximus consequat turpis vitae placerat. Praesent quis sagittis risus, mollis dignissim diam. Aliquam et elit tristique nunc varius egestas eu vitae. Aenean cursus augue nec justo vestibulum, at sollicitudin nisl ullamcorper. Pellentesque sed massa nec arcu ornare rhoncus nec eget nulla.',
	'span34' => 'Ha van még kérdésed, nézd meg',
	'span35' => 'MI az IMPREVO',
	'span36' => 'vagy, hogy',
	'span37' => 'Kik is vagyunk',
	'span38' => 'lorem',
	'span39' => 'ipsum',	
	'span40' => 'lorem',
	'span41' => 'ipsum',	
	'span42' => 'lorem',
	'span43' => 'ipsum',	
	'span44' => 'lorem',
	'span45' => 'ipsum',
	'span46' => 'Játékos, interaktív tanulás!',	
];