<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dear' => 'Dear User,',
    'span1' => 'Welcome to Imprevo!',
	'span2' => 'We hope you’ll have a great time with us and you’ll learn a lot.',
	'span3' => 'Please visit our trial lessons by clicking the link below.',
	'span4' => 'https://imprevo.hu/home',
	'span5' => 'If you have any question, please do not hesitate to contact us.',
	'span6' => 'Imprevo Team!',	
];
