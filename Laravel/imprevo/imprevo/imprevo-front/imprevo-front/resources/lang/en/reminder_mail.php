<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dear' => 'Dear User!!',
    'span1' => 'We would like to inform you that the order you placed at',
	'span2' => 'Order ID:',
	'span3' => 'Selected payment method:',
	'span4' => 'In case you encountered a problem during the order, we are here to help you. Please contact us at:',
	'span5' => 'Phone:',
	'span6' => 'Email:',	
	'span7' => 'Your order can be paid via bank transfer as well. Our bank details are:',	
	'span8' => 'Company name:',	
	'span9' => 'Bank:',	
	'span10' => 'Account number:',	
	'span11' => 'Please note that pending orders are deleted from our system after 2 weeks!',	
	'span12' => 'If you have any question, please do not hesitate to contact us.',	
	'span13' => 'Imprevo Team!',	
	'span14' => 'has not been paid yet.',
];
