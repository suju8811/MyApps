<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
  'recaptcha' => 'You are considered as spam by recaptcha!',
  'wrong_input' => 'Your username and password are wrong.',
  'wrong_email' => 'Registration with this email address is not allowed.',
  'failed' => 'These credentials do not match our records.',
  'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
	'logintitle' => 'Imprevo login',
	'loginspan-1' => 'Please fill out the fields below.',
	'email' => 'Email address',
	'password' => 'Password',
	'remember' => 'Remember me',
	'login' => 'LOGIN',
	'forgot' => 'Forgot your password',
	'login-span-2' => 'Don\'t have an imprevo account yet?',
	'register' => 'Register now!',
	'error-span-1' => 'Please correct the following errors:',
	/*Register part*/
	'registertitle1' => 'Register for a FREE trial!!',
	'registertitlespan1' => 'Please fill out the fields below.',
	'registertitle2' => 'Join Imprevo!',
	'regsitertitlespan2' => 'Please fill out the fields below.',
	'resgistererrorspan1' => 'Please correct the following errors:',
	'resgistererrorspan2' => 'Please correct the following errors: Password don\'t match',
	'registeremail' => 'E-mail address',
	'registerpassword' => 'Password',
	'confirmpassword' => 'Confirm password',
	'registererror1' => 'Password don\'t match',
	'registerspan3' => 'I read and accept the',
	'registerspan4' => 'Terms of Service',
	'registerbtn' => 'Register',
	/*reset*/
	'resettitle' => 'Reset Your Password!',
	'resetspan' => 'Please fill out the fields below.',
	'resetemail' => 'E-mail address',
	'resetpassword' => 'Reset Password'	,
	'forgot_span1' => 'Give us your email address below and we’ll send you a new one!',
	'forgot_span2' => 'Add you email address below and we\'ll let you set a new one.',
	'forgot_span3' => 'The email address cannot be found in our database.',
	'forgot_span4' => 'Invalid email address',
	'forgot_span5' => 'BACK TO LOGIN!',
	'forgot_but1' => 'SUBMIT',
	'success_span1' => 'Successful password recovery!',
	'success_span2' => 'We have sent your new password! Use it to log in to our site.',
	'success_span3' => 'if you experience any problem, please',
	'success_span4' => 'let us know!',
	'reset_password' => 'Password Reset',
	'reset_hello' => 'Hello',
	'reset_span1' => 'You are receiving this email because we received a password reset request for your account.',
	'reset_span2' => 'If you did not request a password reset, no further action is required.',
	'reset_regards' => 'Regards',
	'reset_btn' => 'Reset Password',
	'reset_foot' => 'If you’re having trouble clicking the',
	'reset_foot1' => 'button, copy and paste the URL below into your web browser:',
	'register_email_span1' => 'Registration',
];
