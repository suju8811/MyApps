<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'span1' => 'Lessons',
	'span2' => 'CONTINUE',
	'span3' => 'There are no lessons here!',
	'span4' => 'Current lesson : ',
];