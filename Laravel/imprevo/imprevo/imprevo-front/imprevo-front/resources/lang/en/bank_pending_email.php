<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dear' => 'Dear',
    'span1' => 'Thank you for your purchase.',
	'span2' => 'You have selected Bank transfer for payment method and now your order is pending.',
	'span3' => 'You will have full access to the course after the money arrived to our bank account.',
	'span4' => 'Please make the payment to:',
	'span5' => 'Company name: Imprevo Kft.',
	'span6' => 'Bank account: 10402142-50526771-77801015',
	'span7' => 'Your order ID: ',
	'span8' => 'Success - Order!',
];
