<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'profile' => 'Profile',
	'profilesettings' => 'Profile settings',
	'basicinfo' => 'BASIC INFO',
	'yourname' => 'Your name',
	'youremailaddress' => 'Your email address',
	'passwordsettings' => 'PASSWORD SETTINGS',
	'yourcurrentpassword' => 'Your current password',
	'yournewpassword' => 'Your new password',
	'confirmnewpassword' => 'Confirm new password',
	'billingaddress' => 'BILLING ADDRESS',
	'country' => 'Country',
	'towncity' => 'Town/City',
	'postcode' => 'PostCode',
	'county' => 'County',
	'streetaddress' => 'Street address',
	'companydetail' => 'COMPANY DETAILS',
	'companyname' => 'Company name',
	'taxid' => 'Tax ID',
	'euvat' => 'EU Vat number',
	'save' => 'SAVE',
	'sidetoptitle' => 'Imprevo newsletter',
	'sidespan1' => 'Subscribe for free materials and news!',
	'sidespan2' => 'Useful stuff',
	'sidespan3' => 'Blog categories',
	'sidespan4' => 'FREE COURSE',
];
