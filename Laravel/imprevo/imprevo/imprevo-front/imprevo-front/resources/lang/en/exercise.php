<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'previous' => 'PREVIOUS',
	'next' => 'NEXT',
	'quizempty' => 'Quiz is Empty.',
	'showfull' => 'Show full text',
	'exercise-span1' => 'Congratulations,you\'ve finished the quiz!',	
	'exercise-span2' => 'Here are the correct answers',
	'exercise-span3' => 'We\'re sorry, but this lesson is only available for IMPREVO learners.',		
	'exercise-span4' => 'Join our students!',
	'exercise-but1' => 'Want to know more?',
	'exercise-but2' => 'Play again',					
	'exercise-reporterror' => 'REPORT ERRORS',
	'exercise-but3' => 'NEXT EXERCISE',	
	'exercise-span5' => 'if you noticed anything out of the ordinary, please send us a feedback.',
	'exercise-span6' => 'lesson, exercise',
	'exercise-span7' => 'Your message',
	'exercise-span8' => 'Send',
	'exercise-span9' => 'CANCEL',
	'exercise-span10' => 'Thank you for your feedback!',
	'exercise-span11' => 'Close',
	'yourscore' => 'Your Score: ',
	'exercise-span12' => 'Please type in your answer then hit enter!',
	'exercise-span13' => 'Please type your solution on the yellow line!',
	'exercise-span14' => 'Note',
	'exercise-span15' => 'Your answer is correct',	
	'exercise-span16' => 'Your answer is incorrect',
	'exercise-span17' => 'Question',
	'exercise-span18' => 'Your answer',
	'exercise-span19' => 'Correct answer',
];
