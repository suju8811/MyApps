<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'blog' => 'Blog',
	'by' => 'BY',
	'comment' => 'COMMENT',
	'span1' => 'Imprevo newsletter',
	'span2' => 'Subscribe for free learning materials!',
	'span3' => 'Useful stuff',
	'span4' => 'Categories',
	'span5' => 'FREE COURSE',
	'span6' => 'Categories',
	'span7' => 'No more blog posts!',
];