<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'top_alert_text' => 'Remaining time:',
	'day' => 'day',
	'hour' => 'hour',
	'min' => 'minute',
	'sec' => 'second',
	'bluespan' => 'Próbáld ki az IMPREVO-t még ma ingyen',
	'bluebtn' => 'PRÓBÁLD KI INGYEN!',
	'footer-span1' => 'Értesülj legfrisebb leckéinkről,  ingyenes anyagainkról',
	'footer-span2' => 'FELIRATKOZOM',
	'footer-span3' => 'About',
	'footer-span4' => 'Kik vagyunk?' ,
	'footer-span5' => 'Mi az IMPREVO?',
	'footer-span6' => 'Shop',
	'footer-span7' => 'Contact',
	'footer-span8' => 'Online Angol tanfolyam',
	'footer-span9' => 'Hallás utáni szövegértés leckék',
	'footer-span10' => 'Angol oktatás cégeknek',
	'footer-span11' => 'Meg lehet tanulni az IMPREVO-val angolul?',
	'footer-span12' => 'Ingyenes anyagok',
	'footer-span13' => 'Igeidők összefoglaló táblázat',
	'footer-span14' => '20 részes ingyenes e-mailes tanfolyam',
	'footer-span15' => 'Ingyenes Kezdő Online Angol',
	'footer-span16' => 'Zenés leckék',
	'footer-span17' => 'Nyelvtani anyagok',
	'footer-span18' => 'Blog',
	'footer-span19' => 'Segítség',
	'footer-span20' => 'FAQ',
	'footer-span21' => 'Syllabus',
	'footer-span22' => 'Milyen a tanulási stílusod?',
	'footer-span23' => '8+1 Tipp angoltanuláshoz',
	'footer-span24' => 'Ebookok',
	'footer-span25' => 'Angol igeidők összefoglaló és példatár',
	'footer-span26' => 'Angol levelek és e-mailek írása',
	'footer-span27' => 'Tanulj meg 6 hónap alatt angolul',
	'footer-span28' => 'KÖVESS MINKET',
	'footer-span29' => 'Copyright 2016-2018. Imprevo. Minden jog fenntartva.',
	'footer-span30' => 'Privacy Policy',
	'footer-span31' => 'Terms of Use',
	'footer-span32' => 'Impressum',
	'footer-span33' => 'Terms and Conditions',
	'footer-span34' => 'Data protection',
	'footer-span35' => 'Company',
	'footer-span36' => 'Cookies',
	'footer-span37' => 'Prices',
	'footer-span38' => 'Relationships',
	'header-span1' => 'Az Imprevo',
	'header-span2' => 'Shop',
	'header-span3' => 'Angol tanfolyam',
	'header-span4' => 'Hétköznapi angol',
	'header-span5' => 'Blog',
	'header-span6' => 'Freebies',
	'header-span7' => '20 részes email tanfolyam',
	'header-span8' => '6 hónap alatt angolul',
	'header-span9' => 'Angol igeidők',
	'header-span10' => 'Angol levelek írása',
	'header-button1' => 'Log in',
	'header-button2' => 'Dashboard',
	'header-dropdown1' => 'User profile',
	'header-dropdown2' => 'Log out',
];
