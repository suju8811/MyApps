<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'imprevo' => 'Imprevo',
	'IMP' => 'IMPREVO',
	'dashboard' => 'Dashboard',
	'bannertext-green-title' => 'IMPREVO - Interaktív online angol leckék az IMPREVO-val!',
	'bannertext-green-description' => 'Videós magyarázatokkal és szótanuló játékokkal.',
	'bannerbutton1' => 'TUDJ&nbsp;MEG&nbsp;TÖBBET!',
	'bannerbutton2' => 'KIPRÓBÁLOM',
	'courses' => 'Courses',
	'trythese' => 'Try these!',
	'trythesespan1' => 'Teljesen kezdő szinttőla felsőfokig',
	'trythesebutton' => 'ÉRDEKEL',
	'trythesespan2' => '50 anyanyelvi mini párbeszéd',
	'cookiespan1' => 'In order to give you the best experience, our website uses cookies.By continuing to use this site, you agree to our use of cookies.',
	'cookiemore' => 'More information',
	'cookieaccept' => 'Accept',
	'continue' => 'CONTINUE',
	'freelessons' => 'Free lessons in this course',
	'currentlevel' => 'YOUR CURRENT LEVEL',
	'choosefrom' => 'Choose from the course content!',
	'currentlesson' => 'Current lesson',
	'lessons' => 'lessons',
];
