<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	'h1' => 'Válassz az alábbi tanfolyamok közül!',
	'button' => 'INGYENES',
	'link1' => 'Folytatom',
	'lesson' => 'lecke',
	'button1' => 'BELÉPEK',
	'button2' => 'INGYENES',
	'span1' => 'Ingyenes Próba',
	'link2' => 'Kosárba',
	'button3' => 'KOSÁRBA',
	'button4' => 'BELÉPEK',
	'button5' => 'KIPRÓBÁLOM',
];
