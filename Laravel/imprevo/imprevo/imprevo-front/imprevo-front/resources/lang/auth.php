<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'A megadott adatok nem találhatóak az adatbázisunkban.',
    'throttle' => 'Túl sok belépési kísérlet. Kérlek próbálkozz :seconds másodperc múlva.',
	'logintitle' => 'Imprevo bejelentkezés',
	'loginspan-1' => 'Kérlek töltsd ki az alábbi mezőket.',
	'email' => 'Email cím',
	'password' => 'Jelszó',
	'remember' => 'Megjegyez',
	'login' => 'BEJELENTKEZÉS',
	'forgot' => 'Elfelejtett jelszó',
	'login-span-2' => 'Még nincs IMPREVO fiókod?',
	'register' => 'Regisztrálj most!',
	'error-span-1' => 'Kérlek javítsd a következő hibákat:',
	/*Register part*/
	'registertitle1' => 'Regisztrálj és próbáld ki ingyen!',
	'registertitlespan1' => 'Az IMPREVO első 27 leckéje teljesen ingyenes!',
	'registertitle2' => 'Csatlakozz az Imprevo-hoz!',
	'regsitertitlespan2' => 'Kérlek töltsd ki az alábbi mezőket.',
	'resgistererrorspan1' => 'Kérlek javítsd a következő hibákat:',
	'resgistererrorspan2' => 'Kérlek javítsd a következő hibákat: A jelszavak nem egyeznek',
	'registeremail' => 'E-mail cím',
	'registerpassword' => 'Jelszó',
	'confirmpassword' => 'Jelszó mégegyszer',
	'registererror1' => 'A jelszavak nem egyeznek',
	'registerspan3' => 'Elolvastam és elfogadom a ',
	'registerspan4' => 'Felhasználói feltételeket',
	'registerbtn' => 'Regisztráció',
	/*reset*/
	'resettitle' => 'Új jelszó igénylése!',
	'resetspan' => 'Kérlek töltsd ki az alábbi mezőket.',
	'resetemail' => 'E-mail cím',
	'resetpassword' => 'Új jelszó igénylése',
	'forgot_span1' => 'Kérlek írd be a regisztrált email címedet az alábbi mezőbe és küldünk egy új jelszót.',
	'forgot_span2' => 'Kérlek írd be a regisztrált email címedet az alábbi mezőbe és küldünk egy új jelszót.',
	'forgot_span3' => 'A megadott email nem található az adatbázisunkban!',
	'forgot_span4' => 'Érvénytelen email cím',
	'forgot_span5' => 'VISSZA!',
	'forgot_but1' => 'ELKÜLD',
	'success_span1' => 'Sikeres jelszóigénylés!',
	'success_span2' => 'Elküldtük emailben a jelszó igényléshez szükséges információt.',
	'success_span3' => 'Ha bármi problémába ütköznél,',
	'success_span4' => 'vedd fel velünk a kapcsolatot és segítünk!',
	'reset_password' => 'Jelszóbeállítás',
	'reset_hello' => 'Szia',
	'reset_span1' => 'Azért kapod ezt az emailt, mert az oldalunkon új jelszóigénylés érkezett a profilodhoz.',
	'reset_span2' => 'Ha nem te kérted az új jelszót, akkor tekintsd ezt a levelet tárgytalannak.',
	'reset_regards' => 'Üdv',
	'reset_btn' => 'Új jelszó igénylése',
	'reset_foot' => 'Ha nem működne a fenti gomb,',
	'reset_foot1' => 'másold ki és illeszd be az alábbi linket a böngésződ címsorába:',	
	'register_email_span1' => 'Sikeres regisztráció',
];

