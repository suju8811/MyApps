<?php

namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
        protected $fillable = ['title', 'content', 'seo_title', 'seo_description', 'seo_keywords', 'featured_image', 'is_image', 'categories', 'static_url', 'created_by'];

        public function createdBy()
        {
            return $this->belongsTo('App\User', 'created_by');
        }
        public function blogcats()
        {
            return $this->belongsToMany('App\Blogcat', 'blog_blogcat', 'blog_id', 'blogcat_id');
        }	
}
