<?php

namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;
use App\Word;

class Syllabus extends Model
{
		public $table = 'syllabus';

		protected $fillable = ['module_id', 'module_name', 'course_id', 'lesson_id', 'descripition'];

}
