<?php

namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;
use App\Word;

class Product extends Model
{
	  protected $fillable = ['title', 'description', 'course_id', 'regular_price', 'sale_price', 'sale_price_end_date', 'sale_price_orders_num', 'billing_cycle', 'product_image', 'featured_product', 'features'];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }
}
