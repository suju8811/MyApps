<?php

namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;

class Blogcat extends Model
{
        protected $fillable = ['title', 'created_by'];

        public function createdBy()
        {
            return $this->belongsTo('App\User', 'created_by');
        }
        public function blogs()
        {
            return $this->belongsToMany('App\Blog', 'blog_blogcat', 'blogcat_id', 'blog_id');
        }
}
