<?php

namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    protected $fillable = ['title', 'description', 'type', 'module_id', 'video_url', 'audio', 'content1', 'content2', 'lesson_id', 'quiz_id', 'created_by', 'updated_by'];

    public function module()
    {
        return $this->belongsTo('App\Module');
    }

    public function lesson()
    {
        return $this->belongsTo('App\Lesson');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
}
