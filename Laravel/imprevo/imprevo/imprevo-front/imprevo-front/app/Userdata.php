<?php
namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;
use App\Word;

class Userdata extends Model
{
	protected $fillable = ['id', 'done_exercises', 'current_exercise', 'purchased_courses', 'recent_blogs', 'cookie_accept_date'];

}
