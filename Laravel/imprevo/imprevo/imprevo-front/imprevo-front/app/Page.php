<?php

namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
        protected $fillable = ['title', 'full_width_content','content', 'seo_title', 'seo_description', 'seo_keywords', 'adwords_code', 'facebook_pixel_code', 'is_show', 'static_url', 'created_by'];

        public function createdBy()
        {
            return $this->belongsTo('App\User', 'created_by');
        }
}
