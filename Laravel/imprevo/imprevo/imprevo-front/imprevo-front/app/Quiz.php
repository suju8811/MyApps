<?php

namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;
use App\Word;

class Quiz extends Model
{
	protected $fillable = ['title', 'wordset_id', 'random_type', 'course_id', 'level_id', 'module_id', 'lesson_id', 'shortcode', 'created_by', 'updated_by'];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function level()
    {
        return $this->belongsTo('App\Level');
    }

    public function module()
    {
        return $this->belongsTo('App\Module');
    }

    public function lesson()
    {
        return $this->belongsTo('App\Lesson');
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
}
