<?php

namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;
use App\Word;

class Course extends Model
{
	protected $fillable = ['title', 'description', 'photo', 'is_public', 'is_free', 'is_hide', 'created_by', 'updated_by'];

    public function lessons()
    {
        return $this->hasMany('App\Lesson');
    }

    public function levels()
    {
        return $this->belongsToMany('App\Level');
    }

    public function modules()
    {
        return $this->belongsToMany('App\Module');
    }
	
	public function users()
    {
        return $this->belongsToMany('App\Course');
    }
}
