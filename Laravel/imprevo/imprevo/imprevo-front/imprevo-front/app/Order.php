<?php

namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;
use App\Word;

class Order extends Model
{
	protected $fillable = ['payment_method', 'order_date', 'order_state', 'customer', 'customer_email', 'billing_address', 'purchased_product', 'price_vat', 'price_net_amount', 'price_total'];
}
