<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userdata;
use App\Blog;
use App\Blogcat;
use App\Quiz;
use App\User;
use App\Code;
use Countries;
use Mail;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use Log;


class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

	
    public function PostMessage(Request $request)
    {
		$email = $request->input('email');
		$message = $request->input('message');
		$name = $request->input('name');
		$subject = $request->input('subject');
		
		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
		}	
		
		$data = array('content' => $message, 'site_email' => $settings['siteEmail'], 'from'=> $email, 'name'=>$name, 'subject'=>$subject);	
		
		Mail::send('emails.contact_mail', $data, function($message) use ($data) {
			$message->to($data['site_email']); 
			$message->from($data['from'], $data['name']);
			$message->subject($data['subject']);
		});	
		
		return back()->with('success', 'Your message is sent successfully');		
    }	
}
