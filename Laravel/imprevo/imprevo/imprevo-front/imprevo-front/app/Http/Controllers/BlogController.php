<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userdata;
use App\Blog;
use App\Blogcat;
use App\Quiz;
use App\Code;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use Log;


class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    function ConvertToUTF8($text){

        $encoding = mb_detect_encoding($text, mb_detect_order(), false);

        if($encoding == "UTF-8")
        {
            $text = mb_convert_encoding($text, 'UTF-8', 'UTF-8');
        }


        $out = iconv(mb_detect_encoding($text, mb_detect_order(), false), "UTF-8//IGNORE", $text);


        return $out;
    }
	public function index(Request $request, $name=null)
	{
		$blogcat = null;
		$blogs  = null;
		$page = 1;
    $page_done = 0;
		$blogcat_all = Blogcat::all();
		if ($name){
			$blogcat_f = Blogcat::where('title', $name)->first();
			$blogcat = Blogcat::findOrNew($blogcat_f->id);
			$blogs = $blogcat->blogs;
			$page = $request->input('page');
		}
		else
		{
      $page_num = $request->input('page');
      $total_count = Blog::all()->count();

      if (!$page_num) $page_num = 1;
      if ($page_num*6 >= $total_count) $page_done = 1;

			$blogs=Blog::orderBy('created_at', 'DESC')->limit($page_num * 6)->get();
		}
    $html='';
		if ($page == 1){
			foreach ($blogs as $blog) {
        $strip_str = strip_tags($blog->content);
				$quiz_list = [];
				$array = explode('[quiz-id=', $strip_str);
				unset($array[0]);
				foreach ($array as $item) {
					$item_array = explode(']', $item);
					unset($item_array[0]);
				}
				foreach ($array as $item) {
					$item_array = explode(']', $item);
					if (is_numeric($item_array[0]))
					{
						$quiz = Quiz::findorNew($item_array[0]);
						array_push($quiz_list, $quiz);
					}
				}

				foreach ($quiz_list as $quiz)
				{
					$quizshort = join('', ['[quiz-id=', strval($quiz->id), ']']);
					$strip_str = str_replace($quizshort, '', $strip_str);
				}

				if (strlen($strip_str) > 100)
					$excerpt = substr($strip_str,0,100);
				else
					$excerpt = $strip_str;

        $excerpt = $this->ConvertToUTF8($excerpt);

				$categorylist = array();
				foreach($blogcat_all as $blogcat2)
				{
					$blogs1 = $blogcat2->blogs;
					foreach($blogs1 as $item)
					{
						if ($item->id == $blog->id)
						{
							array_push($categorylist, '#'.$blogcat2->title);
						}
					}
				}

				$category_str = implode(", ", $categorylist);
				/*$blog = Blog::findOrNew($blog->id);
				$blogcat3 = $blog->blogcats();
				foreach ($blogcat3 as $blogcat4)
				{
					Log::info($blogcat4->title);
				}*/


				$html.='<div class="mdl-cell mdl-cell mdl-cell--4-col">
						<div class="mdl-cell mdl-cell--12-col blog-picture-div" style="width:100%; height:200px; background: url('.$blog->featured_image.') no-repeat center/contain"></div>
						<div class="mdl-cell mdl-cell--12-col category-div" style="width:100%;text-align:center">
							<a class="category-name-href" href="#" style="">'.$category_str.'</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col title-div" style="width:100%;text-align:center">
							<a class="title-href" href="/blog/'.$blog->static_url.'" style="">'.$blog->title.'</a>
						</div>
						<div class="mdl-cell mdl-cell--12-col excerpt-div" style="width:100%;text-align:center">
							<span class="excerpt-href" style="font-size:14px; color:#3b4a51;">'.$excerpt.' […]</span>
						</div>
						<div class="mdl-cell mdl-cell--12-col more-button-div" style="padding-top:10px;width:100%;text-align:center">
							<a href="/blog/'.$blog->static_url.'" class="more-button btn btn-default" style="">TOVÁBB OLVASOM</a>
						</div>
					</div>';
			}
		}

        if ($request->ajax()) {
            return [$html, $page_done];
        }

		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];

		}

        return view('blogs', [
            'blogs'=> compact($blogs),
		        'blogcat' => $blogcat,
		         'settings'=>$settings
        ]);
	}
	public function ViewBlogCategory(Request $request, $name)
	{
		Log::info($name);
		$blogcat = BlogCat::where('title','=',$name)->first();
		$blogs = $blogcat->blogs();
        $html='';
        foreach ($blogs as $blog) {
			$strip_str = strip_tags($blog->content);
			if (strlen($strip_str) > 100)
				$excerpt = substr(strip_tags($blog->content),0,100);
			else
				$excerpt = $strip_str;

            $html.='<div class="mdl-cell mdl-cell mdl-cell--4-col mdl-cell--4-col-phone" style="text-align:center">
					<div class="mdl-cell mdl-cell--12-col blog-picture-div" style="width:100%; height:200px; background: url('.$blog->featured_image.') no-repeat center/contain"></div>
					<div class="mdl-cell mdl-cell--12-col category-div" style="width:100%;text-align:center">
						<a class="category-name-href" href="#" style="">#NYELVTANI KISOKOS</a>
					</div>
					<div class="mdl-cell mdl-cell--12-col title-div" style="width:100%;text-align:center">
						<a class="title-href" href="/blog/'.$blog->static_url.'" style="">Donec quis em consequat ubus ferment dolor vel tempus</a>
					</div>
					<div class="mdl-cell mdl-cell--12-col excerpt-div" style="width:100%;text-align:center">
						<span class="excerpt-href" style="font-size:14px; color:#3b4a51; font-weight:bold">'.$excerpt.' […]</span>
					</div>
					<div class="mdl-cell mdl-cell--12-col more-button-div" style="width:100%;text-align:center">
						<a href="/blog/'.$blog->static_url.'" class="more-button mb-xs mt-xs mr-xs btn btn-default">TOVÁBB OLVASOM</a>
					</div>
				</div>';
        }

        if ($request->ajax()) {
            return $html;
        }
		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];

		}
        return view('blogs', [
          'blogs'=> compact($blogs),
		  'settings'=>$settings
        ]);
	}
    public function parseQuiz($blog, $index=1)
    {
      $content = $blog->content;
      $content_array = explode('<!--nextpage-->',$content);
      $page_count = count($content_array);
      $page_index = $index;
      $content = $content_array[$index-1];
      $content_fix = $content;
      $quiz_list = [];



      $array = explode('[quiz-id=', $content);

      unset($array[0]);
      foreach ($array as $item) {
        $item_array = explode(']', $item);
        unset($item_array[0]);
      }
      foreach ($array as $item) {
        $item_array = explode(']', $item);
        if (is_numeric($item_array[0]))
        {
          $quiz = Quiz::findorNew($item_array[0]);
          array_push($quiz_list, $quiz);
        }
      }

      foreach ($quiz_list as $quiz)
      {
        $quiz_replace = join('', ['<div class="mdl-grid mdl-cell mdl-cell--12-col" id="quiz-part-', strval($quiz->id),'" name="quiz-part" style="min-height:500px">Quiz Part!</div>']);
        $quizshort = join('', ['[quiz-id=', strval($quiz->id), ']']);
        $content_fix = str_replace($quizshort, $quiz_replace, $content_fix);
      }

      return [$quiz_list, $content_fix, $page_count, $page_index];
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ViewBlog(Request $request, $page_url)
    {
		//Log::info($page_index);
		Log::info('---------');
        $blog_std = DB::table('blogs')->where('static_url', $page_url)->first();
        $quiz_list = null;
        $page_count = null;
        $page_index = null;
        if ($blog_std){
            $blog = Blog::findOrNew($blog_std->id);
            $parseQuiz = $this->parseQuiz($blog);
            $quiz_list = $parseQuiz[0];
            $page_count = $parseQuiz[2];
            $page_index = $parseQuiz[3];
            $blog['content'] = $parseQuiz[1];
        }
        else{
            return redirect()->to('home');
        }

        $user_data = Userdata::findOrNew(0);
        $recent_blogs_array = null;
		      Log::info($user_data['id']);
        if ($user_data['id'])
        {
          $recent_blogs_array = explode(',' , $user_data['recent_blogs']);
          $bExist = false;
          foreach( $recent_blogs_array as $rb_static_url) {
            if ($rb_static_url == $blog->static_url )
            {
                $bExist = true;
            }
          }
          $i = 0;

          if ($bExist == false)
          {
             while (count($recent_blogs_array) > 5)
             {
               unset($recent_blogs_array[$i]);
               $i++;
             }
             $recent_join = join(",", $recent_blogs_array);
             $user_data['recent_blogs'] = join(",", [$recent_join,$blog->static_url]);
          }
          $user_data->save();
        }
        else
        {
			Log::info($blog->static_url);
			$user_data = Userdata::create([
                            'id' => 0,
                            'recent_blogs' => $blog->static_url]);
        }
        $fpC = json_encode($blog->facebook_pixel_code);

		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];

		}

        return view('blog', [
          'blog' => $blog,
          'facebook_pixel_code' => $fpC,
          'quiz_list' => $quiz_list,
          'recent_blogs' => $recent_blogs_array,
          'page_count' => $page_count,
          'page_index' => $page_index,
		        'settings' => $settings
        ]);
    }

    public function ViewBlogPageByIndex(Request $request, $page_url)
    {
		$page_index = $request->Input('page');

		if (!$page_index)
			$page_index = 1;

        $blog_std = DB::table('blogs')->where('static_url', $page_url)->first();
        $quiz = null;
        if ($blog_std){
            $blog = Blog::findOrNew($blog_std->id);
            $parseQuiz = $this->parseQuiz($blog, $page_index);
            $quiz_list = $parseQuiz[0];
            $page_count = $parseQuiz[2];
            $page_index = $parseQuiz[3];
            $blog['content'] = $parseQuiz[1];
        }
        else{
            return redirect()->to('home');
        }
        $fpC = json_encode($blog->facebook_pixel_code);
        $user = auth()->user();
        $user_id = null;
        if ($user) $user_id = auth()->user()->id;
        if ($user_id)
        {
            $user_data = Userdata::findOrNew($user_id);

            if (!$user_data->id)
            {
              $user_data = null;
            }
        }
        else
        {
          $user_data = null;
        }

        $recent_blogs = null;
        if ($user_data)
        {
		  if ($user_data['recent_blogs'])
			$recent_blogs = explode(',',$user_data['recent_blogs']);
        }

		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];

		}

        return view('blog', [
          'blog' => $blog,
          'facebook_pixel_code' => $fpC,
          'quiz_list' => $quiz_list,
          'recent_blogs' => $recent_blogs,
          'page_count' => $page_count,
          'page_index' => $page_index,
		  'settings' => $settings
        ]);
    }


}
