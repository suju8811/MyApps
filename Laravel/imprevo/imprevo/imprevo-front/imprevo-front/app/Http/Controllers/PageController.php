<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userdata;
use App\Page;
use App\Code;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;


class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ViewPage(Request $request, $page_url=null)
    {
		if ($page_url == null) $page_url = 'home-page';
        $page_std = DB::table('pages')->where('static_url', $page_url)->first();
        if ($page_std){
            $page = Page::findOrNew($page_std->id);
        }
        else{
            return redirect()->to('home');
        }
        $fpC = json_encode($page->facebook_pixel_code);
		
		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
		
		}
		
        return view('page', [
          'page' => $page,
          'facebook_pixel_code' => $fpC,
		  'settings' => $settings
        ]);
    }
}
