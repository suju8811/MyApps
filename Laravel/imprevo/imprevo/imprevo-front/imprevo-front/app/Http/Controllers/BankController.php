<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use DB;
use App\Code;
use Log;
use App\User;
use Mail;
use Illuminate\Support\Facades\Auth;
/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use App\Product;
use App\Userinfo;
use App\Order;
use App\Shoppingcartsetting;

class BankController extends Controller
{
  private $_api_context;
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    //parent::__construct();
    /** setup PayPal api context **/
  }

  /**
  * Store a details of payment with paypal.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function generate_key_string() {

    $tokens = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $segment_chars = 5;
    $num_segments = 4;
    $key_string = '';

    for ($i = 0; $i < $num_segments; $i++) {

      $segment = '';

      for ($j = 0; $j < $segment_chars; $j++) {
        $segment .= $tokens[rand(0, 35)];
      }

      $key_string .= $segment;

      if ($i < ($num_segments - 1)) {
        $key_string .= '-';
      }

    }

    return $key_string;

  }

  public function send($user_email, $itemList, $orderid)
  {
    //$purchased_courses = $course
    Log::info($orderid);
    $user =  User::where('email', '=', $user_email)->first();


    $codeList = Code::all();
    $settings = [];
    for($i = 0; $i < count($codeList); $i++) {
      $settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];

    }
    if ($user->company)
    $name = $user->company;
    else
    $name = $user->name;
    $data = array('name' => $name, 'siteEmail'=>$settings['siteEmail'], 'email' => $user->email, 'orderid' => $orderid);

    //Log::info($data['siteEmail']);

    Mail::send('emails.bank_pending_email', $data, function ($message) use ($data)
    {
      $message->to($data['email']);
      $message->subject(trans('bank_pending_email.span8'));
      $message->from($data['siteEmail'], 'Imprevo');
    });


    return response()->json(['message' => 'Request completed']);
  }

  public function OrderedProduct($products, $status, $user_email, $is_valid_vat)
  {
    //$order = DB::table('orders')->where('customer_email', '=', $user_email)->where('purchased_product', $product->id)->first();
    //if ($order)
    //	$order = Order::findOrNew($order->id);

    $now = new \DateTime();

    $user =  User::where('email', '=', $user_email)->first();
    $purchased_products = [];
    $total_price = 0;

    foreach ($products as $product)
    {
      $dtime = strtotime($product->sale_price_end_date);
      $timecmp = false;
      $numcmp = false;
      if (strtotime("now") < $dtime)
      {
        $timecmp = true;
      }

      if ($product->sale_price_orders_current < $product->sale_price_orders_num)
      {
        $numcmp = true;
      }


      if (!is_null($product->sale_price) && $timecmp && $numcmp)
      {
        $total_price = $product->sale_price + $total_price;
        $product->sale_price_orders_current = $product->sale_price_orders_current + 1;
        $product->save();
      }
      else
      {
        $total_price = $product->regular_price + $total_price;
      }



      array_push ($purchased_products, $product->id);
    }

    $purchased_products = join(',', $purchased_products);
    $pricetotal = 0;
    $pricevat = 0;
    $pricenet = 0;

    if ($is_valid_vat == 1)
    {
      $pricenet = $total_price * 100/127;
      $pricevat = 0;
      $pricetotal = $pricenet;
    }
    else if ($is_valid_vat == 0)
    {
      $pricenet = $total_price * 100/127;
      $pricevat = $total_price - $pricenet;
      $pricetotal = $total_price;
    }
    else
    {
      $price_vat = 0;
      $pricenet = $total_price;
      $pricetotal = $total_price;
    }

    if ($user->company)
    $customer = $user->company;
    else
    $customer = $user->name;
    //if (!$order){
    $order = Order::create([
      'user' => $user->id,
      'purchased_product' => $purchased_products,
      'customer' => $customer,
      'customer_email' => $user->email,
      'billing_address' => $user->street_address.' '.$user->city.' '.$user->country,
      'order_date' => $now,
      'price_vat' => $pricevat,
      'price_net_amount' => $pricenet,
      'price_total' => $pricetotal,
      'payment_method'=> 'Bank'
    ]);
    //}-

    if ($status == 'success')
    {
      $order->order_state = 'Completed';
    }
    else
    {
      $order->order_state = 'Pending';
    }

    $order->save();

    return $order->id;
  }

  public function postPaymentWithBank(Request $request)
  {
    $selectProducts = $request->input('selectProducts');
    $user_email = $request->input('account');

    $user =  User::where('email', '=', $user_email)->first();


    $productids = explode(',', $selectProducts);
    $products = [];
    foreach($productids as $productid)
    {
      $product = Product::findOrNew($productid);
      array_push($products, $product);
    }

    $is_valid_vat = $request->input('is_valid_vat');
    $order_ids = [];
    $total_price = 0;
    foreach ($products as $product)
    {
      if (!is_null($product->sale_price))
      {
        $total_price = $product->sale_price + $total_price;
      }
      else
      {
        $total_price = $product->regular_price + $total_price;
      }

      //$order_id = $this->OrderedProduct($product, 'pending', $user_email, $is_valid_vat);
      //array_push($order_ids, $order_id);
    }
    $orderid = $this->OrderedProduct($products, 'pending', $user_email, $is_valid_vat);
    Log::info($orderid);
    if ($is_valid_vat == 1)
    {
      $total_price = $total_price * 100 / 127;
    }

    $total_price = number_format(round($total_price), 0);
    //$orderid = join('-', $order_ids);
    Log::info($orderid);
    $this->send($user_email, $products, $orderid);

    $url = 	'/order/status/pending?products='.$selectProducts;

    return redirect()->to($url)
    ->with(['results-Payment' => 'Payment pending' ,'orderid' => $orderid, 'total_price' => $total_price]);
  }


  public function getPaymentStatus()
  {
    /** Get the payment ID before session clear **/
    $user_email = Session::get('user_email');
    Log::info('paypal');
    Log::info($user_email);
    $payment_id = Session::get('paypal_payment_id');
    /** clear the session payment ID **/
    Session::forget('paypal_payment_id');
    Session::forget('user_email');
    if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
      Session::put('error','Payment failed');
      return Redirect('/order');
    }
    $payment = Payment::get($payment_id, $this->_api_context);
    /** PaymentExecution object includes information necessary **/
    /** to execute a PayPal account payment. **/
    /** The payer_id is added to the request query parameters **/
    /** when the user is redirected from paypal back to your site **/
    $execution = new PaymentExecution();
    $execution->setPayerId(Input::get('PayerID'));
    /**Execute the payment **/
    $result = $payment->execute($execution, $this->_api_context);
    /** dd($result);exit; /** DEBUG RESULT, remove it later **/
    if ($result->getState() == 'approved') {

      /** it's all right **/
      /** Here Write your database logic like that insert record or value in database if you want **/
      $transaction = $payment->getTransactions();
      $itemList = $transaction[0]->getItemList();
      $order_ids = [];
      foreach ($itemList->items as $item)
      {
        $order_id = $this->OrderedProduct($item, 'success', $user_email);
        array_push($order_ids, $order_id);
      }
      $orderid = join('-', $order_ids);
      return redirect()->to('order')
      ->with(['results-Payment' => 'Payment success', 'orderid' => $orderid]);
    }
    else if ($result->getState() == 'failed')
    {
      return redirect()->to('order')
      ->with('results-Payment','Payment failed');
    }
    else
    {
      $transaction = $payment->getTransactions();
      $itemList = $transaction[0]->getItemList();
      foreach ($itemList->items as $item)
      {
        $order_id = $this->OrderedProduct($item, 'pending', $user_id);
        array_push($order_ids, $order_id);
      }
      $orderid = join('-', $order_ids);

      return redirect()->to('order')
      ->with(['results-Payment' => 'Payment pending' ,'orderid' => $orderid]);
    }
  }
}
