<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Code;
use App\Spam;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use DB;
use Mail;


function logConsole($name, $data = NULL, $jsEval = FALSE)
{
    if (! $name) return false;

    $isevaled = false;
    $type = ($data || gettype($data)) ? 'Type: ' . gettype($data) : '';

    if ($jsEval && (is_array($data) || is_object($data)))
    {
        $data = 'eval(' . preg_replace('#[\s\r\n\t\0\x0B]+#', '', json_encode($data)) . ')';
        $isevaled = true;
    }
    else
    {
        $data = json_encode($data);
    }

    # sanitalize
    $data = $data ? $data : '';
    $search_array = array("#'#", '#""#', "#''#", "#\n#", "#\r\n#");
    $replace_array = array('"', '', '', '\\n', '\\n');
    $data = preg_replace($search_array,  $replace_array, $data);
    $data = ltrim(rtrim($data, '"'), '"');
    $data = $isevaled ? $data : ($data[0] === "'") ? $data : "'" . $data . "'";

    $js = <<<JSCODE
\n<script>
 // fallback - to deal with IE (or browsers that don't have console)
 if (! window.console) console = {};
 console.log = console.log || function(name, data){};
 // end of fallback

 console.log('$name');
 console.log('------------------------------------------');
 console.log('$type');
 console.log($data);
 console.log('\\n');
</script>
JSCODE;

    echo $js;
} # end logConsole

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['email'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'permission'=> 1,
        ]);
    }

	public function showRegistrationForm(Request $request)
	{
		$is_freeview = $request->input('free');
    $codeList = Code::all();
    $settings = [];
    for($i = 0; $i < count($codeList); $i++) {
      $settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
    }
		return View('auth/register',['is_free' => $is_freeview, 'settings' => $settings]);
	}

  public function registerSendy($email, $list) {
      $service_url = 'http://sendy.imprevo.info/subscribe';
      $curl = curl_init($service_url);
	  $name = explode('@', $email)[0];
	  Log::info($email);
	  Log::info($list);
	  Log::info($name);
      $curl_post_data = array(
        'email' => $email,
        'list' => $list,
      );
	  $post_items = [];
	  foreach ( $curl_post_data as $key => $value)
	  {
	  	 $post_items[] = $key . '=' . $value;
	  }

	  $post_string = implode ('&', $post_items);
	  Log::info($post_string);
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $post_string);
	  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
	  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $curl_response = curl_exec($curl);

      if ($curl_response === false) {
          $info = curl_getinfo($curl);
		   Log::info('response false');
		  Log::info($info);
          curl_close($curl);
      }

      curl_close($curl);
      $decoded = json_decode($curl_response);
      if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
		  Log::info('error');
		   Log::info($decoded->response->errormessage);
          die('error occured: ' . $decoded->response->errormessage);
      }
      echo 'response ok!';
  }

    public function register(Request $request)
    {
        $input = $request->all();
        $validator = $this->validator($input);

        $spam = Spam::all()->first();
        $email = $request->input('email');

        $remoteip = $_SERVER["REMOTE_ADDR"];
        $url = "https://www.google.com/recaptcha/api/siteverify";
        $response = $request->input("g-recaptcha-response");
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array(
                  'secret' => '6LezFUcUAAAAAP3haCnbvxrtJE1FP8caQcvxW1Ec',
                  'response' => $response,
                  'remoteip' => $remoteip
        ));
        $curlData = curl_exec($curl);
        curl_close($curl);

        $recaptcha = json_decode($curlData, true);
        if (!$recaptcha["success"])
          return back()->with('spam_email', trans('auth.recaptcha'));


        if ($spam) {
          $spam_email = $spam->spam_emails;
          $spam_domains = $spam->spam_domains;

          if ($spam_email) {
            $spam_emails = explode("\n", $spam_email);


            foreach ($spam_emails as $item){
              $item = trim($item);

              if ($item == trim($email)){
                Log::info('*****************');
                return back()->with('spam_email', trans('auth.wrong_email'));
              }
            }
          }

          if ($spam_domains){
            $spam_domains = explode("\n", $spam_domains);
            foreach ($spam_domains as $item){
              $item = trim($item);
              if (strpos($email, $item) !== false) {
                Log::info('*****************');
                return back()->with('spam_email', trans('auth.wrong_email'));
              }
            }
          }
        }

        if ($validator->passes()) {
            $user = $this->create($input)->toArray();
            $user['link'] = str_random(30);

            /*DB::table('user_activations')->insert(['id_user'=>$user['id'],'token'=>$user['link']]);

            Mail::send('emails.activation', $user, function($message) use ($user) {
                $message->to($user['email']);
                $message->subject('Site - Activation Code');
            });*/

            if (auth()->attempt(array('email' => $request->input('email'), 'password' => $request->input('password'))))
            {
                /*if(auth()->user()->is_activated == '0'){
                    $this->logout($request);
                    return back()->with('warning',"First please active your account.");
                }*/
        				$codeList = Code::all();
        				$settings = [];
        				for($i = 0; $i < count($codeList); $i++) {
        					$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];

        				}

        				$user['from'] = $settings['siteEmail'];
						if ($settings['enable_sendy']){
							$this->registerSendy ($user['email'], $settings['sendy_list_id']);
						}
        				Mail::send('emails.register_mail', $user, function($message) use ($user) {
        					$message->to($user['email']);
        					$message->subject(trans('auth.register_email_span1'));
        					 $message->from($user['from'], 'Imprevo');
        				});

                return redirect()->to('home');
            }
        }

        return back()->with('errors',$validator->errors());
    }

    public function userActivation($token)
    {
        return redirect()->to('login')
            ->with('success',"user are already actived.");
        /*$check = DB::table('user_activations')->where('token',$token)->first();

        if(!is_null($check)){
            $user = User::find($check->id_user);

            if($user->is_activated == 1){
                return redirect()->to('login')
                    ->with('success',"user are already actived.");
            }

            $user['is_activated'] = 1;
            $user->save();
            DB::table('user_activations')->where('token',$token)->delete();

            return redirect()->to('login')
                ->with('success',"user active successfully.");
        }

        return redirect()->to('login')
            ->with('warning',"your token is invalid.");*/
    }
}
