<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userdata;
use App\Blog;
use App\Blogcat;
use App\Quiz;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use Log;


class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

	public function index(Request $request, $name=null)
	{			
        return view('test');		
	}
}
