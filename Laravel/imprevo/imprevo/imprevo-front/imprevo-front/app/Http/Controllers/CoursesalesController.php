<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userdata;
use App\Course;
use App\Product;
use App\Code;
use App\Shoppingcartsetting;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use Log;

class CoursesalesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ViewPage(Request $request, $id)
    {
		$course = Course::findOrNew($id);
		$product = Product::where('course_id', '=', $id)->first();

		$codeList = Shoppingcartsetting::all();
		$Shoppingcartsettings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$Shoppingcartsettings[$codeList[$i]["name"]] = $codeList[$i]["value"];
		}

		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
		}
    $product->sales = false;
    $dtime = strtotime($product->sale_price_end_date);
    $timecmp = false;
    $numcmp = false;
    if (strtotime("now") < $dtime){
      $timecmp = true;
    }

    if ($product->sale_price_orders_current < $product->sale_price_orders_num) {
      $numcmp = true;
    }

    if (!is_null($product->sale_price) && $timecmp && $numcmp) {
      $product->sales = true;
    }
		if ($id == 10) {
			return view('coursesalespage10', [
			'course' => $course,
			'product' => $product,
			'shoppingcartsetting' => $Shoppingcartsettings,
			'settings' => $settings
			]);
		}
		else if ($id == 11) {
			return view('coursesalespage11', [
			'course' => $course,
			'product' => $product,
			'shoppingcartsetting' => $Shoppingcartsettings,
			'settings' => $settings
			]);
		}
		else if ($id == 12) {
			return view('coursesalespage12', [
			'course' => $course,
			'product' => $product,
			'shoppingcartsetting' => $Shoppingcartsettings,
			'settings' => $settings
			]);
		}
    else if ($id == 14) {
      return view('coursesalespage14', [
      'course' => $course,
      'product' => $product,
      'shoppingcartsetting' => $Shoppingcartsettings,
      'settings' => $settings
      ]);
    }
  }
}
