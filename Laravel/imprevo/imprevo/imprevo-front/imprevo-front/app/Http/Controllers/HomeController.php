<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lesson;
use App\Course;
use App\Product;
use App\Level;
use App\Userdata;
use App\User;
use App\Exercise;
use App\Code;
use App\Syllabus;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use Log;

class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function AcceptCookie()
  {
    $user_id = auth()->user()->id;
    $now = new \DateTime();
    if ($user_id)
    {
      $user_data = Userdata::findorNew($user_id);
    }
    else
    {
      $user_data = null;
    }

    if ($user_data)
    {
      $current_date = date('Y-m-d H:i:s');
      $user_data->id = $user_id;
      $user_data->cookie_accept_date = $now;
      $user_data->save();
    }
  }
  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $courses = Course::all();
    $products = Product::all();
    $user_id = auth()->user()->id;
    $lessons = Lesson::paginate(15);
    $cookie_date = null;
    $user = User::where('id', $user_id)->first();
    if ($user_id)
    {
      $user_data = Userdata::where('id', $user_id)->first();
    }
    else
    {
      $user_data = null;
    }

    foreach($courses as $course){
      $course->is_purchased = false;
      $course->lesson_count = count($course->lessons);
      $course->done_lesson_count = 0;
      $course->last_exercise = null;
      $course->product = null;
      $course->product_image = null;
      foreach ($products as $product){
        if ($product->course->id == $course->id){
          $course->product = $product->id;
          $course->product_image = $product->product_image;
        }
      }
    }
    $cookie_diff = 1;
    if ($user_data)
    {
      $current_exercise = Exercise::findOrNew($user_data->current_exercise);
      $cookie_date = $user_data->cookie_accept_date;
      $current_date = date('Y-m-d H:i:s');
      $cookie_diff = strtotime($current_date) - strtotime($cookie_date);
      $cookie_diff = $cookie_diff / (60 * 60 * 24);

      if ($cookie_diff > 90){
        $cookie_diff = 1;
      } else {
        $cookie_diff = 0;
      }

      Log::info('---------------');
      Log::info($cookie_diff);
      if ($user_data->purchased_courses){
        $user_data_array = explode(',', $user_data->purchased_courses);
        foreach($courses as $course){
          $is_purchased = false;
          foreach ($user_data_array as $item) {
            if ($item == $course->id){
              $is_purchased = true;
            }
          }
          $course->is_purchased = $is_purchased;

          if ($user_data->done_exercises){
            $done_exercises = explode(',',$user_data->done_exercises);
            foreach ($course->lessons as $lesson){
              $bcompleted = true;
              foreach($lesson->exercises as $exe){
                if (!in_array($exe->id, $done_exercises)){
                  $bcompleted = false;
                }
              }
              if ($bcompleted == true){
                $course->done_lesson_count++;
              }
            }
          }
        }
      }
      if ($user_data->done_exercises){
        $done_exercises = explode(',',$user_data->done_exercises);
        foreach($courses as $course){
          foreach($done_exercises as $done_exe){
            foreach ($course->lessons as $lesson){
              foreach($lesson->exercises as $exe){
                if ($done_exe == $exe->id){
                  $course->last_exercise = $done_exe;
                }
              }
            }
          }
        }
      }
    }else {
      $current_exercise = null;
    }

    $lesson = null;
    if ($current_exercise){
      $lesson = Lesson::findorNew($current_exercise->lesson_id);
    }

    $level = null;
    if ($lesson){
      $level = Level::findorNew($lesson->level_id);
    }

    $codeList = Code::all();
    $settings = [];
    for($i = 0; $i < count($codeList); $i++) {
      $settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
    }


    return view('index', [
      'courses' => $courses,
      'lessons' => $lessons,
      'user_data' => $user_data,
      'user' => $user,
      'level' => $level,
      'current_exercise' => $current_exercise,
      'is_cookie' => $cookie_diff,
      'cookie_date' => $cookie_date,
      'cookieUrl' => isset($settings['cookieUrl'])? $settings["cookieUrl"]:'',
      'settings' => $settings
    ]);
  }
}
