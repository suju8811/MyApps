<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userdata;
use App\Blog;
use App\Blogcat;
use App\Quiz;
use App\User;
use App\Code;
use Countries;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use Log;


class ProfileController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function ViewProfile(Request $request, $name=null)
  {
    $blogcat = null;
    $blogs  = null;
    $blogcat_all = Blogcat::all();
    $blog_recents = Blog::orderBy('created_at', 'desc')->paginate(5);

    $countrylist = Countries::all()->pluck('name.common');
    $countrylist = (array)$countrylist;

    foreach ($countrylist as $countries)
    {
      $countrylist = $countries;
      break;
    }
    sort($countrylist);

    $user = User::findOrNew(Auth::user()->id);
    $states = [];
    if ($user->country)
    $states = Countries::where('name.common', $user->country)->first()->states->pluck('name');

    $codeList = Code::all();
    $settings = [];
    for($i = 0; $i < count($codeList); $i++) {
      $settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
    }

    return view('profile', [
      'blogs'=> compact($blogs),
      'blogcat' => $blogcat,
      'recent_blogs' => $blog_recents,
      'countrylist' => $countrylist,
      'user' => $user,
      'states'=>$states,
      'settings' => $settings
    ]);
  }

  public function getstates(Request $request)
  {

    $countryname = $request->input('countryname');
    $states = Countries::where('name.common', $countryname)->first()->states->pluck('name');
    $countryregions = Countries::all()->pluck('region', 'name.common');
    $region = $countryregions[$countryname];

    return [$states, $region];


  }

  public function PostProfile(Request $request)
  {
    $user = User::findOrNew(Auth::user()->id);
    $password = $request->input('password');
    $password_confirm = $request->input('password_confirm');
    $current_password = $request->input('current_password');

    if (!Hash::check($current_password, $user->password))
    {
      return redirect()->route('profile', [$user])->with('error','Your current password is wrong. Please try it again.');
    }

    if ($password != $password_confirm)
    {

      return redirect()->route('profile', [$user])->with('error','Please try it again for password and confirmation!');
    }
    else
    {
      if ($password != '')
        $user['password'] = bcrypt($password);

      if ($user)
      {
        $user['name'] = $request->input('name');
        $user['city'] = $request->input('city');
        $user['country'] = $request->input('country');
        $user['county'] = $request->input('county');
        $user['zipcode'] = $request->input('postcode');
        $user['street_address'] = $request->input('street_address');
        $user['company'] = $request->input('company');
        $user['tax_num'] = $request->input('tax_num');
        $user['eu_vat_num'] = $request->input('eu_vat_num');

        $user->save();

        return redirect('/profile');
      }
    }
  }
}
