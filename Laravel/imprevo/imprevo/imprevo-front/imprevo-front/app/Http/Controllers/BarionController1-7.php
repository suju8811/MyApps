<?php


namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use Log;
use DB;
use App\Barionlibrary\BarionClient;
use App\Barionlibrary\BarionEnvironment;
use App\Barionlibrary\ItemModel;
use App\Barionlibrary\PaymentTransactionModel;
use App\Barionlibrary\PreparePaymentRequestModel;
use App\Barionlibrary\PaymentType;
use App\Barionlibrary\FundingSourceType;
use App\Barionlibrary\UILocale;
use App\Barionlibrary\Currency;
use App\Product;
use App\Course;
use App\Userdata;
use App\Shoppingcartsetting;
use App\Order;
use App\User;
use Mail;
//use App\Barionlibrary\PreparePaymentRequestModel;
//use App\Barionlibrary\PreparePaymentRequestModel;

class BarionController extends Controller
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	private $myPosKey;
    public function __construct()
    {
        //parent::__construct();

        /** setup PayPal api context **/

    }
    /**
     * Show the application paywith paypalpage.
     *
     * @return \Illuminate\Http\Response
     */
    public function paywithbarion()
    {
        return view('paywithbarion');
    }

	public function generate_key_string() {

		$tokens = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$segment_chars = 5;
		$num_segments = 4;
		$key_string = '';

		for ($i = 0; $i < $num_segments; $i++) {

			$segment = '';

			for ($j = 0; $j < $segment_chars; $j++) {
					$segment .= $tokens[rand(0, 35)];
			}

			$key_string .= $segment;

			if ($i < ($num_segments - 1)) {
					$key_string .= '-';
			}

		}

		return $key_string;

	}

	public function send($user_email, $itemList, $orderid, $is_valid_vat )
    {
        //$purchased_courses = $course
		$user =  User::where('email', '=', $user_email)->first();
		$purchased_courses=[];
		$total_price =  0;
		foreach ($itemList as $item)
		{
			$product = Product::where('title', '=', $item->Name)->first();
			if ($product)
			{
				array_push($purchased_courses, $product->course->title);
			}
			if (!is_null($product->sale_price))
			{
				$total_price = $product->sale_price + $total_price;
			}
			else
			{
				$total_price = $product->regular_price + $total_price;
			}
		}
		if ($is_valid_vat == 1)
		{
			$total_price = $total_price * 100/127;

		}
		$total_price = number_format(round($total_price), 0);
		$codeList = Shoppingcartsetting::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["name"]] = $codeList[$i]["value"];
		}

		if ($settings['currency'] == "USD")
		{
			$total_price = '$'.$total_price;
		}
		else
		{
			$total_price = $total_price.' Ft';
		}
		$purchases = join(',',$purchased_courses);

        Mail::send('emails.purchased_email', ['total' => $total_price, 'purchased_courses' => $purchases], function ($message) use ($user)
        {
            $message->to($user['email']);

        });
        return response()->json(['message' => 'Request completed']);
    }

	public function ApplyUserInfo($order)
	{
		$user = User::Where('email', '=', $order->customer_email)->first();


		if (!$user) return;
		if ($order->order_state == 'Completed' || $order->order_state == 'Invoiced')
		{
			$user_data = Userdata::Where('id', '=', $user->id)->first();
			$productids = explode(',', $order->purchased_product);
			$purchased_courses = [];
			$purchasedids = [];
			foreach ($productids as $productid)
			{
				$product = Product::findOrNew($productid);
				array_push($purchased_courses, $product->course->title);
				array_push($purchasedids, $product->course->id);
			}
			$purchasedids = join(',', $purchasedids);

			if ($user_data->id)
			{
			   if($user_data->purchased_courses){
				      foreach ($productids as $productid)
				      {
					           $product = Product::findOrNew($productid);

					           $purchased_array = explode(',', $user_data['purchased_courses']);
					            $bExist = false;

          					foreach( $purchased_array as $pur_id) {
          						Log::info($pur_id);

          						if ((int)$pur_id == $product->course->id)
          						{
          							$bExist = true;
          						}
          					}

          					if (!$bExist)
          						$user_data['purchased_courses'] = $user_data['purchased_courses'].','.$product->course->id;

          					$user_data->save();
				  }
			   }
			   else
			   {
				   $user_data['purchased_courses'] = $purchasedids;
				   $user_data->save();
			   }
			}
			else
			{

				$user_data = Userdata::create([
					'id' => $user->id,
					'purchased_courses' => $purchasedids
				]);
			}
		}
	}
	public function OrderedProduct($items, $status, $user_email, $is_valid_vat)
	{
		$now = new \DateTime();

		$user =  User::where('email', '=', $user_email)->first();

		$purchased_productids = [];

		$total_price = 0;
		foreach ($items as $item)
		{

			$product = Product::where('title', '=', $item->Name)->first();
			$dtime = strtotime($product->sale_price_end_date);
			$timecmp = false;
			$numcmp = false;
			if (strtotime("now") < $dtime)
			{
				$timecmp = true;
			}

			if ($product->sale_price_orders_current < $product->sale_price_orders_num)
			{
				$numcmp = true;
			}
			if (!is_null($product->sale_price) && $timecmp && $numcmp)
			{
				$total_price = $product->sale_price + $total_price;
				$product->sale_price_orders_current = $product->sale_price_orders_current + 1;
				$product->save();
			}
			else
			{
				$total_price = $product->regular_price + $total_price;
			}
			array_push ($purchased_productids, $product->id);
		}
		$purchased_productids = join (',', $purchased_productids);

		$pricetotal = 0;
		$pricevat = 0;
		$pricenet = 0;

		if ($is_valid_vat == 1)
		{
			$pricenet = $total_price * 100/127;
			$pricevat = 0;
			$pricetotal = $pricenet;
		}
		else if ($is_valid_vat == 0)
		{
			$pricenet = $total_price * 100/127;
			$pricevat = $total_price - $pricenet;
			$pricetotal = $total_price;
		}
		else
		{
			$price_vat = 0;
			$pricenet = $total_price;
			$pricetotal = $total_price;
		}

		if ($user->company)
			$customer = $user->company;
		else
			$customer = $user->name;

		$now = new \DateTime();
		$order = Order::create([
			'user' => $user->id,
      'purchased_product' => $purchased_productids,
			'customer' => $customer,
			'customer_email' => $user->email,
			'billing_address' => $user->street_address.' '.$user->city.' '.$user->country,
			'price_vat' => $pricevat,
			'price_net_amount' => $pricenet,
			'price_total' => $pricetotal,
			'order_date' => $now,
			'payment_method'=> 'Barion'
		]);

		if ($status == 'success')
		{
			$order->order_state = 'Completed';
			$this->ApplyUserInfo($order);
		}
		else if ($status == "pending")
		{
			$order->order_state = 'Pending';
		}
		else
		{
			$order->order_state = 'failed';
		}
		$order->save();
		return $order->id;
	}

    /**
     * Store a details of payment with paypal.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postPaymentWithbarion(Request $request)
    {
		Log::info('barion');

		$codeList = Shoppingcartsetting::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["name"]] = $codeList[$i]["value"];
		}

		$myPosKey = $settings['barion_secret_key'];
		$this->myPosKey = $settings['barion_secret_key'];
		$myEmailAddress = $settings['barion_email'];
		$is_valid_vat = $request->input('is_valid_vat');

		//$myPosKey = "fd37e202d012422ea8ecbbcb7d4bb180"; // <-- Replace this with your POSKey!
		//if (!$myPosKey)
		//$myPosKey = "7ab2a760898c491993338269b836f4db";//"fd37e202d012422ea8ecbbcb7d4bb180";
		//if (!$myEmailAddress)
		//$myEmailAddress = "skyclean906@gmail.com"; // <-- Replace this with your e-mail address in Barion!*/
		$selectProducts = $request->input('selectProducts');
		$user_email = $request->input('account');
		$user =  User::where('email', '=', $user_email)->first();

		$productids = explode(',', $selectProducts);
		$products = [];
		foreach($productids as $productid)
		{
			$product = Product::findOrNew($productid);
			array_push($products, $product);
		}
		// Barion Client that connects to the TEST environment
		$BC = new BarionClient($myPosKey, 2, BarionEnvironment::Prod);

		$trans = new PaymentTransactionModel();
		$trans->POSTransactionId = $user->email;
		$trans->Payee = $myEmailAddress; // no more than 256 characters

		$trans->Comment = $is_valid_vat; // no more than 640 characters

		$item_array=[];
		$total_amount = 0;
		$sku = 0;
		Log::info($products);
		foreach ($products as $product)
		{
			$item = new ItemModel();
			$item->Name = $product->title;
			if ($product->description)
				$item->Description = $product->description;
			else
				$item->Description = "imprevo course";
            $item->Quantity = 1;
			$item->Unit = "piece"; // no more than 50 characters
			if ($product->sale_price)
			{
				if ($is_valid_vat)
				{
					$item->UnitPrice = $product->sale_price * 100 / 127;
					$item->ItemTotal = $product->sale_price * 100 / 127;
				}
				else
				{
					$item->UnitPrice = $product->sale_price;
					$item->ItemTotal = $product->sale_price;
				}
				$total_amount = $total_amount + $item->UnitPrice;
			}
			else
			{
				if ($is_valid_vat)
				{
					$item->UnitPrice = $product->regular_price * 100 / 127;
					$item->ItemTotal = $product->regular_price * 100 / 127;
				}
				else
				{
					$item->UnitPrice = $product->regular_price;
					$item->ItemTotal = $product->regular_price;
				}

				$total_amount = $total_amount + $item->UnitPrice;
			}

			$item->SKU = "ITEM-".$sku;
			$trans->AddItem($item); // add the item to the transaction
			$sku++;

			array_push($item_array, $item);
		}

		$trans->Total = $total_amount;
		// create the request model
		$psr = new PreparePaymentRequestModel();
		$psr->GuestCheckout = true; // we allow guest checkout
		$psr->PaymentType = PaymentType::Immediate; // we want an immediate payment
		$psr->FundingSources = array(FundingSourceType::All); // both Barion wallet and bank card accepted
		$psr->PaymentRequestId = "PAY-01"; // no more than 100 characters
		$psr->PayerHint = $user->email; // no more than 256 characters

		//$psr->Locale = UILocale::EN; // the UI language will be English

		if ($settings['currency'] == "USD")
		{
			$psr->Currency = Currency::USD;
			$psr->Locale = 'en-US';

		}
		else
		{
			$psr->Currency = Currency::HUF;
			$psr->Locale = 'hu-HU';
		}

		$psr->OrderNumber = "ORDER-0001"; // no more than 100 characters
		$psr->ShippingAddress = "12345 NJ, Example ave. 6.";
		$psr->AddTransaction($trans); // add the transaction to the payment

		$protocol = empty($_SERVER['HTTPS']) ? 'http' : 'https';

		// domain name
		$domain = $_SERVER['SERVER_NAME'];


		// server port
		$port = $_SERVER['SERVER_PORT'];
		$disp_port = ($protocol == 'http' && $port == 80 || $protocol == 'https' && $port == 443) ? '' : ":$port";

		// put em all together to get the complete base URL
		$url = "${protocol}://${domain}${disp_port}";
		$url = env('RELATIVE_URL', 'https://imprevo.hu');
		$psr->RedirectUrl = $url.'/home';
		$psr->CallbackUrl = $url.'/barion/callback';

		// send the request
		$myPayment = $BC->PreparePayment($psr);

		$selectProducts = $request->input('selectProducts');
		Log::info($myPayment->Errors);
		Session::put('selectProducts', $selectProducts);
		if ($myPayment->RequestSuccessful == true) {
			// redirect the user to the Barion Smart Gateway
			Log::info('success 1');
			return Redirect($myPayment->PaymentRedirectUrl);
		}
		else
		{
			$url = '/order?products='.$selectProducts;
			return Redirect($url)->with('results-Payment','Payment failed');
		}

    }

    public function getPaymentStatus(Request $request)
    {
        /** Get the payment ID before session clear **/
		Log::info('---------status----------');
		Log::info('success 2');
		Log::info($request->Input('PaymentId'));
		$codeList = Shoppingcartsetting::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["name"]] = $codeList[$i]["value"];
		}

		$myPosKey = $settings['barion_secret_key'];

        $paymentId = $request->Input('PaymentId');
		//$myPosKey = "7ab2a760898c491993338269b836f4db";
		$BC = new BarionClient($myPosKey, 2, BarionEnvironment::Prod);
		$paymentDetails = $BC->GetPaymentState($paymentId);
		$transaction = $paymentDetails->Transactions;

		Log::info($transaction[0]->Items);

		$user_email = $transaction[0]->POSTransactionId;
		$is_valid_vat = $transaction[0]->Comment;
		$selectProducts = $transaction[0]->Items;

		Log::info($user_email);
		Log::info($is_valid_vat);
		Log::info($selectProducts);

		/*Log::info($paymentDetails->Status);
        /** clear the session payment ID **/
		if ($paymentDetails->Status == "Succeeded")
		{
			$transaction = $paymentDetails->Transactions;
			$itemList = $transaction[0]->Items;

			$orderids = $this->OrderedProduct($itemList, 'success', $user_email, $is_valid_vat);
			$this->send($user_email, $itemList, $orderids, $is_valid_vat);
			Log::info("barion-Payment success");
		}
		return response()->json(['success' => 'success'], 200);
    }
  }
