<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use DB;
use Log;
use App\User;
use Mail;
use Illuminate\Support\Facades\Auth;
/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use App\Product;
use App\Userdata;
use App\Course;
use App\Order;
use App\Shoppingcartsetting;
use Fahim\PaypalIPN\PaypalIPNListener;


class PaypalController extends Controller
{
  private $_api_context;
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    //parent::__construct();
    /** setup PayPal api context **/
    $paypal_conf = \Config::get('paypal');

    $token = null;

    $codeList = Shoppingcartsetting::all();
    $Shoppingcartsettings = [];
    for($i = 0; $i < count($codeList); $i++) {
      $Shoppingcartsettings[$codeList[$i]["name"]] = $codeList[$i]["value"];
    }
    $token = $Shoppingcartsettings['paypal_identify_token'];

    Log::info($token);
    Log::info($paypal_conf['settings']);
    //$token = null;
    if ($token)
    {
      $token_split = explode('|', $token);
      $client_id = $token_split[0];
      $secret = $token_split[1];
      Log::info($secret);
      Log::info($client_id);
      $this->_api_context = new ApiContext(new OAuthTokenCredential($client_id, $secret));
    }
    else
    {
      $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
    }
    $this->_api_context->setConfig($paypal_conf['settings']);

  }

  /**
  * Store a details of payment with paypal.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function generate_key_string() {

    $tokens = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $segment_chars = 5;
    $num_segments = 4;
    $key_string = '';

    for ($i = 0; $i < $num_segments; $i++) {

      $segment = '';

      for ($j = 0; $j < $segment_chars; $j++) {
        $segment .= $tokens[rand(0, 35)];
      }

      $key_string .= $segment;

      if ($i < ($num_segments - 1)) {
        $key_string .= '-';
      }

    }

    return $key_string;

  }

  public function paypalIpn()
  {
    $ipn = new PaypalIPNListener();
    $ipn->use_sandbox = true;

    $verified = $ipn->processIpn();

    $report = $ipn->getTextReport();

    Log::info("-----new payment-----");

    Log::info($report);

    if ($verified) {
      if ($_POST['address_status'] == 'confirmed') {
        //payer_email  //payment_status //receiver_email // mc_gross //item_name //item_number //payment_date
        // Check outh POST variable and insert your logic here
        Log::info("payment verified and inserted to db");
      }
    }
    else {
      Log::info("Some thing went wrong in the payment !");
    }
  }

  public function send($user_email, $itemList, $orderid, $is_valid_vat)
  {
    //$purchased_courses = $course
    $user =  User::where('email', '=', $user_email)->first();
    $purchased_courses=[];
    $total_price =  0;
    foreach ($itemList as $item)
    {
      $product = Product::where('title', '=', $item->name)->first();
      if ($product)
      {
        array_push($purchased_courses, $product->course->title);
      }
      if (!is_null($product->sale_price))
      {
        $total_price = $product->sale_price + $total_price;
      }
      else
      {
        $total_price = $product->regular_price + $total_price;
      }
    }

    if ($is_valid_vat == 1)
    {
      $total_price = $total_price * 100/127;

    }

    $purchases = join(',',$purchased_courses);

    $total_price = number_format(round($total_price), 0);
    $codeList = Shoppingcartsetting::all();
    $settings = [];
    for($i = 0; $i < count($codeList); $i++) {
      $settings[$codeList[$i]["name"]] = $codeList[$i]["value"];
    }

    if ($settings['currency'] == "USD")
    {
      $total_price = '$'.$total_price;
    }
    else
    {
      $total_price = $total_price.' Ft';
    }

    Log::info($purchases);
    Log::info($total_price);
    Mail::send('emails.purchased_email', ['total' => $total_price, 'purchased_courses' => $purchases], function ($message) use ($user)
    {
      $message->to($user['email']);
    });

    return response()->json(['message' => 'Request completed']);
  }
  public function ApplyUserInfo($order)
  {
    $user = User::Where('email', '=', $order->customer_email)->first();
    Log::info('paypal apply user info');
    Log::info($user->email);
    Log::info($user->id);
    if (!$user) return;
    if ($order->order_state == 'Completed')
    {
      Log::info('paypal apply user completed');
      $user_data = Userdata::Where('id', '=' , $user->id)->first();
      $productids = explode(',', $order->purchased_product);
      $purchased_courses = [];
      $purchasedids = [];
      foreach ($productids as $productid)
      {
        $product = Product::findOrNew($productid);
        array_push($purchased_courses, $product->course->title);
        array_push($purchasedids, $product->course->id);
      }
      $purchasedids = join(',', $purchasedids);
      Log::info('paypal apply user purchaseddids');
      Log::info($purchasedids);
      Log::info('paypal apply user userdata');
      Log::info($user_data);
      if ($user_data)
      {
        if($user_data->purchased_courses){
          Log::info('paypal apply user purchase courses');
          Log::info($user_data->purchased_courses);
          Log::info('paypal apply user $productids');

          foreach ($productids as $productid)
          {
            $product = Product::findOrNew($productid);

            $purchased_array = explode(',', $user_data['purchased_courses']);
            $bExist = false;

            foreach( $purchased_array as $pur_id) {
              Log::info($pur_id);

              if ((int)$pur_id == $product->course->id)
              {
                $bExist = true;
              }
            }

            if (!$bExist)
            $user_data['purchased_courses'] = $user_data['purchased_courses'].','.$product->course->id;

            $user_data->save();
          }
        }
        else
        {
          Log::info('paypal apply user else');
          Log::info($purchasedids);
          $user_data['purchased_courses'] = $purchasedids;
          $user_data->save();
        }
      }
      else
      {
        Log::info('paypal apply user data created');
        Log::info($purchasedids);
        $user_data = Userdata::create([
          'id' => $user->id,
          'purchased_courses' => $purchasedids
        ]);
        $user_data['purchased_courses'] = $purchasedids;
        $user_data->save();
      }
    }
  }
  public function OrderedProduct($items, $status, $user_email, $is_valid_vat)
  {

    $now = new \DateTime();

    $user =  User::where('email', '=', $user_email)->first();
    Log::info('paypal ordered product');
    $total_price = 0;
    $purchased_products = [];


    foreach ($items as $item)
    {
      $product = Product::where('title', '=', $item->name)->first();
      $dtime = strtotime($product->sale_price_end_date);
      $timecmp = false;
      $numcmp = false;
      if (strtotime("now") < $dtime)
      {
        $timecmp = true;
      }

      if ($product->sale_price_orders_current < $product->sale_price_orders_num)
      {
        $numcmp = true;
      }
      if (!is_null($product->sale_price) && $timecmp && $numcmp)
      {
        $total_price = $product->sale_price + $total_price;
        $product->sale_price_orders_current = $product->sale_price_orders_current + 1;
        $product->save();
      }
      else
      {
        $total_price = $product->regular_price + $total_price;
      }
      array_push ($purchased_products, $product->id);
    }

    $purchased_products = join(',', $purchased_products);
    $pricetotal = 0;
    $pricevat = 0;
    $pricenet = 0;

    if ($is_valid_vat == 1)
    {
      $pricenet = $total_price * 100/127;
      $pricevat = 0;
      $pricetotal = $pricenet;
    }
    else if ($is_valid_vat == 0)
    {
      $pricenet = $total_price * 100/127;
      $pricevat = $total_price - $pricenet;
      $pricetotal = $total_price;
    }
    else
    {
      $price_vat = 0;
      $pricenet = $total_price;
      $pricetotal = $total_price;
    }
    if ($user->company)
    $customer = $user->company;
    else
    $customer = $user->name;
    //if (!$order){
    $order = Order::create([
      'user' => $user->id,
      'purchased_product' => $purchased_products,
      'customer' => $customer,
      'customer_email' => $user->email,
      'billing_address' => $user->street_address.' '.$user->city.' '.$user->country,
      'order_date' => $now,
      'price_vat' => $pricevat,
      'price_net_amount' => $pricenet,
      'price_total' => $pricetotal,
      'payment_method'=> 'Paypal'
    ]);

    if ($status == 'success')
    {
      $order->order_state = 'Completed';
      $this->ApplyUserInfo($order);
    }
    else if ($status == "pending")
    {
      $order->order_state = 'Pending';
    }
    else
    {
      $order->order_state = 'failed';
    }
    $order->save();
    return $order->id;
  }

  public function postPaymentWithpaypal(Request $request)
  {
    $codeList = Shoppingcartsetting::all();
    $settings = [];
    for($i = 0; $i < count($codeList); $i++) {
      $settings[$codeList[$i]["name"]] = $codeList[$i]["value"];
    }
    $is_valid_vat = $request->input('is_valid_vat');
    $selectProducts = $request->input('selectProducts');
    $user_email = $request->input('account');
    Log::info($user_email);
    $user =  User::where('email', '=', $user_email)->first();

    Log::info($selectProducts);
    $productids = explode(',', $selectProducts);
    $products = [];
    foreach($productids as $productid)
    {
      $product = Product::findOrNew($productid);
      array_push($products, $product);
    }


    $payer = new Payer();
    $payer->setPaymentMethod('paypal');

    $item_array=[];
    $total_amount = 0;

    foreach ($products as $product)
    {
      $item = new Item();
      $item->setName($product->title);
      if ($settings['currency'] == "USD")
      {
        $item->setCurrency('USD');
      }
      else
      {
        $item->setCurrency('HUF');
      }
      $item->setQuantity(1);
      if ($product->sale_price)
      {
        if ($is_valid_vat)
        {
          $item->setPrice($product->sale_price * 100 / 127);
          $total_amount = $total_amount + $product->sale_price * 100 / 127;
        }
        else
        {
          $item->setPrice($product->sale_price);
          $total_amount = $total_amount + $product->sale_price;
        }
      }
      else
      {
        if ($is_valid_vat)
        {
          $item->setPrice($product->regular_price * 100 / 127);
          $total_amount = $total_amount + $product->regular_price * 100 / 127;
        }
        else
        {
          $item->setPrice($product->regular_price);
          $total_amount = $total_amount + $product->regular_price;
        }
      }
      array_push($item_array, $item);
    }

    $item_list = new ItemList();
    $item_list->setItems($item_array);

    $amount = new Amount();

    if ($settings['currency'] == "USD")
    {
      $amount->setCurrency('USD');
    }
    else
    {
      $amount->setCurrency('HUF');
    }

    $amount->setTotal($total_amount);

    $transaction = new Transaction();
    $transaction->setAmount($amount)
    ->setItemList($item_list)
    ->setDescription('Your transaction description');

    $redirect_urls = new RedirectUrls();
    $redirect_urls->setReturnUrl(URL::route('payment.paypal.status'))
    ->setCancelUrl("https://imprevo.hu/order?products=".$selectProducts);

    $payment = new Payment();
    $payment->setIntent('Sale')
    ->setPayer($payer)
    ->setRedirectUrls($redirect_urls)
    ->setTransactions(array($transaction));


    try {
      $payment->create($this->_api_context);
    } catch (\PayPal\Exception\PayPalConnectionException  $ex) {
      if (\Config::get('app.debug')) {
        \Session::put('error','Connection timeout');
        return back()->with('result', 'Connection timeout');
      } else {
        \Session::put('error','Some error occur, sorry for inconvenient');
        return back()->with('result', 'unknown error occurred');
      }
    }

    foreach($payment->getLinks() as $link) {
      if($link->getRel() == 'approval_url') {
        $redirect_url = $link->getHref();
        break;
      }
    }
    /** add payment ID to session **/
    Session::put('paypal_payment_id', $payment->getId());
    Session::put('is_valid_vat', $is_valid_vat);
    Session::put('user_email', $user_email);
    Session::put('selectProducts', $selectProducts);
    if(isset($redirect_url)) {
      return Redirect::away($redirect_url);
    }

    //Session::put('error','Unknown error occurred');
    return back()->with('result', 'unknown error occurred');
  }

  public function getPaymentStatus()
  {
    /** Get the payment ID before session clear **/
    $user_email = Session::get('user_email');

    $payment_id = Session::get('paypal_payment_id');
    $is_valid_vat = Session::get('is_valid_vat');
    $selectProducts = session::get('selectProducts');
    /** clear the session payment ID **/
    Log::info('payapl payment status');
    Session::forget('paypal_payment_id');
    Session::forget('user_email');
    Session::forget('is_valid_vat');
    Session::forget('selectProducts');
    if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
      Session::put('error','Payment failed');
      return Redirect('/order?products='.$selectProducts);
    }
    $payment = Payment::get($payment_id, $this->_api_context);
    /** PaymentExecution object includes information necessary **/
    /** to execute a PayPal account payment. **/
    /** The payer_id is added to the request query parameters **/
    /** when the user is redirected from paypal back to your site **/
    $execution = new PaymentExecution();
    $execution->setPayerId(Input::get('PayerID'));
    /**Execute the payment **/
    $result = $payment->execute($execution, $this->_api_context);
    /** dd($result);exit; /** DEBUG RESULT, remove it later **/
    if ($result->getState() == 'approved') {
      Log::info('payapl payment approved');
      /** it's all right **/
      /** Here Write your database logic like that insert record or value in database if you want **/
      $transaction = $payment->getTransactions();
      $itemList = $transaction[0]->getItemList();
      /*foreach ($itemList->items as $item)
      {
      $this->OrderedProduct($item, 'success', $user_email);
    }*/

    $orderid  = $this->OrderedProduct($itemList->items, 'success', $user_email, $is_valid_vat);
    $this->send($user_email, $itemList->items, $orderid, $is_valid_vat);

    return redirect()->to('/order/status/successful?products='.$selectProducts)
    ->with('results-Payment','Payment success');
  }
  else if ($result->getState() == 'failed')
  {
    $transaction = $payment->getTransactions();
    $itemList = $transaction[0]->getItemList();
    /*foreach ($itemList->items as $item)
    {
    $this->OrderedProduct($item, 'failed', $user_email);
  }*/
  $this->OrderedProduct($itemList->items, 'failed', $user_email, $is_valid_vat);
  return redirect()->to('/order/status/failed?products='.$selectProducts)
  ->with('results-Payment','Payment failed');
}
else
{
  $order_ids = [];
  $transaction = $payment->getTransactions();
  $itemList = $transaction[0]->getItemList();
  $total_price = 0;

  foreach ($itemList->items as $item)
  {
    //$order_id = $this->OrderedProduct($item, 'pending', $user_id);
    $total_price = $total_price + $item->getPrice();
    //array_push($order_ids, $order_id);
  }
  $total_price = number_format(round($total_price),0);
  $orderid = $this->OrderedProduct($itemList->items, 'pending', $user_email, $is_valid_vat);
  $this->send($user_email, $itemList->items, $orderid, $is_valid_vat);
  return redirect()->to('/order/status/pending?products='.$selectProducts)
  ->with(['results-Payment' => 'Payment pending' ,'orderid' => $orderid, 'total_price' => $total_price]);
}
}
}
