<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lesson;
use App\Course;
use App\Level;
use App\Userdata;
use App\Exercise;
use App\Code;
use App\Product;
use App\Shoppingcartsetting;
use App\Order;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use Log;

class ShoppingcartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(Request $request)
    {
		$productstr = $request->input('products');		
		$productarray = explode(',', $productstr);
		$productsales = [];
		foreach ($productarray as $productid)
		{
			//Log::info($productid);
			$product = Product::findOrNew($productid);
			array_push($productsales, $product);
		}
		
		$products = Product::all();
		$similar_products = [];
		if (count($productsales) > 0)
		{
			foreach ($products as $product)
			{	
				if ($product->id != $productsales[0]->id){
					/*$similarity = similar_text($product->title, $productsales[0]->title);
					if ($similarity > 10)
					{					
						array_push($similar_products, $product);
					}*/
					array_push($similar_products, $product);
				}
			}
		}	

		$codeList = Shoppingcartsetting::all();
		$Shoppingcartsettings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$Shoppingcartsettings[$codeList[$i]["name"]] = $codeList[$i]["value"];
		}

		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
		}
		
		$user_id = null;
		if (Auth::user())
			$user_id = Auth::user()->id;
		$user = null;
		if ($user_id)
			$user = User::findOrNew($user_id);
		
		/*$userdata = Userdata::where('id', $user_id)->first();		
		if ($userdata)
			$purchased_courses = $userdata->purchased_courses;*/
		
		$orders = Order::all();		
		$purchased_products = [];		
		Log::info($user);
		foreach ($orders as $order)
		{
			if ($user){
				if ($user->email == $order->customer_email)
				{
					if ($order->order_state == "Completed" || $order->order_state == "Pending" || $order->order_state == "Invoiced")
					{
						array_push ($purchased_products, $order->purchased_product);
					}
				}
			}
		}
		
		$purchased_products = join(',', $purchased_products);
				
        return view('shoppingcart', [
            'productsales' => $productsales,
			'products' => $similar_products,
			'shoppingcartsetting' => $Shoppingcartsettings,
			'settings' => $settings,
			'purchased_products' => $purchased_products,
        ]);
    }
}
