<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lesson;
use App\Course;
use App\Level;
use App\Userdata;
use App\Exercise;
use App\User;
use App\Product;
use App\Shoppingcartsetting;
use App\Code;
use Countries;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use Log;

class ShopController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function index(Request $request)
    {
		$products = Product::all();

		$product_sale = [];
    $product_featured = [];
		$products_not_sale_feature = [];
		foreach ($products as $product)
		{
			$dtime = strtotime($product->sale_price_end_date);
			$timecmp = false;
			$numcmp = false;
			if (strtotime("now") < $dtime){
				$timecmp = true;
			}

			if ($product->sale_price_orders_current < $product->sale_price_orders_num) {
				$numcmp = true;
			}

			if (!is_null($product->sale_price) && $timecmp && $numcmp) {
				$product['price'] = $product->sale_price;
        array_push($product_sale, $product);
			}

			if ($product->featured_product && !(!is_null($product->sale_price) && $timecmp && $numcmp)) {
          $product['price'] = $product->regular_price;
					array_push($product_featured, $product);
			}

			if (!(!is_null($product->sale_price) && $timecmp && $numcmp) && !$product->featured_product){
				$product['price'] = $product->regular_price;
				array_push($products_not_sale_feature, $product);
			}
		}

		$codeList = Shoppingcartsetting::all();
		$Shoppingcartsettings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$Shoppingcartsettings[$codeList[$i]["name"]] = $codeList[$i]["value"];
		}

    $codeList = Code::all();
    $settings = [];
    for($i = 0; $i < count($codeList); $i++) {
      $settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
    }
      return view('shop', [
			'product_sale' => $product_sale,
			'product_featured' => $product_featured,
			'products' => $products_not_sale_feature,
			'shoppingcartsetting' =>   $Shoppingcartsettings,
      'settings' => $settings
      ]);
    }
}
