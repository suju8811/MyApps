<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lesson;
use App\Course;
use App\Level;
use App\Userdata;
use App\Exercise;
use App\User;
use App\Product;
use App\Shoppingcartsetting;
use App\Code;
use Countries;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use Log;

class OrderController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	private $products = null;
	private $productstr = '';
	public function __construct()
	{
		//$this->middleware('auth');
	}

	public static function checkVATID($id) {
		$vatCountry = substr($id, 0, 2); // get country code - first two letters of VAT ID
		$vatNumber = substr($id, 2);
		$apiURL = "http://ec.europa.eu/taxation_customs/vies/viesquer.do?ms=".$vatCountry."&vat=".$vatNumber;
		$resp = file_get_contents($apiURL);
		if(strpos($resp, '="validStyle"') !== false) return true;
		else return false;
	}

	public function index(Request $request)
	{
		$user = null;
		if (Auth::user()){
			$user_id = Auth::user()->id;
			$user = User::where('id', $user_id)->first();
		}

		$back = $request->input('back');




		$countryregions = Countries::all()->pluck('region', 'name.common');
		$countryregions = (array)$countryregions;
		//Log::info($countryregions);
		foreach ($countryregions as $countryregion)
		{
			$countryregions = $countryregion;
			break;
		}
		ksort($countryregions);

		$codeList = Shoppingcartsetting::all();
		$Shoppingcartsettings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$Shoppingcartsettings[$codeList[$i]["name"]] = $codeList[$i]["value"];
		}

		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
		}

		if ($back == 1){
			session_start();
			$productstr = $_SESSION["productstr"];
			$productarray = explode(',', $productstr);
			$products = [];
			foreach ($productarray as $productid)
			{
				//Log::info($productid);
				$product = Product::findOrNew($productid);
				array_push($products, $product);
			}
			$this->productstr = $productstr;
			$this->products = $products;
			return view('order/shop-register', [
				'products' => $products,
				'productstr' => $productstr,
				'user' => $user,
				'countrylist' => $countryregions,
				'shoppingcartsetting' => $Shoppingcartsettings,
				'settings' =>   $settings
			]);
		} else if ($back == 2) {
			session_start();
			$productstr = $_SESSION["productstr"];


			$productarray = explode(',', $productstr);
			$products = [];
			foreach ($productarray as $productid)
			{
				//Log::info($productid);
				$product = Product::findOrNew($productid);
				array_push($products, $product);
			}
			$this->productstr = $productstr;
			$this->products = $products;
			return view('order/billing-details', [
				'products' => $products,
				'productstr' => $productstr,
				'user' => $user,
				'countrylist' => $countryregions,
				'shoppingcartsetting' => $Shoppingcartsettings,
				'settings' =>   $settings
			]);
		} else if ($back == 3) {
			session_start();
			$productstr = $_SESSION["productstr"];
			$tax_num = $_SESSION['tax_num'] ;
			$is_valid_vat = $_SESSION['is_valid_vat'];
			$eu_vat_num = $_SESSION['eu_vat_num'];

			$productarray = explode(',', $productstr);
			$products = [];
			foreach ($productarray as $productid)
			{
				$product = Product::findOrNew($productid);
				array_push($products, $product);
			}

			return view('order/select-payment-method', [
				'products' => $products,
				'productstr' => $productstr,
				'user' => $user,
				'countrylist' => $countryregions,
				'shoppingcartsetting' => $Shoppingcartsettings,
				'settings' =>   $settings,
				'tax_num' => $tax_num,
				'is_valid_vat' => $is_valid_vat,
				'eu_vat_num' => $eu_vat_num,
			]);
		} else {
			$productstr = $request->input('products');
			$productarray = explode(',', $productstr);
			$products = [];
			foreach ($productarray as $productid)
			{
				//Log::info($productid);
				$product = Product::findOrNew($productid);
				array_push($products, $product);
			}
			$this->productstr = $productstr;
			$this->products = $products;
			session_start();
			$_SESSION["productstr"] = $productstr;
			if ($user){
				return view('order/billing-details', [
					'products' => $products,
					'productstr' => $productstr,
					'user' => $user,
					'countrylist' => $countryregions,
					'shoppingcartsetting' => $Shoppingcartsettings,
					'settings' =>   $settings
				]);
			} else {
				return view('order/shop-register', [
					'products' => $products,
					'productstr' => $productstr,
					'user' => $user,
					'countrylist' => $countryregions,
					'shoppingcartsetting' => $Shoppingcartsettings,
					'settings' =>   $settings
				]);
			}
		}
	}

	public function login(Request $request)
	{

		$this->validate($request, [
			'login-email' => 'required|email',
			'login-password' => 'required',
		]);

		if (auth()->attempt(array('email' => $request->input('login-email'), 'password' => $request->input('login-password'))))
		{
			$user = User::where('id', Auth::user()->id)->first();

			$productstr = $request->input('productstr');
			$productarray = explode(',', $productstr);
			$products = [];
			foreach ($productarray as $productid)
			{
				//Log::info($productid);
				$product = Product::findOrNew($productid);
				array_push($products, $product);
			}

			$countryregions = Countries::all()->pluck('region', 'name.common');
			$countryregions = (array)$countryregions;
			//Log::info($countryregions);
			foreach ($countryregions as $countryregion)
			{
				$countryregions = $countryregion;
				break;
			}
			ksort($countryregions);

			$codeList = Shoppingcartsetting::all();
			$Shoppingcartsettings = [];
			for($i = 0; $i < count($codeList); $i++) {
				$Shoppingcartsettings[$codeList[$i]["name"]] = $codeList[$i]["value"];
			}

			$codeList = Code::all();
			$settings = [];
			for($i = 0; $i < count($codeList); $i++) {
				$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
			}

			return view('order/billing-details', [
				'products' => $products,
				'productstr' => $productstr,
				'user' => $user,
				'countrylist' => $countryregions,
				'shoppingcartsetting' => $Shoppingcartsettings,
				'settings' =>   $settings
			]);
		}
		else
		{
			return back()->with('error-login', trans('auth.wrong_input'));
		}
	}

	protected function validator(array $data)
	{
		return Validator::make($data, [
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|min:5',
		]);
	}

	public function register(Request $request)
	{
		$input = $request->all();
		$input_correct = [];
		$input_correct['email'] = $input['register-email'];
		$input_correct['password'] = $input['register-password'];
		$validator = $this->validator($input_correct);

		if ($validator->passes()) {
			$user = User::create([
				'name' => $input['register-email'],
				'email' => $input['register-email'],
				'password' => bcrypt($input['register-password']),
				'permission'=> 1,
			]);


			if (auth()->attempt(array('email' => $request->input('register-email'), 'password' => $request->input('register-password'))))
			{
				$productstr = $request->input('productstr');
				$productarray = explode(',', $productstr);
				$products = [];
				foreach ($productarray as $productid)
				{
					$product = Product::findOrNew($productid);
					array_push($products, $product);
				}

				$countryregions = Countries::all()->pluck('region', 'name.common');
				$countryregions = (array)$countryregions;
				//Log::info($countryregions);
				foreach ($countryregions as $countryregion)
				{
					$countryregions = $countryregion;
					break;
				}
				ksort($countryregions);

				$codeList = Shoppingcartsetting::all();
				$Shoppingcartsettings = [];
				for($i = 0; $i < count($codeList); $i++) {
					$Shoppingcartsettings[$codeList[$i]["name"]] = $codeList[$i]["value"];
				}

				$codeList = Code::all();
				$settings = [];
				for($i = 0; $i < count($codeList); $i++) {
					$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
				}

				return view('order/billing-details', [
					'products' => $products,
					'productstr' => $productstr,
					'user' => $user,
					'countrylist' => $countryregions,
					'shoppingcartsetting' => $Shoppingcartsettings,
					'settings' =>   $settings
				]);
			}
		}
		return back()->with('errors',$validator->errors());
	}

	public function purchase_overview(Request $request){
		$user = User::where('id', Auth::user()->id)->first();

		$productstr = $request->input('selectProducts');
		$productarray = explode(',', $productstr);
		$products = [];
		foreach ($productarray as $productid)
		{
			$product = Product::findOrNew($productid);
			array_push($products, $product);
		}

		$countryregions = Countries::all()->pluck('region', 'name.common');
		$countryregions = (array)$countryregions;

		foreach ($countryregions as $countryregion)
		{
			$countryregions = $countryregion;
			break;
		}
		ksort($countryregions);

		$codeList = Shoppingcartsetting::all();
		$Shoppingcartsettings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$Shoppingcartsettings[$codeList[$i]["name"]] = $codeList[$i]["value"];
		}

		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
		}

		$tax_num = $request->input('tax_num');
		$is_valid_vat = $request->input('is_valid_vat');
		$eu_vat_num = $request->input('eu_vat_num');
		$account = $request->input('account');
		$payment_input = $request->input('payment_input');

		return view('order/purchase-overview', [
			'products' => $products,
			'productstr' => $productstr,
			'user' => $user,
			'countrylist' => $countryregions,
			'shoppingcartsetting' => $Shoppingcartsettings,
			'settings' =>   $settings,
			'is_valid_vat' => $is_valid_vat,
			'tax_num' => $tax_num,
			'eu_vat_num' => $eu_vat_num,
			'account' => $account,
			'payment_input' => $payment_input
		]);
	}

	public function payment_pending(Request $request){
		$user = User::where('id', Auth::user()->id)->first();

		$productstr = $request->input('products');
		$productarray = explode(',', $productstr);
		$products = [];
		foreach ($productarray as $productid)
		{
			$product = Product::findOrNew($productid);
			array_push($products, $product);
		}

		$codeList = Shoppingcartsetting::all();
		$Shoppingcartsettings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$Shoppingcartsettings[$codeList[$i]["name"]] = $codeList[$i]["value"];
		}

		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
		}

		return view('order/pending-order', [
			'products' => $products,
			'productstr' => $productstr,
			'user' => $user,
			'shoppingcartsetting' => $Shoppingcartsettings,
			'settings' =>   $settings,
		]);
	}

	public function payment_success(Request $request){
		$user = User::where('id', Auth::user()->id)->first();

		$productstr = $request->input('products');
		$productarray = explode(',', $productstr);
		$products = [];
		foreach ($productarray as $productid)
		{
			$product = Product::findOrNew($productid);
			array_push($products, $product);
		}

		$codeList = Shoppingcartsetting::all();
		$Shoppingcartsettings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$Shoppingcartsettings[$codeList[$i]["name"]] = $codeList[$i]["value"];
		}

		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
		}

		return view('order/successful-order', [
			'products' => $products,
			'productstr' => $productstr,
			'user' => $user,
			'shoppingcartsetting' => $Shoppingcartsettings,
			'settings' =>   $settings,
		]);
	}

	public function payment_failed(Request $request){
		$user = User::where('id', Auth::user()->id)->first();

		$productstr = $request->input('products');
		$productarray = explode(',', $productstr);
		$products = [];
		foreach ($productarray as $productid)
		{
			$product = Product::findOrNew($productid);
			array_push($products, $product);
		}

		$codeList = Shoppingcartsetting::all();
		$Shoppingcartsettings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$Shoppingcartsettings[$codeList[$i]["name"]] = $codeList[$i]["value"];
		}

		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
		}

		return view('order/unsuccessful-order', [
			'products' => $products,
			'productstr' => $productstr,
			'user' => $user,
			'shoppingcartsetting' => $Shoppingcartsettings,
			'settings' =>   $settings,
		]);
	}

	public function payments(Request $request){
		$user = User::where('id', Auth::user()->id)->first();

		$productstr = $request->input('productstr');
		$productarray = explode(',', $productstr);
		$products = [];
		foreach ($productarray as $productid)
		{
			$product = Product::findOrNew($productid);
			array_push($products, $product);
		}

		$countryregions = Countries::all()->pluck('region', 'name.common');
		$countryregions = (array)$countryregions;

		foreach ($countryregions as $countryregion)
		{
			$countryregions = $countryregion;
			break;
		}
		ksort($countryregions);

		$codeList = Shoppingcartsetting::all();
		$Shoppingcartsettings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$Shoppingcartsettings[$codeList[$i]["name"]] = $codeList[$i]["value"];
		}

		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
		}

		$tax_num = $request->Input('tax_num');
		$is_valid_vat = $request->Input('is_valid_vat');
		$eu_vat_num = $request->Input('eu_vat_num');

		session_start();
		$_SESSION['tax_num'] = $tax_num;
		$_SESSION['is_valid_vat'] = $is_valid_vat;
		$_SESSION['eu_vat_num'] = $eu_vat_num;

		return view('order/select-payment-method', [
			'products' => $products,
			'productstr' => $productstr,
			'user' => $user,
			'countrylist' => $countryregions,
			'shoppingcartsetting' => $Shoppingcartsettings,
			'settings' =>   $settings,
			'tax_num' => $tax_num,
			'is_valid_vat' => $is_valid_vat,
			'eu_vat_num' => $eu_vat_num,
		]);
	}

	public function billinginfo(Request $request)
	{
		$user = User::findOrNew(Auth::user()->id);
		$is_valid_vat = -1;
		if (!$request->input('company-name')){
			$user['name'] = $request->input('name');
			$user['city'] = $request->input('city');
			$user['country'] = $request->input('country');
			$user['zipcode'] = $request->input('postcode');
			$user['street_address'] = $request->input('street_address');
		}
		else
		{
			$user['city'] = $request->input('company-city');
			$user['country'] = $request->input('company-country');
			$user['zipcode'] = $request->input('company-postcode');
			$user['street_address'] = $request->input('company-street_address');
			$user['company'] = $request->input('company-name');
			$user['tax_num'] = $request->input('tax_num');
			$user['eu_vat_num'] = $request->input('eu_vat_num');

			$validvat = $this->checkVATID($request->input('eu_vat_num'));
			//Log::info($validvat);
			if ($validvat) $is_valid_vat='1';
			else $is_valid_vat = '0';

		}

		$user->save();

		return ['is_valid_vat' => $is_valid_vat];
	}
}
