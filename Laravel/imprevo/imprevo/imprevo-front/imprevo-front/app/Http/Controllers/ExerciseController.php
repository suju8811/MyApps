<?php

namespace App\Http\Controllers;

/******************************************************
* IM - Vocabulary Builder
* Version : 1.0.2
* Copyright© 2016 Imprevo Ltd. All Rights Reversed.
* This file may not be redistributed.
* Author URL:http://imprevo.net
******************************************************/
use App\Lesson;
use App\Module;
use App\Exercise;
use App\Quiz;
use App\Word;
use App\QuizSetting;
use App\Userdata;
use App\User;
use App\Code;

use Log;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use Input;
use Mail;

class ExerciseController extends Controller
{
  public function __construct()
  {
    //$this->middleware('auth');
  }

  public function ReportError(Request $request)
  {

    Log::info('Controller- Report Error');
    $content = $request->input('report_message');
    $id = $request->input('exercise_id');
    Log::info($content);
    $user_id = auth()->user()->id;
    $from = 'unknown';
    if ($user_id)
    {
      $user = User::findOrNew($user_id);
      if ($user) $from = $user->email;
    }

    $codeList = Code::all();
    $settings = [];
    for($i = 0; $i < count($codeList); $i++) {
      $settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
    }

    $data = array('exercise_id'=> $id, 'content' => $content, 'email' => $settings['siteEmail'], 'from'=> $from);

    Mail::send('emails.report_mail', $data, function($message) use ($data) {
      $message->to($data['email']);
      $message->from($data['email'], $data['from']);
      $message->subject(trans('report_mail.span2'));
    });

    return back();
  }

  public function AddUserData(Request $request, $id, $quiz=true)
  {
    $exercise= Exercise::where('id',$id)->first();
    $modules = Module::all();
    $lesson = $exercise->lesson;

    $level = $lesson->level;

    $user_id = null;
    if (auth()->user()){
        $user_id = auth()->user()->id;
    }else {
        return;
    }


    $temp = false;
    $next_exe_id = null;

    if ($user_id)
    {
      $user_data = Userdata::where('id', $user_id)->first();

      if (!isset($user_data->id))
      {
        $user_data = null;
      }
    }
    else
    {
      $user_data = null;
    }

    foreach($modules as $mod)
    {
      if ($next_exe_id) break;
      foreach ($mod->exercises as $m_exe)
      {
        if ($next_exe_id) break;
        foreach ($lesson->exercises as $exe)
        {
          if ($m_exe->id == $exe->id)
          {
            if ($exe->id != $exercise->id)
            {
              $done_exercises_array = explode(',' , $user_data['done_exercises']);
              $bExist = false;
              foreach( $done_exercises_array as $exe_done_id)
              {
                if ((int)$exe_done_id == $exe->id )
                {
                  $bExist = true;
                  break;
                }
              }
              if ($bExist == false)  {
                $next_exe_id = $exe->id;
                break;
              }
            }
          }
        }
      }
    }
    if (!$next_exe_id)
    {
      foreach($modules as $mod)
      {
        if ($next_exe_id) break;
        foreach ($mod->exercises as $m_exe)
        {
          if ($next_exe_id) break;
          foreach ($level->lessons as $le)
          {
            if ($next_exe_id) break;
            foreach($le->exercises as $e)
            {
              if ($e->id == $m_exe->id)
              {
                $bExist = false;
                $done_exercises_array = explode(',' , $user_data['done_exercises']);
                foreach( $done_exercises_array as $exe_done_id) {
                  if ((int)$exe_done_id == $e->id || $e->id == $exercise->id)
                  {
                    $bExist = true;
                    break;
                  }
                }
                if (!$bExist)
                {
                  $next_exe_id = $e->id;
                  break;
                }
              }
            }
          }
        }
      }
    }
    if ($exercise->type != 'quiz' || $quiz==true)
    {
      if ($user_data)
      {
        $done_exercises_array = explode(',' , $user_data['done_exercises']);
        $bExist = false;


        while(in_array($exercise->id, $done_exercises_array)){
          foreach( $done_exercises_array as $key => $exe_id) {
            if ((int)$exe_id == $exercise->id )
            {
              array_splice($done_exercises_array, $key, 1);
              break;
            }
          }
        }

        $done_exercises_str = join(',',$done_exercises_array);
        $user_data['done_exercises'] = join(",", [$done_exercises_str,$exercise->id]);

        if ($next_exe_id)
        {
          $user_data['current_exercise'] = $next_exe_id;
        }
        else
        {
          $user_data['current_exercise'] =  $exercise->id;
        }
        $user_data->save();
      }
      else
      {
        // echo("<script>console.log('PHP: user_id:".json_encode($user_id)."');</script>");
        // echo("<script>console.log('PHP: exercise_id:".strval($exercise->id)."');</script>");
        // echo("<script>console.log('PHP: next_exe_id:".$next_exe_id."');</script>");
        $user_data = Userdata::create([
          'id' => $user_id,
          'done_exercises' =>  strval($exercise->id),
          'current_exercise' => $next_exe_id]);
        }
      }
      else if ($exercise->type == 'quiz')
      {
        if ($user_data)
        {
          $user_data['current_exercise'] =  $exercise->id;
          echo("<script>console.log('PHP: ".json_encode($exercise->id)."');</script>");
          $user_data->save();
        }
        else
        {
          $user_data = Userdata::create([
            'id' => $user_id,
            'current_exercise' => $exercise->id]);
          }
        }
      }

      public function index(Request $request, $id)
      {
        $exercise= Exercise::findOrNew($id);

        $lesson = $exercise->lesson;
        $quizsettings = QuizSetting::all();
        $modules = Module::all();
        $words = Word::all();
        $quiz = null;
        if ($exercise->type == 'quiz' && $exercise['quiz_id'])
        {
          $quiz = Quiz::find($exercise['quiz_id']);
          $words = Word::all()->where('wordset_id', $quiz->wordset_id);
        }
        //echo("<script>console.log('PHP: exercise_id:".strval($request)."');</script>");


        $this->AddUserData($request, $id);
        $user_id = null;
        if (auth()->user())
        $user_id = auth()->user()->id;

        $user = User::where('id', $user_id)->first();
        $user_data = null;
        if ($user_id)
        {
          $user_data = Userdata::where('id', $user_id)->first();

          if (!$user_data->id)
          {
            $user_data = null;
          }
        }



        $done_exercises_array = explode(',' , $user_data['done_exercises']);
        $count_done = 0;
        $count_lesson = 0;
        foreach ($lesson->exercises as $exe)
        {
          $done_exercises_array = explode(',' , $user_data['done_exercises']);
          $bExist = false;
          foreach( $done_exercises_array as $exe_done_id)
          {
            if ((int)$exe_done_id == $exe->id )
            {
              $count_done++;
              break;
            }
          }
          $count_lesson++;
        }
        $pro = 0;
        if ($count_lesson != 0)
        {
          $pro = intval($count_done*100/$count_lesson);
        }
        //$courses = Course::paginate(15);

        $is_permission = 0;

        if ($user)
        {
          if ($user->permission > 2) $is_permission = 1;

          if ($user->courses)
          {
            foreach ($user->courses as $course)
            {
              if ($course->id == $lesson->course_id) $is_permission = 1;

            }
          }

          if ($user_data)
          {
            $courseids = explode(',', $user_data->purchased_courses);
            foreach ($courseids as $courseid)
            {
              if ($courseid == $lesson->course_id) $is_permission = 1;
            }
          }
        }

        $codeList = Code::all();
        $settings = [];
        for($i = 0; $i < count($codeList); $i++) {
          $settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
        }

        return view('Exercise', [
          'exercise' => $exercise,
          'lesson' => $lesson,
          'lessons'=> Lesson::all(),
          'modules' => $modules,
          'quiz'  => $quiz,
          'words' => $words,
          'quizsettings' => $quizsettings,
          'pro' => $pro,
          'is_permission' => $is_permission,
          'settings' => $settings
        ]);
      }

    }
