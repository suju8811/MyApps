<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/
use App\Lesson;
use App\Module;
use Illuminate\Http\Request;
use DB;
use Input;

class LessonHomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $id)
    {
        $lesson = Lesson::findOrNew($id);
        $modules = Module::all();

        //$courses = Course::paginate(15);
        return view('lessonHome', [
            'lesson' => $lesson,
            'modules' => $modules
        ]);
    }
}
