<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/
use App\Lesson;
use App\Course;
use App\Level;
use App\Userdata;
use App\Exercise;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use DB;
use Input;

class LessonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $id)
    {
        $lessons = Lesson::paginate(15);

        $user_id = auth()->user()->id;

        if ($user_id)
        {
            $user_data = Userdata::findorNew($user_id);
        }
        else
        {
          $user_data = null;
        }

        if ($user_data)
        {
          $current_exercise = Exercise::findOrNew($user_data->current_exercise);
        }
        else
        {
          $current_exercise = null;
        }
        

        //$courses = Course::paginate(15);
        return view('lessons', [
            'lessons' => $lessons,
            'level' =>  Level::findOrNew($id),
            'user_data' => $user_data,
            'current_exercise' => $current_exercise
        ]);
    }
}
