<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/
use App\Lesson;
use App\Course;
use App\Level;
use App\Userdata;
use App\Exercise;
use App\Syllabus;
use App\Product;
use App\Module;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use DB;
use Input;
use Log;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function query(Request $request){

        $course_id = $request->input('course_id');
        $query = $request->input('query');

        $lesson_all = Lesson::where('course_id', $course_id)->where('title', 'like', '%'.$query.'%')->get();
        $syllabus = Syllabus::where('course_id', $course_id)->where('description', 'like', '%'.$query.'%')->get();

        $lessons = [];
        foreach($lesson_all as $item){
          if (!in_array($item->id, $lessons)){
            array_push($lessons, $item->id);
          }
        }

        foreach($syllabus as $item){
          if (!in_array($item->lesson_id, $lessons)){
            array_push($lessons, $item->lesson_id);
          }
        }

        if ($query == ''){
          $lessons = null;
        }
        echo json_encode ($lessons);
    }

    public function index(Request $request, $id)
    {
        $course = Course::find($id);

        $tab = $request->input('tab');

        $course->is_purchased = false;

        $lessons = Lesson::where('course_id', $id)->get();

        $course->total_exe_count = 0;
        $course->done_exe_count = 0;
        $course->last_exercise = null;
        $course->last_level =  $course->levels[0]->title;

        $product = Product::where('course_id', $course->id)->first();
        $course->product = $product->id;

        $bFirst = false;
        foreach ($lessons as $lesson){
          if (!$bFirst){
            foreach ($lesson->exercises as $exe){
              if ($exe->id){
                $bFirst = true;
                $course->last_exercise = $exe->id;
                break;
              }
            }
          }

          $lesson->status = 'A';
          $lesson->done_exe_count = 0;
          $lesson->exe_count = count($lesson->exercises);

          $syllabus = Syllabus::where('course_id', $course->id)->where('lesson_id', $lesson->id)->get();
          foreach($syllabus as $item){
            $module = Module::where('id', $item->module_id)->first();
            $item->module_icon = $module->icon;
          }


          $lesson->syllabus = $syllabus;

          $course->total_exe_count = $course->total_exe_count + count($lesson->exercises);
        }

        $user_id = auth()->user()->id;

        if ($user_id)
        {
            $user_data = Userdata::findorNew($user_id);
        }else {
          $user_data = null;
        }

        if ($user_data)
        {
          $current_exercise = Exercise::where('id', $user_data->current_exercise)->first();

          if ($user_data->purchased_courses){
            $user_data_array = explode(',', $user_data->purchased_courses);
            if (in_array($course->id, $user_data_array)){
              $course->is_purchased = true;
            }
          }

          foreach ($lessons as $lesson){
            $lesson->status = 'A';

            foreach($lesson->exercises as $exe){
              if ($user_data->done_exercises){
                  $done_exercises = explode(',', $user_data->done_exercises);
                  if (in_array($exe->id, $done_exercises)){
                    $lesson->done_exe_count++;
                  }
              }
            }

            $course->done_exe_count = $course->done_exe_count + $lesson->done_exe_count;
            if ($lesson->done_exe_count == $lesson->exe_count){
              $lesson->status = 'C';
            }else if ($lesson->done_exe_count > 0 && $lesson->done_exe_count < $lesson->exe_count){
              $lesson->status = 'B';
            }
          }

          if ($user_data->done_exercises){
            $done_exercises = explode(',', $user_data->done_exercises);
            for ($i = count($done_exercises)-1; $i >= 0; $i=$i-1){
              $exe = Exercise::where('id', $done_exercises[$i])->first();
              if ($exe->lesson->course_id = $course->id){
                $course->last_exercise = $exe->id;
                foreach($course->levels as $course_level){
                  if ($course_level->id == $exe->lesson->level->id){
                    $course->last_level = $exe->lesson->level->title;
                  }
                }
                Log::info('here');
                Log::info($course->last_exercise);
                Log::info('start');
                break;
              }
            }
          }

        }
        else
        {
          $current_exercise = null;
        }

        $levels = $course->levels;

        foreach($levels as $level){
          $course_lessons = [];

          foreach($lessons as $lesson){
            if ($level->id == $lesson->level_id){
              array_push($course_lessons, $lesson);
            }
          }
          $level->course_lessons = $course_lessons;
        }

        $last_exercises = Exercise::orderBy('created_at', 'desc')->get();
        $latest_exercises = [];

        $exe_count = 0;
        foreach($last_exercises as $exe){
          $exe_lesson = $exe->lesson;
          if ($exe_lesson){
            if ($exe_lesson->course_id == $course->id){
              array_push($latest_exercises, $exe);
              $exe_count++;
              if ($exe_count > 10){
                break;
              }
            }
          }
        }
        //$course->total_exe_count = 0;
        //$course->done_exe_count = 0;
        $course->percent_done = 0;

        if ($course->total_exe_count > 0){
          $course->percent_done = intval($course->done_exe_count * 100 / $course->total_exe_count);
        }

        return view('course', [
            'course' => $course,
            'levels' => $levels,
            'lessons' => $lessons,
            'latest_exercises' => $latest_exercises,
            'current_exercise' => $current_exercise,
            'tab' => $tab,
        ]);
    }
}
