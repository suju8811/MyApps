<?php

namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;
use App\Word;

class Lesson extends Model
{
	protected $fillable = ['title', 'description', 'image', 'is_trial', 'course_id', 'level_id', 'is_public', 'is_free', 'created_by', 'updated_by'];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function level()
    {
        return $this->belongsTo('App\Level');
    }

    public function quizzes()
    {
        return $this->hasMany('App\Quiz');
    }

    public function exercises()
    {
        return $this->hasMany('App\Exercise');
    }
}
