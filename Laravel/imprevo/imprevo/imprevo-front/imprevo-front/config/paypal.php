<?php

return [
    // set your paypal credential
    'client_id' => env('PAYPAL_CLIENT_ID','AfV1gOzUGEsVQ5HE_OC87c9_lkBMEUk27R8yJ8WNcuHYDNFmvzIN3fDU_WYCohw7PJ-1pXuOrd-gAZlm'),
    'secret' => env('PAYPAL_SECRET','EMyBx2KFCP9SOiQHcMe7gdKGPbWL3USttCOFOY1mgC6FrWReJDq8qRn_9Ms2JOSudtjqWPMcOxuQlbIL'),

    /**
    * SDK configuration
    */
    'settings' => array(
    /**
    * Available option 'sandbox' or 'live'
    */
    'mode' => env('PAYPAL_MODE', 'live'),

    /**
    * Specify the max request time in seconds
    */
    'http.ConnectionTimeOut' => 30,

    /**
    * Whether want to log to a file
    */
    'log.LogEnabled' => true,

    /**
    * Specify the file that want to write on
    */
    'log.FileName' => storage_path() . '/logs/paypal.log',

    /**
    * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
    * Logging is most verbose in the 'FINE' level and decreases as you
    * proceed towards ERROR
    */
    'log.LogLevel' => 'FINE'
    ),
];
