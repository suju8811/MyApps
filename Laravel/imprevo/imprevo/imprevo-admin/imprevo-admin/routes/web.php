<?php
use App\Cat;
use App\Word;
use App\Code;
use App\Exercise;
use Illuminate\Support\Facades\Input;

//$languages = ['en', 'zh'];
$relPath = env('RELATIVE_URL', '');
Config::set('RELATIVE_URL', $relPath);
$frontPath = env('FRONT_URL', '');
Config::set('FRONT_URL', $frontPath);
//Session::put('relPath', $relPath);

$locale = Code::where("code_name", "lang")->first()->code_value;
App::setLocale($locale);

$title = "IMPREVO";
$titleObj = Code::where("code_name", "title")->first();
if($titleObj)
    $title = $titleObj->code_value;

Config::set('SITE_TITLE', $title);

//Route::get('/', ['as' => 'admin.login', 'uses' => 'Auth\AuthController@getLogin']);
//Route::get('/signin', ['as' => 'admin.login', 'uses' => 'Auth\AuthController@getLogin']);
//Route::post('/signin', ['as' => 'admin.login.post', 'uses' => 'Auth\AuthController@postLogin']);
//Route::get('/signout', ['as' => 'admin.logout', 'uses' => 'Auth\AuthController@getLogout']);
// Registration Routes...
//Route::get('/register', ['as' => 'admin.register', 'uses' => 'Auth\AuthController@getRegister']);
//Route::post('/register', ['as' => 'admin.register.post', 'uses' => 'Auth\AuthController@postRegister']);

//Route::get('forgot-password', array('as'   => 'admin.forgot.password',
//    'uses' => 'Auth\AuthController@getForgotPassword'));
//Route::post('forgot-password', array('as'   => 'admin.forgot.password.post',
//    'uses' => 'Auth\AuthController@postForgotPassword'));

Route::get ( '/', ['as' => 'admin.courses', 'uses' => 'CourseController@courses']);
Route::get ( '/home', ['as' => 'admin.courses', 'uses' => 'CourseController@courses']);
Route::get ( '/exercise/{id}', function ($id) {
    $exercise = Exercise::findOrNew($id);
    $codeList = Code::all();
    $settings = [];
    for($i = 0; $i < count($codeList); $i++) {
        $settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
    }

    $list = explode(',', $exercise->category);

    return view ( 'exercise', [
        'cats' => Cat::leftJoin ( 'words', 'cats.id', '=', 'words.categoryId' )->whereIn('cats.id', $list)->select ( 'cats.*', DB::raw ( 'count(words.categoryId) as count' ) )->groupBy ( 'cats.id' )->get (),
        'exercise' => $exercise,
        'settings' => $settings
    ] );
} );

Route::get ( '/getWords', 'WordController@getWords' );

//words
Route::get('/words', ['as' => 'admin.words', 'uses' => 'WordController@index']);
Route::post('/word', ['as' => 'admin.word.post', 'uses' => 'WordController@create']);
Route::get('/words/{id}', ['as' => 'admin.words.edit', 'uses' => 'WordController@edit']);
Route::post('/words/{id}', ['as' => 'admin.words.edit.post', 'uses' => 'WordController@postEdit']);
Route::delete('/words/{id}', ['as' => 'admin.word.delete', 'uses' => 'WordController@destroy']);

//categories
Route::get('/cats', ['as' => 'admin.categories', 'uses' => 'CategoryController@index']);
Route::post('/cat', ['as' => 'admin.category.post', 'uses' => 'CategoryController@create']);
Route::get('/cats/{id}', ['as' => 'admin.categories.edit', 'uses' => 'CategoryController@edit']);
Route::post('/cats/{id}', ['as' => 'admin.categories.edit.post', 'uses' => 'CategoryController@postEdit']);
Route::delete('/cats/{id}', ['as' => 'admin.category.delete', 'uses' => 'CategoryController@destroy']);

//Levels
Route::get('/levels', ['as' => 'admin.levels', 'uses' => 'LevelController@index']);
Route::post('/level', ['as' => 'admin.level.post', 'uses' => 'LevelController@create']);
Route::get('/levels/{id}', ['as' => 'admin.level.edit', 'uses' => 'LevelController@edit']);
Route::post('/levels/{id}', ['as' => 'admin.levels.edit.post', 'uses' => 'LevelController@postEdit']);
Route::delete('/levels/{id}', ['as' => 'admin.levels.delete', 'uses' => 'LevelController@destroy']);

//Modules
Route::get('/modules', ['as' => 'admin.modules', 'uses' => 'ModuleController@index']);
Route::post('/module', ['as' => 'admin.module.post', 'uses' => 'ModuleController@create']);
Route::get('/modules/{id}', ['as' => 'admin.module.edit', 'uses' => 'ModuleController@edit']);
Route::post('/modules/{id}', ['as' => 'admin.modules.edit.post', 'uses' => 'ModuleController@postEdit']);
Route::delete('/modules/{id}', ['as' => 'admin.modules.delete', 'uses' => 'ModuleController@destroy']);

//exercises
Route::get('/exercises', ['as' => 'admin.exercises', 'uses' => 'ExerciseController@index']);
Route::post('/exercise', ['as' => 'admin.exercise.post', 'uses' => 'ExerciseController@create']);
Route::get('/exercises/{id}/edit', ['as' => 'admin.exercises.edit', 'uses' => 'ExerciseController@edit']);
Route::post('/exercises/{id}/edit', ['as' => 'admin.exercises.edit.post', 'uses' => 'ExerciseController@postEdit']);
Route::delete('/exercises/{id}', ['as' => 'admin.exercise.delete', 'uses' => 'ExerciseController@destroy']);

//Users
Route::get('/users/new', ['as' => 'admin.user.new', 'uses' => 'UserController@newUser']);
Route::get('/users', ['as' => 'admin.users', 'uses' => 'UserController@users']);
Route::get('/users/{id}', ['as' => 'admin.userEdit', 'uses' => 'UserController@editUser']);
Route::delete('/users/{id}', ['uses' => 'UserController@destroy']);
Route::post('/user', ['as' => 'admin.users', 'uses' => 'UserController@postEdit']);

//Course
Route::get('/courses/new', ['as' => 'admin.courses.new', 'uses' => 'CourseController@newCourse']);
Route::get('/courses', ['as' => 'admin.courses', 'uses' => 'CourseController@courses']);
Route::get('/courses/{id}', ['as' => 'admin.courseEdit', 'uses' => 'CourseController@editCourse']);
Route::delete('/courses/{id}', ['uses' => 'CourseController@destroy']);
Route::post('/course', ['uses' => 'CourseController@postEdit']);

//Lesson
Route::get('/lessons/new', ['as' => 'admin.lessons.new', 'uses' => 'LessonController@newLesson']);
Route::get('/lessons', ['as' => 'admin.lessons', 'uses' => 'LessonController@lessons']);
Route::get('/lessons/{id}', ['as' => 'admin.lessonEdit', 'uses' => 'LessonController@editLesson']);
Route::delete('/lessons/{id}', ['uses' => 'LessonController@destroy']);
Route::post('/lesson', ['uses' => 'LessonController@postEdit']);

Route::get('/getLevelsByCourse', ['uses' => 'LessonController@getLevelsByCourse']);

//Quiz
Route::get('/quizzes/new', ['uses' => 'QuizController@create']);
Route::get('/quizzes', ['uses' => 'QuizController@index']);
Route::get('/quizzes/{id}', ['uses' => 'QuizController@edit']);
Route::post('/quizzes/duplicate', ['uses' => 'QuizController@postDuplicate']);

Route::delete('/quizzes/{id}', ['uses' => 'QuizController@destroy']);
Route::post('/quiz', ['uses' => 'QuizController@postEdit']);


//Question
Route::get('/questions/new/{currentType}', ['uses' => 'QuestionController@create']);
Route::get('/questions/{id}', ['uses' => 'QuestionController@edit']);
Route::delete('/questions/{id}', ['uses' => 'QuestionController@destroy']);
Route::post('/question', ['uses' => 'QuestionController@postEdit']);

//Exercise
Route::post('/exercise', ['uses' => 'ExerciseController@postEdit']);
Route::post('/exercise/{id}', ['uses' => 'ExerciseController@postEdit']);
Route::delete('/exercise/{id}', ['uses' => 'ExerciseController@destroy']);

//setting
Route::get('/setting', ['as' => 'admin.setting', 'uses' => 'SettingController@setting']);
Route::post('/setting', ['as' => 'admin.setting.post', 'uses' => 'SettingController@postSetting']);
Route::get('/profile', ['as' => 'admin.profile', 'uses' => 'ProfileController@profile']);
Route::post('/profile', ['as' => 'admin.profile.post', 'uses' => 'ProfileController@postProfile']);

Auth::routes();

Route::get('/signin', 'Auth\AuthController@signin')->name('signin');
Route::get('/signout', 'Auth\AuthController@signout')->name('signout');
Route::get('/register', 'Auth\AuthController@register')->name('register');
