<?php
/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/
?>
@extends('layouts.back')

@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>
                Word Database
            </h2>
        </header>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Add new word</h2>
                    </header>
                    <div class="panel-body">
                        <!-- Add New Words -->
                        @include('common.errors')
                        <form id="wordForm" role="form" class="form-horizontal form-bordered" action="{{ Config::get('RELATIVE_URL') }}/word" method="post" encType="multipart/form-data">
                            <div class="form-group">
                                <label class="col-md-3 control-label label-left" for="sourceWord">Source word</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="sourceWord" name="sourceWord" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label label-left" for="translation">Translation</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="translation" name="translation" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label label-left" for="uploadImage">Upload image</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control" id="uploadImage" name="uploadImage">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label label-left" for="copyrightUrl">Copyright URL</label>
                                <div class="col-md-6">
                                    <input type="url" class="form-control" id="copyrightUrl" name="copyrightUrl">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label label-left" for="uploadAudio">Upload audio</label>
                                <div class="col-md-6">
                                    <input type="file" id="uploadAudio" class="form-control" name="uploadAudio">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label label-left" for="categoryId">Select category</label>
                                <div class="col-md-6">
                                    <select name="categoryId" id="categoryId" class="form-control mb-md">
                                        @foreach ($categories as $category)
                                            <option value="{{$category->id}}">{{$category->category}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label label-left" for="note">Note</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="note" name="note">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label label-left" for="example">Example sentense</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="example" name="example">
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary" style="width:120px">{!! trans('flashcard.save') !!}</button>
                            </div>
                        </form>
                        <!-- END Add New Words -->
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Submitted words</h2>
                    </header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-none">
                                <thead><tr>
                                    <th>Id</th>
                                    <th>Source word</th>
                                    <th>Translation</th>
                                    <th>Image</th>
                                    <th>Copyright URL</th>
                                    <th>Audio</th>
                                    <th>Category</th>
                                    <th>Note</th>
                                    <th>Example sentence</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($words as $key=>$word)
                                    <tr>
                                        <td>{{ $word->id }}</td>
                                        <td>{{ $word->source_word }}</td>
                                        <td>{{ $word->translation }}</td>
                                        <td>
                                            @if ($word->image == '')
                                                <a style="color:#00a65a;"><i class="fa fa-close"></i></a>
                                            @else
                                                <a style="color:#00a65a;"><i class="fa fa-check"></i></a>
                                            @endif
                                        </td>
                                        <td>{{ $word->copyright_url }}</td>
                                        <td>
                                            @if ($word->audio == '')
                                                <a style="color:#00a65a;"><i class="fa fa-close"></i></a>
                                            @else
                                                <a style="color:#00a65a;"><i class="fa fa-check"></i></a>
                                            @endif
                                        </td>
                                        <td>{{ $word->category }}</td>
                                        <td>{{ $word->note }}</td>
                                        <td>{{ $word->example }}</td>
                                        <!-- Task Delete Button -->
                                        <td class="actions-hover actions-fade">
                                            <a href="{{ Config::get('RELATIVE_URL') }}/words/{{ $word->id }}"><i class="fa fa-pencil"></i></a>
                                            <a href="#" class="delete-row" onclick="removeWord({{ $word->id }}, event)"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody></table>
                            {{ $words->links() }}
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
    <script>
        function removeWord(id, e) {
            res = confirm("Do you really want to delete this item?");
            if (res){
            console.log(e);
            $.ajax({
                url:'/words/' + id,
                type:'delete'
            }).then(function(ret){
                console.log(ret);
                $(e.target).parents("tr").remove();
            }, function(err){
                console.log(err);
            })
            }
        }
    </script>
@endsection
