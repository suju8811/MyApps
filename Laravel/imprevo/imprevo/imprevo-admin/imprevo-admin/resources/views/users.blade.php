<?php
/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/
?>
@extends('layouts.back')

@section('content')
        <section role="main" class="content-body">
            @php
                function getUserGroupName($id, $userGroups) {
                    foreach($userGroups as $user) {
                        if($user->id == $id) {
                            return $user->name;
                        }
                    }

                    return "-";
                }
                function getCourseName($id, $courses) {
                    foreach($courses as $course) {
                        if($course->id == $id) {
                            return $course->name;
                        }
                    }

                    return "-";
                }
            @endphp
            <header class="page-header">
                <h2>User management</h2>
            </header>
            <div class="panel-body" id="pageDocument">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mb-md">
                            <a href="/users/new" id="addToTable" class="btn btn-primary">Add <i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email address</th>
                        <th>Usergroup</th>
                        <th>Purchased courses</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $user)
                        <tr id="{{$user->id}}">
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{getUserGroupName($user->permission, $userGroups)}}</td>
                            <td>{{getCourseName($user->course, $courses)}}</td>
                            <td class="actions">
                                <a href="/users/{{$user->id}}" class="on-default edit-row"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="on-default remove-row" onclick="removeUser({{$user->id}})"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $users->links() }}
            </div>
        </section>
    <script>

        function removeUser(id) {
          res = confirm("Do you really want to delete this item?");
          if (res){
            $.ajax({
              url:'/users/' + id,
              type:'delete'
            }).then(function(ret){
                console.log(ret);
                location.href = "{{$users->url($users->currentPage())}}"
            }, function(err){
                console.log(err);
            })
          }
        }

    $(function() {
    });
</script>

@endsection
