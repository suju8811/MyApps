<?php
/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/
?>

@extends('layouts.back')
@section('content')
	<section role="main" class="content-body">

		<header class="page-header">
			<h2>
				User management
			</h2>
		</header>
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						@if($user['id'])
							<h2 class="panel-title">Edit user</h2>
						@else
							<h2 class="panel-title">Add new user</h2>
						@endif
					</header>
					<div class="panel-body">
						@include('common.errors')
						<form id="form" role="form" class="form-horizontal form-bordered" action="{{ Config::get('RELATIVE_URL') }}/user" method="post">
							@if($user['id'])
								<input type="hidden" name="id" value="{{$user->id}}">
							@endif
							<div class="form-group">
								<label class="col-md-3 control-label label-left" for="email">Email address <span class="required">*</span></label>
								<div class="col-md-6">
									<input type="email" class="form-control" id="email" name="email" required value="{{$user['email']}}" @if($user['email'] != '') readonly @endif>
								</div>
							</div>
							@if(!$user['id'])
							<div class="form-group">
								<label class="col-md-3 control-label label-left" for="password">Password<span class="required">*</span></label>
								<div class="col-md-6">
									<input type="password" class="form-control" id="password" name="password" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label label-left" for="passwordConfirm">Confirm password<span class="required">*</span></label>
								<div class="col-md-6">
									<input type="password" class="form-control" id="passwordConfirm" name="passwordConfirm" required>
								</div>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-3 control-label label-left" for="name">Name</label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="name" name="name" value="{{$user['name']}}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label label-left" for="permission">User group</label>
								<div class="col-md-6">
									<select name="permission" id="permission" class="form-control mb-md">
										@foreach ($userGroups as $group)
											<option value={{$group->id}} @if($user['permission']==$group->id) selected @endif>{{$group->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label label-left" for="course" >Assign course to this user</label>
								<div class="col-md-6">
									<select name="course" id="course" class="form-control mb-md" value="{{$user['course']}}">
										<option value="">Select a course</option>
										@foreach ($courses as $course)
											<option value="{{$course->id}}">{{$course->title}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div>
								<button type="submit" class="btn btn-primary" style="width:120px">{!! trans('flashcard.save') !!}</button>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
	<script type="text/javascript">
		$(function(){
			$("#form").validate({
				rules: {
					password: "required",
					passwordConfirm: {
						equalTo: "#password"
					}
				},
				highlight: function( label ) {
					$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
				},
				success: function( label ) {
					$(label).closest('.form-group').removeClass('has-error');
					label.remove();
				},
				errorPlacement: function( error, element ) {
					var placement = element.closest('.input-group');
					if (!placement.get(0)) {
						placement = element;
					}
					if (error.text() !== '') {
						placement.after(error);
					}
				}
			});
		});
	</script>
@endsection