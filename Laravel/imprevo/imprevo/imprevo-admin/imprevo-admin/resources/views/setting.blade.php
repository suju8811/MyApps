<?php
/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/
?>

@extends('layouts.back')
@section('content')
	<section role="main" class="content-body">

		<header class="page-header">
		<h2>
			{!! trans('flashcard.generalSettings') !!}
		</h2>
		</header>
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						<h2 class="panel-title">General Settings</h2>
					</header>
					<div class="panel-body">
						@include('common.errors')
						<form id="settingForm" role="form" class="form-horizontal form-bordered" action="{{ Config::get('RELATIVE_URL') }}/setting" method="post">
							<div class="form-group">
								<label class="col-md-3 control-label label-left" for="siteTitle">Site Title<span class="required">*</span></label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="siteTitle" name="siteTitle" value="{{isset($settings['siteTitle'])? $settings["siteTitle"]:''}}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label label-left" for="siteDesc">Site Description</label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="siteDesc" name="siteDesc" value="{{isset($settings['siteDesc'])? $settings["siteDesc"]:''}}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label label-left" for="keywords">Site Keywords</label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="keywords" name="keywords" value="{{isset($settings['keywords'])? $settings["keywords"]:''}}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label label-left" for="siteEmail">Site Email<span class="required">*</span></label>
								<div class="col-md-6">
									<input type="email" class="form-control" id="siteEmail" name="siteEmail" value="{{isset($settings['siteEmail'])? $settings["siteEmail"]:''}}" required>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-3 control-label label-left" for="lang">Site Language<span class="required">*</span></label>
								<div class="col-md-6">
									<select name="lang" id="lang" class="form-control bfh-languages mb-md" data-language="{{App::getLocale()}}" data-available="{{$langStr}}" data-blank=false requried></select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label label-left" for="gaCode">Google Analytics Code</label>
								<div class="col-md-6">
									<textarea class="form-control" rows="5" data-plugin-textarea-autosize id="gaCode" name="gaCode">{{isset($settings['gaCode'])? $settings["gaCode"]:''}}</textarea>
								</div>
							</div>
							<div>
								<button type="submit" class="btn btn-primary" style="width:120px">{!! trans('flashcard.save') !!}</button>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
	<script type="text/javascript">
		$(function(){

			$("#settingForm").validate({
				highlight: function( label ) {
					$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
				},
				success: function( label ) {
					$(label).closest('.form-group').removeClass('has-error');
					label.remove();
				},
				errorPlacement: function( error, element ) {
					var placement = element.closest('.input-group');
					if (!placement.get(0)) {
						placement = element;
					}
					if (error.text() !== '') {
						placement.after(error);
					}
				}
			});
		});
	</script>
@endsection