<?php

namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['instruction', 'note', 'type', 'quiz_id', 'answer_data', 'correct_answer_id', 'created_by', 'updated_by'];

    public function quiz()
    {
        return $this->belongsTo('App\Quiz');
    }

 /*   public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }*/
}
