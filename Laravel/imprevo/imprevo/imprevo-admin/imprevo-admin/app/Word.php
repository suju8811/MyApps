<?php

namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    protected $fillable = ['source_word', 'translation', 'image', 'copyright_url', 'audio', 'category_id', 'note', 'example'];
}
