<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use App\Quiz;
use App\Exercise;
use App\Lesson;
use App\User;
use App\UserGroup;
use App\Course;
use App\Level;
use App\Module;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Illuminate\Support\Facades\Auth;

class QuizController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $quizzes = Quiz::paginate(15);

        return view('quizzes', [
            'quizzes' => $quizzes
        ]);
    }

    public function create()
    {
        return view('quizEdit', [
            'courses' => Course::all(),
            'quiz' => array('id'=>null, 'title'=>'', 'random_type'=>'', 'course_id'=>'','level_id'=>'', 'module_id'=>'', 'lesson_id'=>'')
        ]);
    }

    public function edit(Request $request, $id)
    {
        $quiz = Quiz::findOrNew($id);

        return view('quizEdit', [
            'quiz' => $quiz,
            'courses' => Course::all(),
        ]);
    }

    public function postEdit(Request $request)
    {
        if($request->input('id') != '') {
            $quiz = Quiz::findOrNew($request->input('id'));
            $quiz['title'] = $request->input('title');
            $quiz['random_type'] = $request->input('randomType');
            $quiz['level_id'] = $request->input('levelId');
            $quiz['course_id'] = $request->input('courseId');
            $quiz['module_id'] = $request->input('moduleId');
            $quiz['lesson_id'] = $request->input('lessonId');
            $quiz['updated_by'] = Auth::user()->id;
            $quiz['shortcode'] = '[quiz-id='.$request->input('id').']';
            $quiz->save();
        } else { //create
            $quiz = Quiz::create([
                'title' => $request->input('title'),
                'random_type' => $request->input('randomType'),
                'course_id' => $request->input('courseId'),
                'level_id' => $request->input('levelId'),
                'lesson_id' => $request->input('lessonId'),
                'module_id' => $request->input('moduleId'),
                'created_by' => Auth::user()->id,
            ]);

            $quiz['shortcode'] = '[quiz-id='.$quiz->id.']';
            $quiz->save();
        }

        return redirect('/quizzes');
    }

    public function postDuplicate(Request $request)
    {
        $quiz = Quiz::findOrNew($request->input('id'));
        $quizNew = $quiz->replicate();
        $last_quiz = Quiz::all()->last();
        $quizNew['id'] = $last_quiz['id'] + 1; // the new project_id
        $quizNew['shortcode'] = '[quiz-id='.$quizNew->id.']';
        $quizNew->save();
        return redirect('/quizzes');
    }

    public function destroy($id)
    {
    	$u = Quiz::findOrNew($id);
        $u->delete();
        $ret = array("result"=>"ok");
        return json_encode($ret);
    }
}
