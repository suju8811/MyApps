<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use App\Cat;
use App\Word;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Config;

class WordController extends Controller
{
    public function __construct()
    {
    }

    public function index(Request $request)
    {
    	$query = $request->input('query');
    	if($query == null)
    		$query = '';
    	
    	$words = Word::join('cats', 'words.category_id', '=', 'cats.id')->where('words.source_word', 'like', '%'.$query.'%')
    	->select('words.*', 'cats.category')->paginate(10);
    	$words->setPath(Config::get('RELATIVE_URL').'/words');
    
        return view('words', [
            'categories' => Cat::all(),
        	'words' => $words,
        	'query' => $query,
        ]);
    }
    
    public function edit($id)
    {
    	$word = Word::findOrNew($id);
        if($word->audio != '' && $word->audio != null) {
            $pos = strrpos($word->audio, "/");
            $word->audio = substr($word->audio, $pos + 1);
        }
        if($word->image != '' && $word->image != null) {
            $pos = strrpos($word->image, "/");
            $word->image = substr($word->image, $pos + 1);
        }
    	return view('wordEdit', [
    			'categories' => Cat::all(),
    			'word' => $word,
    	]);
    }
    
    public function postEdit(Request $request, $id)
    {
    	$word = Word::findOrNew($id);

        $attributes = Input::all();
        $audioFileUrl = null;
        $imageFileUrl = null;
        if (isset($attributes['uploadAudio'])) {

            $file = $attributes['uploadAudio'];

            // delete old image
            $destinationPath = public_path() . "/upload/audio";
            $fileName = time().'_'.$file->getClientOriginalName();
            $fileSize = $file->getClientSize();
            $upload_success = $file->move($destinationPath, $fileName);

            if ($upload_success) {
                $audioFileUrl = "/upload/audio/".$fileName;
                $word->audio = $audioFileUrl;
            }
        }
        if (isset($attributes['uploadImage'])) {

            $file = $attributes['uploadImage'];

            // delete old image
            $destinationPath = public_path() . "/upload/image";
            $fileName = time().'_'.$file->getClientOriginalName();
            $fileSize = $file->getClientSize();
            $upload_success = $file->move($destinationPath, $fileName);

            if ($upload_success) {
                $imageFileUrl = "/upload/image/".$fileName;
                $word->image = $imageFileUrl;
            }
        }

        $word->source_word = $request->input('sourceWord');
        $word->translation = $request->input('translation');
        $word->copyright_url = $request->input('copyrightUrl');
        $word->example = $request->input('example');
        $word->category_id = $request->input('categoryId');
        $word->note = $request->input('note');

    	$word->save();

    	return redirect('/words');
    }
    
    /**
     * Create a new category.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        /*$this->validate($request, [
            'wordDesc' => 'required|max:255',
			'categoryId' => 'required'
        ]);*/
        
        //save audio file
        $attributes = Input::all();
        $audioFileUrl = null;
        $imageFileUrl = null;
        if (isset($attributes['uploadAudio'])) {

            $file = $attributes['uploadAudio'];

            // delete old image
            $destinationPath = public_path() . "/upload/audio";
            $fileName = time().'_'.$file->getClientOriginalName();
            $fileSize = $file->getClientSize();
            $upload_success = $file->move($destinationPath, $fileName);

            if ($upload_success) {
                $audioFileUrl = "/upload/audio/".$fileName;
            }
        }
        if (isset($attributes['uploadImage'])) {

            $file = $attributes['uploadImage'];

            // delete old image
            $destinationPath = public_path() . "/upload/image";
            $fileName = time().'_'.$file->getClientOriginalName();
            $fileSize = $file->getClientSize();
            $upload_success = $file->move($destinationPath, $fileName);

            if ($upload_success) {
                $imageFileUrl = "/upload/image/".$fileName;
            }
        }
        Word::create([
            'source_word' => $request->input('sourceWord'),
            'translation' => $request->input('translation'),
            'audio' => $audioFileUrl,
            'image' => $imageFileUrl,
            'copyright_url' => $request->input('copyrightUrl'),
        	'example' => $request->input('example'),
            'category_id' => $request->input('categoryId'),
            'note' => $request->input('note'),
        ]);

        return redirect('/words');
    }

    public function destroy($id)
    {
    	$word = Word::findOrNew($id);
        $word->delete();

        $ret = array("result"=>"ok");
        return json_encode($ret);
    }
    
    public function getWords(Request $request) 
    {
    	$categoryIds = $request->input('categoryId');
		$categoryIdList = explode(",", $categoryIds);
		$words = [];
		for($i = 0; $i < sizeof($categoryIdList); ++$i)
		{
			//$words = Word::where('categoryId', $categoryIdList[$i])->get();
			$list = Word::where('categoryId', $categoryIdList[$i])->get();
			for($j = 0 ; $j < sizeof($list) ; $j++) {
				$item = $list[$j];
				array_push($words,$item);
			}
		}
		return $words;
    }
}
