<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use App\Question;
use App\Answer;
use App\Lesson;
use App\User;
use App\UserGroup;
use App\Course;
use App\Level;
use App\Module;
use App\Word;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Illuminate\Support\Facades\Auth;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $questions = QuestionController::paginate(15);

        return view('questions', [
            'questions' => $questions
        ]);
    }

    public function create(Request $request, $currentType)
    {
        $value =  $request['quizid'];
        $words = Word::all();
        return view('questionEdit', [
            'question' => array('id'=>null, 'instruction'=>'', 'note'=>'', 'type'=>$currentType,'quiz_id'=>$value, 'answer_data'=>'', 'correct_answer_id'=>''),
            'words' => $words
        ]);
    }

    public function edit(Request $request, $id)
    {
        $question = Question::findOrNew($id);
        $words = Word::all();
        return view('questionEdit', [
            'question' => $question,
            'words' => $words
        ]);
    }

    public function postEdit(Request $request)
    {
        if($request->input('id') != '') {
            $question = Question::findOrNew($request->input('id'));
            $question['instruction'] = $request->input('instruction');
            $question['note'] = $request->input('note');
            $question['type'] = $request->input('type');
            $question['quiz_id'] = $request->input('quiz_id');
            $question['answer_data'] = $request->input('answer_data');
            $question['correct_answer_id'] = $request->input('correct_answer_id');
            $question->save();
        } else { //create
            $question = Question::create([
                'instruction' => $request->input('instruction'),
                'note' => $request->input('note'),
                'type' => $request->input('type'),
                'quiz_id' => $request->input('quiz_id'),
                'answer_data' => $request->input('answer_data'),
                'correct_answer_id' => $request->input('correct_answer_id'),
            ]);
            $question->save();
        }
        return redirect('/quizzes/'.$request->input('quiz_id'));
    }

    public function destroy($id)
    {
    	$u = Question::findOrNew($id);
        $u->delete();
        $ret = array("result"=>"ok");
        return json_encode($ret);
    }
}
