<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use App\Cat;
use App\User;
use App\UserGroup;
use App\Course;
use App\Level;
use App\Module;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Input;

function logConsole($name, $data = NULL, $jsEval = FALSE)
{
    if (! $name) return false;

    $isevaled = false;
    $type = ($data || gettype($data)) ? 'Type: ' . gettype($data) : '';

    if ($jsEval && (is_array($data) || is_object($data)))
    {
        $data = 'eval(' . preg_replace('#[\s\r\n\t\0\x0B]+#', '', json_encode($data)) . ')';
        $isevaled = true;
    }
    else
    {
        $data = json_encode($data);
    }

    # sanitalize
    $data = $data ? $data : '';
    $search_array = array("#'#", '#""#', "#''#", "#\n#", "#\r\n#");
    $replace_array = array('"', '', '', '\\n', '\\n');
    $data = preg_replace($search_array,  $replace_array, $data);
    $data = ltrim(rtrim($data, '"'), '"');
    $data = $isevaled ? $data : ($data[0] === "'") ? $data : "'" . $data . "'";

    $js = <<<JSCODE
\n<script>
 // fallback - to deal with IE (or browsers that don't have console)
 if (! window.console) console = {};
 console.log = console.log || function(name, data){};
 // end of fallback

 console.log('$name');
 console.log('------------------------------------------');
 console.log('$type');
 console.log($data);
 console.log('\\n');
</script>
JSCODE;

    echo $js;
} # end logConsole
class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function courses(Request $request)
    {
        $courses = Course::paginate(15);

        return view('courses', [
            'courses' => $courses
        ]);
    }

    public function newCourse()
    {
        $levels = Level::all();
        $modules = Module::all();

        return view('courseEdit', [
            'levels' => Level::all(),
            'modules' => Module::all(),
            'course' => array('id'=>null, 'title'=>'', 'description'=>'', 'photo'=>'', 'is_public'=>0, 'is_free'=>0)
        ]);
    }

    public function editCourse(Request $request, $id)
    {
        $course = Course::findOrNew($id);
        $levels = Level::all();
        $modules = Module::all();
        return view('courseEdit', [
            'levels' => $levels,
            'modules' => $modules,
            'course' => Course::findOrNew($id)
        ]);
    }

    public function postEdit(Request $request)
    {
        if($request->input('id') != '') {
            $course = Course::findOrNew($request->input('id'));
            $imageFileUrl = null;
            $attributes = Input::all();
            if (isset($attributes['uploadPhoto'])) {

                $file = $attributes['uploadPhoto'];

                // delete old image
                $destinationPath = public_path() . "/upload/image";
                $fileName = time().'_'.$file->getClientOriginalName();
                $fileSize = $file->getClientSize();
                $upload_success = $file->move($destinationPath, $fileName);

                if ($upload_success) {
                    $imageFileUrl = "/upload/image/".$fileName;
                    $course['photo'] = $imageFileUrl;
                }
            }

            $course['title'] = $request->input('title');
            $course['description'] = $request->input('description');
            $course['is_public'] = $request->input('isPublic');
            $course['is_free'] = $request->input('isFree');

            $course->save();

            //levels
            $course->levels()->sync($request->input('levels'));
            $course->modules()->sync($request->input('modules'));
        } else { //create
            $attributes = Input::all();
            $imageFileUrl = null;
            if (isset($attributes['uploadPhoto'])) {

                $file = $attributes['uploadPhoto'];

                // delete old image
                $destinationPath = public_path() . "/upload/image";
                $fileName = time().'_'.$file->getClientOriginalName();
                $fileSize = $file->getClientSize();
                $upload_success = $file->move($destinationPath, $fileName);

                if ($upload_success) {
                    $imageFileUrl = "/upload/image/".$fileName;
                }
            }
            $course = Course::create([
                'title' => $request->input('title'),
                'photo' => $imageFileUrl,
                'description' => $request->input('description'),
                'is_public' => $request->input('isPublic'),
                'is_free' => $request->input('isFree'),
            ]);
            $course->save();
            $course->levels()->sync($request->input('levels'));
            $course->modules()->sync($request->input('modules'));
        }
        return redirect('/courses');
    }

    public function destroy($id)
    {
    	$u = Course::findOrNew($id);
        //$this->authorize('destroy', $category);
		//Cat::destroy([$category]);
        $u->delete();
        $ret = array("result"=>"ok");
        return json_encode($ret);
    }
}
