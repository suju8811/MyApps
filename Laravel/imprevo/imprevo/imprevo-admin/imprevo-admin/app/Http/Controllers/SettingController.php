<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use App\User;
use App\Code;
use Validator;
use App\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use Redirect;
use Input;
use Reminder;
use Mail;
use Session;
use File;

class SettingController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function setting()
    {
     	//get directory listing
    	$basePath = base_path();
    	$list = File::directories($basePath.'/resources/lang');
    	$langStr = "";
    	for($i = 0; $i < count($list); $i++) {
    		$langStr = $langStr.substr($list[$i], strrpos($list[$i], '/lang') + 6).',';
    	}
    	
    	if(strlen($langStr) > 0) {
    		$langStr = substr($langStr, 0, strlen($langStr) - 1);
    	}

		$codeList = Code::all();
		$settings = [];
		for($i = 0; $i < count($codeList); $i++) {
			$settings[$codeList[$i]["code_name"]] = $codeList[$i]["code_value"];
		}
    	
    	return view('setting', ['langStr' =>$langStr, 'settings'=>$settings]);
    }

    public function postSetting(Request $request)
    {
        $code = Code::firstOrCreate(["code_name" => "lang"]);
        $code->code_value = $request->get('lang');
        $code->save();

        $code = Code::firstOrCreate(["code_name" => "siteTitle"]);
        $code->code_value = $request->get('siteTitle');
        $code->save();

        $code = Code::firstOrCreate(["code_name" => "siteDesc"]);
        $code->code_value = $request->get('siteDesc');
        $code->save();

        $code = Code::firstOrCreate(["code_name" => "keywords"]);
        $code->code_value = $request->get('keywords');
        $code->save();

        $code = Code::firstOrCreate(["code_name" => "siteEmail"]);
        $code->code_value = $request->get('siteEmail');
        $code->save();

        $code = Code::firstOrCreate(["code_name" => "gaCode"]);
        $code->code_value = $request->get('gaCode');
        $code->save();

        return Redirect::route('admin.setting');
    }
}
