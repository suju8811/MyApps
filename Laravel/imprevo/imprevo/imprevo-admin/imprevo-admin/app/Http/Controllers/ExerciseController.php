<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use App\Cat;
use App\Exercise;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use Input;
use Illuminate\Support\Facades\URL;

class ExerciseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('exercises', [
            'cats' => Cat::all(),
            'exercises' => Exercise::all(),
        ]);
    }

    public function edit($id)
    {
    	$exercise = Exercise::findOrNew($id);
    	return view('exerciseEdit', [
    			'cats' => Cat::all(),
    			'exercise' => $exercise,
    	]);
    }

    /**
     * Create/Update
     *
     * @param  Request  $request
     * @return Response
     */
    public function postEdit(Request $request)
    {
        $attributes = Input::all();
        $audioFileUrl = null;
        $type = $request->input('type');
        
        if($type == 'quiz') {
            if (isset($attributes['quizUploadAudio'])) {

                $file = $attributes['quizUploadAudio'];

                // delete old image
                $destinationPath = public_path() . "/upload/audio";
                $fileName = time().'_'.$file->getClientOriginalName();
                $fileSize = $file->getClientSize();
                $upload_success = $file->move($destinationPath, $fileName);

                if ($upload_success) {
                    $audioFileUrl = "/upload/audio/".$fileName;
                }
            }
        } else if($type == 'translation') {
            if (isset($attributes['uploadAudio'])) {

                $file = $attributes['uploadAudio'];

                // delete old image
                $destinationPath = public_path() . "/upload/audio";
                $fileName = time().'_'.$file->getClientOriginalName();
                $fileSize = $file->getClientSize();
                $upload_success = $file->move($destinationPath, $fileName);

                if ($upload_success) {
                    $audioFileUrl = "/upload/audio/".$fileName;
                }
            }
        }

        $exercise = [];
        if($request->input('id') == '') {
            if($type == 'video') {
                $exercise = Exercise::create([
                    'title' => $request->input('title'),
                    'description' => $request->input('description'),
                    'module_id' => $request->input('moduleId'),
                    'type' => $type,
                    'lesson_id' => $request->input('lessonId'),
                    'video_url' => $request->input('videoUrl'),
                    'created_by' => Auth::user()->id
                ]);
            } else if($type == 'text') {
                $exercise = Exercise::create([
                    'title' => $request->input('title'),
                    'description' => $request->input('description'),
                    'module_id' => $request->input('moduleId'),
                    'type' => $type,
                    'lesson_id' => $request->input('lessonId'),
                    'content1' => $request->input('content'),
                    'created_by' => Auth::user()->id
                ]);
            } else if($type == 'translation') {
                $exercise = Exercise::create([
                    'title' => $request->input('title'),
                    'description' => $request->input('description'),
                    'module_id' => $request->input('moduleId'),
                    'type' => $type,
                    'lesson_id' => $request->input('lessonId'),
                    'audio' => $audioFileUrl,
                    'content1' => $request->input('firstColumn'),
                    'content2' => $request->input('secondColumn'),
                    'created_by' => Auth::user()->id
                ]);
            } else if($type == 'quiz') {

  /*              $quizName = $request->input('quizId');
                $quizzes = Quiz::all();
                $quizid = 0;
                foreach ($quizzes as $quiz)
                {
                    if ($quiz['title'] == $quizName)
                    {
                        $quizid = $quiz['id'];
                    }
                }*/

                $exercise = Exercise::create([
                    'title' => $request->input('title'),
                    'description' => $request->input('description'),
                    'module_id' => $request->input('moduleId'),
                    'type' => $type,
                    'lesson_id' => $request->input('lessonId'),
                    'audio' => $audioFileUrl,
                    'content1' => $request->input('example'),
                    'content2' => $request->input('quizText'),
                    'quiz_id' => $request->input('quizId'),
                    'created_by' => Auth::user()->id
                ]);
            }
        } else {
            $exercise = Exercise::findOrNew($request->input('id'));
            $exercise->title = $request->input('title');
            $exercise->description = $request->input('description');
            $exercise->module_id = $request->input('moduleId');
            $exercise->type = $type;

            if($type == 'video') {
                $exercise->video_url = $request->input('videoUrl');
            } else if($type == 'text') {
                $exercise->content1 = $request->input('content');
            } else if($type == 'translation') {
                $exercise->content1 = $request->input('firstColumn');
                $exercise->content2 = $request->input('secondColumn');
                if($audioFileUrl != null) {
                    $exercise->audio = $audioFileUrl;
                }
            } else if($type == 'quiz') {
                $exercise->content1 = $request->input('example');
                $exercise->content2 = $request->input('quizText');
                $exercise->quiz_id = $request->input('quizId');
                if($audioFileUrl != null) {
                    $exercise->audio = $audioFileUrl;
                }
            }

            $exercise->save();
        }



        return json_encode(Exercise::find($exercise->id));
    }

    public function destroy($id)
    {
        $u = Exercise::findOrNew($id);
        $u->delete();
        $ret = array("result"=>"ok");
        return json_encode($ret);
    }
}
