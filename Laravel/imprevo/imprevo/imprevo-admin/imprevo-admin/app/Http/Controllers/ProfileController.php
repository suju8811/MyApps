<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use App\User;
use App\Code;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use Redirect;
use Input;
use Reminder;
use Mail;
use Session;
use File;

class ProfileController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile()
    {
    	return view('profile');
    }    
    
    public function postProfile(Request $request)
    {
    	$user = Auth::user();

    	 $credentials = array(
            'username'    => $user->username,
            'password' => $request->get('currentPassword')
        );
    	 
    	 $result = Auth::authenticate($credentials);
    	 if($result) {
    	 	if($request->get('password') != "") {
    	 		$user->password = $request->get('password');
    	 	} else {
    	 		$user->password = $request->get('currentPassword');
    	 	}
    	 	
    	 	if($request->get('email') != "") {
    	 		$user->email = $request->get('email');
    	 	}
    	 	
    	 	if($request->get('username') != "") {
    	 		$user->username = $request->get('username');
    	 	} 
    	 	
    	 	$credentials = array(
    	 			'username'    => $user->username,
    	 			'password' => $user->password
    	 	);
    	 	
    	 	//save photo file
    	 	$attributes = Input::all();
    	 	$fileUrl = null;
    	 	if (isset($attributes['photoImageFile'])) {
    	 		 
    	 		$file = $attributes['photoImageFile'];
    	 		 
    	 		// delete old image
    	 		$destinationPath = public_path() . "/upload/profile";
    	 		$fileName = time().'_'.$file->getClientOriginalName();
    	 		$fileSize = $file->getClientSize();
    	 		$upload_success = $file->move($destinationPath, $fileName);
    	 		 
    	 		if ($upload_success) {
    	 			$fileUrl = "/upload/profile/".$fileName;
    	 			$user->photo = $fileUrl;
    	 		}
    	 	}
    	 	
    	 	Sentinel::update($user, $credentials);    	 	
    	 	Sentinel::authenticate($credentials);
    	 } else {
    	 	//error
    	 	return Redirect::back()->withErrors("Current password does not matched.");
    	 }

    	return Redirect::route('admin.logout');
    }
}
