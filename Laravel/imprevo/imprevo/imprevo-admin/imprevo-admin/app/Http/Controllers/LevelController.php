<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use App\Level;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LevelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('levels', [
            'levels' => Level::all(),
        ]);
    }
    
    public function edit($id)
    {
    	return view('levelEdit', [
    			'level' => Level::findOrNew($id),
    	]);
    }
    
    public function postEdit(Request $request, $id)
    {
    	$level = Level::findOrNew($id);
        $level->title = $request->input("title");
        $level->save();//->updateOrCreate(['level'=>$request->input("level")]);

    	return redirect('/levels');
    }
    
    /**
     * Create a new level.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
        ]);

        Level::create([
            'title' => $request->get('title'),
        ]);

        return redirect('/levels');
    }

    public function destroy($id)
    {
    	$level = Level::findOrNew($id);
        //$this->authorize('destroy', $level);
		//Cat::destroy([$level]);
        $level->delete();

        $ret = array("result"=>"ok");
        return json_encode($ret);
    }
}
