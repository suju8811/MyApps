<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use App\Cat;
use App\User;
use App\UserGroup;
use App\Course;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function users(Request $request)
    {
        $users = DB::table('users')->paginate(15);
        return view('users', [
            'userGroups' => UserGroup::all(),
            'courses' => Course::all(),
            'users' => $users
        ]);
    }

    public function newUser()
    {
        return view('userEdit', [
            'userGroups' => UserGroup::all(),
            'courses' => Course::all(),
            'user' => array('id'=>null, 'name'=>'', 'email'=>'', 'permission'=>0, 'course'=>null)
        ]);
    }

    public function editUser(Request $request, $id)
    {
        return view('userEdit', [
            'userGroups' => UserGroup::all(),
            'courses' => Course::all(),
            'user' => User::findOrNew($id)
        ]);
    }

    public function postEdit(Request $request)
    {
        if($request->input('id') != '') {
            $user = User::findOrNew($request->input('id'));
            if(!$request->input('name')) {
                $pos = stripos($request->input('email'),"@");
                $userName =  substr($request->input('email'), 0, $pos);
            } else {
                $userName = $request->input('name');
            }

            $user->name = $userName;
            $user->permission = intval($request->input('permission'));
            $user->course = $request->input('course')?intval($request->input('course')):null;

            $user->save();
        } else {
            $exists = User::where('email', $request->input('email'))->get();
            if(sizeof($exists) > 0) {
                return Redirect::back()->withErrors("This email already used.");
            }
            $userName = "";

            if(!$request->input('name')) {
                $pos = stripos($request->input('email'),"@");
                $userName =  substr($request->input('email'), 0, $pos);
            } else {
                $userName = $request->input('name');
            }

            User::create([
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'name' => $userName,
                'permission' => intval($request->input('permission')),
                'course' => $request->input('course')?intval($request->input('course')):null
            ]);
        }


        return redirect('/users');
    }

    public function destroy($id)
    {
    	$u = User::findOrNew($id);
        //$this->authorize('destroy', $category);
		//Cat::destroy([$category]);
        $u->delete();
        $ret = array("result"=>"ok");
        return json_encode($ret);
    }
}
