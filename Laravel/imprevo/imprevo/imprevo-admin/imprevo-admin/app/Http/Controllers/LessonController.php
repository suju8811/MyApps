<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use App\Cat;
use App\Exercise;
use App\Lesson;
use App\User;
use App\UserGroup;
use App\Course;
use App\Level;
use App\Module;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Input;

class LessonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function lessons(Request $request)
    {
        $lessons = Lesson::paginate(15);

        return view('lessons', [
            'lessons' => $lessons
        ]);
    }

    public function newLesson()
    {
        return view('lessonEdit', [
            'courses' => Course::all(),
            'lesson' => array('id'=>null, 'title'=>'', 'description'=>'', 'image'=>'','course_id'=>'','level_id'=>'', 'is_trial'=>0, 'is_public'=>0, 'is_free'=>0)
        ]);
    }

    public function getLevelsByCourse(Request $request)
    {
        $courseId = $request->input('courseId');
        $levels = Level::where("course_id", $courseId)->get();
        return json_encode($levels);
    }

    public function editLesson(Request $request, $id)
    {
        $lesson = Lesson::findOrNew($id);

        return view('lessonEdit', [
            'lesson' => $lesson,
            'courses' => Course::all(),
        ]);
    }

    public function postEdit(Request $request)
    {
        if($request->input('id') != '') {
            $lesson = Lesson::findOrNew($request->input('id'));
            $imageFileUrl = null;
            $attributes = Input::all();
            if (isset($attributes['uploadPhoto'])) {

                $file = $attributes['uploadPhoto'];

                // delete old image
                $destinationPath = public_path() . "/upload/image";
                $fileName = time().'_'.$file->getClientOriginalName();
                $fileSize = $file->getClientSize();
                $upload_success = $file->move($destinationPath, $fileName);

                if ($upload_success) {
                    $imageFileUrl = "/upload/image/".$fileName;
                    $lesson['image'] = $imageFileUrl;
                }
            }

            $lesson['title'] = $request->input('title');
            $lesson['description'] = $request->input('description');
            $lesson['is_public'] = $request->input('isPublic');
            $lesson['is_free'] = $request->input('isFree');
            $lesson['is_trial'] = $request->input('isTrial');
            $lesson['level_id'] = $request->input('levelId');
            $lesson['course_id'] = $request->input('courseId');
            $lesson->save();
        } else { //create
            $attributes = Input::all();
            $imageFileUrl = null;
            if (isset($attributes['uploadPhoto'])) {

                $file = $attributes['uploadPhoto'];

                // delete old image
                $destinationPath = public_path() . "/upload/image";
                $fileName = time().'_'.$file->getClientOriginalName();
                $fileSize = $file->getClientSize();
                $upload_success = $file->move($destinationPath, $fileName);

                if ($upload_success) {
                    $imageFileUrl = "/upload/image/".$fileName;
                }
            }

            $lesson = Lesson::create([
                'title' => $request->input('title'),
                'image' => $imageFileUrl,
                'description' => $request->input('description'),
                'is_public' => $request->input('isPublic'),
                'is_free' => $request->input('isFree'),
                'is_trial' => $request->input('isTrial'),
                'course_id' => $request->input('courseId'),
                'level_id' => $request->input('levelId'),
            ]);
        }


        return redirect('/lessons');
    }

    public function destroy($id)
    {
    	$u = Lesson::findOrNew($id);
        //$this->authorize('destroy', $category);
		//Cat::destroy([$category]);
        $u->delete();
        $ret = array("result"=>"ok");
        return json_encode($ret);
    }
}
