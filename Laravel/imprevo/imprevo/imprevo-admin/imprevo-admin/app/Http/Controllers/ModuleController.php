<?php

namespace App\Http\Controllers;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use App\Module;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModuleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('modules', [
            'modules' => Module::all(),
        ]);
    }
    
    public function edit($id)
    {
    	return view('moduleEdit', [
    			'module' => Module::findOrNew($id),
    	]);
    }
    
    public function postEdit(Request $request, $id)
    {
    	$module = Module::findOrNew($id);
        $module->title = $request->input("title");
        $module->save();//->updateOrCreate(['module'=>$request->input("module")]);

    	return redirect('/modules');
    }
    
    /**
     * Create a new module.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
        ]);

        Module::create([
            'title' => $request->get('title'),
        ]);

        return redirect('/modules');
    }

    public function destroy($id)
    {
    	$module = Module::findOrNew($id);
        //$this->authorize('destroy', $module);
		//Cat::destroy([$module]);
        $module->delete();

        $ret = array("result"=>"ok");
        return json_encode($ret);
    }
}
