<?php
namespace App;

/******************************************************
 * IM - Vocabulary Builder
 * Version : 1.0.2
 * Copyright© 2016 Imprevo Ltd. All Rights Reversed.
 * This file may not be redistributed.
 * Author URL:http://imprevo.net
 ******************************************************/

use Illuminate\Database\Eloquent\Model;
use App\Word;

class Level extends Model
{
	protected $fillable = ['title'];

    public function courses()
    {
        return $this->belongsToMany('App\Course');
    }

    public function lessons()
    {
        return $this->hasMany('App\Lesson');
    }
}
