@extends('layouts.back')

@section('title', ucfirst(__('word.title')) . ' | ' . ucfirst(__('word.dashboard')))

@push('page_css')

@endpush

@section('page_name', ucfirst(__('word.dashboard')))

@push('page_content')

@endpush

@push('page_js')

@endpush

@push('page_script')

@endpush