<?php

return [

    'title'                 =>  'Fortalezas Personales',
    'site_name'             =>  'Fortalezas Personales',
    'copyright'             =>  '© 2019 Fortalezas Personales. Todos los derechos reservados.',

    'home'                  =>  'casa',
    'take'                  =>  'tomar',
    'finish'                =>  'terminar',

    'dashboard'             =>  'tablero',
    'category'              =>  'categoría',
    'point'                 =>  'punto',
    'surveyor'              =>  'topógrafa',
    'survey'                =>  'encuesta',
    'result'                =>  'resultado',
    'detail_result'         =>  'resultado detallado',

    'sign_in'               =>  'registrarse',
    'sign_out'              =>  'desconectar',

    'new'                   =>  'nueva',
    'edit'                  =>  'editar',
    'delete'                =>  'borrar',

    'save'                  =>  'salvar',
    'cancel'                =>  'cancelar',

    'yes'                   =>  'sí',
    'no'                    =>  'no',

    'enable'                =>  'habilitar',
    'enabled'               =>  'habilitada',
    'disable'               =>  'inhabilitar',
    'disabled'              =>  'discapacitada',

    'no'                    =>  'no',
    'name'                  =>  'nombre',
    'status'                =>  'estado',
    'memo'                  =>  'memorándum',
    'action'                =>  'acción',
    'display_order'         =>  'Orden de visualización',

    'section'               =>  'sección',
    'definition'            =>  'definición',
    'description'           =>  'descripción',
    'score'                 =>  'Puntuación',
    'label'                 =>  'etiqueta',
    'question'              =>  'pregunta',

    'first_name'            =>  'primer nombre',
    'last_name'             =>  'Primer apellido',
    'email'                 =>  'correo electrónico',
    'gender'                =>  'género',
    'select_your_gender'    =>  'Selecciona tu género',
    'male'                  =>  'Masculino',
    'female'                =>  'Femenino',
    'age'                   =>  'Edad',
    'take_the_free_survey'  =>  'tomar la encuesta gratis',
    'save_and_continue'     =>  'guardar y continuar',
    'finish'                =>  'terminar',
    'password'              =>  'contraseña',

    'started_at'            =>  'Empezó a las',
    'finished_at'           =>  'terminado en',
    'in_progress'           =>  'en progreso',
    'abandon'               =>  'abandonar',
    'completed'             =>  'terminada',

    'excel'                 =>  'sobresalir',
    'questionnaire'         =>  'Cuestionario',
];
