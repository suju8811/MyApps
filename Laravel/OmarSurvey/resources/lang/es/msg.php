<?php

return [

    'enable_confirm'                        =>  '¿Seguro que quieres habilitar esta fila?',
    'data_enable_success'                   =>  'Datos habilitados con éxito.',
    'data_enable_error'                     =>  'Error al habilitar.',

    'disable_confirm'                       =>  '¿Seguro que quieres deshabilitar esta fila?',
    'data_disable_success'                  =>  'Datos deshabilitados con éxito.',
    'data_disable_error'                    =>  'Error al deshabilitar.',

    'data_insert_success'                   =>  'Datos insertados con éxito.',
    'data_insert_duplicate'                 =>  'Duplicar al insertar.',
    'data_insert_error'                     =>  'Error al insertar.',

    'data_update_success'                   =>  'Datos actualizados con éxito.',
    'data_update_duplicate'                 =>  'Duplicado al actualizar.',
    'data_update_error'                     =>  'Error al actualizar.',

    'delete_confirm'                        =>  '¿Seguro que quieres eliminar esta fila?',
    'data_delete_success'                   =>  'Datos eliminados con éxito.',
    'data_delete_error'                     =>  'Error al eliminar.',

    'email_duplicated'                      =>  'Correo electrónico duplicado',
    'register_error'                        =>  'Error al registrarse.',

    'request_is_not_valid'                  =>  'La solicitud no es válida.',
    'request_error'                         =>  'Error al solicitar.',

    'submit_error'                          =>  'Error al enviar',

    'finish_s1'                             =>  'Gracias por completar el cuestionario.',
    'finish_s2'                             =>  'Sus fortalezas personales se muestran a continuación, ordenadas según la relevancia.',
    'finish_s3'                             =>  'Para descargar un informe en pdf, haga clic aquí.',
    'finish_s4'                             =>  'Para descargar un informe detallado de Excel, haga clic aquí.',

    'sign_in_description'                   =>  'Inicia sesión para comenzar tu sesión.',
    'remember_me'                           =>  'Recuérdame',

    'page_take'     => [
        's1'                                =>  'El cuestionario se compone de 120 preguntas, es gratuito y toma aproximadamente diez minutos completarlo.',
        's2'                                =>  'Asegúrese de que está en un lugar tranquilo donde nadie le va a molestar.',
        's3'                                =>  'Apague su teléfono para evitar distracciones.',
        's4'                                =>  'Para responder a cada pregunta, escoja cualquier valor entre el 1 al 10, donde 1 representa “nada parecido a mí” y 10 representa “muy parecido a mí”',
        's5'                                =>  'Es muy importante que responda rápidamente y con sinceridad. No responda cómo le gustaría ser ni cómo quiere que otras personas lo vean, responda como realmente es usted. Los resultados de este cuestionario son solo para usted, por lo que no hay necesidad de engañar a nadie.',
    ],
];
