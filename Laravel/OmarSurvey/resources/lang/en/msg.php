<?php

return [

    'enable_confirm'                        =>  'Are you sure you want to enable this row?',
    'data_enable_success'                   =>  'Data enabled successfully.',
    'data_enable_error'                     =>  'Error on enabling.',

    'disable_confirm'                       =>  'Are you sure you want to disable this row?',
    'data_disable_success'                  =>  'Data disabled successfully.',
    'data_disable_error'                    =>  'Error on disabling.',

    'data_insert_success'                   =>  'Data inserted successfully.',
    'data_insert_duplicate'                 =>  'Duplicate on inserting.',
    'data_insert_error'                     =>  'Error on inserting.',

    'data_update_success'                   =>  'Data updated successfully.',
    'data_update_duplicate'                 =>  'Duplicate on updating.',
    'data_update_error'                     =>  'Error on updating.',

    'delete_confirm'                        =>  'Are you sure you want to delete this row?',
    'data_delete_success'                   =>  'Data deleted successfully.',
    'data_delete_error'                     =>  'Error on deleting.',

    'email_duplicated'                      =>  'Email duplicated.',
    'register_error'                        =>  'Error on registering.',

    'request_is_not_valid'                  =>  'Request is not valid.',
    'request_error'                         =>  'Error on requesting.',

    'submit_error'                          =>  'Error on submitting.',

    'finish_s1'                             =>  'Thank you for taking the test.',
    'finish_s2'                             =>  'Your character strengths are shown below ranked from most relevant to least relevant.',
    'finish_s3'                             =>  'To download a pdf report please click here.',
    'finish_s4'                             =>  'To download a excel detail report please click here.',

    'sign_in_description'                   =>  'Sign in to start your session.',
    'remember_me'                           =>  'Remember Me',

    'page_take'     => [
        's1'                                =>  'The questionnaire consists of 120 questions, is free and takes approximately ten minutes to complete.',
        's2'                                =>  'Make sure you are in a quiet place where nobody is going to bother you.',
        's3'                                =>  'Turn off your phone to avoid distractions.',
        's4'                                =>  'To answer each question, choose any value between 1 and 10, where 1 represents "nothing like me" and 10 represents "very much like me"',
        's5'                                =>  'It is very important that you respond quickly and honestly. Do not answer how you would like to be or how you want other people to see you, respond as you really are. The results of this questionnaire are only for you, so there is no need to fool anyone.',
    ],
];
