<?php

return [

    'title'                 =>  'Online Personality',
    'site_name'             =>  'Online Personality',
    'copyright'             =>  '© 2019 Online Personality.  All rights reserved.',

    'home'                  =>  'home',
    'take'                  =>  'take',
    'finish'                =>  'finish',

    'dashboard'             =>  'dashboard',
    'category'              =>  'category',
    'point'                 =>  'point',
    'surveyor'              =>  'surveyor',
    'survey'                =>  'survey',
    'result'                =>  'result',
    'detail_result'         =>  'detail result',

    'sign_in'               =>  'sign in',
    'sign_out'              =>  'sign out',

    'new'                   =>  'new',
    'edit'                  =>  'edit',
    'delete'                =>  'delete',

    'save'                  =>  'save',
    'cancel'                =>  'cancel',

    'yes'                   =>  'yes',
    'no'                    =>  'no',

    'enable'                =>  'enable',
    'enabled'               =>  'enabled',
    'disable'               =>  'disable',
    'disabled'              =>  'disabled',

    'no'                    =>  'no',
    'name'                  =>  'name',
    'status'                =>  'status',
    'memo'                  =>  'memo',
    'action'                =>  'action',
    'display_order'         =>  'display order',

    'section'               =>  'section',
    'definition'            =>  'definition',
    'description'           =>  'description',
    'score'                 =>  'score',
    'label'                 =>  'label',
    'question'              =>  'question',

    'first_name'            =>  'first name',
    'last_name'             =>  'last name',
    'email'                 =>  'email',
    'gender'                =>  'gender',
    'select_your_gender'    =>  'select your gender',
    'male'                  =>  'male',
    'female'                =>  'female',
    'age'                   =>  'age',
    'take_the_free_survey'  =>  'take the free survey',
    'save_and_continue'     =>  'save and continue',
    'finish'                =>  'finish',
    'password'              =>  'password',

    'started_at'            =>  'started at',
    'finished_at'           =>  'finished at',
    'in_progress'           =>  'in progress',
    'abandon'               =>  'abandon',
    'completed'             =>  'completed',

    'excel'                 =>  'excel',
    'questionnaire'         =>  'questionnaire',
];
