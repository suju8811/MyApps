<?php
/**
 * Created by PhpStorm.
 * User: chan
 * Date: 7/9/2019
 * Time: 9:14 PM
 */

return [
    'E_FAILED'          =>  'FAILED',
    'E_DUPLICATED'      =>  'DUPLICATED',
    'E_INVALID'         =>  'INVALID',
    'S_OK'              =>  'SUCCESS'
];