import React from 'react';
import PropTypes from 'prop-types';
import { Router, Route, Link, History } from 'react-router';
import { Row, Col, Nav, Navbar, NavItem, NavDropdown, MenuItem, Form, FormGroup, Button, FormControl } from 'react-bootstrap';

import api from '../../API/api';
import SiteService from '../../API/SiteService';
import BusinesService from '../../API/BusinesService';
import UtilService from '../../Common/UtilService';

class Landing extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            error: false,
            msgError: "",
            businessList: {
                total: 0,
                items: []
            },
            businessRegister: 0
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillUnmount() {
        this.ifMounted = false;
    }

    componentDidMount() {
        this.ifMounted = true;
        document.title = "Gogo Eats";
        this.formInstance = $('form#registerForm')
        .parsley()
        .on('field:validated', () => {
            this.ifMounted && this.setState({
                error: false
            })
        });
        this._searchBusiness();
    }

    _searchBusiness() {
        SiteService.readBusinesses((res) => {
            console.log("getData", res);
            this.ifMounted && this.setState({
                businessList: res
            })
        }, (err) => {
            console.log(err)
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        var isValid = this
            .formInstance
            .isValid();

        if (isValid) {
            const name     = this.refs.name.value;
            const email    = this.refs.email.value;
            const password = this.refs.password.value;

            BusinesService.register(name, email, password, (res) => {
                // var _router = this.context.router;
                // _router.push('/introduce');
                this.setState({ businessRegister: 1 });
            }, (err) => {
                console.log(err.message)
                this.setState({ error: true, msgError: err.message })
            });
        }
    }

    render() {
        return(
            <div>
            <Navbar className="header_site" collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand className="logo_site">
                        <a href="#"><img src="/img/logo_1.png" width="100" /></a>
                    </Navbar.Brand>
                <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav pullRight>
                        <NavItem eventKey={1} href="#">Registrarse</NavItem>
                        <NavItem eventKey={2} href="#"><em className="icon-handbag"></em></NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>

            <div className="slide">
            <Row>
                <Col lg={12} className="navigation_search">

                    <div className="food_right">
                        <img src="/img/food_right.png"/>
                    </div>

                    <div className="box_search">
                    <h2>Choose, Order & Take Out</h2>
                        <div className="serach_form">
                            <input type="text" name="search" className="form-control" placeholder="Pizza, Comida China"/>
                            <button type="submit" className="btn">Search Food</button>
                        </div>
                    </div>
                </Col>
            </Row>
            </div>
                <div className="container home_results">
                    <h2>Lo mas popular de San Jose</h2>
                    <div className="_grids_business">
                        <Row>
                        {
                            this.state.businessList.items.map((item, i) => {
                                return (
                                    <Col key={'_Business' + i} lg={4}>
                                        <div className="_img_business">
                                            {<img src={UtilService.getProfileFromPath(item.logo)} className="img-responsive" />}
                                        </div>
                                        <div className="_business_data">
                                            <div className="restaurant_name"><a href="#">{ item.name }</a></div>
                                            <div className="restaurant_filter">Hamburguesas</div>
                                            <span className="restaurant_time">30 - 40 mins</span>
                                        </div>
                                    </Col>
                                );
                            })
                        }
                        {(() => {
                            if (this.state.businessList.total == 0) {
                                return (
                                    <tr>
                                        <td colSpan={11}>
                                            <p className="text-center">There is no any data.</p>
                                        </td>
                                    </tr>
                                )
                            }
                        })()}
                        </Row>
                    </div>

                    </div>

                    <div className="how_works">
                        <h2>How it works</h2>
                            <div className="container cont_works">
                                <Row>

                                    <Col lg={4}>
                                        <img src="/img/merchant.png"/>
                                        <h4>Choose a restaurant</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla cum voluptate, iste mollitia eius vero sed</p>
                                    </Col>
                                    <Col lg={4}>
                                        <img src="/img/merchant.png"/>
                                        <h4>Chosse a dish</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla cum voluptate, iste mollitia eius vero sed</p>
                                    </Col>
                                    <Col lg={4}>
                                        <img src="/img/merchant.png"/>
                                        <h4>Enjoy Delivery</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla cum voluptate, iste mollitia eius vero sed</p>
                                    </Col>
                                </Row>
                            </div>
                    </div>{/* how_works */}

                    <div className="register_restaurants">
                        <div className="container">
                        <Row>
                                <Col lg={6}>
                                    <div className="form_register" style={{ display: this.state.businessRegister == 1 ? 'none' : 'block' }}>
                                        <h3 className="welcome_register">Register Restaurant</h3>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>

                                        <form id="registerForm" method="post" data-parsley-validate="" noValidate className="mb-lg" onSubmit={this.handleSubmit}>
                                            <p className="text-danger text-center" style={{ display: this.state.error ? 'block' : 'none' }}>
                                                {(() => {
                                                    return (
                                                        <span>
                                                            { this.state.msgError }
                                                        </span>
                                                    )
                                                })()}
                                            </p>
                                            <div className="form-group has-feedback">
                                                <input id="input_email" type="text" ref="name" placeholder="Name Restaurant" autoComplete="off" required="required" data-parsley-error-message="" className="form-control" />
                                            </div>
                                            <div className="form-group has-feedback">
                                                <input id="input_email" type="email" ref="email" placeholder="E-mail" autoComplete="off" required="required" data-parsley-error-message="" className="form-control" />
                                            </div>
                                            <div className="form-group has-feedback">
                                                <input id="input_password" type="password" ref="password" placeholder="Password" required="required" className="form-control" data-parsley-error-message="" />
                                            </div>
                                            <button type="submit" className="btn _btn_regiters">Register</button>
                                        </form>
                                    </div>

                                    <div className="business_register" style={{ display: this.state.businessRegister == 1 ? 'block' : 'none' }}>
                                            Hemos Recibido su información, Pronto el equipo de Gogo se pondra en contacto con usted.
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </div>{/* register_restaurants */}

                    <div className="download_app">
                        <div className="bg_app"></div>
                        <div className="container relative">
                            <Col lg={5}>
                                <img src="/img/iphone.png" width="480"/>
                            </Col>
                            <Col lg={3} className="download_box">
                                <h3>Download our app</h3>
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ad officia minima cum libero, commodi
                            
                                <div className="stores_app">
                                    <a href="#"><img src="/img/app_store.png"/></a>
                                    <a href="#"><img src="/img/google_play.png"/></a>
                                </div>{/* stores_app */}

                            </Col>
                        </div>{/* container */}

                    </div>{/* download_app */}
            </div>
        )
    }
}

Landing.contextTypes = {
    router: PropTypes.object.isRequired
}

export default Landing;