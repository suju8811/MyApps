const DEV_MODE = 1
const PRODUCT_MODE = 2

const CONFIG_MODE = DEV_MODE

export const SERVICE_API_URL = (
    CONFIG_MODE==DEV_MODE?
    "http://192.168.1.118:3001/gogo/api/v1":
    "http://18.218.1.204:3001/gogo/api/v1"       //To do: replace with real service url
);
export const SERVICE_FILE_URL = (
    CONFIG_MODE==DEV_MODE?
    "http://192.168.1.118:3002/":
    "http://18.218.1.204:3002/"       //To do: replace with real service url
);

export const WS_CENTRIFUGO = "ws://18.218.1.204:9000/connection/websocket"

export const PREPARATION = ['10-25 MIN', '25-35 MIN', '35-45 MIN', 'OVER 1hr']
export const PRICE_LEVEL = ['', '$', '$$', '$$$', '$$$$']
export const GROUP_NAME = ['More', 'Popular near by you', 'Recommended', 'Under 30min']

export const ORDER_ACCEPTED = "OrderAccepted"
export const ORDER_PREPARED = "OrderPrepared"
export const ORDER_REQUEST = "OrderRequest"
export const ORDER_COMPLETED = "OrderCompleted"

export const TRIP_ACCEPTED = "TripAccepted"
export const TRIP_CONFIRMED = "TripConfirmed"
export const TRIP_STARTED = "TripStarted"
export const TRIP_ARRIVED = "TripArrived"
export const TRIP_DROPPED = "TripDropped"
export const TRIP_CANCELLED = "TripCancelled"
export const TRIP_COMPLETED = "TripCompleted"

export const NO_RATED = 1
export const RATED = 2