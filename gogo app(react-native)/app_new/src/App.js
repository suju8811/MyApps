
import React, { PureComponent } from 'react';
import {
    Platform,
} from 'react-native';

import OneSignal from 'react-native-onesignal';
import { createStore, applyMiddleware, combineReducers } from 'redux';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import thunk from 'redux-thunk';
import { Actions, ActionConst, Scene, Router } from 'react-native-router-flux';

import Cache from './utils/Cache'

/*   service   */
import authService from './service/authService'

/*   redux   */
import * as commonActions from './redux/actions';
import * as commonActionTypes from './redux/actionTypes';

/*   first   */
import Introduce from './scenes/introduce';
import Main from './scenes/main';

/*   auth   */
import Login from './scenes/auth/login'
import Signup from './scenes/auth/signup'
import Verification from './scenes/auth/verification'
import Forgot from './scenes/auth/forgot'

/*  home  */
import Home from './scenes/home/home'
import Business from './scenes/home/business'
import Food from './scenes/home/food'
import ViewCart from './scenes/home/viewCart'
import OrderStatus from './scenes/home/orderStatus'
import SelectLocation from './scenes/home/selectLocation'
import RatingScreen from './scenes/home/ratingScreen'
import GetNote from './scenes/home/getNote'

/*  search  */
import Search from './scenes/search/search'
import ShowResult from './scenes/search/showResult'

/*  history  */
import History from './scenes/history/history'

/*  setting  */
import Setting from './scenes/setting/setting'
import Favorite from './scenes/setting/favorite'
import Payment from './scenes/setting/payment'
import AddPayment from './scenes/setting/payment/addPayment'
import Help from './scenes/setting/help'
import Promotion from './scenes/setting/promotion'
import FreeDelivery from './scenes/setting/freeDelivery'
import DeliverWithUber from './scenes/setting/deliverWithUber'
import Settings from './scenes/setting/settings'
import About from './scenes/setting/about'
import Html from './scenes/setting/help/html'
import GetLocation from './scenes/setting/settings/getLocation'
import EditAccount from './scenes/setting/settings/editAccount'


class App extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            initialize: false,
            loggedIn: false,
        };
        this.onReceived = this.onReceived.bind(this)
    }

    componentWillMount() {
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
        OneSignal.addEventListener('registered', this.onRegistered);
        OneSignal.addEventListener('ids', this.onIds);
    }

    componentWillUnmount() {
        OneSignal.removeEventListener('received', this.onReceived);
        OneSignal.removeEventListener('opened', this.onOpened);
        OneSignal.removeEventListener('registered', this.onRegistered);
        OneSignal.removeEventListener('ids', this.onIds);
    }

    onReceived(notification) {
        this.props.commonActions.receivedNotification(notification)
        setTimeout(()=>this.props.commonActions.receivedNotification(null), 10)
        console.log("Notification received: ", notification);
    }

    onOpened(openResult) {
        // console.log('Message: ', openResult.notification.payload.body);
        // console.log('Data: ', openResult.notification.payload.additionalData);
        // console.log('isActive: ', openResult.notification.isAppInFocus);
        // console.log('openResult: ', openResult);
    }

    onRegistered(notifData) {
        // console.log("Device had been registered for push notifications!", notifData);
    }

    onIds(device) {
        Cache.device = device
        console.log('Device info: ', device);
    }

    componentDidMount() {
        authService.init((err, activeUser) => {
            setTimeout(() => {
                if (activeUser) {
                    this.setState({
                        initialize: true,
                        loggedIn: true
                    });
                    return;
                }
                this.setState({
                    initialize: true,
                    loggedIn: false,
                })
            }, 20)
        });
    }

    render() {
        if (this.state.initialize === false) return null;
        const scenes = Actions.create(
            <Scene key="root">
                <Scene key="Introduce" component={Introduce} type={ActionConst.RESET} hideNavBar />
                <Scene key="Main" component={Main} initial={this.state.loggedIn} hideNavBar />

                {/* Auth */}
                <Scene key="Login" component={Login} hideNavBar type={ActionConst.RESET} />
                <Scene key="Signup" component={Signup} hideNavBar />
                <Scene key="Verification" component={Verification} hideNavBar />
                <Scene key="Forgot" component={Forgot} hideNavBar />

                {/* Home */}
                <Scene key="Home" component={Home}/>
                <Scene key="Business" component={Business}/>
                <Scene key="Food" component={Food} duration={0}/>
                <Scene key="ViewCart" component={ViewCart} duration={0}/>
                <Scene key="OrderStatus" component={OrderStatus}/>
                <Scene key="SelectLocation" component={SelectLocation}/>
                <Scene key="RatingScreen" component={RatingScreen}/>
                <Scene key="GetNote" component={GetNote}/>

                {/* Search */}
                <Scene key="Search" component={Search} hideNavBar />
                <Scene key="ShowResult" component={ShowResult} hideNavBar />

                {/* History */}
                <Scene key="History" component={History} hideNavBar />

                {/* Setting */}
                <Scene key="Setting" component={Setting} hideNavBar />
                <Scene key="Favorite" component={Favorite} hideNavBar />
                <Scene key="Payment" component={Payment} hideNavBar />
                <Scene key="AddPayment" component={AddPayment} duration={0}/>
                <Scene key="Help" component={Help} hideNavBar />
                <Scene key="Promotion" component={Promotion} hideNavBar />
                <Scene key="FreeDelivery" component={FreeDelivery} hideNavBar />
                <Scene key="DeliverWithUber" component={DeliverWithUber} hideNavBar />
                <Scene key="Settings" component={Settings} hideNavBar />
                <Scene key="About" component={About} hideNavBar />
                <Scene key="Html" component={Html} hideNavBar />
                <Scene key="GetLocation" component={GetLocation} hideNavBar />
                <Scene key="EditAccount" component={EditAccount} hideNavBar />
            </Scene>
        )

        return (
            <Router hideNavBar scenes={scenes} />
        );
    }
}

export default connect(props => ({
    commonStatus: props.common.status,
    notification: props.common.notification,
}),
    (dispatch) => ({
        commonActions: bindActionCreators(commonActions, dispatch),
    })
)(App);