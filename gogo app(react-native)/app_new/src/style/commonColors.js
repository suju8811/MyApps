
export const theme = '#db1410';
//export const theme = 'rgb(67, 164, 34)';
export const lightTheme = 'rgb(92,187,94)'
export const background = 'rgb(242, 242, 244)'
export const menu = 'white';
export const normalText = 'rgb(58,58,72)'
export const menuRight = 'rgb(205, 205, 211)'
export const menuBorder = 'rgb(169,169,171)'
export const borderColor = 'rgb(217,217,222)'
export const textColor1 = 'rgb(157, 157, 163)'
export const textColor2 = 'rgb(107, 107, 118)'
export const textColor3 = 'rgb(178, 178, 186)'
export const placeholderText = 'rgb(158,158,164)';



export const greyfont = 'rgb(150, 150, 150)'
export const greenColor = 'rgb(79, 158, 61)'
export const darkfont = 'rgb(100, 100, 100)'
export const lightgrey = 'rgb(200, 200, 200)'




export const button = '#DF0101';
export const title = '#696969';
export const lightText = '#f0f0f0';
export const grayText = '#808080';
export const yellow = 'yellow';
export const grayMoreText = '#a4a4a3';
export const line = '#d4ebf6';
export const dateDot = '#f59271';
export const point = '#fff';
export const question = '#696969';
export const detailTitle = '#3d9aa2';
export const bottomButton = '#f59174';
export const currentUserLeadboardCell = '#eaf5fb';
export const percentListCellWeakBackground = '#d4ebf640';
export const percentListCellStrongBackground = '#82ccbe33';
export const mainColor = '#db1410';
