import {
    StyleSheet,
    Dimensions,
    Platform,
  } from 'react-native';
  
  export const { width: screenWidth, height: screenHeight } = Dimensions.get('window');
  export const headerHeight = 80;
  export const menuHeight = 60;
  export const viewHeight = screenHeight - headerHeight - menuHeight;
  
  export function wp(percentage) {
    const value = (percentage * screenWidth) / 100;
    return Math.round(value);
  }
  
  export function hp(percentage) {
    const value = (percentage * screenHeight) / 100;
    return Math.round(value);
  }
  
  export function scaleScreen() {
  
    if (screenWidth > 320)
      return 1;
  
    return 0.85;
  }
  
  export const carouselHeight                     = screenHeight * 0.35;
  export const carouselWidth                      = wp(88);
  export const carouselerWidth                    = screenWidth;
  export const carouselItemHorizontalPadding      = wp(1);
  export const carouselItemWidth                  = wp(90);
  export const activityCellSize                   = screenWidth * 0.22;
  export const categoryCellSize                   = 100;
  
  export const NavNoneButton              = 0;
  export const NavBackButton              = 1;
  export const NavFilterButton            = 2;
  export const NavOfflineButton           = 4;
  export const NavCloseButton             = 8;
  export const NavCloseTextButton         = 16;
  export const NavMenuButton              = 32;
  export const NavNotificationButton      = 64;
  export const NavPinButton               = 128;
  
  
  export const geoLocation = {
    enableHighAccuracy: Platform.OS !== 'android',
    timeout: 10000,
    maximumAge: 1000,
  };
  
  // XXX Kostas: Not crazy about this offset-based approach; slugs is more reliable.

export const introduceImg = require('../../public/images/introduceScreen.png')
export const signinImg = require('../../public/images/signinScreen.png')
export const registerImg = require('../../public/images/registerScreen.png')
export const confirmImg = require('../../public/images/confirmScreen.png')

/*  main  */
export const homeIcon               = require('../../public/images/nav/gogo_nav_home.png');
export const homeSelectedIcon       = require('../../public/images/nav/gogo_nav_home_selected.png');
export const searchIcon             = require('../../public/images/nav/gogo_nav_search.png');
export const searchSelectedIcon     = require('../../public/images/nav/gogo_nav_search_selected.png');
export const activityIcon           = require('../../public/images/nav/gogo_nav_order.png');
export const activitySelectedIcon   = require('../../public/images/nav/gogo_nav_order_selected.png');
export const userIcon               = require('../../public/images/nav/gogo_nav_user.png');
export const userSelectedIcon       = require('../../public/images/nav/gogo_nav_user_selected.png');

/*  header  */
export const left_back_arrow        = require('../../public/images/header/back_arrow.png')

/*  profile  */
export const userEmptyIcon          = require('../../public/images/gogo_user.png')

/*  home   */
export const selectedIcon           = require('../../public/images/modal/gogo_modal_selected.png')
export const priceIcon1             = require('../../public/images/modal/gogo_modal_price1_black.png')
export const priceOutlineIcon1      = require('../../public/images/modal/gogo_modal_price1.png')
export const priceIcon2             = require('../../public/images/modal/gogo_modal_price2_black.png')
export const priceOutlineIcon2      = require('../../public/images/modal/gogo_modal_price2.png')
export const priceIcon3             = require('../../public/images/modal/gogo_modal_price3_black.png')
export const priceOutlineIcon3      = require('../../public/images/modal/gogo_modal_price3.png')
export const priceIcon4             = require('../../public/images/modal/gogo_modal_price4_black.png')
export const priceOutlineIcon4      = require('../../public/images/modal/gogo_modal_price4.png')

export const recommendedIcon        = require('../../public/images/modal/gogo_modal_recommend.png')
export const popularIcon            = require('../../public/images/modal/gogo_modal_popular.png')
export const deliveryIcon           = require('../../public/images/modal/gogo_modal_delivery.png')
export const vegetableIcon          = require('../../public/images/modal/gogo_modal_vegetarian.png')
export const veganIcon              = require('../../public/images/modal/gogo_modal_vegan.png')
export const glutenIcon             = require('../../public/images/modal/gogo_modal_gluten.png')

export const gogo_logo              = require('../../public/images/gogo_logo.png')

export const carImg                 = require('../../public/images/ic_driver_car.png')
