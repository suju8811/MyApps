import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Image,
    Text,
    Alert,
    Animated,
    TouchableOpacity,
    TouchableHighlight,
    findNodeHandle,
    UIManager,
    Platform,
    StatusBar,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import IonIcons from 'react-native-vector-icons/Ionicons';
import StarRatingBar from 'react-native-star-rating-view'
import Modal from 'react-native-modal'

import { screenWidth } from '../../style/commonStyles';
import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';

import Cache from '../../utils/Cache'
import * as config from '../../config'
import businessService from '../../service/businessService'
import baseService from '../../service/baseService'

export default class OrderItem extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            imagePressed: false,
            order: this.props.data,
            image: { uri: props.data.business.logo == '' ? null : config.SERVICE_FILE_URL + props.data.business.logo },
            score: props.data.businessRate,
            foods: props.data.foods,
            driver: props.data.driver != undefined ? props.data.driver.name : '',
            tax: 0,
            bookingFee: 0,
            subTotal: 0,
            totalPrice: 0,
            showReceipt:false,
        }
    }

    getHelp() {
        baseService.getHelpContent((err, res) => {
            Actions.Help({ data: res });
        })
    }

    refreshStatusBar(){
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    viewMenu() {
        businessService.getBesiness(this.state.order.business.id, (err, res) => {
            if (err == null) {
                Actions.Business({
                    data: res,
                    updateData: () => {
                        this.refreshStatusBar()
                    }
                })
            }
        })
    }

    componentDidMount() {
        this.calcPrice()
    }

    renderImage() {
        return (
            <TouchableOpacity activeOpacity={1} onPress={() => this.viewMenu()}
                onPressIn={() => this.setState({ imagePressed: true })} style={styles.image}
                onPressIn={() => this.setState({ imagePressed: true })} onPressOut={() => this.setState({ imagePressed: false })}>
                <Image style={{ height: 95, width: '100%' }} source={this.state.image} />
                <View style={{ backgroundColor: this.state.imagePressed ? '#20200040' : '#202000a0', marginLeft: 15, width: '100%', height: 95, position: 'absolute', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: 'white', fontSize: 18 }}>{this.state.order.business.name}</Text>
                    <Text style={{ color: 'white', marginTop: 8, fontSize: 12 }}>VIEW MENU</Text>
                </View>
            </TouchableOpacity>
        )
    }

    getDescription(status) {
        switch (status) {
            case config.ORDER_REQUEST:
                return 'Confirming your order'
            case config.ORDER_ACCEPTED:
                return 'Preparing your order'
            case config.ORDER_PREPARED:
                return 'Prepared your order'
            case config.ORDER_COMPLETED:
            case config.TRIP_DROPPED:
            case config.TRIP_COMPLETED:
                return 'Order Delivered'
        }
    }

    getScheduleTime(status) {
        switch (status) {
            case config.ORDER_REQUEST:
                res = this.state.order.statusAt.OrderRequest
                break;
            case config.ORDER_ACCEPTED:
                res = this.state.order.statusAt.OrderAccepted
                break;
            case config.ORDER_PREPARED:
                res = this.state.order.statusAt.OrderPrepared
                break;
            case config.ORDER_COMPLETED:
                res = this.state.order.statusAt.OrderCompleted
                break;
            case config.TRIP_DROPPED:
                res = this.state.order.statusAt.TripDropped
                break;
            case config.TRIP_COMPLETED:
                res = this.state.order.statusAt.TripCompleted
                break;
        }
        let date = new Date(Number(res)*1000)
        return date.getHours()+':'+date.getMinutes()
    }

    calcPrice() {
        var total = 0
        this.state.order.foods.map((item) => {
            total = total + item.price * item.count
        })
        this.setState({
            tax: Cache.tax,
            bookingFee: Cache.bookingFee,
            subTotal: total,
            totalPrice: total + Cache.bookingFee,
        })

        //console.log(total, '->')
    }

    renderOrderStatus() {
        return (
            <View style={styles.orderStatus} >
                <View style={{ width: 30 }}>
                    <IonIcons name="ios-checkmark-circle-outline" size={18} color={commonColors.theme} />
                </View>
                <View style={{ flex: 1 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 16, color: commonColors.normalText }}>{this.getDescription(this.state.order.orderStatus)}</Text>
                    <Text style={{ marginTop: 5, fontSize: 13, color: commonColors.textColor2 }}>{this.getScheduleTime(this.state.order.orderStatus)}</Text>
                    <Text style={{ marginTop: 5, fontSize: 13, color: commonColors.textColor2 }}>#{this.state.order.number}</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    {this.state.score > 0 && <StarRatingBar
                        starStyle={{
                            width: 20,
                            height: 20,
                        }}
                        spacing={2}
                        readOnly={true}
                        continuous={true}
                        allowsHalfStars={true}
                        value={this.state.score}
                        onStarValueChanged={(score) => {
                            this.setState({ restaurantScore: score });
                        }}
                    />}
                </View>
            </View>
        )
    }

    renderFood(item, index) {
        return (
            <View key={index} style={{ flexDirection: 'row' }}>
                <View style={{ width: 30 }}>
                    <View style={{ height: 15, width: 15, borderWidth: 0.5, borderColor: commonColors.textColor3, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 10 }}>{item.count}</Text>
                    </View>
                </View>
                <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 15, color: commonColors.normalText }}>{item.food.food.name}</Text>
                </View>
                <View>
                    {item.like > 0 && <IonIcons name={"ios-thumbs-up"} size={20} color={commonColors.normalText} />}
                    {item.like < 0 && <IonIcons name={"ios-thumbs-down"} size={20} color={commonColors.normalText} />}
                </View>
            </View>
        )
    }

    renderAdditionalInfo() {
        return (
            <View style={styles.additionalInfo} >
                {this.state.foods.map((item, index) => {
                    return this.renderFood(item, index)
                })}
                <View style={{ flexDirection: 'row', marginTop: 15 }}>
                    <View style={{ width: 30 }}>
                        <IonIcons name="ios-person-outline" size={18} color={commonColors.normalText} />
                    </View>
                    <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 15, color: commonColors.normalText }}>Your delivery by {this.state.driver}</Text>
                    </View>
                </View>
            </View >
        )
    }

    reorder() {
        Cache.order = []
        Cache.currentBusinessId = this.state.order.business.id
        this.state.order.foods.map((item, index) => {
            Cache.order.push({ count: item.count, data: item.food.food, price: item.price, note: item.note })
        })
        Actions.ViewCart({
            updateData: () => {
                Cache.order = []
                Cache.currentBusinessId = ''
                this.refreshStatusBar()
            }
        })
    }

    renderPriceInfo() {
        return (
            <View style={styles.priceInfo} >
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: commonColors.normalText, fontSize: 15, fontWeight: 'bold' }}>Total:{this.state.totalPrice}</Text>
                </View>
                <TouchableOpacity onPress={() => this.reorder()}
                    activeOpacity={0.7} style={{ borderRadius: 3, width: 140, height: 40, backgroundColor: commonColors.theme, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: 'white', fontSize: 13 }}>REORDER</Text>
                </TouchableOpacity>
            </View>
        )
    }
    renderLink() {
        return (
            <View style={styles.link}>
                <TouchableOpacity onPress={()=>this.setState({showReceipt:true})}
                    style={{ flex: 1, height: 40, borderWidth: 0.5, borderColor: commonColors.textColor1, backgroundColor: 'rgb(250,250,250)', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: commonColors.normalText, fontSize: 10 }}>VIEW RECEIPT</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.getHelp()} style={{ flex: 1, height: 40, borderWidth: 0.5, borderColor: commonColors.textColor1, backgroundColor: 'rgb(250,250,250)', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: commonColors.normalText, fontSize: 10 }}>GET HELP</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderOptions(options) {
        return (
            options.map((item, index) => {
                if (item.enabled == false) return null
                return (<View key={index}>
                    {item.options.map((node, index2) => {
                        if (node.default == true) {
                            return (
                                <Text key={index2} style={{ fontSize: 12, color: commonColors.textColor1, marginTop: 5 }}>
                                    {node.name + (node.price > 0 ? ' ($' + node.price + ')' : '')}
                                </Text>)
                        }

                    })}
                </View>)
            })
        )
    }

    renderOrders(index, isDetail) {
        let bFirst = (index == 0)
        let { count, food, price } = this.state.order.foods[index]
        let options = food.food.foodType.foodOptions
        return (
            <View key={index} style={{
                flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 10, alignItems: 'flex-start',
                borderTopColor: commonColors.background, borderTopWidth: bFirst ? 0 : 0.5
            }}>
                <View style={{ borderWidth: 0.5, borderColor: commonColors.background, paddingHorizontal: 4 }}>
                    <Text style={{ fontSize: 12, color: commonColors.theme }}>{count}</Text>
                </View>
                <View style={{ marginLeft: 15, flex: 1 }}>
                    <Text style={{ fontSize: 15, color: commonColors.normalText }}>{food.food.name}</Text>
                    {isDetail && this.renderOptions(options)}
                </View>
                {isDetail && <Text style={{ marginLeft: 15, fontSize: 15, color: commonColors.normalText }}>{'$' + (count * price)}</Text>}
            </View>
        )
    }

    renderPriceTable() {
        return (
            <View style={{ borderTopColor: commonColors.background, borderTopWidth: 0.5, paddingHorizontal: 20, paddingVertical: 15 }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 12, color: commonColors.textColor2, flex: 1 }}>Subtotal</Text>
                    <Text style={{ fontSize: 12, color: commonColors.textColor2 }}>{'$' + this.state.subTotal}</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 15 }}>
                    <Text style={{ fontSize: 12, color: commonColors.textColor2, flex: 1 }}>Delivery Fee</Text>
                    <Text style={{ fontSize: 12, color: commonColors.textColor2 }}>{'$' + this.state.bookingFee}</Text>
                </View>
                <View style={{ height: 0.5, marginVertical: 15, backgroundColor: commonColors.background }} />
                <View style={{ flexDirection: 'row', marginBottom: 15 }}>
                    <Text style={{ fontSize: 15, color: 'black', flex: 1 }}>Total</Text>
                    <Text style={{ fontSize: 15, color: 'black' }}>{this.state.totalPrice}</Text>
                </View>
            </View>
        )
    }

    renderModal() {
        return (
            <Modal isVisible={this.state.showReceipt}>
                <View style={{ backgroundColor: 'white', borderRadius: 4 }}>
                    <View style={{
                        width: '100%', height: 54, justifyContent: 'center', marginBottom: 10,
                        alignItems: 'center', borderBottomColor: commonColors.textColor1, borderBottomWidth: 0.5
                    }}>
                        <Text style={{ color: 'black', fontSize: 18 }}>ORDER RECEIPT</Text>
                    </View>
                    {this.state.order.foods.map((item, index) => {
                        return this.renderOrders(index, true)
                    })}
                    {this.renderPriceTable()}
                    <TouchableOpacity style={{
                        width: '100%', height: 48, justifyContent: 'center',
                        alignItems: 'center', borderTopColor: commonColors.textColor1, borderTopWidth: 0.5
                    }}
                        onPress={() => this.setState({ showReceipt: false })}>
                        <Text style={{ fontSize: 15, color: commonColors.textColor2 }}>CLOSE</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderImage()}
                {this.renderOrderStatus()}
                {this.renderAdditionalInfo()}
                {this.renderPriceInfo()}
                {this.renderLink()}
                {this.renderModal()}
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
    },
    image: {
        width: '100%',
        paddingHorizontal: 15,
        marginTop: 15,
    },
    orderStatus: {
        flexDirection: 'row',
        marginHorizontal: 15,
        marginTop: 15,
        borderBottomWidth: 0.5,
        borderBottomColor: commonColors.textColor3,
        paddingBottom: 10,
    },
    additionalInfo: {
        marginHorizontal: 15,
        paddingVertical: 10,
        borderBottomWidth: 0.5,
        borderBottomColor: commonColors.textColor3,
    },
    priceInfo: {
        flexDirection: 'row',
        marginHorizontal: 15,
        paddingVertical: 10
    },
    link: {
        flexDirection: 'row',
        borderBottomWidth: 8,
        borderBottomColor: commonColors.background,

    }

});