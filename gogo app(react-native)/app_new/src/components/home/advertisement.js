import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import IonIcons from 'react-native-vector-icons/Ionicons';
import Slideshow from 'react-native-slideshow';

import { screenWidth } from '../../style/commonStyles';
import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';

import Cache from '../../utils/Cache'
import * as config from '../../config'

import baseService from '../../service/baseService'

export default class Advertisement extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            images: [],
            activeSlide: 0,
        }
    }

    componentDidMount(){
        baseService.getAds((err, res)=>{
            console.log('------', err, res, config)
            let images = []
            res.items.map((item, index)=>{
                images.push({url:config.SERVICE_FILE_URL+item.image})
            })
            this.setState({images})
        })
    }

    render() {
        return (
            <View style={styles.container}>
                {this.state.images.length>0&&<Image source={this.state.images[0]}/>}
                {this.state.images.length>0&&<Slideshow
                    dataSource={this.state.images} 
                        height={180}/>}

                {/*<View style={{backgroundColor:'rgba(0, 0, 0, 0.2)', width:screenWidth, height:200, position:'absolute'}} pointerEvents='none'/>*/}
            </View >

        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 8,
        flex: 1,
        backgroundColor: 'white',
    },
    title: {
        paddingHorizontal: 15,
        paddingTop: 20,
        paddingBottom: 10,
    },
    titleText: {
        color: commonColors.normalText,
        fontSize: 20,
    },
    slider: {
    },
    sliderContainer: {
    },
});