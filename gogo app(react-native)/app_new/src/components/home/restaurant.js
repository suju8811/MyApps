import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Image,
    Text,
    Alert,
    Animated,
    TouchableOpacity,
    TouchableHighlight,
    findNodeHandle,
    UIManager,
} from 'react-native';

import { Actions, ActionConst, Scene, Router } from 'react-native-router-flux';
import IonIcon from 'react-native-vector-icons/Ionicons';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as commonActions from '../../redux/actions';
import * as commonActionTypes from '../../redux/actionTypes';

import { screenWidth } from '../../style/commonStyles';
import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';

import Cache from '../../utils/Cache'
import * as config from '../../config'
import businessService from '../../service/businessService'

class Restaurant extends PureComponent {

    constructor(props) {
        super(props);

        let data = this.convertData(props.data)
        let { toggleheart, title, image, description, preparation, closed } = data
        this.state = {
            id: props.data.id,
            toggleheart: businessService.isFavorite(props.data.id),
            title: data.title,
            image: data.image,
            description: data.description,
            preparation: data.preparation,
            closed: data.closed,
            heartSize: new Animated.Value(1),
            type : props.type
        }
    }

    componentWillReceiveProps(nextProps) {
        let { commonStatus, favoriteId } = nextProps
        if (commonStatus == commonActionTypes.CHANGE_FAVORITE_STATE) {
            if (favoriteId == this.state.id){
                this.setState({toggleheart: !this.state.toggleheart})
            }
        }
    }

    convertData(data) {
        if (data == undefined || data == null) return {
            toggleheart: false,
            title: null,
            image: null,
            description: null,
            preparation: config.PREPARATION[0],
            closed: false
        }
        var day = new Date()
        let { favorites } = Cache.currentUser.user
        //heart
        let heart = false
        for (var i = 0; i < favorites.length; i++) {
            if (favorites[i] == data.id) {
                heart = true;
                break;
            }
        }
        //description
        var detail = ''
        if (data.dietaries) {
            for (var i = 0; i < data.dietaries.length; i++) {
                detail = detail + data.dietaries[i] + ' • '
            }
        }
        detail = detail + config.PRICE_LEVEL[data.priceLevel]

        //close
        let closed = false
        let { schedules } = data
        if (schedules != undefined && schedules != '') {
            var currentTime = day
            var openTime = new Date(schedules[day.getDay()].openTime * 1000)
            var closeTime = new Date(schedules[day.getDay()].closeTime * 1000)
            if (openTime > currentTime || closeTime < currentTime) {
                closed = true
            }
        }
        return {
            toggleheart: heart,
            title: data.name,
            image: data.logo ? { uri: config.SERVICE_FILE_URL + data.logo } : null,
            description: detail,
            preparation: config.PREPARATION[data.preparationTime],
            closed: closed
        }
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextProps == this.props) return
        return;
        let data = this.convertData(nextProps.data)
        this.setState({
            id: data.id,
            toggleheart: data.toggleheart,
            title: data.title,
            image: data.image,
            description: data.description,
            preparation: data.preparation,
            closed: data.closed,
        });
    }

    onPressHeart() {
        businessService.changeFavorite(this.state.id, (err, res) => {
            if (err) {
                // console.log('heart->', err);
            } else {
                // console.log('heart->', res);
                this.state.favorites.splice(index, 1)
                this.forceUpdate()
            }
        })
        this.props.data.toggleheart = !this.state.toggleheart
        this.setState({ toggleheart: !this.state.toggleheart })    
        this.state.heartSize.setValue(0.8)
        Animated.spring(
            this.state.heartSize,
            {
                toValue: 1,
                friction: 0.9,
            }
        ).start()
    }

    showDetail() {
        Actions.Business({ data: this.props.data });
    }

    render() {
        return (
            <View style={[styles.container, {
                margin: this.state.type == 'row' ? 15 : 0,
                width: this.state.type == 'row' ? screenWidth - 30 : screenWidth - 45,
            }]}>
                <TouchableOpacity onPress={this.showDetail.bind(this)} activeOpacity={0.8}>
                    <View style={styles.imageContainer}>
                        <Image
                            style={styles.image}
                            source={this.state.image}>
                        </Image>
                    </View>
                    <View style={styles.content}>
                        <View style={styles.firstLine}>
                            <Text style={styles.titleText}>
                                {this.state.title}
                            </Text>
                            <Text style={styles.preparation}>
                                {this.state.preparation}
                            </Text>
                        </View>
                        <Text style={styles.description}>
                            {this.state.description}
                        </Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.onPressHeart()} style={styles.heart}>
                    {!this.state.toggleheart && <IonIcon name="md-heart-outline" size={27} color={'white'} style={styles.heartIcon} />}
                    {this.state.toggleheart && <Animated.View style={{ transform: [{ scale: this.state.heartSize }] }}>
                        <IonIcon name="md-heart" size={25} color={'white'} style={styles.heartIcon} />
                    </Animated.View>}
                </TouchableOpacity>
            </View >
        );
    }
}

export default connect(props => ({
    commonStatus: props.common.status,
    favoriteId: props.common.favoriteId,

}),
    (dispatch) => ({
        commonActions: bindActionCreators(commonActions, dispatch),
    })
)(Restaurant);

const styles = StyleSheet.create({
    container: {
        marginVertical: 15,
        backgroundColor: 'white',
    },
    imageContainer: {
        height: 190,
        width: '100%',
    },
    content: {
        marginTop: 15,
    },
    titleText: {
        fontSize: 15,
        fontWeight: 'bold',
        color: commonColors.normalText,
    },
    description: {
        marginTop: 3,
        fontSize: 13,
        color: commonColors.textColor1,
    },
    image: {
        height: '100%',
        width: '100%',
    },
    heart: {
        position: 'absolute',
        top: 15,
        right: 15,
        padding: 5,
    },
    preparation:{
        fontSize:13,
        color:commonColors.textColor2,
    },
    firstLine:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    heartIcon:{
        backgroundColor:'transparent',
        shadowColor: '#000',
        shadowOpacity: 1,
        shadowRadius: 5,
        shadowOffset: { height: 1 }
    }
});