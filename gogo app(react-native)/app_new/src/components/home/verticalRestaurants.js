import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import IonIcon from 'react-native-vector-icons/Ionicons';

import { screenWidth } from '../../style/commonStyles';
import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';

import Cache from '../../utils/Cache'

import Restaurant from './restaurant'

export default class MoreRestaurants extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            restaurants: props.dataList,
            title: props.title,
        }
    }

    render() {
        let { restaurant } = this.state
        return (
            <View style={styles.container}>
                <View style={styles.title}>
                    <Text style={styles.titleText}>
                        {this.state.title}
                    </Text>
                </View>
                {this.state.restaurants.map((item, index)=>{
                    return <Restaurant type="row" key={index} data={item}/>
                })}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 8,
        flex: 1,
        backgroundColor: 'white',
    },
    title: {
        paddingHorizontal: 15,
        paddingTop: 20,
        paddingBottom: 10,
    },
    titleText: {
        color: commonColors.normalText,
        fontSize: 20,
    }
});