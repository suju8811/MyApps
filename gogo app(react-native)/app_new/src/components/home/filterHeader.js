import React, { PureComponent } from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
    TouchableHighlight,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import IonIcon from 'react-native-vector-icons/Ionicons';

import { screenWidth } from '../../style/commonStyles';
import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';

import Cache from '../../utils/Cache'


export default class FilterHeader extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            filter: props.changed,
            filterText: props.title
        }
    }
    componentWillReceiveProps(nextProps){
        this.setState({
            filter: nextProps.changed,
            filterText: nextProps.title,
        })
    }
    render() {
        return (
            <View style={{ backgroundColor: 'white', borderBottomColor:commonColors.textColor1, borderBottomWidth:0.5 }}>
                <TouchableOpacity onPress={()=>this.props.onPress()} activeOpacity={0.7} style={styles.container}>
                    <View style={styles.left}>
                        <IonIcon name="md-options" size={18} color={this.state.filter?commonColors.theme:commonColors.normalText} />
                    </View>
                    <View style={styles.middle}>
                        <Text style={styles.titleText} numberOfLines={1}>
                            {this.state.filterText}
                        </Text>
                    </View>
                    <View style={styles.right}>
                    </View>
                </TouchableOpacity>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 40,
        width: '100%',
        backgroundColor: 'white',
        flexDirection: 'row',
        shadowColor: '#000',
        shadowOpacity: 0.2,
        shadowRadius: 1,
        shadowOffset: { height: 1 }
    },
    left: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    middle: {
        flex: 6,
        justifyContent: 'center',
        alignItems: 'center'
    },
    right: {
        flex: 1,
    },
    titleText: {
        fontSize: 12,
        color: commonColors.textColor1,
    }
});