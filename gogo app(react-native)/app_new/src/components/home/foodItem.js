import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Image,
    Text,
    Alert,
    Animated,
    TouchableOpacity,
    TouchableHighlight,
    findNodeHandle,
    UIManager,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import IonIcon from 'react-native-vector-icons/Ionicons';

import { screenWidth } from '../../style/commonStyles';
import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';

import Cache from '../../utils/Cache'
import * as config from '../../config'


export default class Restaurant extends PureComponent {
    constructor(props) {
        super(props);
        let { image, description, price } = props.data
        this.state = {
            image: image ? { uri: config.SERVICE_FILE_URL + image } : null,
            description: description,
            price:price,
            isLast:props.isLast,
        }
    }


    showDetail() {
    }

    render() {
        let imgWidth = 80
        let imgHeight = 80
        let imgMargin = 20
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <View style={styles.text}>
                        <Text style={styles.description}>{this.state.description}</Text>
                        <Text style={styles.price}>{'$'+this.state.price}</Text>
                    </View>
                    <Image source={this.state.image} style={styles.image}/>
                </View>
                {this.state.isLast==false&&<View style={{backgroundColor:commonColors.background, height:0.5}}/>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        paddingHorizontal:20,
    },
    content: {
        marginVertical:16,
        flexDirection: 'row',
        flex:1,
    },
    text:{
        flex:1,
    },
    image:{
        marginLeft:20,
        height:80,
        width:80,
        borderWidth:0.5,
        borderColor:'grey',
    },
    description:{
        fontSize:13,
        color:commonColors.normalText,
    },
    price:{
        fontSize:10,
        marginTop:15,
        color:commonColors.normalText
    }
});