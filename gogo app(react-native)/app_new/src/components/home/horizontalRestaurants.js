import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import IonIcon from 'react-native-vector-icons/Ionicons';
import Carousel from 'react-native-snap-carousel';

import { screenWidth } from '../../style/commonStyles';
import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';

import Cache from '../../utils/Cache'
import Restaurant from './restaurant'

const carouselLeftMargin = (commonStyles.carouselerWidth - commonStyles.carouselItemWidth) / 2 - commonStyles.carouselItemHorizontalPadding;
export default class NewUberEATS extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            restaurants: props.dataList,
            title:props.title,
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.title}>
                    <Text style={styles.titleText}>
                        {this.state.title}
                    </Text>
                </View>
                <Carousel
                    sliderWidth={screenWidth}
                    itemWidth={screenWidth - 30}
                    inactiveSlideScale={1}
                    inactiveSlideOpacity={1}
                    enableMomentum={false}
                    containerCustomStyle={styles.slider}
                    contentContainerCustomStyle={styles.sliderContainer}
                    showsHorizontalScrollIndicator={false}
                    snapOnAndroid={true}
                    removeClippedSubviews={false}>

                    {
                        this.state.restaurants.map((item, index) => {
                            return (
                                <View key={index} style={{ marginHorizontal: 7.5 }}>
                                    <Restaurant type="col" key={index} data={item}/>
                                </View>
                            )
                        })
                    }

                </Carousel>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 8,
        flex: 1,
        backgroundColor: 'white',
    },
    title: {
        paddingHorizontal: 15,
        paddingTop: 20,
        paddingBottom: 10,
    },
    titleText: {
        color: commonColors.normalText,
        fontSize: 20,
    },
    slider: {
    },
    sliderContainer: {
    },
});