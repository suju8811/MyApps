import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import IonIcon from 'react-native-vector-icons/Ionicons';
import Carousel from 'react-native-snap-carousel';

import { screenWidth } from '../../style/commonStyles';
import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';

import Cache from '../../utils/Cache'
import Restaurant from './restaurant'
import DeliveredOrder from '../../components/home/deliveredOrder'
import orderService from '../../service/orderService'

const carouselLeftMargin = (commonStyles.carouselerWidth - commonStyles.carouselItemWidth) / 2 - commonStyles.carouselItemHorizontalPadding;
export default class NoRatedOrders extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            unRatedOrders: [],
        }
    }

    componentWillUpdate(nextProps, nextState) {
        if ( nextProps.index != this.props.index ) this.updateItems()
    }

    componentDidMount() {
        this.updateItems()
    }

    updateItems(){
        orderService.getNoRatedOrders(0, 15, (err, res) => {
            this.setState({ unRatedOrders: res.items })
        })
    }
    render() {
        if ( this.state.unRatedOrders.length == 0 ) return null;
        return (
            <View style={styles.container}>
                <Carousel
                    sliderWidth={screenWidth}
                    itemWidth={screenWidth}
                    inactiveSlideScale={1}
                    inactiveSlideOpacity={1}
                    enableMomentum={false}
                    containerCustomStyle={styles.slider}
                    contentContainerCustomStyle={styles.sliderContainer}
                    showsHorizontalScrollIndicator={false}
                    snapOnAndroid={true}
                    removeClippedSubviews={false}>

                    {
                        this.state.unRatedOrders.map((item, index) => {
                            return (
                                <DeliveredOrder key={index} data={item} updateData={()=>this.updateItems()}/>
                            )
                        })
                    }

                </Carousel>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 8,
        flex: 1,
        width:screenWidth,
        backgroundColor: 'white',
    },
    title: {
        paddingHorizontal: 15,
        paddingTop: 20,
        paddingBottom: 10,
    },
    titleText: {
        color: commonColors.normalText,
        fontSize: 20,
    },
    slider: {
    },
    sliderContainer: {
    },
});