import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Image,
    Text,
    TouchableOpacity,
    TouchableHighlight
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import EvilIcon from 'react-native-vector-icons/EvilIcons';

import { screenWidth } from '../../style/commonStyles';
import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';

import Cache from '../../utils/Cache'


export default class FilterHeader extends PureComponent {
    constructor(props) {
        super(props);
        let {address} = props.address
        this.state={
            deliverMode: 'ASAP',
            city:address!=undefined?address.split(',')[0]:'',
        }
    }

    componentWillReceiveProps(nextProps) {
        let {address} = nextProps
        this.setState({
            city:address!=undefined?address.split(',')[0]:'',
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={()=>this.props.onPress()} activeOpacity={0.7} style={styles.headerButton}>
                    <Text style={styles.title}>
                        {this.state.deliverMode}
                    </Text>
                    <EvilIcon name="arrow-right" size={22} color={commonColors.textColor2}/>
                    <Text style={styles.title}>
                        {this.state.city}
                    </Text>
                    <EvilIcon name="chevron-down" size={22} color={commonColors.textColor2}/>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        height:44,
        width:'100%',
        backgroundColor:'white',
        justifyContent:'center',
        alignItems:'center',
        borderBottomWidth: 0.5,
        borderBottomColor: commonColors.menuRight,
    }, 
    headerButton:{
        flexDirection:'row',
    },
    title:{
        fontSize:15,
        fontWeight:'bold',
        color:commonColors.normalText,
    }
});