import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Image,
    Text,
    Alert,
    Animated,
    TouchableOpacity,
    TouchableHighlight,
    findNodeHandle,
    UIManager,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import IonIcon from 'react-native-vector-icons/Ionicons';

import { screenWidth } from '../../style/commonStyles';
import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';

import Cache from '../../utils/Cache'
import * as config from '../../config'
import businessService from '../../service/businessService'
import baseService from '../../service/baseService'

export default class DeliveredOrder extends PureComponent {

    constructor(props) {
        super(props);

        //console.log(this.props.data)

        this.state = {
            order: this.props.data
        }
    }

    getHelp() {
        baseService.getHelpContent((err, res) => {
            Actions.Help({ data: res });
        })
    }

    ratingOrder(){
        Actions.RatingScreen({data:this.state.order, updateData:()=>{
            if ( this.props.updateData ){
                this.props.updateData()
            }
        }})
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 8 }}>
                        <Text style={{ color: commonColors.textColor3, fontSize: 14, marginTop: 15 }}>{this.state.order.business.name}</Text>
                        <Text style={{ fontSize: 32, color: commonColors.normalText, marginTop: 8 }}>Delivered</Text>
                        <Text style={{ color: commonColors.textColor1, fontSize: 14, marginTop: 8 }}>Your order was delivered successfully</Text>
                        <View style={{ flexDirection: 'row', marginVertical: 20 }}>
                            {this.state.order.rated == false &&<TouchableOpacity onPress={() => this.ratingOrder()} activeOpacity={0.7}
                                style={{
                                    backgroundColor: commonColors.theme, borderRadius: 3, height: 40,
                                    justifyContent: 'center', alignItems: 'center', width: 120
                                }}>
                                <Text style={{ color: 'white', fontSize: 14 }}>RATE ORDER</Text>
                            </TouchableOpacity>}
                            <TouchableOpacity onPress={() => this.getHelp()} activeOpacity={0.7}
                                style={{
                                    backgroundColor: 'white', borderRadius: 3, borderColor: 'black', height: 40, marginLeft: 10,
                                    borderWidth: 1, justifyContent: 'center', alignItems: 'center', width: 120
                                }}>
                                <Text style={{ color: 'black', fontSize: 14 }}>GET HELP</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ width: 80, marginTop: 15 }}>
                        <Image source={{uri:config.SERVICE_FILE_URL+this.state.order.business.logo}} style={{ borderRadius: 40, width: 80, height: 80 }} />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 8,
        width:screenWidth,
        backgroundColor: 'white',
        paddingHorizontal: 15,
    },

});