import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Image,
    Text,
    Alert,
    Animated,
    TouchableOpacity,
    TouchableHighlight,
} from 'react-native';

import IonIcons from 'react-native-vector-icons/Ionicons';

import { screenWidth } from '../../style/commonStyles';
import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';

import Cache from '../../utils/Cache'
import * as config from '../../config'


export default class RadioView extends PureComponent {
    constructor(props) {
        super(props);
        let { single, data } = props
        this.state = {
            single: single,
            items: data
        }
    }

    check(index) {
        if (this.state.single == true) {
            this.state.items.map((item, i) => {
                this.state.items[i].default = (i == index)
            })
        } else {
            this.state.items[index].default = !this.state.items[index].default
        }
        this.setState({ items: JSON.parse(JSON.stringify(this.state.items)) })
        this.props.update(this.state.items)
        //console.log('===>', this.state.items)
    }

    renderItem(index) {
        let { name, price, enabled } = this.state.items[index]
        if ( enabled == false ) return null;
        let isChecked = this.state.items[index].default
        let iconName = (this.state.single == false) ? (isChecked ? "ios-checkbox" : "ios-square-outline") : (isChecked ? "ios-radio-button-on" : "ios-radio-button-off")
        let color = isChecked ? commonColors.theme : 'grey'
        let { itemStyle, buttonStyle } = this.props
        return (
            <View key={index}>
                {index > 0 &&
                    <View style={{ backgroundColor: commonColors.textColor1, height: 0.5, width: '100%' }} />}
                <TouchableOpacity activeOpacity={0.7} onPress={() => this.check(index)}
                    style={[{ flexDirection: 'row', marginVertical: 15, justifyContent: 'center' }, itemStyle]}>
                    <IonIcons name={iconName} size={24} color={color} style={buttonStyle} />
                    <View style={{ flexDirection: 'row', justifyContent: 'center', flex: 1, paddingTop: 3 }}>
                        <Text style={{ flex: 1, color: commonColors.normalText, fontSize: 15, marginLeft: 15 }}>{name}</Text>
                        <Text style={{ color: commonColors.textColor1, fontSize: 14 }}>{'$' + price}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        let { style } = this.props
        return (
            <View style={[{ paddingHorizontal: 15 }, style]}>
                {
                    this.state.items.map((item, index) => {
                        return this.renderItem(index)
                    })
                }
            </View>

        );
    }
}

const styles = StyleSheet.create({
});