import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Image,
    Text,
    Alert,
    Animated,
    TouchableOpacity,
    TouchableHighlight,
    findNodeHandle,
    UIManager,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import IonIcon from 'react-native-vector-icons/Ionicons';

import { screenWidth } from '../../style/commonStyles';
import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';

import Cache from '../../utils/Cache'
import * as config from '../../config'


export default class FavoriteItem extends PureComponent {
    constructor(props) {
        super(props);
        let { image, title, description, preparation } = props.data
        this.state = {
            image: image,
            title: title,
            description: description,
            additional: preparation,
            editing: props.editing,
        }
    }


    showDetail() {
    }

    render() {
        let imgWidth = 80
        let imgHeight = 80
        let imgMargin = 16
        return (
            <View style={styles.container}>
                <Image source={this.state.image} style={{ width: imgWidth, height: imgHeight, marginBottom: imgMargin }} />
                <View style={{ paddingHorizontal: imgMargin, flex:1}}>
                    <Text style={styles.header}>
                        {this.state.title}
                    </Text>
                    <Text numberOfLines={2} style={styles.description}>
                        {this.state.description}
                    </Text>
                    {this.state.additional != undefined && this.state.additional != null &&
                        <View style={{ height: 0.5, width: '100%', backgroundColor: commonColors.textColor3 }} />}
                    <Text numberOfLines={2} style={styles.additional}>
                        {this.state.additional}
                    </Text>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 16,
        marginHorizontal: 16,
        flexDirection: 'row',
        flex:1,
    },
    header: {
        color: commonColors.normalText,
        fontWeight: 'bold',
        fontSize: 15,
    },
    description: {
        color: commonColors.textColor3,
        fontSize: 13,
        flexWrap:'wrap',
    },
    additional: {
        color: commonColors.textColor3,
        fontSize: 13,
    }
});