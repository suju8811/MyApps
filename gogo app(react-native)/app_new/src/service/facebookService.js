import {
    AsyncStorage,
    Platform,
} from 'react-native';
import * as async from 'async'
import * as _ from 'underscore'
import moment from 'moment'
import Cache from '../utils/Cache'
import UtilService from '../utils/util'
import * as config from '../config'

module.exports = {
    async facebookLogin(data, cb) {
        try {
            let response = await fetch(config.SERVICE_API_URL + '/user/facebook/login',
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify([data, { onesignalPlayerId: Cache.device.userId }])
                })
            let responseJson = await response.json();
            if (response.status == 200) {
                Cache.currentUser = responseJson
                await UtilService.saveLocalObjectData('currentUser', responseJson)
                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },

}
