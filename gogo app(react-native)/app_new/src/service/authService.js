import {
    AsyncStorage,
    Platform,
} from 'react-native';
import * as async from 'async'
import * as _ from 'underscore'
import moment from 'moment'
import Cache from '../utils/Cache'
import UtilService from '../utils/util'
import * as config from '../config'

module.exports = {
    async init(cb) {
        //check if current user exists or not
        var username = await UtilService.getLocalStringData('username');
        var password = await UtilService.getLocalStringData('password');

        Cache.selectedLocation = await UtilService.getLocalObjectData('selectedLocation');
        Cache.customLocation = await UtilService.getLocalObjectData('customeLocation');
        Cache.selectedLocationIndex = await UtilService.getLocalObjectData('selectedLocationIndex');
        if (password) {
            this.login(username, password, (err, user) => {
                if (err == null) {
                }
                cb(err, user)
            })
        } else {
            cb(null)
        }
    },


    async getDeviceId() {
        let res = await UtilService.getLocalStringData('onesignalDeviceId');
        //console.log('------>', res)
        if (res == null) {
            if (Cache.device == undefined || Cache.device == null ||
                Cache.device.userId == undefined || Cache.device.userId == null)
                return null;
            res = Cache.device.userId
        }

        return res;
    },
    /**
     * login
     *
     * @param username
     * @param password
     * @param cb - callback(error, user),
     *             if login is success, then returns user instance, else returns error
     */
    async login(username, password, cb) {
        var deviceId = await this.getDeviceId()
        console.log(username, password, deviceId)
        if (deviceId == undefined || deviceId == null) {
            cb('err', null)
            return;
        }
        try {
            let response = await fetch(config.SERVICE_API_URL + '/user/login',
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        email: username,
                        password: password,
                        platform: Platform.OS,
                        onesignalPlayerId: deviceId,
                    })
                })

            let status = response.status

            let responseJson = await response.json();
            if (status == 200) {
                Cache.currentUser = responseJson
                //Cache.selectedLocation = responseJson.user.homeLocation
                await UtilService.saveLocalStringData('username', username);
                await UtilService.saveLocalStringData('password', password);
                await UtilService.saveLocalStringData('onesignalDeviceId', deviceId);
                cb(null, responseJson);
            } else {
                // console.log('error', responseJson)
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },

    async logout() {
        Cache.resetMap();
        Cache.currentUser = null;
        Cache.order = null
        //await UtilService.removeLocalObjectData('currentUser')
        await UtilService.removeLocalObjectData('username')
        await UtilService.removeLocalObjectData('password')
        await UtilService.removeLocalObjectData('onesignalDeviceId')

        await UtilService.removeLocalObjectData('selectedLocation');
        await UtilService.removeLocalObjectData('customeLocation');
        await UtilService.removeLocalObjectData('selectedLocationIndex');
    },
    /**
     * signup
     *
     * @param userData - user information to create
     *                   must include username, password
     * @param cb - callback(error, user)
     *             if success, then returns created user instance, else return error
     *             created user is altivated already, so can logged in automatically
     */
    async signup(userData, cb) {
        try {
            let response = await fetch(config.SERVICE_API_URL + '/user/register',
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(userData)
                })

            let status = response.status

            let responseJson = await response.json();
            if (status == 200) {
                Cache.currentUser = { token: null, user: responseJson }
                await UtilService.saveLocalObjectData('currentUser', Cache.currentUser)

                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },

    resetPassword(email, cb) {
        cb(null)
    },

    async updateUser(user, cb) {
        if (user == null)
            return;

        try {
            let response = await fetch(config.SERVICE_API_URL + '/users/' + user.id,
                {
                    method: 'PUT',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + Cache.currentUser['token'],
                    },
                    body: JSON.stringify(user)
                })

            let responseJson = await response.json();
            if (response.status == 200) {
                Cache.currentUser['user'] = responseJson
                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },

    getActiveUser() {
        return Cache.currentUser.user;
    },

    getUser(cb) {
        cb(null, Cache.currentUser)
    },

    getUserWithoutCache(cb) {
        cb(null, Cache.currentUser)
    },

    setUserInfo() {
        //get user full information
        this.getUser((err, user) => {
            if (err) {
                // console.log(err); return;
            }

            //save user
            AsyncStorage.setItem('GoGo-user', JSON.stringify(user));
        })
    },

    getUserInfo() {
        if (!this.getActiveUser()) return null;

        var str = AsyncStorage.getItem('GoGo-user');
        if (str && str.length > 0) {
            return JSON.parse(str)
        }
    },

    clearUserInfo() {
        AsyncStorage.removeItem('GoGo-user');
    },

    //-------- start of home apis ---------------

    //get all categories

    getCategory(catId, cb) {
        if (Cache.categories) {
            var category = _.find(Cache.categories, (o) => {
                return o._id == catId
            })
            cb(null, category);
            return;
        }

        cb(null)
    },

    async verification(data, cb) {
        try {
            let response = await fetch(config.SERVICE_API_URL + '/verifyCode',
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data)
                })
            let responseJson = await response.json();
            if (response.status == 200) {
                Cache.currentUser.token = responseJson.token
                await UtilService.saveLocalObjectData('currentUser', Cache.currentUser)
                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },
    async forgotPassword(data, cb) {
        try {
            let response = await fetch(config.SERVICE_API_URL + '/forgotPassword',
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data)
                })
            let responseJson = await response.json();
            if (response.status == 200) {
                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },
    async facebookLogin(data, cb) {
        try {
            let response = await fetch(config.SERVICE_API_URL + '/user/facebook/login',
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify([data, { onesignalPlayerId: Cache.device.userId }])
                })
            let responseJson = await response.json();
            if (response.status == 200) {
                Cache.currentUser = responseJson
                await UtilService.saveLocalObjectData('currentUser', responseJson)
                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },

}
