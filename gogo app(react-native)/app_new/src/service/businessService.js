import {
    AsyncStorage,
    Platform,
} from 'react-native';
import * as async from 'async'
import * as _ from 'underscore'
import moment from 'moment'
import Cache from '../utils/Cache'
import UtilService from '../utils/util'
import * as config from '../config'

module.exports = {
    
    async getBesinesses(data, cb) {

        try {
            let response = await fetch(config.SERVICE_API_URL + '/businesses/query',
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + Cache.currentUser['token'],
                    },
                    body: JSON.stringify(data)
                })

            let status = response.status

            let responseJson = await response.json();
            if (status == 200) {
                cb(null, responseJson);
            } else {
                cb(responseJson.message)
            }
        } catch (error) {
            cb(error)
        }
    },

    async getBesiness(id, cb){
        try {
            let response = await fetch(config.SERVICE_API_URL + '/businesses/'+id,
                {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + Cache.currentUser['token'],
                    },
                })

            let status = response.status

            let responseJson = await response.json();
            if (status == 200) {
                cb(null, responseJson);
            } else {
                cb(responseJson.message)
            }
        } catch (error) {
            cb(error)
        }
    },

    async getFood(id, mealKind, cb) {

        try {
            let response = await fetch(config.SERVICE_API_URL + '/foods/by/business?businessId=' + id + '&mealKindCode=' + mealKind,
                {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + Cache.currentUser['token'],
                    },
                })

            let status = response.status

            let responseJson = await response.json();
            if (status == 200) {
                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },

    async getFavorite(cb) {
        try {
            let response = await fetch(config.SERVICE_API_URL + '/users/' + Cache.currentUser.user.id + '/favorite',
                {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + Cache.currentUser['token'],
                    },
                })
            let responseJson = await response.json();
            if (response.status == 200) {
                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },

    async changeFavorite(id, cb) {
        try {
            let response = await fetch(config.SERVICE_API_URL + '/users/' + Cache.currentUser.user.id + '/favorite',
                {
                    method: 'PUT',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + Cache.currentUser['token'],
                    },
                    body: JSON.stringify(id)
                })
            let responseJson = await response.json();
            if (response.status == 200) {

                let index = Cache.currentUser.user.favorites.indexOf(id)
                if ( index < 0 ) {
                    Cache.currentUser.user.favorites.push(id)
                }else{
                    Cache.currentUser.user.favorites.splice(index,1)
                }

                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },

    async sendOrder(cb) {

        if (Cache.order == undefined || Cache.order.length == 0) {
            cb('no order')
            return
        }
        var foods = []
        var price = 0
        _.map(Cache.order, (item) => {
            foods.push({
                food: {
                    food: item.data,
                    price: item.price,
                    instruction: item.instruction,
                },
                count: item.count,
                price: item.price * item.count,
            })
            price = price + item.price * item.count
        })
        price = price + Cache.tax + Cache.bookingFee
        try {
            let response = await fetch(config.SERVICE_API_URL + '/orders',
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + Cache.currentUser['token'],
                    },
                    body: JSON.stringify({
                        userId: Cache.currentUser.user.id,
                        businessId: Cache.businessId,
                        foods: foods,
                        tax: Cache.tax,
                        bookingFee: Cache.bookingFee,
                        price: price,
                        deliveryLocation: Cache.location,
                    })
                })

            let responseJson = await response.json();
            if (response.status == 200) {

                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },

    async getCategories(cb) {
        try {
            let response = await fetch(config.SERVICE_API_URL + '/dietaries?top=2',
                {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + Cache.currentUser['token'],
                    },
                })
            let responseJson = await response.json();
            if (response.status == 200) {
                let favorites = []
                for ( var i = 0; i < responseJson.length; i ++ ){
                    favorites.push(responseJson[i].id)
                }
                Cache.currentUser.user.favorites = favorites
                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },
    isFavorite(id){
        return (Cache.currentUser.user.favorites.indexOf(id)>=0)
    }
}
