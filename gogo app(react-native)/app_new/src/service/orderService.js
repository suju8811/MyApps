import {
    AsyncStorage
} from 'react-native';

import * as async from 'async'
import * as _ from 'underscore'
import moment from 'moment'
import Cache from '../utils/Cache'
import UtilService from '../utils/util'
import * as config from '../config'

module.exports = {

    async orderRequest(url, method, body, cb) {
        let header = {
            method: method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + Cache.currentUser['token'],
            },
        }

        _.extend(header, method == "GET" ? {} : { body: JSON.stringify(body) })
        try {
            let response = await fetch(config.SERVICE_API_URL + url, header)

            let responseJson = await response.json();
            if (response.status == 200) {

                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },

    getOrder(id, cb) {
        this.orderRequest('/orders/' + id, 'GET', null, (err, res) => {
            cb(err, res)
        })
    },

    cancelOrder(id, cb){
        this.orderRequest('/orders/business/process', 'POST', {id:id, orderStatus:'OrderCancelled'}, (err, res)=>{
            cb(err, res)
        })
    },

    submitOrder(id, businessId, cb){
        this.orderRequest('/orders/business/process', 'POST', {id:id, businessId:businessId, orderStatus:'OrderCompleted'}, (err, res)=>{
            cb(err, res)
        })
    },

    getPastOrders(offset, count, cb){
        this.getOrders(offset, count, true, false, 0, cb)
    },

    getUpcomingOrders(offset, count, cb){
        this.getOrders(offset, count, false, true, 0, cb)
    },

    getNoRatedOrders(offset, count, cb){
        this.getOrders(offset, count, true, false, config.NO_RATED, cb)
    },

    getOrders(offset, count, is_past, is_upcoming, rated, cb){
        this.orderRequest(`/orders?userId=${Cache.currentUser.user.id}&offset=${offset}&count=${count}&is_upcoming=${is_upcoming}&is_past=${is_past}&rated=${rated}`, 
        'GET', null, (err, res)=>{
            cb(err, res)
        })
    },
    async submitRate(orderId, businessRate, businessFeedback, driverRate, driverFeedback, cb){
        this.orderRequest('/orders/submit/by/user', 'POST', {
            id:orderId,
            userId:Cache.currentUser.user.id,
            businessRate:businessRate,
            businessFeedback:businessFeedback,
            driverRate:driverRate,
            driverFeedback:driverFeedback,
        }, (err, res)=>{
            cb(err, res)
        })
    }
}
