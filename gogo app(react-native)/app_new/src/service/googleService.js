import {
    AsyncStorage
} from 'react-native';

import * as async from 'async'
import * as _ from 'underscore'
import moment from 'moment'
import Cache from '../utils/Cache'
import UtilService from '../utils/util'
import * as config from '../config'
import Polyline from '@mapbox/polyline'
import Geocoder from 'react-native-geocoder'

const googleApiUrl = 'https://maps.googleapis.com/maps/api/'
const googleApiKey = 'AIzaSyBTyrdlrF8B60Va6y9Wx-aJasqaryaVzV4'


module.exports = {

    async googleRequest(url, cb) {
        try {
            let response = await fetch(googleApiUrl + url + `&key=${googleApiKey}`,
                {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                })
            let responseJson = await response.json();
            if (response.status == 200) {
                // console.log("======>1", responseJson)
                cb(null, responseJson);
            } else {
                // console.log("======>2", responseJson)
                cb(responseJson)
            }
        } catch (error) {
            // console.log("======>3", error)
            cb(error)
        }
    },

    getTripDuration(start, end, cb) {
        this.googleRequest(
            `distancematrix/json?origins=${start}&destinations=${end}&mode=driving`,
            (err, res) => {
                cb(err, res)
            })
    },

    getTripPath(start, end, cb){
        this.googleRequest(
            'directions/json?origin='+start+'&destination='+end+'&mode=driving',
            (err, res) => {
                let points = Polyline.decode(res.routes[0].overview_polyline.points)
                let path = points.map((point)=>{
                    return {latitude: point[0], longitude: point[1]}
                })
                cb(err, path)
            })
    },

    async getAddress(lng, lat, cb){
        this.googleRequest(
            'geocode/json?latlng='+lat+','+lng,
            (err, res) => {
                if ( err == null ){
                    cb(null, res.results[0])
                    return;
                }
                cb(err, null)
            })
    }
}
