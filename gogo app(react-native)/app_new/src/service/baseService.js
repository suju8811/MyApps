import {
    AsyncStorage,
    Platform,
} from 'react-native';
import * as async from 'async'
import * as _ from 'underscore'
import moment from 'moment'
import Cache from '../utils/Cache'
import UtilService from '../utils/util'
import * as config from '../config'

module.exports = {
    async uploadFile(file, cb) {
        try {
            let image = {
                uri: file,
                type: 'image/jpeg',
                name: 'file.jpeg'
            }

            let formData = new FormData();
            formData.append('path', 'users');
            formData.append('file', image);
            let response = await fetch(config.SERVICE_API_URL + '/uploads/image',
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'Authorization': 'Bearer ' + Cache.currentUser['token'],
                    },
                    body: formData
                })

            let status = response.status

            let responseJson = await response.json();
            if (status == 200) {
                cb(null, responseJson);
            } else {
                cb(responseJson.message)
            }
        } catch (error) {
            cb(error)
        }
    },

    async getHelpContent(cb) {
        try {
            let response = await fetch(config.SERVICE_API_URL + '/public/help/en',
                {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + Cache.currentUser['token'],
                    },
                })
            let responseJson = await response.json();
            if (response.status == 200) {
                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },

    async facebookLogin(data, cb) {
        try {
            let response = await fetch(config.SERVICE_API_URL + '/user/facebook/login',
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify([data, { onesignalPlayerId: Cache.device.userId }])
                })
            let responseJson = await response.json();
            if (response.status == 200) {
                Cache.currentUser = responseJson
                await UtilService.saveLocalObjectData('currentUser', responseJson)
                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },
    async getAds(cb) {
        try {
            let response = await fetch(config.SERVICE_API_URL + '/ads',
                {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + Cache.currentUser['token'],
                    },
                })
            let responseJson = await response.json();
            if (response.status == 200) {
                cb(null, responseJson);
            } else {
                cb(responseJson)
            }
        } catch (error) {
            cb(error)
        }
    },
}
