'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Keyboard,
    TouchableWithoutFeedback,
    Alert,
} from 'react-native';


import { Actions } from 'react-native-router-flux';
import CountryPicker from 'react-native-country-picker-modal'

import * as commonStyles from '../../style/commonStyles'
import * as commonColors from '../../style/commonColors';
import { screenWidth, screenHeight } from '../../style/commonStyles';

import NavTitleBar from '../../components/navTitle'

import authService from '../../service/authService';

export default class Signup extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            mobile: '',
            callingCode: '1',
            cca2: 'US',
            rightCallback:null,
        };
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    next() {
        Keyboard.dismiss();

        if (this.state.email == '') {
            Alert.alert('E-mail Required', 'Please enter your email address.');
            return;
        }

        if (this.state.password == '') {
            Alert.alert('Password Required', 'Please enter and confirm your password.');
            return;
        } else if (this.state.password.length < 5) {
            Alert.alert('Your password must be at least 5 characters.');
            return;
        } 


        authService.signup({
            email: this.state.email.trim(),
            password: this.state.password.trim(),
            phone: (this.state.mobile || '').trim(),
            phoneCode: this.state.callingCode,
            countyCode: this.state.cca2,
        }, (error, user) => {
            if (error) {
                // console.log(error);
                return;
            }
            Actions.Verification({email:this.state.email.trim(), password:this.state.password.trim()});
        })
    }

    goBack() {
        Actions.pop();
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.container}>
                <NavTitleBar
                    buttons={commonStyles.NavBackButton}
                    onBack={this.goBack}
                    title={'Sign in'}
                    rightText={'NEXT'}
                    rightCallback={this.state.rightCallback}
                />
                <View style={styles.content}>
                    <View style={styles.inputContainer}>
                        <View style={styles.inputTitle}>
                            <Text style={styles.inputTitleText}>Email</Text>
                        </View>
                        <View style={styles.input}>
                            <TextInput
                                ref="email"
                                autoCapitalize="none"
                                autoCorrect={false}
                                placeholder="name@example.com"
                                placeholderTextColor={commonColors.placeholderText}
                                textAlign="left"
                                style={styles.inputText}
                                underlineColorAndroid="transparent"
                                returnKeyType={'next'}
                                keyboardType="email-address"
                                value={this.state.email}
                                onChangeText={(text) => {
                                    this.setState({ email: text.replace(/\t/g, '') })
                                    if ( text != '' && this.state.password != '' && this.state.mobile != '' ){
                                        this.setState({ rightCallback: this.next.bind(this)})
                                    }else{
                                        this.setState({ rightCallback: null})
                                    }
                                }}
                                onSubmitEditing={() => this.refs.password.focus()}
                            />
                        </View>
                    </View>
                    <View style={styles.inputContainer}>
                        <View style={styles.inputTitle}>
                            <Text style={styles.inputTitleText}>Mobile</Text>
                        </View>
                        <View style={styles.input}>
                            <TextInput
                                ref="mobile"
                                autoCapitalize="none"
                                autoCorrect={false}
                                placeholder=""
                                secureTextEntry={false}
                                placeholderTextColor={commonColors.placeholderText}
                                textAlign="left"
                                style={styles.inputText}
                                underlineColorAndroid="transparent"
                                returnKeyType={'next'}
                                keyboardType="number-pad"
                                value={this.state.mobile}
                                onChangeText={(text) => {
                                    this.setState({ mobile: text.replace(/\t/g, '') })
                                    if ( text != '' && this.state.password != '' && this.state.email != '' ){
                                        this.setState({ rightCallback: this.next.bind(this)})
                                    }else{
                                        this.setState({ rightCallback: null})
                                    }
                                }}
                                onSubmitEditing={() => this.refs.password.focus()}
                            />

                        </View>
                        <View style={{ position: 'absolute', right: 0 }}>
                            <CountryPicker
                                onChange={(value) => {
                                    this.setState({ cca2: value.cca2, callingCode: value.callingCode });
                                }}
                                cca2={this.state.cca2}
                                translation='eng'
                                closeable={true}
                                styles={{ emojiFlag: { width: 20, height: 20, top: -4 } }}
                            />
                        </View>
                    </View>
                    <View style={styles.inputContainer}>
                        <View style={styles.inputTitle}>
                            <Text style={styles.inputTitleText}>Password</Text>
                        </View>
                        <View style={styles.input}>
                            <TextInput
                                ref="password"
                                autoCapitalize="none"
                                autoCorrect={false}
                                placeholder="Min 5 characters"
                                secureTextEntry={true}
                                placeholderTextColor={commonColors.placeholderText}
                                textAlign="left"
                                style={styles.inputText}
                                underlineColorAndroid="transparent"
                                returnKeyType={'go'}
                                value={this.state.password}
                                onChangeText={(text) => {
                                    this.setState({ password: text.replace(/\t/g, '') })
                                    if ( text != '' && this.state.email != '' && this.state.mobile != '' ){
                                        this.setState({ rightCallback: this.next.bind(this)})
                                    }else{
                                        this.setState({ rightCallback: null})
                                    }
                                }}
                                onSubmitEditing={() => this.next()}
                            />

                        </View>
                    </View>
                    <View style={styles.descriptionContainer}>
                        <Text style={styles.description}>
                            We use your email and mobile number to send you order confirmation and receipts.
                    </Text>
                    </View>
                    {/*<Image
                    style={{ width: '100%', height: '100%' }}
                    source={commonStyles.signinImg}
                />*/}
                </View>
            </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: commonColors.background,
    },
    content: {
        paddingTop: 16,
        paddingHorizontal: 8,
    },
    contentTitle: {
        height: 78,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentTitleText: {
        fontSize: 12,
        color: commonColors.normalText,
    },
    inputContainer: {
        flexDirection: 'row',
        height: 48,
        width: '100%',
        borderColor: commonColors.borderColor,
        borderWidth: 0.5,
        backgroundColor: 'white',
        justifyContent: 'center',
    },
    descriptionContainer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 20,
    },
    description: {
        fontSize: 12,
        color: commonColors.textColor2,
        textAlign: 'center'
    },
    inputTitle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    input: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    inputTitleText: {
        color: commonColors.textColor1,
        fontSize: 15,
        fontWeight: 'bold'
    },
    inputText: {
        fontSize: 14,
        color: 'black',
        height: 50,
        alignSelf: 'stretch',
        marginLeft: 15,
        marginRight: 40,
    }
});
