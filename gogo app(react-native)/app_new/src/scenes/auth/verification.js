'use strict';

import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    ListView,
    TextInput,
    TouchableOpacity,
    Alert,
    Linking,
    Platform,
    TouchableHighlight,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import CodeInput from 'react-native-confirmation-code-input';

import NavTitleBar from '../../components/navTitle';

import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';
import { screenWidth, screenHeight } from "../../style/commonStyles";

import authService from '../../service/authService'
import Cache from '../../utils/Cache'

class Verification extends PureComponent {
    constructor(props) {
        super(props);

    }

    componentWillMount() {

    }

    onBack() {
        Actions.pop()
    }


    onFulfill(code) {
        authService.verification({
            email: Cache.currentUser.user.email,
            role: 'user',
            code: code
        }, (err, res) => {
            if (err == null) {
                Actions.Login({email:this.props.email, password:this.props.password})
            }else{
                Alert.alert("incorrect code!")
            }
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <NavTitleBar
                    buttons={commonStyles.NavCloseButton}
                    onBack={this.onBack}
                    title={'VERIFY MOBITE'}
                />
                <View style={styles.titleContainer}>
                    <Text style={styles.titleText}>Please enter your verification code</Text>
                    <Text style={styles.underText}>that was sent to your mobile number</Text>
                </View>

                <View style={styles.inputContainer}>
                    <CodeInput
                        ref="codeInput"
                        codeLength={4}
                        inputPosition='left'
                        onFulfill={(code) => this.onFulfill(code)}
                        inactiveColor={'rgb(86, 86, 86)'}
                        activeColor={'rgb(64, 64, 64)'}
                        size={68}
                        space={8}
                        keyboardType="numeric"
                        codeInputStyle={{ margin: 8, marginTop: 0, borderRadius: 3, backgroundColor: 'white' }}
                    />
                </View>

                <View style={styles.commandContainter}>
                    <View style={styles.resend}>
                        <TouchableOpacity
                            onPress={() => { }}>
                            <Text style={styles.commandText}>RESEND THE TEXT</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.change}>
                        <TouchableOpacity
                            onPress={()=>{}}>
                            <Text style={styles.commandText}>CHANGE MOBILE NUMBER</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        )

    }
}

export default Verification;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: commonColors.background
    },
    inputContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
    },
    titleContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleText: {
        color: commonColors.textColor3,
        fontSize: 18,
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: 10,
    },
    underText: {
        color: commonColors.textColor3,
        fontSize: 13,
        textAlign: 'center',
    },
    commandContainter: {
        margin: 20,
        flexDirection: 'row'
    },
    resend: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    change: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    commandText: {
        color: commonColors.lightTheme,
        fontWeight: 'bold',
        fontSize: 12,
    }
})
