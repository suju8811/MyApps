'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Keyboard,
    TouchableWithoutFeedback,
    Alert,
} from 'react-native';


import { Actions } from 'react-native-router-flux';

import * as commonStyles from '../../style/commonStyles'
import * as commonColors from '../../style/commonColors';
import { screenWidth, screenHeight } from '../../style/commonStyles';

import NavTitleBar from '../../components/navTitle'

import authService from '../../service/authService';
import Cache from '../../utils/Cache'

export default class Login extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            email: this.props.email||'',
            password: this.props.password||'',
            rightCallback: null,
        };
        if ( this.props.email && this.props.password ){
            this.Done()
        }
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    onForgotPassword() {
        Actions.Forgot()
    }

    onCreateAccount() {
        Actions.Signup();
    }

    onToggleConfirmPassword() {
    }

    goBack() {
        Actions.Introduce();
    }

    Done() {
        Keyboard.dismiss();

        if (this.state.email == '') {
            Alert.alert('Please enter your email address.');
            return;
        }

        if (this.state.password == '') {
            Alert.alert('Please enter your password.');
            return;
        }

        

        authService.login((this.state.email || '').trim(),
            (this.state.password || '').trim(), (error, user) => {
                console.log(error, user)
                if (error) {
                    this.setState({
                        password: '',
                    });

                    /*timer.setTimeout(this, 'LoginFailed', () => {
                        timer.clearTimeout(this, 'LoginFailed');
                        Alert.alert("Invalid credentials. Please check your email and password and try again.")
                    }, 200);*/
                    Alert.alert("Invalid credentials. Please check your email and password and try again.")
                    // console.log("login", error)
                    return
                }
                Actions.SelectLocation({
                    update: () => {
                        this.setState({ address: Cache.selectedLocation.address })
                        setTimeout(()=>Actions.Main(),20);
                    }
                })
            })
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.container}>
                <NavTitleBar
                    buttons={commonStyles.NavBackButton}
                    onBack={this.goBack}
                    title={'Sign in'}
                    rightText={'DONE'}
                    rightCallback={this.state.rightCallback}
                />
                <View style={styles.contentTitle}>
                    <Text style={styles.contentTitleText}>
                        LOG IN WITH YOUR GOGO ACCOUNT
                    </Text>
                </View>
                <View style={styles.inputContainer}>
                    <View style={styles.inputTitle}>
                        <Text style={styles.inputTitleText}>Email</Text>
                    </View>
                    <View style={styles.input}>
                        <TextInput
                            ref="email"
                            autoCapitalize="none"
                            autoCorrect={false}
                            placeholder="name@example.com"
                            placeholderTextColor={commonColors.placeholderText}
                            textAlign="left"
                            style={styles.inputText}
                            underlineColorAndroid="transparent"
                            returnKeyType={'next'}
                            keyboardType="email-address"
                            value={this.state.email}
                            onChangeText={(text) => {
                                this.setState({ email: text.replace(/\t/g, '') })
                                if ( text != '' && this.state.password != '' ){
                                    this.setState({ rightCallback: this.Done.bind(this)})
                                }else{
                                    this.setState({ rightCallback: null})
                                }
                            }}
                            onSubmitEditing={() => this.refs.password.focus()}
                        />
                    </View>
                </View>
                <View style={styles.inputContainer}>
                    <View style={styles.inputTitle}>
                        <Text style={styles.inputTitleText}>Password</Text>
                    </View>
                    <View style={styles.input}>
                        <TextInput
                            ref="password"
                            autoCapitalize="none"
                            autoCorrect={false}
                            placeholder=""
                            secureTextEntry={true}
                            placeholderTextColor={commonColors.placeholderText}
                            textAlign="left"
                            style={styles.inputText}
                            underlineColorAndroid="transparent"
                            returnKeyType={'go'}
                            value={this.state.password}
                            onChangeText={(text) => {
                                this.setState({ password: text.replace(/\t/g, '') })
                                if ( this.state.email != '' && text != '' ){
                                    this.setState({ rightCallback: this.Done.bind(this)})
                                }else{
                                    this.setState({ rightCallback: null})
                                }
                            }}
                            onSubmitEditing={() => this.Done()}
                        />
                    </View>
                </View>
                <View style={styles.forgot}>
                    <TouchableOpacity style={{ marginTop: 25 }} onPress={()=>this.onForgotPassword()}>
                        <Text style={styles.forgotText}>
                            FORGOT PASSWORD?
                        </Text>
                    </TouchableOpacity>
                    <View style={{flex:1}}/>
                    <TouchableOpacity style={styles.register}
                        onPress={()=>Actions.Signup()}>
                        <Text style={styles.registerText}>REGISTER</Text>
                    </TouchableOpacity>
                </View>
                {/*<Image
                    style={{ width: '100%', height: '100%' }}
                    source={commonStyles.signinImg}
                />*/}
            </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: commonColors.background,
    },
    contentTitle: {
        height: 78,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentTitleText: {
        fontSize: 12,
        color: commonColors.normalText,
    },
    inputContainer: {
        flexDirection: 'row',
        height: 48,
        width: '100%',
        borderColor: commonColors.borderColor,
        borderWidth: 0.5,
        backgroundColor: 'white',
        justifyContent: 'center',
    },
    forgot: {
        flex:1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    forgotText: {
        fontSize: 10,
        color: commonColors.theme,
        fontWeight: 'bold',
    },
    inputTitle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    input: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    inputTitleText: {
        color: commonColors.textColor1,
        fontSize: 15,
        fontWeight: 'bold'
    },
    inputText: {
        fontSize: 14,
        color: 'black',
        height: 50,
        alignSelf: 'stretch',
        marginHorizontal: 20,
    }, 
    register:{
        marginBottom:35,
        borderWidth:1,
        borderColor:commonColors.normalText,
        borderRadius:3,
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal:30,
        paddingVertical:7,
    },
    registerText:{
        color:commonColors.normalText,
        fontSize:13,
        fontWeight:'bold'
    }
});
