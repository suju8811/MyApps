'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    Platform,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import * as commonStyles from '../style/commonStyles'
import * as commonColors from '../style/commonColors'

export default class Introduce extends PureComponent {
    constructor(props) {
        super(props);

        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    onLogin() {
        Actions.Login();
    }

    onRegister() {
        Actions.Signup();
    }

    render() {
        return (
            <View>
                <Image
                    source={commonStyles.introduceImg}
                    style={styles.background}
                >
                    <View style={{ flex: 1 }} />

                    <TouchableOpacity
                        style={styles.login}
                        onPress={() => this.onLogin()}>
                        <Text style={styles.loginText}>Log in with GoGo</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.register}
                        onPress={() => this.onRegister()}>
                        <Text style={styles.registerText}>Register</Text>
                    </TouchableOpacity>
                </Image>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    background: {
        width: commonStyles.screenWidth,
        height: commonStyles.screenHeight,
    },
    loginText: {
        color: '#fff',
        fontSize: 14,
        fontWeight:'bold',
        fontFamily: 'openSans',
    },
    registerText: {
        color: '#fff',
        fontSize: 12,
        fontFamily: 'openSans',
        fontWeight:'bold',
        color: commonColors.theme
    },
    login: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        backgroundColor: commonColors.theme,
        borderRadius: 3,
        marginHorizontal: 8,
        marginVertical:5
    },
    register: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: 46,
        marginHorizontal: 8,
        marginBottom:5
    },
});