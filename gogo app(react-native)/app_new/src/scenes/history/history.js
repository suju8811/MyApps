'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import IonIcons from 'react-native-vector-icons/Ionicons'

import * as commonColors from '../../style/commonColors'
import * as commonStyles from '../../style/commonStyles'
import { screenHeight, screenWidth } from '../../style/commonStyles';
import OrderItem from '../../components/history/orderItem'
import UpcomingOrderItem from '../../components/history/upcomingOrderItem'

import Cache from '../../utils/Cache'
import * as _ from 'underscore'
import orderService from '../../service/orderService'


const carouselLeftMargin = (commonStyles.carouselerWidth - commonStyles.carouselItemWidth) / 2 - commonStyles.carouselItemHorizontalPadding;


export default class History extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            index: 0,
            routes: [
                { key: '1', title: 'PAST ORDERS' },
                { key: '2', title: 'UPCOMING' },
            ],
            pastOrders: [],
            upcomingOrders: [],
            changes: this.props.changes,
        };
    }

    componentDidMount() {
        this.refresh()
    }

    componentWillReceiveProps(nextProps) {
        //console.log('history props', this.props.changes, nextProps.changes)
        if (this.state.changes != nextProps.changes) {
            this.setState({
                changes: nextProps.changes,
            })
            this.refresh()
        }
    }

    refresh() {
        orderService.getPastOrders(0, 15, (err, res) => {
            //(err, res)
            if (err == null) this.setState({ pastOrders: res.items })
        })
        orderService.getUpcomingOrders(0, 15, (err, res) => {
            if (err == null) this.setState({ upcomingOrders: res.items })
            console.log('----upcomingOrders----->')
        })
    }

    onCheck() {
        Actions.ViewCart({
            updateData: () => {
                this.forceUpdate()
            }
        });
    }

    renderCheckout() {
        var count = 0
        var price = 0
        _.map(Cache.order, (item) => {
            count = count + item.count
            price = price + item.count * item.price
        })
        if (Cache.order != undefined && Cache.order.length > 0) {
            return (
                <TouchableOpacity onPress={this.onCheck.bind(this)} style={{
                    position: 'absolute', top: screenHeight - 93, width: screenWidth, height: 48, justifyContent: 'center',
                    alignItems: 'center', backgroundColor: commonColors.theme, flexDirection: 'row'
                }}>
                    <View style={{ flex: 2, alignItems: 'flex-start', justifyContent: 'center', paddingLeft: 15 }}><Text style={{ color: '#ffffff', fontSize: 14 }}>${price}</Text></View>
                    <View style={{ flex: 4, alignItems: 'center', justifyContent: 'center' }}><Text style={{ color: '#ffffff', fontSize: 16, fontWeight: '700' }}>CHECK OUT</Text></View>
                    <View style={{ flex: 2, alignItems: 'flex-end', justifyContent: 'center', paddingRight: 15 }}><Text style={{ color: '#ffffff', fontSize: 14, paddingHorizontal: 5, borderRadius: 3, borderWidth: 1, borderColor: 'white' }}>{count}</Text></View>
                </TouchableOpacity>
            )
        }
        return null
    }

    _handleIndexChange = index => this.setState({ index });

    _renderHeader = props =>
        <TabBar {...props}
            indicatorStyle={{ backgroundColor: 'black', width: 120, marginLeft: screenWidth / 4 - 60 }}
            labelStyle={{ color: commonColors.normalText, fontSize: 12.5, fontWeight: 'bold' }}
            style={styles.tabBar} />;

    renderPastOrder() {
        return (
            <View style={{ height: screenHeight - 100, width: screenWidth, backgroundColor: 'white' }}>
                <ScrollView style={{ backgroundColor: 'white' }}>
                    {this.state.pastOrders.length == 0 && <View style={{ position: 'absolute', width: '100%', height: '80%', justifyContent: 'center', alignItems: 'center' }}>
                        <IonIcons name="ios-document-outline" size={80} color={commonColors.textColor1} />
                        <Text style={{ fontSize: 15, color: commonColors.textColor2 }}>No orders yet</Text>
                    </View>}
                    {this.state.pastOrders.length == 0 && <TouchableOpacity activeOpacity={0.8} style={{
                        position: 'absolute', bottom: 0, backgroundColor: commonColors.theme,
                        height: 48, width: '100%', justifyContent: 'center', alignItems: 'center'
                    }}>
                        <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>BROWSE RESTAURANTS</Text>
                    </TouchableOpacity>}

                    {this.state.pastOrders.length > 0 &&
                        <View>
                            {this.state.pastOrders.map((item, index) => {
                                return (
                                    <OrderItem key={index} data={item} />
                                )
                            })}
                        </View>
                    }
                </ScrollView>
            </View>
        )
    }

    renderUpcoming() {
        return (
            <View style={{ height: screenHeight - 100, width: screenWidth, backgroundColor: 'white' }}>
                <ScrollView style={{ backgroundColor: 'white' }}>
                    {this.state.upcomingOrders.length == 0 && <View style={{ position: 'absolute', width: '100%', height: '80%', justifyContent: 'center', alignItems: 'center' }}>
                        <IonIcons name="ios-document-outline" size={80} color={commonColors.textColor1} />
                        <Text style={{ fontSize: 15, color: commonColors.textColor2 }}>No upcoming orders</Text>
                    </View>}
                    {this.state.upcomingOrders.length == 0 && <TouchableOpacity activeOpacity={0.8} style={{
                        position: 'absolute', bottom: 0, backgroundColor: commonColors.theme,
                        height: 48, width: '100%', justifyContent: 'center', alignItems: 'center'
                    }}>
                        <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>BROWSE RESTAURANTS</Text>
                    </TouchableOpacity>}

                    {this.state.upcomingOrders.length > 0 &&
                        <View>
                            {this.state.upcomingOrders.map((item, index) => {
                                return (
                                    <UpcomingOrderItem key={index} data={item} />
                                )
                            })}
                        </View>
                    }
                </ScrollView>
            </View>
        )
    }

    _renderScene = ({ route }) => {
        switch (route.key) {
            case '1':
                return this.renderPastOrder()
                break
            case '2':
                return this.renderUpcoming()
                break
            default:
                return null;
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <TabViewAnimated
                    style={styles.tabview}
                    navigationState={this.state}
                    renderScene={this._renderScene}
                    renderHeader={this._renderHeader}
                    onIndexChange={this._handleIndexChange}
                />
                {this.renderCheckout()}
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: commonColors.backgroundColor,
    },
    tabview: {
        flex: 1,
        marginTop: 20,
    },
    tabBar: {
        backgroundColor: 'white'
    },

    textTitle: {
        color: commonColors.grayMoreText,
        //fontFamily: 'OpenSans-Semibold',
        fontSize: 14,
        padding: 10,
    },
    page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
})
