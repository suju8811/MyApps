'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Keyboard,
    ListView,
    ScrollView,
    TouchableWithoutFeedback,
    Alert,
    Platform,
    StatusBar,
} from 'react-native';


import { Actions } from 'react-native-router-flux';

import * as commonStyles from '../../style/commonStyles'
import * as commonColors from '../../style/commonColors';
import { screenWidth, screenHeight } from '../../style/commonStyles';

import NavTitleBar from '../../components/navTitle'
import VerticalRestaurants from '../../components/home/verticalRestaurants'

import authService from '../../service/authService';
import * as config from '../../config'

export default class ShowResult extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            title: props.title,
            items: props.data,
        }

        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    goBack() {
        Actions.pop();
    }

    rightCallback() {

    }

    render() {
        return (
            <View style={styles.container}>
                <NavTitleBar
                    buttons={commonStyles.NavBackButton}
                    onBack={this.goBack}
                    title={this.state.title}
                />
                <ScrollView>
                    <VerticalRestaurants
                        dataList={this.state.items}
                        title={this.state.items.length + ' Results'} />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
                    container: {
                    flex: 1,
    },
});

