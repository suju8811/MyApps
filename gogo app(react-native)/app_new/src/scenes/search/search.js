'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    TextInput,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import IonIcons from 'react-native-vector-icons/Ionicons'
import { createFilter } from 'react-native-search-filter';

import * as commonColors from '../../style/commonColors'
import * as commonStyles from '../../style/commonStyles'
import { screenHeight, screenWidth } from '../../style/commonStyles';

import Cache from '../../utils/Cache'
import businessService from '../../service/businessService'
import * as _ from 'underscore'
import * as config from '../../config'

export default class Search extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            groupIndex: -1,
            itemIndex: -1,
            categories: [
                {
                    title: 'Top categories',
                    items: []
                },
                {
                    title: 'More categories',
                    items: []
                },
            ],
            allCategory: [],
            searchResult: [],
            searchText: '',
            searchFocus: false,
        }
    }

    onCheck() {
        Actions.ViewCart({
            updateData: () => {
                this.forceUpdate()
            }
        });
    }

    renderRow(item, index) {
        return (
            <TouchableOpacity key={index}
                onPress={() => this.onPressItem(item.code, item.name)}
                style={{
                    flexDirection:'row',
                    height: 50,
                    padding: 10,
                    alignItems: 'center'
                }}
            >
                <IonIcons name="ios-search-outline" size={15} color={commonColors.textColor1} style={{backgroundColor:'transparent'}}/>
                <Text style={{ color: 'black', fontSize:15, marginLeft:10, backgroundColor:'transparent' }}>{item.name}</Text>
            </TouchableOpacity>
        );
    }

    get ViewSearch() {
        if (this.state.searchFocus) {
            return (
                <ScrollView style={{ paddingHorizontal: 10 }}>
                    {
                        this.state.searchResult.map((item, index) => {
                            return this.renderRow(item, index)
                        })
                    }
                </ScrollView>
            )
        }
    }

    renderCheckout() {
        var count = 0
        var price = 0
        _.map(Cache.order, (item) => {
            count = count + item.count
            price = price + item.count * item.price
        })
        if (Cache.order != undefined && Cache.order.length > 0) {
            return (
                <TouchableOpacity onPress={this.onCheck.bind(this)} style={{
                    position: 'absolute', top: screenHeight - 113, width: screenWidth, height: 48, justifyContent: 'center',
                    alignItems: 'center', backgroundColor: commonColors.theme, flexDirection: 'row'
                }}>
                    <View style={{ flex: 2, alignItems: 'flex-start', justifyContent: 'center', paddingLeft: 15 }}><Text style={{ color: '#ffffff', fontSize: 14 }}>${price}</Text></View>
                    <View style={{ flex: 4, alignItems: 'center', justifyContent: 'center' }}><Text style={{ color: '#ffffff', fontSize: 16, fontWeight: '700' }}>CHECK OUT</Text></View>
                    <View style={{ flex: 2, alignItems: 'flex-end', justifyContent: 'center', paddingRight: 15 }}><Text style={{ color: '#ffffff', fontSize: 14, paddingHorizontal: 5, borderRadius: 3, borderWidth: 1, borderColor: 'white' }}>{count}</Text></View>
                </TouchableOpacity>
            )
        }
        return null
    }

    componentDidMount() {
        businessService.getCategories((err, res) => {

            if (err == null) {
                var top = []
                var more = []
                this.state.allCategory = []
                _.map(res.items, (item) => {
                    if (item.top == true) {
                        this.state.categories[0].items.push(item)
                    } else {
                        this.state.categories[1].items.push(item)
                    }
                    this.state.allCategory.push(item)
                })
                //this.setState({categories:this.state.categories})
                this.forceUpdate()
                //this.setState({ categories: top, moreCategories: more, allCategory: all, searchResult: all })
                //this.state.allCategory.sort()
                // console.log(res)
            } else {
                // console.log(err)
            }
        })
    }

    onPressItem(code, title) {
        businessService.getBesinesses({
            lng: Cache.selectedLocation.geoJson.coordinates[0],
            lat: Cache.selectedLocation.geoJson.coordinates[1],
            placeId: Cache.selectedLocation.placeId,
            sort: 0,
            price: [],
            dietary: [code],
        }, (err, res) => {
            if (err == null) {
                Actions.ShowResult({ data: res[0].items, title: title })
            } else {
            }
        })
    }

    renderCategory(category, index) {
        let margin = 16
        let interval = 8
        let itemWidth = (screenWidth - 2 * margin - interval) / 2
        let itemHeight = 136
        let totalHeight = (itemHeight + interval) * (Math.trunc((category.items.length - 1) / 2) + 1)
        let totalWidth = screenWidth

        return (
            <View key={index}>
                <Text style={{
                    fontSize: 12, color: commonColors.textColor2,
                    marginHorizontal: margin, marginTop: 10, backgroundColor: 'transparent'
                }}>
                    {category.title}
                </Text>
                <View style={{ margin: margin, width: totalWidth, height: totalHeight }}>
                    {category.items.map((item, index2) => {
                        let backgroundColor = this.state.groupIndex == index && this.state.itemIndex == index2 ? '#20200040' : '#202000a0'
                        return (
                            <TouchableOpacity onPress={() => this.onPressItem(item.code, item.name)} activeOpacity={1} key={index2}
                                onPressIn={() => this.setState({ groupIndex: index, itemIndex: index2 })}
                                onPressOut={() => this.setState({ groupIndex: -1, itemIndex: -1 })}
                                style={{
                                    width: itemWidth, height: itemHeight, position: 'absolute',
                                    top: Math.trunc(index2 / 2) * (itemHeight + interval), left: (index2 % 2) * (itemWidth + interval),
                                    backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center'
                                }}>
                                <Image style={{
                                    width: itemWidth, height: itemHeight,
                                    position: 'absolute'
                                }} source={ item.image == '' ? null : { uri: config.SERVICE_FILE_URL + item.image }} />
                                <View style={{
                                    width: itemWidth, height: itemHeight,
                                    position: 'absolute',
                                    backgroundColor: backgroundColor
                                }} />
                                <Text style={{ color: 'white', fontSize: 18, backgroundColor: 'transparent' }}>{item.name}</Text>
                            </TouchableOpacity>
                        )
                    })}
                </View>
            </View>
        )
    }

    renderSearchBar() {
        return (
            <View activeOpacity={1} style={{
                width: '100%', height: 54,
                flexDirection: 'row', alignItems: 'center', shadowColor: '#aaaaaa',
                shadowOpacity: 0.2, shadowRadius: 3, shadowOffset: { height: 5 }
            }}>
                <IonIcons name="md-search" size={22} color={commonColors.textColor2} style={{ margin: 15, marginTop: 20 }} />
                <TextInput
                    ref="search"
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholder="Search"
                    placeholderTextColor={commonColors.placeholderText}
                    textAlign="left"
                    style={styles.inputText}
                    underlineColorAndroid="transparent"
                    returnKeyType={'go'}
                    value={this.state.searchText}
                    onChangeText={(text) => {
                        this.setState({ searchResult: this.state.allCategory.filter(createFilter(text, ['name'])) })
                        this.setState({ searchText: text })
                    }}
                    onFocus={() => {
                        this.setState({ searchResult: this.state.allCategory })
                        this.setState({ searchFocus: true })
                    }}
                />
                <View style={{ height: 40, width: 0.5, backgroundColor: commonColors.background }} />
                {this.state.searchFocus&&<TouchableOpacity style={{ margin: 15 }} onPress={() => {
                    this.setState({ searchFocus: false })
                    this.refs.search.blur()
                }}>
                    <Text style={{ color: 'black', fontSize: 10 }}>CANCEL</Text>
                </TouchableOpacity>}
            </View>
        )
    }

    renderCategories() {
        if (this.state.searchFocus) return null
        return (
            <ScrollView>
                {this.state.categories.map((item, index) => {
                    return this.renderCategory(item, index)
                })}
            </ScrollView>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderSearchBar()}
                {this.ViewSearch}
                {this.renderCategories()}
                {this.renderCheckout()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: 20
    },
    inputText: {
        flex: 1,
    }
})
