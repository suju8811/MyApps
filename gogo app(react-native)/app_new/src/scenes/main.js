'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    StatusBar,
    Platform,
    Modal,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import TabNavigator from 'react-native-tab-navigator';

import Home from './home/home';
import Search from './search/search';
import History from './history/history';
import Setting from './setting/setting';

import * as commonColors from '../style/commonColors';
import * as commonStyles from '../style/commonStyles';
import { screenWidth, screenHeight } from '../style/commonStyles';

import Cache from '../utils/Cache'

export default class Main extends PureComponent {
    constructor(props) {
        super(props);

        let tab = this.props.tab;

        if (tab == null) {
            tab = 'home';
        }

        this.state = {
            selectedTab: tab,
            badge: 0,
            searchAutoFocus: false,
            showingModal: false,
            countByCategory: {},
            firstLogin: this.props.firstLogin == undefined ? false : true,
            showModal: false,
            changes: 0,
        };

        this.last = null;

        StatusBar.setHidden(false);

        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }


    componentDidMount() {
        Cache.bookingFee = 1.9
        Cache.location = Cache.currentUser.user.homeLocation
    }

    onSelectTab(tab) {
        this.setState({
            selectedTab: tab,
        });
        
        if (tab == 'history') {
            this.setState((old) => {
                old.changes = this.state.changes + 1
            })
        }
    }

    render() {
        let { user } = Cache.currentUser;
        return (
            <View style={styles.container}>
                <TabNavigator
                    tabBarStyle={styles.tab}
                >
                    {/* Home */}
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'home'}
                        selectedTitleStyle={styles.selectedText}
                        titleStyle={styles.text}
                        renderIcon={() => <Image source={commonStyles.homeIcon} style={styles.iconTabbarHome} />}
                        renderSelectedIcon={() => <Image source={commonStyles.homeSelectedIcon} style={styles.iconTabbarHome} />}
                        onPress={() => this.onSelectTab('home')}>
                        {<Home />}
                    </TabNavigator.Item>

                    {/* Search */}
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'search'}
                        selectedTitleStyle={styles.selectedText}
                        titleStyle={styles.text}
                        renderIcon={() => <Image source={commonStyles.searchIcon} style={styles.iconTabbar} />}
                        renderSelectedIcon={() => <Image source={commonStyles.searchSelectedIcon} style={styles.iconTabbar} />}
                        onPress={() => this.onSelectTab('search')}>
                        {<Search />}
                    </TabNavigator.Item>

                    {/* History */}
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'history'}
                        selectedTitleStyle={styles.selectedText}
                        titleStyle={styles.text}
                        renderIcon={() => 
                            <View style={{paddingTop:3}}>
                                <Image source={commonStyles.activityIcon} style={styles.iconTabbar} resizeMode="contain" />
                                <View style={{position:'absolute', right:0, top: 0, width:8, height:8, borderRadius:4, 
                                    backgroundColor:commonColors.theme, borderColor:'white', borderWidth:1}}/>
                            </View>
                        }
                        renderSelectedIcon={() => <View style={{paddingTop:3}}>
                            <Image source={commonStyles.activitySelectedIcon} style={styles.iconTabbar} resizeMode="contain" />
                            <View style={{position:'absolute', right:0, top: 0, width:8, height:8, borderRadius:4, 
                                    backgroundColor:commonColors.theme, borderColor:'white', borderWidth:1}}/>
                        </View>}
                        badgeText={this.state.badge}
                        onPress={() => this.onSelectTab('history')}>
                        <History changes={this.state.changes} />
                    </TabNavigator.Item>

                    {/* Setting */}
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'setting'}
                        selectedTitleStyle={styles.selectedText}
                        titleStyle={styles.text}
                        renderIcon={() => <Image source={commonStyles.userIcon} style={styles.iconTabbar} />}
                        renderSelectedIcon={() => <Image source={commonStyles.userSelectedIcon} style={styles.iconTabbar} />}
                        onPress={() => this.onSelectTab('setting')}>
                        <Setting />
                    </TabNavigator.Item>
                </TabNavigator>

                {/* check documents */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showModal}
                >
                    <View style={styles.modal}>
                        <Text>{commonStyles.checkDocumentText}</Text>
                        <View style={styles.modalContent}>
                            <TouchableOpacity onPress={() => {
                                this.setState({ showModal: false, firstLogin: false })
                                Actions.vehicle({ cityId: user.locationPlaceId })
                            }}>
                                <Text style={{ color: '#0cc5e7' }}>OK</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    background: {
        width: screenWidth,
        height: screenHeight,
    },
    iconTabbarHome: {
        width: 27,
        height: 27,
    },
    iconTabbar: {
        width: 23,
        height: 23,
    },
    text: {
        fontFamily: 'Open Sans',
        fontSize: 10,
    },
    selectedText: {
        color: commonColors.theme,
        fontFamily: 'Open Sans',
        fontSize: 10,
    },
    tab: {
        borderStyle: 'solid',
        borderTopWidth: 1,
        borderTopColor: '#edefee',
        backgroundColor: 'white',
    },
    modal: {
        flexDirection: 'column',
        height: 100,
        width: screenWidth - 60,
        position: "absolute",
        marginHorizontal: 30,
        top: (screenHeight - 120) / 2,
        backgroundColor: '#ffffff',
        padding: 20,
        shadowRadius: 5,
        shadowColor: 'black',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2
    },
    modalContent: {
        flexDirection: 'row',
        paddingTop: 20,
        justifyContent: 'flex-end'
    }
});
