'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Keyboard,
    TouchableWithoutFeedback,
    Alert,
    ScrollView,
    Platform,
    StatusBar,
} from 'react-native';


import { Actions } from 'react-native-router-flux';
import Swipeout from 'react-native-swipeout'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as commonActions from '../../redux/actions';
import * as commonActionTypes from '../../redux/actionTypes';

import * as commonStyles from '../../style/commonStyles'
import * as commonColors from '../../style/commonColors';
import { screenWidth, screenHeight } from '../../style/commonStyles';

import NavTitleBar from '../../components/navTitle'
import FavoriteItem from '../../components/setting/favoriteItem'

import businessService from '../../service/businessService';
import * as _ from 'underscore'
import * as config from '../../config'

class Favorite extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            rightMenuText: 'EDIT',
            editing: false,
            favorites: [],
            isEditable: false,
            checked: true,
        }
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }


    componentWillMount() {

        businessService.getFavorite((err, res) => {
            _.map(res, (item) => {
                var detail = ''
                if (item.dietaries) {
                    for (var i = 0; i < item.dietaries.length; i++) {
                        detail = detail + item.dietaries[i] + ' • '
                    }
                }
                detail = detail + config.PRICE_LEVEL[item.priceLevel]
                this.state.favorites.push({
                    title: item.name,
                    image: item.logo ? { uri: config.SERVICE_FILE_URL + item.logo } : null,
                    description: detail,
                    preparation: config.PREPARATION[item.preparationTime],
                    id: item.id,
                    closed: item.closed,
                    rawData: item,
                    visible:true,
                })
            })

            this.forceUpdate()
            
        })
    }

    onBack() {
        Actions.pop()
    }

    goCancelMode() {
        this.setState({ isEditable: false });

    }

    goEditableMode() {
        this.setState({ isEditable: true });

    }


    rightCallback() {
        if (this.state.rightMenuText == 'EDIT') {
            this.setState({ rightMenuText: 'CANCEL' })
        } else {
            this.setState({ rightMenuText: 'EDIT' })
        }
    }

    renderItem(item, index, bLast) {
        let rightButtons = [{
            component: (
                <View style={{ justifyContent: 'center', alignItems: 'center', height: '100%', backgroundColor: 'rgb(215,63,13)' }}>
                    <Text style={{ color: 'white', fontSize: 13, fontWeight: 'bold' }}>REMOVE</Text>
                </View>
            ),
            onPress: () => {
                businessService.changeFavorite(item.rawData.id, (err, res) => {
                    if (err) {
                        // console.log('heart->', err);
                    } else {
                        this.props.commonActions.changeFavoriteState(this.state.favorites[index].id)
                        this.state.favorites[index].visible = false;
                        this.setState({favorites:[...this.state.favorites]})
                    }
                })
            },
        }]

        return (
            
            <View key={index}>
                {item.visible != false &&<Swipeout disabled={this.state.editing} right={rightButtons}
                    style={{ backgroundColor: 'white' }} buttonWidth={90}>
                    <TouchableOpacity onPress={() => Actions.Business({ data: item.rawData })}>
                        <FavoriteItem editing={this.state.editing} data={item} />
                    </TouchableOpacity>
                    {bLast != true && <View style={{ backgroundColor: commonColors.background, height: 0.5, width: '100%' }} />}
                </Swipeout>}
            </View>
        )
    }

    renderGroup(group, index) {
        return (
            <View key={index} style={{ marginTop: 8, backgroundColor: 'white', paddingTop: 24 }}>
                <Text style={{ color: commonColors.textColor2, fontSize: 15, marginLeft: 16 }}>{group.title}</Text>
                {group.items.map((item, index2) => {
                    let bLast = (index2 == group.items.length)
                    return this.renderItem(item, index2, bLast)
                })}
                {group.status == 'disabled' && <View style={{ backgroundColor: 'rgba(255, 255, 255, 0.5)', width: '100%', height: '100%' }} />}
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <NavTitleBar
                    buttons={commonStyles.NavBackButton}
                    onBack={this.onBack}
                    title={'Your Favorites'}
                    //rightText={this.state.rightMenuText}
                    //rightCallback={this.rightCallback.bind(this)}
                />
                <ScrollView>
                    {this.state.favorites.map((item, index) => {
                        return this.renderItem(item, index, (index == this.state.favorites.length - 1))
                    })}
                </ScrollView>
            </View>
        );
    }
}

export default connect(props => ({
}),
    (dispatch) => ({
        commonActions: bindActionCreators(commonActions, dispatch),
    })
)(Favorite);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: commonColors.background,
    },

});
