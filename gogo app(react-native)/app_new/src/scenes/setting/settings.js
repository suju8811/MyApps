'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Keyboard,
    TouchableWithoutFeedback,
    Alert,
    Platform,
    StatusBar,
} from 'react-native';


import { Actions } from 'react-native-router-flux';
import IonIcons from 'react-native-vector-icons/Ionicons'

import * as commonStyles from '../../style/commonStyles'
import * as commonColors from '../../style/commonColors';
import { screenWidth, screenHeight } from '../../style/commonStyles';
var { GooglePlacesAutocomplete } = require('react-native-google-places-autocomplete');

import NavTitleBar from '../../components/navTitle'

import authService from '../../service/authService';
import Cache from '../../utils/Cache'
import * as config from '../../config'

export default class Settings extends PureComponent {
    constructor(props) {
        super(props);
        let { workLocation, homeLocation } = Cache.currentUser.user
        this.state = {
            selectedWork: false,
            selectedHome: false,
            selectedSignout: false,
            userName: Cache.currentUser.user.firstname + ' ' + Cache.currentUser.user.lastname,
            homeAddress: homeLocation == '' ? '' : homeLocation.address,
            workAddress: workLocation == '' ? '' : workLocation.address,
            selected: '',
            location:'',
            avatar: Cache.currentUser.user.avatar?{uri:config.SERVICE_FILE_URL+Cache.currentUser.user.avatar}:commonStyles.userEmptyIcon,
        }
        
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    renderRow(row) {
        let { description } = row
        let items = description.split(", ")
        let desc = ''
        for (var i = 1; i < items.length; i++) {
            if (i > 1) desc = desc + ', '
            desc = desc + items[i]
        }
        return (
            <View style={{
                marginTop: -8, flexDirection: 'row', height: 36, width: screenWidth,
                marginLeft: -12, alignItems: 'center'
            }}>
                <IonIcons name="ios-pin-outline" size={22} color={commonColors.normalText} style={{ marginHorizontal: 15 }} />
                <View style={{ flex: 1, paddingRight: 25 }}>
                    <Text numberOfLines={1} style={{ color: commonColors.normalText, fontSize: 15 }}>{items[0]}</Text>
                    {desc.length > 0 && <Text numberOfLines={1} style={{ color: commonColors.textColor1, fontSize: 13 }}>{desc}</Text>}
                </View>
            </View>
        )
    }

    renderAutoComplete() {
        return (
            <View style={{ flexDirection: 'row', height: 50, backgroundColor: 'white' }}>
                <View style={{ flex: 1 }}>
                    <View style={{ position: 'absolute', width: '100%', backgroundColor: 'white' }}>
                        <GooglePlacesAutocomplete
                            placeholder='Enter a new address'
                            minLength={1} // minimum length of text to search
                            autoFocus={false}
                            textInputProps={{
                                ref: "input",
                                autoCorrect: false,
                                autoCapitalize: "none",
                                value: this.state.address,
                                onChangeText: (text) => {
                                    this.setState({ filtering: (text.length > 0), filledInput: false })
                                },
                                onFocus: (text) => {
                                    this.setState({ filtering: this.state.filledInput })
                                }
                            }}
                            returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                            listViewDisplayed='auto'    // true/false/undefined
                            fetchDetails={true}
                            enablePoweredByContainer={false}
                            renderRow={(row) => this.renderRow(row)}
                            renderDescription={(row) => row.description.split(',')[0]} // custom description render
                            onPress={(data, details) => { // 'details' is provided when fetchDetails = true
                                let location = {
                                    placeId: details.place_id,
                                    address: details.formatted_address,
                                    geoJson: {
                                        type: "Point",
                                        coordinates: [
                                            details.geometry.location.lng,
                                            details.geometry.location.lat
                                        ]
                                    }
                                }
                                this.setState({ location: location })
                            }}

                            getDefaultValue={() => ''}

                            query={{
                                // available options: https://developers.google.com/places/web-service/autocomplete
                                key: 'AIzaSyBTyrdlrF8B60Va6y9Wx-aJasqaryaVzV4',
                                language: 'en', // language of the results
                                types: 'geocode' // default: 'geocode'
                            }}

                            styles={{
                                textInputContainer: {
                                    borderBottomWidth: 8,
                                    borderBottomColor: commonColors.background,
                                    width: '100%',
                                    height: 58,
                                    backgroundColor: 'white',
                                },
                                textInput: {
                                    height: 32,
                                    color: commonColors.normalText,
                                    fontSize: 15,
                                    marginTop: 9,
                                    marginLeft: -10
                                },
                                predefinedPlacesDescription: {
                                    color: commonColors.textColor1
                                },
                            }}
                            nearbyPlacesAPI='GoogleReverseGeocoding' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                            GoogleReverseGeocodingQuery={{
                                // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                            }}
                            GooglePlacesSearchQuery={{
                                // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                rankby: 'distance',
                                types: 'food'
                            }}
                            renderLeftButton={() => <IonIcons name="ios-pin-outline" size={22} color={commonColors.normalText} style={{ margin: 15 }} />}
                        />
                    </View>
                </View>
            </View>
        )
    }

    componentDidMount(){
        console.log( '--------->', this.state.avatar)
    }

    goBack() {
        Actions.pop();
    }

    signout() {
        authService.logout()
        Actions.Login()
    }

    rightCallback() {
        if ( this.state.location == '' ) return 
        if (this.state.selected == 'home') {
            Cache.currentUser.user.homeLocation = this.state.location
            this.setState({ homeAddress: this.state.location.address })
        } else if (this.state.selected == 'work') {
            Cache.currentUser.user.workLocation = this.state.location
            this.setState({ workAddress: this.state.location.address })
        }
        authService.updateUser(Cache.currentUser.user, (err, res) => {
            //console.log(err, res)
        })

        this.setState({selected:'', location:''})
    }

    getLocation(type) {
        this.setState({ selected: type })
    }

    closeSelectLocation() {
        this.setState({ selected: '' })
    }

    renderSelectLocation() {
        if (this.state.selected == '') return null
        return (
            <View>
                <NavTitleBar
                    buttons={commonStyles.NavCloseButton}
                    onBack={this.closeSelectLocation.bind(this)}
                    title={'Delivery Location'}
                    rightText={'DONE'}
                    rightCallback={this.rightCallback.bind(this)}
                />
                {this.renderAutoComplete()}
            </View>
        )
    }

    editAccount(){
        Actions.EditAccount({update:()=>{
            this.setState({
                userName: Cache.currentUser.user.firstname + ' ' + Cache.currentUser.user.lastname,
                avatar: Cache.currentUser.user.avatar?{uri:config.SERVICE_FILE_URL+Cache.currentUser.user.avatar}:commonStyles.userEmptyIcon,
            })
        }})
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderSelectLocation()}
                {/*  header  */}
                {this.state.selected == '' && <View>
                    <View style={styles.header}>
                        <Image source={this.state.avatar} style={{ width: 80, height: 80, borderRadius: 15, margin: 10 }} />
                        <Text style={{ color: commonColors.normalText, fontSize: 18 }}>
                            {this.state.userName}
                        </Text>
                        <TouchableOpacity onPress={()=>this.editAccount()} style={{ padding: 5 }}>
                            <Text style={{ fontSize: 10, color: commonColors.theme, fontWeight: 'bold' }}>EDIT ACCOUNT</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPressIn={() => this.goBack()} style={{ position: 'absolute', left: 15, top: 0 }} >
                            <IonIcons name="ios-arrow-round-back" size={40} color={'black'} />
                        </TouchableOpacity>
                    </View>
                    <Text style={{ marginLeft: 15, marginTop: 20, marginBottom: 10, color: commonColors.textColor2, fontSize: 13 }}>SAVED PLACES</Text>
                    {/*  Home  */}
                    <TouchableOpacity onPress={() => this.getLocation('home')}
                        activeOpacity={1} onPressIn={() => this.setState({ selectedHome: true })} onPressOut={() => this.setState({ selectedHome: false })}
                        style={{ flexDirection: 'row', backgroundColor: this.state.selectedHome ? commonColors.background : 'white', alignItems: 'center' }}>
                        <IonIcons name="ios-home-outline" size={20} color={commonColors.normalText} style={{ margin: 16 }} />
                        <View style={{ marginVertical: 12 }}>
                            <Text style={{ color: commonColors.normalText, fontSize: 15 }}>Home</Text>
                            <Text style={{ color: commonColors.textColor1, fontSize: 13 }}>
                                {this.state.homeAddress == '' ? 'Add Home' : this.state.homeAddress}
                            </Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{ height: 0.5, width: '100%', backgroundColor: 'white', paddingHorizontal: 15 }}>
                        <View style={{ height: 0.5, backgroundColor: commonColors.background, width: '100%' }} />
                    </View>
                    {/*  Work  */}
                    <TouchableOpacity onPress={() => this.getLocation('work')}
                        activeOpacity={1} onPressIn={() => this.setState({ selectedWork: true })} onPressOut={() => this.setState({ selectedWork: false })}
                        style={{ flexDirection: 'row', backgroundColor: this.state.selectedWork ? commonColors.background : 'white', alignItems: 'center' }}>
                        <IonIcons name="ios-briefcase-outline" size={20} color={commonColors.normalText} style={{ margin: 16 }} />
                        <View style={{ marginVertical: 12 }}>
                            <Text style={{ color: commonColors.normalText, fontSize: 15 }}>Work</Text>
                            <Text style={{ color: commonColors.textColor1, fontSize: 13 }}>
                                {this.state.workAddress == '' ? 'Add Work' : this.state.workAddress}
                            </Text>
                            {/*<Text style={{ color: commonColors.textColor1, fontSize: 13 }}>Deliver to door</Text>*/}
                        </View>
                    </TouchableOpacity>
                    {/*  Sign out  */}
                    <TouchableOpacity activeOpacity={1} onPress={() => this.signout()}
                        onPressIn={() => this.setState({ selectedSignout: true })} onPressOut={() => this.setState({ selectedSignout: false })}
                        style={{ flexDirection: 'row', backgroundColor: this.state.selectedSignout ? commonColors.background : 'white', marginTop: 15 }}>
                        <View style={{ marginHorizontal: 15, marginVertical: 20 }}>
                            <Text style={{ color: commonColors.normalText, fontSize: 15 }}>Sign Out</Text>
                        </View>
                    </TouchableOpacity>
                </View>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: commonColors.background,
        marginTop: 20,
    },
    header: {
        height: 208,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    }

});
