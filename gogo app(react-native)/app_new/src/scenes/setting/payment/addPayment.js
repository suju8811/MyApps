'use strict';

import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    ListView,
    TextInput,
    TouchableOpacity,
    Alert,
    Linking,
    RefreshControl,
    Button,
    Platform,
    StatusBar,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { CreditCardInput, LiteCreditCardInput } from "react-native-mycredit-card-input";
import { CardIOView, CardIOUtilities, CardIOModule } from 'react-native-mycard-io'
import {AddCard} from 'react-native-checkout'
import IonIcons from 'react-native-vector-icons/Ionicons'
import NavTitleBar from '../../../components/navTitle'

import * as commonColors from '../../../style/commonColors';
import * as commonStyles from '../../../style/commonStyles';
import { screenWidth, screenHeight } from "../../../style/commonStyles";

class AddPayment extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            disabled: true,
        }
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    componentWillMount(){
        CardIOUtilities.preload()
    }

    componentDidMount() {
        //this.refs.CCInput.setValues({number: "4242424242424242", expiry:"12/18"});
        //this.refs.CCInput.focus("expiry");
        this.hasMounted = true
    }

    componentWillUnmount(){
        this.hasMounted = false
    }


    goBack() {
        if ( this.props.addedPayment != undefined ){
            this.props.addedPayment(false)
        }
        Actions.pop();
    }

    _onChange(form) {
        //console.log(form)
        this.hasMounted && this.setState({ disabled: !form.valid })
    }

    addPayment() {
        if (this.props.addedPayment != undefined) {
            this.props.addedPayment(true)
        }
        setTimeout(()=>Actions.pop(),20)
    }

    withCamera() {
        CardIOModule
            .scanCard({ suppressManualEntry: true, suppressConfirmation: true, requireExpiry: false, requireCVV: false })
            .then(card => {
                //console.log(card)
                this.refs.CCInput.setValues({ number: card.cardNumber })
                this.refs.CCInput.focus("expiry");
            }).catch(() => {

            })
    }

    didScanCard(card){
        //console.log(card)
    }

    render() {
        return (
            <View style={styles.container}>
                <NavTitleBar
                    buttons={commonStyles.NavCloseButton}
                    onBack={this.goBack.bind(this)}
                    title={'Add Payment'}
                />
                {/* <TouchableOpacity style={{ position:'absolute', right:15, top:30 }} onPress={() => this.withCamera()}>
                    <IonIcons name="ios-camera" size={26} color={'black'} />
                </TouchableOpacity> */}
                {/* <CreditCardInput
                    ref="CCInput"
                    onChange={this._onChange.bind(this)}
                /> */}
                    <AddCard
                        addCardHandler={(cardNumber, cardExpiry, cardCvc)=>{
                            // console.log(cardNumber, cardExpiry, cardCvc)
                            this.addPayment()
                            return Promise.resolve(cardNumber)
                        }}
                    />
                {/* <TouchableOpacity onPress={() => this.addPayment()} disabled={this.state.disabled}
                    style={{ height: 50, backgroundColor: this.state.disabled ? 'grey' : 'black', justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
                    <Text style={{ color: 'white' }}>SAVE</Text>
                </TouchableOpacity> */}
            </View>
            
        )
    }
}

export default AddPayment;

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    header_title: {
        fontSize: 16,
        fontWeight: '500',
    },
    header: {
        paddingTop: 15,
        height: 80,
        width: screenWidth,
        borderBottomWidth: 1,
        shadowColor: 'rgb(204, 205, 208)',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderBottomColor: 'rgb(204, 205, 208)',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
})
