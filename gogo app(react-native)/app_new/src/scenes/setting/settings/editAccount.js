'use strict';

import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Platform,
    StatusBar,
    ImageBackground,
    Alert,
    TouchableWithoutFeedback,
    Keyboard,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import IonIcons from 'react-native-vector-icons/Ionicons'
import { BlurView, VibrancyView } from 'react-native-blur'
import ImagePicker from 'react-native-image-picker';

import NavTitleBar from '../../../components/navTitle'

import * as commonColors from '../../../style/commonColors';
import * as commonStyles from '../../../style/commonStyles';
import { screenWidth, screenHeight } from "../../../style/commonStyles";

import authService from '../../../service/authService'
import baseService from '../../../service/baseService'

import Cache from '../../../utils/Cache'
import * as config from '../../../config'

export default class EditAccount extends PureComponent {
    constructor(props) {
        super(props);

        this.user = authService.getActiveUser();

        this.state = {
            avatar: this.user.avatar ? { uri: config.SERVICE_FILE_URL + this.user.avatar } : commonStyles.userEmptyIcon,
            firstname: this.user.firstname,
            lastname: this.user.lastname,
            phone: this.user.phone,
            email: this.user.email,

            user: this.user,
            profilePhoto: commonStyles.userEmptyIcon,
            profilePhotoFile: null,
            activityStatus: false,
            isUploadingFile: false,
        }
        
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('light-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    componentDidMount() {
        this.hasMounted = true
        console.log( '--------->', this.state.avatar)
    }
    componentWillUnmount() {
        this.hasMounted = false
    }

    goBack() {
        if (this.props.update)
            this.props.update()
        Actions.pop();
    }

    updateUserInfo(file) {

        this.user.firstname = this.state.firstname
        this.user.lastname = this.state.lastname
        this.user.phone = this.state.phone
        this.user.email = this.state.email
        if (file) this.user.avatar = file.path

        console.log(this.user)
        authService.updateUser(this.user, (error, result) => {

            this.hasMounted && this.setState({ activityStatus: false });

            if (error) {
                console.log(error);
                return;
            }

            Cache.currentUser.user = this.user
            setTimeout(() => {
                Alert.alert("Profile Updated", "Your changes have been saved.");
            }, 200);
        })
    }

    done() {
        this.hasMounted && this.setState({ activityStatus: true });

        if (this.state.profilePhotoFile) {
            //upload image first
            baseService.uploadFile(this.state.profilePhotoFile, (error, file) => {
                this.hasMounted && this.setState({
                    isUploadingFile: false
                })
                if (error) {

                    this.hasMounted && this.setState({ activityStatus: false });

                    setTimeout(() => {
                        alert("Failed to upload file. Please try again later");
                    }, 200);

                    return;
                }
                console.log('file--->', file)
                this.updateUserInfo(file);
            },
                {
                    _workflow: 'avatar'
                });
        } else {
            this.updateUserInfo();
        }
    }

    onPickProfilePhoto() {

        let options;

        if (this.state.avatar === commonStyles.userEmptyIcon) {
            options = {
                quality: 1.0,
                storageOptions: {
                    skipBackup: true,
                }
            };
        } else {
            options = {
                quality: 1.0,
                storageOptions: {
                    skipBackup: true,
                },
                customButtons: [{
                    name: "remove",
                    title: "Remove Photo"
                }]
            };
        }

        ImagePicker.showImagePicker(options, (response) => {

            if (response.customButton == 'remove') {
                this.hasMounted && this.setState({
                    avatar: commonStyles.userEmptyIcon,
                    profilePhotoFile: null,
                });
                return;
            }
            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else {
                let source = { uri: response.uri };

                this.hasMounted && this.setState({
                    avatar: source,
                    profilePhotoFile: response,
                });
            }
        });
    }

    renderHeader() {
        return (
            <View style={{ width: '100%', height: 260 }}>
                <Image source={this.state.avatar} style={{ position: 'absolute', width: '100%', height: '100%' }} />
                <BlurView
                    style={{ width: '100%', height: '100%', position: 'absolute' }}
                    viewRef={this.state.viewRef}
                    blurType="dark"
                    blurAmount={5}
                />
                <View style={styles.navContainer}>
                    <TouchableOpacity onPress={() => this.goBack()} style={{ marginLeft: 15 }}>
                        <IonIcons name="md-arrow-back" size={20} color={'white'} />
                    </TouchableOpacity>
                    <Text style={{ color: 'white', textAlign: 'center', flex: 1 }}>EDIT PROFILE</Text>
                    <TouchableOpacity onPress={() => this.done()} style={{ marginRight: 15 }}>
                        <Text style={{ color: 'white', fontSize: 12 }}>DONE</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity activeOpacity={0.7} onPress={() => this.onPickProfilePhoto()} style={{ justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
                    <Image source={this.state.avatar} style={{ borderRadius: 50, width: 100, height: 100 }} >
                    </Image>
                    <View style={{
                        width: 30, height: 30, backgroundColor: 'rgb(200,70, 86)', borderRadius: 15, justifyContent: 'center', alignItems: 'center',
                        position: 'absolute', left: screenWidth / 2 + 20, bottom: 0
                    }}>
                        <IonIcons name="ios-camera-outline" size={20} color={'white'} />
                    </View>
                </TouchableOpacity>
                <View style={{ flexDirection: 'row', flex: 1, paddingHorizontal: 40, marginTop: 15, }}>
                    <View style={styles.textInputContainer}>
                        <TextInput
                            ref="firstname"
                            autoCapitalize="none"
                            autoCorrect={false}
                            placeholder="firstname"
                            placeholderTextColor={commonColors.placeholderText}
                            textAlign="left"
                            style={styles.inputName}
                            underlineColorAndroid="transparent"
                            returnKeyType={'next'}
                            value={this.state.firstname}
                            onChangeText={(text) => this.setState({ firstname: text.replace(/\t/g, '') })}
                            onSubmitEditing={() => this.refs.lastname.focus()}
                        />
                    </View>
                    <View style={{ width: 15 }} />
                    <View style={styles.textInputContainer}>
                        <TextInput
                            ref="lastname"
                            autoCapitalize="none"
                            autoCorrect={false}
                            placeholder="lastname"
                            placeholderTextColor={commonColors.placeholderText}
                            textAlign="left"
                            style={styles.inputName}
                            underlineColorAndroid="transparent"
                            value={this.state.lastname}
                            onChangeText={(text) => this.setState({ lastname: text.replace(/\t/g, '') })}
                        />
                    </View>
                </View>

            </View >
        )
    }

    renderBody() {
        return (
            <View>
                <Text style={{ color: commonColors.textColor1, marginTop: 20, marginLeft: 35, marginBottom: 10 }}>E-mail</Text>
                <TextInput
                    ref="email"
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholderTextColor={commonColors.placeholderText}
                    textAlign="left"
                    style={styles.input}
                    underlineColorAndroid="transparent"
                    returnKeyType={'next'}
                    value={this.state.email}
                    onChangeText={(text) => this.setState({ email: text.replace(/\t/g, '') })}
                    onSubmitEditing={() => this.refs.phone.focus()}
                />
                <Text style={{ color: commonColors.textColor1, marginTop: 20, marginLeft: 35, marginBottom: 10 }}>Phone Number</Text>
                <TextInput
                    ref="phone"
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholderTextColor={commonColors.placeholderText}
                    textAlign="left"
                    style={styles.input}
                    underlineColorAndroid="transparent"
                    keyboardType='phone-pad'
                    value={this.state.phone}
                    onChangeText={(text) => this.setState({ phone: text.replace(/\t/g, '') })}
                />
            </View>
        )
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.container}>
                    {this.renderHeader()}
                    {this.renderBody()}
                </View>
            </TouchableWithoutFeedback>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        backgroundColor: '#ffffff'
    },
    inputName: {
        fontSize: 14,
        color: 'white',
        alignSelf: 'stretch',
        marginHorizontal: 0,
        backgroundColor: 'transparent',
        paddingHorizontal: 5,
        flex: 1,
    },
    textInputContainer: {
        flex: 1,
        height: 36,
        borderBottomWidth: 1,
        borderBottomColor: 'white'
    },
    input: {
        fontSize: 14,
        color: commonColors.title,
        height: 40,
        alignSelf: 'stretch',
        marginHorizontal: 20,
        borderColor: '#aaa',
        backgroundColor: '#fff',
        borderWidth: 1,
        borderRadius: 3,
        marginBottom: 3,
        paddingHorizontal: 15,
    },
    navContainer: {
        flexDirection: 'row',
        backgroundColor: 'transparent',
        marginTop: 40,
        alignItems: 'center'
    },
})
