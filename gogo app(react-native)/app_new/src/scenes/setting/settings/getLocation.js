'use strict';

import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    ListView,
    TextInput,
    TouchableOpacity,
    Alert,
    Linking,
    RefreshControl,
    Button,
    Platform,
    StatusBar,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { CreditCardInput, LiteCreditCardInput } from "react-native-mycredit-card-input";
import { CardIOView, CardIOModule } from 'react-native-mycard-io'
import IonIcons from 'react-native-vector-icons/Ionicons'
var { GooglePlacesAutocomplete } = require('react-native-google-places-autocomplete');

import NavTitleBar from '../../../components/navTitle'

import * as commonColors from '../../../style/commonColors';
import * as commonStyles from '../../../style/commonStyles';
import { screenWidth, screenHeight } from "../../../style/commonStyles";

export default class GetLocation extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            location:null
        }
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    goBack() {
        Actions.pop();
    }

    renderRow(row) {
        let { description } = row
        let items = description.split(", ")
        let desc = ''
        for (var i = 1; i < items.length; i++) {
            if (i > 1) desc = desc + ', '
            desc = desc + items[i]
        }
        return (
            <View style={{
                marginTop: -8, flexDirection: 'row', height: 36, width: screenWidth,
                marginLeft: -12, alignItems: 'center'
            }}>
                <IonIcons name="ios-pin-outline" size={22} color={commonColors.normalText} style={{ marginHorizontal: 15 }} />
                <View style={{ flex: 1, paddingRight: 25 }}>
                    <Text numberOfLines={1} style={{ color: commonColors.normalText, fontSize: 15 }}>{items[0]}</Text>
                    {desc.length > 0 && <Text numberOfLines={1} style={{ color: commonColors.textColor1, fontSize: 13 }}>{desc}</Text>}
                </View>
            </View>
        )
    }

    renderAutoComplete() {
        return (
            <View style={{ marginTop: 8, flexDirection: 'row', height: 50, backgroundColor: 'white' }}>
                <View style={{ flex: 1 }}>
                    <View style={{ position: 'absolute', width: '100%', backgroundColor: 'white' }}>
                        <GooglePlacesAutocomplete
                            placeholder='Enter a new address'
                            minLength={1} // minimum length of text to search
                            autoFocus={false}
                            textInputProps={{
                                ref: "input",
                                autoCorrect: false,
                                autoCapitalize: "none",
                                value: this.state.address,
                                onChangeText: (text) => {
                                    this.setState({ filtering: (text.length > 0), filledInput: false })
                                },
                                onFocus: (text) => {
                                    this.setState({ filtering: this.state.filledInput })
                                }
                            }}
                            returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                            listViewDisplayed='auto'    // true/false/undefined
                            fetchDetails={true}
                            enablePoweredByContainer={false}
                            renderRow={(row) => this.renderRow(row)}
                            renderDescription={(row) => row.description.split(',')[0]} // custom description render
                            onPress={(data, details) => { // 'details' is provided when fetchDetails = true
                                this.setState({
                                    location: {
                                        placeId: details.place_id,
                                        address: details.formatted_address,
                                        geoJson: {
                                            type: "Point",
                                            coordinates: [
                                                details.geometry.location.lng,
                                                details.geometry.location.lat
                                            ]
                                        }
                                    }
                                })
                            }}

                            getDefaultValue={() => ''}

                            query={{
                                // available options: https://developers.google.com/places/web-service/autocomplete
                                key: 'AIzaSyBTyrdlrF8B60Va6y9Wx-aJasqaryaVzV4',
                                language: 'en', // language of the results
                                types: 'geocode' // default: 'geocode'
                            }}

                            styles={{
                                textInputContainer: {
                                    borderBottomWidth: 8,
                                    borderBottomColor: commonColors.background,
                                    width: '100%',
                                    height: 58,
                                    backgroundColor: 'white',
                                },
                                textInput: {
                                    height: 32,
                                    color: commonColors.normalText,
                                    fontSize: 15,
                                    marginTop: 9,
                                    marginLeft: -10
                                },
                                predefinedPlacesDescription: {
                                    color: commonColors.textColor1
                                },
                            }}
                            nearbyPlacesAPI='GoogleReverseGeocoding' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                            GoogleReverseGeocodingQuery={{
                                // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                            }}
                            GooglePlacesSearchQuery={{
                                // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                rankby: 'distance',
                                types: 'food'
                            }}
                            renderLeftButton={() => <IonIcons name="ios-pin-outline" size={22} color={commonColors.normalText} style={{ margin: 15 }} />}
                        />
                    </View>
                </View>
            </View>
        )
    }

    done(){
        if ( this.props.updateData ){
            this.props.updateData(this.state.location)
        }
        Actions.pop()
    }

    render() {
        return (
            <View style={styles.container}>
                <NavTitleBar
                    buttons={commonStyles.NavCloseButton}
                    onBack={this.goBack.bind(this)}
                    title={'Delivery Location'}
                />
                {this.renderAutoComplete()}
                <View style={{height:400}}/>
                <TouchableOpacity onPress={()=>this.done()} style={{height:48, width:'100%', position:'absolute', 
                    bottom:0, backgroundColor:commonColors.theme, justifyContent:'center', alignItems:'center'}}>
                    <Text style={{fontSize:16, color:'white', fontWeight:'bold'}}>DONE</Text>
                </TouchableOpacity>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        backgroundColor: '#ffffff'
    },
})
