'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Keyboard,
    TouchableWithoutFeedback,
    Alert,
    Platform,
    StatusBar,
} from 'react-native';


import { Actions } from 'react-native-router-flux';

import * as commonStyles from '../../style/commonStyles'
import * as commonColors from '../../style/commonColors';
import { screenWidth, screenHeight } from '../../style/commonStyles';

import NavTitleBar from '../../components/navTitle'

import authService from '../../service/authService';

export default class Payment extends PureComponent {
    constructor(props) {
        super(props);
        this.state={
            pressed:false,
        }
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    goBack() {
        Actions.pop();
    }

    rightCallback() {
        
    }

    render() {
        return (
            <View style={styles.container}>
                <NavTitleBar
                    buttons={commonStyles.NavCloseButton}
                    onBack={this.goBack}
                    title={'Payment'}
                />
                <Text style={{marginLeft:20, marginTop:30, fontSize:13, color:commonColors.normalText}}>Payment Methods</Text>
                <TouchableOpacity onPress={()=>Actions.AddPayment()} activeOpacity={1} onPressIn={()=>this.setState({pressed:true})} onPressOut={()=>this.setState({pressed:false})}
                    style={{marginTop:10, backgroundColor:this.state.pressed?'rgb(230,230,230)':'white', padding:20, justifyContent:'center'}}>
                    <Text style={{color:commonColors.theme, fontSize:13}}>Add Payment Method</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: commonColors.background,
    },
    
});
