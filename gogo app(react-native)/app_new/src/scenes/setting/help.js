'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Keyboard,
    ListView,
    TouchableWithoutFeedback,
    Alert,
    Platform,
    StatusBar,
} from 'react-native';


import { Actions } from 'react-native-router-flux';
import IonIcons from 'react-native-vector-icons/Ionicons'

import * as commonStyles from '../../style/commonStyles'
import * as commonColors from '../../style/commonColors';
import { screenWidth, screenHeight } from '../../style/commonStyles';

import NavTitleBar from '../../components/navTitle'

import authService from '../../service/authService';
import * as config from '../../config'

export default class Help extends PureComponent {
    constructor(props) {
        super(props);

        this.dataSource = new ListView.DataSource(
            { rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            navTitle: 'HELP',
            subTitle: null,
            items: [
                
            ]
        }
        this.state.items = props.data

        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    goBack() {
        Actions.pop();
    }

    rightCallback() {
        
    }

    toGO(data){
        if ( data.url != '' || data.url){
            Actions.Html({url:config.SERVICE_FILE_URL+data.url})
        }else if (data.subMenus != null){
            Actions.Help({data:data.subMenus})
        }
        
    }

    renderItem(rowData, sectionID, rowID) {
        return (
            <TouchableOpacity onPress={() => this.toGO(rowData)} style={styles.options}>
                {rowData.image!=''&&rowData.image!=null&&<Image source={{uri:config.SERVICE_FILE_URL+rowData.image}} 
                    style={{width:20, height:20, resizeMode:'contain', marginRight:20}}/>}
                <Text style={{ color: commonColors.textColor2, flex:1 }}>{rowData.title}</Text>
                <IonIcons name="ios-arrow-forward" size={20} color={commonColors.textColor1}/>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <NavTitleBar
                    buttons={commonStyles.NavBackButton}
                    onBack={this.goBack}
                    title={'How can we help?'}
                />
                <View style={styles.topics}>
                    {this.state.subTitle != null && <View style={styles.options}>
                        <Text style={{ fontWeight: 'bold', color: '#000000' }}>{this.state.subTitle}</Text>
                    </View>}
                    {this.state.items!=undefined&&this.state.items.length>0&&<ListView
                        enableEmptySections={true}
                        scrollEnabled={false}
                        dataSource={this.dataSource.cloneWithRows(this.state.items)}
                        renderRow={this.renderItem.bind(this)}
                    />}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:commonColors.background
    },
    topics: {
        flex: 1,
        margin: 10,
        marginTop: 10,
        flexDirection: 'column',

    },
    options: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: commonColors.textColor1,
        height: 50,
        padding: 10,
        backgroundColor:'white'
    },
});

