'use strict';

import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Text,
    WebView,
    TouchableOpacity,
    Image,
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import NavTitleBar from '../../../components/navTitle';
import * as commonColors from '../../../style/commonColors';
import * as commonStyles from '../../../style/commonStyles';
import { screenWidth, screenHeight } from "../../../style/commonStyles";

export default class Html extends PureComponent {
    constructor(props) {
        super(props);
    }

    onBack() {
        Actions.pop()
    }

    render() {
        return (
            <View style={styles.container}>
                <NavTitleBar
                    buttons={commonStyles.NavBackButton}
                    onBack={this.onBack}
                    title={'HELP'}
                />
                <WebView
                    source={{ uri: this.props.url }}
                    style={{ marginTop: 5 }}
                />
            </View>
        )

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    header: {
        paddingTop: 15,
        height: 80,
        borderBottomWidth: 1,
        shadowColor: 'rgb(204, 205, 208)',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        borderBottomColor: 'rgb(204, 205, 208)',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingLeft: 10,
        marginBottom: 10,
    },
    topics: {
        flex: 1,
        margin: 10,
        marginTop: 30,
        flexDirection: 'column',

    },
    options: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: 'rgb(204, 205, 208)',
        height: 50,
        padding: 10,
    },
    buttonWrap: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        backgroundColor: 'transparent',
    },
    button: {
        width: screenWidth * 0.15,
        height: screenWidth * 0.12,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: 14,
        height: 14,
    },
})
