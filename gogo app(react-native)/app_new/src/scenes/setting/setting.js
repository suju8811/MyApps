'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    Platform,
    StatusBar,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import IonIcons from 'react-native-vector-icons/Ionicons'

import * as commonColors from '../../style/commonColors'
import * as commonStyles from '../../style/commonStyles'
import { screenHeight, screenWidth } from '../../style/commonStyles';

import Cache from '../../utils/Cache'
import * as config from '../../config'
import baseService from '../../service/baseService'
import * as _ from 'underscore'

export default class Setting extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            userName: Cache.currentUser.user.firstname + ' ' + Cache.currentUser.user.lastname,
            menus: [
                { iconName: 'ios-heart-outline', title: 'Your Favorites' },
                { iconName: 'ios-card-outline', title: 'Payment' },
                { iconName: 'ios-help-buoy-outline', title: 'Help' },
                { iconName: 'ios-pricetag-outline', title: 'Promotions' },
                { iconName: 'ios-bicycle', title: 'Free Delivery' },
                { iconName: 'ios-briefcase-outline', title: 'Deliver With GoGo' },
                { iconName: 'ios-settings-outline', title: 'Settings' },
            ],
            selectItem: -1,
            avatar: Cache.currentUser.user.avatar?{uri:config.SERVICE_FILE_URL+Cache.currentUser.user.avatar}:commonStyles.userEmptyIcon,
        }
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    onCheck() {
        Actions.ViewCart({
            updateData: () => {
                this.forceUpdate()
            }
        });
    }

    renderCheckout() {
        var count = 0
        var price = 0
        _.map(Cache.order, (item) => {
            count = count + item.count
            price = price + item.count * item.price
        })
        if (Cache.order != undefined && Cache.order.length > 0) {
            return (
                <TouchableOpacity onPress={this.onCheck.bind(this)} style={{
                    position: 'absolute', top: screenHeight - 113, width: screenWidth, height: 48, justifyContent: 'center',
                    alignItems: 'center', backgroundColor: commonColors.theme, flexDirection: 'row'
                }}>
                    <View style={{ flex: 2, alignItems: 'flex-start', justifyContent: 'center', paddingLeft: 15 }}><Text style={{ color: '#ffffff', fontSize: 14 }}>${price}</Text></View>
                    <View style={{ flex: 4, alignItems: 'center', justifyContent: 'center' }}><Text style={{ color: '#ffffff', fontSize: 16, fontWeight: '700' }}>CHECK OUT</Text></View>
                    <View style={{ flex: 2, alignItems: 'flex-end', justifyContent: 'center', paddingRight: 15 }}><Text style={{ color: '#ffffff', fontSize: 14, paddingHorizontal: 5, borderRadius: 3, borderWidth: 1, borderColor: 'white' }}>{count}</Text></View>
                </TouchableOpacity>
            )
        }
        return null
    }

    connectItem(index) {
        switch (index) {
            case 0://Your Favorites
                Actions.Favorite()
                break;
            case 1://Payment
                Actions.Payment()
                break;
            case 2://Help
                baseService.getHelpContent((err, res) => {
                    Actions.Help({ data: res });
                })
                break;
            case 3://Promotions
                Actions.Promotion()
                break;
            case 4://Free Delivery
                Actions.FreeDelivery()
                break;
            case 5://Deliver With Uber
                Actions.DeliverWithUber()
                break;
            case 6://Settings
                Actions.Settings()
                break;
            case 7://---
                break;
            case 8://About
                Actions.About()
                break;
        }
    }

    renderItem(index) {
        let { iconName, title } = this.state.menus[index]
        return (
            <TouchableOpacity key={index} activeOpacity={1} onPress={() => this.connectItem(index)}
                onPressIn={() => { this.setState({ selectItem: index }) }} onPressOut={() => this.setState({ selectItem: -1 })}
                style={{
                    flexDirection: 'row', marginVertical: 5, height: 48, width: '100%', alignItems: 'center',
                    backgroundColor: this.state.selectItem == index ? commonColors.background : 'white'
                }}>
                <IonIcons name={iconName} size={25} color={commonColors.normalText} style={{ margin: 15 }} />
                <Text style={{ fontSize: 15, color: commonColors.normalText, marginLeft: 5 }}>{title}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.headerContainer}>
                        <Image source={this.state.avatar} style={{ width: 50, height: 50, borderRadius: 15, margin: 15 }} />
                        <Text style={{ color: commonColors.normalText, fontSize: 15, fontWeight: 'bold' }}>
                            {this.state.userName}
                        </Text>
                    </View>
                    <View style={{ backgroundColor: 'white', marginTop: 0.5 }}>
                        {this.state.menus.map((item, index) => {
                            return this.renderItem(index)
                        })}
                    </View>
                </ScrollView>
                {this.renderCheckout()}
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: 20,
    },
    headerContainer: {
        flexDirection: 'row',
        height: 80,
        backgroundColor: 'white',
        //justifyContent:'center',
        alignItems: 'center',
    }
})
