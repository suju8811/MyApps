'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Keyboard,
    TouchableWithoutFeedback,
    Alert,
    Platform,
    StatusBar,
} from 'react-native';


import { Actions } from 'react-native-router-flux';

import * as commonStyles from '../../style/commonStyles'
import * as commonColors from '../../style/commonColors';
import { screenWidth, screenHeight } from '../../style/commonStyles';

import NavTitleBar from '../../components/navTitle'

import authService from '../../service/authService';

export default class Favorite extends PureComponent {
    constructor(props) {
        super(props);
        this.state={
            promoCode:'',
        }
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    goBack() {
        Actions.pop();
    }

    rightCallback() {

    }

    render() {
        return (
            <View style={styles.container}>
                <NavTitleBar
                    buttons={commonStyles.NavBackButton}
                    onBack={this.goBack}
                    title={'Promotion'}
                />
                <View style={{
                    marginHorizontal: 16, marginTop: 20, borderColor: commonColors.textColor3, borderRadius:3,
                    borderWidth: 0.5, padding: 8, flexDirection: 'row', backgroundColor: 'white'
                }}>
                    <TextInput
                        autoCapitalize="none"
                        autoCorrect={false}
                        placeholder="Enter Promo Code"
                        placeholderTextColor={commonColors.placeholderText}
                        textAlign="left"
                        style={{flex:1, marginLeft:16, fontSize:15}}
                        underlineColorAndroid="transparent"
                        value={this.state.promoCode}
                        onChangeText={(text) => {
                            this.setState({ promoCode: text.replace(/\t/g, '') })
                        }}
                    />
                    <TouchableOpacity style={{ borderRadius: 3, backgroundColor: commonColors.theme, 
                        justifyContent: 'center', alignItems: 'center', width:90, height:30 }}>
                        <Text style={{ color: 'white', fontSize: 12, fontWeight: 'bold' }}>APPLY</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: commonColors.background,
    },

});
