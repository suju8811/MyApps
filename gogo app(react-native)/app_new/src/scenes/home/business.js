'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    Animated,
    ListView,
    Dimensions,
    Platform,
    StatusBar,
} from 'react-native';

import { Actions} from 'react-native-router-flux';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as commonActions from '../../redux/actions';
import * as commonActionTypes from '../../redux/actionTypes';

import { TouchThroughView, TouchThroughWrapper } from 'react-native-touch-through-view'
import LinearGradient from 'react-native-linear-gradient'
import IonIcons from 'react-native-vector-icons/Ionicons'
import ScrollableTabView, { ScrollableTabBar, DefaultTabBar, } from 'react-native-scrollable-tab-view';
import * as _ from 'underscore'

import * as commonColors from '../../style/commonColors'
import * as commonStyles from '../../style/commonStyles'
import { screenHeight, screenWidth } from '../../style/commonStyles';

import businessService from '../../service/businessService'
import Cache from '../../utils/Cache'
import * as config from '../../config'
import util from '../../utils/util'
import FoodItem from '../../components/home/foodItem'

class Business extends PureComponent {
    constructor(props) {
        super(props);

        let data = util.convertRestaurantData(props.data)
        console.log("mealKind", props.data)
        this.state = {
            id: props.data.id,
            image: data.image,
            title: data.title,
            description: data.description,
            preparation: data.preparation,
            toggleheart: businessService.isFavorite(props.data.id),
            heartSize: new Animated.Value(1),
            headerPadding: 0,
            showStickyHeader: false,
            tabName: props.data.mealKinds,
            mealIndex: 0,
            food: [],
            categories: [],
            positionY: [],
            currentFoodGroup: 0,
        }
        Cache.currentBusinessId = props.data.id
        this.y = 0;
        this.alive = true;
        this.scrollLock = false;
    }

    back() {
        if ( this.props.updateData ){
            this.props.updateData(false)
        }
        Actions.pop()
        setTimeout(() => {
            Actions.refresh()
        }, 20)
    }

    changeHeart() {

    }

    onPressHeart() {
        businessService.changeFavorite(this.state.id, (err, res) => {
            if (err) {
                // console.log('heart->', err);
            } else {
                this.props.commonActions.changeFavoriteState(this.state.id)
            }
        })
        this.setState({ toggleheart: !this.state.toggleheart })
        this.state.heartSize.setValue(0.8)
        Animated.spring(
            this.state.heartSize,
            {
                toValue: 1,
                friction: 0.9,
            }
        ).start()
    }

    renderBackground() {
        let height = 210 - this.y
        return (
            <View style={{ position: 'absolute', width: '100%' }}>
                <TouchableOpacity onPress={() => this.props.click()}>
                    <Image style={{ height: height }} source={this.state.image} />
                </TouchableOpacity>
                <LinearGradient
                    colors={gradientColors}
                    locations={gradientPoints}
                    style={{ flex: 1, position: 'absolute', height: height, width: '100%' }}>
                </LinearGradient>
            </View>
        )
    }
    renderFixedHeader() {
        let bShow = this.state.showStickyHeader
        return (
            <View style={{ position: 'absolute', width: '100%', height: 130, backgroundColor: bShow ? 'white' : 'transparent' }}>
                {bShow &&
                    <View>
                        <Text style={{ fontSize: 26, color: commonColors.normalText, marginLeft: 56, marginTop: 23 }}>{this.state.title}</Text>
                        <ScrollableTabView
                            style={{ marginTop: 10 }}
                            initialPage={0}
                            tabBarActiveTextColor='black'
                            tabBarInactiveTextColor='black'
                            tabBarUnderlineStyle={{ bottom: 7, height: 36, backgroundColor: 'rgba(0,0,0,0.2)', position: 'absolute', borderRadius: 18 }}
                            renderTabBar={() => <ScrollableTabBar style={{ borderColor: 'white' }} />}
                            page={this.state.currentFoodGroup}
                            onChangeTab={(obj) => {
                                if ( this.lockSelect ) return
                                this.scrollLock = true;
                                setTimeout(()=>this.scrollLock = false, 1000)
                                this.refs.businessScroll.scrollTo({
                                    x: 0,
                                    y: this.state.positionY[obj.i] + 220,
                                    animated: true
                                })
                                this.setState({currentFoodGroup:obj.i})
                            }}>

                            {this.state.categories.map((o, index) => {
                                return (
                                    <View key={index} tabLabel={o} style={{ backgroundColor: 'green' }}>
                                    </View>
                                )
                            })}
                        </ScrollableTabView>
                    </View>}
                <TouchableOpacity onPress={() => this.back()} style={styles.left}>
                    <IonIcons name="md-arrow-back" size={28} color={bShow ? 'black' : 'white'} />
                </TouchableOpacity>
                {bShow == false && <TouchableOpacity onPress={() => this.onPressHeart()} style={styles.heart}>
                    {!this.state.toggleheart && <IonIcons name="md-heart-outline" size={27} color={'white'} style={{ backgroundColor: 'transparent', }} />}
                    {this.state.toggleheart && <Animated.View style={{ transform: [{ scale: this.state.heartSize }] }}>
                        <IonIcons name="md-heart" size={25} color={'white'} style={{ backgroundColor: 'transparent' }} />
                    </Animated.View>}
                </TouchableOpacity>}
            </View>
        )
    }

    handleScroll(event) {
        if (this.alive == false) return;

        let { contentOffset } = event.nativeEvent
        this.dx = contentOffset.y - this.y;
        this.y = contentOffset.y;


        if (contentOffset.y < 110) {
            this.setState({ headerPadding: contentOffset.y / 10, showStickyHeader: false })
            if (Platform.OS === 'ios') {
                StatusBar.setBarStyle('light-content', false);
            } else if (Platform.OS === 'android') {
                StatusBar.setBackgroundColor(commonColors.theme, false);
            }
        } else {
            this.setState({ showStickyHeader: true })
            if (Platform.OS === 'ios') {
                StatusBar.setBarStyle('dark-content', false);
            } else if (Platform.OS === 'android') {
                StatusBar.setBackgroundColor(commonColors.theme, false);
            }
        }
        
        if ( this.scrollLock == false ){
            for ( var i = 0; i < this.state.positionY.length; i ++ ){
                if ( this.y > this.state.positionY[i]+220 && this.y < this.state.positionY[i+1]+220){
                    this.setState({currentFoodGroup:i})
                    break;
                }
            }    
        }
        return;

    }

    renderMovingHeader() {
        let rate = this.state.headerPadding > 0 ? this.state.headerPadding : 0
        let height = 150 - 2.5 * rate
        return (
            <View style={{ position: 'absolute', width: '100%', paddingHorizontal: 15 - rate }} pointerEvents="none">
                <View style={{
                    backgroundColor: 'white', borderRadius: 5, height: height, top: -80,
                    width: '100%', shadowColor: '#999', shadowOpacity: 0.1, shadowRadius: 3, shadowOffset: { height: 4 }
                }}>
                    <Text numberOfLines={1} style={{
                        fontSize: 30 - rate * 0.4, color: commonColors.normalText,
                        marginLeft: 30 + 2 * rate,
                        marginTop: 15 + rate
                    }}>{this.state.title}</Text>
                </View>
                <Text numberOfLines={1} style={{
                    position: 'absolute', left: 45, top: -15, fontSize: 13,
                    color: 'rgba(58,58,72,' + (1 - (2 + rate) / 13) + ')', backgroundColor: 'transparent'
                }}>
                    {this.state.description}</Text>
                <Text numberOfLines={1} style={{
                    position: 'absolute', left: 45, top: 25, fontSize: 13,
                    color: 'rgba(58,58,72,' + (1 - (2 + rate) / 13) + ')', backgroundColor: 'transparent'
                }}>
                    {this.state.preparation}</Text>
            </View>
        )
    }

    onCheck() {
        this.alive = false
        Actions.ViewCart({
            updateData: () => {
                this.alive = true;
                this.forceUpdate()
            }
        });
    }

    renderCheckout() {
        var count = 0
        var price = 0
        _.map(Cache.order, (item) => {
            count = count + item.count
            price = price + item.count * item.price
        })
        if (Cache.order != undefined && Cache.order.length > 0) {
            return (
                <TouchableOpacity onPress={this.onCheck.bind(this)} style={{
                    position: 'absolute', top: screenHeight - 48, width: screenWidth, height: 48, justifyContent: 'center',
                    alignItems: 'center', backgroundColor: commonColors.theme, flexDirection: 'row'
                }}>
                    <View style={{ flex: 2, alignItems: 'flex-start', justifyContent: 'center', paddingLeft: 15 }}><Text style={{ color: '#ffffff', fontSize: 14 }}>${price}</Text></View>
                    <View style={{ flex: 4, alignItems: 'center', justifyContent: 'center' }}><Text style={{ color: '#ffffff', fontSize: 16, fontWeight: '700' }}>CHECK OUT</Text></View>
                    <View style={{ flex: 2, alignItems: 'flex-end', justifyContent: 'center', paddingRight: 15 }}><Text style={{ color: '#ffffff', fontSize: 14, paddingHorizontal: 5, borderRadius: 3, borderWidth: 1, borderColor: 'white' }}>{count}</Text></View>
                </TouchableOpacity>
            )
        }
        return null
    }

    componentWillMount() {
        this.getFood()
    }
    getFood() {
        let mealKindCode = this.props.data.mealKindCodes[this.state.mealIndex]
        businessService.getFood(this.props.data.id, mealKindCode, (err, res) => {
            if (err == null) {
                this.setState({ food: res.items })
                var categories = []
                this.state.positionY = [];
                res.items.map((item) => {
                    categories.push(item.foodType)
                    this.state.positionY.push(0)
                })
                this.setState({ categories: categories })
            } else {
            }
        })
    }

    orderFood(food) {
        this.alive = false
        Actions.Food({
            foodData: food, updateData: () => {
                this.alive = true;
                this.forceUpdate()
                //this.props.updateData()
            }
        });
    }

    onChangeTab(index) {
        this.state.mealIndex = index
        this.getFood()
    }

    renderSelectMealKind() {
        return (
            <View style={{
                height: 160,
                backgroundColor: commonColors.background,
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'flex-end'
            }}>
                <ScrollableTabView
                    style={{ marginTop: 70, }}
                    initialPage={0}
                    tabBarActiveTextColor={commonColors.normalText}
                    tabBarInactiveTextColor={commonColors.textColor2}
                    tabBarUnderlineStyle={{ backgroundColor: 'black', height: 3 }}
                    renderTabBar={() => <ScrollableTabBar style={{ borderColor: commonColors.background }} />}
                    onChangeTab={(obj) => this.onChangeTab(obj.i)}
                >
                    {
                        _.map(this.state.tabName, (o, index) => {
                            return (
                                <View key={index} tabLabel={o} />
                                /*<View key={index} tabLabel={o} style={[styles.container, { flex: 1, backgroundColor: commonColors.background, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }]}>
                                    <IonIcons name="ios-time-outline" size={15} color={commonColors.normalText} />
                                    <Text style={{ marginLeft: 10, color:commonColors.textColor2 }}>10:30 AM - 11:45 PM</Text>
                                </View>*/
                            )
                        })
                    }

                </ScrollableTabView>
            </View>
        )
    }

    getPosition(event, index) {
        this.state.positionY[index] = event.nativeEvent.layout.y

        //console.log('--->', this.state.positionY[index])
    }

    renderContent() {
        return (
            <View style={styles.content}>
                {this.renderSelectMealKind()}
                {this.renderMovingHeader()}
                <View style={{ marginTop: 0, backgroundColor: BACKGROUND_COLOR }} >
                    {this.state.food.map((item, index) => {
                        return (
                            <View key={index} onLayout={(event) => this.getPosition(event, index)}>
                                <Text style={styles.foodGroup}>
                                    {item.foodType}
                                </Text>
                                {item.foods.map((o, index2) => {
                                    return (
                                        <TouchableOpacity key={index2} activeOpacity={0.8}
                                            onPress={() => { this.orderFood(o) }}>
                                            <FoodItem data={o}
                                                isLast={item.foods.length - 1 == index2} />
                                        </TouchableOpacity>
                                    )
                                })}
                                {index != this.state.food.length - 1 &&
                                    <View style={{ height: 0.5, width: '100%', backgroundColor: commonColors.textColor3 }} />}
                            </View>
                        )
                    })}
                </View>
            </View>

        )
    }

    render() {
        let { title } = this.props.data
        return (
            <View style={styles.container}>
                {this.renderBackground()}
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ marginTop: -20 }}
                    ref="businessScroll"
                    onMomentumScrollBegin={()=>this.lockSelect = true}
                    onMomentumScrollEnd={()=>this.lockSelect = false}
                    onScroll={this.handleScroll.bind(this)}
                    scrollEventThrottle={20}>
                    <View style={{ height: 210 }} />
                    {this.renderContent()}
                </ScrollView>
                {this.renderFixedHeader()}
                {this.renderCheckout()}
            </View >
        )
    }

}

export default connect(props => ({
}),
    (dispatch) => ({
        commonActions: bindActionCreators(commonActions, dispatch),
    })
)(Business);


const window = Dimensions.get('window');

const PARALLAX_HEADER_HEIGHT = 210;
const STICKY_HEADER_HEIGHT = 126;
const gradientColors = ['#00000080', '#00000020', '#00000000'];
const gradientPoints = [0, 0.15, 0.25];
const BACKGROUND_COLOR = 'rgb(251,251,253)'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: BACKGROUND_COLOR,
    },
    content: {
        flex: 1,
        backgroundColor: BACKGROUND_COLOR,
    },
    stickySection: {
        height: STICKY_HEADER_HEIGHT,
        backgroundColor: 'white',
        width: '100%',
        justifyContent: 'flex-end'
    },
    stickySectionText: {
        color: 'black',
        fontSize: 20,
        margin: 10
    },
    image: {
        height: 210,
    },
    heart: {
        position: 'absolute',
        top: 15,
        right: 15,
        paddingVertical: 10,
        paddingHorizontal: 5,
    },
    left: {
        position: 'absolute',
        top: 15,
        left: 15,
        paddingVertical: 10,
        paddingHorizontal: 5,
        backgroundColor: 'transparent'
    },
    foodGroup: {
        fontSize: 18,
        color: commonColors.normalText,
        marginLeft: 20,
        marginTop: 20,
    },

})
