
'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Alert,
    Platform,
    Modal,
    Animated,
    StatusBar,
} from 'react-native';

import * as _ from 'underscore'
import { Actions } from 'react-native-router-flux';
import IonIcons from 'react-native-vector-icons/Ionicons'
import LinearGradient from 'react-native-linear-gradient'

import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';
import { screenWidth, screenHeight } from "../../style/commonStyles";

import RadioView from '../../components/home/radioView';

import * as config from '../../config'
import Cache from '../../utils/Cache'


const gradientColors = ['#00000070', '#00000040', '#00000000', '#00000000', '#00000010'];
const gradientPoints = [0, 0.15, 0.35, 0.9, 1.0];

export default class Food extends PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            count: props.count == undefined ? 1 : props.count,
            price: props.foodData.price,
            isRefreshing: false,
            foodData: JSON.parse(JSON.stringify(props.foodData)),
            socialmodalVisible: false,
            title: props.foodData.name,
            buttonEnabled: false,
            description: props.foodData.description,
            foodImage: props.foodData.image == undefined || props.foodData.image == '' ? { uri: config.SERVICE_FILE_URL + props.foodData.foodType.image } : { uri: config.SERVICE_FILE_URL + props.foodData.image }
        }
        this.renderLayoutSocial = this.renderLayoutSocial.bind(this);
        this.state.modalY = new Animated.Value(-100);


        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('light-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }

        this.calcPrice(true)
    }

    renderHeader(bShow) {
        return (
            <View style={{ position: 'absolute', top: 0, width: '100%' }}>
                <TouchableOpacity onPress={() => this.onBack()} style={styles.left}>
                    <IonIcons name="md-close" size={28} color={bShow ? 'black' : 'white'} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { this.setSocialModalVisible(true) }} style={styles.right}>
                    <IonIcons name="md-share" size={24} color={bShow ? 'black' : 'white'} />
                </TouchableOpacity>
            </View>
        )
    }

    componentDidMount() {
        this.refs.scroll.scrollTo({ x: 0, y: 0, animated: false })
    }

    renderBackground() {
        if (this.state.foodImage == null) return null
        return (
            <View>
                <TouchableOpacity onPress={() => { }}>
                    <Image style={styles.image} source={this.state.foodImage} />
                </TouchableOpacity>
                <LinearGradient
                    colors={gradientColors}
                    locations={gradientPoints}
                    style={{ flex: 1, position: 'absolute', height: 210, width: '100%' }}>
                </LinearGradient>
            </View>
        )
    }

    openModal() {
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
        this.setState({ topModalVisible: true });
        Animated.timing(this.state.modalY, {
            duration: 300,
            toValue: 0
        }).start();
    }

    closeModal() {
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('light-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
        this.setState({ topModalVisible: false });
        // console.log('closemodal');
        Animated.timing(this.state.modalY, {
            duration: 200,
            toValue: -100
        }).start();
    }

    onRefresh() {

    }

    onBack() {
        if ( this.props.updateData){
            this.props.updateData()
        }
        Actions.pop()
    }
    setSocialModalVisible(visible) {
        // console.log('setModalVisible');
        this.setState({ socialmodalVisible: visible });

    }
    renderLayoutSocial() {
        // console.log('layout social')
        if (this.state.socialmodalVisible) {
            return (<TouchableOpacity onPress={() => { this.setSocialModalVisible(!this.state.socialmodalVisible) }} style={{ width: screenWidth, height: screenHeight, position: 'absolute', top: 0, left: 0, backgroundColor: 'rgba(0, 0, 0, 0.5)' }}></TouchableOpacity>)
        }
    }

    handleScroll(event) {
        // console.log(event.nativeEvent.contentOffset.y);
        if (event.nativeEvent.contentOffset.y > 130) {
            if (!this.state.topModalVisible) this.openModal();
        }
        else {
            if (this.state.topModalVisible) this.closeModal();
        }
    }

    renderOptions(options) {
        return (
            <View style={{ marginTop: 40 }}>
                {options.map((item, index) => {
                    if (item.enalbed == false) return null
                    return (
                        <View key={index} >
                            <View style={{
                                backgroundColor: commonColors.background, flexDirection: 'row', paddingVertical: 8,
                                borderWidth: 0.5, borderColor: commonColors.textColor1, paddingHorizontal: 15
                            }}>
                                <Text style={{ color: commonColors.textColor1, fontSize: 13, flex: 1 }}>{item.title}</Text>
                                <View style={{ backgroundColor: commonColors.textColor1, justifyContent: 'center', alignItems: 'center' }}>
                                    {item.isRequired && <Text style={{ color: 'white', fontSize: 10, marginHorizontal: 5 }}>REQUIRED</Text>}
                                </View>
                            </View>
                            <RadioView data={item.options} single={item.isSingle} update={(data) => {
                                this.state.foodData.foodType.foodOptions[index].options = JSON.parse(JSON.stringify(data))
                                this.calcPrice()
                            }} />
                        </View>
                    )
                })}
            </View>
        )
    }

    onAddCard() {
        if (Cache.order == undefined) {
            Cache.order = []
        }
        if (Cache.order.length > 0 && Cache.businessId != Cache.currentBusinessId) {
            Alert.alert(
                'Confirm',
                'Do you want to clear all added carts?',
                [
                    {
                        text: 'Cancel', onPress: () => {

                        }
                    },
                    {
                        text: 'OK', onPress: () => {
                            Cache.order = []
                            Cache.businessId = Cache.currentBusinessId
                            Cache.order.push({ count: this.state.count, data: this.state.foodData, price: this.state.price })
                            this.props.updateData()
                            Actions.pop()
                        }
                    },
                ],
                { cancelable: false }
            )
            return
        }
        Cache.businessId = Cache.currentBusinessId
        Cache.order.push({ count: this.state.count, data: this.state.foodData, price: this.state.price, instruction: "hello" })
        this.props.updateData()
        // console.log(Cache.order)
        Actions.pop()
    }

    onDecCount() {
        if (this.state.count > 1) {
            this.setState({ count: this.state.count - 1 })
        }
    }

    onIncCount() {
        this.setState({ count: this.state.count + 1 })
    }

    removeOrder() {
        Cache.order.splice(this.props.orderNumber, 1)
        this.props.updateData()
        Actions.pop()
    }

    updateOrder() {
        Cache.order[this.props.orderNumber] = { count: this.state.count, data: this.state.foodData, price: this.state.price }
        this.props.updateData()
        Actions.pop()
    }

    calcPrice(first) {
        var price = this.state.foodData.price
        var buttonEnabled = true
        _.map(this.state.foodData.foodType.foodOptions, (item) => {
            var checked = false
            _.map(item.options, (o) => {
                if (o.enabled == true && o.default == true) {
                    checked = true
                    price = price + o.price
                }
            })
            if (checked == false && item.isRequired == true && item.enabled == true) {
                buttonEnabled = false
            }
        })
        if (first == true) {
            this.state.price = price
            this.state.buttonEnabled = buttonEnabled
        } else {
            this.setState({ price: price, buttonEnabled: buttonEnabled })
        }

    }

    render() {

        return (
            <View style={{ padding: 0 }}>
                <ScrollView
                    ref="scroll"
                    style={styles.scrollView}
                    showsVerticalScrollIndicator={false}
                    scrollEventThrottle={1}
                    onScroll={this.handleScroll.bind(this)}>

                    {this.renderBackground()}
                    <View style={styles.part2}>
                        <Text style={{ color: '#000000', fontSize: 25 }}>{this.state.title}</Text>
                        <View style={{ height: 2, marginTop: 20, width: 50, backgroundColor: '#9c9da2' }} />
                    </View>

                    <View style={styles.part2}>
                        <Text style={{ color: commonColors.textColor1, fontSize: 15 }}>{this.state.description}</Text>
                    </View>
                    {this.props.foodData.foodType.foodOptions != undefined && this.props.foodData.foodType.foodOptions.length > 0 &&
                        this.renderOptions(this.props.foodData.foodType.foodOptions)
                    }
                    <View>
                        <View style={{
                            borderWidth: 0.5, borderColor: commonColors.textColor1, flex: 1,
                            paddingHorizontal: 15, paddingVertical: 8, backgroundColor: commonColors.background,
                            justifyContent: 'center'
                        }}>
                            <Text style={{ flex: 1, fontSize: 14, color: commonColors.textColor1 }}>SPECIAL INSTRUCTIONS</Text>
                        </View>
                        <View style={{ margin: 15, borderBottomWidth: 0.5, borderBottomColor: commonColors.textColor1 }}>
                            <TextInput
                                style={{ height: 40 }}
                                onChangeText={(text) => this.setState({ text })}
                                placeholder={"Add a note (extra sauce, no onions, etc.)"}
                                placeholderTextColor={commonColors.textColor3}
                            />
                        </View>
                    </View>
                    <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                        <View style={{ flexDirection: 'row', height: 48, width: 120, borderRadius: 23, borderWidth: 1, borderColor: commonColors.textColor1, justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity
                                onPress={() => this.onDecCount()}
                                style={{ flex: 1, alignItems: 'center' }}
                            >
                                <Text style={{ fontSize: 28, fontWeight: 'bold' }}>-</Text>
                            </TouchableOpacity>
                            <Text style={{ fontSize: 18 }}>{this.state.count}</Text>
                            <TouchableOpacity onPress={() => this.onIncCount()} style={{ flex: 1, alignItems: 'center' }}>
                                <Text style={{ fontSize: 28 }}>+</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    {this.props.orderNumber == undefined &&
                        <TouchableOpacity disabled={!this.state.buttonEnabled} onPress={() => this.onAddCard()} style={{
                            height: 48, margin: 10, marginTop: 50, backgroundColor:
                            this.state.buttonEnabled ? commonColors.theme : 'grey', justifyContent: 'center', alignItems: 'center', borderRadius: 3
                        }}>
                            <Text style={{ fontSize: 21, color: 'white' }}>ADD {this.state.count} TO CART</Text>
                            <View style={{ position: 'absolute', right: 10, alignItems: 'center', justifyContent: 'flex-end' }}>
                                <Text style={{ color: 'white' }}>${this.state.price * this.state.count}</Text>
                            </View>
                        </TouchableOpacity>
                    }
                    {this.props.orderNumber != undefined &&
                        <View style={{ justifyContent: 'center', marginTop: 30, margin: 10 }}>
                            <TouchableOpacity disabled={!this.state.buttonEnabled} onPress={() => this.removeOrder()} style={{ alignItems: 'center' }}>
                                <Text style={{ color: 'grey' }}>REMOVE ITEM FROM CART</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.updateOrder()} style={{
                                height: 48, marginTop: 20,
                                backgroundColor: this.state.buttonEnabled ? commonColors.theme : 'grey',
                                justifyContent: 'center', alignItems: 'center', borderRadius: 3
                            }}>
                                <Text style={{ fontSize: 21, color: 'white' }}>UPDATE CART</Text>
                            </TouchableOpacity>
                        </View>
                    }
                </ScrollView>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.socialmodalVisible}
                >
                    {this.renderLayoutSocial()}
                    <View style={{ position: 'absolute', right: 0, bottom: 0, backgroundColor: '#ffffff', width: screenWidth }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', padding: 10 }}>
                            <Text style={{ fontSize: 20 }}>Share with</Text>
                        </View>
                        <View style={{ flexDirection: 'row', padding: 10 }}>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('../../../public/images/social/gogo_social_wechat.png')} style={{ width: 50, height: 50 }} />

                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('../../../public/images/social/gogo_social_messages.png')} style={{ width: 50, height: 50 }} />

                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('../../../public/images/social/gogo_social_bluetooth.png')} style={{ width: 50, height: 50 }} />

                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('../../../public/images/social/gogo_social_notes.png')} style={{ width: 50, height: 50 }} />

                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('../../../public/images/social/gogo_social_more.png')} style={{ width: 50, height: 50 }} />

                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', padding: 10 }}>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                <Text>WeChat</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                <Text>Mes- sages</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                <Text>Blue- tooth</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>

                                <Text>Notes</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                <Text>More</Text>
                            </View>
                        </View>
                        <TouchableOpacity onPress={() => { this.setSocialModalVisible(!this.state.socialmodalVisible) }} style={{ justifyContent: 'center', alignItems: 'center', padding: 10 }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                {this.renderHeader(false)}
                <Animated.View style={[styles.modal, { transform: [{ translateY: this.state.modalY }] }]}>
                    <View style={{ flex: 1, height: 50, paddingHorizontal: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontWeight: 'bold', color: commonColors.normalText, fontSize: 15 }}>{this.state.title}</Text>
                    </View>
                    {this.renderHeader(true)}
                </Animated.View>

            </View>

        )
    }
}

const styles = StyleSheet.create({
    modal: {
        paddingTop: 15,
        width: screenWidth,
        position: 'absolute',
        top: 0,
        left: 0,
        backgroundColor: '#ffffff',
        flexDirection: 'column',
    },
    container: {
        width: screenWidth,
        flexDirection: 'row',
    },
    scrollView: {
        flexDirection: 'column',
        marginTop: -21,
    },
    part1: {
        flexDirection: 'column',
        height: screenHeight * 2 / 5,
    },
    part2: {
        alignItems: 'center',
        paddingVertical: 20,

    },
    buttonWrap: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        backgroundColor: 'transparent',
    },
    button: {
        justifyContent: 'center',
    },
    image: {
        width: 14,
        height: 14,
    },

    toppart: {
        padding: 20,
        flexDirection: 'column',
        alignItems: 'center',
    },
    bottompart: {
        marginTop: 30,
        padding: 10,
        flex: 1,
        alignItems: 'center',
    },
    component1: {
        flexDirection: 'row',
        justifyContent: 'center',
        width: 150,
        height: 50,
        borderWidth: 1,
        borderColor: '#cacbcf',
        padding: 10,
        borderRadius: 20

    },
    textTitle: {
        fontSize: 20,
        flex: 1,
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'center'
    },
    textTitle1: {
        fontSize: 20,
        flex: 1,
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        marginLeft: screenWidth / 2 - 85,
        color: '#ffffff',
    },
    image: {
        height: 210,
    },
    right: {
        position: 'absolute',
        top: 16,
        right: 15,
        paddingVertical: 10,
        paddingHorizontal: 5,
        backgroundColor: 'transparent'
    },
    left: {
        position: 'absolute',
        top: 13,
        left: 15,
        paddingVertical: 10,
        paddingHorizontal: 5,
        backgroundColor: 'transparent'
    },
})
