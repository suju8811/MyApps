'use strict';

import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    Animated,
    PermissionsAndroid,
    Platform,
    ListView,
    StatusBar,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import IonIcons from 'react-native-vector-icons/Ionicons'
import StarRatingBar from 'react-native-star-rating-view'

import * as commonColors from '../../style/commonColors'
import * as commonStyles from '../../style/commonStyles'
import { screenHeight, screenWidth } from '../../style/commonStyles';

import businessService from '../../service/businessService'
import baseService from '../../service/baseService'
import orderService from '../../service/orderService'
import Cache from '../../utils/Cache'
import * as config from '../../config'

const ImageSize = 100
export default class RatingScreen extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            step: 0,
            restaurantScore: 0,
            restaurantFeedback: '',
            driverScore: 0,
            driverFeedback: '',
            foodScores: [],
            order: this.props.data,
        }
        for ( var i = 0; i < this.props.data.length; i ++ ){
            this.state.foodScores.push(0)
        }
    }

    renderFirst() {
        this.state.order.driver = {
            name: 'jacky',
            logo: this.state.order.business.logo,

        }
        return (
            <View style={[styles.page, { alignItems: 'center', paddingTop: 50, paddingHorizontal: 20, height: 380 }]}>
                <View style={{ marginTop: -ImageSize / 2, width: '100%', position: 'absolute', justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={{ uri: config.SERVICE_FILE_URL + this.state.order.driver.logo }} style={{ width: ImageSize, height: ImageSize, borderRadius: ImageSize / 2 }} />
                </View>
                <Text style={{ fontSize: 32, color: commonColors.normal, textAlign: 'center', marginTop: 20 }}>How was {this.state.order.driver.name}'s Delivery?</Text>
                <Text style={{ color: commonColors.textColor1, textAlign: 'center', fontSize: 18, marginTop: 20 }}>Your feedback will help improved the delivery experience</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: '100%', margin: 20 }}>
                    <TouchableOpacity onPress={() => this.setState({ driverScore: -1 })} style={{
                        justifyContent: 'center', alignItems: 'center', borderRadius: 30, width: 60, height: 60,
                        borderColor: commonColors.textColor1, borderWidth: 1, margin: 8, backgroundColor: this.state.driverScore < 0 ? commonColors.normalText : 'white'
                    }}>
                        <IonIcons name="ios-thumbs-down-outline" size={28} color={this.state.driverScore < 0 ? 'white' : commonColors.normalText} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ driverScore: 1 })} style={{
                        justifyContent: 'center', alignItems: 'center', borderRadius: 30, width: 60, height: 60,
                        borderColor: commonColors.textColor1, borderWidth: 1, margin: 8, backgroundColor: this.state.driverScore > 0 ? commonColors.normalText : 'white'
                    }}>
                        <IonIcons name="ios-thumbs-up-outline" size={28} color={this.state.driverScore > 0 ? 'white' : commonColors.normalText} />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={() => this.getNote(1)} >
                    <Text style={{ color: commonColors.theme }}>Add a note</Text>
                </TouchableOpacity>
            </View>
        )
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        })
    }

    renderSecond() {
        return (
            <View style={[styles.page, { alignItems: 'center', paddingTop: 50, paddingHorizontal: 20, height: 300 }]}>
                <View style={{ marginTop: -ImageSize / 2, width: '100%', position: 'absolute', justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={commonStyles.introduceImg} style={{ width: ImageSize, height: ImageSize, borderRadius: ImageSize / 2 }} />
                </View>
                <Text style={{ fontSize: 32, color: commonColors.normal, textAlign: 'center', marginTop: 20 }}>How was {this.state.order.business.name}'s Belen?</Text>
                <Text style={{ color: commonColors.textColor1, textAlign: 'center', fontSize: 18, marginTop: 20 }}>Rate your overall experience</Text>
                <StarRatingBar
                    starStyle={{
                        width: 20,
                        height: 20,
                    }}
                    readOnly={false}
                    continuous={true}
                    allowsHalfStars={true}
                    onStarValueChanged={(score) => {
                        this.setState({ restaurantScore: score });
                    }}
                />
            </View>
        )
    }

    renderFood(item, index) {
        return (
            <View key={index} style={{ flexDirection: 'row', alignItems: 'center', width: '100%', marginTop: 20 }}>
                <Text style={{ color: commonColors.normalText, fontSize: 16, flex: 1 }}>{item.food.food.name}</Text>
                <TouchableOpacity onPress={() => {
                    this.state.foodScores[index] = -1
                    this.setState({ foodScores: [...this.state.foodScores] })
                }} style={{
                    justifyContent: 'center', alignItems: 'center', borderRadius: 20, width: 40, height: 40,
                    borderColor: commonColors.textColor1, borderWidth: 1, margin: 5, backgroundColor: this.state.foodScores[index] < 0 ? commonColors.normalText : 'white'
                }}>
                    <IonIcons name="ios-thumbs-down-outline" size={20} color={this.state.foodScores[index] < 0 ? 'white' : commonColors.normalText} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    this.state.foodScores[index] = 1
                    this.setState({ foodScores: [...this.state.foodScores] })
                }}
                    style={{
                        justifyContent: 'center', alignItems: 'center', borderRadius: 20, width: 40, height: 40,
                        borderColor: commonColors.textColor1, borderWidth: 1, margin: 5, backgroundColor: this.state.foodScores[index] > 0 ? commonColors.normalText : 'white'
                    }}>
                    <IonIcons name="ios-thumbs-up-outline" size={20} color={this.state.foodScores[index] > 0 ? 'white' : commonColors.normalText} />
                </TouchableOpacity>
            </View>
        )
    }

    getNote(index) {
        Actions.GetNote({
            note: index == 3 ? this.state.restaurantFeedback : (index == 1 ? this.state.driverFeedback : ''), updateData: (note) => {
                if (index == 3) {
                    this.setState({ restaurantFeedback: note })
                }
                if (index == 1) {
                    this.setState({ driverFeedback: note })
                }
                //console.log(index)
            }
        })
    }

    renderThird() {
        return (
            <View>
                <View style={[styles.page, { alignItems: 'center', paddingTop: 50, paddingHorizontal: 20, paddingBottom: 30 }]}>
                    <View style={{ marginTop: -ImageSize / 2, width: '100%', position: 'absolute', justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={commonStyles.introduceImg} style={{ width: ImageSize, height: ImageSize, borderRadius: ImageSize / 2 }} />
                    </View>
                    <Text style={{ fontSize: 32, color: commonColors.normal, textAlign: 'center', marginTop: 20 }}>How was {this.state.order.business.name}'s Belen?</Text>
                    <Text style={{ color: commonColors.textColor1, textAlign: 'center', fontSize: 16, marginTop: 20 }}>Your feedback will help the restaurants improve</Text>
                    {this.state.order.foods.map((item, index) => {
                        return this.renderFood(item, index)
                    })}
                    <TouchableOpacity onPress={() => this.getNote(3)} style={{ marginTop: 20 }}>
                        <Text style={{ color: commonColors.theme }}>Leave more feedback</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    nextPage() {
        if (this.state.step == 2) {
            orderService.submitRate(
                this.state.order.id,
                this.state.restaurantScore,
                this.state.restaurantFeedback,
                this.state.driverScore < 0 ? 1 : (this.state.driverScore == 0 ? 3 : 5),
                this.state.driverFeedback,
                (err, res) => {
                    // console.log(this.state.order.id,
                    //     this.state.restaurantScore,
                    //     this.state.restaurantFeedback,
                    //     this.state.driverScore < 0 ? 1 : (this.state.driverScore == 0 ? 3 : 5),
                    //     this.state.driverFeedback, err, res)
                    this.props.updateData()
                    Actions.pop()
                }
            )
            return;
        }
        this.setState({ step: this.state.step + 1 })
        setTimeout(() => {
            this.refs.scroll.scrollTo({ x: this.state.step * (screenWidth - 40), y: 0, animated: true })
        }, 10)
    }

    prevPage() {
        if (this.state.step == 0) {
            this.props.updateData()
            Actions.pop()
            return;
        }
        this.setState({ step: this.state.step - 1 })
        setTimeout(() => {
            this.refs.scroll.scrollTo({ x: this.state.step * (screenWidth - 40), y: 0, animated: true })
        }, 10)

    }

    renderHeader() {
        return (
            <View style={{ position: 'absolute', top: 0, width: '100%' }}>
                <TouchableOpacity onPress={() => this.prevPage()} style={styles.left}>
                    <IonIcons name={this.state.step == 0 ? "ios-close" : "ios-arrow-round-back"} size={28} color={'black'} />
                </TouchableOpacity>
                {this.state.step < 2 && <TouchableOpacity onPress={() => { this.nextPage() }} style={styles.right}>
                    <Text style={{ color: 'black' }}>Skip</Text>
                </TouchableOpacity>}
            </View>
        )
    }

    render() {
        let bShow = (this.state.step == 0 && this.state.restaurantScore > 0) ||
            (this.state.step == 1 && this.state.driverScore != 0) ||
            (this.state.step == 2)
        return (
            <View style={styles.container}>
                <ScrollView
                    ref="scroll"
                    horizontal={true}
                    scrollEnabled={false}
                >
                    <View style={{ width: 20 }} />
                    {this.renderSecond()}
                    {this.renderFirst()}
                    {this.renderThird()}
                </ScrollView>
                {bShow && <TouchableOpacity onPress={() => this.nextPage()}
                    style={{
                        backgroundColor: commonColors.theme, justifyContent: 'center', alignItems: 'center',
                        height: 48, width: '100%'
                    }}>
                    <Text style={{ color: 'white' }}>{this.state.step == 2 ? 'Done' : 'Continue'}</Text>
                </TouchableOpacity>}
                {this.renderHeader()}
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: commonColors.background
    },
    page: {
        marginTop: 150,
        width: screenWidth - 50,
        marginHorizontal: 5,
        borderRadius: 5,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOpacity: 0.2,
        shadowRadius: 3,
        shadowOffset: { height: 1 }
    },
    right: {
        position: 'absolute',
        top: 16,
        right: 15,
        paddingVertical: 10,
        paddingHorizontal: 5,
        backgroundColor: 'transparent'
    },
    left: {
        position: 'absolute',
        top: 13,
        left: 15,
        paddingVertical: 10,
        paddingHorizontal: 5,
        backgroundColor: 'transparent'
    },
})
