'use strict';

import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    Animated,
    PermissionsAndroid,
    Platform,
    ListView,
    StatusBar,
    Linking,
} from 'react-native';

import { Actions, ActionConst, Scene, Router } from 'react-native-router-flux';
import MapView from 'react-native-maps'
import isEqual from 'lodash/isEqual';
import { TouchThroughView, TouchThroughWrapper } from 'react-native-touch-through-view'
import LinearGradient from 'react-native-linear-gradient'
import IonIcons from 'react-native-vector-icons/Ionicons'
import Modal from 'react-native-modal'
import phoneCall from 'react-native-phone-call'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as commonActions from '../../redux/actions';
import * as commonActionTypes from '../../redux/actionTypes';

import * as commonColors from '../../style/commonColors'
import * as commonStyles from '../../style/commonStyles'
import { screenHeight, screenWidth } from '../../style/commonStyles';

import businessService from '../../service/businessService'
import orderService from '../../service/orderService'
import baseService from '../../service/baseService'
import googleService from '../../service/googleService'
import UtilService from '../../utils/util'
import Cache from '../../utils/Cache'
import * as config from '../../config'
import * as _ from 'underscore'

class OrderStatus extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            mapHeight: commonStyles.viewHeight,
            mapTop: 0,
            region: null,
            latitudeDelta: 0.1,
            myPosition: null,
            contentHeight: 1000,
            old: 0,
            dir: 0,
            momentum: false,
            showStatus: false,
            arrivalPercent: 0,
            curTime: '',
            showReceipt: false,
            orders: Cache.upcomingOrder.foods,
            orderAccepted: false,
            tripDropped: false,
            orderNumber: Cache.upcomingOrder.number,
            businessId: Cache.upcomingOrder.businessId,
            business: null,
            totalPrice: 0,
            subTotal: 0,
            tax: 0,
            bookingFee: 0,
            notifications: [
                {
                    time: this.getCurrentTime(),
                    description: 'Confirming order with restaurant'
                }
            ],
            seconds: 0,
            driverData: {
                driver:{
                    phone: '9093900003',
                    firstname : 'jacky',
                    lastname: 'chan',
                    avatar: require('../../../public/images/gogo_user.png'),
                    rating: 4.8,
                },
                vehicle:{
                    number:'JHS42345'
                }
            },
            visibleContactDialog: false,
        }

        this.hasMounted = false
        this.oldCarPosition = null
    }

    _initConnection(channel) {
        UtilService.getWebSocketConnection(channel, (err, con) => {
            if (err) {
                console.log("connection error:", err)
                // this._showErrorConnection()
                return;
            }
            this.connection = con
            this._subscribeSocket(channel);
        })
    }

    _sendMessage(msg) {
        if (this.subscription == null) return
        this.subscription.publish(msg).then(() => {
            // success ack from Centrifugo received
            console.log("publish success: ", this.subscription);
        }, (err) => {
            // publish call failed with error
            console.log("publish error: ", err);
        });
    }

    _disconnect() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        if (this.connection) {
            this.connection.disconnect();
            this.connection = null;
        }
    }

    calcHeading(cur, old) {
        if (old == null) return 0
        let rate = 3.14159265358979323846264338327950288 / 180
        let X = Math.cos(cur.coords.latitude * rate) * Math.sin(cur.coords.longitude * rate - old.coords.longitude * rate)
        let Y = Math.cos(old.coords.latitude * rate) * Math.sin(cur.coords.latitude * rate)
            - Math.sin(old.coords.latitude * rate) * Math.cos(cur.coords.latitude * rate) * Math.cos(cur.coords.longitude * rate - old.coords.longitude * rate)
        return Math.atan2(X, Y) / rate
    }

    setCarPosition(pos) {
        pos.coords.heading = this.calcHeading(pos, this.oldCarPosition)

        this.oldCarPosition = pos

        if (this.oldCarPosition != null) {
            this.setState({ driverPosition: pos })
        }

    }

    _subscribeSocket(channel) {
        var callbacks = {
            "message": (message) => {
                // See below description of message format
                this.setCarPosition(JSON.parse(message.data))
                console.log(JSON.parse(message.data))
            },
            "join": (message) => {
                // See below description of join message format
                console.log("join", message);
            },
            "leave": (message) => {
                // See below description of leave message format
                console.log("leave", message);
            },
            "subscribe": (context) => {
                // See below description of subscribe callback context format
                console.log("subscribe", context);
            },
            "error": (errContext) => {
                // See below description of subscribe error callback context format
                console.log("error", err);
            },
            "unsubscribe": (context) => {
                // See below description of unsubscribe event callback context format
                console.log("unsubscribe", context);
            },
            "presence": (message) => {
                console.log("message", message);
            }
        }

        this.subscription = this.connection.subscribe("public:" + channel, callbacks)
    }

    getCurrentTime() {
        let date = new Date()
        let hours = '' + date.getHours()
        let minutes = '' + date.getMinutes();

        return ((hours.length == 1 ? '0' + hours : hours) + ':' + (minutes.length == 1 ? '0' + minutes : minutes))
    }

    componentWillReceiveProps(nextProps) {
        let { commonStatus, notification } = nextProps
        if (commonStatus == commonActionTypes.RECEIVED_NOTIFICATION) {
            // console.log(notification)
            if (notification) {
                let { type, orderId } = notification.payload.additionalData
                this.receiveOrder(type, orderId)
            }
        }
    }

    componentWillUnmount() {
        this.hasMounted = false
    }

    calcPrice() {
        var total = 0
        _.map(Cache.order, (item) => {
            total = total + item.price * item.count
        })
        this.setState({
            tax: Cache.tax,
            bookingFee: Cache.bookingFee,
            subTotal: total,
            totalPrice: total + Cache.bookingFee,
        })

    }

    componentDidMount() {
        this.calcPrice()
        businessService.getBesiness(this.state.businessId, (err, res) => {
            //            console.log(this.state.businessId, err, res, Cache.selectedLocation)
            if (err == null) {
                this.state.business = res;
                let latitude = (res.geoLocation.geoJson.coordinates[1] + Cache.selectedLocation.geoJson.coordinates[1]) / 2
                let longitude = (res.geoLocation.geoJson.coordinates[0] + Cache.selectedLocation.geoJson.coordinates[0]) / 2
                let latitudeDelta = Math.abs(res.geoLocation.geoJson.coordinates[1] - Cache.selectedLocation.geoJson.coordinates[1])
                let longitudeDelta = Math.abs(res.geoLocation.geoJson.coordinates[0] - Cache.selectedLocation.geoJson.coordinates[0])
                this.setState({
                    region: {
                        latitude: latitude - latitudeDelta * 0.2,
                        longitude: longitude,
                        latitudeDelta: latitudeDelta * 1.5,
                        longitudeDelta: longitudeDelta * 1.1
                    }
                })
            }
        })
        this.hasMounted = true
        var interval = setInterval(() => {
            if (this.hasMounted == false || this.state.arrivalPercent >= 100 ) {
                clearInterval(interval)
            } else {
                this.state.seconds = this.state.seconds + 1
                this.setState({ curTime: this.getCurrentTime(), arrivalPercent: this.state.seconds / 18 })
            }
        }, 1000)

        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    receiveOrder(type, orderId) {
        console.log("received notification", type, orderId)
        switch (type) {
            case config.ORDER_ACCEPTED:
                this.setState({
                    notifications: [...this.state.notifications, {
                        time: this.getCurrentTime(),
                        description: 'Preparing your order'
                    }],
                    orderAccepted: true
                })
                break
            case config.ORDER_PREPARED:
                this.setState({
                    notifications: [...this.state.notifications, {
                        time: this.getCurrentTime(),
                        description: 'Prepared your order'
                    }]
                })
                break
            case config.TRIP_DROPPED:
                this._disconnect()
                this.setState({
                    notifications: [...this.state.notifications, {
                        time: this.getCurrentTime(),
                        description: 'Foods dropped'
                    }],
                    tripDropped: true,
                })
                break
            case config.TRIP_CONFIRMED:
                orderService.getOrder(orderId, (err, res) => {
                    console.log('---->confirmed', res)
                    this.setState({driverData: {
                        driver:{
                            phone: res.driver.phone,
                            firstname : res.driver.firstname,
                            lastname: res.driver.lastname,
                            avatar: res.driver.avatar?{uri:config.SERVICE_FILE_URL+res.driver.avatar}:require('../../../public/images/gogo_user.png'),
                            rating: res.driver.rating,
                        },
                        vehicle:{
                            number:res.driver.driverVehicles[0].number
                        }
                    }})
                    this._initConnection(res.driverId)
                })
            default:
                this.setState({
                    notifications: [...this.state.notifications, {
                        time: this.getCurrentTime(),
                        description: type
                    }]
                })
                break

        }
    }

    goBack() {
        Cache.order = []
        Cache.requestedOrder = null;
        if (this.props.updateData) {
            this.props.updateData()
        }
        Actions.pop()
    }

    renderItem(title) {
        return (
            <TouchableOpacity style={{ flex: 1, flexDirection: 'row', width: '100%', justifyContent: 'space-between' }}>
                <Text style={{ color: commonColors.greyfont, fontSize: 12 }}>{title}</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    <IonIcons name="ios-arrow-forward" size={15} color={commonColors.textColor2} />
                </View>
            </TouchableOpacity>
        )
    }

    renderContent(title, content, img) {
        return (
            <View style={{ flexDirection: 'row' }}>
                <View style={{ width: '60%' }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 20, marginTop: 15 }}>
                        {title}
                    </Text>
                    <Text style={{ marginTop: 20, marginBottom: 20 }}>
                        {content}
                    </Text>
                </View>
                <View style={{ width: '40%', justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                    <Image source={img}
                        style={{ width: 100, height: 100 }} />
                </View>
            </View>
        )
    }

    handleScroll(event) {
    }

    onRegionChange(region) {
        this.setState({ region: region });
    }

    contactWithDriver() {
        this.setState({visibleContactDialog:true})
    }

    renderDriverItem() {
        if (this.state.driverData == null) return null;
        let {firstname, lastname, avatar, rating} = this.state.driverData.driver
        let {number} = this.state.driverData.vehicle
        return (
            <View style={[styles.viewStyle, { paddingHorizontal: 0, paddingBottom: 0 }]}>
                <View style={{ paddingHorizontal: 20, paddingBottom: 20, flexDirection:'row', alignItems:'center' }}>
                    <Image source={avatar} style={{width:50, height:50}}/>
                    <View style={{marginLeft:20, flex:1, flexDirection:'row'}}>
                        <Text style={{ fontSize: 15, color: commonColors.textColor1, fontSize:13}}>{firstname} {lastname} {rating}</Text>
                        <IonIcons name="ios-star" size={14} color={'black'} style={{marginLeft:3}}/>
                    </View>
                    <Text style={{ fontSize: 15, color: commonColors.textColor1, fontSize:13}}>{number}</Text>
                </View>

                <TouchableOpacity onPress={() => { this.contactWithDriver() }} style={{
                    borderTopColor: commonColors.background, borderTopWidth: 0.5, backgroundColor: 'white',
                    height: 40, width: '100%', justifyContent: 'center', alignItems: 'center'
                }}>
                    <Text style={{ fontSize: 13, color: commonColors.normalText }}>
                        CONTACT
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderFirstItem() {
        return (
            <View style={[styles.viewStyle, { paddingHorizontal: 0, paddingBottom: 0 }]}>
                <View style={{ paddingHorizontal: 20, paddingBottom: 20 }}>
                    {this.state.business != null && <Text style={{ fontSize: 15, color: commonColors.textColor1, marginTop: 10 }}>
                        {this.state.business.name}
                    </Text>}
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'flex-end' }}>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end' }}>
                            <Text style={{ fontSize: 44, color: commonColors.normalText }}>
                                {this.state.curTime}
                            </Text>
                            {/*<Text style={{ fontSize: 15, color: commonColors.textColor2, marginBottom: 8 }}>AM</Text>*/}
                        </View>
                        <Text style={{ fontSize: 15, color: commonColors.textColor1, marginBottom: 8 }}>Estimated arrival</Text>
                    </View>
                    <View style={{ backgroundColor: commonColors.background, height: 3, width: '100%', marginTop: 20, borderRadius: 3 }}>
                        <View style={{ backgroundColor: commonColors.theme, height: 3, width: this.state.arrivalPercent + '%' }} />
                    </View>
                    <TouchableOpacity onPress={() => this.setState({ showStatus: !this.state.showStatus })} style={{ flexDirection: 'row', marginTop: 20 }}>
                        <Text style={{ fontSize: 15, color: commonColors.textColor2, flex: 1 }}>
                            {this.state.notifications[this.state.notifications.length - 1].description}
                        </Text>
                        <IonIcons name={this.state.showStatus ? "ios-arrow-up" : "ios-arrow-down"}
                            size={20} color={commonColors.textColor1} />
                    </TouchableOpacity>
                    {this.state.showStatus == true && <View style={{}}>
                        {this.state.notifications.map((item, index) => {
                            return (
                                <View key={index} style={{ flexDirection: 'row', marginTop: 15 }}>
                                    <Text style={{ fontSize: 13, color: commonColors.textColor3 }}>{item.time}</Text>
                                    <Text style={{ fontSize: 13, color: commonColors.textColor1, marginLeft: 15 }}>{item.description}</Text>
                                </View>
                            )
                        })}
                    </View>}
                </View>
                {this.renderCancelButton()}
            </View>
        )
    }

    renderSecondItem() {
        return (
            <View style={styles.viewStyle}>
                <Text style={{ fontSize: 13, color: commonColors.textColor1, marginTop: 10 }}>
                    {'#' + this.state.orderNumber}
                </Text>
                <View style={{ marginLeft: -15 }}>
                    {this.state.orders.map((item, index) => {
                        return this.renderOrders(index, false)
                    })}
                </View>
                <View style={{ flexDirection: 'row', marginTop: 20, marginLeft: 35 }}>
                    <Text style={{ flex: 1, fontSize: 16, color: commonColors.normalText }}>{'Total: $' + this.state.totalPrice}</Text>
                    <TouchableOpacity onPress={() => this.setState({ showReceipt: true })}>
                        <Text style={{ fontSize: 13, color: commonColors.textColor2 }}>VIEW RECEIPT</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderOptions(options) {
        return (
            options.map((item, index) => {
                if (item.enabled == false) return null
                return (<View key={index}>
                    {item.options.map((node, index2) => {
                        if (node.default == true) {
                            return (
                                <Text key={index2} style={{ fontSize: 12, color: commonColors.textColor1, marginTop: 5 }}>
                                    {node.name + (node.price > 0 ? ' ($' + node.price + ')' : '')}
                                </Text>)
                        }

                    })}
                </View>)
            })
        )
    }

    renderOrders(index, isDetail) {
        let bFirst = (index == 0)
        let { count, food, price } = this.state.orders[index]
        let options = food.food.foodType.foodOptions
        return (
            <View key={index} style={{
                flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 10, alignItems: 'flex-start',
                borderTopColor: commonColors.background, borderTopWidth: bFirst ? 0 : 0.5
            }}>
                <View style={{ borderWidth: 0.5, borderColor: commonColors.background, paddingHorizontal: 4 }}>
                    <Text style={{ fontSize: 12, color: commonColors.theme }}>{count}</Text>
                </View>
                <View style={{ marginLeft: 15, flex: 1 }}>
                    <Text style={{ fontSize: 15, color: commonColors.normalText }}>{food.food.name}</Text>
                    {isDetail && this.renderOptions(options)}
                </View>
                {isDetail && <Text style={{ marginLeft: 15, fontSize: 15, color: commonColors.normalText }}>{'$' + (count * price)}</Text>}
            </View>
        )
    }

    renderPriceTable() {
        return (
            <View style={{ borderTopColor: commonColors.background, borderTopWidth: 0.5, paddingHorizontal: 20, paddingVertical: 15 }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 12, color: commonColors.textColor2, flex: 1 }}>Subtotal</Text>
                    <Text style={{ fontSize: 12, color: commonColors.textColor2 }}>{'$' + this.state.subTotal}</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 15 }}>
                    <Text style={{ fontSize: 12, color: commonColors.textColor2, flex: 1 }}>Delivery Fee</Text>
                    <Text style={{ fontSize: 12, color: commonColors.textColor2 }}>{'$' + this.state.bookingFee}</Text>
                </View>
                <View style={{ height: 0.5, marginVertical: 15, backgroundColor: commonColors.background }} />
                <View style={{ flexDirection: 'row', marginBottom: 15 }}>
                    <Text style={{ fontSize: 15, color: 'black', flex: 1 }}>Total</Text>
                    <Text style={{ fontSize: 15, color: 'black' }}>{this.state.totalPrice}</Text>
                </View>
            </View>
        )
    }

    sendNotification() {
        if (this.state.orderAccepted == false) {
            orderService.cancelOrder(Cache.requestedOrder.id, (err, res) => {
                Actions.pop()
            })
        } else if (this.state.tripDropped == true) {
            //orderService.submitRate(Cache.requestedOrder.id, (err, res)=>{
            orderService.submitOrder(Cache.requestedOrder.id, this.state.businessId, (err, res) => {
                console.log(Cache.requestedOrder.id, this.state.businessId, err, res)
            })
            orderService.getOrder(Cache.requestedOrder.id, (err, res) => {
                Actions.RatingScreen({
                    data: res, updateData: () => {
                        Cache.order = []
                        Cache.requestedOrder = null;
                        if (this.props.updateData) {
                            this.props.updateData()
                        }
                        setTimeout(() => Actions.pop(), 10)
                    }
                })
            })
            //})
        }
    }

    renderCancelButton() {
        let height = (this.state.orderAccepted == false || this.state.tripDropped == true) ? 40 : 0
        return (
            <TouchableOpacity onPress={() => { this.sendNotification() }} style={{
                borderTopColor: commonColors.background, borderTopWidth: 0.5, backgroundColor: 'white',
                height: height, width: '100%', justifyContent: 'center', alignItems: 'center'
            }}>
                {this.state.orderAccepted == false && <Text style={{ fontSize: 13, color: commonColors.normalText }}>
                    CANCEL ORDER
                </Text>}
                {this.state.tripDropped == true && <Text style={{ fontSize: 13, color: commonColors.normalText }}>
                    SUBMITE
                </Text>}
            </TouchableOpacity>
        )
    }

    renderMapMarker(title, description, type) {
        if (this.state.business == null || title == null) return
        let width = description == null ? title.length * 10 + 20 : Math.max(title.length, description.length + 1) * 10 + 20
        let height = type == 'user' ? 40 : 55
        let isTop = Cache.selectedLocation.geoJson.coordinates[0] < this.state.business.geoLocation.geoJson.coordinates[0]
        let isLeft = Cache.selectedLocation.geoJson.coordinates[1] > this.state.business.geoLocation.geoJson.coordinates[1]
        if (type == 'user') {
            isTop = !isTop
            isLeft = !isLeft
        }
        return (
            <View style={{ height: 14, width: 14, borderWidth: 5, backgroundColor: 'white', borderColor: 'black', borderRadius: type == 'user' ? 7 : 0 }}>
                {this.state.driverPosition == null && <View style={{
                    position: 'absolute', backgroundColor: 'white', shadowColor: '#000', shadowOpacity: 0.5, shadowRadius: 3, shadowOffset: { height: 3 },
                    flex: 1, left: isLeft ? 20 : -width - 5, top: isTop ? 20 : -height - 5, width: width, height: height, padding: 10, justifyContent: 'center'
                }}>
                    {title != null && <Text style={{ fontSize: 15, color: 'black' }}>{title}</Text>}
                    {description != null && <Text style={{ fontSize: 13, color: commonColors.textColor3 }}>{'#' + description}</Text>}
                </View>}
            </View>
        )
    }

    renderHeader() {
        return (
            <View style={{ position: 'absolute', top: 0, padding: 20, width: '100%', backgroundColor: 'transparent' }}>
                <TouchableOpacity onPress={() => this.goBack()} style={styles.left}>
                    <IonIcons name="ios-arrow-round-back" size={28} color={'black'} />
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        let heading = this.state.driverPosition == null ? '0deg' : this.state.driverPosition.coords.heading + 'deg'
        
        return (
            <View style={styles.container}>
                <MapView
                    style={{ height: screenHeight - 200, width: '100%', position: 'absolute' }}
                    showsUserLocation={true}
                    initialRegion={this.state.region}
                >
                    {this.state.business != null && <MapView.Marker coordinate={{
                        latitude: this.state.business.geoLocation.geoJson.coordinates[1],
                        longitude: this.state.business.geoLocation.geoJson.coordinates[0]
                    }} >
                        {this.renderMapMarker(this.state.business.name, this.state.orderNumber, 'restaurant')}
                    </MapView.Marker>}
                    {this.state.business != null && <MapView.Marker coordinate={{
                        latitude: Cache.selectedLocation.geoJson.coordinates[1],
                        longitude: Cache.selectedLocation.geoJson.coordinates[0]
                    }} >
                        {this.renderMapMarker(Cache.currentUser.user.firstname + ' ' + Cache.currentUser.user.lastname, null, 'user')}
                    </MapView.Marker>}
                    {this.state.driverPosition != null && <MapView.Marker coordinate={{
                        latitude: this.state.driverPosition.coords.latitude,
                        longitude: this.state.driverPosition.coords.longitude
                    }} style={{ transform: [{ rotate: heading }], width: 40, height: 40 }}>
                        <Image source={commonStyles.carImg} style={{ width: 20, height: 50 }} />
                    </MapView.Marker>}
                </MapView>
                <TouchThroughWrapper style={{ flex: 1 }}>
                    <ScrollView
                        ref="scrollView"
                        scrollEventThrottle={1}
                        showsVerticalScrollIndicator={false}
                        onScroll={(event) => { this.handleScroll(event) }}
                        style={{ flex: 1, width: '100%' }}
                    >
                        <TouchThroughView style={{ height: 350, width: '100%' }} />
                        <View>
                            <LinearGradient
                                colors={['#ffffff00', commonColors.background]}
                                locations={[0, 50 / this.state.contentHeight]}
                                style={{ flex: 1, borderRadius: 5, position: 'absolute', height: this.state.contentHeight, width: '100%' }}>
                            </LinearGradient>
                            <View onLayout={(event) => {
                                let { width, height } = event.nativeEvent.layout;
                                this.setState({ contentHeight: height + screenHeight })
                            }} style={{ backgroundColor: 'transparent' }}>
                                {this.renderDriverItem()}
                                {this.renderFirstItem()}
                                {this.renderSecondItem()}
                            </View>
                        </View>
                    </ScrollView>
                </TouchThroughWrapper>
                {this.renderHeader()}

                <Modal isVisible={this.state.showReceipt}>
                    <View style={{ backgroundColor: 'white', borderRadius: 4 }}>
                        <View style={{
                            width: '100%', height: 54, justifyContent: 'center', marginBottom: 10,
                            alignItems: 'center', borderBottomColor: commonColors.textColor1, borderBottomWidth: 0.5
                        }}>
                            <Text style={{ color: 'black', fontSize: 18 }}>ORDER RECEIPT</Text>
                        </View>
                        {this.state.orders.map((item, index) => {
                            return this.renderOrders(index, true)
                        })}
                        {this.renderPriceTable()}
                        <TouchableOpacity style={{
                            width: '100%', height: 48, justifyContent: 'center',
                            alignItems: 'center', borderTopColor: commonColors.textColor1, borderTopWidth: 0.5
                        }}
                            onPress={() => this.setState({ showReceipt: false })}>
                            <Text style={{ fontSize: 15, color: commonColors.textColor2 }}>CLOSE</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>

                <Modal isVisible={this.state.visibleContactDialog} style={{justifyContent: "flex-end", margin: 0}}>
                    <View style={{padding:10, backgroundColor:commonColors.background}}>
                        <Text style={{color:commonColors.textColor2, textAlign:'center', marginTop:10}}>Contact Driver</Text>
                        <TouchableOpacity onPress={() => Linking.openURL('sms:' + this.state.driverData.driver.phone + '?body=Hi')}
                             style={{width:'100%', height:40, borderRadius:3, borderColor:commonColors.textColor3,borderWidth:1,
                            backgroundColor:'white', justifyContent:'center', alignItems:'center', marginTop:15}}>
                            <Text style={{color:commonColors.textColor1}}>MESSAGE</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => phoneCall({number: this.state.driverData.driver.phone,prompt: false,}).catch(console.error)}
                             style={{width:'100%', height:40, borderRadius:3, borderColor:commonColors.textColor3,borderWidth:1,
                            backgroundColor:'white', justifyContent:'center', alignItems:'center'}}>
                            <Text style={{color:commonColors.textColor1}}>CALL</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={()=>this.setState({visibleContactDialog:false})} 
                            style={{width:'100%', height:40, borderRadius:3, borderColor:commonColors.textColor3,borderWidth:1,
                            backgroundColor:'black', justifyContent:'center', alignItems:'center', marginTop:15}}>
                            <Text style={{color:'white'}}>CANCEL</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
            </View>
        )
    }

}

export default connect(props => ({
    commonStatus: props.common.status,
    notification: props.common.notification,

}),
    (dispatch) => ({
        commonActions: bindActionCreators(commonActions, dispatch),
    })
)(OrderStatus);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: commonColors.background
    },
    viewStyle: {
        backgroundColor: 'white',
        padding: 20,
        margin: 10,
        shadowColor: '#000',
        shadowOpacity: 0.2,
        shadowRadius: 3,
        shadowOffset: { width: 1, height: 1 }
    }
})
