'use strict';

import React, { PureComponent } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    Animated,
    PermissionsAndroid,
    RefreshControl,
    Platform,
    ListView,
    StatusBar,
    Alert,
} from 'react-native';

import { Actions, ActionConst, Scene, Router } from 'react-native-router-flux';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import IonIcons from 'react-native-vector-icons/Ionicons'
import * as _ from 'underscore'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as commonActions from '../../redux/actions';
import * as commonActionTypes from '../../redux/actionTypes';

import * as commonColors from '../../style/commonColors'
import * as commonStyles from '../../style/commonStyles'
import { screenHeight, screenWidth } from '../../style/commonStyles';

import googleService from '../../service/googleService'
import businessService from '../../service/businessService'
import Cache from '../../utils/Cache'
import * as config from '../../config'

import LocationHeader from '../../components/home/locationHeader'
import FilterHeader from '../../components/home/filterHeader'
import VerticalRestaurants from '../../components/home/verticalRestaurants'
import HorizontalRestaurants from '../../components/home/horizontalRestaurants'
import Advertisement from '../../components/home/advertisement'
import NoRatedOrders from '../../components/home/noRatedOrders'

class Home extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            isRefreshing: true,
            restaurantGroup: [],
            sort: 0,
            price: 0,
            dietary: 0,
            index: 0,
            routes: [
                { key: '1', title: 'Sort' },
                { key: '2', title: 'Price' },
                { key: '3', title: 'Dietary' },
            ],
            modalY: new Animated.Value(-screenHeight),
            modalVisible: false,
            address: Cache.selectedLocation.address,
            refreshNumber:0,
        }

        this.filterItems = [
            {
                key: 0,
                title: 'Sort',
                items: [
                    { code: 0, name: 'Recommended', image: commonStyles.recommendedIcon, size: 30 },
                    { code: 1, name: 'Most popular', image: commonStyles.popularIcon, size: 24 },
                    { code: 2, name: 'Delivery time', image: commonStyles.deliveryIcon, size: 28 },
                ],
            },
            {
                key: 1,
                title: 'Price',
                items: [
                    { code: 0, name: '$', image: commonStyles.priceIcon1, image2: commonStyles.priceOutlineIcon1 },
                    { code: 1, name: '$$', image: commonStyles.priceIcon2, image2: commonStyles.priceOutlineIcon2 },
                    { code: 2, name: '$$$', image: commonStyles.priceIcon3, image2: commonStyles.priceOutlineIcon3 },
                    { code: 3, name: '$$$$', image: commonStyles.priceIcon4, image2: commonStyles.priceOutlineIcon4 }
                ]
            },
            {
                key: 2,
                title: 'Dietary',
                items: [
                    { code: 0, name: 'Vegetable', image: commonStyles.vegetableIcon, size: 18 },
                    { code: 1, name: 'Vegan', image: commonStyles.veganIcon, size: 20 },
                    { code: 2, name: 'Gluten-free', image: commonStyles.glutenIcon, size: 24 },
                ]
            }
        ]

        this.setStatusBarColor()

        //console.log(Cache.currentUser)
    }

    changeStatus(key, value) {
        this.setState((prev) => {
            if (key == 'sort') {
                prev[key] = value
            }
            if (key == 'price' || key == 'dietary') {
                prev[key] ^= (1 << value)
            }
        })
    }

    RenderTick(key, index) {
        if (key == 'sort') {
            return (
                <View style={{ flex: 5, alignItems: 'flex-end', justifyContent: 'center' }}>
                    {this.state.sort == index && <IonIcons name="ios-checkmark" size={40} color={commonColors.theme} style={{ marginRight: 10 }} />}
                </View>
            )
        }
        if (key == 'dietary') {
            return (
                <View style={{ flex: 5, alignItems: 'flex-end', justifyContent: 'center' }}>
                    {(this.state.dietary & (1 << index)) > 0 && <IonIcons name="ios-checkmark" size={40} color={commonColors.theme} style={{ marginRight: 10 }} />}
                </View>
            )
        }
    }

    renderFilterItem(page, index) {
        let key = ['sort', 'price', 'dietary']
        let items = this.filterItems[page].items
        return (
            <TouchableOpacity key={index} onPress={() => this.changeStatus(key[page], index)}
                style={{ flexDirection: 'row', flex: 1, justifyContent: 'center', height: 60 }}>
                <View style={{ flex: 5, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                    <View style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={items[index].image} style={{ width: items[index].size, height: items[index].size }} />
                    </View>
                    <Text style={{ marginLeft: 10 }}>{items[index].name}</Text>
                </View>
                {this.RenderTick(key[page], index)}
            </TouchableOpacity>
        )
    }

    renderFilterPage(page) {

        let items = this.filterItems[page].items
        return (
            <View style={{}}>
                {page != 1 && items.map((item, index) => {
                    return this.renderFilterItem(page, index)
                })}
                {page == 1 && <View>
                    <Text style={{ color: commonColors.textColor2, fontSize: 15, marginTop: 30 }}>Price Range</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        {items.map((item, index) => {
                            return (
                                <TouchableOpacity key={index} onPress={() => this.changeStatus('price', index)} style={{ marginHorizontal: 5, marginTop: 20 }}>
                                    {(this.state.price & (1 << index)) > 0 && <Image source={items[index].image}
                                        style={{ height: 60, width: 60 }} />}
                                    {(this.state.price & (1 << index)) == 0 && <Image source={items[index].image2}
                                        style={{ height: 60, width: 60 }} />}
                                </TouchableOpacity>
                            )
                        })}
                    </View>
                </View>}
            </View>
        )
    }
    _handleIndexChange = index => this.setState({ index });
    _renderHeader = props => <TabBar {...props} renderLabel={(scene) => this.renderLabel(scene)} indicatorStyle={{ backgroundColor: '#ffffff' }} labelStyle={{ color: '#000000', textAlign: 'left' }} style={styles.tabBar} />;
    _renderScene = ({ route }) => {
        return this.renderFilterPage(parseInt(route.key, 10) - 1)
    }

    renderLabel(scene) {
        let { sort, price, dietary } = this.state
        let bShowIcon = (scene.index == 0 && sort != 0) || (scene.index == 1 && price != 0) || (scene.index == 2 && dietary != 0)
        let focused = scene.focused
        return (
            <View style={{ flexDirection: 'row', marginLeft: -50, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: focused ? commonColors.normalText : commonColors.textColor1, fontSize: 18 }}>{scene.route.title}</Text>
                {bShowIcon && <View style={{width:4, height:4, borderRadius:2, backgroundColor:commonColors.theme, margin:3, marginLeft:8}}/>}
            </View>
        )
    }

    openModal() {
        this.setState({ modalVisible: true });
        Animated.timing(this.state.modalY, {
            duration: 300,
            toValue: 0
        }).start();
    }

    resetFilter() {
        this.setState({
            sort: 0,
            price: 0,
            dietary: 0,
        })
        this.filtering()
    }

    filtering() {
        var price = []
        var dietary = []
        for (var i = 0; i < 4; i++) {
            if ((this.state.price & (1 << i)) > 0) {
                price.push(i + 1)
            }
            if ((this.state.dietary & (1 << i)) > 0) {
                dietary.push(i)
            }
        }
        this.getBesiness({
            lng: Cache.selectedLocation.geoJson.coordinates[0],
            lat: Cache.selectedLocation.geoJson.coordinates[1],
            placeId: Cache.selectedLocation.placeId,
            sort: this.state.sort + 1,
            price: price,
            dietary: dietary,
        })
    }

    closeModal() {
        this.setState({ modalVisible: false });
        Animated.timing(this.state.modalY, {
            duration: 300,
            toValue: -400
        }).start();

        this.filtering()
    }

    renderLayout() {
        if (this.state.modalVisible) {
            return (<TouchableOpacity onPress={() => this.closeModal()} style={{ width: screenWidth, height: screenHeight, position: 'absolute', top: 0, left: 0, backgroundColor: 'rgba(0, 0, 0, 0.5)' }}></TouchableOpacity>)
        }
    }

    componentWillMount() {
        this.filtering()
    }

    getBesiness(data) {
        businessService.getBesinesses(data, (err, res) => {
            if (err == null) {
                this.setState({ restaurantGroup: JSON.parse(JSON.stringify(res)) })
                setTimeout(() => {
                    this.forceUpdate()
                }, 100)
                // console.log(res)
            } else {
                // console.log(err)
            }
        })
    }
    
    setStatusBarColor(){
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    componentWillReceiveProps(nextProps) {
        let { commonStatus, notification, name, favoriteId } = nextProps
        if (commonStatus == commonActionTypes.RECEIVED_NOTIFICATION) {
            // console.log(notification.payload, JSON.parse(notification.payload.body))
        }
        
        this.setStatusBarColor()
        this.filtering()
    }

    componentDidMount() {
        this.loadAllData();
        if (Platform.OS === 'android') {
            PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                .then(granted => {
                    if (granted && this.mounted) this.watchLocation();
                });
        } else {
            this.watchLocation();
        }
    }

    updateMyPosition(currentPosition) {
        let {longitude, latitude} = currentPosition
        googleService.getAddress(longitude, latitude, (err, res)=>{
            if ( err == null ){
                Cache.currentLocation = {
                    placeId: res.place_id,
                    address: res.formatted_address,
                    geoJson: {
                        type: "Point",
                        coordinates: [
                            longitude,
                            latitude
                        ]
                    }
                }
            }
        })
    }

    watchLocation() {
        this.watchID = navigator.geolocation.watchPosition((position) => {
            const myLastPosition = this.state.myPosition;
            const myPosition = position.coords;

            //if (!isEqual(myPosition, myLastPosition)) {
                this.updateMyPosition(myPosition)
            //}
        }, null,
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 });
    }
    componentWillUnmount() {
        if (this.watchID) navigator.geolocation.clearWatch(this.watchID);
    }

    loadAllData() {
        setTimeout(() => {
            this.setState({ isRefreshing: false });
        }, 2000)
    }

    onRefresh() {
        //console.log(this.refreshNumber)
        this.setState({refreshNumber:this.state.refreshNumber+1})
        this.filtering()
    }

    getTitle() {
        let { sort, price, dietary } = this.state
        let title = this.filterItems[0].items[sort].name
        for (var i = 0; i < this.filterItems[1].items.length; i++) {
            if ((price & (1 << i)) > 0) title = title + ',' + this.filterItems[1].items[i].name
        }
        for (var i = 0; i < this.filterItems[2].items.length; i++) {
            if ((dietary & (1 << i)) > 0) title = title + ',' + this.filterItems[2].items[i].name
        }
        return title;
    }

    selectLocation() {
        Actions.SelectLocation({
            update: () => {
                this.filtering()
                this.setState({ address: Cache.selectedLocation.address })
                //console.log(this.state.address)
            }
        })
    }

    onCheck() {
        Actions.ViewCart({
            updateData: () => {
                this.forceUpdate()
            }
        });
    }

    renderCheckout() {
        if( Cache.order == null || Cache.order == undefined ) return null
        var count = 0
        var price = 0
        _.map(Cache.order, (item) => {
            count = count + item.count
            price = price + item.count * item.price
        })
        if (Cache.order != undefined && Cache.order.length > 0) {
            return (
                <TouchableOpacity onPress={this.onCheck.bind(this)} style={{
                    position: 'absolute', top: screenHeight - 113, width: screenWidth, height: 48, justifyContent: 'center',
                    alignItems: 'center', backgroundColor: commonColors.theme, flexDirection: 'row'
                }}>
                    <View style={{ flex: 2, alignItems: 'flex-start', justifyContent: 'center', paddingLeft: 15 }}><Text style={{ color: '#ffffff', fontSize: 14 }}>${price}</Text></View>
                    <View style={{ flex: 4, alignItems: 'center', justifyContent: 'center' }}><Text style={{ color: '#ffffff', fontSize: 16, fontWeight: '700' }}>CHECK OUT</Text></View>
                    <View style={{ flex: 2, alignItems: 'flex-end', justifyContent: 'center', paddingRight: 15 }}><Text style={{ color: '#ffffff', fontSize: 14, paddingHorizontal: 5, borderRadius: 3, borderWidth: 1, borderColor: 'white' }}>{count}</Text></View>
                </TouchableOpacity>
            )
        }
        return null
    }

    renderLogo(){
        return(
            <View style={{height:44, width:'100%', justifyContent:'center', alignItems:'center', backgroundColor:'white'}}>
                <Image source={commonStyles.gogo_logo} style={{width:60, height:40}}/>
            </View>
        )
    }

    render() {
        let { sort, price, dietary } = this.state
        let changed = (sort + price + dietary) > 0 ? true : false
        let title = this.getTitle()
        return (
            <View style={styles.container}>
                {this.renderLogo()}
                <LocationHeader address={this.state.address} onPress={() => this.selectLocation()} />
                <FilterHeader onPress={() => this.openModal()} changed={changed} title={title} />
                <ScrollView style={styles.content}
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={() => this.onRefresh()}
                            tintColor={'grey'}
                        />
                    }>
                    <NoRatedOrders index={this.state.refreshNumber}/>
                    <Advertisement/>
                    {
                        this.state.restaurantGroup.map((item, index) => {
                            if (item.isSlide) {
                                if (item.items.length > 0) {
                                    return (<HorizontalRestaurants key={index} dataList={item.items} title={config.GROUP_NAME[item.queryCode]} />)
                                } else { return null }
                            } else {
                                if (item.items.length > 0) {
                                    return (<VerticalRestaurants key={index} dataList={item.items} title={config.GROUP_NAME[item.queryCode]} />)
                                } else { return null }
                            }
                        })
                    }
                </ScrollView>
                {this.renderCheckout()}
                {this.renderLayout()}
                <Animated.View style={[styles.modal, { transform: [{ translateY: this.state.modalY }] }]}>
                    <View style={{
                        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
                        marginHorizontal: 15
                    }}>
                        <TouchableOpacity onPress={() => this.closeModal()}>
                            <IonIcons name="ios-close" size={40} color={commonColors.normalText} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.resetFilter()}>
                            <Text style={{ fontSize: 10, color: 'black', fontWeight: '400' }}>RESET</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ padding: 10, paddingTop: 0 }}>
                        <TabViewAnimated
                            style={styles.tabview}
                            navigationState={this.state}
                            renderScene={this._renderScene}
                            renderHeader={this._renderHeader}
                            onIndexChange={this._handleIndexChange}
                        />
                    </View>
                    <TouchableOpacity onPress={this.closeModal.bind(this)} style={{ borderTopWidth: 0.5, borderTopColor: commonColors.background, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: commonColors.normalText }}>DONE</Text>
                    </TouchableOpacity>
                </Animated.View>

            </View>
        )
    }

}

export default connect(props => ({
    commonStatus: props.common.status,
    notification: props.common.notification,

}),
    (dispatch) => ({
        commonActions: bindActionCreators(commonActions, dispatch),
    })
)(Home);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: commonColors.background,
        marginTop: 20,
    },
    content: {
        flex: 1,
    },
    tabview: {

    },
    tabBar: {
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
    },
    modal: {
        width: screenWidth,
        position: 'absolute',
        top: 0,
        left: 0,
        backgroundColor: 'white',
    },
})
