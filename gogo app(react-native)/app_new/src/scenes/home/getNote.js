
'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Alert,
    Platform,
    Modal,
    Animated,
    StatusBar,
} from 'react-native';

import * as _ from 'underscore'
import { Actions } from 'react-native-router-flux';


import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';
import { screenWidth, screenHeight } from "../../style/commonStyles";
import NavTitleBar from '../../components/navTitle'

import * as config from '../../config'
import Cache from '../../utils/Cache'


export default class GetNote extends PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            note: this.props.note,
        }

        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    onBack() {
        Actions.pop()
    }

    done(){
        if (this.props.updateData) {
            this.props.updateData(this.state.note)
        }
        Actions.pop()
    }


    render() {

        return (
            <View style={styles.container}>
                <NavTitleBar
                    buttons={commonStyles.NavCloseButton}
                    onBack={this.onBack.bind(this)}
                    title={''}
                    rightText={'DONE'}
                    rightCallback={this.done.bind(this)}
                />
                <TextInput
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholder="start typing"
                    placeholderTextColor={commonColors.placeholderText}
                    textAlign="left"
                    style={{width:'100%', flex:1, padding:10, fontSize:16}}
                    multiline={true}
                    underlineColorAndroid="transparent"
                    value={this.state.note}
                    onChangeText={(text) => {
                        this.setState({ note: text })
                    }} />
            </View>

        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1
    },

})
