'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Alert,
    Platform,
    Modal,
    Animated,
    StatusBar,
} from 'react-native';

import * as _ from 'underscore'
import { Actions } from 'react-native-router-flux';
import IonIcons from 'react-native-vector-icons/Ionicons'
import LinearGradient from 'react-native-linear-gradient'
import MapView from 'react-native-maps'

import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';
import { screenWidth, screenHeight } from "../../style/commonStyles";

import RadioView from '../../components/home/radioView';

import * as config from '../../config'
import Cache from '../../utils/Cache'
import businessService from '../../service/businessService'

const IMAGE_HEIGHT = screenHeight
const gradientColors = ['#000000dd', '#00000060', '#00000000', '#00000000', '#000000ff', '#000000ff'];
const gradientPoints = [0, 0.08, 0.2, 0.5, 0.55, 1.0];

export default class ViewCart extends PureComponent {

    constructor(props) {
        super(props);

        let { address } = Cache.selectedLocation;
        let addressArray = address.split(', ')
        let detail = ''
        for (var i = 1; i < addressArray.length; i++) {
            if (i > 1) detail = detail + ', '
            detail = detail + addressArray[i]
        }

        this.state = {
            socialmodalVisible: false,
            businessImage: null,
            scrollY: 0,
            orders: Cache.order,
            totalPrice: 0,
            subTotal: 0,
            tax: 0,
            bookingFee: 0,
            isAddedPayment: false,
            address: addressArray[0],
            detailAddress: detail,
            businessName: '',
            businessDescription: '',
        }
        this.renderLayoutSocial = this.renderLayoutSocial.bind(this);
        this.state.modalY = new Animated.Value(-100);
        this.topModalVisible = false;
        this.setStatusBar()

    }

    componentDidMount() {
        this.calcPrice()
        businessService.getBesiness(Cache.currentBusinessId, (err, res) => {
            //console.log('order=>', res)
            this.setState({
                businessName: res.name,
                businessDescription: config.PREPARATION[res.preparationTime],
                businessImage: res.logo == null ? null : { uri: config.SERVICE_FILE_URL + res.logo }
            })
        })
    }

    calcPrice() {
        var total = 0
        _.map(Cache.order, (item) => {
            total = total + item.price * item.count
        })
        this.setState({
            tax: Cache.tax,
            bookingFee: Cache.bookingFee,
            subTotal: total,
            totalPrice: total + Cache.bookingFee,
        })

    }

    checkOut() {
        if (this.state.isAddedPayment == true) {
            Cache.upcomingOrder = Cache.requestedOrder
            Actions.OrderStatus({
                updateData: () => {
                    if (Cache.order.length==0){
                        if ( this.props.updateData ) this.props.updateData()
                        setTimeout(()=>Actions.pop(),10)
                    }
                    this.forceUpdate()
                }
            })
            return;
        }
        Actions.AddPayment({
            addedPayment: (status) => {
                this.setStatusBar()
                if (status == false) return;
                businessService.sendOrder((err, res) => {
                    if (err == null) {
                        //console.log("send orders successfully!", res)
                        Cache.requestedOrder = res
                        this.setState({ isAddedPayment: true })
                    } else {
                        //console.log(err)
                    }
                })
            }
        });
    }

    goBack() {
        if (this.props.updateData)
            this.props.updateData()
        Actions.pop();
    }

    renderHeader(bShow) {
        return (
            <View style={{ position: 'absolute', top: 0, width: '100%' }}>
                <TouchableOpacity onPress={() => this.goBack()} style={styles.left}>
                    <IonIcons name="md-close" size={28} color={bShow ? 'black' : 'white'} />
                </TouchableOpacity>
            </View>
        )
    }
    renderBackground() {
        //if (this.state.businessImage == null) return null
        let top = -this.state.scrollY / 3
        if (top > 0) top = 0
        return (
            <View style={{ position: 'absolute', top: top, width: screenWidth, height: IMAGE_HEIGHT }}>
                <TouchableOpacity onPress={() => { }}>
                    <Image style={styles.image} source={this.state.businessImage} />
                </TouchableOpacity>
                <LinearGradient
                    colors={gradientColors}
                    locations={gradientPoints}
                    style={{ flex: 1, position: 'absolute', height: IMAGE_HEIGHT, width: '100%' }}>
                </LinearGradient>
            </View>
        )
    }

    setStatusBar() {
        let type = this.topModalVisible ? 'dark-content' : 'light-content'
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle(type, false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    openModal() {
        this.topModalVisible = true;
        Animated.timing(this.state.modalY, {
            duration: 30,
            toValue: 0
        }).start();
        this.setStatusBar()
    }

    closeModal() {
        this.topModalVisible = false;
        Animated.timing(this.state.modalY, {
            duration: 20,
            toValue: -100
        }).start();
        this.setStatusBar()
    }

    onRefresh() {

    }

    onBack() {
        Actions.pop()
    }
    setSocialModalVisible(visible) {
        this.setState({ socialmodalVisible: visible });

    }
    renderLayoutSocial() {
        if (this.state.socialmodalVisible) {
            return (<TouchableOpacity onPress={() => { this.setSocialModalVisible(!this.state.socialmodalVisible) }} style={{ width: screenWidth, height: screenHeight, position: 'absolute', top: 0, left: 0, backgroundColor: 'rgba(0, 0, 0, 0.5)' }}></TouchableOpacity>)
        }
    }

    handleScroll(event) {
        this.setState({ scrollY: event.nativeEvent.contentOffset.y })
        if (event.nativeEvent.contentOffset.y > 190) {
            if (!this.topModalVisible) {
                this.openModal();
            }
        }
        else {
            if (this.topModalVisible) {
                this.closeModal();
            }
        }
    }

    renderTitle() {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 30, color: commonColors.normalText, marginTop: 30 }}>
                    {this.state.businessName}
                </Text>
                <Text style={{ fontSize: 10, color: commonColors.textColor1, marginTop: 20 }}>{this.state.businessDescription}</Text>
                <View style={{ height: 1, backgroundColor: commonColors.normalText, width: 40, marginTop: 30 }} />
            </View>
        )
    }
    changeTarget() {
        Actions.SelectLocation({
            update: () => {
                let { address } = Cache.selectedLocation;
                let addressArray = address.split(', ')
                let detail = ''
                for (var i = 1; i < addressArray.length; i++) {
                    if (i > 1) detail = detail + ', '
                    detail = detail + addressArray[i]
                }
                this.setState({ address: addressArray[0], detailAddress: detail })
            }
        })
    }

    renderPosition() {
        return (
            <View>
                <TouchableOpacity activeOpacity={0.7} onPress={() => { this.changeTarget() }}
                    style={{ flex: 1, padding: 15, flexDirection: 'row', marginTop: 10, borderBottomColor: commonColors.background, borderBottomWidth: 0.5 }}>
                    <MapView
                        pointerEvents='none'
                        style={{ height: 100, width: 100}}
                        initialRegion={{
                            latitude:Cache.selectedLocation.geoJson.coordinates[1],
                            longitude:Cache.selectedLocation.geoJson.coordinates[0],
                            latitudeDelta:0.1,
                            longitudeDelta:0.05,
                        }}
                    >
                        <MapView.Marker coordinate={{
                            latitude:Cache.selectedLocation.geoJson.coordinates[1], 
                            longitude:Cache.selectedLocation.geoJson.coordinates[0]}}/>
                    </MapView>
                    <View style={{ padding: 10, flex: 1 }}>
                        <Text numberOfLines={1} style={{ fontSize: 15, color: commonColors.normalText }}>{this.state.address}</Text>
                        <Text numberOfLines={1} style={{ fontSize: 13, color: commonColors.textColor1, marginTop: 5 }}>{this.state.detailAddress}</Text>
                        {/*<Text numberOfLines={1} style={{ fontSize: 13, color: commonColors.textColor1, marginTop: 5 }}>Deliver to door, Khfcfhhgg,Hsdfsdfsd</Text>
                        <Text numberOfLines={1} style={{ fontSize: 13, color: commonColors.textColor1, marginTop: 5 }}>gasdf</Text>*/}
                    </View>
                </TouchableOpacity>
                <View style={{ flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 15 }}>
                    <IonIcons name="ios-alarm-outline" size={15} color={commonColors.normalText} />
                    <Text style={{ fontSize: 13, color: commonColors.normalText, marginLeft: 15 }}>As soon as possible</Text>
                </View>
            </View>
        )
    }

    editOrdering(id, count, data) {
        Actions.Food({
            orderNumber: id, count: count, foodData: data, updateData: () => {
                this.calcPrice()
                this.forceUpdate()
            }
        })
    }

    renderOrders(index) {
        let bFirst = (index == 0)
        let { count, data, price } = this.state.orders[index]
        let options = data.foodType.foodOptions
        return (
            <TouchableOpacity key={index} onPress={() => this.editOrdering(index, count, data)} style={{
                flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 10, alignItems: 'flex-start',
                borderTopColor: commonColors.background, borderTopWidth: bFirst ? 0 : 0.5
            }}>
                <View style={{ borderWidth: 0.5, borderColor: commonColors.background, paddingHorizontal: 4 }}>
                    <Text style={{ fontSize: 12, color: commonColors.theme }}>{count}</Text>
                </View>
                <View style={{ marginLeft: 15, flex: 1 }}>
                    <Text style={{ fontSize: 15, color: commonColors.normalText }}>{data.name}</Text>
                    <View style={{ marginTop: 0 }}>
                        {
                            options.map((item, index) => {
                                if (item.enabled == true) {
                                    return (
                                        <View key={index}>
                                            {item.options.map((node, index2) => {
                                                if (node.default == true) {
                                                    return (
                                                        <Text key={index2} style={{ fontSize: 12, color: commonColors.textColor1, marginTop: 5 }}>
                                                            {node.name + (node.price > 0 ? ' ($' + node.price + ')' : '')}
                                                        </Text>)
                                                }

                                            })}
                                        </View>
                                    )
                                }
                            })
                        }
                    </View>
                </View>
                <Text style={{ marginLeft: 15, fontSize: 15, color: commonColors.normalText }}>{'$' + (count * price)}</Text>
            </TouchableOpacity>
        )
    }

    renderPriceTable() {
        return (
            <View style={{ borderTopColor: commonColors.background, borderTopWidth: 0.5, paddingHorizontal: 20, paddingVertical: 15 }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 12, color: commonColors.textColor2, flex: 1 }}>Subtotal</Text>
                    <Text style={{ fontSize: 12, color: commonColors.textColor2 }}>{this.state.subTotal}</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 15 }}>
                    <Text style={{ fontSize: 12, color: commonColors.textColor2, flex: 1 }}>Delivery Fee</Text>
                    <Text style={{ fontSize: 12, color: commonColors.textColor2 }}>{this.state.bookingFee}</Text>
                </View>
                <View style={{ height: 0.5, marginVertical: 15, backgroundColor: commonColors.background }} />
                <View style={{ flexDirection: 'row', marginBottom: 15 }}>
                    <Text style={{ fontSize: 15, color: 'black', flex: 1 }}>Total Price</Text>
                    <Text style={{ fontSize: 15, color: 'black' }}>{this.state.totalPrice}</Text>
                </View>
            </View>
        )
    }

    render() {
        let deltaMargin = this.state.scrollY / 200 * 8
        if (deltaMargin < 0) deltaMargin = 0
        if (deltaMargin > 8) deltaMargin = 8
        return (
            <View style={{ padding: 0 }}>
                {this.renderBackground()}
                <ScrollView
                    ref="scroll"
                    style={styles.scrollView}
                    showsVerticalScrollIndicator={false}
                    scrollEventThrottle={1}
                    onScroll={this.handleScroll.bind(this)}>

                    <View style={{ marginHorizontal: 8 - deltaMargin, backgroundColor: 'white', marginTop: 280, borderTopColor: commonColors.background, borderTopWidth: 0.5, overflow:'hidden', justifyContent:'center', alignItems:'center' }}>
                        <View style={{width:screenWidth-16}}>
                        {this.renderTitle()}
                        {this.renderPosition()}
                        {this.state.orders.map((item, index) => {
                            return this.renderOrders(index)
                        })}
                        <View style={{
                            paddingHorizontal: 20, paddingVertical: 15, width: '100%',
                            borderTopColor: commonColors.background, borderTopWidth: 0.5
                        }}>
                            <TextInput placeholder={'Add a Note (extra napkins, extra sauce...)'} placeholderTextColor={'#b1b1b9'} style={{ fontSize: 13, height: 20 }} />
                        </View>
                        {this.renderPriceTable()}
                        {/* add payment */}
                        <TouchableOpacity onPress={() => Actions.Payment()} style={{
                            flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 15,
                            alignItems: 'center', borderTopColor: commonColors.background, borderTopWidth: 0.5
                        }}>
                            <View style={{
                                borderWidth: 0.5, borderColor: commonColors.background, borderRadius: 2,
                                paddingHorizontal: 8, justifyContent: 'center', alignItems: 'center', paddingVertical: 3
                            }}>
                                <IonIcons name="md-add" size={14} color={commonColors.textColor2} />
                            </View>
                            <Text style={{ fontSize: 12, color: commonColors.normalText, marginLeft: 10 }}>ADD PAYMENT</Text>
                        </TouchableOpacity>
                        {/* add promo code */}
                        <TouchableOpacity onPress={() => Actions.Promotion()} style={{
                            flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 15,
                            alignItems: 'center', borderTopColor: commonColors.background, borderTopWidth: 0.5
                        }}>
                            <View style={{ borderWidth: 0.5, borderColor: commonColors.background, borderRadius: 2, paddingHorizontal: 8, paddingVertical: 3 }}>
                                <IonIcons name="md-pricetag" size={14} color={commonColors.textColor2} />
                            </View>
                            <Text style={{ fontSize: 12, color: commonColors.textColor1, marginLeft: 20 }}>Add Promo Code</Text>
                        </TouchableOpacity>
                        <View style={{ height: 50 }} />
                        </View>
                    </View>
                </ScrollView>

                {this.renderHeader(false)}
                <Animated.View style={[styles.modal, {
                    transform: [{ translateY: this.state.modalY }],
                    borderBottomColor: commonColors.normalText, borderBottomWidth: 0.5
                }]}>
                    <View style={{ flex: 1, height: 50, paddingHorizontal: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontWeight: 'bold', color: commonColors.normalText, fontSize: 15 }}>Your Cart</Text>
                    </View>
                    {this.renderHeader(true)}
                </Animated.View>
                {/*  BEGIN CHECKOUT  */}
                <TouchableOpacity activeOpacity={1} onPress={() => { this.checkOut() }} style={{
                    height: 48, width: '100%', position: 'absolute', bottom: 0,
                    backgroundColor: commonColors.theme, justifyContent: 'center', alignItems: 'center'
                }}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', color: 'white' }}>{this.state.isAddedPayment ? "BEGIN CHECKOUT" : "ADD PAYMENT"}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    modal: {
        paddingTop: 15,
        width: screenWidth,
        position: 'absolute',
        top: 0,
        left: 0,
        backgroundColor: '#ffffff',
        flexDirection: 'column',
    },
    container: {
        width: screenWidth,
        flexDirection: 'row',
    },
    scrollView: {
        flexDirection: 'column',
        marginTop: -21,
    },
    part1: {
        flexDirection: 'column',
        height: screenHeight * 2 / 5,
    },
    part2: {
        alignItems: 'center',
        paddingTop: 20,

    },
    buttonWrap: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        backgroundColor: 'transparent',
    },
    button: {
        justifyContent: 'center',
    },
    toppart: {
        padding: 20,
        flexDirection: 'column',
        alignItems: 'center',
    },
    bottompart: {
        marginTop: 30,
        padding: 10,
        flex: 1,
        alignItems: 'center',
    },
    component1: {
        flexDirection: 'row',
        justifyContent: 'center',
        width: 150,
        height: 50,
        borderWidth: 1,
        borderColor: '#cacbcf',
        padding: 10,
        borderRadius: 20

    },
    textTitle: {
        fontSize: 20,
        flex: 1,
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'center'
    },
    textTitle1: {
        fontSize: 20,
        flex: 1,
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        marginLeft: screenWidth / 2 - 85,
        color: '#ffffff',
    },
    image: {
        height: 400,
    },
    right: {
        position: 'absolute',
        top: 16,
        right: 15,
        paddingVertical: 10,
        paddingHorizontal: 5,
        backgroundColor: 'transparent'
    },
    left: {
        position: 'absolute',
        top: 13,
        left: 15,
        paddingVertical: 10,
        paddingHorizontal: 5,
        backgroundColor: 'transparent'
    },
})
