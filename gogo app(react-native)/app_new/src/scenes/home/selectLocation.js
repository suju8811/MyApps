'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Alert,
    Platform,
    Modal,
    Animated,
    StatusBar,
} from 'react-native';

import * as _ from 'underscore'
import { Actions } from 'react-native-router-flux';
import IonIcons from 'react-native-vector-icons/Ionicons'
var { GooglePlacesAutocomplete } = require('react-native-google-places-autocomplete');

import * as commonColors from '../../style/commonColors';
import * as commonStyles from '../../style/commonStyles';
import { screenWidth, screenHeight } from "../../style/commonStyles";

import * as config from '../../config'
import Cache from '../../utils/Cache'
import NavTitleBar from '../../components/navTitle'
import UtilService from '../../utils/util'


const gradientColors = ['#00000070', '#00000040', '#00000000', '#00000000', '#00000010'];
const gradientPoints = [0, 0.15, 0.35, 0.9, 1.0];

export default class SelectLocation extends PureComponent {

    constructor(props) {
        super(props);

        if ( Cache.selectedLocationIndex == undefined ) Cache.selectedLocationIndex=1;
        this.state = {
            selectedLocation: Cache.selectedLocationIndex,
            selectedMode: 0,
            filtering: false,
            filledInput: false,
        }

        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content', false);
        } else if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(commonColors.theme, false);
        }
    }

    onBack() {
        Actions.pop()
    }

    renderRow(row) {
        let { description } = row
        let items = description.split(", ")
        let desc = ''
        for (var i = 1; i < items.length; i++) {
            if (i > 1) desc = desc + ', '
            desc = desc + items[i]
        }
        return (
            <View style={{
                marginTop: -8, flexDirection: 'row', height: 36, width: screenWidth,
                marginLeft: -12, alignItems: 'center'
            }}>
                <IonIcons name="ios-pin-outline" size={22} color={commonColors.normalText} style={{ marginHorizontal: 15 }} />
                <View style={{ flex: 1, paddingRight: 25 }}>
                    <Text numberOfLines={1} style={{ color: commonColors.normalText, fontSize: 15 }}>{items[0]}</Text>
                    {desc.length > 0 && <Text numberOfLines={1} style={{ color: commonColors.textColor1, fontSize: 13 }}>{desc}</Text>}
                </View>
            </View>
        )
    }

    renderAutoComplete() {
        return (
            <View style={{ marginTop: 8, flexDirection: 'row', height: 50, backgroundColor: 'white' }}>
                <View style={{ flex: 1 }}>
                    <View style={{ position: 'absolute', width: '100%', backgroundColor: 'white' }}>
                        <GooglePlacesAutocomplete
                            placeholder='Enter a new address'
                            minLength={1} // minimum length of text to search
                            autoFocus={false}
                            textInputProps={{
                                ref: "input",
                                autoCorrect: false,
                                autoCapitalize: "none",
                                value: this.state.address,
                                onChangeText: (text) => {
                                    this.setState({ filtering: (text.length > 0), filledInput: false })
                                },
                                onFocus: (text) => {
                                    this.setState({ filtering: this.state.filledInput })
                                }
                            }}
                            returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                            listViewDisplayed='auto'    // true/false/undefined
                            fetchDetails={true}
                            enablePoweredByContainer={false}
                            renderRow={(row) => this.renderRow(row)}
                            renderDescription={(row) => row.description.split(',')[0]} // custom description render
                            onPress={(data, details) => { // 'details' is provided when fetchDetails = true
                                Cache.customLocation = {
                                    placeId: details.place_id,
                                    title: details.formatted_address.split(',')[0],
                                    address: details.formatted_address,
                                    geoJson: {
                                        type: "Point",
                                        coordinates: [
                                            details.geometry.location.lng,
                                            details.geometry.location.lat
                                        ]
                                    }
                                }
                                this.setState({ filledInput: true })
                                this.setState({ filtering: false })
                            }}

                            getDefaultValue={() => ''}

                            query={{
                                // available options: https://developers.google.com/places/web-service/autocomplete
                                key: 'AIzaSyBTyrdlrF8B60Va6y9Wx-aJasqaryaVzV4',
                                language: 'en', // language of the results
                                types: 'geocode' // default: 'geocode'
                            }}

                            styles={{
                                textInputContainer: {
                                    borderBottomWidth: 8,
                                    borderBottomColor: commonColors.background,
                                    width: '100%',
                                    height: 58,
                                    backgroundColor: 'white',
                                },
                                textInput: {
                                    height: 32,
                                    color: commonColors.normalText,
                                    fontSize: 15,
                                    marginTop: 9,
                                    marginLeft: -10
                                },
                                predefinedPlacesDescription: {
                                    color: commonColors.textColor1
                                },
                            }}
                            nearbyPlacesAPI='GoogleReverseGeocoding' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                            GoogleReverseGeocodingQuery={{
                                // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                            }}
                            GooglePlacesSearchQuery={{
                                // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                rankby: 'distance',
                                types: 'food'
                            }}
                            renderLeftButton={() => <IonIcons name="ios-pin-outline" size={22} color={commonColors.normalText} style={{ margin: 15 }} />}
                        />
                    </View>
                </View>
            </View>
        )
    }

    renderLocations() {
        let { homeLocation, workLocation } = Cache.currentUser.user
        let { customLocation, currentLocation } = Cache
        return (
            <View style={{ marginTop: 8, backgroundColor: 'white' }}>
                {currentLocation != undefined &&<TouchableOpacity onPress={() => this.setState({ selectedLocation: 0 })}
                    style={{ flexDirection: 'row', height: 40, alignItems: 'center', marginVertical: 8 }}>
                    <IonIcons name="ios-send-outline" size={26} color={commonColors.normalText} style={{ margin: 15 }} />
                    <View style={{ flex: 1, paddingRight: 15 }}>
                        <Text style={{ fontSize: 15, color: commonColors.normalText }}>Current Location</Text>
                        <Text numberOfLines={2} style={{ fontSize: 13, color: commonColors.textColor1 }}>{currentLocation.address}</Text>
                    </View>
                    {this.state.selectedLocation == 0 && <IonIcons name="ios-checkmark" size={30} color={commonColors.theme} style={{ marginRight: 15 }} />}
                </TouchableOpacity>}
                <View style={{ height: 0.5, width: '100%', backgroundColor: commonColors.background }} />
                {homeLocation.address.length > 0 && <TouchableOpacity onPress={() => this.setState({ selectedLocation: 1 })}
                    style={{ flexDirection: 'row', height: 40, alignItems: 'center', marginVertical: 8 }}>
                    <IonIcons name="ios-home-outline" size={20} color={commonColors.normalText} style={{ margin: 15 }} />
                    <View style={{ flex: 1, paddingRight: 15 }}>
                        <Text style={{ fontSize: 15, color: commonColors.normalText }}>Home</Text>
                        <Text numberOfLines={2} style={{ fontSize: 13, color: commonColors.textColor1 }}>{homeLocation.address}</Text>
                    </View>
                    {this.state.selectedLocation == 1 && <IonIcons name="ios-checkmark" size={30} color={commonColors.theme} style={{ marginRight: 15 }} />}
                </TouchableOpacity>}
                <View style={{ height: 0.5, width: '100%', backgroundColor: commonColors.background }} />
                {workLocation.address.length > 0 && <TouchableOpacity onPress={() => this.setState({ selectedLocation: 2 })}
                    style={{ flexDirection: 'row', height: 40, alignItems: 'center', marginVertical: 8 }}>
                    <IonIcons name="ios-briefcase-outline" size={20} color={commonColors.normalText} style={{ margin: 15 }} />
                    <View style={{ flex: 1, paddingRight: 15 }}>
                        <Text style={{ fontSize: 15, color: commonColors.normalText }}>Work</Text>
                        <Text numberOfLines={2} style={{ fontSize: 13, color: commonColors.textColor1 }}>{workLocation.address}</Text>
                    </View>
                    {this.state.selectedLocation == 2 && <IonIcons name="ios-checkmark" size={30} color={commonColors.theme} style={{ marginRight: 15 }} />}
                </TouchableOpacity>}
                {customLocation != undefined && <TouchableOpacity onPress={() => this.setState({ selectedLocation: 3 })}
                    style={{ flexDirection: 'row', height: 40, alignItems: 'center', marginVertical: 8 }}>
                    <IonIcons name="ios-time-outline" size={20} color={commonColors.normalText} style={{ margin: 15 }} />
                    <View style={{ flex: 1, paddingRight: 15 }}>
                        <Text style={{ fontSize: 15, color: commonColors.normalText }}>{customLocation.title}</Text>
                        <Text numberOfLines={2} style={{ fontSize: 13, color: commonColors.textColor1 }}>{customLocation.address}</Text>
                    </View>
                    {this.state.selectedLocation == 3 && <IonIcons name="ios-checkmark" size={30} color={commonColors.theme} style={{ marginRight: 15 }} />}
                </TouchableOpacity>}

            </View>
        )
    }

    renderDeliveryMode() {
        return (
            <View style={{ marginTop: 8, backgroundColor: 'white' }}>
                <Text style={{ fontSize: 15, color: commonColors.textColor2, marginVertical: 16, marginLeft: 10 }}>When</Text>
                <TouchableOpacity onPress={() => this.setState({ selectedMode: 0 })}
                    style={{ flexDirection: 'row', height: 40, alignItems: 'center', marginVertical: 8 }}>
                    <IonIcons name="ios-send-outline" size={26} color={commonColors.normalText} style={{ margin: 15 }} />
                    <View style={{ flex: 1, paddingRight: 15 }}>
                        <Text style={{ fontSize: 15, color: commonColors.normalText }}>As soon as possible</Text>
                    </View>
                    {this.state.selectedMode == 0 && <IonIcons name="ios-checkmark" size={30} color={commonColors.theme} style={{ marginRight: 15 }} />}
                </TouchableOpacity>
                <View style={{ height: 0.5, width: '100%', backgroundColor: commonColors.background }} />
                <TouchableOpacity disabled={true} onPress={() => this.setState({ selectedMode: 1 })}
                    style={{ flexDirection: 'row', height: 40, alignItems: 'center', marginVertical: 8 }}>
                    <IonIcons name="ios-home-outline" size={26} color={commonColors.normalText} style={{ margin: 15 }} />
                    <View style={{ flex: 1, paddingRight: 15 }}>
                        <Text style={{ fontSize: 15, color: commonColors.textColor1 }}>Fri, 22 Dec, 5:15-5:45 PM</Text>
                    </View>
                    {this.state.selectedMode == 1 && <IonIcons name="ios-checkmark" size={30} color={commonColors.theme} style={{ marginRight: 15 }} />}
                </TouchableOpacity>
            </View>
        )
    }

    Done() {
        switch (this.state.selectedLocation) {
            case 0:
                Cache.selectedLocation = Cache.currentLocation;
                break;
            case 1:
                Cache.selectedLocation = Cache.currentUser.user.homeLocation
                break;
            case 2:
                Cache.selectedLocation = Cache.currentUser.user.workLocation
                break;
            case 3:
                Cache.selectedLocation = Cache.customLocation
                break;
        }
        Cache.selectedLocationIndex = this.state.selectedLocation

        UtilService.saveLocalObjectData('selectedLocation', Cache.selectedLocation);
        UtilService.saveLocalObjectData('customeLocation', Cache.customLocation);
        UtilService.saveLocalObjectData('selectedLocationIndex', Cache.selectedLocationIndex);

        if ( this.props.update ) this.props.update()
        Actions.pop()
    }
    render() {

        return (
            <View style={styles.container}>
                <NavTitleBar
                    buttons={commonStyles.NavCloseButton}
                    onBack={this.onBack}
                    title={'Delivery Details'}
                />
                {this.renderAutoComplete()}
                {!this.state.filtering && this.renderLocations()}
                {!this.state.filtering && this.renderDeliveryMode()}

                <TouchableOpacity onPress={() => this.Done()} activeOpacity={0.7} style={{
                    position: 'absolute', bottom: 0, height: 48, width: '100%',
                    backgroundColor: commonColors.theme, justifyContent: 'center', alignItems: 'center'
                }}>
                    <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 15 }}>DONE</Text>
                </TouchableOpacity>
            </View>

        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: commonColors.background
    },

})
