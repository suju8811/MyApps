/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { PureComponent } from 'react';
import {
  AppRegistry,
} from 'react-native';

import App from './src/App'

import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import * as reducers from './src/reducer';
import { createStore, applyMiddleware, combineReducers } from 'redux';
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const reducer = combineReducers(reducers);
const store = createStoreWithMiddleware(reducer);

class User extends PureComponent {
  constructor(props) {
    super(props);
  }

  render(){
    return(
      <Provider store={store}>
        <App/>
      </Provider>
    )
  }
}

AppRegistry.registerComponent('User', () => User);